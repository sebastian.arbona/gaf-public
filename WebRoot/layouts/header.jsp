<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ page language="java"%>
<%@page import="com.asf.security.SessionHandler"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<script type="text/javascript">
	var $j = jQuery.noConflict();
	$j(document).ready(
			function() {
				inicializar();
				mostrarLogos("logoCabeceraEmpresa", "Logo Cabecera Empresa");

				function inicializar() {
					$j('#logoCabeceraEmpresa').hide();
				}

				function mostrarLogos(div, img) {
					$j.ajax({
						url : "logo.config?tipoPersonalizacion=" + img
								+ "&servicio=ddbb",
						success : function(data) {
							if (data != "") {
								$j('#' + div).show();
							} else {
								$j('#' + div).hide();
							}
						}
					});
				}

			});
</script>

<bean:define id="transferenciasPendientes"
	value="${sessionScope.SessionHandler.transferenciasPendientes}" />

<div id="header">
	<a href="${pageContext.request.contextPath}/" id="btnLogo">Sistema
		de Gesti�n de Cr�ditos</a>
	<div id="logoCabeceraEmpresa">
		<img class="logo-cli" alt="Sistema de Gesti�n de Cr�ditos"
			src='logo.config?tipoPersonalizacion=Logo Cabecera Empresa&servicio=ddbb'
			title="Sistema de Gesti�n de Cr�ditos">
	</div>
	<logic:greaterThan value="0" name="transferenciasPendientes">
		<div id="transferencias" align="center"
			style="background-color: #BABDB6; color: #2E3436">
			<b>-- Existen ${transferenciasPendientes} transferencias
				pendientes de recepci�n -- </b>
		</div>
	</logic:greaterThan>
</div>

<%--este div se usa para ocultar el menu impresiones javascripts --%>
<div id="boxMnu" class="noPrint">
	<asf:menu />
</div>
<br>