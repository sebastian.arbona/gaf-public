<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="com.asf.security.SessionHandler"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf" %>
<%@page import="com.asf.security.BusinessPersistance" %>

<html:html>
<head>
<!-- meta http-equiv="X-UA-Compatible" content="edge" /-->
<!-- meta charset="UTF-8" -->
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/">
<title><tiles:getAsString name="title" /></title>

<!-- Styles -->

<!-- Bootstrap -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/bootstrap/css/bootstrap.min.css" type="text/css">

<!-- DataTables / Editor -->

<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/dataTables/jquery.dataTables.min.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/dataTables/buttons.dataTables.min.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/dataTables/select.dataTables.min.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/dataTables/editor.dataTables.min.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/dataTables/css/datatable-civitas.css" type="text/css">
<!-- Fonts -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">


<!-- Select2 -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/select2/select2.min.css" type="text/css" media="screen" />

<!-- Sweet Alert -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/sweetalert/sweetalert2.min.css" type="text/css" media="screen" />

<!-- DateTimePicker -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/datetimepicker/bootstrap-datetimepicker.css" type="text/css" media="screen" />


<!-- Metis Menu -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/metis/metisMenu.css" type="text/css" media="screen" /> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/metis/mm-vertical.css" type="text/css" media="screen" /> 

<!-- HoldOn -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/holdOn/HoldOn.min.css" type="text/css" media="screen" /> 

<!-- Civitas -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/civitas/template-civitas.css" type="text/css" media="screen" /> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/civitas/civitasgrh.css" type="text/css" media="screen" /> 
 
<!-- +Styles -->
<style>
body {font-size: 14px;}
</style>

<!-- Scripts -->

<!-- Jquery -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/jquery/jquery-1.11.3.min.js"></script>

<!-- Icon -->
<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico"/>



<script type="text/javascript">
var $j = jQuery.noConflict();
var pathSistema = "${pageContext.request.contextPath}";
// 	$j.fn.select2.defaults.set( "theme", "bootstrap" );
</script>

</head>

<body>
	<div id="wrapper" class="clase-general">
		<div id="page-content-wrapper">
			<div id="content" class="container">
				<tiles:insert attribute="body" />
			</div>
		</div>
	</div>

	<footer class="clase-general">
	<div id="preloader"></div>
	<div class="row">
	<div class="span6">
		<p class="usuario">
			Usuario Actual: <em><%=com.asf.security.SessionHandler.getSessionHandler().getCurrentUser()%></em>
		</p>
		<p class="fecha">
			Fecha Actual: <em><%=java.text.DateFormat.getDateInstance(DateFormat.SHORT).format(new Date())%></em>
		</p>
	</div>
	<div class="span2 ver">
		


		<div class="margin-bottom">
        <div class="btn-group">
          <p class="btn btn-primary"><i class="fa fa-youtube-play"></i> Tutorial</p>
          <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
            <span class="fa fa-caret-up" title="Toggle dropdown menu"></span>
          </a>
          <ul class="dropdown-menu video footer">
			<li><a href="javascript:abrir('videos/video1.html')">1.- Alta Legajos</a></li>
          	<li><a href="javascript:abrir('videos/video2.html')">2.- Proceso de Liquidación</a></li>
          	<li><a href="javascript:abrir('videos/video3.html')">3.- Remuneraciones</a></li>
          	<!-- li><a href="javascript:abrir('video4.html')">4 Recibo de Sueldo</a></li>
          	<li><a href="javascript:abrir('video5.html')">5 Reporte de Liquidacion</a></li>
          	<li><a href="javascript:abrir('video6.html')">6 Conceptos y Variables</a></li>
        	<li><a href="javascript:abrir('video7.html')">7 Planilla Rubricada</a></li -->
          </ul>
        </div>
      </div>
	</div>
	<div class="logo">
		<img alt="GRH"
			src="${pageContext.request.contextPath}/images/logoa.svg"
			title="Civitas GRH">
	</div>
</footer>
</body>
</html:html>
<!-- Scripts -->

<!-- BootStrap -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/bootstrap/js/bootstrap.min.js"></script>

<!-- DataTables / Editor -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/jquery.dataTables.min.js"></script>
<%-- <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/js/datatables.min.js"></script> --%>
<!-- <script language="JavaScript" type="text/javascript" src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
<%-- <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/js/dataTables.buttons.min.js"></script> --%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/js/dataTables.select.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/js/percentageBars.js"></script>


<!-- 
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/dataTables.buttons.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/dataTables.select.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/dataTables.editor.min.js"></scri<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/estilos2/lib/bootstrap/css/bootstrap-responsive.min.css"> --%>pt>
-->

<!-- Select2 -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/select2/select2.min.js"></script>

<!-- Metis Menu -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/metis/metisMenu.js"></script>

<!-- Sweet Alert -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/sweetalert/sweetalert2.min.js"></script>

<!-- DateTimePicker -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/datetimepicker/moment-with-locales.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/datetimepicker/bootstrap-datetimepicker.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/datetimepicker/calendario-civitas.js"></script>

<!-- HoldOn -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/holdOn/HoldOn.min.js"></script>

<!-- Civitas -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/dataTable-civitas.js"></script>
<%-- <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/formateador-civitas.js"></script> --%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/highcharts-civitas.js"></script>
<!--  <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/lcs-witch-civitas.js"></script> Componente no resuelto aun (Trabaja Pablo) -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/select2-civitas.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/template-civitas.js"></script>

<!-- Funciones comunes -->
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/civitas/crud.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/components.js"></script>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/estilos2/lib/dataTables/js/dataTable-es.js"></script>
	
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js"></script>


<script> 
function abrir(url) { 
open(url,'','top=10,left=100,width=1000,height=800') ; 
} 
</script>
