<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<html:html>
<head>
<base
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/frames.css"
	type="text/css">
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/functions.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/ajax.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/prototype1.6.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/ajax.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/scriptaculous/scriptaculous.js"></script>
</head>
<body>
	<center>
		<tiles:insert attribute="body" />
	</center>

</body>
</html:html>