<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html:html>
<head>
<base
	href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/">
<title><tiles:getAsString name="title" /></title>
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/estilos_public_FTyC.css"
	type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/bootstrap-datetimepicker.css" type="text/css">
	
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/functions.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/prototype1.6.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/ajax.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/comprobante.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/scriptaculous/scriptaculous.js"></script>
<script language="JavaScript" type="text/javascript" 
	src="${pageContext.request.contextPath}/scripts/civitas/highcharts-civitas.js"></script>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/sweetalert2.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/sweetalert2.min.css" type="text/css">
	
<script type="text/javascript">
//var $j = jQuery.noConflict();
var pathSistema = "${pageContext.request.contextPath}";
</script>
</head>
<body>
	<div id="wrapper" style="position: relative">
		<tiles:insert attribute="header" />
		<div class="contentArea">
			<div id="contentArea">
				<div id="content">
					<tiles:insert attribute="body" />
				</div>
			</div>
		</div>
		<div id="footer">
			<div id="sessionData">
				<h4>
					Usuario Actual: <em><%=com.asf.security.SessionHandler.getSessionHandler().getCurrentUser()%></em>
				</h4>
				<h4>
					Fecha Actual: <em><%=java.text.DateFormat.getDateInstance(DateFormat.SHORT).format(new Date())%></em>
				</h4>
			</div>
		</div>
	</div>
</body>
</html:html>