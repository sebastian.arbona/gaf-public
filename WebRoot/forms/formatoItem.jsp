<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">
	Formato del Ítem de Importación
</div>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	
	<html:hidden property="entity.tipo" value="0" />
	
	<table border="0">

		<logic:notPresent parameter="load">
			<html:hidden property="entity.nuevo" value="true" />
			<tr>
				<th>
					Formato de Importación:
				</th>
				<td>
					<asf:select name="entity.id.idformato"
						entityName="com.asf.hibernate.mapping.Formatoimportacion"
						listCaption="detalle" listValues="idformato" />
				</td>
			</tr>
			<tr>
				<th>
					Item:
				</th>
				<td>
					<asf:select name="entity.id.item"
						filter="categoria='Item_Importacion'" orderBy="codigo"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="descripcion" listValues="codigo"
						value="AbmForm.entity.id.item" />
				</td>
			</tr>

		</logic:notPresent>
		<logic:present parameter="load">
			<html:hidden property="entity.id.idformato"
				value="${ AbmForm.entity.id.idformato }" />
			<tr>
				<th>
					Formato de Importación:
				</th>
				<td>
					<b>${ param.formatoImportacion }</b>
				</td>
			</tr>
			<tr>
				<th>
					Item:
				</th>
				<td>
					<b><bean:write name="AbmForm"
							property="entity.id.itemTpf(Item_Importacion_GAT)" />
					</b>
				</td>
			</tr>
		</logic:present>

		<tr>
			<th>
				Posición:
			</th>
			<td>
				<asf:text property="AbmForm.entity.id.posicion" type="long"
					maxlength="10" readonly="${!empty param.load }" />
			</td>
		</tr>
		<tr>
			<th>
				Longitud:
			</th>
			<td>
				<asf:text name="AbmForm" property="entity.longitud" type="long"
					maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Tipo de Dato:
			</th>
			<td>
				<asf:select name="entity.tipodato" filter="categoria='Importacion'"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="descripcion" listValues="codigo"
					value="AbmForm.entity.tipodato" />
			</td>
		</tr>

	</table>

	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>

	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
