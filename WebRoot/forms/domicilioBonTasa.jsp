<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@page import="com.asf.util.DirectorHelper"%>

<%
	pageContext.setAttribute("claveGMaps", DirectorHelper.getString("gmaps"));
%>

<script type="text/javascript">
	function terminadaLaCargaDelMapa() {
		cargarMapa(null);
	}

	function ocultarFrame(oculta) {
		if (oculta) {
			$("mapagmap").style.display = 'none';
		} else {
			$("mapagmap").style.display = 'block';
		}
	}
</script>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="guardar" styleId="accion" />
	<html:hidden property="process.personaTitular.id"
		styleId="idPersonaTitular"
		value="${ProcessForm.process.personaTitular.id}" />
	<html:hidden property="process.idBonTasa" styleId="idBonT"
		value="${ProcessForm.process.idBonTasa}" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<table border="0" align="center">
		<tr>
			<th>Id:</th>
			<td><html:text property="process.domicilio.id" readonly="true" />
			</td>
		</tr>
		<tr>
			<th>Tipo:</th>
			<td><asf:select name="process.domicilio.tipo"
					nullText="Seleccione el Tipo de Domicilio" nullValue="true"
					entityName="Tipificadores" listCaption="codigo,descripcion"
					listValues="codigo" value="${ProcessForm.process.domicilio.tipo}"
					filter="codigo='Dom. Proyecto' OR codigo='Especial'"></asf:select>
			</td>
		</tr>
		<tr>
			<th>Pa�s:</th>
			<td><asf:select name="process.idpais" entityName="Pais"
					listCaption="nombrePais" listValues="idPais"
					value="${ProcessForm.process.domicilio.provincia.idpais}"
					filter="nombrePais LIKE '%' ORDER BY nombrePais" /></td>
		</tr>
		<tr>
			<th>Provincia:</th>
			<td><asf:lselect attribs="onblur='cargarMapa();'"
					property="process.domicilio.provincia_id" entityName="Provin"
					listCaption="deta08" listValue="codi08"
					value="${ProcessForm.process.domicilio.provincia_id}"
					linkFK="idpais" linkName="process.idpais" orderBy="deta08"
					nullValue="true" nullText="Seleccione la Provincia" /></td>
		</tr>
		<tr>
			<th>Departamento:</th>
			<td><html:text size="73" onfocus="cargarMapa();"
					property="process.domicilio.departamentoNom"
					value="${ProcessForm.process.domicilio.departamentoNom}"
					styleId="departamentoNom" maxlength="80" readonly="true" /></td>
		</tr>
		<tr>
			<th>Localidad:</th>
			<td><asf:selectpop name="process.domicilio.localidad_id"
					title="Localidad"
					columns="C&oacute;digo, CP, Localidad, Departamento, Provincia"
					captions="id.localidad.idlocalidad,id.localidad.cp,id.localidad.nombre,id.departamento.nombre,id.localidad.provin.deta08"
					caseSensitive="true" values="id.localidad.idlocalidad"
					entityName="com.nirven.creditos.hibernate.LocalidadDepartamento"
					cantidadDescriptors="3"
					value="${ProcessForm.process.domicilio.localidad_id}"
					onChange="changeLocalidad();" />
				<div id="nombreDepartamento"></div></td>
		</tr>
		<tr>
			<th>Barrio:</th>
			<td colspan="3"><asf:selectpop
					name="process.domicilio.barrio_id" title="Barrio"
					columns="Codigo,Nombre" captions="codiBrr,descBrr" values="codiBrr"
					entityName="com.civitas.hibernate.persona.Barrio"
					cantidadDescriptors="1"
					value="${ProcessForm.process.domicilio.barrio_id}" />
				<a href="javascript:void(0);"
				onclick="popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaGenericaAction.do?do=newEntity&entityName=Barrio&forward=BarrioAltaRapida');"><b>Registrar
						Barrio</b> </a></td>
		</tr>
		<tr>
			<th>Calle:</th>
			<td><asfgat:callepopup attribs="onblur='cargarMapa();'"
					property="process.domicilio.calle_id"
					value="${ProcessForm.process.domicilio.calle_id}" />
				<a href="javascript:void(0);"
				onclick="popUpBigBig('${pageContext.request.contextPath}/actions/CalleAction.do?do=newEntity&entityName=Calle&forward=CalleAltaRapida');"><b>Registrar
						Calle</b> </a></td>
		</tr>
		<tr>
			<th>Calle:</th>
			<td><html:text size="73" onblur="cargarMapa();"
					property="process.domicilio.calleNom" readonly="true"
					value="${ProcessForm.process.domicilio.calleNom}"
					styleId="calleNom" maxlength="80" /></td>
		</tr>
		<tr>
			<th>Observacion:</th>
			<td id="calle1" colspan="3"><html:text size="73"
					onblur="cargarMapa();" property="process.domicilio.observacion"
					value="${ProcessForm.process.domicilio.observacion}" styleId="obs"
					maxlength="80" /></td>
		</tr>
		<tr>
			<th>N�mero:</th>
			<td><html:text onfocus="cargarMapa();"
					property="process.domicilio.numero"
					value="${ProcessForm.process.domicilio.numero}" styleId="numero"
					maxlength="5" /></td>
		</tr>
		<tr>
			<th>Manzana / Piso:</th>
			<td><html:text onfocus="cargarMapa();"
					property="process.domicilio.manzana"
					value="${ProcessForm.process.domicilio.manzana}" styleId="mza"
					maxlength="2" /></td>
		</tr>
		<tr>
			<th>Lote / Departamento:</th>
			<td><html:text onfocus="cargarMapa();"
					property="process.domicilio.lote"
					value="${ProcessForm.process.domicilio.lote}" styleId="lote"
					maxlength="2" /></td>
		</tr>
	</table>
	<center>
		<iframe width="100%" name="mapagmap" id="mapagmap" height="320px"
			src="${pageContext.request.contextPath}/gmaps.jsp" scrolling="no"
			style="display: none"></iframe>
	</center>
	<br>
	<html:submit onfocus="cargarMapa();" value="Guardar" />
	<input type="button" value="Cancelar" onclick="cancelar();" />
</html:form>
<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');

	//Carga del Mapa.
	//M�s oportunidades de encontrar una ubicaci�n determinada, si se le da el siguiente formato a la b�squeda:
	//Direcci�n, ciudad, estado o Direcci�n, ciudad, c�digo postal..
	function cargarMapa() {
		if ('${claveGMaps}' != null && '${claveGMaps}' != 0
				&& '${claveGMaps}' != '') {
			var provincia1 = "";
			var provincia2 = "";
			var provincia0 = "";
			var departamento1 = "";
			var departamento2 = "";
			var departamento0 = "";
			var localidad1 = "";
			var localidad2 = "";
			var localidad0 = "";
			var calle1 = "";
			var calle2 = "";
			var calle0 = "";
			var cpostal1 = "";
			var cpostal2 = "";
			var cpostal0 = "";
			var numero1 = "";
			var numero2 = "";
			var numero0 = "";
			var direccion = "";
			var pais = $('process.idpais');
			var nombrePais = "";
			//pais
			nombrePais = pais.options[pais.selectedIndex].text;
			//provincia
			provincia1 = getElem('process.domicilio.provincia_id').options[getElem('process.domicilio.provincia_id').selectedIndex].text;
			provincia2 = getElem('provinciaNom').value;
			provincia0 = (provincia1 != null && provincia1 != ''
					&& provincia1 != undefined && provincia1 != '<<NONE>>') ? provincia1
					: provincia2;
			//departamento
			departamento1 = getElem('departamentoNom').value
			departamento2 = "";
			departamento0 = (departamento1 != null && departamento1 != ''
					&& departamento1 != undefined && departamento1 != '<<NONE>>') ? departamento1
					: departamento2;
			//localidad
			localidad1 = getElem('process.domicilio.localidad_id').options[getElem('process.domicilio.localidad_id').selectedIndex].text
					.split("-")[1];
			localidad2 = getElem('localidadNom').value;
			localidad0 = (localidad1 != null && localidad1 != ''
					&& localidad1 != undefined && localidad1 != '<<NONE>>') ? localidad1
					: localidad2;
			//codigo postal
			cpostal1 = getElem('process.domicilio.localidad_id').options[getElem('process.domicilio.localidad_id').selectedIndex].text
					.split("-")[0];
			cpostal2 = "";
			cpostal0 = (cpostal1 != null && cpostal1 != ''
					&& cpostal1 != undefined && cpostal1 != '<<NONE>>') ? cpostal1
					: cpostal2;
			//calle
			//    calle1 = getElem('process_domicilio_calle_idDescriptor').value;
			calle1 = getElem('calleNom').value;
			calle2 = '';
			calle0 = (calle1 != null && calle1 != '' && calle1 != undefined && calle1 != '<<NONE>>') ? calle1
					: calle2;
			//numero
			numero1 = getElem('numero').value;
			numero2 = '';
			numero0 = (numero1 != null && numero1 != '' && numero1 != undefined && numero1 != '<<NONE>>') ? numero1
					: numero2;
			direccion = provincia0 + " , " + localidad0 + " , " + cpostal0
					+ " , " + calle1 + " , " + numero0 + " , " + nombrePais;

			window.frames["mapagmap"].buscarDireccion(direccion);
		}
	}// fin cargarMapa.-

	function cancelar() {
		var url = '';
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=cancelar&';
		url += 'process.idBonTasa=${ProcessForm.process.idBonTasa}';
		window.location = url;
	}//fin  cancelar.-

	function changeLocalidad() {
		buscarDepartamento();
		cargarMapa();
	}

	function buscarDepartamento() {
		var localidad = $('process.domicilio.localidad_id');
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarDepartamento&idLocalidad="
				+ localidad.value;
		retrieveURL(url, buscarDepartamentoResponse);
	}

	function buscarDepartamentoResponse(responseText) {
		if (responseText && responseText.length > 0) {
			$('departamentoNom').setValue(responseText);
		}
	}

	function buscarCalles() {
		var localidad = $('process.domicilio.localidad_id');
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarCalles&idLocalidad="
				+ localidad.value;
		retrieveURL(url, buscarCallesResponse);
	}

	function buscarCallesResponse(responseText) {
		if (responseText && responseText.length > 0) {
			$('').setValue(responseText);
		}
	}

	Event.observe(window, 'load', function() {
		this.name = "Domicilio";
		parent.push(this);

		if ('${claveGMaps}' != null && '${claveGMaps}' != 0
				&& '${claveGMaps}' != '') {
			terminadaLaCargaDelMapa();
		}
	});
</script>
