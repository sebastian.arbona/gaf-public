<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title"><b>Ente recaudador</b></div>
<br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
<html:hidden property="entity.id"/>
<html:hidden property="entity.cuenBa"/>
<html:hidden property="entity.idcaja"/>

<table border="0"  width="70%">
<tr>
	<th width="40%">C�digo:</th>
	<td>
		<html:text property="entity.id" readonly="true"/>
	</td>
</tr>

<tr>
	<th>Nombre:</th>
	<td>
		<html:text property="entity.detaBa" maxlength="13"/>
	</td>
</tr>

<tr>
	<th>Recaudador:</th>
	<td>
		<asf:select name="entity.recaudador" listCaption="Si,No"
					listValues="true,false" value="${AbmForm.entity.recaudador}"/>
	</td>
</tr>
<tr>
	<th>Financiador:</th>
	<td>
		<asf:select name="entity.financiador" listCaption="Si,No"
					listValues="true,false" value="${AbmForm.entity.financiador}"/>
	</td>
</tr>
<tr>
	<th>Bonificador:</th>
	<td>
		<asf:select name="entity.bonificador" listCaption="Si,No"
					listValues="true,false" value="${AbmForm.entity.bonificador}"/>
	</td>
</tr>


</table>

<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>	
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>