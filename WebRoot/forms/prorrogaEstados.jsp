<%@ page language="java"%>
<%@page import="com.asf.cred.business.SolicitudProrrogaProcess"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<%pageContext.setAttribute( "agregaEstado", SolicitudProrrogaProcess.agregaEstado ); %>
<%pageContext.setAttribute( "agregaObs",    SolicitudProrrogaProcess.agregaObs ); %>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" enctype="multipart/form-data" styleId="ProcessForm" >
	<html:hidden property="process.cid"/>
    <html:hidden property="process.action" value="guardarEstadoProrroga" styleId="action"/>
	<html:hidden property="process.agrega" styleId="agrega"/>
	
	<br/>
    <div style="width:70%" align="left"><html:errors/></div>
    <br/>
    <table>
    	<tr>
    		<th>Prórroga Nro: </th>
    		<td>
    			<html:text property="process.idSolicitudProrroga" readonly="true">
    			</html:text>
    		</td>
    	</tr>
    </table>
    <br/>
    <table>
		<tr>
			<td>
				<input name="funcion" type="radio" onclick="habilitarFuncion( false );" id="agregaEstado">Modificar Estado
			</td>
			<td>
				<input name="funcion" type="radio" onclick="habilitarFuncion( false );" id="agregaObs">Agregar Observación
			</td>
		</tr>
	</table>
<br/>
<table>
	<tr id="filaEstado">
		<th>Estado: </th>
	    <td>
	    	 <asf:select name="process.idEstado" entityName="Estado" listCaption="idEstado,nombreEstado"
            			 listValues="idEstado"
                         value="${ProcessForm.process.idEstado}"
                         nullValue="true" nullText="Seleccione un Estado"
                         filter="tipo = 'Prorroga' AND manual = true"
                         attribs="onChange='mostrarCampos(this.value);'"/>
	   </td>
	</tr>
	<tr id="nroRes">
		<th>Nro. Resolución: </th>
		<td>
			<html:text property="process.numeroResolucion"></html:text>
		</td>
	</tr>
	<tr id="fecRes">
		<th>Fecha de Resolución: </th>
		<td>
			<asf:calendar property="ProcessForm.process.fechaResolucion" />
		</td>	
	</tr>
	<tr id="filaObs">
		<th>Detalle Observación: </th>
		<td>
			<html:textarea property="process.detalleObservacion" 
			               onkeypress="caracteres(this,1000,event)" cols="60" rows="5"/>
		</td>
	</tr>
	<tr id="doc">
		<th>Documento: </th>
		<td colspan="3">
			<html:file property="process.formFile" styleId="formFile"/>
		</td>
	</tr>
    <tr id="filaObsEstado">
    	<th>Observaciones: </th>
        <td>
	        <html:textarea property="process.obsEstado"
		                   onkeypress="caracteres(this,1000,event)" cols="60" rows="5"/>
        </td>
    </tr>
    
    </table>
    <br>
    <html:submit onclick="return guardar();">Guardar</html:submit>
    <html:submit onclick="return cancelar();">Cancelar</html:submit>
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
        src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<script language='javascript'>
//CONTROLES
	var accion            = $('accion');
	var formulario        = $('oForm');
	var ctrlAgrega        = $('agrega');
	var ctrlAgregaEstado  = $('agregaEstado');
	var ctrlAgregaObs     = $('agregaObs');
	var ctrlFilaEstado    = $('filaEstado');
	var ctrlFilaObs       = $('filaObs');
	var ctrlFilaObsEstado = $('filaObsEstado');

	ocultar("nroRes","fecRes","doc");
//AL CARGAR LA PÁGINA

function guardar(){
	var action = document.getElementById('action');
	action.value = 'guardarEstadoProrroga';
}//fin guardar.-


habilitarFuncion(true);

	function cancelar()
    {
		var action = document.getElementById('action');
		action.value = 'cancelar';
		document.getElementById('formFile').disabled = true;
    }//fin cancelar.-

    function mostrarCampos( valor ){
		
    	if(valor==24){
			mostrar("nroRes","fecRes","doc");
			document.getElementById('formFile').disabled = false;
        }else{
        	ocultar("nroRes","fecRes","doc");
        	document.getElementById('formFile').disabled = true;
        }
    }

    function habilitarFuncion( primeraVez )
    {
    	if(primeraVez)
    	{
    		ctrlAgregaEstado.checked = true;
    	}
    	
    	if(ctrlAgregaEstado.checked)
    	{
			ctrlAgrega.value                = '${agregaEstado}';
        	ctrlFilaEstado.style.display    = "";
        	ctrlFilaObs.style.display       = "none";
        	ctrlFilaObsEstado.style.display = "";
    	}
        else if(ctrlAgregaObs.checked)
    	{
        	ctrlAgrega.value                = '${agregaObs}';
        	ctrlFilaObs.style.display       = "";
        	ctrlFilaEstado.style.display    = "none";
        	ctrlFilaObsEstado.style.display = "none";
    	}
    }//fin habilitarFuncion.-

    Event.observe(window, 'load', function() 
    {
  		this.name = "Estados y Observaciones";
    	parent.push(this);
    });
</script>