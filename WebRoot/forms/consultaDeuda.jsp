<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">
	Consulta de Deuda
	<logic:equal name="ProcessForm" property="process.mostrar" value="personas">
		ID ${ProcessForm.process.idConsultaDeuda}
	</logic:equal>
</div>


	<logic:notEmpty name="ProcessForm" property="process.errors">
		<html:form action="/actions/process.do?do=process&processName=${param.processName}">
			<html:hidden property="process.action" value="cargar" styleId="action"/>
			<div id="errores" style="width: 70%" align="left">
				<html:errors />
			</div>
			<html:submit value="Volver"></html:submit>
		</html:form>
	</logic:notEmpty>

<logic:empty name="ProcessForm" property="process.errors">
	<logic:equal name="ProcessForm" property="process.mostrar" value="lista">
		<html:form action="/actions/process.do?do=process&processName=${param.processName}">
			<br><br>

			<fieldset style="border-style: ridge; width: 53%;">
				<legend style="color: gray;" >Filtro de Consulta de Deuda</legend>
		
				<table>
					<tr>
						<th>Consulta ID:</th>
						<td>
							<asf:text property="ProcessForm.process.idConsultaDeuda" id="idConsultaDeuda" type="long" maxlength="8"/>
						</td>
					</tr>
					<tr>
						<th>Fecha desde:</th>
						<td>
							<asf:calendar property="ProcessForm.process.fechaDesdeStr" value="${process.fechaDesdeStr}" />
						</td>
					</tr>
					<tr>
						<th>Fecha hasta:</th>
						<td>
							<asf:calendar property="ProcessForm.process.fechaHastaStr" value="${process.fechaHastaStr}"/>
						</td>
					</tr>
					<tr>
						<th>Estado:</th>
						<td>
							<asf:select name="process.estado" value="ProcessForm.process.estado" listValues="0,1,2,3" listCaption="Todos,No Iniciados,Iniciados,Finalizados" ></asf:select>
						</td>
					</tr>
					<tr>
						<th>Persona:</th>
						<td>
							<asf:selectpop	name="process.idpersonaStr" values="id"
								columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L, Personer�a"
								captions="idpersona,nomb12,nudo12,cuil12,tipo12" entityName="com.civitas.hibernate.persona.Persona"
								value="${ProcessForm.process.idpersonaStr}"/>
						</td>
					</tr>
					<tr>
						<td><input type="button" value="Consultar" onclick="consultar();"></td>
					</tr>
				</table>
			</fieldset>

			<div class="row">
				<input type="button" value="Nueva Consulta de Deuda" onclick="nuevaConsultaDeuda();">
				<input type="button" value="Nueva Consulta desde Carga Masiva" onclick="nuevaConsultaDeudaMasiva();">
			</div>
			
			<div style="overflow: auto;" class="grilla">
				<display:table style="border: solid; border-color: #CCCCCC;" name="ProcessForm" property="process.consultaDeudaList"  id="consultaDeuda" 
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}" 
					export="true" pagesize="25">
					<display:setProperty name="report.title" value="Consultas de Deuda"/>
			        <display:caption>
			            Estado: ${ProcessForm.process.estadoStr}			            
			        </display:caption>			
					<display:column property="id"></display:column>
					<display:column title="Mendoza Fiduciaria">
						<logic:equal name="consultaDeuda" property="mf" value="true">SI</logic:equal>
						<logic:notEqual name="consultaDeuda" property="mf" value="true">NO</logic:notEqual>
					</display:column>
					<display:column title="Fondo Vitivin�cola">
						<logic:equal name="consultaDeuda" property="fv" value="true">SI</logic:equal>
						<logic:notEqual name="consultaDeuda" property="fv" value="true">NO</logic:notEqual>
					</display:column>
					
					<display:column title="Emergencia Agropecuaria">
						<logic:equal name="consultaDeuda" property="ea" value="true">SI</logic:equal>
						<logic:notEqual name="consultaDeuda" property="ea" value="true">NO</logic:notEqual>
					</display:column>
					<display:column title="Deuda Provincial del Fondo">
						<logic:equal name="consultaDeuda" property="ft" value="true">SI</logic:equal>
						<logic:notEqual name="consultaDeuda" property="ft" value="true">NO</logic:notEqual>
					</display:column>
					<display:column title="CFI Turismo">
						<logic:equal name="consultaDeuda" property="cfi" value="true">SI</logic:equal>
						<logic:notEqual name="consultaDeuda" property="cfi" value="true">NO</logic:notEqual>
					</display:column>
					<display:column property="fechaStr" title="Fecha"></display:column>
					<display:column title="Personas" media="html excel">
						<ol>
							<logic:notEmpty name="consultaDeuda" property="personas">
							<logic:iterate id="persona" name="consultaDeuda" property="personas">
								<li>${persona.persona.nomb12}</li>
							</logic:iterate></logic:notEmpty>
						</ol>
					</display:column>
					<display:column title="Personas" media="pdf">
						<logic:iterate id="persona" name="consultaDeuda" property="personas">
							${persona.persona.nomb12}
						</logic:iterate>
					</display:column>
					<display:column title="Proceso" media="html excel">
						<logic:empty name="consultaDeuda" property="proceso">
							<a href="javascript: iniciar(${consultaDeuda.id});">Iniciar</a>
							<a href="#" onclick="eliminar(${consultaDeuda.id});return false;">Eliminar</a>
						</logic:empty>
						<logic:notEmpty name="consultaDeuda" property="proceso">
							<logic:notEmpty name="consultaDeuda" property="proceso.end">
								<strong>Finalizado</strong>
								<br/>Inicio: ${consultaDeuda.startStr}, ${consultaDeuda.iniciador}
								<br/>Fin ${consultaDeuda.endStr}
							</logic:notEmpty>
							<logic:empty name="consultaDeuda" property="proceso.end">
								<strong>Iniciado</strong>
								<br/>${consultaDeuda.startStr}, ${consultaDeuda.iniciador}
							</logic:empty>
						</logic:notEmpty>
					</display:column>
					<display:column title="Proceso" media="pdf">
						<logic:empty name="consultaDeuda" property="proceso"><a href="javascript: iniciar(${consultaDeuda.id});">Iniciar</a></logic:empty>
						<logic:notEmpty name="consultaDeuda" property="proceso">
							<logic:notEmpty name="consultaDeuda" property="proceso.end">
								Finalizado
								Inicio: ${consultaDeuda.startStr}, ${consultaDeuda.iniciador}
								Fin ${consultaDeuda.endStr}
							</logic:notEmpty>
							<logic:empty name="consultaDeuda" property="proceso.end">
								Iniciado
								${consultaDeuda.startStr}, ${consultaDeuda.iniciador}
							</logic:empty>
						</logic:notEmpty>
					</display:column>
					<display:column title="Imprimir" media="html">
						<a href="javascript: imprimirReporte('${consultaDeuda.id}');">IMPRIMIR</a>
					</display:column>
				<%--<display:column title="Personas"  media="html">
						<a href="javascript: listarGrupoDePersonas('${consultaDeuda.id}');">PERSONAS</a>
					</display:column>--%>
						
				</display:table>
				
			</div>
			<br>
		</html:form>
	</logic:equal>

	<logic:equal name="ProcessForm" property="process.mostrar" value="personas">
		<html:form action="/actions/process.do?do=process&processName=${param.processName}">
			<html:hidden property="process.action" styleId="action"/>

			<br><br>
			<table style="border: solid; border-color: #CCCCCC;">
				<tr>
					<th align="center">Nombre</th>
					<th align="center">DNI</th>
					<th align="center">CUIL</th>
					<th align="center">Car�cter</th>
				</tr>
	
				<logic:iterate name="ProcessForm" property="process.consultaDeudaPersonaList" id="consultaDeudaPersona" indexId="index">
					<tr>
						<td align="center">
							${consultaDeudaPersona.persona.nomb12}
						</td>
						<td align="center">
							${consultaDeudaPersona.persona.nudo12}
						</td>
						<td align="center">
							${consultaDeudaPersona.persona.cuil12}
						</td>
						<td align="center">
							${consultaDeudaPersona.caracter}
						</td>
					</tr>
				</logic:iterate>
			</table>

			<br>
			<button onclick="cancelar();">Cancelar</button>
		</html:form>
	</logic:equal>

	<!--  ConsultaDeudaPersona -->
	<logic:equal name="ProcessForm" property="process.mostrar" value="nuevaConsultaDeudaPersona">
		<html:form action="/actions/process.do?do=process&processName=${param.processName}" >
			<html:hidden property="process.action" value="guardarConsultaDeudaPersona" styleId="action"/>
			<html:hidden property="process.detalleQuitar" styleId="detalleQuitar"/>

			<table style="border: solid; border-color: #CCCCCC;">
				<tr>
					<th align="center">Mendoza Fiduciaria</th>
					<th align="center">Fondo Vitivin�cola</th>
					<th align="center">Emergencia Agropecuaria</th>
					<th align="center">Deuda Provincial del Fondo</th>
					<th align="center">CFI Turismo</th>
				</tr>
				<tr>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.mendozaFiduciaria"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.fondoVitivinicola"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.emergenciaAgropecuaria"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.deudaProvincialDelFondo"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.cfiTurismo"/>
					</td>
				</tr>
			</table>
						
			<div class="title">
				A&ntilde;adir Persona
			</div>

			<table style="border: solid; border-color: #CCCCCC;">
				<tr>
					<th>Persona:</th>
					<td>
						<asf:selectpop name="process.idpersona" values="id"
							columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L, Personer�a"
							captions="idpersona,nomb12,nudo12,cuil12,tipo12" entityName="com.civitas.hibernate.persona.Persona"/>
					</td>
				</tr>

				<tr>
					<th>Car�cter:</th>
					<td colspan="3">
						<select id="caracter">
							<option value="1" selected>Tomador</option>
							<option value="2">Fiador</option>
							<option value="3">Garante</option>
							<option value="4">Director Titular o Socio Gerente</option>
							<option value="5">Otros</option>
						</select>
						<html:hidden property="process.caracterSelected" styleId="caracterSelected"/> 
					</td>
				</tr>

				<tr>
					<td colspan="3" style="text-align: center;">
						<input type="submit" value="Agregar" onclick="agregarPersona();"/>
					</td>
				</tr>
			</table>

			<br>

			<display:table name="ProcessForm" property="process.consultaDeudaPersonaArray" export="false" id="reportTable">
	      		<display:column title="Nombre" property="persona.nomb12"/>
	      		<display:column title="DNI" property="persona.nudo12" />
	      		<display:column title="CUIL" property="persona.cuil12" />
	      		<display:column title="Car�cter" property="caracter" />
	      		<display:column title="Eliminar" style="cursor: pointer;">
	      			<a onclick="quitar(${reportTable_rowNum});">Eliminar</a>
	      		</display:column>
			</display:table>

			<br><br>
			<input type="submit" value="Guardar">
			<button onclick="cancelar();">Cancelar</button>

		</html:form>
	</logic:equal>
	<!--  /ConsultaDeudaPersona -->
	
	
	<!--  ConsultaDeudaMasiva -->
	<logic:equal name="ProcessForm" property="process.mostrar" value="nuevaConsultaDeudaMasiva">
		<html:form action="/actions/process.do?do=process&processName=${param.processName}" >
			<html:hidden property="process.action" value="guardarConsultaDeudaPersona" styleId="action"/>
			<html:hidden property="process.detalleQuitar" styleId="detalleQuitar"/>

			<table style="border: solid; border-color: #CCCCCC;">
				<tr>
					<th align="center">Mendoza Fiduciaria</th>
					<th align="center">Fondo Vitivin�cola</th>
					<th align="center">Emergencia Agropecuaria</th>
					<th align="center">Deuda Provincial del Fondo</th>
					<th align="center">CFI Turismo</th>
				</tr>
				<tr>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.mendozaFiduciaria"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.fondoVitivinicola"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.emergenciaAgropecuaria"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.deudaProvincialDelFondo"/>
					</td>
					<td align="center">
						<html:checkbox name="ProcessForm" property="process.cfiTurismo"/>
					</td>
				</tr>
			</table>					
			
			<!-- Control Examinar para importaci�n masiva -->
			<div class="title">
				Importaci&oacute;n Masiva de Personas
			</div>
			
			<iframe	class="iframe"		
				src="${pageContext.request.contextPath}/actions/importacionConsultaDeuda.do?do=loadImportarConsultaDeuda"
				style="border: 0" width="100%" height="200px" name="ImportacionForm"></iframe>
				
			<div class="row">
				<input type="submit" value="Guardar">
				<button onclick="cancelar();">Cancelar</button>		
			</div>
		</html:form>

	</logic:equal>
	<!--  ConsultaDeudaMasiva -->
</logic:empty>

<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>

<script type="text/javascript">
var fechaDesde;
var fechaHasta;
var idConsultaDeuda;
var action;
var caracterSelected;
var estado;
var idpersonaStr;
jQuery.noConflict();
jQuery(document)
		.ready(
				function($) {

					fechaDesde 				= document.getElementById('process.fechaDesdeStr');
					fechaHasta 				= document.getElementById('process.fechaHastaStr');
					idConsultaDeuda         = document.getElementById("idConsultaDeuda");
					action			        = document.getElementById("action");
					caracterSelected		= document.getElementById("caracterSelected");
					estado					= document.getElementById("process.estado");
					idpersonaStr			= document.getElementById("process.idpersonaStr");
});


function validaFecha() {
	retorno = true;

	console.log(fechaDesde);
	console.log(fechaHasta);

	var arrayFechaDesde = fechaDesde.value.split("/");
	var arrayFechaHasta = fechaHasta.value.split("/");

	console.log(arrayFechaDesde);
	console.log(arrayFechaHasta);

	if(retorno && arrayFechaDesde.length == 3 && arrayFechaDesde[2].length == 4 && arrayFechaHasta.length == 3 && arrayFechaHasta[2].length == 4) {
		if(arrayFechaDesde[2] > arrayFechaHasta[2]) {
			alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
			retorno=false;
		}
		else {
			if(arrayFechaDesde[2] == arrayFechaHasta[2] && arrayFechaDesde[1] > arrayFechaHasta[1]) {
				alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
				retorno=false;
			}
			else {
				if(arrayFechaDesde[2] == arrayFechaHasta[2] && arrayFechaDesde[1] == arrayFechaHasta[1] && arrayFechaDesde[0] > arrayFechaHasta[0]) {
					alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
					retorno=false;
				}
			}
		}
	}

	return retorno;
}

function consultar() {
	var url = "";

	if(!validaFecha())
		return false;

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=consultar";
	url += "&process.fechaDesdeStr=" + fechaDesde.value;
	url += "&process.fechaHastaStr=" + fechaHasta.value;
	url += "&process.idConsultaDeuda=" + idConsultaDeuda.value;
	url += "&process.estado=" + estado.value;
	url += "&process.idpersonaStr=" + idpersonaStr.value;

	document.location = url;
}

/*
function listarGrupoDePersonas(idConsulta) {
	var url = "";

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=listarGrupoDePersonas";
	url += "&process.idConsultaDeuda=" + idConsulta;

	document.location = url;
}*/

function iniciar(idConsulta) {
	var url = "";

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=iniciar";
	url += "&process.idConsultaDeuda=" + idConsulta;

	document.location = url;
}

function eliminar(idConsulta) {
	if(!confirm("Est� deguro de Eliminar el registro ?"))
		return ;
	var url = "";

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=eliminar";
	url += "&process.idConsultaDeuda=" + idConsulta;

	document.location = url;
	return ;
}

function nuevaConsultaDeuda() {
	var url = "";

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=nuevaConsultaDeuda";

	document.location = url;
}

function nuevaConsultaDeudaMasiva() {
	var url = "";

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=nuevaConsultaDeudaMasiva";

	document.location = url;
}

function agregarPersona() {
	caracterSelected.value = $('caracter').options[$('caracter').selectedIndex].value;
	action.value = "agregarPersona";
	return true;
}

function quitar(i) {
	action.value = 'eliminarPersona';

	var indice = $('detalleQuitar');
	indice.value = i;

	var form = document.forms['ProcessForm'];
	form.submit();

	return true;
}

function cancelar() {
	action.value = 'cargar';
	
	var form = document.forms["ProcessForm"];
	form.submit();
}

function imprimirReporte(idConsulta) {
	var url = "";

	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=ConsultaDeuda";
	url += "&process.action=imprimirReporte";
	url += "&process.idConsultaDeuda=" + idConsulta;

	document.location = url;
}
</script>