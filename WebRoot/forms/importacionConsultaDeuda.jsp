<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@page import="com.asf.cred.business.ProgressStatus"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>

<html:form action="/actions/importacionConsultaDeuda.do?do=importarConsultaDeuda" method="post" enctype="multipart/form-data" styleId="oForm">
	<html:hidden property="idCaratula" value="${ImportacionForm.idCaratula}"/>
	<html:hidden property="idDetalleArchivo" value="${ImportacionForm.idDetalleArchivo}"/>
	<table style="border: solid; border-color: #CCCCCC; width: 582px; float:left;">
		<tr>
			<th>
				Seleccione Archivo:
			</th>
			<td>
				<html:file name="ImportacionForm" property="file" styleId="archivo"
					size="50" disabled="${ImportacionForm.deshabilitado}" />
			</td>
			<td style="vertical-align: middle;">
				<html:submit onclick="return importar();" disabled="${ImportacionForm.deshabilitado}">Importar</html:submit>
			</td>
		</tr>
	</table>
	<br />
	<div style="clear: both;"></div>
	<logic:notEmpty name="ImportacionForm" property="consultaDeudaPersona">
		<display:table name="ImportacionForm" property="consultaDeudaPersona" export="false" id="reportTable" style="float: left;">
     		<display:column title="Nombre" property="persona.nomb12"/>
     		<display:column title="DNI" property="persona.nudo12" />
     		<display:column title="CUIL" property="persona.cuil12" />
     		<display:column title="Car�cter" property="caracter" />
     		<display:column title="Eliminar" style="cursor: pointer;">
     			<a onclick="quitar(${reportTable_rowNum});">Eliminar</a>
     		</display:column>
		</display:table>		
	</logic:notEmpty>	
	<logic:notEmpty name="ImportacionForm" property="resumen">
		<div style="text-align: left; font-size: 12px; margin-top: 45px;">${ImportacionForm.resumen}</div>
	</logic:notEmpty>
	<div style="width: 70%" align="center">
		<html:messages id="mensajes"/>
	</div>
	<div style="text-align: left; font-size: 12px">
		<html:errors />
	</div>	
	<logic:notEmpty name="ImportacionForm" property="nroReporte">
		<a href="javascript://" onclick="return reporte();">Errores de
			Importaci�n Nro: <bean:write name="ImportacionForm"
				property="nroReporte" /><a /><br>
	</logic:notEmpty>
	<br/>
	<br/>
</html:form>
${ImportacionForm.script}
<%ProgressStatus.onProcess = false;%>
<script type="text/javascript">
var fechaDesde          	= $("process.fechaDesdeStr");
var fechaHasta      		= $("process.fechaHastaStr");
var idConsultaDeuda         = $("idConsultaDeuda");
var action			        = $("action");
var caracterSelected		= $("caracterSelected");
var estado					= $("process.estado");
var idpersonaStr			= $("process.idpersonaStr");


    function importar(){
    	/*window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
					"width=450, height=180");  */     	      
       	return true;
    }

    function eliminar(){
        var oForm = $('oForm');
        oForm.action = '${pageContext.request.contextPath}/actions/importacionCobranza.do?do=eliminar'; 
        return true;
    }

    function reporte(){
        popUpBig( "${pageContext.request.contextPath}/actions/process.do?do=process&processName=ReporteErrorImportacion&process.nroreporte=<bean:write name="ImportacionForm" property="nroReporte"/>");
    }

    function quitar(i) {
    	action.value = 'eliminarPersona';

    	var indice = $('detalleQuitar');
    	indice.value = i;

    	var form = document.forms['ProcessForm'];
    	form.submit();

    	return true;
    }
</script>
