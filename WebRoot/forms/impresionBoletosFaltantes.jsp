<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Impresi�n de Boletos</div>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion"/>
	<html:hidden property="process.moneda" styleId="moneda"/>
	<html:errors/>
	<html:radio property="process.moneda" value="pesos" styleId="pesos" onclick="seleccionar('pesos');">Boletos en Pesos</html:radio>
	<html:radio property="process.moneda" value="dolares" onclick="seleccionar('dolares');">Boletos en D�lares</html:radio>
	<br/>
	<br/>
	<html:submit value="Imprimir" onclick="enviarAccion('imprimir');"/>	
</html:form>
<script>
var moneda = $('moneda'); 
if(moneda.value == "" || moneda.value == $('pesos').value){
	$('pesos').checked = true;	
	$('moneda').value = 'pesos';
}else{
	$('dolares').checked = true;
	$('moneda').value = 'dolares';
}

function enviarAccion(accion){
	$('accion').value = accion;
}

function seleccionar(seleccion){
	$('moneda').value = seleccion;
}
</script>