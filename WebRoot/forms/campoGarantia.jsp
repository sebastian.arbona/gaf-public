<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">
	A�adir Campo - ${param.nombre}
</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true&paramName[0]=tipoGarantia.id&paramComp[0]=%3d"
	styleId="AbmForm">

	<html:hidden property="entity.idTipoGarantia" value="${paramValue[0]}" />
	<html:hidden property="entity.id" />
    <html:hidden property="paramValue[0]" value="${paramValue[0]}" />
	<html:hidden property="nombre" value="${param.nombre}" />
	
	<table border="0">
		<tr>
			<th>
				Id:
			</th>
			<td>
				<asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true" />
			</td>
		</tr>
				
		<tr>
			<th>
				Nombre del Campo:
			</th>
			<td>
				<asf:text property="entity.nombre" value="${AbmForm.entity.nombre}"
					type="Text" maxlength="250" name="AbmForm" />
			</td>
		</tr>
		<tr>
			<th>
				Tipo de Campo:
			</th>
			<td>
				<asf:select name="entity.tipoDeCampo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoDeCampo}" listCaption="getDescripcion"
					listValues="getCodigo"
					filter="categoria = 'garantia.tipocampo'  order by codigo" />
			</td>
		</tr>
		<tr>
			<th>
				Tama�o:
			</th>
			<td>
				<asf:text property="entity.maximoCaracteres"
					value="${AbmForm.entity.maximoCaracteres}" type="Long"
					maxlength="20" name="AbmForm" />
			</td>
		</tr>
		<tr>
			<th>
				Obligatorio:
			</th>
			<td>
				<asf:select name="entity.obligatorio"
					value="${AbmForm.entity.obligatorio}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Familia de Campo:
			</th>
			<td>
				<asf:select name="entity.familia"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.familia}" listCaption="getDescripcion"
					listValues="getCodigo"
					filter="categoria = 'garantia.familiacampo' order by codigo" />
			</td>
		</tr>
		<tr>
			<th>Orden:</th>
			<td>
				<asf:text property="entity.orden"
					value="${AbmForm.entity.orden}" type="Long"
					maxlength="20" name="AbmForm"/>
			</td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>