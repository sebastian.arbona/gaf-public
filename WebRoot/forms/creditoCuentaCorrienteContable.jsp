<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 15px">Cuenta Corriente</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=CreditoCuentaCorrienteContable"
	styleId="oForm">

	<html:hidden name="ProcessForm" property="process.idObjetoi" />
	<html:hidden name="ProcessForm" property="process.idBoleto"/>
	<html:hidden name="ProcessForm" property="process.personaId" />
	<html:hidden name="ProcessForm" property="process.cid" />
	<html:hidden property="process.action" styleId="action" value="buscar"/>

<table style="margin-bottom: 20px">
	<tr id="fechas">
		<th>Desde fecha:</th>
		<td><asf:calendar property="ProcessForm.process.desdeStr" value="${process.desdeStr}" /></td>
		<th>Hasta fecha:</th>
		<td><asf:calendar property="ProcessForm.process.hastaStr" value="${process.hastaStr}" /></td>
		<td style="text-align: center">
				<html:submit>Buscar</html:submit>
		</td>
		<td style="text-align: center">
				<html:submit onclick="imprimirCtaCteContable();">Imprimir PDF</html:submit>
		</td>
	</tr>
</table>	
<br/>	

	<div class="grilla">
		<display:table name="ProcessForm" property="process.beans"
			id="reportTable" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoCuentaCorrienteContable">
			<display:caption>Proyecto ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12} - Periodo: ${ProcessForm.process.desdeStr} al ${ProcessForm.process.hastaStr}</display:caption>
			
			<display:setProperty name="report.title" value="Cuenta Corriente"></display:setProperty>

			<display:column title="N� Asiento">
				<a href="${ProcessForm.process.contextoGAF}/actions/process.do?do=process&processName=Movimientos&process.action=exportar&process.tipoExportacion=PDF&process.idAs=${reportTable.idAsiento}" target="_blank">${reportTable.nroAsiento}</a>
			</display:column>
			<display:column title="Tipo Comp." property="tipoBoleto"
				media="pdf excel xml" />
			<display:column title="Tipo Comp." style="min-width: 80px"
				media="html">
				<logic:equal value="Factura" name="reportTable"
					property="tipoBoleto">
					<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=imprimirBoletos&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.cid=${ProcessForm.process.cid}">
						${reportTable.tipoBoleto} </a>
				</logic:equal>
				<logic:equal value="Recibo" name="reportTable" property="tipoBoleto">
					<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=imprimirPago&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoletoRecibo}&process.idCaratula=${reportTable.idCaratula}&process.cid=${ProcessForm.process.cid}">
						${reportTable.tipoBoleto} </a>
				</logic:equal>
				<logic:equal value="Nota Debito" name="reportTable"
					property="tipoBoleto">
					<a href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualDebito&process.importe=${reportTable.debe}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.cid=${ProcessForm.process.cid}">
						${reportTable.tipoBoleto} </a>
				</logic:equal>
				<logic:equal value="Nota Credito" name="reportTable"
					property="tipoBoleto">
					<logic:notEqual value="Pago Gastos" name="reportTable" property="detalle">
						<a href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualCredito&process.importe=${reportTable.haber}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.cid=${ProcessForm.process.cid}">
							${reportTable.tipoBoleto} </a>
					</logic:notEqual>
				</logic:equal>
				<logic:equal value="Minuta Debito" name="reportTable"
					property="tipoBoleto">
					<a href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualDebito&process.importe=${reportTable.debe}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.cid=${ProcessForm.process.cid}&process.minuta=true">
						${reportTable.tipoBoleto} </a>
				</logic:equal>
				<logic:equal value="Minuta Credito" name="reportTable"
					property="tipoBoleto">
					<a href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualCredito&process.importe=${reportTable.haber}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.cid=${ProcessForm.process.cid}&process.minuta=true">
						${reportTable.tipoBoleto} </a>
				</logic:equal>
				<logic:notEqual value="Factura" name="reportTable"
					property="tipoBoleto">
					<logic:notEqual value="Recibo" name="reportTable"
						property="tipoBoleto">
						<logic:notEqual value="Nota Credito" name="reportTable"
							property="tipoBoleto">
							<logic:notEqual value="Nota Debito" name="reportTable"
								property="tipoBoleto">
								<logic:notEqual value="Minuta Debito" name="reportTable"
									property="tipoBoleto">
									<logic:notEqual value="Minuta Credito" name="reportTable"
										property="tipoBoleto">
										${reportTable.tipoBoleto}
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual>
					</logic:notEqual>
				</logic:notEqual>

				<logic:equal value="Desembolso" name="reportTable"
					property="detalle">
					<a href="${ProcessForm.process.contextoGAF}/actions/process.do?do=process&processName=PagoOP&process.action=imprimirReciboPago&process.ordenpago.id=${reportTable.idOrdenPago}&process.ordenpago.ordenpago=${reportTable.numeroOrdenPago}&process.ordenpago.ejercicio=${reportTable.ejercicioOrdenPago}"
						target="_blank"> Recibo </a>
				</logic:equal>
				<logic:equal value="Pago Gastos" name="reportTable" property="detalle">
					<a href="${ProcessForm.process.contextoGAF}/actions/process.do?do=process&processName=PagoOP&process.action=imprimirReciboPago&process.ordenpago.id=${reportTable.idOrdenPago}&process.ordenpago.ordenpago=${reportTable.numeroOrdenPago}&process.ordenpago.ejercicio=${reportTable.ejercicioOrdenPago}"
						target="_blank"> Recibo </a>
				</logic:equal>
			</display:column>
			<display:column title="N� Comp." class="center"
				property="numeroBoleto" />
				<display:column class="center" title="Fecha Proceso"
				property="fechaProceso"
				decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column class="center" title="Fecha Vencimiento/Aplicaci�n"
				property="fechaVencimiento"
				decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column class="center" title="Fecha Pago"
				property="fechaGeneracion"
				decorator="com.asf.displayDecorators.DateDecorator" />
				
			<display:column title="Detalle" property="detalle" />

			<logic:notEqual value="true" name="ProcessForm" property="process.esDolares">
				<display:column class="right" title="Debe" property="debe"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Haber" property="haber"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Saldo" property="saldo"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</logic:notEqual>

			<logic:equal value="true" name="ProcessForm" property="process.esDolares">
					<display:column class="right" title="Debe U$S" property="debe"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Haber U$S" property="haber"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Saldo U$S" property="saldo"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Cot." property="cotizaMov"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Debe $" property="debePeso"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Haber $" property="haberPeso"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Saldo $" property="saldoPeso"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			
			</logic:equal>
		</display:table>
	</div>

</html:form>
<script type="text/javascript">
function imprimirCtaCteContable() {
	var accion = $('action');
	accion.value = 'imprimirCtaCteContable';
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>