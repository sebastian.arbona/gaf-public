<%@page import="com.asf.util.DoubleHelper"%>
<%@page import="com.asf.gaf.hibernate.Recaudacion"%>
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.4.2.min.js"></script>

<script type="text/javascript">
<!--
	$j = jQuery.noConflict();
	
	function actualizarSaldo(check) {
		var saldoInicial = new Number($j("#saldoInicial").val());
		$j("input.select").each(function(i) {
			if (this.checked == true) {
				saldoInicial -= new Number(this.value);
			}
		});

		saldoInicial = new Number(Math.round(saldoInicial * 100) / 100);
		
		if (saldoInicial < 0) {
			saldoInicial += new Number(check.value);
			check.checked = false;
		}
		
		$j("#saldo").val(saldoInicial);
		$j("#saldoActual").html(saldoInicial.toString());
	}
//-->
</script>

<div class="title">
	Vinculación de registros del txt con el Extracto Bancario seleccionado
</div>
<html:form action="/actions/process.do?do=process&processName=ConciliacionRecaudaciones" styleId="ProcessForm">
	<html:hidden property="process.accion" styleId="accion" value="vincular"/>
	<html:hidden property="process.cid"/>
	
	<input type="hidden" id="saldo" value="${ProcessForm.process.recaudacion.saldoTemporal}"/>
	<input type="hidden" id="saldoInicial" value="${ProcessForm.process.recaudacion.saldoTemporal}"/>
	
	<bean:define id="rec" name="ProcessForm" property="process.recaudacion"/>
	
	<table width="100%">
		<tr>
			<th>Fecha Movimiento</th>
			<td>${rec.fechamovimientoStr}</td>
			<th>Ente Rec</th>
			<td>${rec.bancos.detaBa}</td>
			<th>Sucursal</th>
			<td>${rec.bancos.codiBa}</td>
		</tr>
		<tr>
			<th>Cuenta Bancaria</th>
			<td>${rec.bancos.cuenBa}</td>
			<th>Cod Operacion</th>
			<td>${rec.codigoOperacion}</td>
			<th>Nro Operacion</th>
			<td>${rec.nroOperacion}</td>
		</tr>
		<tr>
			<th>Importe</th>
			<td class="right"><%= DoubleHelper.getString(((Recaudacion) rec).getImporte()) %></td>
			<th>Saldo Real</th>
			<td class="right"><%= DoubleHelper.getString(((Recaudacion) rec).getSaldo()) %></td>
			<th>Saldo Actual</th>
			<td><div id="saldoActual" style="text-align: right"><%= String.format("%,.2f", ((Recaudacion) rec).getSaldoTemporal()) %></div></td>
		</tr>
	</table>
	
	<table width="100%">
		<tr>
			<th>Titular</th>
			<th>Proyecto</th>
			<th>Expediente</th>
			<th>Fecha Pago/Acreditación</th>
			<th>Importe</th>
			<th style="width: 140px">Nro Cheque</th>
			<th>Nro Op</th>
			<th>Estado</th>
			<th>Vincular</th>
		</tr>
		<logic:notEmpty name="ProcessForm" property="process.detallesSeleccionar">
		<logic:iterate id="det" name="ProcessForm" property="process.detallesSeleccionar">
		<tr>
			<td>${det.objetoi.persona.nomb12}</td>
			<td>${det.numeroProyecto}</td>
			<td>${det.objetoi.expediente}</td>
			<td>${det.fechaAcreditacionValor}</td>
			<td class="right">${det.importeStr}</td>
			<td style="text-align: center">${det.numeroValor}</td>
			<td>${det.numeroOperacion}</td>
			<td>${det.estadoActual.estado}</td>
			<td><input type="checkbox" class="select" name="det-${det.id}" value="${det.importe}" onclick="actualizarSaldo(this);"/></td>
		</tr>
		</logic:iterate>
		</logic:notEmpty>
	</table>
	
	<div style="margin-top: 30px">
		<html:submit value="Volver"></html:submit>
	</div>
</html:form>
<iframe width=174 height=220 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>