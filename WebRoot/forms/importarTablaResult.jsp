<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<br><br>
<logic:empty name="result" scope="request">
<div align="left" style="width:70%">
<ul>	
<li>La importación fue exitosa</li>
</ul>

</logic:empty>
<logic:notEmpty name="result" scope="request">
<ul>
	<li>Ocurrieron los siguientes errores en la importación:</li>
</ul>

	<display:table name="result">
				
	</display:table>

</logic:notEmpty>