<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<br>
<br>

<html:form action="/actions/process.do?do=process&processName=BonTasaRequisitos" styleId="oForm" enctype="multipart/form-data">
  <html:hidden name="ProcessForm" property="process.accion" value="guardarReq" />
  <html:hidden name="ProcessForm" property="process.idBonTasa" value="${ProcessForm.process.idBonTasa}"/>
  
  <table>   
      <tr>
        <th></th>
        <th>
          ${ProcessForm.process.nombre}
        </th>
      </tr>
      <tr>
        <th>
          ID:
        </th>
        <td>
          <html:text readonly="true" property="process.id"  name="ProcessForm" value="${ProcessForm.process.id}" />
        </td>
      </tr>
      <tr>
        <th>
          Fecha Cumplimiento:
        </th>
        <td>
          <asf:calendar property="ProcessForm.process.fechaCumpStr" />
        </td>
      </tr>
      <tr>
        <th>
          Observaciones:
        </th>
        <td>
          <html:textarea property="process.observaciones" name="ProcessForm" value="${ProcessForm.process.observaciones}" cols="80" rows="5" />
        </td>
      </tr>
      <tr>
        <th>
          Documento:
        </th>
        <td>
          <html:file property="testFile(${ProcessForm.process.archivo})" value=""/>
          <logic:notEmpty name="ProcessForm" property="process.carga.archivo">
            <html:button property="idEntity" value="ver"
              onclick="window.open('${pageContext.request.contextPath}/actions/RequisitosAction.do?do=descargar&idEntity=${carga.id}')"
            />
          </logic:notEmpty>
        </td>
      </tr>
    
  </table>
 
    
      <html:submit value="Guardar"/>
    <input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<script type="text/javascript">
function cancelar(){
	window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=BonTasaRequisitos&process.accion=listar&process.idBonTasa=${ProcessForm.process.idBonTasa}";
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
