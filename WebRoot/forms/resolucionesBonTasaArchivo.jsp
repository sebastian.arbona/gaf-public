<%@ page language="java"%>
<%@ page import="com.nirven.creditos.hibernate.FuncionPersonaResolucion" %>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>


<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />


<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>



<script language="javascript" type="text/javascript">

	$j = jQuery.noConflict();

	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
  
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
    }
	
    function actualizar( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&expediente=" + inputIn.value;
    	urlDinamica += "&" + defecto;

    	return urlDinamica;
    }
</script>
<body class=" yui-skin-sam">
  
  
  <div class="title">
   Nuevo Documento
  </div>
  <br>
  <br>
  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
    <html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
    <html:hidden name="ProcessForm" property="process.estado_id" styleId="estado_id" />
    <html:hidden name="ProcessForm" property="process.idBonTasa" styleId="idBonTasa" />
    
    <div style="width: 70%" align="left">
      <html:errors />
    </div>
    
    	<table>
			<tr>
	    		<th><span style="required" class="required">(*)</span>&nbspDocumento:</th>
	    		<td colspan="3">
	        		<html:file name="ProcessForm" property="process.theFile"/>
	    		</td>
			</tr> 
			<tr>
	            <th>Detalle: </th>
	            <td>
	                <html:textarea property="process.detalleArchivoResolucion" cols="60" rows="4" onkeypress="caracteres(this,255,event);"></html:textarea>
	            </td>
        	</tr>
		</table>

      
  
  	<div style="margin-top: 10px">
			<input type="button" value="Guardar Documento" onclick="guardar();"/>
			<input type="button" value="Cancelar" onclick="cancelar();"/>
		</div>
  </html:form>
  



<script language="JavaScript" type="text/javascript">




$j(document).ready(

				function($j) {					
									
//					cargarTipoResolucion();
//					mostrarDef();
				});
				
				
</script>

<script type="text/javascript">

function guardar() {

	accion.value = 'guardarArchivoResolucion';
	var form = $('oForm');
	form.submit();
}


function cancelar(){
	accion.value = 'listResolucionArchivo';	
	var form = $('oForm');
	form.submit();
}

</script>
