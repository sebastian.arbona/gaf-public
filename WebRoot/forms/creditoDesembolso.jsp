<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
	
<html:form
	action="/actions/desembolsoAction.do?do=save&entityName=${param.entityName}&filter=true">
	<html:hidden property="paramValue[0]" value="${AbmForm.paramValue[0]}" />
	<html:hidden property="paramName[0]" value="credito.id" />
	<html:hidden property="paramComp[0]" value="=" />
	<html:hidden property="entity.credito_id" value="${AbmForm.paramValue[0]}"
		styleId="idCredito" />
	<html:hidden property="entity.id" value="${AbmForm.entity.id}" />

	<div style="width: 70%" align="left">
		<html:errors />
	</div>

	<table border="0">
		<tr>
			<th>Numero:</th>
			<td><logic:equal name="enabled" value="true">
				<asf:text name="AbmForm" property="entity.numero"
					type="long" maxlength="6" readonly="true" />
				</logic:equal>
				<logic:equal name="enabled" value="false">
					${AbmForm.entity.numero}
				</logic:equal>
			</td>
		</tr>
		<tr>
			<th>Fecha:</th>
			<td>
				<logic:equal name="enabled" value="true">
					<asf:calendar property="AbmForm.entity.fechaStr" />
				</logic:equal>
				<logic:equal name="enabled" value="false">
					${AbmForm.entity.fechaStr }
				</logic:equal>
			</td>
		</tr>
		<tr>
			<th>Importe:</th>
			<td>
				
				<logic:equal name="enabled" value="true">
					<asf:text name="AbmForm" property="entity.importe"
						type="decimal" maxlength="13" />
				</logic:equal>
				<logic:equal name="enabled" value="false">
					${AbmForm.entity.importe}
				</logic:equal>
			</td>
		</tr>
	    <tr>
		    <th>
		      Expediente:
		    </th>
		    <td>
		    	<logic:equal name="enabled"  value="true">
			      	<asf:selectpop name="entity.expedientePago" entityName="DocumentoADE"
					value="${empty AbmForm.entity.expedientePago ? '' : AbmForm.entity.expedientePago}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI"
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					size1="20" size2="50" cantidadDescriptors="2" /> <%--  filter="tipoDeDocumento in (${tipoDocumento})"/> --%>
				</logic:equal>
		    	<logic:equal name="AbmForm" property="entity.estado" value="4">
			      	<asf:selectpop name="entity.expedientePago" entityName="DocumentoADE"
					value="${empty AbmForm.entity.expedientePago ? '' : AbmForm.entity.expedientePago}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI"
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					size1="20" size2="50" cantidadDescriptors="2" /> <%--  filter="tipoDeDocumento in (${tipoDocumento})"/> --%>
				</logic:equal>
				<logic:notEqual name="AbmForm" property="entity.estado" value="4">
					${AbmForm.entity.expedientePago }
				</logic:notEqual>
		    </td>
	    </tr>		
		<tr>
			<th>Observaci�n:</th>
			<td>
				<logic:equal name="enabled" value="true">
					<fck:editor toolbarSet="Civitas"
						instanceName="entity.observacion" width="600" height="200"
						value="${AbmForm.entity.observacion}" />
				</logic:equal>
				<logic:equal name="enabled" value="false">
					${AbmForm.entity.observacion}
				</logic:equal></td>
		</tr>

	</table>


	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<logic:equal name="AbmForm" property="entity.estado" value="4">
			<html:submit>
				<bean:message key="abm.button.save" />
			</html:submit>
		</logic:equal>
		<logic:equal name="AbmForm" property="entity.estado" value="">
			<html:submit>
				<bean:message key="abm.button.save" />
			</html:submit>
		</logic:equal>
		
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<script type="text/javascript">
	$j = jQuery.noConflict();

	$j(document)
			.ready(
					function() {
						
						if($j("#entity\\.numero").length==0)
							return;
						var idCredito = $j("#idCredito").val();
						$j
								.get(
										'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarSiguienteDesembolso',
										'idCredito=' + idCredito,
										function(data) {
											$j("#entity\\.numero").val(data);
										});
					});
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>