<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="title">Consulta de Uso de Garant&iacute;as</div>
<html:form action="/actions/process.do?do=process&processName=ConsultaObjetoiGarantia" styleId="ProcessForm">
	<html:hidden name="ProcessForm" property="process.cid"/>
	<html:hidden name="ProcessForm" property="process.action" value="buscar"/>
	
	<div class="grilla">
		<table width="100%">
			<tr>
				<th>Tipo de Garant&iacute;a</th>
				<td><asf:select name="process.idTipoGarantia" listCaption="nombre"
					value="${ProcessForm.process.idTipoGarantia}"
					entityName="com.nirven.creditos.hibernate.TipoGarantia"
					listValues="id" /></td>
			</tr>
			<tr>
				<th>Identificador Unico</th>
				<td><html:text name="ProcessForm" property="process.identificador"/></td>
			</tr>
			<tr>
				<th colspan="2">
					<html:submit value="Buscar"></html:submit>
				</th>
			</tr>
		</table>
		
		<html:errors/>
		
		<table width="100%" style="margin-top: 30px">
			<tr>
				<th>Descripci&oacute;n</th>
				<td>${ProcessForm.process.garantia.observacion}</td>
			</tr>
			<tr>
				<th>Producto de Referencia</th>
				<td>${ProcessForm.process.garantia.tipoProductoNombre}</td>
			</tr>
		</table>
		<br/>
		<h4>Proyectos Asociados</h4>
		<display:table name="ProcessForm" property="process.garantias" id="og">
			<display:column title="Proyecto">
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${og.objetoi.id}&process.action=buscar&process.personaId=${og.objetoi.persona.idpersona}">${og.objetoi.numeroAtencion}</a>
			</display:column>
			<display:column title="Línea" property="objetoi.linea.nombre" sortable="true"/>
			<display:column title="Estado Proyecto" property="objetoi.estadoActual.estado.nombreEstado" sortable="true"/>
			<display:column title="Estado Garantia" property="estadoStr" sortable="true"/>
			<display:column title="Deuda pendiente">
		      	<div id="deuda-${og.objetoi.id}" style="float: left"></div>
		      	<div id="loader-${og.objetoi.id}" style="display: none"><img src="${pageContext.request.contextPath}/images/loader.gif"/></div>
		      	<div style="float: right">
		      		<a href="javascript:void(0);" onclick="cargarDeuda(${og.objetoi.id});">Ver</a>
		      	</div>
		    </display:column>
			<display:column title="Importe const. garantia" property="valor" sortable="true" decorator="com.asf.displayDecorators.DoubleDecorator"/>
			<display:column title="Comportamiento de Pago" property="objetoi.comportamientoActual" sortable="true"/>
			<display:column title="Baja" property="baja" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
		</display:table>
	
	</div>
</html:form>
<script type="text/javascript">
function cargarDeuda(idCredito) {
	var loader = document.getElementById("loader-"+idCredito);
	loader.style.display = "inline";
	var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularDeuda";
	url += "&idCredito=" + idCredito;
	url += "&deudaVencida=true";
	retrieveURL(url, cargarDeudaResponse);
	return false;
}
function cargarDeudaResponse(responseText) {
	  var objetos = parseXml(responseText);
	  var credito = objetos.Credito;
	  var loader = document.getElementById("loader-"+credito.id);
	  loader.style.display = "none";
	  var div = document.getElementById("deuda-"+credito.id);
	  div.innerHTML = credito.deuda;
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>