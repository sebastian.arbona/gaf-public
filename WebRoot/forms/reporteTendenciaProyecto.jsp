<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.12.1.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/bootstrap/css/bootstrap-responsive.css">
  
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/style/bootstrap/js/bootstrap.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/moment-with-locales.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/bootstrap-datetimepicker.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/civitas/calendar-civitas.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/highcharts.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/highcharts-exporting.js"></script>

<div>
	<div class="title">Reporte Estadistico por Tendencia de Proyectos</div>
	<br>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
		<html:hidden property="process.action" value="consultar" styleId="action"/>
		<table border="0" class="tabla">			
        	<tr>
				<th>Fecha:</th>
				<td>
					<div class="input-group date dp-calendario" id="fecha">
			            <input id="f-fecha_mes_anio" name="f-fecha_mes_anio" type="text" class="form-control" data-date-format="MM/YYYY"/>
			            <span class="input-group-addon">
			                <span class="glyphicon glyphicon-calendar">
			                </span>
			            </span>
			        </div>
				</td>
			</tr>
			<!-- 
			<tr>
				<th>Linea:</th>
		        <td>
		          <asf:select entityName="com.nirven.creditos.hibernate.Linea" listCaption="getId,getNombre"
          			listValues="getId" name="process.idLinea" value="${ProcessForm.process.idLinea}" nullText="Todas" nullValue="true"
          			filter="nombre IS NOT NULL ORDER BY id"/>
		        </td>
			</tr>
			-->
		</table>
		<br>
			
	</html:form>

	<div>
		<button style="float: left;" type="button" onclick="consultarPeriodo()">Consultar</button>
		
		<div style="float: right;">
			<button id="limpiar">Limpiar</button>
			<button id="column"><i class="fa fa-bar-chart"></i>Columna</button>
			<button id="spline"><i class="fa fa-line-chart"></i>Spline</button>
		</div>
	</div>
	
		
	<div id="container-column" style="min-width: 400px; height: 400px; margin: 2em 0 auto;"></div>
</div>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>



<script type="text/javascript">
	var $j = jQuery.noConflict();
	var chart = null;
	
	$j(document).ready( function() {		
		inicializarDatetimepickerFecha();
		
		$j('#limpiar').click(function () {
			h_remover_categorias( chart );
			h_eliminar_series( chart );
		});

		$j('#column').click(function () {
		    h_cambiarOpcionRenderizar( chart, null, 0 );
		});

		$j('#spline').click(function () {
		    h_cambiarOpcionRenderizar( chart, null, 1 );
		});

		
		chart = Highcharts.chart('container-column', {		    
			chart: {
		        type: 'column'
		    },
			title: {
		        text: ''
		    },
		    subtitle: {
		        text: ''
		    },
		    credits: {
   				enabled: false
   			},
		    xAxis: {	        
		        type: 'category',
		        categories:[],
		        tickPixelInterval: 200,
		        title: {
		            text: 'LINEAS'
		        }
		    },
		    yAxis: [{
		        min: 0,
		        type: 'category',
		        title: {
		            text: 'CANTIDAD'
		        },
		    }],
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.2,
		            borderWidth: 0
		        }
		    },
		    series: []
		   });
		
		
	});

	function consultarPeriodo() {		
		let fecha = $j('#f-fecha_mes_anio').val();
		let jsonObject = JSON.parse('{"fecha":"'+fecha+'"}');
		//let linea = getElem('process.idLinea').value;
		//linea = (linea == "" ? null : linea);
		//let jsonObject = JSON.parse('{"idlinea":'+ linea +',"fecha":"'+fecha+'"}');
				
		return $j.ajax({
			url : pathSistema+"/actions/creditosAjaxHelperAction.do?do=consultarPeriodoProyecto",
			type: 'POST',
			data: jsonObject,
	 		dataType : 'json'
	    })
	    .done(function(json) {	    	
	    	if(json != null && json.exito){
	    		if( JSON.parse(json.resultado).length > 0 ) {
		    		if(chart.xAxis[0].categories.length > 0) {
		    			let textAceptar = "Si, agregar!";
		    			let textCancelar = "No, quiero un reporte nuevo!";
		    			confirmarModalAvanzado("Confirmar", "Desea agregar la Serie de datos al reporte actual?", "warning", json, dibujarGrafica, dibujarNuevaGrafica,textAceptar,textCancelar);
		    		}
		    		else {
		    			dibujarGrafica(json);
		    		}
	    		}
	    		else {
	    			mensajeModal('Atenci�n!', 'No se encontraron datos para el periodo ingresado.', 'warning');
	    		}
			}
		})
		.fail(function(xhr) {
			mensajeModal('Error!', 'Se prudujo un error en la consulta.', 'error');
		});
	} 
	
	
	function dibujarGrafica( json ) {		
		let periodo = json.periodo;
		let datos = JSON.parse(json.resultado);
		
		datos.forEach(element => {			
			h_agregarSerieNoRepetida( chart, element.linea );
        });			
		h_equilibrarSeries( chart );
		
		let array_valores = new Array();
		datos.forEach(element => {			
			array_valores.push([element.linea,element.cantidad]);
        });
		
		h_agregarCategorieSerieDinamico( chart, periodo, array_valores );
	}
	
	function dibujarNuevaGrafica( json ) {		
		let periodo = json.periodo;
		let datos = JSON.parse(json.resultado);
		
		h_remover_categorias( chart );
		h_eliminar_series( chart );
		
		datos.forEach(element => {			
			h_agregarSerieNoRepetida( chart, element.linea );
        });
		
		let array_valores = new Array();
		datos.forEach(element => {			
			array_valores.push([element.linea,element.cantidad]);
        });
		
		h_agregarCategorieSerieDinamico( chart, periodo, array_valores );
	}
</script>