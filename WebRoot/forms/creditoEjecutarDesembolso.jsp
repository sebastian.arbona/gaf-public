<%@page language="java"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@page import="com.asf.gaf.DirectorHandler"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<html:form
	action="/actions/process.do?do=process&processName=CreditoDesembolsos"
	styleId="ProcessForm">
	<html:hidden property="process.objetoi.id"
		value="${ProcessForm.process.objetoi.id}" />
	<html:hidden property="process.idObjetoi"
		value="${ProcessForm.process.idObjetoi}" />
	<html:hidden property="process.idDesembolso"
		value="${ProcessForm.process.idDesembolso}" />
	<html:hidden property="process.relacionId"
		value="${ProcessForm.process.relacionId}" />
	<html:hidden property="process.desembolso.estado" />

	<html:hidden property="process.action" styleId="action"
		value="ejecutar" />

	<html:errors></html:errors>

	<h3>Datos Asociados a la Ejecución del Desembolso Seleccionado</h3>
	<table>
		<tr>
			<td colspan="2"><em>Medio de pago</em></td>
		</tr>
		<tr>
			<td colspan="2"><html:radio styleId="interbanking"
					property="process.usaInterbanking" value="true"
					disabled="${ProcessForm.process.ejecutado}"
					onclick="seleccionarInterbanking();">Datos Bancarios</html:radio> <html:radio
					styleId="interbanking" property="process.usaInterbanking"
					value="false" disabled="${ProcessForm.process.ejecutado}"
					onclick="seleccionarInterbanking();">Otro</html:radio></td>
		</tr>
		<logic:equal value="true" name="ProcessForm"
			property="process.usaInterbanking">
			<tr>
				<td colspan="2"><em>Tipo de Cuenta:</em></td>
			</tr>
			<tr>
				<td colspan="2"><html:radio name="ProcessForm" styleId="propia"
						property="process.esCuentaPropia" value="true"
						onclick="seleccionarInterbanking();">Cuenta Propia</html:radio> <html:radio
						name="ProcessForm" styleId="propia"
						property="process.esCuentaPropia" value="false"
						onclick="seleccionarInterbanking();">Cuenta Persona Vinculado:</html:radio>
				</td>
			</tr>
			<tr>
				<logic:equal value="true" name="ProcessForm"
					property="process.esCuentaPropia">
					<th>Cuenta Destino:</th>
					<td><asf:select name="process.desembolso.cuentaInterbankingId"
							entityName="com.asf.gaf.hibernate.CuentaBancaria"
							listCaption="tipoCuentaBancariaStr,nroCuentaBancaria,persona.nomb12"
							listValues="idCuentaBancaria"
							value="${ProcessForm.process.desembolso.cuentaInterbankingId}"
							filter="persona.id=${ProcessForm.process.objetoi.persona.id}"
							enabled="${!ProcessForm.process.ejecutado}" /></td>
				</logic:equal>
				<logic:notEqual value="true" name="ProcessForm"
					property="process.esCuentaPropia">
					<th>Cuenta Destino:</th>
					<td><asf:select name="process.cuentaTerceroId"
							entityName="com.asf.gaf.hibernate.CuentaBancaria"
							listCaption="tipoCuentaBancariaStr,nroCuentaBancaria,persona.nomb12"
							listValues="idCuentaBancaria"
							value="${ProcessForm.process.desembolso.cuentaInterbankingId}"
							filter="persona.id IN (SELECT pv.personaVinculada.id FROM PersonaVinculada pv WHERE pv.relacion.idRelacion in (${ProcessForm.process.relacionId}) AND pv.personaTitular.id = ${ProcessForm.process.objetoi.persona.id})"
							enabled="${!ProcessForm.process.ejecutado}" /></td>
				</logic:notEqual>
			</tr>
		</logic:equal>
		<tr>
			<th>Importe Real</th>
			<logic:equal name="ProcessForm" property="process.desembolso.estado"
				value="1">
				<td><asf:text name="ProcessForm"
						property="process.desembolso.importeSolicitado" maxlength="15"
						type="decimal" readonly="true" /></td>
			</logic:equal>
			<logic:notEqual name="ProcessForm"
				property="process.desembolso.estado" value="1">
				<td><asf:text name="ProcessForm"
						property="process.desembolso.importeReal" maxlength="15"
						type="decimal" readonly="${ProcessForm.process.pendiente}" /></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Importe de Gastos</th>
			<td><asf:text name="ProcessForm"
					property="process.desembolso.importeGastos" maxlength="15"
					type="decimal" readonly="${ProcessForm.process.ejecutado}" /></td>
		</tr>
		<tr>
			<th>Importe de Gastos a Recuperar</th>
			<td><asf:text name="ProcessForm" property="process.gastos"
					maxlength="15" type="decimal" readonly="true" /></td>
		</tr>
		<asf:security action="/actions/ejecutarDesembolso.do" access="">
			<tr>
				<th>Fecha Ejecuci&oacute;n</th>
				<td><asf:calendar
						property="ProcessForm.process.desembolso.fechaRealStr" /></td>
			</tr>
		</asf:security>
		<tr>
			<th colspan="2">Observaciones</th>
		</tr>
		<tr>
			<td colspan="2"><logic:equal name="ProcessForm"
					property="process.ejecutado" value="true">
    				${ProcessForm.process.desembolso.observacion}
    			</logic:equal> <logic:equal name="ProcessForm" property="process.ejecutado"
					value="false">
					<fck:editor toolbarSet="Civitas"
						instanceName="process.desembolso.observacion" width="600"
						height="200" value="${ProcessForm.process.desembolso.observacion}" />
				</logic:equal></td>
		</tr>
		<tr>
			<td colspan="2" style="padding-top: 30px; text-align: center;">
				<a style="color: #009" onclick="imprimir();"
				href="javascript:void(0);"> Imprimir Solicitud de Desembolso </a><br />
			</td>

			<!-- 			<td colspan="2" style="padding-top: 30px; text-align: center;">
				<a style="color: #009" onclick="adjuntar();"
				href="javascript:void(0);"> adjuntar </a><br />
			</td>
 -->
		</tr>
	</table>
	<div style="width: 100%; text-align: center">
		<logic:equal value="true" name="ProcessForm"
			property="process.editable">
			<input type="button" value="Guardar" onclick="guardar();" />
		</logic:equal>
		<logic:equal value="false" name="ProcessForm"
			property="process.editable">
			<input type="button" value="Guardar" onclick="guardar();"
				disabled="disabled" />
		</logic:equal>
		<logic:notEqual value="1" name="ProcessForm"
			property="process.desembolso.estado">
			<logic:equal value="true" name="ProcessForm"
				property="process.editable">
				<input type="button" value="Solicitar a GAF" id="btnSolicitarGAF1"
					onclick="solicitarGaf(this.id);" />
			</logic:equal>
			<logic:equal value="false" name="ProcessForm"
				property="process.editable">
				<input type="button" value="Solicitar a GAF" id="btnSolicitarGAF2"
					onclick="solicitarGaf(this.id);" disabled="disabled" />
			</logic:equal>
		</logic:notEqual>
		<asf:security action="/actions/ejecutarDesembolso.do" access="">
			<logic:equal value="true" name="ProcessForm"
				property="process.pendiente">
				<logic:equal value="true" name="ProcessForm"
					property="process.desembolso.ejecutable">
					<html:submit value="Ejecutar" disabled="false"></html:submit>
				</logic:equal>
				<logic:notEqual value="true" name="ProcessForm"
					property="process.desembolso.ejecutable">
					<html:submit value="Ejecutar" disabled="true"></html:submit>
				</logic:notEqual>
			</logic:equal>
			<logic:equal value="false" name="ProcessForm"
				property="process.pendiente">
				<html:submit value="Ejecutar" disabled="true"></html:submit>
			</logic:equal>
		</asf:security>
		<input type="button" value="Cancelar" onclick="cancelar();" />
	</div>

	<iframe width="174" height="189" name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
	</iframe>

</html:form>

<script type="text/javascript">
	$j = jQuery.noConflict();

	$j(document)
			.ready(
					function() {
						$j("#interbanking")
								.change(
										function() {
											if (this.disabled == false) {
												var interbanking = this.value;
												$j(
														"#process\\.desembolso\\.cuentaInterbankingId")
														.attr(
																"disabled",
																interbanking == 'false');
											}
										}).change();
						$j("#propia")
								.change(
										function() {
											if (this.disabled == false) {
												var propia = this.value;
												$j(
														"#process\\.desembolso\\.cuentaInterbankingId")
														.attr(
																"disabled",
																propia == 'false');
											}
										}).change();
					});

	function cancelar() {
		var action = $('action');
		action.value = "load";
		var form = $('ProcessForm');
		form.submit();
	}

	function solicitar() {
		var action = $('action');
		action.value = "solicitud";
		var form = $('ProcessForm');
		form.submit();
	}

	function solicitarGaf(id) {
		document.getElementById(id).disabled = true;
		var action = $('action');
		action.value = "solicitarGaf";
		var form = $('ProcessForm');
		form.submit();
	}

	function guardar() {
		var action = $('action');
		action.value = "guardar";
		var form = $('ProcessForm');
		form.submit();
	}
	function seleccionarInterbanking() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'loadEjecucion';
		form.submit();
	}
	function imprimir() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'reporte';
		form.submit();
	}
	function adjuntar() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'adjuntarComprobantes';
		form.submit();
	}
	function imprimirRev1() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'reporteRev1';
		form.submit();
	}
	function imprimirRev2() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'reporteRev2';
		form.submit();
	}

	function adjuntarDocumentos() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'adjuntarDocumentos';
		form.submit();
	}

	function guardarCuentaBancaria() {
		var form = $('ProcessForm');
		var action = $('action');
		action.value = 'guardarCuentaBancaria';
		form.submit();
	}
</script>