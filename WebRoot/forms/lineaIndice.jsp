<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.tipoTasa"
		value="${ProcessForm.process.tipoTasa}" />
	<html:hidden property="process.action" styleId="accion" />

	<html:errors></html:errors>

	<h3>Tipo Tasa: Inter�s ${ProcessForm.process.tipoTasa}</h3>

	<table>
		<tr>
			<th>Tasa Inter�s Compensatorio</th>
			<td><asf:selectpop name="process.idIndice" title="Tasa"
					columns="Codigo, Nombre" captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.Indice"
					value="${ProcessForm.process.idIndice}" filter="tipo in (1, 4)"
					onChange="tasa();" /> <asf:text name="ProcessForm"
					id="indiceValor" readonly="true" property="process.indiceValor"
					value="${ProcessForm.process.indiceValor}" type="decimal"
					maxlength="15" /></td>
		</tr>
		<tr>
			<th>Mas</th>
			<td><html:text property="process.valorMasStr" size="15"
					onchange="calcularValorFinal();" styleId="mas" /></td>
		</tr>
		<tr>
			<th>Por</th>
			<td><asf:text id="por" name="ProcessForm"
					property="process.valorPorStr"
					attribs='onchange="calcularValorFinal();"' type="decimal"
					maxlength="15" /></td>
		</tr>
		<tr>
			<th>D�as antes</th>
			<td><html:text value="${ProcessForm.process.diasAntes}"
					property="process.diasAntes" size="15" /></td>
		</tr>
		<tr>
			<th>Tasa Inter�s Compensatorio final</th>
			<td><html:text property="process.valorFinal" readonly="true"
					size="15" styleId="valorFinal" /></td>
		</tr>
	</table>
	<div style="width: 100%; text-align: center">
		<input type="button" value="Guardar" onclick="guardar();" /> <input
			type="button" value="Cancelar" onclick="cancelar();" />
	</div>
</html:form>
<script type="text/javascript">
	$j = jQuery.noConflict();
	function tasa() {
		var idIndice = $j("#process\\.idIndice").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='
								+ idIndice, function(data) {
							$j("#indiceValor").val(data);
						});
		var form = document.getElementById('oForm');
		var action = document.getElementById('action');
		action.value = 'modificarBonificacion';
		form.submit();
	}

	function cancelar() {
		var form = document.getElementById("oForm");
		var action = document.getElementById("accion");
		action.value = 'cancelarIndice';
		form.submit();
	}

	function guardar() {
		var form = document.getElementById("oForm");
		var action = document.getElementById("accion");
		action.value = "saveIndice";
		form.submit();
	}
	function calcularValorFinal() {
		var tasa = $j("#indiceValor").val().replace(',', '.').replace('%', '');
		var mas = $j("#mas").val().replace(',', '.').replace('%', '');
		var por = $j("#por").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasa").val().replace(',', '.').replace('%', '');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinal").val(data);
						});
	}
</script>