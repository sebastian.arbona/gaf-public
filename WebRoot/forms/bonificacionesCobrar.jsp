<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">Reporte de Bonificaciones a Cobrar</div>

<html:form
	action="/actions/process.do?do=process&processName=BonificacionesCobrar"
	styleId="ProcessForm">

	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="buscar" styleId="action" />
	<html:errors />

	<table>
		<tr>
			<th>Fecha Desde</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaVencimientoDesde"></asf:calendar>
			</td>
		</tr>
		<tr>
		<tr>
			<th>Fecha Hasta</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaVencimientoHasta"></asf:calendar>
			</td>
		</tr>
		<tr>

			<th>Convenio</th>
			<td><asf:select
					entityName="com.nirven.creditos.hibernate.ConvenioBonificacion"
					listCaption="getNombre" listValues="getId"
					name="process.convenioBonificacion.id"
					value="${ProcessForm.process.convenioBonificacion.id}"
					nullValue="true"
					nullText="&lt;&lt;Seleccione Convenio Bonificacion&gt;&gt;"
					attribs="class='id'" /></td>
		</tr>

		<tr>
			<th colspan="2"><html:submit value="Listado en pantalla"></html:submit>
			</th>
		</tr>
	</table>

	<logic:notEmpty name="ProcessForm" property="process.beans">
		<div class="grilla">
			<display:table name="ProcessForm" property="process.beans" id="bean"
				export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do">
				<display:caption>Bonificaciones a Cobrar</display:caption>
				<display:column title="Numero Atencion" property="numeroAtencion"
					class="right" />
				<display:column title="Nombre" property="titularNombre" />
				<display:column title="CUIT" property="titularCuit" />
				<display:column title="Cantidad de Cuotas" property="cuotasTotales"
					class="right" />
				<display:column title="Capital Acreditado"
					property="capitalAcreditado"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"
					class="right" />
				<display:column title="Cuotas Pagadas" property="cuotasPagadas"
					class="right" />
				<display:column title="Cuotas Impagas" property="cuotasImpagas"
					class="right" />
				<display:column title="Dias Vencidos" property="diasVencida"
					class="right" />
					<display:column title="Dias Atraso" property="diasAtraso"
					class="right" />
				<display:column title="Saldo Capital" property="saldoCapital"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"
					class="right" />
				<display:column title="Numero Cuota" property="numeroCuota"
					class="right" />
				<display:column title="Fecha Vencimiento"
					property="fechaVencimiento"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Bonificacion" property="bonificacion"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"
					class="right" />
			</display:table>
		</div>
	</logic:notEmpty>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
