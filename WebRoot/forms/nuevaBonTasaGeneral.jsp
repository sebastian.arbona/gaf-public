<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>


<div class="title">Bonificaci�n de Tasa Bancaria</div>
<html:form
	action="actions/process.do?processName=NuevaBonTasaGeneral&do=process"
	styleId="ProcessForm">
	<html:hidden name="ProcessForm" property="process.accion"
		styleId="accion" />

	<h3>
		<html:errors />
	</h3>
	<ul>
		<br>
		<logic:equal name="ProcessForm" property="process.creada" value="true">
	Bonificaci�n de Tasa Creada Correctamente
	<tr>
				<input type="button" value="Atras" onclick="atras();" />
			</tr>
		</logic:equal>
		<logic:equal property="process.creada" name="ProcessForm"
			value="false">
			<tr>
				<b>Persona:</b>
				<asf:selectpop name="process.idPersona" title="Persona"
					columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
					captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
					entityName="com.civitas.hibernate.persona.Persona"
					value="${ProcessForm.process.idPersona}" onChange="selecPersona();" />
				<a href="javascript:void(0);"
					onclick="popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaAction.do?do=newEntity&entityName=Persona&forward=PersonaAltaRapida');">
					Registrar Persona </a>
			</tr>
			<table border="0" width="90%" align="left">
				<tr>
					<th>Fecha Solicitud</th>
					<td colspan="5"><asf:calendar
							property="ProcessForm.process.fechaHoyStr" readOnly="false" /></td>
				</tr>
				<tr>
					<th>Convenio</th>
					<td colspan="3"><asf:selectpop name="process.convenio_id"
							title="Convenio"
							columns="ID, Nombre, Banco, Linea, Fecha, Operatoria"
							captions="id,nombre,banco.detaBa,linea.nombre,fecha,operatoria"
							values="id"
							entityName="com.nirven.creditos.hibernate.ConvenioBonificacionExterna"
							value="${ProcessForm.process.convenio_id}" caseSensitive="true"
							filter="fechaVencimiento > current_date()"
							onChange="loadConvenio();" /></td>
				</tr>
				<tr>
					<th>Agente Financiero</th>
					<td colspan="3"><html:text property="process.detaBa"
							readonly="true" name="ProcessForm" size="80" /></td>
				</tr>
				<tr>
					<th>L&iacute;nea</th>
					<td colspan="3"><html:text property="process.linea"
							name="ProcessForm" readonly="true" size="80" /></td>
				</tr>
				<tr>
					<th>Operatoria</th>
					<td><asf:text name="ProcessForm" property="process.operatoria"
							type="text" maxlength="150" cols="2" readonly="true" /></td>
					<th>Tipo de Empresa:</th>
					<td><html:text property="process.tipoEmpresa"
							name="ProcessForm" readonly="true" /></td>
				</tr>
				<tr>
					<th>Descripci�n del destino del cr�dito a bonificar</th>
					<td colspan="3" style="height: 10px"><html:textarea
							property="process.objeto" name="ProcessForm" cols="100" rows="1" />
					</td>
				</tr>
				<tr>
					<th>Monto total del emprendimiento</th>
					<td colspan="3"><asf:text name="ProcessForm"
							property="process.montoFinanciamiento" type="decimal"
							maxlength="40" /></td>
				</tr>
				<tr>
					<th>Importe Neto de IVA</th>
					<td><asf:text maxlength="40" type="decimal"
							property="process.montoNeto" name="ProcessForm" /></td>
					<th>IVA</th>
					<td><asf:text maxlength="40" type="decimal"
							property="process.montoIva" name="ProcessForm" /></td>
				</tr>
				<tr>
					<th>Monto del cr�dito a bonificar</th>
					<td colspan="3"><asf:text maxlength="40" type="decimal"
							property="process.montoBonif" name="ProcessForm" /></td>
				</tr>
				<tr>
					<th>Expediente</th>
					<td colspan="3"><asf:selectpop name="process.expediente"
							title="Expediente" columns="Numero, Asunto, Fecha"
							captions="identificacion,asunto,fechaCreacion"
							values="identificacion"
							entityName="com.asf.gaf.hibernate.documentos.DocumentoADE"
							value="${ProcessForm.process.expediente}" caseSensitive="true" />

					</td>
				</tr>
				<tr>
					<td colspan="4"><html:submit value="Guardar"
							onclick="guardar();"></html:submit></td>
				</tr>
			</table>


		</logic:equal>
		<logic:equal value="true" parameter="process.creada">
			<input type="button" value="Volver" onclick="atras();" />
		</logic:equal>


		<logic:equal name="ProcessForm" property="process.creada" value="true">
			<div id="pestanias" class="yui-navset" style="width: 100%;">
				<ul class="yui-nav">
					<li class="selected" id="tab_requisitos"><a href="#requisitos"
						onclick="irARequisitos();"><em>Requisitos</em> </a></li>
				</ul>
				<div class="yui-content">
					<div id="requisitos">
						<iframe
							src="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaRequisitos&process.idBonTasa=${ProcessForm.process.idBonTasa}&process.accion=listar"
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency" id="frameRequisitos">
						</iframe>
					</div>
				</div>
			</div>
		</logic:equal>
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>

<script type="text/javascript">
	//pesta�as.-
	var tabView = new YAHOO.widget.TabView('pestanias');
	var persona = false;
	var frameRequisitos = $('frameRequisitos');

	function selecPersona() {
		persona = true;
	}
	function irARequisitos() {
		var src = '';
		if (frameRequisitos.src == "") {
			src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=BonTasaRequisitos';
			src += '&process.accion=listar';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';
			frameRequisitos.src = src;
		}
	}
	function atras() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?&do=process&processName=BonTasaProcess&process.idPersona=${ProcessForm.process.idPersona}&process.accion=general";
	}
	function guardar() {
		var accion = $('accion');
		accion.value = "guardar";
	}
	function cancelar() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?&do=process&processName=BonTasaProcess&process.idPersona=${ProcessForm.process.idPersona}";
	}
	function loadConvenio() {
		var accion = $('accion');
		accion.value = "loadConvenio";
		var form = $('ProcessForm');
		form.submit();
	}
</script>