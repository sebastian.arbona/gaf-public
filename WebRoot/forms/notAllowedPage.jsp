<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insert definition=".mainLayout">
    <tiles:put name="body" direct="true" >
        <div align="center">
            <div class="title">Privilegios Insuficientes</div>
            <p><p><ul>Usted no tiene permiso para realizar la acci�n solicitada.</ul><p><p>
                <br>
                <BUTTON onclick="window.history.back();">Volver</BUTTON>
        </div>
    </tiles:put>
</tiles:insert>