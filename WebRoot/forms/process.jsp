<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%--@ page import="com.asf.cred.business.Process" --%>

<div class="title">Ejecutar Proceso Externo.</div>
<br/><br/>

<html:form action="/actions/process.do?processName=${param.processName}" method="POST" enctype="multipart/form-data" >
    <html:hidden property="do" value="process"/>
    <html:errors/>
    <%--logic:present name="descriptors">
        <bean:define name="descriptors" id="columnas" type="Object[]" />        
        
        <%
        String sThead = "<thead>\n<tr>\n";
        String sTbody = "<tbody>\n";        
 
        //Construyo el header de la tabla
        
        for( int c=0; c < columnas.length; c++ ){
            Object[] aAux = ( Object[] ) columnas[ c ];
            Object[] oColumnas = ( Object[] ) aAux[ 0 ];
            sThead += "<th>" + oColumnas[ 1 ] + "</th>\n";
        }
                
        //Construyo el body de la tabla
        int iBlancos = 0;
        
        for( int f = 1; f < com.asf.cred.business.Process.FILAS_MAXIMAS; f++ ){
            iBlancos = 0;
            sTbody += "@@<tr class=\"" + ( f % 2 != 0 ? "odd" : "even" ) + "\">\n";
            
            for( int c=0; c < columnas.length; c++ ){
                Object[] aAux = ( Object[] ) columnas[ c ];
                
                if( aAux[ f ] != null )
                    sTbody += "<td>" + aAux[ f ] + "</td>\n";
                else{
                    sTbody += "<td>&nbsp;</td>\n";
                    iBlancos++;
                }
            }
            //si la cantidad de espacios en blancos es igual a la cantidad de columnas, significa que no hay m�s datos para mostrar
            if( iBlancos == columnas.length ){
                sTbody = sTbody.substring( 0, sTbody.lastIndexOf( "@@" ) );
                break;
            }
            
            sTbody += "</tr>\n";
        }
        
        sTbody = sTbody.replace( "@@", "" );
        
        sThead += "</tr>\n</thead>\n";
        
        out.println( "<table>\n" );
        out.println( sThead );
        out.println( sTbody );
        out.println( "</table>\n" );
        %>            
        
    </logic:present--%>
    
    <logic:notEmpty name="ProcessForm" property="process.result">
        <display:table name="ProcessForm.process.result" id="reportTable" >
            <display:column>${reportTable}</display:column>                
        </display:table>
        <br>
        <br>
        <button onclick="history.back();">Volver</button>
    </logic:notEmpty>
    
    <logic:empty  name="ProcessForm" property="process.result" >
        <table border="0">
            <tr><th>Proceso:</th><td><html:file property="process.classToExport"  value="${ProcessForm.process.classToExport}" size="50"  /></td></tr>        
        </table>
        <br>
        <br>
        <html:submit value="Ejecutar" onclick="return confirm('Se ejecutar� la clase seleccionada. � Desea Continuar?');"/> 
    </logic:empty>    
</html:form>


