<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@page import="com.asf.cred.business.ProgressStatus"%>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>

<html:form action="/actions/process.do?processName=${param.processName}&do=process" method="post" enctype="multipart/form-data" styleId="oForm">
	<html:hidden property="process.indice" styleId="indice"/>
	<html:hidden property="process.accion" styleId="accion"/>

	<br/>

	<table border="0" align="left">
		<tr>
			<th>
				Seleccione Archivo:
			</th>
			<td>
				<html:file name="ProcessForm" property="process.file" styleId="archivo"
					size="50" disabled="${ProcessForm.process.deshabilitado}"/>
			</td>
		</tr>
	</table>

	<br/>
	<br/>
	<br/>

	<div align="left" style="float:left;">
		<html:submit onclick="return importar();" disabled="${ProcessForm.process.deshabilitado}">Importar</html:submit>
	</div>

	<div align="left" style="float:left;margin-left:20px;">
		<html:submit onclick="return rellenarFechas();" disabled="${ProcessForm.process.deshabilitado}">Rellenar Fechas</html:submit>
	</div>
</html:form>

<div align="left" style="float:left;margin-left:20px;">
	<button type="button" onclick="volver();">
		Volver
	</button>
</div>

<br/>
<br/>
<br/>

<div id='informeResultado' style="visibility:hidden;color:#222222;position:absolute;">
	<p><i><u>Resultados:</u></i></p>
	<br/>
	<div style="float:left;">
		<p>Filas del archivo:</p>
		<p>Filas insertadas:</p>
		<p>Filas repetidas:</p>
		<p>Filas erroneas:</p>
	</div>
	<div style="float:left;">
		<p>&nbsp;&nbsp;${ProcessForm.process.cantidadFilas}</p>
		<p>&nbsp;&nbsp;${ProcessForm.process.cantidadFilasInsertadas}</p>
		<p>&nbsp;&nbsp;${ProcessForm.process.cantidadFilasRepetidas}</p>
		<p>&nbsp;&nbsp;${ProcessForm.process.cantidadFilasErroneas}</p>
	</div>
</div>

<div id='informeResultadoRelleno' style="visibility:hidden;color:#222222;position:absolute;">
	<p><i><u>Resultados:</u></i></p>
	<br/>
	<div style="float:left;">
		<p>Filas insertadas:</p>
	</div>
	<div style="float:left;">
		<p>&nbsp;&nbsp;${ProcessForm.process.cantidadFilasInsertadas}</p>
	</div>
</div>

<script>
	var accion = document.getElementById("accion");
	var indice = document.getElementById("indice");

    indice.value = "${ProcessForm.process.indice}";

	function importar() {
		var answer = confirm("Se insertar�n las filas nuevas a la tabla.\n" +
							"Las fechas del archivo que coincidan con las de la tabla se consideran como repetidas y no se actualizar�n.\n" +
							"�Desea continuar?")

		if (answer) {
			accion.value = 'importarValores';
			window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
					"width=450, height=180");

			return true;
		}
		else
			return false;
	}

	function rellenarFechas() {
		var answer = confirm("Se completar� la tabla de �ndices con las fechas faltantes entre la primera y la �ltima cargadas en la tabla.\n" +
							"El valor de las nuevas fechas se obtendr� de la fechas del d�a anterior.\n" +
							"�Desea continuar?")

		if (answer) {
			accion.value = 'rellenarFechas';
			window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
						"width=450, height=180");

			return true;
		}
		else
			return false;
	}

	function volver() {
		window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=Indice";
	}
</script>

${ProcessForm.process.script}
<%ProgressStatus.onProcess = false;%>