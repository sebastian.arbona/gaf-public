<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div>
	<div class="title">Reporte Estadistico por Proyectos</div>
	<br>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
		<html:hidden property="process.action" value="consultar" styleId="action"/>
		<table border="0" class="tabla">
			<tr id="filtroMoneda">
				<th >Filtrar por Moneda en:</th>
				<td colspan="1" style="width:80%">
					<html:radio property="process.filtroMoneda" value="1"/>Pesos
					<html:radio property="process.filtroMoneda" value="2"/>Dólares
				</td>
			</tr>
			<tr>
				<th>Desembolso fecha desde:</th>
				<td>
					<asf:calendar property="ProcessForm.process.desdeFechaStr" value="${process.desdeFechaStr}"/>
				</td>
			</tr>
	
			<tr>
				<th>Desembolso fecha hasta:</th>
				<td>
					<asf:calendar property="ProcessForm.process.hastaFechaStr" value="${process.hastaFechaStr}"/>
				</td>
			</tr>
	
			<tr>
				<th>Información a fecha:</th>
				<td>
					<asf:calendar property="ProcessForm.process.informacionAFechaStr" value="${process.informacionAFechaStr}"/>
				</td>
			</tr>
			
			<tr>
				<th>Linea:</th>
		        <td>
		          <asf:select entityName="com.nirven.creditos.hibernate.Linea" listCaption="getId,getNombre"
          			listValues="getId" name="process.idLinea" value="${ProcessForm.process.idLinea}" nullText="Todas" nullValue="true"
          			filter="nombre IS NOT NULL ORDER BY id"/>
		        </td>
			</tr>
			
		</table>
		<html:submit value="Consultar"></html:submit>
	</html:form>
	
	<div id="container-column" style="min-width: 400px; height: 400px; margin: 2em 0 auto;"></div>
</div>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/highcharts.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/highcharts-exporting.js"></script>

<script type="text/javascript">
	var $j = jQuery.noConflict();
	var chart_column = null;
	
	var datosEstadisticos = '${empty ProcessForm.process.estadistica ? null : ProcessForm.process.estadistica}';
	var datosLinea = armarDatosGrafico( datosEstadisticos );
	$j(document).ready( function() {		
		chart_column = Highcharts.chart('container-column', {		    
			chart: {
		        type: 'column'
		    },
			title: {
		        text: ''
		    },
		    subtitle: {
		        text: ''
		    },
		    credits: {
   				enabled: false
   			},
		    xAxis: {	        
		        type: 'category',
		        tickPixelInterval: 200,
		        title: {
		            text: 'Estados'
		        }
		    },
		    yAxis: [{
		        min: 0,
		        type: 'category',
		        title: {
		            text: 'Monto Desembolsado'
		        },
		    }],
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.2f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.2,
		            borderWidth: 0
		        }
		    },
		    series: datosLinea
		   });
		
		
	});
	
	
	function armarDatosGrafico( datos ) {		
		var series = new Array();		
		if( datos != null && datos != "" ){						
			var datosEst = JSON.parse(datos);
			
			datosEst.forEach(element => {
				var desc = element.estado + " (Proyectos: "+element.cantProy+")";
				var serie = new Object();
				serie.type = 'column';
				serie.name = element.estado;
				serie.data = [[desc,element.valor]];
				
				series.push(serie);
	        });
		}
		return series;
	}
	
	
</script>