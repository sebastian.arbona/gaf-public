<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@page import="com.asf.util.DateHelper"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<link rel="stylesheet" href="/CREDITOS/style/frames.css" type="text/css">
<% pageContext.setAttribute( "fechaHoy", DateHelper.getString( new Date() ) ); %>

<div class="title">
	Nuevo Titular
</div>
<html:form action="/actions/process.do?processName=${param.processName}&do=process&idSolicitud=${ProcessForm.process.idSolicitud}&process.action=save">
	<html:hidden property="process.action" value="save"/>
	<html:hidden property="process.idSolicitud" value="${ProcessForm.process.idSolicitud}" styleId="idSolicitud" />
	<table border="0">
		<tr>
			<th>
				Nuevo Titular:
			</th>
			<td>
          		<asf:selectpop name="process.idTitularNuevo" title="Persona" columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
            		captions="idpersona,nomb12,nudo12,cuil12" values="idpersona" entityName="com.civitas.hibernate.persona.Persona"
            		value="${ProcessForm.process.idTitularNuevo}"/>
        	</td>
		</tr>
		<tr>
			<th>
				Fecha Hasta:
			</th>
			<td>
				<asf:calendar property="ProcessForm.process.historialTitularidad.fechaHastaStr" 
					value="${empty ProcessForm.process.historialTitularidad.fechaHastaStr ? fechaHoy : ProcessForm.process.historialTitularidad.fechaHastaStr }"/>
			</td>
		</tr>
		<tr>
			<th>
				N�mero de Resoluci�n:
			</th>
			<td>
				<html:text name="ProcessForm" property="process.historialTitularidad.numeroResolucionCambio" 
					maxlength="50" value="${ProcessForm.process.historialTitularidad.numeroResolucionCambio}"/>
			</td>
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td>
				<html:textarea name="ProcessForm" property="process.historialTitularidad.observaciones" 
					cols="40" rows="4" onkeypress="caracteres(this,100,event)" 
					value="${ProcessForm.process.historialTitularidad.observaciones}"/>
			</td>
		</tr>
	</table>

	<asf:security action="/actions/process.do?processName=${param.processName}&do=process"
		access="do=save">
		<input type="submit" value="Guardar" onclick="return guardar();" />
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>
<script type="text/javascript">

function guardar(){
	if (confirmMsg("�Desea cambiar al titular?")) {
		$("action").value = "save";
		return true;
	}
	return false;
}

</script>