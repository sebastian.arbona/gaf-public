<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Convenio de Bonificaci&oacute;n Externa</div>
<br>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>Id:</th>
			<td><asf:text name="AbmForm" property="entity.id" type="long"
					maxlength="8" readonly="true" /></td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><html:textarea property="entity.nombre" styleId="nombre"></html:textarea>
		</tr>
		<tr>
			<th>Entidad de Cr&eacute;dito:</th>
			<td><asf:select name="entity.idBanco"
					entityName="com.asf.gaf.hibernate.Bancos"
					value="${AbmForm.entity.idBanco}" listCaption="detaBa"
					listValues="codiBa" filter="bonificador=1" orderBy="codiBa" /></td>
		</tr>
		<tr>
			<th>Fecha del Convenio:</th>
			<td><asf:calendar property="AbmForm.entity.fechaStr"
					value="${entity.fechaStr}" /></td>
		</tr>
		<tr>
			<th>Fecha de vencimiento del Convenio:</th>
			<td><asf:calendar property="AbmForm.entity.fechaVencimientoStr"
					value="${entity.fechaVencimientoStr}" /></td>
		</tr>
		<tr>
			<th>Alerta de vencimiento del Convenio:</th>
			<td><asf:calendar
					property="AbmForm.entity.fechaAlertaVencimientoStr"
					value="${entity.fechaAlertaVencimientoStr}" /></td>
		</tr>
		<tr>
			<th>CBU:</th>
			<td><html:text maxlength="25" property="entity.cbu"
					name="AbmForm" value="${AbmForm.entity.cbu}" /></td>
		</tr>
		<tr>
			<th>Expediente</th>
			<td colspan="5"><asf:selectpop name="entity.expe"
					entityName="DocumentoADE" value="${AbmForm.entity.expe}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI"
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					size1="25" size2="70" cantidadDescriptors="2" /></td>
		</tr>
		<tr>
			<th>Operatoria:</th>
			<td><asf:select name="entity.operatoria"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.operatoria}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'operatoria.tipo'  order by codigo" /></td>
		</tr>
		<tr>
			<th>Fondeo de la Operatoria:</th>
			<td><asf:select name="entity.fondeoOperatoria"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.fondeoOperatoria}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'fondeoOperatoria.tipo'  order by codigo" /></td>
		</tr>
		<tr>
			<th>M&aacute;ximo cr&eacute;dito por persona:</th>
			<td><asf:text maxlength="15" type="decimal"
					property="entity.maximoPersona" name="AbmForm" /></td>
		</tr>
		<tr>
			<th>L&iacute;nea de Cr&eacute;dito:</th>
			<td><asf:select entityName="com.nirven.creditos.hibernate.Linea"
					attribs="class='Linea'" listCaption="getId,getNombre"
					filter="id in(${AbmForm.entity.lineaDefault_id})"
					listValues="getId" name="entity.idLinea"
					value="${AbmForm.entity.idLinea}" /></td>
		</tr>
		<tr>
			<th>Destino de los Cr&eacute;ditos:</th>
			<td><asf:select name="entity.destinoConvenio_id"
					entityName="com.nirven.creditos.hibernate.Destino"
					value="${AbmForm.entity.destinoConvenio_id}"
					listCaption="descripcion" listValues="id" /></td>
		</tr>
		<tr>
			<th>Destinatarios - Tama&ntilde;o de empresa:</th>
			<td><asf:select name="entity.tipoEmpresa"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoEmpresa}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'TipoEmpresa'  order by codigo" /></td>
		</tr>
		<tr>
			<th>Moneda de los cr&eacute;ditos:</th>
			<td><asf:select name="entity.idMoneda"
					entityName="com.asf.gaf.hibernate.Moneda"
					listCaption="getAbreviatura" listValues="getId"
					value="${AbmForm.entity.idMoneda}"
					attribs="onchange=\"cambiarCotiz();\"" /></td>
		</tr>
		<tr>
			<th>Cotizacion</th>
			<td><input type="text" id="cotizacion" readonly="readonly" /></td>
			<th>Importe Cotizado</th>
			<td><input type="text" id="cotizado" readonly="readonly" /></td>
		</tr>
		<tr>
			<th>Moneda de pago de bonificaciones:</th>
			<td><asf:select name="entity.idMonedaPago"
					entityName="com.asf.gaf.hibernate.Moneda"
					listCaption="getAbreviatura" listValues="getId"
					value="${AbmForm.entity.idMonedaPago}" /></td>
		</tr>
		<tr>
			<th>Cupo m&aacute;ximo de cr&eacute;ditos por L&iacute;nea de
				Cr&eacute;dito:</th>
			<td><asf:text maxlength="10" type="long"
					property="entity.cupoLinea" name="AbmForm" /></td>
		</tr>
		<tr>
			<th>Cupo m&aacute;ximo de cr&eacute;dito por persona por
				convenio:</th>
			<td><asf:text maxlength="10" type="long"
					property="entity.cupoPersona" name="AbmForm" /></td>
		</tr>
		<tr>
			<th>Tasa Inter�s Compensatorio</th>
			<td><asf:selectpop name="entity.idIndiceComp" title="Tasa"
					columns="Codigo, Nombre" captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.Indice"
					value="${AbmForm.entity.idIndiceComp}" filter="tipo in (1,2,3)"
					onChange="tasaComp();" /> <asf:text name="AbmForm"
					id="indiceValorComp" readonly="true"
					property="entity.indiceValorComp"
					value="${AbmForm.entity.indiceValorComp}" type="decimal"
					maxlength="5" /></td>
		</tr>
		<tr>
			<th>Mas</th>
			<td><html:text property="entity.valorMasCompStr" size="15"
					onchange="calcularValorFinalComp();" styleId="masComp" /></td>
		</tr>
		<tr>
			<th>Por</th>
			<td><asf:text id="porComp" name="AbmForm"
					property="entity.valorPorCompStr"
					attribs='onchange="calcularValorFinalComp();"' type="decimal"
					maxlength="15" /></td>
		</tr>

		<tr>
			<th>D�as antes</th>
			<td><html:text value="${AbmForm.entity.diasAntesComp}"
					property="entity.diasAntesComp" size="15" /></td>
		</tr>
		<tr>
			<th>Tasa Inter�s Compensatorio final</th>
			<td><html:text property="entity.valorFinalComp" readonly="true"
					size="15" styleId="valorFinalComp" /></td>
		</tr>
		<tr>
			<th>Tipo de tasa de bonificaci&oacute;n:</th>
			<td><asf:text maxlength="6" type="decimal"
					property="entity.tasaBon" name="AbmForm" /></td>
		</tr>
		<tr>
			<th>Plazo m&aacute;ximo de bonificaci&oacute;n:</th>
			<td><asf:text maxlength="6" type="long"
					property="entity.plazoMaximo" name="AbmForm" /></td>
		</tr>
	</table>

	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>

	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>

</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script type="text/javascript">
	$j = jQuery.noConflict();

	function calcularValorFinalComp() {
		var idIndice = $j("#entity\\.idIndiceComp").val();
		var tasacom = $j("#indiceValorComp").val().replace(',', '.').replace(
				'%', '');
		var mas = $j("#masComp").val().replace(',', '.').replace('%', '');
		var por = $j("#porComp").val().replace(',', '.').replace('%', '');

		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinalPromedio',
						'&idIndice=' + idIndice + '&tasa=' + tasacom + '&mas='
								+ mas + '&por=' + por, function(data) {
							$j("#valorFinalComp").val(data);
						});
	}

	function calcularValorFinalComp(tasaInd) {
		var idIndice = $j("#entity\\.idIndiceComp").val();
		var tasacom = $j("#indiceValorComp").val().replace(',', '.').replace(
				'%', '');
		var mas = $j("#masComp").val().replace(',', '.').replace('%', '');
		var por = $j("#porComp").val().replace(',', '.').replace('%', '');

		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinalPromedio',
						'&idIndice=' + idIndice + '&tasa=' + tasacom + '&mas='
								+ mas + '&por=' + por, function(data) {

							$j("#valorFinalComp").val(data);

						});
	}

	function tasaComp() {
		var idIndice = $j("#entity\\.idIndiceComp").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndicePromedio',
						'&idIndice=' + idIndice, function(data) {
							$j("#indiceValorComp").val(data);
							$j("#valorFinalComp").val(
									calcularValorFinalComp(data));
						});
	}

	function calcularValorFinalBon() {
		var tasa = $j("#indiceValorBon").val().replace(',', '.').replace('%',
				'');
		var mas = $j("#masBon").val().replace(',', '.').replace('%', '');
		var por = $j("#porBon").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasaBon").val().replace(',', '.').replace('%',
				'');

		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinalBon").val(data);

						});
	}

	function calcularValorFinalBon(tasaInd) {
		var tasa = $j("#indiceValorBon").val().replace(',', '.').replace('%',
				'');
		var mas = $j("#masBon").val().replace(',', '.').replace('%', '');
		var por = $j("#porBon").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasaBon").val().replace(',', '.').replace('%',
				'');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinalMor").val(data);
						});
	}
	function tasaBon() {
		var idIndice = $j("#entity\\.idIndiceBon").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='
								+ idIndice, function(data) {
							$j("#indiceValorBon").val(data);
							$j("#valorFinalBon").val(
									calcularValorFinalBon(data));
						});

		var form = document.getElementById('oForm');
		var action = document.getElementById('action');
		action.value = 'modificarBonificacion';
		form.submit();
	}

	function cambiarCotiz() {
		var moneda = $j("#entity\\.idMoneda").val();
		url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxMonedaCotizacion&idMoneda="
				+ moneda;
		retrieveURL(url, cambiarCotizResponse);
	}

	function cambiarCotizResponse(responseText) {
		$('cotizacion').value = responseText;
		calcularCotizado();
	}

	function calcularCotizado() {
		var importe = $('importe');
		var cotizacion = $('cotizacion');
		$('cotizado').value = new Number(importe.value)
				* new Number(cotizacion.value);
	}
</script>
