<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true">
  <html:hidden property="paramValue[0]" value="${paramValue[0]}" />
  <html:hidden property="paramName[0]" value="credito.id" />
  <html:hidden property="paramComp[0]" value="=" />
  <html:hidden property="entity.credito_id" value="${paramValue[0]}" />
  <html:hidden property="entity.id" value="${AbmForm.entity.id}" />
  <table border="0">
    <tr>
      <th>
        Numero:
      </th>
      <td>
        <asf:text name="AbmForm" property="entity.numero" type="long" maxlength="6" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha:
      </th>
      <td>
        <asf:calendar property="AbmForm.entity.fechaStr" />
      </td>
    </tr>
    <tr>
      <th>
        Importe:
      </th>
      <td>
        <asf:text name="AbmForm" property="entity.importe" type="decimal" maxlength="13"/>
      </td>
    </tr>
    <tr>
      <th>
        Observación:
      </th>
      <td>
        <fck:editor toolbarSet="Civitas" instanceName="entity.observacion" width="600" height="200" value="${AbmForm.entity.observacion}" />
      </td>
    </tr>
    <tr>
      <th>
        Requisito:
      </th>
      <td>
        <asf:select entityName="com.nirven.creditos.hibernate.Requisito" listCaption="getId,getNombre" listValues="getId"
          name="process.credito.requisito_id" value="${AbmForm.entity.requisito_id}"
        />
      </td>
    </tr>
  </table>
  <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
		<html:submit><bean:message key="abm.button.save"/></html:submit>
  </asf:security>
		<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
		
		    <iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
	style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
  </html:form>
<!-- Raul Varela -->