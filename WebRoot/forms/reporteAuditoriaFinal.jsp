<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

<div class="title">Reporte de Auditor�a Final</div>
<p>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.action" name="ProcessForm" styleId="action"/>
	
	<div id="linea" title="Linea de Credito" style="visibility: visible;">
		<table style="border: 2px solid rgb(204, 204, 204);" width="352" height="30">			
			<tr>
				<th>
					L�nea de Cr�dito:
				</th>
				<td>						
					<html:select styleId="idsLineas" property="process.idsLineas" multiple="true"  size="8">
						<html:optionsCollection  property="process.lineas" label="nombre" value="id"/>
					</html:select>
				</td>
			</tr>			
			<tr>
				<th>Fecha:</th>
				<td>
					<asf:calendar property="ProcessForm.process.fecha" 
						value="${ProcessForm.process.fecha}">
					</asf:calendar>
				</td>
			</tr>

	       <tr>		
			  <th>Aplic� Fondos:</th>
				<td>
					<select id="process.aplicaFon" name="process.aplicaFon" class="aplicaFon"  value="todos">
					<option value="TODOS">Todos</option>
					<option value="NO">No</option>
					<option value="SI">Si</option>						
					<option value="PROCESO">En Proceso</option>
					</select>
			  </td>
			</tr>
			
		   <tr>
		        <th>
		          Estados:
		        </th>
		        
		        <td>						
					<html:select styleId="idsEstados" property="process.idsEstados" multiple="true"  size="8">
						<html:optionsCollection  property="process.estados" label="nombreEstado" value="id"/>
					</html:select>
				</td>
	      </tr>

			
		</table>
	</div>
		<input type="button" value="Listar" onclick="listar();">
	
	
	<br/><br/>
	<br/><br/>
	<div class="grilla">
	<display:table name="ProcessForm" property="process.beanAuditorias" export="true" id="reportTable" pagesize="50" 
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
		<display:caption>Reporte de Auditoria Final</display:caption>
		<display:column media="excel xml pdf" property="nroProyecto" title="Proyecto"/>
		<display:column property="nroProyecto" title="Nro Proyecto" sortable="true" />
		<display:column property="titular" title="Titular" sortable="true" />
		<display:column property="linea" title="Linea" sortable="true" />
		<display:column property="expediente" title="Expediente" sortable="true" />
		<display:column property="nombreEstado" title="Etapa" sortable="true" />
		<display:column property="fechaReal" title="Fecha Desembolso" sortable="true" />
		<display:column property="nroDesembolso" title="Nro Desembolso" sortable="true" />
		<display:column property="cantDesembolso" title="Cantidad de Desembolso" sortable="true"/>
		<display:column property="diasTranscurridos" title="D�as Transcurridos" sortable="true" />
		<display:column property="fechaProceso" title=" Fecha Auditor�a" sortable="true" />
		<display:column property="aplicaFondos" title="Aplic� Fondos" sortable="true" />
		<display:column property="observaciones" title="Observaciones" sortable="true" />
	</display:table>
	</div>
	</html:form>

	<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
	style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

	<script type="text/javascript">
		function listar(){ 
			var accion = document.getElementById('action');
			accion.value="listar";
			var formulario = document.getElementById('oForm');
			formulario.submit();
		}
	</script>
	