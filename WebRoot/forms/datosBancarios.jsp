<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.accion" value="guardar" styleId="accion"/>
    <html:hidden property="process.persona.id" styleId="idPersonaTitular" value="${ProcessForm.process.persona.id}" />
    <html:hidden property="process.cuentaBancaria.id" value="${ProcessForm.process.cuentaBancaria.id}" />
    <div style="width:70%" align="left"><html:errors/></div>
    <table>
        <tr>
            <th>Tipo de Cuenta: </th>
            <td>
                <asf:select name="process.idTipoCuentaBancaria" entityName="Tipificadores" listCaption="codigo,descripcion"
                            listValues="codigo" value="${ProcessForm.process.idTipoCuentaBancaria}" filter="categoria='cuentabancaria.tipo'">
                </asf:select>
            </td>
        </tr>
        <tr>
            <th>Banco: </th>
            <td>
               
                <asf:select name="process.idBanco" entityName="Tipificadores" listCaption="codigo,descripcion"
                            listValues="codigo" value="${ProcessForm.process.idBanco}" filter="categoria='banco.interbanking'">
                </asf:select>
            </td>
        </tr>
        <tr>
            <th>N�mero de Cuenta: </th>
            <td>
                <asf:text type="long" name="ProcessForm" property="process.cuentaBancaria.nroCuentaBancaria" maxlength="17"/>
            </td>
        </tr>
        <tr>
            <th>CBU: </th>
            <td>
            	<asf:text type="long" name="ProcessForm" property="process.cuentaBancaria.cbu" maxlength="22"/>
            </td>
        </tr>
        <tr>
            <th>Fecha de Apertura: </th>
            <td>
            	<asf:calendar property="ProcessForm.process.cuentaBancaria.fechaAperturaStr"/>                
            </td>
        </tr>
         <tr>
            <th>Nombre Tercero Titular:</th>
            <td>
            	<html:text property="process.titularCuenta" size="50" maxlength="50" name="ProcessForm"/>
            </td>
        </tr>
         <tr>
            <th>CUIT Tercero Titular:</th>
            <td>
            	<asf:text type="long" property="process.cuitTitular" maxlength="11" name="ProcessForm" />  
            </td>
        </tr>
    </table>
    <br>
    <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=guardar&process.cuentaBancaria.id=${ProcessForm.process.cuentaBancaria.id}">
    	<html:submit value="Guardar"/>
    </asf:security>
     <input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
        src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<script language='javascript'>

	var accion     = $('accion');
	var formulario = $('oForm');
	
	function cancelar()
    {
	    accion.value = "cancelar";

	    formulario.submit();
    }//fin cancelar.-

    Event.observe(window, 'load', function() 
    {
    	this.name = "Bancos";
    	parent.push(this);
    });
</script>