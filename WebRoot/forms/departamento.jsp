<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Departamento</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<table border="0">
		<tr>
			<th>Nombre:</th>
			<td><html:text property="entity.nombre" maxlength="40" /></td>
		</tr>
		<tr>
			<th>Provincia:</th>
			<td><asf:select name="entity.idProvincia"
					entityName="com.civitas.hibernate.persona.Provin"
					listCaption="deta08" listValues="codi08"
					value="${empty AbmForm.entity.provincia ? '' : AbmForm.entity.provincia.codi08}"
					filter="deta08 like '%' order by deta08" /></td>
		</tr>
		<tr>
			<th>Oasis Productivo:</th>
			<td><asf:select name="entity.oasis"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.oasis}" nullValue="true"
					nullText="&lt;&lt;Seleccione el Oasis Productivo&gt;&gt;"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'Departamento.oasisProductivo' order by codigo" /></td>
		</tr>

	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>

	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>