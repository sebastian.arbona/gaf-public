<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<h3 class="title">Nuevo Legajo Digital</h3>
<form role="form" id="form_LegajoDigital" name="form_LegajoDigital">
	<input type="hidden" name="id" id="id" /> <input type="hidden"
		name="persona_id" id="persona_id" />

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="form-group">
				<label for="search-persona">Buscar Persona:</label> <select
					name="search-persona" id="search-persona"
					class="form-group select-persona anchoSelect-Largo"
					data-placeholder="Seleccione una Persona">
					<option></option>
				</select>
			</div>
		</div>
	</div>

	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<div class="form-group">
				<label for="LegajoDigital_Persona_Nombre">Nombre y Apellido:</label>
				<input type="text" name="LegajoDigital_Persona_Nombre"
					id="LegajoDigital_Persona_Nombre" value="" />
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<div class="form-group">
				<label for="LegajoDigital_NumeroLegajo">N&uacute;mero de
					Legajo:</label> <input type="text" name="numeroLegajo" id="numeroLegajo"
					value="" readonly />
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<div class="form-group">
				<label for="LegajoDigital_Persona_DNI">DNI:</label> <input
					type="text" name="LegajoDigital_Persona_DNI"
					id="LegajoDigital_Persona_DNI" value="" readonly />
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<div class="form-group">
				<label for="LegajoDigital_Persona_CUIL">CUIL:</label> <input
					type="text" name="LegajoDigital_Persona_CUIL"
					id="LegajoDigital_Persona_CUIL" value="" readonly />
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<div class="form-group">
				<label for="expediente">Expediente:</label> <select id="expediente"
					name="expediente" class="select-docade select2-ancho-100"
					data-placeholder="Seleccione un Expediente">
					<option></option>
				</select>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-right">
			<div class="form-group">
				<asf:security action="/actions/process.do"
					access="do=process&processName=LegajoDigital&process.accion=doPost">
					<button id="btn-save" type="button" class="btn btn-confirmar"
						onclick="save_LegajoDigital();">Guardar</button>
				</asf:security>
				<button id="btn-cancel" type="button" class="btn btn-cancelar"
					onclick="grid_LegajoDigital();">Cancelar</button>
			</div>
		</div>
	</div>

</form>
<script type="text/javascript">
	$j(document).ready(function() {
		components.SelectPersona({
			selectId : "#search-persona",
		});
		inicializarSelectDoc();

		$j('#search-persona').on('select2:select', function(e) {
			$j('#LegajoDigital_Persona_DNI').val(e.params.data.dni);
			$j('#LegajoDigital_Persona_CUIL').val(e.params.data.cuil);
			$j('#LegajoDigital_Persona_Nombre').val(e.params.data.nombre);
			$j('#persona_id').val(e.params.data.idPersona);
			if (e.params.data.cuil != "0" && e.params.data.cuil != "") {
				firstLetter = e.params.data.nombre.substring(0, 1);
				$j('#numeroLegajo').val(firstLetter + e.params.data.cuil);
			} else {
				$j("#numeroLegajo").val("");
			}
		});
	});
</script>
