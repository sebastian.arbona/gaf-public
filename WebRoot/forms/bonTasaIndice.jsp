<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">  	  	 
  	<html:hidden property="process.accion" styleId="accion" value="saveIndice"/>
  	<html:hidden property="process.idBonTasa" styleId="accion" value="${ProcessForm.process.idBonTasa}"/>
  	<html:errors></html:errors>
  	
  	<h3>Tipo Tasa: Inter�s Compensatorio</h3>
  	
  	<table>
  		<tr>
	  		<th>Tasa Inter�s Compensatorio</th>
	  		<td>
		  		<asf:selectpop name="process.idCompensatorio" title="Tasa"
					columns="Codigo, Nombre"
					captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.Indice"
					value="${ProcessForm.process.idCompensatorio}" 
					filter="tipo=1" />		  								
			</td>
  		</tr>  		
  	</table>
  	<div style="width: 100%; text-align: center">
  		<html:submit value="Guardar"></html:submit>    	
    	<input type="button" value="Cancelar" onclick="cancelar();"/>
    </div>
  </html:form>
  <script type="text/javascript">	
  		
    	function cancelar() {
    		var action = document.getElementById('accion');
    		action.value = '';  		
					
    		var form = document.getElementById('oForm');
    		form.submit(); 
    	}
		
    </script>
