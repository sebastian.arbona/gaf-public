<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">
	Importaci&oacute;n de Correlaci&oacute;n Cr&eacute;ditos-GIC
</div>
<br/>
<html:form action="/actions/process.do?processName=CredGICIntegProcess&do=process" 
	method="post" enctype="multipart/form-data" styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	
	<table border="0">
		<tr>
			<th>
				Archivo:
			</th>
			<td>
				<html:file name="ProcessForm" property="process.archivo" styleId="archivo"
					size="40" />
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Importar"/>
			</th>
		</tr>
	</table>
	
	<logic:equal name="ProcessForm" property="process.ejecutado" value="true">
		<br/>
		<h4>Registros importados correctamente: ${ProcessForm.process.cantidadOk}</h4>
		<display:table name="ProcessForm" property="process.beansOk" export="false" id="bean">
			<display:column title="Nro Creditos" property="codigo1"/>
			<display:column title="Nro Solicitud" property="idCredito"/>
			<display:column title="Titular" property="titular"/>
			<display:column title="linea" property="lineaCredito"/>
			<display:column title="Accion" property="mensaje"/>
		</display:table>
		<br/>
		<h4>Registros con error de importaci&oacute;n: ${ProcessForm.process.cantidadNoExiste}</h4>
		<display:table name="ProcessForm" property="process.beansNoExiste" export="false" id="bean">
			<display:column title="Linea" property="lineaOriginal"/>
			<display:column title="Nro Creditos" property="codigo1"/>
			<display:column title="Nro Solicitud" property="idCredito"/>
			<display:column title="Error" property="mensaje"/>
		</display:table>
		<br/>
		<h4>Registros con error de formato en el archivo: ${ProcessForm.process.cantidadError}</h4>
		<display:table name="ProcessForm" property="process.beansError" export="false" id="bean">
			<display:column title="Linea" property="codigo1"/>
			<display:column title="Registro" property="lineaOriginal"/>
			<display:column title="Error" property="mensaje"/>
		</display:table>
	</logic:equal>
	
	
</html:form>