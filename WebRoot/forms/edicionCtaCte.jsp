<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Edicion Cta Cte</div>
<html:form
	action="/actions/process.do?do=process&processName=EdicionCtaCte"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="buscar"/>
	<table>
		<tr>
			<th>Credito</th>
			<td>
				<html:text property="process.numeroAtencion"></html:text>
				<html:submit value="Buscar"></html:submit>
			</td>
		</tr>
	</table>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.beans" id="bean">
			<display:column title="Id">
				${bean.ctacte.id.movimientoCtacte}|${bean.ctacte.id.itemCtacte}|${bean.ctacte.id.periodoCtacte}
			</display:column>
			<display:column title="FechaG" property="ctacte.fechaGeneracion" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="FechaP" property="ctacte.fechaProceso" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="Tipo" property="ctacte.tipomov.abreviatura"/>
			<display:column title="Cuota">
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=EdicionCtaCte&process.action=editarEstadoCuota&process.cuotaId=${bean.ctacte.cuota.id}">
					${bean.ctacte.cuota.numero} | 
					<logic:equal name="bean" property="ctacte.cuota.estado" value="1">
						No cancelada
					</logic:equal>
					<logic:equal name="bean" property="ctacte.cuota.estado" value="2">
						Cancelada
					</logic:equal>
				</a>
			</display:column>
			<display:column title="Boleto">
				${bean.ctacte.boleto.numeroBoleto} | ${bean.ctacte.boleto.id}
			</display:column>
			<display:column title="Cap">
				<logic:notEqual name="bean" property="capital" value="0">
					${bean.capital}
				</logic:notEqual>
			</display:column>
			<display:column title="Comp">
				<logic:notEqual name="bean" property="compensatorio" value="0">
					${bean.compensatorio}
				</logic:notEqual>
			</display:column>
			<display:column title="Bon">
				<logic:notEqual name="bean" property="bonificacion" value="0">
					${bean.bonificacion}
				</logic:notEqual>
			</display:column>
			<display:column title="Mor">
				<logic:notEqual name="bean" property="moratorio" value="0">
					${bean.moratorio}
				</logic:notEqual>
			</display:column>
			<display:column title="Pun">
				<logic:notEqual name="bean" property="punitorio" value="0">
					${bean.punitorio}
				</logic:notEqual>
			</display:column>
			<display:column title="Mul">
				<logic:notEqual name="bean" property="multas" value="0">
					${bean.multas}
				</logic:notEqual>
			</display:column>
			<display:column title="Gas">
				<logic:notEqual name="bean" property="gastos" value="0">
					${bean.gastos}
				</logic:notEqual>
			</display:column>
			<display:column title="Rec">
				<logic:notEqual name="bean" property="gastosRecuperar" value="0">
					${bean.gastosRecuperar}
				</logic:notEqual>
			</display:column>
			<display:column title="Pago">
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=EdicionCtaCte&process.action=editarPago&process.pagoId=${bean.pago.id.boleto.id}|${bean.pago.id.caratula.id}">
					${bean.pago.id.boleto.id}|${bean.pago.id.caratula.id}|${bean.pago.recibo.numeroBoleto}
				</a>
			</display:column>
			<display:column>
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=EdicionCtaCte&process.action=editar&process.id=${bean.ctacte.id.movimientoCtacte}|${bean.ctacte.id.itemCtacte}|${bean.ctacte.id.periodoCtacte}">Ed</a>
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=EdicionCtaCte&process.action=eliminar&process.id=${bean.ctacte.id.movimientoCtacte}|${bean.ctacte.id.itemCtacte}|${bean.ctacte.id.periodoCtacte}">El</a>
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=EdicionCtaCte&process.action=duplicar&process.id=${bean.ctacte.id.movimientoCtacte}|${bean.ctacte.id.itemCtacte}|${bean.ctacte.id.periodoCtacte}">Du</a>
			</display:column>
		</display:table>
	</div>
</html:form>