<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<div class="title">Administraci�n de Acuerdos de Pago</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=AcuerdoPago"
	styleId="oForm">
	<html:hidden property="process.action" value="save" styleId="accion" />
	<html:hidden property="process.idMoneda" value="save" styleId="moneda" />
	<html:hidden property="process.tipoTasa" styleId="tasa" />
	<html:hidden property="process.exito" styleId="exito" />
	<html:hidden property="process.nroAtencion" styleId="nAtencion" />
	<html:hidden property="process.idPersona" styleId="idPersona" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br>
	<table>
		<tr>
			<th>Persona</th>
			<td><asf:selectpop name="process.idPersona" title="Persona"
					columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
					captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
					entityName="com.civitas.hibernate.persona.Persona"
					value="${ProcessForm.process.idPersona}" /> (Codigo-Apellido y
				Nombre-Documento)</td>
		</tr>
		<tr>
			<th colspan="2">
				<center>
					<input type="button" value="Consultar" onclick="consultar();" />
				</center>
			</th>
		</tr>

	</table>
	<br>
	<logic:notEmpty property="process.personas" name="ProcessForm">
		<display:table name="ProcessForm" property="process.personas"
			defaultsort="1" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/PersonaAction.do?do=list&entityName=${param.entityName}"
			width="100%">
			<display:column media="html" sortProperty="id" property="id"
				title="ID" paramId="entity.id" paramProperty="id" />
			<display:column property="nomb12" title="Apellido y Nombre" />
			<display:column property="razonSocial" title="Raz�n Social" />
			<display:column title="Documento">
    	${reportTable.tipodoc.tido47} ${reportTable.nudo12}
    		</display:column>
			<display:column property="cuil12" title="CUIT/CUIL" />
			<display:column property="estadoActual.estado.nombreEstado"
				title="Estado" />
			<display:column title="Color">
				<div style="background:${reportTable.estadoActual.estado.color};">
					&nbsp;</div>
			</display:column>
			<display:column property="fechaBajaStr" title="Fecha de Baja" />
		</display:table>

		<table width="100%" style="margin-top: 30px">
			<tr>
				<th>Acuerdos de Pago</th>
			</tr>
		</table>

		<display:table name="ProcessForm" property="process.acuerdos"
			id="acuerdo" width="100%">
			<display:column title="Cr�dito N�" property="numeroAtencion" />
			<display:column title="L�nea" property="linea.nombre" />
			<display:column title="Expediente N�" property="expediente" />
			<display:column title="Estado"
				property="estadoActual.estado.nombreEstado" />
			<display:column title="Revertir">
				<logic:notEmpty name="acuerdo" property="estadoActual.estado">
					<logic:notEqual value="ACUERDO CAIDO" name="acuerdo"
						property="estadoActual.estado.nombreEstado">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?processName=AcuerdoPagoCaida&do=process&process.idCredito=${acuerdo.id}">Revertir</a>
					</logic:notEqual>
				</logic:notEmpty>
			</display:column>
		</display:table>

		<table width="100%" style="margin-top: 30px">
			<tr>
				<th>Cr�ditos Actuales</th>
			</tr>
			<tr>
				<td><input type="checkbox" onclick="activar();" id="activacion" />Seleccionar
					todos</td>
			</tr>
		</table>

		<display:table name="ProcessForm" property="process.creditosDeuda"
			id="credito" width="100%">
			<display:column title="Seleccionar">
				<logic:empty name="ProcessForm"
					property="process.creditoSeleccionadoId(${credito.id})">
					<input type="checkbox" name="credito-${credito.id}"
						value="${credito.id}" />
				</logic:empty>
				<logic:notEmpty name="ProcessForm"
					property="process.creditoSeleccionadoId(${credito.id})">
					<input type="checkbox" name="credito-${credito.id}"
						value="${credito.id}" checked="checked" />
				</logic:notEmpty>
			</display:column>
			<display:column title="Cr�dito N�" property="numeroAtencion" />
			<display:column title="L�nea" property="linea.nombre" />
			<display:column title="Expediente N�" property="expediente" />
			<display:column title="Estado"
				property="estadoActual.estado.nombreEstado" />
		</display:table>
		<br />


		<table width="100%">
			<tr>
				<th>L�nea de Cr�dito</th>

				<td><html:select name="ProcessForm" property="process.idLinea">
						<html:optionsCollection name="ProcessForm"
							property="process.lineas" label="nombre" value="id" />
					</html:select></td>

				<th>Judicial</th>

				<td><html:radio property="process.tipoJudicial" value="1">SI</html:radio>
					<html:radio property="process.tipoJudicial" value="0">NO</html:radio>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<th>Cantidad de cuotas capital</th>
				<td colspan="3"><asf:text name="ProcessForm" maxlength="3"
						type="long" property="process.cantidadCuotasCapital" /></td>
			</tr>
			<tr>
				<th>Cantidad de cuotas interes</th>
				<td><asf:text name="ProcessForm" maxlength="3" type="long"
						property="process.cantidadCuotasInteres" /></td>
				<th>Nro Resolucion</th>
				<td><html:text name="ProcessForm"
						property="process.nroResolucion" /></td>
			</tr>
			<tr>
				<th>Primer Vencimiento Capital</th>
				<td><asf:calendar
						property="ProcessForm.process.primerVencCapitalStr" /></td>
				<th>Primer Vencimiento Inter�s</th>
				<td><asf:calendar
						property="ProcessForm.process.primerVencInteresStr" /></td>
			</tr>
			<tr>
				<th>Segundo Vencimiento Capital</th>
				<td><asf:calendar
						property="ProcessForm.process.segundoVencCapitalStr" /></td>
				<th>Segundo Vencimiento Inter�s</th>
				<td><asf:calendar
						property="ProcessForm.process.segundoVencInteresStr" /></td>
			</tr>
			<tr>
				<th>Tasa Interes Compensatorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaCompensatorioStr" readonly="true"
						type="decimal" maxlength="5" /> <input type="button" value="..."
					onclick="editarIndice('compensatorio');" /></td>
				<th>Periodicidad Cuotas Capital</th>
				<td><asf:select name="process.periodicidadCapital"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="ProcessForm.process.objetoi.frecuenciaCapital"
						filter="categoria='amortizacion.periodicidad' order by codigo" />
				</td>
			</tr>
			<tr>
				<th>Tasa Interes Moratorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaMoratorioStr" readonly="true" type="decimal"
						maxlength="5" /> <input type="button" value="..."
					onclick="editarIndice('moratorio');" /></td>
				<th>Periodicidad Cuotas Inter�s</th>
				<td><asf:select name="process.periodicidadInteres"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="ProcessForm.process.objetoi.frecuenciaInteres"
						filter="categoria='amortizacion.periodicidad' order by codigo" />
				</td>
			</tr>
			<tr>
				<th>Tasa Interes Punitorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaPunitorioStr" readonly="true" type="decimal"
						maxlength="5" /> <input type="button" value="..."
					onclick="editarIndice('punitorio');" /></td>
				<th>Moneda</th>
				<td><asf:select name="process.idMoneda"
						entityName="com.asf.gaf.hibernate.Moneda"
						listCaption="getAbreviatura" listValues="getId"
						value="ProcessForm.process.idMoneda" enabled="false" /></td>
			</tr>
			<tr>
				<th>Expediente</th>
				<td><html:select property="process.expediente">
						<html:options name="ProcessForm" property="process.expedientes" />
					</html:select></td>
				<th>Fecha Acuerdo de Pago:</th>
				<td><asf:calendar
						property="ProcessForm.process.fechaAcuerdoStr" /></td>
			</tr>
		</table>
	</logic:notEmpty>
	<br>
	<logic:equal value="false" property="process.generada"
		name="ProcessForm">
		<logic:notEmpty property="process.personas" name="ProcessForm">
		</logic:notEmpty>
		<html:submit>Generar Acuerdo de Pago</html:submit>
	</logic:equal>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
	function consultar() {
		var accion = $('accion');
		accion.value = 'search';
		$('idPersona').value = new Number($('process.idPersona').value);
		var form = $('oForm');
		form.submit();
	}

	$j = jQuery.noConflict();

	function tasa() {
		var action = document.getElementById('action');
		action.value = "modificarBonificacion";
		var form = document.getElementById('ProcessForm');
		form.submit();

		var idIndice = $j("#process\\.idIndice").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice',
						'idIndice=' + idIndice, function(data) {
							$j("#indiceValor").val(data);
							calcularValorFinal();
						});
	}

	function calcularValorFinal() {
		var tasa = $j("#indiceValor").val().replace(',', '.').replace('%', '');
		var mas = $j("#mas").val().replace(',', '.').replace('%', '');
		var por = $j("#por").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasa").val().replace(',', '.').replace('%', '');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinal").val(data);
						});
	}

	function actualizar(IdIndice) {
	}

	function editarIndice(tasa) {
		var accion = $('accion');
		accion.value = 'loadIndice';
		var tasaIn = $('tasa');
		tasaIn.value = tasa;
		var form = $('oForm');
		form.submit();
	}

	function marcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = true;
			}
		}
	}

	function desmarcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}

	function activar() {
		var check = document.getElementById('activacion');
		if (check.checked == true) {
			marcarTodos();
		} else {
			desmarcarTodos();
		}
	}
	function creado() {
		if ($('exito').value == 'true')
			alert("Se creo el Acuerdo de Pago N�" + $('nAtencion').value
					+ " correctamente");
	}
	onload = creado;
</script>