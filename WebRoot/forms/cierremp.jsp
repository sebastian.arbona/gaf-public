<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Proceso Cierre de Ejercicio</div>
<html:form
	action="/actions/process.do?do=process&processName=CierreMP"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" styleId="action" value="listar"/>
	<html:errors/>
	
	<table>
		<tr>
			<th>Periodo</th>
			<td>
				<asf:text maxlength="4" type="long" property="process.periodo" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>Fecha Valor:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaValor" 
					value="${process.custodia.fechaValor}"/>
			</td>
		</tr>
		<tr>
			<th>D&iacute;as L&iacute;mite de C&aacute;lculo</th>
			<td>
				<html:text property="process.diasLimite" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>Estados:</th>
			<td>
				<html:select styleId="estados" name="ProcessForm" property="process.estados" multiple="true" size="8">
					<html:optionsCollection property="process.estadosList" label="nombreEstado" value="idEstado"/>
				</html:select>
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Listar Creditos"></html:submit>
			</th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.listado">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.listado" id="bean" export="true"
 			requestURI="${pageContext.request.contextPath}/actions/process.do">
 			<display:caption>Proceso Cierre de Ejercicio</display:caption>
 			<display:column title="Proyecto Nro" property="numeroAtencion" />
 			<display:column title="Linea" property="linea" />
 			<display:column title="Titular" property="titular"/>
 			<display:column title="Fecha Calculo" property="fechaCalculo" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Moratorios" property="moratorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<display:column title="Punitorios" property="punitorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<display:column title="Primer Venc. Impago" property="primerVencimientoImpago" decorator="com.asf.displayDecorators.DateDecorator"/>
 			<display:column title="Dias Transcurridos" property="diasPrimerVencimientoImpago" class="right"/>
 		</display:table>
	</div>
	<html:submit onclick="guardar();" value="Guardar"></html:submit>
	</logic:notEmpty>
	
	<logic:equal value="true" name="ProcessForm" property="process.success">
		Los movimientos en cuenta corriente fueron generados correctamente.
	</logic:equal>
</html:form>
<script type="text/javascript">
	function guardar() {
		var action = document.getElementById('action');
		action.value = 'guardar';
	}
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>