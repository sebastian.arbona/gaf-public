<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div class="title">Estado de notificaciones de Moras</div>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<br />
	<div>
		<display:table name="ProcessForm"
			property="process.listaEstadoNotificacionDTO" id="estadoNotificacion"
			export="true" defaultsort="2"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:setProperty name="report.title"
				value="Estado de las notificaciones"></display:setProperty>
			<display:column title="Proyecto" property="proyecto.numeroAtencion"
				sortable="true" />
			<display:column title="Relación" property="relacion" sortable="true" />
			<display:column title="Persona Vinculada"
				property="proyecto.persona.nomb12" sortable="true" />
			<display:column title="CUIT/CUIL" property="proyecto.persona.cuil12"
				sortable="true" />
			<display:column title="E-MAIL"
				property="email" sortable="true"></display:column>
			<display:column title="Estado Notificación" property="estadoEnvio"
				sortable="true"></display:column>
		</display:table>
	</div>
	Descargar Zip con las notificaciones generadas:<html:link
		action="/DownloadIt.do?do=generarReporte">notificaciones.zip</html:link>
	<br />
	<br />
	<div>
		<input type="button" value="Atras" onclick="atras();">
	</div>
</html:form>
<script type="text/javascript">
	function atras() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?&do=process&processName=NotificacionesListProcess&process.accion=listarVolver";
	}
</script>
