<%@ page language="java"%>
<%@page import="com.asf.cred.business.CreditoEstados"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<%pageContext.setAttribute( "agregaEstado", CreditoEstados.agregaEstado ); %>
<%pageContext.setAttribute( "agregaObs",    CreditoEstados.agregaObs ); %>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
    <html:hidden property="process.accion" value="guardar" styleId="accion"/>
    <html:hidden property="process.objetoi.id" styleId="objetoi.id" value="${ProcessForm.process.objetoi.id}"/>
	<html:hidden property="process.agrega" styleId="agrega"/>
    <div style="width:70%" align="left"><html:errors/></div>
    <table>
		<tr>
			<td>
				<input name="funcion" type="radio" onclick="habilitarFuncion( false );" id="agregaEstado">Modificar Estado
			</td>
			<td>
				<input name="funcion" type="radio" onclick="habilitarFuncion( false );" id="agregaObs">Agregar Observaci�n
			</td>
		</tr>
	</table>
<br/>
<table>
	<tr id="filaEstado">
		<th>Nombre: </th>
	    <td>
	    	 <asf:select name="process.estado.idEstado" entityName="Estado" listCaption="idEstado,nombreEstado"
            			 listValues="idEstado"
                         value="${ProcessForm.process.estado.idEstado}"
                         nullValue="true" nullText="Seleccione una Etapa"
                         filter="tipo = 'Objetoi' AND manual = true"/>
	   </td>
	</tr>
	<tr id="filaObs">
		<th>Detalle Observaci�n: </th>
		<td>
			<html:textarea property="process.detalleObservacion" 
			               onkeypress="caracteres(this,1000,event)" cols="60" rows="5"/>
		</td>
	</tr>
	<tr>
	<tr>
		<th>N�mero de Resoluci�n:</th>
		<td>
			<html:text name="ProcessForm" property="process.numeroResolucion" size="10" maxlength="10"></html:text>
		</td>
	</tr>
	<tr id="doc">
		<th>Documento: </th>
		<td colspan="3">
			<html:file property="process.formFile" styleId="formFile"/>
		</td>
	</tr>
    <tr id="filaObsEstado">
    	<th>Observaciones: </th>
        <td>
	        <html:textarea property="process.obsEstado"
		                   onkeypress="caracteres(this,1000,event)" cols="60" rows="5"/>
        </td>
    </tr>
    
    </table>
    <br>
    <input type="button" value="Guardar" onclick="validarDesistido();" />
    <input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
        src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<script language='javascript'>
//CONTROLES
	var accion            = $('accion');
	var formulario        = $('oForm');
	var ctrlAgrega        = $('agrega');
	var ctrlAgregaEstado  = $('agregaEstado');
	var ctrlAgregaObs     = $('agregaObs');
	var ctrlFilaEstado    = $('filaEstado');
	var ctrlFilaObs       = $('filaObs');
	var ctrlFilaObsEstado = $('filaObsEstado');
//AL CARGAR LA P�GINA
function validarDesistido(){
	var co = $('process.estado.idEstado');
	var texto = co[co.selectedIndex].text;
	if(texto.indexOf("DESISTIDO") != -1){
		if (confirmMsg("�Desea desistir este Cr�dito?")) {
		}else{
		    accion.value = "cancelar";
		}
	}
	formulario.submit();
}

habilitarFuncion(true);

	function cancelar()
    {
	    accion.value = "cancelar";

	    formulario.submit();
    }//fin cancelar.-

    function habilitarFuncion( primeraVez )
    {
    	if(primeraVez)
    	{
    		ctrlAgregaEstado.checked = true;
    	}
    	
    	if(ctrlAgregaEstado.checked)
    	{
			ctrlAgrega.value                = '${agregaEstado}';
        	ctrlFilaEstado.style.display    = "";
        	ctrlFilaObs.style.display       = "none";
        	ctrlFilaObsEstado.style.display = "";
    	}
        else if(ctrlAgregaObs.checked)
    	{
        	ctrlAgrega.value                = '${agregaObs}';
        	ctrlFilaObs.style.display       = "";
        	ctrlFilaEstado.style.display    = "none";
        	ctrlFilaObsEstado.style.display = "none";
    	}
    }//fin habilitarFuncion.-

    Event.observe(window, 'load', function() 
    {
  		this.name = "Estados y Observaciones";
    	parent.push(this);
    });
</script>