<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<br>
<br>

<html:form action="/actions/RequisitosAction.do?do=save" enctype="multipart/form-data" styleId="RequisitosForm">
  <html:hidden name="cargaRequisitoForm" property="sol" value="${cargaRequisitoForm.credito.id}" />
  <table>
    <logic:iterate id="cargaReq" name="requisitosForm" property="cargaRqList" indexId="index">
      <html:hidden property="requisito(${cargaReq.requisito.id})" value="${cargaReq.requisito.id}" />
      <tr>
        <th></th>
        <th>
          ${cargaReq.requisito.nombre}
        </th>
      </tr>
      <tr>
        <th>
          ID:
        </th>
        <td>
          <html:text readonly="true" property="id(${cargaReq.requisito.id})" value="${cargaReq.id}" />
        </td>
      </tr>
      <tr>
        <th>
          Fecha Cumplimiento:
        </th>
        <td>
          <asf:calendar property="fecha(${cargaReq.requisito.id})" value="${cargaReq.fechaCumplimientoStr}" readOnly="${RequisitosForm.tieneExpediente}"/>
        </td>
      </tr>
      <tr>
        <th>
          Observaciones:
        </th>
        <td>
          <html:textarea property="observacion(${cargaReq.requisito.id})" value="${cargaReq.observaciones}" cols="80" rows="5" />
        </td>
      </tr>
      <tr>
        <th>
          Documento:
        </th>
        <td>
          <html:file property="testFile(${cargaReq.requisito.id})" value="" />
          <logic:notEmpty name="cargaReq" property="objetoiArchivo">
          	${cargaReq.objetoiArchivo.nombre }
          	<html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${cargaReq.objetoiArchivo.id}')" property="process.objetoiArchivo.id">Bajar</html:button>
           
            
          </logic:notEmpty>
        </td>
      </tr>
    </logic:iterate>
  </table>
  <asf:security action="/actions/RequisitosAction.do" access="do=save&entityName=${param.entityName}">
    <html:submit>
      <bean:message key="abm.button.save" />
    </html:submit>
  </asf:security>
  <html:cancel>
    <bean:message key="abm.button.cancel" />
  </html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
<!-- Raul Varela -->