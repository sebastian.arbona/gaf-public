<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Vi�edo</div>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
<html:hidden name="ProcessForm" property="process.existe" styleId="existe" />
<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
<html:hidden name="ProcessForm" property="process.idSolicitud" styleId="solicitud" />
<html:hidden name="ProcessForm" property="process.objetoiVinedo.id" styleId="objetoiVinedo" />
<html:hidden name="ProcessForm" property="process.elaboracionTabla" styleId="elaboracionTabla" />

<html:errors/>
<table>
<tr>
	<th>C�digo:</th>
	<td><asf:text type="text" property="process.objetoiVinedo.vinedo.codigo" maxlength="30"
			value="${ProcessForm.process.objetoiVinedo.vinedo.codigo}"
			readonly="true" name="ProcessForm"/>
	</td>
</tr>
<tr>
	<th>Hect�reas:</th>
	<td><asf:text type="decimal" property="process.objetoiVinedo.vinedo.hectareas" maxlength="30"
				value="${ProcessForm.process.objetoiVinedo.vinedo.hectareas}" name="ProcessForm"/>
	</td>
</tr>
<tr>
	<th>Observaciones:</th>
	<td><html:textarea  property="process.objetoiVinedo.vinedo.observaciones" 
			value="${ProcessForm.process.objetoiVinedo.vinedo.observaciones}"/>
	</td>
</tr>

<tr>
<th>Localidad:</th>
<td>
	<asf:select name="process.objetoiVinedo.vinedo.localidad.id"
	entityName="com.civitas.hibernate.persona.Localidad" 
	value="${ProcessForm.process.objetoiVinedo.vinedo.localidad.id}"
	listCaption="nombre" listValues="id" orderBy="nombre"
	nullValue="true" nullText="&lt;&lt;Ninguna&gt;&gt;"/>					
</td> 
</tr>
<tr>
	<th>QQ Estimados:</th>
	<td><asf:text type="decimal" property="process.objetoiVinedo.qqEstimados" maxlength="30"
			value="${ProcessForm.process.objetoiVinedo.qqEstimados}" name="ProcessForm"/>
	</td>		
</tr>     
<logic:equal value="true" name="ProcessForm" property="process.elaboracionTabla">
<tr>
	<th>QQ Aprobados:</th>
	<td><asf:text type="decimal" property="process.objetoiVinedo.qqAprobadoElab" maxlength="30"
				value="${ProcessForm.process.objetoiVinedo.qqAprobadoElab}" name="ProcessForm"
				readonly="true"/>
	</td>
</tr>
<tr>
	<th>Fecha Aprobado:</th>
	<td><asf:calendar property="process.objetoiVinedo.fechaInformeINVStrElab" maxlength="30"
				value="${ProcessForm.process.objetoiVinedo.fechaInformeINVStrElab}"
				readOnly="true"/>
	</td>
</tr>
</logic:equal>
<logic:notEqual value="true" name="ProcessForm" property="process.elaboracionTabla">
<tr>
	<th>QQ Aprobados:</th>
	<td><asf:text type="decimal" property="process.objetoiVinedo.qqAprobado" maxlength="30"
				value="${ProcessForm.process.objetoiVinedo.qqAprobado}" name="ProcessForm"
				readonly="true"/>
	</td>
</tr>
<tr>
	<th>Fecha Aprobado:</th>
	<td><asf:calendar property="process.objetoiVinedo.fechaInformeINVStr" maxlength="30"
				value="${ProcessForm.process.objetoiVinedo.fechaInformeINVStr}"
				readOnly="true"/>
	</td>
</tr>
</logic:notEqual>
</table>
	<input type="button" value="Guardar" onclick="guardar();">
	<input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<script type="text/javascript">
function guardar(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm"); 
	accion.value = "guardarVinedo";
	formulario.submit();
}
function cancelar(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm"); 
	accion.value = "cancelar";
	formulario.submit();
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
