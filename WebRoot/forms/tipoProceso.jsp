<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
<div class="title">
	Tipo Proceso
</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<logic:equal parameter="do" value="load">
		<html:hidden property="entity.nuevo" value="false" />
	</logic:equal>
	<table>
	
		<tr>
			<th>Nombre del Tipo:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.nombreTipoProceso"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.nombreTipoProceso}</td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Carátula:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.caratula"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.caratula" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Expediente</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.expediente"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.expediente" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Juzgado</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.juzgado"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.juzgado" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Tipo Juzgado</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipo"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipo" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Circunscripción</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.circunscripcion"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.circunscripcion" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Tipo  de Tribunal</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoTribunal"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoTribunal" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Monto</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.monto"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.monto" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Definitivo</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.definitivo"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.definitivo" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Fecha Presentación</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.fechaPresentacion"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.fechaPresentacion" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Fecha Sentencia</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.fechaSentencia"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.fechaSentencia" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Fecha Embargo</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.fechaEmbargo"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.fechaEmbargo" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Nombre Excepción</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.nombre"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.nombre" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Plantilla Mail </th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.nombre"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.nombre" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Notificación </th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.notificacion"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.notificacion" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Tipo Registro</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoRegistro"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoRegistro" disabled="true"/></td>
			</logic:notEqual>
		</tr>	<tr>
			<th>Requiere Tipo Embargo</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoEmbargo"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoEmbargo" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Tipo Medida</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoMedida"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoMedida" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Cámara</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.camara"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.camara" disabled="true"/></td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Requiere Tipo Instancia</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoInstancia"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.tipoInstancia" disabled="true"/></td>
			</logic:notEqual>
		</tr>

	</table>
	<br/>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.selectboxes.min.js"></script>
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.colorPicker.js"/></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/colorPicker.css" type="text/css" />
