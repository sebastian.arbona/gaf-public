<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Modificaci&oacute;n de datos INV</div>
<br><br>
<html:form action="/actions/process.do?do=process&processName=EdicionQQ" styleId="ProcessForm">
<html:hidden property="process.action" value="guardar"/>
<html:hidden property="process.cid"/>

<table border="0">
<tr><th>C&oacute;digo Variedad:</th><td>${ProcessForm.process.detalle.codigoVariedad}</td></tr>
<tr><th>Nombre Variedad:</th><td>${ProcessForm.process.detalle.nombreVariedad}</td></tr>
<tr><th>Hect&aacute;reas:</th><td><asf:text name="ProcessForm" type="decimal" property="process.detalle.hectareas" maxlength="40"/></td></tr>
<tr><th>QQ:</th><td><asf:text name="ProcessForm" type="decimal" property="process.detalle.qq" maxlength="40"/></td></tr>
</table>
	<html:submit><bean:message key="abm.button.save"/></html:submit>
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>