<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">
	Tipo Actividad Agenda Form
</div>
<br>
<br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>Id:</th>
			<td>
				<asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true"/>
			</td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td>
				<html:text property="entity.nombre" maxlength="30"/>
			</td>
		</tr>
		<tr>
			<th>Descripción:</th>
			<td>
				<asf:select entityName="org.jbpm.identity.Group"
							listCaption="getId,getName" listValues="getId"
							name="entity.rol_id" value="${AbmForm.entity.rol_id}"/>
			</td>
		</tr>
	</table>

	<br>
	<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>