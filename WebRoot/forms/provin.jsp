<%@ page language="java"%>
<%@page import="com.asf.security.BusinessPersistance"%>
<%@page import="com.asf.security.SessionHandler"%>
<%@page import="com.civitas.hibernate.persona.Pais"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

	<bean:define id="idpais" value="${paramValue[0]}"/>
	<%
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Pais pais = (Pais)bp.getById(Pais.class,new Long(idpais));
	%>
<div class="title">Provincia</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
<table border="0">
	<bean:define id="filter" name="paramValue" scope="session" type="Object[]"/>
	<html:hidden property="entity.codi08"/>
	<html:hidden property="entity.idpais" value="${filter[0]}"/>	
	<html:hidden property="filter" value="true"/>

	<tr>
		<th>Pa�s:</th>
		<td>
			<b><%=pais.getNombrePais()%></b>
		</td>
	</tr>
	<tr>
		<th>C�digo:</th>
		<td>
			<html:text property="entity.codi08" readonly="true"/>
		</td>
	</tr>
	<tr>
		<th>Detalle:</th>
		<td>
			<html:text property="entity.deta08" maxlength="30"/>
		</td>
	</tr>
	<tr>
		<th>Abreviatura:</th>
		<td>
			<html:text property="entity.abre08" maxlength="5"/>
		</td>
	</tr>
	<tr>
		<th>C�digo D.G.I.:</th>
		<td>
			<asf:text type="long" name="AbmForm" property="entity.cdgi08" maxlength="2"/>
		</td>
	</tr>
</table>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>