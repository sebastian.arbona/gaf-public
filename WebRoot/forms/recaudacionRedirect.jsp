<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>

<script type="text/javascript">
	window.location = 
		'${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos' +
		'&process.fenv12Str=${ProcessForm.process.fenv12Str}';
</script>

<html:errors/>

<a href="javascript:history.go(-1)">Volver</a>