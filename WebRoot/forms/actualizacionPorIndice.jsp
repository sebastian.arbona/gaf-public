<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">
  Actualizaci�n por �ndice
</div>
<br>
<div style="width: 70%" align="left">
  <html:errors />
</div>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" styleId="accion" value="actualizar"/>
  <html:hidden property="process.cid" />
  
    <logic:notEmpty name="ProcessForm" property="process.resultadoCalculo">
	<display:table name="ProcessForm" property="process.resultadoCalculo" id="filaTabla" export="true">
		<display:setProperty name="report.title" value="Actualizacion por Indice"></display:setProperty>
		<display:column title="Nro Solicitud" property="credito.numeroAtencion" sortable="true" />
		<display:column title="Expediente" property="credito.expediente" sortable="true" />
		<display:column title="Titular" property="credito.persona.nomb12" sortable="true" />
		<display:column title="L�nea" property="credito.linea.nombre" sortable="true" />
		<display:column title="Estado" property="credito.estadoActual.estado.nombreEstado" sortable="true" />
		<display:column title="Cuota N�" property="numeroCuota"></display:column>
		<display:column title="Saldo Capital" property="saldoCapital" style="text-align:right" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
		<display:column title="�ndice CER" property="indice" style="text-align:right" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
		<display:column title="Importe CER" property="montoActualizacion" style="text-align:right" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
	</display:table>
	</logic:notEmpty>
   	<br/>
	<input type="button" value="Volver" onclick="cancelar();">
	<logic:notEmpty name="ProcessForm" property="process.resultadoCalculo">
		<input type="button" value="Impactar" onclick="actualizar();">
	</logic:notEmpty>  
</html:form>

<script type="text/javascript">
var accion = $('accion');
var formulario = $('oForm');

negritaSubSaldo();

function negritaSubSaldo() {
	var collection = (document.getElementById("filaTabla")).getElementsByTagName("tr");

	for(var i = 0; i < collection.length; i++) {
		if(collection[i].cells[0].textContent.trim() == "TOTAL" || collection[i].cells[0].textContent.trim() == "Incremento") {
			collection[i].style.fontWeight='bold';
		}
	}
}

function actualizar() {
	window.open("${pageContext.request.contextPath}/progress.jsp?reset=true", "Proceso", "width=450, height=180");
	accion.value = 'actualizar';
	formulario.submit();
}

function cancelar() {
	window.location = 
 	'${pageContext.request.contextPath}/actions/process.do?do=process' +
	'&processName=${param.processName}';
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>