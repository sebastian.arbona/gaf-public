<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script language="javascript" type="text/javascript">
	function exportar() {
	var form = $('oForm');
		var accion = $('accion');
		accion.value = "EXPORTAR";
		form.submit();
	}
</script>


<div class="title">
	Emisi�n de Deuda a Entes Recaudadores
</div>
<br>
<br>

<html:form
	action="/actions/emisionDeuda.do?do=generar"
	styleId="oForm">

	<table border="0">

		<tr>
			<th>
				Ente recaudador:
			</th>

			<td>

				<asf:select name="idBanco"
					entityName="com.asf.gaf.hibernate.Bancos"
					value="${EmisionDeudaForm.idBanco}" listCaption="getDetaBa"
					listValues="getCodiBa" filter="recaudador = 1  order by codiBa" />

			</td>

		</tr>

	</table>

	<html:submit value="Generar"></html:submit>

	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>

