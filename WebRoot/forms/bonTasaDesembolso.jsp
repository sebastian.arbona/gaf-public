<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<%@page import="com.nirven.creditos.hibernate.DesembolsoBonTasa"%>


<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<html:form
	action="/actions/process.do?do=process&processName=BonTasaDatosFinancieros"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />
	<html:hidden property="process.idBonTasa" styleId="idBonTasa"
		value="${ProcessForm.process.idBonTasa}" />
	<html:hidden property="process.idCompensatorio" styleId="comp"
		value="${ProcessForm.process.idCompensatorio}" />
	<html:hidden name="ProcessForm" property="process.indice"
		value="${ProcessForm.process.indice}" />
	<html:hidden name="ProcessForm" property="process.desembolso.id" value="${ProcessForm.process.desembolso.id}"/>

	<html:errors></html:errors>
	<logic:equal value="true" property="process.error" name="ProcessForm">
		<div style="font-size: 16">
	  		No se puede guardar, todos los datos son obligatorios
	  	</div>
	</logic:equal>
	<logic:equal value="true" property="process.errorEjecutar" name="ProcessForm">
		<div style="font-size: 16">
	  		No se puede ejecutar, falta cargar los datos financieros
	  	</div>
	</logic:equal>
	<logic:equal value="true" property="process.crearDesembolso"
		name="ProcessForm">
		<table>
			<tr>
				<th>
					Fecha
				</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaDesembolsoStr"
						readOnly="${ProcessName.process.generada}" value="${ProcessForm.process.fechaDesembolsoStr}"
						/>
				</td>
			</tr>
			<tr>
				<th>
					Importe
				</th>
				<td>
					<asf:text property="process.importeDesembolso" maxlength="20" type="decimal"
						readonly="${ProcessForm.process.generada}" value="${ProcessForm.process.importeDesembolso}"/>
				</td>
			</tr>
		</table>
		<input type="button" value="Guardar" onclick="guardar();" />
	</logic:equal>
	<br/>
	<br/>
	<logic:equal value="prepararEjecucion" property="process.accion" name="ProcessForm">
		<table>
			<tr>
				<th>
					Fecha
				</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaDesembolsoStr" value="${process.fechaDesembolsoStr}" />
				</td>
			</tr>
			<tr>
				<th>
					Importe Real
				</th>
				<td>
					<asf:text property="process.importeReal" name="ProcessForm" maxlength="20" type="decimal" value="${ProcessForm.process.importeReal}"/>
				</td>
			</tr>
			<tr>
				<th>N�mero de pr�stamo</th>
				<td>
					<html:text property="process.numeroPrestamo" maxlength="30" name="ProcessForm" />
				</td>
			</tr>
		</table>
		<input type="button" value="Ejecutar" onclick="ejecutar(${ProcessForm.process.desembolso.id});" />
	</logic:equal>
	
	<logic:notEqual value="prepararEjecucion" property="process.accion" name="ProcessForm">
		<logic:empty name="ProcessForm" property="process.desembolsos">
			<div style="font-size: 16">
				No hay desembolsos
			</div>
		</logic:empty>
		<logic:notEmpty name="ProcessForm" property="process.desembolsos">
			<display:table id="desembolso" name="ProcessForm" property="process.desembolsos" class="com.nirven.creditos.hibernate.DesembolsoBonTasa">
				<display:column title="Numero" property="numero" />
				<display:column title="Fecha" property="fechaStr" />
				<display:column title="Numero Prestamo" property="numeroPrestamo"/>
				<display:column title="Estado" property="estado" />
				<display:column title="Importe" property="importe" />
				<display:column title="Ejecutar">
					<logic:equal value="Creado" name="desembolso" property="estado">
						<input type="button" name="desembolso" value="Ejecutar" onclick="preparar(${desembolso.id});">
					</logic:equal>
				</display:column>
				<display:column title="Eliminar">
					<logic:equal value="Creado" name="desembolso" property="estado">
						<input type="button" name="desembolso" value="Eliminar" onclick="eliminar(${desembolso.id});">
					</logic:equal>
				</display:column>
			</display:table>
		</logic:notEmpty>
		<br/>
		<br/>
		<logic:equal value="false" property="process.generada"
			name="ProcessForm">
			<div style="width: 100%; text-align: center">
				<input type="button" value="Nuevo" onclick="nuevo();" />
			</div>
		</logic:equal>
	</logic:notEqual>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">

	function nuevo(){			
		window.location = 
			'${pageContext.request.contextPath}/actions/process.do?do=process' + 
			'&processName=BonTasaDatosFinancieros' + 
			'&process.accion=mostrarDesembolsos' +
			'&process.idBonTasa=${ProcessForm.process.idBonTasa}' +
			'&process.crearDesembolso=true';
	}
		
	function guardar() {			
		window.location = 
			'${pageContext.request.contextPath}/actions/process.do?do=process' + 
			'&processName=BonTasaDatosFinancieros' + 
			'&process.accion=crearDesembolso' +
			'&process.idBonTasa=${ProcessForm.process.idBonTasa}' +
			'&process.fechaDesembolsoStr=' + document.getElementById('process.fechaDesembolsoStr').value + 
			'&process.importeDesembolso=' + document.getElementById('process.importeDesembolso').value; 
	}

	function preparar(id){
		window.location = 
			'${pageContext.request.contextPath}/actions/process.do?do=process' + 
			'&processName=BonTasaDatosFinancieros' + 
			'&process.accion=prepararEjecucion' +
			'&process.idBonTasa=${ProcessForm.process.idBonTasa}' + 
			'&process.desembolso.id=' + id;
	}
	
	function ejecutar(id){
		var form = document.getElementById('oForm');
		var action = document.getElementById('accion');
		action.value = 'ejecutarDesembolso';
		form.submit();
	}

	function eliminar(id){			
		window.location = 
			'${pageContext.request.contextPath}/actions/process.do?do=process' + 
			'&processName=BonTasaDatosFinancieros' + 
			'&process.accion=eliminarDesembolso' +
			'&process.idBonTasa=${ProcessForm.process.idBonTasa}' + 
			'&process.desembolso.id=' + id;
	}
	</script>