<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@page import="com.asf.cred.business.BonDetalleDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>


<div class="title">Reporte de Bonificaciones Otorgadas</div>
<br>
<div style="width: 70%" align="left"><html:errors /></div>
<br/>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion"/>
	
	<table>
	<tr>
        <th>
          Ente Bonificador:
        </th>
        <td>
          <asf:select entityName="com.asf.gaf.hibernate.Bancos" listCaption="getDetaBa" 
            listValues="getCodiBa" name="process.ente.id" value="${ProcessForm.process.ente.id}"
            nullValue="true" nullText="&lt;&lt;Seleccione Ente&gt;&gt;" attribs="class='id'" filter="bonificador = 1  order by codiBa"
          />
        </td>
      </tr>
     <tr>
      <th>Ejercicio:</th>
		<td>
		<asf:select name="process.ejercicio.ejercicio" entityName="com.asf.gaf.hibernate.Ejercicio" listCaption="ejercicio,denominacion"
		listValues="ejercicio" orderBy="ejercicio"	value="${ProcessForm.process.ejercicio.ejercicio}" 
		nullValue="true" nullText="&lt;&lt;Seleccione Ejercicio&gt;&gt;"
		/>
		</td>
	</tr>
   	 <tr>
	      <th>
			Mes Desde:
	   	 </th>
	   	 <td>
			<html:select property="process.mesDesde" name="ProcessForm">
				<html:option value="-1">&lt;&lt;Seleccione Mes Desde&gt;&gt;</html:option>
				<html:option value="0">ENERO</html:option>
				<html:option value="1">FEBRERO</html:option>
				<html:option value="2">MARZO</html:option>
				<html:option value="3">ABRIL</html:option>
				<html:option value="4">MAYO</html:option>
				<html:option value="5">JUNIO</html:option>
				<html:option value="6">JULIO</html:option>
				<html:option value="7">AGOSTO</html:option>
				<html:option value="8">STIEMBRE</html:option>
				<html:option value="9">OCTUBRE</html:option>
				<html:option value="10">NOVIEMBRE</html:option>
				<html:option value="11">DICIEMBRE</html:option>
			</html:select>			
	   	 </td>
   	 </tr>
   	 <tr>
	      <th>
			Mes Hasta:
	   	 </th>
	   	 <td>
			<html:select property="process.mesHasta" name="ProcessForm">
				<html:option value="-1">&lt;&lt;Seleccione Mes Hasta&gt;&gt;</html:option>
				<html:option value="0">ENERO</html:option>
				<html:option value="1">FEBRERO</html:option>
				<html:option value="2">MARZO</html:option>
				<html:option value="3">ABRIL</html:option>
				<html:option value="4">MAYO</html:option>
				<html:option value="5">JUNIO</html:option>
				<html:option value="6">JULIO</html:option>
				<html:option value="7">AGOSTO</html:option>
				<html:option value="8">STIEMBRE</html:option>
				<html:option value="9">OCTUBRE</html:option>
				<html:option value="10">NOVIEMBRE</html:option>
				<html:option value="11">DICIEMBRE</html:option>
			</html:select>			
	   	 </td>
   	 </tr>
	</table>
	<br/>
	<input type="button" value="Listar" id="listarId" onclick="listar();"/>
	<br/>
    <br/>	
	<logic:notEmpty name="ProcessForm" property="process.creditos">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.creditos" id="credito" class="com.asf.cred.business.ObjetoiDTO" export="true" defaultsort="1"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:setProperty name="report.title" value="Bonificaciones desde ${ProcessForm.process.mesDesdeStr} hasta ${ProcessForm.process.mesHastaStr} del a�o ${ProcessForm.process.ejercicio.ejercicio}"></display:setProperty>
            <display:column title="Expediente" property="expediente" sortable="true"/>
            <display:column title="Nro de Cr�dito" property="numeroAtencion" sortable="true"/>
            <display:column title="Titular" property="titular.nomb12" sortable="true"/>
            <display:column title="Cuil/Cuit" property="titular.cuil12" sortable="false"/>
            <display:column title="Nro de Cuota" property="numeroCuota" sortable="false"/>
            <logic:iterate name="credito" id="bonDetalleMensual" property="bonDetalleDTOs">
            	<display:column title="${credito.mes}" property="monto"/>
            </logic:iterate>
    	</display:table>	
    </div>	
    </logic:notEmpty>
    <logic:empty name="ProcessForm" property="process.creditos">
    	No hay bonificaciones
    </logic:empty>
    
</html:form>

<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');
function listar(){
	accion.value = 'listar';
	formulario.submit();
}
</script>