<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>

<%pageContext.setAttribute( "fechaActual",DateHelper.getString(new Date()) );%>

<div class="title">Dar de Baja la Persona: ${personaForm.entity.idpersona} - ${personaForm.entity.nomb12}</div>
<br>
<html:form action="/actions/PersonaAction.do?do=delete&entityName=${param.entityName}&filter=true">
    <html:hidden property="entity.codi01" value="101"/>
    <html:hidden property="entity.idpersona"/>
	<html:hidden property="paramName[0]" value="idpersona"/>
	<html:hidden property="paramComp[0]" value="="/>

    <table border="0" align="center">
        <tr>
        	<th>Fecha de Baja: </th>
        	<td>
        		<asf:calendar property="entity.fechaBajaStr" value="${fechaActual}"/>
        	</td>
        </tr>
        <tr>
    		<th>Motivo de Baja: </th>    
    		<td>
	        	<html:textarea property="entity.motivoBaja"
		                       onkeypress="caracteres(this,1000,event)" cols="60" rows="5"/>
        	</td>
        </tr>
    </table>
    <br>

    <asf:security action="/actions/PersonaAction.do" access="do=delete&entityName=${param.entityName}">
    	<html:submit>Dar de Baja</html:submit>
    </asf:security>
    <html:cancel><bean:message key="abm.button.cancel"/></html:cancel>

</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>