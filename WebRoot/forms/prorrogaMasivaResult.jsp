<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="grilla">
	<div class="title">Pr&oacute;rroga de vencimientos masiva</div>
	<br>
	<form>
		<logic:notEmpty name="ProcessForm"
			property="process.cuotasProrrogadas">
			<table>
				<tr>
					<th>N&uacute;mero de resoluci&oacute;n:</th>
					<td>${ProcessForm.process.prorroga.resolucion}</td>
				</tr>
				<tr>
					<th>Observaciones:</th>
					<td>${ProcessForm.process.prorroga.observaciones}</td>
				</tr>
				<tr>
					<th>Usuario:</th>
					<td>${ProcessForm.process.prorroga.usuario.id}</td>
				</tr>
			</table>
			<h3 style="margin-top: 30px">Cuotas con prorroga exitosa</h3>
			<display:table name="ProcessForm"
				property="process.cuotasProrrogadas" export="true"
				id="cuotasProrrogadas" class="nocaption"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas con prorroga exitosa
				Resolucion ${ProcessForm.process.prorroga.resolucion} - Usuario ${ProcessForm.process.prorroga.usuario.id} 
				Observaciones: ${ProcessForm.process.prorroga.observaciones}
			</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="credito.persona.cuil12" title="CUIT" />
				<display:column property="credito.expediente" title="Expediente" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimientoOriginal"
					title="Fecha de vencimiento original" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento nueva" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter�s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
		<logic:notEmpty name="ProcessForm"
			property="process.cuotasFechaSuperpuesta">
			<display:table name="ProcessForm"
				property="process.cuotasFechaSuperpuesta" export="true"
				id="cuotasFechaSuperpuesta"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas no prorrogadas, la nueva fecha se superpone con la siguiente cuota del plan</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter�s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
		<logic:notEmpty name="ProcessForm"
			property="process.cuotasFechaAnterior">
			<display:table name="ProcessForm"
				property="process.cuotasFechaAnterior" export="true"
				id="cuotasFechaAnterior"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas no prorrogadas, la nueva fecha es anterior a la fecha actual</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter�s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
		<logic:notEmpty name="ProcessForm" property="process.cuotasCaducidad">
			<display:table name="ProcessForm" property="process.cuotasCaducidad"
				export="true" id="cuotasCaducidad"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas no prorrogadas, el cr&eacute;dito tiene Caducidad de Plazo</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter�s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
	</form>
</div>