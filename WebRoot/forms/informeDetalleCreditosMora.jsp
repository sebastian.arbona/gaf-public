<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
		
<style>
.divScroll {
     overflow:scroll;
     height:400px;
     width:500px;
}
</style>
																							
<div class="title" style="margin-bottom: 30px">Informe Detalle Credito Mora</div>
<html:form action="/actions/process.do?do=process&processName=InformeCreditosMoraProcess"
	styleId="oForm">
	     <html:hidden property="process.action" styleId="action"/>

		<!-- ESTA GRILLA VA EN EL VER DE LA TABLA -->
	<br/><br/>
	<div class="grilla divScroll">
	<logic:notEmpty name="ProcessForm" property="process.detalleCreditoMora">
	<display:table name="ProcessForm" property="process.detalleCreditoMora" id="informeCreditoTable" export="true" 
	requestURI="${pageContext.request.contextPath}/actions/process.do">
	    <display:setProperty name="report.title" value="Detalle Informe Credito Mora"></display:setProperty>
		<display:column property="proyecto" title="Proyecto"/>
		<display:column property="nroCreditoAnt" title="Nro Credito Ant." sortable="true"></display:column>
		<display:column property="departamento" title="Departamento" sortable="true" />	
		<display:column property="idTitutar" title="ID Titular" sortable="true" />
		<display:column property="titular" title="Titular" sortable="true" />
		<display:column property="cuit" title="CUIT" sortable="true" />
		<display:column property="linea" title="Linea" sortable="true" />
		<display:column property="estado" title="Estado" sortable="true"/>
		<display:column property="etapa" title="Etapa" sortable="true" />			
		<display:column property="fechaMora" title="Fecha De Mora" sortable="true" />
		<display:column property="diasAtraso" title="D�as de atraso" sortable="true" />
		<display:column property="importeTotalAdeudado" title="Importe Total Adeudado" sortable="true" />
		<display:column property="deudaExigible" title="Deuda Exigible" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"/>
		<display:column property="fechaUltimaCobranza" title="Fecha �ltima Cobranza" decorator="com.asf.displayDecorators.DateDecorator"></display:column>
		<display:column property="moneda" title="Moneda"></display:column>
		<display:column property="cotizacion" title="Cotizacion" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="bonificado" title="bonificado"/>
		<display:column property="valoresCartera" title="Valores Cartera" sortable="true" />
		<display:column property="tipoUltimaNotificacion" title="Tipo �ltima Notificaci�n"/>
		<display:column property="observacionUltimaNotificacion" title="Observaci�n �ltima Notificaci�n"/>
		<display:column property="fechaUltimaNotificacion" title="Fecha �ltima Notificaci�n"/>
		<display:column property="garantia" title="Garant�a SGR"/>
		<display:column property="fechaBonificacionCaida" title="Fecha Ca�da Bonificaci�n" sortable="true" />
		<display:column property="seguroCaido" title="Seguro Vencido"/>
		<display:column property="ultimaProrroga" title="�ltima Pr�rroga"/>
		<display:column property="estadoUltimaProrroga" title="Estado �ltima Pr�rroga"/>
		<display:column property="aplicoFondos" title="Aplic� fondos" sortable="true" />	
	</display:table>
	</logic:notEmpty>
	</div>
	<html:submit value="Volver"></html:submit>
</html:form>

<script type="text/javascript">
var accion = $('accion');
var formulario = $('oForm');
</script>
