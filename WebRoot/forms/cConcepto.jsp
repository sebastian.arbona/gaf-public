<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<div class="title">
  Concepto
</div>
<br>
<br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
  <table border="0">
    <tr>
      <th>
        Codigo:
      </th>
      <td>
        <html:text property="entity.concepto" maxlength="30" />
      </td>
    </tr>
    <tr>
      <th>
        Detalle:
      </th>
      <td>
        <html:textarea styleId="detalle" onkeypress="caracteres(this,1000,event)" property="entity.detalle" cols="40" rows="5" />
      </td>
    </tr>
    <tr>
      <th>
        Abreviatura:
      </th>
      <td>
        <html:textarea styleId="abreviatura" onkeypress="caracteres(this,250,event)" property="entity.abreviatura" cols="40" rows="5" />
      </td>
    </tr>
  </table>
  <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
    <html:submit>
      <bean:message key="abm.button.save" />
    </html:submit>
  </asf:security>
  <html:cancel>
    <bean:message key="abm.button.cancel" />
  </html:cancel>
</html:form>