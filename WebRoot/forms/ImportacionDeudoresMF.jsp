<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@page import="com.asf.cred.business.ProgressStatus"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>

<html:form
	action="/actions/importacionConsultaDeuda.do?do=importarDeudoresMF"
	method="post" enctype="multipart/form-data" styleId="oForm">

	<table
		style="border: solid; border-color: #CCCCCC; width: 582px; float: left;">
		<tr>
			<th>Seleccione Archivo:</th>
			<td><html:file name="ImportacionForm" property="file"
					styleId="archivo" size="50"
					disabled="${ImportacionForm.deshabilitado}" /></td>
			<td style="vertical-align: middle;">
			<html:submit onclick="this.form.submit(); this.disabled=true; this.value='Enviando...'">Importar</html:submit></td>
		</tr>
	</table>
	<br />
	<logic:notEmpty name="ImportacionForm" property="resumen">
		<div style="text-align: left; font-size: 12px; margin-top: 45px;">${ImportacionForm.resumen}</div>
	</logic:notEmpty>
	<div style="width: 70%" align="center">
		<html:messages id="mensajes" />
	</div>
	<div style="text-align: left; font-size: 12px">
		<html:errors />
	</div>
	<logic:notEmpty name="ImportacionForm" property="nroReporte">
		<a href="javascript://" onclick="return reporte();">Errores de
			Nro: <bean:write name="ImportacionForm" property="nroReporte" /><a /><br>
	</logic:notEmpty>
	<br />
	<br />
</html:form>
