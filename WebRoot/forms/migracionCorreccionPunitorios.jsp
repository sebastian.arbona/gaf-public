<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">Informe Sintético de Desembolsos</div>
<br><br>
<html:form action="/actions/process.do?do=process&processName=MigracionCorreccionPunitorios">
<logic:empty name="ProcessForm" property="process.resultado">
	<html:hidden property="process.action" value="run"/>
	<html:submit value="Procesar"></html:submit>
</logic:empty>
<logic:notEmpty name="ProcessForm" property="process.resultado">
	<div>Total de filas: ${ProcessForm.process.total}<br/>
	Procesadas: ${ProcessForm.process.procesados}
	</div>
	<display:table name="ProcessForm" property="process.resultado" export="true" id="cc">
		<display:column title="Credito" property="id.objetoi.numeroAtencion" />
		<display:column title="Cuota" property="cuota.numero" />
		<display:column title="Fecha" property="fechaGeneracion" decorator="com.asf.displayDecorators.DateDecorator"/>
		<display:column title="Concepto" property="facturado.concepto.abreviatura" />
		<display:column title="Monto" property="importe" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" style="text-align: right"/>
	</display:table>
</logic:notEmpty>
</html:form>
