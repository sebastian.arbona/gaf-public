<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Estado de Seguro</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=GarantiaSeguroEstadoProcess"
	styleId="formulario">
	<html:hidden property="process.action" value="guardar" styleId="action" />
	<html:hidden property="process.cid" />

	<table border="0">
		<tr>
			<th>Estado:</th>
			<td><asf:select name="process.nuevoEstado"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getDescripcion" listValues="getCodigo"
					value="${ProcessForm.process.nuevoEstado}"
					filter="categoria='seguro.estado' order by codigo" /></td>
		</tr>
		<tr>
			<th>Observaciones:</th>
			<td><html:textarea property="process.observaciones" rows="5"
					cols="50" /></td>
		</tr>
	</table>
	<html:submit>
		<bean:message key="abm.button.save" />
	</html:submit>
	<html:submit onclick="volver();">
		<bean:message key="abm.button.cancel" />
	</html:submit>
</html:form>
<script type="text/javascript">
	var action = $('action');
	var formulario = $('formulario');

	function volver() {
		action.value = 'volver';
		formulario.submit();
	}
</script>