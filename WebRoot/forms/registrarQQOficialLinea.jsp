<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/links.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js" ></script>

<script language="javascript" type="text/javascript">
	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
  	}
    function actualizar( inputIn, defecto )
	{
		var urlDinamica = "";
		
		urlDinamica += "&" + defecto;

		return urlDinamica;
	}
</script>

<div class="title">
		Registrar Ingreso de Producto a la Bodega Oficial de L�nea
</div>
    <br/>
<div style="width: 70%" align="left">
  <html:errors />
</div>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion"/>
    <html:hidden property="process.rowNum" value="${ProcessForm.process.rowNum}" styleId="rowNumId"/>
	<html:hidden property="process.persona.id" styleId="idPersona_"/>
	<html:hidden property="process.credito.expediente" styleId="expedienteCredito"/>
	<html:hidden property="process.credito.id" styleId="creditoId"/>
	<html:hidden property="process.qqIngresoStr" styleId="qqIngresoStr"/>
	<html:hidden property="process.qqIngresoId" styleId="qqIngresoId" value="${ProcessForm.process.qqIngresoId}"/>
    <logic:notEmpty name="ProcessForm" property="process.creditos">
    	<display:table name="ProcessForm" property="process.creditos" id="credito">
    		<display:column property="persona.nomb12" title="Solicitante" />
			<display:column property="expediente" title="Expediente"/>
			<display:column property="destinoNombre" title="Destino" />
			<display:column property="productoDeReferencia" title="Producto" />
			<display:column property="sumQQIngresados" title="Total QQ Ingresados"/> 
			<display:column title="Total Ingresado a la Fecha">
				<asf:text maxlength="100" type="text" property="process.qqIngresoStr" name="ProcessForm" id="qqIngresoStr-${credito_rowNum}"></asf:text>
			</display:column>           			
            <display:column media="html" title="Ingresar">
				<a href="javascript:void(0);" onclick="registrarIngreso(${credito.id}, ${credito_rowNum});"> 
				Ingresar
				</a> 
			</display:column>   	
    	</display:table>
    </logic:notEmpty>   
    <br/>
    <table>
    	<tr>
    		<th style="width: 120px" >Persona:</th>
    		<td>
            	<asf:autocomplete name="" tipo="AJAX" idDiv="autocomplete_persona"
                                  idInput="idpersona" classDiv="autocomplete"
                                  origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
                                  options="{callback: actualizarLista, frequency: '0.8', minChars: '4',indicator:'indicadorPersona'}"
								  value="${ProcessForm.process.persona.id == 0 ? null : ProcessForm.process.persona.idpersona} ${ProcessForm.process.persona.id == 0 ? null : '-'} ${ProcessForm.process.persona.id == 0 ? null : ProcessForm.process.persona.nomb12Str2} ${ProcessForm.process.persona.id == 0 ? null : '-'} ${ProcessForm.process.persona.id == 0 ? null : ProcessForm.process.persona.nudo12Str}"
                                  attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'"/>
                                  <span id="indicadorPersona" style="display: none"><img src="images/loader.gif" alt="Trabajando..." /></span>
                                  <input value="..." onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=paramValue&amp;entityName=Persona&amp;title=Selecci�n de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');" type="button">&nbsp;
                                  (C�digo - Apellido y Nombre - N�mero de Documento)
            </td>
        </tr>
        </table>
        <input type="button" value="Listar por Persona" onclick="listarPorPersona();"/>
        <br/>
        <br/>
        <table>
        <tr>
			<th style="width: 120px">Expediente:</th>
   			<td>
   				<asf:autocomplete name="expediente" tipo="AJAX" idDiv="autocomplete_choices" 
   				idInput="expediente" classDiv="autocomplete"
   				origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarExpediente" 
   				options="{callback: actualizar, frequency: '0.8', minChars: '2'}"
   				attribs="size='50%'" 
   				value="${ProcessForm.process.expediente}"/>
			</td>
		</tr>
    	</table>
    <input type="button" value="Listar por Expediente" onclick="listarPorExpediente();"/>
    <br/>
    <br/>
    <input type="button" value="Listar Todos" id="listarId" onclick="listar();"/>
    <br/>
    <br/>
    <logic:equal value="true" name="ProcessForm" property="process.ingresar">
    <div>
		Por favor cargue el valor en QQ del NUEVO producto ingresado a la fecha. El valor cargado se sumar� al total.
	</div>
	<table>
		<tr>
			<th>Solicitante:</th>
			<td>${ProcessForm.process.credito.persona.nomb12}</td>
		</tr>
		<tr>
			<th>Expediente:</th>
			<td>${ProcessForm.process.credito.expediente}</td>
		</tr>
    	<logic:notEmpty name="ProcessForm" property="process.vinedo.garantia.valores">
			<logic:iterate id="valorGarantia" name="ProcessForm" property="process.vinedo.garantia.valores">
			<tr>
				<th>	
					<bean:write name="valorGarantia" property="campo.nombre"/>
				</th>
				<td>
					<bean:write name="valorGarantia" property="valorCadena"/>	
				</td>
			</tr>
			
			</logic:iterate>			
		</logic:notEmpty>
    	<tr>
    		<th>Total QQ Ingresados</th>
    		<td>${ProcessForm.process.vinedo.totalQQIngresados}</td>
    	</tr>
		<tr>
    		<th>Total Ingresado a la Fecha</th>
    		<td><html:text name="ProcessForm" property="process.qqIngresoStr" readonly="${ProcessForm.process.ingresarQQ}" maxlength="20" value="${ProcessForm.process.qqIngresoStr}" /></td>
    	</tr>
	</table>
	<br/>
	<html:errors/>
	<br/>
	<input type="button" value="Registrar Ingreso" id="aceptarId" onclick="registrarIngreso();"/>
    </logic:equal>
</html:form>


<script language="javascript" type="text/javascript">
if($('qqIngresoId') != null){
	if($($('qqIngresoId').value) != null){
		$($('qqIngresoId').value).focus();
	}
}
var ctrlPersona = $('idpersona');
var idPersona = $('idPersona_');
if(idPersona.value == null || ctrlPersona.value == "    " || idPersona.value == 0)
{
	ctrlPersona.value = "";
}
var ctrlExpediente = $('expediente');

    function listar()
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=listar';    
    }
    function listarVinedo()
    {
    	var cod = $('codigo').value
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=listarVinedo&process.civ='+cod;    
    }
    function ingresar(rowNum) 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=ingresar' +
        '&process.rowNum='+rowNum;      
    }
    function registrarIngreso(id,rowNum) 
    {
    	var confirmacion = confirm("�Est� seguro de querer registrar un ingreso de producto por la cantidad ingresada, para el Cr�dito seleccionado?");
    	if(confirmacion){
        	$('qqIngresoId').value = 'qqIngresoStr-'+rowNum;
        	$('qqIngresoStr').value = $('qqIngresoStr-'+rowNum).value;
	        $('accion').value = 'registrarIngreso';
	        $('creditoId').value = id;
	        $('oForm').submit();
        }    
    }
    function listarPorPersona(){
		idPersona.value = trim(ctrlPersona.value.split("-")[0]);
        var form = $('oForm');
        var accion = $('accion');
        accion.value = 'listarPorPersona';
    	ctrlExpediente.value = "";
        form.submit();
    }
    function listarPorExpediente(){
    	var expediente = $('expedienteCredito');
    	expediente.value = ctrlExpediente.value;
        var form = $('oForm');
        var accion = $('accion');
        accion.value = 'listarPorExpediente';
    	ctrlPersona.value = "";
        form.submit();
    }
</script>