<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>

<div class="title"><bean:write name="title"/></div>
<br><br>
<div align="left" style="width:70%">
<ul><li><bean:write name="mensaje"/></li></ul>
</div>
<center>
<button onclick="window.location='${pageContext.request.contextPath}/contenido_principal.jsp';">Aceptar</button>
</center>
