<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>

<div class="title">Test Process</div>
<br><br>
<html:errors />
<html:form action="/actions/process.do?processName=${param.processName}&filter">
<table border="0">
<input type="hidden" name="filter" value="true" />
<tr><th>Nombre:</th><td><html:text property="process.algo"/></td></tr>
</table>

<html:submit><bean:message key="abm.button.save"/></html:submit><html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>