<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Entidades Financieras</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
<table border="0">
	<html:hidden property="entity.id"/>
	<tr>
		<th>C�digo de Entidad Financiera:</th>
		<td>
			<html:text property="entity.codigoEntidad" styleId="codigo"></html:text>
		</td>	
	</tr>
	<tr>
		<th>Nombre de Entidad Financiera:</th>
		<td>
			<html:text property="entity.nombreEntidad"></html:text>
		</td>	
	</tr>
	<tr>
		<th>Descripci�n de Entidad:</th>
		<td><html:textarea property="entity.descripcionEntidad" styleId="descripcion" cols="50" rows="4" onkeypress="caracteres(this,255,event)"/></td>
	</tr>
</table>
<br />
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<script language='javascript' >
var codigo = $('codigo');

codigo.focus();
</script>