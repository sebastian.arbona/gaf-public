<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>


<div class="title">
	Redistribuir Detalle de Recaudaci�n
</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	
	<html:errors/>
	
	<html:hidden property="process.accion" styleId="accion" />
	<html:hidden property="process.detalleRecaudacion.id" styleId="detalleRecaudacionId" />
	<html:hidden property="process.boletosXML" styleId="boletosXML"/>
	<html:hidden property="process.idcaratula" />
	<table>
		<tr>
			<th>
				Proyecto:
			</th>
			<td>
				<asf:text name="ProcessForm" maxlength="10"
					property="process.objetoi.numeroAtencion" type="text"
					readonly="true"></asf:text>
			</td>
			<th>
				Titular:
			</th>
			<td colspan="3">
				<asf:text name="ProcessForm" maxlength="180" attribs="style=\"width: 420px;\""
					property="process.objetoi.persona.nomb12" type="text"
					readonly="true" cols="2"></asf:text>
			</td>
		</tr>
		<tr>
			<th>Deuda Vencida</th>
			<td><asf:text name="ProcessForm" maxlength="20" property="process.deudaVencida" type="decimal" readonly="true" attribs="class=\"right\""/></td>
			<th>Diferencia</th>
			<td colspan="3">
				<asf:text name="ProcessForm" maxlength="20" property="process.saldoVencida" type="decimal" readonly="true" attribs="class=\"right\""/>
			</td>
		</tr>
		<tr>
			<th>Deuda Total</th>
			<td><asf:text name="ProcessForm" maxlength="20" property="process.deudaTotal" type="decimal" readonly="true" attribs="class=\"right\""/></td>
			<th>Diferencia</th>
			<td colspan="3">
				<asf:text name="ProcessForm" maxlength="20" property="process.saldoTotal" type="decimal" readonly="true" attribs="class=\"right\""/>
			</td>
		</tr>
		<tr>
			<th>
				Fecha Pago:
			</th>
			<td>
				<asf:text name="ProcessForm" maxlength="10"
					property="process.detalleRecaudacion.fechaRecaudacionStr"
					type="text" readonly="true"></asf:text>
			</td>
			<th>
				Importe Actual:
			</th>
			<td>
				<asf:text name="ProcessForm" maxlength="100" id="importeActual"
					property="process.detalleRecaudacion.importe" type="decimal"
					readonly="true" attribs="class=\"right\""></asf:text>
			</td>
			<th>
				Importe Final:
			</th>
			<td>
				<asf:text name="ProcessForm" maxlength="100" id="importeFinal"
					property="process.importeFinal" type="decimal" readonly="true" attribs="class=\"right\""></asf:text>
			</td>
		</tr>
	</table>
	<table style="margin-top: 40px;">
		<tr>
			<th width="150px">
					Proyecto Destino:
			</th>
			<td width="348px">
				<asf:selectpop name="process.proyectoDestino.numeroAtencion"
					columns="Nro.Proyecto,Titular" captions="numeroAtencion,persona.nomb12"
					values="numeroAtencion" entityName="com.nirven.creditos.hibernate.Objetoi"
					value="${ProcessForm.process.proyectoDestino.numeroAtencion}"
					title="Consulta de Cr�ditos" onChange="cargarProyectoDestino();" caseSensitive="true" cantidadDescriptors="1" size2="0"/>
			</td>
			<th>Expediente:</th>
			<td><asf:text name="ProcessForm" maxlength="100" property="process.proyectoDestino.expediente" type="text" readonly="true"></asf:text></td>
		</tr>
		<tr>
			<th>Deuda Actual:</th>
			<td><asf:text name="ProcessForm" maxlength="100" property="process.deudaActual" type="decimal" readonly="true" attribs="class=\"right\""/></td>
			<th>Importe Asignado:</th>
			<td><asf:text name="ProcessForm" maxlength="100" property="process.importeAsignado" type="decimal" attribs="onchange='actualizarImporte();' class=\"right\" " id="importeAsignado"></asf:text></td>
		</tr>
	</table>
	<br />
	<input type="button" onclick="guardar();" value="Guardar"/>
	<input type="button" onclick="cancelar();" value="Cancelar"/>
</html:form>

<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');

	function guardar() {
		accion.value = 'guardar';
		formulario.submit();
	}
	function cancelar() {
		formulario.action = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=PreaplicacionPagos";
		accion.value = 'generarBoletos';
		formulario.submit();
	}
	function cargarProyectoDestino(){
		accion.value = 'cargarProyectoDestino';
		formulario.submit();
	}

	function actualizarImporte() {
		var importeActual = new Number($('importeActual').value);
		var importeAsignado = new Number($('importeAsignado').value);
		$('importeFinal').value = (importeActual - importeAsignado);
	}
</script>