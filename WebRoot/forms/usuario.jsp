<%@ page language="java"%>
<%@page import="com.asf.util.TipificadorHelper"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">
	Usuario
</div>
<br>
<br>
<html:form styleId="form" 	action="/actions/abmAction.do?do=save&filter=false&entityName=${param.entityName}">
	
	<html:hidden property="filter" value="true" />

	<table border="0">
		<tr>
			<th>
				Usuario:
			</th>
			<td>
				<logic:equal name="clave" value="false">
					<asf:select nullValue="false" name="entity.id"
						entityName="com.asf.hibernate.mapping.Claves"
						listCaption="id,canombK" listValues="id"
						value="${AbmForm.entity.id}" orderBy="id"
						filter="" />
				</logic:equal>
				<logic:equal name="clave" value="true">
   					${AbmForm.entity.clave.canombK } (${AbmForm.entity.id })
   					<html:hidden property="entity.id" />
				</logic:equal>

			</td>
		</tr>

		<tr>
			<th>
				Unidad:
			</th>
			<td>
				<asf:select2 name="AbmForm" property="entity.unidad_id"
					descriptorLabels="Nombre" entityProperty="id"
					descriptorProperties="nombre" entityName="Unidad"
					accessProperty="codigo" />
			</td>
		</tr>
		<tr>
			<th>
				Usuario Activo:
			</th>
			<td>
				<asf:select name="entity.activo" value="${AbmForm.entity.activo}"
					listCaption="Si,No" listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Persona:
			</th>
			<td>
				
				<asf:selectpop name="entity.persona_id" title="Persona"
				columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
				captions="id,nomb12,nudo12,cuil12" values="id"
				entityName="com.civitas.hibernate.persona.Persona"
				value="${AbmForm.entity.persona_id}" />
				
			</td>
		</tr>
	</table>

	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
	
</html:form>
<script language="JavaScript" type="text/javascript">

$(document).ready(
		function() {

			$("#form").submit(function() {
				$("#funciones").selectOptions(/./);
			});

			$("#agregar_Button").click(
					function() {
						$("#funciones").addOption($("#funcion").val(),
								$("#funcion option:selected").text(), false);
						return false;
					});
			$("#quitar_Button").click(function() {
				$("#funciones").removeOption(/./, true);
				return false;
			});
		});
</script>