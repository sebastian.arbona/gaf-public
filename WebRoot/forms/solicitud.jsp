<%@page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.civitas.importacion.Fecha"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%
pageContext.setAttribute("fechaActual", DateHelper.getString(new Date()));
pageContext.setAttribute("hora", new GregorianCalendar().get(GregorianCalendar.HOUR));
pageContext.setAttribute("min", new GregorianCalendar().get(GregorianCalendar.MINUTE));
%>
<body class=" yui-skin-sam">
	<link type="text/css"
		href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
		rel="stylesheet" />
	<link type="text/css"
		href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
		rel="stylesheet" />
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
		
	</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
		
	</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
		
	</script>
	<div class="title">Solicitud De Financiamiento</div>
	<br>
	<br>
	<html:form
		action="/actions/process.do?do=process&processName=${param.processName}"
		styleId="oForm">
		<html:hidden name="ProcessForm" property="process.accion"
			styleId="accion" />
		<html:hidden name="ProcessForm" property="process.nroAte"
			styleId="nroAtencion" />
		<html:hidden property="process.credito.id" />
		<div style="width: 70%" align="left">
			<html:errors />
		</div>
		<table class="tabla" id="acarreo1">
			<tr>
				<th>Nro. de Atenci&oacute;n:</th>
				<td><asf:text property="ProcessForm.process.nroAte" type="Long"
						maxlength="6" readonly="${!ProcessForm.process.editable}"
						id="numeroAtencion2" /></td>
				<th>Nro. de Solicitud:</th>
				<td><asf:text
						property="ProcessForm.process.credito.numeroAtencion" type="Long"
						maxlength="6" readonly="true" id="objetoiNumeroAtencion" /></td>
			</tr>
			<tr>
				<th>Persona:</th>
				<td colspan="3"><asf:selectpop onChange="estadoPersona();"
						name="process.credito.persona_id" title="Persona"
						columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
						captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
						entityName="com.civitas.hibernate.persona.Persona"
						value="${ProcessForm.process.credito.persona_id}"
						enabled="${ProcessForm.process.editable}"
						filter="fechaBaja is null" /> <a href="javascript:void(0);"
					onclick="popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaAction.do?do=newEntity&entityName=Persona&forward=PersonaAltaRapida');"><b>Registrar
							Persona</b> </a></td>
			</tr>
			<tr>
				<th>Fecha:</th>
				<td><asf:calendar
						property="ProcessForm.process.credito.fechaSolicitudStr"
						attribs="class='fechaSolicitudStr'"
						readOnly="${!ProcessForm.process.editable}" /></td>
				<th>Sector Econ&oacute;mico</th>
				<td><asf:select name="process.credito.tipoSector"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.credito.tipoSector}"
						filter="categoria='credito.sector' order by codigo"
						nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;"
						enabled="${ProcessForm.process.editable}" /></td>
			</tr>
			<tr>
				<th>L&iacute;nea Consultada:</th>
				<td colspan="3"><asf:select
						entityName="com.nirven.creditos.hibernate.Linea"
						attribs="class='Linea' onChange='cambiarMoneda();'"
						listCaption="getId,getNombre" listValues="getId"
						name="process.credito.linea_id"
						value="${ProcessForm.process.credito.linea_id}"
						enabled="${ProcessForm.process.editable}" filter="activa=true " />
					<asf:text id="moneda" maxlength="80" type="text"
						property="process.texto" attribs="disabled=disabled"></asf:text></td>
			</tr>
			<tr>
				<th>Categor&iacute;a del Solicitante:</th>
				<td colspan="3"><asf:select
						name="process.credito.categoriaSolicitante"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.credito.categoriaSolicitante}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'Categoria.Solicitante' order by codigo"
						nullValue="true" nullText="&lt;&lt;Ninguna&gt;&gt;" /></td>
			</tr>
			<tr>
				<th>Delegaci&oacute;n:</th>
				<logic:empty name="ProcessForm" property="process.credito.unidad_id">
					<td colspan="3"><asf:select
							entityName="com.nirven.expedientes.persistencia.Unidad"
							listCaption="getId,getNombre" listValues="getId" enabled="false"
							name="process.credito.unidad_id"
							value="${ProcessForm.process.currentUser.unidad.id}" /></td>
				</logic:empty>
				<logic:notEmpty name="ProcessForm"
					property="process.credito.unidad_id">
					<td colspan="3"><asf:select
							entityName="com.nirven.expedientes.persistencia.Unidad"
							listCaption="getId,getNombre" listValues="getId" enabled="false"
							name="process.credito.unidad_id"
							value="${ProcessForm.process.credito.unidad_id}" /></td>
				</logic:notEmpty>
			</tr>
			<tr>
				<th>Objeto:</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.credito.objeto" cols="80" rows="4"
						readonly="${!ProcessForm.process.editable}" /></td>
			</tr>
			<tr>
				<th>Observaciones:</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.credito.observaciones" cols="80" rows="4"
						readonly="${!ProcessForm.process.editable}" /></td>
			</tr>
			<tr>
				<th>Financiamiento Max. Solicitado:</th>
				<td><asf:text
						property="ProcessForm.process.credito.financiamientoStr"
						type="decimal" maxlength="12"
						value="${ProcessForm.process.credito.financiamientoStr}"
						attribs="onChange='calcularPorcentajes();'"
						readonly="${!ProcessForm.process.editable}" /></td>
				<th>Porcentaje Solicitado:</th>
				<td><asf:text
						property="ProcessForm.process.credito.porcentajeSolicitadoStr"
						type="text" maxlength="6" readonly="true"
						id="porcentajeSolicitado" /></td>
			</tr>
		</table>
		<table class="tabla" id="acarreo2">
			<tr>
				<th>QQ Solicitado:</th>
				<td><asf:text
						property="ProcessForm.process.credito.qqsolicitado" type="decimal"
						maxlength="12" readonly="${!ProcessForm.process.editable}" /></td>
				<th>�Es MiPyME?</th>
				<td><asf:select name="process.credito.esMiPymeStr"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.credito.esMiPymeStr}"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria = 'credito.mipyme' order by codigo" /></td>
			</tr>
			<tr>
				<th>Destino:</th>
				<td colspan="3"><asf:select name="process.credito.destino"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.credito.destino}"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria = 'credito.destino' order by codigo" /></td>
			</tr>
		</table>
		<table class="tabla" id="fecovita">
			<tr>
				<th>�Pertenece a Fecovita?</th>
				<td colspan="3"><asf:select name="process.credito.fecovitaTipo"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.credito.fecovitaTipo}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'Linea.Fecovita' order by codigo"
						nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;" /></td>
			</tr>
		</table>
		<table class="tabla" id="general">
			<tr id="aportePropio">
				<th>Aporte Propio:</th>
				<td><asf:text
						property="ProcessForm.process.credito.aportePropio" type="decimal"
						maxlength="12"
						value="${ProcessForm.process.credito.aportePropioStr}"
						readonly="${!ProcessForm.process.editable}"
						attribs="onChange='calcularPorcentajes();'" /></td>
				<th>Porcentaje Aporte:</th>
				<td><asf:text
						property="ProcessForm.process.credito.porcentajeAporteStr"
						type="text" maxlength="6" readonly="true" id="porcentajeAporte" />
				</td>
			</tr>
			<tr id="montoTotal">
				<th>Monto Total:</th>
				<td><asf:text property="ProcessForm.process.credito.montoTotal"
						type="decimal" maxlength="12"
						value="${ProcessForm.process.credito.montoTotalStr}"
						readonly="true" /></td>
				<th>Porcentaje Total:</th>
				<td><asf:text
						property="ProcessForm.process.credito.porcentajeTotal" type="text"
						maxlength="6" readonly="true" /></td>
			</tr>
			<tr id="amortizacion">
				<th>Plazo de Amortizaci&oacute;n Capital:</th>
				<td><asf:text name="ProcessForm"
						property="process.credito.plazoCapital" type="Long" maxlength="6"
						readonly="${!ProcessForm.process.editable}" /></td>
				<th>Periodicidad Cuotas Capital</th>
				<td><asf:select name="process.credito.frecuenciaCapital"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="ProcessForm.process.credito.frecuenciaCapital"
						filter="categoria='amortizacion.periodicidad' order by codigo"
						enabled="${ProcessForm.process.editable}" /></td>
			</tr>
			<tr id="plazo">
				<th>Plazo de Amortizaci&oacute;n Compensatorio:</th>
				<td><asf:text name="ProcessForm"
						property="process.credito.plazoCompensatorio" type="Long"
						maxlength="6" readonly="${!ProcessForm.process.editable}" /></td>
				<th>Periodicidad Cuotas Inter&eacute;s</th>
				<td><asf:select name="process.credito.frecuenciaInteres"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="ProcessForm.process.credito.frecuenciaInteres"
						filter="categoria='amortizacion.periodicidad' order by codigo"
						enabled="${ProcessForm.process.editable}" /></td>
			</tr>
			<tr id="expediente">
				<th>Expediente:</th>
				<td colspan="3"><html:text name="ProcessForm"
						property="process.credito.expediente" maxlength="20"
						readonly="${!ProcessForm.process.editable}" /></td>
			</tr>
			<tr>
				<th>Vencimiento de Solicitud:</th>
				<td colspan="3"><asf:calendar
						property="ProcessForm.process.credito.vencimientoStr"
						value="${ProcessForm.process.credito.vencimientoStr}"
						readOnly="${!ProcessForm.process.editable}" /></td>
			</tr>
			<tr>
				<th>Proceso de Aprobaci&oacute;n:</th>
				<td colspan="3">
					<div id="iniciarAprob" style="display: none">
						<logic:empty name="ProcessForm"
							property="process.credito.expediente">
							<input type="button" value="Iniciar" id="iniciar" />
						</logic:empty>
					</div> <logic:present name="ProcessForm"
						property="process.credito.procesoAprobacion">
						<bean:define id="objetoi" name="ProcessForm"
							property="process.credito"
							type="com.nirven.creditos.hibernate.Objetoi" />
	        	Iniciado el <%=new SimpleDateFormat("dd/MM/yyyy H:m:s").format(objetoi.getProcesoAprobacion().getStart())%>
					</logic:present>

				</td>
			</tr>
			<logic:equal name="ProcessForm" property="process.accion"
				value="show">
				<tr>
					<th><logic:empty name="ProcessForm"
							property="process.credito.expediente">
      		Expediente sin proceso
      		</logic:empty> <logic:notEmpty name="ProcessForm"
							property="process.credito.expediente">
      		Expediente
      		</logic:notEmpty></th>
					<td colspan="3"><logic:empty name="ProcessForm"
							property="process.credito.expediente">
							<html:text property="process.credito.expediente"
								name="ProcessForm" styleId="expedienteNro" />
							<input type="button" value="Guardar expediente"
								id="guardarExpediente" />
						</logic:empty> <logic:notEmpty name="ProcessForm"
							property="process.credito.expediente">
      			${ProcessForm.process.credito.expediente}
      		</logic:notEmpty></td>
				</tr>
			</logic:equal>
		</table>
		<br>
		<br>
		<logic:notEqual name="ProcessForm" property="process.accion"
			value="show">
			<%--     <div id="pestanias"></div>--%>
			<input type="button" value="Guardar" id="guardar" />
			<input type="button" value="Cancelar" id="cancelar" />
			<div id="tab_cronograma"></div>
			<div id="tab_vinedos"></div>

		</logic:notEqual>
		<logic:equal name="ProcessForm" property="process.accion" value="show">
			<div id="pestanias" class="yui-navset"
				style="width: 100%; display: none;">
				<ul class="yui-nav">
					<li class="selected" id="tab_requisitos"><a href="#requisitos"><em>Requisitos</em>
					</a></li>
					<li id="tab_vinedos"><a href="#vinedos"><em>Vi�edos</em> </a>
					</li>
					<li id="tab_garantias"><a href="#garantias""><em>Garant&iacute;as</em>
					</a></li>
					<li id="tab_cotomadores"><a href="#cotomadores""><em>Co-Tomadores</em>
					</a></li>
				</ul>
				<div class="yui-content">
					<div id="requisitos">
						<iframe
							src='${pageContext.request.contextPath}/actions/RequisitosAction.do?do=list&entityName=CargaRequisito&idSolicitud=${ProcessForm.process.idSolicitud}&paramValue[0]=${ProcessForm.process.idSolicitud}&paramName[0]=credito.id&paramComp[0]=='
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>
					<div id="vinedos">
						<iframe
							src='${pageContext.request.contextPath}/actions/process.do?processName=VinedoProcess&do=process&process.idSolicitud=${ProcessForm.process.idSolicitud}'
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>
					<div id="garantias">
						<iframe
							src='${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.idSolicitud=${ProcessForm.process.idSolicitud}'
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>
					<div id="cotomadores">
						<iframe
							src='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoCotomadores&process.action=listar&process.idObjetoi=${ProcessForm.process.idSolicitud}'
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>

				</div>
			</div>
		</logic:equal>
	</html:form>
	<script language="JavaScript" type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
		
	</script>
	<script language="JavaScript" type="text/javascript">
		cambiarMoneda();
		function cambiarMoneda() {
			var linea = document.getElementById("process.credito.linea_id");
			if (linea == null || linea.value == 0) {
				return;
			} else {
				url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxLineaMonedaCotizacion&idLinea="
						+ linea.value;
				retrieveURL(url, cambiarMonedaResponse);
			}
		}
		function cambiarMonedaResponse(responseText) {
			$('moneda').value = responseText;
		}
		jQuery.noConflict();
		jQuery(document)
				.ready(
						function($) {
							var formulario = jQuery('#oForm');
							var accion = jQuery('#accion');
							var numeroAt = jQuery('#numeroAtencion2');
							jQuery('#guardar').click(function() {
								accion.val('guardar');
								formulario.submit();
							});
							jQuery('#iniciar')
									.click(
											function() {
												if (!confirm('Est� seguro de iniciar el proceso de solicitud?'))
													return false;
												window.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=startProcess&process.idSolicitud=${ProcessForm.process.credito.id}';
											});
							jQuery('#guardarExpediente')
									.click(
											function() {
												if (!confirm('Est� seguro de guardar el expediente '
														+ jQuery(
																"#expedienteNro")
																.val() + '?'))
													return false;
												window.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoExpediente&process.idCredito=${ProcessForm.process.credito.id}&process.expediente='
														+ jQuery(
																"#expedienteNro")
																.val();
											});
							jQuery('#cancelar').click(function() {
								accion.val('cancelar');
								formulario.submit();
							});
							jQuery('#numeroAtencion2').change(function() {
								ajax_traer_turno();
							});
							jQuery('#process\\.credito\\.linea_id')
									.change(
											function() {
												jQuery
														.ajax({
															type : "POST",
															url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTipificadorDeLineaSolicitud&sincotizacion=true&idLinea="
																	+ jQuery(
																			'#process\\.credito\\.linea_id')
																			.val(),
															dataType : "text",
															success : function(
																	responseText) {
																seleccionLinea(responseText);
															}
														});
											}).change();

							function ajax_traer_turno() {

								var goout = 0;
								jQuery
										.ajax({
											type : "POST",
											url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&entityName=Objetoi&listValues=nombre;&filter=turno.numeroAtencion=\'"
													+ $('#numeroAtencion2')
															.val()
													+ "\' order by turno.atencion desc ",
											dataType : "text",
											success : function(responseText) {
												if (responseText != "<Objects></Objects>") {
													alert('El turno ingresado, se encuentra asociado a una solicitud. Por favor intente nuevamente.');
													jQuery('#numeroAtencion2')
															.val("");
													goout = 1;
													return false;
												}
											}
										});
								if (goout == 1) {
									return;
								}
								jQuery
										.ajax({
											type : "POST",
											url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&entityName=Turno&listValues=nombre;linea.id;objeto;mail;observaciones;monto;&filter=numeroAtencion=\'"
													+ $('#numeroAtencion2')
															.val()
													+ "\' order by atencion desc ",
											dataType : "xml",
											success : function(responseText) {
												var objeto;
												jQuery(responseText).find(
														'Turno').each(
														function() {
															objeto = $(this);
															return;
														});
												if (objeto == null) {
													alert('El Turno no se encontr�. Por favor, intente nuevamente.');
													jQuery('#numeroAtencion2')
															.val("");
													return false;
												}
												jQuery('#nroAtencion').val(
														$('#numeroAtencion2')
																.val());
												jQuery(
														".Linea option[value='"
																+ objeto
																		.find(
																				"linea_id")
																		.text()
																+ "']").attr(
														'selected', 'selected');
												jQuery('#objeto').html(
														objeto.find("objeto")
																.text());
												jQuery('.montoTotal').val(
														objeto.find("monto")
																.text());
												jQuery(
														'#process\\.credito\\.linea_id')
														.change(
																function() {
																	jQuery
																			.ajax({
																				type : "POST",
																				url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTipificadorDeLineaSolicitud&sincotizacion=true&idLinea="
																						+ jQuery(
																								'#process\\.credito\\.linea_id')
																								.val(),
																				dataType : "text",
																				success : function(
																						responseText) {
																					seleccionLinea(responseText);
																				}
																			});
																}).change();
											}
										});
							} //ajax_traer_turno
						});

		function estadoPersona() {
			var a = '${ProcessForm.process.credito.persona_id}';
			//alert(a);
		}
	</script>
	<script language='javascript'>
		//pesta�as.-
		if ($('pestanias') != null) {
			var tabView = new YAHOO.widget.TabView('pestanias');
			var pestanias = $('pestanias');
			if ('${ProcessForm.process.idSolicitud}' != "") {
				pestanias.style.display = "";
			}
		}
		var frameRequisitos = $('frameRequisitos');
		function irARequisitos() {
			var src = '';
			if (frameRequisitos.src == "") {
				src += '${pageContext.request.contextPath}/actions/RequisitosAction.do';
				src += '?do=list&entityName=CargaRequisito';
				src += '&idSolicitud=${ProcessForm.process.idSolicitud}';
				frameRequisitos.src = src;
			}
		}
	</script>
	<script type="text/javascript">
		var montoTotal = $('ProcessForm.process.credito.montoTotal');
		var financiamiento = $('ProcessForm.process.credito.financiamientoStr');
		var aportePropio = $('ProcessForm.process.credito.aportePropio');

		var porcentajeAporte = $('porcentajeAporte');
		var porcentajeTotal = $('ProcessForm.process.credito.porcentajeTotal');
		var porcentajeSolicitado = $('porcentajeSolicitado');

		var id = $('process.credito.linea_id');
		var objeto = $('process.credito.objeto');

		var vencimiento = $('ProcessForm.process.credito.vencimientoStr');
		var fecovita = $('process.credito.fecovitaTipo');

		function seleccionMoneda(valor) {
			var mone = $('moneda');
			mone.value = valor;
		}

		function seleccionLinea(valor) {
			var json = JSON.parse(valor);
			var estadoSolicitud = "${ProcessForm.process.estadoSolicitud}";
			if (json[1] == null || json[1] == '') {
				ocultar('fecovita');
			} else {
				fecovita.value = json[1];
				mostrar('fecovita');
			}

			switch (json[0]) {
			case 'general':
				if (estadoSolicitud == "ESPERANDO DOCUMENTACION") {
					mostrar('general');
					ocultar('acarreo2');
					mostrar('aportePropio', 'montoTotal');
					ocultar('amortizacion');
					ocultar('plazo');
					ocultar('expediente');
					ocultar('tab_vinedos');
				} else {
					ocultar('acarreo2');
					mostrar('aportePropio', 'montoTotal');
					mostrar('general');
					ocultar('amortizacion');
					ocultar('plazo');
					ocultar('expediente');
					ocultar('tab_vinedos');
				}
				break;
			case 'cosechaVid':
				if (estadoSolicitud == "ESPERANDO DOCUMENTACION") {
					mostrar('general');
					mostrar('acarreo2', 'tab_vinedos');
					ocultar('aportePropio', 'montoTotal');
					ocultar('amortizacion');
					ocultar('plazo');
					ocultar('expediente');
				} else {
					mostrar('acarreo2', 'tab_vinedos');
					ocultar('aportePropio', 'montoTotal');
					ocultar('amortizacion');
					mostrar('general');
					ocultar('plazo');
					ocultar('expediente');
				}
				break;

			}

		}
		function calcularPorcentajes() {

			montoTotal.value = parseFloat(aportePropio.value)
					+ parseFloat(financiamiento.value);
			if (montoTotal.value != 0.0) {
				porcentajeAporte.value = parseFloat(
						parseFloat(aportePropio.value)
								/ parseFloat(montoTotal.value) * 100)
						.toFixed(2)
						+ '%';
				porcentajeSolicitado.value = parseFloat(
						parseFloat(financiamiento.value)
								/ parseFloat(montoTotal.value) * 100)
						.toFixed(2)
						+ '%';
				porcentajeTotal.value = parseFloat(100).toFixed(2) + '%';
			} else {
				porcentajeAporte.value = parseFloat(0).toFixed(2) + '%';
				porcentajeSolicitado.value = parseFloat(0).toFixed(2) + '%';
			}
		}
	</script>
	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>