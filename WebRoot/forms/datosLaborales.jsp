<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.accion" value="guardar" styleId="accion"/>
    <html:hidden property="process.persona.id" styleId="idPersonaTitular" value="${ProcessForm.process.persona.id}"/>
    <html:hidden property="process.referencia.id" value="${ProcessForm.process.referencia.id}"/>
    <div style="width:70%" align="left"><html:errors/></div>
    <table>
        <tr>
            <th>Tipo de Relaci�n Laboral: </th>
            <td>
                <asf:select name="process.tipoRelacionLaboral.id" entityName="TipoRelacionLaboral" listCaption="id,denominacion"
                            listValues="id"
                            value="${ProcessForm.process.referencia.tipoRelacionLaboral.id}"/>
            </td>
        </tr>
        <tr>
            <th>Detalle: </th>
            <td>
                <html:textarea name="ProcessForm" property="process.referencia.detalle" cols="60" rows="4" onkeypress="caracteres(this,255,event);"></html:textarea>
            </td>
        </tr>
        <tr>
            <th>Desde: </th>
            <td><asf:calendar property="ProcessForm.process.referencia.fechaDesdeStr" /></td>
        </tr>
        <tr>
            <th>Hasta: </th>
            <td><asf:calendar property="ProcessForm.process.referencia.fechaHastaStr" /></td>
        </tr>
        <tr>
            <th>Empleador: </th>
            <td>
                <html:text property="process.referencia.empleador"/>
            </td>
        </tr>
        <tr>
            <th>CUIT Empleador: </th>
            <td>
               <html:text property="process.referencia.cuitEmpleador" maxlength="11" />
            </td>
        </tr>
        <tr>
            <th>Observaciones: </th>
            <td>
                <html:textarea name="ProcessForm" property="process.referencia.observaciones" cols="60" rows="4" onkeypress="caracteres(this,255,event);"></html:textarea>
            </td>
        </tr>
    </table>
    
    <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=guardar&process.referencia.id=${ProcessForm.process.referencia.id}">
    	<html:submit value="Guardar"/>
    </asf:security>
    <input type="button" value="Cancelar" onclick="cancelar();" >
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
        src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<script language='javascript'>
	var accion     = $('accion');
	var formulario = $('oForm');

	function cancelar()
    {
		accion.value = "cancelar";

		formulario.submit();
    }//fin cancelar.-

    Event.observe(window, 'load', function() 
    {
 	  	this.name = "Datos Laborales";
       	parent.push(this);
    });
</script>