<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 



<script type="text/javascript">
var persona = false;
function guardar(){
	var por= document.getElementById("porcentaje");
	por.value=document.getElementById("porcentajeTit").value;
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="guardar";
	var id=document.getElementById("id");
	formulario.submit();
}

function cancelar(){
	var id=document.getElementById("id");
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="cancelar";
}
function selecPersona(){
	persona= true;
}
function modificarTitular(){
	var por= document.getElementById("porcentaje");
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="modificar";
	por.value=document.getElementById("porcentajeNuevo").value;
	formulario.submit();
}
function habilitarNuevo(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	var id=document.getElementById("id");
	accion.value="nuevo";
	id.value=document.getElementById("id").value;
	formulario.submit();
}

function habilitarVolver(){
	window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=GarantiaProcess&process.accionStr=VOLVER_LISTAR&process.idSolicitud=${ProcessForm.process.idObjetoi}";	
    }
function sinLetras(e)
{
key=(document.all) ? e.keyCode : e.which;

if (key < 48 || key > 57 ){
if (key==8 || key==46 || key==44 ){
return true;
}
else if(e.preventDefault) e.preventDefault();
e.returnValue = false;
return false;
}
}
function verificarPorcentaje(porcentaje){
    var tasa = porcentaje.value.replace(',', '.').replace('%', '');
    var resto = document.getElementById("resto");
    var persona = document.getElementById("idpersona");
    if(parseFloat(tasa) < 0 || parseFloat(tasa) > parseFloat(resto.value.replace(',', '.').replace('%', ''))){
            alert("Debe ingresar un valor entre 0 y "+resto.value);
            porcentaje.value = '0%';
    }else if(persona== false){
    	alert("Debe elegir una Persona");
    }else{
            porcentaje.value = porcentaje.value.concat('%');
            guardar();
    }
}
function verificarPorcentajeMod(porcentaje){
    var tasa = porcentaje.value.replace(',', '.').replace('%', '');
    var resto = document.getElementById("resto");   
    if(parseFloat(tasa) < 0 || parseFloat(tasa) > parseFloat(resto.value.replace(',', '.').replace('%', ''))){
            alert("Debe ingresar un valor entre 0 y "+resto.value);
            porcentaje.value = '0%';
            
    }else{
            porcentaje.value = porcentaje.value.concat('%');
            modificarTitular();
    }
}
</script>

<div class="title">
	VER TITULARES
</div>
<br>
<br>

		<html:form action="actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
		<html:hidden property="process.accion" styleId="accion"  />
		<html:hidden property="process.restoStr" styleId="resto"/>
		<html:hidden property="process.porcentajeStr" styleId="porcentaje"/>
		<html:hidden property="process.id" styleId="id" value="${ProcessForm.process.id}"/>
		<html:hidden property="process.idGarantiaUso" value="${ProcessForm.process.idGarantiaUso}"/>
		<html:hidden property="process.idObjetoi" value="${ProcessForm.process.idObjetoi}"/>
		
		<span style="color: red; font-size:11pt; font-weight: bold">
			<html:errors />
		</span>
		
		<logic:equal value="false" property="process.nuevo" name="ProcessForm">
			<b>C�digo de Persona:</b>
		<asf:selectpop name="process.idPersona" title="Persona"
			columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
			captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
			entityName="com.civitas.hibernate.persona.Persona" value="" onChange="selecPersona();"/>
		
			<table>
				<tr>
				<th>
				El porcentaje no debe ser mayor a: ${ProcessForm.process.restoStr}
				</th>
				</tr>
				<tr>
					<th>
						Porcentaje de Titularidad
					</th>
					<td>
						<html:text name="ProcessForm" styleId="porcentajeTit" property="process.porcentajeStr" onkeypress="sinLetras(event);"/>		
					</td>
					</tr>
					</table>
					<html:button property="process.porcentaje"  styleId="guardarNuevo" value="Guardar" onclick="verificarPorcentaje(porcentajeTit);" />

					<html:submit styleId="cancelarNuevo" value="Cancelar" onclick="cancelar();" />

				
			
		</logic:equal>
		
		<logic:equal value="true" property="process.modifico" name="ProcessForm">
			<table border="0">
				<tr>
					<td>
						Nombre del titular
					</td>
					<td>
						<html:text property="process.gu.persona.nomb12" name="ProcessForm"
							maxlength="30" disabled="true" />
						<br />
					</td>
				</tr>
				<tr>
					<td>
						Porcentaje actual de titularidad
					</td>
					<td>
						<html:text property="process.gu.porcentaje"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
				</tr>
				<th>
				El porcentaje no debe ser mayor a: ${ProcessForm.process.restoStr}
				</th>
				<tr>
					<td>
						Porcentaje nuevo
					</td>
					<td>
						<html:text property="process.porcentajeStr" value="${ProcessForm.process.porcentajeStr}"
							name="ProcessForm" maxlength="30" styleId="porcentajeNuevo" onkeypress="sinLetras(event);"/>
			</table>
		
			<tr>	
			    <td>
				<html:button property="process.porcentaje" styleId="modificarTit" value="Modificar" onclick="verificarPorcentajeMod(porcentajeNuevo);" />
				</td>
				<td>
				<html:submit styleId="cancelarModif" value="Cancelar" onclick="cancelar();" />
				</td>
			</tr>
		
		</logic:equal>
		
		<display:table name="ProcessForm" property="process.gusos"
				defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.garantiaUso" export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">	
				<display:column media="excel xml pdf" property="id" title="ID"/>
				<display:column property="id" title="ID" sortProperty="id" paramId="process.idGarantiaUso" paramProperty="id" sortable="true"
			    	href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accion=habilitarMod&process.id=${ProcessForm.process.id}&process.idObjetoi=${ProcessForm.process.idObjetoi}"/>
			    					
				<display:column  property="persona.nomb12" title="Nombre" />
				<display:column property="porcentajeStr" title="Porcentaje de titularidad" />
				<display:column title="Eliminar" paramId="process.idGarantiaUso" paramProperty="id"
					href="${pageContext.request.contextPath}/actions/process.do?processName=VerTitulares&do=process&process.accion=eliminar&process.id=${ProcessForm.process.id}&process.idObjetoi=${ProcessForm.process.idObjetoi}">
					Eliminar
				</display:column>
		  		
		</display:table>
		
		
		<tr>
		<logic:equal name="ProcessForm" value="true" property="process.nuevo">
		     <input type="button" id="volverGarantias" value="Volver" onclick="habilitarVolver();" />
			<input type="button"  id="nuevoTit" value="Nuevo" onclick="habilitarNuevo();"/>
		</logic:equal>
		</tr>	
	</html:form>