<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Nuevo Estado</div>

<html:form
	action="/actions/process.do?do=process&processName=EstadosBonTasa"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" styleId="action" value="save"/>
	<html:errors/>
	
	<table>	
		<tr>	
			<th>Fecha Cambio</th>
			<td>
				<asf:calendar property="ProcessForm.process.estado.fechaCambioStr" 
 					value="${process.estado.fechaCambioStr}"/> 
			</td>
		</tr>	
		<tr>
			<th>Tipo Estado</th>
			<td>
				<asf:select name="process.estado.idEstado"
					entityName="com.civitas.hibernate.persona.Estado"
					value="${ProcessForm.process.estado.idEstado}"
					listCaption="getNombreEstado" listValues="getIdEstado"
					filter="tipo = 'BonTasa'  order by idEstado" />
			</td>		
		</tr>	
		<tr>
			<th>Tipo Resolución</th>
			<td>
				<html:select styleId="estados" name="ProcessForm" property="process.estado.tipoResolucionStr">
					<html:optionsCollection property="process.tipoResolucionList" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>		
		<tr>
			<th>Numero Resolución</th>
			<td>
				<html:text property="process.estado.numeroResolucion" name="ProcessForm"/>
			</td>
		</tr>
		<tr>	
			<th>Fecha Resolución</th>
			<td>
				<asf:calendar property="ProcessForm.process.estado.fechaResolucionStr" 
 					value="${process.estado.fechaResolucionStr}"/> 
			</td>
		</tr>
		<tr>
			<th>Ubicación del Expediente</th>
			<td>
				<html:text property="process.estado.ubicacionExpediente" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>Condiciones previas al pago</th>
			<td>
				<html:text property="process.estado.condiciones" name="ProcessForm"/>
			</td>
		</tr>
		
	</table>
	<div>
		<html:submit  value="Guardar" ></html:submit>
	</div>
	
</html:form>	
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>