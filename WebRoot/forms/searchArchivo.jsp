<h3 class="title">
	<span class="fa fa-fw fa-file"></span>&nbsp;<span>B&uacute;squeda de Archivos</span>
</h3>

<div id="table-container" class="contenedor-dtable grilla-datatable">
		
	<div class="contenedor-dtable grilla-datatable style-datatable">	
		<table id="dt-searchArchivo" class="table table-hover user-list" cellspacing="0">
        </table>
    </div>
</div>

<script type="text/javascript">
$j(document).ready(function(){
	format_dt_searchArchivo();
	dTable_searchArchivo = inicializarDatatable( "dt-searchArchivo", 6, null, dt_personalizacion_searchArchivo );
})
</script>
