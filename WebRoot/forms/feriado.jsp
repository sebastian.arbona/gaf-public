<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Feriado</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>Id:</th>
			<td><asf:text name="AbmForm" property="entity.id" type="long"
					maxlength="8" readonly="true" /></td>
		</tr>
		<tr>
			<th>A�o:</th>
			<td><html:text property="entity.anio" maxlength="4" /></td>
		</tr>
		<th>Fecha:</th>
		<td colspan="3"><asf:calendar property="AbmForm.entity.fechaStr"
				maxlength="10" /></td>
		<tr>
			<th>Descripci�n:</th>
			<td><html:text property="entity.descripcion" maxlength="100" />
			</td>
		</tr>

	</table>

	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
