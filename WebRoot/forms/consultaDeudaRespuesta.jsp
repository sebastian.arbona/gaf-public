<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">${ProcessForm.process.entidadToStr}</div>
<html:form action="/actions/process.do?do=process&processName=${param.processName}&process.consulta_id=${ProcessForm.process.consulta_id}&process.entidadStr=${ProcessForm.process.entidadStr}&process.consultaPersona_id=${ProcessForm.process.consultaPersona_id}">
	<html:hidden property="process.consultaRespuesta.id" />
	<html:hidden property="process.action" value="guardar" styleId="action" />
	
	<table border="0">
		
		
		<tr><th>Persona:</th><td>${result.persona.persona.nomb12}</td></tr>
		<tr><th>CUIT/CUIL:</th><td>${result.persona.persona.cuil12}</td></tr>
		<tr><th>Car�cter:</th><td>${result.persona.caracter}</td></tr>
		<tr><th>Fecha de Consulta:</th><td>${result.fechaConsultaStr}</td></tr>
		<tr><th>Monto:</th><td><asf:text type="decimal" maxlength="10" property="ProcessForm.process.consultaRespuesta.monto" /></td></tr>
		<tr><th>Fecha de Vencimiento:</th><td><asf:calendar property="ProcessForm.process.consultaRespuesta.fechaVencimientoStr" value="${ProcessForm.process.consultaRespuesta.fechaVencimientoStr}"/></td></tr>
		<tr><th>Observaciones:</th><td><html:textarea name="ProcessForm" property="process.consultaRespuesta.observaciones" /></td></tr>
	    
	</table>
	<input type="submit" value="Guardar" />
	<input type="submit" value="Cancelar" onclick="$('action').value='cancelar';" />
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
<script>
var bs=window.parent.document.getElementsByTagName("input");
for(i = 0; i < bs.length; i++){
	if(bs[i].type === 'submit')
		bs[i].disabled=true;
}
</script>


