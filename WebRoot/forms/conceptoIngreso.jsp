<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Parametrizaci&oacute;n de Conceptos de Ingresos</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

<html:hidden property="entity.id"/>
<table border="0">
<tr><th>Concepto:</th><td>
<asf:select name="entity.conceptoId" entityName="com.nirven.creditos.hibernate.CConcepto" listCaption="detalle"
                             listValues="concepto"
                            value="${empty AbmForm.entity.conceptoId ? '' : AbmForm.entity.conceptoId}"/>
</td></tr>
<tr><th>Tipo de concepto:</th><td>
<asf:select name="entity.tipoConcepto"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoConcepto}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'ctacte.detalle'  order by codigo" />
</td></tr>
<tr><th>C&oacute;digo de ingreso:</th><td><html:text property="entity.codigoIngreso" maxlength="40"/></td></tr>
<tr><th>Concepto de ingreso:</th><td><html:text property="entity.conceptoIngreso" maxlength="100" size="50"/></td></tr>
</table>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>