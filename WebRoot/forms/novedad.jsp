<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Novedad de Cuenta Corriente</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=NovedadesProcess"
	styleId="ProcessForm">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="guardar" />
	
	<html:errors />

	<table border="0">
		<tr>
			<th>Fecha:</th>
			<td><asf:calendar
					property="ProcessForm.process.novedad.fechaStr"
					value="${process.novedad.fechaStr}" /></td>
		</tr>
		<tr>
			<th>Concepto:</th>
			<td><select id="process.novedad.concepto"
				name="process.novedad.concepto" onchange="cargarTipos();"></select>
			</td>
		</tr>
		<tr>
			<th>Tipo de Concepto:</th>
			<td><select id="process.novedad.tipoConcepto"
				name="process.novedad.tipoConcepto" onchange="cargarConfig();"></select>
			</td>
		</tr>
		<tr>
			<th>Importe:</th>
			<td><html:text styleId="process.novedad.importe"
					property="process.novedad.importe" name="ProcessForm" /></td>
		</tr>
		<tr>
			<th>Observaciones:</th>
			<td><html:text property="process.novedad.detalle"
					name="ProcessForm" size="70" /></td>
		</tr>
	</table>

	<html:submit>
		<bean:message key="abm.button.save" />
	</html:submit>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<script type="text/javascript">
	function cargarConceptos() {
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarConceptosNovedades";
		retrieveURL(url, cargarConceptosResponse);
	}

	function cargarConceptosResponse(responseText) {
		var conceptoSelect = $('process.novedad.concepto');
		var optNull = document.createElement("option");
		optNull.text = "Seleccione";
		optNull.value = "";
		conceptoSelect.options.add(optNull);
		var objects = parseXml(responseText);
		if (objects.conceptos && objects.conceptos.concepto) {
			var concepto = objects.conceptos.concepto;
			if (concepto.constructor === Array) {
				for (var i = 0; i < concepto.length; i++) {
					var c = concepto[i];
					var opt = document.createElement("option");
					opt.text = c.detalle;
					opt.value = c.id;
					conceptoSelect.options.add(opt);
				}
			} else {
				var c = concepto;
				var opt = document.createElement("option");
				opt.text = c.detalle;
				opt.value = c.id;
				conceptoSelect.options.add(opt);
			}
		}
	}

	function cargarTipos() {
		var concepto = $('process.novedad.concepto');
		if (concepto.value == null || concepto.value == '') {
			return;
		}

		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarTiposConceptosNovedades&concepto="
				+ concepto.value;
		retrieveURL(url, cargarTiposResponse);
	}

	function cargarTiposResponse(data) {
		var objects = parseXml(data);
		var tipoConceptoSelect = $('process.novedad.tipoConcepto');
		while (tipoConceptoSelect.options.length > 0) {
			tipoConceptoSelect.options[0].remove();
		}

		var optNull = document.createElement("option");
		optNull.text = "Seleccione";
		optNull.value = "";
		tipoConceptoSelect.options.add(optNull);

		if (objects.tipos && objects.tipos.tipo) {
			var tipo = objects.tipos.tipo;
			if (tipo.constructor === Array) {
				for (var i = 0; i < tipo.length; i++) {
					var t = tipo[i];
					var opt = document.createElement("option");
					opt.text = t.text;
					opt.value = t.value;
					tipoConceptoSelect.options.add(opt);
				}
			} else {
				var t = tipo;
				var opt = document.createElement("option");
				opt.text = t.text;
				opt.value = t.value;
				tipoConceptoSelect.options.add(opt);
			}
		}
	}

	function cargarConfig() {
		var concepto = $('process.novedad.concepto');
		var tipoConcepto = $('process.novedad.tipoConcepto');
		if (concepto.value == null || concepto.value == ''
				|| tipoConcepto.value == null || tipoConcepto.value == '') {
			return;
		}
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarConfigNovedades&concepto="
				+ concepto.value;
		url += "&tipoConcepto=" + tipoConcepto.value;
		retrieveURL(url, cargarConfigResponse);
	}

	function cargarConfigResponse(responseText) {
		if (responseText == null || responseText == '') {
			return;
		}
		var objects = parseXml(responseText);
		$("process.novedad.importe").value = objects.config.importe;
	}

	cargarConceptos();
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>