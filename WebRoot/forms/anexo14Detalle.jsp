<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Acuerdo 2988 - Articulo 14 inc. A</div>
<html:form
	action="/actions/process.do?do=process&processName=Anexo14Detalle"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="buscar" styleId="action"/>
	<html:errors/>

	
	<table>
		<tr>
			<th>Fecha de:</th>
			<td>
				<html:radio name="ProcessForm" property="process.filtrarFecha" value="proceso"/>Proceso
				<html:radio name="ProcessForm" property="process.filtrarFecha" value="valor"/>Valor
			</td>
		</tr>
		<tr>
			<th>Fecha desde</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th>Fecha hasta</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th>Nro de Operaci&oacute;n</th>
			<td><html:text name="ProcessForm" property="process.numeroBoleto"/></td>
		</tr>
		<tr>
			<th>Tomador Cuit Nro</th>
			<td><html:text name="ProcessForm" property="process.cuit"/></td>
		</tr>
		<tr>
			<th>Tipo de Operaci&oacute;n</th>
			<td>
				<html:select name="ProcessForm" property="process.tipoBoleto">	
					<html:option value="${null}">Seleccione</html:option>
					<html:option value="Factura">Factura</html:option>
					<html:option value="Minuta Credito">Minuta Credito</html:option>
					<html:option value="Minuta Debito">Minuta Debito</html:option>
					<html:option value="Nota Credito">Nota Credito</html:option>
					<html:option value="Nota Debito">Nota Debito</html:option>
					<html:option value="Recibo">Recibo</html:option>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Concepto</th>
			<td>
				<asf:select name="process.concepto"
						entityName="com.nirven.creditos.hibernate.CConcepto"
						listCaption="getDetalle" listValues="getConcepto"
						nullValue="true" nullText="Seleccione" />
			</td>
		</tr>
		<tr>
			<th>Cuenta Contable</th>
			<td><html:text name="ProcessForm" property="process.cuentaContable"/></td>
		</tr>
		<tr>
			<th>Etapa del Cr&eacute;dito</th>
			<td>
				<asf:select name="process.idEstado"
						entityName="com.civitas.hibernate.persona.Estado"
						listCaption="getNombreEstado" listValues="getIdEstado"
						nullValue="true" nullText="Seleccione" />
			</td>
		</tr>
		<tr>
			<th>Comportamiento de Pago</th>
			<td>
				<asf:select name="process.comportamiento"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.comportamiento}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					nullValue="true" nullText="Seleccione"
					filter="categoria = 'comportamientoPago'  order by codigo" />
			</td>
		</tr>
		<tr>
			<th>Area Responsable</th>
			<td>
				<html:select name="ProcessForm" property="process.areaResponsable">	
					<html:option value="${null}">Seleccione</html:option>
					<!-- 
					<html:option value="Sub Dirección de Desarrollo">Sub Dirección de Desarrollo</html:option>
					<html:option value="Departamento Tesorería y Administración de Créditos">Departamento Tesorería y Administración de Créditos</html:option>
					<html:option value="Departamento Seguimiento de Créditos (Mora Aplicación y Cobranza)">Departamento Seguimiento de Créditos (Mora Aplicación y Cobranza)</html:option>
					<html:option value="Departamento Asesoría Letrada">Departamento Asesoría Letrada</html:option>
					 -->
					<html:option value="54">Sub Dirección de Desarrollo</html:option>
					<html:option value="179">Departamento Tesorería y Administración de Créditos</html:option>
					<html:option value="115">Departamento Seguimiento de Créditos (Mora Aplicación y Cobranza)</html:option>
					<html:option value="5">Departamento Asesoría Letrada</html:option>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Moneda</th>
			<td>
				<asf:select name="process.idMoneda"
					entityName="com.asf.gaf.hibernate.Moneda"
					listCaption="getAbreviatura" listValues="getId"
					value="ProcessForm.process.idMoneda" />
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Listado en pantalla"></html:submit>
			</th>
		</tr>
	</table>
	
	
	
	<logic:notEmpty name="ProcessForm" property="process.beans">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.beans" id="bean" export="true"
 			requestURI="${pageContext.request.contextPath}/actions/process.do">
 			<display:caption>Acuerdo 2988 - Articulo 14 inc. A</display:caption>
 			<display:column title="Codigo de Ingreso" property="codigoIngreso"/>
 			<display:column title="Concepto de ingreso" property="conceptoIngreso"/>
 			<display:column title="Nº de Cuenta Patrimonial" property="cuentaContable"  />
 			<display:column title="Fecha Valor" property="fechaValor" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Fecha Proceso" property="fechaProceso" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Proyecto" property="numeroAtencion"  />
 			<display:column title="Nº Persona" property="idPersona"  />
 			<display:column title="Tomador" property="persona"  />
 			<display:column title="CUIT" property="cuit"  />
 			<display:column title="Linea" property="linea"  />
 			<display:column title="Nº Comp." property="mov.numeroBoleto"  />
 			<display:column title="Moneda" property="mov.moneda"  />
 			<display:column title="Tipo Comp." property="mov.tipoBoleto"  />
 			<display:column title="Detalle" property="mov.detalle"  />
 			<logic:equal name="ProcessForm" property="process.idMoneda" value="1">
	 			<display:column title="Capital" property="mov.capital" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="CER" property="mov.cer" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="Compensatorios" property="mov.compensatorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="Moratorio" property="mov.moratorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="Punitorio" property="mov.punitorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="Gastos Administrativos" property="mov.gastos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="Gastos a recuperar" property="mov.gastosRec" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
	 			<display:column title="Multas" property="mov.multas" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			</logic:equal>
 			<logic:notEqual name="ProcessForm" property="process.idMoneda" value="1">
	 			<display:column title="Capital moneda original" property="mov.capital" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Capital en pesos" property="mov.capitalPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
	 			<display:column title="Compensatorios moneda original" property="mov.compensatorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Compensatorios en pesos" property="mov.compensatorioPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
	 			<display:column title="Moratorio moneda original" property="mov.moratorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Moratorio en pesos" property="mov.moratorioPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
	 			<display:column title="Punitorio moneda original" property="mov.punitorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Punitorio en pesos" property="mov.punitorioPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
	 			<display:column title="Gastos administrativos en moneda original" property="mov.gastos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Gastos administrativos en pesos" property="mov.gastosPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
	 			<display:column title="Gastos a recuperar en moneda original" property="mov.gastosRec" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Gastos a recuperar en pesos" property="mov.gastosRecPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
	 			<display:column title="Multas en moneda original" property="mov.multas" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
<%-- 	 			<display:column title="Multas en pesos" property="mov.multasPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" /> --%>
 			</logic:notEqual>
 			<display:column title="Total" property="mov.total" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<logic:notEqual name="ProcessForm" property="process.idMoneda" value="1">
 				<display:column title="Total Pesos" property="mov.totalPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			</logic:notEqual>
 			<display:column title="Etapa del Cr&eacute;dito" property="estado"  />
 			<display:column title="Comportamiento de Pago" property="comportamiento"  />
 			<display:column title="Area Responsable" property="areaResponsable"  />
 			<display:column title="Funcionario Responsable" property="funcionarioResponsable"> </display:column>
 			<display:column title="Observaciones"> </display:column>
 		</display:table>
	</div>
	</logic:notEmpty>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>