<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
<%
	pageContext.setAttribute("fechaActual", DateHelper
			.getString(new Date()));
	SimpleDateFormat formatoHora = new SimpleDateFormat("HH");
	SimpleDateFormat formatoMinuto = new SimpleDateFormat("mm");
	pageContext.setAttribute("hora", formatoHora.format(new Date()));
	pageContext.setAttribute("min", formatoMinuto.format(new Date()));
%>
<div class="title">
	Plantilla de Instrumento
</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<logic:equal parameter="do" value="load">
		<html:hidden property="entity.nuevo" value="false" />
	</logic:equal>
	<table border="0">
		<tr>
			<th width="40%">
				C�digo:
			</th>
			<td>
				<html:text property="entity.id" readonly="true" />
			</td>
		</tr>
		<tr>
			<th>
				Fecha:
			</th>
			<td>
				<asf:calendar property="entity.fechaStr"
					value="${(AbmForm.entity.fechaStr == null) ? fechaActual : AbmForm.entity.fechaStr}" />
			</td>

		</tr>
		<tr>
			<th>
				Nombre de la Plantilla:
			</th>
			<td>
				<html:text property="entity.identificacion" />
			</td>
		</tr>
		<tr>
			<th>
				Tipo:
			</th>
			<td>
				<asf:select name="entity.tipoEscrito"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoEscrito}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'escrito.tipo'  order by codigo"
					nullValue="true" nullText="Seleccione tipo de escrito..." />
			</td>
		</tr>
		<tr>
			<th>
				Modelo Base:
			</th>
			<td>
				<asf:lselect property="modelo" entityName="Modelo"
					listCaption="nombre" listValue="id" value="${modelo.id}"
					linkName="entity.tipoEscrito" linkFK="tipoEscrito"
					attribs="onchange='traerBody(this.value);'" nullValue="true"
					nullText="Seleccione modelo..." />
			</td>
		</tr>
			<tr>
				<th>
					Detalle:
				</th>
				<td>
					<fck:editor instanceName="entity.bodyStr" width="750" height="600"
						value='${AbmForm.entity.bodyStr}' />
				</td>
			</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
<script type="text/javascript" language="javascript">
	$j = jQuery.noConflict();
	var xml;

	function traerBody() {
		var modeloid = $j("#modelo").val();
		url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerBody&modeloid="
				+ modeloid;
		retrieveURL(url, respuesta2);
	}

	function respuesta2(responseText) {
		xml = responseText;
		cargarBody();
	}

	function cargarBody() {
		$j("entity\\.bodyStr");
		var oEditor = FCKeditorAPI.GetInstance('entity.bodyStr');
		oEditor.SetHTML(xml);
	}
</script>