<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">

<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<%
	pageContext.setAttribute("fechaActual", DateHelper.getString(new Date()));
	SimpleDateFormat formatoHora = new SimpleDateFormat("HH");
	SimpleDateFormat formatoMinuto = new SimpleDateFormat("mm");
	pageContext.setAttribute("hora", formatoHora.format(new Date()));
	pageContext.setAttribute("min", formatoMinuto.format(new Date()));
%>

<script language="javascript" type="text/javascript">
	/**
	 * @inputIn: control sobre el input que dispara la consulta ajax.-
	 * @defecto: para parametro=valor que tiene,
	 * a la izquierda del igual el id del input que dispara la consulta ajax.-
	 * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
	 */
	function actualizarLista(inputIn, defecto) {
		var urlDinamica = "";

		urlDinamica += "&persona=" + inputIn.value;
		urlDinamica += "&" + defecto;

		return urlDinamica;
	}
</script>

<div class="title">Turnos</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.accion"
		styleId="accion" />
	<html:hidden name="ProcessForm" property="process.turno.dni"
		styleId="dni" />
	<html:hidden name="ProcessForm" property="process.turno.cuit"
		styleId="cuit" />
	<html:hidden property="paramValue" styleId="paramValue"
		onchange="consultar();" value="" />

	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<table border="0">
		<tr>
			<th>Nombre:</th>
			<td><html:text property="process.turno.nombre" styleId="nombre"
					size='50'></html:text> <input value="..."
				onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=paramValue&amp;entityName=Persona&amp;title=Selecci�n de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');"
				type="button">&nbsp;</td>
		</tr>
		<tr>
			<th>Fecha:</th>
			<td><html:text property="process.turno.llegadaStr"
					value="${fechaActual}" readonly="true" /></td>
		</tr>
		<tr>
			<th>L�nea:</th>
			<td><asf:select entityName="com.nirven.creditos.hibernate.Linea"
					attribs="class='Linea'" listCaption="id,nombre" listValues="id"
					name="process.turno.linea_id" value="${process.turno.linea_id}"
					filter="activa=true" /></td>
		</tr>
		<tr>
			<th>Unidad:</th>
			<td><asf:select name="process.turno.unidadId"
					entityName="com.nirven.expedientes.persistencia.Unidad"
					value="${ProcessForm.process.turno.unidadId}"
					listCaption="codigo,nombre" listValues="id" orderBy="nombre" /></td>
		</tr>
		<tr>
			<th>Objeto:</th>
			<td><html:textarea styleId="objeto"
					onkeypress="caracteres(this,500,event)"
					property="process.turno.objeto" cols="40" rows="4" /></td>
		</tr>
		<tr>
			<th>Mail:</th>
			<td><html:text property="process.turno.mail" styleId="mail"></html:text>
			</td>
		</tr>
		<tr>
			<th>Hora:</th>
			<td><html:text property="process.hora" styleId="hora"
					value="${hora}:${min}" readonly="true"></html:text></td>
		</tr>
	</table>
	<input type="button" value="Guardar" onclick="guardar();">
	<input type="button" value="Cancelar" onclick="cancelar();">
	<input type="button" value="Atencion Turnos" onclick="siguiente();">
</html:form>
<script language='javascript'>
	$j = jQuery.noConflict();

	/*
	 var formulario = $('oForm');
	 var accion = $('accion');
	 */

	function guardar() {
		$('accion').value = "save";
		$('oForm').submit();
	}
	function cancelar() {
		$('accion').value = "cancelar";
		$('oForm').submit();
	}
	function siguiente() {
		window.open('${pageContext.request.contextPath}'
				+ '/actions/process.do?do=process&processName=AtencionTurnos',
				"_self");
	}
	function consultar() {

		var personaid = $j("#paramValue").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona2',
						'&personaid=' + personaid, function(data) {
							var temp = new Array();
							temp = data.split("#");
							$j("#nombre").val(temp[0]);
							$j("#cuit").val(temp[1]);
							$j("#dni").val(temp[2]);
						});

	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>