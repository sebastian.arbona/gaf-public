<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.civitas.importacion.Fecha"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%pageContext.setAttribute( "fechaActual",DateHelper.getString(new Date()) );%>
<%pageContext.setAttribute( "hora",new GregorianCalendar().get(GregorianCalendar.HOUR )) ;%>
<%pageContext.setAttribute( "min",new GregorianCalendar().get(GregorianCalendar.MINUTE) );%>
<body class=" yui-skin-sam">
  <link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
  <link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
</script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
</script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
</script>
  <div class="title">
   Nuevo Sub Tipo Resolución
  </div>
  <br>
  <br>
  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  
    <html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
    <html:hidden name="ProcessForm" property="process.idSubTipoResolucion" styleId="idSubTipoResolucion" />
    
    
    <div style="width: 70%" align="left">
      <html:errors />
    </div>
    
    <table border="0" class="tabla" id="acarreo1">
    
    	<tr>
        	<th>
          		Proceso:
        	</th>
        	<td colspan="3">
          		<asf:select entityName="com.nirven.creditos.hibernate.TipoResolucion"  
          		attribs="class='TipoResolucion'" listCaption="getId,getNombre"
            	listValues="getId" name="process.idTipoResolucion" value="${ProcessForm.process.idTipoResolucion}"
            	enabled="${ProcessForm.process.editable}"/>
        	</td>
      	</tr>
      
      <tr>
        <th>
           Nombre:
        </th>
        <td colspan="3">
          <asf:text property="ProcessForm.process.nombre" type="Text" maxlength="30" 
            readonly="${!ProcessForm.process.editable}" id="nombre"/>
        </td>
      </tr>       
      <tr>
        <th>
          Descripción:
        </th>
        <td colspan="3">
        <asf:text property="ProcessForm.process.observaciones" type="Text" maxlength="30" 
            readonly="${!ProcessForm.process.editable}" id="observaciones"/>
        </td>
      </tr>
      <tr>
		<th>Seleccionar Procesos</th>
				<td>
					<html:select styleId="procesosDisponiblesId" property="process.disponibles" multiple="true"  size="8">
						<html:optionsCollection  property="process.tipoProcesos" label="nombreTipoProceso" value="id"/>
					</html:select>
				</td>
				<td>
					<BR>
					<BR>
					<input type="button" id="agregar" value="Agregar >>" style='width:75px;'/><BR>
					<input type="button" id="quitar" value="<< Quitar" style='width:75px;'/>
				</td>
				<td>
					<html:select styleId="procesosSeleccionadosId" property="process.seleccion" multiple="true"  size="8" >
						<html:optionsCollection  property="process.tipoProcesosSeleccionados" label="nombreTipoProceso" value="id"/>
					</html:select>
				</td>
			</tr>
      </table>

    <logic:notEqual name="ProcessForm" property="process.accion" value="show">
	<br/>
	<br/>

      <input type="button" value="Guardar" id="guardar" />
      <input type="button" value="Cancelar" id="cancelar" />
      <div id="tab_cronograma"></div>
      <div id="tab_vinedos"></div>

    </logic:notEqual>
   
  </html:form>
  

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>
<script language="JavaScript" type="text/javascript">
jQuery.noConflict();
jQuery(document)
		.ready(
				function($) {
			
					var formulario = jQuery('#oForm');
					var accion = jQuery('#accion');
					

					jQuery('#guardar').click(function() {
					
						jQuery('#procesosSeleccionadosId option').attr('selected', 'selected');

						accion.val('guardar');

						formulario.submit();
					});
					jQuery('#cancelar').click(function() {
						accion.val('cancelar');
						formulario.submit();
					});
					
					 $("#agregar").click( function (){
                    	$('#procesosDisponiblesId option:selected').appendTo("#procesosSeleccionadosId");
               		});
               		
               		 $("#quitar").click( function (){
                    	$('#procesosSeleccionadosId option:selected').appendTo("#procesosDisponiblesId");
               		});
					
				});
			
</script>
<script language='javascript'>

</script>
<script type="text/javascript">
</script>
  <iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
    src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
    style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
  ></iframe>