<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/frames.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/links.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js" ></script>

<script language="javascript" type="text/javascript">
	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
  	}
    function actualizar( inputIn, defecto )
	{
		var urlDinamica = "";
		
		urlDinamica += "&" + defecto;

		return urlDinamica;
	}
</script>

<div class="title">
		Registrar Ingreso de Producto a la Bodega
</div>
    <br/>
<div style="width: 70%" align="left">
  <html:errors />
</div>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion"/>
    <html:hidden property="process.rowNum" value="${ProcessForm.process.rowNum}" styleId="rowNumId"/>
	<html:hidden property="process.persona.id" styleId="idPersona_"/>
	<html:hidden property="process.credito.expediente" styleId="expedienteCredito"/>
	<html:hidden property="process.credito.id" styleId="creditoId"/>
	<html:hidden property="process.autorizacionId" styleId="autorizacionId"/>
	<html:hidden property="process.qqIngresoStr" styleId="qqIngresoStr"/>
	<html:hidden property="process.qqIngresoId" styleId="qqIngresoId" value="${ProcessForm.process.qqIngresoId}"/>
    <logic:notEmpty name="ProcessForm" property="process.creditos">
    	<div>
    		<div style="float: right">
    			<input type="checkbox" id="seleccionarTodos" name="seleccionarTodos"/> <span style="font-size: 12px">Seleccionar Todos</span>
    			<input type="button" value="Consultar Seleccionados en INV" onclick="consultaINV();"/>
    			<input type="button" value="Guardar Ingresos Manuales" onclick="guardarQQManual();"/>
    		</div>
    		<div style="clear: both"/>
    	</div>
    	
    	<display:table name="ProcessForm" property="process.creditos" id="credito" export="true"
    	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}"
    	>
    		<display:column property="persona.nomb12" title="Solicitante" />
			<display:column property="expediente" title="Expediente"/>
			<display:column property="destinoNombre" title="Destino" />
			<display:column property="productoDeReferencia" title="Producto" />
			<display:column title="QQ Ingresados a la Bodega" media="html">
				<a href="javascript:void(0);" style="float: right" onclick="mostrarDetalleInvg(${credito.id})">
					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosGarantia}"/>
				</a>
				<div class="tooltip" style="display: none; min-width: 100px" id="detalleg-${credito.id}">
		  			<div class="tooltip-close" onclick="cerrarDetalleg(${credito.id});">X</div>
		  			<div class="tooltip-content" id="detalleg-content-${credito.id}">
		  				<logic:iterate id="qq" name="credito" property="detalleQQIngresadosGarantia">
		  					${qq.vinedo.vinedo.codigo}<br/>
		  					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${qq.qqIngreso}"/>
		  					<hr/>
		  				</logic:iterate>
		  				<logic:empty name="credito" property="detalleQQIngresadosGarantia">
		  					No hay informaci&oacute;n
		  				</logic:empty>
		  			</div>
		  		</div>
			</display:column>
			<display:column title="QQ Ingresados a la Bodega" media="excel pdf">
				<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosGarantia}"/>
			</display:column>
			<display:column title="QQ Ingresados en otra Bodega"  media="html">
				<a href="javascript:void(0);" style="float: right" onclick="mostrarDetalleInvx(${credito.id})">
					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosExternos}"/>
				</a>
				<div class="tooltip" style="display: none" id="detallex-${credito.id}">
		  			<div class="tooltip-close" onclick="cerrarDetallex(${credito.id});">X</div>
		  			<div class="tooltip-content" id="detallex-content-${credito.id}">
		  				<logic:iterate id="qq" name="credito" property="detalleQQIngresadosExternos">
		  					${qq.vinedo.vinedo.codigo}<br/>
		  					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${qq.qqIngreso}"/>
		  					<hr/>
		  				</logic:iterate>
		  				<logic:empty name="credito" property="detalleQQIngresadosExternos">
		  					No hay informaci&oacute;n
		  				</logic:empty>
		  			</div>
		  		</div>
			</display:column>
			<display:column title="QQ Ingresados en otra Bodega" media="excel pdf">
				<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosExternos}"/>
			</display:column>
			<display:column property="qqIngresadosTotales" title="Total QQ Ingresados" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>			
			<display:column title="Ingreso Manual QQ" media="html">
				<input type="text" name="qqmanual-${credito.id}" style="width: 60px"/>
			</display:column> 
			<display:column title="Consultar INV" style="text-align: center" media="html">
				<input type="checkbox" name="qqconsulta" value="${credito.id}" class="qqconsulta"/>
			</display:column>		
            <display:column media="html" title="Ingresar">
				<a href="javascript:void(0);" onclick="autorizar(${credito.id});"> 
					Autorizar
				</a> 
			</display:column>   	
    	</display:table>
    	<div>
    		<div style="float: right">
    			<input type="button" value="Consultar Seleccionados en INV" onclick="consultaINV();"/>
    			<input type="button" value="Guardar Ingresos Manuales" onclick="guardarQQManual();"/>
    		</div>
    		<div style="clear: both"/>
    	</div>
    </logic:notEmpty>   
    <br/>
    <table>
    	<tr>
			<th>L�neas</th>
			<td>
				<html:select property="process.seleccion" name="ProcessForm" multiple="true" size="4">
					<html:optionsCollection name="ProcessForm" property="process.lineas" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>
    	<tr>
    		<th style="width: 120px" >Persona:</th>
    		<td>
            	<asf:autocomplete name="" tipo="AJAX" idDiv="autocomplete_persona"
                                  idInput="idpersona" classDiv="autocomplete"
                                  origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
                                  options="{callback: actualizarLista, frequency: '0.8', minChars: '4',indicator:'indicadorPersona'}"
								  value="${ProcessForm.process.persona.id == 0 ? null : ProcessForm.process.persona.idpersona} ${ProcessForm.process.persona.id == 0 ? null : '-'} ${ProcessForm.process.persona.id == 0 ? null : ProcessForm.process.persona.nomb12Str2} ${ProcessForm.process.persona.id == 0 ? null : '-'} ${ProcessForm.process.persona.id == 0 ? null : ProcessForm.process.persona.nudo12Str}"
                                  attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'"/>
                                  <span id="indicadorPersona" style="display: none"><img src="images/loader.gif" alt="Trabajando..." /></span>
                                  <input value="..." onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=paramValue&amp;entityName=Persona&amp;title=Selecci�n de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');" type="button">&nbsp;
                                  (C�digo - Apellido y Nombre - N�mero de Documento)
            </td>
        </tr>
        </table>
        <input type="button" value="Listar por Persona" onclick="listarPorPersona();"/>
        <br/>
        <br/>
        <table>
        <tr>
			<th style="width: 120px">Expediente:</th>
   			<td>
   				<asf:autocomplete name="expediente" tipo="AJAX" idDiv="autocomplete_choices" 
   				idInput="expediente" classDiv="autocomplete"
   				origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarExpediente" 
   				options="{callback: actualizar, frequency: '0.8', minChars: '2'}"
   				attribs="size='50%'" 
   				value="${ProcessForm.process.expediente}"/>
			</td>
		</tr>
    	</table>
    <input type="button" value="Listar por Expediente" onclick="listarPorExpediente();"/>
    <br/>
    <br/>
    <input type="button" value="Listar Todos" id="listarId" onclick="listar();"/>
    <br/>
    <br/>
    <logic:equal value="true" name="ProcessForm" property="process.ingresar">
    <div>
		Por favor cargue el valor en QQ del NUEVO producto ingresado a la fecha. El valor cargado se sumar� al total.
	</div>
	<table>
		<tr>
			<th>Solicitante:</th>
			<td>${ProcessForm.process.credito.persona.nomb12}</td>
		</tr>
		<tr>
			<th>Expediente:</th>
			<td>${ProcessForm.process.credito.expediente}</td>
		</tr>
    	<logic:notEmpty name="ProcessForm" property="process.vinedo.garantia.valores">
			<logic:iterate id="valorGarantia" name="ProcessForm" property="process.vinedo.garantia.valores">
			<tr>
				<th>	
					<bean:write name="valorGarantia" property="campo.nombre"/>
				</th>
				<td>
					<bean:write name="valorGarantia" property="valorCadena"/>	
				</td>
			</tr>
			
			</logic:iterate>			
		</logic:notEmpty>
    	<tr>
    		<th>Total QQ Ingresados</th>
    		<td>${ProcessForm.process.vinedo.totalQQIngresados}</td>
    	</tr>
		<tr>
    		<th>Total Ingresado a la Fecha</th>
    		<td><html:text name="ProcessForm" property="process.qqIngresoStr" readonly="${ProcessForm.process.ingresarQQ}" maxlength="20" value="${ProcessForm.process.qqIngresoStr}" /></td>
    	</tr>
	</table>
	<br/>
	<html:errors/>
	<br/>
	<input type="button" value="Registrar Ingreso" id="aceptarId" onclick="registrarIngreso();"/>
    </logic:equal>
</html:form>


<script language="javascript" type="text/javascript">
<logic:notEmpty name="ProcessForm" property="process.creditos">
$("seleccionarTodos").observe('click', function(e) {
	$$('.qqconsulta').invoke('setValue',this.checked);
});
</logic:notEmpty>

if($('qqIngresoId') != null){
	if($($('qqIngresoId').value) != null){
		$($('qqIngresoId').value).focus();
	}
}
var ctrlPersona = $('idpersona');
var idPersona = $('idPersona_');
if(idPersona.value == null || ctrlPersona.value == "    " || idPersona.value == 0)
{
	ctrlPersona.value = "";
}
var ctrlExpediente = $('expediente');

    function listarVinedo()
    {
    	var cod = $('codigo').value
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=listarVinedo&process.civ='+cod;    
    }
    function ingresar(rowNum) 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=ingresar' +
        '&process.rowNum='+rowNum;      
    }
    function registrarIngreso(id,rowNum) 
    {
    	var confirmacion = confirm("�Est� seguro de querer registrar un ingreso de producto por la cantidad ingresada, para el Cr�dito seleccionado?");
    	if(confirmacion){
        	$('qqIngresoId').value = 'qqIngresoStr-'+rowNum;
        	$('qqIngresoStr').value = $('qqIngresoStr-'+rowNum).value;
	        $('accion').value = 'registrarIngreso';
	        $('creditoId').value = id;
	        $('oForm').submit();
        }    
    }
    function consultaINV() {
		var form = $('oForm');
        var accion = $('accion');
        accion.value = 'consultaINV';
        form.submit();
    }
    function guardarQQManual() {
    	var form = $('oForm');
        var accion = $('accion');
        accion.value = 'guardarQQManual';
        form.submit();
    }
    function autorizar(idCredito) {
        if (confirm('�Desea autorizar el desembolso para el proyecto seleccionado?')) {
	    	var form = $('oForm');
	        var accion = $('accion');
	        accion.value = 'autorizar';
	        $('autorizacionId').value = idCredito;
	        form.submit();
        }
    }
    function listar()
    {
    	var form = $('oForm');
        var accion = $('accion');
        accion.value = 'listar';
        ctrlPersona.value = "";
        ctrlExpediente.value = "";
        form.submit();
        
//         window.location =         
//         '${pageContext.request.contextPath}/actions/process.do?do=process' +
//         '&processName=${param.processName}' +
//         '&process.accion=listar';    
    }
    function listarPorPersona(){
		idPersona.value = trim(ctrlPersona.value.split("-")[0]);
        var form = $('oForm');
        var accion = $('accion');
        accion.value = 'listarPorPersona';
    	ctrlExpediente.value = "";
        form.submit();
    }
    function listarPorExpediente(){
    	var expediente = $('expedienteCredito');
    	expediente.value = ctrlExpediente.value;
        var form = $('oForm');
        var accion = $('accion');
        accion.value = 'listarPorExpediente';
    	ctrlPersona.value = "";
        form.submit();
    }

    function mostrarDetalleInvg(id) {
    	var tooltip = document.getElementById("detalleg-"+id);
    	tooltip.style.display = 'block';
    }
    function mostrarDetalleInvx(id) {
    	var tooltip = document.getElementById("detallex-"+id);
    	tooltip.style.display = 'block';
    }
    function cerrarDetalleg(id) {
    	var tooltip = document.getElementById("detalleg-"+id);
    	tooltip.style.display = 'none';
    }
    function cerrarDetallex(id) {
    	var tooltip = document.getElementById("detallex-"+id);
    	tooltip.style.display = 'none';
    }
</script>