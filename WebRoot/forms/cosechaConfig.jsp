<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<div class="title">Configuraci�n de Cosecha</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<script type="text/javascript">
	
function verificarPorcentaje(porcentaje){
	var tasa = porcentaje.value.replace(',', '.').replace('%', '');
	if(tasa < 0 || tasa > 100){
		alert("Debe ingresar un valor entre 0 y 100");
		porcentaje.value = '0%';
	}else{
		porcentaje.value = porcentaje.value.concat('%');
	}
}

function verificarPrecio(monto){
	var precio = monto.value.replace(',','.');
	if(precio < 0){
		alert("Debe ingresar un valor mayor que cero.");		
		monto.value = '0.00';
	}else{
		monto.value = precio;
	}
}

</script>
<html:form styleId="oForm"
	action="/actions/process.do?do=process&processName=CosechaConfigProcess">

	<html:hidden property="process.action" styleId="action" />
	<html:hidden property="process.cid" />
	<html:hidden property="process.periodoIndex" styleId="periodoIndex" />

	<table border="0" width="100%">
		<tr>
			<th>Temporada:</th>
			<td><html:text property="process.cosechaConfig.temporada"
					maxlength="4" /></td>
			<th>Varietales:</th>
			<td><html:checkbox name="ProcessForm"
					property="process.cosechaConfig.varietales"></html:checkbox></td>
		</tr>
		<tr>
			<th>Costo QQ por cosecha:</th>
			<td><html:text
					property="process.cosechaConfig.costoQQCosechaStr" maxlength="50"
					styleId="costoQQCosechaStr"
					onchange="verificarPrecio(costoQQCosechaStr);" /></td>
			<th>Fecha Inicio Periodo:</th>
			<td><asf:calendar
					property="ProcessForm.process.cosechaConfig.fechaInicioPerStr"
					value="${process.cosechaConfig.fechaInicioPerStr}" /></td>
		</tr>
		<tr>
			<th>Primer Vencimiento:</th>
			<td><asf:calendar
					property="ProcessForm.process.cosechaConfig.fechaPrimerVtoStr"
					value="${process.cosechaConfig.fechaPrimerVtoStr}" /></td>
			<th>Superficie permitida (ha):</th>
			<td><html:text
					property="process.cosechaConfig.superficiePermitida" maxlength="50" />
			</td>
		</tr>
		<tr>
			<th>Fecha Fin Periodo:</th>
			<td><asf:calendar
					property="ProcessForm.process.cosechaConfig.fechaFinPerStr"
					value="${process.cosechaConfig.fechaFinPerStr}" /></td>
		</tr>
		<tr>
			<th>Costo QQ por elaboraci�n:</th>
			<td><html:text property="process.cosechaConfig.costoQQElabStr"
					maxlength="50" styleId="costoQQElabStr"
					onchange="verificarPrecio(costoQQElabStr);" /></td>
			<th>Fecha Primer Desembolso:</th>
			<td><asf:calendar
					property="ProcessForm.process.cosechaConfig.primerDesembolsoStr"
					value="${process.cosechaConfig.primerDesembolsoStr}" /></td>
		</tr>
		<tr>
			<th>Cantidad m�xima QQ</th>
			<td><html:text property="process.cosechaConfig.maxQQ" /></td>
			<th>Fecha Segundo Desembolso:</th>
			<td><asf:calendar
					property="ProcessForm.process.cosechaConfig.segundoDesembolsoStr"
					value="${process.cosechaConfig.segundoDesembolsoStr}" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Datos Garant�as Fiduciarias</td>
		</tr>
		<tr>
			<table border="0" " width="90%">
				<tr>
					<th>Frecuencia Cuotas:</th>
					<td width='20%'><asf:select
							name="process.cosechaConfig.periodicidad"
							entityName="com.asf.hibernate.mapping.Tipificadores"
							listCaption="getCodigo,getDescripcion" listValues="getCodigo"
							value="${ProcessForm.process.cosechaConfig.periodicidad}"
							filter="categoria='amortizacion.periodicidad' order by codigo" />
					</td>
					<th>Precio del Vino Tinto:</th>
					<td width="20%"><html:text
							property="process.cosechaConfig.precioVinoTintoStr"
							maxlength="50" styleId="precioVinoTintoStr"
							onchange="verificarPrecio(precioVinoTintoStr);" /></td>
					<th>% del Vino Tinto:</th>
					<td width="20%"><html:text
							property="process.cosechaConfig.porcVinoTinto" maxlength="10"
							styleId="porcVinoTinto" /></td>
				</tr>
				<tr>

					<th>Aforo:</th>
					<td><html:text property="process.cosechaConfig.aforo"
							maxlength="50" styleId="aforoStr"
							onchange="verificarPrecio(aforoStr);" /></td>
					<th>Precio del Vino Blanco:</th>
					<td><html:text
							property="process.cosechaConfig.precioVinoBlancoStr"
							maxlength="50" styleId="precioVinoBlancoStr"
							onchange="verificarPrecio(precioVinoBlancoStr);" /></td>
					<th>% del Vino Blanco:</th>
					<td><html:text property="process.cosechaConfig.porcVinoBlanco"
							maxlength="10" styleId="porcVinoBlanco" /></td>
				</tr>
				<tr>
					<th>Cantidad Cuotas:</th>
					<td><html:text property="process.cosechaConfig.cantCuotas" />
					</td>
					<th>Precio del Mosto:</th>
					<td><html:text property="process.cosechaConfig.precioMostoStr"
							maxlength="50" styleId="precioMontoStr"
							onchange="verificarPrecio(precioMontoStr);" /></td>
					<th>% del Mosto:</th>
					<td><html:text property="process.cosechaConfig.porcMosto"
							maxlength="10" styleId="porcMosto" /></td>
				</tr>
				<tr>
					<th>% Auditoria Final:</th>
					<td><html:text
							property="process.cosechaConfig.porcAuditoriaFinal"
							maxlength="10" styleId="porcAuditoriaFinal" /></td>
					<th>Precio Malbec:</th>
					<td><html:text
							property="process.cosechaConfig.precioMalbecStr" maxlength="50"
							styleId="precioMalbecStr"
							onchange="verificarPrecio(precioMalbecStr);" /></td>
					<th>% del Malbec:</th>
					<td><html:text property="process.cosechaConfig.porcMalbec"
							maxlength="10" styleId="porcMalbec" /></td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<th>Precio Otros varietales:</th>
					<td><html:text
							property="process.cosechaConfig.precioOtrosVarietalesStr"
							maxlength="50" styleId="precioOtrosVarietalesStr"
							onchange="verificarPrecio(precioOtrosVarietalesStr);" /></td>
					<th>% Otros varietales:</th>
					<td><html:text
							property="process.cosechaConfig.porcOtrosVarietales"
							maxlength="10" styleId="porcOtrosVarietales" /></td>
				</tr>
			</table>
	</table>
	<br />
	<br />

	<html:hidden property="process.periodoCosechaConfig.id" />
	<table width="100%">
		<tr>
			<td colspan="4" align="center">Intereses para el Sub Per�odo</td>
		</tr>
		<tr>
			<th>Desde:</th>
			<td><asf:calendar
					property="ProcessForm.process.periodoCosechaConfig.fechaDesdeStr" />
			</td>
			<th>Hasta:</th>
			<td><asf:calendar
					property="ProcessForm.process.periodoCosechaConfig.fechaHastaStr" />
			</td>
		</tr>
		<tr height="25px" />

		<tr height="25px" />
		<tr>
			<th>Si la garant�a asociada es:</th>
			<td><asf:select
					name="process.periodoCosechaConfig.tipoGarantia_id"
					entityName="com.nirven.creditos.hibernate.TipoGarantia"
					value="${ProcessForm.process.periodoCosechaConfig.tipoGarantia_id}"
					listCaption="getNombre" listValues="getId" nullText="Sin Garant�a"
					nullValue="true" /></td>
			<th>Producto</th>
			<td><asf:select name="process.periodoCosechaConfig.tipoProducto"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="${ProcessForm.process.periodoCosechaConfig.tipoProducto}"
					filter="categoria='Garantia.tipoProducto' order by codigo"
					nullValue="true" nullText="N/A" /></td>
		</tr>
		<tr>
			<th>�ndice</th>
			<td colspan="3"><asf:selectpop
					name="process.periodoCosechaConfig.idIndiceGar" title="Tasa"
					columns="Codigo, Nombre" captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.Indice"
					value="${ProcessForm.process.periodoCosechaConfig.idIndiceGar}"
					filter="tipo in (1, 4)" onChange="tasaGar();" /> <asf:text
					name="ProcessForm" id="process.periodoCosechaConfig.indiceValorGar"
					readonly="true" property="entity.valorIndiceGar"
					value="${ProcessForm.process.periodoCosechaConfig.indiceValorGar}"
					type="decimal" maxlength="15" /></td>
		</tr>
		<tr>
			<th>Mas</th>
			<td><html:text
					property="process.periodoCosechaConfig.valorMasGarStr" size="15"
					onchange="calcularValorFinalGar();" styleId="masGar" /></td>
			<th>Por</th>
			<td><html:text
					property="process.periodoCosechaConfig.valorPorGarStr" size="15"
					onchange="calcularValorFinalGar();" styleId="porGar" /></td>
		</tr>
		<tr>
			<th>D�as antes</th>
			<td><html:text
					value="${ProcessForm.process.periodoCosechaConfig.diasAntesGar}"
					property="process.periodoCosechaConfig.diasAntesGar" size="15" /></td>
			<th>Tasa Inter�s final</th>
			<td><asf:text name="ProcessForm" id="valorFinalGar"
					readonly="true"
					property="process.periodoCosechaConfig.valorFinalGar"
					value="${ProcessForm.process.periodoCosechaConfig.valorFinalGar}"
					type="decimal" maxlength="15" /></td>
		</tr>
		<tr>
			<th>Entidad De La Bonificaci�n:</th>
			<td><asf:select name="process.periodoCosechaConfig.idEnteGar"
					entityName="com.asf.gaf.hibernate.Bancos"
					value="${ProcessForm.process.periodoCosechaConfig.idEnteGar}"
					listCaption="getDetaBa" listValues="getCodiBa"
					filter="bonificador = 1  order by codiBa" /></td>
			<th>L�nea de bonificaci�n:</th>
			<td><asf:lselect
					property="process.periodoCosechaConfig.idBonificacionGar"
					entityName="Bonificacion" listCaption="nombre" listValue="id"
					value="${ProcessForm.process.periodoCosechaConfig.idBonificacionGar}"
					linkFK="enteBonificador.codiBa"
					linkName="process.periodoCosechaConfig.idEnteGar" orderBy="nombre"
					nullValue="true" /></td>
		</tr>
	</table>
	<html:submit onclick="agregarPeriodo();">Agregar Per�odo</html:submit>
	<br />
	<br />
	<display:table name="ProcessForm" property="process.periodos"
		export="false" id="reportTable">
		<display:column title="Desde" property="fechaDesde"
			decorator="com.asf.displayDecorators.DateDecorator" />
		<display:column title="Hasta" property="fechaHasta"
			decorator="com.asf.displayDecorators.DateDecorator" />
		<display:column title="Garant�a" property="tipoGarantiaNombre" />
		<display:column title="Producto" property="tipoProductoNombre" />
		<display:column title="Tasa Comp. Final" property="valorFinalGar" />
		<display:column title="Tasa Bon." property="bonificacionGar.nombre" />
		<display:column title="Acciones">
			<div onclick="modificar(${reportTable_rowNum});"
				style="float: left; margin-right: 10px; cursor: pointer;">Modificar</div>
			<div onclick="eliminar(${reportTable_rowNum});"
				style="float: left; cursor: pointer">Eliminar</div>
		</display:column>
	</display:table>
	<br />
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>

	<html:submit onclick="guardar();">Guardar</html:submit>
	<html:submit onclick="cancelar();">Cancelar</html:submit>

</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>
<script type="text/javascript">
$j = jQuery.noConflict();
function editarIndice(idIndice) {
	var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?";
	url += "do=buscarValorIndice&idindice=" + idIndice;
	retrieveURL(url, editarIndiceResponse);
	
<%--	actions/process.do?do=process&processName=CreditoDatosFinancieros";--%>
<%--	url += "&process.action=loadIndice";--%>
<%--	url += "&process.idObjetoiIndice=" + idIndice;--%>
}
function editarIndiceResponse(text){
	
}
function calcularValorFinal(){
	var tasa = $j("#indiceValor").val().replace(',', '.').replace('%', '');
	var mas = $j("#mas").val().replace(',', '.').replace('%','');
	var por = $j("#por").val().replace(',', '.').replace('%','');
	var topeTasa = $j("#topeTasa").val().replace(',', '.').replace('%',	'');
		
	$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal', 
			'&tasa='+tasa+'&mas='+mas+'&por='+por+'&topeTasa='+topeTasa,
			function(data) {
				$j("#valorFinal").val(data);
			}); 
}
		
function calcularValorFinal(tasaInd){
  	var tasa = $j("#indiceValor").val().replace(',', '.').replace('%', '');
	var mas = $j("#mas").val().replace(',', '.').replace('%','');
	var por = $j("#por").val().replace(',', '.').replace('%','');
	var topeTasa = $j("#topeTasa").val().replace(',', '.').replace('%',	'');
	
	$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal', 
			'&tasa='+tasa+'&mas='+mas+'&por='+por +'&topeTasa='+topeTasa,
			function(data) {
				$j("#valorFinal").val(data);
			}); 
		}
function tasa() {
	var idIndice = $j("#process\\.periodoCosechaConfig\\.idIndice").val();
		$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='+idIndice, 
			function(data) {
				$j("#indiceValor").val(data);
				$j("#valorFinal").val(calcularValorFinal(data));
			}
		);														
}

function calcularValorFinalGar(){
	var tasa = $j("#indiceValorGar").val().replace(',', '.').replace('%', '');
	var mas = $j("#masGar").val().replace(',', '.').replace('%','');
	var por = $j("#porGar").val().replace(',', '.').replace('%','');
	var topeTasa = $j("#topeTasaGar").val().replace(',', '.').replace('%','');
	$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal', 
			'&tasa='+tasa+'&mas='+mas+'&por='+por+'&topeTasa='+topeTasa,
			function(data) {
				$j("#valorFinal").val(data);
			}); 
}

function calcularValorFinalGar(tasaInd){
	var tasa = $j("#indiceValorGar").val().replace(',', '.').replace('%', '');
	var mas = $j("#masGar").val().replace(',', '.').replace('%','');
	var por = $j("#porGar").val().replace(',', '.').replace('%','');
	var topeTasa = $j("#topeTasaGar").val().replace(',', '.').replace('%','');
	$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal', 
			'&tasa='+tasa+'&mas='+mas+'&por='+por+'&topeTasa='+topeTasa,
			function(data) {
				$j("#valorFinalGar").val(data);
			}); 
}

function tasaGar() {
			var idIndice = $j("#process\\.periodoCosechaConfig\\.idIndiceGar").val();
				$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='+idIndice, 
					function(data) {
						$j("#indiceValorGar").val(data);
						$j("#valorFinalGar").val(calcularValorFinalGar(data));
					}
				);													
		}

function agregarPeriodo() {
	var accion = $('action');
	accion.value = 'agregarPeriodo';
}

function guardar() {
	var accion = $('action');
	accion.value = 'guardar';
}

function modificar(i) {
	var accion = $('action');
	accion.value = 'modificarPeriodo';
	var periodoIndex = $('periodoIndex');
	periodoIndex.value = i;
	$('oForm').submit();
}

function eliminar(i) {
	var accion = $('action');
	accion.value = 'eliminarPeriodo';
	var periodoIndex = $('periodoIndex');
	periodoIndex.value = i;
	$('oForm').submit();
}

function cancelar() {
	var accion = $('action');
	accion.value = 'cancelar';
}

</script>
