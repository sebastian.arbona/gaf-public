<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Categor�a de Entidad Financiera</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true">
<input type="hidden" name="entity.idEntidad" value="${paramValue[0]}">
<table border="0">
	<html:hidden property="entity.id"/>
	<tr>
		<th>C�digo:</th>
		<td><html:text property="entity.codigoCategoria" styleId="codigo"/></td>
	</tr>
	<tr>
		<th>Categor�a:</th>
		<td>
			<html:text property="entity.nombreCategoria"/>
		</td>	
	</tr>
	<tr>
		<th>Descripci�n de la Categor�a:</th>
		<td><html:textarea property="entity.descripcionCategoria" cols="50" rows="4" onkeypress="caracteres(this,255,event)"/></td>
	</tr>
</table>
<br/>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<script language='javascript' >
var codigo = $('codgo');

codigo.focus();
</script>