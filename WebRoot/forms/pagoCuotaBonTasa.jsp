<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Pagar Cuotas</div>

<html:form
	action="/actions/process.do?do=process&processName=BonTasaPagoCuota"
	styleId="ProcessForm">
	<html:hidden name="ProcessForm" property="process.cuotasSeleccionadas" value="${ProcessForm.process.cuotasSeleccionadas}" />
	<html:errors/>
	
	<table>	
		<tr>	
			<th>Nro Orden Pago</th>
			<td>
			<html:text property="process.numeroOrdenPago" name="ProcessForm"/>
			</td>
		</tr>	
		<tr>	
			<th>fecha Pago</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaPago" 
 					value="${process.fechaPago}"/> 
			</td>
		</tr>	
		<tr>	
			<th>Observaciones</th>
			<td>
			<html:text property="process.observaciones" name="ProcessForm"/>
			</td>
		</tr>	
	</table>
	<div>
		<html:submit  value="Guardar" ></html:submit>
	</div>
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>