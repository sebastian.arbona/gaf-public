<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div style="width: 70%" align="left">
	<html:errors />
</div>

<html:form	action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	
	<div class="title">Consulta de Hect�reas Cultivadas</div>
	
	<div style="overflow: auto;" class="grilla"> 
	
		<display:table name="ProcessForm" property="process.hectareasCultivadasSeleccionadas" id="sessionTable" style="border: solid; border-color: #CCCCCC;">
			<display:column title="C.U.I.T." property="cuit" sortable="true"/>
			<display:column title="Nombre" property="persona.nomb12" sortable="true"/>
			<display:column title="Vi�edos" property="vinedos" sortable="true" align="right"/>
			<display:column title="Superficie" property="superficie" sortable="true" align="right"  decorator="com.asf.displayDecorators.DoubleDecorator" />
			<display:column>
				<input type="button" value="Quitar" onclick="quitar(${sessionTable.cuit});" />
			</display:column>
			<display:footer>
				<tr>
					<th align="right" colspan="3">
						Total Superficie:
					</th>
					<td align="right">
						<b>${ProcessForm.process.totalHectareas}</b>
					</td>
					<th>
						Hect�reas
					</th>			
				</tr>
			</display:footer>
		</display:table>	
	</div>
	<br />
	<input type="button" value="Imprimir Consulta" onclick="imprimir();" />
	<br />
	
	<div class="title">A�adir Personas</div>
	
	<div style="overflow: auto;" class="grilla"> 
	
		<table border="0" style="border: solid; border-color: #CCCCCC;">
			<tr>
				<th>Nro Cuit:</th>
				<td><asf:text name="ProcessForm"
						property="process.cuit" type="Long" maxlength="16"
						attribs="class='numeroAtencion'" /></td>
				<th><input type="button" value="Buscar" onclick="buscar();" /></th>
				<th><input type="button" value="A�adir como nuevo CUIT" onclick="nuevo();" /></th>
			</tr>
			
		</table>
		<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
		<html:hidden name="ProcessForm" property="process.cuitSeleccionado"	styleId="idCuitSeleccionado" />		
	</div>

	
	<div style="overflow: auto;" class="grilla"> 
	
		<display:table name="ProcessForm" property="process.hectareasCultivadas" id="reportTable" style="border: solid; border-color: #CCCCCC;">
			<display:column title="C.U.I.T" property="cuit" sortable="true"/>
			<display:column title="Nombre" property="persona.nomb12" sortable="true"/>
			<display:column title="Vi�edos" property="vinedos" sortable="true" 	align="right"/>
			<display:column title="Superficie" property="superficie" sortable="true" align="right"  decorator="com.asf.displayDecorators.DoubleDecorator" />
			<display:column>
				<input type="button" value="Agregar" onclick="agregar(${reportTable.cuit});" />
			</display:column>
		</display:table>	
	</div>
	
</html:form>

<script language='javascript'>
	function buscar() {
		var form = $('oForm');
		accion.value = 'buscar';
		form.submit();
	}
	
	function nuevo() {
		var form = $('oForm');
		accion.value = 'nuevo';
		form.submit();
	}
	
	function agregar(id) {
		var form = $('oForm');
		var idCuitSeleccionado = $('idCuitSeleccionado');
		idCuitSeleccionado.value = id;
		accion.value = 'agregar';
		form.submit();
	}	
	
	function quitar(id) {
		var form = $('oForm');
		var idCuitSeleccionado = $('idCuitSeleccionado');
		idCuitSeleccionado.value = id;
		accion.value = 'quitar';
		form.submit();
	}

	function aceptar() {
		var form = $('oForm');
		accion.value = 'load';
		form.submit();
	}	

	function imprimir() {
		var form = $('oForm');
		accion.value = 'imprimir';
		form.submit();
	}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
	
