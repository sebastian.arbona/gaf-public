<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>
<script>
    function importar(){      
       return true;
    }

    function eliminar(){
        var oForm = $('oForm');
        oForm.action = '${pageContext.request.contextPath}/actions/importacion.do?do=eliminar'; 
        return true;
    }

    function reporte(){
        popUpBig( "${pageContext.request.contextPath}/actions/process.do?do=process&processName=ReporteErrorImportacion&process.nroreporte=<bean:write name="ImportacionForm" property="nroReporte"/>");
    }
</script>

<html:form action="/actions/importacion.do?do=importar" method="post" enctype="multipart/form-data" styleId="oForm">
	<table border="0">
		<tr>
			<th>
				Formato de Importaci�n:
			</th>
			<td>
				<asf:select name="idFormato" 
					entityName="com.asf.hibernate.mapping.Formatoimportacion"
					listCaption="id,detalle" listValues="getId" 
					value="${ImportacionForm.idFormato}"/>
			</td>
		</tr>
		<tr>
			<th>
				Archivo:
			</th>
			<td>
				<html:file name="ImportacionForm" property="file" styleId="archivo"
					size="40" disabled="${ImportacionForm.deshabilitado}"/>
			</td>
		</tr>
	</table>
	<br>
	<display:table name="ImportacionForm" property="result" export="false" id="reportTable">
		<display:column title="Proyecto" property="numeroProyecto" />
		<display:column title="Titular" property="credito.persona.nomb12" />
		<display:column title="Importe Pagado" property="importe" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" align="right" />

		<display:footer>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>
					<b>Total: $ <bean:write name="ImportacionForm" property="totalPagado" format="#,###,###.##" />
					</b>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>
					<b>Total de Boletos Le�dos: <bean:write name="ImportacionForm" property="totalBoletos" />
					</b>
				</td>
			</tr>
		</display:footer>

	</display:table>
	<br>
	<div style="width: 70%" align="center">
		<html:messages id="mensajes"/>
	</div>
	<html:errors />
	<logic:notEmpty name="ImportacionForm" property="nroReporte">
		<a href="javascript://" onclick="return reporte();">Errores de
			Importaci�n Nro: <bean:write name="ImportacionForm"
				property="nroReporte" /><a /><br>
	</logic:notEmpty>
	<br>
	<html:submit onclick="return importar();" disabled="${ImportacionForm.deshabilitado}">Importar</html:submit>
	<html:submit onclick="return eliminar();">Eliminar</html:submit>
</html:form>

${ImportacionForm.script}

