<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="javascript" type="text/javascript">
	function cambiarLiquidacion() {
		var aLiquidar;
		var objeto = getElem("objLiquidar");
		var liquidaPor = getElem("process.liquidaPor").value;
		if (liquidaPor == "1") {
			ocultar("credito");
			ocultar("rangoCreditos");
			ocultar("rangoLineas");
			ocultar("fecovitaTipo");
			mostrar("moneda");
			mostrar("linea");
			aLiquidar = getElem("process.tipoLinea");
		} else if (liquidaPor == "2") {
			ocultar("linea");
			ocultar("rangoCreditos");
			ocultar("rangoLineas");
			ocultar("fecovitaTipo");
			mostrar("moneda");
			mostrar("credito");
			aLiquidar = getElem("process.idObjetoi");
		} else if (liquidaPor == "3") {
			ocultar("linea");
			ocultar("credito");
			ocultar("rangoCreditos");
			ocultar("rangoLineas");
			ocultar("fecovitaTipo");
			mostrar("moneda");
			mostrar("rangoFechas");
		} else if (liquidaPor == "4") {
			//liquida por credito
			ocultar("linea");
			ocultar("credito");
			ocultar("rangoLineas");
			ocultar("fecovitaTipo");
			mostrar("moneda");
			mostrar("rangoCreditos");
		} else if (liquidaPor == "5") {
			//liquida por rango de lineas
			ocultar("linea");
			ocultar("credito");
			ocultar("rangoCreditos");
			ocultar("fecovitaTipo");
			mostrar("moneda");
			mostrar("rangoLineas");
		} else if (liquidaPor == "6") {
			//liquidacion completa
			ocultar("linea");
			ocultar("credito");
			ocultar("rangoCreditos");
			ocultar("rangoLineas");
			ocultar("fecovitaTipo");
			mostrar("moneda");
		} else if (liquidaPor == "7") {
			//liquida por fecovita
			ocultar("linea");
			ocultar("credito");
			ocultar("rangoCreditos");
			ocultar("rangoLineas");
			mostrar("moneda");
			mostrar("fecovitaTipo");
		}
		objeto.value = aLiquidar.value;
	}

	function sendForm() {
		/*document.getElementById("oForm").submit();*/
		window.open("${pageContext.request.contextPath}/progress.jsp",
				"Proceso", "width=450, height=180");
		return true;
	}
</script>


<div class="title">Proceso de Facturaci�n - Calcular Facturaci�n</div>
<br>
<br>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="calcular"
		styleId="accion" />

	<div id="liquida">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th>�ltima actualizaci�n Comportamientos Pago:</th>
				<td colspan="3"><html:text
						property="process.ultimaActualizacionInicio" readonly="true"></html:text>
					- <html:text property="process.ultimaActualizacionFin"
						readonly="true"></html:text></td>
			</tr>

			<tr>
				<th>�ltima recaudaci�n realizada el d�a:</th>
				<td><html:text property="process.ultimaRecaudacion"
						readonly="true"></html:text></td>
				<th>Con fecha de Cobranza el d�a:</th>
				<td><html:text property="process.ultimaCobranza"
						readonly="true"></html:text></td>
			</tr>
		</table>
		<br>

		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Liquidaci�n por:</th>
				<td width="515px"><asf:select name="process.liquidaPor"
						attribs="onChange='cambiarLiquidacion()' onBlur='cambiarLiquidacion()'"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessAction.process.liquidaPor}"
						filter="categoria='liquidacion.criterio'"></asf:select></td>
			</tr>
			<tr>
				<th>Incluir cr&eacute;ditos en estado:</th>
				<td><html:select styleId="estadoCreditoId"
						property="process.seleccion" multiple="true" size="8">
						<html:optionsCollection property="process.estados"
							label="nombreEstado" value="nombreEstado" />
					</html:select></td>
			</tr>
			<html:hidden property="process.objLiquidar" styleId="objLiquidar" />
		</table>
	</div>
	<br>
	<div id="rangoFechas" title="Rango de Fechas">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Fecha Vencimiento Hasta:</th>
				<td width="515px"><asf:calendar
						property="ProcessForm.process.fechaHasta" /></td>
			</tr>
		</table>
	</div>
	<div id="moneda" title="Moneda">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Moneda:</th>
				<td width="515px"><asf:select name="process.idMoneda"
						entityName="com.asf.gaf.hibernate.Moneda"
						listCaption="getId,getAbreviatura" listValues="getId"
						value="${ProcessAction.process.idMoneda}"></asf:select></td>
			</tr>
		</table>
	</div>
	<div id="credito" title="Liquidaci�n" style="display: none;">
		<table border="0" style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Credito:</th>
				<td width="515px"><asf:selectpop name="process.idCreditoDesde"
						columns="ID,Numero Credito" captions="id,numeroCredito"
						values="id" entityName="com.nirven.creditos.hibernate.Objetoi"
						value="${ProcessForm.process.idCredito}" attribs=""
						title="Consulta de Cr�ditos" /></td>
			</tr>
		</table>
	</div>
	<div id="linea" title="Linea de Credito" style="display: none;">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Linea de Credito:</th>
				<td width="515px"><asf:select name="process.idTipoLinea"
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="getId,getNombre" listValues="getId"
						filter="activa = true order by id" attribs=""
						value="${ProcessForm.process.idTipoLinea}" nullValue="true"
						nullText="Seleccione l�nea..." /></td>
			</tr>
		</table>
	</div>
	<div id="rangoCreditos" title="Rango de Cr�ditos">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Proyecto:</th>
				<td width="515px"><asf:selectpop name="process.nroAtencion"
						columns="Nro.Proyecto,Titular"
						captions="numeroAtencion,persona.nomb12" values="numeroAtencion"
						entityName="com.nirven.creditos.hibernate.Objetoi"
						value="${ProcessForm.process.idCreditoDesde}" attribs=""
						title="Consulta de Cr�ditos" caseSensitive="true"
						filter="linea.id != ${ProcessForm.process.idLineaEmergencia}" /></td>
			</tr>
		</table>
	</div>
	<div id="rangoLineas" title="Rango de Lineas de Credito"
		style="display: none;">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Linea de Credito Desde:</th>
				<td width="515px"><asf:select name="process.idTipoLineaDesde"
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="id,nombre" listValues="id" attribs=""
						value="${ProcessForm.process.idTipoLineaDesde}"
						filter="id != ${ProcessForm.process.idLineaEmergencia}" /></td>
			</tr>
			<tr>
				<th width="185px">Linea de Credito Hasta:</th>
				<td width="515px"><asf:select name="process.idTipoLineaHasta"
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="id,nombre" listValues="id" attribs=""
						value="${ProcessForm.process.idTipoLineaHasta}"
						filter="id != ${ProcessForm.process.idLineaEmergencia}" /></td>
			</tr>
		</table>
	</div>
	<div id="fecovitaTipo" title="Tipo de Fecovita" style="display: none;">
		<table style="border: 2px solid #CCCCCC;" width="750">
			<tr>
				<th width="185px">Tipo Fecovita</th>
				<td width="515px"><asf:select name="process.fecovitaTipo"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.fecovitaTipo}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'Linea.Fecovita' order by codigo"
						nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;" /></td>
			</tr>

		</table>
	</div>


	<br>
	<html:submit value="Calcular" onclick="sendForm();" />
	<br>
</html:form>
<script language="javascript">
	addEvent("process.idObjetoi", "blur", cambiarLiquidacion);
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>

