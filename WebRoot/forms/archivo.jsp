<%@ page language="java"%>
<%@ page import="com.asf.security.BusinessPersistance"%>
<%@ page import="com.asf.security.SessionHandler"%>
<%@page import="com.civitas.hibernate.persona.*"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/ArchivoAction.do?do=save&entityName=${param.entityName}&entity.id=${archivoForm.entity.id}"
	enctype="multipart/form-data">
	

	
	<html:hidden property="filter" value="true" />
	<bean:define id="filter" name="paramValue" scope="session"
		type="Object[]" />
	<bean:define id="idpersona" value="${paramValue[0]}" />
	<%
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Persona pers = (Persona) bp.getById(Persona.class, new Long(idpersona));
	%>

	<table border="0">
		<tr>
			<td colspan="2">
				<html:errors/>	
			</td>
		</tr>
		<tr>
			<th>Persona:</th>
			<td><html:text property="entity.idpersona"
					value="${paramValue[0]}" size="3" readonly="true" />&nbsp;<b><%=pers.getNomb12().toUpperCase()%></b>
			</td>
		</tr>
		<tr>
			<th>Fecha de Vigencia Desde:</th>
			<td><asf:calendar property="archivoForm.entity.vigenciaDesdeStr"
					attribs="size=\"55\"" /></td>
		</tr>
		<tr>
			<th>Fecha de Vigencia Hasta:</th>
			<td><asf:calendar property="archivoForm.entity.vigenciaHastaStr"
					attribs="size=\"55\"" /></td>
		</tr>
		<tr>
			<th>Descripción:</th>
			<td><html:textarea property="entity.descripcion"
					styleId="descripcion" cols="50" rows="4"
					onkeypress="caracteres(this,255,event)" /></td>
		</tr>
		<tr>
			<th colspan="1">Tipo de Documento</th>
			<td colspan="3"><asf:select2 name="archivoForm"			
						property="entity.archivoRepo.idTipoDeArchivo"
						value="${archivoForm.entity.archivoRepo.idTipoDeArchivo}"
						descriptorLabels=" Nombre" entityProperty="id"
						descriptorProperties="nombre, descripcion" entityName="TipoDeArchivo"
						where2=" padre_id IS NOT NULL "
						accessProperty="id" />
			</td>
		</tr>
		
		<tr>
			<th>Documento:</th>
			<td colspan="3"><html:file property="theFile" /></td>

		</tr>
		<logic:present parameter="load">
			
			<tr>
				<th>Descarga:</th>
				<td colspan="3"><html:button
						onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoAction.do?do=descargar&id=${archivoForm.entity.id}')"
						property="">Bajar Documento</html:button></td>
			</tr>
		</logic:present>
	</table>
	<asf:security action="/actions/ArchivoAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>

<script language='javascript'>
	Event.observe(window, 'load', function() {
		this.name = "Documentacion";
		parent.push(this);
	});
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
