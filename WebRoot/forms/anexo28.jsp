<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript">

function exportarTexto() {
	$('action').value = 'exportarTexto';
	return true;
}

function buscar() {
	$('action').value = 'buscar';
	return true;
}

</script>

<div class="title">Anexo 28</div>
<html:form
	action="/actions/process.do?do=process&processName=Anexo28"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="exportarXls" styleId="action"/>
	<table>
		<tr>
			<th>Fecha desde</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th>Fecha hasta</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Listado en pantalla" onclick="buscar();"></html:submit>
				<html:submit value="Exportar XLS"></html:submit>
				<html:submit value="Exportar TXT" onclick="exportarTexto();"></html:submit>
			</th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.beans">
	<div class="grilla">
		<table>
			<thead>
				<tr>
					<th rowspan="3">Codigo Contable</th>
					<th rowspan="3">Detalle</th>
					<th rowspan="3">Saldo segun contabilidad al ${ProcessForm.process.fechaDesdeStr}</th>
					<th rowspan="3">Desembolsado</th>
					<th colspan="5">Deudores Corrientes</th>
					<th colspan="5">Deudores Morosos</th>
					<th colspan="5">Deudores Gestion Judicial</th>
					<th rowspan="3">Saldo segun contabilidad al ${ProcessForm.process.fechaHastaStr}</th>
				</tr>
				<tr>
					<th rowspan="2">Saldo al ${ProcessForm.process.fechaDesdeStr}</th>
					<th colspan="2">Recupero</th>
					<th rowspan="2">Ajuste</th>
					<th rowspan="2">Saldo al ${ProcessForm.process.fechaHastaStr}</th>
					<th rowspan="2">Saldo al ${ProcessForm.process.fechaDesdeStr}</th>
					<th colspan="2">Recupero</th>
					<th rowspan="2">Ajuste</th>
					<th rowspan="2">Saldo al ${ProcessForm.process.fechaHastaStr}</th>
					<th rowspan="2">Saldo al ${ProcessForm.process.fechaDesdeStr}</th>
					<th colspan="2">Recupero</th>
					<th rowspan="2">Ajuste</th>
					<th rowspan="2">Saldo al ${ProcessForm.process.fechaHastaStr}</th>
				</tr>
				<tr>
					<th>%</th>
					<th>Pesos</th>
					<th>%</th>
					<th>Pesos</th>
					<th>%</th>
					<th>Pesos</th>
				</tr>
			</thead>
			<tbody>
				<logic:iterate id="bean" name="ProcessForm" property="process.beans">
				<tr>
					<td>${bean.cuentaContable}</td>
					<td>${bean.linea}</td>
					<td style="text-align: right">${bean.saldoInicialStr}</td>
					<td style="text-align: right">${bean.desembolsadoStr}</td>
					<td style="text-align: right">${bean.corriente.saldoInicialStr}</td>
					<td style="text-align: right">${bean.corriente.porcentajeRecuperoStr}</td>
					<td style="text-align: right">${bean.corriente.recuperoStr}</td>
					<td style="text-align: right">${bean.corriente.ajustesStr}</td>
					<td style="text-align: right">${bean.corriente.saldoFinalStr}</td>
					<td style="text-align: right">${bean.morosos.saldoInicialStr}</td>
					<td style="text-align: right">${bean.morosos.porcentajeRecuperoStr}</td>
					<td style="text-align: right">${bean.morosos.recuperoStr}</td>
					<td style="text-align: right">${bean.morosos.ajustesStr}</td>
					<td style="text-align: right">${bean.morosos.saldoFinalStr}</td>
					<td style="text-align: right">${bean.judicial.saldoInicialStr}</td>
					<td style="text-align: right">${bean.judicial.porcentajeRecuperoStr}</td>
					<td style="text-align: right">${bean.judicial.recuperoStr}</td>
					<td style="text-align: right">${bean.judicial.ajustesStr}</td>
					<td style="text-align: right">${bean.judicial.saldoFinalStr}</td>
					<td style="text-align: right">${bean.saldoFinalStr}</td>
				</tr>
				</logic:iterate>
			</tbody>
		</table>
	</div>
	</logic:notEmpty>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>