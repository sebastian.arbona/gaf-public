<%@ page language="java"%>
<%@ page import="com.nirven.creditos.hibernate.FuncionPersonaResolucion"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/links.css"
	type="text/css">

<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/functions.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<script language="JavaScript" type="text/javascript">
$j = jQuery.noConflict();
	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto ){
    	var urlDinamica = "";
    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;
		return urlDinamica;
  	}
  	
  	function mostrarCampos(){
  		var indice = $('process.tipoProceso').selectedIndex;
		if(indice == -1)
			var texto = $('process.tipoProceso').options[1].innerHTML;
		else
			var texto = $('process.tipoProceso').options[indice].innerHTML;
		$j.ajax( {url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerCamposTipoProceso&nombreTipoProceso=" + texto,																										
				 success : function(data) {
					 var myObject = eval('('+data+')');
					 var arr = myObject.split("-");
					 for(var i = 0; i < arr.length; i++) {
						 var arrAtributos = arr[i].split(":");
						 var x = '#'+arrAtributos[0];
						 if(arrAtributos[1] == 'false'){
							 $j(x).hide();
	           			 }else{
	           				 $j(x).show();
	           			 }
					 }
					}
			});
}	
</script>

<body class=" yui-skin-sam">
	<div class="title">Nueva Instancia Judicial</div>
	<br>
	<br>
	<html:form
		action="/actions/process.do?do=process&processName=${param.processName}"
		styleId="oForm" enctype="multipart/form-data">
		<html:hidden name="ProcessForm" property="process.accion"
			styleId="accion" />
		<html:hidden property="process.cadenaPersonaSeleccionada"
			styleId="cadenaPersonaSeleccionadaInput" />
		<html:hidden name="ProcessForm"
			property="process.idSubTipoResolucion2"
			styleId="idSubTipoResolucion2" />
		<html:hidden name="ProcessForm"
			property="process.procesoResolucion.idTipoProceso"
			styleId="idTipoProceso2" />
		<html:hidden name="ProcessForm" property="process.sid" />
		<html:hidden name="ProcessForm"
			property="process.funcionPersonaResolucionQuitar"
			styleId="funcionPersonaResolucionQuitar" />
		<html:hidden name="ProcessForm" property="process.resolucion.id" />
		<html:hidden name="ProcessForm"
			property="process.procesoResolucion.id" />
		<html:hidden name="ProcessForm"
			property="process.procesoResolucion.tipoInstancia" />
		<html:hidden property="process.idSolicitud" styleId="idSolicitud" />
		<html:hidden property="process.funcionPersonResolucion.idPersona"
			styleId="idpersona" />
		<html:hidden property="process.notificacionPesonaInstancia.idPersona"
			styleId="idpersonaNotificacion" />
		<html:hidden name="ProcessForm"
			property="process.notificacionPersonaInstanciaQuitar"
			styleId="notificacionPersonaInstanciaQuitar" />
		<html:hidden property="process.destinatarioMail"
			styleId="idDestinatarioMail" />
		<html:hidden property="process.idproyectoPop" />

		<div style="width: 70%" align="left">
			<html:errors />
		</div>

		<table border="0" class="tabla" id="acarreo1">
			<tr>
				<th>Nro de Resolución:</th>
				<td><asf:text name="ProcessForm"
						property="process.resolucion.numero" type="Text" maxlength="30"
						readonly="true" id="numero" attribs="size=\"60\""
						value="${ProcessForm.process.procesoResolucion.resolucion.numero}" />
				</td>
			</tr>
			<tr>
				<th>Instancia:</th>
				<td><asf:select
						entityName="com.nirven.creditos.hibernate.TipoProceso"
						attribs="class='idTipoProceso;'onchange='mostrarCampos();'"
						listCaption="getNombreTipoProceso" listValues="getId"
						name="process.tipoProceso" style="width:330px"
						value="${ProcessForm.process.procesoResolucion.tipoProceso.id}" />
				</td>
			</tr>
			<tr>
				<th>Tipo de proceso:</th>
				<td><asf:select name="process.procesoResolucion.subTipoProceso"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.procesoResolucion.subTipoProceso}"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria = 'subTipoProceso'  order by codigo"
						style="width:330px" /></td>
			</tr>
			<tr id="tipoRegistroDiv">
				<th>Tipo Registro:</th>
				<td><asf:select name="process.procesoResolucion.tipoRegistro"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria='gestionjudicial.tiporegistro' order by codigo"
						style="width:330px"
						value="${ProcessForm.process.procesoResolucion.tipoRegistro}" /></td>
			</tr>
			<tr id="tipoEmbargoDiv">
				<th>Tipo Embargo:</th>
				<td><asf:select name="process.procesoResolucion.tipoEmbargo"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria='gestionjudicial.tipoembargo' order by codigo"
						style="width:330px"
						value="${ProcessForm.process.procesoResolucion.tipoEmbargo}" /></td>
			</tr>

			<tr id="tipoMedidaDiv">
				<th>Tipo Medida:</th>
				<td><asf:text
						property="ProcessForm.process.procesoResolucion.tipoMedida"
						type="Text" maxlength="30" id="tipoMedida" attribs="size=\"60\"" />
				</td>
			</tr>
			<tr id="expedienteDiv">
				<th>Número de Expediente Judicial:</th>
				<td><asf:text
						property="ProcessForm.process.procesoResolucion.expediente"
						type="Text" maxlength="15" id="expediente" attribs="size=\"60\""
						value="${ProcessForm.process.procesoResolucion.expediente}" /></td>
			</tr>
			<tr id="caratulaDiv">
				<th>Caratula:</th>
				<td><asf:text
						property="ProcessForm.process.procesoResolucion.caratula"
						type="Text" maxlength="100" id="caratula" attribs="size=\"60\"" />
				</td>
			</tr>
			<tr id="nombreDiv">
				<th>Nombre Excepción:</th>
				<td><asf:text
						property="ProcessForm.process.procesoResolucion.nombre"
						type="Text" maxlength="30" id="nombre" attribs="size=\"60\"" /></td>
			</tr>
			<tr id="camaraDiv">
				<th>Cámara:</th>
				<td><asf:select name="process.procesoResolucion.camara"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria='gestionjudicial.camara' order by codigo"
						style="width:330px"
						value="${ProcessForm.process.procesoResolucion.camara}" /></td>
			</tr>
			<tr id="tipoTribunalDiv">
				<th>Tipo de Tribunal:</th>
				<td><asf:select name="process.idTipoTribunal"
						entityName="com.nirven.creditos.hibernate.TipoTribunal"
						listCaption="getNombre" listValues="getId" style="width:330px"
						value="${ProcessForm.process.procesoResolucion.tipoTribunal.id}" />
				</td>
			</tr>
			<tr id="juzgadoDiv">
				<th>Número de Tribunal:</th>
				<td><asf:lselect property="process.idTribunal"
						listCaption="nombre" listValue="id"
						value="${ProcessForm.process.procesoResolucion.tribunal.id}"
						entityName="Tribunal" linkFK="tipoTribunal.id"
						linkName="process.idTipoTribunal" orderBy="id" nullValue="false" />
				</td>
			</tr>
			<tr id="circunscripcionDiv">
				<th>Circunscripción:</th>
				<td><asf:select
						name="process.procesoResolucion.circunscripcion"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria='gestionjudicial.circunscripcion' order by codigo"
						style="width:330px"
						value="${ProcessForm.process.procesoResolucion.circunscripcion}" />
				</td>
			</tr>

			<tr id="montoDiv">
				<th>Capital Reclamado:</th>
				<td><asf:text name="ProcessForm"
						property="process.procesoResolucion.montoStr" type="decimal"
						maxlength="12" id="monto" attribs="size=\"60\"" /></td>
			</tr>
			<tr>
				<th>Moneda</th>
				<td><asf:select name="process.procesoResolucion.idMoneda"
						entityName="com.asf.gaf.hibernate.Moneda"
						listCaption="getAbreviatura" listValues="getId"
						value="ProcessForm.process.procesoResolucion.idMoneda" /></td>
			</tr>
			<tr>
				<th>Fecha Inicio:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaInicioStr"
						attribs="class='fechaInicioStr' size=\"60\"" /></td>
			</tr>
			<tr>
				<th>Fecha Vencimiento:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaVencimientoStr"
						attribs="class='fechaVencimientoStr' size=\"60\"" /></td>
			</tr>
			<tr id="fechaPresentacionDiv">
				<th>Fecha Presentanción:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaPresentacionStr"
						attribs="class='fechaPresentacionStr' size=\"60\"" /></td>
			</tr>
			<tr id="fechaSentenciaDiv">
				<th>Fecha Sentencia:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaSentenciaStr"
						attribs="class='fechaSentenciaStr' size=\"60\"" /></td>
			</tr>
			<tr id="fechaEmbargoDiv">
				<th>Fecha Embargo:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaEmbargoStr"
						attribs="class='fechaEmbargoStr' size=\"60\"" /></td>
			</tr>
			<tr id="fechaCancelacionDiv">
				<th>Fecha Cancelación:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaCancelacionStr"
						attribs="class='fechaCancelacionStr' size=\"60\"" /></td>
			</tr>
			<tr id="fechaFinDiv">
				<th>Fecha Fin:</th>
				<td><asf:calendar
						property="ProcessForm.process.procesoResolucion.fechaFinStr"
						attribs="class='fechaFinStr' size=\"60\"" /></td>
			</tr>
			<tr>
				<th>Observaciones:</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.procesoResolucion.observaciones" cols="80"
						rows="4" /></td>
			</tr>
			<tr id="plantillaMailDiv">
				<th>Seleccionar notificación mail:</th>
				<td><asf:select
						entityName="com.nirven.creditos.hibernate.ConfiguracionNotificacion"
						listCaption="denominacion" listValues="id" orderBy="denominacion"
						name="process.idCn" nullValue="true"
						nullText="&lt;&lt;Seleccione Notificación&gt;&gt;"
						value="${ProcessForm.process.idCn}" style="width:330px" /></td>
			</tr>
			<tr id="destinatarioMailDiv">
				<th>Destinatario del mail:</th>
				<td><html:checkbox property="process.destinatarioMail"
						onclick="mostrarDef();" styleId="defCheck"></html:checkbox><span
					id="def"></span></td>
			</tr>
		</table>
		<br />

		<div id="personaDiv">
			<div class="title">Administración de Personas</div>
			<br />
			<table>
				<tr>
					<th>Persona:</th>
					<td><asf:autocomplete name="" tipo="AJAX"
							idDiv="autocomplete_persona" idInput="idpersonaPop"
							classDiv="autocomplete"
							origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
							options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
							value=""
							attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'" />
						<span id="indicadorPersona" style="display: none"><img
							src="images/loader.gif" alt="Trabajando..." /></span> <input
						value="..."
						onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=idpersonaPop&amp;entityName=Persona&amp;title=Selección de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');"
						type="button">&nbsp; (Código-Apellido y Nombre-DNI) <a
						href="javascript:void(0);"
						onclick="popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaAction.do?do=newEntity&entityName=Persona&forward=PersonaAltaRapida');"><b>Registrar
								Persona</b></a></td>
				</tr>
				<tr>
					<th>Funcion Judicial:</th>
					<td><asf:select
							entityName="com.nirven.creditos.hibernate.FuncionJudicial"
							attribs="class='idFuncion;'" listCaption="getNombre"
							listValues="getId"
							name="process.funcionPersonResolucion.idFuncion"
							style="width:330px" /> <html:submit value="Agregar Funcion"
							onclick="agregar();" /></td>
				</tr>
			</table>
			<br />
			<display:table name="ProcessForm"
				property="process.funcionesPersonasResolucion" export="false"
				id="reportTable">

				<display:column title="Persona" property="persona.nomb12" />
				<display:column title="Funcion" property="funcion.nombre" />
				<display:column title="Eliminar">
					<a onclick="quitarPersona(${reportTable_rowNum});">Eliminar</a>
				</display:column>
			</display:table>
		</div>
		<div id="NotificacionDiv">
			<div class="title">Administración de Notificaciones</div>
			<br />
			<table width="54%">
				<tr>
					<td align="right" width="40%">Cantidad de valores
						seleccionados:</td>
					<td width="10%" align="left" id="cantidadSeleccionados">${seleccionados}</td>
				</tr>
			</table>
			<br />
			<logic:empty name="ProcessForm"
				property="process.funcionesPersonasResolucion">
				<Strong>No existen personas vinculadas a una Instancia de
					Alta - </Strong>
			</logic:empty>

			<display:table name="ProcessForm"
				property="process.funcionesPersonasResolucion" export="false"
				id="reportTable">
				<display:column title="Selec." align="center" media="html">
					<input type="checkbox" name="persona-${reportTable.id}"
						value="${reportTable.id}" class="chkSelect"
						id="chk${reportTable.id}" />
				</display:column>
				<display:column title="Persona" property="persona.nomb12" />
				<display:column title="Funcion" property="funcion.nombre" />
				<display:column title="Fecha Última Notificación"
					property="fechaNotificacionInstancia" />
			</display:table>
			<br /> <br />
			<table>
				<tr>
					<th>Fecha Notificación:</th>
					<td><asf:calendar
							property="ProcessForm.process.notificacionPesonaInstancia.fechaStr"
							attribs="class='fechaNotificacionStr' size=\"60\"" /></td>
				</tr>
				<tr>
					<th>Observaciones:</th>
					<td colspan="3"><html:textarea styleId="detalleNotificacion"
							onkeypress="caracteres(this,400,event)"
							property="process.notificacionPesonaInstancia.detalle" cols="60"
							rows="4" /></td>
				</tr>
			</table>
		</div>

		<div class="title">Administración de Archivos</div>
		<br />
		<div class="grilla">
			<table>
				<tr>
					<th><span style="" class="required">(*)</span>&nbspDocumento:</th>
					<td colspan="3"><br> <html:file name="ProcessForm"
							property="process.theFile" /> <br></td>
				</tr>
			</table>
			<br>
			<display:table name="ProcessForm" property="process.archivos"
				export="false" id="reportTable">

				<display:column title="Nombre" property="nombre" />
				<display:column title="Descargar">
					<html:button property="" value="Bajar Documento"
						onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&tipo=documentoJudicial&id=${reportTable.id}')" />
				</display:column>
				<asf:security action="/actions/process.do"
					access="do=process&entityName=${param.entityName}">
					<display:column media="html" honClick="return confirmDelete();"
						title="Eliminar"
						href="${pageContext.request.contextPath}/actions/process.do?processName=ResolucionJudicialProcess&do=process&process.accion=eliminarArchivo&process.procesoResolucion.id=${ProcessForm.process.procesoResolucion.id}"
						paramId="process.idArchivo" paramProperty="id">
						<bean:message key="abm.button.delete" />
					</display:column>
				</asf:security>
			</display:table>
			<div style="margin-top: 10px">
				<input type="button" value="Guardar Instancia" onclick="guardar();" />
				<input type="button" value="Cancelar" onclick="cancelar();" />
			</div>
		</div>
	</html:form>

	<script language="JavaScript" type="text/javascript">
	var personaSeleccionada = ["-1"];
	$j(document).ready(function($) {	
						var seleccionados=0;
						mostrarDef();
						mostrarTipoProceso();
						mostrarCampos();
						$j('#process\\.procesoResolucion\\.tipoEmbargo option[value="${ProcessForm.process.procesoResolucion.tipoEmbargo}"]').attr('selected', 'selected');
						$j('#process\\.procesoResolucion\\.tipoRegistro option[value="${ProcessForm.process.procesoResolucion.tipoRegistro}"]').attr('selected', 'selected');
						
		$j('.chkSelect').click(function(){
		var id = this.id.replace('chk', '');
		
		if($j("#chk"+id).attr("checked")) {
				seleccionados++;
				personaSeleccionada[id] = id;
		} else {
				personaSeleccionada = jQuery.grep(personaSeleccionada, function(value) {
  					return value != id;
  				});
				seleccionados--;
		}
		$j('#cantidadSeleccionados').html(seleccionados);
	});
						
});
				
				
function mostrarTipoProceso() {
	combo = document.getElementById('process.tipoProceso');
	limpiarCombo(combo);
	$j.ajax({
			async:false, 
			url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerTipoProceso&idSubTipoResolucion=" + ${ProcessForm.process.procesoResolucion.resolucion.tipoResolucion.id},																													
			success : function(data) {
				var myObject = eval('('+data+')');
				for(var i = 0; i < myObject.length; i++ ){
					combo.options[combo.length] = new Option(myObject[i].nombreTipoProceso, myObject[i].id);
				}
				var x = "${ProcessForm.process.accion}";
				if(x != "nuevaInstancia"){
					$j('#process\\.tipoProceso option[value="${ProcessForm.process.tipoProceso}"]').attr('selected', 'selected');
				}else{
					$j('#process\\.tipoProceso option[value="${ProcessForm.process.procesoResolucion.tipoProceso.id}"]').attr('selected', 'selected');
				}			
			}			
		});
}

</script>
	<script type="text/javascript">
var ctrlPersona = $('idpersonaPop');
var ctrlPersonaNotificacion = $('idpersonaPop');
var idpersona = $('idpersona');
			
function guardar() {
	var idPersonaLong = trim(ctrlPersona.value.split("-")[0]);
	idpersona.value = idPersonaLong;
	accion.value = 'guardarInstancia';
	if($j("#defCheck").is(':checked')) {
		idDestinatarioMail.value = true;
	}else{
		idDestinatarioMail.value = false;
	}
	var form = $('oForm');
	var indice = $('process.tipoProceso').selectedIndex;
	var idSub = $('process.tipoProceso').options[indice].value;
	idTipoProceso2.value = idSub;
	
	//Datos necesarios para obtener los id de las personas seleccionadas
	var cadenaPersonaSeleccionada = "";
	for(var i = 0; i<personaSeleccionada.length;i++){
		if(personaSeleccionada[i] != null && personaSeleccionada[i] != -1){
			if(cadenaPersonaSeleccionada == ""){
				cadenaPersonaSeleccionada = personaSeleccionada[i];
			}else{
				cadenaPersonaSeleccionada = cadenaPersonaSeleccionada + "-" + personaSeleccionada[i];
			}
		}
	}

	cadenaPersonaSeleccionadaInput.value = cadenaPersonaSeleccionada;
	form.submit();
}

function cancelar(){
	var idPersonaLong = trim(ctrlPersona.value.split("-")[0]);
	idpersona.value = idPersonaLong;
	accion.value = 'listarInstancias';	
	var form = $('oForm');
	form.submit();
}

function limpiarCombo(combo){
	while(combo.length > 0){
		combo.remove(combo.length-1);
	}
}

function agregar(){
	var idPersonaLong = trim(ctrlPersona.value.split("-")[0]);
	idpersona.value = idPersonaLong;
	accion.value = 'persona';
	return true;
}

function quitarPersona(i) {
	var idPersonaLong = trim(ctrlPersonaNotificacion.value.split("-")[0]);
	idpersona.value = idPersonaLong;
	accion.value = 'quitarPersona';
	var indice = $('funcionPersonaResolucionQuitar');
	indice.value = i;
	var form = $('oForm');
	form.submit();
	return true;
}

function quitarPersonaNotificacion(i) {
	var idPersonaLong = trim(ctrlPersona.value.split("-")[0]);
	idpersonaNotificacion.value = idPersonaLong;
	accion.value = 'quitarPersonaNotificacion';
	var indice = $('notificacionPersonaInstanciaQuitar');
	indice.value = i;
	var form = $('oForm');
	form.submit();
	return true;
}

function mostrarDef(){
	if($j("#defCheck").is(':checked')) {
		document.getElementById("def").innerHTML='<STRONG>'+'ABOGADO'+'</STRONG>';
    } else {  
        document.getElementById("def").innerHTML='<STRONG>'+'ABOGADO Y TITULAR'+'</STRONG>';            
    }  
}

</script>
	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>