<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="title">Reimputaci&oacute;n de Pagos y Cr&eacute;ditos</div>
<html:form
	action="/actions/process.do?do=process&processName=Reimputacion"
	styleId="ProcessForm">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="seleccionarCredito"
		styleId="action" />
	<html:errors />
	<table>
		<tr>
			<th>Recaudacion</th>
			<th>Importe</th>
		</tr>
		<logic:iterate id="rs" name="ProcessForm"
			property="process.seleccionadas">
			<tr>
				<td>${rs.recaudacion}</td>
				<td>${rs.importeStr}</td>
			</tr>
		</logic:iterate>
		<tr>
			<td>Total</td>
			<td>${ProcessForm.process.totalRecaudacionesStr}</td>
		</tr>
		<tr>
			<th>Proyecto</th>
			<td><html:text property="process.numeroAtencion" /></td>
		</tr>
		<tr>
			<th colspan="2"><html:submit value="Buscar Proyecto" /></th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.movpagos">
		<h5 style="margin-top: 15px">Pagos a Reimputar</h5>
		<div class="grilla">
			<display:table name="ProcessForm" property="process.movpagos"
				export="true" id="reportTable" width="100%"
				requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process">
				<display:column title="Fecha Recaudacion"
					property="ctacte.fechaProceso"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Banco - Cuenta Depósito" property="infoBanco" />
				<display:column title="Importe" property="importe"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
				<display:column title="Fecha Pago" property="ctacte.fechaGeneracion"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Estado">
					<bean:message key="${reportTable.estadoReimputacion}" />
				</display:column>
			</display:table>
			<div>
				<div style="float: right">
					<html:submit value="Siguiente" onclick="siguiente();" />
				</div>
			</div>
		</div>
	</logic:notEmpty>
</html:form>

<script type="text/javascript">
	var action = $('action');

	function siguiente() {
		action.value = 'listarPreDesaplicacion';
	}
</script>