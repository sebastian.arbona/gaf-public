<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@page import="com.asf.cred.business.ProgressStatus"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>
<script>
    function importar(){
    	window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
					"width=450, height=180");       	      
       	return true;
    }

    function eliminar(){
        var oForm = $('oForm');
        oForm.action = '${pageContext.request.contextPath}/actions/importacionCobranza.do?do=eliminar'; 
        return true;
    }

    function reporte(){
        popUpBig( "${pageContext.request.contextPath}/actions/process.do?do=process&processName=ReporteErrorImportacion&process.nroreporte=<bean:write name="ImportacionForm" property="nroReporte"/>");
    }
</script>

<html:form action="/actions/importacionCobranza.do?do=importarCobranza" method="post" enctype="multipart/form-data" styleId="oForm">
	<html:hidden property="idCaratula" value="${ImportacionForm.idCaratula}"/>
	<html:hidden property="idDetalleArchivo" value="${ImportacionForm.idDetalleArchivo}"/>
	<table border="0" align="left">
		<tr>
			<th>
				Seleccione Archivo:
			</th>
			<td>
				<html:file name="ImportacionForm" property="file" styleId="archivo"
					size="50" disabled="${ImportacionForm.deshabilitado}" />
			</td>
		</tr>
	</table>
	<br/>
	<br/>
	<br/>
	<logic:notEmpty name="ImportacionForm" property="resumen">
		<div style="text-align: left; font-size: 12px;">${ImportacionForm.resumen}</div>
	</logic:notEmpty>
	<div style="width: 70%" align="center">
		<html:messages id="mensajes"/>
	</div>
	<html:errors />
	<logic:notEmpty name="ImportacionForm" property="nroReporte">
		<a href="javascript://" onclick="return reporte();">Errores de
			Importación Nro: <bean:write name="ImportacionForm"
				property="nroReporte" /><a /><br>
	</logic:notEmpty>
	<br/>
	<br/>
	<div align="left">
		<html:submit onclick="return importar();" disabled="${ImportacionForm.deshabilitado}">Importar</html:submit>
	</div>
<%--	<html:submit onclick="return eliminar();">Eliminar</html:submit>--%>
</html:form>
${ImportacionForm.script}
<%ProgressStatus.onProcess = false;%>