<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<script type="text/javascript" language="javascript">
	var nav4 = window.Event ? true : false;
	var __VENCIDO = "VENCIDO";
	var __FCOBRO = "FCOBRO";
	var sTotal = new Number(0);
	var boleto = '';
	var __RETORNO = false;
	var boletosFecovita;
	function modificarRecaudacion(tipo, importe) {
		var irRecaud = $('recauId');
		var url = "";
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=ajaxModificarRecaudacion";
		url += "&idrecaudacion=" + irRecaud.value;
		url += "&importeSaldo=" + tipo + "" + importe;
		retrieveURL(url, modificarRecaudacionResponse);
	}

	function modificarRecaudacionResponse(responseText) {
		selectAll();
		$('processForm').submit();
	}

	function modificarRecaudacionProcesamiento(tipo, importe) {
		var irRecaud = $('recauId');
		var url = "";
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=ajaxModificarRecaudacion";
		url += "&idrecaudacion=" + irRecaud.value;
		url += "&importeSaldo=" + tipo + "" + importe;
		retrieveURL(url, modificarRecaudacionResponseProcesamiento);
	}

	function modificarRecaudacionResponseProcesamiento(responseText) {
		selectAll();
		$('processForm').submit();
	}

	function agregar(val) {
		if (val == '') {
			return false;
		} else {
			var nb = getElem("nb");
			var cmb = getElem("lis");
			var fechaCobranza = $('idFechaCobranza').value;
			var arr = val.replace("/", "-").split("-");
			val = trim(nb.value);
			var nroProyecto = "0";
			if (val.length == 44 && arr.length == 1) { //C�digo de Barras
				// revisar si es numero de proyecto o de boleto
				codi = val.substring(7, 15);
				digito = val.substring(15, 16);
				periodo = val.substring(3, 7);
				nroProyecto = 0; // substring
			} else if (arr.length == 1) {
				nroProyecto = val; // se ingresa directo el numero de proyecto
			} else {
				if (arr.length != 3) {
					alert('El N�MERO DE BOLETO ingresado no es v�lido. Por favor, intente nuevamente.');
					$('nb').focus();
					return false;
				}

				codi = arr[0];
				digito = arr[1];
				periodo = arr[2];
			}

			if (nroProyecto == "0") {
				url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&listValues=id.periodoBoleto;id.numeroBoleto;id.verificadorBoleto;fechaVencimiento;importe&entityName=Boleto&filter="
						+ "id.periodoBoleto='"
						+ periodo
						+ "' AND id.verificadorBoleto='"
						+ digito
						+ "' AND id.numeroBoleto='" + codi + "'";
			} else {
				url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=generarBoleto&nroProyecto="
						+ nroProyecto + "&fechaCobranza=" + fechaCobranza;
				/*
				if($('metodoCarga').value == '3'){
					url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=generarBoleto&nroProyecto="
						+ nroProyecto + "&fechaCobranza=" + fechaCobranza + "&deuda=" + $('importeBoleto').value + "&metodoCarga=" + $('metodoCarga').value;
				}else{
					url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=generarBoleto&nroProyecto="
						+ nroProyecto + "&fechaCobranza=" + fechaCobranza;
				}*/

			}
			retrieveURL(url, boletoResponse);
			//nb.value='';
			//nb.focus();

		}
	}
	function boletoResponse(responseText) {
		__RETORNO = true;
		if (responseText == "<Objects></Objects>") {
			alert('El N�MERO DE BOLETO ingresado no es v�lido. Por favor, intente nuevamente.');
			$('nb').focus();
			return;
		} else if (responseText.startsWith("<error>")) {
			var error = parseXml(responseText).error;
			alert(error);
			$('nb').focus();
			return;
		}

		var objetos = parseXml(responseText);
		boleto = objetos.Objects.Boleto;
		getElem('titular').value = objetos.Objects.Persona.NOMB_12;
		getElem('cuil').value = objetos.Objects.Persona.CUIL_12;
		validaBoleto();
	}

	function validaBoleto() {
		url2 = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&listValues=id.boleto.id;&entityName=Pagos&filter="
				+ "id.boleto.id=" + boleto.id;
		retrieveURL(url2, boletoResponse2);
	}

	function boletoResponse2(responseText) {

		if (responseText == "<Objects></Objects>") {
			var importe = boleto.importe;
			if (importe == __FCOBRO) {
				alert("La FECHA DE COBRANZA est� vac�a. Por favor, intente nuevamente.");
				return null;
			}
<%--	  getElem('importeBoleto').value = importe;--%>
	var importeTot = $('importe').value;
			var importeBol = importe;
			var imp = new Number(importeTot) + new Number(importeBol);
			if ($('metodoCarga').value != '2') {
				$('importe').value = imp;
				$('process.idmediopago').value = 41;
				var idMoneda = boleto.moneda;
				$('process.idMoneda').value = new Number(idMoneda);
			}

			//getElem('importeBoleto').focus();
			return;
		} else {
			alert("El N�MERO DE BOLETO ingresado ya fue registrado. Por favor, intente nuevamente.");
			return null;
		}

	}

	function boletoResponseImportado(responseText) {
		if (responseText == "<Objects></Objects>") {
			var importe = boleto.importe;
			if (importe == __FCOBRO) {
				alert("La FECHA DE COBRANZA est� vac�a. Por favor, intente nuevamente.");
				return null;
			}

			var importeTot = $('importe').value;
			var importeBol = importe;
			var imp = new Number(importeTot) + new Number(importeBol);
			$('importe').value = imp;
			$('process.idmediopago').value = 41;
			//getElem('importeBoleto').focus();

			return;

		} else {
			alert("El N�MERO DE BOLETO ingresado ya fue registrado. Por favor, intente nuevamente.");
			return null;
		}

	}

	function agregarBoletoManual() {
		var importe = getElem('importeBoleto').value;
		var metodoCarga = getElem('metodoCarga').value;
		var importeNumber = new Number(getElem('importeBoleto').value);
		if (importe == '' || importeNumber <= 0 || importeNumber == null
				|| importeNumber == "undefined" || isNaN(importeNumber)) {
			return;
		}
		if (boleto.id == "undefined") {
			alert("Debe ingresar un boleto previamente.");
			return;
		}
		var cmb = getElem("lis");
		var nro;
		for (var i = 0; i < cmb.length; i++) {
			nro = cmb.options[i].value.split("$")[1];
			nro = nro.split("-")[1];
			nro = nro.split(":")[1]
			if (boleto.nroProyecto == nro.replace(/^\s+/g, '').replace(/\s+$/g,
					'')) {
				return;
			}
		}
		var rec = $('recauId');
		if (rec.value != null && rec.value != "" && rec.value != 0
				&& metodoCarga == 2) {
			if (new Number(new Number($('recauSaldo').value) - importe) < new Number(
					0)) {
				alert("El importe a cobrar es superior al saldo de la recaudaci�n.");
				return;
			}
			$('recauSaldo').value = new Number(
					new Number($('recauSaldo').value) - importe).toFixed(2);
			var acobrar = $('aCobrar');
			var valor = new Number(acobrar.value);
			valor += new Number(importe);
			acobrar.value = new Number(valor).toFixed(2);
			var imp = $('aCobrar');
			imp.value = new Number(acobrar.value).toFixed(2);

			var impor = $('importe');
			impor.value = new Number(acobrar.value).toFixed(2);
		} else if (metodoCarga == 1 || metodoCarga == 0 || metodoCarga == 3) {
			var acobrar = $('aCobrar');
			var valor = new Number(acobrar.value);
			valor += new Number(importe);
			acobrar.value = new Number(valor).toFixed(2);
			var imp = $('aCobrar');
			imp.value = new Number(acobrar.value).toFixed(2);

			var impor = $('importe');
			impor.value = new Number(acobrar.value).toFixed(2);
			if (metodoCarga == 3) {
				$('recauSaldo').value = $('recauSaldo').value
						- $('aCobrar').value;
			}
		}
		var boletoAProcesar = boleto.id + ' $' + importe + ' - NRO. PROYECTO: '
				+ boleto.nroProyecto + ' - TITULAR: '
				+ getElem('titular').value + '  - CUIL/CUIT - '
				+ getElem('cuil').value;

		lista[lista.length] = boleto.id, boletoAProcesar;
		cargarItem(boleto.id, boletoAProcesar, importe, boleto,
				boleto.nroProyecto, getElem('titular').value,
				getElem('cuil').value);

		getElem('nb').value = '';
		getElem('importeBoleto').value = '';
		getElem('titular').value = '';
		getElem('cuil').value = '';
		getElem('nb').focus();
		proyectos[lista.length] = boleto.nroProyecto;
		return;
	}

	function agregarBoleto(titular, cuit, nroProyecto) {
		var importe = boleto.importe;
		if (boleto.id == "undefined") {
			alert("Debe ingresar un boleto previamente.");
			return;
		}
		var rec = $('recauId');
		if (rec.value != null && rec.value != "" && rec.value != 0) {
			if (new Number(new Number($('recauSaldo').value) - importe) < new Number(
					0)) {
				alert("El importe a cobrar es superior al saldo de la recaudaci�n.");
				return;
			}
			$('recauSaldo').value = new Number(
					new Number($('recauSaldo').value) - importe).toFixed(2);
			var acobrar = $('aCobrar');
			var valor = new Number(acobrar.value);
			valor += new Number(importe);
			acobrar.value = new Number(valor).toFixed(2);
			var imp = $('aCobrar');
			imp.value = new Number(acobrar.value).toFixed(2);
			var impor = $('importe');
			impor.value = new Number(acobrar.value).toFixed(2);
		}
		var boletoAProcesar = boleto.id + ' $' + importe + ' - NRO. PROYECTO: '
				+ nroProyecto + ' - TITULAR: ' + titular + '  - CUIL/CUIT - '
				+ cuit + ' EXPEDIENTE: ' + boleto.expediente + ' CUOTAS: '
				+ boleto.cuotas + ' DEUDA: ' + boleto.deuda + ' CAPITAL: '
				+ boleto.capital + ' COMPENSATORIO: ' + boleto.compensatorio
				+ ' MORATORIO: ' + boleto.moratorio + ' PUNITORIO: '
				+ boleto.punitorio + ' GASTOS: ' + boleto.gastos + ' MULTAS: '
				+ boleto.multas;
		lista[lista.length] = boleto.id, boletoAProcesar;
		cargarItemImportado(boleto.id, boletoAProcesar, importe, boleto,
				nroProyecto, titular, cuit);
		return;
	}

	function agregarBoletoFecovita() {
		var importeFec = boleto.importe;
		var rec = $('recauId');
		if (rec.value != null && rec.value != "" && rec.value != 0) {
			if (new Number(new Number($('recauSaldo').value) - importeFec) < new Number(
					0)) {
				return true;
			}
			$('recauSaldo').value = new Number(
					new Number($('recauSaldo').value) - importeFec).toFixed(2);
			var acobrar = $('aCobrar');
			var valor = new Number(acobrar.value);
			valor += new Number(importeFec);
			acobrar.value = new Number(valor).toFixed(2);
			var imp = $('aCobrar');
			imp.value = new Number(acobrar.value).toFixed(2);
			var impor = $('importe');
			impor.value = new Number(acobrar.value).toFixed(2);
		}
		var boletoAProcesar = boleto.id + ' $' + boleto.importe
				+ ' - NRO. PROYECTO: ' + boleto.nroProyecto + ' - TITULAR: '
				+ boleto.NOMB_12 + '  - CUIL/CUIT - ' + boleto.CUIL_12
				+ ' EXPEDIENTE: ' + boleto.expediente + ' CUOTAS: '
				+ boleto.cuotas + ' DEUDA: ' + boleto.deuda + ' CAPITAL: '
				+ boleto.capital + ' COMPENSATORIO: ' + boleto.compensatorio
				+ ' MORATORIO: ' + boleto.moratorio + ' PUNITORIO: '
				+ boleto.punitorio + ' GASTOS: ' + boleto.gastos + ' MULTAS: '
				+ boleto.multas;
		lista[lista.length] = boleto.id, boletoAProcesar;
		cargarItem(boleto.id, boletoAProcesar, importeFec, boleto,
				boleto.nroProyecto, boleto.NOMB_12, boleto.CUIL_12);
		return false;
	}

	function getDate(fecha) {
		var sFecha = fecha.split("-");
		var iAnio = 0;
		var iMes = 0;
		var iDia = 0;
		var iAux;

		if (sFecha.length <= 1)
			sFecha = fecha.split("/");

		if (sFecha == '' || sFecha.length <= 1)
			return null;

		iAnio = new Number(sFecha[0]);
		iMes = new Number(sFecha[1]) - 1;
		iDia = new Number(sFecha[2]);

		if (iDia >= 1900) {
			iAux = iAnio;
			iAnio = iDia;
			iDia = iAux;
		}

		return new Date(iAnio, iMes, iDia);
	}

	function cargarItem(key, value, importe, boleto, nroProyecto, titular, cuit) {
		var nb = getElem("nb");
		var cmb = getElem("lis");
		for (var i = 0; i < cmb.length; i++) {
			if (cmb.options[i].value == value) {
				alert("El N�MERO DE BOLETO elegido ya fue agregado. Por favor, intente nuevamente.");
				nb.value = '';
				nb.focus();
				return false;
			}
		}
		var opt = cmb.appendChild(document.createElement('option'));
		opt.appendChild(document.createTextNode(value));
		opt.setAttribute('value', value);

		var importeRedondeado = new Number(importe);
		importeRedondeado = Math.round(importeRedondeado * 100) / 100;
		sTotal += importeRedondeado; //calcula el importe total de los boletos ingresados..
		sTotal = Math.round(sTotal * 100) / 100;
		$('timp12').value = new Number(new Number($('timp12').value)
				+ importeRedondeado).toFixed(2); //actualiza el importe total de la caratula.
		$('tcomp12').value = new Number($('tcomp12').value) + new Number(1); //actualiza el nro total de comprobantes de la caratula.
		$('aCobrar').value = new Number(sTotal).toFixed(2);

		if (cmb.length >= 0) {
			getElem('bQuitar').disabled = false;
		}

		// llama al server para armar preaplicacion
		if ($('fecovitaTipo').value == null || $('fecovitaTipo').value == "") {
			consultarPreaplicacion(nroProyecto, importe,
					$('idFechaCobranza').value, $('idFechaCobranza').value);
		} else {
			consultarPreaplicacion(nroProyecto, importe,
					$('idFechaCobranza').value,
					$('process.fechaCalculoFecovitaStr').value);
		}
	}

	function consultarPreaplicacion(nroProyecto, importe, fechaCobranza,
			fechaCalculo) {
		var idMoneda = $('process.idMoneda').value;
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=consultarPreaplicacion"
				+ "&nroProyecto="
				+ nroProyecto
				+ "&fechaCobranza="
				+ fechaCobranza
				+ "&fechaCalculo="
				+ fechaCalculo
				+ "&importe="
				+ importe + "&idMoneda=" + idMoneda;
		retrieveURL2(url);
	}

	function retrieveURL2(url) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				cargarPreaplicacionResponse(this.responseText);
			}
		};
		xhttp.open("GET", url, true);
		xhttp.send();
	}

	function buscarSiguienteIndice() {
		var table = getElem("tabla");
		var siguiente = 0;
		for (var r = 1; r < table.rows.length; r++) {
			var row = table.rows[r];
			var content = row.cells[0].innerHTML;
			if (content != null && content != '') {
				siguiente++;
			}
		}
		return siguiente;
	}

	function cargarPreaplicacionResponse(responseText) {
		var table = getElem("tabla");
		var posicion = table.rows.length;
		var posicionPago = buscarSiguienteIndice(); // guardo la de la primera fila agregada, porque lleva el link QUITAR
		var fila = table.insertRow(posicion);

		var objetos = parseXml(responseText);
		var credito = objetos.Objects.Objetoi;

		var c = 0;
		var cell = fila.insertCell(c++);
		cell.innerHTML = credito.numeroAtencion;
		cell = fila.insertCell(c++);
		cell.innerHTML = credito.Persona.nombre;
		cell = fila.insertCell(c++);
		cell.innerHTML = credito.importePago;
		cell = fila.insertCell(c++);
		cell.innerHTML = credito.fechaPago;

		var cuotas = credito.Cuota;
		var length = cuotas.length;
		var cuota;

		if (cuotas.length == null) {
			length = 1;
			cuota = cuotas;
		}

		for (var i = 0; i < length; i++) {
			if (i > 0) {
				// a partir de la segunda cuota, agregar nueva fila
				posicion = table.rows.length;
				fila = table.insertRow(posicion);
				for (var caux = 0; caux < 4; caux++) { // 4 celdas vacias
					fila.insertCell(caux);
				}
			}

			c = 4;
			if (cuota == null) {
				cuota = cuotas[i];
			}
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.numero;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.aplicado;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.capital;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.compensatorio;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.moratorio;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.punitorio;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.gastos;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.gastosRec;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.multas;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.saldoPendiente;
			cell = fila.insertCell(c++);
			if (i == 0) {
				cell.innerHTML = "<a onclick=\"quitarTabla(" + (posicionPago)
						+ ");\">Quitar</a>";
			}

			cuota = null;
		}
	}

	function cargarItemImportado(key, value, importe, boleto, nroProyecto,
			titular, cuit) {
		var cmb = getElem("lis");
		for (var i = 0; i < cmb.length; i++) {
			if (cmb.options[i].value == value) {
				alert("El N�MERO DE BOLETO elegido ya fue agregado. Por favor, intente nuevamente.");
				return false;
			}
		}
		var opt = cmb.appendChild(document.createElement('option'));
		opt.appendChild(document.createTextNode(value));
		opt.setAttribute('value', value);

		var table = getElem("tabla")
		var posicion = table.rows.length;
		var fila = table.insertRow(posicion);
		cell = fila.insertCell(0);
		cell.innerHTML = nroProyecto;
		cell = fila.insertCell(1);
		cell.innerHTML = titular;
		cell = fila.insertCell(2);
		cell.innerHTML = cuit;
		cell = fila.insertCell(3);
		cell.innerHTML = boleto.expediente;
		cell = fila.insertCell(4);
		cell.innerHTML = boleto.cuotas;
		cell = fila.insertCell(5);
		cell.innerHTML = Math.round(new Number(importe) * 100) / 100;
		cell = fila.insertCell(6);
		cell.innerHTML = Math.round(new Number(boleto.deuda) * 100) / 100;
		cell = fila.insertCell(7);
		cell.innerHTML = Math.round(new Number(boleto.capital) * 100) / 100;
		cell = fila.insertCell(8);
		cell.innerHTML = Math.round(new Number(boleto.compensatorio) * 100) / 100;
		cell = fila.insertCell(9);
		cell.innerHTML = Math.round(new Number(boleto.moratorio) * 100) / 100;
		cell = fila.insertCell(10);
		cell.innerHTML = Math.round(new Number(boleto.punitorio) * 100) / 100;
		cell = fila.insertCell(11);
		cell.innerHTML = Math.round(new Number(boleto.gastos) * 100) / 100;
		cell = fila.insertCell(12);
		cell.innerHTML = Math.round(new Number(boleto.multas) * 100) / 100;
		cell = fila.insertCell(13);
		cell.innerHTML = Math.round((new Number(boleto.deuda) - new Number(
				importe)) * 100) / 100;
		cell = fila.insertCell(14);
		cell.innerHTML = "<a onclick=\"quitarTabla(" + (posicion - 1)
				+ ");\">Quitar</a>";

		var importeRedondeado = new Number(importe);
		importeRedondeado = Math.round(importeRedondeado * 100) / 100;
		sTotal += importeRedondeado; //calcula el importe total de los boletos ingresados..
		sTotal = Math.round(sTotal * 100) / 100;
		$('timp12').value = new Number(new Number($('timp12').value)
				+ importeRedondeado).toFixed(2); //actualiza el importe total de la caratula.
		$('tcomp12').value = new Number($('tcomp12').value) + new Number(1); //actualiza el nro total de comprobantes de la caratula.
		$('aCobrar').value = new Number(sTotal).toFixed(2);
		if (cmb.length >= 0) {
			getElem('bQuitar').disabled = false;
		}
	}

	function buscarFilaPago(pago) {
		var table = getElem("tabla");
		var pagoActual = 0;
		for (var r = 1; r < table.rows.length; r++) {
			var row = table.rows[r];
			var content = row.cells[0].innerHTML;
			if (content != null && content != '') {
				if (pagoActual == pago) {
					return r;
				}
				pagoActual++;
			}
		}
		return -1;
	}

	function quitarTabla(i) {
		var filaPago = buscarFilaPago(i);

		if (filaPago == -1) {
			return;
		}

		var table = getElem("tabla");
		var nroProy;
		do {
			table.deleteRow(filaPago);
			if (filaPago == table.rows.length) {
				break;
			}

			nroProy = table.rows[filaPago].cells[0].innerHTML;
		} while (nroProy == null || nroProy == '');

		// 	for (var r = filaPago; r < table.rows.length; r++) {
		// 		var row = table.rows[r];
		// 		var content = row.cells[0].innerHTML;
		// 		if (content == null || content == '') { // borro las filas de cuotas, hasta que encuentro el siguiente pago
		// 			table.deleteRow(r);
		// 		} else {
		// 			break;
		// 		}
		// 	}

		var pago = 0;
		for (var j = 1; j < table.rows.length; j++) {
			var cel = table.rows[j].cells[14];
			var content = table.rows[j].cells[0].innerHTML;
			if (content != null && content != '') {
				cel.innerHTML = "<a onclick=\"quitarTabla(" + pago
						+ ");\">Quitar</a>";
				pago++;
			}
		}

		var cmb = getElem("lis");
		cmb.selectedIndex = i;
		quitar();
		return false;
	}

	function quitar() {
		var nb = getElem("nb");
		var cmb = getElem("lis");
		var i = cmb.selectedIndex;
		var impo = cmb.options[i].value.split("$")[1];
		impo = impo.split("-")[0];

		if (navigator.appName != "Netscape")
			cmb.removeChild(cmb.children(i));
		else {
			cmb.options[i] = null;
		}

		var importeRedondeado = new Number(impo);
		importeRedondeado = Math.round(importeRedondeado * 100) / 100;
		sTotal -= importeRedondeado; //actualiza el importe total de los boletos ingresados..
		sTotal = Math.round(sTotal * 100) / 100;
		$('timp12').value = new Number(new Number($('timp12').value)
				- importeRedondeado).toFixed(2); //actualiza el importe total de la caratula.
		$('tcomp12').value = new Number($('tcomp12').value) - new Number(1); //actualiza el nro total de comprobantes de la caratula.
		$('aCobrar').value = new Number(sTotal).toFixed(2);
<%--    $('difTotal').value = new Number(0).toFixed(2);--%>
	if (cmb.length == 0) {
			nb.value = '';
			nb.focus();
			getElem('bQuitar').disabled = true;
		}
		var importeAgregado = new Number(importe.value);
		if (importeAgregado <= 0.0) {
			return;
		}
		importeAgregado -= importeRedondeado;
		importeAgregado = Math.round(importeAgregado * 100) / 100;
		importe.value = importeAgregado;
		if ($('metodoCarga').value == '2' || $('metodoCarga').value == '3') {
			saldo = new Number($('recauSaldo').value) + importeRedondeado;
			$('recauSaldo').value = new Number(saldo).toFixed(2);
		}
	}

	function quitarTodos() {
		tabla.deleteRow(2);
		var cmb = getElem("lis");
		var impo;
		var cantidad = cmb.length;
		var importeQuitado = sTotal;
		for (var i = 0; i < cantidad; i++) {
			impo = cmb.options[cmb.length - 1].value.split("$")[1];
			impo = impo.split("-")[0];
			if (navigator.appName != "Netscape")
				cmb.removeChild(cmb.children(cmb.length - 1));
			else {
				cmb.options[cmb.length - 1] = null;
			}
			var importeRedondeado = new Number(impo);
			importeRedondeado = Math.round(importeRedondeado * 100) / 100;
			sTotal -= importeRedondeado; //actualiza el importe total de los boletos ingresados..
			sTotal = Math.round(sTotal * 100) / 100;
			$('timp12').value = new Number(new Number($('timp12').value)
					- importeRedondeado).toFixed(2); //actualiza el importe total de la caratula.
			$('tcomp12').value = new Number($('tcomp12').value) - new Number(1); //actualiza el nro total de comprobantes de la caratula.
			$('aCobrar').value = new Number(sTotal).toFixed(2);
			saldo = new Number($('recauSaldo').value) + importeRedondeado;
			$('recauSaldo').value = new Number(saldo).toFixed(2);
		}
		var importeAgregado = new Number(importe.value);
		if (importeAgregado <= 0.0) {
			return;
		}
		importeAgregado -= importeQuitado;
		importeAgregado = Math.round(importeAgregado * 100) / 100;
		importe.value = importeAgregado;
	}

	function selectAll() {
		lista = getElem('lis');
		for (i = 0; i < lista.length; i++) {
			lista.options[i].selected = true;
		}
		return true;
	}

	function mostrarMedio(valor) {
		if (valor != null && valor != 0) {
			mostrar("mPago");
			mostrar("impo");
			var url = "";
			url += "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?";
			url += "do=ajaxQuery";
			url += "&entityName=Mediopago";
			url += "&listValues=entidademisora;nrocomprobante;femision;fvencimiento;cuotas;plan;autorizacion;tipomedio";
			url += "&filter=id=" + valor;
			retrieveURL(url, respuesta);
		} else {
			ocultar("mPago");
			ocultar("impo");
		}
	}

	function respuesta(responseText) {
		var medioPago = parseXml(responseText);
		medioPago = medioPago.Objects.Mediopago;

		medioPago.entidademisora == 1 ? $('entidad').style.display = ''
				: $('entidad').style.display = 'none';
		medioPago.nrocomprobante == 1 ? $('nro').style.display = ''
				: $('nro').style.display = 'none';
		medioPago.femision == 1 ? $('femi').style.display = ''
				: $('femi').style.display = 'none';
		medioPago.fvencimiento == 1 ? $('fvto').style.display = ''
				: $('fvto').style.display = 'none';
		medioPago.cuotas == 1 ? $('cuot').style.display = ''
				: $('cuot').style.display = 'none';
		medioPago.plan == 1 ? $('tplan').style.display = ''
				: $('tplan').style.display = 'none';
		medioPago.autorizacion == 1 ? $('autoriza').style.display = ''
				: $('autoriza').style.display = 'none';
		getElem("tipomedio").value = medioPago.tipomedio;
	}

	function cambiar(valor) {
		var moneda = $('process.idMoneda');
		var cotiz = $('cotizacion');
		var imp = $('importe');
		limpiarRecaudacion();
		if (valor == 0) {
			mostrar("tr_iframe");
			ocultar("comprobantesOcultos");
			ocultar("medioPago");
			imp.disabled = true;
			moneda.disabled = true;
			cotiz.disabled = true;
			ocultar("recauDatos_iframe");
			cargarPopUpMetodoAutomatico();
		} else if (valor == 1) {
			ocultar("tr_iframe");
			ocultar("comprobantesOcultos");
			mostrar("medioPago");
			mostrar("mPago");
			moneda.disabled = false;
			cotiz.disabled = false;
			imp.disabled = false;
			ocultar("recauDatos_iframe");
		} else if (valor == 2) {
			ocultar("tr_iframe");
			ocultar("comprobantesOcultos");
			mostrar("medioPago");
			mostrar("recauDatos_iframe");
			moneda.disabled = true;
			cotiz.disabled = true;
			mostrar("mPago");
			mostrar("impo");
			imp.disabled = true;
			cargarPopUp();
		}
	}

	function limpiarRecaudacion() {
		$('recauId').value = "0";
		$('recauFecha').value = "";
		$('recauImporte').value = "0.0";
		$('recauSaldo').value = "0.0";
	}

	var idRecaudacion;

	function cargarPopUp() {
		var url = "";
		url += "${pageContext.request.contextPath}/actions/process.do?do=process";
		url += "&processName=Recaudaciones";
		url += "&process.metodoCarga=2";
		url += "&process.fecha=${ProcessForm.process.fcob12}";
		url += "&process.idEnteRecaudador=${ProcessForm.process.caratula.banco.codiBa}";
		popUp(url);
	}

	function cargarPopUpMetodoAutomatico() {
		var url = "";
		url += "${pageContext.request.contextPath}/actions/process.do?do=process";
		url += "&processName=Recaudaciones";
		url += "&process.metodoCarga=0";
		url += "&process.fecha=${ProcessForm.process.fcob12}";
		url += "&process.idEnteRecaudador=${ProcessForm.process.caratula.banco.codiBa}";
		popUp(url);
	}

	function cargarPopUpPagoACuenta() {
		var url = "";
		url += "${pageContext.request.contextPath}/actions/process.do?do=process";
		url += "&processName=PagosACuenta";
		url += "&process.metodoCarga=3";
		url += "&process.fecha=${ProcessForm.process.fcob12}";
		url += "&process.idEnteRecaudador=${ProcessForm.process.caratula.banco.codiBa}";
		popUp(url);
	}
	function navegadorValido() {
		var esValidoNav = true;
		if (!isIE) {
			try {
				var version = navigator.userAgent;
				var mensaje = "";
				if (version.indexOf("2.0") > 0) {
					version = version.substring(version.indexOf("2.0"));
					if (version == "2.0") {
						mensaje += "Esta funcionalidad no est� disponible con Mozilla Firefox 2.0\n";
						mensaje += "Por favor, actualice su navegador a una versi�n posterior";
						alert(mensaje);
						esValidoNav = false;
					}
				}
			} catch (e) {
				esValidoNav = false;
			}
		}
		return esValidoNav;
	}
</script>
<div class="title">Procesar Recaudaci�n</div>
<bean:define id="lista" name="ProcessForm" property="process.lista"
	type="String[]" />

<bean:define id="impuesto" name="ProcessForm"
	property="process.impuesto" type="String[]" />

<html:form styleId="processForm"
	action="/actions/process.do?do=process&processName=${param.processName}"
	onsubmit="selectAll();">

	<html:hidden property="filter" value="true" />
	<html:hidden property="process.idcaratula" styleId="idcaratula" />
	<html:hidden property="process.lote" styleId="lote" />
	<html:hidden property="process.save" styleId="save" />
	<html:hidden property="process.metodoCarga" styleId="metodoCarga"
		value="${ProcessForm.process.metodoCarga}" />
	<html:hidden property="process.impoTotal"
		value="${ProcessForm.process.impoTotal}" styleId="impoTotal" />
	<html:hidden property="process.recaudId" styleId="recaudId"
		value="${ProcessForm.process.recaudId}" />
	<html:hidden property="process.movIngresosVariosId"
		styleId="movIngresosVariosId"
		value="${ProcessForm.process.movIngresosVariosId}" />
	<html:hidden property="process.caratula.fecovita" styleId="fecovita" />
	<html:hidden property="process.caratula.fecovitaTipo"
		styleId="fecovitaTipo" />
	<html:hidden property="process.caratula.fechaCobranzaStr"
		styleId="idFechaCobranza" />
	<input type="hidden" id="idDetalleArchivo" />
	<%-- Detalle Caratula --%>
	<table border="0">
		<tr>
			<th>Fecha:</th>
			<td><html:text property="process.fenv12Str" readonly="true" />
			</td>
			<th>Nro Caja:</th>
			<td><html:text property="process.codi12" readonly="true" /></td>
		</tr>
		<logic:notEqual name="ProcessForm" property="process.metodoCarga"
			value="0">
			<tr>
				<th>Ente Recaudador:</th>
				<td><html:text property="process.detaba" readonly="true" /></td>
				<th>Fecha de Cobranza:</th>
				<td><html:text styleId="process.fcob12"
						property="process.fcob12" readonly="true" /></td>
			</tr>
		</logic:notEqual>
		<tr id="comprobantesOcultos">
			<th>Total de Comprobantes:</th>
			<td><html:text property="process.tcomp12" styleId="tcomp12"
					readonly="true" /></td>
			<th>Importe Total:</th>
			<td><html:text property="process.timp12" styleId="timp12"
					readonly="true" /></td>
		</tr>
		<tr>
			<th>M�todo de Carga:</th>
			<td><html:select property="process.metodoCarga" disabled="true"
					styleId="metodoCarga">
					<html:option value="1">Manual</html:option>
					<html:option value="0">Autom�tica</html:option>
					<html:option value="2">Procesamiento</html:option>
				</html:select></td>
			<th></th>
			<td></td>
		</tr>
		<tr>
			<th>Moneda:</th>
			<td><asf:select name="process.idMoneda"
					entityName="com.asf.gaf.hibernate.Moneda"
					listCaption="denominacion" listValues="idMoneda" orderBy="id"
					value="${ProcessForm.process.idMoneda}"
					attribs="onchange='traerCotizacion();'" /></td>
			<th>Cotizaci�n:</th>
			<td><asf:text type="decimal" name="ProcessForm" id="cotizacion"
					property="process.cotizacion" maxlength="5" nullText="1.000"
					attribs="onchange=\"calcularCotizacion(); \"" /></td>
		</tr>
		<logic:notEqual name="ProcessForm" property="process.metodoCarga"
			value="0">
			<tr>
				<logic:notEmpty name="ProcessForm"
					property="process.caratula.fecovitaTipo">
					<th style="display: none">Nro. de Proyecto:</th>
					<td style="display: none"><input type="text" NAME="nb" id="nb"
						size="20" maxlength="44" /> <input type="button" id="bBuscar"
						value="Buscar"
						onclick="agregar(this.value); getElem('importe').focus(); return false;" />
					</td>
					<th style="display: none">Monto a Imputar:</th>
					<td style="display: none"><input type="text"
						name="importeBoleto" id="importeBoleto" size="20"
						onkeypress="if(event.keyCode==13){agregarBoletoManual(); return false;}">
						<input type="button" id="bAgregar" value="Agregar"
						onclick="agregarBoletoManual();"></td>
				</logic:notEmpty>
				<logic:empty name="ProcessForm"
					property="process.caratula.fecovitaTipo">
					<th>Nro. de Proyecto:</th>
					<td><input type="text" NAME="nb" id="nb" size="20"
						maxlength="44" /> <input type="button" id="bBuscar"
						value="Buscar"
						onclick="agregar(this.value); getElem('importe').focus(); return false;" />
					</td>
					<th>Monto a Imputar:</th>
					<td><input type="text" name="importeBoleto" id="importeBoleto"
						size="20"
						onkeypress="if(event.keyCode==13){agregarBoletoManual(); return false;}">
						<input type="button" id="bAgregar" value="Agregar"
						onclick="agregarBoletoManual();"></td>
				</logic:empty>
				<logic:notEmpty name="ProcessForm"
					property="process.caratula.fecovitaTipo">
					<td colspan="2"><input type="button" value="Cargar Fecovita"
						onclick="cargarBoletosFecovita();"></td>
					<th>Fecha de C�lculo:</th>
					<td><asf:calendar
							property="ProcessForm.process.fechaCalculoFecovitaStr" /></td>
				</logic:notEmpty>
			</tr>
			<tr>
				<th>Persona:</th>
				<td><asf:text maxlength="100" property="" id="titular"
						type="text" readonly="true" /></td>
				<th>CUIL:</th>
				<td><asf:text maxlength="100" property="" id="cuil" type="text"
						readonly="true" /></td>
			</tr>
		</logic:notEqual>
	</table>
	<div id="tr_iframe"
		style="width: 600px; height: 200px; margin-top: 30px;">
		<iframe
			src="${pageContext.request.contextPath}/actions/importacionCobranza.do?do=loadImportarCobranza&idCaratula=${ProcessForm.process.idcaratula}"
			style="border: 0" width="100%" height="200px" name="ImportacionForm"></iframe>
	</div>
	<br />
	<input type="button" value="Ver Resultado" id="idVerResultado"
		onclick="verResultado();" />
	<input type="button" value="Conciliar sin Importar"
		id="idConciliarSinImportar" onclick="conciliarSinImportar();" />
	<table border="0" id="boletosAProcesar">
		<tr>
			<th>Boletos a procesar:</th>
			<td colspan="5">
				<table id="tabla">
					<tr>
						<th>Nro.</th>
						<th>Titular</th>
						<th>Importe Total Pagado</th>
						<th>Fecha Pago</th>
						<th>Cuota</th>
						<th>Total Aplicado</th>
						<th>Capital</th>
						<th>Comp.</th>
						<th>Morat.</th>
						<th>Punit.</th>
						<th>Gastos Adm.</th>
						<th>Gastos Rec.</th>
						<th>Multas</th>
						<th>Saldo Pte Aplicar</th>
						<th>Elim.</th>
					</tr>
				</table>
				<table style="display: none">
					<tr>
						<td><select name="process.lista" id="lis" size="7"
							multiple="true" style="width: 790px" /></td>
					</tr>
					<tr>
						<td><input type="button" id="bQuitar" value="Quitar"
							disabled="true" onclick="quitar();" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th>Total a procesar:</th>
			<td><asf:text name="ProcessForm" property="process.ACobrar"
					type="decimal" maxlength="11" id="aCobrar" readonly="true" /></td>
		</tr>
	</table>

	<div id="recauDatos_iframe">
		<table>
			<tr>
				<th>Recaudaci�n</th>
				<th>Fecha</th>
				<th>Importe</th>
				<th>Saldo</th>
				<th>Saldo en Partidas Pendientes</th>
			</tr>
			<tr>
				<td><input id="recauId" readonly="readonly"
					value="${ProcessForm.process.recaudId}" /></td>
				<td><input id="recauFecha" readonly="readonly"
					value="${ProcessForm.process.recaudFecha}" /></td>
				<td><input id="recauImporte" readonly="readonly"
					value="${ProcessForm.process.recaudImporte}" /></td>
				<td><input id="recauSaldo" readonly="readonly"
					value="${ProcessForm.process.recaudSaldo}" /></td>

			</tr>
		</table>
	</div>
	<br>

	<script>
		function traerCotizacion() {
			var moneda = $('process.idMoneda');
			var fecha = $('process.fcob12');
			url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxCotizacionFecha&idMoneda="
					+ moneda.value + "&fecha=" + fecha.value;
			retrieveURL(url, traerCotizacionResponse);
		}

		function traerCotizacionResponse(responseText) {
			$('cotizacion').value = responseText;
		}

		function foco() {
			document.getElementById('nb').focus();
			if ($('save').value == 'true')
				alert("Se gener� el LOTE NRO "
						+ $('lote').value
						+ ". Finaliz� correctamente el proceso Registrar Cobro.-");
		}
		onload = foco;

		function verResultado() {
			var detalleArchivo = $('idDetalleArchivo');
			if (detalleArchivo == null || detalleArchivo.value == null
					|| detalleArchivo.value == "") {
				return;
			}
			window.location = "${pageContext.request.contextPath}/actions/process.do?do=process"
					+ "&processName=DetalleRecaudaciones"
					+ "&process.idDetalleArchivo="
					+ detalleArchivo.value
					+ "&process.idDetalleArchivo="
					+ detalleArchivo.value
					+ "&process.idCaratula="
					+ $('idcaratula').value
					+ "&process.idFechaCobranza="
					+ $('idFechaCobranza').value
					+ "&process.accion=listarRecaudaciones";
		}

		function conciliarSinImportar() {
			window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConciliacionRecaudaciones"
					+ "&process.idFechaCobranza="
					+ $('idFechaCobranza').value
					+ "&process.idCaratula=${ProcessForm.process.idcaratula}";
		}
	</script>
	<%-- Formulario Cobro Pago --%>
	<table id="medioPago">
		<tr>
			<th>Medio de Pago:</th>
			<td><asf:select name="process.idmediopago"
					entityName="com.asf.gaf.hibernate.Mediopago"
					listCaption="getDenominacion" listValues="getId" nullValue="true"
					nullText="SELECCIONE MEDIO"
					value="${ProcessForm.process.idmediopago}"
					attribs="onChange='mostrarMedio(this.value);'"
					filter="id like '%' order by id" /></td>
		</tr>
		<div id="mPago" style="display: none">
			<tr id="entidad" style="display: none">
				<th>Entidad Emisora:</th>
				<td><html:text property="process.entidademisora"
						styleId="entidademisora" /></td>
			</tr>
			<tr id="nro" style="display: none">
				<th>Nro de Comprobante:</th>
				<td><asf:text property="process.nrocomprobante" type="long"
						maxlength="11" /></td>
			</tr>
			<tr id="femi" style="display: none">
				<th>Fecha de Emisi�n:</th>
				<td><asf:calendar property="ProcessForm.process.femisionStr" />
				</td>
			</tr>
			<tr id="fvto" style="display: none">
				<th>Fecha de Vencimiento:</th>
				<td><asf:calendar
						property="ProcessForm.process.fvencimientoStr" /></td>
			</tr>
			<tr id="cuot" style="display: none">
				<th>Cuotas:</th>
				<td><asf:text property="process.cuotas" id="cuotas"
						name="ProcessForm" type="long" maxlength="2" /></td>
			</tr>
			<tr id="tplan" style="display: none">
				<th>Plan:</th>
				<td><html:text property="process.plan" styleId="plan" /></td>
			</tr>
			<tr id="autoriza" style="display: none">
				<th>Autorizaci�n:</th>
				<td><html:text property="process.autorizacion"
						styleId="autorizacion" /></td>
			</tr>
			<tr id="tmedio" style="display: none">
				<th>Tipo de Medio:</th>
				<td><html:text property="process.tipomedio" styleId="tipomedio" />
				</td>
			</tr>
			<tr id="impo" style="display: none">
				<th>Importe:</th>
				<td><asf:text name="ProcessForm" property="process.importe"
						id="importe" type="decimal" maxlength="11" /></td>
			</tr>
			<%--		<tr id="vuelto" style="display:none"><th>Vuelto:</th><td><asf:text name="ProcessForm" property="process.difTotal" id="difTotal" type="decimal" maxlength="11" readonly="true"/></td></tr> --%>
		</div>
	</table>
	<br>
	<input type="button" id="bAgregarPagos" value="Agregar"
		onclick="agregarPagos();" />
	<br>
	<br>

	<%-- Detalle CobroPago --%>
	<%--<div  id="mostrarDetalle" style="display:none">        --%>
	<table id="detalle" style="border: 1px solid black">
		<tr>
			<th class="head">C�digo</th>
			<th class="head">Tipo Medio</th>
			<th class="head">Denominaci�n</th>
			<th class="head">Entidad Emisora</th>
			<th class="head">Nro Comprobante</th>
			<th class="head">Fecha Emisi�n</th>
			<th class="head">Fecha Vencimiento</th>
			<th class="head">Cuotas</th>
			<th class="head">Plan</th>
			<th class="head">Autorizaci�n</th>
			<th class="head">Importe</th>
			<th class="head">Acci�n</th>
		</tr>
		<tr>
			<th colspan="12">
				<hr>
			</th>
		</tr>
		<tr>
			<th colspan="12">
				<hr>
			</th>
		</tr>
		<tr id="idTotal">
			<th colspan="10" align="right">TOTAL</th>
			<td style="font-weight: bold;" id="total" align="right">0.00</td>
		</tr>
		<tr id="idVuelto">
			<th colspan="10" align="right">VUELTO</th>
			<td style="font-weight: bold;" id="totalV" align="right">0.00</td>
		</tr>
	</table>
	<br>
	<html:submit value="Guardar" onclick="return valida();"
		styleId="guardarId" />
	<input type="button" value="Guardar" onclick="guardarProcesamiento();"
		id="guardarProcesamientoId" />
	<input type="button" value="Guardar" onclick="guardarAutomatico();"
		id="guardarAutomaticoId" />
	<input type="button" value="Salir" onclick="salir();" />
	<%--</div>--%>

</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script language="javascript">
	
<%String elementos = ""; //par�metros para iniciar el array js que carga boletos.-
			for (int i = 0; i < lista.length; i++) //por cada pago; agregamos un elemento a elementos.-
			{
				elementos += "'" + lista[i] + "',"; //armamos los par�metros para iniciar el array
			}%>
	lista = new Array(
<%=(elementos.equals("") || elementos.length() <= 1
					? "''"
					: elementos.substring(0, elementos.length() - 1))%>
	); //inicializamos el array con lo que tra�a la lista.- 
	if (lista[0] != "") {
		for (i = 0; i < lista.length; i++) {
			key = lista[i];
			value = lista[i];
			cargarItem(key, value); //llamamos a cargarItem con el key y value anterior.-
			calcularTotalBoletos(aKey[2]);

		}
	}

	var tabla = $('detalle'); //control sobre la tabla
	var medioPago = $('process.idmediopago');
	var tipoMedio = $('tipomedio');
	var eEmisora = $('entidademisora');
	var nro = $('process.nrocomprobante');
	var femi = $('process.femisionStr');
	var fvto = $('process.fvencimientoStr');
	var cuot = $('cuotas');
	var tplan = $('plan');
	var autoriza = $('autorizacion');
	var importe = $('importe');
	var acobrar = $('aCobrar');
	var item = 0;
	var tot = new Number(0);

	function agregarPagos() {
		var rows = tabla.rows.length;
		var posicion = rows - 3;
		if (item != 0) {
			posicion = item + 1;
		}
<%--	var rec = $('recauId');--%>
	
<%--	if(rec.value != null && rec.value != "" && rec.value != 0){--%>
	
<%--		var acobrar = $('aCobrar');--%>
	
<%--		modificarRecaudacion("", new Number(acobrar.value));--%>
	
<%--	}--%>
	//Valida datos ingresados..
		if ($('entidad').style.display == ''
				&& (eEmisora.value == null || eEmisora.value == '')) {
			alert("La ENTIDAD EMISORA est� vac�a o no es correcta. Por favor, intente nuevamente.");
			return false;
		}
		if ($('nro').style.display == ''
				&& (nro.value == null || nro.value == '' || nro.value == 0)) {
			alert("El NRO DE COMPROBANTE est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if ($('femi').style.display == ''
				&& (femi.value == null || femi.value == '')) {
			alert("La FECHA DE EMISI�N est� vac�a o no es correcta. Por favor, intente nuevamente.");
			return false;
		}
		if ($('fvto').style.display == ''
				&& (fvto.value == null || fvto.value == '')) {
			alert("La FECHA DE VENCIMIENTO est� vac�a o no es correcta. Por favor, intente nuevamente.");
			return false;
		}
		if ($('femi').style.display == ''
				&& $('fvto').style.display == ''
				&& femi.value != null
				&& fvto.value != null
				&& getDate(femi.value).getTime() > getDate(fvto.value)
						.getTime()) {
			alert("La FECHA DE EMISION no es correcta, debe ser menor a la FECHA DE VENCIMIENTO. Por favor, intente nuevamente.");
			return false;
		}
		if ($('cuot').style.display == ''
				&& (cuot.value == null || cuot.value == '')) {
			alert("El n�mero de CUOTAS est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if ($('tplan').style.display == ''
				&& (tplan.value == null || tplan.value == '')) {
			alert("El PLAN est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if ($('autoriza').style.display == ''
				&& (autoriza.value == null || autoriza.value == '')) {
			alert("El detalle de AUTORIZACI�N est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if (isNaN(new Number(importe.value)) || importe.value == 0) {
			alert("El IMPORTE ingresado est� vac�o o no es correcto, debe ser un valor mayor a 0.0. Por favor, intente nuevamente.");
			return false;
		}

		var importeRedondeado = new Number(importe.value);
		importeRedondeado = Math.round(importeRedondeado * 100) / 100;
		if (importe.value != null
				&& importe.value != 0
				&& (medioPago.value != null && tipoMedio.value != 'E' && importeRedondeado > new Number(
						$('aCobrar').value))) {
			alert("El IMPORTE ingresado no es correcto, debe ser menor o igual al Total a Cobrar. Por favor, intente nuevamente.");

			return false;
		}

		var metodoCarga = getElem('metodoCarga').value;
		if (metodoCarga == 1) {
			medioPago.selectedIndex = 1;
			medioPago.value = 1;
			tipoMedio.value = 'E';
		}
		var fila = tabla.insertRow(posicion);
		cell = fila.insertCell(0);
		cell.innerHTML = medioPago.value;
		cell = fila.insertCell(1);
		cell.innerHTML = tipoMedio.value;
		cell = fila.insertCell(2);
		var denominacion = medioPago.options[medioPago.selectedIndex].text;
		cell.innerHTML = denominacion;
		cell = fila.insertCell(3);
		cell.innerHTML = eEmisora.value;
		cell = fila.insertCell(4);
		cell.innerHTML = nro.value;
		cell = fila.insertCell(5);
		cell.innerHTML = femi.value;
		cell = fila.insertCell(6);
		cell.innerHTML = fvto.value;
		cell = fila.insertCell(7);
		cell.innerHTML = cuot.value;
		cell = fila.insertCell(8);
		cell.innerHTML = tplan.value;
		cell = fila.insertCell(9);
		cell.innerHTML = autoriza.value;
		cell = fila.insertCell(10);
		cell.innerHTML = parseFloat(importe.value).toFixed(2);
		cell.setAttribute("align", "right");
		addButtons(fila.insertCell(11), modificar, borrar, posicion); //botones de acci�n.-
		calcularTotal(posicion);

		var dif = new Number(new Number(tot) - new Number(sTotal)).toFixed(2);
		var vuelto = dif > 0 ? dif : new Number(0).toFixed(2);
		$('totalV').innerHTML = parseFloat(vuelto).toFixed(2);

		item = 0;
		resetVal();

	}

	function agregarPagosProcesamiento(fila) {
		var rows = tabla.rows.length;
		var posicion = rows - 3;
		if (item != 0) {
			posicion = item + 1;
		}

		//Valida datos ingresados..
		if ($('entidad').style.display == ''
				&& (eEmisora.value == null || eEmisora.value == '')) {
			alert("La ENTIDAD EMISORA est� vac�a o no es correcta. Por favor, intente nuevamente.");
			return false;
		}
		if ($('nro').style.display == ''
				&& (nro.value == null || nro.value == '' || nro.value == 0)) {
			alert("El NRO DE COMPROBANTE est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if ($('femi').style.display == ''
				&& (femi.value == null || femi.value == '')) {
			alert("La FECHA DE EMISI�N est� vac�a o no es correcta. Por favor, intente nuevamente.");
			return false;
		}
		if ($('fvto').style.display == ''
				&& (fvto.value == null || fvto.value == '')) {
			alert("La FECHA DE VENCIMIENTO est� vac�a o no es correcta. Por favor, intente nuevamente.");
			return false;
		}
		if ($('femi').style.display == ''
				&& $('fvto').style.display == ''
				&& femi.value != null
				&& fvto.value != null
				&& getDate(femi.value).getTime() > getDate(fvto.value)
						.getTime()) {
			alert("La FECHA DE EMISION no es correcta, debe ser menor a la FECHA DE VENCIMIENTO. Por favor, intente nuevamente.");
			return false;
		}
		if ($('cuot').style.display == ''
				&& (cuot.value == null || cuot.value == '')) {
			alert("El n�mero de CUOTAS est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if ($('tplan').style.display == ''
				&& (tplan.value == null || tplan.value == '')) {
			alert("El PLAN est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if ($('autoriza').style.display == ''
				&& (autoriza.value == null || autoriza.value == '')) {
			alert("El detalle de AUTORIZACI�N est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		if (isNaN(new Number(importe.value)) || importe.value == 0) {
			alert("El IMPORTE ingresado est� vac�o o no es correcto, debe ser un valor mayor a 0.0. Por favor, intente nuevamente.");
			return false;
		}

		var metodoCarga = getElem('metodoCarga').value;
		if (metodoCarga == 3) {
			var importeRedondeado = new Number($('aCobrar').value);
			importeRedondeado = Math.round(importeRedondeado * 100) / 100;
			$('recauSaldo').value = $('recauSaldo').value - importeRedondeado;
		} else {
			var importeRedondeado = new Number(importe.value);
			importeRedondeado = Math.round(importeRedondeado * 100) / 100;
		}

		if (metodoCarga == 1 || metodoCarga == 3) {
			medioPago.selectedIndex = 1;
			medioPago.value = 1;
			tipoMedio.value = 'E';
		}

		if (metodoCarga == 3) {
			if (importeRedondeado > new Number($('recauImporte').value)) {
				alert("El IMPORTE ingresado no es correcto, debe ser menor o igual al Total a Cobrar. Por favor, intente nuevamente.");
				return false;
			} else {
				if (importe.value != null
						&& importe.value != 0
						&& (medioPago.value != null && tipoMedio.value != 'E' && importeRedondeado > new Number(
								$('aCobrar').value))) {
					alert("El IMPORTE ingresado no es correcto, debe ser menor o igual al Total a Cobrar. Por favor, intente nuevamente.");
					return false;
				}
			}
		}

		var fila = tabla.insertRow(posicion);
		cell = fila.insertCell(0);
		cell.innerHTML = medioPago.value;
		cell = fila.insertCell(1);
		cell.innerHTML = tipoMedio.value;
		cell = fila.insertCell(2);
		var denominacion = medioPago.options[medioPago.selectedIndex].text;
		cell.innerHTML = denominacion;
		cell = fila.insertCell(3);
		cell.innerHTML = eEmisora.value;
		cell = fila.insertCell(4);
		cell.innerHTML = nro.value;
		cell = fila.insertCell(5);
		cell.innerHTML = femi.value;
		cell = fila.insertCell(6);
		cell.innerHTML = fvto.value;
		cell = fila.insertCell(7);
		cell.innerHTML = cuot.value;
		cell = fila.insertCell(8);
		cell.innerHTML = tplan.value;
		cell = fila.insertCell(9);
		cell.innerHTML = autoriza.value;
		cell = fila.insertCell(10);
		cell.innerHTML = parseFloat(importe.value).toFixed(2);
		cell.setAttribute("align", "right");
		addButtons(fila.insertCell(11), modificar, borrar, posicion); //botones de acci�n.-

		calcularTotalProcesamiento(posicion);

		var dif = new Number(new Number(tot) - new Number(sTotal)).toFixed(2);
		var vuelto = dif > 0 ? dif : new Number(0).toFixed(2);
		$('totalV').innerHTML = parseFloat(vuelto).toFixed(2);

		item = 0;

	}

	function modificar(evt) {
		if (isIE) {
			evt = window.event;
		}
		elem = (evt.target) ? evt.target : evt.srcElement;

		elem = elem.parentNode.parentNode;
		idItem = elem.rowIndex - 2;

		item = idItem + 1;

		medioPago.value = elem.childNodes[0].innerHTML;
		tipoMedio.value = elem.childNodes[1].innerHTML;
		eEmisora.value = elem.childNodes[3].innerHTML;
		nro.value = elem.childNodes[4].innerHTML;
		femi.value = elem.childNodes[5].innerHTML;
		fvto.value = elem.childNodes[6].innerHTML;
		cuot.value = elem.childNodes[7].innerHTML;
		tplan.value = elem.childNodes[8].innerHTML;
		autoriza.value = elem.childNodes[9].innerHTML;
		importe.value = elem.childNodes[10].innerHTML;

		eEmisora.value != null && eEmisora.value != '' ? $('entidad').style.display = ''
				: $('entidad').style.display = 'none';
		nro.value != null && nro.value != '' && nro.value != 0 ? $('nro').style.display = ''
				: $('nro').style.display = 'none';
		femi.value != null && femi.value != '' ? $('femi').style.display = ''
				: $('femi').style.display = 'none';
		fvto.value != null && fvto.value != '' ? $('fvto').style.display = ''
				: $('fvto').style.display = 'none';
		cuot.value != null && cuot.value != 0 ? $('cuot').style.display = ''
				: $('cuot').style.display = 'none';
		tplan.value != null && tplan.value != '' ? $('tplan').style.display = ''
				: $('tplan').style.display = 'none';
		autoriza.value != null && autoriza.value != '' ? $('autoriza').style.display = ''
				: $('autoriza').style.display = 'none';

		elem.parentNode.removeChild(elem);

		tot -= new Number(importe.value); // actualiza importe total de la grilla..
		$('total').innerHTML = parseFloat(tot).toFixed(2);
		var dif = new Number(new Number(tot) - new Number(sTotal)).toFixed(2);
		var vuelto = dif > 0 ? dif : new Number(0).toFixed(2);
		$('totalV').innerHTML = parseFloat(vuelto).toFixed(2);

	}//fin modificar.-

	function borrar(evt) {
		if (!evt) {
			evt = window.event;
		}
		var elem = (evt.target) ? evt.target : evt.srcElement;
		var index;

		var respuesta = confirm('�Est� seguro que desea eliminar este �tem?');
		if (respuesta == true) {
			index = (elem.parentNode.parentNode.rowIndex - 2);
			elem.parentNode.parentNode.parentNode
					.removeChild(elem.parentNode.parentNode);

			tot -= new Number(
					elem.parentNode.parentNode.childNodes[10].innerHTML); // actualiza importe total de la grilla..
			$('total').innerHTML = parseFloat(tot).toFixed(2);
			var dif = new Number(new Number(tot) - new Number(sTotal))
					.toFixed(2);
			var vuelto = dif > 0 ? dif : new Number(0).toFixed(2);
			$('totalV').innerHTML = parseFloat(vuelto).toFixed(2);

		}
	}//fin borrar.-

	function validaProcesamiento() {
		var rows = tabla.rows.length;
		var i;
		var conEfectivo = 0;
		var totEfectivo = 0;

		if (tot == 0) {
			alert("El TOTAL est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}

		for (i = 2; i < rows - 3; i++) {
			conEfectivo += tabla.getElementsByTagName("tr")[i]
					.getElementsByTagName("td")[1].innerHTML == 'E' ? 1 : 0;
			totEfectivo += tabla.getElementsByTagName("tr")[i]
					.getElementsByTagName("td")[1].innerHTML == 'E' ? tabla
					.getElementsByTagName("tr")[i].getElementsByTagName("td")[10].innerHTML
					: 0;
		}

		if (conEfectivo > 0
				&& new Number($('totalV').innerHTML) > new Number(totEfectivo)) {
			alert("EL Importe total VUELTO no debe ser mayor al Importe EFECTIVO ingresado. Por favor, intente nuevamente.");
			return false;
		}

		if (tot != null
				&& ((conEfectivo == 0 && tot > sTotal) || (sTotal > tot))) {
			if (conEfectivo == 0)
				alert("El pago no incluye medio de pago EFECTIVO, por lo tanto el TOTAL debe ser igual al Total a Cobrar. Por favor, intente nuevamente.");
			else
				alert("EL TOTAL no es correcto, debe ser igual al Total a Cobrar. Por favor, intente nuevamente.");

			return false;
		}

		for (i = 2; i < rows - 3; i++) {

			var pos = i - 2;

			fila = tabla.getElementsByTagName("tr")[i];

			cell = fila.getElementsByTagName("td")[0];
			addHidden(cell, "process.cobroPagos[" + pos + "].mediopagoId",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[2];
			addHidden(cell, "process.cobroPagos[" + pos + "].idcaratula",
					$F('idcaratula'), "");
			cell = fila.getElementsByTagName("td")[3];
			addHidden(cell, "process.cobroPagos[" + pos + "].entidadEmisora",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[4];
			addHidden(cell,
					"process.cobroPagos[" + pos + "].numeroComprobante",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[5];
			addHidden(cell, "process.cobroPagos[" + pos + "].fechaEmisionStr",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[6];
			addHidden(cell, "process.cobroPagos[" + pos
					+ "].fechaVencimientoStr", cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[7];
			addHidden(cell, "process.cobroPagos[" + pos + "].cuotas",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[8];
			addHidden(cell, "process.cobroPagos[" + pos + "].plan",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[9];
			addHidden(cell, "process.cobroPagos[" + pos + "].autorizacion",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[10]
			var impo = cell.innerHTML;
			if (fila.getElementsByTagName("td")[1].innerHTML == 'E')
				impo = cell.innerHTML - $('totalV').innerHTML;
			addHidden(cell, "process.cobroPagos[" + pos + "].importe", impo, "");

			resetVal();

		}

		$('save').value = true;
		return true;

	}

	function valida() {
		var rows = tabla.rows.length;
		var i;
		var conEfectivo = 0;
		var totEfectivo = 0;

		if (tot == 0) {
			alert("El TOTAL est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}

		for (i = 2; i < rows - 3; i++) {
			conEfectivo += tabla.getElementsByTagName("tr")[i]
					.getElementsByTagName("td")[1].innerHTML == 'E' ? 1 : 0;
			totEfectivo += tabla.getElementsByTagName("tr")[i]
					.getElementsByTagName("td")[1].innerHTML == 'E' ? tabla
					.getElementsByTagName("tr")[i].getElementsByTagName("td")[10].innerHTML
					: 0;
		}

		if (conEfectivo > 0
				&& new Number($('totalV').innerHTML) > new Number(totEfectivo)) {
			alert("EL Importe total VUELTO no debe ser mayor al Importe EFECTIVO ingresado. Por favor, intente nuevamente.");
			return false;
		}

		if (tot != null
				&& ((conEfectivo == 0 && tot > sTotal) || (sTotal > tot))) {
			if (conEfectivo == 0)
				alert("El pago no incluye medio de pago EFECTIVO, por lo tanto el TOTAL debe ser igual al Total a Cobrar. Por favor, intente nuevamente.");
			else
				alert("EL TOTAL no es correcto, debe ser igual al Total a Cobrar. Por favor, intente nuevamente.");

			return false;
		}

		for (i = 2; i < rows - 3; i++) {

			var pos = i - 2;

			fila = tabla.getElementsByTagName("tr")[i];

			cell = fila.getElementsByTagName("td")[0];
			addHidden(cell, "process.cobroPagos[" + pos + "].mediopagoId",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[2];
			addHidden(cell, "process.cobroPagos[" + pos + "].idcaratula",
					$F('idcaratula'), "");
			cell = fila.getElementsByTagName("td")[3];
			addHidden(cell, "process.cobroPagos[" + pos + "].entidadEmisora",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[4];
			addHidden(cell,
					"process.cobroPagos[" + pos + "].numeroComprobante",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[5];
			addHidden(cell, "process.cobroPagos[" + pos + "].fechaEmisionStr",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[6];
			addHidden(cell, "process.cobroPagos[" + pos
					+ "].fechaVencimientoStr", cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[7];
			addHidden(cell, "process.cobroPagos[" + pos + "].cuotas",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[8];
			addHidden(cell, "process.cobroPagos[" + pos + "].plan",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[9];
			addHidden(cell, "process.cobroPagos[" + pos + "].autorizacion",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[10]
			var impo = cell.innerHTML;
			if (fila.getElementsByTagName("td")[1].innerHTML == 'E')
				impo = cell.innerHTML - $('totalV').innerHTML;
			addHidden(cell, "process.cobroPagos[" + pos + "].importe", impo, "");

			resetVal();

		}

		$('save').value = true;
		return true;

	}

	function validaAutomatico() {
		var rows = tabla.rows.length;
		var i;
		var conEfectivo = 0;
		var totEfectivo = 0;
		if (tot == 0) {
			alert("El TOTAL est� vac�o o no es correcto. Por favor, intente nuevamente.");
			return false;
		}
		for (i = 2; i < rows - 3; i++) {
			conEfectivo += tabla.getElementsByTagName("tr")[i]
					.getElementsByTagName("td")[1].innerHTML == 'E' ? 1 : 0;
			totEfectivo += tabla.getElementsByTagName("tr")[i]
					.getElementsByTagName("td")[1].innerHTML == 'E' ? tabla
					.getElementsByTagName("tr")[i].getElementsByTagName("td")[10].innerHTML
					: 0;
		}
		for (i = 2; i < rows - 3; i++) {
			var pos = i - 2;
			fila = tabla.getElementsByTagName("tr")[i];

			cell = fila.getElementsByTagName("td")[0];
			addHidden(cell, "process.cobroPagos[" + pos + "].mediopagoId",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[2];
			addHidden(cell, "process.cobroPagos[" + pos + "].idcaratula",
					$F('idcaratula'), "");
			cell = fila.getElementsByTagName("td")[3];
			addHidden(cell, "process.cobroPagos[" + pos + "].entidadEmisora",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[4];
			addHidden(cell,
					"process.cobroPagos[" + pos + "].numeroComprobante",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[5];
			addHidden(cell, "process.cobroPagos[" + pos + "].fechaEmisionStr",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[6];
			addHidden(cell, "process.cobroPagos[" + pos
					+ "].fechaVencimientoStr", cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[7];
			addHidden(cell, "process.cobroPagos[" + pos + "].cuotas",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[8];
			addHidden(cell, "process.cobroPagos[" + pos + "].plan",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[9];
			addHidden(cell, "process.cobroPagos[" + pos + "].autorizacion",
					cell.innerHTML, "");
			cell = fila.getElementsByTagName("td")[10]
			var impo = cell.innerHTML;
			if (fila.getElementsByTagName("td")[1].innerHTML == 'E')
				impo = cell.innerHTML - $('totalV').innerHTML;
			addHidden(cell, "process.cobroPagos[" + pos + "].importe", impo, "");

			resetVal();

		}

		$('save').value = true;
		return true;

	}

	function guardarProcesamiento() {
		var cmb = getElem("lis");
		for (var i = 0; i < cmb.length; i++) {
			if (agregarPagosProcesamiento(i) == false) {
				return;
			}
		}
		if (validaProcesamiento() == true) {
			$('recaudId').value = $('recauId').value;
			var rec = $('recauId');
			if (rec.value != null && rec.value != "" && rec.value != 0) {
				var acobrar = $('aCobrar');
				modificarRecaudacionProcesamiento("", new Number(acobrar.value));
			} else {
				selectAll();
				$('processForm').submit();
			}
		}
	}

	function guardarAutomatico() {
		if (validaAutomatico() == true) {
			$('recaudId').value = $('recauId').value;
			var rec = $('recauId');
			if (rec.value != null && rec.value != "" && rec.value != 0) {
				var acobrar = $('aCobrar');
				modificarRecaudacion("", new Number(acobrar.value));
			} else {
				selectAll();
				$('processForm').submit();
			}
		}
	}

	function salir() {
		if (tot == 0) {
			if (confirm('�Desea salir sin guardar datos?')) {
				window.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&process.fenv12Str=${ProcessForm.process.fenv12Str}&filter=true';
			}
		} else {
			window.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&process.fenv12Str=${ProcessForm.process.fenv12Str}&filter=true';
		}
	}

	function calcularTotal(i) {

		var rows = tabla.rows.length;
		var fila;

		fila = tabla.getElementsByTagName("tr")[i];
		cell = fila.getElementsByTagName("td")[10];
		tot += new Number(cell.innerHTML);

		$('total').innerHTML = parseFloat(tot).toFixed(2);

	}

	function calcularTotalProcesamiento(i) {

		var rows = tabla.rows.length;
		var fila;

		fila = tabla.getElementsByTagName("tr")[i];
		cell = fila.getElementsByTagName("td")[10];
		tot = new Number(cell.innerHTML);

		$('total').innerHTML = parseFloat(tot).toFixed(2);

	}

	function calcularTotalBoletos(val) {
		var bTotal = val.split("$");
		sTotal += new Number(bTotal[1]).toFixed(2);

	}

	function resetVal() {
		medioPago.value = '';
		eEmisora.value = '';
		nro.value = '';
		femi.value = '';
		fvto.value = '';
		cuot.value = '';
		tplan.value = '';
		autoriza.value = '';
		importe.value = new Number(0);

	}

	cambiar(getElem("metodoCarga").options[getElem("metodoCarga").selectedIndex].value);

	function navegadorValido() {
		var esValidoNav = true;
		if (!isIE) {
			try {
				var version = navigator.userAgent;
				var mensaje = "";
				if (version.indexOf("2.0") > 0) {
					version = version.substring(version.indexOf("2.0"));
					if (version == "2.0") {
						mensaje += "Esta funcionalidad no est� disponible con Mozilla Firefox 2.0\n";
						mensaje += "Por favor, actualice su navegador a una versi�n posterior";
						alert(mensaje);
						esValidoNav = false;
					}
				}
			} catch (e) {
				esValidoNav = false;
			}
		}

		return esValidoNav;
	}

	function cargarBoletosFecovita() {
		window.open("${pageContext.request.contextPath}/progress.jsp",
				"Proceso", "width=450, height=180");
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=generarBoletosFecovita&saldo="
				+ $('recauSaldo').value
				+ "&fechaCobranza="
				+ $('idFechaCobranza').value
				+ "&fechaCalculo="
				+ $('process.fechaCalculoFecovitaStr').value
				+ "&fecovitaTipo="
				+ $('fecovitaTipo').value;
		retrieveURL(url, boletoFecovitaResponse);
	}

	function boletoFecovitaResponse(responseText) {
		__RETORNO = true;
		if (responseText == "<Objects></Objects>") {
			alert('No hay deuda de los cr�ditos Fecovita.');
			return;
		}
		var objetos = parseXml(responseText);
		boletosFecovita = objetos.Objects.Credito;
		var importeTot = $('importe').value;
		var importeFec = new Number();
		if (boletosFecovita.length != null) {
			for (var i = 0; i < boletosFecovita.length; i++) {
				boleto = boletosFecovita[i];
				importeFec = boleto.importe;
				getElem('importeBoleto').value = importeFec;
				if (agregarBoletoFecovita()) {
					boleto.importe = $('recauSaldo').value;
					getElem('importeBoleto').value = boleto.importe;
					importeFec = boleto.importe;
					agregarBoletoFecovita();
					importeTot = new Number(importeTot)
							+ new Number(importeFec);
					break;
				}
				importeTot = new Number(importeTot) + new Number(importeFec);
			}
			importeFec = new Number();
			importe.value = importeTot;
		} else {
			boleto = boletosFecovita;
			importeFec = boleto.importe;
			getElem('importeBoleto').value = importeFec;
			if (agregarBoletoFecovita()) {
				boleto.importe = $('recauSaldo').value;
				getElem('importeBoleto').value = boleto.importe;
				importeFec = boleto.importe;
				agregarBoletoFecovita();
			}
			importeTot = new Number(importeTot) + new Number(importeFec);
			importeFec = new Number();
			importe.value = importeTot;
		}
	}
</script>

<script>
	
<%-- Iniciar pantalla --%>
	console.info('En la inicializaci�n de la pantalla');

	var metodoCarga = $('metodoCarga').value;
	var moneda = $('process.idMoneda');
	var cotiz = $('cotizacion');
	var imp = $('importe');
	limpiarRecaudacion();
	ocultar("guardarProcesamientoId");
	ocultar("guardarAutomaticoId");
	ocultar("idConciliarSinImportar");
	ocultar("idVerResultado");
	console.log('metodoCarga', metodoCarga);
	if (metodoCarga == 0) {
		mostrar("tr_iframe");
		ocultar("comprobantesOcultos");
		ocultar("medioPago");
		imp.disabled = true;
		moneda.disabled = true;
		cotiz.disabled = true;
		ocultar("recauDatos_iframe");
<%--    cargarPopUpMetodoAutomatico();--%>
	ocultar("guardarId");
		ocultar("guardarAutomaticoId");
		ocultar("bQuitar");
		ocultar("idTotal");
		ocultar("idVuelto");
		ocultar("bAgregarPagos");
		ocultar("boletosAProcesar");
		ocultar("detalle");
		mostrar("idConciliarSinImportar");
		mostrar("idVerResultado");
	} else if (metodoCarga == 1) {
		ocultar("tr_iframe");
		ocultar("comprobantesOcultos");
		ocultar("medioPago");
		mostrar("mPago");
		moneda.disabled = false;
		cotiz.disabled = false;
		imp.disabled = false;
		ocultar("recauDatos_iframe");
	} else if (metodoCarga == 2) {
		ocultar("tr_iframe");
		ocultar("comprobantesOcultos");
		mostrar("medioPago");
		mostrar("recauDatos_iframe");
		moneda.disabled = true;
		cotiz.disabled = true;
		mostrar("mPago");
		mostrar("impo");
		imp.disabled = true;
		cargarPopUp();
		ocultar("bAgregarPagos");
		ocultar("detalle");
		$('process.idmediopago').disabled = true;
		ocultar("guardarId");
		mostrar("guardarProcesamientoId");
	} else if (metodoCarga == 3) { /* Pago A Cuenta */

		// Hasta aqu�, hab�amos copiado desde el metodo de carga 2

		ocultar("tr_iframe");
		ocultar("comprobantesOcultos");
		ocultar("medioPago");
		mostrar("recauDatos_iframe");
		moneda.disabled = true;
		cotiz.disabled = true;
		mostrar("mPago");
		mostrar("impo");
		imp.disabled = true;
		cargarPopUpPagoACuenta();
		ocultar("bAgregarPagos");
		ocultar("detalle");
		$('process.idmediopago').disabled = true;
		ocultar("guardarId");
		mostrar("guardarProcesamientoId");

		// Copiamos desde el metodo de carga 1
		/*
		ocultar("tr_iframe");
		ocultar("comprobantesOcultos");
		ocultar("medioPago");
		mostrar("mPago");
		moneda.disabled = false;
		cotiz.disabled = false;
		imp.disabled = false;
		ocultar("recauDatos_iframe");
		cargarPopUpPagoACuenta();
		 */
	}
<%-- Fin Iniciar pantalla --%>
	
</script>