<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Concepto Contable</div>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

	<html:messages id="messages" />

	<table border="0">

		<tr>
			<th>C�digo:</th>
			<td><html:text property="entity.id" readonly="true" /></td>
		</tr>
		<tr>
			<th>Concepto:</th>
			<td><asf:select name="entity.idConcepto"
					entityName="com.nirven.creditos.hibernate.Concepto"
					value="${AbmForm.entity.idConcepto}"
					listCaption="periodo,descripcion" listValues="id"
					orderBy="periodo desc" /></td>
		</tr>
		<tr>
			<th>Tipo:</th>
			<td><asf:select name="entity.tipo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipo}" listCaption="getDescripcion"
					listValues="getCodigo"
					filter="categoria = 'ctacte.detalle' order by codigo" /></td>
		</tr>
		<tr>
			<th>Modelo Asiento Debito:</th>
			<td><asf:select
					entityName="com.nirven.creditos.hibernate.ModeloAsiento"
					listCaption="ejercicio,denominacion" listValues="id"
					name="entity.idModeloAsientoDebito"
					value="${AbmForm.entity.idModeloAsientoDebito}"
					filter="tipoModelo in ('c', 'x') and ejercicio = ${AbmForm.entity.ejercicio}"
					orderBy="ejercicio,denominacion" nullValue="true"
					nullText="&lt;&lt;Sin modelo seleccionado&gt;&gt;" /></td>
		</tr>
		<tr>
			<th>Modelo Asiento Credito:</th>
			<td><asf:select
					entityName="com.nirven.creditos.hibernate.ModeloAsiento"
					listCaption="ejercicio,denominacion" listValues="id"
					name="entity.idModeloAsientoCredito"
					value="${AbmForm.entity.idModeloAsientoCredito}"
					filter="tipoModelo in ('c', 'x') and ejercicio = ${AbmForm.entity.ejercicio}"
					orderBy="ejercicio,denominacion" nullValue="true"
					nullText="&lt;&lt;Sin modelo seleccionado&gt;&gt;" /></td>
		</tr>
	</table>

	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>

	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>


