<%@page import="com.nirven.creditos.hibernate.DetalleRecaudacion"%>
<%@page import="com.asf.util.DoubleHelper"%>
<%@page import="com.asf.gaf.hibernate.Recaudacion"%>
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.4.2.min.js"></script>

<script type="text/javascript">
	$j = jQuery.noConflict();

	function deselectAll(check) {
		$j(".select").attr("checked", false);
		check.checked = true;
	}

	function validarCheck(id) {
		if ($j("#"+id).attr("checked") == true) {
			$j("#accion").val("seleccionar");
			$j("#idRec").val(id.substring(4));
			$("ProcessForm").submit();
		}
	}

	function desvincular(id) {
		$j("#accion").val("desvincular");
		$j("#idDet").val(id);
		$("ProcessForm").submit();
	}

	function guardar() {
		$j("#accion").val("guardar");
		$("ProcessForm").submit();
	}
	
	function regSinVincular() {
		$j("#accion").val("regSinVincular");
		$("ProcessForm").submit();
	}
	
	function toggleTabla(id) {
		$j("#"+id).toggle(200);
	}
</script>

<div class="title">
	Conciliación de Recaudaciones
</div>
<html:form action="/actions/process.do?do=process&processName=ConciliacionRecaudaciones" styleId="ProcessForm">
	<html:hidden property="process.accion" styleId="accion" value="buscar"/>
	<html:hidden property="process.idRecaudacionSeleccionada" name="ProcessForm" styleId="idRec"/>
	<html:hidden property="process.idDetalleRec" name="ProcessForm" styleId="idDet"/>
	<html:hidden property="process.idFechaCobranza" styleId="idFechaCobranza" />
	<html:hidden property="process.cid"/>
	
	<div style="background-color: #939598; color: #FFF; font-size: 11px; padding: 4px 6px; text-align: center; margin: 0; font-weight: bold"
		onclick="toggleTabla('recCero');">
		Extractos imputados a registros del txt con saldo CERO
	</div>
	
	<div id="recCero">		
		<table width="100%">
			<tr>
				<th>Fecha Mov</th>
				<th>Ente Rec</th>
				<th>Sucursal</th>
				<th>Cuenta</th>
				<th>Cod Op</th>
				<th>Nro Op</th>
				<th>Importe</th>
				<th>Saldo</th>
				<th>Saldo temporal</th>
				<th colspan="2">Seleccionar</th>
			</tr>
			<logic:notEmpty name="ProcessForm" property="process.recaudacionesCero">
			<logic:iterate  id="rec" name="ProcessForm" property="process.recaudacionesCero" indexId="index">
				<tr>
					<td>${rec.fechamovimientoStr}</td>
					<td>${rec.bancos.detaBa}</td>
					<td>${rec.bancos.codiBa}</td>
					<td>${rec.bancos.cuenBa}</td>
					<td>${rec.codigoOperacion}</td>
					<td>${rec.nroOperacion}</td>
					<td align="right">${rec.importeStr}</td>
					<td align="right">${rec.saldoStr}</td>
					<td align="right">${rec.saldoTemporalStr}</td>
					<td><input type="checkbox" class="select" id="rec-${rec.id}" onclick="deselectAll(this);"/></td>
					<td><input type="submit" value="Seleccionar" name="rec-${rec.id}" onclick="validarCheck('rec-${rec.id}');"/></td>
				</tr>
				<tr>
					<td colspan="11" style="padding-left: 30px">
						<table style="background-color: white; width: 100%">
							<tr>
								<th>Titular</th>
								<th>Proyecto</th>
								<th>Expediente</th>
								<th>Fecha Pago</th>
								<th>Importe</th>
								<th style="width: 180px">Nro Cheque</th>
								<th>Estado</th>
								<th>Desvincular</th>
							</tr>
							<bean:define id="process" name="ProcessForm" property="process"></bean:define>
							<logic:notEmpty name="process" property='<%= "detallesVinculados(" + ((com.asf.gaf.hibernate.Recaudacion) rec).getId() + ")" %>'>
								<logic:iterate id="det" name="process" property='<%= "detallesVinculados(" + ((com.asf.gaf.hibernate.Recaudacion) rec).getId() + ")" %>'>
								<tr style="background-color: white">
									<td>${det.objetoi.persona.nomb12}</td>
									<td>${det.numeroProyecto}</td>
									<td>${det.objetoi.expediente}</td>
									<td>${det.fechaRecaudacion}</td>
									<td class="right">${det.importeStr}</td>
									<td style="text-align: center">${det.numeroValor}</td>
									<td>${det.estadoActual.estado}</td>
									<td><input type="button" value="..." onclick="desvincular('${det.id}');"/></td>
								</tr>
								</logic:iterate>
							</logic:notEmpty>
						</table>
					</td>
				</tr>
			</logic:iterate>
			</logic:notEmpty>
		</table>
	</div>
	
	<div style="background-color: #939598; color: #FFF; font-size: 11px; padding: 4px 6px; text-align: center; margin: 0; font-weight: bold; margin-top: 30px"
		onclick="toggleTabla('recPend');">
		Extractos imputados a registros del txt con saldo pendiente
	</div>
	<div id="recPend">
		<table width="100%">
			<tr>
				<th>Fecha Mov</th>
				<th>Ente Rec</th>
				<th>Sucursal</th>
				<th>Cuenta</th>
				<th>Cod Op</th>
				<th>Nro Op</th>
				<th>Importe</th>
				<th>Saldo</th>
				<th>Saldo temporal</th>
				<th colspan="2">Seleccionar</th>
			</tr>
			<logic:notEmpty name="ProcessForm" property="process.recaudacionesPendientes">
			<logic:iterate  id="rec" name="ProcessForm" property="process.recaudacionesPendientes" indexId="index">
				<tr>
					<td>${rec.fechamovimientoStr}</td>
					<td>${rec.bancos.detaBa}</td>
					<td>${rec.bancos.codiBa}</td>
					<td>${rec.bancos.cuenBa}</td>
					<td>${rec.codigoOperacion}</td>
					<td>${rec.nroOperacion}</td>
					<td align="right"><%= DoubleHelper.getStringAsCurrency(((Recaudacion) rec).getImporte()) %></td>
					<td align="right"><%= DoubleHelper.getStringAsCurrency(((Recaudacion) rec).getSaldo()) %></td>
					<td align="right"><%= DoubleHelper.getStringAsCurrency(((Recaudacion) rec).getSaldoTemporal()) %></td>
					<td><input type="checkbox" class="select" id="rec-${rec.id}" onclick="deselectAll(this);"/></td>
					<td><input type="submit" value="Seleccionar" name="rec-${rec.id}" onclick="validarCheck('rec-${rec.id}');"/></td>
				</tr>
				<tr>
					<td colspan="11" style="padding-left: 30px">
						<table style="background-color: white; width: 100%">
							<tr>
								<th>Titular</th>
								<th>Proyecto</th>
								<th>Expediente</th>
								<th>Fecha Pago</th>
								<th>Importe</th>
								<th style="width: 180px">Nro Cheque</th>
								<th>Estado</th>
								<th>Desvincular</th>
							</tr>
							<bean:define id="process" name="ProcessForm" property="process"></bean:define>
							<logic:notEmpty name="process" property='<%= "detallesVinculados(" + ((com.asf.gaf.hibernate.Recaudacion) rec).getId() + ")" %>'>
								<logic:iterate id="det" name="process" property='<%= "detallesVinculados(" + ((com.asf.gaf.hibernate.Recaudacion) rec).getId() + ")" %>'>
								<tr style="background-color: white">
									<td>${det.objetoi.persona.nomb12}</td>
									<td>${det.numeroProyecto}</td>
									<td>${det.objetoi.expediente}</td>
									<td>${det.fechaRecaudacion}</td>
									<td><%= DoubleHelper.getStringAsCurrency(((DetalleRecaudacion) det).getImporte()) %></td>
									<td>${det.numeroValor}</td>
									<td>${det.estadoActual.estado}</td>
									<td><input type="button" value="..." onclick="desvincular('${det.id}');"/></td>
								</tr>
								</logic:iterate>
							</logic:notEmpty>
						</table>
					</td>
				</tr>
			</logic:iterate>
			</logic:notEmpty>
		</table>
	</div>
	
	<br/>
	<br/>
	
	<table width="100%">
		<tr>
			<th colspan="6">Buscar Extractos Bancarios para vincular manualmente a registros del txt</th>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<tr>
			<th>Fecha Movimiento</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaMov"/></td>
			<th>Numero Comprobante</th>
			<td><html:text name="ProcessForm" property="process.nroComprobante"/></td>
			<th>Importe Desde $</th>
			<td><asf:text property="ProcessForm.process.importeDesde" type="decimal" maxlength="12" /></td>
		</tr>
		<tr>
			<th>Codigo Operacion</th>
			<td><html:text name="ProcessForm" property="process.codOp"/></td>
			<th>Estado</th>
			<td><html:select property="process.estadoComprobante" name="ProcessForm">
					<html:option value="P">Pendiente de aplicacion</html:option>
					<html:option value="A">Aplicado a proyectos</html:option>
					<html:option value="T">Todos</html:option>
				</html:select></td>
			<th>Importe Hasta $</th>
			<td><asf:text property="ProcessForm.process.importeHasta" type="decimal" maxlength="12" /></td>
		</tr>
		<tr>
			<td colspan="6" align="center">
				<html:submit value="Buscar Extractos"></html:submit>
				<input type="button" value="Registros sin Vincular" onclick="regSinVincular();"/>
			</td>
		</tr>
	</table>
	
	<table width="100%">
		<tr>
			<th>Fecha Mov</th>
			<th>Ente Rec</th>
			<th>Sucursal</th>
			<th>Cuenta</th>
			<th>Cod Op</th>
			<th>Nro Op</th>
			<th>Importe</th>
			<th>Saldo</th>
			<th>Saldo temporal</th>
			<th colspan="2">Seleccionar</th>
		</tr>
		<logic:notEmpty name="ProcessForm" property="process.recaudacionesFiltro">
		<logic:iterate  id="rec" name="ProcessForm" property="process.recaudacionesFiltro" indexId="index">
			<tr>
				<td>${rec.fechamovimientoStr}</td>
				<td>${rec.bancos.detaBa}</td>
				<td>${rec.bancos.codiBa}</td>
				<td>${rec.bancos.cuenBa}</td>
				<td>${rec.codigoOperacion}</td>
				<td>${rec.nroOperacion}</td>
				<td align="right"><%= DoubleHelper.getStringAsCurrency(((Recaudacion) rec).getImporte()) %></td>
				<td align="right"><%= DoubleHelper.getStringAsCurrency(((Recaudacion) rec).getSaldo()) %></td>
				<td align="right"><%= DoubleHelper.getStringAsCurrency(((Recaudacion) rec).getSaldoTemporal()) %></td>
				<td><input type="checkbox" class="select" id="rec-${rec.id}" onclick="deselectAll(this);"/></td>
				<td><input type="button" value="Seleccionar" name="rec-${rec.id}" onclick="validarCheck('rec-${rec.id}');"/></td>
			</tr>
		</logic:iterate>
		</logic:notEmpty>
	</table>
	
	<br/>
	<div style="float: left">
		<input type="button" value="Volver" 
		onclick="javascript:window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&filter=true&process.fenv12Str=${ProcessForm.process.idFechaCobranza}'"/>
	</div>
	<div style="float: right">
		<logic:equal value="false" name="ProcessForm" property="process.guardado">
			<input type="button" value="Guardar"
				onclick="guardar();"/>
		</logic:equal>
		<logic:equal value="true" name="ProcessForm" property="process.guardado">
			<input type="button" value="Siguiente"
				onclick="javascript:window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=PreaplicacionPagos&process.idFechaCobranza=${ProcessForm.process.idFechaCobranza}&process.idcaratula=${ProcessForm.process.idCaratula}'"/>
		</logic:equal>
	</div>
	<div style="clear: both"></div>
</html:form>

<script>
	toggleTabla('recCero');
	if ('${ProcessForm.process.colapsarPendientes}' == 'true') {
		toggleTabla('recPend');
	}
</script>

<iframe width=174 height=220 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>