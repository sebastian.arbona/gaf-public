<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<link rel="stylesheet" href="/CREDITOS/style/frames.css" type="text/css">
<div style="font-size: 20px; margin: 15px" align="left">
	<html:errors />
</div>
<div class="title">
	Actividad de Agenda
</div>
<br>
<br>
<html:form	action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm"  enctype="multipart/form-data">
	<html:hidden name="ProcessForm" property="process.actividadAgenda.id"  />
	<html:hidden name="ProcessForm" property="process.idObjetoi"  />
	<html:hidden name="ProcessForm" property="process.actividadAgenda.objetoi_id"  />
	<html:hidden name="ProcessForm" property="process.pestana" />
	<html:hidden name="ProcessForm" property="process.action" styleId="action" />
	<table border="0" height="200">
		 <tr>
		 	<th>
    	 		 Análisis
    	 	</th>
    		<td>
    			<asf:select name="process.actividadAgenda.analisis_id"
	                    entityName="com.nirven.creditos.hibernate.Analisis" 
						listCaption="getTipo,getSituacion" 
						listValues="getId"
						value="${ProcessForm.process.actividadAgenda.analisis_id}"
						filter="objetoi.id=${ProcessForm.process.actividadAgenda.objetoi_id}"                           
	                  	/>
    		</td>
    	</tr>
    	<tr>
		 	<th>
    	 		 Tipo de Actividad
    	 	</th>
    		<td>
    			<asf:select name="process.actividadAgenda.tipoActividadAgenda_id"
	                    entityName="com.nirven.creditos.hibernate.TipoActividadAgenda" 
						listCaption="getId,getNombre" 
						listValues="getId"
						value="${ProcessForm.process.actividadAgenda.tipoActividadAgenda_id}"						                           
	                  	/>
    		</td>
    	</tr>
		<tr>
			<th>
				Fecha Creación
			</th>
			<td>
				${ProcessForm.process.actividadAgenda.fechaCreacionStr}
			</td>
		</tr>
		<tr>
			<th>
				Fecha Vencimiento
			</th>
			<td>
				<asf:calendar property="ProcessForm.process.actividadAgenda.fechaVencimientoStr"	maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Fecha Cumplimiento
			</th>
			<td>
				<asf:calendar property="ProcessForm.process.actividadAgenda.fechaCumplimientoStr"	maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Detalle:
			</th>
			<td>
				<html:textarea property="process.actividadAgenda.detalle"
					name="ProcessForm" cols="40" rows="3" />
			</td>
		</tr>
		<tr>
			<th>
				Creador:
			</th>
			<td>
				${ProcessForm.process.currentUser}
			</td>
		</tr>
		<tr>
			<th>
				Responsable:
			</th>
			<td>
				<asf:select name="process.actividadAgenda.responsable_id"
	                    entityName="com.nirven.creditos.hibernate.Usuario" 
						listCaption="id" 
						listValues="id"
						value="${ProcessForm.process.actividadAgenda.responsable_id}"
						filter="activo=true"						                           
	                  	/>
			</td>
		</tr>
		
		<logic:notEmpty property="process.actividadAgenda.objetoiArchivo" name="ProcessForm">
			<tr>
				<th colspan="2">
				Este Actividad ya tiene un documento asociado con el nombre: ${ProcessForm.process.actividadAgenda.objetoiArchivo.nombre}.
				Si sube otro documento remplazará el anterior.
				</th>
			</tr>	
		</logic:notEmpty>
		<tr>
			<th>Adjuntar Documentación:
			</th>
			<td>
				<html:file property="process.formFile"/>
			</td>
		</tr>
				
	</table>

	<input type="submit" value="Guardar" onclick="guardar();">
	<input type="submit" value="Cancelar" onclick="cancelar();">

</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script type="text/javascript">
function guardar() {
	var action = document.getElementById("action");
	action.value = "save";
}
function cancelar() {
	var action = document.getElementById("action");
	action.value = "list";
}
</script>
