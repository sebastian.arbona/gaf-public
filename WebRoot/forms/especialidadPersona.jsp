<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">Especialidades Personas</div>
<br>

<br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true&paramValue[0]=${param.paramValue[0]}" styleId="cd" >

<table border="0">

 <html:hidden property="entity.especialidad_id"/>
 <html:hidden property="entity.id"/>

<tr><th>Detalle:</th><td><html:textarea styleId="detalle" onkeypress="caracteres(this,4000,event)" property="entity.detalle" cols="40" rows="5"/></td></tr>
	<tr>
		<th>Persona:</th>
		<td>
			<asf:selectpop name="entity.persona_id" title="Sociedad"
				columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
				captions="id,nomb12,nudo12,cuil12" values="id"
				entityName="com.civitas.hibernate.persona.Persona"
				value="${AbmForm.entity.persona_id}" />
		</td>
	</tr>

</table>

<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>



<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
  
