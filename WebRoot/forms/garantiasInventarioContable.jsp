<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Reporte de Inventario Contable de Garantías </div>
<html:form
	action="/actions/process.do?do=process&processName=GarantiasInventarioContable"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.usuario" styleId="usuario"/>
	<html:errors/>
	
	<table>
		<tr>
			<th>Moneda:</th>
			<td>
				<asf:select name="process.idMoneda"
					entityName="com.asf.gaf.hibernate.Moneda"
					listCaption="getAbreviatura" listValues="getId"
					value="ProcessForm.process.idMoneda" enabled="true" />
			</td>
		</tr>
		<tr>
			<th>Tipos de Garantías:</th>
			<td>
				<html:select styleId="tiposGarantias" name="ProcessForm" property="process.tiposGarantias" multiple="true" size="8">
					<html:optionsCollection property="process.tiposGarantiasList" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Fecha de:</th>
			<td>
				 <html:radio name="ProcessForm" property="process.fechaProsValor" value="1"></html:radio>Procesos
				 <html:radio name="ProcessForm" property="process.fechaProsValor" value="2"></html:radio>Valor
			</td>
		</tr>		
		<tr>
			<th>Información a fecha:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaStr" 
				value="${process.fechaStr}"/>
			</td>
		</tr>		
		
		<tr>
			<th colspan="2">
				<html:submit value="Listado en pantalla"></html:submit>
			</th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.resultado">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.resultado" id="bean" export="true"
 			requestURI="${pageContext.request.contextPath}/actions/process.do" pagesize="30">
 			<display:caption>Reporte de Inventario Contable de Garantias</display:caption>
 			<display:column title="Nro Inventario de Garantia" property="idGarantia"/>
 			<display:column title="Proyecto Nro" property="numeroAtencion" />
 			<display:column title="Inversor" property="inversor"/>
 			<display:column title="Cuit / Cuil" property="cuit"  />
 			<display:column title="Linea" property="linea" />
 			<display:column title="Expediente" property="expediente"  />
 			<display:column title="Identificador Unico" property="id"/>
 			<display:column title="Tipo de Garantia" property="tipoGarantia"  />
 			<display:column title="Monto Inversion" property="financiamiento" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<display:column title="Sector de custodia" property="sectorCustodia" />
 			<display:column title="Importe Garantizado" property="importe" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			
 			<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
 				<display:column title="Aforo" property="aforo" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			</logic:notEqual>

 			<display:column title="Importe en Pesos" property="valorPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<display:column title="Fecha Vencimiento Garantia" property="vencimiento" decorator="com.asf.displayDecorators.DateDecorator" />
 			
 			<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
 				<display:column title="Estado Garantia" property="estadoGarantia"  /> 
 			</logic:notEqual>
 			
 			<logic:equal value="2" name="ProcessForm" property="process.fechaProsValor">
 			<display:column title="Fecha Ultimo Estado" property="fechaEstado" decorator="com.asf.displayDecorators.DateDecorator" />
 			</logic:equal>
 			
 			<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
	  			<logic:equal value="1" name="ProcessForm" property="process.fechaProsValor">
	 			<display:column title="Fecha Proceso" property="fechaEstado" decorator="com.asf.displayDecorators.DateDecorator" />
	 			</logic:equal>			
 			</logic:notEqual>

 			<display:column title="Orden Físico" property="ordenFisico"/>
 		</display:table>
	</div>
	</logic:notEmpty>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<script type="text/javascript">
var usuario						= $('usuario');
var processForm = document.getElementById("ProcessForm")
processForm.action += "&process.usuario=" + usuario.value;
</script>


