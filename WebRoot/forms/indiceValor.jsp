<%@ page language="java"%>
<%@page import="com.asf.gaf.hibernate.Bantip"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">
	�ndice Valor
</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true&indice.id=${paramValue[0]}&nombre=${param.nombre}&tipo=${param.tipo}&paramValue[1]=${paramValue[1]}&paramName[1]=fecha&paramComp[1]=>=&paramValue[2]=${paramValue[2]}&paramName[2]=fecha&paramComp[2]=<=" > 

	<table border="0">
		<tr><th>Id:</th><td><asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true" /></td></tr>
				
		<tr><th>�ndice:</th><td><asf:text name="AbmForm" property="entity.indice.nombre" type="long" maxlength="50" readonly="true" value="${param.nombre}" /></td></tr>

		<tr>
			<th>
				Fecha:
			</th>
			<td colspan="3">
				<asf:calendar property="AbmForm.entity.fechaStr" maxlength="10"/>
			</td>
		</tr>

		<tr>
			<th>
				Cantidad:
			</th>
			<td>
				<html:text property='entity.cantidad' readonly="false" />
			</td>
		</tr>

		<tr>
			<th>
				Importe:
			</th>
			<td>
				<html:text property='entity.importe' readonly="false" />
			</td>
		</tr>

		<tr>
			<th>
				Valor:
			</th>
			
			<bean:define value="${param.tipo}" id="tipoTasa"/>
			
			<logic:equal  value="Tasa"  name="tipoTasa">
				<td>
					<html:text property='entity.valorStr' readonly="false" maxlength="8"/>
				</td>
			</logic:equal>
			
			<logic:notEqual value="Tasa"  name="tipoTasa">
				<td>
					<html:text property='entity.valorStr' readonly="false" maxlength="5"/>
				</td>
			</logic:notEqual>
			
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td>
				<html:text property='entity.observaciones' readonly="false" />
			</td>
		</tr>

		<html:hidden property='entity.indice_id' value="${paramValue[0]}" />

	</table>

	<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>	
	
		<button type="button" onclick="cancel();">
			<bean:message key="abm.button.cancel" />
		</button>
		
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>

</html:form>

<script language="javascript" type="text/javascript">
function cancel() 
{
	window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=IndiceValor&filter=true&paramValue[0]=${paramValue[0]}&paramName[0]=indice.id&paramComp[0]==&nombre=${param.nombre}&tipo=${param.tipo}" + 
						"&paramValue[1]=${paramValue[1]}&paramName[1]=fecha&paramComp[1]=>=&paramValue[2]=${paramValue[2]}&paramName[2]=fecha&paramComp[2]=<=";
}
</script>