<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<div class="title">Turnos del �rea:
	${ProcessForm.process.currentUser.unidad.nombre}</div>
<br>
<br>
<logic:present name="ProcessForm" property="process.message">
	<bean:message key="${ProcessForm.process.message.key}"
		arg0="${ProcessForm.process.message.arg0}"
		arg1="${ProcessForm.process.message.arg1}"
		arg2="${ProcessForm.process.message.arg2}"
		arg3="${ProcessForm.process.message.arg3}"
		arg4="${ProcessForm.process.message.arg4}" />
	<logic:notEmpty name="ProcessForm" property="process.message.report">
		<html:form styleId="fExport" method="post"
			action="actions/jreport.do?do=exec" target="_blank">
			<html:hidden property="reportName"
				value="${ProcessForm.process.message.report.reportName}" />
			<html:hidden property="params"
				value="${ProcessForm.process.message.report.params}" />
			<html:hidden property="export"
				value="${ProcessForm.process.message.report.export}" />
		</html:form>
		<script type="text/javascript" language="javascript">
			getElem("fExport").submit();
		</script>
	</logic:notEmpty>
</logic:present>
<logic:present name="ProcessForm" property="process.redirect">
	<script type="text/javascript" language="javascript">
		document.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=SolicitudesHandler&process.accion=nuevo&process.turno.id=${ProcessForm.process.redirect}';
	</script>
</logic:present>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.accion"
		styleId="accion" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<table border="0">
		<tr>
			<th>Nro Atenci�n:</th>
			<td><asf:text name="ProcessForm"
					property="process.numeroatencion" type="Long" maxlength="6"
					attribs="class='numeroAtencion'" /></td>
		</tr>

		<tr>
			<th>�Desea Atender un Nuevo Turno?</th>
		</tr>
	</table>
	<br />
  Turnos entregados en espera: ${ProcessForm.process.cantidadTurnos}
  <br />
	<br />

	<input type="button" value="Buscar" onclick="buscar();" />
	<logic:greaterThan name="ProcessForm" property="process.cantidadTurnos"
		value="0">
		<input type="button" value="Aceptar" onclick="aceptar();" />
	</logic:greaterThan>

	<%--  <html:cancel>--%>
	<%--    <bean:message key="abm.button.reject" />--%>
	<%--  </html:cancel>--%>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script language='javascript'>
	function buscar() {
		var form = $('oForm');
		accion.value = 'buscar';
		form.submit();
	}

	function aceptar() {
		var form = $('oForm');
		accion.value = 'load';
		form.submit();
	}
</script>