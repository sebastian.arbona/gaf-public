<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Pa�s</div>

<br>
<br>
<div class="formContainer">
	<html:form
		action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=false">

		<table border="0">
			<tr>
				<th>ID:</th>
				<td><html:text property="entity.id" readonly="true"
						maxlength="6" value="${AbmForm.entity.id}" /></td>
			</tr>
			<tr>
				<th>C�digo Pa�s:</th>
				<td><html:text property="entity.codigoPais" styleId="codigo"
						maxlength="50" value="${AbmForm.entity.codigoPais}" /></td>
			</tr>
			<tr>
				<th>Nombre:</th>
				<td><html:text property="entity.nombrePais"
						styleId="denominacion" maxlength="30"
						value="${AbmForm.entity.nombrePais}" /></td>
			</tr>
			<tr>
				<th>Abreviatura:</th>
				<td><html:text property="entity.abreviatura"
						styleId="abreviatura" maxlength="10"
						value="${AbmForm.entity.abreviatura}" /></td>
			</tr>
		</table>

		<br>
		<br>
		<asf:security action="/actions/abmAction.do"
			access="do=save&entityName=${param.entityName}">
			<html:submit onclick="return validar();" styleId="bGuardar">
				<bean:message key="abm.button.save" />
			</html:submit>
		</asf:security>
		<html:cancel>
			<bean:message key="abm.button.cancel" />
		</html:cancel>
	</html:form>
</div>
<script type="text/javascript">
	//--------Variables utilitarias------------//
	var msjError = "";
	//--------Controles de la p�gina-----------//
	var codigo = $('codigo');
	var abreviatura = $('abreviatura');
	var denominacion = $('denominacion');
	var bGuardar = $('bGuardar');
	//---------Al Iniciar la p�gina------------//
	if ('${AbmForm.entity.id}' == 0) {
		codigo.focus();
	} else {
		abreviatura.focus();
	}
	//--------funciones-----------//
	function validar() {
		var retorno = true;

		msjError = "";
		if (trim(codigo.value) == '') {
			msjError += "Debe ingresar un C�DIGO para el Pa�s.\n";
		}

		if (trim(denominacion.value) == '') {
			msjError += "Debe ingresar un Nombre para el Pa�s.\n";
		}

		retorno = msjError == "";
		error();

		return retorno;
	}

	/**
	 * Muestra los errores cargados en msjError; y limpia la variable.-
	 */
	function error() {
		var retorno = true;

		if (msjError != "") {
			msjError += "Por favor, intente nuevamente.";
			alert(msjError);
			msjError = "";
			retorno = false;
		}

		return retorno;
	}//fin error.-
</script>