<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="com.civitas.importacion.Fecha"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%pageContext.setAttribute( "fechaActual",DateHelper.getString(new Date()) );%>
<%pageContext.setAttribute( "hora",new GregorianCalendar().get(GregorianCalendar.HOUR )) ;%>
<%pageContext.setAttribute( "min",new GregorianCalendar().get(GregorianCalendar.MINUTE) );%>
<body class=" yui-skin-sam">
  <link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
  <link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
</script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
</script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
</script>

  <div class="title">
   Solicitud de Financiamiento
  </div>
  <br>
  <br>
  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
    <html:hidden name="ProcessForm" property="process.idPersona" styleId="idpersona" />
    <html:hidden property="process.credito.id" />
    <div style="width: 70%" align="left">
      <html:errors />
    </div>
    <table border="0">
      <tr>
        <th>
          Nro Solicitud:
        </th>
        <td colspan="3">
          <asf:text property="ProcessForm.process.credito.numeroAtencion" type="Long" maxlength="6" 
            readonly="true"
          />
        </td>
      </tr>
      <tr>
        <th>
          Fecha:
        </th>
        <td colspan="3">
          <asf:calendar property="ProcessForm.process.credito.fechaSolicitudStr" attribs="class='fechaSolicitudStr'"
            readOnly="${!ProcessForm.process.editable}"
          />
        </td>
      </tr>
      <tr>
        <th>
          L�nea Consultada:
        </th>
        <td colspan="3">
          <asf:select entityName="com.nirven.creditos.hibernate.Linea" attribs="class='Linea'; habilitarContinuar();cambiarMoneda();" listCaption="getId,getNombre"
            listValues="getId" name="process.credito.linea_id" value="${ProcessForm.process.credito.linea_id}"
            enabled="${ProcessForm.process.editable}" filter="activa=true" />
          <asf:text id="moneda" maxlength="80" type="text" property="process.texto" attribs="disabled=disabled"></asf:text>
        </td>
      </tr>
      <tr>
        <th>Cotizacion del dia:</th>
	        <td><asf:text property="ProcessForm.process.credito.cotizaInicial" type="decimal" maxlength="6" 
	            readonly="false" id="cotiza"/>
	        </td>
	    <th>Cotizacion del Proyecto:</th>
	        <td><asf:text property="ProcessForm.process.credito.cotizaInicial" type="decimal" maxlength="6" 
	            readonly="true" id="cotiza"/>
	        </td>
       </tr>
      
      <tr>
        <th>
          Delegaci�n:
        </th>
        <td colspan="3">
          <asf:select entityName="com.nirven.expedientes.persistencia.Unidad" listCaption="getId,getNombre" listValues="getId"
            enabled="false" name="process.credito.unidad_id" value="${ProcessForm.process.currentUser.unidad.id}"
          />
        </td>
      </tr>
      <tr>
        <th>
          Objeto:
        </th>
        <td colspan="3">
          <html:textarea styleId="objeto" onkeypress="caracteres(this,400,event)" property="process.credito.objeto" cols="80" rows="4"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
      </tr>
      <tr>
        <th>
          Observaciones:
        </th>
        <td colspan="3">
          <html:textarea styleId="objeto" onkeypress="caracteres(this,400,event)" property="process.credito.observaciones" cols="80" rows="4"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
      </tr>
          <tr>
    <th colspan="4">
    <table border="0">
        <th>
          Financiamiento Max. Solicitado:
        </th>
        <td >
          <asf:text property="ProcessForm.process.credito.financiamientoStr" type="decimal" maxlength="12"
            value="${ProcessForm.process.credito.financiamientoStr}" attribs="onChange='calcularPorcentajes()'"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
        
        <th>
          Porcentaje Solicitado:
        </th>
        <td>
          <asf:text property="ProcessForm.process.credito.porcentajeSolicitado" type="text" maxlength="6" readonly="true" />
        </td>
        </tr>
        </table>
        </th>
      </tr>
    </table>
    <table class="tabla" id="acarreo2">    
      <tr>
        <th>
          QQ Solicitado:
        </th>
        <td>
          <asf:text property="ProcessForm.process.credito.qqsolicitado" type="decimal" maxlength="12"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
         <th>
          �Es MiPyME?
        </th>
        <td>
          <asf:select name="process.credito.esMiPymeStr"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.credito.esMiPymeStr}"
					listCaption="getDescripcion" listValues="getCodigo"
					filter="categoria = 'credito.mipyme' order by codigo" />
        </td>
      </tr>
      <tr>
         <th>
          Destino:
        </th>
        <td>
          <asf:select name="process.credito.destino"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.credito.destino}"
					listCaption="getDescripcion" listValues="getCodigo"
					filter="categoria = 'credito.destino'  order by codigo" />
        </td>
        <th>
          �Pertenece a Fecovita?
        </th>
        <td>
          <asf:select name="process.credito.fecovitaStr"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.credito.fecovitaStr}"
					listCaption="getDescripcion" listValues="getCodigo"
					filter="categoria = 'credito.fecovita'  order by codigo" />
        </td>
      </tr>
    </table>
<%--    Nuevo--%>
<table class="tabla" id="general">
      <tr id="aportePropio">
        <th>
          Aporte Propio:
        </th>
        <td colspan="3">
          <asf:text property="ProcessForm.process.credito.aportePropio" type="Long" maxlength="12"
            value="${ProcessForm.process.credito.aportePropioStr}" readonly="${!ProcessForm.process.editable}"
            attribs="onChange='calcularPorcentajes()'"
          />
        </td>
        <th>
          Porcentaje Aporte:
        </th>
        <td>
          <asf:text property="ProcessForm.process.credito.porcentajeAporte" type="text" maxlength="6" readonly="true" />
        </td>
      </tr>
      <tr id="montoTotal">
        <th>
          Monto Total:
        </th>
        <td>
          <asf:text property="ProcessForm.process.credito.montoTotal" type="decimal" maxlength="12"
            value="${ProcessForm.process.credito.montoTotalStr}" 
            readonly="true"
          />
        </td>
        <th>
          Porcentaje Total:
        </th>
        <td>
          <asf:text property="ProcessForm.process.credito.porcentajeTotal" type="text" maxlength="6" readonly="true" />
        </td>
      </tr>
      <tr id="amortizacion">
        <th>
          Plazo de Amortizaci�n Capital:
        </th>
        <td colspan="2">
          <asf:text name="ProcessForm" property="process.credito.plazoCapital" type="Long" maxlength="6"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
        <th>Per�odicidad Cuotas Capital</th>
    	<td>
    		<asf:select name="process.credito.frecuenciaCapital"
    			entityName="com.asf.hibernate.mapping.Tipificadores"
    			listCaption="getCodigo,getDescripcion" listValues="getCodigo"
    			value="ProcessForm.process.credito.frecuenciaCapital"
    			filter="categoria='amortizacion.periodicidad' order by codigo"
    			enabled="${ProcessForm.process.editable}"/>
    	</td>
      </tr>
      <tr id="plazo">
        <th>
          Plazo de Amortizaci�n Compensatorio:
        </th>
        <td colspan="2">
          <asf:text name="ProcessForm" property="process.credito.plazoCompensatorio" type="Long" maxlength="6"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
        <th>Per�odicidad Cuotas Inter�s</th>
   		<td>
   			<asf:select name="process.credito.frecuenciaInteres"
   				entityName="com.asf.hibernate.mapping.Tipificadores"
   				listCaption="getCodigo,getDescripcion" listValues="getCodigo"
   				value="ProcessForm.process.credito.frecuenciaInteres"
   				filter="categoria='amortizacion.periodicidad' order by codigo"
   				enabled="${ProcessForm.process.editable}"/>
   		</td>
      </tr>
      <tr id="expediente">
        <th>
          Expediente:
        </th>
        <td colspan="3">
          <html:text name="ProcessForm" property="process.credito.expediente" maxlength="20"
            readonly="${!ProcessForm.process.editable}"
          />
        </td>
      </tr>
      <tr>
        <th>
          Vencimiento de Solicitud:
        </th>
        <td colspan="3">
          <asf:calendar property="ProcessForm.process.credito.vencimientoStr" value="${ProcessForm.process.credito.vencimientoStr}"
            readOnly="${!ProcessForm.process.editable}"
          />
        </td>
      </tr>
      <tr>
        <th>
          Proceso de Aprobaci�n:
        </th>
        <td colspan="3">
        	<div id="iniciarAprob" style="display:none">
	        	<input type="button" value="Iniciar" id="iniciar" />
	       	</div>
	        <logic:present name="ProcessForm" property="process.credito.procesoAprobacion">
	        <bean:define id="objetoi" name="ProcessForm" property="process.credito" type="com.nirven.creditos.hibernate.Objetoi" />
	        	Iniciado el <%= new SimpleDateFormat("dd/MM/yyyy H:m:s").format(objetoi.getProcesoAprobacion().getStart())%>   
	        </logic:present>
	          
        </td>
      </tr>
      
    </table>
<%--Fin Nuevo--%>
    <table class="tabla" id="general">
    </table>
    <br>
    <br>
    <logic:notEqual name="ProcessForm" property="process.accion" value="show">
      <input type="submit" value="Guardar" id="guardar" />
      <input type="button" value="Cancelar" id="cancelar" />
      <div id="tab_vinedos"></div>
    </logic:notEqual>
      <logic:equal name="ProcessForm" property="process.accion" value="show">
      	<input type="button" value="Continuar..." disabled="disabled" id="continuar" onclick="verCredito();"/>
      <div id="tab_vinedos"></div>
    </logic:equal>
    <br/>
    <br/>
    <logic:equal name="ProcessForm" property="process.accion" value="show">
      
      <div id="pestanias" class="yui-navset" style="width: 100%; display: none;">
        
        <ul class="yui-nav">
        
        <logic:notEmpty name="ProcessForm" property="process.isCosechaYAcarreo">
          
          <li id="tab_vinedos">
            
            <a href="#vinedos"><em>Vi�edos</em> </a>
          
          </li>
          <li class="selected" id="tab_requisitos">
            <a href="#requisitos"><em>Requisitos</em> </a>
          </li>
          
         </logic:notEmpty>
         
         <logic:empty name="ProcessForm" property="process.isCosechaYAcarreo">
          
          <li class="selected" id="tab_requisitos">
            <a href="#requisitos"><em>Requisitos</em> </a>
          </li>
         
<%--          <li id="tab_cronograma">--%>
<%--            <a href="#cronograma"><em>Cronograma Desembolso</em> </a>--%>
<%--          </li>--%>
           
          </logic:empty>
         
         <li class="selected" id="tab_garantias">
            <a href="#garantias"><em>Garant�as</em> </a>
         </li>
         
        </ul>
        
        <div class="yui-content">
					<logic:notEmpty name="ProcessForm" property="process.isCosechaYAcarreo">
						<div id="vinedos">
							<iframe 
							src='${pageContext.request.contextPath}/actions/process.do?processName=VinedoProcess&do=process&process.idSolicitud=${ProcessForm.process.idSolicitud}'
								width="100%" height="400" scrolling="auto" style="border: none" allowtransparency="allowtransparency">
							</iframe>
						</div>
						<div id="requisitos">
							<iframe
								src='${pageContext.request.contextPath}/actions/RequisitosAction.do?do=list&entityName=CargaRequisito&idSolicitud=${ProcessForm.process.idSolicitud}&paramValue[0]=${ProcessForm.process.idSolicitud}&paramName[0]=credito.id&paramComp[0]=='
								width="100%" height="400" scrolling="auto" style="border: none"
								allowtransparency="allowtransparency">
							</iframe>
						</div>
						<div id="garantias">
		          			<iframe
				              src='${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.idSolicitud=${ProcessForm.process.idSolicitud}'
				              width="100%" height="400" scrolling="auto" style="border: none" allowtransparency="allowtransparency"
				            >
				            </iframe>
				         </div>
					</logic:notEmpty>

				<logic:empty name="ProcessForm" property="process.isCosechaYAcarreo">

						<div id="requisitos">
							<iframe	src='${pageContext.request.contextPath}/actions/RequisitosAction.do?do=list&entityName=CargaRequisito&idSolicitud=${ProcessForm.process.idSolicitud}&paramValue[0]=${ProcessForm.process.idSolicitud}&paramName[0]=credito.id&paramComp[0]=='
								width="100%" height="400" scrolling="auto" style="border: none" allowtransparency="allowtransparency">
							</iframe>
						</div>
						<div id="garantias">
		          			<iframe
				              src='${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.idSolicitud=${ProcessForm.process.idSolicitud}'
				              width="100%" height="400" scrolling="auto" style="border: none" allowtransparency="allowtransparency"
				            >
				            </iframe>
				         </div>
<%--						<div id="cronograma">--%>
<%--							<iframe src='${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=Desembolso&filter=true&paramValue[0]=${ProcessForm.process.idSolicitud}&paramName[0]=credito.id&paramComp[0]=='--%>
<%--								width="100%" height="400" scrolling="auto" style="border: none" allowtransparency="allowtransparency">--%>
<%--							</iframe>--%>
<%--						</div>						--%>
						
					</logic:empty>
				</div>
      </div>
    </logic:equal>
  </html:form>
  <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>
  <script language="JavaScript" type="text/javascript">
 cambiarMoneda();
  
function cambiarMoneda(){
	var linea = document.getElementById("process.credito.linea_id");
<%--	var linea = $('process.credito.linea_id');--%>
	if(linea == null || linea.value == 0){
		return;
	}else{
		url="${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxLineaMonedaCotizacion&idLinea="+ linea.value;
		retrieveURL(url, cambiarMonedaResponse);
	}
}

function cambiarMonedaResponse(responseText){
	$('moneda').value = responseText; 
} 

jQuery.noConflict();
jQuery(document)
		.ready(
				function($) {

					var formulario = jQuery('#oForm');
					var accion = jQuery('#accion');
					var idpersona = jQuery('#idpersona');

					jQuery('#guardar').click(function() {
						accion.val('guardar');
						idpersona.val("${ProcessForm.process.idPersona}");
						formulario.submit();
					});
					
					jQuery('#iniciar').click(function() {
						if(!confirm('Est� seguro de iniciar el proceso de solicitud?'))
							return false;						
						window.location= '${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=startProcess&process.idSolicitud=${ProcessForm.process.credito.id}';
					});

					jQuery('#cancelar').click(function() {
						accion.val('cancelar');
						formulario.submit();
					});

					jQuery('#process\\.credito\\.linea_id')
							.change(
									function() {

										jQuery
												.ajax( {
													type : "POST",
													url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTipificadorDeLinea&idLinea="
															+ jQuery(
																	'#process\\.credito\\.linea_id')
																	.val(),
													dataType : "text",
													success : function(responseText) {
														seleccionLinea(responseText);
													}
												});
									}).change();
					function ajax_traer_turno() {

						jQuery
								.ajax( {
									type : "POST",
									url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&entityName=Turno&listValues=nombre;linea.id;objeto;mail;observaciones;monto;&filter=numeroAtencion=\'"
											+ $('.numeroAtencion').val()
											+ "\' order by atencion desc ",
									dataType : "xml",
									success : function(responseText) {

										var objeto;

										jQuery(responseText).find('Turno')
												.each(function() {
													objeto = $(this);
													return;
												});

										if (objeto == null) {
											alert('El Turno no se encontr�. Por favor, intente nuevamente.');
											jQuery('#numeroAtencion').val("");
											return false;
										}

										jQuery(
												".Linea option[value='"
														+ objeto.find(
																"linea_id")
																.text() + "']")
												.attr('selected', 'selected');

										jQuery('#objeto').html(
												objeto.find("objeto").text());

										jQuery('.montoTotal').val(
												objeto.find("monto").text());

										jQuery(
												'#process\\.credito\\.linea_id')
												.change(
														function() {

															jQuery
																	.ajax( {
																		type : "POST",
																		url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTipificadorDeLinea&idLinea="
																				+ jQuery(
																						'#process\\.credito\\.linea_id')
																						.val(),
																		dataType : "text",
																		success : function(
																				responseText) {
																			seleccionLinea(responseText);
																		}
																	});
														}).change();

									}
								});

					}												
				});
</script>
 <script language='javascript'>
//pesta�as.-
var tabView = new YAHOO.widget.TabView('pestanias');
var pestanias = $('pestanias');
if ('${ProcessForm.process.idSolicitud}' != "") {
	pestanias.style.display = "";
}
</script>
<script type="text/javascript">				
function seleccionLinea(valor) {
	var tipo1 = valor.split("-")[0];
	var tipo2 = valor.split("-")[1];
	switch (tipo1) {
	case 'cosechaVid':
			mostrar('acarreo2', 'tab_vinedos');
			ocultar('aportePropio','montoTotal');
			ocultar('amortizacion');
			mostrar('general');
			ocultar('plazo');
			ocultar('expediente');
		break;
	default:
			ocultar('acarreo2', 'tab_vinedos');
			mostrar('aportePropio','montoTotal');
			ocultar('amortizacion');
			ocultar('plazo');
			ocultar('expediente');
		break;
	}	
	$('cotiza').value = tipo2;
}
function verCredito(){
	window.location = 
		'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=CreditoProcess&process.action=buscar' +      
        '&process.idObjetoi=${ProcessForm.process.credito.id}';  
}

function habilitarContinuar(){
	var idLinea = ${'process.credito.linea_id'};
	var botonContinuar = ${'continuar'};
	if(idLinea == 1){
		${'continuar'}.disabled="disabled";
	}else{
		${'continuar'}.disabled="enabled";
	}
}
var montoTotal = $('ProcessForm.process.credito.montoTotal');
var financiamiento = $('ProcessForm.process.credito.financiamientoStr');
var aportePropio = $('ProcessForm.process.credito.aportePropio');

var porcentajeAporte = $('ProcessForm.process.credito.porcentajeAporte');
var porcentajeTotal = $('ProcessForm.process.credito.porcentajeTotal');
var porcentajeSolicitado = $('ProcessForm.process.credito.porcentajeSolicitado');

function calcularPorcentajes() {
	montoTotal.value = parseInt(aportePropio.value) + parseInt(financiamiento.value);
	porcentajeAporte.value = parseFloat(
			aportePropio.value / montoTotal.value * 100).toFixed(2) + '%';
	porcentajeSolicitado.value = parseFloat(
			financiamiento.value / montoTotal.value * 100).toFixed(2) + '%';
	porcentajeTotal.value = parseFloat(100).toFixed(2) + '%';
}
</script>
  <iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
    src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
    style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
  ></iframe>