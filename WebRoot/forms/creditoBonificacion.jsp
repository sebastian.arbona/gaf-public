

<%@ page language="java"%>
<%@page import="com.asf.security.BusinessPersistance"%>
<%@page import="com.asf.security.SessionHandler"%>
<%@page import="com.nirven.creditos.hibernate.Objetoi"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

	<bean:define id="idCredito" value="${paramValue[0]}"/>
	<%
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Objetoi credito = (Objetoi)bp.getById(Objetoi.class, new Long(idCredito));
	%>
<div class="title">Bonificaci�n de Cr�dito</div>
<br><br>
<html:errors/>
<html:form action="/actions/bonificacionesAction.do?do=save&entityName=${param.entityName}&from=${param.do}" styleId="AbmForm">
<input type="hidden" name="idIndice" value="${param.idIndice}"/>
<table border="0">
	<bean:define id="filter" name="paramValue" scope="session" type="Object[]"/>
	<html:hidden property="entity.idCredito" value="${filter[0]}"/>	
	<html:hidden property="filter" value="true"/>
	<html:hidden property="entity.id" value="${AbmForm.entity.id}"/>
	

	<tr>
		<th>Cr�dito:</th>
		<td>
			<b><%= credito.getNumeroAtencion() %> - <%= credito.getPersona().getNomb12() %></b>
		</td>
	</tr>
	<tr>
	<th>
		Ente y Convenio de bonificaci�n:
	</th>
	<td>
		<asf:select 		name="convenioId" entityName="ConvenioBonificacion"
                            listCaption="getBanco.getDetaBa,getNombre" listValues="id" 
                            orderBy="banco"                          
                            />
	</td>
	</tr>
	<tr>
		<th>
			L�nea de bonificaci�n:
		</th>
		<td>
			<asf:lselect property="entity.idBonificacion" entityName="Bonificacion"
                             listCaption="nombre" listValue="id"
                             value="${AbmForm.entity.idBonificacion}"
                             linkFK="convenio.id" 
                             linkName="convenioId" orderBy="nombre"
                             nullValue="true"/>
		</td>
	</tr>
	<tr>
		<th>
			Valor Bonificaci�n:
		</th>
		<td>
			<input type="text" id="tasaBonificadaStr" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Tipo de Bonificaci�n:
		</th>
		<td>
			<input type="text" id="tipoDeBonificacion" readonly="readonly"/>
		</td>
	</tr>	
	<tr>
		<th>
			Bonificaci�n m�xima del Convenio:
		</th>
		<td>
			<input type="text" id="bonMax" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Financiamiento m�ximo del Convenio:
		</th>
		<td>
			<input type="text" id="finMax" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Tasa Neta no Inferior a:
		</th>
		<td>
			<input type="text" id="minimoInteresStr" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			D�as de mora:
		</th>
		<td>
			<input type="text" id="diasMora" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Fecha de Vencimiento Real:
		</th>
		<td>
			<input type="text" id="fechaVencimientoRealStr" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Fecha de Vencimiento Aplicaci�n:
		</th>
		<td>
			<input type="text" id="fechaVencimientoAplicacionStr" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			M�ximo Dias Bonificaci�n:
		</th>
		<td>
			<input type="text" id="maxDiasBon" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Bonificaci�n M�xima por Cr�dito:
		</th>
		<td>
			<input type="text" id="maxBonCredito" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Financiamiento M�xima por Cr�dito:
		</th>
		<td>
			<input type="text" id="maxFinanciamientoCredito" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Bonificaci�n M�xima por Persona:
		</th>
		<td>
			<input type="text" id="maxBonPersona" readonly="readonly"/>
		</td>
	</tr>
	<tr>
		<th>
			Financiamiento M�xima por Persona:
		</th>
		<td>
			<input type="text" id="maxFinanciamientoPersona" readonly="readonly"/>
		</td>
	</tr>
</table>
<html:submit><bean:message key="abm.button.save"/></html:submit>
<html:submit onclick="cancelar();"><bean:message key="abm.button.cancel"/></html:submit>
</html:form>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>

<script type="text/javascript">
	$j = jQuery.noConflict();
	
	$j(document).ready(function() {
		$j("#entity\\.idBonificacion").change(
			function() {
			var lineaBonificacion = $j("#entity\\.idBonificacion").val();
			$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=completarDatosBonificacion', 
					'&idBonificacion='+lineaBonificacion,
					function(data) {
						var arrayDatos = data.split("@");
						
						$j("#tasaBonificadaStr").val(arrayDatos[0]);
						$j("#tipoDeBonificacion").val(arrayDatos[1]);
						$j("#minimoInteresStr").val(arrayDatos[2]);
						$j("#fechaVencimientoRealStr").val(arrayDatos[3]);
						$j("#fechaVencimientoAplicacionStr").val(arrayDatos[4]);
						$j("#diasMora").val(arrayDatos[5]);
						$j("#maxDiasBon").val(arrayDatos[6]);
						$j("#maxBonCredito").val(arrayDatos[7]);
						$j("#maxFinanciamientoCredito").val(arrayDatos[8]);
						$j("#minimoInteresStr").val(arrayDatos[9]);
						$j("#bonMax").val(arrayDatos[10]);
						$j("#finMax").val(arrayDatos[11]);
						$j("#maxBonPersona").val(arrayDatos[12]);
						$j("#maxFinanciamientoPersona").val(arrayDatos[13]);
					});
			});
		});

		function cancelar() {
			var form = document.forms["AbmForm"];
			form.action = "${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}";
			return true;
		}
</script>
