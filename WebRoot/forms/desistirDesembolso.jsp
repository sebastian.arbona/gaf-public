<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<logic:equal name="ProcessForm" property="process.desistidoOk"
	value="true">
	<script language="JavaScript" type="text/javascript">
window.close();
window.opener.location.reload(true);
</script>
</logic:equal>

<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>
<br/>
<br/>
<div class="title" style="padding-left:20px;">
	Desistir desembolso
</div>
<html:errors />
<html:form
	action="/actions/process.do?do=process&processName=CreditoDesembolsos"
	styleId="oForm">
	<html:hidden property="process.objetoi.id" value="${idObjetoi}" />
	<html:hidden property="process.idObjetoi" value="${idObjetoi}" />
	<html:hidden property="process.action" styleId="action"
		value="desistir" />
	<html:hidden property="process.desembolso.id" styleId="desembolsoId" />
<br/>
<br/>
	<table border="0" style="padding:20px">
		<tr>
			<th>
			Seleccione el motivo de desistimiento:
			</th>
			<td>
				<select id="estadoDesistimiento"
					value="${ProcessForm.process.estadoDesistimiento}">
					<option value="1">
						Solicitado por la parte tomadora
					</option>
					<option value="2">
						Econom�a de fondos en la aplicaci�n al destino del cr�dito
					</option>
					<option value="3">
						Modificaci�n del destino y/o importe inicial del cr�dito
					</option>
					<option value="4">
						Incumplimiento de otras obligaciones contractuales
					</option>
					<option value="5">
						Otras causales de reducci�n
					</option>
				</select>
			</td>
		</tr>
	</table>
	<div style="width: 100%; text-align: center">
 	<html:submit>Guardar</html:submit>
	<input type="button" value="Cancelar" onclick="window.close();"></input>

</html:form>
<script type="text/javascript">
function desistir() {
	if (confirm('Est� seguro en desistir el desembolso seleccionado?')) {
		return true;
	} else {
		return false;
	}
}
</script>
