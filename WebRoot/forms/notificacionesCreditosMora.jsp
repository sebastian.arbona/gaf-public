<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>

<%@ page import="java.util.Date"%>
<%@ page import="com.asf.util.DateHelper"%>

<%pageContext.setAttribute("fechaHoy", DateHelper.getString(new Date()));%>

<div class="title">Reporte de Proyectos a Notificar</div>
<p>
	<html:form
		action="/actions/process.do?do=process&processName=NotificacionesCreditosMora"
		styleId="oForm">
		<html:hidden property="process.action" name="ProcessForm"
			styleId="action" />

		<div id="linea" title="Linea de Credito" style="visibility: visible;">
			<table style="border: 2px solid rgb(204, 204, 204);" width="352"
				height="30">
				<tr>
					<th>Comportamiento:</th>
					<td><asf:select name="process.codigo"
							entityName="com.asf.hibernate.mapping.Tipificadores"
							listCaption="getCodigo,getDescripcion" listValues="getCodigo"
							filter="categoria = 'comportamientoPago' order by codigo"
							value="${ProcessForm.process.codigo}" nullValue="true"
							nullText="Todos los estados" /></td>
				</tr>
				<tr>
					<th>L�nea de Cr�dito:</th>
					<td><html:select styleId="idsLineas"
							property="process.idsLineas" multiple="true" size="8">
							<html:optionsCollection property="process.lineas" label="nombre"
								value="id" />
						</html:select></td>
				</tr>
				<tr>
					<th>C�digo de Persona:</th>
					<td><asf:selectpop name="process.idPersona" title="Persona"
							columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
							captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
							entityName="com.civitas.hibernate.persona.Persona" value="" /></td>
				</tr>
				<tr>
					<th>Proyecto</th>
					<td><asf:selectpop name="process.numeroAtencion"
							entityName="Objetoi" title="Credito" caseSensitive="true"
							values="numeroAtencion" columns="Proyecto,Tomador"
							captions="numeroAtencion,persona.nomb12" cantidadDescriptors="1"
							value="${ProcessForm.process.numeroAtencion > 0 ? ProcessForm.process.numeroAtencion : null}" />
					</td>
				</tr>
				<tr>
					<th>Fecha de Mora</th>
					<td><asf:calendar property="ProcessForm.process.fechaMora"
							value="${process.fechaMora}">
						</asf:calendar></td>
				</tr>
			</table>
		</div>
		<input type="button" value="Listar" onclick="listar();">

		<br />
		<br />
		<div class="grilla">
			<display:table name="ProcessForm" property="process.creditosBean"
				export="true" id="reportTable"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column media="excel xml pdf"
					property="credito.numeroAtencion" title="Proyecto" />
				<display:column title="Proyecto" media="html">
					<a
						href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.credito.id}&process.action=buscar&process.personaId=">${reportTable.credito.numeroAtencion}</a>
				</display:column>
				<display:column property="credito.numeroCredito"
					title="Nro Credito Ant." sortable="true"></display:column>
				<display:column property="departamento" title="Departamento"
					sortable="true" />
				<display:column property="credito.persona.idpersona"
					title="ID Titular" sortable="true" />
				<display:column property="credito.persona.nomb12" title="Titular"
					sortable="true" />
				<display:column property="credito.persona.cuil12" title="CUIT"
					sortable="true" />
				<display:column property="credito.linea.nombre" title="Linea"
					sortable="true" />
				<display:column
					property="credito.objetoiComportamientoActual.nombreComportamiento"
					title="Estado" sortable="true" />
				<display:column property="credito.estadoActual.estado.nombreEstado"
					title="Etapa" sortable="true" />
				<display:column property="credito.fechaUltimoVencimiento"
					title="Fecha De Mora" sortable="true" />
				<display:column property="credito.diasDeAtraso"
					title="D�as de atraso" sortable="true" />
				<display:column property="deudaTotal" title="Importe Total Adeudado"
					sortable="true"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
				<display:column property="deudaExigible" title="Deuda Exigible"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
				<display:column property="credito.fechaUltimoPago"
					title="Fecha �ltima Cobranza"
					decorator="com.asf.displayDecorators.DateDecorator"></display:column>
				<display:column property="credito.linea.moneda.denominacion"
					title="Moneda"></display:column>
				<display:column property="credito.linea.moneda.cotizacion"
					title="Cotizacion"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
				<display:column title="Bonificado">
					<logic:equal name="reportTable" property="bonificado" value="true">
			Si
		</logic:equal>
					<logic:equal name="reportTable" property="bonificado" value="false">
			No
		</logic:equal>
				</display:column>
				<display:column property="credito.montoValoresCartera"
					title="Valores Cartera" sortable="true"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
				<display:column title="Tipo �ltima Notificaci�n">
					<logic:notEmpty name="reportTable"
						property="credito.ultimaNotificacion">
				${reportTable.credito.ultimaNotificacion.tipoAviso}
			</logic:notEmpty>
				</display:column>
				<display:column title="Observaci�n �ltima Notificaci�n">
					<logic:notEmpty name="reportTable"
						property="credito.ultimaNotificacion">
				${reportTable.credito.ultimaNotificacion.observaciones}
			</logic:notEmpty>
				</display:column>
				<display:column title="Fecha �ltima Notificaci�n">
					<logic:notEmpty name="reportTable"
						property="credito.ultimaNotificacion">
				${reportTable.credito.ultimaNotificacion.fechaCreadaStr}
			</logic:notEmpty>
				</display:column>
				<display:column title="Garant�a SGR">
					<logic:equal name="reportTable" property="garantiaSGR" value="true">
			Si
		</logic:equal>
					<logic:equal name="reportTable" property="garantiaSGR"
						value="false">
			No
		</logic:equal>
				</display:column>
				<display:column property="credito.fechaBajaBonificacion"
					title="Fecha Ca�da Bonificaci�n" sortable="true" />
				<display:column title="Seguro Vencido">
					<logic:equal name="reportTable" property="seguroVencido"
						value="true">
			Si
		</logic:equal>
					<logic:equal name="reportTable" property="seguroVencido"
						value="false">
			No
		</logic:equal>
				</display:column>
				<display:column title="�ltima Pr�rroga">
					<logic:notEmpty name="reportTable"
						property="credito.ultimaProrroga">
				${reportTable.credito.ultimaProrroga.id}
			</logic:notEmpty>
				</display:column>
				<display:column title="Estado �ltima Pr�rroga">
					<logic:notEmpty name="reportTable"
						property="credito.ultimaProrroga">
				${reportTable.credito.ultimaProrroga.nombreEstado}
			</logic:notEmpty>
				</display:column>
				<display:column property="auditoriaFinalPosee" title="Aplic� fondos"
					sortable="true" />

			</display:table>
		</div>
	</html:form>

	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

	<script type="text/javascript">
		function listar() {
			var accion = document.getElementById('action');
			accion.value = "listar";
			var formulario = document.getElementById('oForm');
			formulario.submit();
		}
	</script>