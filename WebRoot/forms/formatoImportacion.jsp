<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Formato de Importaci�n</div>
<br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
    
    <table border="0">
        <tr><th>C�digo:</th><td><html:text property="entity.id" readonly="true" value="${ AbmForm.entity.idformato }"/></td></tr>
        <tr><th>Detalle:</th><td><html:text property="entity.detalle" maxlength="50"/></td></tr>
        <tr><th>Archivo:</th><td><html:text property="entity.archivo" maxlength="500"/></td></tr>
        <%--tr><th>C�digo Detalle:</th><td><html:text property="entity.codigodetalle" maxlength="50"/></td></tr>
        <tr><th>C�digo Controlador:</th><td><html:text property="entity.codigocontrolador" maxlength="50"/></td></tr--%>
        <tr><th>C�digo de Secciones Unificadas:</th><td><html:text property="entity.seccionesunificadas" maxlength="50"/></td></tr>
        <%--tr><th>Fecha de Inicio Fechas Julianas:</th><td><asf:calendar property="AbmForm.entity.fechaJulianaStr" /></td></tr--%>
    </table>
    
    <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
        <html:submit><bean:message key="abm.button.save"/></html:submit>
    </asf:security>	
    
    <html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
