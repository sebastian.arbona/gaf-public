<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<div class="yui-skin-sam">
	<div class="title">Bonificación de Tasa Bancaria</div>
	<html:form
		action="/actions/process.do?do=process&processName=BonTasaProcess"
		styleId="ProcessForm">
		<html:hidden property="process.accion" styleId="accion" />
		<html:hidden property="process.idPersona" styleId="persona" />

		<div class="grilla">
			<table>
				<tr>
					<th>Nro Bonificación</th>
					<th>Titular</th>
					<th>CUIL/CUIT</th>
					<th>Expediente</th>
					<th>Fecha Solicitud</th>
					<th>Linea</th>
					<th>Destino de Fondos</th>
					<th>Estado</th>
				</tr>
				<tr>
					<td>${ProcessForm.process.bonificacion.numeroBonificacion}</td>
					<td><logic:notEmpty name="ProcessForm"
							property="process.bonificacion.persona.nomb12">
						${ProcessForm.process.bonificacion.persona.nomb12}
					</logic:notEmpty> <logic:notEmpty name="ProcessForm"
							property="process.bonificacion.persona.razonSocial">
							<logic:empty name="ProcessForm"
								property="process.bonificacion.persona.nomb12">
							${ProcessForm.process.bonificacion.persona.razonSocial}
						</logic:empty>
						</logic:notEmpty></td>
					<td>${ProcessForm.process.bonificacion.persona.cuil12}</td>
					<td><logic:notEmpty name="ProcessForm"
							property="process.bonificacion.expediente">
						${ProcessForm.process.bonificacion.expediente}
					</logic:notEmpty></td>
					<td>${ProcessForm.process.bonificacion.fechaSolicitud}</td>
					<td>${ProcessForm.process.linea.nombre}</td>
					<td>${ProcessForm.process.bonificacion.objeto}</td>
					<td>${ProcessForm.process.estadoActual.estado.nombreEstado}</td>
				</tr>
			</table>
			<div id="pestanias" class="yui-navset" style="width: 100%;">
				<ul class="yui-nav">
					<li class="selected" id="tab_requisitos"><a href="#requisitos"
						onclick="irARequisitos();"><em>Requisitos</em> </a></li>
					<logic:equal value="true" name="ProcessForm"
						property="process.requisitosCompletos">
						<li><a href="#titulares" onclick="irATitulares();"><em>Titulares</em>
						</a></li>
						<li><a href="#domicilioBonTasa" onclick="irADomicilio();"><em>Domicilios</em></a>
						</li>
						<li class="selected"><a href="#datosgenerales"
							onclick="irADatosGenerales();"><em>Datos Generales</em></a></li>
						<li><a href="#datosfinancieros"
							onclick="irADatosFinancieros();"><em>Datos Financieros</em></a></li>
						<li><a href="#desembolsos" onclick="irADesembolsos();"><em>Desembolsos</em>
						</a></li>
						<li><a href="#cuotas" onclick="irACuotas();"><em>Cuotas</em></a>
						</li>
						<li><a href="#estados" onclick="irAEstados();"><em>Estados</em></a>
						</li>
						<li><a href="#resoluciones" onclick="irAResoluciones();"><em>Resoluciones</em></a>
						</li>
						<%--					<li>--%>
						<%--						<a href="#pagos" onclick="irAPagos();"><em>Pagos</em></a>--%>
						<%--					</li>--%>
					</logic:equal>
				</ul>

				<div class="yui-content">
					<div id="requisitos">
						<iframe
							src="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaRequisitos&process.idBonTasa=${ProcessForm.process.idBonTasa}&process.accion=listar&process.esPersona=${ProcessForm.process.esPersona}"
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency" id="frameRequisitos">
						</iframe>
					</div>
					<logic:equal value="true" name="ProcessForm"
						property="process.requisitosCompletos">
						<div id="titulares">
							<iframe
								src="${pageContext.request.contextPath}/actions/process.do?do=process&processName=TitularesBonTasa&process.idBonTasa=${ProcessForm.process.idBonTasa}"
								width="100%" height="400" scrolling="auto" style="border: none;"
								id="frameTitulares" allowtransparency="allowtransparency">
							</iframe>
						</div>
						<div id="domicilioBonTasa">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDomicilioBonTasa"> </iframe>
						</div>
						<div id="datosgenerales">
							<iframe
								src="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaDatosGenerales&process.idBonTasa=${ProcessForm.process.idBonTasa}"
								width="100%" height="400" scrolling="auto" style="border: none"
								id="frameDatosGenerales" allowtransparency="allowtransparency">
							</iframe>
						</div>

						<div id="datosfinancieros">
							<iframe width="100%" height="1000" scrolling="auto"
								style="border: none;" id="frameDatosFinancieros"
								allowtransparency="allowtransparency"> </iframe>
						</div>
						<div id="desembolsos">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDesembolsos"
								allowtransparency="allowtransparency"> </iframe>
						</div>
						<div id="cuotas">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameCuotas"
								allowtransparency="allowtransparency"> </iframe>
						</div>
						<div id="estados">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameEstados"
								allowtransparency="allowtransparency"> </iframe>
						</div>
						<div id="resoluciones">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameResoluciones"
								allowtransparency="allowtransparency"> </iframe>
						</div>
						<div id="pagos">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="framePagos"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</div>
			</div>
		</div>
	</html:form>
	<script language="javascript" type="text/javascript">
		//pestañas.-
		var tabView = new YAHOO.widget.TabView('pestanias');
		var frameDomicilioBonTasa = $('frameDomicilioBonTasa');
		var frameCuotas = $('frameCuotas');
		var frameCuentaCorriente = $('frameCuentaCorriente');
		var frameDatosFinancieros = $('frameDatosFinancieros');
		var frameDatosGenerales = $('frameDatosGenerales');
		var frameEstados = $('frameEstados');
		var frameResoluciones = $('frameResoluciones');
		var framePagos = $('framePagos');
		var frameRequisitos = $('frameRequisitos');
		var frameTitulares = $('frameTitulares');
		var frameDesembolsos = $('frameDesembolsos');

		function irADomicilio() {
			var src = '';

			//if(frameDomicilioBonTasa.src == "")
			//{
			src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=DomicilioBonTasa';
			src += '&process.accion=listar';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

			frameDomicilioBonTasa.src = src;
			//}
		}

		function irARequisitos() {
			var src = '';

			//if(frameRequisitos.src == "")
			//{
			src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=BonTasaRequisitos';
			src += '&process.accion=listar';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

			frameRequisitos.src = src;
			//}
		}

		function irATitulares() {
			var src = '';
			//if(frameTitulares.src == "")
			//{
			src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=TitularesBonTasa';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

			frameTitulares.src = src;
			//}
		}
		function irACuotas() {
			var src = '';
	<%--			if(frameCuotas.src == "")--%>
		
	<%--			{--%>
		src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=BonTasaCuotas';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

			frameCuotas.src = src;
	<%--			}--%>
		}

		function irADatosFinancieros() {
			var src = '';
	<%--			if(frameDatosFinancieros.src == "")--%>
		
	<%--			{--%>
		src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=BonTasaDatosFinancieros';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

			frameDatosFinancieros.src = src;
	<%--			}--%>
		}

		function irADatosGenerales() {
			var src = '';

			if (frameDatosGenerales.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=BonTasaDatosGenerales';
				src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

				frameDatosGenerales.src = src;
			}
		}

		function irAEstados() {
			var src = '';
			if (frameEstados.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=EstadosBonTasa';
				src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

				frameEstados.src = src;
			}
		}

		function irAResoluciones() {
			var src = '';
			//			if(frameResoluciones.src == "")
			//			{
			src += '${pageContext.request.contextPath}/actions/process.do?';
			src += 'do=process&processName=ResolucionesBonTasa';
			src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}&process.accion=listResolucion';

			frameResoluciones.src = src;
			//			}
		}

		function irAPagos() {
			var src = '';
			if (framePagos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=BonTasaProcess&process.accion=pagos';
				src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

				framePagos.src = src;
			}
		}

		function irADesembolsos() {
			var src = '';
			if (frameDesembolsos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=BonTasaDatosFinancieros&process.accion=mostrarDesembolsos';
				src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

				frameDesembolsos.src = src;
			}
		}
	</script>
</div>