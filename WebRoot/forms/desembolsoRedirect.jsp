<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<logic:notEmpty name="redirectDesembolso">
<script type="text/javascript">
	var url = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoDesembolsos";
	url += "&process.idObjetoi=${idObjetoi}";
	window.location = url;
</script>
</logic:notEmpty>

<html:errors/>

<a href="javascript:history.go(-1)">Volver</a>