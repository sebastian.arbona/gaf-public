<%@ page language="java"%>
<%@ page import="com.asf.util.DateHelper"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.GregorianCalendar"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" value="calcular" styleId="accion" />
  <div style="width: 70%" align="left">
    <html:errors />
  </div>
  <table>
    <tr>
      <th>
        Tasa:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.tasaAmortizacion" maxlength="5" />
      </td>
    </tr>
    <tr>
      <th>
        Método Amortización:
      </th>
      <td>
        <asf:select name="process.metodoAmortizacion" entityName="com.asf.hibernate.mapping.Tipificadores" value="${ProcessForm.process.metodoAmortizacion}" listCaption="getDescripcion" listValues="getCodigo" filter="categoria = 'amortizacion.metodo' order by descripcion" />
      </td>
    </tr>
    <tr>
      <th>
        Plazo de Amortización Capital:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.plazoCapital" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Plazo de Amortización Compensatorio:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.plazoCompensatorio" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Periodicidad Capital:
      </th>
      <td>
        <asf:select name="process.periodicidadCapital" entityName="com.asf.hibernate.mapping.Tipificadores" value="${ProcessForm.process.periodicidadCapital}" listCaption="getDescripcion" listValues="getCodigo" filter="categoria = 'amortizacion.periodicidad' order by descripcion" />
      </td>
    </tr>
    <tr>
      <th>
        Periodicidad Compensatorio:
      </th>
      <td>
        <asf:select name="process.periodicidadCompensatorio" entityName="com.asf.hibernate.mapping.Tipificadores" value="${ProcessForm.process.periodicidadCompensatorio}" listCaption="getDescripcion" listValues="getCodigo" filter="categoria = 'amortizacion.periodicidad' order by descripcion" />
      </td>
    </tr>
    <tr>
      <th>
        Primer Vencimiento Capital:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.primerVencimientoCapitalStr" value="${ProcessForm.process.primerVencimientoCapitalStr}" />
      </td>
    </tr>
    <tr>
      <th>
        Primer Vencimiento Compensatorio:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.primerVencimientoCompensatorioStr" value="${ProcessForm.process.primerVencimientoCompensatorioStr}" />
      </td>
    </tr>
    <tr>
      <th>
        Segundo Vencimiento Capital:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.segundoVencimientoCapitalStr" value="${ProcessForm.process.segundoVencimientoCapitalStr}" />
      </td>
    </tr>
    <tr>
      <th>
        Segundo Vencimiento Compensatorio:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.segundoVencimientoCompensatorioStr" value="${ProcessForm.process.segundoVencimientoCompensatorioStr}" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha Desembolso 1:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.fecha1Str" value="${ProcessForm.process.fecha1Str}" />
      </td>
      <th>
        Importe Desembolso 1:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.importe1" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha Desembolso 2:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.fecha2Str" value="${ProcessForm.process.fecha2Str}" />
      </td>
      <th>
        Importe Desembolso 2:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.importe2" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha Desembolso 3:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.fecha3Str" value="${ProcessForm.process.fecha3Str}" />
      </td>
      <th>
        Importe Desembolso 3:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.importe3" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha Desembolso 4:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.fecha4Str" value="${ProcessForm.process.fecha4Str}" />
      </td>
      <th>
        Importe Desembolso 4:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.importe4" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha Desembolso 5:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.fecha5Str" value="${ProcessForm.process.fecha5Str}" />
      </td>
      <th>
        Importe Desembolso 5:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.importe5" maxlength="10" />
      </td>
    </tr>
    <tr>
      <th>
        Fecha Desembolso 6:
      </th>
      <td>
        <asf:calendar property="ProcessForm.process.fecha6Str" value="${ProcessForm.process.fecha6Str}" />
      </td>
      <th>
        Importe Desembolso 6:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.importe6" maxlength="10" />
      </td>
    </tr>
  </table>
  <br>
  <html:submit value="Calcular" />
  <input type="button" value="Cancelar" onclick="cancelar();">
  <br>
  <br>
  <logic:notEmpty name="ProcessForm" property="process.cuotas">
    <display:table name="ProcessForm" property="process.cuotas" pagesize="1000" id="reportTable" export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
      <display:caption>Cuotas:</display:caption>
      <display:column title="Numero" property="numero" sortable="false" />
      <display:column title="Desembolso" property="desembolso" sortable="false" />
      <display:column title="Vencimiento" property="fechaVencimientoStr" sortable="false" />
      <display:column title="Vencimiento Anterior" property="fechaVencimientoAnteriorStr" sortable="false" />
      <display:column title="Capital" property="capital" sortable="false" />
      <display:column title="Saldo Capital" property="saldoCapital" sortable="false" />
      <display:column title="Compensatorio" property="compensatorio" sortable="false" />
      <display:column title="Moratorio" property="moratorio" sortable="false" />
      <display:column title="Punitorio" property="punitorio" sortable="false" />
      <display:column title="DiasPeriodo" property="diasPeriodo" sortable="false" />
      <display:column title="DiasTransaccion" property="diasTransaccion" sortable="false" />
    </display:table>
  </logic:notEmpty>
  <logic:empty name="ProcessForm" property="process.cuotas">
        No se pudieron calcular cuotas.
    </logic:empty>
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>
<script language="javascript">
var accion = $('accion');
var formulario = $('oForm');

function cancelar() {
	accion.value = "cancelar";
	formulario.submit();
}//fin cancelar.-
</script>