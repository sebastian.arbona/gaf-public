<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">Importaci&oacute;n de Planillas</div>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br>
<html:form
	action="/actions/process.do?do=process&processName=MigracionPlanillas"
	styleId="ProcessForm" enctype="multipart/form-data">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="importar"
		styleId="action" />

	<table border="0">
		<tr>
			<th>Formato de Carga:</th>
			<td><asf:select name="process.formatoCarga" entityName=""
					listCaption="Generico,Mendoza Fiduciaria,Garantia Seguro,FTyC Personas"
					listValues="formato1,formato2,formato3,formato4"
					attribs="onchange='cambiar( this.value )'" />
		</tr>
		<tr>
			<th>Archivo:</th>
			<td><html:file styleId="file" property="process.archivo"
					accept=".xlsx" /></td>
		</tr>
		<tr>
			<th colspan="2"><input type="button" value="Procesar Archivo"
				id="idProcesarArchivo" onclick="procesarArchivo();" />
		</tr>
	</table>

</html:form>

<script type="text/javascript">
	var form = $('ProcessForm');
	var action = $('action');

	function procesarArchivo() {
		action.value = 'importar';
		form.submit();
	}

	function cambiar(valor) {
		if (valor == "formato3" || valor == "formato4") {
			ocultar('linea');
			ocultar('fideicomiso');
		} else {
			mostrar('linea');
			mostrar('fideicomiso');
		}
	}
</script>