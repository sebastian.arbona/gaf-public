<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 


<div class="title">Comportamiento de Pago</div>
<br/>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion"/>
	
	<input type="button" value="Actualizar Ahora" id="actualizarId" onclick="actualizar();"/>
	<br/>
	Ultima Actualizaci�n: ${ProcessForm.process.ultimoInicio} - ${ProcessForm.process.ultimoFin}
	
	<br/>
	<br/>
	<div style="width: 70%" align="left">
	  <html:errors />
	</div>
	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<br/>
		<display:table name="ProcessForm" property="process.creditos" 
    			id="credito" class="com.asf.cred.business.ObjetoiDTO">
    		<display:caption title="Cr�ditos con cuotas vencidas"></display:caption>
            <display:column title="Nro Solicitud" media="html" sortable="true">
                <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ActualizacionCreditos&process.accion=listar" target="_parent">${ credito.numeroAtencion }</a>
            </display:column>
            <display:column title="Titular" property="titular.nomb12" sortable="true"/>
            <display:column title="L�nea" property="linea" sortable="true"/>
            <display:column title="Expediente N�" property="expediente" sortable="true"/>
            <display:column title="Estado" property="estado" sortable="true"/>
            <display:column title="Comportamiento" property="comportamiento" sortable="true"/>
            <display:column title="Deuda" property="deuda" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
            <display:column title="D�as Atraso" property="diasAtraso" sortable="true"/>
            
            <display:column title="Selecci�n">
            	<input  type="checkbox" name="requisito-${credito.idObjetoi}" value="${credito.idObjetoi}"/>
			</display:column>
    	</display:table>
    </logic:notEmpty>
    <br/>
    <div class="title">Actualizaci�n Masiva de Etapa</div>
    <br/>
    <th>
		D�as de atraso, entre
    </th>
    <asf:text maxlength="4" type="long" property="process.desde" name="ProcessForm" attribs="'size=2'"/>
    y
    <asf:text maxlength="4" type="long" property="process.hasta" name="ProcessForm" attribs="'size=2'"/>
    d�as.
    <input type="button" value="Listar" id="listarId" onclick="listar();"/>
    <br/>
    <br/>
	<tr>
        <th>
          Estado:
        </th>
        <td>
          <asf:select   entityName="com.civitas.hibernate.persona.Estado" listCaption="idEstado,nombreEstado" attribs="class='idEstado'"
            listValues="idEstado" name="process.idEstado" value="${ProcessForm.process.idEstado}" 
            filter="nombreEstado='GESTION EXTRAJUDICIAL' OR nombreEstado='GESTION JUDICIAL'" nullValue="true" nullText="&lt;&lt;Seleccione Estado&gt;&gt;"
          />
        </td>
      </tr>
      <input type="button" value="Actualizar Selecci�n" id="actualizarSeleccionId" onclick="actualizarSeleccion();"/>
</html:form>

<script type="text/javascript">
	var accion = $('accion');
	var estado = $('process.idEstado');
	var formulario = $('oForm');
	
function listar(){
	window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
		"width=450, height=180");
	accion.value = 'listar';
	formulario.submit();
}
function actualizar(){
	window.location = "${pageContext.request.contextPath}/actions/process.do?do=process" + 
	"&processName=${param.processName}" +
	"&process.accion=actualizar";
}
function actualizarSeleccion(){
	if(estado != null && estado.value != null && estado.value != ''){
		accion.value = 'actualizarSeleccion';
		formulario.submit();
	} else {
		alert('No se ha seleccionado el estado.');
	}
}
</script>