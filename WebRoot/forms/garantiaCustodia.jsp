<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/links.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js" ></script>
<script language="javascript" type="text/javascript">
	 /**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
  }//fin actualizarLista.-
</script>
																								
<div class="title" style="margin-bottom: 30px">
<logic:equal value="true" name="ProcessForm" property="process.alta">
	Alta en Custodia
</logic:equal>
<logic:equal value="false" name="ProcessForm" property="process.alta">
	Traslado
</logic:equal>
</div>
<html:form action="/actions/process.do?do=process&processName=GarantiaCustodiaProcess" styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="guardar"/>
	<html:errors/>

	<table style="margin-top: 30px; width: 100%">
		<logic:equal value="false" name="ProcessForm" property="process.alta">
		<tr>
			<th>Nro de Inventario</th>
			<td>${ProcessForm.process.custodia.nroInventario}</td>
		</tr>
		<tr>
			<th>Orden</th>
			<td>${ProcessForm.process.custodia.orden}</td>
		</tr>
		<tr>
			<th>Fecha Movimiento</th>
			<td>
				<asf:calendar property="ProcessForm.process.custodia.fechaIngresoStr" 
					value="${process.custodia.fechaIngresoStr}"/>
			</td>
		</tr>
		</logic:equal>
		<tr>
			<th>
				<logic:equal value="false" name="ProcessForm" property="process.alta">
				Sector Destino
				</logic:equal>
				<logic:equal value="true" name="ProcessForm" property="process.alta">
				Sector depositario
				</logic:equal>
			</th>
			<td>
				<asf:select name="process.custodia.sectorId"
					entityName="com.nirven.expedientes.persistencia.Unidad"
					value="${ProcessForm.process.custodia.sectorId}"
					listCaption="getCodigo,getNombre" listValues="getId"
					filter="activo = true order by nombre"></asf:select>
			</td>
		</tr>
		<tr>
			<th>
				<logic:equal value="false" name="ProcessForm" property="process.alta">
				Responsable Destino
				</logic:equal>
				<logic:equal value="true" name="ProcessForm" property="process.alta">
				Responsable Custodia
				</logic:equal>
			</th>
			<td>
				<asf:selectpop name="process.responsableId" title="Persona"
					columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
					captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
					entityName="com.civitas.hibernate.persona.Persona"
					value="${ProcessForm.process.responsableId}" />
				(Codigo-Apellido y Nombre-Documento)
			</td>
		</tr>
		<logic:equal value="true" name="ProcessForm" property="process.alta">
		<tr>
			<th>Fecha Ingreso</th>
			<td>
				<asf:calendar property="ProcessForm.process.custodia.fechaIngresoStr" 
					value="${process.custodia.fechaIngresoStr}"/>
			</td>
		</tr>
		</logic:equal>
		<tr>
			<th>Ubicaci&oacute;n F&iacute;sica</th>
			<td>
				<html:text name="ProcessForm" property="process.custodia.ubicacion"/>
			</td>
		</tr>
		<tr>
			<th>Orden F&iacute;sico</th>
			<td>
				<html:text name="ProcessForm" property="process.custodia.ordenFisico" maxlength="10"/>
			</td>
		</tr>
		<logic:equal value="false" name="ProcessForm" property="process.alta">
		<tr>
			<th>Fecha L&iacute;mite Pr&eacute;stamo Instrumento</th>
			<td>
				<asf:calendar property="ProcessForm.process.custodia.fechaDevolucionStr" 
					value="${process.custodia.fechaDevolucionStr}"/>
			</td>
		</tr>
		<tr>
			<th>Expediente ADE</th>
			<td>
				<asf:selectpop name="process.custodia.expediente" entityName="DocumentoADE" 
						value="${ProcessForm.process.custodia.expediente}" 
						caseSensitive="true" values="identificacion" columns="N�,Asunto,Iniciador,CUIT/DNI" captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
						size1="25" size2="70" cantidadDescriptors="2" />
			</td>
		</tr>
		</logic:equal>
		<tr>
			<th>Observaciones</th>
			<td>
				<html:textarea name="ProcessForm" property="process.custodia.observaciones"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Guardar"></html:submit>
				<input type="button" value="Cancelar" onclick="cancelar();"/>
			</th>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
	function cancelar() {
		self.location = "${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.accionStr=MODIFICAR_GARANTIA&process.idSolicitud=${ProcessForm.process.objetoiGarantia.objetoi.id}&process.oGarantiaId=${ProcessForm.process.objetoiGarantia.id}";
	}
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>