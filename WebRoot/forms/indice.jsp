<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script language="javascript" type="text/javascript">
	function cambiaTipo(tipo) {
		if (tipo == "1" || tipo == "4") {
			var valor;
			var fecha = $('fecha');
			fecha.style.display = 'none';
			var valorInicial = $('valorInicial');
			valorInicial.style.display = 'none';
			var valorInicialTasa = $('valorInicialTasa');
			valorInicialTasa.style.display = '';
			var diasPromedio = $('diasPromedio');
			diasPromedio.style.display = 'none';
			var valorMaximo = $('valorMaximo');
			valorMaximo.style.display = 'none';
		} else if (tipo == "2") {
			var fecha = $('fecha');
			fecha.style.display = '';
			var valorInicialTasa = $('valorInicialTasa');
			valorInicialTasa.style.display = 'none';
			var valorInicial = $('valorInicial');
			valorInicial.style.display = '';
			var diasPromedio = $('diasPromedio');
			diasPromedio.style.display = 'none';
			var valorMaximo = $('valorMaximo');
			valorMaximo.style.display = 'none';
		} else if (tipo == "3") {
			var fecha = $('fecha');
			fecha.style.display = 'none';
			var valorInicial = $('valorInicial');
			valorInicial.style.display = 'none';
			var valorInicialTasa = $('valorInicialTasa');
			valorInicialTasa.style.display = '';
			var diasPromedio = $('diasPromedio');
			diasPromedio.style.display = '';
			var valorMaximo = $('valorMaximo');
			valorMaximo.style.display = '';
		}
	}
</script>

<div class="title">�ndice o Tasa</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>Tipo:</th>
			<td><asf:select name="entity.tipo" entityName="Tipificadores"
					listCaption="descripcion" listValues="codigo"
					filter="categoria='TipoIndice'"
					value="${AbmForm.entity.tipo} order descripcion" nullValue="true"
					nullText="Seleccione tipo"
					attribs="onchange='cambiaTipo(this.value)' ">
				</asf:select></td>
		</tr>
		<tr>
			<th>Aplica en:</th>
			<td><asf:select name="entity.aplicaEn"
					entityName="Tipificadores" listCaption="descripcion"
					listValues="codigo" filter="categoria='indice.aplica'"
					value="${AbmForm.entity.aplicaEn}" nullValue="true"
					nullText="Seleccione donde aplica">
				</asf:select></td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><html:text property="entity.nombre" maxlength="50" /></td>
		</tr>
		<tr id="valorInicial" style="display: none;">
			<th>Valor Inicial:</th>
			<td><html:text property="entity.valorInicialStr" maxlength="5" /></td>
		</tr>
		<tr id="valorInicialTasa" style="display: none;">
			<th>Valor Inicial:</th>
			<td><html:text property="entity.valorInicialStr" maxlength="8" /></td>
		</tr>
		<tr id="diasPromedio" style="display: none;">
			<th>Dias Promedio:</th>
			<td><html:text property="entity.diasPromedio" maxlength="2" /></td>
		</tr>
		<tr id="valorMaximo" style="display: none;">
			<th>Valor M&aacute;ximo:</th>
			<td><html:text property="entity.valorMaximoStr" maxlength="8" /></td>
		</tr>
		<tr id="fecha" style="display: none;">
			<th>Fecha base:</th>
			<td><asf:calendar property="entity.fechaBaseStr" /></td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>

	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
</html:form>

