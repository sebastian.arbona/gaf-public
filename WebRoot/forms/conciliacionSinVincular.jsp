<%@page import="com.asf.util.DateHelper"%>
<%@page import="com.nirven.creditos.hibernate.DetalleRecaudacion"%>
<%@page import="java.util.List"%>
<%@page import="com.asf.cred.business.ConciliacionRecaudaciones"%>
<%@page import="com.asf.util.DoubleHelper"%>
<%@page import="com.asf.gaf.hibernate.Recaudacion"%>
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">
	Registros del txt sin vincular a un Extracto Bancario
</div>
<html:form action="/actions/process.do?do=process&processName=ConciliacionRecaudaciones" styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	
	<table width="100%">
		<tr>
			<th>Titular</th>
			<th>Proyecto</th>
			<th>Fecha Pago</th>
			<th>Importe</th>
			<th style="width: 180px">Nro Cheque</th>
			<th>Estado</th>
		</tr>
		<logic:notEmpty name="ProcessForm" property="process.detallesSinVincular">
		<bean:define id="process" name="ProcessForm" property="process"/>
		
		<%
			List<DetalleRecaudacion> dsv = ((ConciliacionRecaudaciones) process).getDetallesSinVincular();
			String banco = "";
		%>
		
		<logic:iterate id="det" name="ProcessForm" property="process.detallesSinVincular">
		<%
			if (!((DetalleRecaudacion)det).getBanco().getDetaBa().equalsIgnoreCase(banco)) {
				banco = ((DetalleRecaudacion)det).getBanco().getDetaBa();
		%>
		<tr>
			<td colspan="7"><b><%= banco %></b></td>
		</tr>
		<%
			}
		%>
		<tr>
			<td>${det.objetoi.persona.nomb12}</td>
			<td>${det.numeroProyecto}</td>
			<td><%= DateHelper.getString(((DetalleRecaudacion) det).getFechaAcreditacionValor()) %></td>
			<td class="right">${det.importeStr}</td>
			<td style="text-align: center">${det.numeroValor}</td>
			<td>${det.estadoActual.estado}</td>
		</tr>
		</logic:iterate>
		</logic:notEmpty>
	</table>
	
	<div style="margin-top: 30px">
		<html:submit value="Volver"></html:submit>
	</div>
</html:form>
<iframe width=174 height=220 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>