<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Director</div>
<br><br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
<table border="0">
<html:hidden property="entity.id" value="${AbmForm.entity.id}"/>

<tr><th>C�digo:</th><td><html:text property="entity.codigo" readonly="${clave}" maxlength="8"/></td></tr>
<tr><th>Nombre:</th><td><html:text property="entity.nombre" maxlength="30"/></td></tr>
<tr><th>Descripci�n:</th><td><html:textarea styleId="Descripci�n" onkeypress="caracteres(this,50,event)" property="entity.descripcion" cols="40" rows="4"/></td></tr>
<tr><th>Valor:</th><td><html:text property="entity.valor" maxlength="500"/></td></tr>
    
</table>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<!-- Alejandro Bargna -->
