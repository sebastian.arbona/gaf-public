<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" value="guardar" styleId="accion" />
  <html:hidden property="process.personaTitular.id" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.id}" />
  <div style="width: 70%" align="left">
    <html:errors />
  </div>
  <table border="0" align="center">
    <tr>
      <th>
        Id:
      </th>
      <td>
        <html:text property="process.contacto.id" readonly="true" />
      </td>
    </tr>
    <tr>
      <th>
        Medio:
      </th>
      <td>
        <asf:select name="process.contacto.medio" nullText="Seleccione el Medio de Contacto" nullValue="true" entityName="Tipificadores"
          listCaption="codigo,descripcion" listValues="codigo" value="${ProcessForm.process.contacto.medio}"
          filter="categoria='MedioContacto'"
        ></asf:select>
      </td>
    </tr>
    <tr>
      <th>
        Tipo:
      </th>
      <td>
        <asf:select name="process.contacto.tipo" nullText="Seleccione el Tipo de Contacto" nullValue="true" entityName="Tipificadores"
          listCaption="codigo,descripcion" listValues="codigo" value="${ProcessForm.process.contacto.tipo}"
          filter="categoria='TipoContacto'"
        ></asf:select>
      </td>
    </tr>
    <tr>
      <th>
        Detalle:
      </th>
      <td>
        <html:text size="73" property="process.contacto.detalle" value="${ProcessForm.process.contacto.detalle}" styleId="detalle"
          maxlength="80"
        />
      </td>
    </tr>
    <tr>
      <th>
        Observaciones:
      </th>
      <td>
        <html:text size="73" property="process.contacto.observaciones" value="${ProcessForm.process.contacto.observaciones}"
          styleId="observaciones" maxlength="80"
        />
      </td>
    </tr>
    <tr>
      <th>
        Domicilio:
      </th>
      <td>
        <asf:select name="process.contacto.domicilio_id" nullText="Seleccione el Domicilio" nullValue="true"
          entityName="com.civitas.hibernate.persona.Domicilio" listCaption="domicilioStr" listValues="id"
          value="${ProcessForm.process.contacto.domicilio_id}" filter="persona.id='${ProcessForm.process.personaTitular.id}'"
        ></asf:select>
      </td>
    </tr>
  </table>
  <br>
  <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=guardar&process.contacto.id=${ProcessForm.process.contacto.id}">
  	<html:submit value="Guardar" />
  </asf:security>
  <input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<script type="text/javascript">
var accion = $('accion');
var formulario = $('oForm');

function cancelar() {
	accion.value = "cancelar";
	formulario.submit();
}//fin cancelar.-

Event.observe(window, 'load', function() {
	this.name = "Contacto";
	parent.push(this);
});
</script>