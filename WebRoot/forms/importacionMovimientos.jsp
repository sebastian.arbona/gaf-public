<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">Importaci&oacute;n de Movimientos</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=ImportacionMovimientosProcess"
	styleId="ProcessForm"
	enctype="multipart/form-data">

	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="importar"
		styleId="action" />

	<table border="0">
		<tr>
			<th>Archivo de Texto:</th>
			<td><html:file property="process.archivo" /></td>
		</tr>
		<tr>
			<th>Tipo de boleto a generar</th>
			<td><html:select property="process.tipoBoleto"
					name="ProcessForm">
					<html:option value="DB">Nota de Debito</html:option>
					<html:option value="CR">Nota de Credito</html:option>
				</html:select></td>
		</tr>
		<tr>
			<th>Separador de campos</th>
			<td><html:select property="process.separadorCampos"
					name="ProcessForm">
					<html:option value=";">; (punto y coma)</html:option>
					<html:option value=",">, (coma)</html:option>
					<html:option value="tab">tabulador</html:option>
				</html:select></td>
		</tr>
		<tr>
			<th>Separador decimales</th>
			<td><html:select property="process.separadorDecimal"
					name="ProcessForm">
					<html:option value=",">, (coma)</html:option>
					<html:option value=".">. (punto)</html:option>
				</html:select></td>
		</tr>
		<tr>
			<th>Codificacion del archivo</th>
			<td><html:select property="process.charset" name="ProcessForm">
					<html:option value="iso-8859-1">ISO-8859-1</html:option>
					<html:option value="utf-8">UTF-8</html:option>
				</html:select></td>
		</tr>
		<tr>
			<th colspan="2"><html:submit>Importar Archivo</html:submit></th>
		</tr>
	</table>

	<logic:notEmpty name="ProcessForm" property="process.movimientos">
		<logic:empty name="ProcessForm" property="process.resultado">
			<br />
			<br />
	
			<div class="title">Vista previa de movimientos a generar</div>
	
			<display:table name="ProcessForm" property="process.movimientos"
				id="mov"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ImportacionMovimientosProcess">
				<display:column title="Proyecto" property="numeroAtencion" />
				<display:column title="Fecha Proceso" property="fechaProceso"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Fecha Vencimiento" property="fechaVencimiento"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Fecha Generacion" property="fechaGeneracion"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Cuota" property="numeroCuota" />
				<display:column title="Importe" property="importe"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Concepto" property="concepto" />
				<display:column title="Tipo Concepto" property="tipoConceptoNombre" />
			</display:table>
	
			<br />
			<br />
	
			<html:submit onclick="aplicar();">Impactar en Cuenta Corriente</html:submit>
		</logic:empty>
	</logic:notEmpty>
	
	<logic:notEmpty name="ProcessForm" property="process.resultado">
		
		<br />
		<br />

		<div class="title">Movimientos generados</div>

		<display:table name="ProcessForm" property="process.resultado"
			id="mov"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ImportacionMovimientosProcess&process.cid=${ProcessForm.process.cid}&process.action=exportar"
			export="true">
			<display:setProperty name="report.title" value="Movimientos generados"></display:setProperty>
			<display:caption title="Movimientos generados"></display:caption>
			<display:column title="Proyecto" property="id.objetoi.numeroAtencion" />
			<display:column title="Fecha Proceso" property="fechaProceso"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Fecha Vencimiento" property="fechaVencimiento"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Fecha Generacion" property="fechaGeneracion"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Cuota" property="cuota.numero" />
			<display:column title="Importe" property="importe"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column title="Concepto" property="asociado.concepto.detalle" />
			<display:column title="Tipo Concepto" property="tipoConceptoNombre"/>
			<display:column title="Tipo Boleto" property="boleto.tipo" />
			<display:column title="Boleto" property="boleto.numeroBoleto" />
		</display:table>
		
	</logic:notEmpty>
</html:form>

<script type="text/javascript">
var form = $('ProcessForm');
var action = $('action');

function aplicar() {
	action.value = 'aplicar';
	form.submit();
}
</script>