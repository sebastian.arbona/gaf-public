<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="javascript" type="text/javascript">

function cancelar2() {
	accion.value = "cancelar";
	formulario.submit();
}

function cancelar(){
	var accion = $('action');
	var formulario = $('oForm');
	accion.value = 'cancelarGeneracion';
	formulario.submit();
}

function sendForm() {
	window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
			"width=450, height=180");
	return true;
}
</script>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <br>
  <br>
  Proceso de Facturación - Generación de Cargos
  <br>
  <br>
  <html:hidden property="process.accion" value="calcular" styleId="action" />
<%--  <html:hidden property="process.idEmision" styleId="emision_id"/>--%>
  <div style="width: 70%" align="left">
    <html:errors />
  </div>
  <div id="liquida">
    <table style="border: 2px solid #CCCCCC;" width="550">
      <tr>
        <th>
          Liquidación por (Liquidación - Orden - Vencimiento):
        </th>
        <td>
        <logic:equal value="false" property="process.deshabilito" name="ProcessForm">
          <asf:select name="process.idEmision" entityName="com.nirven.creditos.hibernate.Emision" listCaption="fechaEmisionStr,ordenEmision,filtroFechaVencimiento,moneda"
            listValues="id" value="${ProcessForm.process.idEmision}" filter="cerrada = 0 AND facturada = 0"
            orderBy="fechaEmision DESC, ordenEmision DESC" nullValue="0" nullText="Seleccione una liquidación" enabled="true">
          </asf:select>
        </logic:equal>
        <logic:equal value="true" property="process.deshabilito" name="ProcessForm">
          <asf:select name="process.idEmision" entityName="com.nirven.creditos.hibernate.Emision" listCaption="fechaEmisionStr,ordenEmision,filtroFechaVencimiento,moneda"
            listValues="id" value="${ProcessForm.process.idEmision}" filter="cerrada = 0 AND facturada = 0"
            orderBy="fechaEmision DESC, ordenEmision DESC" nullValue="0" nullText="Seleccione una liquidación" enabled="false">
          </asf:select>
        </logic:equal>
        </td>
      </tr>
    </table>
  </div>
  <br>
  <br>
  <html:submit value="Impactar en Cta Cte" onclick="sendForm();"/>
  <input type="button" value="Cancelar" onclick="cancelar();">
  <br>
  <br>
</html:form>