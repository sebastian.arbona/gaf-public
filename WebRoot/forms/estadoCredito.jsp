<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Administración de Estado de Solicitudes</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

	<table border="0">

		<tr><th>Id:</th><td><asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true" /></td>	</tr>
		<tr>
			<th>Nombre:</th>
			
			<td>
				<html:text property="entity.nombre" styleId="nombre"></html:text>
			</td>
		</tr>
		<tr>
			<th>Tipo:</th>
			<td>
				<asf:select name="entity.tipo" entityName="com.asf.hibernate.mapping.Tipificadores" value="${AbmForm.entity.tipo}" listCaption="getCodigo,getDescripcion" listValues="getCodigo" filter="categoria = 'estadoCredito.tipo'  order by codigo" />
			</td>
		</tr>

		<tr>
			<th>Criterio Modificación:</th>
			<td>
			
			<asf:select name="entity.criterioModificacion" entityName="com.asf.hibernate.mapping.Tipificadores" value="${AbmForm.entity.criterioModificacion}" listCaption="getCodigo,getDescripcion" listValues="getCodigo" filter="categoria = 'estadoCredito.criterioModificacion'  order by codigo" />
			</td>
		</tr>
		<tr>
			<th>Cuotas Desde:</th> 
			<td>
			<html:text property="entity.cuotasDesde" styleId="cuotasDesde"></html:text>
			</td>
				<th>Cuotas Hasta:</th>
			<td>
			<html:text property="entity.cuotasHasta" styleId="cuotasHasta"></html:text>
			</td>
		</tr>
		
	</table>
	<asf:security action="/actions/abmAction.do"	access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<!-- Raul Varela -->