
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<link rel="stylesheet" href="/CREDITOS/style/frames.css" type="text/css">
<div style="font-size: 20px; margin:15px" align="left">
  <html:errors />
</div>
<div class="title">
  Administración de Viñedos
</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
  <html:hidden name="ProcessForm" property="process.existe" styleId="existe" />
  <html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
  <html:hidden name="ProcessForm" property="process.idSolicitud" styleId="solicitud" />
  <html:hidden name="ProcessForm" property="process.objetoiVinedo.id" styleId="objetoiVinedo" />
  <html:hidden name="ProcessForm" property="process.elaboracionTabla" styleId="elaboracionTabla" />
  
  <table border="0">
  
  <logic:equal value="true" property="process.noValido" name="ProcessForm">
  	El codigo no es valido. Debe tener la primer letra en Mayusculas seguido de 5 numeros.
  </logic:equal>
  
  <logic:equal value="true" property = "process.elaboracion" name = "ProcessForm">
  
    <tr>
      <th>
        C.I.V.:
      </th>
      <td>
        <html:text property="process.codigo" maxlength="40" styleId="cod"/>
      </td>
    </tr>
    <logic:equal value="false" property="process.civ" name="ProcessForm">
    <tr>
      <th>
        Has. Afectadas:
      </th>
      <td>
        <asf:text type="decimal" property="ProcessForm.process.hectareas" maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    <tr>
      <th>
        QQ Estimados:
      </th>
      <td>
        <asf:text type="decimal" name="ProcessForm" property="process.qqestimados" maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    <tr>
      <th>
        Observaciones:
      </th>
      <td>
        <asf:text type="textarea" name="ProcessForm" property="process.vinedoActual.observaciones" cols="70" maxlength="250" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    <tr>
      <th>
        Departamento:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.vinedoActual.departamento"  maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    <tr>
      <th>
        Localidad:
      </th>
      <td>
        <asf:select name="process.idLocalidad" listCaption="nombre" 
          entityName="com.civitas.hibernate.persona.Localidad" listValues="id" enabled="false"
        />
      </td>
    </tr>
    <tr>
      <th>
        Titular:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.actual.persona.nomb12"  maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    
    <tr>
      <th>
        Fecha Solicitud:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.actual.fechaSolicitud"  maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    
    <tr>
      <th>
        QQ Aprobados:
      </th>
      <td>
        <asf:text type="decimal" name="ProcessForm" property="process.qqaprobados" maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
    </td>
    
    
    <tr>
      <th>
        Fecha Aprobacion:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.fechaInformeINV"  maxlength="40" readonly="true" attribs=" disabled='disabled' "/>
      </td>
    </tr>
    
    </logic:equal>
    </logic:equal>
  
  <logic:notEqual value="true" property = "process.elaboracion" name = "ProcessForm">
  
    <tr>
      <th>
        C.I.V.:
      </th>
      <td>
        <html:text property="process.codigo" maxlength="40" value="${ProcessForm.process.codigo}" styleId="cod"/>
      </td>
    </tr>
    <logic:equal value="false" property="process.civ" name="ProcessForm">
    <tr>
      <th>
        Has. Afectadas:
      </th>
      <td>
        <asf:text name="ProcessForm" type="decimal" property="process.hectareas" maxlength="40" value="${ProcessForm.process.hectareas}" />
      </td>
    </tr>
    <tr>
      <th>
        QQ Estimados:
      </th>
      <td>
        <asf:text type="decimal" name="ProcessForm" property="process.qqestimados" value="${ProcessForm.process.qqestimados}" maxlength="40" />
      </td>
    </tr>
    <tr>
      <th>
        Observaciones:
      </th>
      <td>
        <asf:text type="textarea" name="ProcessForm" property="process.observaciones" cols="70" maxlength="250"/>
      </td>
    </tr>
    <tr>
      <th>
        Departamento:
      </th>
      <td>
        <asf:text type="text" name="ProcessForm" property="process.departamento"  maxlength="40" />
      </td>
    </tr>
    <tr>
      <th>
        Localidad:
      </th>
      <td>
        <asf:select name="process.idLocalidad" listCaption="nombre" value="${ProcessForm.process.idLocalidad}"
          entityName="com.civitas.hibernate.persona.Localidad" listValues="id" enabled="true"
        />
      </td>
    </tr>
    </logic:equal>
    </logic:notEqual>
  
  </table>
  <logic:notEqual value="false" property="process.civ" name="ProcessForm">
  	<input type="submit" value="Comprobar" onclick="comprobar();">
  	<logic:equal value="true" property="process.requisitosCumplidos" name= "ProcessForm">
  		<input type="submit" value="Calcular Financiamiento" onclick="calcularDatosFinancieros();">
  	</logic:equal>
  	
  	<logic:equal value="false" property="process.requisitosCumplidos" name= "ProcessForm">
  		<input type="submit" value="Calcular Financiamiento" onclick="calcularDatosFinancieros();" disabled="disabled">
  	</logic:equal>
  </logic:notEqual>
 
  <input type="submit" value="Reporte QQ" onclick="imprimirINV();"/>
  
  
  <logic:equal value="true" property="process.civ" name="ProcessForm">
  <logic:notEmpty property="process.vinedos" name="ProcessForm">
  <display:table name="ProcessForm" property="process.vinedos" export="true" id="reportTable" defaultsort="1"
  requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
  	<display:caption>Proyecto ${ProcessForm.process.cred.numeroAtencion} - ${ProcessForm.process.cred.linea.nombre} - ${ProcessForm.process.cred.persona.nomb12}</display:caption>
  	  <display:column media="excel xml pdf" property="id" title="ID"/>
	  <display:column title="Nro INV." sortable="true">
	  	<div onmouseout="ocultarIndicador(${reportTable.id});" style="width: 60px">
	  		<span onmouseover="mostrarIndicador(${reportTable.id});">
	  			${reportTable.vinedo.codigo}
	  		</span>
	  		<span id="detalle-indicador-${reportTable.id}" style="visibility: hidden;">
	  			<img src="${pageContext.request.contextPath}/images/arrow1.gif" onmouseover="mostrarDetalleInv(${reportTable.id});"/>
	  		</span>
	  		<div class="tooltip" style="display: none" id="detalle-${reportTable.id}">
	  			<div class="tooltip-close" onclick="cerrarDetalle(${reportTable.id});">X</div>
	  			<div class="tooltip-content" id="detalle-content-${reportTable.id}">
	  				Cargando...
	  			</div>
	  		</div>
	  	</div>
	  </display:column>
	  <display:column property="vinedo.hectareas" title="Has. Afectadas" sortable="true" />
	  <display:column property="vinedo.observaciones" title="Observaciones" sortable="true" />
	  <display:column property="vinedo.localidad.nombre" title="Localidad" sortable="true" />
	  <display:column property="qqEstimados" title="QQ Estimados" sortable="true" />
	  <logic:equal value="true" name="ProcessForm" property="process.elaboracionTabla">
	  	<display:column property="qqAprobadoElab" title="QQ Aprobados" sortable="true" />
	  	<display:column property="fechaInformeINVStrElab" title="Fecha Aprobado" sortable="true" />
	  </logic:equal>
	
	  <logic:notEqual value="true" name="ProcessForm" property="process.elaboracionTabla">
	  	<display:column property="qqAprobado" title="QQ Aprobados" sortable="true" />
	  	<display:column property="fechaInformeINV" title="Fecha Aprobado" sortable="true" />
	  </logic:notEqual>
	  <logic:equal name="ProcessForm" value="true" property="process.estadoParaModificarYEliminar">
		  <asf:security action="/actions/modificarVinedo.do" access="">
		  	<display:column media="html" title="Modificar"><a href="javascript:void(0);" onclick="modificar(${reportTable.id});">MODIFICAR</a></display:column>
		  </asf:security>
		  <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
		      href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accion=eliminar&process.idSolicitud=${ProcessForm.process.idSolicitud}"
		      paramId="process.idVinedo" paramProperty="id">
		      <bean:message key="abm.button.delete" />
	      </display:column>
      </logic:equal>
      <logic:equal name="ProcessForm" value="true" property="process.estadoParaModificarYEliminar">
		  <asf:security action="/actions/invRest.do" access="">
		  	<display:column media="html" title="Actualizacion INV">
		  	  <input type="button" onclick="consultaInv(${reportTable.id});" value="Consultar"></input>
		  	</display:column>
		  </asf:security>
      </logic:equal>
      <logic:equal name="ProcessForm" value="true" property="process.estadoParaModificarYEliminar">
      		<asf:security action="/actions/process.do?processName=EdicionQQ" access="">
      			<display:column media="html" title="Modificar QQ INV"
      				href="${pageContext.request.contextPath}/actions/process.do?processName=EdicionQQ&do=process"
      				paramId="process.idVinedo" paramProperty="id">
      				Modificar QQ INV
      			</display:column>
      		</asf:security>
      </logic:equal>
  </display:table>

  </logic:notEmpty>
  </logic:equal>
  <logic:equal value="false" property="process.civ" name="ProcessForm">
  <input type="submit" value="Guardar" onclick="guardar();">
  <input type="submit" value="Cancelar" onclick="cancelar();">
  </logic:equal>
</html:form>
<script type="text/javascript">
function comprobar(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="ver";
}
function guardar(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="guardar";
}
function cancelar(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="";
}

function calcularDatosFinancieros(){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	accion.value="calcularDatosFinancieros";
	formulario.submit();
}
function modificar(id){
	var accion = document.getElementById("accion");	
	var formulario = document.getElementById("oForm");
	var objetoiVinedo = document.getElementById("objetoiVinedo");
	objetoiVinedo.value = id; 
	accion.value = "modificar";
	formulario.submit();
}
function imprimirINV() {
	var accion = document.getElementById("accion");
	var formulario = document.getElementById("oForm");
	accion.value = "imprimirINV";
	formulario.submit();
}
function mostrarIndicador(id) {
	var icon = document.getElementById("detalle-indicador-"+id);
	icon.style.visibility = 'visible';
}
function ocultarIndicador(id) {
	var icon = document.getElementById("detalle-indicador-"+id);
	icon.style.visibility = 'hidden';
}
function mostrarDetalleInv(id) {
	var tooltip = document.getElementById("detalle-"+id);
	tooltip.style.display = 'block';
	var url = '${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=consultaDetalleVinedo&v='+id;
	retrieveURL(url, mostrarDetalleInvResponse);
}
function mostrarDetalleInvResponse(data) {
	var objects = parseXml(data);
	var html = '';
	var id = null;
	if (objects.consultavinedo) {
		id = objects.consultavinedo.id;
		if (objects.consultavinedo.detalle) {
			var detalles = objects.consultavinedo.detalle;
			if (detalles.constructor === Array) {
				for (var i = 0; i < detalles.length; i++) {
					var d = detalles[i];
					html += 'Codigo: '+d.codvariedad + '<br/>';
					html += 'Variedad: '+d.nombrevariedad + '<br/>';
					html += 'Hectareas: '+d.hectareas + '<br/>';
					html += 'QQ Aprobados: '+d.qq;
					if (i + 1 < detalles.length)
						html += '<hr/>';
				}
			} else {
				var d = detalles;
				html += 'Codigo: '+d.codvariedad + '<br/>';
				html += 'Variedad: '+d.nombrevariedad + '<br/>';
				html += 'Hectareas: '+d.hectareas + '<br/>';
				html += 'QQ Aprobados: '+d.qq;
			}
		}
	}
	
	if (html === '') {
		html = 'No hay datos.';
	}
	var tooltip = document.getElementById("detalle-content-"+id);
	tooltip.innerHTML = html;
}

function cerrarDetalle(id) {
	var tooltip = document.getElementById("detalle-"+id);
	tooltip.style.display = 'none';
}

function consultaInv(id) {
	var url = '${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=consultaInv&v='+id;
	retrieveURL(url, consultaInvResponse);
}

function consultaInvResponse(data) {
	var objects = parseXml(data);
	if (objects.consultainv) {
		if (objects.consultainv.success === "false") {
			alert(objects.consultainv.error);
		} else {
			window.location.reload();
		}
	}
}
</script>