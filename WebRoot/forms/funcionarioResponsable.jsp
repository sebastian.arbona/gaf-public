<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Funcionario Responsable</div>

<br>
<br>
<div class="formContainer">
	<html:form
		action="/actions/FuncionarioResponsableAction.do?do=save&entityName=${param.entityName}&filter=false">

		<table border="0">
			<tr>
				<th>ID:</th>
				<td><html:text property="entity.id" readonly="true"
						maxlength="6" value="${funcionarioResponsableForm.entity.id}" /></td>
			</tr>
			<tr>
				<th>Estado:</th>
				<td><asf:select name="entity.idEstado"
						entityName="com.civitas.hibernate.persona.Estado"
						listCaption="nombreEstado" listValues="idEstado"
						value="${empty funcionarioResponsableForm.entity.idEstado ? '' : funcionarioResponsableForm.entity.idEstado}" />
				</td>
			</tr>
			<tr>
				<th>Comportamiento de Pago:</th>
				<td><asf:select name="entity.comportamiento"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${funcionarioResponsableForm.entity.comportamiento}"
						nullValue="true" nullText="N/A"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'comportamientoPago' order by codigo" /></td>
			</tr>

			<th>Fecha Inicio:</th>
			<td colspan="3"><asf:calendar
					property="funcionarioResponsableForm.entity.fechaDesdeStr"
					maxlength="10" /></td>

			<tr>
				<th>Nombre Responsable:</th>
				<td><html:text property="entity.nombreResponsable"
						styleId="nombreResponsable"
						value="${funcionarioResponsableForm.entity.nombreResponsable}" /></td>
			</tr>
			<tr>
				<th>Usuario Responsable:</th>
				<td><asf:select name="entity.idUsuario"
						entityName="com.nirven.creditos.hibernate.Usuario"
						listCaption="nombre" listValues="id" nullValue="true"
						nullText="N/A"
						value="${empty funcionarioResponsableForm.entity.idUsuario ? '' : funcionarioResponsableForm.entity.idUsuario}"
						filter="persona_IDPERSONA != null order by causerk" /></td>
			</tr>

		</table>

		<br>
		<br>

		<asf:security action="/actions/FuncionarioResponsableAction.do"
			access="do=save&entityName=${param.entityName}">
			<html:submit styleId="bGuardar">
				<bean:message key="abm.button.save" />
			</html:submit>
		</asf:security>
		<html:cancel>
			<bean:message key="abm.button.cancel" />
		</html:cancel>
	</html:form>
</div>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
