<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.persona.id" styleId="idPersonaTitular" value="${ProcessForm.process.persona.id}" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br />
	<div class="grilla">
		<logic:notEmpty name="ProcessForm" property="process.rolesPersonas">
			<display:table name="ProcessForm"  property="process.rolesPersonas"
				export="true" id="reportTable" width="100%"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column property="rol" title="Rol" />
				<display:column property="detalle1" title="Detalle" />
				<display:column property="detalle2" title="Detalle" />
			</display:table>
		</logic:notEmpty>
		<logic:empty name="ProcessForm" property="process.rolesPersonas">
    		La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no tiene Roles Asociados.
		</logic:empty>
	</div>
</html:form>