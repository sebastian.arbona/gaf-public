<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Reporte de Inventario de Garant&iacute;as</div>
<html:form
	action="/actions/process.do?do=process&processName=GarantiasInventario&process.usuario=${param.usuario}"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>	
	<html:hidden property="process.usuario" styleId="usuario"/>
	<html:errors/>

	
	<table>
		<tr>
			<th>Tipos de Garant&iacute;as:</th>
			<td>
				<html:select styleId="tiposGarantias" name="ProcessForm" property="process.tiposGarantias" multiple="true" size="8">
					<html:optionsCollection property="process.tiposGarantiasList" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>
		
		<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
			<tr>
				<th>Estados de Garant&iacute;as:</th>
				<td>
					<html:select styleId="estadosGarantias" name="ProcessForm" property="process.estadosGarantias" multiple="true" size="8">
						<html:optionsCollection property="process.estadosGarantiasList" label="nombreEstado" value="idEstado"/>
					</html:select>
				</td>
			</tr>		
		</logic:notEqual>
		
		<logic:equal value="tribunalDeCuentas" parameter="process.usuario">
			<tr>
				<th>Estados de Garant&iacute;as:</th>
				<td>
					<html:select styleId="estadosGarantias" name="ProcessForm" property="process.estadosGarantias" multiple="true" size="8">
						<html:optionsCollection property="process.estadosGarantiasHtcList" label="nombreEstado" value="idEstado"/>
					</html:select>
				</td>
		</tr>
		</logic:equal>
		
		<tr>
			<th>L&iacute;neas:</th>
			<td>
				<html:select styleId="lineas" name="ProcessForm" property="process.lineas" multiple="true" size="8">
					<html:optionsCollection property="process.lineasList" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Estado de Seguro:</th>
			<td>
				<html:select name="ProcessForm" property="process.estadoSeguro">	
					<html:option value="${null}">Todos</html:option>
					<html:option value="activo">Activo</html:option>
					<html:option value="vencido">Vencido</html:option>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Fecha Desde:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaDesdeStr" value="${process.fechaDesdeStr}" />
			</td>			
		</tr>
		<tr>
			<th>Fecha Hasta:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaHastaStr" value="${process.fechaHastaStr}" />
			</td>			
		</tr>
		<tr>
			<th>Fecha de:</th>
			<td>
				 <html:radio name="ProcessForm" property="process.fechaProsValor" value="1"></html:radio>Procesos
				 <html:radio name="ProcessForm" property="process.fechaProsValor" value="2"></html:radio>Valor
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Listado en pantalla" onclick="return validaFecha();"></html:submit>
			</th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.resultado">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.resultado" id="bean" export="true"
 			requestURI="${pageContext.request.contextPath}/actions/process.do" pagesize="30">
 			<display:caption>Reporte de Inventario de Garantias</display:caption>
 			<display:column title="Orden Fisico" property="ordenFisico"/>
 			<display:column title="Nro Inventario" property="numeroInventarioOriginal"/>
 			<display:column title="Proyecto Nro" property="numeroAtencion" />
 			<display:column title="Expediente" property="expediente"  />
 			<display:column title="Linea" property="linea" />
 			<display:column title="Inversor" property="inversor"/>
 			<display:column title="Cuit / Cuil" property="cuit"  />
 			<display:column title="Monto Inversion" property="financiamiento" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
 				<display:column title="Aforo" property="aforo" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			</logic:notEqual>
 			<display:column title="Etapa del Credito" property="etapaCredito"  />
 			<display:column title="Tipo de Garantia" property="tipoGarantia"  />
 			<display:column title="Identificador" property="identificador"  />
 			<display:column title="Descripcion" property="descripcion"  />
 			<display:column title="Importe Garantizado" property="valor" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right" />
 			<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
 				<display:column title="Estado Garantia" property="estado"  />
			</logic:notEqual>
 			<display:column title="Fecha Ultimo Estado" property="fechaEstado" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Fecha Vencimiento Garantia" property="vencimiento" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Fecha Inscripcion/Reinscripcion en Registro" property="inscripcion" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Fecha Vencimiento del Seguro" property="vencimientoSeguro" decorator="com.asf.displayDecorators.DateDecorator" />
 			<display:column title="Sector de custodia" property="sectorCustodia" />
 			<display:column title="Responsable de custodia" property="responsableCustodia" />
 			
 			
 			<logic:equal value="2" name="ProcessForm" property="process.fechaProsValor">
 			<display:column title="Fecha Ultimo Estado" property="fechaEstado" decorator="com.asf.displayDecorators.DateDecorator" />
 			</logic:equal>
 			<logic:notEqual value="tribunalDeCuentas" parameter="process.usuario">
	 			<logic:equal value="1" name="ProcessForm" property="process.fechaProsValor">
	 				<display:column title="Fecha Proceso" property="fechaProceso" decorator="com.asf.displayDecorators.DateDecorator" />
	 			</logic:equal>
 			</logic:notEqual>
 		</display:table>
	</div>
	</logic:notEmpty>
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>

<script type="text/javascript">
var fechaDesde          	= $("process.fechaDesdeStr");
var fechaHasta      		= $("process.fechaHastaStr");

var usuario						= $('usuario');
var processForm = document.getElementById("ProcessForm")
processForm.action += "&process.usuario=" + usuario.value;

function validaFecha() {
	retorno = true;

	var arrayFechaDesde = fechaDesde.value.split("/");
	var arrayFechaHasta = fechaHasta.value.split("/");

	if(retorno && arrayFechaDesde.length == 3 && arrayFechaDesde[2].length == 4 && arrayFechaHasta.length == 3 && arrayFechaHasta[2].length == 4) {
		if(arrayFechaDesde[2] > arrayFechaHasta[2]) {
			alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
			retorno=false;
		}
		else {
			if(arrayFechaDesde[2] == arrayFechaHasta[2] && arrayFechaDesde[1] > arrayFechaHasta[1]) {
				alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
				retorno=false;
			}
			else {
				if(arrayFechaDesde[2] == arrayFechaHasta[2] && arrayFechaDesde[1] == arrayFechaHasta[1] && arrayFechaDesde[0] > arrayFechaHasta[0]) {
					alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
					retorno=false;
				}
			}
		}
	}

	return retorno;
}

</script>