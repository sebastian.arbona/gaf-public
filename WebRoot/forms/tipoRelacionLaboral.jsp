<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Tipo de Relación Laboral</div>
<br><br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

<table border="0">
	<tr>
		<th>ID:</th>
		<td><html:text property="entity.id" readonly="true" /></td>
	</tr>
	<tr>
		<th>Denominación:</th>
		<td>
			<html:text property="entity.denominacion" styleId="denominacion"></html:text>
		</td>	
	</tr>
	<tr>
		<th>Descripción:</th>
		<td><html:textarea property="entity.descripcion" styleId="descripcion" cols="50" rows="4" onkeypress="caracteres(this,100,event)" /></td>
	</tr>
</table>
<br />
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<script language='javascript' >
var denominacion = $( 'denominacion' );

denominacion.focus();
</script>