<%@ page language="java"%>
<%@ page import="com.nirven.creditos.hibernate.FuncionPersonaResolucion"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>


<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/links.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/multiple-emails.css">


<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/functions.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/multiple-emails.js"></script>


<script language="JavaScript" type="text/javascript">
	$j = jQuery.noConflict();
</script>

<body class=" yui-skin-sam">


	<div class="title">Movimiento Instancia</div>
	<br>
	<br>

	<html:form
		action="/actions/process.do?do=process&processName=${param.processName}"
		styleId="oForm" enctype="multipart/form-data">

		<html:hidden name="ProcessForm" property="process.accion"
			styleId="accion" />
		<html:hidden name="ProcessForm"
			property="process.movimientoInstancia.id" />
		<html:hidden name="ProcessForm"
			property="process.procesoResolucion.id" styleId="idInstancia" />
		<html:hidden name="ProcessForm" property="process.current_emails"
			styleId="current_emails" />

		<div style="width: 70%" align="left">
			<html:errors />
		</div>

		<table border="0" class="tabla" id="acarreo1">

			<tr id="tipoEmbargoDiv">
				<th>Tipo Movimiento:</th>
				<td><asf:selectpop name="process.movimientoInstancia.tipoMovimientoInstancia.id" title="Tasa"
					columns="Codigo, Nombre"
					captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.TipoMovimientoInstancia"
					value="${ProcessForm.process.movimientoInstancia.tipoMovimientoInstancia.id}" 
					 />	</td>
			</tr>
			<tr>
				<th>Observaciones:</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.movimientoInstancia.observacion" cols="60"
						rows="4" /></td>
			</tr>
			<tr>
				<th>Fecha Carga Movimiento:</th>
				<td><asf:text
						property="ProcessForm.process.movimientoInstancia.fechaCargaMovimientoStr"
						type="Text" maxlength="30" attribs="size=\"60\"" readonly="true" />
				</td>
			</tr>
			<tr>
				<th>Fecha Movimiento:</th>
				<td>
					<!--   <asf:calendar property="ProcessForm.process.movimientoInstancia.fechaMovimientoStr" attribs="size=\"60\""/> -->
					<asf:calendar
						property="ProcessForm.process.movimientoInstancia.fechaMovimientoStr"
						attribs="size=\"60\"" />
				</td>
			<tr>
				<th>Observación Relevante</th>
				<td><asf:select
						name="process.movimientoInstancia.observacionRelevanteIdStr"
						entityName="com.nirven.creditos.hibernate.ObservacionRelevante"
						listCaption="getObservacion" listValues="getId" nullValue="true"
						nullText="--" style="width:330px"
						value="${ProcessForm.process.movimientoInstancia.observacionRelevanteIdStr}"
						attribs="onChange='mostrarMail(this.value);'" /></td>
			<tr>
				<th>Descripción</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.movimientoInstancia.observacionRelevanteDesc"
						cols="60" rows="4" /></td>
			</tr>

			<tr id="mailTr">
				<th>Enviar mail de aviso:</th>
				<td><html:checkbox property="process.enviarAviso"
						onclick="mostrarEnviarMail();" styleId="enviarMailCheck"></html:checkbox><span
					id="enviarMail"></span></td>
			</tr>
			<tr id="multipleMailDiv">
				<th>Destinatarios Adicionales:</th>
				<td>
					<div class='container'>
						<div class='col-sm-4'>
							<input type='text' id='demo' name='email' class='form-control'
								value='' />
						</div>
					</div>
				</td>
			</tr>

		</table>

		<div class="title">Administración de Archivos</div>

		<br />
		<div class="grilla">
			<table>
				<tr>
					<th><span style="" class="required">(*)</span>&nbspDocumento:</th>
					<td colspan="3"><br> <html:file name="ProcessForm"
							property="process.theFile" /> <br></td>
				</tr>
			</table>
			<br>
			<display:table name="ProcessForm"
				property="process.archivosMovimientoInstancia" export="false"
				id="reportTable">

				<display:column title="Nombre" property="nombre" />
				<display:column title="Descargar">
					<html:button
						onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargarArchivoMovimientoInstancia&tipo=documentoJudicial&id=${reportTable.id}')"
						property="" value="Bajar Documento" />
				</display:column>
				<asf:security action="/actions/process.do"
					access="do=process&entityName=${param.entityName}">
					<display:column media="html" honClick="return confirmDelete();"
						title="Eliminar"
						href="${pageContext.request.contextPath}/actions/process.do?processName=ResolucionJudicialProcess&do=process&process.accion=eliminarDocumentosMovimientoInstancia&process.movimientoInstancia.id=${ProcessForm.process.movimientoInstancia.id}"
						paramId="process.idArchivo" paramProperty="id">
						<bean:message key="abm.button.delete" />
					</display:column>
				</asf:security>
			</display:table>
			<div style="margin-top: 10px">
				<input type="button" value="Guardar Movimiento" onclick="guardar();" />
				<input type="button" value="Cancelar" onclick="cancelar();" />
			</div>
		</div>
	</html:form>

	<script language="JavaScript" type="text/javascript">
		var formulario = $j('#oForm');

		$j(document).ready(function() {
			$j('#demo').multiple_emails();
			$j('#demo').change(function() {
				$j('#current_emails').text($j(this).val());
			});
			$j('#current_emails').text($j('#demo').val());
		});
	</script>

	<script language="JavaScript" type="text/javascript">
		var tipoMovimiento = $('process.movimientoInstancia.tipoMovimientoInstancia.id');
		var observacionRelevante = $('process.movimientoInstancia.observacionRelevante.id');

		$j(document).ready(function($) {

			traertipoMovimientoAjax();
			traerobservacionRelevanteAjax();

		});

		function mostrarEnviarMail() {

			if ($j("#enviarMailCheck").is(':checked')) {
				document.getElementById("enviarMail").innerHTML = '<STRONG>'
						+ 'Enviar mail' + '</STRONG>';
				$j('#multipleMailDiv').show();

			} else {
				document.getElementById("enviarMail").innerHTML = '<STRONG>'
						+ 'No enviar mail' + '</STRONG>';
				$j('#multipleMailDiv').hide();

			}
		}

		function mostrarMail(t) {
			if (t) {
				$('mailTr').show();
				$('multipleMailDiv').show();

			} else {
				$('mailTr').hide();
				$('multipleMailDiv').hide();
			}
		}
		mostrarMail();

		function traerobservacionRelevanteAjax() {
			//alert("traerobservacionRelevanteAjax");
			url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?";
			url += "do=populateObservacionRelevante";
			retrieveURL(url, traerobservacionRelevante);
		}

		function traerobservacionRelevante() {
			//alert("traerobservacionRelevante");
			alert(json);
			var arr_from_json = JSON.parse(json);
			var select = getElem("process.movimientoInstancia.observacionRelevante.id");
			select.innerHTML = "";

			var options = '';

			for (var i = 0; i < arr_from_json.length; i++) {
				var newListItem = document.createElement('OPTION');
				newListItem.text = arr_from_json[i].observacion;
				alert(newListItem.text);
				newListItem.value = arr_from_json[i].id;
				select.add(newListItem);
			}

			observacionRelevante.value = "${ProcessForm.process.movimientoInstancia.observacionRelevante.id}";
		}

		function traertipoMovimientoAjax() {
			var idProcesoResolucion = document.getElementById("idInstancia").value;
			url = "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?";
			url += "do=populateTipoMovimiento&";
			url += "filter=" + idProcesoResolucion;

			retrieveURL(url, traertipoMovimiento);
		}

		function traertipoMovimiento(json) {
			var arr_from_json = JSON.parse(json);
			var select = getElem("process.movimientoInstancia.tipoMovimientoInstancia.id");
			select.innerHTML = "";

			var options = '';
			for (var i = 0; i < arr_from_json.length; i++) {

				var newListItem = document.createElement('OPTION');
				newListItem.text = arr_from_json[i].nombre;
				newListItem.value = arr_from_json[i].id;

				select.add(newListItem);

			}

			tipoMovimiento.value = "${ProcessForm.process.movimientoInstancia.tipoMovimientoInstancia.id}";
		}

		function guardar() {
			current_emails.value = $j('#current_emails').text();
			accion.value = 'guardarMovimientoBlanco';
			var form = $('oForm');
			form.submit();
		}

		function cancelar() {

			accion.value = 'listarMovimientosBlanco';
			var form = $('oForm');
			form.submit();
		}

		function limpiarCombo(combo) {

			while (combo.length > 0) {
				combo.remove(combo.length - 1);
			}
		}
	</script>

	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
	</iframe>