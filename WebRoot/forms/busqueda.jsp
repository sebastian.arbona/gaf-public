<%@ page language="java"%>
<%@ page import="com.civitas.hibernate.persona.*" %>
<%@ page import="com.asf.security.BusinessPersistance" %>
<%@ page import="com.asf.security.SessionHandler" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
 
<html:form action="/actions/ArchivoAction.do?do=save&entityName=${param.entityName}" enctype="multipart/form-data">


<html:hidden  property="filter" value="true" />
<bean:define id="filter" name="paramValue" scope="session" type="Object[]"/>
<bean:define id="idpersona" value="${paramValue[0]}"/>
<%
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
    
    Persona pers = (Persona)bp.getById(Persona.class,new Long(idpersona));
%>

<html:hidden  property="entity.id"/>

<div class="title">Documentos Relacionados</div>

<BR>
<BR>
<table border="0">

<tr>
<th>Persona:
</th>
<td>
<html:text property="entity.idpersona" value="${paramValue[0]}"size="3" readonly="true" />&nbsp;<b><%=pers.getNomb12().toUpperCase()%></b>
</td>
</tr>
<tr>
    <th>Tipo de Documento:</th>
    <td>
        <asf:select entityName="com.civitas.hibernate.persona.TipoArchivo"  listCaption="descripcion" listValues="idtipoarchivo" name="entity.idtipoarchivo" value="AbmForm.entity.idtipoarchivo"/>
    </td>
</tr>


<tr>
    <th>Documento:</th>
    <td colspan="3">
        <html:file property="theFile"/>
    </td>

</tr> 
<logic:present parameter="load">
<tr>
<th></th>
    <td colspan="3">
   
   <logic:equal value="1"  name="AbmForm" property="entity.tipoArchivo.imagen"> 
 	<img  src="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=ver&id=${AbmForm.entity.id}" />
   	</logic:equal>

   	<logic:notEqual  value="1"  name="AbmForm"   property="entity.tipoArchivo.imagen">
  	<img src="${pageContext.request.contextPath}/images/${AbmForm.entity.tipoArchivo.ruta}" />
  	</logic:notEqual>
   
   
   
   		   


	</td>
</tr> 


<tr>
<th>Descarga:</th>
    <td colspan="3">
   
   	<html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoAction.do?do=descargar&id=${AbmForm.entity.id}')" property="">Bajar Documento</html:button>	   


	</td>
</tr> 
</logic:present>


   

</table>

<asf:security action="/actions/ArchivoAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel" /></html:cancel>
</html:form>
