<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script type="text/javascript">
	function estado(opc) {
		switch (opc) {
		case 0:
			mostrar("ruta", "ruta2");
			break;
		case 1:
			ocultar("ruta", "ruta2");
			break;
		}
	}
</script>
<div class="title">Tipo de Documentación</div>
<br>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<table border="0">
		<tr>
			<th>Descripcion:</th>
			<td><html:text property="entity.descripcion" maxlength="40" />
			</td>
		</tr>
		<tr>
			<th>Imagen:</th>
			<td><asf:select
					attribs="onchange='estado(options.selectedIndex)'"
					listCaption="NO,SI" listValues="0,1" name="entity.imagen"
					value="${AbmForm.entity.imagen}" /></td>
		</tr>
		<tr>
			<th>
				<div id="ruta">Ruta:</div>
			</th>
			<td>
				<div id="ruta2">
					<html:text property="entity.ruta" />
				</div>
			</td>
		</tr>

	</table>
	<logic:notPresent parameter="load">
		<script type="text/javascript">
			mostrar("ruta", "ruta2");
		</script>
	</logic:notPresent>
	<logic:present parameter="load">
		<script type="text/javascript">
			<logic:equal value="0" name="AbmForm" property="entity.imagen" >
			estado(0);
			getElem("imagen").value = "0";
			</logic:equal>
			<logic:notEqual  value="0" name="AbmForm" property="entity.imagen" >
			estado(1);
			getElem("imagen").value = "1";
			</logic:notEqual>
		</script>
	</logic:present>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
