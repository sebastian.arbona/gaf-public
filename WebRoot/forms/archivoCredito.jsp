<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	enctype="multipart/form-data" styleId="oForm">
	<html:hidden property="process.accion" name="ProcessForm"
		styleId="accion" />
	<html:hidden property="process.objetoiArchivo.credito.id"
		name="ProcessForm" styleId="creditoId" />
	<html:hidden property="process.objetoiArchivo.id" name="ProcessForm"
		styleId="archivoId" />

	<logic:equal value="listar" name="ProcessForm"
		property="process.accion">
		<input type="button" Value="Adjuntar Nuevo" onclick="adjuntar();">
		<br />
		<br />
		<logic:notEmpty name="ProcessForm" property="process.antecedentes">
			<h3>Antecedentes de Desembolsos:</h3>
			<div id="grilla">
				<display:table name="ProcessForm" property="process.antecedentes"
					id="archivo" export="true"
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
					<display:caption>Proyecto ${ProcessForm.process.objetoiArchivo.credito.numeroAtencion} - ${ProcessForm.process.objetoiArchivo.credito.linea.nombre} - ${ProcessForm.process.objetoiArchivo.credito.persona.nomb12}</display:caption>
					<display:column property="archivoRepo.id" title="Identificación" />
					<display:column property="nombre" title="Nombre" />
					<display:column property="fechaAdjuntoStr" title="Fecha Adjunto" />
					<display:column property="usuario" title="Usuario" />
					<display:column property="detalle" title="Detalle" />
					<display:column property="tipoDeArchivoNombreStr" title="Tipo de Archivo" />
					<display:column property="paginasStr" title="Páginas" />
	
					<display:column title="Bajar" media="html">
						<html:button
							onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${archivo.id}')"
							property="process.objetoiArchivo.id">Bajar</html:button>
					</display:column>
					<display:column title="Sólo Lectura" property="noBorrar"
						decorator="com.asf.displayDecorators.BooleanDecorator" />
					<display:column title="Eliminar" media="html">
						<logic:equal value="false" name="archivo" property="noBorrar">
							<logic:equal name="archivo" property="autorizado" value="true">
								<html:button onclick="eliminar(${archivo.id});"
									property="process.objetoiArchivo.id">Eliminar</html:button>
							</logic:equal>
						</logic:equal>
	
					</display:column>
				</display:table>
			</div>
		</logic:notEmpty>
		
		
		
		
		
		<logic:notEmpty name="ProcessForm" property="process.normas">
			<h3>Normas y Contratos:</h3>
			<div id="grilla">
				<display:table name="ProcessForm" property="process.normas"
					id="archivo" export="true"
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
					<display:caption>Proyecto ${ProcessForm.process.objetoiArchivo.credito.numeroAtencion} - ${ProcessForm.process.objetoiArchivo.credito.linea.nombre} - ${ProcessForm.process.objetoiArchivo.credito.persona.nomb12}</display:caption>
					<display:column property="archivoRepo.id" title="Identificación" />
					<display:column property="nombre" title="Nombre" />
					<display:column property="fechaAdjuntoStr" title="Fecha Adjunto" />
					<display:column property="usuario" title="Usuario" />
					<display:column property="detalle" title="Detalle" />
					<display:column property="tipoDeArchivoNombreStr" title="Tipo de Archivo" />
					<display:column property="paginasStr" title="Páginas" />
	
					<display:column title="Bajar" media="html">
						<html:button
							onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${archivo.id}')"
							property="process.objetoiArchivo.id">Bajar</html:button>
					</display:column>
					<display:column title="Sólo Lectura" property="noBorrar"
						decorator="com.asf.displayDecorators.BooleanDecorator" />
					<display:column title="Eliminar" media="html">
						<logic:equal value="false" name="archivo" property="noBorrar">
							<logic:equal name="archivo" property="autorizado" value="true">
								<html:button onclick="eliminar(${archivo.id});"
									property="process.objetoiArchivo.id">Eliminar</html:button>
							</logic:equal>
						</logic:equal>
	
					</display:column>
				</display:table>
			</div>
		</logic:notEmpty>
		
		
		
		
		<logic:notEmpty name="ProcessForm" property="process.notificaciones">
			<h3>Notificaciones:</h3>
			<div id="grilla">
				<display:table name="ProcessForm" property="process.notificaciones"
					id="archivo" export="true"
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
					<display:column property="archivoRepo.id" title="Identificación" />
					<display:caption>Proyecto ${ProcessForm.process.objetoiArchivo.credito.numeroAtencion} - ${ProcessForm.process.objetoiArchivo.credito.linea.nombre} - ${ProcessForm.process.objetoiArchivo.credito.persona.nomb12}</display:caption>
					<display:column property="nombre" title="Nombre" />
					<display:column property="fechaAdjuntoStr" title="Fecha Adjunto" />
					<display:column property="usuario" title="Usuario" />
					<display:column property="detalle" title="Detalle" />
					<display:column property="tipoDeArchivoNombreStr" title="Tipo de Archivo" />
					<display:column property="paginasStr" title="Páginas" />
	
					<display:column title="Bajar" media="html">
						<html:button
							onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${archivo.id}')"
							property="process.objetoiArchivo.id">Bajar</html:button>
					</display:column>
					<display:column title="Sólo Lectura" property="noBorrar"
						decorator="com.asf.displayDecorators.BooleanDecorator" />
					<display:column title="Eliminar" media="html">
						<logic:equal value="false" name="archivo" property="noBorrar">
							<logic:equal name="archivo" property="autorizado" value="true">
								<html:button onclick="eliminar(${archivo.id});"
									property="process.objetoiArchivo.id">Eliminar</html:button>
							</logic:equal>
						</logic:equal>
	
					</display:column>
				</display:table>
			</div>
		</logic:notEmpty>
		
		
		
		
		
		<br />
		<br />
		<logic:notEmpty name="ProcessForm" property="process.adjuntosProceso">
			<h3>Archivos Adjuntados en Proceso de Otorgamiento:</h3>
			<div id="grilla">
				<display:table name="ProcessForm" property="process.adjuntosProceso"
					id="node" export="true"
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
					
					
					<display:column property="fileName" title="Nombre" />
					<display:column property="lastModified.time" title="Fecha"
						decorator="com.asf.displayDecorators.DateDecorator" />
					<display:column media="html">
						<a
							href="${pageContext.request.contextPath}/actions/jbpm.do?do=getVariableContent&id=${node.id}"
							target="_blank">Descargar</a>
					</display:column>
				</display:table>
			</div>
		</logic:notEmpty>
		<br />
		<br />
		<logic:notEmpty name="ProcessForm"
			property="process.documentoResolucion">
			<h3>Documentos Adjuntos en Gestión Judicial</h3>
			<div id="grilla">
				<display:table name="ProcessForm"
					property="process.documentoResolucion" export="true" id="judicial">
					<display:setProperty name="report.title"
						value="Listado de Documentos Judiciales"></display:setProperty>
					<display:column property="id" title="Identificación" />
					<display:column property="nombre" title="Nombre" />
					<display:column property="detalle" title="Detalle" />
					<display:column title="Bajar" media="html">
						<html:button
							onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargarArchivoJudicial&id=${reportTable.id}')"
							property="process.objetoiArchivo.id">Bajar</html:button>
					</display:column>
				</display:table>
			</div>
		</logic:notEmpty>

	</logic:equal>

	<br />
	
	
	
	<logic:notEmpty name="ProcessForm" property="process.general">
			<h3>Información General:</h3>
			<div id="grilla">
				<display:table name="ProcessForm" property="process.general"
					id="archivo" export="true"
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
					<display:caption>Proyecto ${ProcessForm.process.objetoiArchivo.credito.numeroAtencion} - ${ProcessForm.process.objetoiArchivo.credito.linea.nombre} - ${ProcessForm.process.objetoiArchivo.credito.persona.nomb12}</display:caption>
					<display:column property="archivoRepo.id" title="Identificación" />
					<display:column property="nombre" title="Nombre" />
					<display:column property="fechaAdjuntoStr" title="Fecha Adjunto" />
					<display:column property="usuario" title="Usuario" />
					<display:column property="detalle" title="Detalle" />
					<display:column property="tipoDeArchivoNombreStr" title="Tipo de Archivo" />
					<display:column property="paginasStr" title="Páginas" />
	
					<display:column title="Bajar" media="html">
						<html:button
							onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${archivo.id}')"
							property="process.objetoiArchivo.id">Bajar</html:button>
					</display:column>
					<display:column title="Sólo Lectura" property="noBorrar"
						decorator="com.asf.displayDecorators.BooleanDecorator" />
					<display:column title="Eliminar" media="html">
						<logic:equal value="false" name="archivo" property="noBorrar">
							<logic:equal name="archivo" property="autorizado" value="true">
								<html:button onclick="eliminar(${archivo.id});"
									property="process.objetoiArchivo.id">Eliminar</html:button>
							</logic:equal>
						</logic:equal>
	
					</display:column>
				</display:table>
			</div>
		</logic:notEmpty>
	
	
	

	<logic:equal value="adjuntar" name="ProcessForm"
		property="process.accion">
		<table border="0">
			<tr>
				<th>Credito:</th>
				<td><html:text
						property="process.objetoiArchivo.credito.numeroAtencion"
						value="${ProcessForm.process.objetoiArchivo.credito.numeroAtencion}" />
				</td>
			</tr>
			<tr>
				<th>
					Origen:
				</th>
				<td>
					<asf:select name="process.funcionalidadOrigen"  attribs="class='familia'"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.objetoiArchivo.funcionalidadOrigen}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'objetoiArchivo.funcionalidadOrigen'  order by codigo" />
				</td>
			</tr>
			
		
			<tr>
				<th colspan="1">Tipo de Documento</th>
				<td colspan="3"><asf:select2 name="ProcessForm"
						property="process.idTipoDeArchivo"
						value="${ProcessForm.process.idTipoDeArchivo}"
						descriptorLabels=" Nombre" entityProperty="id"
						descriptorProperties="nombre, descripcion"
						entityName="TipoDeArchivo" where2=" padre_id IS NOT NULL "
						accessProperty="id" /></td>
			</tr>
			<tr>
				<th colspan="1">Descripci&oacute;n</th>
				<td colspan="3"><fck:editor
						instanceName="process.objetoiArchivo.detalle" toolbarSet="Civitas"
						width="650" height="200"
						value="${ProcessForm.process.objetoiArchivo.detalle}"></fck:editor></td>
			</tr>
			<tr>
				<th>S&oacute;lo Lectura:</th>
				<td><html:select property="process.objetoiArchivo.noBorrar"
						name="ProcessForm"
						value="${ProcessForm.process.objetoiArchivo.noBorrar}">
						<html:option value="false">NO</html:option>
						<html:option value="true">SI</html:option>
					</html:select></td>
			</tr>
			<tr>
				<th>Documento:</th>
				<td colspan="3"><html:file property="process.formFile" accept=".pdf"/></td>
			</tr>
		</table>
		<br />
		<input type="button" value="Guardar" onclick="guardar();" />
		<input type="button" value="Cancelar" onclick="cancelar();" />
	</logic:equal>
	<br />

</html:form>

<script language='javascript'>
	var accion = $('accion');
	var form = $('oForm');
	function guardar(){
		accion.value = 'guardar';
		form.submit();
	}		
	function adjuntar(){
		accion.value = 'adjuntar';
		form.submit();
	}
	function eliminar(id){
		var archivoId = $('archivoId');
		archivoId.value = id;
		accion.value = 'eliminar';
		form.submit();
	}		
	function cancelar(){
		accion.value = 'cancelar';
		form.submit();
	}	
</script>