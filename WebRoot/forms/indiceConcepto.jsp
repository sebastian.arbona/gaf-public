<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Asociaci&oacute;n Indice-Concepto</div>
<html:form
	action="/actions/process.do?do=process&processName=IndiceConcepto"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="guardar"/>
	<table>
		<tr>
			<th>Indice</th>
			<td>
				${ProcessForm.process.indice.nombre}
			</td>
		</tr>
		<tr>
			<th>Concepto</th>
			<td>
				<asf:select name="process.conceptoId"
					entityName="com.nirven.creditos.hibernate.CConcepto"
					value="${ProcessForm.process.conceptoId}"
					listCaption="getConcepto,getDetalle" listValues="getConcepto" />
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Guardar"></html:submit>
			</th>
		</tr>
	</table>
</html:form>