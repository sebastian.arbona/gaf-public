<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>

<script language="javascript" type="text/javascript">

	
	$(document).ready(function(){
	
		//Se ocultan todas los elementos
	   $('#nombre').hide();
	   $('#expediente').hide();
	   $('#caratula').hide();
	   $('#juzgado').hide();
	   $('#definitivo').hide();
	   $('#fecha').hide();
	   $('#tipo').hide();
	   $('#monto').hide();
	   
	   
	   //Se muestran los elementos que corresponden al proceso seleccionado
	   $j = jQuery.noConflict();
	   var idTipoProceso = $j("#idTipoProceso").val();
		 $j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=getElementosAMostrar', 
			'&idTipoProceso='+idTipoProceso,
			function(data) {
			var elem = data.split('-');
				for (x=0;x<elem.length;x++){
					$('#'+elem[x]).show();
				}
			}); 
	});

</script>

<div class="title">
	Proceso
</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<input type="hidden" name="entity.proceso_id" value="${paramValue[0]}">
	<input id="idTipoProceso" type="hidden" name="idTipoProceso" value="${paramValue[0]}">
	<logic:equal parameter="do" value="load">
		<html:hidden property="entity.nuevo" value="false" />
	</logic:equal>
	<table>
		
		<tr>
			<th>Nombre del Proceso:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.nombreProceso"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.nombreProceso}</td>
			</logic:notEqual>
		</tr>
		<tr id="nombre">
			<th>Nombre:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.nombre"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.nombre}</td>
			</logic:notEqual>
		</tr>
		<tr id="expediente">
			<th>Expediente:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.expediente"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.expediente}</td>
			</logic:notEqual>
		</tr>
		<tr id="caratula">
			<th>Caratula:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.caratula"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.caratula}</td>
			</logic:notEqual>
		</tr>
		<tr id="juzgado">
			<th>Juzgado:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.juzgado"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.juzgado}</td>
			</logic:notEqual>
		</tr>
		<tr id="definitivo">
			<th>Definitivo:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:checkbox property="entity.definitivo"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.definitivo}</td>
			</logic:notEqual>
		</tr>
		<tr id="fechaAlta">
			<th>Fecha de Alta:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><asf:calendar property="AbmForm.entity.fechaAltaStr" attribs="class='fechaStr'"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.fechaAltaStr}</td>
			</logic:notEqual>
		</tr>
		<tr id="fecha">
			<th>Fecha:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><asf:calendar property="AbmForm.entity.fechaStr" attribs="class='fechaStr'"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.fechaStr}</td>
			</logic:notEqual>
		</tr>
		
		<tr id="fechaVencimiento">
			<th>Fecha de Vencimiento:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
			<td><asf:calendar property="AbmForm.entity.fechaVencimientoStr" attribs="class='fechaStr'"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.fechaVencimientoStr}</td>
			</logic:notEqual>
		</tr>
		<tr id="tipo">
			<th>Tipo:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.tipo"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.tipo}</td>
			</logic:notEqual>
		</tr>
		<tr id="monto">
			<th>Monto:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.monto"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="">
				<td>${AbmForm.entity.monto}</td>
			</logic:notEqual>
		</tr>
		
		
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
  
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.selectboxes.min.js"></script>
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.colorPicker.js"/></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/colorPicker.css" type="text/css" />
