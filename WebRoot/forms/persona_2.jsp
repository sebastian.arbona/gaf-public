<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<script>

    function terminadaLaCargaDelMapa(){
        cargarMapa( null );
    }

    function ocultarFrame(oculta) {
        if (oculta) {
            $("mapagmap").style.display = 'none';
        } else {
            $("mapagmap").style.display = 'block';
        }
    }
</script>

<div class="title">Datos Personales</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true">
    <table border="0">

        <html:hidden property="entity.codi01" value="101"/>
        <html:hidden property="entity.id"/>
        
        <tr><th>Tipo de Persona:</th><td><asf:select attribs="OnChange='tipo(this.options[this.selectedIndex].value)'" name="entity.tipo12" listCaption="F�sica,Jur�dica" listValues="F,J" value="${AbmForm.entity.tipo12}"/></td></tr>
        <tr><th>Apellido y Nombre:</th><td><html:text property="entity.nomb12Str" size="40" maxlength="100" /></td></tr>
        <tr><th>Estado Civil:</th><td><asf:select name="entity.eciv12" nullValue="true" entityName="com.asf.hibernate.mapping.Tipificadores" listCaption="getCodigo,getDescripcion" listValues="getCodigo" value="AbmForm.entity.eciv12" filter="categoria='EstadoCivil' order by codigo"/></td></tr>
        <tr><th>Tipo de Documento:</th><td><asf:select name="entity.codi47" entityName="com.civitas.hibernate.persona.Tipodoc" listCaption="getTido47,getDeta47" listValues="getCodi47" value="${AbmForm.entity.codi47}"/></td></tr>
        <tr><th>N�mero de Documento:</th><td><asf:text type="long" name="AbmForm" property="entity.nudo12Str" maxlength="9"/></td></tr>
        <tr><th>C�dula de Identidad:</th><td><asf:text type="long" name="AbmForm" property="entity.cedu12Str" maxlength="8"/></td></tr>
        <tr><th>Expedida por:</th><td><html:text property="entity.expe12" maxlength="25"/></td></tr>
        <tr><th>C.U.I.L.:</th><td><asf:text type="long" name="AbmForm" property="entity.cuil12Str" maxlength="11"/></td></tr>
        <tr><th>Fecha de Nacimiento:</th><td><asf:calendar property="AbmForm.entity.fena12Str"/></td></tr>
        <%--tr><th>Domicilio:</th><td><html:text property="entity.domi12" maxlength="35"/></td></tr--%>
        <%--tr><th>Localidad:</th><td><html:text property="entity.loca12" maxlength="25"/></td></tr--%>
        <tr>
            <th>Calle:</th>
            <td><asfgat:callepopup attribs="onblur='cargarMapa();'" property="entity.idcalleStr" value="${AbmForm.entity.idcalleStr}" onChange="traerLocalidades();"/></td>
        </tr>
        <tr><th>Calle:</th>
            <td id="calle1" colspan="3">
                <html:text size="73" onblur="cargarMapa();" property="entity.calle" value="${AbmForm.entity.calle}" styleId="calle" maxlength="80"/></td>
        </tr>

        <tr><th>N�mero:</th><td><html:text onblur="cargarMapa();" property="entity.numero" value="${AbmForm.entity.numero}" styleId="num" maxlength="5"/></td></tr>
        <tr><th>Manzana / Piso:</th><td><html:text property="entity.piso" value="${AbmForm.entity.piso}" styleId="mza" maxlength="2"/></td></tr>
        <tr><th>Lote / Departamento:</th><td><html:text property="entity.dpto" value="${AbmForm.entity.dpto}" styleId="lote" maxlength="2"/></td></tr>

        <tr><th>Barrio:</th><td><asf:select name="entity.idbarrioStr" listCaption="descBrr" listValues="codiBrr" entityName="com.civitas.hibernate.persona.Barrio" value="${AbmForm.entity.idbarrioStr}" filter="codiBrr like '%' order by descBrr" nullValue="true" nullText="&lt;&lt;NONE&gt;&gt;"/></td></tr>
        <tr><th>Localidad:</th><td><asf:select attribs="onChange='cargarMapa(options[selectedIndex].text);'"  name="entity.idlocalidadStr" nullValue="true" entityName="com.civitas.hibernate.persona.Localidad" listValues="getIdlocalidad" listCaption="getCp,getNombre,provin.deta08" filter="cp LIKE '%' ORDER BY cp" value="${AbmForm.entity.idlocalidadStr}"/></td>
            <td id="loc1"><b>Localidad:</b>&nbsp;<html:text onblur="cargarMapa();" styleId="loc" value="${AbmForm.entity.localidad2}" property="entity.localidad2" maxlength="30" /></td>
        </tr>
        <tr><th>Ubicaci�n:</th><td><html:text property="entity.zona12" maxlength="35"/></td></tr>
        <tr><th>Lugar:</th><td><asf:select attribs="onChange='cargarMapa(options[selectedIndex].text);'"  name="entity.codi08" entityName="com.civitas.hibernate.persona.Provin" listCaption="getDeta08" listValues="getCodi08" value="AbmForm.entity.codi08" filter="codi08 LIKE '%' ORDER BY deta08"/></td></tr>
        <%--tr><th>C�digo Postal:</th><td><html:text property="entity.cpos12" maxlength="4"/></td></tr--%>
        <tr><th>Tel�fono:</th><td><html:text property="entity.tele12" maxlength="15"/></td></tr>
        <tr><th>Email:</th><td><html:text property="entity.emia12" maxlength="100"/></td></tr>
        <tr><th>Grupo Sangu�neo:</th><td><html:text property="entity.gsan12" maxlength="7"/></td></tr>
        <tr><th>Sexo:</th><td><asf:select name="entity.sexo12" listCaption="&lt;&lt;NONE&gt;&gt;,Masculino,Femenino" listValues=",M,F" value="${AbmForm.entity.sexo12}"/></td></tr>
        <tr><th>Nacionalidad:</th><td><asf:select name="entity.pais12Str" nullValue="true" entityName="com.civitas.hibernate.persona.Nacionalidad" listCaption="getNacionalidad" listValues="getIdnacionalidad" value="${AbmForm.entity.pais12}" /></td></tr>

    </table>
    <br>
    <center>
        <iframe width="75%" name="mapagmap" id="mapagmap" height="320px" src="${pageContext.request.contextPath}/gmaps.html"  scrolling="no" style="display: none" ></iframe>
    </center>
    <br>
    <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
        <html:submit><bean:message key="abm.button.save"/></html:submit>
    </asf:security>
    <html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
<script>
    var controlLocalidad = $('entity.idlocalidadStr');
    var controlProvincia = $('entity.codi08');

    //Carga del Mapa.
    //M�s oportunidades de encontrar una ubicaci�n determinada, si se le da el siguiente formato a la b�squeda:
    //Direcci�n, ciudad, estado o Direcci�n, ciudad, c�digo postal..
    function cargarMapa(direccion) {
        var calle = "";
        var cpostal = "";
        var localidad = "";
        var provincia = "";

        if (direccion == null || direccion == '' || direccion == undefined ) {
            direccion = "";
            direccion = controlLocalidad.options[controlLocalidad.selectedIndex].text;
        }
	
        direccion = direccion.split("-");
        cpostal = direccion[0] != null && direccion[0] != '<<NONE>>' ? direccion[0] : '';
        localidad = direccion[1] != null && direccion[1] != '' ? direccion[1] : $F("loc");
        provincia = direccion[2] != null ? direccion[2] : controlProvincia.options[controlProvincia.selectedIndex].text;
        calle = $F("entity_idcalleStrDescriptor") != null && $F("entity_idcalleStrDescriptor") != '' ? $F("entity_idcalleStrDescriptor") : $F("calle");

        direccion = calle + " , " + $F("num") + " , " + localidad + " , "+ provincia + " , " + cpostal;
        window.frames["mapagmap"].buscarDireccion(direccion);

    }
</script>