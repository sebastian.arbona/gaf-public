<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Nueva Factura</div>



<html:messages id="errores"></html:messages>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.4.2.min.js">
</script>

<script type="text/javascript">
	$j = jQuery.noConflict();
	$j(document).ready(function() {
		cambiarCotiz();
	});
</script>

<html:form
	action="/actions/process.do?do=process&processName=GastosRecuperarProcess"
	styleId="ProcessForm">
	<html:errors />
	<html:hidden property="process.accion" styleId="accion" />
	<html:hidden property="process.sid" />
	<html:hidden property="process.factura.id"  />
	<html:hidden property="process.contextoGAF" styleId="contextoGAF" />
	<html:hidden property="process.detalleQuitar" styleId="detalleQuitar" />
	<div class="grilla">
		<table>
			<tr>
				<th>Fecha</th>
				<td><asf:calendar
						property="ProcessForm.process.factura.fechaStr" maxlength="10"></asf:calendar></td>
				<th>Fecha Contable</th>
				<td><asf:calendar
						property="ProcessForm.process.factura.fechaContableStr"
						maxlength="10"></asf:calendar></td>
				<th>Tipo de Comprobante</th>
				<td><asf:select name="process.factura.tipoComprobante"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.factura.tipoComprobante}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'TipoComprobanteExt'  order by codigo" /></td>

			</tr>
			<tr>
				<th>Sucursal y N�mero</th>
				<td><asf:text type="long" maxlength="12"
						property="process.factura.sucursal" name="ProcessForm" /> <asf:text
						type="long" maxlength="12" property="process.factura.numero"
						name="ProcessForm" /></td>
				<th>Importe</th>
				<td><asf:text type="decimal" maxlength="30" name="ProcessForm"
						property="process.factura.importe"
						attribs="onchange=\"calcularCotizado();\"" id="importe"></asf:text></td>
				<th>Saldo</th>
				<td><asf:text type="decimal" maxlength="30" name="ProcessForm"
						property="process.saldo" id="saldo" readonly="true"></asf:text></td>
			</tr>
			<tr>
				<th>Moneda</th>
				<td><asf:select name="process.factura.idMoneda"
						entityName="com.asf.gaf.hibernate.Moneda"
						listCaption="getAbreviatura" listValues="getId"
						value="ProcessForm.process.factura.idMoneda"
						attribs="onchange=\"cambiarCotiz();\"" /></td>
				<th>Cotizacion</th>
				<td><input type="text" id="cotizacion" readonly="readonly" /></td>
				<th>Importe Cotizado</th>
				<td><input type="text" id="cotizado" readonly="readonly" /></td>
			</tr>
			<tr>
				<th>Proveedor</th>
				<td colspan="3"><asf:select2 name="ProcessForm"
						property="process.factura.idProveedor"
						descriptorLabels="Razon Social"
						descriptorProperties="persona.nomb12" entityName="Proveedor"
						accessProperty="codigo" entityProperty="id" /></td>
				<th>Motivo</th>
				<td><asf:select name="process.factura.motivo"
						value="${ProcessForm.process.factura.motivo}"
						listCaption="Tasacion, Auditoria, Honorarios Notariales, Gastos Gesti�n Prenda"
						listValues="Tasaci�n, Auditor�a, HonorariosNotariales, GastosGestionPrenda" />
				</td>
			</tr>
			<tr>
				<th>Expte Pago</th>
				<td colspan="5"><asf:selectpop name="process.factura.expepago"
						entityName="DocumentoADE"
						value="${ProcessForm.process.factura.expepago}"
						caseSensitive="true" values="identificacion"
						columns="N�,Asunto,Iniciador,CUIT/DNI"
						captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
						size1="25" size2="70" cantidadDescriptors="2" /></td>
			</tr>
		</table>
	</div>

	<div class="title">Detalle de la Factura</div>
	<div class="grilla">
		<table>
			<tr>
				<th>Proyecto</th>
				<td colspan="3"><asf:selectpop
						name="process.detalle.numeroAtencion"
						value="${ProcessForm.process.detalle.numeroAtencion}"
						entityName="ObjetoiEstado" title="Credito" caseSensitive="true"
						values="objetoi.numeroAtencion"
						columns="Proyecto, Tomador, Expediente, Estado"
						onChange="mostrarExpediente();"
						captions="objetoi.numeroAtencion,objetoi.persona.nomb12,objetoi.expediente,estado.nombreEstado"
						filter="fechaHasta is null AND estado.nombreEstado NOT IN (%27CANCELADO%27,%27FINALIZADO%27,%27DESISTIDO%27) ORDER BY objetoi.numeroAtencion"
						cantidadDescriptors="1" /></td>
			</tr>
			<tr>
				<th>Expediente</th>
				<td colspan="3"><asf:text
						maxlength="30" type="text"
						name="ProcessForm" property="process.detalle.expediente" readonly="true"/>
			</tr>
			<tr>
				<th>Observaciones</th>
				<td colspan="3"><html:textarea name="ProcessForm"
						property="process.detalle.observaciones" cols="70" rows="1" /></td>
			</tr>
			<tr>
				<th>Importe</th>
				<td colspan="3"><asf:text maxlength="30" type="decimal"
						name="ProcessForm" property="process.detalle.importe"></asf:text>
					<html:submit value="Agregar Detalle" onclick="agregar();" /></td>
			</tr>
		</table>

		<display:table name="ProcessForm" property="process.detalles"
			export="false" id="reportTable">
			<display:column title="Importe" property="importe"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column title="Tomador" property="credito.persona.nomb12" />
			<display:column title="Proyecto" property="credito.numeroAtencion" />
			<display:column title="Expediente" property="expediente" />
			<display:column title="Observaciones" property="observaciones" />
			<display:column title="Eliminar">
				<a onclick="quitar(${reportTable_rowNum});">Eliminar</a>
			</display:column>
		</display:table>

		<div style="margin-top: 10px">
			<html:submit value="Guardar Factura" onclick="guardar();" />
			<input type="button" value="Cancelar" onclick="cancelar();" /> <input
				type="button" value="Imprimir Volante" onclick="imprimirVolante();" />
		</div>
	</div>
</html:form>

<script>
//var form = $('ProcessForm');
var accion = $('accion');

function agregar(){
	accion.value = 'detalle';
	return true;
}

function quitar(i) {
	accion.value = 'quitar';

	var indice = $('detalleQuitar');
	indice.value = i;
	
	var form = $('ProcessForm');
	form.submit();
	
	return true;
}

function cancelar() {
	var url = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=GastosRecuperarProcess';
	window.location = url;
	return;
}

function imprimirVolante(){
	var url = '${ProcessForm.process.contextoGAF}/actions/jreport.do?do=exec&reportName=volanteDebito.jasper&params=REPORT_QUERY%3D+%3BWHERE%3D+WHERE+d.nroDebito+%3D${ProcessForm.process.factura.nroDebito}+AND+d.ejercicio%3D${ProcessForm.process.factura.ejercicio}&export=PDF';
	window.location = url;
	return;

}

function guardar() {
	accion.value = 'guardar';
	return true;
}

function cambiarCotiz(){
    var moneda = $('process.factura.idMoneda'); 
    url="${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxMonedaCotizacion&idMoneda="+ moneda.value;
    retrieveURL(url, cambiarCotizResponse);
}

function cambiarCotizResponse(responseText){
    $('cotizacion').value = responseText;
    calcularCotizado(); 
}

function calcularCotizado() {
	var importe = $('importe');
	var cotizacion = $('cotizacion');
	$('cotizado').value = new Number(importe.value) * new Number(cotizacion.value);
}

function mostrarExpediente() {
    //cnoguerol, requiere un timeout de lo contrario el popupaction escribe primero el metodo para actualizar el detalle del expediente
    //y no alcanza a escribir el metodo para el detalle del proyecto.
    setTimeout(function(){
        var proyecto = $('process.detalle.numeroAtencion');
        url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarExpedientePorProyecto&numeroAtencion="+proyecto.value;
        retrieveURL(url, mostrarExpedienteResponse);
        },1000);
}

function mostrarExpedienteResponse(responseText) {
	$('process.detalle.expediente').value = responseText;
	$('process.detalle.expediente').onchange();
	
}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>