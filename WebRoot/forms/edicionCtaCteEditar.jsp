<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Edicion Cta Cte</div>
<html:form
	action="/actions/process.do?do=process&processName=EdicionCtaCte"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="guardar"/>
	<table>
		<tr>
			<th>Credito</th>
			<td>
				${ProcessForm.process.ctacte.id.objetoi.numeroAtencion}
			</td>
			<th>Cuota</th>
			<td>${ProcessForm.process.ctacte.cuota.numero} 
				<logic:equal name="ProcessForm" property="process.ctacte.cuota.estado" value="1">
					(No cancelada)
				</logic:equal>
				<logic:equal name="ProcessForm" property="process.ctacte.cuota.estado" value="2">
					(Cancelada)
				</logic:equal>
			</td>
		</tr>
		<tr>
			<th>Boleto</th>
			<td>${ProcessForm.process.ctacte.boleto.numeroBoleto} | ${ProcessForm.process.ctacte.boleto.id}</td>
			<th>Importe actual</th>
			<td>${ProcessForm.process.ctacte.boleto.importe}</td>
		</tr>
	</table>
	<table>
		<tr>
	    	<th>Mov</th>
	    	<th>Item</th>
	    	<th>Per</th>
	    	<th>Fecha Gen</th>
	    	<th>Concepto</th>
	    	<th>Importe</th>
	    	<th>Boleto</th>
	    	<th>Cuota</th>
	  	</tr>
	  	<tr>
	    	<td>${ProcessForm.process.ctacte.id.movimientoCtacte}</td>
	    	<td>${ProcessForm.process.ctacte.id.itemCtacte}</td>
	    	<td>${ProcessForm.process.ctacte.id.periodoCtacte}</td>
	    	<td><asf:calendar property="ProcessForm.process.fechaGeneracion" value="${process.fechaGeneracion}"/></td>
	    	<td>
	    		<asf:select name="process.concepto"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.concepto}"
					listCaption="getDescripcion" listValues="getCodigo"
					filter="categoria = 'cuota.concepto' order by codigo" />
	    	</td>
	    	<td><html:text property="process.importe"></html:text></td>
	    	<td>
	    		<asf:select entityName="com.nirven.creditos.hibernate.Boleto" 
        				listCaption="getNumeroBoleto,getId" listValues="getId"
          				name="process.boletoId" 
          				value="${ProcessForm.process.ctacte.boleto.id}"
          				filter=" objetoi.id = ${ProcessForm.process.ctacte.id.objetoi.id} and tipo = '${ProcessForm.process.ctacte.boleto.tipo}' "/>
	    	</td>
	    	<td>
	    		<asf:select entityName="com.nirven.creditos.hibernate.Cuota" 
        				listCaption="getNumero,getEstadoStr" listValues="getId"
          				name="process.cuotaId" 
          				value="${ProcessForm.process.cuotaId}"
          				filter=" credito.id = ${ProcessForm.process.ctacte.id.objetoi.id}"/>
	    	</td>
	  	</tr>
	  	<tr>
	  		<th colspan="8">
	  			<html:submit value="Guardar"></html:submit>
	  		</th>
	  	</tr>
	</table>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>