<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="com.nirven.jbpm.forms.VariableType"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@page import="javax.jcr.Node"%>
<%@page import="com.nirven.jbpm.forms.*"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %> 
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="FCK" %>
<%@ taglib uri="/WEB-INF/jbpm.tld" prefix="jbpm" %>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.4.2.min.js"></script>
<script language="JavaScript">
$.noConflict();

</script>

<html:form styleId="formularioJbpm" action="/actions/jbpm.do" onsubmit="if(getElem('do').value=='')getElem('do').value='save';"  enctype="multipart/form-data">
	<html:hidden property="do" value="save" styleId="do" />
	<div class="title"><b>${JBPMForm.taskBean.tramite.nombre}: ${JBPMForm.taskBean.name}</b></div><p />
		
  <html:hidden value="${JBPMForm.taskBean.id}" property="id" />


		<bean:define id="tinfo" name="JBPMForm" property="taskInfo">
		</bean:define>
		<logic:equal value="false" name="JBPMForm" property="taskDynamic">		
			<jsp:include page="${tinfo.pageName}"></jsp:include>
		</logic:equal>
		<logic:equal value="true" name="JBPMForm" property="taskDynamic">
	<table>
		<logic:iterate name="JBPMForm" property="taskVariables" id="taskVar">
			<bean:define id="taskVar" name="taskVar" type="com.nirven.jbpm.forms.Variable"/>
			<logic:notEqual name="taskVar" property="type" value="<%= VariableType.IFRAME.toString() %>">
				<tr>
					<th title="${taskVar.description}" >
						${taskVar.etiqueta}
					</th>
					<td title="${taskVar.description}">
						<%-- TEXT --%>
						<logic:equal name="taskVar" property="type" value="<%= VariableType.TEXT.toString() %>">
							<logic:equal value="true" name="taskVar" property="readOnly">
								${taskVar.value}
							</logic:equal>
							<logic:equal value="false" name="taskVar" property="readOnly">
								<html:text value="${taskVar.value}" size="50"
									property="taskFormParameters(${taskVar.name})"
									title="${taskVar.description}" />
							</logic:equal>
						</logic:equal>
						<%-- TEXT AREA --%>
						<logic:equal name="taskVar" property="type" value="<%= VariableType.TEXTAREA.toString() %>">
							<logic:equal value="true" name="taskVar" property="readOnly">
								${taskVar.value}
							</logic:equal>
							<logic:notEqual value="true" name="taskVar" property="readOnly">
								<FCK:editor instanceName="taskFormParameters(${taskVar.name})" basePath="/fckeditor/" 
									toolbarSet="Civitas" width="650" height="200" value="${taskVar.value}"></FCK:editor>
							</logic:notEqual>
							
						</logic:equal>
						<%-- ENTERO --%>
						<logic:equal name="taskVar" property="type" value="<%= VariableType.NUMBER.toString() %>">
							<logic:equal value="true" name="taskVar" property="readOnly">
								${taskVar.value}
							</logic:equal>
							<logic:equal value="false" name="taskVar" property="readOnly">
								<asf:text property="taskFormParameters(${taskVar.name})"
									name="taskFormParameters(${taskVar.name})"
									value="${taskVar.value}" type="long" maxlength="20"
									readonly="${taskVar.readOnly}" />									
							</logic:equal>
						</logic:equal>
						<%-- DECIMAL --%>
						<logic:equal name="taskVar" property="type" value="<%= VariableType.DECIMAL.toString() %>">
							<logic:equal value="true" name="taskVar" property="readOnly">
								${taskVar.value}
							</logic:equal>
							<logic:equal value="false" name="taskVar" property="readOnly">
								<asf:text property="taskFormParameters(${taskVar.name})"
									name="taskFormParameters(${taskVar.name})"
									value="${taskVar.value}" type="decimal" maxlength="20" />
							</logic:equal>
						</logic:equal>
					  	<%-- DATE  --%>
						<logic:equal name="taskVar" property="type" value="<%= VariableType.DATE.toString() %>">
							<logic:equal value="true" name="taskVar" property="readOnly">
								${taskVar.value}
							</logic:equal>
							<logic:equal value="false" name="taskVar" property="readOnly">
								<asf:calendar property="taskFormParameters(${taskVar.name})" toolTips="${taskVar.description}" value="${taskVar.value}" />
							</logic:equal> 
						</logic:equal>
						<%-- SELECT--%>	
						<logic:equal name="taskVar" property="type" value="<%= VariableType.SELECT.toString() %>">
						    <asf:select name="taskFormParameters(${taskVar.name})" entityName="${taskVar.entityName}" filter="${taskVar.filter}" listValues="${taskVar.listValues}" listCaption="${taskVar.listCaption}" 
							    nullValue="${taskVar.nullValue}" nullText="${taskVar.nullText}" value="${taskVar.value}" enabled="${!taskVar.readOnly}" />
						</logic:equal>
						<%-- POPUP--%>	
						<logic:equal name="taskVar" property="type" value="<%= VariableType.POPUP.toString() %>">
						    <asf:selectpop name="taskFormParameters(${taskVar.name})" entityName="${taskVar.entityName}" filter="${taskVar.filter}" values="${taskVar.listValues}" captions="${taskVar.listCaption}" 
							    caseSensitive="true" value="${taskVar.value}" enabled="${!taskVar.readOnly}" columns="${taskVar.nullText}"  />
						</logic:equal>
	                    <%-- YES/NO   --%>	
	                    <logic:equal name="taskVar" property="type" value="<%= VariableType.YESNO.toString() %>">
	                    	<asf:select name="taskFormParameters(${taskVar.name})" listValues="0,1" listCaption="No,Si" nullValue="${taskVar.nullValue}" 
	                    		enabled="${!taskVar.readOnly}" nullText="${taskVar.nullText}" value="${taskVar.value}" />
	                    </logic:equal>
	                    <%-- FILE   --%>
	                    <logic:equal name="taskVar" property="type" value="<%= VariableType.FILE.toString() %>">
	                    	<jbpm:file name="taskVar"  downloable="true" paramName="taskFormParameters(${taskVar.name})" readonly="${taskVar.readOnly}"></jbpm:file>
		                    
	                    </logic:equal>	
	                     <%-- LINK   --%>
	                    <logic:equal name="taskVar" property="type" value="<%= VariableType.LINK.toString() %>">
	                    	<logic:equal value="true" name="taskVar" property="readOnly">
								${taskVar.etiqueta}
							</logic:equal>
							<logic:equal value="false" name="taskVar" property="readOnly">  
								<logic:empty name="taskVar" property="nullText">
									<a href="${pageContext.request.contextPath}/<jbpm:taskTitle name="JBPMForm" property="taskBean.taskInstance" expression="${taskVar.listValues}" />" title="${taskVar.description}" target="${taskVar.listCaption}">${taskVar.etiqueta}</a>
								</logic:empty>
								<logic:notEmpty name="taskVar" property="nullText">
									<a href="<%=DirectorHelper.getString(((VariableSelect)taskVar).getNullText()) %>/<jbpm:taskTitle name="JBPMForm" property="taskBean.taskInstance" expression="${taskVar.listValues}" />" title="${taskVar.description}" target="${taskVar.listCaption}">${taskVar.etiqueta}</a>
								</logic:notEmpty>
	                    	</logic:equal>
	                    </logic:equal>                    	
					</td>
				</tr>
			</logic:notEqual>
			

             <%-- IFRAME   --%>
			<logic:equal name="taskVar" property="type" value="<%= VariableType.IFRAME.toString() %>">
				<td title="${taskVar.description}" colspan="2">
					<logic:equal value="true" name="taskVar" property="readOnly">
						<iframe src="${pageContext.request.contextPath}/<jbpm:taskTitle name="JBPMForm" property="taskBean.taskInstance" expression="${taskVar.listValues}"/>" width="900px" height="350px"></iframe>						
					</logic:equal>
					<logic:equal value="false" name="taskVar" property="readOnly">  
						<iframe src="${pageContext.request.contextPath}/<jbpm:taskTitle name="JBPMForm" property="taskBean.taskInstance" expression="${taskVar.listValues}"/>" width="900px" height="350px"></iframe>						
					</logic:equal>
				</td>  
			</logic:equal>
			
		</logic:iterate>
	</table>
	</logic:equal>
	
 	<div>	
		<logic:notEmpty name="JBPMForm" property="taskBean.taskInstance.actorId">
			<div style="display: inline;float: left; ">
				<logic:notEmpty name="JBPMForm"	property="taskBean.availableTransitions">
			     Acciones: 			    
				     <logic:iterate id="availableTransition" name="JBPMForm"
							property="taskBean.availableTransitions">
							<html:submit property="transition"
								value="${availableTransition.name}"
								onclick="getElem('do').value='saveAndClose'" />
					</logic:iterate>
				</logic:notEmpty>
			</div>
			
			<div style="display: inline;float: right; ">
				<logic:empty name="JBPMForm" property="taskBean.availableTransitions">
					<html:submit value="Aprobar Tarea"
						onclick="aprobarTarea();" />
			
				</logic:empty>
				
				<html:submit value="Guardar" onclick="guardar();" styleClass="saveAndClose"/>
			
			</logic:notEmpty>
			<logic:empty name="JBPMForm" property="taskBean.taskInstance.actorId">
				<div style="display: inline;float: right; ">
					<html:submit value="Tomar Tarea" onclick="tomarTarea();"/>
			</logic:empty>
				
			<html:submit value="Cancelar" onclick="window.location='${pageContext.request.contextPath}/actions/jbpm.do?do=showTaskInstanceList';return false;" />
			</div>
		<div style="display: block;clear: both;"></div></div>
	
	
</html:form>
<script type="text/javascript">	
function aprobarTarea(){
	if (confirm('�Estas seguro que desea aprobar la tarea?')){ 
		getElem('do').value='saveAndClose';
	}else{
		getElem('do').value='showTaskInstance';
	}
}

function guardar(){
	if (confirm('�Estas seguro que desea guardar la tarea?')){ 
		getElem('do').value='save';
	}else{
		getElem('do').value='showTaskInstance';
	}
}
function tomarTarea(){
	if (confirm('�Estas seguro que desea tomar la tarea?')){ 
		getElem('do').value='getTaskInstance';
	}else{
		getElem('do').value='showTaskInstance';
	}
}
</script>     
<iframe width="174px" height="189px" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

