<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Impacto Manual de Gastos a Recuperar</div>
<html:form
	action="/actions/process.do?do=process&processName=CtaCteManual"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" styleId="action" value="buscarGastos"/>
	<html:errors/>
	
	<logic:equal value="false" name="ProcessForm" property="process.success">
	<table>
		<tr>
			<th>Numero Atencion</th>
			<td>
				<asf:text maxlength="15" type="long" property="process.numeroAtencion" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th colspan="2"><html:submit value="Buscar Facturas"/></th>
		</tr>
	</table>
	</logic:equal>

	<logic:notEmpty name="ProcessForm" property="process.detalles">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.detalles" id="bean" export="true">
			<display:column title="Selec." media="html">
				<html:radio property="process.idDetalle" value="${bean.id}"/>
			</display:column>
 			<display:column class="center" title="Fecha" property="factura.fecha" decorator="com.asf.displayDecorators.DateDecorator"/>
  			<display:column title="Proveedor" property="factura.proveedor.persona.nomb12" sortable="true"/>
      		<display:column title="Pagado">
      				<logic:empty name="bean" property="factura.fechaPago">
      					No
      				</logic:empty>
      				
      				<logic:notEmpty name="bean" property="factura.fechaPago">
      					Sí
      				</logic:notEmpty>
      		</display:column>
      		<display:column title="Fecha Pago" property="factura.fechaPago" decorator="com.asf.displayDecorators.DateDecorator"/>
      		<display:column title="Sucursal" property="factura.sucursal" sortable="true"/>
      		<display:column title="Nro Factura" property="factura.numero" sortable="true"/>
      		<display:column title="Expte Pago" property="factura.expepago" sortable="true"/>
      		<display:column title="Total" property="factura.importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
      		<display:column title="Moneda" property="factura.moneda.abreviatura" sortable="true"/>
      		<display:column title="Importe" property="importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
      		<display:column title="Impactado Cta Cte">
   				<logic:notEqual name="bean" property="impactado" value="true">
   					No
   				</logic:notEqual>
   				<logic:equal name="bean" property="impactado" value="true">
   					Sí
   				</logic:equal>
      		</display:column>
      		<display:column title="Observaciones" property="observaciones" sortable="true"/>
 		</display:table>
	</div>
	
	<table>
		<tr>
			<th>Desembolso</th>
			<td>
				<html:select styleId="desembolsos" name="ProcessForm" property="process.idDesembolso">
					<html:optionsCollection property="process.desembolsos" label="numero" value="id"/>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Cuota</th>
			<td>
				<html:select styleId="cuotas" name="ProcessForm" property="process.idCuota">
					<html:optionsCollection property="process.cuotas" label="numero" value="id"/>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>Fecha Generacion:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaGeneracion" 
 					value="${process.custodia.fechaGeneracion}"/> 
			</td>
		</tr>
		<tr>
			<th>Fecha Proceso:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaProceso" 
 					value="${process.custodia.fechaProceso}"/> 
			</td>
		</tr>
		<tr>
			<th>Periodo</th>
			<td>
				<html:text property="process.periodo" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>Tipo Concepto</th>
			<td>
				<asf:select name="process.tipoConcepto"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.tipoConcepto}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'ctacte.detalle'  order by codigo" />
			</td>
		</tr>
		<tr>
			<th>Generar:</th>
			<td>
				<html:checkbox property="process.debito" name="ProcessForm"/>Debito
				<html:checkbox property="process.credito" name="ProcessForm"/>Credito
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Impactar" onclick="impactar();"></html:submit>
			</th>
		</tr>
	</table>
	</logic:notEmpty>
	
	<logic:equal value="true" name="ProcessForm" property="process.success">
		Los movimientos en cuenta corriente fueron generados correctamente.
	</logic:equal>
</html:form>
<script type="text/javascript">
	function impactar() {
		var action = document.getElementById('action');
		action.value = 'impactar';
	}
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>