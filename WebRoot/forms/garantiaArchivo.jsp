<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	enctype="multipart/form-data" styleId="oForm">
	
	<div id="grilla">
	
	<html:hidden property="process.action" name="ProcessForm"
		styleId="action" />
	<html:hidden property="process.cid" name="ProcessForm" />

	<html:errors/>

	<table>
		<tr>
			<th>Tipo:</th>
			<td><asf:select name="process.tipoId" listCaption="descripcion"
					listValues="id"
					entityName="com.civitas.hibernate.persona.TipoArchivo"
					value="${ProcessForm.process.tipoId}" /></td>
		</tr>
		<tr>
			<th>Documento:</th>
			<td colspan="3"><html:file property="process.formFile" /></td>
		</tr>
		<tr>
			<th>Descripci&oacute;n:</th>
			<td><html:textarea property="process.detalle"></html:textarea></td>
		</tr>
	</table>
	<br />
	<input type="button" value="Guardar" onclick="guardar();" />
	<input type="button" value="Cancelar" onclick="cancelar();" />
	
	</div>
</html:form>

<script language='javascript'>
	var accion = $('action');
	var form = $('oForm');
	function guardar(){
		accion.value = 'guardar';
		form.submit();
	}
	function cancelar(){
		accion.value = 'cancelar';
		form.submit();
	}														
</script>