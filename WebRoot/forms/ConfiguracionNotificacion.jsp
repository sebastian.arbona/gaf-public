<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

<div class="title">Configuracion de Notificaciones</div>
<p>
<html:form
	action="/actions/process.do?do=process&processName=ConfNotificacionProcess"
	styleId="oForm">
	<html:hidden property="process.action" name="ProcessForm" styleId="action"/>
	<html:hidden property="process.idConfiguracion"  name="ProcessForm" value="${ProcessForm.process.idConfiguracion}"/>
	
	<html:hidden property="process.eliminar"  name="ProcessForm" value="${ProcessForm.process.eliminar}"/>
	<div id="linea" title="Configuracion de Notificaciones" style="visibility: visible;">
		<table>
			<tr><th>Denominacion:</th><td><html:text name="ProcessForm" property="process.denominacion" maxlength="30"/></td></tr>
			<tr><th>Asunto:</th><td><html:text name="ProcessForm" property="process.asunto" maxlength="30"/></td></tr>
			<tr><th>Texto de aviso:</th><td>
			<fck:editor toolbarSet="Civitas" instanceName="process.texto" width="600" height="200" value="${ProcessForm.process.texto}" />
		</table>
	</div>
		<input type="button" value="${ProcessForm.process.eliminar}" onclick="guardar();">
		<input type="button" value="Cancelar" onclick="cancelar();">
	
	<br/><br/>
	</html:form>
	<script type="text/javascript">
		function guardar(){ 
			var accion = document.getElementById('action');
			accion.value="guardar";
			var formulario = document.getElementById('oForm');
			formulario.submit();
		}
		
	function cancelar() {
   		var accion = document.getElementById('action');
   		accion.value="cancelar";
   		var form = document.getElementById('oForm');
   		form.submit();
    }
	</script>
	