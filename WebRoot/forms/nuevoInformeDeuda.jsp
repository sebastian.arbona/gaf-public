<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
																							
<div class="title" style="margin-bottom: 30px">Generaci�n Informe de Deuda</div>

<html:form action="/actions/process.do?do=process&processName=InformeDeudaProcess" styleId="ProcessForm">
	<html:errors/>

	<html:hidden property="process.action" value="guardarInforme"/>
	<html:hidden property="process.cid"/>

	<table style="border: 2px solid #CCCCCC;" width="750">
		<tr>
			<th>�ltima actualizaci�n Comportamientos Pago:</th>
			<td colspan="3"><html:text name="ProcessForm" property="process.ultimaActualizacion" readonly="true"></html:text></td>
		</tr>
		<tr>
			<th>�ltima recaudaci�n realizada el d�a:</th>
			<td><html:text name="ProcessForm" property="process.ultimaRecaudacion" readonly="true"></html:text></td>
			<th>Con fecha de Cobranza el d�a:</th>
			<td><html:text name="ProcessForm" property="process.ultimaCobranza" readonly="true"></html:text>
			</td>
		</tr>
	</table>

	<table style="margin-top: 30px; width: 100%">
		<tr>
			<th>Procesar vencimiento hasta</th>
			<td><asf:calendar property="ProcessForm.process.fechaHasta"/></td>
		</tr>
		<tr>
			<th>Procesar l�nea</th>
			<td>
				<html:select styleId="lineaCreditoId" property="process.lineasSeleccionadas" multiple="true"  size="8">
					<html:optionsCollection  property="process.lineas" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>
		<tr>
			<th colspan="2"><html:submit value="Generar Informe Deuda"/></th>
		</tr>
	</table>
</html:form>

<html:form action="/actions/informeEnte.do" styleId="InformeEnteRecaudadorForm">
	<input type="hidden" id="do" name="do"/>
	<table style="margin-top: 20px; width: 100%">
		<tr>
			<th colspan="2">Generar Informe Clientes</th>
		</tr>
		<tr>
			<th>Acci�n</th>
			<td>
				<div id="tipoRegistroABM">
					<select name="tipoRegistros">
						<option value="A">Alta</option>
						<option value="B">Baja</option>
						<option value="M">Modificaci�n</option>
					</select>
				</div>
				<div id="tipoRegistroA" style="display: none">
					<select name="tipoRegistros">
						<option value="A" selected="selected">Alta</option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<th colspan="2"><html:submit value="Exportar a TXT" onclick="imprimirClientes();"/></th>
		</tr>
	</table>
</html:form>

<script>
var form = $('InformeEnteRecaudadorForm');
var doField = $('do');
function imprimirClientes() {
	window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
		"width=450, height=180");
	doField.value = 'generarClientes';
	return true;
}
</script>

<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>