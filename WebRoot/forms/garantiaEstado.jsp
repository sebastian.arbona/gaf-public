<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<div class="title">
	garantiaEstado.jsp
</div>
<br>
<br>
<html:form
	action="/actions/garantiaEstadoAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" value="0" />
	<html:hidden property="entity.objetoiGarantia.id" value="${garantiaEstadoForm.entity.objetoiGarantia.id}" />
		
	<html:hidden property="paramValue[0]" styleId="paramValue"	value="${paramValue[0]}" />
	<html:hidden property="paramName[0]" styleId="paramName" value="${paramName[0]}" />
	<html:hidden property="paramComp[0]" styleId="paramComp" value="${paramComp[0]}" />
	<html:hidden property="entity.usuario" styleId="paramComp" value="${garantiaEstadoForm.entity.usuario}" />
	<html:hidden property="filter" value="true" />
	<table border="0">
		<tr>
			<th>
				Estado:
			</th>
			<td>
				<asf:select name="entity.estado.id"
 					entityName="com.civitas.hibernate.persona.Estado" 
					value="${garantiaEstadoForm.entity.estado}"
					listCaption="nombreEstado" listValues="idEstado"
					filter="tipo = 'Garantia' and manual = 1  order by nombreEstado"
					nullValue="true" nullText="Seleccione Estado..." />
			</td>
		</tr>
		<tr>
			<th>
				Fecha de Estado:
			</th>
			<td>
				<asf:calendar  property="garantiaEstadoForm.entity.fechaEstadoStr" value="${garantiaEstadoForm.entity.fechaEstadoStr}"/>
			</td>
		</tr>
		<tr>
			<th>
				Fecha de Proceso:
			</th>
			<td>
				<asf:calendar property="garantiaEstadoForm.entity.fechaProcesoStr" value="${garantiaEstadoForm.entity.fechaProcesoStr}"/>
			</td>
		</tr>
	    <tr>
      		<th>
        		Importe:
      		</th>
      	<td>
        	<asf:text attribs="class='monto'" name="garantiaEstadoForm" property="entity.importe" type="decimal" maxlength="12" />
      	</td>
    </tr>		
	</table>
	<asf:security action="/actions/garantiaEstadoAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>Guardar</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<!-- Raul Varela -->
