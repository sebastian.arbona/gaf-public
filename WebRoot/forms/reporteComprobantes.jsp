<%@ page language="java"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.nirven.creditos.hibernate.BoletoTipoEnum"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<%
	List<BoletoTipoEnum> boletoTipos = Arrays.asList(BoletoTipoEnum.values());
	pageContext.setAttribute("boletoTipos", boletoTipos);
%>

<div class="title">Reporte de Comprobantes</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />
	<table>
		<asf:security
			action="/actions/process.do?processName=${param.processName}"
			access="do=alta">
			<tr>
				<th>Tipo de Comprobante:</th>
				<td><logic:notEmpty name="boletoTipos" scope="page">
						<asf:select name="process.tipoBoleto"
							value="${ProcessForm.process.tipoBoleto}"
							entityName="boletoTipos" scope="page" listCaption="texto"
							listValues="texto" nullValue="true"
							nullText="-- Seleccione el tipo de comprobante --" />
					</logic:notEmpty></td>
			</tr>
		</asf:security>
		<tr>
			<th>Fecha Proceso Desde:</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaProcesoDesdeStr"></asf:calendar></td>
		</tr>
		<tr>
			<th>Fecha Proceso Hasta:</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaProcesoHastaStr"></asf:calendar></td>
		</tr>
	</table>
	<br />

	<input type="button" value="Listar" id="listarId" onclick="listar();" />
	<br />
	<br />

	<logic:notEmpty name="ProcessForm" property="process.comprobantes">
		<div class="grilla">
			<display:table name="ProcessForm" property="process.comprobantes"
				id="credito" class="com.asf.cred.business.BoletoDTO" export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">

				<%-- <display:setProperty name="report.title"
					value="${ProcessForm.process.tipoBoleto} desde ${ProcessForm.process.fechaProcesoDesdeStr} hasta ${ProcessForm.process.fechaProcesoHastaStr}"></display:setProperty> --%>
				<display:column title="Tipo Comprobante" property="tipoComprobante"
					sortable="false" />
				<display:column title="Numero Comprobante"
					property="numeroComprobante" sortable="false" />
				<display:column title="Fecha de Proceso" property="fechaProceso"
					sortable="false" decorator="com.asf.displayDecorators.DateDecorator"/>
				<display:column title="Proyecto" property="numeroAtencion"
					sortable="true" />
				<display:column title="Tomador" property="tomador" sortable="false" />
				<display:column title="Cuil/Cuit" property="cuil" sortable="false" />
				<display:column title="Linea" property="linea" sortable="false" />
				<display:column title="Moneda" property="moneda" sortable="false" />
				<display:column title="Capital" property="capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Compensatorio" property="compensatorio"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Moratorio" property="moratorio"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Punitorio" property="punitorio"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Bonificación" property="bonificacion"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Gtos. Administrativos"
					property="gastosAdministrativos" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Multas" property="multas" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Gtos. a Recuperar" property="gastosRecuperar"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Total" property="total" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</div>
	</logic:notEmpty>
	<logic:empty name="ProcessForm" property="process.comprobantes">
    	No hay Comprobantes
    </logic:empty>

</html:form>

<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');
	function listar() {
		accion.value = 'listar';
		formulario.submit();
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
