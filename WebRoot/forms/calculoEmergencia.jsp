<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">
  C�lculo de Deuda de Cr�ditos de Emergencia Agropecuaria
</div>
<br>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">

    <html:hidden property="process.accion" styleId="accion" />
    <html:hidden property="process.idPersona" value="${ProcessForm.process.idPersona}" styleId="idPersona"/>

    <div style="width:70%" align="left"><html:errors/></div>
    
    <table>
        <tr>
            <th>Nombre:</th>
            <td>
               <asf:text name="ProcessForm" property="process.nombre" type="text" maxlength="50"  />
           </td>
        </tr>
    </table>
    <br/>
    	<input type="button" value="Buscar" onclick="javascript: listar();" />
    <br/>
    <br/>    
    <display:table name="ProcessForm" property="process.personas" pagesize="10" id="reportTable" export="false"
      requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
      
      <display:column title="ID" media="html" sortable="true">
      	<a href="javascript: calcular('${reportTable.id}');">${reportTable.id}</a>
      </display:column>
      
      <display:column property="nomb12" title="Nombre" sortable="true" />
      <display:column property="cuil12Str" title="CUIT" sortable="true" />
      <display:column title="Calcular" media="html">
        <a href="javascript: calcular( ${reportTable.id} );">Calcular Deuda</a>
      </display:column>
    </display:table>
    <br/>
    <br/>
    <table>
        <tr>
            <th>Capital Original:</th>
            <td>
               <asf:text name="ProcessForm" property="process.rd.capitalOriginal" type="text" maxlength="50"  />
           </td>
        </tr>
        <tr>
            <th>Capital Neto:</th>
            <td>
               <asf:text name="ProcessForm" property="process.rd.capitalNeto" type="text" maxlength="50"  />
           </td>
        </tr>
        <tr>
            <th>Interes Moratorio:</th>
            <td>
               <asf:text name="ProcessForm" property="process.rd.moratorio" type="text" maxlength="50"  />
           </td>
        </tr>
        <tr>
            <th>Interes Compensatorio:</th>
            <td>
               <asf:text name="ProcessForm" property="process.rd.compensatorio" type="text" maxlength="50"  />
           </td>
        </tr>
        <tr>
            <th>CER:</th>
            <td>
               <asf:text name="ProcessForm" property="process.rd.cer" type="text" maxlength="50"  />
           </td>
        </tr>
		<tr>
            <th>Deuda Total:</th>
            <td>
               <asf:text name="ProcessForm" property="process.rd.porce2" type="text" maxlength="50"  />
           </td>
        </tr>
    </table>
    
</html:form>

<script type="text/javascript">

function listar(){
	$('accion').value = "listar";
	$('oForm').submit();
}

function calcular(id){
	var accion = $('accion');
	accion.value = "calcular";
	$('idPersona').value = new Number(id);
	$('oForm').submit();
}
</script>
