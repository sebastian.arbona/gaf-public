<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Carga de Auditoria Final</div>

<html:form action="/actions/process.do?do=process&processName=NuevaAuditoriaFinal" enctype="multipart/form-data">
	<html:hidden property="process.action" value="guardar"/>
	<html:hidden property="process.cid"/>
	<html:hidden property="process.idObjetoi" value="${ProcessForm.process.idObjetoi}"/>
	<html:hidden property="process.idauditoriafinal"/>
	
	<div class="grilla">
		<table style="margin: 20px 0">
			<tr>
				<th>Fecha de Proceso</th>
				<td>
					<input type="text" value="${ProcessForm.process.fechaPro}" name="process.fechaPro" readonly/>
				</td>
			</tr>
			<tr>
				<th>Observaciones</th>
				<td>
					<html:textarea property="process.observacion" name="ProcessForm" cols="40" rows="5"/>
				</td>
			</tr>
			<logic:equal name="ProcessForm" property="process.aplicaFon" value=""> 
		  		<tr>		
					<th>Aplica Fondos:</th>
					<td>
						<select id="process.aplicaFon" name="process.aplicaFon" class="aplicaFon" onchange="cambiarValor()">						
						<option value="NO">No</option>
						<option value="SI">Si</option>						
						<option value="PROCESO">En Proceso</option>
						</select>
					</td>
				</tr>
			</logic:equal>
			<logic:notEqual name="ProcessForm" property="process.aplicaFon" value=""> 
		  		<tr>		
					<th>Aplica Fondos:</th>
					<td>
						<select id="process.aplicaFon" name="process.aplicaFon" class="aplicaFon" onchange="cambiarValor()">
						<option value="${ProcessForm.process.aplicaFon}">${ProcessForm.process.aplicaFon}</option>
						<option value="si">Si</option>
						<option value="no">No</option>
						<option value="proceso">En Proceso</option>
						</select>
					</td>
				</tr>
			</logic:notEqual>
			<tr>
				<th>Fecha No aplica Fondos</th>
				<td class="fechanoapli">
					<asf:calendar value="${process.fechanoapli}" property="ProcessForm.process.fechanoapli"/>
				</td>
			</tr>
			<logic:equal name="ProcessForm" property="process.caducidadpla" value=""> 
		  		<tr>		
					<th>Caducidad de Plazos:</th>
					<td class="caducidadpla">
						<select id="process.caducidadpla" name="process.caducidadpla">											
						<option value="no">No</option>
						<option value="si">Si</option>										
						</select>
					</td>
				</tr>
			</logic:equal>
			<logic:notEqual name="ProcessForm" property="process.caducidadpla" value=""> 
		  		<tr>		
					<th>Caducidad de Plazos:</th>
					<td class="caducidadpla">
						<select id="process.caducidadpla"  name="process.caducidadpla">
						<option value="${ProcessForm.process.caducidadpla}">${ProcessForm.process.caducidadpla}</option>					
						<option value="no">No</option>
						<option value="si">Si</option>										
						</select>
					</td>
				</tr>
			</logic:notEqual>
			<tr>
			   <th>Multa Porcentaje:</th>
				<td class="multaporcen">
					<input type="text" id="process.multaporcen" name="process.multaporcen" value="${ProcessForm.process.multaporcen}">
				</td>
			</tr>
			<tr>
			   <th>Importe:</th>
				<td class="importeaudi">
					<input type="text" id="process.importeaudi" name="process.importeaudi" value="${ProcessForm.process.importeaudi}">
				</td>
			</tr>
			<tr>
	            <th>Interes Especial Tasa:</th>
	            <td class="inttasa">
					<asf:select entityName="com.nirven.creditos.hibernate.Indice" listCaption="id,nombre"
		        	listValues="nombre" name="process.inttasa" value="${ProcessForm.process.inttasa}" filter=""
		         	nullValue="true" nullText="&lt;&lt;Seleccione Indice&gt;&gt;" />		         	
	            </td>
	        </tr>
					
			<tr>
			   <th>Base de Calculo:</th>
				<td class="basecal">
					<input type="text" id="process.basecal" name="process.basecal" value="${ProcessForm.process.basecal}">
				</td>
			</tr>		
			<logic:equal name="ProcessForm" property="process.idauditoriafinal" value=""> 
		  		<tr id="doc">
					<th>Documento </th>
					<td colspan="3">
						<html:file property="process.formFile" styleId="formFile"/>
					</td>
				</tr>
			</logic:equal>		
			<tr>
				<th colspan="2">
				    <logic:equal name="ProcessForm" property="process.guarda" value="Si">
						<html:submit value="Guardar"/>
					</logic:equal>
					<input type="button" value="Cancelar" onclick="cancelar();"/>
				</th>
			</tr>

		</table>
	</div>

</html:form>

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<script type="text/javascript">
	function cancelar() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=AuditoriaFinalProcess&process.accion=verNotif&process.idCredito=${ProcessForm.process.idObjetoi}";
	}

    $j = jQuery.noConflict(); 

	function cambiarValor(){
		//var x = document.getElementById("aplicaFon").value;		
		var x=$j(".aplicaFon").val();
		if(x=='si' ||  x=='proceso'){
			//$j(".multaporcen").attr("disabled", "disabled");
			$j('.fechanoapli').hide();		
			$j('.caducidadpla').hide();
			$j('.multaporcen').hide();
			$j('.importeaudi').hide(); 
			$j('.inttasa').hide();
			$j('.basecal').hide();
		}else{
			//$j(".multaporcen").removeAttr("disabled");
			$j('.fechanoapli').show();
			$j('.caducidadpla').show();
			$j('.multaporcen').show(); 			
			$j('.importeaudi').show();
			$j('.inttasa').show();
			$j('.basecal').show(); 	
		}
		
	}
	
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>