<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administración de Unidad</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

<table border="0">
<tr><th>Id:</th><td><asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true" /></td></tr>
<tr>
   <th>Codigo:</th>
   <td><html:text  property='entity.codigo'  maxlength="50"  /></td>
</tr>
<tr>
   <th>Nombre:</th>
   <td><html:text  property='entity.nombre' maxlength="50" /></td>
</tr>

<tr>
   <th>Observaciones:</th>
   <td><html:textarea  property='entity.observaciones' onkeypress="caracteres(this,100,event)"/></td>
</tr>

<tr>
   <th>Unidad Superior:</th>
	<td>
		<asf:select2 name="AbmForm" property="entity.unidadSuperior_id" descriptorLabels="Nombre" entityProperty="id" 
				descriptorProperties="nombre" entityName="Unidad" accessProperty="codigo" />
	</td>
</tr>



<tr>
   <th>Unidad Activa:</th>
   <td><asf:select name="entity.activoStr" value="${AbmForm.entity.activo}" listCaption="Si,No" listValues="true,false" /></td>
</tr>
<tr>
   <th>Nivel:</th>
   <td><html:text  property='entity.nivelStr' maxlength="250" /></td>
</tr>
</table>

<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
    <html:submit>
        <bean:message key="abm.button.save"/>
    </html:submit>
</asf:security>
    <html:cancel>
        <bean:message key="abm.button.cancel"/>
    </html:cancel>
</html:form>
