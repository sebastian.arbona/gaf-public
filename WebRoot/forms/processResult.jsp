<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">Administraci�n de Bancos y Sucursales</div>
<br><br>
<p>

<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>

<div class="grilla">
<display:table name="result" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
    <display:setProperty name="report.title" value="Bancos y Sucursales"></display:setProperty>

    <display:column  property="codi99" title="C�digo"  />
    <display:column  property="deta99" title="C�digo"  />
    
</display:table>
</div>