<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.accion" value="guardar" styleId="accion"/>
    <html:hidden property="process.persona.id" styleId="idPersonaTitular" value="${ProcessForm.process.persona.id}"/>
    <html:hidden property="process.situacionEntidad.id"/>
    <div style="width:70%" align="left"><html:errors/></div>
<br>
<table>
	<tr>
		<th>Entidad:</th>
	    <td>
	    	 <asf:select name="entidad" entityName="EntidadFinanciera" listCaption="codigoEntidad,nombreEntidad"
            			 listValues="idEntidad"
                         value = "${ProcessForm.process.categoria.entidad.id}"
                         nullValue="true" nullText="Seleccione una Entidad"/>
	   </td>
	</tr>
	<tr>
		<th>Categor�a: </th>
		<td>
			<asf:lselect property="process.categoria.id" entityName="CategoriaEntidad"
                         listCaption="CodigoCategoria,nombreCategoria" listValue="idCategoria"
                             value="${ProcessForm.process.categoria.id}"
                             linkFK="entidad.id" linkName="entidad" orderBy="codigoCategoria"
                             nullText="Seleccione una Categor�a" nullValue="true"/>
		</td>
	</tr>
    <tr>
    	<th>Fecha: </th>
        <td><asf:calendar property="ProcessForm.process.situacionEntidad.fechaDesdeStr"/></td>
    </tr>
    <tr>
    	<th>Observaciones: </th>
        <td>
	        <html:textarea property="process.situacionEntidad.observaciones"
		                   onkeypress="caracteres(this,255,event)" cols="60" rows="5"/>
        </td>
    </tr>
    </table>
    <br>
    <html:submit value="Guardar"/>
    <input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
        src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<script language='javascript'>
//CONTROLES
	var accion            = $('accion');
	var formulario        = $('oForm');

	function cancelar()
    {
	    accion.value = "cancelar";

	    formulario.submit();
    }//fin cancelar.-

    Event.observe(window, 'load', function() 
    {
    	this.name = "Situaci�n Entidades";
    	parent.push(this);
    });
</script>