<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">
	Administración de Requisitos de Desembolso
</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}&idObjetoi=${param.idObjetoi}" styleId="AbmForm">
	<html:hidden property="entity.desembolsoId" value="${paramValue[0]}"/>
	<html:hidden name="AbmForm" property="filter" value="true"/>
	<html:hidden property="entity.id"/>
	<input type="hidden" name=idObjetoi" value="${param.idObjetoi}"/>
	
	<table border="0">
		<tr>
			<th colspan="2">
				Seleccione un requisito:
			</th>
		</tr>
		<tr>
			<td colspan="2">
				<asf:select entityName="com.nirven.creditos.hibernate.Requisito"
					listCaption="getId,getNombre" listValues="getId"
					name="entity.requisitoId" value="${AbmForm.entity.requisitoId}" 
					filter="tipo = 'R'" />
				<input type="text" id="descripcionRequisito" name="descripcionRequisito" size="30" readonly="readonly"/>
			</td>
		</tr>
		<tr>
			<th>Fecha Cumplimiento:</th>
			<td><asf:calendar property="AbmForm.entity.fechaCumplidoStr"/></td>
		</tr>
		<tr>
    		<th colspan="2">Observaciones:</th>
    	</tr>
    	<tr>
    		<td colspan="2">
    			<html:textarea property="entity.observaciones" cols="60" rows="3"></html:textarea>
    		</td>
    	</tr>

	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>

<script language="JavaScript" type="text/javascript">
 	$j = jQuery.noConflict();
	$j(document).ready(function() {

		$j("#entity\\.requisitoId").change(function() {
			var idReq = this.value;
			$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarDescripcionRequisito', 
					'idReq='+idReq,
					function(data) {
						$j("#descripcionRequisito").val(data);
					});
		}).change();

	});
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>