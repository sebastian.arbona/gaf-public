<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Ajuste por Diferencia de Cotizaci&oacute;n</div>
<div class="grilla">
<html:form action="/actions/process.do?processName=AjusteCambioProcess&do=process" styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="realizarAjuste"/>
	
	<html:errors/>
	
	<table>
		<tr>
			<th>Fecha de ajuste</th>
			<td>
				<asf:calendar property="ProcessForm.process.fecha" value="${process.fecha}" readOnly="true"/>
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Realizar Ajuste"></html:submit>
			</th>
		</tr>
	</table>
	
	<div style="margin: 10px 0">
		<logic:notEmpty name="ProcessForm" property="process.mensaje">
			${ProcessForm.process.mensaje}
		</logic:notEmpty>
	</div>
	
	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<display:table name="ProcessForm" property="process.creditos" id="credito">
            <display:column title="Número" media="html" sortable="false" >
                <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${credito.id}&process.action=buscar&process.personaId=" target="_blank">${credito.numeroAtencion}</a>
            </display:column>
            <display:column title="Titular" property="persona.nomb12" sortable="false"/>
        	<display:column title="Nro de Crédito" property="numeroCredito" sortable="false"/>
            <display:column title="Línea" property="linea.nombre" sortable="false"/>
            <display:column title="Expediente Nº" property="expediente" sortable="true"/>
            <display:column title="Etapa" property="estadoActual.estado.nombreEstado" sortable="flase"/>
			<display:column title="Comportamiento Pago" property="comportamientoActual" sortable="false"/>            
    	</display:table>
	</logic:notEmpty>
</html:form>
</div>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>