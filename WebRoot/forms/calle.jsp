<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Calle</div>
<br>

<%--><html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}"><--%>
<html:form action="/actions/CalleAction.do?do=save&entityName=${param.entityName}${ empty param.altaRapida ? '' : '&forward=CalleAltaRapida' }">
    <html:hidden property="entity.id" styleId="idcalle" />

    <table border="0">
        <tr><th>C�digo:</th><td><html:text property="entity.idcalle" readonly="true"/></td></tr>
        <tr><th>Calle:</th><td><html:text property="entity.deta08" maxlength="50"/></td></tr>
        <tr><th>Localidad:</th><td><asf:select name="entity.codi19"   entityName="Localidad" listCaption="getIdlocalidad,getNombre" listValues="getIdlocalidad" value="${calleForm.entity.codi19}"  filter="idlocalidad like '%' order by nombre"/></td></tr>
        <tr><th>Observaciones:</th><td><html:textarea property="entity.observaciones" cols="40" rows="4" styleId="Observaciones" onkeypress="caracteres(this,100,event)"/></td></tr>
    </table>

    <asf:security action="/actions/CalleAction.do" access="do=save&entityName=${param.entityName}">
        <html:submit><bean:message key="abm.button.save"/></html:submit>
    </asf:security>

    <html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>

<%--script type="text/javascript">
// GENERAL ROCA debe aparecer siempre como localidad seleccionada en caso de una alta.-
if($('idcalle').value =='0'){
	$('entity.codi19').value = 412;
}
</script--%>
