<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 15px">Saldos de intereses facturados</div>

<div style="width: 70%" align="left">
	<html:errors />
</div>

<html:form action="/actions/process.do?do=process&processName=SaldosDeInteresesFacturados" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion"/>
	
	<table style="margin-bottom:20px">
		<tr>
			<th>Fecha Vencimiento Hasta:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaVencimientoHastaStr" 
				value="${process.fechaVencimientoHastaStr}"/>
			</td>
		</tr>

		<tr>
			<th>Fecha de Calculo:</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaDeCalculoStr" 
				value="${process.fechaDeCalculoStr}"/>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="text-align: left">
				<html:submit onclick="return valida();">Actualizar</html:submit>
			</td>
		</tr>
	</table>

	<div class="grilla">
		<logic:equal name="ProcessForm" property="process.mostrar" value="1">
			 <display:table export="true" name="ProcessForm" property="process.saldosDeInteresesFacturadosBeanList" id="reportTable" pagesize="50"
			 				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column title="Proyecto" property="proyecto"/>
				<display:column title="N� de Cr�dito" property="numeroCredito"/>
				<display:column title="Linea" property="linea"/>
				<display:column title="Expediente" property="expediente"/>
				<display:column title="titular" property="titular"/>
				<display:column title="CUIL" property="cuil"/>
				<display:column title="Total Int. Comp." property="totalInteresesCompensatorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
				<display:column title="Total Notas de Cr�dito" property="totalNotasDeCredito" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
				<display:column title="Total Notas de D�bito" property="totalNotasDeDebito" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
				<display:column title="Pagos Int. Comp." property="pagosIntComp" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
				<display:column title="Estado" property="estado"/>
				<display:column title="Comportamiento" property="comportamiento"/>
			</display:table>
		</logic:equal>
	</div>

</html:form>

<script type="text/javascript">
var accion					= $('accion');

var fechaVencimientoHasta	= $("process.fechaVencimientoHastaStr");
var fechaDeCalculo 			= $("process.fechaDeCalculoStr");

function cargarTabla() {
	accion.value = 'cargarTabla';
	return true;
}

function valida() {
	retorno = true;

	if(fechaVencimientoHasta.value.length == 0 || fechaDeCalculo.value.length == 0) {
		alert("Debe cargar todas las fechas para poder continuar");
		retorno=false;
	}

	if(retorno)
		cargarTabla();

	return retorno;
}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>