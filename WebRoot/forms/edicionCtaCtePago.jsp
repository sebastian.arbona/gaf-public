<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Edicion Cta Cte</div>
<html:form
	action="/actions/process.do?do=process&processName=EdicionCtaCte"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="guardarPago"/>
	<table>
		<tr>
			<th>Recibo actual</th>
			<td>
				${ProcessForm.process.pago.recibo.numeroBoleto} - ${ProcessForm.process.pago.recibo.importe}
			</td>
			<th>Recibo nuevo</th>
			<td>
				<asf:select entityName="com.nirven.creditos.hibernate.Boleto" 
        				listCaption="getNumeroBoleto,getImporte" listValues="getId"
          				name="process.boletoId" 
          				value="${ProcessForm.process.pago.recibo.id}"
          				filter=" objetoi.id = ${ProcessForm.process.pago.recibo.objetoi.id} and tipo = 'Recibo' "/>
			</td>
		</tr>
		<tr>
			<th colspan="4">
	  			<html:submit value="Guardar"></html:submit>
	  		</th>
		</tr>
	</table>
</html:form>