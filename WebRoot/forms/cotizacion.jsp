<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Cotización</div>
<br>
<br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true&paramValue[0]=${param.paramValue[0]}" styleId="cd" >

	<table border="0">
	
	 <html:hidden property="entity.moneda_id"/>
	
		<tr><th>Id:</th><td><asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true" /></td>	</tr>
		
		<tr>
			<th>Fecha desde:</th>
			
			<td>
				<asf:calendar property="AbmForm.entity.fechaDesdeStr"/>				
			</td>
		</tr>
		<tr>
			<th>Fecha hasta:</th>
			
			<td>
			<asf:calendar property="AbmForm.entity.fechaHastaStr"/>	
				
			</td>
		</tr>
		<tr>
			<th>Compra:</th>
			
			<td>
				
				<asf:text name="AbmForm" property="entity.compra" type="decimal" maxlength="6"/>
			</td>
		</tr>
		<tr>
			<th>Venta:</th>
			
			<td>
				<asf:text name="AbmForm" property="entity.venta" type="decimal" maxlength="6"/>
			</td>
		</tr>
		
		
		
	</table>
	<asf:security action="/actions/abmAction.do"	access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<!-- Raul Varela -->