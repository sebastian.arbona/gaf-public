<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">Administraci&oacute;n de Configuraciones de
	Categor&iacute;as de Solicitante</div>
<br>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>Id:</th>
			<td><asf:text name="AbmForm" property="entity.id" type="long"
					maxlength="8" readonly="true" /></td>
		</tr>
		<tr>
			<th>L&iacute;nea de Cr&eacute;dito:</th>
			<td><asf:select name="entity.lineaStr"
					entityName="com.nirven.creditos.hibernate.Linea"
					value="${AbmForm.entity.lineaStr}" listCaption="getId,getNombre"
					listValues="getId" filter="activa = true order by id"
					nullValue="true" nullText="-- Seleccione una l&iacute;nea --" /></td>
		</tr>
		<tr>
			<th>Categor&iacute;a del Solicitante:</th>
			<td><asf:select name="entity.categoriaSolicitante"
					attribs="class='categoriaSolicitante'"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.categoriaSolicitante}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'Categoria.Solicitante' order by codigo"
					nullValue="true"
					nullText="-- Seleccione una Categoria de Solicitante --" /></td>
		</tr>
		<tr>
			<th>Fecha Desde:</th>
			<td><asf:calendar property="AbmForm.entity.fechaDesdeStr" maxlength="10"></asf:calendar></td>
		</tr>
		<tr>
			<th>Fecha Hasta:</th>
			<td><asf:calendar property="AbmForm.entity.fechaHastaStr" maxlength="10"></asf:calendar></td>
		</tr>
		<tr>
			<th>Tipo Tasa:</th>
			<td><asf:select name="entity.tipoTasa"
					attribs="class='tipoTasa'"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoTasa}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'TipoTasa' order by codigo" nullValue="true"
					nullText="-- Seleccione un Tipo de Tasa --" /></td>
		</tr>
		<tr>
			<th>Tasa Inter&eacute;s</th>
			<td><asf:selectpop name="entity.indiceStr" title="Tasa"
					columns="Codigo, Nombre" captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.Indice"
					value="${AbmForm.entity.indiceStr}" filter="tipo = 1" /></td>
		</tr>
		<tr>
			<th>Mas</th>
			<td><html:text property="entity.valorMasStr"
					value="${AbmForm.entity.valorMasStr}" size="15" styleId="mas" /></td>
		</tr>
		<tr>
			<th>Por</th>
			<td><asf:text id="por" name="AbmForm"
					property="entity.valorPorStr" type="decimal" maxlength="15" /></td>
		</tr>
		<tr>
			<th>Tope Tasa</th>
			<td><html:text property="entity.topeTasaStr" size="15"
					styleId="topeTasaStr" /></td>
		</tr>
		<tr>
			<th>D&iacute;as antes</th>
			<td><html:text value="${AbmForm.entity.diasAntes}"
					property="entity.diasAntes" size="15" /></td>
		</tr>
		<tr>
			<th>Tipo C&aacute;lculo:</th>
			<td><asf:select name="entity.tipoCalculo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="${AbmForm.entity.tipoCalculo}"
					filter="categoria='Interes.TipoCalculo' order by codigo desc"
					nullValue="true"
					nullText="-- Seleccione un Tipo de C&aacute;lculo --" /></td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
	
</script>

<script language="JavaScript" type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function($) {

		jQuery('.tipo').change(function() {

			deshabilitar_linea();
		});

		function deshabilitar_linea() {

			if (jQuery('.tipo').val() == 'L')
				jQuery('.linea').removeAttr("disabled");
			else
				jQuery('.linea').attr("disabled", true);
		}

		deshabilitar_linea();

	});
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
