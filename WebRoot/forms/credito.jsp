<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<div class="yui-skin-sam">
	<div class="title">Financiamiento</div>
	<html:form
		action="/actions/process.do?do=process&processName=CreditoProcess"
		styleId="ProcessForm">
		<html:hidden property="process.personaId" styleId="personaId"
			value="${ProcessForm.process.personaId}" />
		<table>
			<tr>
				<th>Proyecto</th>
				<logic:equal value="true" property="process.hayNroCredito"
					name="ProcessForm">
					<th>Nro de Cr&eacute;dito</th>
				</logic:equal>
				<th>Titular</th>
				<th>CUIL/CUIT</th>
				<th>Expediente</th>
				<th>L&iacute;nea</th>
				<th>Objeto</th>
				<th>Etapa</th>
				<th>Comp. Pago</th>
			</tr>
			<tr>
				<td>${ProcessForm.process.objetoi.numeroAtencion}</td>
				<logic:equal value="true" property="process.hayNroCredito"
					name="ProcessForm">
					<td>${ProcessForm.process.objetoi.numeroCredito}</td>
				</logic:equal>
				<td><logic:notEmpty name="ProcessForm"
						property="process.objetoi.persona.nomb12">
						${ProcessForm.process.objetoi.persona.nomb12}
					</logic:notEmpty> <logic:notEmpty name="ProcessForm"
						property="process.objetoi.persona.razonSocial">
						<logic:empty name="ProcessForm"
							property="process.objetoi.persona.nomb12">
							${ProcessForm.process.objetoi.persona.razonSocial}
						</logic:empty>
					</logic:notEmpty></td>
				<td>${ProcessForm.process.objetoi.persona.cuil12}</td>
				<td>${ProcessForm.process.objetoi.expediente}</td>
				<td>${ProcessForm.process.objetoi.linea.nombre}</td>
				<td>${ProcessForm.process.objetoi.objeto}</td>
				<td
					style="padding: 0 10px 0 6px; vertical-align: middle; border-left: 28px solid ${ProcessForm.process.estadoActual.color}">
					${ProcessForm.process.estadoActual.nombreEstado}</td>
				<td
					style="padding: 0 10px 0 6px; vertical-align: middle; border-left: 28px solid ${ProcessForm.process.objetoi.comportamientoActualColor}">
					${ProcessForm.process.objetoi.comportamientoActual}</td>
			</tr>
		</table>
		<logic:equal value="true" property="process.valorCartera"
			name="ProcessForm">
			<br>
			<div align="center" style="color: blue">
				<h1>
					<b>-- Valores en cartera existentes --</b>
				</h1>
			</div>
		</logic:equal>
		<div align="right" style="color: #3390ff; padding-right: 60px;">
			<h3>
				<b>-- ${ProcessForm.process.auditoriaFinalPos} --</b>
			</h3>
		</div>
		<br>
		<div id="pestanias" class="yui-navset" style="width: 100%;">
			<ul class="yui-nav">
				<!-- Cotomadores -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCotomadores">
					<li><a href="#cotomadoresObjetoi" onclick="irACotomadores();"><em>Co-Tomadores</em></a>
					</li>
				</asf:security>
				<!-- Domicilio -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniadomicilioObjetoi">
					<li><a href="#domicilioObjetoi" onclick="irADomicilio();"><em>Domicilios</em></a>
					</li>
				</asf:security>
				<!-- Datos Generales -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniadatosgenerales">
					<li class="selected"><a href="#datosgenerales"
						onclick="irADatosGenerales();"><em>Datos Generales</em></a></li>
				</asf:security>
				<!-- Vi�edos -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniavinedos">
					<logic:equal value="true" property="process.lineaCyA"
						name="ProcessForm">
						<li id="tab_vinedos"><a href="#vinedos"
							onclick="irAVinedos();"><em>Vinedos</em> </a></li>
					</logic:equal>
				</asf:security>
				<!-- Analisis CyA -->
				<asf:security action="/actions/process.do"
					access="processName=pestanialineaCyA">
					<logic:equal value="true" property="process.lineaCyA"
						name="ProcessForm">
						<li id="tab_analisis"><a href="#analisis"><em>An&aacute;lisis
									Cosecha</em> </a></li>
					</logic:equal>
				</asf:security>
				<!-- Analisis -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAnalisis">
					<li id="tab_analisis2"><a href="#analisis2"><em>An&aacute;lisis</em>
					</a></li>
				</asf:security>
				<!-- Agenda -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAgenda">
					<li id="tab_agenda"><a href="#agenda"><em>Agenda</em> </a></li>
				</asf:security>
				<!-- Garantias -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaGarantias">
					<li id="tab_garantias"><a href="#garantias"><em>Garant&iacute;as</em>
					</a></li>
				</asf:security>
				<!-- Datos Financieros -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaDatFinancieros">
					<li><a href="#datosfinancieros"
						onclick="irADatosFinancieros();"><em>Datos Financieros</em></a></li>
				</asf:security>
				<!-- Desembolsos -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaDesembolsos">
					<li><logic:equal value="false" name="ProcessForm"
							property="process.lineaAcuerdo">
							<a href="#desembolsos" onclick="irADesembolsos();"><em>Desembolsos</em></a>
						</logic:equal> <logic:equal value="true" name="ProcessForm"
							property="process.lineaAcuerdo">
							<a href="#desembolsos" onclick="irADesembolsos();"><em>Deuda
									consolidada</em></a>
						</logic:equal></li>
				</asf:security>
				<!-- Cuotas -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCuotas">
					<li><a href="#cuotas" onclick="irACuotas();"><em>Cuotas</em></a>
					</li>
				</asf:security>
				<!-- Composicion -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaComposicion">
					<logic:equal name="ProcessForm" value="true"
						property="process.pasoGestionJudicial">
						<asf:security action="/actions/process.do"
							access="processName=rulePasoGestionJudicial">
							<li><a href="#cuentacorriente"
								onclick="irACuentaCorriente();"><em>Composici&oacute;n
										de Cr&eacute;dito</em></a></li>
						</asf:security>
					</logic:equal>
					<logic:equal name="ProcessForm" value="false"
						property="process.pasoGestionJudicial">
						<li><a href="#cuentacorriente"
							onclick="irACuentaCorriente();"><em>Composici&oacute;n
									de Cr&eacute;dito</em></a></li>
					</logic:equal>
				</asf:security>
				<!-- Cuenta Corriente -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCuentaCorriente">
					<li><a href="#cuentacorrientecontable"
						onclick="irACuentaCorrienteContable();"><em>Cuenta
								Corriente</em></a></li>
				</asf:security>
				<!-- Cuenta Corriente Ampliada -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCtaCteAmpliada">
					<li><a href="#cuentacorrienteampliada"
						onclick="irACuentaCorrienteAmpliada();"><em>Cta Cte
								Ampliada</em></a></li>
				</asf:security>
				<!-- Estados -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaHistorial">
					<li><a href="#estados" onclick="irAEstados();"><em>Historial</em></a>
					</li>
				</asf:security>
				<!-- Comportamientos -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaComportamientosdePago">
					<li><a href="#comportamientos" onclick="irAComportamientos();"><em>Comportamientos
								de Pago</em></a></li>
				</asf:security>
				<!-- Resumenes de Pago -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaResumenesdeCuenta">
					<li><a href="#boletosDePago" onclick="irABoletosDePago();"><em>Res&uacute;menes
								de Cuenta</em></a></li>
				</asf:security>
				<!-- Recibos de Pago -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaRecibos">
					<li><a href="#pagos" onclick="irAPagos();"><em>Recibos</em></a>
					</li>
				</asf:security>
				<!-- Notificaciones -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaHistorialdeNotificaciones">
					<li><a href="#noti" onclick="irANoti();"><em>Historial
								de Notificaciones</em></a></li>
				</asf:security>
				<!-- Documentacion -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaDocumentacion">
					<li><a href="#documentacion" onclick="irADocumentacion();"><em>Documentaci&oacute;n</em></a>
					</li>
				</asf:security>
				<!-- Linea Emergencia -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaEmergencias">
					<logic:equal value="true" property="process.lineaE"
						name="ProcessForm">
						<li><a href="#emer" onclick="irAEmer();"><em>Emergencias</em></a>
						</li>
					</logic:equal>
				</asf:security>
				<!-- Cesion Titularidad -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCesiondeTitularidad">
					<li><a href="#cesion" onclick="irACesion();"><em>Cesi&oacute;n
								de Titularidad</em></a></li>
				</asf:security>
				<!-- Instancias Judiciales -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaInstanciasJudiciales">
					<logic:equal name="ProcessForm" value="true"
						property="process.pasoGestionJudicial">
						<li><a href="#instanciasJudiciales"
							onclick="irAInstanciasJudiciales();"><em>Instancias
									Judiciales</em></a></li>
					</logic:equal>
				</asf:security>
				<!-- Original de Acuerdo -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaOriginal">
					<logic:equal value="true" name="ProcessForm"
						property="process.lineaAcuerdo">
						<li><a href="#acuerdoPago" onclick="irAAcuerdoPago();"><em>Original</em></a>
						</li>
					</logic:equal>
				</asf:security>
				<!-- Acuerdo de Original -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAcuerdodePago">
					<logic:notEmpty name="ProcessForm"
						property="process.objetoi.acuerdoPago">
						<li><a href="#acuerdoPago2" onclick="irAAcuerdoPago2();"><em>Acuerdo
									de Pago</em></a></li>
					</logic:notEmpty>
				</asf:security>
				<!-- Gastos Recuperar -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaGastosaRecuperar">
					<logic:equal value="true" name="ProcessForm"
						property="process.hayGastosARecuperar">
						<li><a href="#gastosARecuperar"
							onclick="irAGastosARecuperar();"><em>Gastos a Recuperar</em></a></li>
					</logic:equal>
				</asf:security>
				<!-- Novedades -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaNovedades">
					<li><a href="#novedades" onclick="irANovedades();"><em>Novedades</em></a>
					</li>
				</asf:security>
				<!-- Auditoria Final -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAuditoriaFinal">
					<li><a href="#auditoriafinal" onclick="irAAuditoriaFinal();"><em>Auditor&iacute;a
								Final</em></a></li>
				</asf:security>
				<!-- Prorroga -->
				<asf:security action="/actions/process.do"
					access="processName=pestaniaProrroga">
					<li><a href="#prorroga" onclick="irAProrroga();"><em>Pr&oacute;rroga</em></a>
					</li>
				</asf:security>
			</ul>
			<div class="yui-content">
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCotomadores">
					<div id="cotomadores">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameCotomadores"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniadomicilioObjetoi">
					<div id="domicilioObjetoi">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameDomicilioObjetoi"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniadatosgenerales">
					<div id="datosgenerales">
						<iframe
							src="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoDatosGenerales&process.idObjetoi=${ProcessForm.process.objetoi.id}"
							width="100%" height="400" scrolling="auto" style="border: none"
							id="frameDatosGenerales" allowtransparency="allowtransparency">
						</iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniavinedos">
					<logic:equal value="true" property="process.lineaCyA"
						name="ProcessForm">
						<div id="vinedos">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameVinedos"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestanialineaCyA">
					<logic:equal value="true" property="process.lineaCyA"
						name="ProcessForm">
						<div id="analisisCrediticio">
							<iframe
								src='${pageContext.request.contextPath}/actions/process.do?processName=AnalisisCrediticioProcess&do=process&process.id=${ProcessForm.process.idObjetoi}'
								width="100%" height="500" scrolling="auto" style="border: none"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAnalisis">
					<div id="analisisCrediticio2">
						<iframe
							src='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoAnalisisCrediticio&process.idSolicitud=${ProcessForm.process.idObjetoi}'
							width="100%" height="500" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAgenda">
					<div id="agenda">
						<iframe
							src='${pageContext.request.contextPath}/actions/process.do?do=process&processName=AgendaProcess&process.idObjetoi=${ProcessForm.process.idObjetoi}'
							width="100%" height="500" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaGarantias">
					<div id="garantias">
						<iframe
							src='${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.idSolicitud=${ProcessForm.process.idObjetoi}'
							width="100%" height="400" scrolling="auto" style="border: none"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaDatFinancieros">
					<div id="datosfinancieros">
						<iframe width="100%" height="450" scrolling="auto"
							style="border: none;" id="frameDatosFinancieros"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaDesembolsos">
					<div id="desembolsos">
						<iframe width="100%" height="600" scrolling="auto"
							style="border: none;" id="frameDesembolsos"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCuotas">
					<div id="cuotas">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameCuotas"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaComposicion">
					<logic:equal name="ProcessForm" value="true"
						property="process.pasoGestionJudicial">
						<asf:security action="/actions/process.do"
							access="processName=rulePasoGestionJudicial">
							<div id="cuentacorriente">
								<iframe width="100%" height="400" scrolling="auto"
									style="border: none;" id="frameCuentaCorriente"
									allowtransparency="allowtransparency"> </iframe>
							</div>
						</asf:security>
					</logic:equal>
					<logic:equal name="ProcessForm" value="false"
						property="process.pasoGestionJudicial">
						<div id="cuentacorriente">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameCuentaCorriente"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCuentaCorriente">
					<div id="cuentacorrientecontable">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameCuentaCorrienteContable"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCtaCteAmpliada">
					<div id="cuentacorrienteampliada">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameCuentaCorrienteAmpliada"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaHistorial">
					<div id="estados">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameEstados"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaComportamientosdePago">
					<div id="comportamientos">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameComportamientos"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaResumenesdeCuenta">
					<div id="boletosDePago">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameBoletosDePago"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaRecibos">
					<div id="pagos">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="framePagos"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaHistorialdeNotificaciones">
					<div id="noti">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameNoti"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaDocumentacion">
					<div id="documentacion">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameDocumentacion"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaEmergencias">
					<logic:equal value="true" property="process.lineaE"
						name="ProcessForm">
						<div id="emer">
							<iframe
								src='${pageContext.request.contextPath}/actions/process.do?processName=EmergenciaAgropecuariaProcess&do=process&process.idPersona=${ProcessForm.process.personaId}&process.action=listarEmergencias'
								width="100%" height="400" scrolling="auto" style="border: none;"
								id="frameEmergencia" allowtransparency="allowtransparency">
							</iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaCesiondeTitularidad">
					<div id="cesion">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameCesion"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaInstanciasJudiciales">
					<logic:equal name="ProcessForm" value="true"
						property="process.pasoGestionJudicial">
						<div id="instanciasJudiciales">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameInstanciasJudiciales"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaOriginal">
					<logic:equal value="true" name="ProcessForm"
						property="process.lineaAcuerdo">
						<div id="acuerdoPago">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameAcuerdoPago"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAcuerdodePago">
					<logic:notEmpty name="ProcessForm"
						property="process.objetoi.acuerdoPago">
						<div id="acuerdoPago2">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameAcuerdoPago2"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:notEmpty>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaGastosaRecuperar">
					<logic:equal name="ProcessForm"
						property="process.hayGastosARecuperar" value="true">
						<div id="gastosARecuperar">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameGastosARecuperar"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</logic:equal>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaNovedades">
					<div id="novedades">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameNovedades"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaAuditoriaFinal">
					<div id="auditoriafinal">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameAuditoriaFinal"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
				<asf:security action="/actions/process.do"
					access="processName=pestaniaProrroga">
					<div id="prorroga">
						<iframe width="100%" height="400" scrolling="auto"
							style="border: none;" id="frameProrroga"
							allowtransparency="allowtransparency"> </iframe>
					</div>
				</asf:security>
			</div>
		</div>
	</html:form>
	<script language="javascript" type="text/javascript">
		//pesta�as.-
		var tabView = new YAHOO.widget.TabView('pestanias');
		var frameDomicilioObjetoi = $('frameDomicilioObjetoi');
		var frameCuotas = $('frameCuotas');
		var frameCotomadores = $('frameCotomadores');
		var frameCuentaCorriente = $('frameCuentaCorriente');
		var frameCuentaCorrienteContable = $('frameCuentaCorrienteContable');
		var frameCuentaCorrienteAmpliada = $('frameCuentaCorrienteAmpliada');
		var frameDatosFinancieros = $('frameDatosFinancieros');
		var frameDatosGenerales = $('frameDatosGenerales');
		var frameDesembolsos = $('frameDesembolsos');
		var frameEstados = $('frameEstados');
		var frameBoletosDePago = $('frameBoletosDePago');
		var framePagos = $('framePagos');
		var frameNoti = $('frameNoti');
		var frameCesion = $('frameCesion');
		var frameInstanciasJudiciales = $('frameInstanciasJudiciales');
		var frameComportamientos = $('frameComportamientos');
		var frameEmergencia = $('frameEmergencia');
		var frameDocumentacion = $('frameDocumentacion');
		var frameVinedos = $('frameVinedos');
		var frameAcuerdoPago = $('frameAcuerdoPago');
		var frameAcuerdoPago2 = $('frameAcuerdoPago2');
		var frameGastosARecuperar = $('frameGastosARecuperar');
		var frameNovedades = $('frameNovedades');
		var frameProrroga = $('frameProrroga');
		var frameAuditoriaFinal = $('frameAuditoriaFinal');

		function irADomicilio() {
			var src = '';
			if (frameDomicilioObjetoi.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=DomicilioObjetoi';
				src += '&process.accion=listar';
				src += '&process.personaTitular.id=${ProcessForm.process.objetoi.persona.idpersona}';
				src += '&process.objetoi.id=${ProcessForm.process.objetoi.id}';
				src += '&process.objetoi.numeroCredito=${ProcessForm.process.objetoi.numeroCredito}';
				frameDomicilioObjetoi.src = src;
			}
		}

		function irACuotas() {
			var src = '';
			if (frameCuotas.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoCuotas';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameCuotas.src = src;
			}
		}

		function irACotomadores() {
			var src = '';
			if (frameCotomadores.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoCotomadores&process.action=listar';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameCotomadores.src = src;
			}
		}

		function irACuentaCorriente() {
			var src = '';
			if (frameCuentaCorriente.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoCuentaCorriente';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				src += '&process.personaId=${ProcessForm.process.personaId}';
				frameCuentaCorriente.src = src;
			}
		}

		function irACuentaCorrienteContable() {
			var src = '';
			if (frameCuentaCorrienteContable.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoCuentaCorrienteAmpliada';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				src += '&process.personaId=${ProcessForm.process.personaId}';
				src += '&process.reducida=true';
				frameCuentaCorrienteContable.src = src;
			}
		}

		function irACuentaCorrienteAmpliada() {
			var src = '';
			if (frameCuentaCorrienteAmpliada.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoCuentaCorrienteAmpliada';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				src += '&process.personaId=${ProcessForm.process.personaId}';
				frameCuentaCorrienteAmpliada.src = src;
			}
		}

		function irADatosFinancieros() {
			var src = '';
			if (frameDatosFinancieros.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoDatosFinancieros';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameDatosFinancieros.src = src;
			}
		}

		function irADatosGenerales() {
			var src = '';
			if (frameDatosGenerales.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoDatosGenerales';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameDatosGenerales.src = src;
			}
		}

		function irADesembolsos() {
			var src = '';
			if (frameDesembolsos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoDesembolsos';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameDesembolsos.src = src;
			}
		}

		function irAEstados() {
			var src = '';
			if (frameEstados.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CreditoEstados';
				src += '&process.objetoi.id=${ProcessForm.process.idObjetoi}';
				frameEstados.src = src;
			}
		}

		function irAComportamientos() {
			var src = '';
			if (frameComportamientos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=ComportamientoPago';
				src += '&process.objetoi.id=${ProcessForm.process.idObjetoi}';
				frameComportamientos.src = src;
			}
		}

		function irABoletosDePago() {
			var src = '';
			if (frameBoletosDePago.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=BoletosDePago';
				src += '&process.objetoi.id=${ProcessForm.process.idObjetoi}';
				frameBoletosDePago.src = src;
			}
		}
		function irAPagos() {
			var src = '';
			if (framePagos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=PagosProcess';
				src += '&process.objetoi.id=${ProcessForm.process.idObjetoi}';
				framePagos.src = src;
			}
		}
		function irANoti() {
			var src = '';
			if (frameNoti.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=NotificacionProcess';
				src += '&process.idCredito=${ProcessForm.process.idObjetoi}&process.accion=verNotif&process.pestania=true';
				frameNoti.src = src;
			}
		}

		function irACesion() {
			var src = '';
			if (frameCesion.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do'
						+ '?do=process'
						+ '&processName=HistorialTitularidadProcess'
						+ '&process.action=list'
						+ '&process.idSolicitud=${ProcessForm.process.idObjetoi}';
				frameCesion.src = src;
			}
		}
		function irAInstanciasJudiciales() {
			var src = '';
			if (frameInstanciasJudiciales.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=ResolucionJudicialProcess';
				src += '&process.accion=listarResolucionFiltroInicio'
				src += '&process.idproyectoPop=${ProcessForm.process.idObjetoi}';
				frameInstanciasJudiciales.src = src;
			}
		}
		function irAEmer() {
			var src = '';
			if (frameEmergencia.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=EmergenciaAgropecuariaProcess';
				src += '&process.idPersona=${ProcessForm.process.personaId}&process.action=listarEmergencias';
				frameEmergencia.src = src;
			}
		}
		function irADocumentacion() {
			var src = '';
			if (frameDocumentacion.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?do=process';
				src += '&processName=ArchivoCreditoProcess';
				src += '&process.accion=listar';
				src += '&process.objetoiArchivo.credito.id=${ProcessForm.process.idObjetoi}';
				frameDocumentacion.src = src;
			}
		}
		function irAVinedos() {
			var src = '';
			if (frameVinedos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?processName=VinedoProcess&do=process';
				src += '&process.idSolicitud=${ProcessForm.process.idObjetoi}';
				frameVinedos.src = src;
			}
		}

		function irAAcuerdoPago() {
			var src = '';
			if (frameAcuerdoPago.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?do=process';
				src += '&processName=ListarAcuerdoPago';
				src += '&process.accion=listar';
				src += '&process.acuerdoPago.id=${ProcessForm.process.idObjetoi}';
				src += '&process.persona.idpersona=${ProcessForm.process.personaId}';
				frameAcuerdoPago.src = src;
			}
		}
		function irAAcuerdoPago2() {
			var src = '';
			if (frameAcuerdoPago2.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?do=process';
				src += '&processName=ListarAcuerdoPago';
				src += '&process.accion=listar2';
				src += '&process.acuerdoPago.id=${ProcessForm.process.idObjetoi}';
				src += '&process.persona.idpersona=${ProcessForm.process.personaId}';
				frameAcuerdoPago2.src = src;
			}
		}
		function irAGastosARecuperar() {
			var src = '';
			if (frameGastosARecuperar.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?do=process';
				src += '&processName=GastosRecuperarProcess';
				src += '&process.accion=listarGastosRecuperarPestania';
				src += '&process.idCredito=${ProcessForm.process.idObjetoi}';
				frameGastosARecuperar.src = src;
			}
		}
		function irANovedades() {
			var src = '';
			if (frameNovedades.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?do=process';
				src += '&processName=NovedadesProcess';
				src += '&process.accion=listar';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameNovedades.src = src;
			}
		}
		function irAProrroga() {
			var src = '';
			if (frameProrroga.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?do=process';
				src += '&processName=ProrrogaProcess';
				src += '&process.accion=listar';
				src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
				frameProrroga.src = src;
			}
		}

		function irAAuditoriaFinal() {
			var src = '';
			if (frameAuditoriaFinal.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=AuditoriaFinalProcess';
				src += '&process.idCredito=${ProcessForm.process.idObjetoi}&process.accion=verNotif';
				frameAuditoriaFinal.src = src;
			}
		}
	</script>
</div>