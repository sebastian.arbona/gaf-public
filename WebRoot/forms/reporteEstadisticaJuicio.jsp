<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div>
	<div class="title">Reporte Estadistico por Juicios</div>
	<br>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
		<html:hidden property="process.action" value="consultar" styleId="action"/>
		<table border="0" class="tabla">
			<tr>
				<th>Fecha Desde</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
				</td>
			</tr>
			
			<tr>
				<tr>
				<th>Fecha Hasta</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
				</td>
			</tr>
			
			<tr>
				<th>Abogados:</th>
				<td><asf:select name="process.codAbogado"
						entityName="com.civitas.hibernate.persona.Persona"
						listCaption="getNomb12" listValues="getId"
						filter="id in (select e.persona.id from com.nirven.creditos.hibernate.EspecialidadPersona e where e.especialidad.id = 6)"
						value="${ProcessForm.process.codAbogado}" nullValue="true"
						nullText="Todos los Abogados"/></td>
			</tr>
			           
		</table>
		<html:submit value="Consultar"></html:submit>
	</html:form>

	<div id="container-column" style="min-width: 400px; height: 400px; margin: 2em 0 auto;"></div>
</div>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/highcharts.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/highcharts-exporting.js"></script>

<script type="text/javascript">
	var $j = jQuery.noConflict();
	var chart_column = null;
	
	var datosEstadisticos = '${empty ProcessForm.process.estadistica ? null : ProcessForm.process.estadistica}';
		
	$j(document).ready( function() {
	
		chart_column = Highcharts.chart('container-column', {		    
			chart: {
		        type: 'column'
		    },
			title: {
		        text: ''
		    },
		    subtitle: {
		        text: ''
		    },
		    credits: {
   				enabled: false
   			},
		    xAxis: {		        
		        categories: [
		        	'D�as de Atraso >= 7','D�as de Atraso >= 15','D�as de Atraso >= 20'		            
		        ],		        
		        crosshair: true,
		        type: 'category',
		        tickPixelInterval: 200,
		        title: {
		            text: 'Estados'
		        }
		    },
		    yAxis: [{
		        min: 0,
		        type: 'category',
		        title: {
		            text: 'Cantidad de Juicios'
		        },
		    }],
		    tooltip: {
		        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		            '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
		        footerFormat: '</table>',
		        shared: true,
		        useHTML: true
		    },
		    plotOptions: {
		        column: {
		            pointPadding: 0.2,
		            borderWidth: 0
		        }
		    },

		    series: [
		    	{
		    		type: 'column',
		            name: 'Primera Instancia',
		            data: []
		        },		        
		        {
		    		type: 'column',
		            name: 'Segunda Instancia',
		            data: []
		        },
		        {
		    		type: 'column',
		            name: 'SCJM',
		            data: []
		        }
		  ]

		});
		
		actualizarGrafico();
	});
	
	function actualizarGrafico() {
		if( this.datosEstadisticos != "undefined" && this.datosEstadisticos != "" ){
			var datos = JSON.parse(datosEstadisticos);
			datos.forEach(element => {
				try {
					if( element.estado == "Primera Instancia" ) {
						chart_column.series[0].setData(JSON.parse(element.cantidad));
					}
					else if( element.estado == "Primera Segunda" ) {
						chart_column.series[1].setData(JSON.parse(element.cantidad));
					}
					else if( element.estado == "SCJM" ) {
						chart_column.series[2].setData(JSON.parse(element.cantidad));
					}
				} catch (e) {
					console.log("Problemas para refrescar estadistica");
				}
				
	        });
		}
		
	}
</script>