<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="font-size: 18px">Ingrese los datos de Finalizaci�n de este Cr�dito</div>

<br/>
<html:errors/>
<br/>
<html:form action="actions/process.do?processName=${param.processName}&do=process">

    <html:hidden name="ProcessForm" property="process.action" styleId="action"/>
    <html:hidden name="ProcessForm" property="process.idObjetoi"/>
    
    <logic:notEmpty name="ProcessForm" property="process.mensajeFinalizado">
    
    	<b>${ProcessForm.process.mensajeFinalizado}</b>
    	
    	<div style="clear :both"/>
    	
    	<html:submit onclick="volver()">Volver</html:submit>
    
    </logic:notEmpty>

	<logic:empty name="ProcessForm" property="process.mensajeFinalizado">
	<div style="font-size: 16px">
		Expediente: ${ProcessForm.process.objetoi.expediente} - Proyecto: ${ProcessForm.process.objetoi.numeroAtencion} - Titular: ${ProcessForm.process.objetoi.persona.nomb12}
	</div>
	<table>

	
		<tr>
			
			<th>Fecha Firma de Resoluci�n</th>
			
			<td>
			
				<asf:calendar property="ProcessForm.process.fechaFirmaResolucionStr" value="${ProcessForm.process.fechaFirmaResolucionStr}" readOnly="false"/>
			
			</td>
		
		</tr>
		
		<tr>
			
			<th>N�mero de Resoluci�n Aprobatoria</th>
			
			<td>
				
				<html:text name="ProcessForm" property="process.numeroResolucion"/>
			
			</td>
			
		</tr>
	
		<tr>
			
			<th>Fecha de Firma del Mutuo de Finalizaci�n</th>
			
			<td>
			
				<asf:calendar property="ProcessForm.process.fechaFirmaFinalizacionStr" value="${ProcessForm.process.fechaFirmaFinalizacionStr}" readOnly="false"/>
			
			</td>
		
		</tr>
<%--		<tr>--%>
<%--			<th>Se han liberado todas las garant�as</th>--%>
<%--			<td>--%>
<%--				<html:select property="process.garantiasLiberadas" name="ProcessForm" value="${ProcessForm.process.garantiasLiberadas}">--%>
<%--    				<html:option value="false">NO</html:option>--%>
<%--    				<html:option value="true">SI</html:option>--%>
<%--    			</html:select>--%>
<%--			</td>--%>
<%--		</tr>--%>
		
	</table>
	
	<asf:security action="/actions/process.do"
		access="do=process&processName=${param.ProcessName}">
		<html:submit onclick="guardar()">
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:submit onclick="volver()">
		<bean:message key="abm.button.cancel" />
	</html:submit>
	
	</logic:empty>

</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<script type="text/javascript">

function volver() {
	var action = $('action');
	action.value = 'VOLVER';		
}

function guardar(){
	var action = $('action');
	action.value = 'GUARDAR';
}
</script>
	