<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.objetoi.id"
		value="${ProcessForm.process.objetoiIndice.credito.id}" />
	<html:hidden property="process.objetoiIndice.id"
		value="${ProcessForm.process.objetoiIndice.id}" />
	<html:hidden property="process.idObjetoi" styleId="process_idObjetoi"
		value="${ProcessForm.process.objetoiIndice.credito.id}" />
	<html:hidden property="process.idObjetoiIndice"
		styleId="process_idObjetoiIndice"
		value="${ProcessForm.process.idObjetoiIndice}" />
	<html:hidden property="process.action" styleId="action"
		value="saveIndice" />

	<html:errors></html:errors>

	<h3>Tipo Tasa: Inter&eacute;s ${ProcessForm.process.tipoTasa}</h3>

	<table>
		<tr>
			<th>Tasa Inter&eacute;s ${ProcessForm.process.tipoTasa}</th>
			<td><asf:selectpop name="process.objetoiIndice.indice.id"
					title="Tasa" columns="Codigo, Nombre" captions="id,nombre"
					values="id" entityName="com.nirven.creditos.hibernate.Indice"
					value="${ProcessForm.process.objetoiIndice.indice.id}"
					filter="tipo = 1" onChange="tasa();" /> <asf:text
					name="ProcessForm" id="indiceValor" readonly="true"
					property="process.indiceValorStr" type="decimal" maxlength="15" />
			</td>
		</tr>
		<tr>
			<th>Mas</th>
			<td><html:text property="process.objetoiIndice.valorMasStr"
					value="${ProcessForm.process.objetoiIndice.valorMasStr}" size="15"
					onchange="calcularValorFinal();" styleId="mas"
					readonly="${ProcessForm.process.acuerdoPago}" /></td>
		</tr>
		<tr>
			<th>Por</th>
			<td><asf:text id="por" name="ProcessForm"
					property="process.objetoiIndice.valorPorStr"
					attribs='onchange="calcularValorFinal();"' type="decimal"
					maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
		</tr>
		<tr>
			<th>Tope Tasa:</th>
			<td><asf:text id="topeTasa" name="ProcessForm"
					property="process.objetoiIndice.tasaTopeStr"
					attribs='onchange="calcularValorFinal();"' type="decimal"
					maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
		</tr>
		<tr>
			<th>D&iacute;as antes</th>
			<td><html:text
					value="${ProcessForm.process.objetoiIndice.diasAntes}"
					property="process.objetoiIndice.diasAntes" size="15"
					readonly="${ProcessForm.process.acuerdoPago}" /></td>
		</tr>
		<tr>
			<th>Tasa Inter&eacute;s ${ProcessForm.process.tipoTasa} final</th>
			<td><html:text property="process.valorFinal" readonly="true"
					size="15" styleId="valorFinal" /></td>
		</tr>
		<logic:equal name="ProcessForm" property="process.compensatorio"
			value="true">
			<tr>
				<th>Bonificaci&oacute;n</th>
				<td><html:text property="process.bonificacionStr"
						readonly="true" size="15" /> <input type="button"
					value="Modificar..." onclick="modificarBonificacion();" /></td>
			</tr>
			<tr>
				<th>Tasa Inter&eacute;s Compensatorio Neta</th>
				<td><html:text property="process.tasaNetaStr" readonly="true"
						size="15" /></td>
			</tr>
		</logic:equal>
		<tr>
			<th>Tipo C&aacute;lculo:</th>
			<td><asf:select name="process.objetoiIndice.tipoCalculo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="ProcessForm.process.objetoiIndice.tipoCalculo"
					filter="categoria='Interes.TipoCalculo' order by codigo desc"
					nullValue="true"
					nullText="-- Seleccione un Tipo de C&aacute;lculo --" /></td>
		</tr>
	</table>
	<div style="width: 100%; text-align: center">
		<html:submit value="Guardar"></html:submit>
		<input type="button" value="Volver" onclick="cancelar();" />
	</div>
</html:form>
<script type="text/javascript">
  		$j = jQuery.noConflict();
		function tasa() {
			var idIndice = $j("#process\\.objetoiIndice\\.indice\\.id").val();
				$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='+idIndice, 
					function(data) {
						$j("#indiceValor").val(data);
						calcularValorFinal();
					}
				);
			if (${ProcessForm.process.compensatorio} == true) {
				var form = document.getElementById('oForm');
				var action = document.getElementById('action');
	    		action.value = 'modificarBonificacion';
	    		form.submit();
			}
		}
  		
    	function cancelar() {
    	    var form = document.getElementById('oForm');    	
    		var action = document.getElementById('action');
    		action.value = 'load';
    		form.submit();
    	}

    	function modificarBonificacion() {
        	if (confirm("Los cambios no guardados se perder�n. �Desea continuar?")) {
	    		var idCredito = $j("#process_idObjetoi").val();
				var idIndice = $j("#process_idObjetoiIndice").val();
	        	var url = "${pageContext.request.contextPath}/actions/abmAction.do?entityName=ObjetoiBonificacion&filter=true&do=list";
	        	url += "&paramName[0]=idCredito&paramComp[0]==&paramValue[0]=" + idCredito;
	        	url += "&idIndice="+idIndice;
	        	window.location = url;
        	}
    	}
    	
    	function calcularValorFinal(){
    		var tasa = $j("#indiceValor").val().replace(',', '.').replace('%', '');
			var mas = $j("#mas").val().replace(',', '.').replace('%','');
			var por = $j("#por").val().replace(',', '.').replace('%','');
			var topeTasa = $j("#topeTasa").val().replace(',', '.').replace('%','');
			$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal', 
					'&tasa='+tasa+'&mas='+mas+'&por='+por+'&topeTasa='+topeTasa,
					function(data) {
						$j("#valorFinal").val(data);
					}); 
		}
    </script>
