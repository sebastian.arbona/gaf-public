<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<!-- diegobq -->
<div class="title">Moneda</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
<html:hidden property="entity.id"/>
<table border="0">
	<tr>
		<th>Denominación:</th>
		<td><html:text property="entity.denominacion" maxlength="40" styleId="denominacion" /></td>
	</tr>
	<tr>
		<th>Abreviatura:</th>
		<td><html:text property="entity.abreviatura" maxlength="30" /></td>
	</tr>
	<tr>
		<th>Símbolo:</th>
		<td><html:text property="entity.simbolo" maxlength="10" /></td>
	</tr>
</table>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<script language='javascript' >
var denominacion = $( 'denominacion' );

denominacion.focus();
</script>