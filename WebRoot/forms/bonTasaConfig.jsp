<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 


<div class="title">
	Configuraci�n de cr�ditos externos
</div>
<br>
<html:form action="/actions/process.do?do=process&processName=BonTasaProcess" styleId="oForm">
  	<html:hidden property="process.accion" styleId="accion" value="save"/>
  	<html:hidden property="process.idConfig" styleId="config" value="${ProcessForm.process.idConfig}"/>
  	
  	<logic:equal value="false" property="process.conf" name="ProcessForm">
		<div class="title">
			Bonificaciones M�ximas por Ente Externo
		</div>
		<table>
		<tr>
    		<th>Ente Financiador</th>
    		<td colspan="3"><asf:selectpop name="process.idEnte" title="Agente Financiero"
				columns="C�digo, Nombre"
				captions="codiBa,detaBa" values="codiBa"
				entityName="com.asf.gaf.hibernate.Bancos" 
				value="${ProcessForm.process.idEnte}" 
				filter="financiador = 1  order by codiBa"/>
    		</td>      		
    	</tr>
    	<tr>
	    	<th>
	    		Bonificaci�n Maxima ($)
	    	</th>
    		<td>
    			<html:text property="process.maximoEnte" name="ProcessForm"/>    			
    		</td>
    		<th>
    			Bonificaci�n Maxima por Persona($)
    		</th>
    		<td>
    			<html:text property="process.bonMaxPersona" name="ProcessForm"/>
    			<input type="button" value="Agregar" onclick="agregar();"/>
    		</td>    		
    	</tr>
    	</table>
    	<logic:equal value="true" property="process.error" name="ProcessForm">
    		No se puede guardar la configuracion, la entidad bancaria no es correcta
    	</logic:equal>
    	<br/>
    	<display:table name="ProcessForm" property="process.configuraciones"
				defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.BonTasaEntidadConf"
				export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column property="banco.detaBa" title="Ente Financiador" paramId="process.idConfig" paramProperty="id"
					sortable="false" href="${pageContext.request.contextPath}/actions/process.do?processName=BonTasaProcess&do=process&process.accion=mostrarMod"/>
				<display:column property="maxBonEnte" title="Bonificacion Maxima ($)"
					sortable="false" />
				<display:column property="bonMaxPersona" title="Bonificacion Maxima por Persona($)"
					sortable="false" />
				<display:column media="html" honClick="return confirmDelete();" title="Eliminar"
				      href="${pageContext.request.contextPath}/actions/process.do?processName=BonTasaProcess&do=process&process.accion=eliminarConfig"
				      paramId="process.idConfig" paramProperty="id">
				      <bean:message key="abm.button.delete" />
				</display:column>    
	    		
		</display:table>

  	</logic:equal>
  	<logic:equal value="true" property="process.conf" name="ProcessForm">
 		<div class="title">
		Bonificaciones M�ximas por Ente Externo
		</div>
		<table>
		<tr>
    		<th>Ente Financiador</th>
    		<td colspan="3"><asf:selectpop name="process.idEnte" title="Agente Financiero"
				columns="C�digo, Nombre"
				captions="codiBa,detaBa" values="codiBa"
				entityName="com.asf.gaf.hibernate.Bancos" 
				value="${ProcessForm.process.idEnte}" 
				filter="financiador = 1  order by codiBa" enabled="false"/>
    		</td>  
    	</tr>
    	<tr>	
    	    <th>Bonificaci�n Maxima ($)</th>
    		<td>
    			<html:text property="process.maximoEnte" name="ProcessForm"/>
    		</td>
    	</tr>
    	<tr>	
    	    <th>Bonificaci�n Maxima por Persona($)</th>    	
    		<td>
    			<html:text property="process.bonMaxPersona" name="ProcessForm"/>    			
    		</td>
    	</tr>
    	</table>
    	<br/>
    	<input type="button" value="Modificar" onclick="modificar();">    		
  	</logic:equal>
</html:form>
 <script type="text/javascript">
 	function agregar(){
		var accion = document.getElementById("accion");		
			var formulario = document.getElementById("oForm");
			accion.value="configAgregar";
			formulario.submit();
	}
 	function guardar(){
 		var accion = document.getElementById("accion");		
			var formulario = document.getElementById("oForm");
			accion.value="configGuardar";
			formulario.submit();
 	}
 	function modificar(){
 	var accion = document.getElementById("accion");		
			var formulario = document.getElementById("oForm");
			accion.value="configModificar";
			formulario.submit();
 	} 	
 </script>