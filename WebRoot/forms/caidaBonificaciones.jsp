
<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<div class="title">
  Caida de Bonificaciones
</div>
<html:form action="actions/process.do?processName=${param.processName}&do=process" styleId="oForm"> 
	<html:hidden property="process.accion" styleId="action" value="listar"/>  
	<logic:equal value="false" property="process.baja" name="ProcessForm">
  
  	<div class="grilla">
  	<table style="margin: 20px 0">
  		<tr>
  			<th>Convenio</th>
  			<td>
  				<asf:select entityName="com.nirven.creditos.hibernate.ConvenioBonificacion" listCaption="getId,getNombre"
          			listValues="getId" name="process.idConvenio" value="${ProcessForm.process.idConvenio}" nullText="Todos" nullValue="true"/>
  			</td>
  		</tr>
  		<tr>
  			<th>M&aacute;s de</th>
  			<td>
  				<asf:text maxlength="4" type="long" property="process.diasMoraFiltro" name="ProcessForm"/>
  				d&iacute;as de mora
  			</td>
  		</tr>
  		<tr>
  			<th>L&iacute;nea de cr&eacute;dito</th>
  			<td>
  				<asf:select entityName="com.nirven.creditos.hibernate.Linea" listCaption="getId,getNombre"
          			listValues="getId" name="process.idLinea" value="${ProcessForm.process.idLinea}" nullText="Todas" nullValue="true"/>
  			</td>
  		</tr>
  		<tr>
  			<th colspan="2">
  				<html:submit value="Buscar"></html:submit>
  			</th>
  		</tr>
  	</table>
  
  	<logic:notEmpty property="process.bonifs" name="ProcessForm">
  	<input type="checkbox" onclick="activar();" id="activacion"/> Seleccionar todos
 	</logic:notEmpty>
  
    <display:table name="ProcessForm" property="process.bonifs" export="true" id="reportTable" defaultsort="1"
    requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
    	  <display:column title="Selecci�n">
				<input type="checkbox" name="credito-${reportTable.idBonificacion}" value="${reportTable.idBonificacion}" />
		  </display:column>
	      <display:column property="numeroAtencion" title="Proyecto" sortable="true" />
	      <display:column title="Expediente" property="expediente" sortable="true" />
	      <display:column property="titular" title="Titular" sortable="true" />
	      <display:column property="cuit" title="CUIL/CUIT" sortable="true" />
	      <display:column property="linea" title="Linea" sortable="true" />
	      <display:column property="comportamiento" title="Comportamiento" sortable="true" />
	      <display:column property="convenio" title="Convenio" sortable="true" />
	      <display:column property="tasaBonificacion" title="Tasa Bonif." sortable="true"/>
	      <display:column property="tipoBonificacion" title="Tipo Bonif." sortable="true"/>
	      <display:column title="Deuda">
	      	<div id="deuda-${reportTable.idObjetoi}" style="float: left"></div>
	      	<div id="loader-${reportTable.idObjetoi}" style="display: none"><img src="${pageContext.request.contextPath}/images/loader.gif"/></div>
	      	<div style="float: right">
	      		<a href="javascript:void(0);" onclick="cargarDeuda(${reportTable.idObjetoi});">Ver</a>
	      	</div>
	      </display:column>
	      <display:column property="diasMora" title="D�as Mora" sortable="true" />
	      <display:column property="diasExcedido" title="D�as Excedido" sortable="true" />      	        
      </display:table>
      
      <table>
      	<tr>
      		<th>Fecha de ca&iacute;da</th>
      		<td>
      			<asf:calendar property="ProcessForm.process.fechaCaida" value="${process.fechaCaida}"/>
      		</td>
      	</tr>
      	<tr>
      		<th>Observaciones</th>
      		<td>
      			<html:textarea property="process.texto" name="ProcessForm" rows="2" cols="110"></html:textarea>
      		</td>
      	</tr>
      	<tr>
      		<th colspan="2">
      			<input type="button" value="Dar de baja Bonificaciones" onclick="darBaja();">
      		</th>
      	</tr>
      </table>    
   </div>
   </logic:equal>
   <logic:notEqual value="false" property="process.baja" name="ProcessForm">
   Se dio de baja bonificaciones exitosamente
   <input type="button" value="Atras" onclick="atras();">
   </logic:notEqual>
</html:form>
<script type="text/javascript">
	
	  
	  function marcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = true;
	        }
	    }
	  }

	  function desmarcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = false;
	        }
	    }
	  }
	  
	  function darBaja(){
	  	if (confirm("Se van a dar de baja las bonificaciones de los Cr�ditos seleccionados. �Desea continuar?")) {
    		var accion = $('action');
			var formulario = $('oForm');
        	accion.value='baja';
        	formulario.submit();
       	}
	  }
	  
	  function atras(){	  	
   		window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CaidaBonificaciones&process.accion=listar";
	  }
	  
	  function activar(){
		var check = document.getElementById('activacion');
		if (check.checked==true){
			marcarTodos();
		}else{
			desmarcarTodos();
		}
	  }
	  
	  function cargarDeuda(idCredito) {
		var loader = document.getElementById("loader-"+idCredito);
		loader.style.display = "inline";
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularDeuda";
		url += "&idCredito=" + idCredito;
		retrieveURL(url, cargarDeudaResponse);
		return false;
	  }

	  function cargarDeudaResponse(responseText) {
		  var objetos = parseXml(responseText);
		  var credito = objetos.Credito;
		  var loader = document.getElementById("loader-"+credito.id);
		  loader.style.display = "none";
		  var div = document.getElementById("deuda-"+credito.id);
		  div.innerHTML = credito.deuda;
	  }
	  </script>
	  
	  <iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>