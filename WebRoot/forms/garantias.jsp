<%@ page language="java"%>
<%@ page import="com.nirven.creditos.hibernate.FamiliaCampoGarantia"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>

<div class="title">Agregar Garant�a</div>
<br>
<br>

<html:form
	action="actions/process.do?processName=${param.processName}&do=process"
	styleId="formulario">

	<html:hidden name="ProcessForm" property="process.accionStr"
		value="GUARDAR_GARANTIA" styleId="accion" />
	<html:hidden property="process.idGarantia" name="ProcessForm"
		styleId="idGarantia" />
	<html:hidden property="process.idGarantiaTasacion" name="ProcessForm" />
	<html:hidden property="process.idGarantiaTasacionInmueble"
		name="ProcessForm" />
	<html:hidden property="process.idGarantiaTasacionMueble"
		name="ProcessForm" />
	<html:hidden property="process.idSeguro" name="ProcessForm" />
	<html:hidden property="process.idSolicitud" name="ProcessForm"
		styleId="idSolicitud" />
	<html:hidden property="process.oGarantiaId" name="ProcessForm"
		styleId="oGarantiaId" />

	<span style="color: red; font-size: 11pt; font-weight: bold"> <html:errors />
	</span>

	<logic:notEmpty name="ProcessForm" property="process.mensajeError">
		<bean:write name="ProcessForm" property="process.mensajeError" />
	</logic:notEmpty>

	<%
		int offset;
	%>

	<table border="0" width="90%" align="left">
		<html:hidden property="process.idSolicitud" />

		<tr>
			<th>Id:</th>
			<td><asf:text name="ProcessForm" property="process.idSolicitud"
					type="long" maxlength="8" readonly="true" /></td>
		</tr>
		<tr>
			<th>Tipo de Garant�a:</th>
			<td style="text-transform: capitalize"><asf:select
					name="process.idTipo" listCaption="nombre"
					value="${ProcessForm.process.idTipo}"
					enabled="${ProcessForm.process.garantia.id==0 || ProcessForm.process.garantia.id==null}"
					entityName="com.nirven.creditos.hibernate.TipoGarantia"
					listValues="id" attribs="onChange='seleccionTipoGarantia()'" /></td>
		</tr>
		<logic:equal value="MODIFICAR_GARANTIA" name="ProcessForm"
			property="process.accionStr">
			<tr>
				<logic:equal value="2" name="ProcessForm"
					property="process.oGarantia.estado">
					<th>Fecha Sustituci&oacute;n:</th>
					<td><asf:text name="ProcessForm"
							property="process.fechaSustitucionStr" type="text" maxlength="50"
							readonly="true" /></td>
				</logic:equal>
			</tr>
		</logic:equal>
		<logic:equal value="true" property="process.campoId"
			name="ProcessForm">
			<tr>
				<th>${ProcessForm.process.nombreCampoIdentificador}</th>
				<td><html:text name="ProcessForm"
						property="process.garantia.identificadorUnico"
						disabled="${ProcessForm.process.modifica}" onblur="comprobar();">
						<jsp:attribute name="value">
						
							${ProcessForm.process.garantia.identificadorUnico}
						
					</jsp:attribute>
					</html:text> <logic:equal value="false" property="process.modifica"
						name="ProcessForm">
						<input type="button" value="Comprobar" onclick="comprobar();">
					</logic:equal> <logic:equal value="true" name="ProcessForm"
						property="process.garantiaCompartidaOtrosProyectos">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConsultaObjetoiGarantia&process.idGarantia=${ProcessForm.process.garantia.id}"
							target="_blank">Ver Uso en Otros Proyectos</a>
					</logic:equal></td>
			</tr>
		</logic:equal>
		<tr>
		</tr>

		<logic:equal name="ProcessForm" value="false"
			property="process.modTitular">
			<tr>
				<th>Porcentaje de Titularidad</th>
				<td><html:text name="ProcessForm" styleId="porcentajeTit"
						property="process.porcentajeStr"
						onclick="verificarPorcentaje(porcentajeTit)" />
			</tr>
		</logic:equal>
		<tr>
			<th>Breve descripci�n del Bien ofrecido en Garant�a:</th>
			<td colspan="3"><html:textarea
					property="process.oGarantia.observacion" name="ProcessForm"
					cols="70" rows="5"></html:textarea></td>
		</tr>
		<logic:equal value="true" property="process.inmovilizacion"
			name="ProcessForm">
			<tr>
				<th>Producto de Referencia</th>
				<td><asf:select name="process.tipoProducto"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.tipoProducto}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria='Garantia.tipoProducto' order by codigo" /></td>
			</tr>
			<tr>
				<th>CUIT Asociado</th>
				<td><html:text name="ProcessForm" property="process.cuit"
						value="${ProcessForm.process.cuit}" /> Ej: 30123456789</td>
			</tr>
		</logic:equal>


		<logic:iterate id="objeto" property="process.valoresGeneral"
			name="ProcessForm" indexId="index">
			<tr>
				<th colspan="2"><bean:write name="objeto"
						property="campo.nombre" /></th>
				<td colspan="2"><logic:equal name="ProcessForm"
						property="<%=\"process.tipoDeCampo[\" + index + \"]\"%>"
						value="Texto">

						<html:text name="ProcessForm"
							property="<%=\"process.valorCadena[\" + index +\"]\"%>" />
					</logic:equal> <logic:equal name="ProcessForm"
						property="<%=\"process.tipoDeCampo[\" + index + \"]\"%>"
						value="Area">

						<html:textarea name="ProcessForm"
							property="<%=\"process.valorCadena[\" + index +\"]\"%>" rows="4" />
					</logic:equal> <logic:equal name="ProcessForm"
						property="<%=\"process.tipoDeCampo[\" + index + \"]\"%>"
						value="Numerico">
						<asf:text name="ProcessForm"
							property="<%=\"process.valorDouble[\" + index +\"]\"%>"
							type="decimal" maxlength="" rows="1" />
					</logic:equal> <logic:equal name="ProcessForm"
						property="<%=\"process.tipoDeCampo[\" + index + \"]\"%>"
						value="Fecha">
						<asf:calendar
							property="<%= \"ProcessForm.process.valorCadena[\" + index + \"]\" %>"
							value="${process.valorCadena[index]}"></asf:calendar>
					</logic:equal></td>
			</tr>
		</logic:iterate>
	</table>


	<div style="clear: both" />

	<logic:equal name="ProcessForm"
		property="process.garantiaTasacionInmueble" value="true">
		<div>
			<table border="0" width="60%" align="left">
				<tr>
					<td style="text-transform: capitalize" colspan="6" align="center">
						<b> Datos de Identificaci�n del Inmueble </b>
					</td>
				</tr>
				<tr>
					<td colspan="6" style="text-transform: capitalize"><b>
							Descripci�n del Inmueble </b></td>
				</tr>
				<tr>
					<th>Calle:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.calle"
							value="${ProcessForm.process.tasacionInmueble.calle}" /></td>
					<th>Nro:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.nro"
							value="${ProcessForm.process.tasacionInmueble.nro}" /></td>
					<th>Piso:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.piso"
							value="${ProcessForm.process.tasacionInmueble.piso}" /></td>
				</tr>
				<tr>
					<th>Depto/Oficina:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.deptoOf"
							value="${ProcessForm.process.tasacionInmueble.deptoOf}" /></td>
					<th>Barrio/Mzna/Casa:</th>
					<td colspan="4"><html:text name="ProcessForm"
							property="process.tasacionInmueble.barrioMznaCasa"
							value="${ProcessForm.process.tasacionInmueble.barrioMznaCasa}" />
					</td>
				</tr>
				<tr>
					<th>Lugar:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.lugar"
							value="${ProcessForm.process.tasacionInmueble.lugar}" /></td>
					<th>Distrito:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.distrito"
							value="${ProcessForm.process.tasacionInmueble.distrito}" /></td>
					<th>Departamento:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.departamento"
							value="${ProcessForm.process.tasacionInmueble.departamento}" />
					</td>
				</tr>
				<tr>
					<th>Provincia:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionInmueble.provincia"
							value="${ProcessForm.process.tasacionInmueble.provincia}" /></td>
				</tr>
				<tr>
					<th>Superficie seg�n Titulo (m2):</th>
					<td colspan="6"><asf:text name="ProcessForm" type="decimal"
							maxlength="20"
							property="process.tasacionInmueble.superficieTitulo"
							value="${ProcessForm.process.tasacionInmueble.superficieTitulo}" />
					</td>
				</tr>
				<tr>
					<th>Superficie seg�n Plano (m2):</th>
					<td><asf:text name="ProcessForm" type="decimal" maxlength="20"
							property="process.tasacionInmueble.superficiePlano"
							value="${ProcessForm.process.tasacionInmueble.superficiePlano}" />
					</td>
					<th>Numero de Plano:</th>
					<td colspan="4"><asf:text name="ProcessForm" type="long"
							maxlength="20" property="process.tasacionInmueble.nroPlano"
							value="${ProcessForm.process.tasacionInmueble.nroPlano}" /></td>
				</tr>
				<tr>
					<th>Nomenclatura Catastral:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionInmueble.nomenclaturaCatastral"
							value="${ProcessForm.process.tasacionInmueble.nomenclaturaCatastral}" />
					</td>
				</tr>
				<tr>
					<td colspan="6" style="text-transform: capitalize"><b>
							Inscripci�n Registral: </b></td>
				</tr>
				<tr>
					<th>Forma de Inscripci�n:</th>

					<td colspan="6"><html:select property="process.seleccion"
							name="ProcessForm" styleId="periodo" onchange="seleccion();">
							<html:option value="-1">Por numero de Matricula</html:option>
							<html:option value="1">Por numero de Asiento</html:option>
						</html:select></td>
				</tr>
				<tr id="matricula">
					<th>Nro Matricula:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.nroMatricula"
							value="${ProcessForm.process.tasacionInmueble.nroMatricula}" />
					</td>
					<th>Asiento:</th>
					<td colspan="4"><html:text name="ProcessForm"
							property="process.tasacionInmueble.asiento" maxlength="5"
							value="${ProcessForm.process.tasacionInmueble.asiento}" /></td>
				</tr>
				<tr id="asiento">
					<th>Nro Asiento:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.nroAsiento" maxlength="10"
							value="${ProcessForm.process.tasacionInmueble.nroAsiento}" /></td>
					<th>Fojas:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.fojas" maxlength="4"
							value="${ProcessForm.process.tasacionInmueble.fojas}" /></td>
					<th>Tomos:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacionInmueble.tomo1" maxlength="20"
							value="${ProcessForm.process.tasacionInmueble.tomo1}" /></td>
				</tr>
				<tr>
					<th>Grado:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionInmueble.grado"
							value="${ProcessForm.process.tasacionInmueble.grado}" /></td>
				</tr>
				<tr>
					<th>Grav�menes:</th>
					<td colspan="6"><html:textarea name="ProcessForm"
							property="process.tasacionInmueble.gravamenes"
							value="${ProcessForm.process.tasacionInmueble.gravamenes}" /></td>
				</tr>
				<tr>
					<th>Medidas Cautelares:</th>
					<td colspan="6"><html:textarea name="ProcessForm"
							property="process.tasacionInmueble.medidasCaut"
							value="${ProcessForm.process.tasacionInmueble.medidasCaut}" /></td>
				</tr>
				<tr>
					<th>Otros:</th>
					<td colspan="6"><html:textarea name="ProcessForm"
							property="process.tasacionInmueble.otros"
							value="${ProcessForm.process.tasacionInmueble.otros}" /></td>
				</tr>
				<tr>
					<td style="text-transform: capitalize" colspan="6"><b>
							Datos Tasaci�n </b></td>
				</tr>
				<tr>
					<th>Fecha de Tasaci�n:</th>
					<td colspan="6"><asf:calendar
							property="ProcessForm.process.tasacionInmueble.fechaTasacionStr"
							attribs="class='fechaTasacionStr'" /></td>
				</tr>
				<tr>
					<th>Nombre del Tasador:</th>
					<td colspan="6"><asf:selectpop
							name="process.tasacionInmueble.personaTasadorId" title="Persona"
							columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
							captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
							entityName="com.civitas.hibernate.persona.Persona"
							filter="id in (select e.persona.id from EspecialidadPersona e where e.especialidad.id = (select d.valor from Director d where d.codigo = 'especialidad.tasador'))"
							value="${ProcessForm.process.tasacionInmueble.personaTasadorId}" />
					</td>
				</tr>
				<tr>
					<th>Tasador (migrado)</th>
					<td>${ProcessForm.process.tasacionInmueble.tasador}</td>
				</tr>
				<tr>
					<th>Valor Terreno:</th>
					<td colspan="6"><html:text name="ProcessForm"
							styleId="valorTerreno"
							property="process.tasacionInmueble.valorTerreno"
							value="${ProcessForm.process.tasacionInmueble.valorTerreno}"
							onchange="calcularTotalTasacion();" /></td>
				</tr>
				<tr>
					<th>Valor Mejoras:</th>
					<td colspan="6"><html:text name="ProcessForm"
							styleId="valorMejoras"
							property="process.tasacionInmueble.valorMejoras"
							value="${ProcessForm.process.tasacionInmueble.valorMejoras}"
							onchange="calcularTotalTasacion();" /></td>
				</tr>
				<tr>
					<th>Valor Cultivos:</th>
					<td colspan="6"><html:text name="ProcessForm"
							styleId="valorCultivos"
							property="process.tasacionInmueble.valorCultivos"
							value="${ProcessForm.process.tasacionInmueble.valorCultivos}"
							onchange="calcularTotalTasacion();" /></td>
				</tr>
				<tr>
					<th>Valor Riego:</th>
					<td colspan="6"><html:text name="ProcessForm"
							styleId="valorRiego"
							property="process.tasacionInmueble.valorRiego"
							value="${ProcessForm.process.tasacionInmueble.valorRiego}"
							onchange="calcularTotalTasacion();" /></td>
				</tr>
				<tr>
					<th>Valor Otros:</th>
					<td colspan="6"><html:text name="ProcessForm"
							styleId="valorOtros"
							property="process.tasacionInmueble.valorOtros"
							value="${ProcessForm.process.tasacionInmueble.valorOtros}"
							onchange="calcularTotalTasacion();" /></td>
				</tr>
				<tr>
					<th>Valor Total de Tasaci�n:</th>
					<td colspan="6"><html:text name="ProcessForm"
							styleId="valorTotalTasacion"
							property="process.tasacionInmueble.valorTotalTasacion"
							readonly="true"
							value="${ProcessForm.process.tasacionInmueble.valorTotalTasacion}" />
					</td>
				</tr>
				<tr>
					<th>Valor Neto de Realizaci�n:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionInmueble.valorNetoRealizacion"
							value="${ProcessForm.process.tasacionInmueble.valorNetoRealizacion}" />
					</td>
				</tr>

				<%
					offset = FamiliaCampoGarantia.INMUEBLE.ordinal() * 1000;
				%>
				<logic:iterate id="objeto" property="process.valoresInmueble"
					name="ProcessForm" indexId="index">
					<tr>
						<th colspan="2"><bean:write name="objeto"
								property="campo.nombre" /></th>
						<td colspan="2"><logic:equal name="ProcessForm"
								property="<%=\"process.tipoDeCampo[\" + (index + offset) + \"]\"%>"
								value="Texto">

								<html:text name="ProcessForm"
									property="<%=\"process.valorCadena[\" + (index + offset) +\"]\"%>" />
							</logic:equal> <logic:equal name="ProcessForm"
								property="<%=\"process.tipoDeCampo[\" + (index + offset) + \"]\"%>"
								value="Area">

								<html:textarea name="ProcessForm"
									property="<%=\"process.valorCadena[\" + (index + offset) +\"]\"%>"
									rows="4" />
							</logic:equal> <logic:equal name="ProcessForm"
								property="<%=\"process.tipoDeCampo[\" + (index + offset) + \"]\"%>"
								value="Numerico">
								<asf:text name="ProcessForm"
									property="<%=\"process.valorDouble[\" + (index + offset) +\"]\"%>"
									type="decimal" maxlength="" rows="1" />
							</logic:equal> <logic:equal name="ProcessForm"
								property="<%=\"process.tipoDeCampo[\" + (index + offset) + \"]\"%>"
								value="Fecha">
								<asf:calendar
									property="<%= \"ProcessForm.process.valorCadena[\" + (index + offset) + \"]\" %>"
									value="${process.valorCadena[index]}"></asf:calendar>
							</logic:equal></td>
					</tr>
				</logic:iterate>

			</table>
		</div>
	</logic:equal>
	<logic:equal name="ProcessForm"
		property="process.garantiaTasacionMueble" value="true">
		<div>
			<table border="0" width="60%" align="left">
				<tr>
					<td style="text-transform: capitalize" colspan="6"><b>
							Datos Tasaci�n Mueble </b></td>
				</tr>
				<tr>
					<th>Fecha de Tasaci�n:</th>
					<td colspan="6"><asf:calendar
							property="ProcessForm.process.tasacionMueble.fechaTasacionStr"
							attribs="class='fechaTasacionStr'" /></td>
				</tr>
				<tr>
					<th>Nombre del Tasador:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionMueble.tasador"
							value="${ProcessForm.process.tasacionMueble.tasador}" /></td>
				</tr>
				<tr>
					<th>Valor Total de Tasaci�n:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionMueble.valorTotalTasacion"
							value="${ProcessForm.process.tasacionMueble.valorTotalTasacion}" />
					</td>
				</tr>
				<tr>
					<th>Valor Neto de Realizaci�n:</th>
					<td colspan="6"><html:text name="ProcessForm"
							property="process.tasacionMueble.valorNetoRealizacion"
							value="${ProcessForm.process.tasacionMueble.valorNetoRealizacion}" />
					</td>
				</tr>
			</table>
		</div>
	</logic:equal>
	<div>
		<table border="0" width="60%" align="left">
			<tr>
				<th>Importe constituci&oacute;n de la Garant&iacute;a:</th>
				<td><logic:equal name="ProcessForm"
						property="process.modifValorCons" value="true">
						<asf:text name="ProcessForm" maxlength="20" type="decimal"
							property="process.oGarantia.valor"
							value="${ProcessForm.process.oGarantia.valor}" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.modifValorCons" value="true">
						<asf:text name="ProcessForm" maxlength="20" type="decimal"
							property="process.oGarantia.valor" readonly="true"
							value="${ProcessForm.process.oGarantia.valor}" />
					</logic:notEqual></td>
			</tr>

			<tr>
				<th>Aforo:</th>
				<td><logic:equal name="ProcessForm"
						property="process.modifValorCons" value="true">
						<asf:text name="ProcessForm" maxlength="20" type="decimal"
							property="process.aforo" value="${ProcessForm.process.aforo}" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.modifValorCons" value="true">
						<asf:text name="ProcessForm" maxlength="20" type="decimal"
							property="process.aforo" readonly="true"
							value="${ProcessForm.process.aforo}" />
					</logic:notEqual></td>
			</tr>


		</table>
	</div>
	<logic:equal name="ProcessForm" property="process.garantiaTasaciones"
		value="true">
		<div>
			<table border="0" width="60%" align="left">
				<tr>
					<td colspan="2" style="text-transform: capitalize"><b>
							Gravamen e Instrumento de Garant�a </b></td>
				</tr>
				<tr>
					<th>N�mero de Escritura o Contrato:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.gravNroEscritura">
							<jsp:attribute name="value">
						 		${ProcessForm.process.tasacion.gravNroEscritura}
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Foja:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.gravFoja">
							<jsp:attribute name="value">
							<fmt:formatNumber maxFractionDigits="0" minFractionDigits="0">
						 		${ProcessForm.process.tasacion.gravFoja}
						 		</fmt:formatNumber>
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Fecha de Escritura o Constituci�n de Garant�a:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.gravFechaEscrituraStr"
							attribs="class='gravFechaEscrituraStr'" /></td>
				</tr>
				<tr>
					<th>Escribano:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.gravEscribano">
							<jsp:attribute name="value">
						 		${ProcessForm.process.tasacion.gravEscribano}
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Asesor Notarial:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.gravAsesorNotarial">
							<jsp:attribute name="value">
						 		${ProcessForm.process.tasacion.gravAsesorNotarial}
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Fecha de Presentacion en Registro:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.gravFechaPresentacionRegStr"
							attribs="class='gravFechaPresentacionRegStr'" /></td>
				</tr>
				<tr>
					<th>Inscripci�n:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.gravInscripcion">
							<jsp:attribute name="value">
						 		${ProcessForm.process.tasacion.gravInscripcion}
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Fecha de Inscripci�n:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.gravFechaInscripcionStr"
							attribs="class='gravFechaInscripcionStr' onblur='calcularFechaVencimientoGarantia();'" />
					</td>
				</tr>
				<tr>
					<th>Fecha de Vencimiento de la Garantia:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.gravFechaVencGarantiaStr"
							attribs="class='gravFechaVencGarantiaStr'" /></td>
				</tr>
				<tr>
					<th>Fecha de Reinscripcion:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.gravFechaReinscripcionStr"
							attribs="class='gravFechaReinscripcionStr' onblur='calcularFechaVencimientoGarantia();'" />
					</td>
				</tr>
				<tr>
					<th>Fecha Modificacion del Instrumento 1:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaSustitucionStr"
							attribs="class='fechaSustitucionStr'" /></td>
				</tr>
				<tr>
					<th>Numero Modificacion del Instrumento 1:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.nroSustitucion"
							value="${ProcessForm.process.tasacion.nroSustitucion}" /></td>
				</tr>
				<tr>
					<th>Fecha Modificacion del Instrumento 2:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaSustitucion2Str"
							attribs="class='fechaSustitucion2Str'" /></td>
				</tr>
				<tr>
					<th>Numero Modificacion del Instrumento 2:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.nroSustitucion2"
							value="${ProcessForm.process.tasacion.nroSustitucion2}" /></td>
				</tr>
				<tr>
					<th>Fecha Modificacion del Instrumento 3:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaSustitucion3Str"
							attribs="class='fechaSustitucion3Str'" /></td>
				</tr>
				<tr>
					<th>Numero Modificacion del Instrumento 3:</th>
					<td><html:text name="ProcessForm"
							property="process.tasacion.nroSustitucion3"
							value="${ProcessForm.process.tasacion.nroSustitucion3}" /></td>
				</tr>
				<tr>
					<th>Fecha de Finalizaci�n:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaCancelacionStr"
							attribs="class='fechaCancelacionStr'" /></td>
				</tr>
				<tr>
					<th>N�mero de Finalizaci�n:</th>
					<td><asf:text type="text" maxlength="10" name="ProcessForm"
							property="process.tasacion.nroCancelacion"
							value="${ProcessForm.process.tasacion.nroCancelacion}" /></td>
				</tr>
				<tr>
					<th>Fecha de Cancelaci�n:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaCancelacionCreditoStr" />
					</td>
				</tr>
				<tr>
					<th>Fecha de Alta Contable:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaAltaContableStr" /></td>
				</tr>
				<tr>
					<th>Fecha de Ejecuci�n:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaEjecucionStr" /></td>
				</tr>
				<tr>
					<th>Fecha de Cesi�n:</th>
					<td><asf:calendar
							property="ProcessForm.process.tasacion.fechaCesionStr" /></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</div>
	</logic:equal>
	<logic:equal name="ProcessForm" property="process.garantiaSeguro"
		value="true">
		<div>
			<table border="0" width="60%" align="left">
				<tr>
					<td style="text-transform: capitalize" colspan="2"><b>
							Datos del Seguro </b></td>
				</tr>
				<tr>
					<th>Fecha Certificado de Cobertura:</th>
					<td><asf:calendar
							value="${process.seguro.fechaCertifCoberturaStr}"
							property="ProcessForm.process.seguro.fechaCertifCoberturaStr"
							attribs="class='fechaCertifCoberturaStr'" /></td>
				</tr>
				<tr>
					<th>Vencimiento Certificado de Cobertura:</th>
					<td><asf:calendar
							value="${process.seguro.vencCertifCoberturaStr}"
							property="ProcessForm.process.seguro.vencCertifCoberturaStr"
							attribs="class='vencCertifCoberturaStr'" /></td>
				</tr>
				<tr>
					<th>N�mero de Poliza:</th>
					<td><html:text name="ProcessForm"
							property="process.seguro.nroPoliza"
							value="${ProcessForm.process.seguro.nroPoliza}" /></td>
				</tr>
				<tr>
					<th>Aseguradora:</th>
					<td><html:text name="ProcessForm"
							property="process.seguro.nombreAseguradora"
							value="${ProcessForm.process.seguro.nombreAseguradora}" /></td>
				</tr>
				<tr>
					<th>Riesgo Cubierto:</th>
					<td><html:text name="ProcessForm"
							property="process.seguro.riesgoCubierto"
							value="${ProcessForm.process.seguro.riesgoCubierto}" /></td>
				</tr>
				<tr>
					<th>Monto:</th>
					<td><html:text name="ProcessForm"
							property="process.seguro.montoStr"
							value="${ProcessForm.process.seguro.montoStr}" /></td>
				</tr>
				<tr>
					<th>Fecha de Vencimiento:</th>
					<td><asf:calendar
							property="ProcessForm.process.seguro.fechaVencimientoStr"
							attribs="class='fechaVencimientoStr'" /></td>
				</tr>
				<tr>
					<th>Ubicacion:</th>
					<td><html:text name="ProcessForm"
							property="process.seguro.ubicacion">
							<jsp:attribute name="value">
						 
						 		${ProcessForm.process.seguro.ubicacion}
						
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Numero de Orden:</th>
					<td><html:text name="ProcessForm"
							property="process.seguro.nroOrden">
							<jsp:attribute name="value">
						 
						 		${ProcessForm.process.seguro.nroOrden}
						
						</jsp:attribute>
						</html:text></td>
				</tr>
				<tr>
					<th>Observaciones:</th>
					<td><html:textarea name="ProcessForm"
							property="process.seguro.observaciones"
							value="${ProcessForm.process.seguro.observaciones}">
						</html:textarea></td>
				</tr>
				<logic:notEmpty name="ProcessForm" property="process.estadoSeguro">
					<tr>
						<th>Estado actual:</th>
						<td>${ProcessForm.process.estadoSeguro.fechaStr}-
							${ProcessForm.process.estadoSeguro.estadoStr}<br />
							${ProcessForm.process.estadoSeguro.observaciones}
						</td>
					</tr>
				</logic:notEmpty>
				<logic:empty name="ProcessForm" property="process.estadoSeguro">
					<tr>
						<td colspan="2">No hay estado actual del seguro</td>
					</tr>
				</logic:empty>
				<tr>
					<th colspan="2"><input type="button"
						value="Cambiar Estado Seguro" onclick="cambiarEstadoSeguro();" />
						<input type="button" value="Historial de Estados"
						onclick="historialEstadosSeguro();" /></th>
				</tr>
			</table>
		</div>
	</logic:equal>
	<logic:equal name="ProcessForm" property="process.garantiaCustodia"
		value="true">
		<div>
			<table border="0" width="60%" align="left">
				<tr>
					<td style="text-transform: capitalize" colspan="2"><b>
							Datos de la Custodia </b></td>
				</tr>
				<logic:notEmpty name="ProcessForm"
					property="process.custodiaActual.objetoiGarantia">
					<tr>
						<th>Orden</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.orden" readonly="true" /></td>
					</tr>
					<tr>
						<th>Nro de Inventario</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.nroInventario" readonly="true" />
						</td>
					</tr>
					<tr>
						<th>Sector depositario</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.sector.nombre" readonly="true"
								size="50" /></td>
					</tr>
					<tr>
						<th>Responsable Custodia</th>
						<td><logic:notEmpty name="ProcessForm"
								property="process.custodiaActual.responsable">
								<html:text name="ProcessForm"
									property="process.custodiaActual.responsable.nomb12"
									readonly="true" size="50" />
							</logic:notEmpty></td>
					</tr>
					<tr>
						<th>Fecha de Ingreso</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.fechaIngresoStr"
								readonly="true" styleClass="fechaIngresoStr" /></td>
					</tr>
					<tr>
						<th>Ubicaci&oacute;n F&iacute;sica</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.ubicacion" readonly="true"
								size="50" /></td>
					</tr>
					<tr>
						<th>Orden F&iacute;sico</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.ordenFisico" readonly="true"
								size="50" /></td>
					</tr>
					<tr>
						<th>Fecha L&iacute;mite Pr&eacute;stamo Instrumento</th>
						<td><html:text name="ProcessForm"
								property="process.custodiaActual.fechaDevolucionStr"
								readonly="true" styleClass="fechaDevolucionStr" /></td>
					</tr>
					<tr>
						<th>Observaciones:</th>
						<td><html:textarea name="ProcessForm"
								property="process.custodiaActual.observaciones" cols="40"
								readonly="true"
								value="${ProcessForm.process.custodiaActual.observaciones}" />
						</td>
					</tr>
				</logic:notEmpty>
				<tr>
					<th colspan="2"><logic:empty name="ProcessForm"
							property="process.custodiaActual.objetoiGarantia">
							<logic:notEmpty name="ProcessForm"
								property="process.oGarantia.id">
								<input type="button" value="Alta en Custodia"
									onclick="nuevaCustodia();" />
							</logic:notEmpty>
						</logic:empty> <logic:greaterThan value="0" name="ProcessForm"
							property="process.custodiaActual.orden">
							<input type="button" value="Traslados" onclick="nuevaCustodia();" />
							<input type="button" value="Ver hist�rico de movimientos"
								onclick="historicoCustodia();" />
						</logic:greaterThan></th>
				</tr>
			</table>
		</div>
	</logic:equal>
	<table border="0" width="41%" align="left" bgcolor="">
		<tr>
			<td><input type="button" value="Guardar" onclick="guardar()" />
			</td>
			<td><input type="button" value="Cancelar" onclick="cancelar();">
			</td>
		</tr>
	</table>

	<script type="text/javascript">
		/* Control sobre cambio del Tipo de Producto. Ticket 9161*/
		var classSelectTipoProducto = function(){
				var self = this;
				var select = document.getElementById("process.tipoProducto");
				var idGarantia = document.getElementById("idGarantia");
				var prevIndex = select.selectedIndex;
				var prevValue = select.value;
				var previous = select.options[prevIndex].text;
		 		
				self.onchange = function(){
					if(!idGarantia.value)
						return;
					if (prevIndex != select.selectedIndex) {
			       		var conf = confirm("Ha modificado el Tipo de Producto original. Si guarda los cambios, se liberar� el producto: " + previous);
			       		if (conf == false) {
			       			select.value = prevValue;   
			 	       }
					}
		 	    }
				
				self.select = select;
		};
		
		var tipoProductoInstance = new classSelectTipoProducto();
		tipoProductoInstance.select.addEventListener("change", tipoProductoInstance.onchange);
	</script>

</html:form>

<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>

<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('formulario');
	var idTipo = $('process.idTipo');
	ocultar('asiento');
	
	function seleccionTipoGarantia() {
		accion.value = 'SELECCIONAR_TIPO';
		formulario.submit();
	}

	function guardar() {
		accion.value = 'GUARDAR_GARANTIA';
		formulario.submit();
		
	}

	function calcularTotalTasacion() {
		var valor1 = parseFloat($('valorTerreno').value);
		var valor2 = parseFloat($('valorMejoras').value);
		var valor3 = parseFloat($('valorCultivos').value);
		var valor4 = parseFloat($('valorRiego').value);
		var valor5 = parseFloat($('valorOtros').value);

		var total = valor1+valor2+valor3+valor4+valor5;

		$('valorTotalTasacion').value = total;
	}

	function cancelar() {
		var idSolicitud = $('idSolicitud').value;
		self.location = "${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.idSolicitud="+idSolicitud;
	}
	function nuevaCustodia() {
		self.location = "${pageContext.request.contextPath}/actions/process.do?processName=GarantiaCustodiaProcess&do=process&process.id=${ProcessForm.process.oGarantia.id}&process.action=nuevo";
	}
	function historicoCustodia() {
		self.location = "${pageContext.request.contextPath}/actions/process.do?processName=GarantiaCustodiaProcess&do=process&process.id=${ProcessForm.process.oGarantia.id}&process.action=listar";
	}
	function comprobar() {
		if (${ProcessForm.process.modifica} == false) {
			accion.value = 'COMPROBAR';
			formulario.submit();
		}
	}
	
	function verificarPorcentaje(porcentaje){
	       var tasa = porcentaje.value.replace(',', '.').replace('%', '');
	       if(tasa < 0 || tasa > 100){
	               alert("Debe ingresar un valor entre 0 y 100");
	               porcentaje.value = '0%';
	       }else{
	               porcentaje.value = porcentaje.value.concat('%');
	       }
	}
	function seleccion(){
		var per = document.getElementById('periodo');
		if(per.value=='1'){
			ocultar('matricula');
			mostrar('asiento');
			per.value='2';
		}else{
			ocultar('asiento');
			mostrar('matricula');
		}
	}
	function verificarCambioTasa() {
		var tipoProd = $('process.tipoProducto').value;
		var idObjetoiGarantia = $('oGarantiaId').value;
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=verificarCambioTasa";
		url += "&idObjetoiGarantia="+idObjetoiGarantia;
		url += "&tipoProd="+tipoProd;
		retrieveURL(url, verificarCambioTasaResponse);
	}
	function verificarCambioTasaResponse(text) {
		if (text == 'true') {
			alert('El cambio de producto implica un cambio en la tasa.');
		}
	}

	function calcularFechaVencimientoGarantia() {
		var insc = $('process.tasacion.gravFechaReinscripcionStr').value;
		var idTipo = $('process.idTipo').value;
		
		if(insc == ''){
			insc =  $('process.tasacion.gravFechaInscripcionStr').value;
		}
		
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularFechaVencimientoGarantia";
		url += "&fi="+insc;
		url += "&t="+idTipo;
		retrieveURL(url, calcularFechaVencimientoGarantiaResponse);
	}

	function calcularFechaVencimientoGarantiaResponse(text) {
		var venc = $('process.tasacion.gravFechaVencGarantiaStr');
		venc.value = text;
	}

	function cambiarEstadoSeguro() {
	  	var idObjetoiGarantia = $('oGarantiaId').value;
	  	var url = "${pageContext.request.contextPath}/actions/process.do?processName=GarantiaSeguroEstadoProcess&do=process&process.action=nuevo&process.idObjetoiGarantia="+idObjetoiGarantia;
	  	self.location = url;
	}

	function historialEstadosSeguro() {
		var idObjetoiGarantia = $('oGarantiaId').value;
		var url = "${pageContext.request.contextPath}/actions/process.do?processName=GarantiaSeguroEstadoProcess&do=process&process.action=listar&process.idObjetoiGarantia="+idObjetoiGarantia;
		self.location = url;
	}
	
	
</script>