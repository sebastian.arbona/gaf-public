<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div><html:errors /></div>

<div class="title">Reporte movimiento judicial</div>
<p>

	<html:form action="/actions/process.do?do=process&processName=ReporteMovimientoJudicial" styleId="ProcessForm">
			<html:hidden property="process.action" value="buscar" styleId="action"/>
			<html:hidden property="process.cid"/>	
	
			<div title="Reporte movimiento judicial">
			
			<table style="border: 2px solid rgb(204, 204, 204);" width="352"
				height="30">
				<tr>
					<th>Abogados:</th>
					<td><asf:select name="process.codAbogado"
							entityName="com.civitas.hibernate.persona.Persona"
							listCaption="getNomb12" listValues="getId"
							filter="id in (select e.persona.id from com.nirven.creditos.hibernate.EspecialidadPersona e where e.especialidad.id = 6)"
							value="${ProcessForm.process.codAbogado}" nullValue="true"
							nullText="Todos los Abogados"/></td>
				</tr>			
				
				<tr>
					<th>Fecha Desde</th>
					<td>
						<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
					</td>
				</tr>
				
				<tr>
					<th>Fecha Hasta</th>
					<td>
						<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
					</td>
				</tr>
				
				<tr>
					<th>D�as Per�odo Anterior:</th>
					<td>
						<html:text property="process.diasPeriodoAnterior" styleId="diasPeriodoAnterior"></html:text>
					</td>	
				</tr>				

				<tr>
			      <th colspan="2">
				   		<html:submit value="Listado en pantalla"></html:submit>
			      </th>
	            </tr>
				
			</table>
			</div>
			<br />
			<br />
			<div class="grilla">
				<display:table name="ProcessForm" property="process.beanreportemovimientojudicial"
					export="true" id="reportTable" pagesize="50"
					requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=verNotif">
					<display:column property="credito.persona.nomb12" title="Tomador"
					sortable="true" />
					
					<display:column media="excel xml pdf"
						property="credito.numeroAtencion" title="Nro Proyecto" />
					<display:column title="Proyecto" media="html">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.credito.id}&process.action=buscar&process.personaId=">${reportTable.credito.numeroAtencion}</a>
					</display:column>
					<display:column property="expediente" title="Expediente"
						sortable="true" />
												
					<display:column property="fechainicio" title="Fecha Inicio" decorator="com.asf.displayDecorators.DateDecorator"
						sortable="true" />						

					<display:column property="autos" title="N� Autos"
						sortable="true" />								
						
					<display:column property="caratula" title="Car�tula"
						sortable="true" />						

					<display:column property="tipotribunal" title="Tipo de Tribunal"
						sortable="true" />
																		
					<display:column property="nrotribunal" title="Nro Tribunal"
						sortable="true" />						
																							
					<display:column property="saldo" title="Saldo" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" 
						sortable="true" />
						
					<display:column property="fechamovim" title="Fecha Movimiento" decorator="com.asf.displayDecorators.DateDecorator"
						sortable="true" />
						
					<display:column property="ultimomov" title="�ltimo Movimiento"
						sortable="true" />
						
					<display:column property="fechamovimanterior" title="Fecha Movimiento Anterior" decorator="com.asf.displayDecorators.DateDecorator"
						sortable="true" />						
											
					<display:column property="movanterior" title="Movimiento Anterior"
						sortable="true" />						

                    <display:column property="abogado" title="Abogado"
						sortable="true" />
																							
					<display:column property="conmovimiento" title="Con Movimiento" decorator="com.asf.displayDecorators.BooleanDecorator"
						sortable="true" />
						
					<display:column property="deudaTotal" title="Deuda Exigible"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
						
				</display:table>
			</div>
			
	
	</html:form>
	
	<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>


	