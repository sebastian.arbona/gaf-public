<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title"><b>Zona Geogr�fica</b></div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
    <table border="0">
        <tr><th>C�digo:</th><td><html:text property="entity.id" readonly="true"/><html:errors property="id" /></td></tr>
        <tr><th>Zona Geogr�fica:</th><td><html:text property="entity.deta05" maxlength="40"/><html:errors property="deta05" /></td></tr>
        <tr><th>Reducci�n:</th><td><asf:text type="decimal" name="AbmForm" property="entity.porc05Str" maxlength="6" /></td></tr>
        <tr><th>Provincia:</th><td>
                <asf:select name="entity.codi08" entityName="com.civitas.hibernate.persona.Provin" listCaption="getDeta08" listValues="getId" value="${AbmForm.entity.codi08}" filter="id LIKE '%' ORDER BY deta08"/>
                <br/>
            </td></tr>

    </table>
    <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
        <html:submit>Guardar</html:submit>
    </asf:security>
    <html:cancel>Cancelar</html:cancel>
</html:form>