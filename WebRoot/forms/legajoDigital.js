/* 
 * Plantilla CRUD DataTable Editor con Tablas asociadas
 * CIVITAS SA
 * Versión2.0 - mPalma
 */

// Variables Globales
var dTable_legajoDigital = null;
var dt_personalizacion_legajoDigital = {
        ajax: {
            method: "GET",
            url: "actions/LegajoDigitalAction.do?do=getAll",
            data: null,
            dataType : 'json'
        },
		"columns":[
			{ "title": "Id",						"data": "id", "className": "bold"},
		    { "title": "N&uacute;mero de Legajo",	"data": "numero" },
		    { "title": "Nombre y Apellido",			"data": "nombre" },
		    { "title": "DNI",						"data": "dni" },
		    { "title": "CUIL",						"data": "cuil" },
		],
		"columnDefs":[
		    {
		    	"title": "Acciones",
		        "render": function ( data, type, row, meta ) {
		        	var acciones = [];
		        	acciones.push( ["consultar","dt_details_LegajoDigital("+row.id+")"] );
		        	acciones.push( ["eliminar","dt_delete_LegajoDigital("+row.id+")"] );
		        	return acciones_dataTable(acciones);
		        },        
		        "className": "",
		        "visible": true,
		        "searchable": false,
		        "targets": 5
		    }
		],
		"buttons": [            
		    {
		        extend: 'excel',
		        title: 'Imputaciones',
		        text: 'Imprimir XLS',
		        exportOptions: {
		        	columns: [ 0,1,2,3,4]
		        }
		    },
		    {
		        extend: 'pdfHtml5',
		        title: 'Imputaciones',
		        text: 'Imprimir PDF',
		        pageSize: 'A4',
		        exportOptions: {
		            columns: [ 0,1,2,3,4]
		        }                 
		    }
		]
	};

// CUERPO PRINCIPAL
$j(document).ready(function() {

	dTable_legajoDigital = inicializarDatatable( "dt-legajoDigital", 1, null, dt_personalizacion_legajoDigital );
    $j('#addElement').click(function(e) {    	
    	e.preventDefault();
    	$j("#table-container").hide();
    	$j('#form-container').load(pathSistema + '/forms/legajoDigital.jsp', function(){
    		$j("#form-container").fadeIn();
    	});
    });
    
    $j('#dataTable tbody').on( 'click', '.dtConsultar', function () {
    	var data = dTable_legajoDigital.row( $j(this).parents('div').parents('tr') ).data();        
    	$j("#table-container").hide();
    	$j('#form-container').load(pathSistema + '/forms/legajoDigitalDetails.jsp?idLegajoDigital=' + data.id, function(){
    		$j("#form-container").fadeIn();
    	});
    } );
    
    $j('#dataTable tbody').on('click', '.dtEliminar', function(){
    	
    	var data = dTable.row( $j(this).parents('div').parents('tr') ).data();
    	crud.deleteL({
    		url: pathSistema + "/actions/LegajoDigitalAction.do?do=delete",
    		id: data.id,
    		dTable: dTable_legajoDigital
    	});
    	
    });
});

function dt_delete_LegajoDigital( idElement ) {
	crud.deleteL({
		url: pathSistema + "/actions/LegajoDigitalAction.do?do=delete",
		id: idElement,
		html: "&iquest;Desea eliminar este Legajo Digital?",
	});
}

function dt_details_LegajoDigital( idElement ){     
	$j("#table-container").hide();
	$j('#form-container').load(pathSistema + '/forms/legajoDigitalDetails.jsp?idLegajoDigital=' + idElement, function(){
		$j("#form-container").fadeIn();
	});
}

function back_Details_Principal(){
	dTable_legajoDigital.ajax.reload(); 
	$j("#form-container").hide();
	$j("#table-container").fadeIn();
}
