<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
<div class="title">
	Escrito
</div>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}">
	<html:hidden property="process.action" value="save" />
	<table border="0">
		<tr>
			<th width="40%">
				C�digo:
			</th>
			<td>
				<html:text property="process.documento.id" readonly="true" />
			</td>
		</tr>
		<tr>
			<th>
				Fecha:
			</th>
			<td>
				<html:text property="process.documento.fechaStr" readonly="true" />
			</td>

		</tr>
		<tr>
			<th>
				Identificacion:
			</th>
			<td>
				<html:text property="process.documento.numero" readonly="true" />
			</td>
		</tr>
		<tr>
			<th>
				Detalle:
			</th>
			<td>
				<fck:editor instanceName="process.documento.archivoStr" width="750"
					height="300" value='${process.documento.archivoStr}' 
					readonly="true"	/>
			</td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.print" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>