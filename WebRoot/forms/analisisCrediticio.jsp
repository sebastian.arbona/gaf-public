<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<link rel="stylesheet" href="/CREDITOS/style/frames.css" type="text/css">
<div style="font-size: 20px; margin: 15px" align="left">
	<html:errors />
</div>
<div class="title">
	An�lisis Crediticio
</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.id" styleId="solicitud" />
	<html:hidden name="ProcessForm" property="process.action" styleId="accion" />
	<%--  Ticket 9165 --%>
	<html:hidden property="process.credito.id" value="${ProcessForm.process.credito.id}"/>

	<table border="0" height="200">
		 <tr>
		 	<th>
    	 		 Esperando Requisitos?
    	 	</th>
    		<td>
    			<html:radio name="ProcessForm"  property="process.esperandoRequisitos" value="true">S�</html:radio>
    			&nbsp;&nbsp;
    			<html:radio name="ProcessForm"  property="process.esperandoRequisitos" value="false">No</html:radio>
    		</td>
    	</tr>
		<tr>
			<th>
				Se han verificado inhibiciones en la fecha:
			</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaInhibicion"
					maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td>
				<html:textarea property="process.observacionInhibicion"
					name="ProcessForm" cols="40" rows="3" />
			</td>
		</tr>

		<tr>
			<th>
				Se han verificado deudas:
			</th>
			<td>
			<asf:calendar property="ProcessForm.process.fechaVerificacion"
					maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td>
				<html:textarea property="process.observacionVerificacion"
					name="ProcessForm" cols="40" rows="3" />
			</td>
		</tr>

		<tr>
			<th>
				Informe de Asesoria Letrada:
			</th>
			<td>


				<asf:calendar property="ProcessForm.process.fechaAsesoriaLetrada"
					maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td>
				<html:textarea property="process.observacionAsesoriaLetrada"
					name="ProcessForm" cols="40" rows="3" />
			</td>
		</tr>
		<tr>
			<th>
				Se ha verificado el Analisis Patrimonial en la fecha:
			</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaAnalisisPatrimonial"
					maxlength="10" />
			</td>
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td>
				<html:textarea property="process.observacionAnalisisPatrimonial"
					name="ProcessForm" cols="40" rows="3" />
			</td>
		</tr>
	</table>
	
	<table>	
		<tr>
			<td style="text-transform: capitalize" colspan="4" align="center">
				<b> Vitivin�cola </b>
			</td>
		</tr>
		<tr>
			<th>QQ Solicitados</th>
			<td><asf:text name="ProcessForm" property="process.credito.qqsolicitadoStr" type="decimal" maxlength="15" readonly="true"/></td>
			<th>
	          �Pertenece a Fecovita?
	        </th>
	        <td>	           
	          <asf:select name="process.fecovita"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.fecovita}"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria = 'credito.fecovita' order by codigo" />
	        </td> 
		</tr> 		
		<tr>
    		<th>QQ del Contrato</th>
    		<td><asf:text name="ProcessForm" property="process.credito.qqFinalStr" type="decimal" maxlength="15" readonly="true"/></td>
    		<th>Destino</th>
    		<td><html:text property="process.credito.destinoStr" name="ProcessForm" size="30" disabled="true"/></td>
    	</tr> 
    	<tr>
    		<th>Volumen Inmovilizado</th>
    		<td><asf:text maxlength="100" property="process.volumenInmovilizado" type="text" readonly="true" name="ProcessForm"></asf:text></td>
    		<th>Fecha Inmovilizaci�n</th>
    		<td><asf:text maxlength="10" property="process.fechaInmovilizacion" type="text" readonly="true" name="ProcessForm"></asf:text></td>
    	</tr>
    	<tr>
       		<th>Volumen Liberado</th>
    		<td><asf:text maxlength="100" property="process.volumenLiberado" type="text" readonly="true" name="ProcessForm"></asf:text></td> 	
    		<th>�ltima Liberaci�n</th>
    		<td><asf:text maxlength="10" property="process.fechaUltimaLiberacion" type="text" readonly="true" name="ProcessForm"></asf:text></td>
    	</tr>
    	<tr>
    		<th>Tipo de Producto Inmovilizado</th>
    		<td><asf:text maxlength="100" property="process.tipoProductoGarantia" type="text" readonly="true" name="ProcessForm"></asf:text></td>
    	</tr>
    </table>	
    	
    <display:table name="ProcessForm" property="process.credito" id="credito">
    	<tr>
    		<th>QQ Ingresados a la Bodega</th>
			<th>QQ Ingresados en otra Bodega</th>
			<th>Total QQ Ingresados</th>
		</tr>
		<tr>
			<display:column title="QQ Ingresados a la Bodega">
				<div style="float: right">
					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosGarantia}"/>
				</div>
			</display:column>
			<display:column title="QQ Ingresados en otra Bodega">
				<div style="float: right">
					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosExternos}"/>
				</div>
			</display:column>
			<display:column title="Total QQ Ingresados">
				<div style="float: right">
					<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2" value="${credito.qqIngresadosTotales}"/>
				</div>
			</display:column>
			<display:column title="">
				<div style="float: center">
			    	<input type="submit" value="Actualizar INV" onclick="actualizarInv();">
				</div>
			</display:column>			    	
		</tr>
	</display:table>

	<input type="submit" value="Guardar" onclick="guardar();">
	<input type="submit" value="Cancelar" onclick="cancelar();">
	<input type="submit" value="Reporte qq cosechados" onclick="reporte();" >
	

</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script type="text/javascript">
function guardar() {
	var action = document.getElementById("accion");
	var formulario = document.getElementById("oForm");
	action.value = "guardar";
}
function reporte() {
	var action = document.getElementById("accion");
	var formulario = document.getElementById("oForm");
	action.value = "reporte";
}
function cancelar() {
	var action = document.getElementById("accion");
	var formulario = document.getElementById("oForm");
	action.value = "";
}
function actualizarInv(){
	var action = document.getElementById("accion");
	var formulario = document.getElementById("oForm");
	action.value = "actualizarInv";
}
</script>
