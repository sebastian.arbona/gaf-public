<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title" style="margin-bottom: 15px">Composici&oacute;n
	de Cr&eacute;dito</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br />
<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
	value="true">
	<div style="width: 70%" align="right">
		<a
			href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoCuentaCorriente&process.action=imprimirInforme&process.idObjetoi=${ProcessForm.process.idObjetoi}">
			Informe General </a><br /> <a href="javascript:void(0);"
			onclick="imprimirCtaCte();">Informe Composici&oacute;n de Cr&eacute;dito</a>
	</div>
</logic:notEqual>
<html:form
	action="/actions/process.do?do=process&processName=CreditoCuentaCorriente"
	styleId="oForm">

	<html:hidden name="ProcessForm" property="process.idObjetoi" />
	<html:hidden name="ProcessForm" property="process.personaId" />
	<html:hidden property="process.action" styleId="action" />
	<html:hidden name="ProcessForm" property="process.boleto.id"
		styleId="idBoleto" />

	<table style="margin-bottom: 20px">
		<tr>
			<th>Hasta fecha:</th>
			<td><asf:calendar property="process.hastaFechaStr"
					value="${ProcessForm.process.hastaFechaStr}" /></td>
		</tr>
	</table>


	<html:submit onclick="buscar();">Buscar</html:submit>
	<html:submit onclick="calcularIntereses();">Calcular</html:submit>
	<asf:security action="/actions/process.do"
		access="processName=CreditoCuentaCorriente&process.action=crearCuota&process.idObjetoi=1">
		<logic:equal name="ProcessForm" property="process.ocultarCampos"
			value="true">
			<input type="button" onclick="irACrearCuota();"
				value="Modificar cuota" disabled="disabled" />
		</logic:equal>
		<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
			value="true">
			<input type="button" onclick="irACrearCuota();"
				value="Modificar cuota" />
		</logic:notEqual>
	</asf:security>

	<br />
	<br />
	<div class="grilla">
		<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
			value="true">
			<display:table name="ProcessForm" property="process.beans"
				id="reportTable" export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Credito Nro: ${ProcessForm.process.objetoi.numeroAtencion} - Hasta fecha: ${ProcessForm.process.hastaFechaStr}</display:caption>
				<display:setProperty name="report.title" value="Cuenta Corriente"></display:setProperty>
				<display:column title="Cuota" media="html">
					<logic:notEqual value="0" name="reportTable" property="numero">
	    		${reportTable.numero}
	    	</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="font-size: 14">Total</div>
					</logic:equal>
				</display:column>
				<display:column title="Cuota" media="excel xml pdf">
					<logic:notEqual value="0" name="reportTable" property="numero">
	    		${reportTable.numero}
	    	</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
	    		Total
	    	</logic:equal>
				</display:column>
				<display:column property="detalle" title="Detalle" />
				<display:column property="fechaStr" title="Fecha" />
				<display:column property="estado" title="Estado" />
				<display:column class="right" title="Capital" media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.capitalStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<logic:equal value="false" name="reportTable"
								property="bonificacion">
								<div style="color: red">${reportTable.capitalStr}</div>
							</logic:equal>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.capitalStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Capital" media="excel xml pdf"
					property="capitalStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<logic:equal name="ProcessForm" property="process.cer" value="true">
					<display:column class="right" title="CER" media="html"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
						<logic:notEqual value="0" name="reportTable" property="numero">
							<logic:equal value="true" name="reportTable" property="cuota">
				   			${reportTable.cerStr}
				    	</logic:equal>
							<logic:equal value="false" name="reportTable" property="cuota">
								<logic:equal value="false" name="reportTable"
									property="bonificacion">
									<div style="color: red">${reportTable.cerStr}</div>
								</logic:equal>
							</logic:equal>
						</logic:notEqual>
						<logic:equal value="0" name="reportTable" property="numero">
							<div style="color: blue">${reportTable.cerStr}</div>
						</logic:equal>
					</display:column>
					<display:column class="right" title="CER" media="excel xml pdf"
						property="cerStr"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				</logic:equal>
				<display:column class="right" title="Int. Compens." media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.compensatorioStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<div style="color: red">${reportTable.compensatorioStr}</div>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.compensatorioStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Int. Compens."
					media="excel xml pdf" property="compensatorioStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Int. Moratorio" media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.moratorioStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<logic:equal value="false" name="reportTable"
								property="bonificacion">
								<div style="color: red">${reportTable.moratorioStr}</div>
							</logic:equal>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.moratorioStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Int. Moratorio"
					media="excel xml pdf" property="moratorioStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Int. Punit." media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.punitorioStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<logic:equal value="false" name="reportTable"
								property="bonificacion">
								<div style="color: red">${reportTable.punitorioStr}</div>
							</logic:equal>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.punitorioStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Int. Punit."
					media="excel xml pdf" property="punitorioStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Multas" media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.multasStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<logic:equal value="false" name="reportTable"
								property="bonificacion">
								<div style="color: red">${reportTable.multasStr}</div>
							</logic:equal>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.multasStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Multas" media="excel xml pdf"
					property="multasStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Gastos" media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.gastosSimplesStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<logic:equal value="false" name="reportTable"
								property="bonificacion">
								<div style="color: red">${reportTable.gastosSimplesStr}</div>
							</logic:equal>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.gastosSimplesStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Gastos" media="excel xml pdf"
					property="gastosStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Gastos Rec" media="html"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal value="true" name="reportTable" property="cuota">
		   			${reportTable.gastosRecStr}
		    	</logic:equal>
						<logic:equal value="false" name="reportTable" property="cuota">
							<logic:equal value="false" name="reportTable"
								property="bonificacion">
								<div style="color: red">${reportTable.gastosRecStr}</div>
							</logic:equal>
						</logic:equal>
					</logic:notEqual>
					<logic:equal value="0" name="reportTable" property="numero">
						<div style="color: blue">${reportTable.gastosRecStr}</div>
					</logic:equal>
				</display:column>
				<display:column class="right" title="Gastos" media="excel xml pdf"
					property="gastosStr"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column class="right" title="Saldo Cuota"
					property="saldoCuotaStr" style="color: blue"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEqual>
	</div>
	<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
		value="true">
		<div id="resumen">
			<table border="0">
				<tr>
					<th>
						<h3>Resumen de saldos al</h3>
					</th>
					<th>${ProcessForm.process.hastaFechaStr}</th>
				</tr>
				<tr>
					<th>Intereses Compensatorios:</th>
					<td align="right">
						${ProcessForm.process.interesCompensatorioStr}</td>
				</tr>
				<tr>
					<th>Intereses Moratorios y punitorios:</th>
					<td align="right">
						${ProcessForm.process.interesMoratoriosPunitoriosStr}</td>
				</tr>
				<tr>
					<th>Bonificaciones:</th>
					<td align="right">${ProcessForm.process.bonificacionesStr}</td>
				</tr>
				<tr>
					<th>Gastos:</th>
					<td align="right">${ProcessForm.process.gastosStr}</td>
				</tr>
				<tr>
					<th>Capital exigible:</th>
					<td align="right">${ProcessForm.process.capitalDevengadoStr}</td>
				</tr>
				<tr>
					<th>Total exigible:</th>
					<td align="right">${ProcessForm.process.totalDevengadoStr}</td>
				</tr>
				<tr>
					<th>Total pagado:</th>
					<td align="right">${ProcessForm.process.totalPagadoStr}</td>
				</tr>
				<tr>
					<th>Total adeudado:</th>
					<td align="right">${ProcessForm.process.totalAdeudadoStr}</td>
				</tr>
			</table>
		</div>
		<br />
	</logic:notEqual>
	<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
		value="true">
		<asf:security action="/actions/process.do"
			access="processName=CreditoCuentaCorriente&process.action=emitir&process.idObjetoi=1">
			<input type="button" value="Emitir Boleto Total Deuda"
				onclick="emitir();">
		</asf:security>
		<logic:equal value="true" property="process.emitir" name="ProcessForm">
			<br />
			<br />
			<td><html:radio name="ProcessForm"
					property="process.deudaParcial" value="true">Deuda Parcial</html:radio>
				&nbsp;&nbsp; <html:radio name="ProcessForm"
					property="process.deudaParcial" value="false">Deuda Total</html:radio>
			</td>
			<br />
			<br />
			<table>
				<tr>
					<th>Leyenda:</th>
					<td><asf:select
							entityName="com.nirven.creditos.hibernate.Leyenda"
							listCaption="nombre" listValues="texto" orderBy="nombre"
							name="process.leyenda" nullValue="true"
							nullText="&lt;&lt;Seleccione Leyenda&gt;&gt;" /></td>
				</tr>
			</table>

			<br />
			<br />
			<input type="button" value="Generar" onclick="generar();">
		</logic:equal>
	</logic:notEqual>
</html:form>
<script type="text/javascript">
	var action = $('action');
	var formulario = $('oForm');
	ocultar("resumen");
	function irACrearCuota() {
		var src = '';

		src += '${pageContext.request.contextPath}/actions/process.do?';
		src += 'do=process&processName=CreditoCuentaCorriente';
		src += '&process.idObjetoi=${ProcessForm.process.idObjetoi}';
		src += '&process.action=crearCuota';
		window.location = src;
	}

	function buscar() {
		accion = document.getElementById("action");
		accion.value = "load";

	}
	function calcularIntereses() {
		accion = document.getElementById("action");
		accion.value = "calcularIntereses";
		/*form = document.getElementById("oForm");
		form.submit();*/
	}
	function emitir() {
		action = $('action');
		formulario = $('oForm');
		action.value = 'emitir';
		formulario.submit();
	}
	function generar() {
		action.value = 'generar';
		formulario.submit();
	}
	function imprimirBoleto(id) {
		var boleto = $('idBoleto');
		action.value = 'imprimirBoleto';
		boleto.value = id;
		formulario.submit();
	}

	function imprimirPago(id) {
		var boleto = $('idBoleto');
		action.value = 'imprimirPago';
		boleto.value = id;
		formulario.submit();
	}
	function imprimirCtaCte() {
		action.value = 'imprimirCtaCte';
		formulario.submit();
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
