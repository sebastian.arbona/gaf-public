<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="ProcessForm">
	<html:hidden property="process.objetoi.id"
		value="${ProcessForm.process.objetoi.id}" />
	<html:hidden property="process.idObjetoi"
		value="${ProcessForm.process.idObjetoi}" />
	<html:hidden property="process.action" styleId="action" value="save" />
	<html:hidden property="process.objetoi.financiamiento" />
	<html:hidden property="process.cantidadCuentasBancarias"
		value="${ProcessForm.process.cantidadCuentasBancarias}"
		styleId="cantidadCuentasBancarias" />
	<html:hidden name="ProcessForm"
		property="process.objetoi.montoGastosCuota" styleId="montoGastosCuota" />
	<html:hidden name="ProcessForm"
		property="process.objetoi.porcentajeGastosCuota"
		styleId="porcentajeGastosCuota" />
	<html:hidden name="ProcessForm"
		property="process.objetoi.linea.importe" styleId="importeGastosLinea" />
	<html:hidden name="ProcessForm"
		property="process.objetoi.linea.porcentaje"
		styleId="porcentajeGastosLinea" />

	<html:errors></html:errors>

	<logic:notEqual name="ProcessForm"
		property="process.estadoActual.nombreEstado" value="ANALISIS">
		<table>
			<tr>
				<div style="width: 100%; text-align: center">
					<asf:security
						action="actions/process.do?processName=VerificacionDatosFinancieros&do=process&process.action=verificar">
						<input type="button" value="Datos Verificados"
							onclick="verificarCBU();" disabled="disabled" />
					</asf:security>
				</div>
			</tr>
			<tr>
				<th>Sistema de Amortizaci&oacute;n</th>
				<td><asf:select name="process.objetoi.tipoAmortizacion"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.objetoi.tipoAmortizacion}"
						filter="categoria='amortizacion.metodo' order by codigo"
						enabled="false" /></td>
			</tr>
			
			<tr>
				<th colspan="2">Inter&eacute;s Compensatorio</th>
			</tr>
			
			<tr>
				
				<th>Tasa Inter&eacute;s Compensatorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaCompensatorioStr" readonly="true"
						type="decimal" maxlength="5" /> <input type="button" value="..."
					onclick="editarIndice(${ProcessForm.process.idCompensatorio});"
					disabled="disabled" /></td>
				
				<th>Primer Vencimiento Capital</th>
				<td><asf:calendar
						property="ProcessForm.process.objetoi.primerVencCapitalStr"
						readOnly="true" /></td>
				
				<th>Cantidad Cuotas Capital</th>
				<td><html:text property="process.objetoi.plazoCapital"
						size="3" readonly="true" /></td>
						
			</tr>
			
			<tr>
				<th>Mas</th>
				<td><html:text property="process.objetoiIndice.valorMasStr"
						value="${ProcessForm.process.objetoiIndice.valorMasStr}" size="15"
						onchange="calcularValorFinal();" styleId="mas"
						readonly="${ProcessForm.process.acuerdoPago}" /></td>
				
				<th>Periodicidad Cuotas Capital</th>
				<td><asf:select name="process.objetoi.frecuenciaCapital"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.objetoi.frecuenciaCapital}"
						filter="categoria='amortizacion.periodicidad' order by codigo"
						enabled="false" /></td>
			</tr>
			
			<tr>
				<th>Por</th>
				<td><asf:text id="por" name="ProcessForm"
						property="process.objetoiIndice.valorPorStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
						
				<th>Primer Vencimiento Inter&eacute;s</th>
				<td><asf:calendar
						property="ProcessForm.process.objetoi.primerVencInteresStr"
						readOnly="true" /></td>
				
				<th>Cantidad Cuotas Inter&eacute;s</th>
				<td><logic:equal name="ProcessForm"
						property="process.ocultarCampos" value="true">
						<html:text property="process.objetoi.plazoCompensatorio" size="3"
							disabled="true" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.ocultarCampos" value="true">
						<html:text property="process.objetoi.plazoCompensatorio" size="3" />
					</logic:notEqual></td>
			</tr>
			
			<tr>
				<th>Tope Tasa:</th>
				<td><asf:text id="topeTasa" name="ProcessForm"
						property="process.objetoiIndice.tasaTopeStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
				
				<th>Periodicidad Cuotas Inter&eacute;s</th>
				<td><asf:select name="process.objetoi.frecuenciaInteres"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.objetoi.frecuenciaInteres}"
						filter="categoria='amortizacion.periodicidad' order by codigo"
						enabled="false" /></td>
							
			</tr>
			
			<tr>
				<th>Bonificaci�n</th>
				<td><html:text name="ProcessForm"
						property="process.bonificacionStr"
						value="${ProcessForm.process.bonificacionStr}" readonly="true"
						maxlength="5" /></td>
				<th></th>
				<td></td>
			</tr>
			<tr>
				<th>Tasa Inter&eacute;s Compensatorio Neta</th>
				<td><html:text name="ProcessForm"
						property="process.tasaNetaStr" readonly="true" maxlength="5" /></td>
				
			</tr>
			
			<tr>
				<th>Tope Tasa Inter&eacute;s Compensatorio</th>
				<td><html:text name="ProcessForm"
						property="process.topeTasaCompensatorioStr" readonly="true"
						maxlength="5" /></td>
				
			</tr>
			
			
			
			<tr>
				<th colspan="2">Inter&eacute;s Moratorio</th>
			</tr>
			
			<tr>
				<th>Tasa Inter&eacute;s Moratorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaMoratorioStr" readonly="true" type="decimal"
						maxlength="5" /> <input type="button" value="..."
					onclick="editarIndice(${ProcessForm.process.idMoratorio});"
					disabled="disabled" /></td>
			</tr>

			<tr>
				<th>Mas</th>
				<td><html:text property="process.objetoiIndice.valorMasStr"
						value="${ProcessForm.process.objetoiIndice.valorMasStr}" size="15"
						onchange="calcularValorFinal();" styleId="mas"
						readonly="${ProcessForm.process.acuerdoPago}" /></td>
				<th>Por</th>
				<td><asf:text id="por" name="ProcessForm"
						property="process.objetoiIndice.valorPorStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
			</tr>
			

			<tr>
				<th>Tope Tasa Inter&eacute;s Moratorio</th>
				<td><html:text name="ProcessForm"
						property="process.topeTasaMoratorioStr" readonly="true"
						maxlength="5" /></td>
				<th></th>
				<td></td>
			</tr>

			<tr>
				<th>Mas</th>
				<td><html:text property="process.objetoiIndice.valorMasStr"
						value="${ProcessForm.process.objetoiIndice.valorMasStr}" size="15"
						onchange="calcularValorFinal();" styleId="mas"
						readonly="${ProcessForm.process.acuerdoPago}" /></td>
				<th>Por</th>
				<td><asf:text id="por" name="ProcessForm"
						property="process.objetoiIndice.valorPorStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
			</tr>
			<tr>

				<th>Tope Tasa:</th>
				<td><asf:text id="topeTasa" name="ProcessForm"
						property="process.objetoiIndice.tasaTopeStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
			</tr>



			<tr>
				<th colspan="2">Inter&eacute;s Punitorio</th>
			</tr>
			
			<tr>
				<th>Tasa Inter&eacute;s Punitorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaPunitorioStr" readonly="true" type="decimal"
						maxlength="5" /> <input type="button" value="..."
					onclick="editarIndice(${ProcessForm.process.idPunitorio});"
					disabled="disabled" /></td>
				
			</tr>

			<tr>
				<th>Mas</th>
				<td><html:text property="process.objetoiIndice.valorMasStr"
						value="${ProcessForm.process.objetoiIndice.valorMasStr}" size="15"
						onchange="calcularValorFinal();" styleId="mas"
						readonly="${ProcessForm.process.acuerdoPago}" /></td>
				<th>Por</th>
				<td><asf:text id="por" name="ProcessForm"
						property="process.objetoiIndice.valorPorStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
			</tr>
			<tr>

				<th>Tope Tasa:</th>
				<td><asf:text id="topeTasa" name="ProcessForm"
						property="process.objetoiIndice.tasaTopeStr"
						attribs='onchange="calcularValorFinal();"' type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /></td>
			</tr>

			<tr>
				<th>Tope Tasa Inter&eacute;s Punitorio</th>
				<td><html:text name="ProcessForm"
						property="process.topeTasaPunitorioStr" readonly="true"
						maxlength="5" /></td>
				<th></th>
				<td></td>
			</tr>
			
			
			<tr>
				<th colspan="2">Datos de Calculos</th>
			</tr>
			
			<tr>
				<th>Porcentaje Gastos Sobre Cuota</th>
				<td><asf:text name="ProcessForm" id="porcentajeGastos"
						property="process.porcentajeGastos" type="decimal" maxlength="5"
						readonly="true" /></td>
				<th>Monto Gastos Sobre Cuota</th>
				<td><asf:text name="ProcessForm" id="montoGastos"
						property="process.importeGastos" type="decimal" maxlength="5"
						readonly="true" /></td>
			</tr>
			<tr>
				<th>Tomar Gastos de la Linea:</th>
				<td><html:radio styleId="calculaGastosLocal"
						property="process.calcularGastosLocal" value="true"
						onclick="tomarGastosLinea();" />Calcular localmente <html:radio
						styleId="calculaGastosLinea"
						property="process.calcularGastosLocal" value="false"
						onclick="tomarGastosLinea();" />Calcular de la l&iacute;nea</td>
				<th>Moneda</th>
				<td><html:text
						property="process.objetoi.linea.moneda.abreviatura"
						value="${ProcessForm.process.objetoi.linea.moneda.abreviatura}"
						readonly="true" disabled="true" size="30" /></td>
			</tr>

			<tr>
				<th>Categor&iacute;a del Solicitante:</th>
				<td><asf:select name="process.objetoi.categoriaSolicitante"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.objetoi.categoriaSolicitante}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'Categoria.Solicitante' order by codigo"
						nullValue="true" nullText="&lt;&lt;Ninguna&gt;&gt;" attribs="disabled"/></td>
			</tr>

			<tr>
				<th>Aporte F.T. y C.</th>
				<td colspan="3"><asf:text name="ProcessForm" id="aporteAgente"
						property="process.financiamientoSolicitadoStr" type="text"
						maxlength="15" readonly="true" /> <asf:text name="ProcessForm"
						id="porcentajeAgente" readonly="true"
						property="process.objetoi.porcentajeAgenteFinanciero"
						type="decimal" maxlength="5" /></td>
			</tr>
			<tr>
				<th>Agente Cofinanciador</th>
				<td colspan="3"><asf:selectpop
						name="process.objetoi.cofinanciadorId"
						title="Agente Cofinanciador" columns="C&oacute;digo, Nombre"
						captions="codiBa,detaBa" values="codiBa"
						entityName="com.asf.gaf.hibernate.Bancos"
						filter="financiador = 1  order by codiBa"
						value="${ProcessForm.process.objetoi.cofinanciadorId}" /></td>
			</tr>
			<tr>
				<th>Aporte Agente Cofinanciador</th>
				<td colspan="3"><asf:text name="ProcessForm"
						id="aporteCofinanciador"
						property="process.objetoi.aporteCofinanciador" type="decimal"
						maxlength="15" readonly="false" /> <asf:text name="ProcessForm"
						id="porcentajeCofinanciador" readonly="true"
						property="process.objetoi.porcentajeAgenteCofinanciador"
						type="decimal" maxlength="5" /></td>
			</tr>
			<tr>
				<th>Total Financiamiento</th>
				<td colspan="3"><span id="totalFinanciamiento">${ProcessForm.process.objetoi.totalFinanciamiento}</span>
				</td>
			</tr>
			<tr>
				<th>Aporte Propio</th>
				<td colspan="3"><asf:text name="ProcessForm" id="aportePropio"
						property="process.objetoi.aportePropio" type="decimal"
						maxlength="15" readonly="true" /> <asf:text name="ProcessForm"
						id="porcentajeAportePropio" readonly="true"
						property="process.objetoi.porcentajeAportePropio" type="decimal"
						maxlength="5" /></td>
			</tr>
			<tr>
				<th>Otros Aportes</th>
				<td colspan="3"><asf:text name="ProcessForm" id="otrosAportes"
						property="process.objetoi.otrosAportes" type="decimal"
						maxlength="15" readonly="true" /> <asf:text name="ProcessForm"
						id="porcentajeOtrosAportes" readonly="true"
						property="process.objetoi.porcentajeOtrosAportes" type="decimal"
						maxlength="5" /></td>
			</tr>
			<tr>
				<th>Total Aportes</th>
				<td colspan="3"><span id="totalAportes">${ProcessForm.process.objetoi.totalAportes}</span>
				</td>
			</tr>
		</table>
		<div style="width: 100%; text-align: center">
			<html:submit value="Guardar" disabled="true"></html:submit>
			<input type="button" value="Cancelar" onclick="cancelar();"
				disabled="disabled" /> <input type="button"
				value="Cargar Datos Financieros" onclick="generarTasa();"
				readonly="${ProcessForm.process.acuerdoPago}" disabled="disabled" />
			<logic:equal name="ProcessForm"
				property="process.permisoInteresCompensatorio" value="true">

				<select id="idModificarInteres">
					<option value="Compensatorio">Modificar Inter&eacute;s
						Compensatorio</option>
					<option value="Moratorio">Modificar Inter&eacute;s
						Moratorio</option>
					<option value="Punitorio">Modificar Inter&eacute;s
						Punitorio</option>
				</select>
				<input type="button" value="Modificar"
					onclick="editarIndiceDeshabilitado(${ProcessForm.process.idCompensatorio});" />
			</logic:equal>
		</div>
	</logic:notEqual>
	<logic:equal name="ProcessForm"
		property="process.estadoActual.nombreEstado" value="ANALISIS">
		<table>
			<tr>
				<div style="width: 100%; text-align: center">
					<asf:security
						action="actions/process.do?processName=VerificacionDatosFinancieros&do=process&process.action=verificar">
						<input type="button" value="Datos Verificados"
							onclick="verificarCBU();" />
					</asf:security>
				</div>
			</tr>
			<tr>
				<th>Sistema de Amortizaci�n</th>
				<td><asf:select name="process.objetoi.tipoAmortizacion"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.objetoi.tipoAmortizacion}"
						filter="categoria='amortizacion.metodo' order by codigo"
						enabled="true" /></td>
			</tr>
			
			<tr>
				<th colspan="2">Inter&eacute;s Compensatorio</th>
			</tr>

			<tr>
				<th>Tasa Inter&eacute;s Compensatorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaCompensatorioStr" readonly="true"
						type="decimal" maxlength="5" /> <logic:equal value="true"
						name="ProcessForm" property="process.acuerdoPago">
						<input type="button" value="..."
							onclick="editarIndice(${ProcessForm.process.idCompensatorio});"
							disabled="disabled" />
					</logic:equal> <logic:equal value="false" name="ProcessForm"
						property="process.acuerdoPago">
						<input type="button" value="..."
							onclick="editarIndice(${ProcessForm.process.idCompensatorio});" />
					</logic:equal></td>
					<th>Primer Vencimiento Capital</th>
				<td><logic:equal name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<asf:calendar
							property="ProcessForm.process.objetoi.primerVencCapitalStr" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<asf:calendar
							property="ProcessForm.process.objetoi.primerVencCapitalStr"
							readOnly="${ProcessForm.process.acuerdoPago}" />
					</logic:notEqual></td>

				<th>Cantidad Cuotas Capital</th>
				<td><logic:equal name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<html:text property="process.objetoi.plazoCapital" size="3" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<html:text property="process.objetoi.plazoCapital" size="3"
							readonly="${ProcessForm.process.acuerdoPago}" />
					</logic:notEqual></td>
			</tr>
		
			<tr>
				<th>Mas</th>
				<td><input
					value="${ProcessForm.process.objetoiIndice.valorMasStr}" size="15"
					disabled="disabled" /></td>
				<th>Periodicidad Cuotas Capital</th>
				<td><asf:select name="process.objetoi.frecuenciaCapital"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.objetoi.frecuenciaCapital}"
						filter="categoria='amortizacion.periodicidad' order by codigo"
						enabled="${!ProcessForm.process.acuerdoPago}" /></td>
			</tr>

			<tr>
				<th>Por</th>
				<td><input name="ProcessForm"
					value="${ProcessForm.process.objetoiIndice.valorPorStr}"
					maxlength="15" disabled="disabled" /></td>
				
				<th>Primer Vencimiento Inter&eacute;s</th>
				<td><logic:equal name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<asf:calendar
							property="ProcessForm.process.objetoi.primerVencInteresStr" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<asf:calendar
							property="ProcessForm.process.objetoi.primerVencInteresStr"
							readOnly="${ProcessForm.process.acuerdoPago}" />
					</logic:notEqual></td>
				<th>Cantidad Cuotas Inter&eacute;s</th>
				<td><logic:equal name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<html:text property="process.objetoi.plazoCompensatorio" size="3" />
					</logic:equal> <logic:notEqual name="ProcessForm"
						property="process.ocultarCamposPago" value="true">
						<html:text property="process.objetoi.plazoCompensatorio" size="3" />
					</logic:notEqual></td>
					
			</tr>

			<tr>
				<th>Tope Tasa:</th>
				<td><input name="ProcessForm"
					value="${ProcessForm.process.objetoiIndice.tasaTopeStr}"
					maxlength="15" disabled="disabled" /></td>
				<th>Periodicidad Cuotas Inter&eacute;s</th>
				<td><asf:select name="process.objetoi.frecuenciaInteres"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						value="${ProcessForm.process.objetoi.frecuenciaInteres}"
						filter="categoria='amortizacion.periodicidad' order by codigo"
						enabled="${!ProcessForm.process.acuerdoPago}" /></td>
			</tr>

			<tr>
				<th>Bonificaci�n</th>
				<td><html:text name="ProcessForm"
						property="process.bonificacionStr"
						readonly="${ProcessForm.process.acuerdoPago}" maxlength="5" /></td>
			</tr>
			
			<tr>
				<th>Tasa Inter&eacute;s Compensatorio Neta</th>
				<td><html:text name="ProcessForm"
						property="process.tasaNetaStr"
						readonly="${ProcessForm.process.acuerdoPago}" maxlength="5" /></td>
			</tr>
			
			
			<tr>
				<th colspan="2">Inter&eacute;s Moratorio</th>
			</tr>
			<tr>
				<th>Tasa Inter&eacute;s Moratorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaMoratorioStr" readonly="true" type="decimal"
						maxlength="5" /> <logic:equal value="true" name="ProcessForm"
						property="process.acuerdoPago">
						<input type="button" value="..."
							onclick="editarIndice(${ProcessForm.process.idMoratorio});"
							disabled="disabled" />
					</logic:equal> <logic:equal value="false" name="ProcessForm"
						property="process.acuerdoPago">
						<input type="button" value="..."
							onclick="editarIndice(${ProcessForm.process.idMoratorio});" />
					</logic:equal></td>
			</tr>

			<tr>
				<th>Mas</th>
				<td><input value="${ProcessForm.process.valorMas2Str}"
					size="15" disabled="disabled" /></td>
			</tr>

			<tr>
				<th>Por</th>
				<td><input name="ProcessForm"
					value="${ProcessForm.process.valorPor2Str}" maxlength="15"
					disabled="disabled" /></td>
			</tr>
			<tr>
				<th>Tope Tasa:</th>
				<td><input name="ProcessForm"
					value="${ProcessForm.process.tasaTope2Str}" maxlength="15"
					disabled="disabled" /></td>
			</tr>

			<tr>
				<th colspan="2">Inter&eacute;s Punitorio</th>
			</tr>

			<tr>
				<th>Tasa Inter&eacute;s Punitorio</th>
				<td><asf:text name="ProcessForm"
						property="process.tasaPunitorioStr"
						readonly="${ProcessForm.process.acuerdoPago}" type="decimal"
						maxlength="5" /> <logic:equal value="false" name="ProcessForm"
						property="process.acuerdoPago">
						<input type="button" value="..."
							onclick="editarIndice(${ProcessForm.process.idPunitorio});" />
					</logic:equal> <logic:equal value="true" name="ProcessForm"
						property="process.acuerdoPago">
						<input type="button" value="..."
							onclick="editarIndice(${ProcessForm.process.idPunitorio});"
							disabled="disabled" />
					</logic:equal></td>
			</tr>
			<tr>
				<th>Mas</th>
				<td><input value="${ProcessForm.process.valorMas3Str}"
					size="15" disabled="disabled" /></td>
			</tr>
			
			<tr>
			<th>Por</th>
				<td><input name="ProcessForm"
					value="${ProcessForm.process.valorPor3Str}" maxlength="15"
					disabled="disabled" /></td>
			</tr>
			
			<tr>
			<th>Tope Tasa:</th>
				<td><input name="ProcessForm"
					value="${ProcessForm.process.tasaTope3Str}" maxlength="15"
					disabled="disabled" /></td>
			</tr>

			<tr>
				<th colspan="4">Datos de Calculos</th>
			</tr>

			<tr>
				<th>Porcentaje Gastos Sobre Cuota</th>
				<td><asf:text name="ProcessForm" id="porcentajeGastos"
						property="process.porcentajeGastos" type="decimal" maxlength="5"
						readonly="${!ProcessForm.process.acuerdoPago}" /></td>
				<th>Monto Gastos Sobre Cuota</th>
				<td><asf:text name="ProcessForm" id="montoGastos"
						property="process.importeGastos" type="decimal" maxlength="5"
						readonly="${!ProcessForm.process.acuerdoPago}" /></td>
			</tr>
			<tr>
				<th>Tomar Gastos de la Linea:</th>
				<td><html:radio styleId="calculaGastosLocal"
						property="process.calcularGastosLocal" value="true"
						onclick="tomarGastosLinea();" disabled="disabled" />Calcular
					localmente <html:radio styleId="calculaGastosLinea"
						property="process.calcularGastosLocal" value="false"
						onclick="tomarGastosLinea();" disabled="disabled" />Calcular de
					la l&iacute;nea</td>
				<th>Moneda</th>
				<td><html:text
						property="process.objetoi.linea.moneda.abreviatura"
						value="${ProcessForm.process.objetoi.linea.moneda.abreviatura}"
						readonly="true" disabled="true" size="30" /></td>
			</tr>

			<tr>
				<th>Categor&iacute;a del Solicitante:</th>
				<td><asf:select name="process.categoriaSolicitante"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.objetoi.categoriaSolicitante}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'Categoria.Solicitante' order by codigo"
						nullValue="true" nullText="&lt;&lt;Ninguna&gt;&gt;" 
						attribs="onchange='cambioValoresSelect();'"/></td>
			</tr>


			<tr>
				<th>Aporte F.T. y C.</th>
				<td colspan="3"><asf:text name="ProcessForm" id="aporteAgente"
						property="process.financiamientoSolicitadoStr" type="text"
						maxlength="15" readonly="true" /> <asf:text name="ProcessForm"
						id="porcentajeAgente" readonly="true"
						property="process.objetoi.porcentajeAgenteFinanciero"
						type="decimal" maxlength="5" /></td>
			</tr>
			<tr>
				<th>Agente Cofinanciador</th>
				<td colspan="3"><asf:selectpop
						name="process.objetoi.cofinanciadorId"
						title="Agente Cofinanciador" columns="C&oacute;digo, Nombre"
						captions="codiBa,detaBa" values="codiBa"
						entityName="com.asf.gaf.hibernate.Bancos"
						filter="financiador = 1  order by codiBa"
						value="${ProcessForm.process.objetoi.cofinanciadorId}" /></td>
			</tr>
			<tr>
				<th>Aporte Agente Cofinanciador</th>
				<td colspan="3"><asf:text name="ProcessForm"
						id="aporteCofinanciador"
						property="process.objetoi.aporteCofinanciador" type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /> <asf:text
						name="ProcessForm" id="porcentajeCofinanciador" readonly="true"
						property="process.objetoi.porcentajeAgenteCofinanciador"
						type="decimal" maxlength="5" /></td>
			</tr>
			<tr>
				<th>Total Financiamiento</th>
				<td colspan="3"><span id="totalFinanciamiento">${ProcessForm.process.objetoi.totalFinanciamiento}</span>
				</td>
			</tr>
			<tr>
				<th>Aporte Propio</th>
				<td colspan="3"><asf:text name="ProcessForm" id="aportePropio"
						property="process.objetoi.aportePropio" type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /> <asf:text
						name="ProcessForm" id="porcentajeAportePropio" readonly="true"
						property="process.objetoi.porcentajeAportePropio" type="decimal"
						maxlength="5" /></td>
			</tr>
			<tr>
				<th>Otros Aportes</th>
				<td colspan="3"><asf:text name="ProcessForm" id="otrosAportes"
						property="process.objetoi.otrosAportes" type="decimal"
						maxlength="15" readonly="${ProcessForm.process.acuerdoPago}" /> <asf:text
						name="ProcessForm" id="porcentajeOtrosAportes" readonly="true"
						property="process.objetoi.porcentajeOtrosAportes" type="decimal"
						maxlength="5" /></td>
			</tr>
			<tr>
				<th>Total Aportes</th>
				<td colspan="3"><span id="totalAportes">${ProcessForm.process.objetoi.totalAportes}</span>
				</td>
			</tr>
		</table>
		<div style="width: 100%; text-align: center">
			<html:submit value="Guardar"></html:submit>
			<input type="button" value="Cancelar" onclick="cancelar();" /> <input
				type="button" value="Cargar Datos Financieros"
				onclick="generarTasa();"
				readonly="${ProcessForm.process.acuerdoPago}" />
			<logic:equal name="ProcessForm"
				property="process.permisoInteresCompensatorio" value="true">

				<select id="idModificarInteres">
					<option value="Compensatorio">Modificar Inter&eacute;s
						Compensatorio</option>
					<option value="Moratorio">Modificar Inter&eacute;s
						Moratorio</option>
					<option value="Punitorio">Modificar Inter&eacute;s
						Punitorio</option>
				</select>
				<input type="button" value="Modificar"
					onclick="editarIndiceDeshabilitado(${ProcessForm.process.idCompensatorio});" />
			</logic:equal>
		</div>
	</logic:equal>
</html:form>
<iframe width=174 height=220 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
$j = jQuery.noConflict();

$j(document).ready(function() {
	$j("#aporteAgente").change(function() {
		actualizarPorcentajes();
	});
	$j("#aporteCofinanciador").change(function() {
		actualizarPorcentajes();
	});
	$j("#aportePropio").change(function() {
		actualizarPorcentajes();
	});
	$j("#otrosAportes").change(function() {
		actualizarPorcentajes();
	});
	
});

function actualizarPorcentajes() {
	var aporteAgente = parseFloat($j("#aporteAgente").val());
	var aporteCofinanciador = parseFloat($j("#aporteCofinanciador").val());
	var aportePropio = parseFloat($j("#aportePropio").val());
	var otrosAportes = parseFloat($j("#otrosAportes").val());

	var totalAportes = calcularTotalAportes();

	if (totalAportes > 0) {
		$j("#porcentajeAgente").val(
				new Number(aporteAgente / totalAportes * 100).toFixed(2) + "%");
		$j("#porcentajeCofinanciador").val(
				new Number(aporteCofinanciador / totalAportes * 100).toFixed(2)
						+ "%");
		$j("#porcentajeAportePropio").val(
				new Number(aportePropio / totalAportes * 100).toFixed(2) + "%");
		$j("#porcentajeOtrosAportes").val(
				new Number(otrosAportes / totalAportes * 100).toFixed(2) + "%");
	}
	var totalFinanciamiento = calcularTotalFinanciamiento();
	$j("#totalAportes").html(totalAportes);
	$j("#totalFinanciamiento").html(totalFinanciamiento);
}

function calcularTotalFinanciamiento() {
	var aporteAgente = parseFloat($j("#aporteAgente").val());
	var aporteCofinanciador = parseFloat($j("#aporteCofinanciador").val());
	var total = aporteAgente + aporteCofinanciador;
	return total;
}

function calcularTotalAportes() {
	var aporteAgente = parseFloat($j("#aporteAgente").val());
	var aporteCofinanciador = parseFloat($j("#aporteCofinanciador").val());
	var aportePropio = parseFloat($j("#aportePropio").val());
	var otrosAportes = parseFloat($j("#otrosAportes").val());
	var totalAportes = aporteAgente + aporteCofinanciador + aportePropio
			+ otrosAportes;
	return totalAportes;

}
function generarTasa() {
	var action = $('action');
	action.value = "generarTasa";
	var form = $('ProcessForm');
	form.submit();
}
function cancelar() {
	var action = $('action');
	action.value = "load";
	var form = $('ProcessForm');
	form.submit();
}

function verificar() {
  	if (confirm("Se van a verificar los datos. �Desea continuar?")) {  		
		var action = $('action');
		action.value = "verificar";
		var form = $('ProcessForm');
		form.submit();
  	}	
}

function tomarGastosLinea(){
	var montoLinea = $('importeGastosLinea');
	var porcentajeLinea = $('porcentajeGastosLinea');
	var montoCredito = $('montoGastosCuota');
	var porcentajeCredito = $('porcentajeGastosCuota');
	var porcentajeGastos = $('porcentajeGastos');
	var montoGastos = $('montoGastos');
	if(calculaGastosLocal.checked == true){
		porcentajeGastos.value= porcentajeCredito.value;
		montoGastos.value= montoCredito.value;
	}else{
		porcentajeGastos.value= porcentajeLinea.value;
		montoGastos.value= montoLinea.value;
	}
}

function cambioValoresSelect() {
	
	var e = document.getElementById("process.categoriaSolicitante");
	var strUser = e.options[e.selectedIndex].text;
	var idCatSolicitante = strUser.split(' - ');
	
	var url1 = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoDatosFinancieros";
	url1 += "&process.action=saveConfSolicitante";
	url1 += "&process.categoriaSolicitante=" + idCatSolicitante[0];	
	url1 += "&process.idCompensatorio=" + ${ProcessForm.process.idCompensatorio};	
	url1 += "&process.idPunitorio=" + ${ProcessForm.process.idPunitorio};	
	url1 += "&process.idMoratorio=" + ${ProcessForm.process.idMoratorio};	
	url1 += "&process.idObjetoiIndice=" + ${ProcessForm.process.idObjetoi};	
	
	$j.ajax({url : url1,
		success : function(data) {
			location.reload(true);
				}});
}	 

function verificarCBU(){
	var cantidadCuentasBancarias = $ ('cantidadCuentasBancarias');
	if(cantidadCuentasBancarias.value != -1){
		if(cantidadCuentasBancarias.value == 0){
			alert('El titular no posee cuentas de Banco asociadas. Por favor ingrese una cuenta en la Persona, e intente nuevamente.');
			return;
		}
		if(cantidadCuentasBancarias.value == 1){
			if(confirm('El primer desembolso no posee CBU, desea vincular autom&aacute;ticamente el CBU encontrado en la Persona?')){
				if (confirm("Se van a verificar los datos. �Desea continuar?")) {  		
					var action = $('action');
					action.value = "verificar";
					var form = $('ProcessForm');
					form.submit();
			  	}
				return;
			}else{
				return;
			}
		}
		if(cantidadCuentasBancarias.value > 1){
			alert('El titular posee m&aacute;s de una cuenta bancaria asociada. Por favor ingrese al primer Desembolso, seleccione una cuenta e intente nuevamente.');
			return;
		}					
	}
	verificar();
}
	
function editarIndice(idIndice) {
	var url = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoDatosFinancieros";
	url += "&process.action=loadIndice";
	url += "&process.idObjetoiIndice=" + idIndice;	
	window.frameElement.src = url;
}

function editarIndiceDeshabilitado(interes) {
	if ($j('#idModificarInteres').val() == 'Compensatorio') {
		editarIndice(${ProcessForm.process.idCompensatorio});
	} else if ($j('#idModificarInteres').val() == 'Moratorio') {
		editarIndice(${ProcessForm.process.idMoratorio});
	} else if ($j('#idModificarInteres').val() == 'Punitorio') {
		editarIndice(${ProcessForm.process.idPunitorio});
	}
}



</script>
