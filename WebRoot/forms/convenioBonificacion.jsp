<%@ page language="java"%>
<%@page import="com.asf.gaf.hibernate.Ejercicio"%>
<%@page import="com.asf.gaf.hibernate.Tiponomenclador"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%pageContext.setAttribute( "ejercicio", Ejercicio.getEjercicioActual() );%>
<%pageContext.setAttribute( "idTipoNomenclador", Tiponomenclador.IDCOMBINADO);%>	
<%pageContext.setAttribute( "nomenclador1", Tiponomenclador.getNombre( Tiponomenclador.IDCOMBINADO ) );%>
<%pageContext.setAttribute( "nomenclador2", Tiponomenclador.getNombre( Tiponomenclador.IDEXPOSICION ) );%>
<%pageContext.setAttribute( "nomenclador3", Tiponomenclador.getNombre( Tiponomenclador.IDEXPOSICIONGERENCIAL ) );%>
<div class="title">
  Convenio de Bonificaci�n
  
 </div>

<br>
<br>

<html:form
	action="actions/process.do?processName=${param.processName}&do=process" styleId="oForm">
		<html:hidden property="process.accion" name="ProcessForm" value="${ProcessForm.process.accion}" styleId="action"/>
	    <table border="0" width="90%">			
			<tr>
				<th>
					Id:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.id"
						type="long" maxlength="8" readonly="true" />
				</td>
			</tr>
			<tr>
				<th>
					Nombre:
				</th>
				<td>
					<html:text property="process.convenio.nombre"></html:text>
				</td>
			</tr>			
			<tr>
				<th>
					Entidad:
				</th>
				<td>
							<asf:select name="process.idBanco"
										entityName="com.asf.gaf.hibernate.Bancos"
										value="${ProcessForm.process.idBanco}"
										listCaption="detaBa"
										listValues="codiBa"	
										filter="bonificador=1"									
										orderBy="codiBa" />
						</td>
			</tr>
			<tr>
				<th>
					Bonificacion M�xima del Convenio:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxBonConvenio" maxlength="10" type="decimal">									
					</asf:text>
				</td>
			</tr>
			<tr>
				<th>
					Financiamiento M�ximo del Convenio:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxFinanciamientoConvenio" maxlength="10" type="decimal">									
					</asf:text>
				</td>
			</tr>
			<tr>
				<th>
					Dias Mora:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.diasMora"  maxlength="4" type="long">									
					</asf:text>
				</td>
			</tr>
			<tr>
				<th>
					Fecha de Vencimiento Real:
				</th>
				<td>
					<asf:calendar property="ProcessForm.process.convenio.fechaVencimientoRealStr" attribs="class='fechaVencimientoRealStr'"></asf:calendar>
				</td>
			</tr>
			<tr>
				<th>
					Fecha de Vencimiento de Aplicaci�n:
				</th>
				<td>
					<asf:calendar property="ProcessForm.process.convenio.fechaVencimientoAplicacionStr" attribs="class='fechaVencimientoAplicacionStr'"></asf:calendar>
				</td>
			</tr>
			<tr>
				<th>
					Maximo Dias Bonificaci�n:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxDiasBon" maxlength="4" type="long">									
					</asf:text>
				</td>
			</tr>
			<tr>
				<th>
					Bonificaci�n M�x por Cr�dito:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxBonCredito" maxlength="10" type="decimal">													
					</asf:text>
				</td>
			</tr>
			<tr>
				<th>
					Financiamiento M�x por Cr�dito:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxFinanciamientoCredito" maxlength="10" type="decimal">													
					</asf:text>
				</td>
			</tr>	
			<tr>
				<th>
					Bonificaci�n M�x por Persona:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxBonPersona" maxlength="10" type="decimal">													
					</asf:text>
				</td>
			</tr>
			<tr>
				<th>
					Financiamiento M�x por Persona:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.convenio.maxFinanciamientoPersona" maxlength="10" type="decimal">													
					</asf:text>
				</td>
			</tr>	
		</table>
		<div>
			<br>
			
			<a>Configuraci�n Contable</a>
			<table border="0">
				<tr>
			        <th>
			          Cuenta Contable Patrimonio:
			        </th>
			        <td>
			        <asf:selectpop name="process.convenio.codigoCtaContablePatrimonio" title="Cuenta contable"
						columns="Codigo,Denominacion, Codigo Corto"
						captions="codigo,denominacion,codigoCorto" values="codigo"
						entityName="com.asf.gaf.hibernate.Plancta"
						value="${ProcessForm.process.convenio.codigoCtaContablePatrimonio}"
						filter="ejercicio=${ejercicio}"/>
				     </td>
			   	</tr>
			   	<tr>
			        <th>
			          Cuenta Contable Recurso:
			        </th>
			        <td>
			        <asf:selectpop name="process.convenio.codigoCtaContableRecurso" title="Cuenta contable"
						columns="Codigo,Denominacion, Codigo Corto"
						captions="codigo,denominacion,codigoCorto" values="codigo"
						entityName="com.asf.gaf.hibernate.Plancta"
						value="${ProcessForm.process.convenio.codigoCtaContableRecurso}"
						filter="ejercicio=${ejercicio}"/>
				     </td>
			   	</tr>
			</table>
		</div>
		<div id="configuracionContable">
			<br>
			
			<a>Configuraci�n Presupuestaria</a>
			<table border="0">
				<tr>
					<th style="width: 268px;">Nomenclador ${nomenclador1}:</th>
					<td>
						<asf:selectpop name="process.convenio.codigoIegreso" title="Nomenclador"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Iegreso"
								value="${ProcessForm.process.convenio.codigoIegreso}"
								filter="ejercicio=${ejercicio} and imputable=1 and idTipoNomenclador=${idTipoNomenclador}"/>
					</td>
				</tr>
				<tr>
					<th>Nomenclador ${nomenclador2}:</th>
					<td>
					
					 <asf:selectpop name="process.convenio.codigoInstitucional" title="Nomenclador"
								columns="Codigo,Denominacion"
								captions="codigo,denominacion" values="codigo"
								entityName="com.asf.gaf.hibernate.Institucional"
								value="${ProcessForm.process.convenio.codigoInstitucional}"
								filter="ejercicio=${ejercicio} and imputable=1"/>
					
					 	
						
					</td>
				</tr>
				<tr>
					<th>Nomenclador ${nomenclador3}:</th>
					<td>
							<asf:selectpop name="process.convenio.codigoNomenclador" title="Nomenclador"
								columns="Codigo,Denominacion"
								captions="codigo,denominacion" values="codigo"
								entityName="com.asf.gaf.hibernate.Nomenclador"
								value="${ProcessForm.process.convenio.codigoNomenclador}"
								filter="ejercicio=${ejercicio} and imputable=1"/>
					</td>
				</tr>
  			</table>
		</div>	

		<br/>
		<html:submit value="Guardar" onclick="guardar();"/>
		<input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>
<script type="text/javascript">
 function cancelar(){
 	window.location="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConvenioBonificacionProcess&process.accion=listar";
 }
 function guardar(){
 	var accion = document.getElementById("action");	
	var formulario = document.getElementById("oForm");
	accion.value="guardar";
 }
 </script>