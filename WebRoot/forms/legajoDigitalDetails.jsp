<%@ page language="java"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<style>
.pointer {
	cursor: pointer;
}
.button_camera_container {
    text-align: left;
    width: 100%;
    margin-left: 20px;
    margin-bottom: -20px;
    margin-top: -38px;
</style>
<div id="idLegajosConfigurados" class="template-style">
	
	<form id="form_LegajoDigital" name="form_LegajoDigital" >
		<input type="hidden" name="id" id="id" value="<%= request.getParameter("id") %>">
		<input type="hidden" name="tipo" id="tipo" />
		<input type="hidden" name="numeroLegajo" id="numeroLegajo" />
		<input type="hidden" name="persona_id" id="persona_id" />
		<input type="hidden" name="persona_cuil12" id="persona_cuil12" />
		<input type="hidden" name="persona_nudo12" id="persona_nudo12" />
		<input type="hidden" name="persona_nomb12" id="persona_nomb12" />	
		<input type="hidden" name="expediente" id="expediente" />
	</form>
		
	<div class="contenedor-menu-lateral row">

		<div class="col-md-12 col-lg-3 alfa-cabecera">
			<div class="alfa-principal grit">
				<div class="button_camera_container">
	                <button type="button" class="btn btn-default" id="addElement" onclick="add_LegajoDigitalFoto();">
						<i class="fa fa-camera" aria-hidden="true"></i>
					</button> 				
				</div> 
				<img id="img-perfil-user" class="img-responsive" src="${pageContext.request.contextPath}/images/avatar.jpg" />           
                <span class="dp-ppal" id="span_nombre"></span>
                <span class="dp-ppal" id="span_cuil"></span>
                <span class="dp-ppal" id="span_expediente" style="color: #0b5aa0;"></span>
            </div><!-- /.alfa-principal grit -->
            
 			<div class="alfa-enlaces">
				<div class="contenedor-menu-acordion">
					<nav class="sidebar-nav">
						<ul class="metismenu" id="menu">
							<li class="volver-busqueda" onclick="grid_LegajoDigital();">
								<i class="fa fa-reply" aria-hidden="true"></i>&nbsp;Volver al Listado de Legajos
							</li>
							
							<li class="volver-busqueda" onclick="dashboard_ObservacionLegajoDigital();" style="margin-top: 5px !important;">
								<i class="fa fa-comments"></i>&nbsp;Lista de Observaciones
							</li>
							
							<asf:security action="/actions/process.do"
								access="do=process&processName=ArchivoLegajoDigital&process.accion=doGet">
															
							<!-- Documentacion -->
							<li>
								<a class="has-arrow item-ppal" href="#" aria-expanded="true"> 
									<span class="fa fa-fw fa-folder-open fa-lg"></span> DOCUMENTACI&Oacute;N
								</a>								
								
								<ul aria-expanded="true">								
																	
                                    <li onclick="grid_ArchivoLegajoDigital(1);" class="pointer">
										<a> <span class="fa fa-fw fa-file"></span> Documentaci&oacute;n General</a>
									</li>
                                    
                                    <li onclick="grid_ArchivoLegajoDigital(2);" class="pointer">
										<a> <span class="fa fa-fw fa-file"></span> Documentaci&oacute;n T&eacute;cnica</a>
									</li>
																	
                                    <li onclick="grid_ArchivoLegajoDigital(3);" class="pointer">
										<a> <span class="fa fa-fw fa-file"></span> Documentaci&oacute;n Contable</a>
									</li> 									
																	
                                    <li onclick="grid_ArchivoLegajoDigital(4);" class="pointer">
										<a> <span class="fa fa-fw fa-file"></span> Documentaci&oacute;n Jur&iacute;dica</a>
									</li>  									 
									
								</ul>
							</li><!-- /.Documentacion -->
							
							</asf:security>
							
							<!-- Observaciones -->
							
							<asf:security action="/actions/process.do"
								access="do=process&processName=ObservacionLegajoDigital&process.accion=doGet">
							
							<li>
								<a class="has-arrow item-ppal" href="#" aria-expanded="true"> 
									<span class="fa fa-fw fa-comment fa-lg"></span> OBSERVACIONES
								</a>
								
								<ul aria-expanded="true">							
																	
                                    <li onclick="grid_ObservacionLegajoDigital(1);" class="pointer">
										<a> <span class="fa fa-fw fa-comment"></span> Observaciones Generales</a>
									</li>
                                    
                                    <li onclick="grid_ObservacionLegajoDigital(2);" class="pointer">
										<a> <span class="fa fa-fw fa-comment"></span> Observaciones T&eacute;cnicas</a>
									</li>
																	
                                    <li onclick="grid_ObservacionLegajoDigital(3);" class="pointer">
										<a> <span class="fa fa-fw fa-comment"></span> Observaciones Contables</a>
									</li> 									
																	
                                    <li onclick="grid_ObservacionLegajoDigital(4);" class="pointer">
										<a> <span class="fa fa-fw fa-comment"></span> Observaciones Jur&iacute;dicas</a>
									</li>  									 
									
								</ul>
							</li><!-- ./Observaciones -->
							
							</asf:security>
							
							<li class="volver-busqueda" onclick="report_archivoLegajoDigital();">
								<i class="fa fa-print" aria-hidden="true"></i> Impresi&oacute;n de Legajo Digital
							</li>			
						</ul>
					</nav>
				</div>
			</div><!-- /.alfa-enlaces --> 
		</div> <!-- /.alfa-cabecera -->			
			
		<div class="col-md-12 col-lg-9">
<!-- 		<div class="col-lg-4"> -->
			<div id="beta-cuerpo">
				<div id="beta-grid"></div>
				<div id="beta-form"></div>
			</div>
		</div>
       
		
		
	</div> <!-- /.contenedor-menu-lateral -->
</div>	

<script>
filterLegajoDigital =<%= request.getParameter("id") %>;

$j(document).ready(function() {
	crud.populate({
		url: pathSistema + "/rest/LegajoDigitalAction.do?do=doGet",
		id: $j('#id').val(),
		formId: '#form_LegajoDigital',
		entidad: "LegajoDigital",
	},
	function (){
		$j('#span_nombre').html($j('#persona_nomb12').val());
		$j('#span_cuil').html('CUIL: ' + $j('#persona_cuil12').val());	
		var expedienteAde = $j('#expediente').val();
		var htmlExpediente = goExpediente(expedienteAde);		
		$j('#span_expediente').html(htmlExpediente);

	});

	$j('#menu').metisMenu();	
});
</script>