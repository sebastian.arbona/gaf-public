<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" value="save" />
  	<html:hidden name="ProcessForm" property="process.idBonTasa" styleId="idBon" value="${ProcessForm.process.idBonTasa}" />
  	<html:hidden name="ProcessForm" property="process.idPersona" styleId="idPer" value="${ProcessForm.process.idPersona}" />
  	<html:hidden name="ProcessForm" property="process.fechaResolucion2" styleId="idPer" value="${ProcessForm.process.fechaResolucion2}" />
  	<html:hidden name="ProcessForm" property="process.fechaMutuo2" styleId="idPer" value="${ProcessForm.process.fechaMutuo2}" />
  	<html:hidden name="ProcessForm" property="process.expediente" styleId="expediente" value="${ProcessForm.process.expediente}" />
  	<html:errors></html:errors>
  	
    <table>
    	<tr>
    		<th>Actividad Principal</th>
			<td>
				<asf:select name="process.actividadPrincipalSolicitante"
							entityName="com.asf.hibernate.mapping.Tipificadores" 
							listCaption="getCodigo,getDescripcion" 
							listValues="getCodigo"
							value="${ProcessForm.process.actividadPrincipalSolicitante}"
							filter="categoria='ActividadPrincipalSolicitante' order by codigo" />
			</td>
			<th>Sector Econ�mico</th>
    		<td colspan="7">
				<asf:select name="process.sectorEconomico"
							entityName="com.nirven.creditos.hibernate.Sector" 
							listCaption="getId,getNombre" 
							listValues="getId"
							value="${ProcessForm.process.sectorEconomico}"
							filter="bonificacion = 1" />
			</td>    		
    	</tr>
    	<tr>    		
    		<th>Tipo de Empresa</th>
			<td >
				<asf:select name="process.idTipoEmpresa"
							entityName="com.asf.hibernate.mapping.Tipificadores" 
							listCaption="getCodigo,getDescripcion" 
							listValues="getCodigo"
							value="${ProcessForm.process.tipoEmpresa}"
							filter="categoria='TipoEmpresa' order by codigo" />
			</td>   
			<th>N�mero Inscripci�n IIBB</th>
    		<td colspan="7">
    			<html:text name="ProcessForm" property="process.numeroInscripcionIIBB" size="30" disabled="false"/>
    		</td> 		
    	</tr>
    	<tr>
			<th>Has. Beneficiadas</th>
			<td>
				<html:text property="process.hasBeneficiadas" name="ProcessForm" size="30"/>
			</td>
			<th>Cantidad Asociados</th>
			<td>
				<html:text property="process.cantidadAsociados" name="ProcessForm" size="30"/>
			</td>
		</tr>
		<tr>
    		<th>Mano de obra Permanente</th>
    		<td>
    			<html:text name="ProcessForm" property="process.manoObraPermanente" size="30" disabled="false"/>
    		</td>
    		<th>Mano de obra temporal</th>
    		<td>
    			<html:text name="ProcessForm" property="process.manoObraTemporaria" size="30" disabled="false"/>
    		</td>
    	</tr>
    	<tr>
    		<th>Sucursal Entidad Cr�dito</th>
    		<td>
    			<html:text name="ProcessForm" property="process.sucursalEntidadCredito" size="30" disabled="false"/>
    		</td>
    		<th>Fecha Instrumentaci�n</th>
    		<td colspan="7"><asf:calendar property="ProcessForm.process.fechaMutuo" value="${process.bonificacion.fechaMutuo}"/></td>    	
    	</tr>
		<tr>
    		<th>Nro Decreto Provincial</th>
    		<td>
    			<html:text name="ProcessForm" property="process.numeroDecretoProvincial" size="30" disabled="false"/>
    		</td>
    		<th>Nro Acta de APF</th>
    		<td>
    			<html:text name="ProcessForm" property="process.numeroActaAPF" size="30" disabled="false"/>
    		</td>
    	</tr>
    	<tr>
			<th>Fecha Autorizacion Especial</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaAutorizacionEspecial" value="${process.fechaAutorizacionEspecial}"/>
			</td>
			<th>Destino de Fondos</th>
			<td width="50%">
				<html:text property="process.objeto" name="ProcessForm"  size="30" disabled="true"/>
			</td>
    	</tr>
    	
    	
    	
    	
    	<%--     	
    	<tr>
    		<th>Fecha de Solicitud</th>
    		<td>
    			<html:text name="ProcessForm" property="process.fechaSolicitud" size="30" disabled="true"/>
    		</td>
    		<th>Delegaci�n</th>
    		<td>
    			<html:text name="ProcessForm" property="process.idUnidad" size="30" disabled="true"/>
    		</td>
    	</tr>
    	<tr>
    		<th>Expediente</th>
    		<td colspan="7"><html:text name="ProcessForm" property="process.expediente" size="30"/></td>
    	</tr>
    	<tr>
    		<th>Fecha Resoluci�n</th>
    		<td><asf:calendar property="ProcessForm.process.fechaResolucion" value="${ProcessForm.process.bonificacion.fechaResolucion}"/></td>
    		<th>Nro Resoluci�n Aprobatoria</th>
    		<td><asf:text maxlength="10" type="text" property="process.resolucion" name="ProcessForm" id="nroResolucion"
    				attribs="onkeypress='validate(this, event,1); insertarBarra(event);' 
    						onkeyup='updateNumeros(this,1);'"/>
			</td>    						
    	</tr>
    	<tr>
    		<th>Fecha Instrumentaci�n</th>
    		<td colspan="7"><asf:calendar property="ProcessForm.process.fechaMutuo" value="${ProcessForm.process.bonificacion.fechaMutuo}"/></td>
    	</tr>
    	<tr>
    		<th>Volumen Venta</th>
    		<td><asf:text name="ProcessForm" property="process.volumenVtaAnual" type="decimal" maxlength="15"/></td>
    		<th>En</th>
    		<td><html:select property="process.periodoVolVtaAnual" name="ProcessForm" styleId="periodo">
				<html:option value="-1">Seleccione Periodo</html:option>
				<html:option value="1">�ltimos 6 meses</html:option>
				<html:option value="2">�ltimo a�o</html:option>
				<html:option value="3">�ltimos 2 a�os</html:option>
				<html:option value="4">�ltimos 3 a�os</html:option>
				</html:select>
			</td>
    	</tr>
    	<tr>
			<th>
				Tipo de Empresa:
			</th>
			<td colspan="7">
				<asf:select name="process.idTipoEmpresa"
							entityName="com.asf.hibernate.mapping.Tipificadores" 
							listCaption="getCodigo,getDescripcion" 
							listValues="getId"
							value="${ProcessForm.process.idTipoEmpresa}"
							filter="categoria='TipoEmpresa' order by codigo" />
			</td>
		</tr>
    	<tr>
    		<th>
				Inicio de Actividad
			</th>
			<td>
				<asf:calendar property="ProcessForm.process.inicioActividad" value="${ProcessForm.process.bonificacion.inicioActividadStr}"/>
			</td>
			<th>
				Personal Ocupado
			</th>
			<td>
				<html:text property="process.personalOcupado" name="ProcessForm"/>
		</tr>
    	<tr>
			<th>
				Facturacion al mercado externo ($)
			</th>
			<td colspan="7">
				<html:text property="process.facturacionMercExterno" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Has. Beneficiadas
			</th>
			<td>
				<html:text property="process.hasBeneficiadas" name="ProcessForm"/>
			</td>
			<th>
				QQ Cosechados
			</th>
			<td>
				<html:text property="process.qqCosechados" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Destino de Fondos
			</th>
			<td width="50%">
				<html:text property="process.objeto" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Certificacion de Normas de Calidad
			</th>
			<td width="50%">
				<html:text property="process.certifNormas" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Condicionamientos
			</th>
			<td width="50%">
				<html:text property="process.condicionamientos" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Observaciones
			</th>
			<td width="50%">
				<html:text property="process.observaciones" name="ProcessForm"/>
			</td>
		</tr> --%>
		
		</table>
		<div>
			<html:submit  value="Guardar" onclick="guardar();" ></html:submit>
		</div>
		</html:form>
		<iframe width="174" height="189" name="gToday:normal:agenda.js"
			id="gToday:normal:agenda.js"
			src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
			scrolling="no" frameborder="0"
			style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
		</iframe>
		<script type="text/javascript">
		
		function guardar(){
			var accion= $('accion');
			accion.value="save";
		}

		function insertarBarra(e){
		    var key;
		    if(e.which)
		        key=e.which;
		    else
		        key=e.keyCode;
	        
		    if(key!=8){
		        if( key>=48 && key<=57 ){
					var nroResolucion = $('nroResolucion');
					var longitud = nroResolucion.value.length;
					if(longitud == 5 && nroResolucion.value.charAt(longitud - 1) != '/')
					nroResolucion.value = nroResolucion.value + "/";
			    } else {
		            if (e.which)
		                e.preventDefault(true);
		            else
		                e.returnValue=false;
		        }
		    }     
		}
		</script>