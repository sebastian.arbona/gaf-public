<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%
	pageContext.setAttribute("fechaActual", DateHelper.getString(new Date()));
	SimpleDateFormat formatoHora = new SimpleDateFormat("HH");
	SimpleDateFormat formatoMinuto = new SimpleDateFormat("mm");
	pageContext.setAttribute("hora", formatoHora.format(new Date()));
	pageContext.setAttribute("min", formatoMinuto.format(new Date()));
%>
<div class="title">Atenci�n Turno</div>
<br>
<br>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">

	<html:hidden name="ProcessForm" property="process.accion"
		styleId="accion" />
	<html:hidden property="process.turno.id" />
	<html:hidden property="process.unidad" />

	<html:errors />
	<table border="0">
		<tr>
			<th>Nro Atenci&oacute;n:</th>
			<td><asf:text name="ProcessForm"
					property="process.turno.numeroAtencion" type="Long" maxlength="6"
					attribs="class='numeroAtencion'" readonly="true" /></td>
		</tr>

		<tr>
			<th>Nombre:</th>
			<td><html:text property="process.turno.nombre" styleId="nombre"
					size='50'></html:text></td>
		</tr>

		<tr>
			<th>Dni:</th>
			<td><asf:text name="ProcessForm" property="process.turno.dni"
					type="Long" maxlength="20" attribs="class='dni'" /></td>
			<td><html:errors property="AtencionTurnos.error.dni" /></td>

		</tr>

		<tr>
			<th>Cuit/Cuil:</th>
			<td><asf:text name="ProcessForm" property="process.turno.cuit"
					type="Long" maxlength="20" attribs="class='cuit'" /></td>
		</tr>
		<tr id="fs7">
			<th>Sexo:</th>
			<td colspan="3"><asf:select name="process.turno.sexo"
					listCaption="Hombre,Mujer" listValues="M,F"
					value="${ProcessForm.process.turno.sexo}" /></td>
		</tr>
		<tr id="fs8">
			<th>Nacionalidad:</th>
			<td colspan="3"><asf:select name="process.idNacionalidad"
					entityName="com.civitas.hibernate.persona.Nacionalidad"
					nullValue="false" listCaption="getNacionalidad"
					listValues="getIdnacionalidad"
					value="${ProcessForm.process.idNacionalidad}" /></td>
		</tr>
		<tr>
			<th>Domicilio:</th>
			<td><html:text property="process.turno.domicilio"
					styleId="domicilio" maxlength="50" size="50"></html:text></td>
		</tr>
		<tr>
			<th>Localidad:</th>
			<td><asf:selectpop name="process.idLocalidad" title="Localidad"
					columns="C&oacute;digo, CP, Localidad, Departamento, Provincia"
					captions="id.localidad.idlocalidad,id.localidad.cp,id.localidad.nombre,id.departamento.nombre,id.localidad.provin.deta08"
					caseSensitive="true" values="id.localidad.idlocalidad"
					entityName="com.nirven.creditos.hibernate.LocalidadDepartamento"
					cantidadDescriptors="2"
					value="${ProcessForm.process.idLocalidad}"
					onChange="buscarDepartamento();" />
		</tr>
		<tr>
			<th>Departamento:</th>
			<td colspan="3"><html:text
					property="process.turno.departamentoNom" styleId="departamentoNom"
					maxlength="80" readonly="true" /></td>
		</tr>
		<tr>
			<th>Telefono:</th>
			<td><asf:text name="ProcessForm"
					property="process.turno.telefono" type="Long" maxlength="20"
					attribs="class='telefono'" /></td>
		</tr>

		<tr>
			<th>Mail:</th>
			<td><html:text property="process.turno.mail" styleId="mail"></html:text>
			</td>
		</tr>
		<tr>
			<th>Fecha:</th>
			<td><asf:calendar property="process.turno.llegadaStr"
					attribs="class='llegadaStr'" value="${fechaActual}" readOnly="true" />
				<html:text property="process.hora" styleId="process.hora"
					readonly="true"></html:text></td>
		</tr>
		<tr>
			<th>L�nea Consultada:</th>
			<td><asf:select entityName="com.nirven.creditos.hibernate.Linea"
					attribs="class='Linea'" listCaption="getId,getNombre"
					listValues="getId" name="process.turno.linea_id"
					value="${ProcessForm.process.turno.linea_id}" /></td>
		</tr>
		<tr>
			<th>Unidad:</th>
			<td>${ProcessForm.process.turno.unidad.nombre}</td>
		</tr>
		<tr>
			<th>Objeto:</th>
			<td><html:textarea styleId="objeto"
					onkeypress="caracteres(this,400,event)"
					property="process.turno.objeto" cols="40" rows="4" /></td>
		</tr>
		<tr>
			<th>Monto:</th>
			<td><asf:text attribs="class='monto'" name="ProcessForm"
					property="process.turno.monto" type="decimal" maxlength="12" /></td>
		</tr>
		<tr>
			<th>Estado:</th>
			<td><asf:select attribs="class='estado'"
					name="process.turno.estado" entityName="Tipificadores"
					listCaption="codigo,descripcion" listValues="codigo"
					value="${ProcessForm.process.turno.estado}"
					filter="categoria='turno.estado'"></asf:select></td>
		</tr>
		<tr>
			<th>Fecha Atencion:</th>
			<td><asf:calendar attribs="class='atencionStr'"
					property="process.turno.atencionStr" value="${fechaActual}"
					readOnly="true" /> <html:text property="process.horaAtencion"
					styleId="horaAtencion" value="${hora}:${min}" readonly="true"></html:text>
			</td>
		</tr>
		<tr>
			<th>Observacion del Asesor:</th>
			<td><html:textarea styleId="observaciones"
					onkeypress="caracteres(this,500,event)"
					property="process.turno.observaciones" cols="40" rows="4" />
            </td>
		</tr>
		<tr>
			<th>Imprimir:</th>
			<td><asf:select name="process.imprimir"
					value="${ProcessForm.process.imprimir}" listCaption="Si,No"
					listValues="1,0" /></td>
		</tr>

	</table>
	<br>
	<br>
	<input type="button" value="Guardar" id="guardar">
	<logic:equal name="ProcessForm" property="process.accion"
		value="buscar">
		<input type="button" value="Cancelar" id="cancelarbuscar">
	</logic:equal>
	<logic:equal name="ProcessForm" property="process.accion" value="load">
		<input type="button" value="Cancelar" id="cancelarload">
	</logic:equal>

	<input type="button" value="Solicitud" id="solicitud">
</html:form>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
	
</script>
<script language="JavaScript" type="text/javascript">
	jQuery.noConflict();
	jQuery(document)
			.ready(
					function($) {

						var formulario = jQuery('#oForm');
						var accion = jQuery('#accion');

						jQuery('#guardar').click(function() {

							accion.val('guardar');

							formulario.submit();
						});
						jQuery('#solicitud').click(function() {

							accion.val('solicitud');

							formulario.submit();
						});
						jQuery('#cancelarload').click(function() {
							accion.val('cancelarload');
							formulario.submit();
						});
						jQuery('#cancelarbuscar').click(function() {
							accion.val('cancelarbuscar');
							formulario.submit();
						});
						jQuery('.numeroAtencion').change(function() {
							ajax_traer_turno();
						});

						function ajax_traer_turno() {

							jQuery
									.ajax({
										type : "POST",
										url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&entityName=Turno&listValues=nombre;linea.id;objeto;mail;observaciones;monto;&filter=numeroAtencion=\'"
												+ jQuery('.numeroAtencion')
														.val()
												+ "\' order by atencion desc ",
										dataType : "xml",
										success : function(responseText) {

											var objeto;

											jQuery(responseText).find('Turno')
													.each(function() {
														objeto = jQuery(this);
														return;
													});

											if (objeto == null) {
												alert('El Turno no se encontr�. Por favor, intente nuevamente.');
												jQuery('.numeroAtencion').val(
														"");
												return false;
											}

											jQuery('#nombre').val(
													objeto.find("nombre")
															.text());
											jQuery(
													".Linea option[value='"
															+ objeto.find(
																	"linea_id")
																	.text()
															+ "']").attr(
													'selected', 'selected');

											jQuery('#objeto').html(
													objeto.find("objeto")
															.text());
											jQuery('#mail').val(
													objeto.find("mail").text());
											jQuery('#observaciones')
													.html(
															objeto
																	.find(
																			"observaciones")
																	.text());
											jQuery('.monto')
													.val(
															objeto
																	.find(
																			"monto")
																	.text());

										}
									});
						}
					});

	function buscarDepartamento() { //busca departamento asociada a la localidad 
		var localidad = $('process.idLocalidad');
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarDepartamento&idLocalidad="
				+ localidad.value;
		retrieveURL(url, buscarDepartamentoResponse);
	}

	function buscarDepartamentoResponse(responseText) {
		if (responseText && responseText.length > 0) {
			$('departamentoNom').setValue(responseText);
		}
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
