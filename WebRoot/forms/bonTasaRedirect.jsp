<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="requisitosForm">

<script type="text/javascript">
	
	var url = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaProcess';
	url += '&process.idBonTasa=${ProcessForm.process.idBonTasa}&process.accion=verGeneral&process.idPersona=${ProcessForm.process.idPersona}';
	parent.location=url;
	
</script>
</html:form>