<%@ page language="java"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.asf.util.DateHelper"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js"></script>

<div class="title">
	C�lculo Emergencia - ${ProcessForm.process.rd.persona.nomb12}
</div>
<br>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<html:form action="actions/process.do?processName=${param.processName}&do=process" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" value="${ProcessForm.process.accion}" />
	<html:hidden property="process.idRd" styleId="idRd" value="${ProcessForm.process.idRd}" />
	<table>
		<tr>
			<th>
				Titular:
			</th>
			<td>
				${ProcessForm.process.rd.persona.nomb12}
			</td>
		</tr>
		<tr>
			<th>
				Fecha de c�lculo de intereses:
			</th>
			<td>
				${ProcessForm.process.rd.fechaStr}
			</td>
		</tr>
		<tr>
			<th>
				Deuda Actualizada:
			</th>
			<td align="right">
				${ProcessForm.process.rd.total}
			</td>
		</tr>
	</table>
	<br />
	<table>
		<tr>
			<th>
				Capital Original:
			</th>
			<td align="center">
				U$S
			</td>
			<td align="right">
				${ProcessForm.process.rd.capitalOriginalStr}
			</td>
		</tr>
		<tr>
			<th>
				Capital Original Neto De Deducci�n Por Emergencia:
			</th>
			<td align="center">
				U$S
			</td>
			<td align="right">
				${ProcessForm.process.rd.capitalNetoStr}
			</td>
		</tr>
		<tr>
			<td colspan="3">
				(Aplicaci�n de la Ley 7.148 y modif., Art 11, 2do P�rrafo)
			</td>
		</tr>
		<tr>
			<th>
				Intereses Compensatorios:
			</th>
			<td align="center">
				U$S
			</td>
			<td align="right">
				${ProcessForm.process.rd.compensatorioStr}
			</td>
		</tr>
		<tr>
			<th>
				Intereses Moratorios:
			</th>
			<td align="center">
				U$S
			</td>
			<td align="right">
				${ProcessForm.process.rd.moratorioStr}
			</td>
		</tr>
		<tr>
			<th>
				<b>SUBTOTAL</b>
			</th>
			<td align="center">
				U$S
			</td>
			<td align="right">
				<b>${ProcessForm.process.rd.subtotal}</b>
			</td>
		</tr>
		<tr>
			<th>
				Aplicaci�n Del C.E.R.:
			</th>
			<td align="center">
				$
			</td>
			<td align="right">
				${ProcessForm.process.rd.cerStr}
			</td>
		</tr>
		<tr>
			<td colspan="3">
				(Aplicaci�n de la Ley 7.831, Art 3, inc. b)
			</td>
		</tr>
		<tr>
			<th>
				<b>TOTAL</b>
			</th>
			<td align="center">
				$
			</td>
			<td align="right">
				<b>${ProcessForm.process.rd.total}</b>
			</td>
		</tr>
	</table>
	<br/>
	<table>
		<tr>
			<td>
    			<input type="button" value="Volver" onclick="javascript: window.history.back();" />
			</td>
			<td>
    			<input type="button" value="Imprimir" onclick="javascript: imprimir();" />
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">

function imprimir(){
	$('accion').value = "imprimir";
	$('oForm').submit();
}

</script>
