<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" value="guardar" styleId="accion" />
  <html:hidden property="process.personaTitular.id" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.id}" />
  <html:hidden property="process.cargoPersona.id" styleId="idCargo" value="${ProcessForm.process.cargoPersona.id}" />
  
  
  <div style="width: 70%" align="left">
    <html:errors />
  </div>
  <br/>
  <br/>
  <table border="0" align="center">
 
    	<tr>
            <th>Cargo:</th>
            <td>
                                
                <asf:select attribs="class=\"cargo\""   name="process.cargoPersona.cargo.idCargo" entityName="Cargo" listCaption="idCargo,nombreCargo"
                listValues="idCargo" nullText="Seleccione el Cargo" nullValue="true"
                value="${ProcessForm.process.cargoPersona.cargo.idCargo}" style="width:380px"/><div id="funcion"></div>
            </td>
        </tr>
        <tr>
            <th>Fecha Desde: </th>
            <td><asf:calendar property="ProcessForm.process.cargoPersona.fechaDesdeStr" attribs="size=\"55\"" /></td>
        </tr>
        <tr>
            <th>Fecha Hasta: </th>
            <td><asf:calendar property="ProcessForm.process.cargoPersona.fechaHastaStr" attribs="size=\"55\""/></td>
        </tr>
    <tr>
	 <tr>
      <th>
        Norma Legal Cargo Alta:
      </th>
      <td>
        <html:text size="55" property="process.cargoPersona.normaLegal" value="${ProcessForm.process.cargoPersona.normaLegal}" styleId="normaLegal"
          maxlength="55"
        />
      </td>
      </tr>
      <tr>
      <th>
        Norma Legal Cargo Baja:
      </th>
      <td>
        <html:text size="55" property="process.cargoPersona.normaLegalBaja" value="${ProcessForm.process.cargoPersona.normaLegalBaja}" styleId="normaLegalBaja"
          maxlength="55"
        />
      </td>
      </tr>
	  <tr>
      <th>
        Observaciones:
      </th>
      <td>
          <html:textarea styleId="observaciones" property="process.cargoPersona.observacion" cols="41" rows="4" onkeypress="caracteres(this,255,event);" value="${ProcessForm.process.cargoPersona.observacion}"></html:textarea>
      </td>
    </tr>
   
   
    
  </table>
  <br>
  <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=guardar}">
  	<html:submit value="Guardar" />
  </asf:security>
 <input type="button" value="Cancelar" onclick="cancelar();"> 
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
        src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<script type="text/javascript">
var accion = $('accion');
var formulario = $('oForm');
var $j = jQuery.noConflict();

$j(document).ready(function(){
	mostrarFuncion();

	$j('.cargo').change(function(){
		
		
			mostrarFuncion();
		
		
	});
	
});

function mostrarFuncion(){
	
	var cargo = $j('.cargo').val();

		if(cargo != ""){
	
			$j.get('${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=mostrarFuncion', 
				 	'&idCargo='+cargo,
				 	function(data) {	
				 		$j('#funcion').html(data);
				 		$j('#funcion').show();
				 		
				 	})
	
		}
		
	
}



function cancelar() {
	window.location = 
 	'${pageContext.request.contextPath}/actions/process.do?do=process' +
	'&processName=${param.processName}&process.accion=listar'+
	'&process.personaTitular.id=${ProcessForm.process.personaTitular.id}'
}

Event.observe(window, 'load', function() {
	this.name = "Contacto";
	parent.push(this);
});
</script>