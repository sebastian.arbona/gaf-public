<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Empresa</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
    
<table border="0">

<tr><th>Nombre:</th><td><html:text property="entity.nomb01" size="55" maxlength="250"/></td></tr>
<tr><th>Domicilio:</th><td><html:text property="entity.domi01" size="55" maxlength="250"/></td></tr>
<tr><th>Localidad:</th><td><html:text property="entity.loca01" size="55" maxlength="250"/></td></tr>
<%-- tr><th>Zona:</th><td><asf:select name="entity.nord05" entityName="com.asf.cred.hibernate.Zonasgeo" listCaption="getNord05,getDeta05" listValues="getNord05" value="AbmForm.entity.nord05"/></td></tr --%>
<tr><th>C�digo Postal:</th><td><asf:text type="long" name="AbmForm" maxlength="10" property="entity.cpos01"/></td></tr>
<tr><th>Tel�fonos:</th><td><html:text property="entity.fono01" maxlength="30"/></td></tr>
<tr><th>Actividad:</th><td><html:text property="entity.ramo01" size="40" maxlength="250"/></td></tr>
<tr><th>C.U.I.T.:</th><td><html:text property="entity.cuit01" size="13" maxlength="13"/></td></tr>
<%-- <tr><th>Tipo de Documento:</th><td><asf:select name="entity.codi47" entityName="com.asf.cred.hibernate.Tipodoc" listCaption="getCodi47,getDeta47" listValues="getCodi47" value="AbmForm.entity.codi47"/></td></tr>--%>

</table>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>