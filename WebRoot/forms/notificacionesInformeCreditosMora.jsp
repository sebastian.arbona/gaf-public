<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

<%@ page import="java.util.Date"%>
<%@ page import="com.asf.util.DateHelper"%>

<%pageContext.setAttribute("fechaHoy", DateHelper.getString(new Date()));%>

<div class="title">Reporte de Proyectos Informe a Notificar</div>
<p>
<html:form	action="/actions/process.do?do=process&processName=InformeCreditosMoraProcess"
	styleId="oForm">
	<html:hidden property="process.action" name="ProcessForm" styleId="action"/>
	
	<div id="linea" title="Linea de Credito" style="visibility: visible;">
		<table style="border: 2px solid rgb(204, 204, 204);" width="352" height="30">
			<tr>
				<th>
					Comportamiento:
				</th>
				<td>
					<asf:select name="process.codigo"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'comportamientoPago' order by codigo"
						value="${ProcessForm.process.codigo}" nullValue="true"
						nullText="Todos los estados"/>
				</td>
			</tr>
			<tr>
				<th>
					L�nea de Cr�dito:
				</th>
				<td>						
					<html:select styleId="idsLineas" property="process.idsLineas" multiple="true"  size="8">
						<html:optionsCollection  property="process.lineas" label="nombre" value="id"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<th>C�digo de Persona:</th>
				<td>
					<asf:selectpop name="process.idPersona" title="Persona"
						columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
						captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
						entityName="com.civitas.hibernate.persona.Persona" value="" />
				</td>
			</tr>
			<tr>
				<th>Proyecto</th>
				<td>
					<asf:selectpop name="process.numeroAtencion" 
						entityName="Objetoi" title="Credito" 
						caseSensitive="true" values="numeroAtencion"
						columns="Proyecto,Tomador"
						captions="numeroAtencion,persona.nomb12"
						cantidadDescriptors="1"
						value="${ProcessForm.process.numeroAtencion > 0 ? ProcessForm.process.numeroAtencion : null}"/>
				</td>
			</tr>
			<tr>
				<th>Fecha de Mora</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaMora" 
						value="${process.fechaMora}">
					</asf:calendar>
				</td>
			</tr>
		</table>
	</div>
		<input type="button" value="Generar" onclick="generar();">
	    <input type="button" value="Volver" onclick="Volver();">
	</html:form>

	<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
	style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

	<script type="text/javascript">
		function generar(){ 
			var accion = document.getElementById('action');
			accion.value="generar";
			var formulario = document.getElementById('oForm');
			formulario.submit();
		}
		function Volver(){ 
			var accion = document.getElementById('action');
			accion.value="volver";
			var formulario = document.getElementById('oForm');
			formulario.submit();
		}
	</script>
	