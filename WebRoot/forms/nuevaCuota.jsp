<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="title">Nueva Cuota</div>
<html:form action="/actions/process.do?do=process&processName=NuevaCuotaProcess" styleId="ProcessForm">
	<html:hidden name="ProcessForm" property="process.cid"/>
	<html:hidden name="ProcessForm" property="process.action" value="guardar"/>
	
	<div class="grilla">
		<table>
			<tr>
				<th>Proyecto</th>
				<td>${ProcessForm.process.credito.numeroAtencion}</td>
				<th>Expediente</th>
				<td>${ProcessForm.process.credito.expediente}</td>
			</tr>
			<tr>
				<th>Titular</th>
				<td>${ProcessForm.process.credito.persona.nomb12}</td>
				<th>L&iacute;nea</th>
				<td>${ProcessForm.process.credito.linea.nombre}</td>
			</tr>
		</table>
		
		<html:errors/>
		
		<table style="margin-top: 30px">
			<tr>
				<th>N&uacute;mero de cuota</th>
				<td>
					<html:select property="process.numeroCuota" name="ProcessForm">
						<logic:iterate id="nro" name="ProcessForm" property="process.numerosPosibles">
							<html:option value="${nro}">${nro}</html:option>
						</logic:iterate>
					</html:select>
				</td>
			</tr>
			<tr>
				<th>Fecha de vencimiento</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaVencimiento"
						value="${process.fechaVencimiento}"></asf:calendar>
				</td>
			</tr>
			<tr>
				<th colspan="2">
				<logic:equal value="false" property="process.guardado" name="ProcessForm">
					<html:submit value="Guardar"></html:submit>
				</logic:equal>
				<input type="button" value="Volver" onclick="volver();"/>
				</th>
			</tr>
		</table>
		
		<h2>Cuotas del Proyecto</h2>
		<display:table name="ProcessForm" property="process.cuotas" id="cuota">
			<display:column title="Nro." property="numero" sortable="true"/>
			<display:column title="Vencimiento" property="fechaVencimiento" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="Capital" property="capital" decorator="com.asf.displayDecorators.DoubleDecorator"/>
			<display:column title="Liquidada">
				<logic:notEmpty name="cuota" property="emision_id">
					SI
				</logic:notEmpty>
				<logic:empty name="cuota" property="emision_id">
					NO
				</logic:empty>
			</display:column>
		</display:table>
	
	</div>
</html:form>
<script type="text/javascript">
<!--
function volver() {
	var url = '${pageContext.request.contextPath}/actions/process.do?'+
			'do=process&processName=CreditoCuotas' +
			'&process.idObjetoi=${ProcessForm.process.idCredito}';
	window.location = url;
}
//-->
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>