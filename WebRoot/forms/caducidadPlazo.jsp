<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 15px">Caducidad de Plazo</div>
  <div style="width: 70%" align="left">
    <html:errors />
    <logic:equal value="true" name="ProcessForm" property="process.guardado">
    	La caducidad de plazos se ha realizado con exito.
    </logic:equal>
  </div>
<html:form action="/actions/process.do?do=process&processName=CaducidadPlazoProcess" styleId="oForm">
	<html:hidden property="process.action" styleId="action"/>
	<html:hidden property="process.cid" />
	<html:hidden property="process.idGastoEliminar" styleId="gastoEliminar"/>

	<logic:equal value="false" name="ProcessForm" property="process.guardado">
		<table border=0>
			<tr>
				<th>
					Nro de Solicitud / Proyecto:
				</th>
				<td>
					<html:text name="ProcessForm" property="process.nroAtencion" maxlength="6" value="${ProcessForm.process.nroAtencion}" />
				</td>
				<td>
					<html:submit onclick="buscar();">Buscar</html:submit>
				</td>
			</tr>
		</table>
	</logic:equal>
	
	<logic:notEmpty name="ProcessForm" property="process.objetoi">
		<div class="grilla">
			<table>
				<tr>
					<th>Proyecto</th>
					<th>Titular</th>
					<th>CUIT/CUIL</th>
					<th>Expediente</th>
					<th>L�nea</th>
					<th>Objeto</th>
					<th>Comportamiento</th>
					<th>Etapa</th>
				</tr>
				<tr>
					<td>${ProcessForm.process.objetoi.numeroAtencion}</td>
					<td>
						<logic:notEmpty name="ProcessForm" property="process.objetoi.persona.nomb12">
							${ProcessForm.process.objetoi.persona.nomb12}
						</logic:notEmpty>
						<logic:notEmpty name="ProcessForm" property="process.objetoi.persona.razonSocial">
							<logic:empty name="ProcessForm" property="process.objetoi.persona.nomb12">
								${ProcessForm.process.objetoi.persona.razonSocial}
							</logic:empty>
						</logic:notEmpty>
					</td>
					<td>${ProcessForm.process.objetoi.persona.cuil12}</td>
					<td>${ProcessForm.process.objetoi.expediente}</td>
					<td>${ProcessForm.process.objetoi.linea.nombre}</td>
					<td>${ProcessForm.process.objetoi.objeto}</td>
					<td>${ProcessForm.process.objetoi.comportamientoActual}</td>
					<td>${ProcessForm.process.objetoi.estadoActual.estado.nombreEstado}</td>
				</tr>
			</table>
		</div>
	</logic:notEmpty>

	<logic:notEmpty name="ProcessForm" property="process.objetoi">
		<logic:notEmpty name="ProcessForm" property="process.movimientos">
			<table style="margin-top: 30px" width="100%">
				<tr>
					<th>Fecha de Caducidad</th>
					<td><asf:calendar property="ProcessForm.process.fechaCaducidadStr"/>
						${ProcessForm.process.criterioFecha}</td>
					<asf:security action="/actions/caducidadPlazoFechaCalculo.do" access="do=fechaCalculo">
						<th>Fecha de C�lculo</th>
						<td><asf:calendar property="ProcessForm.process.fechaCalculoStr"/></td>
					</asf:security>
				</tr>
			</table>
			<table style="margin-top: 30px" width="100%">
				<tr>
					<th colspan="5">Detalle de Gastos</th>
				</tr>
				<tr>
					<th>Motivo</th>
					<td>
						<asf:select name="process.motivoGasto"
							entityName="com.asf.hibernate.mapping.Tipificadores"
							value="${ProcessForm.process.motivoGasto}"
							listCaption="getCodigo,getDescripcion" listValues="getCodigo"
							filter="categoria = 'ctacte.detalle'  order by codigo" />
					</td>
					<th>Monto de Gastos</th>
					<td><asf:text type="decimal" name="ProcessForm" property="process.montoGasto" maxlength="20"/></td>
					<td>
						<logic:equal value="false" name="ProcessForm" property="process.guardado">
							<input type="submit" value="Agregar" onclick="agregar();">
						</logic:equal>
					</td>
				</tr>
			</table>
			<logic:notEmpty name="ProcessForm" property="process.gastos">
				<table width="100%">
					<tr>
						<th>Motivo</th>
						<th>Monto</th>
						<th>Eliminar</th>
					</tr>
					<logic:iterate name="ProcessForm" property="process.gastos" id="gasto">
					<tr>
						<td>${gasto.motivo}</td>
						<td>${gasto.montoStr}</td>
						<td>
							<logic:equal value="false" name="ProcessForm" property="process.guardado">
								<div onclick="eliminar('${gasto.id}');" style="font-weight: bold; cursor: pointer">ELIMINAR</div>
							</logic:equal>
						</td>
					</tr>
					</logic:iterate>
				</table>
			</logic:notEmpty>
		</logic:notEmpty>
	</logic:notEmpty>
	
	<logic:notEmpty name="ProcessForm" property="process.movimientos">
		<display:table name="ProcessForm" property="process.movimientos" id="mov" style="margin-top: 30px">
			<display:column class="center" title="Fecha Emision" property="fechaGeneracion" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="Tipo Comprobante" property="tipoBoleto" />
			<display:column class="center" title="Fecha Comprobante" property="fechaVencimiento" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="Detalle" property="detalle" />
			<display:column title="Capital" property="capital" class="right" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column title="Compensatorio" property="compensatorio" class="right" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column title="Moratorio" property="moratorio" class="right" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column title="Punitorio" property="punitorio" class="right" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column title="Gastos/Multas" property="gastos" class="right" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column title="Saldo" property="saldo" class="right" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
		</display:table>
	</logic:notEmpty>

	<logic:notEmpty name="ProcessForm" property="process.objetoi">
	<logic:equal value="false" name="ProcessForm" property="process.guardado">
		<html:submit onclick="vistaPrevia();">Vista Previa</html:submit>
	</logic:equal>
	</logic:notEmpty>

	<logic:equal value="true" name="ProcessForm" property="process.permitido">
	<logic:equal value="false" name="ProcessForm" property="process.guardado">
		<input type="button" onclick="guardar();" value="Generar Caducidad de Plazo"></input>
	</logic:equal>
	</logic:equal>


</html:form>
<script type="text/javascript">
	var action = $('action');
	var formulario = $('oForm');
	var gastoEliminar = $('gastoEliminar');
	
	function buscar(){
		accion = document.getElementById("action");	
		accion.value="buscar";
		
	}
	function guardar() {
		var r = confirm("�Desea aplicar la caducidad de plazo?")
		if (r == true) {
			action.value = 'guardar';
			formulario.submit();
			return true;
		}
		return false;
	}
	function vistaPrevia() {
		action.value = 'vistaPrevia';
		formulario.submit();
	}
	function agregar() {
		action.value = 'agregar';
		formulario.submit();
	}
	function eliminar(idGasto) {
		action.value = 'eliminar';
		gastoEliminar.value = idGasto;
		formulario.submit();
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>