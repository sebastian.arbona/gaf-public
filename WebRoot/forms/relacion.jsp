<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Relaciones entre Personas.</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true">
<input type="hidden" name="entity.tipoRelacion_id" value="${paramValue[0]}">
<table border="0">
	<tr>
		<th>ID:</th>
		<td><html:text property="entity.id" readonly="true" /></td>
	</tr>
	<tr>
		<th>Nombre de la Relaci�n:</th>
		<td>
			<html:text property="entity.nombreRelacion" styleId="nombre"></html:text>
		</td>	
	</tr>
	<tr>
		<th>Descripci�n de la Relaci�n:</th>
		<td><html:textarea property="entity.descripcionRelacion" styleId="descripcion" cols="50" rows="4" onkeypress="caracteres(this,100,event)" /></td>
	</tr>
    <tr>
       <th>Relaci�n Inversa: </th>
       <td>
           <asf:select name="entity.inversa_id" entityName="Relacion" listCaption="idRelacion,nombreRelacion"
                       listValues="idRelacion" nullText="Sin Relaci�n Inversa" nullValue="true" filter="tipoRelacion.id=${paramValue[0]}"
                       value="${AbmForm.entity.inversa_id}"/>
       </td>
        </tr>
</table>
<br />
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<script language='javascript' >
var nombre = $( 'nombre' );

nombre.focus();
</script>