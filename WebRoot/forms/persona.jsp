<%@page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@page import="com.asf.util.DirectorHelper"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/links.css"
	type="text/css">

<%
	pageContext.setAttribute("fechaActual", DateHelper.getString(new Date()));
	pageContext.setAttribute("claveGMaps", DirectorHelper.getString("gmaps"));
	pageContext.setAttribute("Domper", DirectorHelper.getString("Domper"));
%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/cuit.js"></script>
<script type="text/javascript">
	/**
	 * @inputIn: control sobre el input que dispara la consulta ajax.-
	 * @defecto: para parametro=valor que tiene,
	 * a la izquierda del igual el id del input que dispara la consulta ajax.-
	 * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
	 */
	function actualizarLista(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&idprovincia=" + controlProvincia.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}

	function terminadaLaCargaDelMapa() {
		cargarMapa(null);
	}

	function ocultarFrame(oculta) {
		if (oculta) {
			$("mapagmap").style.display = 'none';
		} else {
			$("mapagmap").style.display = 'block';
		}
	}

	function tipo(per) {
		if (per == "J") {
			getElem("fe1").innerHTML = "Fecha de Creaci�n:";
			getElem("cu1").innerHTML = "C.U.I.T.:";
			getElem("fs1").innerHTML = "Tipo de Inscripci�n:";
			getElem("fs2").innerHTML = "N&uacute;mero de Inscripci�n:";
			ocultar("fs3", "fs4", "fs5", "fs7", "fs8");
			mostrar("fs9", "razonSocial");
			try {
				ocultar("e1", "e2", "e21");
				mostrar("e3");
			} catch (e) {
				ocultar("e31");
				mostrar("e32");
			}
		} else {
			getElem("fe1").innerHTML = "Fecha de Nacimiento:";
			getElem("cu1").innerHTML = "C.U.I.L.:";
			getElem("fs1").innerHTML = "Tipo de Documento:";
			getElem("fs2").innerHTML = "N&uacute;mero de Documento:";
			mostrar("fs3", "fs4", "fs5", "fs7", "fs8");
			ocultar("fs9", "razonSocial");
			try {
				mostrar("e1", "e2", "e21");
				ocultar("e3");
			} catch (e) {
				mostrar("e31");
				ocultar("e32");
			}
		}
	}

	function changeCuil() {
		var genero = $("entity.sexo12").value;
		var documento = $("entity.nudo12Str").value;
		if (trim(genero) != '' && trim(documento) != '') {
			var cuil = cuil_cuit(genero, documento);
			if (cuil != '' && cuil != undefined) {
				$('entity.cuil12').value = cuil;
			} else {
				$('entity.cuil12').value = '';
			}
		} else {
			$('entity.cuil12').value = '';
		}
	}
</script>

<div class="title">Persona</div>
<br>
<html:form
	action="/actions/${ empty param.altaRapida ? 'PersonaAction' : 'altaRapidaAction' }.do?do=save&entityName=${param.entityName}&filter=true${ empty param.altaRapida ? '' : '&forward=PersonaAltaRapida' }">
	<html:hidden property="entity.codi01" value="101" />
	<html:hidden property="entity.id" />
	<html:hidden property="paramName[0]" value="idpersona" />
	<html:hidden property="paramComp[0]" value="=" />
	<html:hidden property="entity.idlocalidadStr" styleId="idlocalidadStr" />
	<html:hidden property="entity.cpos12" styleId="cpos12" />
	<table border="0" align="center">
		<tr>
			<th>C&oacute;digo:</th>
			<td colspan="3"><html:text property="entity.idpersona"
					readonly="true" /></td>
		</tr>
		<tr>
			<th>Fecha de Alta:</th>
			<td colspan="3"><asf:calendar property="entity.fechaAltaStr"
					value="${empty personaForm.entity.fechaAltaStr ? fechaActual : personaForm.entity.fechaAltaStr}"
					readOnly="${personaForm.entity.id == 0 ? false : true}" /></td>
		</tr>
		<tr>
			<th>Tipo de Persona:</th>
			<td colspan="3"><asf:select
					attribs="OnChange='tipo(this.options[this.selectedIndex].value)'"
					name="entity.tipo12" listCaption="F&iacute;sica,Jur&iacute;dica"
					listValues="F,J" value="${personaForm.entity.tipo12}" /></td>
		</tr>
		<logic:notPresent parameter="load">
			<tr>
				<th>
					<div id="e1">Apellido:</div>
					<div id="e3" style="display: none">Nombre:</div>
				</th>
				<td><html:text property="entity.nomb12Str" size="20"
						maxlength="50" /></td>
				<th id="e2">Nombres:</th>
				<td id="e21"><html:text property="entity.nomb12Str2" size="20"
						maxlength="49" /></td>
			</tr>
		</logic:notPresent>
		<logic:present parameter="load">
			<tr>
				<th>
					<div id="e31">Apellido y Nombre:</div>
					<div id="e32" style="display: none">Nombre:</div>
				</th>
				<td><html:text property="entity.nomb12Str" size="40"
						maxlength="100" /></td>
			</tr>
		</logic:present>
		<tr id="razonSocial" style="display: none">
			<th>Raz&oacute;n Social:</th>
			<td colspan="3"><html:text property="entity.razonSocial"
					size="40" maxlength="100"></html:text></td>
		</tr>
		<tr>
			<th>
				<div id="fe1">${personaForm.entity.tipo12=="F"?'Fecha de Nacimiento':'Fecha de Creaci&oacute;n'}:</div>
			</th>
			<td colspan="3"><asf:calendar
					property="personaForm.entity.fena12Str" /></td>
		</tr>

		<tr>
			<th><div id="fs1">${personaForm.entity.tipo12=="F"?'Tipo de Documento':'Tipo de Inscripci�n'}:</div></th>
			<td colspan="3"><asf:select name="entity.codi47Str"
					entityName="Tipodoc" nullValue="true"
					nullText="-- Seleccione un Tipo de Documento --"
					listCaption="getTido47,getDeta47" listValues="getCodi47"
					value="personaForm.entity.codi47Str"
					attribs="onchange='changeCuil()'" /></td>
		</tr>
		<tr>
			<th><div id="fs2">${personaForm.entity.tipo12=="F"?'N&uacute;mero de Documento':'N&uacute;mero de Inscripci�n'}:</div></th>
			<td colspan="3"><asf:text property="entity.nudo12Str"
					name="personaForm" type="long" maxlength="9"
					attribs="onchange='changeCuil()'" /></td>
		</tr>
		<tr id="fs3">
			<th><div id="c1">C&eacute;dula de Identidad:</div></th>
			<td colspan="3">
				<div id="c11">
					<asf:text property="entity.cedu12Str" name="personaForm"
						type="long" maxlength="8" />
				</div>
			</td>
		</tr>
		<tr id="fs4">
			<th>Expedida por:</th>
			<td colspan="3"><html:text property="entity.expe12"
					maxlength="20" /></td>
		</tr>
		<tr id="fs5">
			<th>Estado Civil:</th>
			<td colspan="3"><asf:select name="entity.eciv12"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					nullValue="true" listCaption="getCodigo,getDescripcion"
					listValues="getCodigo" value="personaForm.entity.eciv12"
					filter="categoria='EstadoCivil' order by codigo" /></td>
		</tr>
		<tr id="fs7">
			<th>Sexo:</th>
			<td colspan="3"><asf:select name="entity.sexo12"
					listCaption="&lt;&lt;NONE&gt;&gt;,Hombre,Mujer" listValues=" ,M,F"
					value="${personaForm.entity.sexo12}"
					attribs="onchange='changeCuil()'" /></td>
		</tr>
		<tr>
			<th>
				<div id="cu1">${personaForm.entity.tipo12=="F"?'C.U.I.L.':'C.U.I.T.'}:</div>
			</th>
			<td colspan="3"><asf:text property="entity.cuil12"
					name="personaForm" type="long" maxlength="11" /></td>
		</tr>


		<tr id="fs8">
			<th>Nacionalidad:</th>
			<td colspan="3"><asf:select name="entity.pais12"
					entityName="Nacionalidad" nullValue="true"
					listCaption="getNacionalidad" listValues="getIdnacionalidad"
					value="${personaForm.entity.pais12}" /></td>
		</tr>
		<tr id="fs9">
			<th>Tipo de Sociedad:</th>
			<td colspan="3"><asf:select name="entity.codi21"
					entityName="Tsoci" nullValue="true" listCaption="getDeta21"
					listValues="getCodi21" value="${personaForm.entity.codi21}" /></td>
		</tr>
		<tr>
			<th>Condici�n de Iva</th>
			<td colspan="7"><asf:select name="entity.condicionIva_id"
					entityName="com.civitas.hibernate.persona.CondicionIva"
					listCaption="id,nombre,alicuota" listValues="id"
					value="${personaForm.entity.condicionIva.id}" nullValue="true"
					nullText="-- Seleccione la Condici�n ante el IVA --" /></td>
		</tr>
		<logic:equal name="personaForm" property="entity.id" value="0">
			<tr>
				<th colspan="4" align="center">Domicilio ${Domper}</th>
			</tr>
			<tr>
				<th>Pa&iacute;s</th>
				<td colspan="3"><asf:select name="idPais"
						entityName="com.civitas.hibernate.persona.Pais"
						listCaption="nombrePais" listValues="idPais"
						value="${empty personaForm.entity.provin.pais ? '' : personaForm.entity.provin.pais.idPais}"
						filter="nombrePais like '%' order by nombrePais" /></td>
			</tr>
			<tr>
				<th>Provincia:</th>
				<td colspan="3"><asf:lselect property="entity.codi08"
						entityName="com.civitas.hibernate.persona.Provin"
						listCaption="deta08" listValue="codi08"
						value="${personaForm.entity.provin.codi08}" linkName="idPais"
						linkFK="pais.id" orderBy="deta08" /></td>
			</tr>
			<tr>
				<th>Localidad:</th>
				<td><asf:selectpop name="entity.idlocalidad" title="Localidad"
						columns="C&oacute;digo, CP, Localidad, Departamento, Provincia"
						captions="id.localidad.idlocalidad,id.localidad.cp,id.localidad.nombre,id.departamento.nombre,id.localidad.provin.deta08"
						caseSensitive="true" values="id.localidad.idlocalidad"
						entityName="com.nirven.creditos.hibernate.LocalidadDepartamento"
						cantidadDescriptors="2"
						value="${personaForm.entity.localidad.localidad_id}"
						onChange="buscarDepartamento();" /></td>
			</tr>
			<tr>
				<th>Departamento:</th>
				<td colspan="3"><html:text property="entity.zona12"
						value="${ProcessForm.process.domicilio.departamentoNom}"
						styleId="departamentoNom" maxlength="80" readonly="true" /></td>
			</tr>
			<tr>
				<th>Barrio:</th>
				<td colspan="3"><asf:selectpop name="entity.idbarrio"
						title="Barrio" columns="Codigo,Nombre" captions="codiBrr,descBrr"
						values="codiBrr" entityName="com.civitas.hibernate.persona.Barrio"
						cantidadDescriptors="1"
						value="${personaForm.entity.barrio.codiBrr}" /> <a
					href="javascript:void(0);"
					onclick="popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaGenericaAction.do?do=newEntity&entityName=Barrio&forward=BarrioAltaRapida');"><b>Registrar
							Barrio</b> </a></td>
			</tr>
			<tr>
				<th>Calle:</th>
				<td colspan="3"><asfgat:callepopup
						attribs="onblur='cargarMapa();'" property="entity.idcalleStr"
						value="${personaForm.entity.idcalleStr}" /> <a
					href="javascript:void(0);"
					onclick="popUpBigBig('${pageContext.request.contextPath}/actions/CalleAction.do?do=newEntity&entityName=Calle&forward=CalleAltaRapida');"><b>Registrar
							Calle</b> </a></td>
			</tr>
			<tr>
				<th>Calle:</th>
				<td id="calle1" colspan="3"><html:text size="73"
						onblur="cargarMapa();" property="entity.calle"
						value="${personaForm.entity.calle}" styleId="calle" maxlength="80" />
				</td>
			</tr>
			<tr>
				<th>Observacion:</th>
				<td colspan="3"><html:text size="73" onblur="cargarMapa();"
						property="entity.observacion"
						value="${personaForm.entity.observacion}" styleId="obs"
						maxlength="80" /></td>
			</tr>
			<tr>
				<th>N&uacute;mero:</th>
				<td colspan="3"><html:text onblur="cargarMapa();"
						property="entity.numero" value="${personaForm.entity.numero}"
						styleId="num" maxlength="5" /></td>
			</tr>
			<tr>
				<th>Manzana / Piso:</th>
				<td colspan="3"><html:text property="entity.piso"
						value="${personaForm.entity.piso}" styleId="mza" maxlength="2" />
				</td>
			</tr>
			<tr>
				<th>Lote / Departamento:</th>
				<td colspan="3"><html:text property="entity.dpto"
						value="${personaForm.entity.dpto}" styleId="lote" maxlength="2" />
				</td>
			</tr>
		</logic:equal>
	</table>
	<br>
	<center>
		--
		<iframe width="100%" name="mapagmap" id="mapagmap" height="320px"
			src="${pageContext.request.contextPath}/gmaps.jsp" scrolling="no"
			style="display: none"></iframe>
		--
	</center>
	<br>
	<asf:security
		action="/actions/${ empty param.altaRapida ? 'PersonaAction' : 'altaRapidaAction' }.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit styleId="bGuardar">
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
	var ctrlBGuardar = $('bGuardar');
	var cbo = $("entity.tipo12");
	//Controles para la carga del mapa.-
	var controlLocalidad = $('idlocalidadStr');
	var controlProvincia = $('entity.codi08');
	var idlocalidad = $('entity.idlocalidad');
	var cpos12 = $('cpos12');
	var pais = $('idPais');

	//Carga del Mapa.
	//Mas oportunidades de encontrar una ubicaci�n determinada, si se le da el siguiente formato a la busqueda:
	//Direcci�n, ciudad, estado o Direcci�n, ciudad, codigo postal..
	function cargarMapa(direccion) {
		if ('${claveGMaps}' != null && '${claveGMaps}' != 0
				&& '${claveGMaps}' != '') {
			var calle = "";
			var cpostal = "";
			var localidad = "";
			var provincia = "";
			var nombreLocalidad = "";
			var nombrePais = "";

			if (trim(idlocalidad.value) != "") {
				controlLocalidad.value = idlocalidad.value.split("-")[0];
				cpos12.value = idlocalidad.value.split("-")[1];
				nombreLocalidad = idlocalidad.value.split("-")[2];
				direccion = "";
				direccion = cpos12.value + "-" + nombreLocalidad;
			}

			if (direccion != null && direccion != '' && direccion != undefined) {
				direccion = direccion.split("-");
				cpostal = direccion[0] != null && direccion[0] != '<<NONE>>' ? direccion[0]
						: '';
				localidad = direccion[1] != null && direccion[1] != '' ? direccion[1]
						: $F("loc");
				provincia = direccion[2] != null ? direccion[2]
						: controlProvincia.options[controlProvincia.selectedIndex].text;
				calle = $F("entity_idcalleStrDescriptor") != null
						&& $F("entity_idcalleStrDescriptor") != '' ? $F("entity_idcalleStrDescriptor")
						: $F("calle");
				nombrePais = pais.options[pais.selectedIndex].text;
				direccion = calle + " , " + $F("num") + " , " + localidad
						+ " , " + provincia + " , " + cpostal + " , "
						+ nombrePais;
				window.frames["mapagmap"].buscarDireccion(direccion);
			}
		}
	}

	function buscarDepartamento() { //busca departamento asociada a la localidad 
		var localidad = $('entity.idlocalidad');
		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarDepartamento&idLocalidad="
				+ localidad.value;
		retrieveURL(url, buscarDepartamentoResponse);
	}

	function buscarDepartamentoResponse(responseText) {
		if (responseText && responseText.length > 0) {
			$('departamentoNom').setValue(responseText);
		}
	}

	function registrarLocalidad() {
		var idprovincia = controlProvincia.value;

		if (idprovincia != "") {
			var url = "";
			url += "${pageContext.request.contextPath}/actions/altaRapidaGenericaAction.do?";
			url += "do=newEntity&entityName=Localidad";
			url += "&idprovincia=" + idprovincia;
			url += "&paramValue[0]=" + idprovincia;
			url += "&forward=LocalidadAltaRapida";

			popUpBigBig(url, "Localidades");
		} else {
			alert("Debe seleccionar una Provincia");
		}
	}
	Event.observe(window, 'load', function() {
		tipo(cbo.value);
		if ('${claveGMaps}' != null && '${claveGMaps}' != 0
				&& '${claveGMaps}' != '') {
			terminadaLaCargaDelMapa();
		}
	});

	<logic:present parameter="load">
	if ('${personaForm.entity.fechaBaja}' != "") {
		ctrlBGuardar.disabled = true;
	} else {
		ctrlBGuardar.disabled = false;
	}
	</logic:present>

	<logic:notPresent parameter="load">
	ctrlBGuardar.disabled = false;
	</logic:notPresent>
</script>
