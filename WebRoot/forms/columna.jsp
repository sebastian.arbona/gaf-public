<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ page import="com.asf.hibernate.mapping.Reporte" %>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/comprobante.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>
 
<div class="title">Columna</div>
<br><br>
<html:errors/>
<html:form action="/actions/process.do?do=process&processName=${param.processName}&process.action=save">

<html:hidden property="process.reporte.id" styleId="idreporte"  value="${ProcessForm.process.reporte.id}"/>

<div style="border:solid;width:85%;border-color:#CCCCCC">
<table border="0">
<%--  %><tr><th>C�digo de Reporte:</th><td><html:text property="process.reporte.id" styleId="idreporte" readonly="true"/></td></tr> --%>
<tr><th>Tipo de Reporte:</th><td><asf:select name="process.reporte.tipo" listCaption="Reporte HTML,Reporte Jasper,Reporte con retoma Jasper,Action Report" attribs="onchange=\"label();\"" listValues="0,1,2,3" value="${ProcessForm.process.reporte.tipo}"/></td></tr>
<tr><th>T�tulo:</th><td><html:text property="process.reporte.titulo" maxlength="100"/></td></tr>
<tr><th>
    <div id="lb1" style="display:${ProcessForm.process.reporte.tipo==null||ProcessForm.process.reporte.tipo==0?'block':'none'}">Sub-T�tulo:</div>
    <div id="lb2" style="display:${ProcessForm.process.reporte.tipo==1?'block':'none'}">Archivo Jasper:</div>
    <div id="lb3" style="display:${ProcessForm.process.reporte.tipo==2?'block':'none'}">Reporte con retoma Jasper:</div>
    <div id="lb4" style="display:${ProcessForm.process.reporte.tipo==3?'block':'none'}">Action Report:</div>
    </th>
    <td><html:text property="process.reporte.subtitulo" maxlength="30"/></td></tr>
</table>

<div id="sqlt" style="display:${ProcessForm.process.reporte.tipo==2 || ProcessForm.process.reporte.tipo==3?'none':'block'}">
    <table>
        <tr><th>SQL:</th><td><html:textarea property="process.reporte.sql" cols="100" rows="10"  styleId="SQL" onkeypress="caracteres(this,7999,event)"/></td></tr>
    </table>
</div>
</div>

<br><br>

<div id="columnaDisp" style="border:solid;width:85%;border-color:#CCCCCC;display:${ProcessForm.process.reporte.tipo==2 || ProcessForm.process.reporte.tipo==3?'none':'block'}">
<table>
    <tr><th colspan="12" style="text-align:center"><u>Columna</u></th></tr>
    <tr>
        <th>Orden:</th><td><asf:text type="long" name="ProcessForm" property="ordenStr" id="orden" maxlength="6"/></td>
        <th>Nombre:</th><td colspan="3"><input type="text" name="nombre" id="nombre" maxlength="30"></td>
    </tr>
    <tr>
        <th>Ancho:</th><td><asf:text type="long" name="ProcessForm" property="anchoStr" id="ancho" maxlength="6"/></td>
        <th>Alineaci�n:</th><td colspan="3"><asf:select name="alineacion" listCaption="getCodigo,getDescripcion" listValues="getCodigo" entityName="com.asf.hibernate.mapping.Tipificadores" filter="categoria='Alineacion' order by codigo"/></td>        
    </tr>
    <tr>
	    <th>Sumariza:</th><td><asf:select name="sumariza" listCaption="getCodigo,getDescripcion" listValues="getCodigo" entityName="com.asf.hibernate.mapping.Tipificadores" filter="categoria='Sumariza' order by codigo"/></td>
        <th>Pie:</th><td colspan="3"><input type="text" name="pie" id="pie" maxlength="30"></td>
    </tr>
    <tr>
        <th>Tipo:</th><td><asf:select name="tipo" listCaption="getCodigo,getDescripcion" listValues="getCodigo" entityName="com.asf.hibernate.mapping.Tipificadores" filter="categoria='TipoColumna' order by codigo"/></td>
        <th>Formato:</th><td><input type="text" name="formato" id="formato" maxlength="30"/></td>
        <th>Visible:</th><td><asf:select name="muestra" listCaption="Si,No" listValues="1,0"/></td>
    </tr>
</table>
<br><br>

<center><input type="button" id="agregaColumna" value="Agregar" onclick="agregarColumna();"></center>


<br><br>
<table id="columna" style="border: 1px solid black">
    <style>th {text-align:center;}</style>
    <tr><th>Orden</th><th>Nombre</th><th>Ancho</th><th>Alineaci�n</th><th>Sumariza</th><th>Pie</th><th>Tipo</th><th>Formato</th><th>Visible</th></tr>
    <tr><th colspan="10"><hr></hr></th></tr>
</table>

</div>

<br><br>

<div id="filtroDisp" style="border:solid;width:85%;border-color:#CCCCCC;display:${ProcessForm.process.reporte.tipo==2 || ProcessForm.process.reporte.tipo==3?'none':'block'}">
<table>
    <tr><th colspan="6" style="text-align:center"><u>Filtro</u></th></tr>
    <tr>
	    <th>Nombre:</th><td><input type="text" name="nombreFiltro" id="nombreFiltro" maxlength="30"></td>
        <th>Etiqueta:</th><td><input type="text" name="etiquetaStr" id="etiqueta" maxlength="40"></td>
    </tr>
    <tr>
        <th>Expresi�n:</th><td><input type="text" name="expresion" id="expresion" maxlength="50"></td>
        <th>Tabla:</th><td><input type="text" name="tabla" id="tabla" maxlength="30"></td>
    </tr>
    <tr>
        <th>Condici�n:</th><td><asf:select name="comparador" listCaption="getCodigo,getDescripcion" listValues="getCodigo" entityName="com.asf.hibernate.mapping.Tipificadores" filter="categoria='FiltroComparador' order by codigo"/></td>  
        <th>Filtro Tipo:</th><td><asf:select name="idfiltro" listCaption="nombre" listValues="idfiltro" entityName="com.asf.hibernate.mapping.Filtro" filter="idfiltro like '%' order by idfiltro"/></td>                
	</tr>
    <tr>
	    <th>Obligatorio:</th><td><asf:select name="obligatorio" listCaption="Si,No" listValues="1,0"/></td>
        <th>Valor por Defecto:</th><td><input type="text" name="valor" id="valor" maxlength="100"/></td>
        
    </tr>
</table>

<br><br>

<center><input type="button" id="agregaFiltro" value="Agregar" onclick="agregarFiltro('filtro');"></center>
<br>
<table id="filtro" style="border: 1px solid black">
    <style>th {text-align:center;}</style>
    <tr><th>Nombre</th><th>Etiqueta</th><th>Expresi�n</th><th>Tabla</th><th>Condici�n</th><th>Obligatorio</th><th>Filtro Tipo</th><th>Valor por Defecto</th></tr>
    <tr><th colspan="9"><hr></th></tr>
</table>
</div>
<br><br>
<%--asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}"--%>
	<html:submit onclick="return valida();"><bean:message key="abm.button.save"/></html:submit>
<%--/asf:security--%>	
        <input type="button" value="Cancelar" onclick="window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=PColumna&process.action=list'">
</html:form>

<script language="javascript">
var itemC=0;
var itemF=0;
var idcolumna=new Array();
var msjError = "";
    
//Globales____________________________________________________________________________________________________________________________
function label(){
    var valor=getElem('process.reporte.tipo').value;
    switch(valor){
        case '0':
            ocultar('lb2','lb3','lb4');
            mostrar('lb1','sqlt','columnaDisp','filtroDisp');
            break;
        case '1':
            ocultar('lb1','lb3','lb4');
            mostrar('lb2','sqlt','columnaDisp','filtroDisp');
            break;
        case '2':
            ocultar('lb1','lb2','lb4','sqlt','columnaDisp','filtroDisp');
            mostrar('lb3');
            break;
        case '3':
            ocultar('lb1','lb2','lb3','sqlt','columnaDisp','filtroDisp');
            mostrar('lb4');
            break;
    }
}

	 
var nom;
function validarNombre(f){
	nom = $(f != "filtro" ? 'nombre': 'nombreFiltro' ).value;

	var retorno = true;
	 //Nos aseguramos que ingrese un nombre
	 if(trim(nom) == ''){
	 msjError += "Debe ingresar un nombre.\n" ;
	 retorno = false;
	 }
	
	if(f == "filtro"){//agregamos filtro
		if( !existe("columna") )
		{
			msjError = "Nombre debe coinicidir con alg�n nombre de columna.";
			retorno = false;
		}
		
	}
	else {	// agregamos columnas
	
		if( existe("columna") )
		{
			msjError = "Ya existe el nombre ingresado.";
			retorno = false;
		}
	}	
	
	return retorno;
}
//if(  (tabla.childNodes[1].childNodes[i].childNodes[1].innerHTML.toUpperCase()) == nom.toUpperCase() )



function existe(nTabla){
	var tablaCol = $(nTabla);
	var filas = tablaCol.rows.length;
	var c=0;
	var fila; 
	var cell;

	if(nTabla == "columna") 
	var t=1;				// COlumna 1 de tabla columna	
	else t=0;				// COlumna 0 de tabla filtro	
	
	for( var k =2; k <filas; k++ ){
		fila = tablaCol.getElementsByTagName("tr")[k];				//trae cada fila de la tabla.-
		cell = fila.getElementsByTagName("td")[t];					//Hago referencia a los datos 
		if(trim(cell.innerHTML.toUpperCase()) ==  trim(nom.toUpperCase()) ){
		 c = c+1 ;
   		}
	}

	return c > 0;
}


    
//Lista columnas_____________________________________________________________________________________________________________________    
function agregarColumna(){

    var tabla=$("columna");
    var rows=tabla.rows.length;

// Se debe ingresar un nro en Orden.
	if ($("orden").value == "" )
	msjError += "Debe ingresar un n�mero de orden. \n";

	validarNombre();
	if( msjError != "" )
	{
		alert( msjError );
		msjError="";
		
		return;
	}

    if( itemC < 1 ){
        var fila=tabla.insertRow( rows );
        var cell=fila.insertCell(0);
            cell.innerHTML=$("orden").value;
        cell=fila.insertCell(1);
            cell.innerHTML= $("nombre").value;
        cell=fila.insertCell(2);
            cell.innerHTML=$("ancho").value;
        cell=fila.insertCell(3);
            cell.innerHTML=$("alineacion").value;
        cell=fila.insertCell(4);
            cell.innerHTML=$("sumariza").value;
        cell=fila.insertCell(5);
            cell.innerHTML=$("pie").value;
        cell=fila.insertCell(6);
            cell.innerHTML=$("tipo").value;
        cell=fila.insertCell(7);
            cell.innerHTML=$("formato").value;
        cell=fila.insertCell(8);
            cell.innerHTML=$("muestra").value;
        addButtons( fila.insertCell(9) , modificarColumna, borrarColumna, rows-2 );
    }else{
        var fila=tabla.insertRow( itemC+1 );
        var cell=fila.insertCell(0);
            cell.innerHTML=$("orden").value;
        cell=fila.insertCell(1);
            cell.innerHTML= $("nombre").value;
        cell=fila.insertCell(2);
            cell.innerHTML=$("ancho").value;
        cell=fila.insertCell(3);
            cell.innerHTML=$("alineacion").value;
        cell=fila.insertCell(4);
            cell.innerHTML=$("sumariza").value;
        cell=fila.insertCell(5);
            cell.innerHTML=$("pie").value;
        cell=fila.insertCell(6);
            cell.innerHTML=$("tipo").value;
        cell=fila.insertCell(7);
            cell.innerHTML=$("formato").value;
        cell=fila.insertCell(8);
            cell.innerHTML=$("muestra").value;
        addButtons( fila.insertCell(9) , modificarColumna, borrarColumna, itemC );
    }
    itemC=0;

    $("orden").value="";
    $("nombre").value="";
    $("ancho").value="";
    $("alineacion").value="";
    $("sumariza").value="";
    $("pie").value="";
    $("tipo").value="";
    $("formato").value="";
    $("muestra").value="";
}

function modificarColumna( evt ){
    if ( isIE )
        evt  = window.event;
    elem = (evt.target) ? evt.target : evt.srcElement;
        
    elem = elem.parentNode.parentNode;
    idItem=elem.rowIndex-2;

    itemC = idItem+1;

    $("orden").value=elem.childNodes[0].innerHTML;
    $("nombre").value=elem.childNodes[1].innerHTML;
    $("ancho").value=elem.childNodes[2].innerHTML;
    $("alineacion").value=elem.childNodes[3].innerHTML;
    $("sumariza").value=elem.childNodes[4].innerHTML;
    $("pie").value=elem.childNodes[5].innerHTML;
    $("tipo").value=elem.childNodes[6].innerHTML;
    $("formato").value=elem.childNodes[7].innerHTML;
    $("muestra").value=elem.childNodes[8].innerHTML;
       
    elem.parentNode.removeChild( elem );
}

function borrarColumna( evt ){

    if ( !evt ) evt = window.event;
    var elem = (evt.target) ? evt.target : evt.srcElement;
    
    nom = elem.parentNode.parentNode.childNodes[1].innerHTML;
    

		if (existe("filtro")){
		alert("Eliminar antes su filtro.");
		return;
		}
    
    var respuesta=confirm('�Est� seguro de eliminar este �tem?');
    if( respuesta==true ){
    	
		
        elem.parentNode.parentNode.parentNode.removeChild( elem.parentNode.parentNode );
    }
    
}

//Fin Lista columnas_____________________________________________________________________________________________________________________    


//Lista Filtros_________________________________________________________________________________________________________________________    
function agregarFiltro(t){

	validarNombre(t);
	
	if( msjError != "" )
	{
		alert( msjError );
		msjError="";
		return;
	}

    var tabla=$("filtro");
    var rows=tabla.rows.length;

    if( itemF < 1 ){// la primera vez itemF vale cero.
        var fila=tabla.insertRow( rows );

        var cell=fila.insertCell(0);
            cell.innerHTML= $("nombreFiltro").value;

        cell=fila.insertCell(1);
            cell.innerHTML=$("etiqueta").value;

        cell=fila.insertCell(2);
            cell.innerHTML=$("expresion").value;
            
        cell=fila.insertCell(3);
            cell.innerHTML=$("tabla").value;
            
        cell=fila.insertCell(4);
            cell.innerHTML=$("comparador").value;
            
        cell=fila.insertCell(5);
            cell.innerHTML=$("obligatorio").value;
            
        cell=fila.insertCell(6);
            cell.innerHTML=$("idfiltro").value;
            
        cell=fila.insertCell(7);
            cell.innerHTML=$("valor").value;
            
        addButtons( fila.insertCell(8) , modificarFiltro, borrarFiltro, rows-2 );
    }else{
        var fila=tabla.insertRow( itemF+1 );
        var cell=fila.insertCell(0);
            cell.innerHTML= $("nombreFiltro").value;
       cell=fila.insertCell(1);
            cell.innerHTML=$("etiqueta").value;
        cell=fila.insertCell(2);
            cell.innerHTML=$("expresion").value;
        cell=fila.insertCell(3);
            cell.innerHTML=$("tabla").value;
        cell=fila.insertCell(4);
            cell.innerHTML=$("comparador").value;
        cell=fila.insertCell(5);
            cell.innerHTML=$("obligatorio").value;
        cell=fila.insertCell(6);
            cell.innerHTML=$("idfiltro").value;
        cell=fila.insertCell(7);
            cell.innerHTML=$("valor").value;
        addButtons( fila.insertCell(8) , modificarFiltro, borrarFiltro, itemF );
    }
    itemF=0;
	
	$("nombreFiltro").value="";
    $("etiqueta").value="";
    $("expresion").value="";
    $("tabla").value="";
    $("comparador").value="";
    $("obligatorio").value="";
    $("idfiltro").value="";
    $("valor").value="";
}

function modificarFiltro( evt ){
    if ( isIE )
        evt  = window.event;
    elem = (evt.target) ? evt.target : evt.srcElement;

    elem = elem.parentNode.parentNode;
    idItem=elem.rowIndex-2;

    itemF = idItem+1;
    $("nombreFiltro").value=elem.childNodes[0].innerHTML;
    $("etiqueta").value=elem.childNodes[1].innerHTML;
    $("expresion").value=elem.childNodes[2].innerHTML;
    $("tabla").value=elem.childNodes[3].innerHTML;
    $("comparador").value=elem.childNodes[4].innerHTML;
    $("obligatorio").value=elem.childNodes[5].innerHTML;
    $("idfiltro").value=elem.childNodes[6].innerHTML;
    $("valor").value=elem.childNodes[7].innerHTML;
       
    elem.parentNode.removeChild( elem );
}

function borrarFiltro( evt ){
    if ( !evt ) evt = window.event;
    var elem = (evt.target) ? evt.target : evt.srcElement;
    
    var respuesta=confirm('�Est� seguro de eliminar este �tem?');
    if(respuesta==true){
        index=(elem.parentNode.parentNode.rowIndex-2);
        elem.parentNode.parentNode.parentNode.removeChild( elem.parentNode.parentNode );
        try{
            if( index < (idcolumna.length-1) ){
                for( i=index; i < idcolumna.length; i++ ){
                    idcolumna[i]=idcolumna[(i+1)];
                }
                idcolumna.splice( (idcolumna.length-1),1 );
            }else{
                idcolumna.splice( index,1 );
            }
        }catch(e){}
    }
}

//Fin Lista Filtros_____________________________________________________________________________________________________________________    

function valida(){
    var tablaC=$("columna");
    var tablaF=$("filtro");
    var i;
    
    if( tablaC.rows.length > 2 ){
        for( i=2; i < tablaC.rows.length; i++ ){
            fila=tablaC.getElementsByTagName("tr")[i];
            cell=fila.getElementsByTagName("td")[0];
                if( idcolumna[i-2]!="" && idcolumna[i-2]!="undefined" ){
                    addHidden(cell,"process.items["+(i-2)+"].idcolumna",idcolumna[(i-2)],"");
                }
                addHidden(cell,"process.items["+(i-2)+"].idreporte",$("idreporte").value,"");
                addHidden(cell,"process.items["+(i-2)+"].ordenStr",cell.childNodes[0].nodeValue,"");
            cell=fila.getElementsByTagName("td")[1];
                addHidden(cell,"process.items["+(i-2)+"].nombre",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[2];
                addHidden(cell,"process.items["+(i-2)+"].anchoStr",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[3];
                addHidden(cell,"process.items["+(i-2)+"].alineacion",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[4];
                addHidden(cell,"process.items["+(i-2)+"].sumariza",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[5];
                addHidden(cell,"process.items["+(i-2)+"].pie",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[6];
                addHidden(cell,"process.items["+(i-2)+"].tipo",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[7];
                addHidden(cell,"process.items["+(i-2)+"].formato",cell.innerHTML,"");
            cell=fila.getElementsByTagName("td")[8];
                addHidden(cell,"process.items["+(i-2)+"].muestra",cell.innerHTML,"");
        }
    }

    if( tablaF.rows.length > 2 ){
        for( i=2; i < tablaC.rows.length; i++ ){
            filaC=tablaC.getElementsByTagName("tr")[i];
            for( j=2; j < tablaF.rows.length; j++ ){
	            fila=tablaF.getElementsByTagName("tr")[j];	
	            //alert(filaC.getElementsByTagName("td")[1].childNodes[0].nodeValue+"=="+fila.getElementsByTagName("td")[0].childNodes[0].nodeValue);            
	            if( filaC.getElementsByTagName("td")[1].childNodes[0].nodeValue==fila.getElementsByTagName("td")[0].childNodes[0].nodeValue ){
		            fila.getElementsByTagName("td")[0].childNodes[0].nodeValue="";
		            
	                cell=fila.getElementsByTagName("td")[1];
	                    addHidden(cell,"process.items["+(i-2)+"].etiquetaStr",cell.innerHTML,"");
	                cell=fila.getElementsByTagName("td")[2];
	                    addHidden(cell,"process.items["+(i-2)+"].expresion",cell.innerHTML,"");
	                cell=fila.getElementsByTagName("td")[3];
	                    addHidden(cell,"process.items["+(i-2)+"].tabla",cell.innerHTML,"");
	                cell=fila.getElementsByTagName("td")[4];
	                    addHidden(cell,"process.items["+(i-2)+"].comparador",cell.innerHTML,"");
	                    
	                cell=fila.getElementsByTagName("td")[5];
	                    addHidden(cell,"process.items["+(i-2)+"].obligatorio",cell.innerHTML,"");
	                cell=fila.getElementsByTagName("td")[6];
	                    addHidden(cell,"process.items["+(i-2)+"].idfiltro",cell.innerHTML,"");
	                cell=fila.getElementsByTagName("td")[7];
	                    addHidden(cell,"process.items["+(i-2)+"].valor",cell.innerHTML,"");
	                break;
	            }
	        }
        }
    }
    return true;
}

<logic:notEmpty name="ProcessForm" property="process.items">
    var tablaC=$("columna");
    var index=0;
    <logic:iterate id="items" name="ProcessForm" property="process.items">
        var fila=tablaC.insertRow( index+2 );
        idcolumna[index]="${items.idcolumna}";
        var cell=fila.insertCell(0);
            cell.innerHTML="${items.orden}";
        cell=fila.insertCell(1);
            cell.innerHTML="${items.nombre}";
        cell=fila.insertCell(2);
            cell.innerHTML="${items.ancho}";
        cell=fila.insertCell(3);
            cell.innerHTML="${items.alineacion}";
        cell=fila.insertCell(4);
            cell.innerHTML="${items.sumariza}";
        cell=fila.insertCell(5);
            cell.innerHTML="${items.pie}";
        cell=fila.insertCell(6);
            cell.innerHTML="${items.tipo}";
        cell=fila.insertCell(7);
            cell.innerHTML="${items.formato}";
        cell=fila.insertCell(8);
            cell.innerHTML="${items.muestra}";
        addButtons( fila.insertCell(9), modificarColumna, borrarColumna, index+2 );
        index++;
    </logic:iterate>
    var tablaF=$("filtro");
    var index=0;
    <logic:iterate id="items" name="ProcessForm" property="process.items">
    <logic:notEqual name="items" property="comparador" value="0">
        var fila=tablaF.insertRow( index+2 );
        var cell=fila.insertCell(0);
            cell.innerHTML="${items.nombre}";
        cell=fila.insertCell(1);
            cell.innerHTML="${items.etiqueta}";
        cell=fila.insertCell(2);
            cell.innerHTML="${items.expresion}";
        cell=fila.insertCell(3);
            cell.innerHTML="${items.tabla}";
        cell=fila.insertCell(4);
            cell.innerHTML="${items.comparador}";
        cell=fila.insertCell(5);
            cell.innerHTML="${items.obligatorio}";
        cell=fila.insertCell(6);
            cell.innerHTML="${items.idfiltro}";
        cell=fila.insertCell(7);
            cell.innerHTML="${items.valor}";
        addButtons( fila.insertCell(8), modificarFiltro, borrarFiltro, index+2 );
        index++;
    </logic:notEqual>
    </logic:iterate>

</logic:notEmpty>
</script>
