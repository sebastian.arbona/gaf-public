<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
<div class="title">
	Etapa
</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<logic:equal parameter="do" value="load">
		<html:hidden property="entity.nuevo" value="false" />
	</logic:equal>
	<table>
		<tr>
			<th>ID: </th>
			<td>
				<html:text property="entity.idEstado" readonly="true" />
			</td>
		</tr>
		<tr>
			<th>Nombre del Estado:</th>
			<logic:equal name="AbmForm" property="entity.id" value="0">
				<td><html:text property="entity.nombreEstado"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="0">
				<td>${AbmForm.entity.nombreEstado}</td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Color del Estado: </th>
			<td><html:text property="entity.color" styleId="color" /></td>
		</tr>
		<tr>
			<th>
				Manual:
			</th>
			<td>
				<asf:select name="entity.manual"
					value="${AbmForm.entity.manual}" listCaption="SI,NO"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Tipo de estado:
			</th>
			<td>
				<asf:select name="entity.tipo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipo}" 
					listCaption="codigo" listValues="codigo"
					filter="categoria = 'estado'  order by codigo"
					nullValue="true" nullText="Seleccione tipo de estado..." />
			</td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.selectboxes.min.js"></script>
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.colorPicker.js"/></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/colorPicker.css" type="text/css" />
<script language='javascript'>
	//Control sobre el color.-
	jQuery(document).ready(function($)
	{
		$('#color').colorPicker();
	});
</script>