<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<div class="title">Ingrese los Siguientes Datos:</div>
<br>
<br>

<html:form
	action="actions/process.do?processName=NotificacionesListProcess&do=process"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.action"
		styleId="accion" />

	<tr>
		<th>Fecha de Notificacion</th>
		<td><asf:calendar property="ProcessForm.process.fechaCreadaStr"
				readOnly="${ProcessForm.process.fechaCreadaStr}" /></td>
		<th>Fecha de vencimiento de la obligacion</th>
		<td><asf:calendar property="ProcessForm.process.fechaVencStr"
				readOnly="${ProcessForm.process.fechaVencStr}" /></td>
	</tr>
	<table>
		<div>
			<tr>
				<th>Observaciones</th>
				<td colspan="7"><html:textarea name="ProcessForm"
						property="process.observaciones" cols="40" rows="5" /></td>
			</tr>
		</div>
		<tr>
			<th>Tipo de Notificación:</th>
			<td colspan="7"><asf:select name="process.idTipoAviso"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getDescripcion" listValues="getId"
					value="${ProcessForm.process.idTipoAviso}"
					filter="categoria='notificacion.TipoAviso' order by codigo" /></td>
		</tr>
		<tr>
			<th>Tipo Mail:</th>
			<td colspan="7"><asf:select name="process.idConf"
					entityName="com.nirven.creditos.hibernate.ConfiguracionNotificacion"
					listCaption="getDenominacion" listValues="getId"
					value="${ProcessForm.process.idConf}" /></td>
		</tr>
		<tr>
			<th>Emitir Boleto:</th>
			<td colspan="7"><html:checkbox property="process.emitir"
					name="ProcessForm" /></td>
		</tr>
		<tr>
			<th>Notifica Via Mail:</th>
			<td><html:checkbox property="process.mail" name="ProcessForm" />
			</td>
			<th>Notifica a Garantes:</th>
			<td><html:checkbox property="process.terceros"
					name="ProcessForm" /></td>
			<%--		<th>--%>
			<%--			Notifica a Fiador:--%>
			<%--		</th>--%>
			<%--		<td>--%>
			<%--			<html:checkbox property="process.fiador" name="ProcessForm"/>--%>
			<%--		</td>--%>
		</tr>
	</table>
	<input type="button" value="Generar" onclick="generar();">
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
	function generar() {
		var accion = document.getElementById('accion');
		accion.value = "guardar";
		var formulario = document.getElementById('oForm');
		formulario.submit();
	}
</script>