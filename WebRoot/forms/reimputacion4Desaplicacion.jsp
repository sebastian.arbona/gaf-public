<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="title">Reimputaci&oacute;n de Pagos y Cr&eacute;ditos</div>
<html:form
	action="/actions/process.do?do=process&processName=Reimputacion"
	styleId="ProcessForm">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="desaplicar"
		styleId="action" />
	<html:hidden property="process.forward" styleId="processForward" />
	<html:errors />
	<table>
		<tr>
			<th>Recaudacion</th>
			<th>Importe</th>
		</tr>
		<logic:iterate id="rs" name="ProcessForm"
			property="process.seleccionadas">
			<tr>
				<td>${rs.recaudacion}</td>
				<td>${rs.importeStr}</td>
			</tr>
		</logic:iterate>
		<tr>
			<th>Total</th>
			<td>${ProcessForm.process.totalRecaudacionesStr}</td>
		</tr>
		<tr>
			<th>Proyecto</th>
			<td>${ProcessForm.process.credito.numeroAtencion} -
				${ProcessForm.process.credito.persona.nomb12}</td>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm"
		property="process.predesaplicaciones">
		<h5 style="margin-top: 15px">Desaplicaci&oacute;n</h5>
		<div class="grilla">
			<display:table name="ProcessForm" property="process.desaplicaciones"
				export="false" id="reportTable" width="100%"
				requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process">
				<display:column title="Comprobante"
					property="descripcionComprobante" />
				<display:column title="Importe Total Pagado" class="right">
					<logic:equal name="reportTable" property="comprobante" value="true">
						<strong><fmt:formatNumber maxFractionDigits="2"
								minFractionDigits="2" groupingUsed="true"
								value="${reportTable.importeTotal}" /></strong>
					</logic:equal>
				</display:column>
				<display:column title="Fecha Proceso">
					<logic:equal name="reportTable" property="comprobante" value="true">
						<fmt:formatDate pattern="dd/MM/yyyy" type="date"
							value="${reportTable.fechaProceso}" />
					</logic:equal>
				</display:column>
				<display:column title="Fecha Pago">
					<logic:equal name="reportTable" property="comprobante" value="true">
						<fmt:formatDate pattern="dd/MM/yyyy" type="date"
							value="${reportTable.fechaPago}" />
					</logic:equal>
				</display:column>
				<display:column title="Cuota Aplicada" property="numeroCuota" />
				<display:column title="Total a Desaplicar" class="right">
					<strong><fmt:formatNumber maxFractionDigits="2"
							minFractionDigits="2" groupingUsed="true"
							value="${reportTable.importeDesaplicar}" /></strong>
				</display:column>
				<display:column title="Capital" class="right">
					<logic:notEqual name="reportTable" property="capital" value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.capital}" />
					</logic:notEqual>
				</display:column>
				<display:column title="Comp." class="right">
					<logic:notEqual name="reportTable" property="compensatorio"
						value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.compensatorio}" />
					</logic:notEqual>
				</display:column>
				<display:column title="Morat." class="right">
					<logic:notEqual name="reportTable" property="moratorio" value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.moratorio}" />
					</logic:notEqual>
				</display:column>
				<display:column title="Punit." class="right">
					<logic:notEqual name="reportTable" property="punitorio" value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.punitorio}" />
					</logic:notEqual>
				</display:column>
				<display:column title="Gastos Adm." class="right">
					<logic:notEqual name="reportTable" property="gastos" value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.gastos}" />
					</logic:notEqual>
				</display:column>
				<display:column title="Gastos Rec." class="right">
					<logic:notEqual name="reportTable" property="gastosRec" value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.gastosRec}" />
					</logic:notEqual>
				</display:column>
				<display:column title="Multas" class="right">
					<logic:notEqual name="reportTable" property="multas" value="0">
						<fmt:formatNumber maxFractionDigits="2" minFractionDigits="2"
							groupingUsed="true" value="${reportTable.multas}" />
					</logic:notEqual>
				</display:column>
			</display:table>
			<div>
				<div style="float: right">
					<html:submit value="Volver" onclick="anterior();" />
					<html:submit value="Pre-aplicar" onclick="siguiente();" />
				</div>
			</div>
		</div>
	</logic:notEmpty>
</html:form>

<script type="text/javascript">
	//var form = $('ProcessForm');
	var action = $('action');
	var processForward = $('processForward');

	function anterior() {
		processForward.value = 'reimputacion3Predesaplicacion';
		action.value = 'volver';
	}

	function siguiente() {
		action.value = 'preaplicar';
	}
</script>