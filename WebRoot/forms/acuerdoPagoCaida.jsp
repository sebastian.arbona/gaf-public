
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<div class="title">Ca�da de Acuerdo de Pago</div>
<br/>
<html:form action="/actions/process.do?do=process&processName=AcuerdoPagoCaida" styleId="oForm">
	<html:hidden property="process.action" value="guardar" styleId="accion"/>
	<html:hidden property="process.cid" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	
	<table width="100%">
		<tr>
			<th>Proyecto</th>
			<td>${ProcessForm.process.credito.numeroAtencion}</td>
			<th>Expediente</th>
			<td>${ProcessForm.process.credito.expediente}</td>
		</tr>
		<tr>
			<th>Titular</th>
			<td>${ProcessForm.process.credito.persona.nomb12}</td>
			<th>CUIL/CUIT</th>
			<td>${ProcessForm.process.credito.persona.cuil12}</td>
		</tr>
	</table>
	
	<table width="100%" style="margin-top: 30px">
		<tr><th>Cr�ditos Originales</th></tr>
	</table>
		
	<display:table name="ProcessForm" property="process.originales" id="credito" width="100%">
		<display:column title="Cr�dito N�" property="numeroAtencion" />
		<display:column title="L�nea" property="linea.nombre" />
		<display:column title="Expediente N�" property="expediente" />
		<display:column title="Deuda">
			${ProcessForm.process.deudas[credito_rowNum - 1]}
		</display:column>
		<display:column title="Estado" property="estadoActual.estado.nombreEstado" />
	</display:table>
	
	<logic:notEmpty name="ProcessForm" property="process.boletos">
		<table width="100%" style="margin-top: 30px">
			<tr><th>Pagos a Reimputar</th></tr>
		</table>
		
		<display:table name="ProcessForm" property="process.boletos" id="boleto" width="100%">
			<display:column property="numeroBoleto" title="Boleto"/>
			<display:column property="fechaEmision" title="Fecha" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column property="importe" title="Importe" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
			<display:column title="Aplicar a">
				<select name="pago-${boleto.id}">
					<logic:iterate id="credito" name="ProcessForm" property="process.originales">
						<option value="${credito.id}">${credito.numeroAtencion} - ${credito.expediente}</option>
					</logic:iterate>
				</select>
			</display:column>
		</display:table>
	</logic:notEmpty>
	
	<div style="margin-top: 20px">
		<table>
			<tr>
				<th>Fecha de Ca&iacute;da</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaCaida" value="${process.fechaCaida}"/>
				</td>
			</tr>
		</table>
	</div>
	
	<logic:equal name="ProcessForm" property="process.guardado" value="false">
		<div style="margin-top: 30px;">
			<input type="button" value="Revertir Acuerdo de Pago" onclick="guardar();"/>
			<input type="button" value="Volver" onclick="history.go(-1);"/>
		</div>
	</logic:equal>
</html:form>
<script type="text/javascript">
	
	function guardar() {
		var r = confirm("�Desea revertir el acuerdo de pago?");
		if (r == true) {
			document.getElementById('oForm').submit();
			return true;
		}
		return false;
	}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>