<%@ page language="java"%>
<%@ page import="com.asf.grh.hibernate.*" %>
<%@ page import="com.asf.security.BusinessPersistance" %>
<%@ page import="com.asf.security.SessionHandler" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
	
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/functions.js"></script>
	
<style>
#page-content-wrapper {
	padding-top: 0px;
}
.container {
	max-width: 100%;	
}
.fileHide {
	display: none;
}
footer {
	display: none;
	
}
</style>

<h3 class="title" id="title_ArchivoLegajoDigitalLegajoDigital">Foto de Perfil</h3>

<html:form action="/actions/ArchivoAction.do?do=legajoDigitalFoto" enctype="multipart/form-data">
	<input type="hidden" id="legajoDigital_id" name="legajoDigital_id" value="${param.idLegajoDigital}">	
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
        	<button type="button" class="btn btn-default new-usuario" id="browse-file" onclick="fireFileClick()">
				<i class="fa fa-search" aria-hidden="true"></i>&nbsp;Examinar Archivo...
			</button>
			<input type="text" name="uploader_filename" id="uploader_filename" value=""/> 
        	<html:file property="theFile" styleId="theFile" styleClass="inputfile inputfile-5 fileHide"/>
        </div>
    </div> 
       
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 align-right">
			<div class="form-group"> 	
				<html:submit styleClass="btn btn-guardar"><bean:message key="abm.button.save"/></html:submit>
				<html:cancel styleClass="btn btn-cancelar"><bean:message key="abm.button.cancel" /></html:cancel>
			</div>
		</div>
	</div>
	
</html:form>   

<script type="text/javascript">
function fireFileClick(){
	$j('#theFile').click();
}
$j('#theFile').on('change', function(){
	$j('#uploader_filename').val($j('#theFile').val());
});
var errores="${ArchivoForm.entity.chErrores}";
console.log(errores);
</script>
