<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">
	Administraci�n de Requisitos
</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">


		<tr>
			<th>
				Id:
			</th>
			<td>
				<asf:text name="AbmForm" property="entity.id" type="long"
					maxlength="8" readonly="true" />
			</td>
		</tr>
		<tr>
			<th>
				Nombre:
			</th>
			<td>
				<html:text property="entity.nombre" maxlength="30" />
			</td>
		</tr>
		<tr>
			<th>
				Descripci�n:
			</th>
			<td>
				<html:textarea property="entity.descripcion" cols="100" rows="6" />
			</td>
		</tr>

		<tr>
			<th>
				Obligatorio:
			</th>
			<td>
				<asf:select listCaption="Si,No" listValues="true,false"
					name="entity.obligatorio" value="${AbmForm.entity.obligatorio}" />
			</td>
		</tr>

		<tr>
			<th>
				Orden:
			</th>
			<td>
				<asf:text name="AbmForm" property="entity.orden" type="long"
					maxlength="4" value="${AbmForm.entity.orden}"/>
			</td>
		</tr>

		<tr>
			<th>
				Tipo:
			</th>
			<td>
				<asf:select name="entity.tipo" attribs="class='tipo'"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipo}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'requisito.tipo'  order by codigo" />
			</td>
		</tr>

		<tr>
			<th>
				L�nea Credito:
			</th>
			<td>
				<asf:select name="entity.lineaStr"
					entityName="com.nirven.creditos.hibernate.Linea"
					value="${AbmForm.entity.lineaStr}"
					listCaption="getId,getNombre" listValues="getId"
					attribs="class='linea'"
					filter="activa = true order by id"
					nullValue="true" nullText="Seleccione l�nea..." />
			</td>
		</tr>
		
		 <tr>
        	<th>Tipo de Persona:</th>
        	<td colspan="3">
        		<asf:select attribs="OnChange='tipo(this.options[this.selectedIndex].value)'" 
        		name="entity.tipoPersona" 
        		listCaption="F�sica,Jur�dica,Ambos" 
        		listValues="F,J,A" 
        		value="${AbmForm.entity.tipoPersona}"/>
        	</td>
        </tr>
		<tr>
			<th>
				Familia:
			</th>
			<td>
				<asf:select name="entity.familia" attribs="class='familia'"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.familia}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'requisito.familia'  order by codigo" />
			</td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>

<script language="JavaScript" type="text/javascript">
 jQuery.noConflict();
jQuery(document).ready(function($) {

	jQuery('.tipo').change(function() {
		
		deshabilitar_linea();
	});

	function deshabilitar_linea() {
	
		if (jQuery('.tipo').val() == 'L')
			jQuery('.linea').removeAttr("disabled");
		else
			jQuery('.linea').attr("disabled", true);
	}

	deshabilitar_linea();

});
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<!-- Raul Varela -->