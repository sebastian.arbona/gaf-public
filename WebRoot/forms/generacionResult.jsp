<%@ page language="java"%>
<%@page import="com.asf.grh.hibernate.*"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Hashtable"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.text.NumberFormat"%>
<%@page import="com.asf.security.BusinessPersistance"%>
<%@page import="com.asf.security.SessionHandler"%>
<div class="title">
  Resultado de Impacto en Cuenta Corriente
</div>
<br>
<br>
<div class="grilla">
  <%
      Long oKey = null;
      HashMap oErrores = (HashMap) request.getAttribute("errores");
      if (oErrores != null && oErrores.size() != 0) {
          Vector vErrors = new Vector(oErrores.keySet());
          Collections.sort(vErrors);
  %>
  <br />
  <table style="border: 1px black solid">
  <%
        Object osKey = null;
            for (Enumeration e = vErrors.elements(); e.hasMoreElements();) {
                osKey = e.nextElement();
                out.write("<tr style='border: 2px solid black'>");
                if (oErrores.get(new Long(osKey.toString())) != null)
                    out.write("<td>" + oErrores.get(new Long(osKey.toString())).toString() + "</td>");
                else
                    out.write("<td></td>");
                out.write("</tr>");
            }
    %>
  </table>
  <%
      }
  %>
</div>
<br/>
<div>
	<logic:equal name="ProcessForm" property="process.generada" value="true">
		<input type="button" value="Imprimir Liquidacion" onclick="irAImpresionMasiva()">
	</logic:equal>
	<div class="grilla">
	<display:table name="ProcessForm" property="process.impactadas" export="true" id="reportTable" class="com.nirven.creditos.hibernate.Emideta"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=exportar">
		<display:setProperty name="report.title" value="Detalle de Emisi�n"></display:setProperty>
		<display:column property="boleto.numeroBoleto" title="Nro. Boleto"/>
		<display:column property="credito.numeroAtencion" title="Proyecto"/>
		<display:column property="numero" title="Nro. Cuota"/>
		<display:column property="cuota.fechaVencimiento" title="F. Vto." decorator="com.asf.displayDecorators.DateDecorator"/>
		<display:column property="credito.linea.nombre" title="Linea"/>
		<display:column property="credito.linea.moneda.simbolo" title="Moneda"/>
		<display:column property="credito.persona.id" title="Nro. Tit."/>
		<display:column property="emision.fechaEmision" title="Emisi�n/Proceso" decorator="com.asf.displayDecorators.DateDecorator"/>
	    <display:column property="importeCuota" title="Importe Cuota" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
	    <display:column bgcolor="#939598" > </display:column>
	    <display:column property="capital" title="Capital" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	    <display:column property="compensatorio" title="Interes Comp." decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	    <display:column property="tasaCompensatorio" title="Tasa Comp." decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	    <display:column title="Bonif." decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
	    	<logic:equal name="reportTable"  property="bajaEstadoBonificacion" value="false">
				${reportTable.bonificacionStr}
		    </logic:equal>
		    <logic:notEqual name="reportTable"  property="bajaEstadoBonificacion" value="false">
		    		0,00
		    </logic:notEqual>
	   </display:column>
	   <display:column property="gastos" title="Gastos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	<display:column property="gastosRecuperar" title="Gastos Rec" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	</display:table>
</div>
</div>
<script>
function irAImpresionMasiva() {
	window.location = 
	 	'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=ImpresionMasivaBoleto' +
        '&process.accion=seleccionar' +
        '&process.emision.id=${ProcessForm.process.idEmision}';       
}
</script>