<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Acuerdo 2988 - Articulo 14 inc. C</div>
<html:form
	action="/actions/process.do?do=process&processName=Anexo14"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="buscar" styleId="action"/>
	<html:errors/>

	
	<table>
		<tr>
			<th>Fecha desde</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th>Fecha hasta</th>
			<td>
				<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th>Moneda</th>
			<td>
				<asf:select name="process.idMoneda"
						entityName="com.asf.gaf.hibernate.Moneda"
						listCaption="getAbreviatura" listValues="getId"
						value="ProcessForm.process.idMoneda" />
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Listado en pantalla"></html:submit>
			</th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.beans">
	<div class="grilla">
		<display:table name="ProcessForm" property="process.beans" id="bean" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do">
			<display:caption>Acuerdo 2988 - Articulo 14 inc. C</display:caption>
			<display:column title="Per&iacute;odo de presentaci&oacute;n" property="periodo" sortable="true" />
			<display:column title="Tipo de Ingreso" property="concepto" sortable="true" />
			<display:column title="Saldo inicial del Per&iacute;odo" property="saldoInicial" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" class="right"/>
			<display:column title="Total D&eacute;bitos del Per&iacute;odo" property="debitos" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" class="right"/>
			<display:column title="Total Cr&eacute;ditos del Per&iacute;odo por Recaudaci&oacute;n" property="creditosPagos" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" class="right"/>
			<display:column title="Total Cr&eacute;ditos del Per&iacute;odo" property="creditos" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" class="right"/>
			<display:column title="Saldo Final del Per&iacute;odo" property="saldoFinal" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" class="right"/>
		</display:table>
	</div>
	</logic:notEmpty>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>