<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Importes por Concepto</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

<html:hidden property="entity.id"/>
<table border="0">
<tr><th>Aplicaci&oacute;n</th><td>
	<html:select property="entity.aplicacionStr" name="AbmForm">	
		<html:option value="INGRESOS_VARIOS">Ingresos Varios</html:option>
		<html:option value="NOVEDADES_CTACTE">Novedades en Cuenta Corriente</html:option>
	</html:select>
</td></tr>
<tr><th>Concepto:</th><td>
		<asf:select entityName="com.nirven.creditos.hibernate.CConcepto" listCaption="abreviatura,detalle"
 			listValues="concepto" name="entity.conceptoId" value="${AbmForm.entity.conceptoId}" 
 			nullValue="true" nullText="&lt;&lt;Seleccione Concepto&gt;&gt;"  
 			/>
</td></tr>
<tr><th>Tipo de Concepto:</th><td>
		<asf:select name="entity.tipoConcepto" 
			entityName="com.asf.hibernate.mapping.Tipificadores" 
			value="${AbmForm.entity.tipoConcepto}" 
			listCaption="getCodigo,getDescripcion" listValues="getCodigo" 
			filter="categoria = 'ctacte.detalle' order by codigo"  
			/> 
</td></tr>
<tr><th>Importe asociado el tipo de concepto:</th><td>
	<asf:text maxlength="20" type="decimal" property="entity.importe" name="AbmForm"></asf:text>
</td></tr>
<tr><th>Fecha de vencimiento del importe asociado:</th><td>
	<asf:calendar property="AbmForm.entity.vencimientoStr" value="${entity.vencimiento}"></asf:calendar>
</td></tr>
<tr><th>Plazo de vencimiento:</th><td>
	<asf:text maxlength="20" type="decimal" property="entity.plazoVtoCon" name="AbmForm"></asf:text>
</td></tr>
<tr><th>Visible en la web:</th><td>
	<html:checkbox property="entity.web"></html:checkbox>
</td></tr>
<tr><th>Carga manual:</th><td>
	<html:checkbox property="entity.manual"></html:checkbox>
</td></tr>
</table>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>