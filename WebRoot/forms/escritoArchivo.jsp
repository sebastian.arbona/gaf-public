<%@ page language="java"%>
<%@ page import="com.asf.security.BusinessPersistance"%>
<%@ page import="com.asf.security.SessionHandler"%>
<%@page import="com.civitas.hibernate.persona.*"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/EscritoArchivoAction.do?do=save&entityName=${param.entityName}"
	enctype="multipart/form-data">
	<br />
	<br />
	<html:hidden property="entity.id" />
	<table border="0">
		<tr>
			<th>
				Nombre:
			</th>
			<td>
				<html:text property="entity.nombre" maxlength="30" />
			</td>
		</tr>
		<tr>
			<th>
				Detalle:
			</th>
			<td>
				<html:textarea styleId="entity.detalle"
					onkeypress="caracteres(this,50,event)" property="entity.detalle"
					cols="40" rows="4" />
			</td>
		</tr>
		<tr>
			<th>
				Tipo:
			</th>
			<td>
				<asf:select name="entity.tipo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipo}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'escrito.tipo'  order by codigo" />
			</td>
		</tr>
		<tr>
			<th>
				Documento:
			</th>
			<td colspan="3">
				<html:file property="theFile" />
			</td>
		</tr>
		<logic:present parameter="load">
			<html:hidden property="entity.archivo" />
			<html:hidden property="entity.mimeType" />

			<tr>
				<th>
					Descarga:
				</th>
				<td colspan="3">
					<html:button
						onclick="window.open('${pageContext.request.contextPath}/actions/EscritoArchivoAction.do?do=descargar&id=${AbmForm.entity.id}')"
						property="">Bajar Documento</html:button>
				</td>
			</tr>
		</logic:present>
	</table>
	<asf:security action="/actions/EscritoArchivoAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>