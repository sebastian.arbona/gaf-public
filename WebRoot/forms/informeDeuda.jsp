<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 
																							
<div class="title" style="margin-bottom: 30px">Informe de Deuda</div>
<html:form action="/actions/process.do?do=process&processName=InformeDeudaProcess" styleId="ProcessForm">
	<html:errors/>

	<table style="margin-top: 30px; width: 100%">
		<tr>
			<th>Fecha de creaci&oacute;n</th>
			<td>
				<asf:calendar property="ProcessForm.process.informe.fechaCreacion" readOnly="true"/>
			</td>
			<th>Estado</th>
			<td>
				${ProcessForm.process.informe.estado.texto}
			</td>
		</tr>
		<tr>
			<th>Fecha de c&aacute;lculo</th>
			<td><asf:calendar property="ProcessForm.process.informe.fechaHasta" readOnly="true"/></td>
			<th>Fecha de ejecuci&oacute;n</th>
			<td>
				<asf:calendar property="ProcessForm.process.informe.fechaEjecucion" readOnly="true"/>
			</td>
		</tr>
		<tr>
			<th>L&iacute;neas procesadas</th>
			<td colspan="3">
				${ProcessForm.process.informe.lineasDesc}
			</td>
		</tr>
	</table>
	
	<logic:notEmpty name="ProcessForm" property="process.detalles">
		<div class="grilla" style="width: 100%; overflow-x: scroll">
			<display:table id="detalle" style="margin-top: 30px; width: 100%" name="ProcessForm" property="process.detalles" export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do">
				<display:setProperty name="export.excel.filename" value="${ProcessForm.process.nombreArchivo}"></display:setProperty>
				<display:setProperty name="report.title" value="Informe de Deuda"></display:setProperty>
				<display:column title="Fecha Vencimiento" property="fechaVencimiento" decorator="com.asf.displayDecorators.DateDecorator"/>
				<display:column title="Nro Proyecto" property="credito.numeroAtencion"/>
				<display:column title="Detalle" property="detalle"/>
				<display:column title="Capital" property="capital" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Cer" property="cer" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Int. Comp." property="compensatorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Int. MyP" property="moratorioPunitorio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Gastos" property="gastos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Total Fondo" property="totalFondo" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Comisi�n" property="comision" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Env�o" property="envio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="IVA" property="iva" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Total Inv." property="totalInv" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" class="right"/>
				<display:column title="Titular" property="credito.persona.nomb12"/>
				<display:column title="Fecha C�lculo" property="fechaCalculo" decorator="com.asf.displayDecorators.DateDecorator"/>
				<display:column title="Estado" property="credito.etapa"/>
				<display:column title="Comportamiento" property="credito.comportamientoActual"/>
				<display:column title="ID Titular" property="credito.persona.idpersona"/>
				<display:column title="CUIT" property="credito.persona.cuil12"/>
			</display:table>
		</div>
	</logic:notEmpty>
</html:form>

<html:form action="/actions/informeEnte.do" styleId="InformeEnteRecaudadorForm">
	<input type="hidden" id="do" name="do"/>
	<html:hidden property="idInforme" value="${ProcessForm.process.idInforme}"/>
	
	<table style="margin-top: 30px; width: 100%">
		<tr>
			<th colspan="2">Generar Informe en TXT</th>
		</tr>
		<tr>
			<th>Fecha de vigencia</th>
			<td>
				<asf:calendar property="InformeEnteRecaudadorForm.fechaVigencia"/>
			</td>
		</tr>
		<tr>
			<th>Tipo de informe</th>
			<td>
				<select id="tipoInforme" name="tipoInforme" onchange="javascript:actTipoReg();">
					<option value="DT">Deuda Total</option>
					<option value="AC" selected="selected">Actualizaci�n</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>Acci�n</th>
			<td>
				<div id="tipoRegistroABM">
					<select name="tipoRegistros">
						<option value="A">Alta</option>
						<option value="B">Baja</option>
						<option value="M">Modificaci�n</option>
					</select>
				</div>
				<div id="tipoRegistroA" style="display: none">
					<select name="tipoRegistros">
						<option value="A" selected="selected">Alta</option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<th>Generar Informe Deuda</th>
			<td><html:submit value="Exportar a TXT" onclick="imprimirInforme();"/></td>
		</tr>
	</table>
</html:form>
<script>
var form = $('InformeEnteRecaudadorForm');
var doField = $('do');
function imprimirInforme(){
	doField.value = 'generarDeudaTotal';
	return true;
}

function actTipoReg() {
	var tipoInforme = $('tipoInforme');
	if (tipoInforme.value == 'DT') {
		document.getElementById('tipoRegistroABM').style.display = 'none';
		document.getElementById('tipoRegistroA').style.display = 'block';
	} else {
		document.getElementById('tipoRegistroABM').style.display = 'block';
		document.getElementById('tipoRegistroA').style.display = 'none';
	}
}
</script>

<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>