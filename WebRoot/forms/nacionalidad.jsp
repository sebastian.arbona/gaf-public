<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Nacionalidad</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

<table border="0">
<tr><th>C�digo:</th><td><html:text property="entity.id" readonly="true"/></td></tr>
<tr><th>Nombre:</th><td><html:text property="entity.nacionalidad" maxlength="50"/></td></tr>
</table>

<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>

