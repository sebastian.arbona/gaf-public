<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>

<script type="text/javascript">
	window.location = '${pageContext.request.contextPath}${ProcessForm.process.forwardURL}';
</script>

<html:errors/>

<a href="javascript:history.go(-1)">Volver</a>