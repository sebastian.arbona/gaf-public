<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Barrio</div>
<br>

<%--><html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}"><--%>
<html:form action="/actions/${ empty param.altaRapida ? 'abmAction' : 'altaRapidaGenericaAction' }.do?do=save&entityName=${param.entityName}${ empty param.altaRapida ? '' : '&forward=BarrioAltaRapida' }">
    
<table border="0">

<tr><th>C�digo:</th><td><html:text property="entity.id" readonly="true"/></td></tr>
<tr><th>Descripci�n:</th><td><html:text property="entity.descBrr" maxlength="30"/></td></tr>
<tr><th>Abreviatura:</th><td><html:text property="entity.abrBrr" maxlength="15"/></td></tr>
</table>

<asf:security action="/actions/${ empty param.altaRapida ? 'abmAction' : 'altaRapidaGenericaAction' }.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>	
	
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>


