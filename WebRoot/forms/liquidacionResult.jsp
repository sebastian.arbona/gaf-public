<%@ page language="java"%>
<%@page import="com.asf.grh.hibernate.*"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Hashtable"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Vector"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.text.NumberFormat"%>
<%@page import="com.asf.security.BusinessPersistance"%>
<%@page import="com.asf.security.SessionHandler"%>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	>
<div class="title">
  Resultado de Liquidación
</div>
<br>
<br>
<div class="grilla">
  <%
      Long oKey = null;
      HashMap oErrores = (HashMap) request.getAttribute("errores");
      if (oErrores != null && oErrores.size() != 0) {
          Vector vErrors = new Vector(oErrores.keySet());
          Collections.sort(vErrors);
  %>
  <br />
  <table style="border: 1px black solid">
    <tr>
      <td>
<!--        Estado-->
      </td>
    </tr>
    <%
        Object osKey = null;
            for (Enumeration e = vErrors.elements(); e.hasMoreElements();) {
                osKey = e.nextElement();
                out.write("<tr style='border: 2px solid black'>");
                if (oErrores.get(osKey) != null)
                    out.write("<td>" + oErrores.get(osKey).toString() + "</td>");
                else
                    out.write("<td></td>");
                out.write("</tr>");
            }
    %>
  </table>
  
  <html:errors/>
  <%
      }
  %>
</div>
<br>
<br>
<input type="button" value="Ver Detalle" onclick="emisiones();">

</html:form>
<script>
var url = "";
function emisiones(){
//       url += "${pageContext.request.contextPath}/actions/emisionAction.do?do=list&entityName=Emision";
       url += "${pageContext.request.contextPath}/actions/emidetaAction.do?do=list&idEmision=${ProcessForm.process.emision.id}&entityName=Emideta";
       window.location = url;
}
</script>
