<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<div class="title">Bonificación</div>
<br>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<html:hidden property="entity.id" />
		<tr>
			<th>Nombre:</th>
			<td><html:text property="entity.nombre" styleId="nombre"></html:text>
			</td>
		</tr>
		<tr id="enteBonificador">
			<th>Ente Bonificador:</th>
			<td><asf:select entityName="com.asf.gaf.hibernate.Bancos"
					listCaption="getDetaBa" listValues="getCodiBa"
					name="entity.enteBonificador_id"
					value="${AbmForm.entity.enteBonificador_id}"
					filter="bonificador = 1  order by codiBa" /></td>
		</tr>
		<tr>
			<th>Convenio de bonificación:</th>
			<td><asf:lselect property="entity.idConvenio"
					entityName="ConvenioBonificacion" listCaption="nombre"
					listValue="id" value="${AbmForm.entity.idConvenio}"
					linkFK="banco.codiBa" linkName="entity.enteBonificador_id"
					orderBy="nombre" nullValue="true" /></td>
		</tr>

		<tr>
			<th>Tipo de Amortización:</th>
			<td><asf:select name="entity.tipoAmortizacion"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoAmortizacion}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria='amortizacion.metodo' order by codigo" /></td>
		</tr>
		<tr>
			<th>Tipo de Cálculo:</th>
			<td><asf:select name="entity.tipoDeBonificacion"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoDeBonificacion}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria='bonificacion.tipo' order by codigo" /></td>
		</tr>
		<tr>
			<th>Valor de bonificación:</th>
			<td><html:text property="entity.tasaBonificadaStr"
					onkeypress="sinLetras(event)" styleId="tasaBonificadaStr"
					onchange="verificarPorcentaje(tasaBonificadaStr);"></html:text></td>
		</tr>
		<tr>
			<td><h3>Controles</h3></td>
		</tr>
		<tr>
			<th>Tasa neta no inferior a:</th>
			<td><html:text property="entity.minimoInteresStr"
					onkeypress="sinLetras(event)" styleId="minimoInteresStr"
					onchange="verificarPorcentaje(minimoInteresStr);"></html:text></td>
		</tr>
		<tr>
			<th>Concepto Aforo:</th>
			<td><asf:select
					entityName="com.nirven.creditos.hibernate.CConcepto"
					listCaption="concepto,abreviatura" listValues="getId"
					name="entity.conceptoAforo_id"
					value="${AbmForm.entity.conceptoAforo_id}" /></td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<!-- Raul Varela -->

<script type="text/javascript">

	var bonificacion = $('bonificacionInterna');
	var enteBonificador = $('enteBonificador');
	var enteBonificado = $('enteBonificado');
	
	function cambiarComboEnteBonificador(){
		if(bonificacion.value == 'si'){
			enteBonificador.style.display ="";
			enteBonificado.style.display = "none";		
		}else{
			enteBonificador.style.display ="none";
			enteBonificado.style.display = "";
		}
	}
	
	function verificarPorcentaje(porcentaje){
		var tasa = porcentaje.value.replace(',', '.').replace('%', '');
		if(tasa < 0 || tasa > 100){
			alert("Debe ingresar un valor entre 0 y 100");
			porcentaje.value = '0,00%';
		}else{
			porcentaje.value = porcentaje.value.concat('%');
		}
	}
	
	function verificarPrecio(monto){
		var precio = monto.value.replace(',','.').replace('$','');
		if(precio < 0){
			alert("Debe ingresar un valor mayor que cero.");		
			monto.value = '$0,00';
		}else{
			monto.value = '$' + precio;
		}
	}
	function sinLetras(e)
	{
	key=(document.all) ? e.keyCode : e.which;

	if (key < 48 || key > 57 )
	{
		if (key==8 || key==46 || key==44 ){
	
	return true;
	}
	else if(e.preventDefault) e.preventDefault();
	e.returnValue = false;
	return false;
	}
	}

</script>