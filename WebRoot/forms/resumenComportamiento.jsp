<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

<%@ page import="java.util.Date"%>
<%@ page import="com.asf.util.DateHelper"%>

<%pageContext.setAttribute("fechaHoy", DateHelper.getString(new Date()));%>

<div class="title">Resumen de Comportamiento seg�n la Categorizaci�n de los Proyectos</div>
<p>
<html:form
	action="/actions/process.do?do=process&processName=ResumenComportamiento"
	styleId="oForm">
	<html:hidden property="process.action" name="ProcessForm" styleId="action"/>
	
	<div id="linea" title="Linea de Credito" style="visibility: visible;">
		<table style="border: 2px solid rgb(204, 204, 204);" width="352" height="30">
			<tr>
				<th>
					Comportamiento:
				</th>
				<td>
					<asf:select name="process.codigo"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'comportamientoPago' order by codigo"
						value="${ProcessForm.process.codigo}" nullValue="true"
						nullText="Todos los estados"/>
				</td>
			</tr>
			<tr>
				<th>
					L�nea de Cr�dito:
				</th>
				<td>						
					<html:select styleId="idsLineas" property="process.idsLineas" multiple="true"  size="8">
						<html:optionsCollection  property="process.lineas" label="nombre" value="id"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<th>Fecha Hasta</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaHasta" 
						value="${process.fechaHasta}">
					</asf:calendar>
				</td>
			</tr>
		</table>
	</div>
		<input type="button" value="Listar" onclick="listar();">
	
	<br/><br/>
	<div class="grilla">
	<display:table name="ProcessForm" property="process.beans" export="true" id="reportTable" pagesize="50" 
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
		<display:column property="comportamientoPago" title="Comportamiento de Pago" sortable="true"></display:column>
		<display:column property="linea" title="Linea de Credito" sortable="true"></display:column>
		<display:column property="clasificacionMora" title="Moroso o Premora" sortable="true"></display:column>
		<display:column property="deudaExigible" title="Saldo Deuda" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="cantidad" title="Cantidad" sortable="true"></display:column>
		<display:column property="porcMonto" title="% Monto" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="porcCantidad" title="% Cantidad" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="montoValorCartera" title="$ CH/ En Cartera" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="deudaTotal" title="Deuda Total" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="porcDeuTot" title="% Deuda Total" sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
		<display:column property="total" title="Total Deuda - Cheques " sortable="true" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
	</display:table>
	</div>
</html:form>

	<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
	style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

	<script type="text/javascript">
		function listar(){ 
			var accion = document.getElementById('action');
			accion.value="listar";
			var formulario = document.getElementById('oForm');
			formulario.submit();
		}
	</script>