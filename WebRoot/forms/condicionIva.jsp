<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Condici�n de Iva</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<table border="0">
		<tr>
			<th>Id:</th>
			<td><asf:text name="AbmForm" property="entity.id" type="long"
					maxlength="8" readonly="true" /></td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><html:text maxlength="50" property="entity.nombre"
					name="AbmForm" value="${AbmForm.entity.nombre}" /></td>
		</tr>
		<tr>
			<th>Descripci�n:</th>
			<td><html:text maxlength="200" property="entity.descripcion"
					name="AbmForm" value="${AbmForm.entity.descripcion}" /></td>
		</tr>
		<tr>
			<th>Al�cuota:</th>
			<td><asf:text maxlength="5" type="decimal"
					property="entity.alicuota" name="AbmForm"></asf:text>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
