<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/displaytag.css"
	type="text/css" />

<div class="title">Errores</div>
<br>
<br>
<center>
	<div align="left" style="width: 70%">
		<html:errors />
	</div>
	<logic:present parameter="URL" scope="request">
		<div align="left" style="width: 70%">
			<ul>
				<bean:message key="cambio.de.clave" />
				<br>
				<br>
			</ul>
		</div>
		<bean:parameter name="URL" id="URL" />
		<logic:present parameter="BOTON" scope="request">
			<bean:parameter name="BOTON" id="BOTON" />
			<button onclick="window.location='${URL}'">${BOTON}</button>
		</logic:present>
		<logic:notPresent parameter="BOTON" scope="request">
			<button onclick="window.location='${URL}'">Volver</button>
		</logic:notPresent>
	</logic:present>
	<logic:notPresent parameter="URL">
		<button onclick="history.back();">Volver</button>
	</logic:notPresent>
</center>