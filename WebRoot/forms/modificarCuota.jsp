<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 15px">Modificacion de cuota</div>
	<html:form action="/actions/process.do?do=process&processName=CreditoCuentaCorriente" styleId="formulario">
		
		<html:hidden name="ProcessForm" property="process.idObjetoi" value="${ProcessForm.process.idObjetoi}" styleId="id"/>
		<html:hidden property="process.action" styleId="action" value="save"/>
		<logic:equal value="true" property="process.montoCero" name="ProcessForm">
		No se creo el Ajuste, el monto de movimiento debe ser mayor a 0
		</logic:equal>
		<table border="0">
    		<tr>
      			<th>
        		Cuota:
      			</th>
      			<td>
        			<asf:select entityName="com.nirven.creditos.hibernate.Cuota" 
        				listCaption="getNumero" listValues="getId"
          				name="process.cuotaId" 
          				value="${ProcessForm.process.cuotaId}"
          				filter=" credito.id = ${ProcessForm.process.idObjetoi}"
          				attribs="onchange='actualizarCuota();'"/>
        		</td>
        	</tr>
        	<tr>
        		<th>
        		Concepto:
      			</th>
      			<td>
      			   <asf:select name="process.concepto"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.concepto}"
					listCaption="getDescripcion" listValues="getCodigo"
					filter="categoria = 'cuota.concepto' order by codigo" 
					attribs="onchange='actualizarCuota();'"/>
        		</td>
    		</tr>
    		<tr>
    			<th>
    			Tipo:
    			</th>
    			<td>
    				<html:radio property="process.tipo" value="CR" onchange="actualizarCuota()" styleId="RadioCred">Cr�dito</html:radio>
    				<html:radio property="process.tipo" value="DB" onchange="actualizarCuota()" styleId="RadioDeb">D�bito</html:radio>
    			</td>
    		</tr>
    		<asf:security action="/actions/fechaManual.do" access="do=fechaManual">
    		<tr>
    			<th>
    			Fecha:
    			</th>
    			<td>
    				<asf:calendar property="ProcessForm.process.fechaMovimientoStr" value="${ProcessForm.process.fechaMovimientoStr}"/>
    			</td>
    		</tr>
    		</asf:security>
<%--    		<tr>--%>
<%--    			<th>--%>
<%--    				Tipo de movimiento:--%>
<%--    			</th>--%>
<%--    			<td>--%>
<%--      			   <asf:select name="process.concepto" entityName="com.asf.hibernate.mapping.Tipificadores"--%>
<%--					value="${ProcessForm.process.tipoMovimiento}" listCaption="getDescripcion" listValues="getCodigo"--%>
<%--					filter="categoria = 'ctacte.tipoMovimiento' order by codigo" attribs="onchange='actualizarCuota();'"/>--%>
<%--    			</td>--%>
<%--    		</tr>--%>
    		<tr>
    			<th>
    				Motivo:
    			</th>
    			<td>
      			   <asf:select name="process.detalle" entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.detalle}" listCaption="getDescripcion" listValues="getDescripcion"
					filter="categoria = 'ctacte.detalle' order by codigo"/>
    			</td>
    		</tr>
    		<tr>
    			<th>
    			Monto actual:
    			</th>
    			<td>
    				<html:text maxlength="10" property="process.montoActual" readonly="true" value="${ProcessForm.process.montoActual}"  styleId="montoActual"></html:text>
    			</td>
    		</tr>
    		<tr>
    			<th>
    			Monto movimiento:
    			</th>
    			<td>
    				<html:text maxlength="10" property="process.montoMovimiento" readonly="false" value="${ProcessForm.process.montoMovimiento}" onchange="modificarMonto(montoMovimiento)" styleId="montoMovimiento"></html:text>
    			</td>
    		</tr>
    		<tr>
    			<th>
    			Monto final:
    			</th>
    			<td>
    				<html:text maxlength="10" property="process.montoFinal" readonly="true" value="${ProcessForm.process.montoFinal}" styleId="montoFinal"></html:text>
    			</td>
    		</tr>
<%--    		<tr>--%>
<%--    			<th>--%>
<%--    			Nro de Autorizaci�n:--%>
<%--    			</th>--%>
<%--    			<td>--%>
<%--    			<asf:text maxlength="10" type="text" property="process.numeroAutorizacion" readonly="false" value="${ProcessForm.process.numeroAutorizacion}"></asf:text>--%>
<%--    			</td>--%>
<%--    		</tr>--%>
    		<tr>
    			<th>
    			Observaciones:
    			</th>
    			<td>
    			<html:textarea property="process.observaciones" readonly="false" value="${ProcessForm.process.observaciones}"></html:textarea>
    			</td>
    		</tr>
    		<tr>
    			<td>
					<html:submit><bean:message key="abm.button.save"/></html:submit>
					<input type="button" onclick="cancelar();" value="Cancelar"/>
    			</td>
    		</tr>
		</table>
	</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<script type="text/javascript">

var ctrlFormulario = $('formulario');
var ctrlAccion = $('action');

function cancelar(){

	
	window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoCuentaCorriente&process.idObjetoi=${ProcessForm.process.idObjetoi}";

}

function modificarMonto(montoMovimiento){

	if(montoMovimiento.value < 0){
	
	alert('Debe ingresar un valor mayor a cero');
	}else{
	var radioC = $('RadioCred');
	var radioD = $('RadioDeb');
	var montoActual = $('montoActual');
	//var montoMovimiento = document.getElementById('montoMovimiento');
	var montoFinal = $('montoFinal');
	if(radioC.checked == true){
		var mto = parseFloat(parseFloat(montoActual.value) - parseFloat(montoMovimiento.value));
	}else{
		var mto = parseFloat(parseFloat(montoActual.value) + parseFloat(montoMovimiento.value));
	}
	montoFinal.value = mto.toFixed(2);
	}
}
function actualizarCuota(){
	ctrlAccion.value = 'actualizarCuota';
	ctrlFormulario.submit();
}
</script>