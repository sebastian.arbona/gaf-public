<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<html:errors/>

<div class="title">Administración de Importaciones de Tablas</div>
<br><br>
<html:errors />
<html:form action="/actions/process.do?processName=${param.processName}" enctype="multipart/form-data">
<html:hidden property="do" value="process"/>
<table border="0">
	<TR><th>Formato de Importación:</th>
		<TD>
			<asf:select name="process.codi72" entityName="com.asf.cred.hibernate.Loaddef" listCaption="id,deta72" listValues="id" filter="id like '%' order by id"/>
		</TD>
	</TR>
	<TR>
		<th>Archivo:</th>
		<TD><html:file property="process.file" /></TD>
	</TR>

</TABLE>
<center>
<html:submit value="Importar" />
</html:form>