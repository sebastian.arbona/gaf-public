<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>




<div class="title">
	Tipos de Garantia
</div>
<br>
<br>

<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>
				Id:
			</th>
			<td>
				<asf:text name="AbmForm" property="entity.id" type="long"
					maxlength="8" readonly="true" />
			</td>
		</tr>

		<tr>
			<th>
				Nombre:
			</th>
			<td>
				<html:text property="entity.nombre" maxlength="30" />
			</td>
		</tr>
		<tr>
			<th>
				Nombre del N�mero Identificador:
			</th>
			<td>
				<html:text property="entity.nombreNumeroIdentificacion" maxlength="30" /><br/>(Identificador �nico. Ej: Nro de Fianza, Nro de Contato de Prenda, Nro de Contrato Cedido. )
			</td>
		</tr>
		<tr>
			<th>
				Descripci�n:
			</th>
			<td>
				<html:textarea property="entity.descripcion" cols="50" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Datos de Estado:
			</th>
			<td>
				<asf:select name="entity.requiereDatosEstado"
					value="${AbmForm.entity.requiereDatosEstado}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Datos de Gravamen:
			</th>
			<td>
				<asf:select name="entity.necesitaTasacion"
					value="${AbmForm.entity.necesitaTasacion}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Datos de Tasaci�n de Inmueble:
			</th>
			<td>
				<asf:select name="entity.necesitaTasacionInmueble"
					value="${AbmForm.entity.necesitaTasacionInmueble}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Datos de Tasaci�n de Mueble:
			</th>
			<td>
				<asf:select name="entity.necesitaTasacionMueble"
					value="${AbmForm.entity.necesitaTasacionMueble}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Datos de Custod�a:
			</th>
			<td>
				<asf:select name="entity.necesitaCustodia"
					value="${AbmForm.entity.necesitaCustodia}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Datos de Seguro:
			</th>
			<td>
				<asf:select name="entity.requiereSeguro"
					value="${AbmForm.entity.requiereSeguro}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Requiere Inscripcion en Registro:
			</th>
			<td>
				<asf:select name="entity.requiereInscripcionRegistro"
					value="${AbmForm.entity.requiereInscripcionRegistro}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Inscripcion Obligatoria para Desembolsar
			</th>
			<td>
				<asf:select name="entity.registroImpideDesembolso"
					value="${AbmForm.entity.registroImpideDesembolso}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				Cargar Tomador como Titular
			</th>
			<td>
				<asf:select name="entity.tomadorTitular"
					value="${AbmForm.entity.tomadorTitular}" listCaption="Si,No"
					listValues="true,false" />
			</td>
		</tr>
		<tr>
			<th>
				A�os de validez
			</th>
			<td>
				<asf:text maxlength="4" type="long" name="AbmForm" property="entity.validezAnios"/>
			</td>
		</tr>
		<tr>
			<th>Aforo</th>
			<td>
				<asf:text maxlength="8" attribs="size=10px" type="decimal" name="AbmForm" property="entity.aforo"/>
			</td>
		</tr>
		<tr>
			<td style="text-align: center">S�lo para cosecha y acarreo</td>
		</tr>	
		<tr>
			<th>Porcentaje del Primer Desembolso</th>
			<td>
			<html:text property="entity.porcDesembolso1Str" value="${AbmForm.entity.porcDesembolso1Str}"/>
			</td>
		</tr>
		<tr>
			<th>Porcentaje del Segundo Desembolso</th>
			<td>
			<html:text property="entity.porcDesembolso2Str" value="${AbmForm.entity.porcDesembolso2Str}"/>
			</td>
		</tr>
		<tr>
			<th>Vincular a Persona como Titular</th>
			<td>
				<html:select property="entity.esPersona" value="${AbmForm.entity.esPersona}">
					<html:option value="false">NO</html:option>
					<html:option value="true">SI</html:option>					
				</html:select>
			</td>
		</tr>
		<tr>
			<th>No usar vi�edos para calcular QQ del contrato</th>
			<td>
				<html:radio property="entity.calculaQQSinVinedo" value="true"></html:radio>Si
				<html:radio property="entity.calculaQQSinVinedo" value="false"></html:radio>No
			</td>
		</tr>	
		<tr>
			<th>M�ximo QQ por Proyecto</th>
			<td>
				<html:text property="entity.maxQQProyecto"/>
			</td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

