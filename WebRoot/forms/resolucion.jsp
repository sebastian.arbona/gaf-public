<%@ page language="java"%>
<%@ page import="com.nirven.creditos.hibernate.FuncionPersonaResolucion"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@page import="java.util.HashMap"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/links.css"
	type="text/css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/multiple-emails.css">

<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/functions.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/multiple-emails.js"></script>

<%
	request.setAttribute("lineasAcuerdo", DirectorHelper.getString("linea.acuerdoPago"));
%>


<script type="text/javascript">
	$j = jQuery.noConflict();
	$j(document).ready(function() {
		var demandado = document.getElementById("demandado").checked;
		if (demandado) {
			document.getElementById('proyecto').style.display = '';
			document.getElementById('persona').style.display = 'none';
		} else {
			document.getElementById('proyecto').style.display = 'none';
			document.getElementById('persona').style.display = '';
		}
	});

	/**
	 * @inputIn: control sobre el input que dispara la consulta ajax.-
	 * @defecto: para parametro=valor que tiene,
	 * a la izquierda del igual el id del input que dispara la consulta ajax.-
	 * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
	 */
	function actualizarLista(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&ignorarLineas=${lineasAcuerdo}";
		urlDinamica += "&proyecto=" + inputIn.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}

	function actualizar(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&expediente=" + inputIn.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}

	function actualizarListaPersona(inputIn, defecto) {
		var urlDinamica = "";

		urlDinamica += "&persona=" + inputIn.value;
		urlDinamica += "&" + defecto;

		return urlDinamica;
	}

	function seleccion(myRadio) {
		var currentValue = myRadio.value;
		if (currentValue == 1) {
			document.getElementById('proyecto').style.display = '';
			document.getElementById('persona').style.display = 'none';
		} else {
			document.getElementById('proyecto').style.display = 'none';
			document.getElementById('persona').style.display = '';
		}

	}
</script>
<body class=" yui-skin-sam">
	<div class="title">Nueva Resoluci�n Judicial</div>
	<br>
	<br>

	<html:form
		action="/actions/process.do?do=process&processName=${param.processName}"
		styleId="oForm" enctype="multipart/form-data">

		<html:hidden name="ProcessForm" property="process.accion"
			styleId="accion" />
		<html:hidden name="ProcessForm"
			property="process.idSubTipoResolucion2"
			styleId="idSubTipoResolucion2" />
		<html:hidden name="ProcessForm" property="process.idproyectoPop"
			styleId="numeroAtencion" />
		<html:hidden name="ProcessForm" property="process.idEspecialidad"
			styleId="idEspecialidad" />
		<html:hidden name="ProcessForm"
			property="process.idPersonaEspecialista"
			styleId="idPersonaEspecialista" />
		<html:hidden name="ProcessForm" property="process.sid" />
		<html:hidden name="ProcessForm" property="process.resolucion.id" />
		<html:hidden name="ProcessForm" property="process.current_emails"
			styleId="current_emails" />

		<div style="width: 70%" align="left">
			<html:errors />
		</div>

		<table border="0" class="tabla" id="acarreo1">
			<tr>
				<html:radio name="ProcessForm" property="process.opcion" value="1"
					disabled="${!ProcessForm.process.nuevo}"
					onchange="seleccion(this);" styleId="demandado">Demandados </html:radio>
				<html:radio name="ProcessForm" property="process.opcion" value="2"
					disabled="${!ProcessForm.process.nuevo}"
					onchange="seleccion(this);" styleId="demandante">Demandantes</html:radio>
			</tr>
			<tr id="proyecto">
				<th>Proyecto:</th>
				<td><asf:autocomplete name="" tipo="AJAX"
						idDiv="autocomplete_proyecto" idInput="idproyectoPop"
						classDiv="autocomplete"
						origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarProyecto&"
						options="{callback: actualizarLista, frequency: '0.8', minChars: '2',indicator:'indicadorProyecto'}"
						value="${ProcessForm.process.resolucion.proyecto.numeroAtencion}"
						attribs="size='80%' title='Se puede buscar N�mero de Proyecto o Expediente'" />
					<span id="indicadorProyecto" style="display: none"><img
						src="images/loader.gif" alt="Trabajando..." /></span> (N�mero de
					Proyecto - Expediente)</td>
			</tr>
			<tr id="persona">
				<th>Persona:</th>
				<td><asf:autocomplete name="process.persona" tipo="AJAX"
						idDiv="autocomplete_persona"
						value="${ProcessForm.process.personaid}" idInput="personaid"
						classDiv="autocomplete"
						origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
						options="{callback: actualizarListaPersona, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
						attribs="size='20%' title='Se puede buscar por Apellido y Nombre o Nro. de Documento'" />
					<span id="indicadorPersona" style="display: none"><img
						src="images/loader.gif" alt="Trabajando..." /></span> (Apellido
					y Nombre - N�mero de Documento)</td>
			</tr>
			<tr>
				<th>Nro de Resoluci�n:</th>
				<td><asf:text property="ProcessForm.process.resolucion.numero"
						type="text" maxlength="11"
						readonly="${!ProcessForm.process.editable}" id="numeroResolucion"
						attribs="size=\"80\""
						value="${ProcessForm.process.resolucion.numero}" /></td>
			</tr>
			<tr>
				<th>Proceso:</th>
				<td><asf:select
						entityName="com.nirven.creditos.hibernate.TipoResolucion"
						attribs="class='idTipoResolucion';" listCaption="getId,getNombre"
						listValues="getId" name="process.idTipoResolucion"
						value="${ProcessForm.process.resolucion.id}" style="width:425px" />
				</td>
			</tr>
			<tr>
				<th>Fecha:</th>
				<td><asf:calendar
						property="ProcessForm.process.resolucion.fechaStr"
						attribs="class='fechaStr' size=\"80\""
						readOnly="${!ProcessForm.process.editable}" /></td>
			</tr>
			<tr>
				<th>Selecci�n de Especialidad:</th>
				<td><asf:select
						name="ProcessForm.process.resolucion.idEspecialidad"
						entityName="Especialidad" listCaption="getNombre" listValues="id"
						nullText="&lt;&lt;Seleccione&gt;&gt;" nullValue="true"
						value="${ProcessForm.process.resolucion.especialista.especialidad.id}"
						style="width:425px" /></td>
			</tr>
			<tr>
				<th>Especialista:</th>
				<td><asf:lselect
						property="ProcessForm.process.resolucion.idPersonaEspecialista"
						entityName="EspecialidadPersona" listCaption="persona.nomb12"
						listValue="persona.idpersona"
						value="${ProcessForm.process.resolucion.especialista.persona.idpersona}"
						linkFK="especialidad.id"
						linkName="ProcessForm.process.resolucion.idEspecialidad"
						nullValue="true" style="width:425px" /></td>
			</tr>
			<tr>
				<th>Observaciones:</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.resolucion.observaciones" cols="77" rows="4"
						value="${ProcessForm.process.resolucion.observaciones}" /></td>
			</tr>
			<tr>
				<th>Definitiva:</th>
				<td><html:checkbox property="process.definitivo"
						onclick="mostrarDef();" styleId="defCheck"></html:checkbox><span
					id="def"></span></td>
			</tr>
			<tr id="file">
				<th><span style="" class="required">(*)</span>Documento:</th>
				<td colspan="3"><html:file name="ProcessForm"
						property="process.theFile" /> <logic:notEmpty
						property="process.resolucion.id" name="ProcessForm">
						<br>
						<a
							href="javascript: documentoResolucion( ${ProcessForm.process.resolucion.id} );">Administrar
							Archivos</a>
					</logic:notEmpty></td>
			</tr>
			<tr id="observacionArchivo">
				<th>Observaciones del Archivo:</th>
				<td colspan="3"><html:textarea styleId="objeto"
						onkeypress="caracteres(this,400,event)"
						property="process.observacionesArchivo" cols="77" rows="4" /></td>
			</tr>
			<tr>
				<th>Enviar mail de aviso:</th>
				<td><html:checkbox property="process.enviarAviso"
						onclick="mostrarEnviarMail();" styleId="enviarMailCheck"></html:checkbox><span
					id="enviarMail"></span></td>
			</tr>
			<tr id="plantillaMailDiv">
				<th>Seleccionar notificaci�n mail:</th>
				<td><asf:select
						entityName="com.nirven.creditos.hibernate.ConfiguracionNotificacion"
						listCaption="denominacion" listValues="id" orderBy="denominacion"
						name="process.idCn" nullValue="true"
						nullText="&lt;&lt;Seleccione Notificaci�n&gt;&gt;"
						value="${ProcessForm.process.idCn}" style="width:330px" /> <html:checkbox
						property="process.notificar" styleId="notificarCheck"></html:checkbox><span
					id="notificar">Notificar</span></td>
			</tr>
			<tr id="destinatarioMailDiv">
				<th>Destinatario del mail:</th>
				<td><html:checkbox property="process.destinatarioMail"
						onclick="mostrarDestinatario();" styleId="desCheck"></html:checkbox><span
					id="destinatario"></span></td>
			</tr>
			<tr id="multipleMailDiv">
				<th>Destinatarios Adicionales:</th>
				<td>
					<div class='container'>
						<div class='col-sm-4'>
							<input type='text' id='demo' name='email' class='form-control'
								value='["ejemplo@ejemplo.com"]'>
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div style="margin-top: 10px">
			<input type="button" value="Guardar Resoluci�n" onclick="guardar();" />
			<input type="button" value="Cancelar" onclick="cancelar();" />
		</div>
	</html:form>

	<script type="text/javascript">
		var formulario = $j('#oForm');
		//var idSubTipoResolucion2 = $j('#idSubTipoResolucion2');
		$j(document).ready(function() {
			//cargarTipoResolucion();
			mostrarDef();
			mostrarDestinatario();
			mostrarEnviarMail();
			$j('#demo').multiple_emails();
			$j('#demo').change(function() {
				$j('#current_emails').text($j(this).val());
			});
			$j('#current_emails').text($j('#demo').val());
		});

		function limpiarCombo(combo) {
			while (combo.length > 0) {
				combo.remove(combo.length - 1);
			}
		}

		function agregar() {
			accion.value = 'persona';
			return true;
		}

		function guardar() {
			//var indice = $('process.idSubTipoResolucion').selectedIndex;
			//var idSub = $('process.idSubTipoResolucion').options[indice].value;
			var indiceE = $('ProcessForm.process.resolucion.idEspecialidad').selectedIndex;
			var idSubE = $('ProcessForm.process.resolucion.idEspecialidad').options[indiceE].value;
			var indiceP = $('ProcessForm.process.resolucion.idPersonaEspecialista').selectedIndex;
			var idSubP = $('ProcessForm.process.resolucion.idPersonaEspecialista').options[indiceP].value;
			idEspecialidad.value = idSubE;
			idPersonaEspecialista.value = idSubP;
			//idSubTipoResolucion2.value = idSub;
			if ($('idproyectoPop').value != null) {
				var credito = $('idproyectoPop').value.split("-")[0];
				var numeroAtencionVar = credito.split(":")[1];
				if (numeroAtencionVar != null) {
					numeroAtencion.value = numeroAtencionVar;
				} else {
					numeroAtencion.value = credito;
				}
			}
			current_emails.value = $j('#current_emails').text();
			accion.value = 'guardar';
			var form = $('oForm');
			form.submit();
		}

		function quitarPersona(i) {
			accion.value = 'quitarPersona';
			var indice = $j('funcionPersonaResolucionQuitar');
			indice.value = i;
			var form = $('oForm');
			form.submit();
			return true;
		}

		function cancelar() {
			accion.value = 'list';
			var form = $('oForm');
			form.submit();
		}

		function cargarTipoResolucion() {
			//var indice = $('process.idSubTipoResolucion').selectedIndex;
			//var idSub = $('process.idSubTipoResolucion').options[indice].value;
			$j
					.ajax({
						url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerTipoResolucion&idSubTipoResolucion="
								+ idSub,
						success : function(data) {
							document.getElementById("tipoResolucion").innerHTML = '<STRONG>'
									+ data + '</STRONG>';
						}
					});
		}

		function cargarDatosCredito() {
			if ($j('numeroAtencion').value != '') {
				$j
						.ajax({
							url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerDatosCredito&numeroAtencion="
									+ $j('numeroAtencion').value,
							success : function(data) {
								document.getElementById("descripcionCredito").innerHTML = '<STRONG>'
										+ data + '</STRONG>';
							}
						});
			}
		}

		function mostrarDef() {
			if ($j("#defCheck").is(':checked')) {
				document.getElementById("def").innerHTML = '<STRONG>'
						+ 'Es definitiva' + '</STRONG>';
				$j('#file').show();
				$j('#observacionArchivo').show();
			} else {
				document.getElementById("def").innerHTML = '<STRONG>'
						+ 'No es definitiva' + '</STRONG>';
				$j('#file').hide();
				$j('#observacionArchivo').hide();
			}
		}

		function mostrarEnviarMail() {
			if ($j("#enviarMailCheck").is(':checked')) {
				document.getElementById("enviarMail").innerHTML = '<STRONG>'
						+ 'Enviar mail' + '</STRONG>';
				$j('#plantillaMailDiv').show();
				$j('#destinatarioMailDiv').show();
				$j('#multipleMailDiv').show();
			} else {
				document.getElementById("enviarMail").innerHTML = '<STRONG>'
						+ 'No enviar mail' + '</STRONG>';
				$j('#plantillaMailDiv').hide();
				$j('#destinatarioMailDiv').hide();
				$j('#multipleMailDiv').hide();
			}
		}

		function mostrarDestinatario() {
			if ($j("#desCheck").is(':checked')) {
				document.getElementById("destinatario").innerHTML = '<STRONG>'
						+ 'ABOGADO' + '</STRONG>';
			} else {
				document.getElementById("destinatario").innerHTML = '<STRONG>'
						+ 'ABOGADO Y TITULAR' + '</STRONG>';
			}
		}

		function documentoResolucion(id) {
			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=listarDocumentosResolucion&';
			url += 'process.idSolicitud=' + id;
			window.location = url;
		}
	</script>
	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>