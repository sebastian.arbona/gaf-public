<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<h3 class="title">
	<span class="fa fa-fw fa-comment fa-lg"></span>&nbsp;<span id="title_ObservacionLegajoDigital"></span>
</h3>

<form class="form-horizontal" id="form_ObservacionLegajoDigital" name="form_ObservacionLegajoDigital">
	<input type="hidden" name="id" id="id" />
	<input type="hidden" name="legajoDigital_id" id="legajoDigital_id" />
	<input type="hidden" name="tipo" id="tipo" />
	
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <label class="control-label" for="texto">Observaci&oacute;n</label>
            <textarea class="form-control" id="texto" name="texto" rows="12"></textarea>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <label class="control-label" for="mostrar">Mostrar como observaci&oacute;n destacada</label>
            <input type="checkbox" name="mostrar" id="mostrar" class="jubilado" checked="checked" />
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    
		<asf:security action="/actions/process.do"
		access="do=process&processName=ObservacionLegajoDigital&process.accion=doPost">	    
    		
    		<button id="btn-save-observacion" type="button" class="btn btn-confirmar" onclick="save_ObservacionLegajoDigital();" >Guardar</button>
    		
   		</asf:security>
	    <button id="btn-close-observacion"type="button" class="btn btn-cancelar" onclick="grid_ObservacionLegajoDigital(-1);">Cancelar</button>
    </div>
</form>



