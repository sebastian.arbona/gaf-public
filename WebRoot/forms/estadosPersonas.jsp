<%@ page language="java"%>
<%@page import="com.asf.cred.business.EstadosPersonas"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<%
	pageContext.setAttribute("agregaEstado", EstadosPersonas.agregaEstado);
%>
<%
	pageContext.setAttribute("agregaObs", EstadosPersonas.agregaObs);
%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="guardar" styleId="accion" />
	<html:hidden property="process.persona.id" styleId="idPersonaTitular"
		value="${ProcessForm.process.persona.id}" />
	<html:hidden property="process.agrega" styleId="agrega" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<table>
		<tr>
			<td><input name="funcion" type="radio"
				onclick="habilitarFuncion( false );" id="agregaEstado">Modificar
				Estado</td>
			<td><input name="funcion" type="radio"
				onclick="habilitarFuncion( false );" id="agregaObs">Agregar
				Observación</td>
		</tr>
	</table>
	<br>
	<table>
		<tr id="filaEstado">
			<th>Estado:</th>
			<td><asf:select name="process.estado.idEstado"
					entityName="Estado" listCaption="idEstado,nombreEstado"
					listValues="idEstado"
					value="${ProcessForm.process.estado.idEstado}" nullValue="true"
					nullText="Seleccione un Estado"
					filter="tipo = 'Persona' AND manual = true" /></td>
		</tr>
		<tr id="filaObs">
			<th>Detalle Observación:</th>
			<td><html:textarea property="process.detalleObservacion"
					onkeypress="caracteres(this,1000,event)" cols="60" rows="5" /></td>
		</tr>
		<tr id="filaObsEstado">
			<th>Observaciones:</th>
			<td><html:textarea property="process.obsEstado"
					onkeypress="caracteres(this,1000,event)" cols="60" rows="5" /></td>
		</tr>
	</table>
	<br>
	<html:submit value="Guardar" />
	<input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>
<script language='javascript'>
	//CONTROLES
	var accion = $('accion');
	var formulario = $('oForm');
	var ctrlAgrega = $('agrega');
	var ctrlAgregaEstado = $('agregaEstado');
	var ctrlAgregaObs = $('agregaObs');
	var ctrlFilaEstado = $('filaEstado');
	var ctrlFilaObs = $('filaObs');
	var ctrlFilaObsEstado = $('filaObsEstado');
	//AL CARGAR LA PÁGINA
	habilitarFuncion(true);

	function cancelar() {
		accion.value = "cancelar";

		formulario.submit();
	}//fin cancelar.-

	function habilitarFuncion(primeraVez) {
		if (primeraVez) {
			ctrlAgregaEstado.checked = true;
		}

		if (ctrlAgregaEstado.checked) {
			ctrlAgrega.value = '${agregaEstado}';
			ctrlFilaEstado.style.display = "";
			ctrlFilaObs.style.display = "none";
			ctrlFilaObsEstado.style.display = "";
		} else if (ctrlAgregaObs.checked) {
			ctrlAgrega.value = '${agregaObs}';
			ctrlFilaObs.style.display = "";
			ctrlFilaEstado.style.display = "none";
			ctrlFilaObsEstado.style.display = "none";
		}
	}//fin habilitarFuncion.-

	Event.observe(window, 'load', function() {
		this.name = "Estados y Observaciones";
		parent.push(this);
	});
</script>