<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Carga de Notificaci&oacute;n</div>

<html:form action="/actions/process.do?do=process&processName=NuevaNotificacion" enctype="multipart/form-data">
	<html:hidden property="process.action" value="guardar"/>
	<html:hidden property="process.cid"/>
	<html:hidden property="process.idObjetoi" value="${param.idObjetoi}"/>
	
	<div class="grilla">
		<table style="margin: 20px 0">
			<tr>
				<th>Fecha de notificaci&oacute;n</th>
				<td>
					<asf:calendar value="${process.fecha}" property="ProcessForm.process.fecha"/>
				</td>
			</tr>
			<tr>
				<th>Fecha de vencimiento</th>
				<td>
					<asf:calendar value="${process.fechaVencimiento}" property="ProcessForm.process.fechaVencimiento"/>
				</td>
			</tr>
			<tr>
				<th>Observaciones</th>
				<td>
					<html:textarea property="process.observaciones" name="ProcessForm" cols="40" rows="5"/>
				</td>
			</tr>
			<tr>
	  			<th>Tipo de Notificaci&oacute;n</th>
	  			<td>
					<asf:select name="process.idTipoAviso"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.idTipoAviso}"
						listCaption="getDescripcion" listValues="getCodigo"
						filter="categoria='notificacion.TipoAviso' order by codigo" />
	  			</td>
	  		</tr>
	  		<%-- Ticket 9894. Anulo la modificación del adjunto al editar la notificación --%>
	  		<logic:equal name="ProcessForm" property="process.idNotificacion" value=""> 
		  		<tr id="doc">
					<th>Documento </th>
					<td colspan="3">
						<html:file property="process.formFile" styleId="formFile"/>
					</td>
				</tr>
			</logic:equal>
			<tr>
				<th colspan="2">
					<html:submit value="Guardar"/>
					<input type="button" value="Cancelar" onclick="cancelar();"/>
				</th>
			</tr>
		</table>
	</div>

</html:form>
<script type="text/javascript">
	function cancelar() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=NotificacionProcess&process.accion=verNotif&process.pestania=true&process.idCredito=${param.idObjetoi}";
	}
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>