<%@ page language="java"%>
<%@ page import="com.nirven.creditos.hibernate.FuncionPersonaResolucion" %>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>


<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />


<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>



<script language="javascript" type="text/javascript">

	$j = jQuery.noConflict();

	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
  
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
    }
	
    function actualizar( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&expediente=" + inputIn.value;
    	urlDinamica += "&" + defecto;

    	return urlDinamica;
    }
</script>
<body class=" yui-skin-sam">
  
  
  <div class="title">
   Nuevo Documento
  </div>
  <br>
  <br>
  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
  
    <html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
    <html:hidden name="ProcessForm" property="process.idSubTipoResolucion2" styleId="idSubTipoResolucion2" />
    <html:hidden name="ProcessForm" property="process.idEspecialidad" styleId="idEspecialidad" />
     <html:hidden name="ProcessForm" property="process.idPersonaEspecialista" styleId="idPersonaEspecialista" />
    <html:hidden name="ProcessForm" property="process.sid"/>
    <html:hidden property="process.idSolicitud"/>
    <html:hidden name="ProcessForm" property="process.resolucion.id"/>
   
    
    
    <div style="width: 70%" align="left">
      <html:errors />
    </div>
    
    	<table>
			<tr>
	    		<th><span style="required" class="required">(*)</span>&nbspDocumento:</th>
	    		<td colspan="3">
	        		<html:file name="ProcessForm" property="process.theFile"/>
	    		</td>
			</tr> 
			<tr>
	            <th>Detalle: </th>
	            <td>
	                <html:textarea property="process.detalleDocumentoResolucion" cols="60" rows="4" onkeypress="caracteres(this,255,event);"></html:textarea>
	            </td>
        	</tr>
		</table>

      
  
  	<div style="margin-top: 10px">
			<input type="button" value="Guardar Documento" onclick="guardar();"/>
			<input type="button" value="Cancelar" onclick="cancelar();"/>
		</div>
  </html:form>
  



<script language="JavaScript" type="text/javascript">




$j(document).ready(

				function($j) {					
									
					cargarTipoResolucion();
					mostrarDef();
				});
				
				
</script>

<script type="text/javascript">

					var formulario = $j('#oForm');
					var idSubTipoResolucion2 = $j('#idSubTipoResolucion2');
					
					

function limpiarCombo(combo){
	while(combo.length > 0){
		combo.remove(combo.length-1);
	}
}

function agregar(){

accion.value = 'persona';
return true;

}

function guardar() {

	accion.value = 'guardarDocumentoResolucion';
	var form = $('oForm');
	form.submit();
}

function quitarPersona(i) {
	accion.value = 'quitarPersona';

	var indice = $('funcionPersonaResolucionQuitar');
	indice.value = i;
	
	var form = $('oForm');
	form.submit();
	
	return true;
}

function cancelar(){
	accion.value = 'listarDocumentosResolucion';	
	var form = $('oForm');
	form.submit();
}

function cargarTipoResolucion(){

var indice = $('process.idSubTipoResolucion').selectedIndex;
var idSub = $('process.idSubTipoResolucion').options[indice].value;


								$j.ajax( {
									
									url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerTipoResolucion&idSubTipoResolucion="
									+ idSub,																													
									success : function(data) {
												
												
												document.getElementById("tipoResolucion").innerHTML='<STRONG>'+data+'</STRONG>';
												

												}
								});

}

function cargarDatosCredito(){

								if($('numeroAtencion').value != ''){
								
									$j.ajax( {
									
									url : "${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxTraerDatosCredito&numeroAtencion="
									+ $('numeroAtencion').value,																													
									success : function(data) {

												document.getElementById("descripcionCredito").innerHTML='<STRONG>'+data+'</STRONG>';

												}
									});
								
								}
}

function mostrarDef(){

if($j("#defCheck").is(':checked')) {  
            document.getElementById("def").innerHTML='<STRONG>'+'Si'+'</STRONG>';
            $j('#file').show();
        } else {  
            document.getElementById("def").innerHTML='<STRONG>'+'No'+'</STRONG>';
            $j('#file').hide();  
        }  




}

</script>
  <iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
    src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
    style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
  ></iframe>