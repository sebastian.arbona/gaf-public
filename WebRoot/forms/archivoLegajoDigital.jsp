<%@ page language="java"%>
<%@ page import="com.asf.grh.hibernate.*" %>
<%@ page import="com.asf.security.BusinessPersistance" %>
<%@ page import="com.asf.security.SessionHandler" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/ajax.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xml2json.js"></script>
<style>
#page-content-wrapper {
	padding-top: 0px;
}
.container {
	max-width: 100%;	
}
.fileHide {
	display: none;
}
footer {
	display: none;
	
}
</style>
<script>
var readOnlyCheckBoxVisualizar = true;
</script>
<asf:security action="/actions/process.do?do=process&processName=ArchivoLegajoDigitalVisualizar">
	<script>
	readOnlyCheckBoxVisualizar = false;
	</script>
</asf:security>

<h3 class="title"><span id="span_title"></span></h3>
<html:form action="/actions/ArchivoAction.do?do=legajoDigital&entityName=Archivo" enctype="multipart/form-data" styleId="form_ArchivoLegajoDigital">
	
	<input type="hidden" id="id" name="id" value="${param.idArchivoLegajoDigital}">
	
	<input type="hidden" id="legajoDigital_id" name="legajoDigital_id" value="${param.idLegajoDigital}">
	<input type="hidden" id="tipo" name="tipo" value="${param.tipoLegajoDigital}">	
	<input type="hidden" id="archivo_nombre" name="archivo_nombre" value="">
	<input type="hidden" id="archivo_id" name="archivo_id" value="">
	<input type="hidden" id="archivo_tipoDeArchivo_id" name="archivo_tipoDeArchivo_id" value="">
	<asf:security action="/actions/process.do?do=process&processName=ArchivoLegajoDigitalVisualizar">
		<input type="hidden" id="ArchivoLegajoDigitalVisualizar" name="ArchivoLegajoDigitalVisualizar" value="allow" data-crud-populate="disabled" />
	</asf:security>
	
	<div class="form-group row" id="create_upload" style="max-height: 60px; margin-top: -15px;">
		<label for="browse-file" class="col-sm-2 col-form-label" style="padding-top: 25px;">Archivo:</label>
		<div class="col-sm-10">
        	<button type="button" class="btn btn-default new-usuario" id="browse-file" onclick="fireFileClick()">
				<i class="fa fa-search" aria-hidden="true"></i>&nbsp;Examinar...
			</button>
			<input type="text" name="uploader_filename" id="uploader_filename" value=""/> 
			<html:file property="theFile" styleId="theFile" styleClass="inputfile inputfile-5 fileHide"/>
		</div>
	</div>
	
	<div class="form-group row" id="edit_upload">
		<label for="archivo_nombre" class="col-sm-2 col-form-label">Archivo:</label>
		<input type="text" name="archivo_nombre" id="archivo_nombre" value="" disabled="disabled" /> 
	</div>
	
	<div class="form-group row" style="max-height: 60px; margin-top: -15px;">
		<label for="entity.archivoRepo.idTipoDeArchivo" class="col-sm-2 col-form-label" style="padding-top: 25px;">Subfamilia:</label>
		<div class="col-sm-10">
    		    <asf:select2 name="archivoForm"			
						property="entity.archivoRepo.idTipoDeArchivo"
						value="${archivoForm.entity.archivoRepo.idTipoDeArchivo}"
						descriptorLabels=" Nombre" entityProperty="id"
						descriptorProperties="nombre, descripcion" entityName="TipoDeArchivo"
						where2=" padre_id IS NOT NULL "
						accessProperty="id" />  		
		</div>
	</div>
	
	<!-- Descripción -->
	<div class="form-group row">
		<label for="archivoDescripcion" class="col-sm-2 col-form-label">Descripci&oacute;n</label>
		<div class="col-sm-10">
			<textarea id="archivo_descripcion" name="archivo_descripcion" class="form-control"></textarea>
		</div>
	</div>
	
	<!-- Tipo de Archivo -->
	<div class="row">
		<div class="form-group row col-sm-12">
			<label for="desde" class="col-sm-5 col-form-label">Tipo de Documentaci&oacute;n</label>
			<div class="col-sm-5">
				<select class="form-control form-control-sm" id="select_tipo" name="select_tipo" data-crud-populate="disabled" >
  					<option value="1">Documentaci&oacute;n General</option>
  					<option value="2">Documentaci&oacute;n T&eacute;cnica</option>
  					<option value="3">Documentaci&oacute;n Econ&oacute;mica</option>
  					<option value="4">Documentaci&oacute;n Legal</option>
				</select>	
			</div>
		</div>
	</div>

	
	<!-- Fechas -->
	<div class="row">
		
		<!-- Hasta -->
		<div class="form-group row col-sm-6">
			<label for="hasta" class="col-sm-4 col-form-label">Fecha Hasta:</label>
			<div class="col-sm-5">
				<input class="form-control dp-fecha text-right" name="hasta" id="hasta" data-date-format="DD/MM/YYYY" value="">
			</div>
			<label for="hasta" class="col-sm-2 col-form-label">
				<i class="fa fa-calendar"></i>
			</label>
		</div>		
	</div>
	
	<div class="row">
   		<div class="form-group row col-sm-6">
   			<label for="hasta" class="col-sm-4 col-form-label">Visualizar:</label>
        	<div class="form-group">            
            	<input type="checkbox" name="visualizar" id="visualizar" checked="checked" />
        	</div>
    	</div>
	</div>	
	

   
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
   		<asf:security action="/actions/process.do"
			access="do=process&processName=ArchivoLegajoDigital&process.accion=doPost">
			<html:submit styleClass="btn btn-guardar"><bean:message key="abm.button.save"/></html:submit>
		</asf:security>		
		<html:cancel styleClass="btn btn-cancelar"><bean:message key="abm.button.cancel" /></html:cancel>
    </div>
    <div class="errors">  		
    	<html:errors />
    </div> 
    
</html:form>   

<script type="text/javascript">
function fireFileClick(){
	$j('#theFile').click();
}
$j('#theFile').on('change', function(){
	$j('#uploader_filename').val($j('#theFile').val());
});

var errores="${ArchivoForm.entity.chErrores}";
var archivoLegajo_id = $j("#id").val();

$j("#entity_archivoRepo_idTipoDeArchivoAccess").change(function(){
	var archivo_id = $j(this).val();
	getTipoDeArchivo(archivo_id);
});

$j('#select_tipo').on('change', function() {
	$j("#tipo").val($j("#select_tipo").val());
	});

$j(document).ready(function(){
	$j('.dp-fecha').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        showClose:true,
		showClear: true,
		showTodayButton: true,
		toolbarPlacement: 'bottom'
    });
    var tituloPagina;

    if(archivoLegajo_id == ""){

		tituloPagina = "Nueva Documentaci&oacute;n " + getNameTipo($j("#tipo").val());	
		$j("#span_title").html(tituloPagina);	
		$j("#edit_upload").hide();	
		$j("#select_tipo").val($j("#tipo").val());	
    }else{
           	
    	crud.populate({
    		url: pathSistema + "/rest/ArchivoLegajoDigitalAction.do?do=doGet",
    		id: archivoLegajo_id,
    		formId: '#form_ArchivoLegajoDigital',
    		entidad: 'ArchivoLegajoDigital'			
    	}, 
    	function(){
    		var archivo_id = $j("#archivo_id").val();
        	$j("#create_upload").hide();
        	formatInputDate("desde", true);
        	formatInputDate("hasta", false);
        	getTipoDeArchivo(archivo_id);  
        	tituloPagina = "Editar Documentaci&oacute;n " + getNameTipo($j("#tipo").val());	
        	$j("#span_title").html(tituloPagina);
        	$j("#select_tipo").val($j("#tipo").val());
    	});
    	
    }
    $j("#visualizar").attr('disabled', readOnlyCheckBoxVisualizar); 
});

function formatInputDate(inputName, setDefault) {
	 var d = new Date($j("#" + inputName).val());
	 var t = new Date();
	 var nd = ""; 
	 if (Object.prototype.toString.call(d) === "[object Date]") {		  
		  if (isNaN(d.getTime())) {  // d.valueOf() could also work
			if(setDefault == true){
				nd = t.toLocaleDateString("es-AR");
			}else{
				nd = "";
			}
			
		  } else {
			  nd = d.toLocaleDateString("es-AR");
		  }
		} else {
			if(setDefault == true){
				nd = t.toLocaleDateString("es-AR");
			}else{
				nd = '';
			}		  
		}
	$j("#" + inputName).val(nd);	
}

function getTipoDeArchivo(archivo_id){

	jQuery.ajax({
        type: "POST",
        async: true,
        url: pathSistema + "/rest/ArchivoLegajoDigitalAction.do?do=doGet",
        data: {
        	id: archivo_id,
        	entidad: "Archivo",
        	campos: "tipoDeArchivo.id,tipoDeArchivo.nombre",
        	filtro: "id=" + archivo_id
        },
        success: function (response){	        	
        	var responseJSON = JSON.parse(response);
        	$j("#entity_archivoRepo_idTipoDeArchivoAccess").val(responseJSON.data[0].tipoDeArchivo_id);
        	$j("#entity_archivoRepo_idTipoDeArchivoDescriptor").val(responseJSON.data[0].tipoDeArchivo_nombre);
   		},
   		error: function (response) {
 	    	},
		});  
}

function getNameTipo(tipo){
	switch(tipo){
		case "1": case 1:
			return "General"; 
			break;
		case "2": case 2:
			return "T&eacute;cnica"; 
			break;
		case "3": case 3:
			return "Contable"; 
			break;
		case "4": case 4:
			return "Jur&iacute;dica"; 
			break;
	}
 }

</script>


