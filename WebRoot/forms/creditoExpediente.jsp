<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<div class="title">
  Expediente de Solicitud
</div>
<br/>
<html:errors/>
<input type="button" onclick="volver();" value="Volver"/>
<script type="text/javascript">
	function volver() {
		var url = '${pageContext.request.contextPath}/${ProcessForm.process.forwardURL}';
		window.location = url;
	}
</script>