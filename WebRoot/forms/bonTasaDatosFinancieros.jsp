<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>

<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<html:form
	action="/actions/process.do?do=process&processName=BonTasaDatosFinancieros"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" value="save" />
	<html:hidden property="process.idBonTasa" styleId="acci"
		value="${ProcessForm.process.idBonTasa}" />
	<html:hidden property="process.idCompensatorio" styleId="comp"
		value="${ProcessForm.process.idCompensatorio}" />
	<html:hidden name="ProcessForm" property="process.indice"
		value="${ProcessForm.process.indice}" />

	<html:errors></html:errors>
	<logic:equal value="true" property="process.error" name="ProcessForm">
  		No se puede guardar, todos los datos son obligatorios
  	</logic:equal>
	<table>

		<tr>

			<th>Agente Financiero</th>
			<td>${ProcessForm.process.bonificacion.banco.detaBa}</td>

			<th>Banco Sucursal</th>
			<td><asf:select name="process.bancosucursal_id"
					entityName="com.nirven.creditos.hibernate.BancoSucursal"
					listCaption="Id,Banco.detaBa,Nombre" listValues="getId"
					value="${ProcessForm.process.bancosucursal_id}" /></td>

		</tr>


		<tr>
			<th>Fecha de instrumentaci�n por parte de la Entidad de Cr�dito</th>
			<td><asf:calendar property="ProcessForm.process.fechaMutuo"
					value="${process.fechaMutuo}" /></td>
			<th>Moneda</th>
			<td>${ProcessForm.process.moneda.abreviatura}</td>
		</tr>
		<tr>
			<th>Monto de Financiamiento</th>
			<td><asf:text maxlength="40" type="decimal"
					property="process.montoFinanciamiento"
					readonly="${ProcessForm.process.generada}" name="ProcessForm" /></td>
			<th>Sistema de Amortizaci�n</th>
			<td><asf:select name="process.tipoAmortizacion"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="ProcessForm.process.bonificacion.tipoAmortizacion"
					filter="categoria='amortizacion.metodo' order by codigo desc"
					enabled="${ProcessForm.process.gene}" /></td>
		</tr>

		<tr>
			<th>Tasa Inter�s Compensatorio</th>
			<td><asf:selectpop name="process.idIndiceComp" title="Tasa"
					columns="Codigo, Nombre" captions="id,nombre" values="id"
					entityName="com.nirven.creditos.hibernate.Indice"
					value="${ProcessForm.process.idIndiceComp}" filter="tipo in(1,2,3)"
					onChange="tasaComp();" /> <asf:text name="ProcessForm"
					id="indiceValorComp" readonly="true"
					property="process.indiceValorComp"
					value="${ProcessForm.process.indiceValorComp}" type="decimal"
					maxlength="5" /></td>
			<th>Primer Vencimiento Capital</th>
			<td><asf:calendar
					property="ProcessForm.process.primerVencCapitalStr"
					readOnly="${ProcessName.process.generada}" /></td>

		</tr>
		<tr>
			<th>Mas</th>
			<td><html:text property="process.valorMasCompStr" size="15"
					onchange="calcularValorFinalComp();" styleId="masComp" /></td>
			<th>Cantidad Cuotas Capital</th>
			<td><html:text property="process.plazoCapital" size="30"
					name="ProcessForm" readonly="${ProcessForm.process.generada}" /></td>
		</tr>
		<tr>
			<th>Por</th>
			<td><asf:text id="porComp" name="ProcessForm"
					property="process.valorPorCompStr"
					attribs='onchange="calcularValorFinalComp();"' type="decimal"
					maxlength="15" /></td>
			<th>Per�odicidad Cuotas Capital</th>
			<td><asf:select name="process.frecuenciaCapitalStr"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="ProcessForm.process.bonificacion.frecuenciaCapital"
					filter="categoria='amortizacion.periodicidad' order by codigo"
					enabled="${ProcessForm.process.gene}" /></td>
		</tr>
		<tr>
			<th>D�as antes</th>
			<td><html:text value="${ProcessForm.process.diasAntesComp}"
					property="process.diasAntesComp" size="15" /></td>
			<th>Primer Vencimiento Inter�s</th>
			<td><asf:calendar
					property="ProcessForm.process.primerVencInteresStr"
					readOnly="${ProcessName.process.generada}" /></td>
		</tr>
		<tr>
			<th>Tasa Inter�s Compensatorio final</th>
			<td><html:text property="process.valorFinalComp" readonly="true"
					size="15" styleId="valorFinalComp" /></td>
			<th>Cantidad Cuotas Inter�s</th>
			<td><html:text property="process.plazoCompensatorio" size="30"
					readonly="${ProcessForm.process.generada}" /></td>

		</tr>

		<tr>
			<th>Tasa Bonificaci�n</th>
			<td><asf:text name="ProcessForm"
					property="process.tasaBonificacion" type="decimal" maxlength="10"
					value="${ProcessForm.process.tasaBonificacion}" /></td>
			<th>Per�odicidad Cuotas Inter�s</th>
			<td><asf:select name="process.frecuenciaInteresStr"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="ProcessForm.process.bonificacion.frecuenciaInteres"
					filter="categoria='amortizacion.periodicidad' order by codigo"
					enabled="${ProcessForm.process.gene}" /></td>
	</table>
	<logic:equal value="false" property="process.generada"
		name="ProcessForm">
		<div style="width: 100%; text-align: center">
			<input type="button" value="Guardar" onclick="guardar();" />
			<!--  <input type="button" value="Cancelar" onclick="cancelar();"/> -->
		</div>
	</logic:equal>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
	$j = jQuery.noConflict();

	function editarIndice(idIndice) {
		var accion = document.getElementById('accion');
		accion.value = "loadIndice";
		var compen = document.getElementById('comp');
		compen.value = idIndice;
		var form = document.getElementById('oForm');
		form.submit();
	}

	function cancelar() {
		var accion = $('accion');
		accion.value = "load";
		var form = $('ProcessForm');
		form.submit();
	}

	function guardar() {
		var form = document.getElementById('oForm');
		form.submit();
	}

	/*
	 function verificarPorcentaje(porcentaje){
	 var tasa1 = porcentaje.value.replace(',', '.').replace('%', '');
	 if(tasa1 < 0 || tasa1 > 100){
	 alert("Debe ingresar un valor entre 0 y 100");
	 porcentaje.value = '0%';
	 }else{
	 porcentaje.value.replace(',', '.').replace('%', '');
	 porcentaje.value = porcentaje.value.concat('%');
	 }
	 }
	 */

	function actualizarCotizacion() {
		var accion = document.getElementById('accion');
		accion.value = 'actualizarCotizacion';
		var form = document.getElementById('oForm');
		form.submit();
	}

	function calcularValorFinalComp() {
		var idIndice = $j("#process\\.idIndiceComp").val();
		var tasacom = $j("#indiceValorComp").val().replace(',', '.').replace(
				'%', '');
		var mas = $j("#masComp").val().replace(',', '.').replace('%', '');
		var por = $j("#porComp").val().replace(',', '.').replace('%', '');

		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinalPromedio',
						'&idIndice=' + idIndice + '&tasa=' + tasacom + '&mas='
								+ mas + '&por=' + por, function(data) {
							$j("#valorFinalComp").val(data);

						});
	}

	function calcularValorFinalComp(tasaInd) {
		var idIndice = $j("#process\\.idIndiceComp").val();
		var tasacom = $j("#indiceValorComp").val().replace(',', '.').replace(
				'%', '');
		var mas = $j("#masComp").val().replace(',', '.').replace('%', '');
		var por = $j("#porComp").val().replace(',', '.').replace('%', '');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinalPromedio',
						'&idIndice=' + idIndice + '&tasa=' + tasacom + '&mas='
								+ mas + '&por=' + por, function(data) {
							$j("#valorFinalComp").val(data);
						});
	}

	function tasaComp() {
		var idIndice = $j("#process\\.idIndiceComp").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndicePromedio',
						'&idIndice=' + idIndice, function(data) {
							$j("#indiceValorComp").val(data);
							$j("#valorFinalComp").val(
									calcularValorFinalComp(data));
						});

	}
</script>
