<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<style>
.anchoCampo{
	width: 63em;
}
</style>

<div class="title">Director</div>
<br><br>

<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
<table border="0">
<html:hidden property="entity.id" value="${AbmForm.entity.id}"/>

<tr><th>C�digo:</th><td><html:text property="entity.codigo" readonly="${clave}" maxlength="8" styleClass="anchoCampo"/></td></tr>
<tr><th>Nombre:</th><td><html:text property="entity.nombre" maxlength="300" styleClass="anchoCampo"/></td></tr>
<tr><th>Descripci�n:</th><td><html:textarea styleId="Descripci�n" onkeypress="caracteres(this,500,event)" property="entity.descripcion" cols="40" rows="4" styleClass="anchoCampo"/></td></tr>

    
</table>
<br>
<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>

