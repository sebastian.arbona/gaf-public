<%@ page language="java"%>
<jsp:directive.page import="java.net.URLEncoder"/>
<jsp:directive.page import="java.net.URLDecoder"/>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<script type="text/javascript" langauge="javascript">
jQuery.noConflict();
jQuery(document)
		.ready(
				function($) {

					var formulario = jQuery('#oForm');
					var accion = jQuery('#accion');
					var idpersona = jQuery('#idpersona');

					jQuery('#guardar').click(function() {
						accion.val('guardar');
						idpersona.val("${ProcessForm.process.idPersona}");
						formulario.submit();
					});
})

var nav4 = window.Event ? true : false;
var __VENCIDO = "VENCIDO";
var __FCOBRO = "FCOBRO";
var boleto='';

function agregar(val){
   	if( val == ''){										
            return false;
	}else{  
	        var nb = getElem("nb");
            var cmb = getElem("lis");	
            var arr = val.replace("/", "-").split("-");
            val = trim(nb.value);
  	   
	      if( val.length==44 && arr.length==1 ){ //C�digo de Barras
	     	codi = val.substring(7,15);
	     	digito = val.substring(15,16); 
	     	periodo = val.substring(3,7);

	      }
          else if( arr.length==1 ){
             codi = val.substring(0,val.length-5);
             digito = val.substring(val.length-4,val.length-5);
             periodo = val.substring(val.length,val.length-4);
         
          }else{

			if(arr.length!=3){
				alert('El N�MERO DE BOLETO ingresado no es v�lido. Por favor, intente nuevamente.');
				$('nb').focus(); 
				return false;
			}
				
			 codi=arr[0];
			 digito=arr[1];
			 periodo=arr[2];
		  }	    
     
            url="${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&listValues=id.periodoBoleto;id.numeroBoleto;id.verificadorBoleto;fechaVencimiento;importe;tipo&entityName=Boleto&filter="+
            	"id.periodoBoleto='"+periodo+"' AND id.verificadorBoleto='"+digito+"' AND id.numeroBoleto='"+codi+"'";
    		retrieveURL(url, boletoResponse);            
            
            nb.value='';
            nb.focus();
	}
}
function boletoResponse(responseText){
	if(responseText=="<Objects></Objects>"){
		alert('El N�MERO DE BOLETO ingresado no es v�lido. Por favor, intente nuevamente.');
		$('nb').focus();  
		return;
	}
    var objetos=parseXml(responseText);
    boleto = objetos.Objects.Boleto;

    validaBoleto();

}

function validaBoleto(){
            url2="${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxQuery&listValues=id.periodoBoleto;id.numeroBoleto;id.verificadorBoleto;&entityName=Pagos&filter="+
            	"id.periodoBoleto="+boleto.id_periodoBoleto+" and id.numeroBoleto="+boleto.id_numeroBoleto+ " and id.verificadorBoleto="+boleto.id_verificadorBoleto;
            retrieveURL(url2, boletoResponse2);   
            
}

function cambiarCotiz(){
    var moneda = $('entity.moneda_id'); 
    url="${pageContext.request.contextPath}/actions/ajaxHelperAction.do?do=ajaxMonedaCotizacion&idMoneda="+ moneda.value;
    retrieveURL(url, cambiarCotizResponse);
}

function cambiarCotizResponse(responseText){
    $('cotiza').value = responseText; 
}

function boletoResponse2(responseText){
	
	if(responseText=="<Objects></Objects>"){
	  var importe = vencimientos( boleto );
	  if( importe == __FCOBRO ){
        alert( "La FECHA DE COBRANZA est� vac�a. Por favor, intente nuevamente." );
        return null;
      }
     
      //Si el boleto es de tipo anual, semestral o de anticipo, se verifica que no est� vencido a la fecha de cobro..
<%--      var fCobro = getDate( $( "entity.fcob12Str" ).value );--%>
<%--      var fvto = getDate(boleto.vto109);--%>
<%--      var tipo = boleto.tipo == 3 ? 'ANTICIPO' : (boleto.tipo == 2 ? 'ANUAL' : 'SEMESTRAL');--%>
<%--      --%>
<%--      if( boleto.tipo != 0 && ( boleto.vto109!=null && fCobro > fvto ) ) {--%>
<%--      	alert("El N�MERO DE BOLETO "+ tipo +" ingresado est� vencido. Por favor, intente nuevamente.");--%>
<%--      	return null;--%>
<%--      }--%>
    
    
	  lista[lista.length] = boleto.id_numeroBoleto+'-'+boleto.id_verificadorBoleto+'-'+boleto.id_periodoBoleto, boleto.id_numeroBoleto+'-'+boleto.id_verificadorBoleto+'-'+boleto.id_periodoBoleto+' $' + importe;
      cargarItem(boleto.id_numeroBoleto+'-'+boleto.id_verificadorBoleto+'-'+boleto.id_periodoBoleto, boleto.id_numeroBoleto+'-'+boleto.id_verificadorBoleto+'-'+boleto.id_periodoBoleto+' $' + importe);
	  return;
		
	}else{
		alert("El N�MERO DE BOLETO ingresado ya fue registrado. Por favor, intente nuevamente.");
		return null;
	}

}

function vencimientos( boleto ){
var fCobro = $( "entity.fechaCobranzaStr" ).value;

    if( fCobro == '' )        
        return __FCOBRO;

var fechaCobro = getDate( fCobro );
var vto1 = getDate( boleto.fechaVencimiento != null ? boleto.fechaVencimiento : '' );
var dImporte = 0.0;

    if( ( fechaCobro != null && vto1 != null ) && ( fechaCobro.getTime() <= vto1.getTime() ) ){
        dImporte = boleto.importe;
    }
    
    return ( dImporte == null ? 0 : dImporte );
}

function getDate( fecha ){
    var sFecha = fecha.split( "-" );
    var iAnio = 0;
    var iMes = 0;
    var iDia = 0;
    var iAux;
    
        if( sFecha.length <= 1 )
            sFecha = fecha.split( "/" );
       
        if( sFecha == '' || sFecha.length <= 1 )
            return null;
            
        iAnio = new Number( sFecha[ 0 ] );
        iMes = new Number( sFecha[ 1 ] ) - 1;
        iDia = new Number( sFecha[ 2 ] );
    
        if( iDia >= 1900 ){        
            iAux = iAnio;
            iAnio = iDia;
            iDia = iAux;
        }
       
        return new Date( iAnio, iMes, iDia );    
}

function cargarItem(key, value){
	var nb=getElem("nb");
	var cmb = getElem("lis");
    for(var i = 0; i < cmb.length; i++){
       if( cmb.options[ i ].value == value){
             alert("El N�MERO DE BOLETO elegido ya fue agregado. Por favor, intente nuevamente.");
              nb.value='';
              nb.focus();   
              return false;
         }
     } 
	var opt = cmb.appendChild(document.createElement('option'));
    opt.appendChild(document.createTextNode(value));
    opt.setAttribute('value',value);
    
    
    if( cmb.length >= 0 )
    {
		getElem( 'bQuitar' ).disabled = false;
	}
 }

function quitar(){
	var nb=getElem("nb");
	var cmb = getElem("lis");						
	var i = cmb.selectedIndex;						
                                                                                  
	if( navigator.appName != "Netscape" )
    	cmb.removeChild( cmb.children( i ) ); 
	else{    	
        cmb.options[i] = null;
	}

	if( cmb.length == 0 ){
	    nb.value='';
        nb.focus();  
    	getElem( 'bQuitar' ).disabled = true;
    }    
}

function selectAll(){
	lista=getElem('lis');
	for (i = 0; i < lista.length; i++) {
		lista.options[i].selected = true;
	}
	return true;
}

function cambiar( valor ){
    
    if( valor == 0 ){        
        mostrar( "tr_iframe" );       
    }else{                    
        ocultar( "tr_iframe" );
     }

}

function eliminarSesion(){    
    retrieveURL("${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=resetBoletos", null);            
}

function noEliminarSession(){
    getElem( "obody" ).setAttribute( 'onunload', null );
    return true;
}

</script>

<logic:present parameter="cobroPagos">
	<div class="title">Car�tula</div>
</logic:present>
<logic:notPresent parameter="cobroPagos">
   <div class="title">Cobranza</div>
</logic:notPresent>

<br>
<bean:define id="lista" name="AbmFormCaratula" property="lista" type="String[]"/>

<html:form action="/actions/abmActionCaratula.do?do=save&entityName=Caratula" onsubmit="selectAll();">
    <body id="obody" onunload="eliminarSesion();" >
        <html:hidden property="filter" value="true"/>
        <html:hidden property="entity.id" styleId="idcaratula"/>
        <html:hidden property="entity.actualizada" value="0"/> 
        <logic:present parameter="consultaBoletos">
        	<html:hidden property="entity.cobro" value="true"/>
        </logic:present>
     
       <logic:equal parameter="do" value="newEntity">
       		<bean:define id="fenv12" property="entity.fechaEnvioStr" value="${param['paramValue[0]']}" />
       </logic:equal>
       <logic:notEqual parameter="do" value="newEntity">
       		<bean:define id="fenv12" property="entity.fechaEnvioStr" value="${AbmForm.entity.fechaEnvioStr}" />
       </logic:notEqual>

<%--        <bean:define id="filter" name="paramValue" scope="session" type="Object[]"/>   --%>
        <table border="0">
            <tr><th>Nro de Caja:</th><td><asf:text property="entity.grupo" name="AbmForm" type="long" maxlength="4" readonly="true"/></td></tr>
       	    <tr><th>Fecha:</th><td><html:text property="entity.fechaEnvioStr" value="${fenv12}" readonly="true"/></td></tr>
            <tr>
				<th>
					M�todo de Carga:
				</th>
				<td>
					<asf:select name="entity.metodoCargaStr" 
						listCaption="Manual,Autom�tica,Procesamiento,Procesamiento de Pago a cuenta" listValues="1,0,2,3"
						nullValue="true"
						nullText="&lt;&lt;Ninguno&gt;&gt;" attribs="onchange=mostrarFecovita();"
					/>
				</td>
			</tr>

            <tr id="ente">
            	<th>Ente Recaudador:</th><td><asf:select name="entity.banco_id" entityName="com.asf.gaf.hibernate.Bancos" listCaption="getDetaBa" listValues="getCodiBa" value="${AbmForm.entity.banco_id}" filter="recaudador = 1  order by codiBa" /></td>
            </tr>
            <tr id="fechaCobranza">
            	<th>Fecha de Cobranza:</th><td><asf:calendar property="AbmForm.entity.fechaCobranzaStr"/></td>
            </tr>
			<tr id="fecovita">
				<th>Fecovita:</th>
				<td><asf:select name="entity.fecovitaTipo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.fecovitaTipo}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'Linea.Fecovita' order by codigo" 
					nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;"
					/></td>
			</tr>

           <logic:notPresent parameter="cobroPagos"> 
             <tr><th>Total de Comprobantes:</th><td><asf:text property="entity.comprobantes" name="AbmForm" type="long" maxlength="4"/></td></tr>
             <tr><th>Importe Total:</th><td><asf:text property="entity.importe" name="AbmForm" type="decimal" maxlength="10"/></td></tr>
             
           </logic:notPresent>
           
             
           
           <logic:notPresent parameter="cobroPagos"> 
	            <tr><th>M�todo de Carga:</th><td><asf:select name="metodoCarga"  listCaption="Manual,Manual y Autom�tica" listValues="1,0" attribs="onchange='cambiar( this.value )'" /></td></tr>
	            <tr><th>N�mero de Boleto:</th><td>
	                    <input type="text" NAME="nb" id="nb" size="20" onkeypress="if(event.keyCode==13){agregar(this.value);return false;}" maxlength="44">
           		 <input type="button" id="bAgregar" value="Agregar" onclick="agregar(getElem('nb').value);" /></td></tr>
	            <tr><th>Boletos: </th><td><select name="lista" id="lis" size="10" multiple="true" style="width: 300px"/>
	            <input type="button" id="bQuitar" value="Quitar" disabled="true" onclick="quitar();" /></td></tr>       
           </logic:notPresent>
        </table>    
        <br/>
        <logic:notPresent parameter="consultaBoletos">
        <logic:notPresent parameter="cobroPagos">
	        <div id="tr_iframe" style="border: 2px solid;width:60%">
	            <%--iframe src="${pageContext.request.contextPath}/actions/process.do?do=show&processName=Importacion" style="border:0" width="100%" height="200px" ></iframe--%>
	            <iframe src="${pageContext.request.contextPath}/actions/importacion.do?do=load" style="border:0" width="100%" height="200px" ></iframe>
	        </div>
	        <br>
        </logic:notPresent>
        </logic:notPresent>
        
        <asf:security action="/actions/abmActionCaratula.do" access="do=save&entityName=${param.entityName}">
       	  <logic:present parameter="consultaBoletos"><html:submit onclick="noEliminarSession();" ><bean:message key="abm.button.save"/></html:submit></logic:present> 
          <logic:notPresent parameter="consultaBoletos"><html:submit disabled="true" ><bean:message key="abm.button.save"/></html:submit></logic:notPresent>
        </asf:security>
        
        <logic:present parameter="consultaBoletos">
        	<input type="button" value="Cancelar" onclick="window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&process.fenv12Str=${fenv12}&filter=true'"/>
        </logic:present>
        <logic:notPresent parameter="consultaBoletos">
        	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
        </logic:notPresent>
    </body>    
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<script language="javascript" >
<%
	String elementos = "";					//par�metros para iniciar el array js que carga boletos.-
        for(int i = 0; i < lista.length; i++ )	//por cada pago; agregamos un elemento a elementos.-
        {
            elementos += "'" + lista[ i ] + "',";		//armamos los par�metros para iniciar el array
        }
%>
lista = new Array( <%= ( elementos.equals( "" ) || elementos.length() <= 1 ? "''" : elementos.substring( 0, elementos.length() - 1 ) )%>  );	//inicializamos el array con lo que tra�a la lista.- 

if( lista[0] != ""  )
{
	for( i = 0; i < lista.length; i++ )
	{
		aKey  = lista[i].split('-');										
		key   = aKey[0] + '-' + aKey[1] + '-' + aKey[2];					//armamos el key   con: codi - digi - peri.-
		value = aKey[0] + '-' + aKey[1] + '-' + aKey[2] + ' $' + aKey[3];	//armamos el value con: codi - digi - peri - dere.-
		cargarItem(key, value);												//llamamos a cargarItem con el key y value anterior.-
	}
}
try{
    var cmb = getElem( "metodoCarga" );
    cambiar( cmb.options[ cmb.selectedIndex ].value );
}catch(e){}
ocultar('fecovita');
function mostrarFecovita(){
	if($('entity.metodoCargaStr').value == 2)
		mostrar('fecovita');
	else
		ocultar('fecovita');
	
	if($('entity.metodoCargaStr').value == 0)
		ocultar('ente','fechaCobranza');
	else
		mostrar('ente','fechaCobranza');
}
</script>
