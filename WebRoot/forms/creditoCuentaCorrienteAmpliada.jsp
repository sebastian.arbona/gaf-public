<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 15px">Cuenta Corriente</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada"
	styleId="oForm">

	<html:hidden name="ProcessForm" property="process.idObjetoi" />
	<html:hidden name="ProcessForm" property="process.idBoleto" />
	<html:hidden property="process.action" styleId="action" value="buscar" />
	<html:hidden property="process.cid" />
	<table style="margin-bottom: 20px">
		<logic:equal value="false" name="ProcessForm"
			property="process.reducida">
			<tr>
				<th>Ordenar Por:</th>
				<td colspan="4"><html:radio property="process.mostrar"
						value="1" onclick="buscarPorCuotaFecha();" />Por Fecha <html:radio
						property="process.mostrar" value="2"
						onclick="buscarPorCuotaFecha();" />Por Cuota</td>

			</tr>
		</logic:equal>
		<logic:equal value="false" name="ProcessForm"
			property="process.ocultar">
			<tr id="fechas">
				<th>Desde fecha:</th>
				<td><asf:calendar property="ProcessForm.process.desdeFechaStr"
						value="${process.desdeFechaStr}" /></td>
				<th>Hasta fecha:</th>
				<td><asf:calendar property="ProcessForm.process.hastaFechaStr"
						value="${process.hastaFechaStr}" /></td>
			</tr>
			<tr id="busqueda">
				<th>Buscar Por:</th>
				<td colspan="3"><html:radio property="process.fechas" value="1" />Fecha
					Vencimiento <html:radio property="process.fechas" value="2" />Fecha
					Proceso <html:radio property="process.fechas" value="3" />Fecha
					Extracto</td>
			</tr>
		</logic:equal>
		<tr>
			<td colspan="2" style="text-align: center"><html:submit>Actualizar</html:submit>
			</td>
			<td colspan="2" style="text-align: center"><logic:equal
					value="false" name="ProcessForm" property="process.reducida">
					<html:submit onclick="imprimirCtaCteAmpliada();">Imprimir PDF</html:submit>
				</logic:equal> <logic:equal value="true" name="ProcessForm"
					property="process.reducida">
					<html:submit onclick="imprimirCtaCteContable();">Imprimir PDF</html:submit>
				</logic:equal></td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: center">
				<div>
					<a
						href="${ProcessForm.process.contextoGAF}/actions/process.do?processName=ValorCartera&do=process&process.action=list&process.idValores=list&process.nroCredito=${ProcessForm.process.numeroAtencion}"
						target="_blank" style="font-size: 13px;"> MOVIMIENTOS
						PENDIENTES </a>
				</div>
			</td>
		</tr>

	</table>




	<br />
	<br />
	<div class="grilla">
		<logic:equal name="ProcessForm" property="process.mostrar" value="1">
			<display:table name="ProcessForm" property="process.beans"
				id="reportTable" export="true" class="tabla_chica"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada">
				<display:caption>Proyecto ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12} - Periodo: ${ProcessForm.process.desdeFechaStr} al ${ProcessForm.process.hastaFechaStr} (${ProcessForm.process.criterioStr})</display:caption>

				<display:setProperty name="report.title" value="Cuenta Corriente"></display:setProperty>

				<display:column title="Nro. Asiento" style="font-size:11px;">
					<logic:notEqual value="Total" name="reportTable" property="detalle">
						<a
							href="${ProcessForm.process.contextoGAF}/actions/process.do?do=process&processName=Movimientos&process.action=exportar&process.tipoExportacion=PDF&process.idAs=${reportTable.idAsiento}"
							target="_blank">${reportTable.nroAsiento}</a>
					</logic:notEqual>
				</display:column>
				<display:column title="Tipo Comp."
					style="min-width: 80px;font-size:11px;" media="html">
					<logic:equal value="Factura" name="reportTable"
						property="tipoBoleto">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=imprimirBoletos&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}">
							${reportTable.tipoBoleto} </a>
					</logic:equal>
					<logic:equal value="Recibo" name="reportTable"
						property="tipoBoleto">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=imprimirPago&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoletoRecibo}&process.idCaratula=${reportTable.idCaratula}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}">
							${reportTable.tipoBoleto} </a>
					</logic:equal>
					<logic:equal value="Nota Debito" name="reportTable"
						property="tipoBoleto">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualDebito&process.importe=${reportTable.debe}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}">
							Nota Deb </a>
					</logic:equal>
					<logic:equal value="Nota Credito" name="reportTable"
						property="tipoBoleto">
						<logic:notEqual value="Pago Gastos" name="reportTable"
							property="detalle">
							<a
								href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualCredito&process.importe=${reportTable.haber}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}">
								Nota Cred </a>
						</logic:notEqual>
					</logic:equal>
					<logic:equal value="Minuta Debito" name="reportTable"
						property="tipoBoleto">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualDebito&process.importe=${reportTable.debe}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}&process.minuta=true">
							Minuta Deb </a>
					</logic:equal>
					<logic:equal value="Minuta Credito" name="reportTable"
						property="tipoBoleto">
						<a
							href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualCredito&process.importe=${reportTable.haber}&process.fechaRepStr=${reportTable.fechaGeneracionStr}&process.conceptoRep=${reportTable.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${reportTable.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}&process.minuta=true">
							Minuta Cred </a>
					</logic:equal>
					<logic:notEqual value="Factura" name="reportTable"
						property="tipoBoleto">
						<logic:notEqual value="Recibo" name="reportTable"
							property="tipoBoleto">
							<logic:notEqual value="Nota Credito" name="reportTable"
								property="tipoBoleto">
								<logic:notEqual value="Nota Debito" name="reportTable"
									property="tipoBoleto">
									<logic:notEqual value="Minuta Credito" name="reportTable"
										property="tipoBoleto">
										<logic:notEqual value="Minuta Debito" name="reportTable"
											property="tipoBoleto">
											${reportTable.tipoBoleto}
										</logic:notEqual>
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual>
					</logic:notEqual>

					<logic:equal value="Desembolso" name="reportTable"
						property="detalle">
						<a
							href="${ProcessForm.process.contextoGAF}/actions/process.do?do=process&processName=PagoOP&process.action=imprimirReciboPago&process.ordenpago.id=${reportTable.idOrdenPago}&process.ordenpago.ordenpago=${reportTable.numeroOrdenPago}&process.ordenpago.ejercicio=${reportTable.ejercicioOrdenPago}"
							target="_blank"> Recibo </a>
					</logic:equal>
					<logic:equal value="Pago Gastos" name="reportTable"
						property="detalle">
						<a
							href="${ProcessForm.process.contextoGAF}/actions/process.do?do=process&processName=PagoOP&process.action=imprimirReciboPago&process.ordenpago.id=${reportTable.idOrdenPago}&process.ordenpago.ordenpago=${reportTable.numeroOrdenPago}&process.ordenpago.ejercicio=${reportTable.ejercicioOrdenPago}"
							target="_blank"> Recibo </a>
					</logic:equal>
				</display:column>
				<display:column title="Tipo Comp."
					style="min-width: 80px;font-size:11px;" media="pdf excel xml">
					<logic:equal value="Factura" name="reportTable"
						property="tipoBoleto">
							${reportTable.tipoBoleto}
					</logic:equal>
					<logic:equal value="Recibo" name="reportTable"
						property="tipoBoleto">
							${reportTable.tipoBoleto}
					</logic:equal>
					<logic:equal value="Nota Debito" name="reportTable"
						property="tipoBoleto">
				   			Nota Deb
					</logic:equal>
					<logic:equal value="Nota Credito" name="reportTable"
						property="tipoBoleto">
						<logic:notEqual value="Pago Gastos" name="reportTable"
							property="detalle">
					   			Nota Cred
					   	</logic:notEqual>
					</logic:equal>
					<logic:equal value="Minuta Debito" name="reportTable"
						property="tipoBoleto">
				   			Minuta Deb
					</logic:equal>
					<logic:equal value="Minuta Credito" name="reportTable"
						property="tipoBoleto">
				   			Minuta Cred
					</logic:equal>
					<logic:notEqual value="Factura" name="reportTable"
						property="tipoBoleto">
						<logic:notEqual value="Recibo" name="reportTable"
							property="tipoBoleto">
							<logic:notEqual value="Nota Credito" name="reportTable"
								property="tipoBoleto">
								<logic:notEqual value="Nota Debito" name="reportTable"
									property="tipoBoleto">
									<logic:notEqual value="Minuta Credito" name="reportTable"
										property="tipoBoleto">
										<logic:notEqual value="Minuta Debito" name="reportTable"
											property="tipoBoleto">
											${reportTable.tipoBoleto}
										</logic:notEqual>
									</logic:notEqual>
								</logic:notEqual>
							</logic:notEqual>
						</logic:notEqual>
					</logic:notEqual>
					<logic:equal value="Desembolso" name="reportTable"
						property="detalle">
							Recibo
					</logic:equal>
					<logic:equal value="Pago Gastos" name="reportTable"
						property="detalle">
							Recibo
					</logic:equal>
				</display:column>
				<display:column title="Nro. Comp." class="center"
					property="numeroBoleto" style="font-size:11px;" />
				<display:column class="center"
					title="Fecha Proceso/ Contable/ D&iacute;a de Carga/ Comprobante"
					property="fechaProceso"
					decorator="com.asf.displayDecorators.DateDecorator"
					style="font-size:11px;" />
				<display:column class="center"
					title="Fecha Vencimiento/ Aplicaci&oacute;n/ C&aacute;lculo"
					property="fechaVencimiento"
					decorator="com.asf.displayDecorators.DateDecorator"
					style="font-size:11px;" />
				<display:column class="center"
					title="Fecha Extracto/ Ingreso de Fondos/ Efectivo Pago/ Ente Recaudador"
					property="fechaGeneracion"
					decorator="com.asf.displayDecorators.DateDecorator"
					style="font-size:11px;" />
				<display:column title="Detalle" property="detalle"
					style="min-width: 90px; font-size:11px;" />
				<logic:equal value="false" name="ProcessForm"
					property="process.reducida">
					<display:column class="right ctacte" title="Capital" media="html"
						style="font-size:11px;">
						<logic:lessThan value="0" name="reportTable" property="capital">
							<span style="color: red;">${reportTable.capitalStr}</span>
						</logic:lessThan>
						<logic:greaterThan value="0" name="reportTable" property="capital">
							${reportTable.capitalStr}
						</logic:greaterThan>
					</display:column>
					<display:column title="Capital" media="pdf excel xml"
						property="capitalStr" style="font-size:11px;" />
					<logic:equal name="ProcessForm" property="process.cer" value="true">
						<display:column class="right ctacte" title="CER" media="html"
							style="font-size:11px;">
							<logic:lessThan value="0" name="reportTable" property="cer">
								<span style="color: red;">${reportTable.cerStr}</span>
							</logic:lessThan>
							<logic:greaterThan value="0" name="reportTable" property="cer">
							${reportTable.cerStr}
						</logic:greaterThan>
						</display:column>
						<display:column title="CER" media="pdf excel xml"
							property="cerStr" style="font-size:11px;" />
					</logic:equal>
					<display:column class="right ctacte" title="Inter&eacute;s Comp."
						media="html" style="font-size:11px;">
						<logic:lessThan value="0" name="reportTable"
							property="compensatorio">
							<span style="color: red;">${reportTable.compensatorioStr}</span>
						</logic:lessThan>
						<logic:greaterThan value="0" name="reportTable"
							property="compensatorio">
							${reportTable.compensatorioStr}
						</logic:greaterThan>
					</display:column>
					<display:column title="Inter&eacute;s Comp." media="pdf excel xml"
						property="compensatorioStr" style="font-size:11px;" />
					<display:column class="right ctacte" title="Inter&eacute;s Moratorio"
						media="html" style="font-size:11px;">
						<logic:lessThan value="0" name="reportTable" property="moratorio">
							<span style="color: red;">${reportTable.moratorioStr}</span>
						</logic:lessThan>
						<logic:greaterThan value="0" name="reportTable"
							property="moratorio">
							${reportTable.moratorioStr}
						</logic:greaterThan>
					</display:column>
					<display:column title="Inter&eacute;s Moratorio" media="pdf excel xml"
						property="moratorioStr" style="font-size:11px;" />
					<display:column class="right ctacte" title="Inter&eacute;s Punitorio"
						media="html" style="font-size:11px;">
						<logic:lessThan value="0" name="reportTable" property="punitorio">
							<span style="color: red;">${reportTable.punitorioStr}</span>
						</logic:lessThan>
						<logic:greaterThan value="0" name="reportTable"
							property="punitorio">
							${reportTable.punitorioStr}
						</logic:greaterThan>
					</display:column>
					<display:column title="Inter&eacute;s Punitorio" media="pdf excel xml"
						property="punitorioStr" style="font-size:11px;" />
					<display:column class="right ctacte" title="Otros" media="html"
						style="font-size:11px;">
						<logic:lessThan value="0" name="reportTable" property="gastos">
							<span style="color: red;">${reportTable.gastosStr}</span>
						</logic:lessThan>
						<logic:greaterThan value="0" name="reportTable" property="gastos">
							${reportTable.gastosStr}
						</logic:greaterThan>
					</display:column>
					<display:column title="Gastos / Multas" media="pdf excel xml"
						property="gastosStr" style="font-size:11px;" />
				</logic:equal>

				<logic:notEqual value="true" name="ProcessForm"
					property="process.esDolares">
					<display:column class="right" title="Debe" property="debe"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Haber" property="haber"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Saldo" property="saldo"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				</logic:notEqual>
				<logic:equal value="true" name="ProcessForm"
					property="process.esDolares">
					<display:column class="right" title="Debe U$S" property="debe"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Haber U$S" property="haber"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Saldo U$S" property="saldo"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Cot." property="cotizaMov"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Debe $" property="debePesos"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Haber $" property="haberPesos"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Saldo $" property="saldoPesos"
						style="font-size:11px;"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				</logic:equal>
			</display:table>

			<h3>Calificaci&oacute;n del Saldo de Capital seg&uacute;n la fecha de cobro
				prevista</h3>
			<display:table name="ProcessForm" property="process.resumenBeans"
				id="resumen">
				<display:column title="Detalle" property="detalle" />
				<logic:equal value="true" name="ProcessForm"
					property="process.esDolares">
					<display:column class="right" title="Capital U$S"
						property="capital"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Inter&eacute;s U$S"
						property="interes"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Otros U$S" property="otros"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Total U$S" property="total"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Capital $"
						property="pesos.capital"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Inter&eacute;s $"
						property="pesos.interes"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Otros $"
						property="pesos.otros"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Total $"
						property="pesos.total"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				</logic:equal>
				<logic:equal value="false" name="ProcessForm"
					property="process.esDolares">
					<display:column class="right" title="Capital" property="capital"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Inter&eacute;s" property="interes"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Otros" property="otros"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
					<display:column class="right" title="Total" property="total"
						decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				</logic:equal>
			</display:table>

			<logic:notEmpty name="ProcessForm" property="process.resumenExigible">
				<h3>Calificaci&oacute;n del Saldo seg&uacute;n su Exigibilidad a la Fecha</h3>
				<table>
					<tr>
						<th>Detalle</th>
						<th>Capital</th>
						<th>Inter&eacute;s</th>
						<th>Otros</th>
						<th>Total</th>
					</tr>
					<tr>
						<td>${ProcessForm.process.resumenExigible.detalle}</td>
						<td><bean:write name="ProcessForm"
								property="process.resumenExigible.capital"
								format="#,##0.00;-#,##0.00" /></td>
						<td><bean:write name="ProcessForm"
								property="process.resumenExigible.interes"
								format="#,##0.00;-#,##0.00" /></td>
						<td><bean:write name="ProcessForm"
								property="process.resumenExigible.otros"
								format="#,##0.00;-#,##0.00" /></td>
						<td><bean:write name="ProcessForm"
								property="process.resumenExigible.total"
								format="#,##0.00;-#,##0.00" /></td>
					</tr>
				</table>
			</logic:notEmpty>
		</logic:equal>

		<logic:equal name="ProcessForm" property="process.mostrar" value="2">
			<table id="reportTable" class="tabla_chica">
				<thead>
					<tr>
						<th style="font-size: 11px;">Asiento Nro.</th>
						<th style="min-width: 80px; font-size: 11px;">Tipo Comp.</th>
						<th style="font-size: 11px;">Nro. Comp.</th>
						<th style="font-size: 11px;">Fecha Proceso/ Contable/ D&iacute;a
							de Carga/ De Comprobante</th>
						<th style="font-size: 11px;">Fecha Vencimiento/ Aplicaci&oacute;n/
							C&aacute;lculo</th>
						<th style="font-size: 11px;">Fecha Extracto /Ingreso de
							Fondos /Efectivo Pago /Fecha Ente Recaudador</th>



						<th style="font-size: 11px;">Cuota</th>
						<th style="min-width: 90px; font-size: 11px;">Detalle</th>
						<th style="font-size: 11px;">Estado</th>
						<th style="font-size: 11px;">Capital</th>
						<logic:equal name="ProcessForm" property="process.cer"
							value="true">
							<th style="font-size: 11px;">CER</th>
						</logic:equal>
						<th style="font-size: 11px;">Inter&eacute;s Comp.</th>
						<th style="font-size: 11px;">Inter&eacute;s Moratorio</th>
						<th style="font-size: 11px;">Inter&eacute;s Punitorio</th>
						<th style="font-size: 11px;">Gastos / Multas</th>
						<th style="font-size: 11px;">Saldo Cuota</th>
					</tr>
				</thead>
				<tbody>
					<logic:iterate id="bean" name="ProcessForm"
						property="process.beans" indexId="i">
						<tr class="${bean.cuota % 2 == 0 ? 'even' : 'odd'}">
							<td style="font-size: 11px;">${bean.asiento != null ? bean.asiento : ''}</td>
							<td style="font-size: 11px;"><logic:equal value="Factura"
									name="bean" property="tipoBoleto">
									<a
										href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=imprimirBoletos&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${bean.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}">
										${bean.tipoBoleto} </a>
								</logic:equal> <logic:equal value="Recibo" name="bean" property="tipoBoleto">
									<a
										href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=imprimirPago&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${bean.idBoletoRecibo}&process.idCaratula=${bean.idCaratula}&process.pago.medioPago=&process.mostrar=${ProcessForm.process.mostrar}&process.cid=${ProcessForm.process.cid}">
										${bean.tipoBoleto} </a>
								</logic:equal> <logic:equal value="Nota Debito" name="bean"
									property="tipoBoleto">
									<a
										href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualDebito&process.importe=${bean.debe}&process.fechaRepStr=${bean.fechaGeneracionStr}&process.conceptoRep=${bean.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${bean.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}">
										Nota Deb </a>
								</logic:equal> <logic:equal value="Nota Credito" name="bean"
									property="tipoBoleto">
									<a
										href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualCredito&process.importe=${bean.haber}&process.fechaRepStr=${bean.fechaGeneracionStr}&process.conceptoRep=${bean.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${bean.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}">
										Nota Cred </a>
								</logic:equal> <logic:equal value="Minuta Debito" name="bean"
									property="tipoBoleto">
									<a
										href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualDebito&process.importe=${bean.debe}&process.fechaRepStr=${bean.fechaGeneracionStr}&process.conceptoRep=${bean.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${bean.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}">
										Minuta Deb </a>
								</logic:equal> <logic:equal value="Minuta Credito" name="bean"
									property="tipoBoleto">
									<a
										href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=imprimirManualCredito&process.importe=${bean.haber}&process.fechaRepStr=${bean.fechaGeneracionStr}&process.conceptoRep=${bean.detalle}&process.observaciones=&process.idObjetoi=${ProcessForm.process.idObjetoi}&process.idBoleto=${bean.idBoleto}&process.mostrar=${ProcessForm.process.mostrar}">
										Minuta Cred </a>
								</logic:equal> <logic:notEqual value="Factura" name="bean"
									property="tipoBoleto">
									<logic:notEqual value="Recibo" name="bean"
										property="tipoBoleto">
										<logic:notEqual value="Nota Credito" name="bean"
											property="tipoBoleto">
											<logic:notEqual value="Nota Debito" name="bean"
												property="tipoBoleto">
												<logic:notEqual value="Minuta Credito" name="bean"
													property="tipoBoleto">
													<logic:notEqual value="Minuta Debito" name="bean"
														property="tipoBoleto">
														${bean.tipoBoleto}
													</logic:notEqual>
												</logic:notEqual>
											</logic:notEqual>
										</logic:notEqual>
									</logic:notEqual>
								</logic:notEqual></td>
							<td class="center" style="font-size: 11px;">${bean.numeroBoleto}</td>
							<td class="center" style="font-size: 11px;">${bean.fechaProcesoStr}</td>
							<td class="center" style="font-size: 11px;">${bean.fechaVencimientoStr}</td>
							<td class="center" style="font-size: 11px;">${bean.fechaGeneracionStr}</td>

							<td class="center" style="font-size: 11px;">${bean.detalle == 'Capital' ? bean.cuota : ''}</td>
							<td style="font-size: 11px;">${bean.detalle}</td>
							<td style="font-size: 11px;">${bean.detalle == 'Desembolso' ? bean.estado : ''}</td>
							<td class="right" style="font-size: 11px;"><logic:lessThan
									value="0" name="bean" property="capital">
									<span style="color: red;">${bean.capitalStr}</span>
								</logic:lessThan> <logic:greaterThan value="0" name="bean" property="capital">
									${bean.capitalStr}
								</logic:greaterThan></td>
							<logic:equal name="ProcessForm" property="process.cer"
								value="true">
								<td class="right" style="font-size: 11px;"><logic:lessThan
										value="0" name="bean" property="cer">
										<span style="color: red;">${bean.cerStr}</span>
									</logic:lessThan> <logic:greaterThan value="0" name="bean" property="cer">
									${bean.cerStr}
								</logic:greaterThan></td>
							</logic:equal>
							<td class="right" style="font-size: 11px;"><logic:lessThan
									value="0" name="bean" property="compensatorio">
									<span style="color: red;">${bean.compensatorioStr}</span>
								</logic:lessThan> <logic:greaterThan value="0" name="bean"
									property="compensatorio">
									${bean.compensatorioStr}
								</logic:greaterThan></td>
							<td class="right" style="font-size: 11px;"><logic:lessThan
									value="0" name="bean" property="moratorio">
									<span style="color: red;">${bean.moratorioStr}</span>
								</logic:lessThan> <logic:greaterThan value="0" name="bean" property="moratorio">
									${bean.moratorioStr}
								</logic:greaterThan></td>
							<td class="right" style="font-size: 11px;"><logic:lessThan
									value="0" name="bean" property="punitorio">
									<span style="color: red;">${bean.punitorioStr}</span>
								</logic:lessThan> <logic:greaterThan value="0" name="bean" property="punitorio">
									${bean.punitorioStr}
								</logic:greaterThan></td>
							<td class="right" style="font-size: 11px;"><logic:lessThan
									value="0" name="bean" property="gastos">
									<span style="color: red;">${bean.gastosStr}</span>
								</logic:lessThan> <logic:greaterThan value="0" name="bean" property="gastos">
									${bean.gastosStr}
								</logic:greaterThan></td>
							<td class="right" style="font-size: 11px;">${bean.saldoStr}
							</td>
						</tr>
					</logic:iterate>
				</tbody>
			</table>
		</logic:equal>
	</div>
</html:form>
<script type="text/javascript">
	function imprimirCtaCteAmpliada() {
		var accion = $('action');
		accion.value = 'imprimirCtaCteAmpliada';
	}
	function imprimirCtaCteContable() {
		var accion = $('action');
		accion.value = 'imprimirCtaCteContable';
	}
	function buscarPorCuotaFecha() {
		var accion = $('action');
		var formulario = $('oForm');
		accion.value = 'buscarPorCuotaFecha';
		formulario.submit();
	}
	function mostrar(radio) {
		if (radio.value == "1") {
			document.getElementsById('fechas').style.display = "";
			document.getElementsById('busquedas').style.display = "";
		} else if (radio.value == "2") {
			ocultar('fechas', 'busquedas');
			var accion = $('action');
			var formulario = $('oForm');
			accion.value = 'buscarPorCuotaFecha';
			formulario.submit();
		}
	}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
