<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<div class="title">
  Sector
</div>
<br>
<br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
  <table border="0">
    <tr>
      <th>
        Id:
      </th>
      <td>
        <asf:text name="AbmForm" property="entity.id" type="long" maxlength="8" readonly="true" />
      </td>
    </tr>
    <tr>
      <th>
        Nombre:
      </th>
      <td>
        <html:text property="entity.nombre" maxlength="30" />
      </td>
    </tr>
    <tr>
      <th>
        Detalle:
      </th>
      <td>
        <html:textarea styleId="detalle" onkeypress="caracteres(this,50,event)" property="entity.detalle" cols="40" rows="4" />
      </td>
    </tr>
    <tr>
	  <th>
		 Bonificacion:
	  </th>
			<td>
				<asf:select name="entity.bonificacion_sino" value="${AbmForm.entity.bonificacion_sino}"
					listCaption="Si,No" listValues="true,false" />
			</td>
		</tr>
  </table>
  <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
    <html:submit>
      <bean:message key="abm.button.save" />
    </html:submit>
  </asf:security>
  <html:cancel>
    <bean:message key="abm.button.cancel" />
  </html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
<!-- Raul Varela -->