<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="fck"%>
<div class="title">Modelo de Escrito</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<logic:equal parameter="do" value="load">
		<html:hidden property="entity.nuevo" value="false" />
	</logic:equal>
	<table border="0">
		<tr>
			<th width="40%">C�digo:</th>
			<td><html:text property="entity.id" readonly="true" /></td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><html:text property="entity.nombre" /></td>
		</tr>
		<tr>
			<th>Tipo:</th>
			<td><asf:select name="entity.tipoEscrito"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${AbmForm.entity.tipoEscrito}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'escrito.tipo'  order by codigo" /></td>
		</tr>
		<tr>
			<th>Detalle:</th>
			<td><fck:editor instanceName="entity.bodyStr" width="750"
					height="600" value="${AbmForm.entity.bodyStr}" /></td>
		</tr>
	</table>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
