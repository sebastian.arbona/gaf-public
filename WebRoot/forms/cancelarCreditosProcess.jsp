<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Cancelación de Créditos</div>
<html:form
	action="/actions/process.do?do=process&processName=CancelarCreditosProcess"
	styleId="ProcessForm">
	<html:hidden property="process.accion" styleId="accion" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<logic:notEmpty name="ProcessForm" property="process.mensaje">
		<div style="width: 70%" align="left">
			${ProcessForm.process.mensaje}</div>
	</logic:notEmpty>
	<html:submit value="Ejecutar" onclick="actualizar();" />
</html:form>

<script type="text/javascript">
	var accion = $('accion');

	function actualizar() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?do=process"
				+ "&processName=${param.processName}"
				+ "&process.accion=cancelar";
	}
</script>