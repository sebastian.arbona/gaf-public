<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ page import="com.civitas.hibernate.persona.Provin"%>
<%@ page import="com.asf.security.BusinessPersistance"%>
<%@ page import="com.asf.security.SessionHandler"%>

<%
	BusinessPersistance bp = SessionHandler.getSessionHandler()
			.getBusinessPersistance();
	Provin provin = null;
%>

<logic:present parameter="idprovincia">
	<bean:define id="codi08" value="${param.idprovincia}" />

	<%
		provin = (Provin) bp.getById(Provin.class, new Long(codi08));
	%>
</logic:present>
<logic:notPresent parameter="idprovincia">
	<bean:define id="codi08" value="${paramValue[0]}" />

	<%
		provin = (Provin) bp.getById(Provin.class, new Long(codi08));
	%>
</logic:notPresent>
<div class="title">Localidad</div>
<br>
<br>
<html:form
	action="/actions/${ empty param.altaRapida ? 'abmAction' : 'altaRapidaGenericaAction' }.do?do=save&entityName=${param.entityName}${ empty param.altaRapida ? '' : '&forward=LocalidadAltaRapida' }">
	<table border="0">
		<bean:define id="filter" name="paramValue" scope="session"
			type="Object[]" />
		<html:hidden property="entity.idlocalidad" />
		<html:hidden property="entity.codi08"
			value="${empty param.idprovincia ? filter[0] : param.idprovincia}" />
		<html:hidden property="filter" value="true" />

		<tr>
			<th>Provincia:</th>
			<td><b><%=provin.getDeta08().toString()%></b></td>
		</tr>

		<tr>
			<th>Localidad:</th>
			<td><html:text property="entity.nombre" maxlength="50" /></td>
		</tr>
		<tr>
			<th>Abreviatura:</th>
			<td><html:text property="entity.abrev" maxlength="15" /></td>
		</tr>
		<tr>
			<th>C�digo Postal:</th>
			<td><html:text property="entity.cp" maxlength="15" /></td>
		</tr>
	</table>
	<asf:security
		action="/actions/${ empty param.altaRapida ? 'abmAction' : 'altaRapidaGenericaAction' }.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>

