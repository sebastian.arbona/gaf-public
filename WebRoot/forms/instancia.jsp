<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Instancia</div>

<br><br>
<div class="formContainer">
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=false">

	<table border="0">    
	<tr>
		<th>ID:</th>
		<td><html:text property="entity.id" readonly="true" maxlength="6" value="${AbmForm.entity.id}"/></td>
	</tr>
	<tr>
		<th>Nombre:</th>
		<td>
			<html:text property="entity.nombre" styleId="nombre" maxlength="50" value="${AbmForm.entity.nombre}"/>
		</td>
	</tr>
	<tr>
		<th>Gasto asociado:</th>
		<td><asf:text property="entity.gastoAsociado" id="gastoAsociado" maxlength="30" value="${AbmForm.entity.gastoAsociado}" type="decimal" name="AbmForm"/></td>
	</tr>
	<tr>
		<th>Orden:</th>
		<td><asf:text property="entity.orden" id="orden" maxlength="30" value="${AbmForm.entity.orden}" type="long" name="AbmForm" readonly="${AbmForm.entity.ordenSoloLectura}"/></td>
	</tr>	
	</table>

<br><br>

<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit styleId="bGuardar" ><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>
</div>
<script type="text/javascript">
//--------Variables utilitarias------------//
var msjError = "";
//--------Controles de la p�gina-----------//
var nombre			= $('nombre');
var gastoAsociado 	= $('gastoAsociado');
var orden         	= $('orden');
//---------Al Iniciar la p�gina------------//
//if('${AbmForm.entity.id}' == 0)
//{
//	codigo.focus();
//}
//else
//{
//	abreviatura.focus();
//}
//--------funciones-----------//
function validar()
{
	var retorno = true;

	msjError = "";
	if(trim(nombre.value) == '')
	{
		msjError += "Debe ingresar un NOMBRE para la instancia.\n";
	}

	if(trim(gastoAsociado.value) == '')
	{
		msjError += "Debe ingresar gasto asociado a la instancia.\n";
	}
	
	if(trim(orden.value) == '')
	{
		msjError += "Debe ingresar el orden asociado a la instancia.\n";
	}
	
	retorno = msjError == "";
	error();

	return retorno;
}

/**
  * Muestra los errores cargados en msjError; y limpia la variable.-
  */
function error()
{
	var retorno = true;

	if( msjError != "" )
	{
		msjError += "Por favor, intente nuevamente.";
		alert( msjError );
		msjError = "";
		retorno = false;
	}

	return retorno;
}//fin error.-
</script>