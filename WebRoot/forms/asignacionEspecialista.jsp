<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script language="javascript" type="text/javascript">
	function back() {
		window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=Especialidad";
	}
</script>

<div class="title">Asignaci�n de ${param.especialidad}</div>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">

	<html:hidden property="process.asignacion.especialista.id"
		name="ProcessForm" />

	<br>

	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br>
	<table border="0">
		<tr>
			<th>Especialista:</th>
			<td>${param.nombre}</td>
		</tr>
		<tr>
			<th>Descripci�n:</th>
			<td><asf:text maxlength="400" type="textarea" cols="40"
					rows="10" property="ProcessForm.process.asignacion.descripcion"
					value="${process.asignacion.descripcion}"></asf:text></td>
		</tr>
		<tr>
			<th>Asignaci�n Aceptada?:</th>
			<td><html:radio name="ProcessForm"
					property="process.asignacion.acepta" value="true">S�</html:radio> <html:radio
					name="ProcessForm" property="process.asignacion.acepta"
					value="false">No</html:radio></td>
		</tr>
		<tr>
			<th>Fecha Aceptaci�n:</th>
			<td><asf:calendar
					value="${process.asignacion.fechaAceptacionStr}"
					property="ProcessForm.process.asignacion.fechaAceptacionStr"></asf:calendar>
		</tr>
	</table>

	<html:submit>
		<bean:message key="abm.button.save" />
	</html:submit>
	<button type="button" onclick="javascript:back();">
		<bean:message key="abm.button.back" />
	</button>

</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>