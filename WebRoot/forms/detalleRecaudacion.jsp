<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@page import="com.nirven.creditos.hibernate.Linea"%>
<%@page import="com.asf.gaf.hibernate.Recaudacion"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ page import="com.asf.util.DateHelper"%>
<%@ page import="java.util.Date"%>
<div class="title">
	Detalle de Importación de Recaudaciones
</div>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="action" />
	<html:hidden property="process.idCaratula" styleId="idCaratula" />
	<html:hidden property="process.idFechaCobranza" styleId="idFechaCobranza" />
	<div align="left" style="position: relative; left: 120px">
		<html:errors />
	</div>

	<br />
	<table>
		<tr>
			<th>
				Banco:
			</th>
			<td>
				${ProcessForm.process.bancoPantalla}
			</td>
		
			<th>
				Fecha de Importación:
			</th>
			<td>
				${ProcessForm.process.fechaStr}
			</td>
		</tr>
	</table>
<br />
<br/>
	<br />
	Registros del TXT vinculados automáticamente con un Extracto Bancario en GAF: ${ProcessForm.process.countAsociadas} registros.
	<br />
	<table>
	<tr>
		<th>
			Fecha Movimiento
		</th>
		<th>
			Ente Rec.
		</th>
		<th>
			Sucursal
		</th>
		<th>
			Cuenta
		</th>
		<th>
			Cod.Ope.
		</th>
		<th>
			Nro.Ope.
		</th>
		<th>
			Importe
		</th>
		<th>
			Saldo
		</th>
		<th>
			Saldo Temp
		</th>

	</tr>
	<logic:iterate  id="rec" name="ProcessForm" 
	property="process.recaudaciones" indexId="index">
		<tr>
			<td>
				<%= DateHelper.getString(((Recaudacion) rec).getFechamovimiento()) %>
			</td>
			<td>
				${rec.bancos.detaBa}
			</td>
			<td>
				${rec.bancos.codiBa}
			</td>
			<td>
				${rec.bancos.cuenBa}
			</td>
			<td>
				${rec.codigoOperacion}
			</td>
			<td>
				${rec.nroOperacion}
			</td>
			<td>
				${rec.importeStr}
			</td>
			<td>
				${rec.saldoStr}
			</td>
			<td>
				${rec.saldoTemporalStr}
			</td>
		</tr>
		<tr>
			<td colspan="8" style="padding-left: 30px">
				<table style="background-color: white">
				<tr>
					<th>
						Titular
					</th>
					<th>
						Proyecto
					</th>
					<th>
						Correlaci&oacute;n
					</th>
					<th>
						Expediente
					</th>
					<th>
						Fecha Pago
					</th>
					<th>
						Cod. Op.
					</th>
					<th>
						Importe
					</th>
					<th>
						Nro Cheque
					</th>
					<th>
						Estado
					</th>
					
				</tr>
				<bean:define id="process" name="ProcessForm" property="process"></bean:define>
				<logic:iterate id="det" name="process" property='<%= "detalles(" + ((Recaudacion) rec).getId() + ")" %>'>
				<tr style="background-color: white">
					<td>
						${det.objetoi.persona.nomb12}
					</td>
					<td>
						${det.numeroProyecto}
					</td>
					<td>
						${det.numeroCorrelacion}
					</td>
					<td>
						${det.objetoi.expediente}
					</td>
					<td>
						${det.fechaAcreditacionValor}
					</td>
					<td>
						${det.numeroOperacion}
					</td>
					<td class="right">
						${det.importeStr}
					</td>
					<td>
						${det.numeroValor}
					</td>
					<td>
						${det.estadoActual.estado}
					</td>
				</tr>
				</logic:iterate>
				</table>
			</td>
		</tr>
		</logic:iterate>
	</table>
	<br />

	<br>
	Cheques presentados: ${ProcessForm.process.countPendientes} registros.
	<display:table name="ProcessForm.process" property="pendientes"
		id="reportTable" style="background-color: white">
		<display:column property="objetoi.persona.nomb12" title="Titular"></display:column>
		<display:column property="numeroProyecto" title="Proyecto"></display:column>
		<display:column property="numeroCorrelacion" title="Correlación"></display:column>
		<display:column property="objetoi.expediente" title="Expediente"></display:column>
		<display:column property="fechaAcreditacionValor" title="Fecha Pago"></display:column>
		<display:column property="numeroValor" title="Nro Cheque"></display:column>
		<display:column property="numeroOperacion" title="Nro Ope"></display:column>
		<display:column property="importeStr" title="Importe" class="right"></display:column>
		<display:column property="estadoActual.estado" title="Estado"></display:column>
		<display:footer>
			<tr>
				<td colspan="6">TOTAL</td>
				<td colspan="3">
				${ProcessForm.process.totalPendientes}	
				</td>
			</tr>
		</display:footer>
	</display:table>
	<br />

	<br>
	Cheques rechazados: ${ProcessForm.process.countRechazadas} registros.
	<display:table name="ProcessForm.process" property="rechazadas"
		id="reportTable" style="background-color: white">
		<display:column property="objetoi.persona.nomb12" title="Titular"></display:column>
		<display:column property="numeroProyecto" title="Proyecto"></display:column>
		<display:column property="numeroCorrelacion" title="Correlación"></display:column>
		<display:column property="objetoi.expediente" title="Expediente"></display:column>
		<display:column property="fechaAcreditacionValor" title="Fecha Pago"></display:column>
		<display:column property="numeroValor" title="Nro Cheque"></display:column>
		<display:column property="numeroOperacion" title="Nro Ope"></display:column>
		<display:column property="importeStr" title="Importe" class="right"></display:column>
		<display:column property="estadoActual.estado" title="Estado"></display:column>
		<display:footer>
			<tr>
				<td colspan="6">TOTAL</td>
				<td colspan="3">
				${ProcessForm.process.totalRechazadas}	
				</td>
			</tr>
		</display:footer>
	</display:table>
	<br />

	<br>
	Registros del archivo pendientes de imputación, no encontrados en el Extracto Bancario: ${ProcessForm.process.countNoImportadas} registros.
	<display:table name="ProcessForm.process" property="noImportadas"
		id="reportTable" style="background-color: white">
		<display:column property="objetoi.persona.nomb12" title="Titular"></display:column>
		<display:column property="numeroProyecto" title="Proyecto"></display:column>
		<display:column property="numeroCorrelacion" title="Correlación"></display:column>
		<display:column property="objetoi.expediente" title="Expediente"></display:column>
		<display:column property="fechaAcreditacionValor" title="Fecha Pago"></display:column>
		<display:column property="numeroValor" title="Nro Cheque"></display:column>
		<display:column property="numeroOperacion" title="Nro Ope"></display:column>
		<display:column property="importe" title="Importe"></display:column>
		<display:column property="estadoActual.estado" title="Estado"></display:column>
		<display:column property="tipoValor" title="Tipo Valor"></display:column>
		<display:footer>
			<tr>
				<td colspan="7">TOTAL</td>
				<td colspan="3">
				${ProcessForm.process.totalNoImportadas}	
				</td>
			</tr>
		</display:footer>
	</display:table>
	<br />
	
	Registros inválidos: ${ProcessForm.process.countInvalidos} registros.
	<display:table name="ProcessForm" property="process.invalidos" id="reportTable" style="background-color: white">
		<display:column property="numeroProyecto" title="Proyecto"></display:column>
		<display:column property="numeroCorrelacion" title="Correlación"></display:column>
		<display:column property="fechaAcreditacionValor" title="Fecha Pago"></display:column>
		<display:column property="numeroValor" title="Nro Cheque"></display:column>
		<display:column property="numeroOperacion" title="Nro Ope"></display:column>
		<display:column property="importe" title="Importe"></display:column>
		<display:column property="tipoValor" title="Tipo Valor"></display:column>
		<display:footer>
			<tr>
				<td colspan="5">TOTAL</td>
				<td colspan="2">
					${ProcessForm.process.totalInvalidos}
				</td>
			</tr>
		</display:footer>
	</display:table>
	
	<div style="float: left">
		<input type="button" value="Volver" 
		onclick="javascript:window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&filter=true&process.fenv12Str=${ProcessForm.process.idFechaCobranza}'"/>
	</div>
	<div style="float: right">
		<input type="button" value="Siguiente"
			onclick="javascript:window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConciliacionRecaudaciones&process.idFechaCobranza=${ProcessForm.process.idFechaCobranza}&process.idDetalleArchivo=${ProcessForm.process.idDetalleArchivo}&process.idCaratula=${ProcessForm.process.idCaratula}'"/>
	</div>
	<div style="clear:both"/>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">

function listarRecaudaciones() {
	var accion = $('action');
	var formulario = $('oForm');
	accion.value = 'listarRecaudaciones';
	formulario.submit();
}

function Cancelar(){
	var accion = $('action');
	var formulario = $('oForm');
	accion.value= 'cancelar';
	formulario.submit();
}


</script>