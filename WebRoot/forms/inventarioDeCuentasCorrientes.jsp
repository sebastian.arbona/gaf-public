<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<logic:equal name="ProcessForm" property="process.usuario" value="ftyc">
	<div class="title" style="margin-bottom: 15px">Inventario de Proyectos</div>
</logic:equal>
<logic:notEqual name="ProcessForm" property="process.usuario" value="ftyc">
	<div class="title" style="margin-bottom: 15px">Inventario de Cuentas Corrientes</div>
</logic:notEqual>

<div style="width: 70%" align="left">
	<html:errors />
</div>

<html:form action="/actions/process.do?do=process&processName=InventarioDeCuentasCorrientes" styleId="ProcessForm">
	<html:hidden property="process.accion" styleId="accion"/>
	<html:hidden property="process.proceso" styleId="proceso"/>
	<html:hidden property="process.usuario" styleId="usuario"/>

	<table style="margin-bottom:20px">
		<tr id="filtroMoneda">
			<th >Filtrar por Moneda en:</th>
			<td colspan="1" style="width:80%">
				<html:radio property="process.filtroMoneda" value="1"/>Pesos
				<html:radio property="process.filtroMoneda" value="2"/>D�lares
			</td>
		</tr>

		<tr id="busqueda">
			<th >Buscar por:</th>
			<td colspan="1" style="width:80%">
				<html:radio property="process.fechas" value="1"/>Fecha Proceso
				<html:radio property="process.fechas" value="2"/>Fecha Movimiento
			</td>
		</tr>

		<tr>
			<th>Desembolso fecha desde:</th>
			<td>
				<asf:calendar property="ProcessForm.process.desdeFechaStr" 
				value="${process.desdeFechaStr}"/>
			</td>
		</tr>

		<tr>
			<th>Desembolso fecha hasta:</th>
			<td>
				<asf:calendar property="ProcessForm.process.hastaFechaStr" 
				value="${process.hastaFechaStr}"/>
			</td>
		</tr>

		<tr>
			<th>Informaci�n a fecha:</th>
			<td>
				<asf:calendar property="ProcessForm.process.informacionAFechaStr" 
				value="${process.informacionAFechaStr}"/>
			</td>
		</tr>

		<tr>
			<th>Procesar por:</th>
			<td>
				<html:radio onclick="limpiarSelects(1);" property="process.proceso" value="1"/>Sin filtro
				<html:radio onclick="limpiarSelects(2);" property="process.proceso" value="2"/>L�nea
				<html:radio onclick="limpiarSelects(3);" property="process.proceso" value="3"/>Estado
				<html:radio onclick="limpiarSelects(4);" property="process.proceso" value="4"/>�rea
				<html:radio onclick="limpiarSelects(5);" property="process.proceso" value="5"/>CUIT
			</td>
		</tr>

		<tr id="procesoLinea" style="display:none;">
			<th>Procesar L�nea:</th>
			<td>
				<html:select styleId="lineaCreditoId" property="process.lineasSeleccionadas" multiple="true" size="6">
					<html:optionsCollection property="process.lineas" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>

		<tr id="procesoEstado" style="display:none;">
			<th>Procesar Estado:</th>
			<td>
				<html:select styleId="estadoCreditoId" property="process.estadosSeleccionados" multiple="true" size="6">
					<html:optionsCollection property="process.estados" label="nombreEstado" value="id"/>
				</html:select>
			</td>
		</tr>

		<tr id="procesoArea" style="display:none;">
			<th>Procesar �rea:</th>
			<td>
				<html:select styleId="areaCreditoId" property="process.areasSeleccionadas" multiple="true" size="6">
					<html:optionsCollection property="process.areas" label="nombre" value="id"/>
				</html:select>
			</td>
		</tr>

		<tr id="procesoCUIT" style="display:none;">
			<th>Procesar CUIT:</th>
			<td>
				<asf:select2 name="ProcessForm" property="process.cuit" accessProperty="cuil12" entityProperty="id"
					descriptorLabels="Nombre"
					descriptorProperties="nomb12"
					entityName="Persona" 
					where2=" cuil12 IS NOT NULL AND cuil12 > 0 ORDER BY cuil12 "/>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="text-align: left">
				<html:submit onclick="return valida();">Actualizar</html:submit>
			</td>
		</tr>
	</table>

	<div class="grilla">
		<logic:equal name="ProcessForm" property="process.mostrar" value="1">
			 <display:table export="true" name="ProcessForm" property="process.inventarioDeCuentasCorrientesBean" id="reportTable" pagesize="50"
			 				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column title="N� de Solicitud" property="numeroSolicitud"/>
				<display:column title="N� de Cred Ant" property="numeroCredito"/>
				<display:column title="Expediente" property="expediente"/>
				<display:column title="Identificaci�n del Tomador" property="identificacionTomador"/>
				<display:column title="CUIT" property="cuit" />
				<display:column title="N� de Cuenta Patrimonial" property="numeroCuentaPatrimonial"/>
				<display:column title="Linea de Cr�dito" property="lineaCredito" />
				<display:column title="Resoluci�n Aprobatoria" property="resolucionAprobatoria" />
				<display:column title="Fecha Mutuo" property="fechaMutuo" decorator="com.asf.displayDecorators.DateDecorator"/>
				<display:column title="Moneda" property="moneda"/>

				<logic:equal name="ProcessForm" property="process.filtroMoneda" value="1">
					<display:column title="Monto Desembolsado" property="desembolsos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Capital en Moneda Original" property="saldoCapitalMonedaOriginal" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo de Capital Corriente" property="saldoCapitalCorriente" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Capital No Corriente" property="saldoCapitalNoCorriente" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Compensatorios" property="compensatorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Moratorios" property="moratorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Punitorios" property="punitorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="C.E.R." property="cerValor" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Otros" property="otros" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Total" property="saldoTotal" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
				</logic:equal>
				
				
				<logic:equal name="ProcessForm" property="process.filtroMoneda" value="2">
					<display:column title="Monto Desembolsado en Moneda Original" property="desembolsos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
<%--  				<display:column title="Monto Desembolsado en Pesos" property="desembolsosPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/> --%>
					<display:column title="Saldo Capital en Moneda Original" property="saldoCapitalMonedaOriginal" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Capital en Pesos" property="saldoCapitalPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo de Capital Corriente en Moneda Original" property="saldoCapitalCorriente" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Capital Corriente en Pesos" property="saldoCapitalCorrientePesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Capital No Corriente en Moneda Original" property="saldoCapitalNoCorriente" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Capital No Corriente en Pesos" property="saldoCapitalNoCorrientePesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Compensatorios Moneda Original" property="compensatorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Compensatorios en Pesos" property="compensatoriosPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Moratorios en Moneda Original" property="moratorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Moratorios en Pesos" property="moratoriosPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Punitorios Moneda Original" property="punitorios" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Intereses Punitorios en Pesos" property="punitoriosPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Otros Moneda Original" property="otros" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Otros Moneda Pesos" property="otrosPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Total Moneda Original" property="saldoTotal" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="Saldo Total en Pesos" property="saldoTotalPesos" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
				</logic:equal>

				<display:column title="Etapa del Cr�dito" property="estadoCredito"/>
				<display:column title="Comportamiento de Pago" property="comportamientoPago"/>
				<display:column title="�rea Responsable" property="areaResponsable"/>
				<display:column title="Acuerdo Judicial" property="judicial"/>
			</display:table>
		</logic:equal>
	</div>

</html:form>

<script type="text/javascript">
var accion						= $('accion');
var proceso						= $('proceso');
var usuario						= $('usuario');

var cuit						= $('process.cuit');
var cuitAccess					= $('process_cuitAccess');
var cuitDescriptor				= $('process_cuitDescriptor');

var fechaDesde          		= $("process.desdeFechaStr");
var fechaHasta         			= $("process.hastaFechaStr");
var fechaInformacionA			= $("process.informacionAFechaStr");

var processForm = document.getElementById("ProcessForm")

processForm.action += "&process.usuario=" + usuario.value;

hacerVisible(proceso.value);
negritaSubSaldo();

function cargarTabla() {
	accion.value = 'cargarTabla';
	return true;
}

function limpiarNomencladores() {
	cuit.value = "";
	cuitAccess.value = "";
	cuitDescriptor.value = "";
}

function hacerVisible(radio) {
	radio = parseInt(radio);

	switch(radio) {
		case 1:
			document.getElementById('procesoLinea').style.display = 'none';
			document.getElementById('procesoEstado').style.display = 'none';
			document.getElementById('procesoArea').style.display = 'none';
			document.getElementById('procesoCUIT').style.display = 'none';
			proceso.value = 1;
			break;
		case 2:
			document.getElementById('procesoLinea').style.display = 'block';
			document.getElementById('procesoEstado').style.display = 'none';
			document.getElementById('procesoArea').style.display = 'none';
			document.getElementById('procesoCUIT').style.display = 'none';
			document.getElementById('procesoLinea').style = 'inherit';
			proceso.value = 2;
			break;
		case 3:
			document.getElementById('procesoLinea').style.display = 'none';
			document.getElementById('procesoEstado').style.display = 'block';
			document.getElementById('procesoArea').style.display = 'none';
			document.getElementById('procesoCUIT').style.display = 'none';
			document.getElementById('procesoEstado').style = 'inherit';
			proceso.value = 3;
			break;
		case 4:
			document.getElementById('procesoLinea').style.display = 'none';
			document.getElementById('procesoEstado').style.display = 'none';
			document.getElementById('procesoArea').style.display = 'block';
			document.getElementById('procesoCUIT').style.display = 'none';
			document.getElementById('procesoArea').style = 'inherit';
			proceso.value = 4;
			break;
		case 5:
			document.getElementById('procesoLinea').style.display = 'none';
			document.getElementById('procesoEstado').style.display = 'none';
			document.getElementById('procesoArea').style.display = 'none';
			document.getElementById('procesoCUIT').style.display = 'block';
			document.getElementById('procesoCUIT').style = 'inherit';
			proceso.value = 5;
			break;
	}
}

function limpiarSelects(radio) {
	document.getElementById('lineaCreditoId').selectedIndex = -1;
	document.getElementById('estadoCreditoId').selectedIndex = -1;
	document.getElementById('areaCreditoId').selectedIndex = -1;
	limpiarNomencladores();
	hacerVisible(radio);
}

function negritaSubSaldo() {
	var collection;

	if(document.getElementById("reportTable") != null) {
		collection = (document.getElementById("reportTable")).getElementsByTagName("tr");
	
		for(var i = 0; i < collection.length; i++) {
			if (collection[i].cells[0].textContent == "Subtotal por Cuenta Patrimonial") {
				collection[i].style.fontWeight='bold';
			}
		}
	}
}

function valida() {
	retorno = true;

	var arrayFechaDesde = fechaDesde.value.split("/");
	var arrayFechaHasta = fechaHasta.value.split("/");
	var arrayFechaInformacionA = fechaInformacionA.value.split("/");

	if(fechaDesde.value.length == 0 || fechaHasta.value.length == 0 || fechaInformacionA.value.length == 0) {
		alert("Debe cargar todas las fechas para poder continuar");
		retorno=false;
	}

	if(retorno && arrayFechaDesde.length == 3 && arrayFechaDesde[2].length == 4 && arrayFechaHasta.length == 3 && arrayFechaHasta[2].length == 4) {
		if(arrayFechaDesde[2] > arrayFechaHasta[2]) {
			alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
			retorno=false;
		}
		else {
			if(arrayFechaDesde[2] == arrayFechaHasta[2] && arrayFechaDesde[1] > arrayFechaHasta[1]) {
				alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
				retorno=false;
			}
			else {
				if(arrayFechaDesde[2] == arrayFechaHasta[2] && arrayFechaDesde[1] == arrayFechaHasta[1] && arrayFechaDesde[0] > arrayFechaHasta[0]) {
					alert("La fecha 'hasta' debe ser posterior o igual a la fecha 'desde'");
					retorno=false;
				}
			}
		}
	}

	if(retorno && arrayFechaInformacionA.length == 3 && arrayFechaInformacionA[2].length == 4) {
		if(arrayFechaDesde[2] > arrayFechaInformacionA[2]) {
			alert("La fecha 'informe a' debe ser posterior o igual a la fecha 'desde'");
			retorno=false;
		}
		else {
			if(arrayFechaDesde[2] == arrayFechaInformacionA[2] && arrayFechaDesde[1] > arrayFechaInformacionA[1]) {
				alert("La fecha 'informe a' debe ser posterior o igual a la fecha 'desde'");
				retorno=false;
			}
			else {
				if(arrayFechaDesde[2] == arrayFechaInformacionA[2] && arrayFechaDesde[1] == arrayFechaInformacionA[1] && arrayFechaDesde[0] > arrayFechaInformacionA[0]) {
					alert("La fecha 'informe a' debe ser posterior o igual a la fecha 'desde'");
					retorno=false;
				}
			}
		}
	}

	if(retorno)
		cargarTabla();
}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>