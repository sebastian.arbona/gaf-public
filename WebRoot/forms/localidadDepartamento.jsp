<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Asociaci&oacute;n de Localidades</div>

<html:form action="/actions/process.do?processName=LocalidadDepartamentoProcess&do=process">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" value="guardar"/>
	
	<h4>Departamento: ${ProcessForm.process.departamento.nombre}</h4>
	
	<html:submit value="Guardar"></html:submit>
	
	<table>
		<tr>
			<th>Seleccionar</th>
			<th>Localidad</th>
			<th>CP</th>
			<th>Abreviatura</th>
		</tr>
		<logic:iterate id="localidad" property="process.localidades" name="ProcessForm">
		<tr>
			<td>
				<logic:match value="-${localidad.id}-" property="process.localidadesAsociadas" name="ProcessForm">
				<input type="checkbox" name="seleccionLocalidades" checked="checked" value="${localidad.id}"/> 
				</logic:match>
				<logic:notMatch value="-${localidad.id}-" property="process.localidadesAsociadas" name="ProcessForm">
				<input type="checkbox" name="seleccionLocalidades" value="${localidad.id}"/>
				</logic:notMatch>
			</td>
			<td>
				${localidad.nombre}
			</td>
			<td>${localidad.cp}</td>
			<td>${localidad.abrev}</td>
		</tr>
		</logic:iterate>
	</table>
	
	<html:submit value="Guardar"></html:submit>
</html:form>