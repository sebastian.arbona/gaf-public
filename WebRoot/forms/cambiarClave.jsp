<%@ page language="java"%>
<%@ page import="com.asf.security.SessionHandler" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<div class="title">Cambiar Contraseņa</div>
<br><br>
<html:form action="/actions/cambiarClave.do">
<div style="text-align: left;width: 70%;">
	<html:errors/>
</div>
<table border="0">
<tr><th>Usuario:</th><td><input type="text" value="<%= SessionHandler.getSessionHandler().getCurrentUser()%>" disabled></td></tr>
<tr><th>Clave Anterior:</th><td><html:password property="old" maxlength="15"/></td></tr>
<tr><th>Nueva Clave:</th><td><html:password property="new1"  maxlength="15"/></td></tr>
<tr><th>Confirmar Nueva Clave:</th><td><html:password property="new2"  maxlength="15"/></td></tr>
</table>

<asf:security action="/actions/cambiarClave.do">
<html:submit property="cambiar"><bean:message key="abm.button.save"/></html:submit>
</asf:security>
<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>

