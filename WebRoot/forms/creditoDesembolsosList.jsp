<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@page import="com.asf.gaf.hibernate.Ejercicio"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>
<%
	request.setAttribute("ejercicio", Ejercicio.getEjercicioActual());
%>
<logic:notEmpty name="redirectDesembolso">
	<script type="text/javascript">
		var url = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoDesembolsos&process.idObjetoi=${idObjetoi}";
		window.location = url;
	</script>
</logic:notEmpty>
<div class="title">
	<logic:equal value="false" name="ProcessForm"
		property="process.lineaAcuerdo">
		Desembolsos de Solicitud de Financiamiento
	</logic:equal>
	<logic:equal value="true" name="ProcessForm"
		property="process.lineaAcuerdo">
		Deuda Consolidada del Cr&eacute;dito Original
	</logic:equal>
</div>
<html:form
	action="/actions/process.do?do=process&processName=CreditoDesembolsos"
	styleId="oForm">
	<html:hidden property="process.objetoi.id"
		value="${ProcessForm != null && ProcessForm.process != null ? ProcessForm.process.objetoi.id : idObjetoi}" />
	<html:hidden property="process.idObjetoi"
		value="${ProcessForm != null && ProcessForm.process != null ? ProcessForm.process.idObjetoi : idObjetoi}" />
	<html:hidden property="process.action" styleId="action" value="save" />
	<html:hidden property="process.desembolso.id" styleId="desembolsoId" />
	<!--	agregar el value con el parametro-->
	<h4>Autorizados a retirar valores:</h4>
	<table border="0">
		<tr>
			<th>Nombre Autorizado</th>
			<td><html:text styleId="nombre1"
					property="process.objetoi.nombreAuthValores1" size="30"
					readonly="${ProcessForm.process.acuerdoPago}" /></td>
			<th>C.U.I.T.</th>
			<td><html:text styleId="cuit1"
					property="process.objetoi.cuitAuthValores1" size="30"
					readonly="${ProcessForm.process.acuerdoPago}" /></td>
		</tr>
		<tr>
			<th>Nombre Autorizado</th>
			<td><html:text styleId="nombre2"
					property="process.objetoi.nombreAuthValores2" size="30"
					readonly="${ProcessForm.process.acuerdoPago}" /></td>
			<th>C.U.I.T.</th>
			<td><html:text styleId="cuit2"
					property="process.objetoi.cuitAuthValores2" size="30"
					readonly="${ProcessForm.process.acuerdoPago}" /></td>
		</tr>
	</table>
	<logic:equal value="true" name="ProcessForm"
		property="process.acuerdoPago">
		<html:submit disabled="${ProcessForm.process.acuerdoPago}">Guardar</html:submit>
	</logic:equal>
	<logic:equal value="false" name="ProcessForm"
		property="process.acuerdoPago">
		<html:submit>Guardar</html:submit>
	</logic:equal>
	<html:reset onblur="habilitarAut2();">Cancelar</html:reset>
</html:form>
<br />
<%
	request.setAttribute("URL_GAF", DirectorHelper.getString("URL.GAF"));
	request.setAttribute("URL_ADE", DirectorHelper.getString("URL.ADE"));
%>

Expediente de Pago:
<logic:notEmpty name="URL_ADE">
	<a
		href="${URL_ADE}/actions/process.do?processName=DocumentoHelper&do=process&process.action=identificador&process.id=${ProcessForm.process.objetoi.expedientePago }"
		target="_blank">${ProcessForm.process.objetoi.expedientePago}</a>
</logic:notEmpty>

<logic:empty name="URL_ADE">
	${ProcessForm.process.objetoi.expedientePago}
</logic:empty>


<logic:notEmpty name="URL_GAF">
	<a
		href="${URL_GAF}/actions/jreport.do?do=exec&export=PDF&reportName=volanteImputacion.jasper&params=WHERE%3D++WHERE+e.expediente=%27${ProcessForm.process.objetoi.expediente }%27"
		target="_blank">Imputaciones</a>
</logic:notEmpty>

<br />
<br />

<html:form action="/actions/desembolsoAction.do?entityName=Desembolso">
	<html:hidden property="paramValue[0]"
		value="${ProcessForm.process.idObjetoi}" />
	<html:hidden property="paramName[0]" value="credito.id" />
	<html:hidden property="paramComp[0]" value="=" />
	<html:hidden property="do" styleId="accion" value="newEntity" />
	<asf:security action="actions/desembolsoAction.do"
		access="do=newEntity&entityName=Desembolso">
		<logic:equal value="true" name="ProcessForm"
			property="process.acuerdoPago">
			<button type="submit" disabled="disabled">
				<bean:message key="abm.button.new" />
			</button>
		</logic:equal>
		<logic:equal value="false" name="ProcessForm"
			property="process.acuerdoPago">
			<logic:equal value="true" name="ProcessForm"
				property="process.lineaAcuerdo">
				<button type="submit" disabled="disabled">
					<bean:message key="abm.button.new" />
				</button>
			</logic:equal>
			<logic:equal value="false" name="ProcessForm"
				property="process.lineaAcuerdo">
				<button type="submit">
					<bean:message key="abm.button.new" />
				</button>
			</logic:equal>
		</logic:equal>
	</asf:security>
	<br />
	<div style="font-size: 14px">
		<html:errors />
	</div>
	<br>
	<br>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.desembolsos"
			id="reportTable">
			<display:setProperty name="report.title"
				value="Desembolsos de Cr�dito"></display:setProperty>
			<display:column media="html" property="numero" title="Orden"
				href="${pageContext.request.contextPath}/actions/desembolsoAction.do?entityName=Desembolso&do=load&paramName[0]=credito.id&paramComp[0]=%3D&paramValue[0]=${ProcessForm.process.idObjetoi}&filter=true&$entity.credito.id=${ProcessForm.process.idObjetoi}"
				paramId="entity.id" paramProperty="id" />
			<display:column media="html" property="importe"
				title="Imp. Planificado"
				decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column media="html" property="fechaStr"
				title="Fecha Planificada" />
			<display:column media="html" property="importeReal" title="Imp. Real"
				decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
			<display:column media="html" property="fechaRealStr"
				title="Fecha Real" />
			<display:column media="html" property="fechaSolicitudStr"
				title="Fecha Solicitado" />
			<display:column media="html" property="estadoStr" title="Situaci�n" />
			<display:column media="html" title="Orden de Pago">
				<a
					href="${pageContext.request.contextPath}/actions/process.do?processName=CreditoDesembolsos&do=process&process.action=reporteOrden&process.desembolso.id=${reportTable.id}">${reportTable.nroOrden}</a>
			</display:column>
			<logic:equal value="ANALISIS" name="ProcessForm"
				property="process.objetoi.estadoActual.estado.nombreEstado">
				<display:column media="html" title="Eliminar">
					<a onclick="return confirmDelete();"
						href="${pageContext.request.contextPath}/actions/desembolsoAction.do?entityName=Desembolso&do=delete&filter=true&entity.id=${reportTable.id}">Eliminar</a>
				</display:column>
			</logic:equal>
			<display:column media="html" title="Requisitos"
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=list&paramName[0]=desembolsoId&paramComp[0]=%3d&entityName=RequisitoDesembolso&filter=true&idObjetoi=${ProcessForm.process.idObjetoi}"
				paramId="entity.credito.id" paramProperty="id">Requisitos
    		</display:column>
			<display:column media="html" title="Generar Solicitud"
				href="${pageContext.request.contextPath}/actions/process.do?do=process&process.idObjetoi=${ProcessForm.process.idObjetoi}&processName=CreditoDesembolsos&process.action=loadEjecucion"
				paramId="process.idDesembolso" paramProperty="id">
				<logic:empty name="reportTable" property="estadoStr">Generar Solicitud</logic:empty>
				<logic:notEmpty name="reportTable" property="estadoStr">
					<logic:equal value="Nuevo" name="reportTable" property="estadoStr">Generar Solicitud</logic:equal>
					<logic:notEqual value="Nuevo" name="reportTable"
						property="estadoStr">Detalle</logic:notEqual>
				</logic:notEmpty>
			</display:column>
			<asf:security action="/actions/desistirDesembolso.do" access="">
				<display:column title="Desistir" media="html">
					<logic:equal name="reportTable" value="4" property="estado">
						<a href="javascript:void(0);"
							onclick=" cargarPopUp(${reportTable.id});"> DESISTIR </a>
					</logic:equal>
				</display:column>
			</asf:security>
			<display:column media="html" title="Imprimir Autorizaci�n"
				href="${pageContext.request.contextPath}/actions/process.do?do=process&process.idObjetoi=${ProcessForm.process.idObjetoi}
				&processName=CreditoDesembolsos&process.action=imprimirAutorizacion"
				paramId="process.idDesembolso" paramProperty="id">
				<logic:equal name="reportTable" value="4" property="estado">Imprimir</logic:equal>
				<logic:equal name="reportTable" value="3" property="estado">Imprimir</logic:equal>
			</display:column>
		</display:table>
	</div>
</html:form>
<script type="text/javascript">
$j = jQuery.noConflict();

$j(document).ready(function() {
	$j("#nombre1").keyup(function() {
		habilitarAut2();
	});
	$j("#cuit1").keyup(function() {
		habilitarAut2();
	});

	habilitarAut2();
});

function habilitarAut2() {
	var nombre1 = $j("#nombre1").val();
	var cuit1 = $j("#cuit1").val();
	var habilitar = ((nombre1 != null && nombre1 != '') || (cuit1 != null && cuit1 != ''));
	$j("#nombre2").attr("disabled", !habilitar);
	$j("#cuit2").attr("disabled", !habilitar);
	if (!habilitar) {
		$j("#nombre2").val(null);
		$j("#cuit2").val(null);
	}
	return true;
}
function desistir(id){
	if(confirm('Est� seguro en desistir el desembolso seleccionado?')){
		$('action').value = 'desistir';
		$('desembolsoId').value = id;
		$('oForm').submit();
	}
}
function imprimirAutorizacion(){
		$('action').value = 'imprimirAutorizacion';
		$('oForm').submit();
}

function cargarPopUp(idDesembolso){
	var url = "";
	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=CreditoDesembolsos";
	url += "&process.action=cargarDesistir"
	url += "&process.desembolso.id="+idDesembolso;
	url += "&idObjetoi="+${ProcessForm.process.idObjetoi};
	popUp(url);
}
</script>