<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@page import="com.nirven.creditos.hibernate.Linea"%>
<%@page import="com.asf.hibernate.mapping.Director"%>
<%@page import="com.asf.security.BusinessPersistance"%>
<%@page import="com.asf.security.SessionHandler"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<%
	/* Obtenemos, de la tabla Director, si se necesita mostrar la Prorroga . */
	try {
		Director director = null;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		director = (Director) bp.createQuery("FROM Director WHERE codigo = 'PRORROGA'").uniqueResult();

		if (director != null && director.getValor().equals("SI")) {
			pageContext.setAttribute("mostrarProrroga", "SI");
		} else {
			pageContext.setAttribute("mostrarProrroga", "NO");
		}

	} catch (Exception e) {
		pageContext.setAttribute("mostrarProrroga", "NO");
		e.printStackTrace();
	}
%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.accion"
		styleId="accion" />
	<html:hidden property="process.persona.id" styleId="idPersonaTitular"
		value="${ProcessForm.process.persona.id}" />
	<br />
	<table>
		<logic:notEmpty name="ProcessForm" property="process.mensajes">
			<logic:iterate id="mensajesId" name="ProcessForm"
				property="process.mensajes">
				<tr>
					<td><bean:write name="mensajesId" property="value" /></td>
				</tr>
			</logic:iterate>
		</logic:notEmpty>
	</table>
	<br />
	<logic:empty name="ProcessForm" property="process.creditos">
    La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no tiene Datos Crediticios.
    </logic:empty>
	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<display:table name="ProcessForm" property="process.creditos"
			id="credito">
			<display:caption>Solicitudes de financiamiento de:  ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12}</display:caption>
			<display:column title="N�mero" media="html" sortable="false">
				<a
					href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${credito.idObjetoi}&process.action=buscar&process.personaId=${ProcessForm.process.persona.idpersona}"
					target="_parent">${credito.numeroAtencion}</a>
			</display:column>
			<logic:equal value="true" property="process.hayNroCredito"
				name="ProcessForm">
				<display:column title="Nro de Cr�dito" property="numeroCredito"
					sortable="false" />
			</logic:equal>
			<display:column title="L�nea" property="linea" sortable="false" />
			<display:column title="Expediente N�" property="expediente"
				sortable="true" />
			<display:column title="Etapa" property="estado" sortable="flase" />
			<display:column title="Comportamiento Pago" property="comportamiento"
				sortable="false" />
			<display:column title="Deuda" property="totalAdeudadoStr"
				sortable="false" />
			<display:column title="Informe" media="html" sortable="false">
				<a href="javascript:void(0);"
					onclick="imprimir(${credito.idObjetoi});"> Informe </a>
			</display:column>
			<display:column title="Aplica fondos" property="auditoriaFinalPosee"
				sortable="false" />
			<logic:equal name="mostrarProrroga" value="SI">
				<display:column title="Prorroga" media="html" sortable="false">
					<c:if
						test="<%=Linea.esLineaCosecha(((ObjetoiDTO) credito).getIdLinea())
											|| Linea.esLineaCosechaPreaprobada(((ObjetoiDTO) credito).getIdLinea())%>">
						<a href="javascript:void(0);"
							onclick="imprimirProrroga(${credito.idObjetoi});"> Prorroga </a>
					</c:if>
				</display:column>
			</logic:equal>
		</display:table>
	</logic:notEmpty>

	<!-- 
	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<br>
		<br>
		<h2>
			<a href="javascript:void(0);" onclick="verificarLibreDeuda();">
				Obtener Libre Deuda </a>
		</h2>
	</logic:notEmpty>
	-->
	<br>
	<br>
	<h2>
		<a href="javascript:void(0);" onclick="nuevoCredito();"> Nuevo
			Cr�dito </a>
	</h2>
</html:form>


<script language="javascript" type="text/javascript">
    function imprimir(id) 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=imprimirInforme' +
        '&process.idObjetoi=' + id + 
        '&process.idPersona=${ProcessForm.process.persona.idpersona}' +
        '&process.persona.id=${ProcessForm.process.persona.idpersona}';       
    }
    function imprimirProrroga(id) 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=imprimirProrroga' +
        '&process.idObjetoi=' + id + 
        '&process.idPersona=${ProcessForm.process.persona.idpersona}' +
        '&process.persona.id=${ProcessForm.process.persona.idpersona}';       
    }
    function verificarLibreDeuda() 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=verificarLibreDeuda' +
        '&process.idPersona=${ProcessForm.process.persona.idpersona}' +
        '&process.persona.id=${ProcessForm.process.persona.idpersona}';       
    }
    function nuevoCredito() 
    {    	
        parent.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=CreditoManual' +
        '&process.accion=nuevo' +
        '&process.idPersona=${ProcessForm.process.persona.idpersona}' +       
        '&process.persona.id=${ProcessForm.process.persona.idpersona}';       
    }
</script>
