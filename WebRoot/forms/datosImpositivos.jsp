<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.persona_id" styleId="idPersonaTitular"
		value="${ProcessForm.process.persona.id}" />
	<html:hidden property="process.accion" styleId="accion" value="guardar" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br />
	<div class="grilla">
		<logic:notEmpty name="ProcessForm" property="process.impuestos">
			<display:table name="ProcessForm" property="process.impuestos"
				export="true" id="reportTable" width="100%"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column property="actividad" title="Actividad" />
				<display:column property="impuesto" title="Impuesto" />
				<display:column property="fechaAlta" title="Fecha Alta" />
				<display:column property="fechaBaja" title="Fecha Baja" />
			</display:table>
		</logic:notEmpty>
		<logic:empty name="ProcessForm" property="process.impuestos">
	    		La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no est� Inscripta a Impuestos.
			</logic:empty>
	</div>
</html:form>
<script>
	function guardar() {
		var form = $('ProcessForm');
		form.submit();
	}
</script>