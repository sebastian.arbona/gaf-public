<%@ page language="java"%>
<%@page import="com.asf.util.DateHelper"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
<div class="title">
	Tipo Resolución
</div>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<html:hidden property="entity.id" />
	<logic:equal parameter="do" value="load">
		<html:hidden property="entity.nuevo" value="false" />
	</logic:equal>
	<table>
	
		<tr>
			<th>Nombre del Tipo de Resolución:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.nombre"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="0">
				<td>${AbmForm.entity.nombre}</td>
			</logic:notEqual>
		</tr>
		<tr>
			<th>Descripción del tipo:</th>
			<logic:equal name="AbmForm" property="entity.id" value="">
				<td><html:text property="entity.descripcion"/></td>
			</logic:equal>
			<logic:notEqual name="AbmForm" property="entity.id" value="0">
				<td>${AbmForm.entity.descripcion}</td>
			</logic:notEqual>
		</tr>
	</table>
	<br/>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
></iframe>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.selectboxes.min.js"></script>
<script language="javascript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.colorPicker.js"/></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/colorPicker.css" type="text/css" />
