<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Reimputaci&oacute;n de Pagos y Cr&eacute;ditos</div>
<html:form
	action="/actions/process.do?do=process&processName=Reimputacion"
	styleId="ProcessForm">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" value="buscarRecaudaciones"
		styleId="action" />
	<html:hidden property="process.idRecaudacion2" styleId="idRecaudacion2" />
	<html:errors />
	<table>
		<tr>
			<th>Banco</th>
			<td colspan="3"><asf:select name="process.idEnteRecaudador"
					entityName="com.asf.gaf.hibernate.Bancos" listCaption="getDetaBa"
					listValues="getCodiBa" value="ProcessForm.process.idEnteRecaudador"
					filter="recaudador = 1  order by codiBa" /></td>
		</tr>
		<tr>
			<th>Fecha desde</th>
			<td><asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
			</td>
			<th>Fecha hasta</th>
			<td><asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
			</td>
		</tr>
		<tr>
			<th>Importe desde</th>
			<td><asf:text maxlength="15" type="decimal"
					property="process.importeDesde" name="ProcessForm"
					value="${ProcessForm.process.importeDesde}" /></td>
			<th>Importe hasta</th>
			<td><asf:text maxlength="15" type="decimal"
					property="process.importeHasta" name="ProcessForm"
					value="${ProcessForm.process.importeHasta}" /></td>
		</tr>
		<tr>
			<th colspan="4"><html:submit value="Buscar Recaudaciones"></html:submit>
			</th>
		</tr>
		<logic:notEmpty name="ProcessForm" property="process.recaudaciones">
			<tr>
				<th>Recaudaci&oacute;n Seleccionada</th>
				<td colspan="3">
					<div id="detallerec"></div>
				</td>
			</tr>
			<tr>
				<th>Importe del Pago</th>
				<td colspan="3"><asf:text maxlength="15" type="decimal"
						property="process.importe" name="ProcessForm" /></td>
			</tr>
			<tr>
				<th colspan="4"><html:submit onclick="seleccionar();">Agregar Recaudaci&oacute;n Seleccionada</html:submit>
				</th>
			</tr>
		</logic:notEmpty>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.seleccionadas">
		<h3>Recaudaciones Seleccionadas</h3>
		<table>
			<tr>
				<th>Recaudacion</th>
				<th>Importe</th>
				<th>Quitar</th>
			</tr>
			<logic:iterate id="rs" name="ProcessForm"
				property="process.seleccionadas">
				<tr>
					<td>${rs.recaudacion}</td>
					<td>${rs.importeStr}</td>
					<td><html:submit onclick="quitar(${rs.recaudacion.id });">Quitar</html:submit></td>
				</tr>
			</logic:iterate>
			<tr>
				<th><html:submit onclick="continuar();">Continuar</html:submit></th>
			</tr>
		</table>
	</logic:notEmpty>
	<logic:notEmpty name="ProcessForm" property="process.recaudaciones">
		<div class="grilla">
			<display:table name="ProcessForm" property="process.recaudaciones"
				export="true" id="reportTable" width="100%"
				requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process">
				<display:column title="Selec." media="html">
					<logic:greaterThan value="0" name="reportTable" property="saldo">
						<html:radio property="process.idRecaudacion"
							value="${reportTable.id}"
							onchange="mostrarRecaudacion(this, 
						{
							fecha: '${reportTable.fechamovimientoStr}',
							operacion: ${reportTable.nroOperacion},
							saldo: '${reportTable.saldoStr}'
						});" />
					</logic:greaterThan>
				</display:column>
				<display:column title="Número Comprobante" property="nromovimiento"
					sortable="true">
				${reportTable.nromovimiento}
			</display:column>
				<display:column title="Número Comprobante" property="nromovimiento"
					media="pdf excel xml" />
				<display:column title="Fecha Movimiento"
					property="fechamovimientoStr" sortable="true" />
				<display:column title="Fecha de Ingreso"
					property="fechavencimientoStr" sortable="true" />
				<display:column title="Nº Expediente" property="nroExpediente"
					sortable="true" />
				<display:column title="Nº Operación" property="nroOperacion"
					sortable="true" />
				<display:column title="Cod. Operación" property="codigoOperacion"
					sortable="true" />
				<display:column title="Ente Recaudador"
					property="bancos.proveedor.fantasia" sortable="true" />
				<display:column property="tipoComprobanteStr"
					title="Tipo de Comprobante" sortable="true" align="right" />
				<display:column property="estadoComprobanteStr"
					title="Estado de Comprobante" sortable="true" align="right" />
				<display:column title="Moneda" sortable="false" align="center">
					<logic:notEqual value="1" name="reportTable" property="idMoneda">
					${reportTable.valorOriginalStr } a ${reportTable.cotizacionStr }
				</logic:notEqual>
					<logic:equal value="1" name="reportTable" property="idMoneda">
					${reportTable.moneda.simbolo }
				</logic:equal>
				</display:column>
				<display:column title="Saldo" sortProperty="saldo" sortable="true"
					align="right" style="text-align:right;">
					${reportTable.saldoStr}
				</display:column>
				<display:column title="Saldo Partidas Pendientes" sortable="true"
					sortProperty="saldoPP" align="right" style="text-align:right;">
					${reportTable.saldoPPStr}
				</display:column>
				<display:column title="Saldo Moneda Original" property="saldoMo"
					sortable="true" align="right" style="text-align:right;" />
				<display:column title="Importe" property="importe" sortable="true"
					align="right" style="text-align:right;" />
				<display:column title="Comisión" property="comision" sortable="true"
					style="text-align:right;" align="right" />
				<display:column title="I.V.A." property="iva" sortable="true"
					style="text-align:right;" align="right" />
				<display:column title="Neto" property="neto" sortable="true"
					style="text-align:right;" align="right" />
				<display:column property="fechaPaseContabilidadStr"
					title="Fec. Cont." sortable="true" align="right" />
				<display:column property="fechaAnuladoStr" title="Anulada"
					sortable="true" align="right" />
				<display:column property="observacion" title="Observación"
					sortable="true" align="center" />
			</display:table>
		</div>
	</logic:notEmpty>
</html:form>

<script type="text/javascript">
	var action = $('action');
	var detallerec = $('detallerec');
	var idRecaudacion2 = $('idRecaudacion2');
	function seleccionar() {
		action.value = 'seleccionarRecaudacion';
	}

	function continuar() {
		action.value = 'continuar';
	}

	function quitar(id) {
		action.value = 'quitar';
		idRecaudacion2.value=id;
	}

	function mostrarRecaudacion(button, recaudacion) {
		if (button.checked) {
			detallerec.innerHTML = recaudacion.operacion + ' - ' + recaudacion.fecha + ' - Saldo: ' + recaudacion.saldo; 
		}
	}

	function evaluarRadios() {
		var radios = $$('input[name="process.idRecaudacion"]');
		for (var i = 0; i < radios.length; i++) {
			if (radios[i].checked) {
				radios[i].onchange();
			}
		}
	}

	evaluarRadios();
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>