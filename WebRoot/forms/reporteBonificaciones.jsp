<%@ page language="java"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.nirven.creditos.hibernate.BoletoTipoEnum"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>


<div class="title">Reporte de Bonificaciones</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />
	<table>
		<tr>
			<th>Convenio</th>
			<td><asf:select
					entityName="com.nirven.creditos.hibernate.ConvenioBonificacion"
					listCaption="nombre" listValues="id" name="process.convenio.id"
					value="${ProcessForm.process.convenio.id}" nullValue="true"
					nullText="&lt;&lt;Seleccione un Convenio Bonificacion&gt;&gt;"
					attribs="class='id'" /></td>
		</tr>
		<tr>
			<th>Fecha Vencimiento Desde:</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaVencimientoDesdeStr"></asf:calendar></td>
		</tr>
		<tr>
			<th>Fecha Vencimiento Hasta:</th>
			<td><asf:calendar
					property="ProcessForm.process.fechaVencimientoHastaStr"></asf:calendar></td>
		</tr>
	</table>
	<br />

	<input type="button" value="Listar" id="listarId" onclick="listar();" />
	<br />
	<br />

	<logic:notEmpty name="ProcessForm" property="process.bonificaciones">
		<div class="grilla">
			<display:table name="ProcessForm" property="process.bonificaciones"
				id="credito" class="com.asf.cred.business.BonificacionDTO"
				export="true"
				pagesize="100"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">

				<display:column title="Linea" property="linea" sortable="false" />
				<display:column title="Ente Bonificador" property="ente"
					sortable="false" />
				<display:column title="Convenio" property="convenio"
					sortable="false" />
				<display:column title="Proyecto" property="proyecto"
					sortable="false" />
				<display:column title="Titular" property="titular" sortable="false" />
				<display:column title="CUIL" property="cuil" sortable="false" />
				<display:column title="Nro. Resolucion" property="resolucionNumero"
					sortable="false" />
				<display:column title="Fecha Resolucion" property="resolucionFecha"
					decorator="com.asf.displayDecorators.DateDecorator"
					sortable="false" />
				<display:column title="Monto Credito" property="montoCredito"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Tasa Bonificada" property="tasaBonificada"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Nro. Cuota" property="cuotaNumero"
					sortable="false" />
				<display:column title="Venc. Cuota" property="cuotaVencimiento"
					decorator="com.asf.displayDecorators.DateDecorator"
					sortable="false" />
				<display:column title="Monto Bonificado" property="montoBonificado"
					sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Cotomadores" property="cotomadores"
					sortable="false" />
			</display:table>
		</div>
	</logic:notEmpty>
	<logic:empty name="ProcessForm" property="process.bonificaciones">
    	No hay Bonificaciones
    </logic:empty>

</html:form>

<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');
	function listar() {
		accion.value = 'listar';
		formulario.submit();
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
