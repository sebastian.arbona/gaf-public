<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>



<div class="title">Boleto de Ingresos Varios</div>
<br>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />
	<table>
		<tr>
			<th>Persona:</th>
			<td colspan="3"><asf:selectpop
					name="process.deudoresVarios.persona.id" title="Persona"
					columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
					captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
					entityName="com.civitas.hibernate.persona.Persona" /> <a
				href="javascript:void(0);" onclick="registrarPersona();"> <b>Registrar
						Persona</b>
			</a></td>
		</tr>
		<tr>
			<th>Proyecto</th>
			<td colspan="3"><asf:selectpop
					name="process.deudoresVarios.numeroAtencion"
					value="${ProcessForm.process.deudoresVarios.numeroAtencion}"
					entityName="Objetoi" title="Credito" caseSensitive="true"
					values="numeroAtencion" columns="Proyecto,Tomador,Expediente"
					captions="numeroAtencion,persona.nomb12,expediente"
					cantidadDescriptors="1" /></td>
		</tr>
		<tr>
			<th>Expediente</th>
			<td colspan="3"><asf:selectpop
					name="process.deudoresVarios.expediente" entityName="DocumentoADE"
					title="Expediente"
					value="${empty ProcessForm.process.deudoresVarios.expediente ? '' : ProcessForm.process.deudoresVarios.expediente}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI"
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					cantidadDescriptors="2" />
		</tr>
		<tr>
			<th>Concepto:</th>
			<td><select id="process.deudoresVarios.concepto"
				name="process.deudoresVarios.concepto" onchange="cargarTipos();"></select>
			</td>
			<th>Tipo:</th>
			<td><select id="process.deudoresVarios.tipoConcepto"
				name="process.deudoresVarios.tipoConcepto"
				onchange="cargarConfig();"></select></td>
		</tr>
		<tr>
			<th>Fecha de Emisi�n:</th>
			<td><html:text property="process.deudoresVarios.fechaEmisionStr"
					readonly="true"></html:text></td>
			<th>Fecha de Vencimiento:</th>
			<td><html:text styleId="process.deudoresVarios.fechaVtoStr"
					property="process.deudoresVarios.fechaVtoStr" readonly="true" /></td>
		</tr>
		<tr>
			<th>Moneda:</th>
			<td colspan="3"><asf:select
					entityName="com.asf.gaf.hibernate.Moneda"
					listCaption="idMoneda,denominacion" listValues="idMoneda"
					orderBy="idMoneda" name="process.deudoresVarios.moneda.idMoneda"
					value="${ProcessForm.process.deudoresVarios.moneda.idMoneda}"
					nullValue="false" /></td>
		</tr>

		<tr>
			<th>Importe:</th>
			<td colspan="3"><html:text
					styleId="process.deudoresVarios.importe" maxlength="10"
					property="process.deudoresVarios.importe" /></td>
		</tr>
		<tr>
			<th>Leyenda:</th>
			<td colspan="3"><asf:select
					entityName="com.nirven.creditos.hibernate.Leyenda"
					listCaption="nombre" listValues="texto" orderBy="nombre"
					name="process.leyenda.texto" nullValue="true"
					nullText="&lt;&lt;Seleccione Leyenda&gt;&gt;" /></td>
		</tr>
	</table>
	<br />
	<input type="button" value="Imprimir" onclick="imprimir();" />
	<br />
	<br />

</html:form>

<script type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');

	function imprimir() {
		accion.value = 'guardar';
		formulario.submit();
	}

	function registrarPersona() {
		popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaAction.do?do=newEntity'
				+ '&entityName=Persona' + '&forward=PersonaAltaRapida');
	}

	function cargarConceptos() {
		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarConceptosIngresosVarios";
		retrieveURL(url, cargarConceptosResponse);
	}

	function cargarConceptosResponse(responseText) {
		var conceptoSelect = $('process.deudoresVarios.concepto');
		var optNull = document.createElement("option");
		optNull.text = "Seleccione";
		optNull.value = "";
		conceptoSelect.options.add(optNull);
		var objects = parseXml(responseText);
		if (objects.conceptos && objects.conceptos.concepto) {
			var concepto = objects.conceptos.concepto;
			if (concepto.constructor === Array) {
				for (var i = 0; i < concepto.length; i++) {
					var c = concepto[i];
					var opt = document.createElement("option");
					opt.text = c.detalle;
					opt.value = c.id;
					conceptoSelect.options.add(opt);
				}
			} else {
				var c = concepto;
				var opt = document.createElement("option");
				opt.text = c.detalle;
				opt.value = c.id;
				conceptoSelect.options.add(opt);
			}
		}
	}

	function cargarTipos() {
		var concepto = $('process.deudoresVarios.concepto');
		if (concepto.value == null || concepto.value == '') {
			return;
		}

		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarTiposConceptosIngresosVarios&concepto="
				+ concepto.value;
		retrieveURL(url, cargarTiposResponse);
	}

	function cargarTiposResponse(data) {
		var objects = parseXml(data);
		var tipoConceptoSelect = $('process.deudoresVarios.tipoConcepto');
		while (tipoConceptoSelect.options.length > 0) {
			tipoConceptoSelect.options[0].remove();
		}

		var optNull = document.createElement("option");
		optNull.text = "Seleccione";
		optNull.value = "";
		tipoConceptoSelect.options.add(optNull);

		if (objects.tipos && objects.tipos.tipo) {
			var tipo = objects.tipos.tipo;
			if (tipo.constructor === Array) {
				for (var i = 0; i < tipo.length; i++) {
					var t = tipo[i];
					var opt = document.createElement("option");
					opt.text = t.text;
					opt.value = t.value;
					tipoConceptoSelect.options.add(opt);
				}
			} else {
				var t = tipo;
				var opt = document.createElement("option");
				opt.text = t.text;
				opt.value = t.value;
				tipoConceptoSelect.options.add(opt);
			}
		}
	}

	function cargarConfig() {
		var concepto = $('process.deudoresVarios.concepto');
		var tipoConcepto = $('process.deudoresVarios.tipoConcepto');
		var proyecto = $('process.deudoresVarios.numeroAtencion');
		if (concepto.value == null || concepto.value == ''
				|| tipoConcepto.value == null || tipoConcepto.value == '') {
			return;
		}

		var url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarConfigIngresosVarios&concepto="
				+ concepto.value;
		url += "&tipoConcepto=" + tipoConcepto.value;
		url += "&numeroAtencion=" + proyecto.value;
		retrieveURL(url, cargarConfigResponse);
	}

	function cargarConfigResponse(responseText) {
		if (responseText == null || responseText == '') {
			return;
		}
		var objects = parseXml(responseText);
		$("process.deudoresVarios.fechaVtoStr").value = objects.config.vencimiento;
		$("process.deudoresVarios.importe").value = objects.config.importe;
		$("process.deudoresVarios.importe").readOnly = (objects.config.manual == 'false');

	}

	function mostrarExpediente() {
		var proyecto = $('process.deudoresVarios.numeroAtencion');
		if (proyecto.value == null || proyecto.value == '') {
			return;
		}

		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarExpedientePorProyecto&numeroAtencion="
				+ proyecto.value;
		retrieveURL(url, mostrarExpedienteResponse);
	}

	function mostrarExpedienteResponse(responseText) {
		$('process.deudoresVarios.expediente').value = responseText;
		$('process.deudoresVarios.expediente').onchange();
		mostrarPersona();
	}

	function mostrarPersona() {
		var proyecto = $('process.numeroAtencion');
		if (proyecto.value == null || proyecto.value == '') {
			return;
		}

		url = "${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarIdPersonaPorProyecto&numeroAtencion="
				+ proyecto.value;
		retrieveURL(url, mostrarPersonaResponse);
	}

	function mostrarPersonaResponse(responseText) {
		$('process.deudoresVarios.persona.id').value = responseText;
		$('process.deudoresVarios.persona.id').onchange();
	}

	cargarConceptos();
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>