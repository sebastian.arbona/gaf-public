<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Pr�rroga de vencimientos masiva</div>
<br>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.action"
		styleId="action" />
	<html:hidden name="ProcessForm" property="process.tipoCorrimiento"
		styleId="tipoCorrimiento" />
	<logic:equal name="ProcessForm" value="true"
		property="process.finalizo">
		<input type="button" value="Volver"
			onclick="window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=ProrrogaMasivaProcess&process.action=volver'">
	</logic:equal>
	<div>
		<html:errors />
	</div>
	<logic:equal name="ProcessForm" value="false"
		property="process.finalizo">
		<table border="0">
			<tr>
				<th>L&iacute;nea de cr&eacute;dito:</th>
				<td><asf:select
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="id,nombre" listValues="getId" name="process.linea_id"
						value="${ProcessForm.process.linea_id}" orderBy="id"
						nullText="Todas" nullValue="true" /></td>
			</tr>
			<tr>
				<th>Cr&eacute;dito:</th>
				<td><asf:selectpop name="process.nroAtencion"
						columns="Nro.Proyecto,Titular"
						captions="numeroAtencion,persona.nomb12" values="numeroAtencion"
						entityName="com.nirven.creditos.hibernate.Objetoi"
						value="${ProcessForm.process.nroAtencion}"
						title="Consulta de Cr�ditos" caseSensitive="true" /></td>
			</tr>
			<tr>
				<th>Desde:</th>
				<td><asf:calendar property="ProcessForm.process.fechaDesdeStr"
						readOnly="false" /></td>
			</tr>
			<tr>
				<th>Hasta:</th>
				<td><asf:calendar property="ProcessForm.process.fechaHastaStr"
						readOnly="false" /></td>
			</tr>
			<tr>
				<th>Tipo de Pr&oacute;rroga:</th>
				<td><asf:select name="process.tipoProceso"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.tipoProceso}" nullValue="true"
						nullText="&lt;&lt; Seleccione el tipo de Pr&oacute;rroga &gt;&gt;"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'prorroga.tipoProceso' order by codigo" /></td>
			</tr>
		</table>
		<br />
		<html:submit onclick="buscarCuotas();" value="Buscar" />
	</logic:equal>
	<br />
	<br />
</html:form>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="tablaForm">
	<html:hidden name="ProcessForm" property="process.action"
		styleId="action2" />
	<html:hidden name="ProcessForm" property="process.tipoProceso"
		styleId="tipoProceso2" />
	<logic:equal name="ProcessForm" value="false"
		property="process.finalizo">
		<html:radio property="process.tipoCorrimiento"
			onclick="seleccionCorrimiento('correrDias');" value="correrDias"
			styleId="seleccionDias">D&iacute;as</html:radio>
		<html:radio property="process.tipoCorrimiento"
			onclick="seleccionCorrimiento('correrMeses');" value="correrMeses"
			styleId="seleccionMeses">Meses</html:radio>
		<html:radio property="process.tipoCorrimiento"
			onclick="seleccionCorrimiento('fechaVencimiento');"
			value="fechaVencimiento" styleId="seleccionFecha">Fecha Vencimiento</html:radio>
		<br />
		<br />
		<table id="correrDias">
			<tr>
				<th>Movimiento</th>
				<td><html:radio property="process.tipoMovimiento"
						value="adelante">Hacia adelante</html:radio> <html:radio
						property="process.tipoMovimiento" value="atras">Hacia atr&aacute;s</html:radio>
				</td>
			</tr>
			<tr>
				<th>Cantidad de d&iacute;as</th>
				<td><html:text maxlength="10" property="process.dias"
						readonly="false" value="${ProcessForm.process.dias}"></html:text>
				</td>
				<td><html:button property="Process.process.calcularVencimiento"
						onclick="calcularVencimiento();" value="Modificar fechas" /></td>
			</tr>
		</table>
		<table id="correrMeses">
			<tr>
				<th>Movimiento</th>
				<td><html:radio property="process.tipoMovimiento"
						value="adelante">Hacia adelante</html:radio> <html:radio
						property="process.tipoMovimiento" value="atras">Hacia atr&aacute;s</html:radio>
				</td>
			</tr>
			<tr>
				<th>Cantidad de meses</th>
				<td><html:text maxlength="10" property="process.meses"
						readonly="false" value="${ProcessForm.process.meses}"></html:text>
				</td>
				<td><html:button property="Process.process.calcularVencimiento"
						onclick="calcularVencimiento();" value="Modificar fechas" /></td>
			</tr>
		</table>
		<table id="fechaVencimiento">
			<tr>
				<th>Fecha Vencimiento:</th>
				<td><asf:calendar
						property="ProcessForm.process.nuevoVencimientoStr"></asf:calendar></td>
				<td><html:button property="Process.process.calcularVencimiento"
						onclick="calcularVencimiento();" value="Modificar fechas" /></td>
			</tr>
		</table>
		<br />
		<table>
			<tr>
				<th>N&uacute;mero de resoluci&oacute;n:</th>
				<td><html:text name="ProcessForm" property="process.resolucion" /></td>
			</tr>
			<tr>
				<th>Observaciones:</th>
				<td><html:textarea name="ProcessForm"
						property="process.observaciones" /></td>
			</tr>
		</table>
		<h4>Cuotas</h4>
		<br />
		<logic:notEmpty name="ProcessForm" property="process.listaCuotas">
			<table>
				<tr>
					<th><input type="checkbox" onclick="activar();"
						id="activacion" align="left" checked="checked"></th>
				</tr>
			</table>
		</logic:notEmpty>
		<display:table name="ProcessForm" property="process.listaCuotas"
			export="true" id="tablaCuotas"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:column title="Seleccionar" media="html">
				<html:checkbox
					property='<%= "process.seleccion[" + (tablaCuotas_rowNum - 1) + "]" %>'
					value="${ProcessForm.process.seleccion[tablaCuotas_rowNum - 1]}"></html:checkbox>
			</display:column>
			<display:column property="credito.numeroAtencion"
				title="N�mero de cr�dito" sortable="false" />
			<display:column property="credito.persona.nomb12" title="Titular" />
			<display:column property="numero" title="N�mero de cuota"
				sortable="false" />
			<display:column property="fechaVencimiento"
				title="Fecha de vencimiento" sortable="false"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column property="capital" title="Capital" sortable="false"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column property="compensatorio"
				title="Inter�s compensatorio" sortable="false"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
		</display:table>
		<logic:notEmpty name="ProcessForm" property="process.listaCuotas">
			<table>
				<tr>
					<th><input type="checkbox" onclick="activar2();"
						id="activacion2" align="left" checked="checked"></th>
				</tr>
			</table>
		</logic:notEmpty>
	</logic:equal>
	<logic:equal name="ProcessForm" value="true"
		property="process.finalizo">
		<logic:notEmpty name="ProcessForm"
			property="process.cuotasProrrogadas">
			<display:table name="ProcessForm"
				property="process.cuotasProrrogadas" export="true"
				id="cuotasProrrogadas"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas con prorroga exitosa</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter�s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
		<logic:notEmpty name="ProcessForm"
			property="process.cuotasFechaSuperpuesta">
			<display:table name="ProcessForm"
				property="process.cuotasFechaSuperpuesta" export="true"
				id="cuotasFechaSuperpuesta"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas no prorrogadas, la nueva fecha se superpone con la siguiente cuota del plan</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter�s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
		<logic:notEmpty name="ProcessForm"
			property="process.cuotasFechaAnterior">
			<display:table name="ProcessForm"
				property="process.cuotasFechaAnterior" export="true"
				id="cuotasFechaAnterior"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:caption>Cuotas no prorrogadas, la nueva fecha es anterior a la fecha actual</display:caption>
				<display:column property="credito.numeroAtencion"
					title="N�mero de cr�dito" sortable="false" />
				<display:column property="credito.persona.nomb12" title="Titular" />
				<display:column property="numero" title="N�mero de cuota"
					sortable="false" />
				<display:column property="fechaVencimiento"
					title="Fecha de vencimiento" sortable="false"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column property="capital" title="Capital" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column property="compensatorio"
					title="Inter&eacute;s compensatorio" sortable="false"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</logic:notEmpty>
	</logic:equal>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script type="text/javascript">
	function buscarCuotas() {
		var action = $('action');
		var formulario = $('oForm');
		action.value = 'filtrar';
		formulario.submit();
	}

	function calcularVencimiento() {
		var confirmar = confirm("�Seguro que desea realizar la acci�n: Modificar fechas?");
		if (confirmar == true) {
			var action2 = $('action2');
			var formulario = $('tablaForm');
			action2.value = 'modificarFechas';
			var nodoRadio = document.getElementsByTagName("input");
			for (i = 0; i < nodoRadio.length; i++) {
				if (nodoRadio[i].type == "radio" && nodoRadio[i].name != "all"
						&& nodoRadio[i].checked == true) {
					$('tipoCorrimiento').value = nodoRadio[i].value;
					formulario.submit();
				}
			}
		}
	}

	function marcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = true;
			}
		}
	}

	function desmarcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}
	function activar() {
		var check = document.getElementById('activacion');
		if (check.checked == true) {
			marcarTodos();
		} else {
			desmarcarTodos();
		}
	}
	function activar2() {
		var check = document.getElementById('activacion2');
		if (check.checked == true) {
			marcarTodos();
		} else {
			desmarcarTodos();
		}
	}
	function seleccionCorrimiento(tipoCorrimiento) {
		if (tipoCorrimiento == 'correrMeses') {
			mostrar('correrMeses');
			ocultar('correrDias');
			ocultar('fechaVencimiento');
		} else if (tipoCorrimiento == 'correrDias') {
			mostrar('correrDias');
			ocultar('correrMeses');
			ocultar('fechaVencimiento');
		} else if (tipoCorrimiento == 'fechaVencimiento') {
			mostrar('fechaVencimiento');
			ocultar('correrDias');
			ocultar('correrMeses');
		}
	}
<%--iniciar pantalla--%>
	if ($('tipoCorrimiento').value == 'correrMeses') {
		mostrar('correrMeses');
		ocultar('correrDias');
		ocultar('fechaVencimiento');
		$('seleccionMeses').checked = true;
	} else if ($('tipoCorrimiento').value == 'fechaVencimiento') {
		mostrar('fechaVencimiento');
		ocultar('correrDias');
		ocultar('correrMeses');
		$('seleccionFecha').checked = true;
	} else {
		mostrar('correrDias');
		ocultar('correrMeses');
		ocultar('fechaVencimiento');
		$('seleccionDias').checked = true;
	}
</script>

