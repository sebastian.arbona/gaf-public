<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="ProcessForm">
	<html:hidden property="process.objetoi.id"
		value="${ProcessForm.process.objetoi.id}" />
	<html:hidden property="process.idObjetoi"
		value="${ProcessForm.process.idObjetoi}" />
	<html:hidden property="process.action" styleId="action" value="save" />

	<html:errors></html:errors>

	<table>
		<tr>
			<td style="text-transform: capitalize" colspan="4" align="center">
				<b> Datos del Proyecto </b>
			</td>
		</tr>
		<tr>
			<th>Fecha Ingreso Solicitud</th>
			<td colspan="4"><asf:calendar
					property="ProcessForm.process.objetoi.fechaSolicitudStr" /></td>
		</tr>
		<tr>
			<th>Fecha Emisi&oacute;n Resoluci&oacute;n</th>
			<td><asf:calendar
					property="ProcessForm.process.objetoi.fechaResolucionStr"
					readOnly="true" /></td>
			<th>Nro Resoluci&oacute;n</th>
			<td><html:text property="process.objetoi.resolucion"
					name="ProcessForm" size="30" disabled="true" /></td>
		</tr>
		<tr>
			<th>Fecha Firma Contrato</th>
			<td><asf:calendar
					property="ProcessForm.process.objetoi.fechaFirmaContratoStr" /></td>
			<th>Fecha Creaci&oacute;n Expediente</th>
			<td><asf:calendar
					property="ProcessForm.process.objetoi.fechaExpedienteStr"
					readOnly="true" /></td>
		</tr>
		<tr>
			<th>Unidad/Delegaci&oacute;n</th>
			<td colspan="4"><html:text property="process.objetoi.unidadStr"
					name="ProcessForm" size="90" disabled="true" /></td>
		</tr>
		<tr>
			<th>Objeto</th>
			<td colspan="3"><html:text property="process.objetoi.objeto"
					name="ProcessForm" size="90" /></td>
		</tr>
		<tr>
			<th>Judicial</th>
			<td><html:text property="process.judicial" name="ProcessForm"
					size="15" disabled="true" /></td>
			
		</tr>
		<tr>
			<th>Pertenece a Fecovita:</th>
			<td><asf:select name="process.objetoi.fecovitaTipo"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.objetoi.fecovitaTipo}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria = 'Linea.Fecovita' order by codigo"
					nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;" /></td>
		</tr>
		<tr>
			<th>Sector Econ&oacute;mico</th>
			<td><asf:select name="process.objetoi.tipoSector"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="${ProcessForm.process.objetoi.tipoSector}"
					filter="categoria='credito.sector' order by codigo"
					nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;" /></td>
		</tr>
		<tr>
			<td style="text-transform: capitalize" colspan="4" align="center">
				<b> Datos del Titular </b>
			</td>
		</tr>
		<tr>
			<th>C&oacute;digo Actividad</th>
			<td colspan="4"><asf:selectpop
					name="process.objetoi.persona.actividadAfip" title="ActividadAfip"
					columns="C�digo,Nombre,Descripcion"
					captions="codigo,nombre,descripcion" values="codigo"
					entityName="com.nirven.creditos.hibernate.ActividadAfip"
					value="${ProcessForm.process.objetoi.persona.actividadAfip}" /></td>
		</tr>
		<tr>
			<th>Inicio Actividades</th>
			<td colspan="4"><asf:calendar
					property="ProcessForm.process.objetoi.persona.fechaInicioActividadStr" /></td>
		</tr>
		<tr>
			<th>Sector Econ&oacute;mico</th>
			<td><asf:select name="process.objetoi.persona.tipoSector"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="${ProcessForm.process.objetoi.persona.tipoSector}"
					filter="categoria='persona.sector' order by codigo"
					nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;" /></td>
			<th>Cant. Personal</th>
			<td><html:text property="process.objetoi.persona.cantPersonal"
					name="ProcessForm" size="30" /></td>
		</tr>
		<tr>
			<th>�Es MiPyME?</th>
			<td><asf:select name="process.objetoi.esMiPymeStr"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.objetoi.esMiPymeStr}"
					listCaption="getDescripcion" listValues="getCodigo"
					filter="categoria = 'credito.mipyme' order by codigo" /></td>
			<th>Situaci&oacute;n frente al IVA</th>
			<td><asf:select name="process.objetoi.persona.situacionIva"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					value="${ProcessForm.process.objetoi.persona.situacionIva}"
					filter="categoria='persona.situacionIVA' order by codigo" /></td>
		</tr>
		<tr>
			<th>Nro Inscripci&oacute;n IIBB</th>
			<td><html:text property="process.objetoi.persona.iibb"
					name="ProcessForm" size="30" /></td>
			<th>Fecha Cierre Ejercicio Contable</th>
			<td><asf:calendar
					property="ProcessForm.process.objetoi.persona.fechaCierreEjercicioStr" /></td>
		</tr>
		<tr>
			<th>Volumen Venta</th>
			<td><asf:text name="ProcessForm"
					property="process.objetoi.persona.volumenVta" type="decimal"
					maxlength="15" /></td>
			<th>Per&iacute;odo Correspondiente</th>
			<td><html:text
					property="process.objetoi.persona.volumenVtaPeriodo"
					name="ProcessForm" size="30" /></td>
		</tr>
		<tr>
			<td style="text-transform: capitalize" colspan="4" align="center">
				<b> Cultivos </b>
			</td>
		</tr>
		<tr>
			<th>Tipo Cultivo</th>
			<td><html:text property="process.objetoi.tipoCult"
					name="ProcessForm" size="30" /></td>
			<th>Fecha Estimada de Cosecha</th>
			<td><asf:calendar
					property="ProcessForm.process.objetoi.fechaCosechaStr" /></td>
		</tr>
		<tr>
			<th>Cant. Has. del Proyecto</th>
			<td><html:text property="process.objetoi.propiedad.cantHas"
					name="ProcessForm" size="30" /></td>
			<th>Varietales</th>
			<td><html:checkbox property="process.objetoi.varietales"
					name="ProcessForm"></html:checkbox></td>
		</tr>
		<tr>
			<td style="text-transform: capitalize" colspan="4" align="center">
				<b> Bien a Adquirir </b>
			</td>
		</tr>
		<tr>
			<th>Precio Materia Prima</th>
			<td><html:text property="process.objetoi.precioMateria"
					name="ProcessForm" size="30" /></td>
			<th>Cantidad Solicitada</th>
			<td><html:text property="process.objetoi.cantMateria"
					name="ProcessForm" size="30" /></td>
		</tr>
		<tr>
			<th>Gastos Elaboraci&oacute;n</th>
			<td colspan="4"><html:text
					property="process.objetoi.gastosElaboracion" name="ProcessForm"
					size="30" /></td>
		</tr>
		<tr>
			<td style="text-transform: capitalize" colspan="4" align="center">
				<b> Rodados </b>
			</td>
		</tr>
		<tr>
			<th>Modelo</th>
			<td><html:text property="process.objetoi.taxiModelo"
					name="ProcessForm" size="30" /></td>
			<th>Marca</th>
			<td><html:text property="process.objetoi.taxiMarca"
					name="ProcessForm" size="30" /></td>
		</tr>
		<tr>
			<th>Dominio</th>
			<td colspan="4"><html:text
					property="process.objetoi.taxiDominio" name="ProcessForm" size="30" /></td>
		</tr>
	</table>
	<div style="width: 100%; text-align: center">
		<html:submit value="Guardar"></html:submit>
		<input type="button" value="Cancelar" onclick="cancelar();" />
	</div>
</html:form>

<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
	
</script>

<script type="text/javascript">
	$j = jQuery.noConflict();

	function cancelar() {
		var action = $('action');
		action.value = "load";
		var form = $('ProcessForm');
		form.submit();
	}
	function guardar() {
		var action = $('action');
		action.value = "save";
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>