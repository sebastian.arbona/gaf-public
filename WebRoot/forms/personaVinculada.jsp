<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/links.css" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js" ></script>

<script language="javascript" type="text/javascript">
	 /**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
  }//fin actualizarLista.-
</script>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.accion" value="guardar" styleId="accion" />
    <html:hidden property="process.personaTitular.id" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.id}" />
    <html:hidden property="process.personaVinculada.personaVinculada_id" styleId="idpersonaVinculada"/>
    <div style="width:70%" align="left"><html:errors/></div>
    <table>
        <tr><th>Id:</th><td><html:text property="process.personaVinculada.id" readonly="true"/></td></tr>
        <tr>
            <th>Persona:</th>
            <td>
            	<asf:autocomplete name="" tipo="AJAX" idDiv="autocomplete_persona"
                                  idInput="idpersona" classDiv="autocomplete"
                                  origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
                                  options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
                                  value="${empty ProcessForm.process.personaVinculada.personaVinculada.id ? '' : ProcessForm.process.personaVinculada.personaVinculada.id} ${empty ProcessForm.process.personaVinculada.personaVinculada.id ? '' : '-'} ${empty ProcessForm.process.personaVinculada.personaVinculada.id ? '' : ProcessForm.process.personaVinculada.personaVinculada.nomb12Str2} ${empty ProcessForm.process.personaVinculada.personaVinculada.id ? '' : '-'} ${empty ProcessForm.process.personaVinculada.personaVinculada.id ? '' : ProcessForm.process.personaVinculada.personaVinculada.nudo12Str}"
                                  attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'"/>
                                  <span id="indicadorPersona" style="display: none"><img src="images/loader.gif" alt="Trabajando..." /></span>
                                  <input value="..." onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=idpersona&amp;entityName=Persona&amp;title=Selecci�n de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');" type="button">&nbsp;
                                  (C�digo-Apellido y Nombre-DNI)
                                  <a href="javascript:void(0);" onclick="popUpBigBig('${pageContext.request.contextPath}/actions/altaRapidaAction.do?do=newEntity&entityName=Persona&forward=PersonaAltaRapida');"><b>Registrar Persona</b></a>
                 
           </td>
        </tr>
        <tr>
            <th>Tipo de Relaci�n: </th>
            <td>
                <asf:select name="tipoRelacion" entityName="TipoRelacion" listCaption="idTipoRelacion,nombreTipoRelacion"
                            listValues="idTipoRelacion" nullText="Seleccione el Tipo de Relacion" nullValue="true"
                            value="${ProcessForm.process.personaVinculada.relacion.tipoRelacion_id}"/>
            </td>
        </tr>
        <tr>
            <th>Relaci�n: </th>
            <td>
                <asf:lselect property="process.personaVinculada.relacion_id" entityName="Relacion"
                             listCaption="idRelacion,nombreRelacion" listValue="idRelacion"
                             value="${ProcessForm.process.personaVinculada.relacion_id}"
                             linkFK="tipoRelacion.id" linkName="tipoRelacion" orderBy="idRelacion"
                             nullText="Seleccione la Relacion" nullValue="true"/>
            </td>
        </tr>
    </table>
    <br>
    <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=guardar&process.personaVinculada.id=${ProcessForm.process.personaVinculada.id}">
	    <input type="button" value="Guardar" onclick="guardar();">
	</asf:security>
    <input type="button" value="Cancelar" onclick="cancelar();">
</html:form>
<script language='javascript' >
	var ctrlPersona        = $('idpersona');
	var idpersonaVinculada = $('idpersonaVinculada');
	var formulario         = $('oForm');
	var accion             = $('accion');
	
	//AL CARGAR LA PAGINA.-
	if(ctrlPersona.value == "    ")
	{
		ctrlPersona.value = "";
	}
	ctrlPersona.focus();
	
	function cancelar()
	{
		accion.value = "cancelar";

		formulario.submit();
	}//fin cancelar.-
	
	function guardar()
    {
    	var idPersona = trim(ctrlPersona.value.split("-")[0]);
    	idpersonaVinculada.value = idPersona;
    	
		formulario.submit();
   }//fin guardar.-

   Event.observe(window, 'load', function() 
   {
   		this.name = "Persona Vinculada";
		parent.push(this);
   });
</script>
