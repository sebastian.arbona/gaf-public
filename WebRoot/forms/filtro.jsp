<%@ page language="java"%>
<%@page contentType="text/html"%>
<%--@page pageEncoding="ISO-8859-1"--%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Filtro</div>
<br><br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}">

<table border="0">
   
<tr><th>ID:</th><td><html:text property="entity.idfiltro" readonly="true"/></td></tr>
<tr><th>Nombre:</th><td><html:text property="entity.nombre" maxlength="40"/></td></tr>    
<tr><th>Tabla:</th><td><html:text property="entity.tabla" maxlength="20" /></td></tr>    
<tr><th>Tipo:</th><td><asf:select name="entity.tipo" entityName="com.asf.hibernate.mapping.Tipificadores" value="${AbmForm.entity.tipo}" listCaption="getCodigo,getDescripcion" listValues="getCodigo" filter="categoria='TipoControl' order by codigo" /></td></tr>
<tr><th>Etiquetas:</th><td><html:textarea cols="40" rows="4" property="entity.etiquetas" onkeypress="caracteres(this,200,event);"  /></td></tr>    
<tr><th>Valores:</th><td><html:textarea cols="40" rows="4" property="entity.valores" onkeypress="caracteres(this,200,event);" /></td></tr>    
<tr><th>Columnas:</th><td><html:textarea cols="40" rows="4" property="entity.columnas" onkeypress="caracteres(this,200,event);"  /></td></tr>    
<tr><th>NullText:</th><td><html:textarea cols="40" rows="4" property="entity.nulltext" onkeypress="caracteres(this,200,event);" /></td></tr>    
<tr><th>NullValue:</th><td><asf:select name="entity.nullvalue" value="${AbmForm.entity.nullvalue}" listCaption="Si,No" listValues="1,0" /></td></tr>    

</table>

<asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
	<html:submit><bean:message key="abm.button.save"/></html:submit>
</asf:security>
	
<html:cancel><bean:message key="abm.button.cancel"/></html:cancel>
</html:form>

