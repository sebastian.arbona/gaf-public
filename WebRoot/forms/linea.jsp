<%@page language="java"%>
<%@page import="com.asf.gaf.hibernate.Ejercicio"%>
<%@page import="com.asf.gaf.hibernate.Tiponomenclador"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<%
pageContext.setAttribute("ejercicio", Ejercicio.getEjercicioActual());
pageContext.setAttribute("idTipoNomenclador", Tiponomenclador.IDCOMBINADO);
pageContext.setAttribute("nomenclador1", Tiponomenclador.getNombre(Tiponomenclador.IDCOMBINADO));
pageContext.setAttribute("nomenclador2", Tiponomenclador.getNombre(Tiponomenclador.IDEXPOSICION));
pageContext.setAttribute("nomenclador3", Tiponomenclador.getNombre(Tiponomenclador.IDEXPOSICIONGERENCIAL));
%>

<div class="title">L&iacute;nea de Cr&eacute;dito</div>
<br>
<br>
<html:form
	action="/actions/abmAction.do?do=save&entityName=${param.entityName}">
	<asf:security action="/actions/modificarLinea"
		access="form=datosGenerales">
		<div id="datosGenerales">
			<a>Datos Generales</a>
			<div>
				<table border="0">
					<tr>
						<th>Id:</th>
						<td><asf:text name="AbmForm" property="entity.id" type="long"
								maxlength="8" readonly="true" /></td>
					</tr>
					<tr>
						<th>Denominaci&oacute;n Abreviada:</th>
						<td><html:textarea property="entity.nombre" styleId="nombre"></html:textarea>
						</td>
					</tr>
					<tr>
						<th>Fecha Fin de Operaciones:</th>
						<td><asf:calendar property="entity.fechaFinOperacionesStr"
								maxlength="10"></asf:calendar></td>
					</tr>
					<tr>
						<th>Sector:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.Sector"
								listCaption="getId,getNombre" listValues="getId"
								name="entity.sector_id" nullValue="true"
								nullText="&lt;&lt;Seleccione el Sector&gt;&gt;"
								value="${AbmForm.entity.sector_id}" /></td>
					</tr>
					<tr>
						<th>Est&aacute; activa:</th>
						<td><asf:select name="entity.activa"
								value="${AbmForm.entity.activa}" listCaption="SI,NO"
								listValues="true,false" /></td>
					</tr>
					<tr>
						<th>Denominaci&oacute;n:</th>
						<td><html:textarea property="entity.detalle" styleId="nombre"></html:textarea>
						</td>
					</tr>
					<tr>
						<th>Monto M&aacute;ximo Asignado a la L&iacute;nea:</th>
						<td><asf:text name="AbmForm" property="entity.montoStr"
								type="decimal" maxlength="12" /></td>
					</tr>
					<tr>
						<th>Monto del Cr&eacute;dito:</th>
						<td><asf:text name="AbmForm"
								property="entity.montoObjetoiStr" type="decimal" maxlength="12" /></td>
					</tr>
					<tr>
						<th>Moneda:</th>
						<td><asf:select entityName="com.asf.gaf.hibernate.Moneda"
								listCaption="getId,getDenominacion" listValues="getId"
								name="entity.moneda_id" value="${AbmForm.entity.moneda_id}" /></td>
					</tr>
					<tr>
						<th>Sistema de Amortizaci&oacute;n</th>
						<td><asf:select name="entity.tipoAmortizacion"
								entityName="com.asf.hibernate.mapping.Tipificadores"
								listCaption="getCodigo,getDescripcion" listValues="getCodigo"
								value="${AbmForm.entity.tipoAmortizacion}"
								filter="categoria='amortizacion.metodo' order by codigo"
								enabled="false" /></td>
					</tr>
					<tr>
						<th>Frecuencia Cuotas Capital:</th>
						<td><asf:select name="entity.periodicidadCapital"
								entityName="com.asf.hibernate.mapping.Tipificadores"
								listCaption="getCodigo,getDescripcion" listValues="getCodigo"
								value="${AbmForm.entity.periodicidadCapital}"
								filter="categoria='amortizacion.periodicidad' order by codigo" />
						</td>
					</tr>
					<tr>
						<th>Frecuencia Cuotas Inter&eacute;s:</th>
						<td><asf:select name="entity.periodicidadInteres"
								entityName="com.asf.hibernate.mapping.Tipificadores"
								listCaption="getCodigo,getDescripcion" listValues="getCodigo"
								value="${AbmForm.entity.periodicidadInteres}"
								filter="categoria='amortizacion.periodicidad' order by codigo" />
						</td>
					</tr>
					<tr>
						<th>Cantidad Cuotas Capital:</th>
						<td><html:text property="entity.cantCuotasCapital" size="30" />
						</td>
					</tr>
					<tr>
						<th>Cantidad Cuotas Inter&eacute;s:</th>
						<td><html:text property="entity.cantCuotasInteres" size="30" />
						</td>
					</tr>
					<tr>
						<th>Controlar existencia de CBU antes de Resoluci&oacute;n:</th>
						<td><html:checkbox name="AbmForm"
								property="entity.controlaCBU"></html:checkbox></td>
					</tr>
					<tr>
						<th>Permitir una sola Solicitud dentro del Per&iacute;odo:</th>
						<td><html:checkbox name="AbmForm"
								property="entity.solExcluyente"></html:checkbox></td>
					</tr>
					<tr>
						<th>Proceso de Solicitud (BPM):</th>
						<td><html:text property="entity.nombreProceso"
								styleId="nombreProceso"></html:text></td>
					</tr>
					<tr>
						<th>C&oacute;digo de Tipo de Expediente:</th>
						<td><html:text property="entity.tipoExpediente"></html:text></td>
					</tr>
					<tr>
						<th>Responsable de la L&iacute;nea:</th>
						<td><asf:select name="entity.responsableId"
								entityName="com.nirven.creditos.hibernate.Usuario"
								listCaption="id" listValues="id"
								value="${AbmForm.entity.responsableId}" filter="activo=true"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th>Pertenece a Fecovita:</th>
						<td><asf:select name="entity.fecovitaTipo"
								entityName="com.asf.hibernate.mapping.Tipificadores"
								value="${AbmForm.entity.fecovitaTipo}"
								listCaption="getCodigo,getDescripcion" listValues="getCodigo"
								filter="categoria = 'Linea.Fecovita' order by codigo"
								nullValue="true" nullText="&lt;&lt;NO&gt;&gt;" /></td>
					</tr>
				</table>
			</div>
			<div id="tasaCompensatorio">
				<br> <a>Tasa Compensatorio</a>
				<div>
					<table>
						<tr>
							<th>Tasa Inter&eacute;s Compensatorio</th>
							<td><asf:selectpop name="entity.idIndiceComp" title="Tasa"
									columns="Codigo, Nombre" captions="id,nombre" values="id"
									entityName="com.nirven.creditos.hibernate.Indice"
									value="${AbmForm.entity.idIndiceComp}" filter="tipo in (1, 4)"
									onChange="tasaComp();" /> <asf:text name="AbmForm"
									id="indiceValorComp" readonly="true"
									property="entity.indiceValorComp"
									value="${AbmForm.entity.indiceValorComp}" type="decimal"
									maxlength="15" /></td>
						</tr>
						<tr>
							<th>Mas</th>
							<td><html:text property="entity.valorMasCompStr" size="15"
									onchange="calcularValorFinalComp();" styleId="masComp" /></td>
						</tr>
						<tr>
							<th>Por</th>
							<td><asf:text id="porComp" name="AbmForm"
									property="entity.valorPorCompStr"
									attribs='onchange="calcularValorFinalComp();"' type="decimal"
									maxlength="15" /></td>
						</tr>
						<tr>
							<th>Tope Tasa</th>
							<td><html:text property="entity.topeTasaCompensatorioStr"
									size="15" styleId="topeTasaCompensatorioStr"
									onchange="calcularValorFinalComp();" /></td>
						</tr>
						<tr>
							<th>D&iacute;as antes</th>
							<td><html:text value="${AbmForm.entity.diasAntesComp}"
									property="entity.diasAntesComp" size="15" /></td>
						</tr>
						<tr>
							<th>Tasa Inter&eacute;s Compensatorio final</th>
							<td><html:text property="entity.valorFinalComp"
									readonly="true" size="15" styleId="valorFinalComp" /></td>
						</tr>
						<tr>
							<th>Tipo C&aacute;lculo Compensatorio:</th>
							<td><asf:select name="entity.tipoCalculoCompensatorio"
									entityName="com.asf.hibernate.mapping.Tipificadores"
									listCaption="getCodigo,getDescripcion" listValues="getCodigo"
									value="${AbmForm.entity.tipoCalculoCompensatorio}"
									filter="categoria='Interes.TipoCalculo' order by codigo desc"
									nullValue="true"
									nullText="-- Seleccione un Tipo de C&aacute;lculo --" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="tasaMoratorio">
				<br> <a>Tasa Moratorio</a>
				<div>
					<table>
						<tr>
							<th>Tasa Inter&eacute;s Moratorio</th>
							<td><asf:selectpop name="entity.idIndiceMor" title="Tasa"
									columns="Codigo, Nombre" captions="id,nombre" values="id"
									entityName="com.nirven.creditos.hibernate.Indice"
									value="${AbmForm.entity.idIndiceMor}" filter="tipo in (1, 4)"
									onChange="tasaMor();" /> <asf:text name="AbmForm"
									id="indiceValorMor" readonly="true"
									property="entity.indiceValorMor"
									value="${AbmForm.entity.indiceValorMor}" type="decimal"
									maxlength="15" /></td>
						</tr>
						<tr>
							<th>Mas</th>
							<td><html:text property="entity.valorMasMorStr" size="15"
									onchange="calcularValorFinalMor();" styleId="masMor" /></td>
						</tr>
						<tr>
							<th>Por</th>
							<td><asf:text id="porMor" name="AbmForm"
									property="entity.valorPorMorStr"
									attribs='onchange="calcularValorFinalMor();"' type="decimal"
									maxlength="15" /></td>
						</tr>
						<tr>
							<th>Tope Tasa</th>
							<td><html:text property="entity.topeTasaMoratorioStr"
									size="15" styleId="topeTasaMoratorioStr"
									onchange="calcularValorFinalMor();" /></td>
						</tr>
						<tr>
							<th>D&iacute;as antes</th>
							<td><html:text value="${AbmForm.entity.diasAntesMor}"
									property="entity.diasAntesMor" size="15" /></td>
						</tr>
						<tr>
							<th>Tasa Inter&eacute;s Moratorio final</th>
							<td><html:text property="entity.valorFinalMor"
									readonly="true" size="15" styleId="valorFinalMor" /></td>
						</tr>
						<tr>
							<th>Tipo C&aacute;lculo Moratorio:</th>
							<td><asf:select name="entity.tipoCalculoMoratorio"
									entityName="com.asf.hibernate.mapping.Tipificadores"
									listCaption="getCodigo,getDescripcion" listValues="getCodigo"
									value="${AbmForm.entity.tipoCalculoMoratorio}"
									filter="categoria='Interes.TipoCalculo' order by codigo desc"
									nullValue="true"
									nullText="-- Seleccione un Tipo de C&aacute;lculo --" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div id="tasaPunitorio">
				<br> <a>Tasa Punitorio</a>
				<div>
					<table>
						<tr>
							<th>Tasa Inter&eacute;s Punitorio</th>
							<td><asf:selectpop name="entity.idIndicePun" title="Tasa"
									columns="Codigo, Nombre" captions="id,nombre" values="id"
									entityName="com.nirven.creditos.hibernate.Indice"
									value="${AbmForm.entity.idIndicePun}" filter="tipo in (1, 4)"
									onChange="tasaPun();" /> <html:text name="AbmForm"
									styleId="indiceValorPun" onchange="calcularValorFinalPun();"
									readonly="true" property="entity.indiceValorPun"
									value="${AbmForm.entity.indiceValorPun}" maxlength="15" /></td>
						</tr>
						<tr>
							<th>Mas</th>
							<td><html:text property="entity.valorMasPunStr" size="15"
									onchange="calcularValorFinalPun();" styleId="masPun" /></td>
						</tr>
						<tr>
							<th>Por</th>
							<td><asf:text id="porPun" name="AbmForm"
									property="entity.valorPorPunStr"
									attribs='onchange="calcularValorFinalPun();"' type="decimal"
									maxlength="15" /></td>
						</tr>
						<tr>
							<th>Tope Tasa</th>
							<td><html:text property="entity.topeTasaPunitorioStr"
									size="15" styleId="topeTasaPunitorioStr"
									onchange="calcularValorFinalPun();" /></td>
						</tr>
						<tr>
							<th>D&iacute;as antes</th>
							<td><html:text value="${AbmForm.entity.diasAntesPun}"
									property="entity.diasAntesPun" size="15" /></td>
						</tr>
						<tr>
							<th>Tasa Inter&eacute;s Punitorio final</th>
							<td><html:text property="entity.valorFinalPun"
									readonly="true" size="15" styleId="valorFinalPun" /></td>
						</tr>
						<tr>
							<th>Tipo C&aacute;lculo Punitorio:</th>
							<td><asf:select name="entity.tipoCalculoPunitorio"
									entityName="com.asf.hibernate.mapping.Tipificadores"
									listCaption="getCodigo,getDescripcion" listValues="getCodigo"
									value="${AbmForm.entity.tipoCalculoPunitorio}"
									filter="categoria='Interes.TipoCalculo' order by codigo desc"
									nullValue="true"
									nullText="-- Seleccione un Tipo de C&aacute;lculo --" /></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</asf:security>
	<asf:security action="/actions/modificarLinea"
		access="form=gastosAdministrativos">
		<div id="gastosAdministrativos">
			<br> <a>Gastos Administrativos</a>
			<div>
				<table border="0">
					<tr>
						<th style="width: 268px;">Monto M&aacute;ximo:</th>
						<td style="width: 365px;"><asf:text name="AbmForm"
								property="entity.montoMaximo" type="decimal" maxlength="6" /></td>
					</tr>
					<tr>
						<th>Monto M&iacute;nimo:</th>
						<td><asf:text name="AbmForm" property="entity.montoMinimo"
								type="decimal" maxlength="6" /></td>
					</tr>
					<tr>
						<th>Importe:</th>
						<td><asf:text name="AbmForm" property="entity.importe"
								type="decimal" maxlength="6" /></td>
					</tr>
					<tr>
						<th>Porcentaje:</th>
						<td><asf:text name="AbmForm" property="entity.porcentaje"
								type="decimal" maxlength="6" /></td>
					</tr>
				</table>
			</div>
		</div>
	</asf:security>
	<asf:security action="/actions/modificarLinea"
		access="form=gastosDesembolso">
		<div id="gastosDesembolso">
			<br> <a>Gastos Desembolso</a>
			<div>
				<table border="0">
					<tr>
						<th style="width: 268px;">Monto M&aacute;ximo:</th>
						<td style="width: 365px;"><asf:text name="AbmForm"
								property="entity.montoMaximoGastosDes" type="decimal"
								maxlength="6" /></td>
					</tr>
					<tr>
						<th>Monto M&iacute;nimo:</th>
						<td><asf:text name="AbmForm"
								property="entity.montoMinimoGastosDes" type="decimal"
								maxlength="6" /></td>
					</tr>
					<tr>
						<th>Porcentaje Gastos Desembolso:</th>
						<td><asf:text name="AbmForm"
								property="entity.porcentajeGastosDes" type="decimal"
								maxlength="6" /></td>
					</tr>
					<tr>
						<th>Aplica Gastos en primer Desembolso:</th>
						<td><html:checkbox name="AbmForm"
								property="entity.gastosEnDesembolso" /></td>
					</tr>
				</table>
			</div>
		</div>
	</asf:security>
	<asf:security action="/actions/modificarLinea"
		access="form=configuracionContable">
		<div id="configuracionContable">
			<br> <a>Configuraci&oacute;n Contable</a>
			<div id="capital">
				<table border="0">
					<tr>
						<th style="width: 268px;">Capital:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.capitalId" value="${AbmForm.entity.capitalId}"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio Capital:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioCapital"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioCapital}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos Capital:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosCapital"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosCapital}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="compensatorio">
				<table border="0">
					<tr>
						<th style="width: 268px;">Intereses Compensatorios:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.compensatorioId"
								value="${AbmForm.entity.compensatorioId}" nullValue="true"
								nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio
							Compensatorio:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioCompensatorio"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioCompensatorio}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos
							Compensatorio:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosCompensatorio"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosCompensatorio}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="moratorio">
				<table border="0">
					<tr>
						<th style="width: 268px;">Intereses Moratorios:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.moratorioId" value="${AbmForm.entity.moratorioId}"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio
							Moratorio:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioMoratorio"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioMoratorio}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos
							Moratorios:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosMoratorio"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosMoratorio}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="punitorio">
				<table border="0">
					<tr>
						<th style="width: 268px;">Intereses Punitorios:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.punitorioId" value="${AbmForm.entity.punitorioId}"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio
							Punitorio:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioPunitorio"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioPunitorio}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos
							Punitorios:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosPunitorio"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosPunitorio}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="gastos">
				<table border="0">
					<tr>
						<th style="width: 268px;">Gastos:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.gastoId" value="${AbmForm.entity.gastoId}"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio Gastos:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioGasto"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioGasto}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos Gastos:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosGasto"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosGasto}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="gastosRec">
				<table border="0">
					<tr>
						<th style="width: 268px;">Gastos a Recuperar:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.gastoRecId" value="${AbmForm.entity.gastoRecId}"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio Gastos a
							Recuperar:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioGastoRec"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioGastoRec}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos Gastos a
							Recuperar:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosGastoRec"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosGastoRec}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="multas">
				<table border="0">
					<tr>
						<th style="width: 268px;">Multas:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.multaId" value="${AbmForm.entity.multaId}"
								nullValue="true" nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio Multa:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioMulta"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioMulta}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos Multas:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosMulta"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosMulta}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="prejudiciales">
				<table border="0">
					<tr>
						<th style="width: 268px;">Gastos Prejudiciales:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto"
								name="entity.gastosPrejudicialesId"
								value="${AbmForm.entity.gastosPrejudicialesId}" nullValue="true"
								nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio Gastos
							Prejudiciales:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioGastosPrejudiciales"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioGastosPrejudiciales}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos Gastos
							Prejudiciales:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContableRecursosGastosPrejudiciales"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosGastosPrejudiciales}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
			<div id="cer">
				<table border="0">
					<tr>
						<th style="width: 268px;">CER a Cobrar:</th>
						<td><asf:select
								entityName="com.nirven.creditos.hibernate.CConcepto"
								listCaption="concepto" listValues="concepto" name="entity.cerId"
								value="${AbmForm.entity.cerId}" nullValue="true"
								nullText="&lt;&lt;Seleccione&gt;&gt;" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Patrimonio CER a
							Cobrar:</th>
						<td><asf:selectpop
								name="entity.codigoCtaContablePatrimonioCER"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContablePatrimonioCER}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
					<tr>
						<th style="width: 268px;">Cuenta Contable Recursos CER a
							Cobrar:</th>
						<td><asf:selectpop name="entity.codigoCtaContableRecursosCER"
								title="Cuenta contable"
								columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Plancta"
								value="${AbmForm.entity.codigoCtaContableRecursosCER}"
								filter="ejercicio=${ejercicio}" /></td>
					</tr>
				</table>
			</div>
		</div>
	</asf:security>
	<asf:security action="/actions/modificarLinea"
		access="form=configuracionPresupuesto">
		<div id="configuracionPresupuesto">
			<br> <a>Configuraci&oacute;n Presupuestaria</a>
			<div>
				<table border="0">
					<tr>
						<th style="width: 268px;">Nomenclador ${nomenclador1}:</th>
						<td><asf:selectpop name="entity.codigoIegreso"
								title="Nomenclador" columns="Codigo,Denominacion, Codigo Corto"
								captions="codigo,denominacion,codigoCorto" values="codigo"
								entityName="com.asf.gaf.hibernate.Iegreso"
								value="${AbmForm.entity.codigoIegreso}"
								filter="ejercicio=${ejercicio} and imputable=1 and idTipoNomenclador=${idTipoNomenclador}" />
						</td>
					</tr>
					<tr>
						<th>Nomenclador ${nomenclador2}:</th>
						<td><asf:selectpop name="entity.codigoInstitucional"
								title="Nomenclador" columns="Codigo,Denominacion"
								captions="codigo,denominacion" values="codigo"
								entityName="com.asf.gaf.hibernate.Institucional"
								value="${AbmForm.entity.codigoInstitucional}"
								filter="ejercicio=${ejercicio} and imputable=1" /></td>
					</tr>
					<tr>
						<th>Nomenclador ${nomenclador3}:</th>
						<td><asf:selectpop name="entity.codigoNomenclador"
								title="Nomenclador" columns="Codigo,Denominacion"
								captions="codigo,denominacion" values="codigo"
								entityName="com.asf.gaf.hibernate.Nomenclador"
								value="${AbmForm.entity.codigoNomenclador}"
								filter="ejercicio=${ejercicio} and imputable=1" /></td>
					</tr>
				</table>
			</div>
		</div>
	</asf:security>
	<asf:security action="/actions/abmAction.do"
		access="do=save&entityName=${param.entityName}">
		<html:submit>
			<bean:message key="abm.button.save" />
		</html:submit>
	</asf:security>
	<html:cancel>
		<bean:message key="abm.button.cancel" />
	</html:cancel>
</html:form>
<script type="text/javascript">
	$j = jQuery.noConflict();

	function calcularValorFinalComp() {
		var tasa = $j("#indiceValorComp").val().replace(',', '.').replace('%',
				'');
		var mas = $j("#masComp").val().replace(',', '.').replace('%', '');
		var por = $j("#porComp").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasaCompensatorioStr").val().replace(',', '.')
				.replace('%', '');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinalComp").val(data);
						});
	}
	function tasaComp() {
		var idIndice = $j("#entity\\.idIndiceComp").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='
								+ idIndice,
						function(data) {
							$j("#indiceValorComp").val(data);
							$j("#valorFinalComp").val(calcularValorFinalComp());
						});
		var form = document.getElementById('oForm');
		var action = document.getElementById('action');
		action.value = 'modificarBonificacion';
		form.submit();
	}
	function calcularValorFinalPun() {
		var tasa = $j("#indiceValorPun").val().replace(',', '.').replace('%',
				'');
		var mas = $j("#masPun").val().replace(',', '.').replace('%', '');
		var por = $j("#porPun").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasaPunitorioStr").val().replace(',', '.')
				.replace('%', '');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinalPun").val(data);
						});
	}
	function tasaPun() {
		var idIndice = $j("#entity\\.idIndicePun").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='
								+ idIndice, function(data) {
							$j("#indiceValorPun").val(data);
							$j("#valorFinalPun").val(calcularValorFinalPun());
						});
		var form = document.getElementById('oForm');
		var action = document.getElementById('action');
		action.value = 'modificarBonificacion';
		form.submit();
	}
	function calcularValorFinalMor() {
		var tasa = $j("#indiceValorMor").val().replace(',', '.').replace('%',
				'');
		var mas = $j("#masMor").val().replace(',', '.').replace('%', '');
		var por = $j("#porMor").val().replace(',', '.').replace('%', '');
		var topeTasa = $j("#topeTasaMoratorioStr").val().replace(',', '.')
				.replace('%', '');
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularValorFinal',
						'&tasa=' + tasa + '&mas=' + mas + '&por=' + por
								+ '&topeTasa=' + topeTasa, function(data) {
							$j("#valorFinalMor").val(data);

						});
	}
	function tasaMor() {
		var idIndice = $j("#entity\\.idIndiceMor").val();
		$j
				.get(
						'${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarValorIndice&idIndice='
								+ idIndice, function(data) {
							$j("#indiceValorMor").val(data);
							$j("#valorFinalMor").val(calcularValorFinalMor());
						});
		var form = document.getElementById('oForm');
		var action = document.getElementById('action');
		action.value = 'modificarBonificacion';
		form.submit();
	}
</script>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
