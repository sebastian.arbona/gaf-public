<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%pageContext.setAttribute( "urlDestino", request.getParameter( "urlDestino" ) ); %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Sistema de Gesti�n de Cr�ditos</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/estilos_public_FTyC.css" type="text/css" media="screen" />
</head>

<body id="login">
<div id="wrapper">
	<html:errors/>
	<div id="loginForm">
    	<html:form action="/actions/login?do=login" style="padding-top:0px; padding-left:0px; padding-bottom:10px">
    		<html:hidden property="urlDestino" value="${urlDestino}" />
        	Usuario:<html:text property="name" styleClass="username"/>
            Clave:<html:password property="password" maxlength="15" styleClass="password"/>        	
            <input name="" type="submit" class="submit" value=""/>
            
        </html:form>
        
    </div>
    
</div>
</body>
</html>