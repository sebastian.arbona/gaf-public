<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<div class="title">
	Bonificaci�n de Tasa Bancaria
</div>
<br>
<br>

<html:form
	action="actions/process.do?processName=${param.processName}&do=process"
	styleId="ProcessForm">
	
	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
	<html:hidden property="process.idPersona" name="ProcessForm" />
	<html:hidden name="ProcessForm" property="process.esPersona" />
	<html:errors />
	<logic:equal name="ProcessForm" property="process.creada" value="true">
	Bonificaci�n de Tasa Creada Correctamente
	<tr>
		<input type="button" value="Atras" onclick="atras();"/> 
	</tr>
	</logic:equal>
	<logic:equal property="process.creada" name="ProcessForm" value="false">
	<table border="0" width="90%" align="left" >
		<tr>
			<th>
				Fecha Solicitud
			</th>	
			<td colspan="5">
				<html:text property="process.fechaHoy" disabled="true" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Agente Financiero
			</th>	
			<td colspan="3"><asf:selectpop name="process.idEnte" title="Agente Financiero"
				columns="C�digo, Nombre"
				captions="codiBa,detaBa" values="codiBa"
				entityName="com.asf.gaf.hibernate.Bancos"
				value="${ProcessForm.process.idEnte}"  enabled="${ProcessForm.process.gene}" 
				filter="financiador=true"/>
			</td>
		</tr>
		<tr>
			<th>
				Operatoria
			</th>
			<td>	
				<asf:text name="ProcessForm" property="process.operatoria" type="text" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th>
				Monto Financiamiento
			</th>
			<td>	
				<asf:text name="ProcessForm" property="process.montoFinanciamiento" type="decimal" maxlength="40"/>
			</td>
		</tr>
	 	<tr>
	 	<logic:equal value="false" property="process.esPersona" name="ProcessForm">
			<b>Persona:</b>
		<asf:selectpop name="ProcessForm.process.idPersona" title="Persona"
			columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
			captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
			entityName="com.civitas.hibernate.persona.Persona" value="" onChange="selecPersona();"/>
			</logic:equal>
		</tr>
		<tr>
			<th>
				Volumen de Venta:
			</th>
			<td colspan="3">
				<asf:text  name="ProcessForm" property="process.volumenVtaAnual" type="Long" maxlength="40"/>
			</td>
			<th>
				En
			</th>
			
			<td colspan="3">
				<html:select property="process.periodoVolVtaAnual" name="ProcessForm" styleId="periodo">
				<html:option value="-1">Seleccione Periodo</html:option>
				<html:option value="1">Ultimos 6 meses</html:option>
				<html:option value="2">Ultimo a�o</html:option>
				<html:option value="3">Ultimos 2 a�os</html:option>
				<html:option value="4">Ultimos 3 a�os</html:option>
				</html:select>
				</td>
		</tr>
		<tr>
			<th>
				Tipo de Empresa:
			</th>
			<td colspan="5">
				<asf:select name="process.idTipoEmpresa"
							entityName="com.asf.hibernate.mapping.Tipificadores" 
							listCaption="getCodigo,getDescripcion" 
							listValues="getId"
							value="${ProcessForm.process.idTipoEmpresa}"
							filter="categoria='TipoEmpresa' order by codigo" />
			</td>
		</tr>
		<tr>
			<th>
				Inicio de Actividad
			</th>
			<td colspan="3">
				<asf:calendar property="ProcessForm.process.inicioActividad" value="${ProcessForm.process.inicioActividad}" />
			</td>
			<th>
				Personal Ocupado
			</th>
			<td>
				<asf:text property="process.personalOcupado" name="ProcessForm" type="long" maxlength="40" />
		</tr>
		<tr>
			<th>
				Facturacion al mercado externo ($)
			</th>
			<td colspan="5">
				<asf:text property="process.facturacionMercExterno" name="ProcessForm" type="decimal" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th>
				Has. Beneficiadas
			</th>
			<td colspan="3">
				<asf:text property="process.hasBeneficiadas" name="ProcessForm" type="decimal" maxlength="40"/>
			</td>
			<th>
				QQ Cosechados
			</th>
			<td>
				<asf:text property="process.qqCosechados" name="ProcessForm" type="decimal" maxlength="40"/>
			</td>
		</tr>
		<tr>
			<th>
				Destino de Fondos
			</th>
			<td colspan="5">
				<html:textarea property="process.objeto" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Certificacion de Normas de Calidad
			</th>
			<td colspan="5">
				<html:textarea property="process.certifNormas" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Condicionamientos
			</th>
			<td colspan="5">
				<html:textarea property="process.condicionamientos" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Observaciones
			</th>
			<td colspan="5">
				<html:textarea property="process.observaciones" name="ProcessForm"/>
			</td>
		</tr> 
		<tr>					
			<td colspan="5">
				<html:submit  value="Guardar" onclick="guardar();"></html:submit>
				<input type="button" value="Cancelar" onclick="cancelar();">
			</td>
		</tr> 
		</table>
		<div>
		<div>
		
		<logic:equal value="true" parameter="process.creada">
		<input type="button" value="Volver" onclick="atras();"/>
		</logic:equal>
		</div>
		</div>
		</logic:equal>
		<logic:equal name="ProcessForm" property="process.creada" value="true">
		<div id="pestanias" class="yui-navset" style="width: 100%;">
			<ul class="yui-nav">
				<li class="selected" id="tab_requisitos">
		            <a href="#requisitos" onclick="irARequisitos();"><em>Requisitos</em> </a>
		          </li>
			</ul>
			<div class="yui-content">
				<div id="requisitos">
	            <iframe
	              src="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaRequisitos&process.idBonTasa=${ProcessForm.process.idBonTasa}&process.accion=listar&process.esPersona=${ProcessForm.process.esPersona}"
	              width="100%" height="400" scrolling="auto" style="border: none" allowtransparency="allowtransparency" id="frameRequisitos">
	            </iframe>
	        	</div>
			</div>
		</div>
		</logic:equal>
		</html:form>
		<iframe width="174" height="189" name="gToday:normal:agenda.js"
			id="gToday:normal:agenda.js"
			src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
			scrolling="no" frameborder="0"
			style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
		</iframe>
		<script type="text/javascript">
		//pesta�as.-
		var tabView = new YAHOO.widget.TabView( 'pestanias' );
		var persona = false;
		var frameRequisitos = $('frameRequisitos');

		function selecPersona(){
			persona= true;
		}
		function irARequisitos() {
			var src = '';

			if(frameRequisitos.src == "")
			{
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=BonTasaRequisitos';
				src += '&process.accion=listar';
				src += '&process.idBonTasa=${ProcessForm.process.idBonTasa}';

				frameRequisitos.src = src;
			}
		}
		function atras(){
				window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=BonTasaProcess&process.idPersona=${ProcessForm.process.idPersona}&process.esPersona=${ProcessForm.process.esPersona}";
			}
		function guardar(){
			var accion= $('accion');
			accion.value="guardar";
		}
		function cancelar(){
			window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=BonTasaProcess&process.idPersona=${ProcessForm.process.idPersona}";
		}
		</script>