<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<div class="title">
  Concepto
</div>
<br>
<br>
<html:form action="/actions/abmAction.do?do=save&entityName=${param.entityName}&filter=true"
  styleId="cd"
>
  <table border="0">
    <tr>
      <th>
        Id:
      </th>
      <td>
        <asf:text name="AbmForm" property="entity.id" type="long" maxlength="3" readonly="true" />
        <html:hidden property="entity.concepto_id" value="${paramValue[0]}" />
      </td>
    </tr>
    <logic:present parameter="entity.id">
      
      <tr>
        <th>
          Per�odo:
        </th>
        <td>
          <b><bean:write name="AbmForm" property="entity.periodo" /> </b>
        </td>
      </tr>
    </logic:present>
    <logic:notPresent parameter="entity.id">
      <tr>
        <th>
          Per�odo:
        </th>
        <td>
          <asf:text property="entity.periodo" name="AbmForm" type="long" maxlength="4" />
        </td>
      </tr>
    </logic:notPresent>
<%--    <tr>--%>
<%--      <th>--%>
<%--        Partida Presupuestaria:--%>
<%--      </th>--%>
<%--      <td>--%>
<%--        <asf:select entityName="com.asf.gaf.hibernate.Iegreso" listCaption="codigo,denominacion" listValues="idiegreso"--%>
<%--          name="entity.partida_id" value="${AbmForm.entity.partida_id}"--%>
<%--        />--%>
<%--      </td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--      <th>--%>
<%--        Cuenta Extra Presupuestaria:--%>
<%--      </th>--%>
<%--      <td>--%>
<%--        <asf:select entityName="com.asf.gaf.hibernate.Plancta" listCaption="ejercicio,codigo,denominacion" listValues="idplancta"--%>
<%--          name="entity.extraPres_id" value="${AbmForm.entity.extraPres_id}" filter="imputable = 1"--%>
<%--          orderBy="ejercicio,codigo"/>--%>
<%--      </td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--      <th>--%>
<%--        Activo:--%>
<%--      </th>--%>
<%--      <td>--%>
<%--        <asf:select entityName="com.asf.gaf.hibernate.Plancta" listCaption="ejercicio,codigo,denominacion" listValues="idplancta"--%>
<%--          name="entity.activo_id" value="${AbmForm.entity.activo_id}" filter="imputable = 1"--%>
<%--          orderBy="ejercicio,codigo"/>--%>
<%--      </td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--      <th>--%>
<%--        Cuenta de Orden No Patrimonial:--%>
<%--      </th>--%>
<%--      <td>--%>
<%--        <asf:select entityName="com.asf.gaf.hibernate.Plancta" listCaption="ejercicio,codigo,denominacion" listValues="idplancta"--%>
<%--          name="entity.ordenNoPatr_id" value="${AbmForm.entity.ordenNoPatr_id}" filter="imputable = 1"--%>
<%--          orderBy="ejercicio,codigo"/>--%>
<%--      </td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--      <th>--%>
<%--        Nomenclador de Exposici�n:--%>
<%--      </th>--%>
<%--      <td>--%>
<%--        <asf:select entityName="com.asf.gaf.hibernate.Institucional" listCaption="ejercicio,codigo,denominacion" listValues="idinstitucional"--%>
<%--          name="entity.exposicion_id" value="${AbmForm.entity.exposicion_id}" filter="imputable = 1"--%>
<%--          orderBy="ejercicio,codigo"/>--%>
<%--      </td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--      <th>--%>
<%--        Nomenclador de Financiamiento:--%>
<%--      </th>--%>
<%--      <td>--%>
<%--        <asf:select entityName="com.asf.gaf.hibernate.Nomenclador" listCaption="ejercicio,codigo,denominacion" listValues="idnomenclador"--%>
<%--          name="entity.financiamiento_id" value="${AbmForm.entity.financiamiento_id}" filter="imputable = 1"--%>
<%--          orderBy="ejercicio,codigo"/>--%>
<%--      </td>--%>
<%--    </tr>--%>
   
  </table>
  <asf:security action="/actions/abmAction.do" access="do=save&entityName=${param.entityName}">
    <html:submit>
      <bean:message key="abm.button.save" />
    </html:submit>
  </asf:security>
  <html:cancel>
    <bean:message key="abm.button.cancel" />
  </html:cancel>
</html:form>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js">
</script>
