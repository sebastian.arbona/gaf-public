<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="title">Repositorio de Pagos a Cuenta</div>
<html:form action="/actions/process.do?do=process&processName=ConsultaPagoACuenta" styleId="ProcessForm">
	<html:hidden name="ProcessForm" property="process.cid"/>
	<html:hidden name="ProcessForm" property="process.action" value="buscar"/>
	
	<div class="grilla">
		<table width="100%">
			<tr>
				<th>Fecha Desde</th>
				<td colspan="3">
					<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
				</td>
			</tr>
			
			<tr>
			<tr>
				<th>Fecha Hasta</th>
				<td colspan="3">
					<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
				</td>
			</tr>
			<tr>
				<th>Incluir</th>
				<td>
					<html:checkbox name="ProcessForm" property="process.saldoCero"></html:checkbox>
				</td>
			</tr>
			
			<tr>
				<th colspan="2">
					<html:submit value="Buscar"></html:submit>
				</th>
			</tr>
		</table>
		
		<html:errors/>

		<display:table name="ProcessForm" property="process.arrlMovIngresosVarios"
 			export="true" id="reportTable" width="100%"
			requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process">
			
			<display:column title="Número Comprobante" property="deudoresVarios.numero" media="html"	sortable="true">
			
			</display:column>
			<display:column title="Nombre o Razón Social" property="deudoresVarios.persona.nomb12" sortable="true" />	
			<display:column title="CUIT" property="deudoresVarios.persona.cuil12" sortable="true" />
			<display:column title="Fecha Proceso" property="fechaProceso" sortable="true" />
			<display:column title="Fecha Generación" property="fechaProceso" sortable="true" />	
			<display:column title="Moneda" property="deudoresVarios.moneda.denominacion" sortable="true" />
			<display:column title="Banco Ingreso" property="deudoresVarios.caratula.banco.detaBa" sortable="true" />
			<display:column title="Cotización" property="cotizacion" sortable="true" />	
			<display:column title="Saldo" property="saldo" sortable="true" align="right" style="text-align:right;" />
			<display:column title="Importe" property="importe" sortable="true" align="right" style="text-align:right;" />
			
			<display:footer>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td style="text-align: rigth;"><b><bean:write name="ProcessForm" property="process.totalSaldo"/></b></td>
				<td style="text-align: rigth;"><b><bean:write name="ProcessForm" property="process.totalImporte"/></b></td>
			</tr>
			</display:footer>
 		</display:table>
	</div>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>