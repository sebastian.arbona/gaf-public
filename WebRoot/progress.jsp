<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@page import="com.asf.cred.business.ProgressStatus"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%
if(request.getParameter("reset")!=null){
	ProgressStatus.onProcess = true;
	ProgressStatus.iTotal = 0;
	ProgressStatus.iProgress = 0;
	ProgressStatus.abort = false;
}
%>
<html>
    <head>
    	<link rel="stylesheet" href="${pageContext.request.contextPath}/style/estilos_public_FTyC.css" type="text/css">
        <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/functions.js"></script>
        <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/ajax.js"></script>
        <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/timer.js"></script>
        <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
        <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
        <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>
        <script>
            var bolOK = true;
            function init(){
                var progress = new Update();
                if ( bolOK )
                    progress.tick( 30, "progress()" );
            }
            
            function progress(){
                var url ="${pageContext.request.contextPath}/servlet/Progress";
                retrieveURL(url, setProgreso);
            }
            
            function setProgreso( pProgreso ){
                var xmlDoc = new XMLDoc( pProgreso);
                var iTotal = Number(xmlDoc.docNode.getElements( "total" )[0].getText());
                var iProgreso= Number(xmlDoc.docNode.getElements( "progreso" )[0].getText());
                var estado = xmlDoc.docNode.getElements( "status" )[0].getText();
                var abort = xmlDoc.docNode.getElements( "abort" )[0].getText();

                if (abort == "true") {
                    window.close();
                    return;
                }
                
                if(estado.lastIndexOf('Terminada')!=-1){
                	window.close();
                }

				if (iTotal > 0) {				
					var p = Math.round((iProgreso / iTotal) * 100); 
                	
	                getElem( "progreso" ).innerHTML = p + "%";
    	            /*getElem( "total" ).innerHTML = iTotal;*/
        	        getElem("progreso").style.width = p + "%"; 
                	
            	    getElem( "status" ).innerHTML=estado;
            	}
            	
                if ( iTotal>0&&iProgreso>0&&iTotal <= iProgreso )
                    window.close();
            }
        </script>
        <title>Progreso</title>
        <style type="text/css">
        	.barra {
        		width: 400px;
        		padding: 2px;
        		background-color: white;
        		border: 1px solid black;
        		text-align: center;
        		margin: 10 auto;
        	}
        	
        	#progreso {
        		width: 0px;
        		background-color: #3c0;
        		text-align: center;
        	}
        	
        	#status {
        		margin: 10px;
        	}
        </style>
    </head>
    <body onload="init()" style="min-width: 400px">
    	<div>
    	<h3>Progreso</h3>
    	<div class="barra">
    		<div id="progreso">
    			
    		</div>
    	</div>
    	<div id="status"></div>
    	
	    </div>
    </body>
</html>
