<%@ page language="java"%>
<%@page import="com.asf.util.DirectorHelper"%>

<!DOCTYPE html "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
    <script language="JavaScript" type="text/javascript" src="scripts/functions.js"></script>
    <script language="JavaScript" type="text/javascript" src="scripts/prototype1.6.js"></script>
	
	<%pageContext.setAttribute("claveGMaps",DirectorHelper.getString("gmaps"));%>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
				
			<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=${claveGMaps}" type="text/javascript"></script>	
			<!-- PARA CUANDO SE PRUEBE LOCAL ><script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAjIPJmle28t-VIDWu9P_qvBS9J8P-_-wEZOo8dYGTXh_AlB8THBS4sR12Uy1VJNNPwqHPBFxpvKw"	type="text/javascript"></script-->
            <!-- PARA CUANDO SE PRUEBE YAVHIN><script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAjIPJmle28t-VIDWu9P_qvBR6tmuByN1FdjigMD8V6EroZwPzZhSUgAUKACOGuMyT53IMJHc4GNeyyg"	type="text/javascript"></script-->
        <script type="text/javascript">
            var geo = new GClientGeocoder();
            var lat = [];
            var lng = [];
            var map = null;
            var Accu = [];
            var marker=[];
            var miniMarker=[];
            var html=[];
            var precis=[];
            var iwn=0;
            var poet=[];
            var latitude=[];
            var longitude=[];
            var country=[];
            var bounds = new GLatLngBounds();

            var icon = new GIcon();
            icon.image = "http://labs.google.com/ridefinder/images/mm_20_red.png";
            icon.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
            icon.iconSize = new GSize(12, 20);
            icon.shadowSize = new GSize(22, 20);
            icon.iconAnchor = new GPoint(6, 20);

            function showAddress(){
               lat.length=0;
                var search = document.getElementById("search").value + ", Argentina";

                //			var search = direccion + ", Argentina";
                geo.getLocations(search, function (result){
                    // show status code
                    document.getElementById("message").innerHTML = "Resultados de la Búsqueda:<br/>";
                    if (result.Status.code == G_GEO_SUCCESS){
                        // Loop through the results, placing markers
                        for (var i=0; i<result.Placemark.length; i++){
                            lat[i]=result.Placemark[i].Point.coordinates[1];
                            lng[i]=result.Placemark[i].Point.coordinates[0];
                            html[i]="<small>"+(i+1)+": </small> "+ result.Placemark[i].address;

                            precis[i]=5*html[i].split(",").length;

                            var point=new GLatLng(lat[i],lng[i]);

                            marker[i]=new GMarker(point, {title:i+1});
                            map.addOverlay(marker[i]);
                            miniMarker[i]=new GMarker(point,icon);

                            bounds.extend(marker[i].getPoint());
                            var addr=result.Placemark[i].address;
                            if(result.Placemark[i].AddressDetails){
                                var iso=result.Placemark[i].AddressDetails.Country.CountryNameCode;
                            }
                            createIW(i+1,marker[i],lat[i],lng[i],addr,iso);

                            document.getElementById("message").innerHTML +=
                                "<a href=javascript:go("+lat[i]+","+lng[i]+","+precis[i]+","+i+")>"+ html[i]+"</a><br>";
                        }
                        //// center to first result
                        go(lat[0],lng[0],precis[0],0);
                    }else{
                        document.getElementById("message").innerHTML = "No se encontró la CALLE buscada. Por favor,<br> intente nuevamente.<br/>";
                    }
                });
            }



            function go(lat,lng,zoom,num){
                map.setZoom(zoom);
                map.panTo(new GLatLng(lat,lng));
                GEvent.trigger(marker[num],"click");
            }

            function initialize() {
                if (GBrowserIsCompatible()) {
                    map = new GMap2(document.getElementById("map_canvas"));
                    map.addMapType(G_NORMAL_MAP);
                    var start = new GLatLng(0, 0);
                    map.setCenter(start, 2);
                    map.addControl(new GLargeMapControl() );
                }
           }

            function createIW(name,pin,lati,longi,addr,iso){
                GEvent.addListener(pin, "click",
                function() {
                    poet[iwn]=addr;
                    latitude[iwn]=lati;
                    longitude[iwn]=longi;
                    country[iwn]=iso || "??";
                    var acercar = "";//"<a href=javascript:zoomin();>Acercar</a>"
                    pin.openInfoWindowHtml( addr + "<br><br>" + acercar );
                })
                iwn++;
            };

            function zoomin(){
                map.zoomIn();
            }
        </script>
    </head>
    <body onload="initialize()" onunload="GUnload()">

        <div id="map_canvas" style="width: 500px; height: 300px"></div>
        <div id="busqueda" style="position: absolute; top:40px; left:500px; width:400px; height:650px; text-align:left">
            <ul style="list-style:none">
                <!--				<li>-->
                <form onsubmit="showAddress(); return false" id="fmapa">
                    <input type="hidden" property="search" id="search" value=""/>
                    <!--						<input id="search" size="40" type="text" value="" />-->
<!--						<input type="submit" value="Enter" />-->
                </form>

                <!--				</li>-->
                <li>
                    <div id="message" style="width:400px; height:210px; background:white; overflow:auto; white-space:nowrap;">
                        Resultados de la Búsqueda:<br/>
                    </div>
                </li>

            </ul>

        </div>


    </body>
</html>
<script type="text/javascript">
    function buscarDireccion( direccion ){
        if( geo == undefined || geo == null ){
         	 window.parent.ocultarFrame(true);
        }
        else{
        	window.parent.ocultarFrame(false);
        	getElem( "search" ).value=direccion;
            showAddress();
        }
    }

</script>
