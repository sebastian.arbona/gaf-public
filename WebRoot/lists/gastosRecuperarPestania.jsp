<%@ page language="java" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">
	Gastos a Recuperar
</div>
<html:form action="/actions/process.do?do=process&processName=GastosRecuperarProcess" styleId="ProcessForm">
	<html:hidden property="process.accion" styleId="accion"/>
	
	<div class="grilla">
		
		<display:table name="ProcessForm" property="process.listado" export="true" id="reportTable" style="margin: 20px 0;"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=GastosRecuperarProcess">
      		<display:setProperty name="report.title" value="Gastos a Recuperar"/>
      		<display:caption>Proyecto ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12}</display:caption>
      		<display:column class="center" title="Fecha" property="factura.fecha" decorator="com.asf.displayDecorators.DateDecorator" paramId="process.idFactura"/>
  			<display:column title="Proveedor" property="factura.proveedor.persona.nomb12" sortable="true"/>
<%--      		<display:column title="Condicion" property="factura.condicion" sortable="true" />--%>
      		<display:column title="Pagado">
      				<logic:empty name="reportTable" property="factura.fechaPago">
      					No
      				</logic:empty>
      				
      				<logic:notEmpty name="reportTable" property="factura.fechaPago">
      					S�
      				</logic:notEmpty>
      		</display:column>
      		
      		<display:column title="Sucursal" property="factura.sucursal" sortable="true"/>
      		<display:column title="Nro Factura" property="factura.numero" sortable="true"/>
      		<display:column title="Expte Pago" property="factura.expepago" sortable="true"/>
      		<display:column title="Total" property="factura.importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
      		<display:column title="Moneda" property="factura.moneda.abreviatura" sortable="true"/>
<%--      		<display:column title="Tomador" property="credito.persona.nomb12" sortable="true"/>--%>
<%--      		<display:column title="Proyecto" property="credito.numeroAtencion" sortable="true"/>--%>
<%--      		<display:column title="Expediente" property="credito.expediente" sortable="true"/>--%>
      		<display:column title="Importe" property="importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
      		<display:column class="center" title="Fecha Cobranza Boleto" property="deudoresVarios.fechaPago" decorator="com.asf.displayDecorators.DateDecorator" sortable="true"/>
      		<display:column title="Nro Boleto" property="deudoresVarios.numero" sortable="true"/>
      		<display:column class="center" title="Fecha Emision Boleto" property="deudoresVarios.fechaEmision" decorator="com.asf.displayDecorators.DateDecorator" sortable="true"/>
      		<display:column title="Impactado Cta Cte">
      				<logic:notEqual name="reportTable" property="impactado" value="true">
      					No
      				</logic:notEqual>
      				<logic:equal name="reportTable" property="impactado" value="true">
      					S�
      				</logic:equal>
      		</display:column>
      		<display:column title="Observaciones" property="observaciones" sortable="true"/>
      	</display:table>
	</div>
</html:form>


<script>
//var form = $('ProcessForm');
var accion = $('accion');

function listar(){
	accion.value = 'listar';
	return true;
}

function crear() {
	accion.value = 'crear';
	return true;
}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>