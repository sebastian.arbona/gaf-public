<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div class="title">Panel de Administraci�n de Notificaciones</div>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" value="list" />
	
	<div>
		<table style="border: 2px solid rgb(204, 204, 204);" width="352" height="30">
			<tr>
				<th>Fecha Notificaci�n Inicial</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaDesde" 
						value="${process.fechaDesde}">
					</asf:calendar>
				</td>
			</tr>
			<tr>
				<th>Fecha Notificaci�n Final</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaHasta" 
						value="${process.fechaHasta}">
					</asf:calendar>
				</td>
			</tr>
			<tr>
				<th>N� Solicitud/Proyecto Relacionado</th>
				<td>
					<asf:selectpop name="process.numeroAtencion" 
						entityName="Objetoi" title="Credito" 
						caseSensitive="true" values="numeroAtencion"
						columns="Proyecto,Tomador"
						captions="numeroAtencion,persona.nomb12"
						cantidadDescriptors="1"
						value="${ProcessForm.process.numeroAtencion > 0 ? ProcessForm.process.numeroAtencion : null}"/>
				</td>
			</tr>
			<tr>
				<th>Titular</th>
				<td>
					<asf:selectpop name="process.idPersona" title="Persona"
						columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
						captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
						entityName="com.civitas.hibernate.persona.Persona" value="" />
				</td>
			</tr>
			<tr>
				<th>Emisor</th>
			<td>
				<html:text property="process.emisor" styleId="emisor"></html:text>
			</td>
		</tr>
		</table>
	</div>
	<br>
	<html:submit>Buscar</html:submit>
	<br>
	<br>
	<div class="grilla">
	
    	<display:table name="ProcessForm" property="process.notificaciones" id="notificacion" export="true" defaultsort="1"
    	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
    		
    		<display:setProperty name="report.title" value="Panel de Administraci�n de Notificaciones"></display:setProperty>
    		<display:column media="excel xml pdf" property="id" title="ID"/>
    		<display:column title="Nro Solicitud">
    			<logic:notEmpty name="notificacion" property="credito">
					${notificacion.credito.numeroAtencion}
				</logic:notEmpty>
    		</display:column>
			<display:column title="Expediente">
				<logic:notEmpty name="notificacion" property="credito">
					${notificacion.credito.expediente}
				</logic:notEmpty>
			</display:column>
			<display:column title="L�nea">
				<logic:notEmpty name="notificacion" property="credito">
					${notificacion.credito.linea.nombre}
				</logic:notEmpty>
			</display:column>	
            <display:column title="Fecha de la Notificaci�n" property="fechaCreada" decorator="com.asf.displayDecorators.DateDecorator"/>                
            <display:column title="Cuerpo de la Notificaci�n" property="observaciones" sortable="true"/>            
            <display:column title="Tipo de Aviso" property="tipoAvisoStr" sortable="true"/>
            <display:column title="Emisor" property="usuario" sortable="true"></display:column>
            <display:column title="Ver Notificaci�n" media="html" align="center">
            	<logic:notEqual value="gestionMora" property="tipoAviso" name="notificacion">
            		<a href="${pageContext.request.contextPath}/DownloadIt.do?do=pdf&boleto=false&tipoNoti=${notificacion.tipoAviso}&ids=${notificacion.id}">Ver PDF</a>
            	</logic:notEqual>
            </display:column>
            <display:column title="Archivo" media="html">
           		<logic:notEmpty name="notificacion" property="objetoiArchivo">
					<html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${notificacion.objetoiArchivo.id}')" property="objetoiArchivo.id">Bajar</html:button>
				</logic:notEmpty>					
			</display:column>
			<display:column title="Le�do">
			<logic:equal name="notificacion" property="leido" value="true">
				Si
			</logic:equal>
			<logic:equal name="notificacion" property="leido" value="false">
				No
			</logic:equal>
			</display:column>
			<display:column title="Direcci�n IP" property="direccionIP" sortable="true"/>  
    		<display:column title="Eliminar" media="html">
    			<asf:security action="/actions/process.do" access="do=process&processName=EliminarNotificacion">
    				<a href="${pageContext.request.contextPath}/actions/process.do?processName=EliminarNotificacion&do=process&process.idNotificacion=${notificacion.id}" onclick="return confirmDelete();">Eliminar</a>
    			</asf:security>
    		</display:column>
    	</display:table>
   </div>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
