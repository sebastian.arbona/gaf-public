<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Finalizaci�n de Cr�ditos</div>

<br>

<html:form action="actions/process.do?processName=${param.processName}&do=process" styleId="oForm">

    <html:hidden name="ProcessForm" property="process.action" styleId="action"/>
	<html:hidden property="process.idObjetoi" styleId="idObjetoi"/>
	<table>
	
		<tr>
      		<th>
        		L�nea de cr�dito:
      		</th>
      		
      		<td>
        		<asf:select entityName="com.nirven.creditos.hibernate.Linea" listCaption="id,nombre" listValues="id"
          		name="process.linea_id" value="${ProcessForm.process.linea_id}" orderBy="id" nullText="Todas" nullValue="true"/>
      		</td>
    	</tr>
	
		<tr>
      		
      		<th>Expediente:</th>
			
			<td>
				<asf:selectpop name="process.expediente" 
					entityName="DocumentoADE" title="Expediente" 
					value="${empty ProcessForm.process.expediente ? '' : ProcessForm.process.expediente}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI" 
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					cantidadDescriptors="2"/>
			</td>
		
		</tr>
		
		<tr>
		
			<th> N�mero de cr�dito </th>
			
			<td>
			
				<html:text maxlength="10" property="process.credito" name="ProcessForm"/>
			
			</td>
		
		</tr>
		
		<tr>
		
			<th>Per�odo de Fechas de Cancelaci�n</th>
			
			<td>
				<asf:calendar property="ProcessForm.process.desdeStr" value="${ProcessForm.process.desdeStr}" readOnly="false"/>
				<asf:calendar property="ProcessForm.process.hastaStr" value="${ProcessForm.process.hastaStr}" readOnly="false"/>
			</td>

		</tr>
		
	
	</table>
	
	<html:submit value="Buscar" onclick="setAction()"/>

</html:form>

<br><br>
<html:errors/>
<br/>
<div class="grilla">

	<display:table name="ProcessForm" property="process.listaCreditosDTO" export="true" id="reportTable" defaultsort="1" class="com.asf.cred.business.ObjetoiDTO">
	
		<display:setProperty name="report.title" value="Cr�ditos"></display:setProperty>  

		<display:column property="expediente" title="Expediente" sortable="true" />
		<display:column property="numeroAtencion" title="Nro. de Cr�dito" sortable="true"/>
		<display:column property="titular.nomb12" title="Nombre titular" sortable="true" />
		<display:column property="fechaCancelacionStr" title="Fecha de cancelaci�n" sortable="true" />
		
		<asf:security action="actions/process.do" access="processName=${param.processName}&do=process&action=FINALIZAR">
			<display:column media="html" title="Finalizar">
				<a href="javascript:void(0);" onclick="finalizar(${reportTable.idObjetoi});">
					Finalizar
				</a>
			</display:column>
		</asf:security>
	
	</display:table>
	
</div>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<script type="text/javascript">

function setAction() {
	var action = $('action');
	action.value = 'FILTRAR';		
}
function finalizar(id){
	var action = $('action');
	action.value = 'FINALIZAR';
	var idObjetoi = $('idObjetoi');
	idObjetoi.value = id;
	var form = $('oForm');
	form.submit();
}
</script>