<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br>
	<br>
	<logic:notEmpty name="ProcessForm" property="process.comportamientos">
		<display:table name="ProcessForm" property="process.comportamientos"
			pagesize="10" id="reportTable" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:caption>Proyecto ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12}</display:caption>
			<display:column title="Comportamiento" property="nombreComportamiento" sortable="true" />
			<display:column title="Desde" property="fechaStr" sortable="true" />
			<display:column title="Hasta" property="fechaHastaStr" sortable="true" />
			<display:column title="Observaciones" property="observaciones" sortable="true" />
			<display:column title="Area Responsable" property="areaResponsable"/>
		</display:table>
	</logic:notEmpty>
	<logic:empty name="ProcessForm" property="process.comportamientos">
        El Cr�dito : ${ProcessForm.process.objetoi.id} - ${ProcessForm.process.objetoi.numeroCredito} no tiene Comportamientos de Pago.
    </logic:empty>
</html:form>