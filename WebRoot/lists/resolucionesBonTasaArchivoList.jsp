<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />

<body class=" yui-skin-sam">
	<div class="title">
		Administración de Documentos
	</div>
	<br>
	<br>
		
<div><html:errors /></div>

	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"> </script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"> </script>
	
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="nuevo" />
		<html:hidden property="process.estado_id" styleId="estado_id"/>
		<html:hidden property="process.idBonTasa" styleId="idBonTasa"/>
		<html:submit onclick="getElem('accion').value='nuevoDocumentoResolucion';">
			<bean:message key="abm.button.new" />
		</html:submit>
		<input type="button" value="Volver" onclick="cancelar();"/>
	</html:form>
	<br>
	<display:table name="ProcessForm" property="process.bontasEestadoArchivos" export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listarArchivos">
		<display:setProperty name="report.title"
			value="Administración de Documentos"></display:setProperty>
		<display:column property="archivo.nombre" title="Nombre" />
		<display:column property="descripcion" title="descripcion" />
		<display:column title="Bajar" media="html">
			<html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargarArchivoJudicial&id=${reportTable.id}')" property="process.objetoiArchivo.id">Bajar</html:button>
		</display:column>
		<display:column title="Eliminar" media="html">
				<a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>
		</display:column>
	</display:table>

	<script type="text/javascript">

	var idSolicitud = document.getElementById("idSolicitud");

function eliminar(id){
		var url = '';

		var idBonTasa = getElem('idBonTasa').value;
		var estado_id = getElem('estado_id').value;
		/*
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=eliminarDocumentoResolucion&';
		url += 'process.idDocumentoResolucion=' + id;
		url += '&process.idSolicitud=' + idSolicitud.value;

       */
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&processName=ResolucionesBonTasa&process.estado_id='+estado_id;
		url += '&process.bontasaestadoarchivo_id='+ id + '&process.accion=eliminarResolucionArchivo&process.idBonTasa='+idBonTasa;

		window.location = url;
}

function cancelar(){
	getElem('accion').value = 'listResolucion';
	var form = $('oForm');
	form.submit();
}
// fin modificar.-   

//fin irARequisitos.-

</script>