<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>



<div class="title">Administraci�n de Reportes</div>
<br><br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>

<div class="grilla">

<display:table name="ListResult" property="result" defaultsort="1" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
<display:setProperty name="report.title" value="Administraci�n de Reportes"></display:setProperty>

    <display:column title="C�digo Reporte" sortable="true" sortProperty="idreporte" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" paramId="entity.idreporte" paramProperty="idreporte" property="idreporte"/>  
    <display:column title="T�tulo" property="titulo" sortable="true"/>
    <display:column title="Sub-T�tulo" property="subtitulo" sortable="true" />
    <display:column title="SQL" property="sql" sortable="true" />

    <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.idreporte" paramProperty="idreporte">Eliminar</display:column>
    </asf:security>
    
</display:table>
</div>