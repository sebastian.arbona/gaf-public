<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" value="nuevo" />
  <html:hidden property="process.persona.id" styleId="idPersonaTitular" value="${ProcessForm.process.persona.id}" />
  <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=guardar">
    <html:submit>
      <bean:message key="abm.button.new" />
    </html:submit>
  </asf:security>
  <br>
  <br>
  <logic:notEmpty name="ProcessForm" property="process.referencias">
    <display:table name="ProcessForm" property="process.referencias" pagesize="5" id="reportTable" export="true"
      requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar"
    >
      <display:caption>Datos Laborales de:  ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12}</display:caption>
      <display:column title="ID" media="html" sortable="true">
        <a href="javascript: modificar( ${ reportTable.id }  );">${ reportTable.id }</a>
      </display:column>
      <display:column title="Tipo de Relaci�n" property="tipoRelacionLaboral.denominacion" sortable="true" />
      <display:column title="Detalle" property="detalle" sortable="true" />
      <display:column title="Desde" property="fechaDesdeStr" sortable="true" />
      <display:column title="Hasta" property="fechaHastaStr" sortable="true" />
      <display:column title="Empleador" property="empleador" sortable="true" />
      <display:column title="CUIT Empleador" property="cuitEmpleador" sortable="true" />
      <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=eliminar">
        <display:column title="Eliminar" media="html">
          <a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>
        </display:column>
      </asf:security>
    </display:table>
  </logic:notEmpty>
  <logic:empty name="ProcessForm" property="process.referencias">
        La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no tiene Referencias Laborales o Profesionales.
    </logic:empty>
</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js" id="gToday:normal:agenda.js"
  src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0"
  style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"
>
</iframe>
<script language='javascript'>
function modificar(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.persona.id=${ProcessForm.process.persona.id}&';
	url += 'process.referencia.id=' + id;

	window.location = url;
}// fin modificar.-

function eliminar(id) {
	if (confirmDelete()) {
		var url = '';

		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=eliminar&';
		url += 'process.persona.id=${ProcessForm.process.persona.id}&';
		url += 'process.referencia.id=' + id;

		window.location = url;
	}
}//fin eliminar.-

Event.observe(window, 'load', function() {
	this.name = "Datos Laborales";
	parent.pop(this);
});
</script>