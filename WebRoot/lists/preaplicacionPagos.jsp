<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">
	Preaplicaci�n de los Pagos
</div>
<br>
<bean:define id="lista" name="ProcessForm" property="process.lista"
	type="String[]" />
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.idDetalleArchivo"
		styleId="idDetalleArchivo" />
	<html:hidden property="process.accion" styleId="accion" />
	<html:hidden property="process.tcomp12" styleId="tcomp12" />
	<html:hidden property="process.idBoletoList" styleId="idBoletoList" />
	<html:hidden property="process.idDetalleRecaudacionList"
		styleId="idDetalleRecaudacionList" />
	<html:hidden property="process.idFechaCobranza" styleId="idFechaCobranza" />
	<html:hidden property="process.idcaratula" styleId="idcaratula" />
	<html:hidden property="process.boletosXML" styleId="boletosXML"/>
	<html:hidden property="process.idBoleto" styleId="idBoleto"/>
	<html:hidden property="process.idDetalleRecaudacion" styleId="idDetalleRecaudacion"/>
	<html:hidden property="process.detalleRecaudacion.id" styleId="detalleRecaudacionId" />
	
	<logic:notEmpty name="ProcessForm" property="process.mensaje">
		<div style="font-weight: bold">${ProcessForm.process.mensaje}</div>
	</logic:notEmpty>
	
	<table id="tabla" class="tabla_chica">
		<tr>
			<th style="font-size:10px;">
				Proyecto
			</th>
			<th style="font-size:10px;">
				Titular
			</th>
			<th style="font-size:10px;">
				Importe Total Pagado
			</th>
			<th style="font-size:10px;">
				Fecha Pago
			</th>
			<th style="font-size:10px;">
				Cuota
			</th>
			<th style="font-size:10px;">
				Total Aplicado
			</th>
			<th style="font-size:10px;">
				Capital
			</th>
			<th style="font-size:10px;">
				Comp.
			</th>
			<th style="font-size:10px;">
				Morat.
			</th>
			<th style="font-size:10px;">
				Punit.
			</th>
			<th style="font-size:10px;">
				Gastos Adm.
			</th>
			<th style="font-size:10px;">
				Gastos Rec.
			</th>
			<th style="font-size:10px;">
				Multas
			</th>
			<th style="font-size:10px;">
				Saldo Pte Aplicar
			</th>
			<th style="font-size:10px;">
				Elim.
			</th>
			<th style="font-size:10px;">
				Redist.
			</th>
		</tr>
	</table>
	<table id="idBoleto_Detalle">
		<tr>
			<th>
				idBoleto
			</th>
			<th>
				idDetalleRecaudacion
			</th>
		</tr>
	</table>
	<table id="listaTable">
		<tr>
			<td style="font-size:10px;">
				<select name="process.lista" id="lis" multiple="multiple"></select>
			</td>
		</tr>
	</table>
	<table id="medioPago">
		<tr>
			<th>
				Medio de Pago:
			</th>
			<td>
				<asf:select name="process.idmediopago"
					entityName="com.asf.gaf.hibernate.Mediopago"
					listCaption="getDenominacion" listValues="getId" nullValue="true"
					nullText="SELECCIONE MEDIO"
					value="${ProcessForm.process.idmediopago}"
					attribs="onChange='mostrarMedio(this.value);'"
					filter="id like '%' order by id" />
			</td>
		</tr>
		<tr id="impo">
			<th>
				Importe:
			</th>
			<td>
				<asf:text name="ProcessForm" property="process.importe" id="importe"
					type="decimal" maxlength="11" />
			</td>
		</tr>
		<%--		<tr id="vuelto" style="display:none"><th>Vuelto:</th><td><asf:text name="ProcessForm" property="process.difTotal" id="difTotal" type="decimal" maxlength="11" readonly="true"/></td></tr> --%>
	</table>
	<table id="detalle">
		<tr>
			<th>
				C�digo
			</th>
			<th>
				Tipo Medio
			</th>
			<th>
				Denominaci�n
			</th>
			<th>
				Entidad Emisora
			</th>
			<th>
				Nro Comprobante
			</th>
			<th>
				Fecha Emisi�n
			</th>
			<th>
				Fecha Vencimiento
			</th>
			<th>
				Cuotas
			</th>
			<th>
				Plan
			</th>
			<th>
				Autorizaci�n
			</th>
			<th>
				Importe
			</th>
			<th>
				Acci�n
			</th>
		</tr>
	</table>
	<br />
	<div style="float: left">
		<input type="button" onclick="volver();" value="Volver">
	</div>
	<div style="float: right">
		<html:submit onclick="return guardar();">Guardar</html:submit>
		<input type="button" value="Salir" 
		onclick="javascript:window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&filter=true&process.fenv12Str=${ProcessForm.process.idFechaCobranza}'"/>
	</div>
	<div style="clear:both"/>
</html:form>
<script type="text/javascript">

var importeTotal = 0.0;

function cargarBoletosMetodoAutomatico(){
	if($('boletosXML').value == null || $('boletosXML').value == ""){
		window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
		"width=450, height=180");
	    var url="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=generarBoletosMetodoAutomatico&idCaratula="+$('idcaratula').value;
	    retrieveURL(url, boletosResponse);
	}else{
		boletosResponse($('boletosXML').value);
	}
}

function boletosResponse(responseText){
	if(responseText == "<Objects></Objects>"){
		alert('No hay deuda en los cr�ditos.');
		return;
	}
	$('boletosXML').value = responseText;
	var objetos = parseXml(responseText);
	var boletosNuevos = objetos.Objects.Objetoi;
	var boleto;
	var totalBoletos = 0, totalDeuda = 0, totalCapital = 0, totalComp = 0, totalMorat = 0, totalPunit = 0,
		totalGastos = 0, totalGastosRec = 0, totalMultas = 0, totalSaldo = 0, totalRestante = 0;
	
	if(boletosNuevos.length != null){
		for(var i = 0; i < boletosNuevos.length; i++){
			boleto = boletosNuevos[i];
		    cargarItem(boleto);
		    
		    totalBoletos += Math.round(new Number(boleto.importeRaw) * 100) / 100;
		    
		    var cuotas = boleto.Cuota;
		    if (cuotas != null) {
				var length = cuotas.length;
				var cuota;
				
				if(cuotas.length == null){
					length = 1;
					cuota = cuotas;
				}
				
				for(var i2 = 0; i2 < length; i2++){
					if (cuota == null) {
						cuota = cuotas[i2];
					}
					
				    totalCapital += Math.round(new Number(cuota.capitalRaw) * 100) / 100;
				    totalComp += Math.round(new Number(cuota.compensatorioRaw) * 100) / 100;
				    totalMorat += Math.round(new Number(cuota.moratorioRaw) * 100) / 100;
				    totalPunit += Math.round(new Number(cuota.punitorioRaw) * 100) / 100;
				    totalGastos += Math.round(new Number(cuota.gastosRaw) * 100) / 100;
				    totalGastosRec += Math.round(new Number(cuota.gastosRecRaw) * 100) / 100;
				    totalMultas += Math.round(new Number(cuota.multasRaw) * 100) / 100;
				    totalRestante += Math.round(new Number(cuota.saldoPendienteRaw) * 100) / 100;
				    
				    cuota = null;
				}
		    } else {
		    	totalRestante += Math.round(new Number(boleto.importeRaw) * 100) / 100;
		    }
		}    		
	}else{
		boleto = boletosNuevos;
	    cargarItem(boleto);
	    
	    totalBoletos += Math.round(new Number(boleto.importeRaw) * 100) / 100;
	    
	    var cuotas = boleto.Cuota;
	    if (cuotas != null) {
			var length = cuotas.length;
			var cuota;
			
			if(cuotas.length == null){
				length = 1;
				cuota = cuotas;
			}
			
			for(var i2 = 0; i2 < length; i2++){
				if (cuota == null) {
					cuota = cuotas[i2];
				}
				
			    totalCapital += Math.round(new Number(cuota.capitalRaw) * 100) / 100;
			    totalComp += Math.round(new Number(cuota.compensatorioRaw) * 100) / 100;
			    totalMorat += Math.round(new Number(cuota.moratorioRaw) * 100) / 100;
			    totalPunit += Math.round(new Number(cuota.punitorioRaw) * 100) / 100;
			    totalGastos += Math.round(new Number(cuota.gastosRaw) * 100) / 100;
			    totalGastosRec += Math.round(new Number(cuota.gastosRecRaw) * 100) / 100;
			    totalMultas += Math.round(new Number(cuota.multasRaw) * 100) / 100;
			    totalRestante += Math.round(new Number(cuota.saldoPendienteRaw) * 100) / 100;
			    
			    cuota = null;
			}
	    } else {
	    	totalRestante += Math.round(new Number(boleto.importeRaw) * 100) / 100;
	    }
	}
	
	cargarTotal(totalBoletos, totalCapital, totalComp,
			totalMorat, totalPunit, totalGastos, totalGastosRec, totalMultas, totalRestante);
}

function cargarTotal(totalBoletos, totalCapital, totalComp,
		totalMorat, totalPunit, totalGastos, totalGastosRec, totalMultas, totalRestante) {
	
	var table = getElem("tabla");
	var posicion = table.rows.length;
    var fila = table.insertRow( posicion );
	var cell = fila.insertCell(0);
	cell.innerHTML = "TOTAL";
	cell = fila.insertCell(1);
	cell = fila.insertCell(2);
	cell.innerHTML = formatMiles(Math.round(totalBoletos * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(3);
	cell = fila.insertCell(4);
	cell = fila.insertCell(5);
 	cell = fila.insertCell(6);
	cell.innerHTML = formatMiles(Math.round(totalCapital * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(7);
	cell.innerHTML = formatMiles(Math.round(totalComp * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(8);
	cell.innerHTML = formatMiles(Math.round(totalMorat * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(9);
	cell.innerHTML = formatMiles(Math.round(totalPunit * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(10);
	cell.innerHTML = formatMiles(Math.round(totalGastos * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(11);
	cell.innerHTML = formatMiles(Math.round(totalGastosRec * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(12);
	cell.innerHTML = formatMiles(Math.round(totalMultas * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(13);
	cell.innerHTML = formatMiles(Math.round(totalRestante * 100) / 100);
	cell.style.textAlign = "right";
	cell = fila.insertCell(14);
	cell = fila.insertCell(15);
}

function cargarItem(credito){
    var table = getElem("tabla");
    var posicion = table.rows.length;
    var posicionPago = buscarSiguienteIndice();
    
    var fila = table.insertRow( posicion );
    
    var c = 0;
    var cell = fila.insertCell(c++);
	cell.innerHTML = credito.numeroAtencion;
	cell = fila.insertCell(c++);
	cell.innerHTML = credito.Persona.nombre;
	cell = fila.insertCell(c++);
	cell.innerHTML = credito.importePago;
	cell = fila.insertCell(c++);
	cell.innerHTML = credito.fechaPago;
	
	var cuotas = credito.Cuota;
	
	if (cuotas == null) {
		// no tiene deuda
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell = fila.insertCell(c++);
		cell.innerHTML = credito.importePago;
		cell = fila.insertCell(c++);
		if (${ProcessForm.process.guardado} == true && credito.idDetallePadre != null){
			cell.innerHTML = "<input type=\"button\" value=\"...\" onclick=\"quitarTabla(" + (posicionPago) + ");\"></input>";
		}
		cell=fila.insertCell(c++);
		cell.innerHTML = "<input type=\"button\" value=\"...\" onclick= \"redistribuir(" + (posicionPago) + ");\"></input>";
	} else {
		var length = cuotas.length;
		var cuota;
		
		if(cuotas.length == null){
			length = 1;
			cuota = cuotas;
		}
	
		for(var i = 0; i < length; i++){
			if (i > 0) {
				// a partir de la segunda cuota, agregar nueva fila
				posicion = table.rows.length;
				fila = table.insertRow(posicion);
				for (var caux = 0; caux < 4; caux++) { // 4 celdas vacias
					fila.insertCell(caux);
				}
			}
			
			c = 4;
			if (cuota == null) {
				cuota = cuotas[i];
			}
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.numero;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.aplicado;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.capital;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.compensatorio;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.moratorio;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.punitorio;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.gastos;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.gastosRec;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.multas;
			cell = fila.insertCell(c++);
			cell.innerHTML = cuota.saldoPendiente;
			cell = fila.insertCell(c++);
			if (i == 0 && ${ProcessForm.process.guardado} == true && credito.idDetallePadre != "null"){
				cell.innerHTML = "<input type=\"button\" value=\"...\" onclick=\"quitarTabla(" + (posicionPago) + ");\"></input>";
			}
			cell=fila.insertCell(c++);
			if (i == 0) {
				cell.innerHTML = "<input type=\"button\" value=\"...\" onclick= \"redistribuir(" + (posicionPago) + ");\"></input>";
			}
			
			cuota = null;
		}
	}
    
// 	cell=fila.insertCell(4);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.importe) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(5);
// 	cell.innerHTML = boleto.fechaPago;
// 	cell=fila.insertCell(6);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.deuda) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(7);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.capital) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(8);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.compensatorio) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(9);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.moratorio) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(10);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.punitorio) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(11);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.gastos) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(12);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.gastosRec) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(13);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.multas) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(14);
// 	cell.innerHTML = formatMiles(Math.round((new Number(boleto.deuda) - new Number(boleto.importe)) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(15);
// 	cell.innerHTML = formatMiles(Math.round(new Number(boleto.importeRestante) * 100) / 100);
// 	cell.style.textAlign = "right";
// 	cell=fila.insertCell(16);
// 	if (${ProcessForm.process.guardado} == true && boleto.idPadre != "null"){
// 		cell.innerHTML = "<input type=\"button\" value=\"...\" onclick=\"quitarTabla(" + (posicion - 1) + ");\"></input>";
// 	}
// 	cell=fila.insertCell(17);
// 	cell.innerHTML = "<input type=\"button\" value=\"...\" onclick= \"redistribuir(" + (posicion - 1) + ");\"></input>";

	var table2 = getElem("idBoleto_Detalle");
    var posicion2 = table2.rows.length;
    var fila2 = table2.insertRow( posicion2 );
	var cell2 = fila2.insertCell(0);
	cell2.innerHTML = credito.idBoleto;
	cell2 = fila2.insertCell(1);
	cell2.innerHTML = credito.idDetalleRecaudacion;
	
	var boletoAProcesar = credito.idBoleto +' $' + credito.importeRaw + ' - NRO. PROYECTO: ' + credito.numeroAtencion + ' - TITULAR: ' + credito.Persona.nombre + '  - CUIL/CUIT - ' + credito.Persona.cuil;
	var opt = getElem("lis").appendChild(document.createElement('option'));
    opt.appendChild(document.createTextNode(boletoAProcesar));
    opt.setAttribute('value', boletoAProcesar);

    var medioPago = $( 'process.idmediopago' );
    medioPago.value = 1;
    medioPago.selectedIndex = 1;
<%--	var tipoMedio.value = 'E';--%>

	var detalle = getElem("detalle");
	var posicion3 = detalle.rows.length;
	var fila3 = detalle.insertRow( posicion3 );
	var cell3 = fila3.insertCell(0);
	cell3.innerHTML = medioPago.value;
	cell3 = fila3.insertCell(1);
	cell3.innerHTML = "";
	cell3=fila3.insertCell(2);
	var denominacion = medioPago.options[ medioPago.selectedIndex ].text;
	cell3.innerHTML = denominacion;
	cell3=fila3.insertCell(3);
	cell3=fila3.insertCell(4);
	cell3=fila3.insertCell(5);
	cell3=fila3.insertCell(6);
	cell3=fila3.insertCell(7);
	cell3=fila3.insertCell(8);
	cell3=fila3.insertCell(9);
	cell3 = fila3.insertCell(10);
	cell3 = fila3.insertCell(11);
	cell3.innerHTML = parseFloat( credito.importeRaw ).toFixed(2);
	importeTotal += parseFloat( credito.importeRaw ).toFixed(2);
}

function buscarSiguienteIndice() {
	var table = getElem("tabla");
	var siguiente = 0;
	for (var r = 1; r < table.rows.length; r++) {
		var row = table.rows[r];
		var content = row.cells[0].innerHTML;
		if (content != null && content != '' && content != 'TOTAL') {
			siguiente++;
		}
	}
	return siguiente;
}

function buscarFilaPago(pago) {
	var table = getElem("tabla");
	var pagoActual = 0;
	for (var r = 1; r < table.rows.length; r++) {
		var row = table.rows[r];
		var content = row.cells[0].innerHTML;
		if (content != null && content != '') {
			if (pagoActual == pago) {
				return r;
			}
			pagoActual++;
		}
	}
	return -1;
}

function cargarCobroPagos(){
	var tabla = $( 'detalle' );
	var rows = tabla.rows.length;
	var fila;
	var pos;
	var cell;
	for( i = 1; i < rows; i++ ){       						
		pos = i - 1;
		fila = tabla.getElementsByTagName("tr")[i];
		cell = fila.getElementsByTagName("td")[0];
		addHidden(cell,"process.cobroPagos["+pos+"].mediopagoId",cell.innerHTML,"");
		cell = fila.getElementsByTagName("td")[2];
		addHidden(cell,"process.cobroPagos["+pos+"].idcaratula",$('idcaratula').value,"");
		cell = fila.getElementsByTagName("td")[11]
		var impo = cell.innerHTML;
		if( fila.getElementsByTagName("td")[1].innerHTML == 'E' )
				impo = cell.innerHTML;
		addHidden(cell,"process.cobroPagos["+pos+"].importe",impo,"");          
    }
}

function quitarTabla(i) {
	if(confirm('Est� seguro que desea eliminiar?') == false){
		return;
	}
	
	var filaPago = buscarFilaPago(i);
	
	if (filaPago == -1) {
		return;
	}
	
	var table = getElem("tabla");
	var nroProy;
	do {
		table.deleteRow(filaPago);
		if (filaPago == table.rows.length) {
			break;
		}
		
		nroProy = table.rows[filaPago].cells[0].innerHTML;
	} while (nroProy == null || nroProy == '');
	
	var pago = 0;
	for (var j = 1; j < table.rows.length; j++) {
		var cel = table.rows[j].cells[14];
		var content = table.rows[j].cells[0].innerHTML;
		if (content != null && content != '') {
			cel.innerHTML = "<a onclick=\"quitarTabla(" + pago + ");\">Quitar</a>";
			pago++;
		}
	}
	
// 	tabla.deleteRow(i + 1); // la 0 es la cabecera
// 	for (var j = 1; j < tabla.rows.length; j++) {
// 		var cel = tabla.rows[j].cells[16];
// 		cel.innerHTML = "<input type=\"button\" value=\"...\" onclick=\"quitarTabla(" + (j - 1) + ");\"></input>";
// 	}

	var boletoTable = getElem("idBoleto_Detalle");
	var cel = boletoTable.rows[i + 1].cells[0];
	$('idBoleto').value = cel.innerHTML;
	cel = boletoTable.rows[i + 1].cells[1];
	$('idDetalleRecaudacion').value = cel.innerHTML; 
	boletoTable.deleteRow(i + 1);
	var detalle = getElem("detalle");
	detalle.deleteRow(i + 1);
	var lista = getElem("lis");	
	lista.selectedIndex = i;
	quitar();
	$('accion').value = 'eliminar';
	$('oForm').submit();
}

function quitar(){
	var lista = getElem("lis");						
	var i = lista.selectedIndex;	    	                                                                           
	if( navigator.appName != "Netscape" )
    	lista.removeChild( lista.children( i ) ); 
	else{    	
        lista.options[i] = null;
	}
}

function volver(){
	window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConciliacionRecaudaciones" + 
					"&process.idCaratula=${ProcessForm.process.idcaratula}";
}
function guardar(){
	$('tcomp12').value = getElem("tabla").rows.length - 1;
	$('accion').value = 'guardar';
	var table = getElem('idBoleto_Detalle');
	var ids = "";
	var idDetalleRecaudacion = "";
	var cel;
	for (var j = 1; j < table.rows.length; j++) {
		cel = table.rows[j].cells[0];
		ids += cel.innerHTML + "-";
		cel = table.rows[j].cells[1];
		idDetalleRecaudacion += cel.innerHTML + "-"; 
	}
	$('idBoletoList').value = ids;
	$('idDetalleRecaudacionList').value = idDetalleRecaudacion;
	var lista = getElem("lis");
	for (i = 0; i < lista.length; i++) {
		lista.options[i].selected = true;
	}
	cargarCobroPagos();
	return true;
}
function redistribuir(i){
	var table = getElem('idBoleto_Detalle');
	var cel = table.rows[i + 1].cells[1];
	var idDetalleRecaudacion = cel.innerHTML; 
	var form = $('oForm');
	form.action = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=RedistribuirDetalleRecaudacion";
	$('accion').value = "cargar";
	$('detalleRecaudacionId').value = idDetalleRecaudacion;
	form.submit();
	/*
	window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=RedistribuirDetalleRecaudacion" +
					"&process.accion=cargar" + 
					"&process.idCaratula=" + $('idcaratula').value + 
					"&process.boletosXML=" + $('boletosXML').value +
					"&process.detalleRecaudacion.id=" + idDetalleRecaudacion;
	*/
}

function formatMiles(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? ',' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}
</script>
<script>
ocultar("idBoleto_Detalle");
ocultar("detalle");
ocultar("listaTable");
ocultar("medioPago");
if($('accion') != null && $('accion').value == 'generarBoletos'){
	cargarBoletosMetodoAutomatico();
}

</script>