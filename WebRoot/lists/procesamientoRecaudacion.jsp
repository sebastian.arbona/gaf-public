<%@ page language="java"%>
<%@ page import="com.asf.util.DateHelper" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<script type="text/javascript">
function Nuevo(val){
    if( val != '' ){
         getElem("cd").action=getElem("cd").action.toString().replace(getElem("cd").action,"actions/abmActionCaratula.do?do=newEntity&entityName=Caratula&cobroPagos&consultaBoletos&paramName[0]=fechaEnvioStr&paramComp[0]==&paramValue[0]="+val);
         getElem("cd").submit();
     }
}

function Consultar(){
 	getElem("cd").action = getElem("cd").action.toString().replace("CierreCaja", "CobroPagos");
}
</script>


<div class="title">Procesamiento de Recaudacion</div>
<br><br>

<html:form styleId="cd" action="/actions/process.do?do=process&processName=${param.processName}&filter=true">
<html:errors/>
<table border="0">

<tr><th>Fecha:</th>
<td>
<logic:empty name="ProcessForm" property="process.fenv12Str" >
	<asf:calendar property="ProcessForm.process.fenv12Str" value="${ProcessForm.process.fenv12Str}"/>
</logic:empty>
<logic:notEmpty name="ProcessForm" property="process.fenv12Str" >
	<asf:calendar property="process.fenv12Str" value="${ProcessForm.process.fenv12Str}"/>
</logic:notEmpty>
</td></tr>

</table>
	<input type="button" onclick="Nuevo(getElem('process.fenv12Str').value);" value="Nuevo" />
    <html:submit onclick="Consultar();" value="Consultar"/>
   
</html:form>
<br>
<div class="grilla">
<display:table name="ProcessForm" property="process.result" export="true" id="reportTable" defaultsort="1" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&filter=true">
    <display:setProperty name="report.title" value="Administración de Cobro con Valores"/>
    <display:caption>
     Fecha: ${reportTable.fechaEnvioStr}
    </display:caption>
 
 	<display:column title="Grupo" property="grupo" sortable="true"/>
    <display:column title="Ente Recaudador" property="banco.detaBa" sortable="true" />
    <display:column title="Fecha Cobranza" property="fechaCobranzaStr" sortable="true" />
    <display:column title="Total Comprobantes" property="comprobantes" sortable="true" />
    <display:column title="Importe Total" property="importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
   
  
    <asf:security action="/actions/process.do" access="do=process&processName=CierreCaja">
    <display:column media="html" title="Cierre de Caja" >
        <logic:equal name="reportTable" property="actualizada" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CierreCaja&process.idcaratula=${reportTable.id}&process.fenv12Str=${reportTable.fechaEnvioStr}&filter=true&process.cobro=true" onclick="return confirmCierre() && actualizar();">Cierre de Caja</a>
       </logic:equal>
	   <logic:notEqual name="reportTable" property="actualizada" value="0">
	  	 ${reportTable.fechaActualizacionStr}
	   </logic:notEqual>
	</display:column>
    </asf:security> 
<%--    <asf:security action="/actions/process.do" access="do=process&processName=RegistrarPago">--%>
    <display:column media="html" title="Registrar Cobros" >
        <logic:equal name="reportTable" property="actualizada" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=RegistrarCobroPagos&process.idcaratula=${reportTable.id}&process.fenv12Str=${reportTable.fechaEnvioStr}&filter=true">Registrar Cobros</a>
       </logic:equal>
	</display:column>
<%--    </asf:security> --%>
<%--    <asf:security action="/actions/process.do" access="do=process&processName=ConsultarLote">--%>
    <display:column media="html" title="Consultar Lotes" >
        <logic:equal name="reportTable" property="actualizada" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConsultarLotes&process.idCaratula=${reportTable.id}">Consultar Lotes</a>
       </logic:equal>
       <logic:notEqual name="reportTable" property="actualizada" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConsultarLotes&process.idCaratula=${reportTable.id}&consultaLotes">Consultar Lotes</a>
       </logic:notEqual>
	</display:column>
<%--    </asf:security> --%>
<%--    <asf:security action="/actions/process.do" access="do=process&processName=ConsultarBoleto">--%>
    <display:column media="html" title="Consultar Boletos" >
<%--        <logic:equal name="reportTable" property="actu12" value="0">--%>
      	 <a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=load&entityName=Caratula&entity.id=${reportTable.id}&consultaBoletos">Consultar Boletos</a>
<%--       </logic:equal>--%>
<%--       <logic:notEqual name="reportTable" property="actu12" value="0">--%>
<%--      	 <a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=load&entityName=Caratula&entity.id=${reportTable.id}">Consultar Boletos</a>--%>
<%--       </logic:notEqual>--%>
	</display:column>
<%--    </asf:security>--%>
  
<%--    <asf:security action="/actions/process.do" access="do=delete&entityName=${param.entityName}">--%>
      <display:column media="html" title="Eliminar" >
    	<logic:equal name="reportTable" property="actualizada" value="0">
    		<a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=delete&entityName=Caratula&entity.id=${reportTable.id}&entity.fechaEnvioStr=${reportTable.fechaEnvioStr}&filter=true" onclick="return confirmDelete();" >Eliminar</a>
		</logic:equal>
	 </display:column>
<%--	</asf:security>--%>

</display:table>
</div>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
