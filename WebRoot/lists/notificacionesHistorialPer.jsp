<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
  	<html:hidden name="ProcessForm" property="process.idPersona" styleId="idPer" value="${ProcessForm.process.idPersona}" />
    <br/>   
	
	<div>
		<input type="button" onclick="nuevaNotificacion();" value="Nueva"></input>
	</div>
	
	<div>
	
    	<display:table name="ProcessForm" property="process.notificaciones" id="notificacion" export="true" defaultsort="1"
    	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=verNotifPersona">
    		
    		<display:setProperty name="report.title" value="Historial de Notificaciones Persona"></display:setProperty>
    		<display:column media="excel xml pdf" property="id" title="ID"/>
    		<display:column title="Nro Solicitud">
    			<logic:notEmpty name="notificacion" property="credito">
					${notificacion.credito.numeroAtencion}
				</logic:notEmpty>
    		</display:column>
			<display:column title="Expediente">
				<logic:notEmpty name="notificacion" property="credito">
					${notificacion.credito.expediente}
				</logic:notEmpty>
			</display:column>
			<display:column title="L�nea">
				<logic:notEmpty name="notificacion" property="credito">
					${notificacion.credito.linea.nombre}
				</logic:notEmpty>
			</display:column>	
            <display:column title="Fecha de la Notificacion" property="fechaCreada" decorator="com.asf.displayDecorators.DateDecorator"/>                
            <display:column title="Fecha Vencimiento" property="fechaVenc" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
            <display:column title="Observaciones" property="observaciones" sortable="true"/>            
            <display:column title="Tipo de Aviso" property="tipoAvisoStr" sortable="true"/>
            <display:column title="Usuario" property="usuario" sortable="true"></display:column>
            <display:column title="Ver PDF" media="html">
            	<logic:notEqual value="gestionMora" property="tipoAviso" name="notificacion">
            		<a href="${pageContext.request.contextPath}/DownloadIt.do?do=pdf&boleto=false&tipoNoti=${notificacion.tipoAviso}&ids=${notificacion.id}">Ver PDF</a>
            	</logic:notEqual>
            </display:column>
            <display:column title="Archivo" media="html">
           		<logic:notEmpty name="notificacion" property="objetoiArchivo">
					<html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${notificacion.objetoiArchivo.id}')" property="objetoiArchivo.id">Bajar</html:button>
				</logic:notEmpty>					
			</display:column>
			<display:column title="Le�do">
			<logic:equal name="notificacion" property="leido" value="true">
				Si
			</logic:equal>
			<logic:equal name="notificacion" property="leido" value="false">
				No
			</logic:equal>
			</display:column>
			<display:column title="Direcci�n IP" property="direccionIP" sortable="true"/>  
    		<display:column title="Eliminar" media="html">
    			<asf:security action="/actions/process.do" access="do=process&processName=EliminarNotificacion">
    				<a href="${pageContext.request.contextPath}/actions/process.do?processName=EliminarNotificacion&do=process&process.idNotificacion=${notificacion.id}" onclick="return confirmDelete();">Eliminar</a>
    			</asf:security>
    		</display:column>
    	</display:table>
   </div>
   <logic:equal value="false" property="process.pestania" name="ProcessForm">
   <input type="button" value="Atras" onclick="atras();">
   </logic:equal>
</html:form>
<script type="text/javascript">
		
		function atras(){ 
			window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=NotificacionesListProcess&process.action=listarVolver";
		}
		
		function nuevaNotificacion() {
			window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=NuevaNotificacion&process.idPersona=${ProcessForm.process.idPersona}";
		}
	</script>