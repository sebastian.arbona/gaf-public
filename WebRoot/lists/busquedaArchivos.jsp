<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">
	B&uacute;squeda de Archivos
</div>


<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	enctype="multipart/form-data" styleId="oForm">
	<html:hidden property="process.action" name="ProcessForm" value="listar"
		styleId="action" />
	<html:hidden property="process.cid" name="ProcessForm" />
	
	<div id="grilla">
	
	<table>
		<tr>
			<th>N&uacute;mero de Proyecto</th>
			<td>
				<html:text property="process.numeroAtencion" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>Tipo</th>
			<td>
				<asf:select name="process.idTipo" listCaption="descripcion"
					listValues="id"
					entityName="com.civitas.hibernate.persona.TipoArchivo"
					value="${ProcessForm.process.idTipo}" nullValue="true" nullText="Todos" />
			</td>
		</tr>
		<tr>
			<th>Adjuntado desde el</th>
			<td>
				<asf:calendar property="ProcessForm.process.desde" value="${process.desde}"/>
			</td>
		</tr>
		<tr>
			<th>Adjuntado hasta el</th>
			<td>
				<asf:calendar property="ProcessForm.process.hasta" value="${process.hasta}"/>
			</td>
		</tr>
		<tr>
			<th>Descripci&oacute;n</th>
			<td>
				<html:text name="ProcessForm" property="process.detalle"/>
			</td>
		</tr>
		<tr>
			<th>Familia:</th>
			<td>
				<asf:select name="process.origen" 
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${ProcessForm.process.origen}"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'requisito.familia'  order by codigo" nullText="--" nullValue="true" />
						
				
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit>Buscar</html:submit>
			</th>
		</tr>
	</table>
<br/>
<br/>	
		<display:table name="ProcessForm" property="process.archivos"
			id="archivo" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:column property="nombre" title="Nombre" />
			<display:column property="fechaAdjuntoStr" title="Fecha Adjunto" />
			<display:column property="usuario" title="Usuario" />
			<display:column property="tipo.descripcion" title="Tipo" />
			<display:column property="tipoAdjunto" title="Adjunto a"/>
			<display:column property="origen" title="Familia" />
			<display:column property="detalle" title="Descripcion" />
			<display:column title="Bajar" media="html">
				<html:button
					onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${archivo.id}')"
					property="process.objetoiArchivo.id">Bajar</html:button>
			</display:column>
		</display:table>
	</div>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>