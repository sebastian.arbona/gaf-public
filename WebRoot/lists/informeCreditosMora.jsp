<%@page import="com.nirven.creditos.hibernate.informeCreditosMora"%>
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Informes Creditos Mora</div>
<br>

<html:form
	action="/actions/process.do?do=process&processName=InformeCreditosMoraProcess"
	styleId="ProcessForm">
	<html:hidden property="process.idInforme" styleId="idInforme" />
	<html:hidden property="process.action" styleId="action" value="buscar" />

	<table border="0">
		<tr>
			<th>Creado desde</th>
			<td><asf:calendar property="ProcessForm.process.fechaMoraDesde"
					value="${process.fechaMoraDesde}">
				</asf:calendar></td>
		</tr>
		<tr>
			<th>Hasta</th>
			<td><asf:calendar property="ProcessForm.process.fechaMoraHasta"
					value="${fechaMoraHasta}">
				</asf:calendar></td>
		</tr>
		<tr>
			<th colspan="2"><html:submit value="Buscar"></html:submit> <html:submit
					value="Nuevo Informe" onclick="nuevoInforme();"></html:submit></th>
		</tr>
	</table>

	<div class="grilla" style="visible: true">
		<table>
			<display:table name="ProcessForm" property="process.informeCredito"
				id="informeCreditoTable">
				<display:column title="Id" property="id" />
				<display:column title="Fecha Creacion" property="fechaProcesado"
					decorator="com.asf.displayDecorators.DateDecorator" />
				<display:column title="Lineas" property="lineasId" />
				<display:column title="Fecha Calculo" property="fechaHasta" />
				<display:column title="Estado" property="estado" />
				<display:column title="Total Items" property="items" />
				<display:column title="Procesados" property="procesados" />
				<display:column title="Progreso">
					<table style="border: 1px solid; width: 100%; border-spacing: 0">
						<tr>
							<td style="padding: 0;">
								<!-- La condicion de  cambio de color la barra-->
								<hr
									style="
						border: none; 
						margin: 0; 
						background-color: <%=(((informeCreditosMora) informeCreditoTable).getItems() == ((informeCreditosMora) informeCreditoTable)
		.getProcesados()) ? "#AA272F" : "#AAD72F"%>; 
						height: 10px; 
						width: <%=Math.round(((informeCreditosMora) informeCreditoTable).getProcesados() * 1.0
		/ ((informeCreditosMora) informeCreditoTable).getItems() * 100)%>%"
									align="left" /> <!--  && (((informeCreditosMora)informeCreditoTable).getItems()  > 0 ||((informeCreditosMora)informeCreditoTable).getItems()  == 0) -->
							</td>
						</tr>
					</table>
				</display:column>
				<display:column title="Ver Informe">
					<logic:equal name="informeCreditoTable" property="estado"
						value="Procesado">
						<a href="javascript:void(0);"
							onclick="verInforme(${informeCreditoTable.id});">Ver</a>
					</logic:equal>
				</display:column>
			</display:table>

		</table>
	</div>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>


<script type="text/javascript">
	function verInforme(id) {
		var action = document.getElementById("action");
		action.value = "verInforme";
		var idInforme = document.getElementById("idInforme");
		idInforme.value = id;
		document.forms["ProcessForm"].submit();
	}
	
	function nuevoInforme() {
		var action = document.getElementById("action");
		action.value = "nuevoInforme";
		document.forms["ProcessForm"].submit();
	}
	
	function buscar (){
		var action = document.getElementById("action");
		action = "buscar";
		document.forms["ProcessForm"].submit();
	}
</script>
