<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html>

  <body>
  <div class="title">
			Solicitudes Pendientes
		</div>
		<br>
		<br>
  <div class="grilla">
			<display:table name="ProcessForm" property="process.creditos"
				defaultsort="1" id="reportTable">

				<display:column property="persona.nomb12"
					title="Solicitante" />
				<display:column property="expediente" title="Expediente" />
				<display:column property="productoDeReferencia" title="Producto"/>
				<%-- Se oculta columna. Ticket 9158
				<display:column property="financiamiento"
					title="Deposito en garant�a m�nimo" />
				--%>	
				<display:column property="destinoInm"
					title="Destino" />					
				<display:column media="html" title="Ver Detalle"
					href="${pageContext.request.contextPath}/actions/process.do?processName=InmovilizacionPendiente&do=process&process.accion=ver"
					paramId="process.id" paramProperty="id">Ver Detalle</display:column>
			</display:table>
			</div>
			<logic:equal name="ProcessForm" property="process.detalle" value="true">
 			<div class="title">
				Detalle del Titular
			</div>
			<table border="0">				
				<tr>
					<td>
						Solicitante
					</td>
					<td>
						<html:text property="process.credito.persona.nomb12"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
					<td>
						Expediente
					</td>
					<td>
						<html:text property="process.credito.expediente"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
					<td>
						Nro Proyecto
					</td>
					<td>
						<html:text property="process.credito.numeroAtencion"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
				</tr>
			</table>
			<div class="title">
				Detalle de la Garant�a
			</div>
			<table border="0">
				
				<logic:notEmpty name="ProcessForm" property="process.garantia.valores">
				
				<logic:iterate id="valorGarantia" name="ProcessForm" property="process.garantia.valores">
				<tr>
					<th>	
						<bean:write name="valorGarantia" property="campo.nombre"/>
					</th>
					<td>
						<bean:write name="valorGarantia" property="valorCadena"/>	
					</td>
				</tr>
				
				</logic:iterate>
					
				</logic:notEmpty>
				<tr>
					<td>
						Observaciones
					</td>
					<td>
						<html:text property="process.garantia.observacion"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
				</tr>
			</table>
			</logic:equal>
  </body>
</html>
