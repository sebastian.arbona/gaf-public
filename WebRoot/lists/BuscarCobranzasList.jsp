<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet"/>
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet"/>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<script language="javascript" type="text/javascript">
	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
   function imprimir() 
    {
    
  
    
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process'+
        '&processName=CobranzaProcess'+
        '&process.accion=imprimir'+
        '&process.numeroAtencion='+document.getElementById("numeroAtencion").value;             
    }
	
   
</script>

<div class="title">
	Generación de Informe de Cobranzas
</div>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" value="list" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br>
	<table border=0>
		<tr>
			<th>
				Nro de Solicitud / Proyecto:
			</th>
			<td>
				<asf:text id="numeroAtencion" name="ProcessForm" property="process.numeroAtencion"
					type="Long" maxlength="6" value="" />
			</td>
		</tr>
		
	</table>
	<br>
	<html:button property="generarInforme" onclick="imprimir()">Generar Informe</html:button>
	<br>
	<br>
</html:form>