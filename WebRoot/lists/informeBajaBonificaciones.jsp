<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Informe de Baja de Bonificaciones</div>
<html:form action="/actions/process.do?do=process&processName=InformeBajaBonificaciones">
	<html:hidden property="process.action" value="generar"/>
	
	<div class="grilla">
		<table style="margin: 20px 0">
			<tr>
				<th>Fecha desde</th>
				<td>
					<asf:calendar value="${process.fechaDesde}" property="ProcessForm.process.fechaDesde"/>
				</td>
			</tr>
			<tr>
				<th>Fecha hasta</th>
				<td>
					<asf:calendar value="${process.fechaHasta}" property="ProcessForm.process.fechaHasta"/>
				</td>
			</tr>
			<tr>
	  			<th>Convenio</th>
	  			<td>
	  				<asf:select entityName="com.nirven.creditos.hibernate.ConvenioBonificacion" listCaption="getId,getNombre"
	          			listValues="getId" name="process.idConvenio" value="${ProcessForm.process.idConvenio}" nullText="Todos" nullValue="true"/>
	  			</td>
	  		</tr>
	  		<tr>
	  			<th>L&iacute;nea de cr&eacute;dito</th>
	  			<td>
	  				<asf:select entityName="com.nirven.creditos.hibernate.Linea" listCaption="getId,getNombre"
	          			listValues="getId" name="process.idLinea" value="${ProcessForm.process.idLinea}" nullText="Todas" nullValue="true"/>
	  			</td>
	  		</tr>
			<tr>
				<th colspan="2">
					<html:submit value="Consultar"/>
				</th>
			</tr>
		</table>
		
		<display:table name="ProcessForm" property="process.beans" export="true" id="reportTable">
			<display:setProperty name="report.title" value="Informe de Baja de Bonificaciones"></display:setProperty>
			<display:column title="Proyecto" sortable="true">
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.idObjetoi}&process.action=buscar">${reportTable.numeroAtencion}</a>
			</display:column>
			<display:column title="Expediente" property="expediente" sortable="true"></display:column>
			<display:column title="Titular" property="titular" sortable="true"></display:column>
			<display:column title="CUIL/CUIT" property="cuit" sortable="true"></display:column>
			<display:column title="L�nea" property="linea" sortable="true"></display:column>
			<display:column title="Estado" property="estado" sortable="true"></display:column>
			<display:column title="Comport. de Pago" property="comportamiento" sortable="true"></display:column>
			<display:column title="Convenio" property="convenio" sortable="true"></display:column>
			<display:column title="Tasa Bonif." property="tasaBonificacion" sortable="true"></display:column>
			<display:column title="Tipo Bonif." property="tipoBonificacion" sortable="true"></display:column>
			<display:column title="Deuda" property="deuda" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" sortable="true"></display:column>
			<display:column title="D�as Mora" property="diasMora" sortable="true"></display:column>
			<display:column title="Fecha Baja Bonif." property="fechaBaja" decorator="com.asf.displayDecorators.DateDecorator" sortable="true"></display:column>
		</display:table>
	</div>

</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>