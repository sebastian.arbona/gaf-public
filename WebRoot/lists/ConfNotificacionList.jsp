<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>

<div class="title">Configuracion de Notificaciones</div>
<p>
	<html:form
		action="/actions/process.do?do=process&processName=ConfNotificacionListProcess"
		styleId="oForm">
		<html:hidden property="process.action" name="ProcessForm"
			styleId="action" />
		<input type="button" value="Nuevo" onclick="nuevo();">
		<br>
		<br>
		<div class="grilla">
			<display:table name="ProcessForm" property="process.configuraciones"
				export="true" id="reportTable"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">

				<display:column property="denominacion" title="Denominacion"
					sortable="true" />
				<display:column property="asunto" title="Asunto" sortable="true" />
				<display:column property="texto" title="Texto del Mensaje"
					sortable="true" />
				<display:column title="Modificar" sortProperty="id"
					paramId="process.idConfiguracion" paramProperty="id"
					sortable="true"
					href="${pageContext.request.contextPath}/actions/process.do?processName=ConfNotificacionProcess&do=process&process.action=modificar">
    	Modificar</display:column>

				<display:column title="Eliminar" sortProperty="id"
					paramId="process.idConfiguracion" paramProperty="id"
					sortable="true"
					href="${pageContext.request.contextPath}/actions/process.do?processName=ConfNotificacionProcess&do=process&process.action=eliminar">
    	Eliminar</display:column>

			</display:table>



		</div>
	</html:form>
	<script type="text/javascript">
		function nuevo() {
			window.location = "${pageContext.request.contextPath}/actions/process.do?&do=process&processName=ConfNotificacionProcess&process.action=nuevo";
		}
	</script>