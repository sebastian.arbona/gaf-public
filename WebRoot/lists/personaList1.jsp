<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">

<script language="javascript" type="text/javascript">
	function goTo(obj)
    {
        var dest=obj.options[obj.selectedIndex].value;
        if(dest=='') 
        {
            return;
        }
        obj.value='';
        window.location='${pageContext.request.contextPath}/'+dest;
    }

    /**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
  }
</script>
<div class="title">Administraci�n de Datos Personales</div>
<br><br>
<html:form action="/actions/PersonaAction.do?entityName=${param.entityName}&filter=true">
    <table>
    	<tr>
    		<th>Persona:</th>
    		<td>
            	<asf:autocomplete name="" tipo="AJAX" idDiv="autocomplete_persona"
                                  idInput="idpersona" classDiv="autocomplete"
                                  origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
                                  options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
                                  attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'"/>
                                  <span id="indicadorPersona" style="display: none"><img src="images/loader.gif" alt="Trabajando..." /></span>
                                  (C�digo - Apellido y Nombre - N�mero de Documento)
            </td>
        </tr>
    </table>
    <br>
    <html:hidden property="paramName[0]" styleId="paramName" value="idpersona"/>
    <html:hidden property="paramComp[0]" styleId="paramComp" value="="/>
    <html:hidden property="filter" value="true"/>
    <html:hidden property="do" styleId="do" value="list"/>
    <asf:security action="/actions/PersonaAction.do" access="do=newEntity&entityName=${param.entityName}">
        <input type="submit" onclick="getElem('do').value='newEntity';" value="<bean:message key="abm.button.new"/>" />
    </asf:security>
    <input type="button" value="Consultar" onclick="consultar();">
    
    <br><br>
<display:table name="ListResult" property="result" defaultsort="1" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/PersonaAction.do?do=list&entityName=${param.entityName}">
    <display:setProperty name="report.title" value="Administraci�n de Personas"></display:setProperty>
    <display:column media="html" sortProperty="id" property="id" title="ID" href="${pageContext.request.contextPath}/actions/PersonaAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.id" paramProperty="id" />
    <display:column property="nomb12" title="Apellido y Nombre" />
    <display:column property="razonSocial" title="Raz�n Social" />
    <display:column title="Documento">
    	${reportTable.tipodoc.tido47} ${reportTable.nudo12}
    </display:column>
    <display:column property="domicilioCompleto" title="Domicilio"  />
    <display:column property="estadoActual.estado.nombreEstado" title="Estado"/>
    <display:column title="Color"><div style="background:${reportTable.estadoActual.estado.color};" >&nbsp;</div></display:column>
    <display:column property="fechaBajaStr" title="Fecha de Baja"/>
    <display:column sortable="false" title="Links"  media="html"> 
        <select name="combo" id="combo" onchange="javascript:goTo(this);">
            <OPTION value="">SELECCIONE UNA OPCI�N</OPTION>
            <asf:security action="/actions/process.do?do=process&processName=PersonaVinculada">
                <OPTION value="/actions/process.do?do=process&processName=PersonaVinculada&process.personaTitular.id=${reportTable.id}">Personas Vinculadas</OPTION>
            </asf:security>
            <asf:security action="/actions/process.do?do=process&processName=Domicilio">
                <OPTION value="/actions/process.do?do=process&processName=Domicilio&process.personaTitular.id=${reportTable.id}">Domicilios</OPTION>
            </asf:security>
            <asf:security action="/actions/process.do?do=process&processName=Contacto">
                <OPTION value="/actions/process.do?do=process&processName=Contacto&process.personaTitular.id=${reportTable.id}">Contactos</OPTION>
            </asf:security>
            <asf:security action="/actions/process.do?do=process&processName=DatosImpositivos">
                <OPTION value="/actions/process.do?do=process&processName=DatosImpositivos&process.persona.id=${reportTable.id}">Datos Impositivos</OPTION>
            </asf:security>
             <asf:security action="/actions/process.do?do=process&processName=DatosLaborales">
                <OPTION value="/actions/process.do?do=process&processName=DatosLaborales&process.persona.id=${reportTable.id}">Datos Laborales</OPTION>
            </asf:security>
            <asf:security action="/actions/process.do?do=process&processName=DatosBancarios">
                <OPTION value="/actions/process.do?do=process&processName=DatosBancarios&process.persona.id=${reportTable.id}">Datos Bancarios</OPTION>
            </asf:security>
            <asf:security action="/actions/process.do?do=process&processName=EstadosPersonas">
                <OPTION value="/actions/process.do?do=process&processName=EstadosPersonas&process.persona.id=${reportTable.id}">Estados y Observaciones</OPTION>
            </asf:security>
            <asf:security action="/actions/process.do?do=process&processName=Roles">
                <OPTION value="/actions/process.do?do=process&processName=Roles&process.persona.id=${reportTable.id}">Roles</OPTION>
            </asf:security>
            <asf:security action="/actions/ArchivoAction.do?do=list&entityName=Archivo">
        		<OPTION value="actions/ArchivoAction.do?do=list&entityName=Archivo&paramName=idpersona&paramComp==&[0]&paramValue[0]=${reportTable.id}&nomb12=${reportTable.nomb12 }&filter=true">Documentaci�n</OPTION>
			</asf:security>
        </select>
    </display:column>
    <logic:notEmpty name="ListResult" property="result">
	    <logic:empty name="reportTable" property="fechaBaja">
			<display:column media="html" honClick="return confirmDelete();" title="Baja" href="${pageContext.request.contextPath}/actions/PersonaAction.do?do=loadBaja&entityName=${param.entityName}&filter=true" paramId="entity.id" paramProperty="id">Baja</display:column>
		</logic:empty>
	</logic:notEmpty>
</display:table>
</html:form>

<script language="javascript" type="text/javascript">
var ctrlPersona = $('idpersona');

function consultar()
{
	var idPersona = trim(ctrlPersona.value.split("-")[0]);

	var url = "";
    url += '${pageContext.request.contextPath}/actions/PersonaAction.do?'
    url += 'do=list';
    url += "&entityName=${param.entityName}";
	url += '&paramName[0]=idpersona';
	url += '&paramComp[0]==';
	url += '&paramValue[0]=' + idPersona;
	url += "&filter=true";
	
	window.location = url;
}
</script>