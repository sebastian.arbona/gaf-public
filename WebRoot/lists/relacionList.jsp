<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<script language="javascript" type="text/javascript">
	function filtrar()
    {
		 getElem("AbmForm").submit();
    }

	function newEntity(){
        window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}";
    }
</script>

<div class="title">Administración de Relaciones entre Personas</div>
<br><br>

<html:form action="/actions/abmAction.do?entityName=${param.entityName}&do=list&filter=true" styleId="AbmForm">
<html:hidden property="paramName[0]" value="tipoRelacion.id" />
<html:hidden property="paramComp[0]" value="=" />
<br>
<table>
	<tr>
		<th>Tipo de Relación: </th>
		<td>
			<asf:select name="paramValue[0]" entityName="TipoRelacion" listCaption="id,nombreTipoRelacion"
			listValues="id"
			orderBy="id" value="${paramValue[0]}" attribs="onchange='filtrar();'" nullText="Seleccione un Tipo de Relación" nullValue="true" />
		</td>
		
	</tr>
</table>
</html:form>
<br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
    <button type="button" onclick="javascript:newEntity();"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1"
	requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}" >
	<display:setProperty name="report.title" value="Administración de Relaciones entre Personas"></display:setProperty>  

	<display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
		href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" />
	<display:column property="nombreRelacion" title="Nombre" sortable="true" />
	<display:column property="descripcionRelacion" title="Descripción" sortable="true" />
	<display:column property="inversa.nombreRelacion" title="Inversa" sortable="true" />
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
	<display:column media="html" honClick="return confirmDelete();"
		title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}"
		paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete" />
	</display:column>
</asf:security>
</display:table>
</div>