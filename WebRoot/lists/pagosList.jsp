<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <div style="width:70%" align="left"><html:errors/></div>
    <logic:empty name="ProcessForm" property="process.pagos">
    El Cr�dito: ${ProcessForm.process.objetoi.numeroAtencion} no tiene Pagos.
    </logic:empty>
    <logic:notEmpty name="ProcessForm" property="process.pagos">
    	<display:table name="ProcessForm" property="process.pagos" id="pago" export="true" 
    		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
    		<display:caption>Proyecto ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12}</display:caption>
    		<display:setProperty name="report.title" value="Pagos para el cr�dito N� ${ProcessForm.process.objetoi.numeroAtencion} de ${ProcessForm.process.objetoi.persona.nomb12}" />
            <display:column title="Fecha de Pago" media="html" sortable="true">
                <a href="javascript:void(0);" onclick="imprimir(${pago.id.boleto.objetoi.id}, ${pago.id.boleto.id}, '${pago.medioPago}', ${pago.id.caratula.id});" target="_self">${pago.fechaPagoStr}</a>
            </display:column>
            <display:column title="Fecha de Pago" media="pdf excel xml" property="fechaPagoStr"/>
            <display:column title="Monto" property="importe" sortable="true"/>
            <display:column title="Medio Pago" property="medioPago" sortable="true"/>
            <display:column title="Observaciones" property="id.caratula.observaciones" sortable="true"/>
            <logic:empty name="pago" property="recibo">
            	<display:column title="Nro de Recibo" property="id.boleto.numeroBoleto" sortable="true"/>
            </logic:empty>
            <logic:notEmpty name="pago" property="recibo">
				<display:column title="Nro de Recibo" property="recibo.numeroBoleto" sortable="true"/>            
            </logic:notEmpty>
    	</display:table>
    </logic:notEmpty>
</html:form>
<script language="javascript" type="text/javascript">
    function imprimir(id, idBoleto, medioPago, idCaratula) 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process'+
        '&processName=${param.processName}'+
        '&process.accion=imprimir'+
        '&process.objetoi.id='+id+
        '&process.pago.id.boleto.id='+idBoleto+
        '&process.idCaratula='+idCaratula+        
        '&process.pago.medioPago='+medioPago;             
    }
</script>    