<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="nuevo" />
	<html:hidden property="process.solicitudProrroga.id" styleId="solicitudProrroga.id"
		value="${ProcessForm.process.solicitudProrroga.id}" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<!--<asf:security action="/actions/process.do"
		access="do=process&processName=${param.processName}">
		<logic:equal name="ProcessForm" property="process.ocultarCampos" value= "true">
			<html:submit value="Modificar"/>
		</logic:equal>
		
		<logic:notEqual name="ProcessForm" property="process.ocultarCampos" value= "true">
   			<html:submit value="Modificar"/>
		</logic:notEqual>
		
		
		
		idObjetoi - ${ProcessForm.process.idObjetoi}
		
	</asf:security>-->
	
	<input type="button" value="Volver" onclick="volver();">
	<br/>
	<br/>

	<logic:notEmpty name="ProcessForm" property="process.estadosObservaciones">
		<display:table name="ProcessForm" property="process.estadosObservaciones"
			pagesize="10" id="reportTable" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
			<display:caption>Estados Pr�rroga Nro  ${ProcessForm.process.solicitudProrroga.id}</display:caption>
			<display:column title="Estado" property="estaobs" sortable="true" />
			<display:column title="Color" media="html">
				<div style="background:${reportTable.color};">
					&nbsp;
				</div>
			</display:column>
			<display:column title="Desde" property="fechaStr" sortable="true" />
			<display:column title="Hasta" property="fechaHastaStr"
				sortable="true" />
			<display:column title="Observaciones" property="observaciones"
				sortable="true" />
			<display:column title="Area Responsable" property="areaResponsable"/>
		</display:table>
	</logic:notEmpty>
	<logic:empty name="ProcessForm" property="process.prorrogaEstados">
        La Pr�rroga : ${ProcessForm.process.solicitudProrroga.id} no tiene Estado.
    </logic:empty>
</html:form>

<script language='javascript'>

function volver(){

	
	var url = '${pageContext.request.contextPath}/actions/process.do?do=process' +
	'&processName=ProrrogaProcess' +
	'&process.accion=listar' +
	'&process.idObjetoi=${ProcessForm.process.idObjetoi}';
window.location = url;
return true;
	
}


</script>

