<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>

<div class="title">Reporte De Proyectos A Notificar</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="ProcessForm">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" styleId="action" />
	<html:errors />
	<div id="datosGenerales">
		<table style="border: 2px solid rgb(204, 204, 204);">
			<tr>
				<th>Comportamiento:</th>
				<td><asf:select name="process.comportamientoPago"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						listCaption="getCodigo,getDescripcion" listValues="getCodigo"
						filter="categoria = 'comportamientoPago' order by codigo"
						value="${ProcessForm.process.comportamientoPago}" nullValue="true"
						nullText="-- Todos los Comportamientos --" /></td>
			</tr>
			<tr>
				<th>L&iacute;nea de Cr&eacute;dito:</th>
				<td><html:select styleId="idsLinea" property="process.idsLinea"
						multiple="true" size="8">
						<html:optionsCollection property="process.lineasList"
							label="nombre" value="id" />
					</html:select></td>
			</tr>
			<tr>
				<th>C&oacute;digo de Persona:</th>
				<td><asf:selectpop name="process.idPersona" title="Persona"
						columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
						captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
						entityName="com.civitas.hibernate.persona.Persona" value="" /></td>
			</tr>
			<tr>
				<th>Proyecto Desde:</th>
				<td><asf:selectpop name="process.numeroAtencionDesde"
						entityName="Objetoi" title="Credito" caseSensitive="true"
						values="numeroAtencion" columns="Proyecto,Tomador"
						captions="numeroAtencion,persona.nomb12" cantidadDescriptors="1"
						value="${ProcessForm.process.numeroAtencionDesde > 0 ? ProcessForm.process.numeroAtencionDesde : null}" />
				</td>
			</tr>
			<tr>
				<th>Proyecto Hasta:</th>
				<td><asf:selectpop name="process.numeroAtencionHasta"
						entityName="Objetoi" title="Credito" caseSensitive="true"
						values="numeroAtencion" columns="Proyecto,Tomador"
						captions="numeroAtencion,persona.nomb12" cantidadDescriptors="1"
						value="${ProcessForm.process.numeroAtencionHasta > 0 ? ProcessForm.process.numeroAtencionHasta : null}" />
				</td>
			</tr>
			<tr>
				<th>Tipo de Garant&iacute;a:</th>
				<td><asf:selectpop name="process.idTipoGarantia"
						entityName="TipoGarantia" title="Tipos de Garantia"
						caseSensitive="true" values="id" columns="ID,Nombre,Descripcion"
						captions="id,nombre,descripcion" cantidadDescriptors="1"
						value="${ProcessForm.process.idTipoGarantia > 0 ? ProcessForm.process.idTipoGarantia : null}" />
				</td>
			</tr>
			<tr>
				<th>Identificador Garant&iacute;a</th>
				<td><asf:text property="process.identificadorUnico" type="text"
						maxlength="11" name="ProcessForm" id="identificadorUnico"></asf:text></td>
			</tr>
			<tr>
				<th>Fecha de Mora</th>
				<td><asf:calendar property="ProcessForm.process.fechaMora"
						value="${process.fechaMora}">
					</asf:calendar></td>
			</tr>
		</table>
	</div>
	<br />
	<div>
		<input type="button" value="Limpiar Pantalla"
			onclick="limpiarPantalla();" /> <input type="button" value="Listar"
			onclick="listar();" /> <input type="button" value="Exportar"
			onclick="exportar();">
	</div>
	<br />

	<logic:notEmpty name="ProcessForm"
		property="process.beanNotificacionMoraDetalleList">
		<div class="grilla">
			<display:table name="ProcessForm"
				property="process.beanNotificacionMoraDetalleList" export="false"
				id="reportTable" defaultsort="2"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column title="Selecci�n" sortable="false">
					<input type="checkbox"
						name="notificacion-${reportTable.numeroAtencion}"
						value="${reportTable.numeroAtencion}" />
				</display:column>
				<display:column title="Proyecto">
					<a
						href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.idObjetoi}&process.action=buscar&process.personaId=">${reportTable.numeroAtencion}</a>
				</display:column>
				<display:column property="titularNombre" title="Titular"
					sortable="true" />
				<display:column property="lineaNombre" title="Linea" sortable="true" />
				<display:column property="comportamientoNombre" title="Estado"
					sortable="true" />
				<display:column property="fechaMora" title="Fecha De Mora"
					sortable="true" />
				<display:column property="diasAtraso" title="D�as de atraso"
					sortable="true" />
				<display:column property="deudaTotal" title="Importe Total Adeudado"
					sortable="true"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
				<display:column property="deudaExigible" title="Deuda Exigible"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" />
				<display:column property="monedaNombre" title="Moneda"></display:column>
				<display:column property="monedaCotizacion" title="Cotizacion"
					decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator"></display:column>
				<display:column property="bonificado" title="Bonificado"
					decorator="com.asf.cred.decorator.BooleanDecorator">
				</display:column>
				<display:column title="Valores Cartera"
					decorator="com.asf.cred.decorator.BooleanDecorator">
					<logic:equal name="reportTable" property="valorCartera"
						value="true">
						<a
							href="${ProcessForm.process.urlGaf}/actions/process.do?processName=ValorCartera&do=process&process.action=list&process.nroCredito=${reportTable.numeroAtencion}"
							target="_blank"> Si </a>
					</logic:equal>
				</display:column>
				<display:column title="Historial"
					href="${pageContext.request.contextPath}/actions/process.do?processName=NotificacionProcess&do=process"
					paramId="process.idCredito" paramProperty="idObjetoi">Historial</display:column>
			</display:table>
		</div>
		<div>
			<input type="button" id="seleccionarTodos" value="Seleccionar todos"
				onclick="marcarTodos();" /> <input type="button"
				id="deseleccionarTodos" value="Deseleccionar todos"
				onclick="desmarcarTodos()" />
		</div>
		<div>

			<input type="button" value="Generar" onclick="generar();">
		</div>
	</logic:notEmpty>

</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

<script type="text/javascript">
	var formulario = $('ProcessForm');
	var action = $('action');

	function limpiarPantalla() {
		action.value = "limpiarPantalla";
		formulario.submit();
	}

	function listar() {
		action.value = 'listar';
		formulario.submit();
	}

	function exportar() {
		action.value = 'exportar';
		formulario.submit();
	}
	function generar() {
		action.value = 'generar';
		formulario.submit();
	}

	function marcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = true;
			}
		}
	}

	function desmarcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}
</script>