<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Informe Sint�tico de Desembolsos</div>
<br><br>
<html:form action="/actions/process.do?do=process&processName=InformeSinteticoDesembolsos">
	<html:hidden property="process.action" value="buscar"/>
	
	<div class="grilla">
		<table>
			<tr>
				<th>Fecha desde</th>
				<td>
					<asf:calendar value="${process.fechaDesde}" property="ProcessForm.process.fechaDesde"/>
				</td>
			</tr>
			<tr>
				<th>Fecha hasta</th>
				<td>
					<asf:calendar value="${process.fechaHasta}" property="ProcessForm.process.fechaHasta"/>
				</td>
			</tr>
			<tr>
				<th>L�neas</th>
				<td>
					<html:select property="process.seleccion" name="ProcessForm" multiple="true" size="8">
						<html:optionsCollection name="ProcessForm" property="process.lineas" label="nombre" value="id"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<html:submit value="Consultar"/>
				</td>
			</tr>
		</table>
		
		<br/><br/>
		
		<table>
			<tr>
				<th colspan="2">Aprobados</th>
			</tr>
			<tr>
				<th>Cantidad</th>
				<th>Monto</th>
			</tr>
			<tr>
				<td>
					${ProcessForm.process.cantidadAprobados}
				</td>
				<td>
					${ProcessForm.process.montoAprobados}
				</td>
			</tr>
		</table>
		
		<br/>
		
		<display:table name="ProcessForm" property="process.resultado" export="true" id="reportTable">
			<display:setProperty name="report.title" value="Informe Sint�tico de Desembolsos"></display:setProperty>
			<display:column title="Fecha" property="fecha" decorator="com.asf.displayDecorators.DateDecorator"/>
		    <display:column title="Desembolsados" property="cantDesembolsados" style="text-align: right"/>
		    <display:column title="Monto" property="montoDesembolsados" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" style="text-align: right"/>
		    <display:column title="1er desemb. pendiente" property="cantPendiente1" style="text-align: right"/>
		    <display:column title="Monto" property="montoPendiente1" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" style="text-align: right"/>
		    <display:column title="2do desemb. pendiente" property="cantPendiente2" style="text-align: right"/>
		    <display:column title="Monto" property="montoPendiente2" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" style="text-align: right"/>
		    <display:column title="Monto Total" property="montoPendienteTotal" decorator="com.asf.cred.decorator.NegativeSeparatorDoubleDecorator" style="text-align: right"/>
		</display:table>
	</div>

</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>