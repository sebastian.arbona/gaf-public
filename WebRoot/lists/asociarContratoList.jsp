<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@page import="com.nirven.creditos.hibernate.Linea"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ page import="com.asf.util.DateHelper"%>
<%@ page import="java.util.Date"%>

<div class="title">Generar Contratos</div>
<html:form action="actions/asociarContrato.do"
	styleId="AsociarContratoExportForm" enctype="multipart/form-data">
	<html:hidden styleId="do" property="do" value="listar" />

	<div align="left" style="position: relative; left: 15px">
		<html:errors />
	</div>

	<!-- filtrar por linea de credito -->
	<logic:empty name="AsociarContratoExportForm" property="idObjetoi">
		<table border="0">
			<tr>
				<th>L�nea de cr�dito:</th>
				<td><asf:select name="linea"
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="getId,getNombre" listValues="getId"
						filter="activa = true order by id" nullValue="true"
						nullText="Seleccione l�nea..." /></td>
			</tr>
		</table>
		<html:submit value="Listar" styleId="bListar" onclick="listar();" />
	</logic:empty>
	<br />
	<br />
	<display:table name="AsociarContratoExportForm" property="result"
		id="reportTable" offset="0"
		requestURI="${pageContext.request.contextPath}/actions/asociarContratoExport.do?do=listar();">
		<display:column title="Selecci�n">
			<input type="checkbox" name="seleccion-${reportTable.id}"
				value="${reportTable.id}" />
		</display:column>
		<display:column property="persona.cuil12" title="CUIT/CUIL"></display:column>
		<display:column property="persona.nomb12" title="Nombre"></display:column>
		<display:column property="numeroAtencion" title="Proyecto"></display:column>
		<display:column property="fechaExpediente" title="Fecha Expte."></display:column>
		<display:column property="expediente" title="Expediente"></display:column>
	</display:table>
	<br />
	<br />
	<div id="datosGenerales">
		<table>
			<tr>
				<th>Contrato:</th>
				<td><asf:select name="escrito"
						entityName="com.nirven.creditos.hibernate.Escrito"
						value="${AsociarContratoExportForm.escrito}"
						listCaption="identificacion" listValues="id"
						filter="tipoEscrito = 'C' order by identificacion"
						nullValue="true" nullText="Seleccione el modelo de contrato..." /></td>
			</tr>
			<tr>
				<th>Fecha de Contrato:</th>
				<td><asf:calendar property="fechaCStr" /></td>
			</tr>
			<tr>
				<th>Contrato Definitivo:</th>
				<td><input type="checkbox" name="chkDefinitivo" value="checked"
					onclick="habilitarAdjuntar();" /></td>
			</tr>
		</table>
	</div>
	<div id="adjuntar" style="display: none">
		<table>
			<tr>
				<th>Tipo de Documento</th>
				<td><asf:select2 name="AsociarContratoExportForm"
						property="idTipoDeArchivo"
						value="${AsociarContratoExportForm.idTipoDeArchivo}"
						descriptorLabels=" Nombre" entityProperty="id"
						descriptorProperties="nombre, descripcion"
						entityName="TipoDeArchivo" where2=" padre_id IS NOT NULL "
						accessProperty="id" /></td>
			</tr>
			<tr>
				<th>Adjuntar archivo de Contrato Definitivo:</th>
				<td><html:file property="formFile" styleId="adjunto" accept=".pdf" /></td>
			</tr>
		</table>
	</div>
	<br />
	<html:submit onclick="generar();" value="Generar Contrato" />
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
	function generar() {
		var msg = "";
		if ($('fechaCStr').value == null || $('fechaCStr').value == "") {
			msg += "No se ha indicado una fecha de contrato.\n"
		}
		if (msg == "") {
			$("AsociarContratoExportForm").action = "${pageContext.request.contextPath}/actions/asociarContrato.do?do=generar";
			$("do").value = "generar";
		} else {
			alert(msg);
		}
	}

	function listar() {
		$("AsociarContratoExportForm").action = "${pageContext.request.contextPath}/actions/asociarContrato.do?do=listar";
		$("do").value = "listar";
	}

	function habilitarAdjuntar() {
		var nodoCheck = document.getElementsByName("chkDefinitivo");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].checked == true) {
				$('adjuntar').style.display = "";
			} else {
				$('adjuntar').style.display = "none";
			}
		}
	}
</script>