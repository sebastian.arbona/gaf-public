<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@page import=" com.nirven.creditos.hibernate.TipoGarantia"%>
<%pageContext.setAttribute("idTipoGarantiaFianza", DirectorHelper.getString("tipoGarantia.fianzaPersonal"));%>

<div class="title">Sustituci�n de Garant�as</div>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
	<html:hidden property="process.accion" styleId="accion"/>
	<html:errors/>
	<table>
		<tr>
			<th>L�nea:</th>
			<td>
			<asf:select name="process.idLinea"
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="getId,getNombre" listValues="getId"
						filter="activa = true order by id"
						nullValue="true" nullText="&lt;&lt;Todas&gt;&gt;" 
						value="${ProcessForm.process.idLinea}"/>
			</td>
		</tr>
		<tr>
			<th>Tipo de Garant�a:</th>
			<td>
				<asf:select 
					name="process.idTipoGarantia"
					value="${ProcessForm.process.idTipoGarantia}"
					entityName="com.nirven.creditos.hibernate.TipoGarantia"
					listValues="id" listCaption="nombre"
					nullValue="true" nullText="&lt;&lt;Todos&gt;&gt;" />
			</td>
		</tr>
		<tr>
			<th>Estado:</th>
			<td>
				<asf:select name="process.idEstado" entityName="Estado" 
						listCaption="idEstado,nombreEstado"
            			listValues="idEstado"
                        value="${ProcessForm.process.idEstado}"
                        nullValue="true" nullText="&lt;&lt;Todos&gt;&gt;"
                        filter="tipo = 'Objetoi'"/>
			</td>
		</tr>
		<tr>
			<th>Fecovita:</th>
			<td>
				<html:select property="process.fecovita" value="${ProcessForm.process.fecovita}">
					<html:option value="1">SI</html:option>
					<html:option value="0">NO</html:option>
				</html:select>
			</td>
		</tr>
		<tr>
			<th>
				Nro de Proyecto:
			</th>
			<td>
				<asf:text name="ProcessForm" property="process.numeroAtencion" 
					type="Long" maxlength="6" value="${ProcessForm.process.numeroAtencion}" />
			</td>
		</tr>
	</table>
	<html:submit value="Listar" onclick="enviarAccion('listar');"/>
	<br/>
	<br/>
	<table>
		<tr>
			<th>Nueva Garant�a:</th>
			<td>
				<asf:select 
					name="process.tipoGarantia.id" 
					value="${ProcessForm.process.tipoGarantia.id}"
					entityName="com.nirven.creditos.hibernate.TipoGarantia"
					listValues="id" listCaption="nombre" 
					nullValue="true" nullText="&lt;&lt;Ninguna&gt;&gt;" 
					attribs="onchange=enviarForm('mostrarCampos');" />
			</td>
		</tr>
	</table>
	<br/>
	<logic:notEmpty name="ProcessForm" property="process.tipoGarantia.id">
	<logic:equal name="ProcessForm" value="${idTipoGarantiaFianza}" property="process.tipoGarantia.id">
	<table id="tipoFianza">
		<tr>
			<th>${ProcessForm.process.tipoGarantia.nombreNumeroIdentificacion}:</th>
			<td><asf:text property="process.garantia.identificadorUnico" type="long" maxlength="11" name="ProcessForm" id="idIdentificador"></asf:text></td>
		</tr>
		<tr>
			<th>Aforo:</th>
			<td><asf:text property="process.aforo" type="long" maxlength="10" name="ProcessForm" id="idAforo"/>%</td>
		</tr>
		<tr>
			<th>Observaciones:</th>
			<td><html:textarea property="process.garantia.observacion"></html:textarea></td>
		</tr>
		<tr>
			<th>Fecha de Libramiento de Pagar�:</th>
			<td><asf:calendar property="ProcessForm.process.fechaLibramiento"/></td>
		</tr>
		<tr>
	    <th>Resoluci�n:</th>
	    <td colspan="3"><html:file property="process.formFile"/></td>
	</tr> 				
	</table>
	</logic:equal>
	<logic:notEqual name="ProcessForm" value="${idTipoGarantiaFianza}" property="process.tipoGarantia.id">	
	<table id="otrosTipos">
		<tr>
			<th>${ProcessForm.process.tipoGarantia.nombreNumeroIdentificacion}:</th>
			<td><asf:text property="process.garantia.identificadorUnico" type="text" maxlength="11" name="ProcessForm" id="idIdentificador2"></asf:text></td>
		</tr>
		<tr>
			<th>Observaciones:</th>
			<td><html:textarea property="process.garantia.observacion" name="ProcessForm"></html:textarea></td>
		</tr>
		<tr>		
			<th>Resoluci�n:</th>
		    <td colspan="3"><html:file property="process.formFile"/></td>
	    </tr>
	</table>
	</logic:notEqual>	
	</logic:notEmpty>	
	<br/>
	<html:submit value="Sustituir Garant�as" onclick="if(confirm('Est� seguro/a que desea sustituir la Garant�a de los Cr�ditos seleccionados?')) enviarAccion('sustituir');"/>
	<br/>
	<br/>
	<logic:notEmpty name="ProcessForm" property="process.creditos">
	<table>			
		<tr><th><input type="checkbox" onclick="activar();" id="activacion" align="left"></th></tr>
	</table>
	</logic:notEmpty>
	<logic:notEmpty name="ProcessForm" property="process.exito">
		<div style="font-size: 14">${ProcessForm.process.exito}</div>
	</logic:notEmpty>
	<display:table name="ProcessForm" id="credito" property="process.creditos" 
	 export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
	 	<display:column title="Seleccionar" media="html">
			<input type="checkbox" name="process.creditosSeleccionados[${credito_rowNum - 1}]" value="${credito_rowNum - 1}">
		</display:column>
		<display:column title="Proyecto" property="numeroCredito"/>
		<display:column title="Expediente" property="expediente"/>
		<display:column title="Titular" property="titular.nomb12"/>
		<display:column title="CUIT" property="cuil"/>
		<display:column title="Producto" property="producto"/>
		<display:column title="Tipo Garant�a" property="tipoGarantia"/>
		<display:column title="ID Garant�a" property="idUnicoGarantia"/>	 
	</display:table>
</html:form>
<script type="text/javascript">
	function enviarAccion(accion){
		$('accion').value = accion;
	}
	
	function enviarForm(accion){
		$('accion').value = accion;
		$('oForm').submit();
	}
	
	function marcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = true;
			}
		}
	}

	function desmarcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}
	function activar() {
		if ($('activacion').checked == true) {
			marcarTodos();
		} else {
			desmarcarTodos();
		}
	}
<%--iniciar pantalla--%>
<%--	if($('idIdentificador') idAforo!= null)--%>
<%--		$('idIdentificador').value = "";--%>
<%--	if($('idIdentificador2') != null)--%>
<%--		$('idIdentificador2').value = "";--%>
<%--	if($('idAforo') != null)--%>
<%--		$('idAforo').value = "";		--%>
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>