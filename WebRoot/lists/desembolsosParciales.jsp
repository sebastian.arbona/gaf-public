<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
  	
   	<logic:equal value="true" property="process.estadosModificados" name="ProcessForm">
   	Los estados de los creditos seleccionados fueron modificados correctamente
   	</logic:equal>
    <br/>   
	<br/>
	Ingrese los porcentajes entre los cuales desea buscar
	<table border="0">				
				<tr>
					<td>
						Porcentaje Menor
					</td>
					<td>
						<html:text property="process.porcentaje1Str" value="${ProcessForm.process.porcentaje1Str}"
							name="ProcessForm" maxlength="30" />
					</td>
				</tr>
				<tr>
					<td>
						Porcentaje Mayor
					</td>
					<td>
						<html:text property="process.porcentaje2Str" value="${ProcessForm.process.porcentaje2Str}"
							name="ProcessForm" maxlength="30" />
					</td>
				</tr>
		</table>
		<div>
		<input type="button" value="Listar" onclick="listar();">
		</div>		
    <logic:notEmpty name="ProcessForm" property="process.desembolsos">
    	<input type="checkbox" onclick="activar();" id="activacion">
    	<display:table name="ProcessForm" property="process.desembolsos" id="desembolso">    		            
            <display:column title="Selecci�n">
					<input type="checkbox" name="desembolso-${desembolso.numeroAtencion}" value="${desembolso.numeroAtencion}" />
			</display:column>
            <display:column title="Proyecto" property="numeroAtencion" sortable="true"/>
            <display:column title="Expediente N�" property="expediente" sortable="true"/>
            <display:column title="Solicitante" property="nombre" sortable="true"/>
            <display:column title="QQAprobados" property="qqFinal" sortable="true"/>
            <display:column title="Porcentaje QQ Ingresado" property="porcentajeStr" sortable="true" />
    	</display:table>
    	<div>
    	<input type="button" value="Autorizar Segundo Desembolso" onclick="cambiarEstado();">
    	</div>
    </logic:notEmpty>   
</html:form>

	
<script language="javascript" type="text/javascript">
    
    function listar(){
				var accion = document.getElementById("accion");		
				var formulario = document.getElementById("oForm");
				accion.value="listar";
				formulario.submit();				
			}
	function cambiarEstado(){
				var accion = document.getElementById("accion");		
				var formulario = document.getElementById("oForm");
				accion.value="cambiarEstado";
				formulario.submit();				
			}		
	function marcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = true;
	        }
	    }
	  }

	  function desmarcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = false;
	        }
	    }
	  }
	function activar(){
		var check = document.getElementById('activacion');
		if (check.checked==true){
			marcarTodos();
		}else{
			desmarcarTodos();
		}
	}
</script>    