<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<html>
  
  <body>
		<div class="title">
			Inmovilizaciones Realizadas
		</div>
		<br>
		<br>
		<div class="grilla">
			<html:form action="/actions/process.do?do=process&processName=${param.processName}"
				styleId="ProcessForm">
				<html:hidden property="process.accion" name="ProcessForm" value="listar"/>
	  			<table width="100%">
	  				<tr>
	  					<th>Temporada</th>
	  					<td>
	  						<asf:select name="process.idCosechaConfig"
								entityName="com.nirven.creditos.hibernate.CosechaConfig"
								listCaption="getTemporada,getVarietalesStr" listValues="getId"
								value="ProcessForm.process.idCosechaConfig" />
							<html:submit value="Listar"></html:submit>
	  					</td>
	  				</tr>
	  			</table>
  			
	  			<logic:notEmpty name="ProcessForm" property="process.inmovilizaciones">
	  				<%-- Habilito el export. Ticket 9156 --%>
					<display:table name="ProcessForm" property="process.inmovilizaciones"
						defaultsort="1" id="reportTable" export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=InmovilizacionRealizada&process.accion=listar">
		
						<display:column property="objetoi.persona.nomb12" title="Solicitante" />
						<display:column property="objetoi.expediente" title="Expediente" />
						<display:column property="objetoi.volumenContrato"
							title="Volumen Segun Contrato" decorator="com.asf.displayDecorators.DoubleDecorator"/>
						<%-- Oculto la columna. Ticket 9156 --%> 
						<%-- <display:column property="volumen" title="Volumen Inmovilizacion"/> --%>
						<display:column property="nombreProducto" title="Producto"/>
						<display:column property="objetoi.volumenInmovilizado" title="Volumen Inmovilizado Actual"/>
						<display:column property="objetoi.destinoInm" title="Destino" />
						<display:column media="html" title="Ver Detalle"
							href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=InmovilizacionRealizada&process.accion=ver"
							paramId="process.id" paramProperty="objetoi.id">Ver Detalle</display:column>			
					</display:table>
				</logic:notEmpty>
		
				<logic:equal name="ProcessForm" property="process.detalle" value="true">
					<div class="title">
						Detalle del Titular
					</div>
					<table border="0">				
						<tr>
							<th>
								Solicitante
							</th>
							<td>
								${ProcessForm.process.credito.persona.nomb12}
							</td>
							<th>
								Expediente
							</th>
							<td>
								${ProcessForm.process.credito.expediente}
							</td>
							<th>
								Nro Proyecto
							</th>
							<td>
								${ProcessForm.process.credito.numeroAtencion}
							</td>
						</tr>
					</table>
					<div class="title">
						Detalle de la Garant�a
					</div>
					<table border="0">
						
						<logic:notEmpty name="ProcessForm" property="process.garantia.valores">
						
						<logic:iterate id="valorGarantia" name="ProcessForm" property="process.garantia.valores">
						<tr>
							<th>	
								<bean:write name="valorGarantia" property="campo.nombre"/>
							</th>
							<td>
								<bean:write name="valorGarantia" property="valorCadena"/>	
							</td>
						</tr>
						
						</logic:iterate>
							
						</logic:notEmpty>
						
						<tr>
							<th>
								Observaciones
							</th>
							<td>
								<html:textarea property="process.garantia.observacion" 
									name="ProcessForm"  disabled="true" />
							</td>
						</tr>
					</table>
					<div class="title">
						Liberaciones
					</div>
					<display:table name="ProcessForm" property="process.liberaciones">
					  	<display:column title="Volumen" property="volumen" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"></display:column>
					  	<display:column title="Fecha" property="fecha" decorator="com.asf.displayDecorators.DateDecorator"></display:column>
					</display:table>
					
					<%-- Agrego secci�n con el detalle de las Inmovilizaciones. Ticket 9156 --%>
					<div class="title">
						Inmovilizaciones
					</div>
					<display:table name="ProcessForm" property="process.inmovilizaciones">
					  	<display:column title="Volumen" property="volumen" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"></display:column>
					  	<display:column title="Fecha" property="fecha" decorator="com.asf.displayDecorators.DateDecorator"></display:column>
					  	<display:footer>
					  	<th>Saldo inmovilizado:</th>
					  	<td>
						  	<fmt:formatNumber pattern="0.00">
						  		${ProcessForm.process.credito.volumenInmovilizado}
						  	</fmt:formatNumber>
					  	</td>
					  	</display:footer>
					</display:table>
					
				</logic:equal> 
			</html:form>
		</div>
  </body>
</html>
