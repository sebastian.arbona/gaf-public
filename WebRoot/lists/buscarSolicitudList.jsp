<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<script language="javascript" type="text/javascript">
	/**
	 * @inputIn: control sobre el input que dispara la consulta ajax.-
	 * @defecto: para parametro=valor que tiene,
	 * a la izquierda del igual el id del input que dispara la consulta ajax.-
	 * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
	 */
	function actualizarLista(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&persona=" + inputIn.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}

	function actualizar(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&expediente=" + inputIn.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}
</script>

<div class="title">B�squeda de Solicitudes / Proyectos</div>

<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" value="list" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<br>
	<table border=0>
		<tr>
			<th>Nro de Proyecto migraci�n:</th>
			<td><asf:text name="ProcessForm"
					property="process.numeroCredito" type="text" maxlength="15"
					value="" /></td>
		</tr>
		<tr>
			<th>Nro de Solicitud / Proyecto:</th>
			<td><asf:text name="ProcessForm"
					property="process.numeroAtencion" type="Long" maxlength="6"
					value="" /></td>
		</tr>
		<tr>
			<th>Nro de Expediente:</th>
			<td><asf:autocomplete name="process.expediente" tipo="AJAX"
					idDiv="autocomplete_choices" idInput="expepago"
					classDiv="autocomplete" value="${ProcessForm.process.expediente}"
					options="{callback: actualizar, frequency: '0.8', minChars: '2'}"
					origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarExpediente" />
			</td>
		</tr>
		<tr>
			<th>Titular</th>
			<td><asf:autocomplete name="process.persona" tipo="AJAX"
					idDiv="autocomplete_persona" value="${ProcessForm.process.persona}"
					idInput="idpersona" classDiv="autocomplete"
					origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
					options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
					attribs="size='20%' title='Se puede buscar por Apellido y Nombre o Nro de documento'" />
				<span id="indicadorPersona" style="display: none"><img
					src="images/loader.gif" alt="Trabajando..." /></span> (C�digo - Apellido
				y Nombre - N�mero de Documento)</td>
		</tr>
	</table>
	<br>
	<html:submit>Buscar</html:submit>
	<br>
	<br>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.solicitudes"
			export="true" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:setProperty name="report.title"
				value="B�squeda de Solicitudes"></display:setProperty>
			<display:column property="numeroCredito" title="Nro de Cr�dito" />
			<display:column title="Nro Solicitud" media="html" sortable="true">
				<a
					href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.id}&process.action=buscar&process.personaId=${reportTable.persona.idpersona}">${reportTable.numeroAtencion}</a>
			</display:column>
			<display:column media="excel xml pdf" property="numeroAtencion"
				title="Nro solicitud" />
			<display:column property="persona.nomb12Str2" title="Titular" />
			<display:column property="expediente" title="Expediente" />
			<display:column property="linea.nombre" title="L�nea de Cr�dito" />
			<display:column title="Etapa"
				property="estadoActual.estado.nombreEstado" />
			<display:column title="Comportamiento Pago"
				property="comportamientoActual" />
		</display:table>
	</div>
</html:form>