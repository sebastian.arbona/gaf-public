<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Administración de Entes Recaudadores</div>
<p>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
</asf:security>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
<display:setProperty name="report.title" value="Administración de Bancos"></display:setProperty>

    <display:column property="id" title="Código" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.id" paramProperty="id" sortable="true"  />
    <display:column title="Nombre" property="detaBa" sortable="true" />
    <display:column title="Proveedor" property="proveedor.fantasia"  sortable="true" />    
	<asf:security action="/actions/bancosAction.do" access="do=list&entityName=ParametrizacionBancos">        
    <display:column media="html" title="Parametrizacion" >
    		<a href="javascript:listarParametrizacion( ${ reportTable.id },'${ reportTable.detaBa }' );">
    			<bean:message key="abm.button.parametrizar" />
    		</a>
    </display:column>
</asf:security>
	<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
		<display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete" /></display:column>
	</asf:security>
    
</display:table>
</div>

<script language="javascript">
function listarParametrizacion(id,descripcion)
{
	var url = "";

	url += '${pageContext.request.contextPath}/actions/bancosAction.do?';
	url += 'do=list';
	url += '&entityName=ParametrizacionBancos';
	url += '&codiBa=' + id;
	url += '&descripcion=' + descripcion;
	url += '&filter=true';
	window.location = url;
}
</script>