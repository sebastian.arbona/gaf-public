<%@ page language="java"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>


<script type="text/javascript" language="javascript">
  function resumenCobranza() {
	  $('accion').value = 'resumenCobranza';
  }
  function resumenCobranzaAnalitico() {
	  $('accion').value = 'resumenCobranzaAnalitico';
  }
</script>
<div class="title">Consulta de Lotes</div>
  ${param.processName}
<br><br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.idCaratula" value="${ProcessForm.process.idCaratula}"/>
	<html:hidden property="process.accion" styleId="accion" value="imprimirCobranza"/>

<html:errors/>

<br/>
<div align="center">
	<input type="submit" id="imprimirCobranza" value="Imprimir Cobranza"/>
	<input type="submit" id="imprimirCobranza" value="Resumen Cobranza" onclick="resumenCobranza();"/>
	<input type="submit" id="imprimirCobranza" value="Resumen Cobranza Analitico" onclick="resumenCobranzaAnalitico();"/>
</div>
<br/>

<display:table name="ProcessForm" property="process.resultado" export="true" id="row" 
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.idCaratula=${ProcessForm.process.idCaratula}">
	<display:setProperty name="report.title" value="Consulta de Lotes" />
	<display:column title="Nro. Lote" sortable="true">${row[0]}</display:column>
	<display:column title="Nro. Pagos" sortable="true" >${row[2]}</display:column>
	<display:column title="Nro. Boletos" sortable="true" >${row[3]}</display:column>
	<logic:equal name="ProcessForm" property="process.metodoCarga" value="2">
		<display:column title="C�digo de Operaci�n" sortable="true" >${row[4].recaudacion.codigoOperacion}</display:column>
	</logic:equal>
	<display:column title="Total" sortable="true" align="right" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"><%= String.format("%,.2f", ((Object[])row)[1]) %></display:column>	
	<display:column title="Opciones" sortable="false" align="center" media="html">

	<c:set var="cons" scope="page"><logic:present parameter="consultaLotes">&consultaLotes</logic:present></c:set>
	<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.nroLote=${row[0]}&process.metodoCarga=${ProcessForm.process.metodoCarga}&process.idCaratula=${ProcessForm.process.idCaratula}${cons}">Ver Detalles</a>&nbsp;
	<logic:notPresent parameter="consultaLotes">
	<a onclick="return confirmDelete();" href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.nroLote=${row[0]}&process.metodoCarga=${ProcessForm.process.metodoCarga}&process.idCaratula=${ProcessForm.process.idCaratula}&process.delete=true">Eliminar</a></logic:notPresent></display:column>	
</display:table>
</html:form>
<div align="center">
<button onclick="window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=CobroPagos&process.fenv12Str=${ProcessForm.process.caratula.fechaEnvioStr}';">Volver</button>    
</div>
