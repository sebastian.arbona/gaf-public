<%@ page language="java"%>
<%@ page import="com.asf.util.DateHelper" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Administración de Cobranzas</div>
<br><br>

<html:form styleId="cd" action="/actions/abmActionCaratula.do?entityName=Caratula&filter=true">
    <html:errors/>
    <table border="0">
<tr id="fecha"><th>Fecha:</th><td><asf:calendar property="paramValue[0]" value="${paramValue[0]}" /></td></tr>
    <html:hidden property="paramName[0]" styleId="paramName" value="fechaEnvioStr"/> 
    <html:hidden property="paramComp[0]" styleId="paramComp" value="="/>
<%--<tr><th>Fecha Hasta:</th><td><asf:calendar property="paramValue[1]" value="${paramValue[1]}" /></td></tr>--%>
<%--    <html:hidden property="paramName[1]" styleId="paramName" value="fenv12Str"/> --%>
<%--    <html:hidden property="paramComp[1]" styleId="paramComp" value="<="/>--%>
</table>
    <html:hidden property="filter" value="true"/>
    <html:hidden property="do" styleId="do" value="list"/>
    <asf:security action="/actions/abmActionCaratula.do" access="do=newEntity&entityName=${param.entityName}">
        <html:submit onclick="return Nuevo('paramValue[0]');"><bean:message key="abm.button.new"/></html:submit>
    </asf:security>
    <html:submit onclick="getElem('do').value='list';">Consultar</html:submit>
   
</html:form>
<br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1" requestURI="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=list&entityName=${param.entityName}&filter=true">
    <display:setProperty name="report.title" value="Administración de Cobranzas"/>
    <display:caption>
     Fecha: ${reportTable.fechaEnvioStr}
    </display:caption>
 

 	<display:column title="Grupo" sortProperty="grupo" sortable="true" media="html" >
	    <logic:equal name="reportTable" property="actualizada" value="0" >
	    	<a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=load&entityName=${param.entityName}&entity.id=${reportTable.id}" paramId="entity.id" paramProperty="id" sortable="true">${reportTable.grupo}</a>
	    </logic:equal>
	    <logic:notEqual name="reportTable" property="actualizada" value="0">
		    ${reportTable.grupo}
	    </logic:notEqual>
    </display:column>
    <display:column title="Nro de Caja" sortProperty="grupo" sortable="true" media="excel pdf xml" >${reportTable.grupo}</display:column>
    <display:column title="Ente Recaudador" property="banco.detaBa" sortable="true" />
    <display:column title="Fecha Cobranza" property="fechaCobranzaStr" sortable="true" />
    <display:column title="Total Comprobantes" property="comprobantes" sortable="true" />
    <display:column title="Importe Total" property="importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
   
  
    <asf:security action="/actions/process.do" access="do=process&processName=CierreCaja">
    <display:column media="html" title="Cierre de Caja" >
        <logic:equal name="reportTable" property="actualizada" value="0">      
         <a href="actions/abmActionCaratula.do?do=cerrarCaja&entityName=Caratula&filter=true&entity.id=${reportTable.id}&entity.fechaEnvioStr=${reportTable.fechaEnvioStr}">Cierre de Caja</a>
         </logic:equal>
	   <logic:notEqual name="reportTable" property="actualizada" value="0">
	  	 ${reportTable.fechaActualizacionStr}
	   </logic:notEqual>
	</display:column>
    </asf:security> 
  
  
    <asf:security action="/actions/abmActionCaratula.do" access="do=delete&entityName=${param.entityName}">
      <display:column media="html" title="Eliminar" >
    	<logic:equal name="reportTable" property="actualizada" value="0">
    		<a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=delete&entityName=${param.entityName}&entity.id=${reportTable.id}&filter=true" onclick="return confirmDelete();" >Eliminar</a>
		</logic:equal>
	 </display:column>
	</asf:security>

</display:table>
</div>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>