<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>

<div class="title">
	Pagos Realizados Al Ente Financiador
</div>
<br>
<br>

	<html:form 
		action="actions/process.do?processName=${param.processName}&do=process"
		styleId="formulario">
		<tr>
        <th>
          Ente Financiador:
        </th>
        <td>
        <logic:equal value="false" property="process.ente" name="ProcessForm">
        La bonificacion no tiene Pagos
        </logic:equal>
        <logic:equal value="true" property="process.ente" name="ProcessForm">
          <asf:text property="ProcessForm.process.bonificacion.banco.codiBa" type="Long" maxlength="6" 
            readonly="true"
          />
         </logic:equal>
        </td>
     	</tr>
     </html:form>