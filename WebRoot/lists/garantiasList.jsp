<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<div class="title">Administración de Garantías</div>
<br>
<br>
<html:form
	action="actions/process.do?processName=${param.processName}&do=process"
	styleId="oForm">
	<html:hidden property="process.accionStr" name="ProcessForm"
		value="NUEVA_GARANTIA" styleId="accionStr" />
	<html:hidden property="process.idSolicitud" name="ProcessForm" />
	<html:hidden property="process.oGarantiaId" styleId="oGarantiaId"
		name="ProcessForm" />

	<logic:equal name="ProcessForm" property="process.ocultarCampos"
		value="true">
		<button type="submit" disabled="disabled">
			<bean:message key="abm.button.new" />
		</button>
	</logic:equal>

	<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
		value="true">
		<button type="submit">
			<bean:message key="abm.button.new" />
		</button>
	</logic:notEqual>
	<br>

	<div class="grilla">
		<display:table name="ProcessForm" property="process.result"
			export="true" id="reportTable" defaultsort="1"
			requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.idSolicitud=${ProcessForm.process.idSolicitud }">
			<display:caption>Proyecto ${ProcessForm.process.credito.numeroAtencion} - ${ProcessForm.process.credito.linea.nombre} - ${ProcessForm.process.credito.persona.nomb12}</display:caption>
			<display:setProperty name="report.title"
				value="Administración de Garantías"></display:setProperty>
			<display:column title="Tipo Garantia" sortable="true"
				sortProperty="garantia.tipo.id">
  				${reportTable.garantia.tipo.id} ${reportTable.garantia.tipo.nombre}
  			</display:column>
			<display:column property="garantia.identificadorUnico" title="ID"
				sortProperty="garantia.identificadorUnico"
				paramId="process.oGarantiaId" paramProperty="id" sortable="true"
				href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accionStr=MODIFICAR_GARANTIA&process.idSolicitud=${ProcessForm.process.idSolicitud}" />
			<display:column title="Estado" media="html">
				<a href="#"
					onclick="popUp('${pageContext.request.contextPath}/actions/garantiaEstadoAction.do?do=list&entityName=GarantiaEstado&paramName[0]=objetoiGarantia.id&filter=true&paramComp[0]==&paramValue[0]=${reportTable.id}');return false;">${reportTable.estadoEntStr}</a>
			</display:column>
			<%--Agrego columna con el valor sin el link. Ticket 9058--%>
			<display:column title="Estado" media="excel pdf xml">
				${reportTable.estadoEntStr}
			</display:column>
			<display:column property="valor" title="Valor"
				decorator="com.asf.displayDecorators.DoubleDecorator"
				sortable="true" />
			<display:column title="Producto" property="garantia.producto"
				sortable="true" />
			<display:column title="Tipo Prod."
				property="garantia.tipoProductoNombre" sortable="true" />
			<display:column property="observacion" title="Descripcion"
				sortable="true" />
			<display:column media="html" title="Ver Titulares"
				paramId="process.id" paramProperty="id"
				href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=VerTitulares&process.id=${ProcessForm.process.garantia.id}&process.idObjetoi=${ProcessForm.process.idSolicitud}"> 
					Ver Titulares </display:column>
			<display:column media="html" title="Adjuntos"
				paramId="process.idObjetoiGarantia" paramProperty="id"
				href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=GarantiaArchivoProcess"> 
					Adjuntos</display:column>
			<logic:equal name="ProcessForm" value="CANCELADO"
				property="process.credito.estadoActual.estado.nombreEstado">
				<display:column title="Liberacion" media="html">
					<logic:notEqual name="reportTable" value=""
						property="fechaLiberadaStr">
					${reportTable.fechaLiberadaStr}
				</logic:notEqual>
					<logic:equal name="reportTable" value=""
						property="fechaLiberadaStr">
						<a href="javascript:void(0);"
							onclick="liberar(${reportTable.id});">LIBERAR</a>
					</logic:equal>
				</display:column>
			</logic:equal>
			<logic:equal value="ANALISIS" name="ProcessForm"
				property="process.credito.estadoActual.estado.nombreEstado">
				<asf:security
					action="/actions/process.do?processName=${param.processName}&do=process&process.accionStr=ELIMINAR_GARANTIA">
					<display:column media="html" honClick="return confirmDelete();"
						title="Eliminar"
						href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accionStr=ELIMINAR_GARANTIA&process.idSolicitud=${ProcessForm.process.idSolicitud }"
						paramId="process.oGarantiaId" paramProperty="id">
						<bean:message key="abm.button.delete" />
					</display:column>
				</asf:security>
			</logic:equal>
		</display:table>
	</div>
	<logic:equal value="true" property="process.hayFiduciaria"
		name="ProcessForm">
		<br />
		<div align="center" style="font-size: 14; color: red;">
			Liberaciones <br />
			<display:table name="ProcessForm" property="process.liberaciones"
				align="center">
				<display:column title="Volumen" property="volumen"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"></display:column>
				<display:column title="Fecha Sol." property="fecha"
					decorator="com.asf.displayDecorators.DateDecorator"></display:column>
				<display:column title="Fecha Acept." property="fechaAceptacion"
					decorator="com.asf.displayDecorators.DateDecorator"></display:column>
				<display:column title="Producto" property="nombreProducto" />
				<display:column title="Estado" property="estado.texto" />
				<display:column title="Aforo" property="aforo"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Precio Producto" property="precioProducto"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Importe" property="importe"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			</display:table>
		</div>
		<div align="center" style="font-size: 14; color: red;">
			Inmovilizaciones <br />
			<display:table name="ProcessForm" property="process.inmovilizaciones"
				align="center">
				<display:column title="Volumen" property="volumen"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"></display:column>
				<display:column title="Fecha Sol." property="fecha"
					decorator="com.asf.displayDecorators.DateDecorator"></display:column>
				<display:column title="Fecha Acept." property="fechaAceptacion"
					decorator="com.asf.displayDecorators.DateDecorator"></display:column>
				<display:column title="Producto" property="nombreProducto" />
				<display:column title="Estado" property="estado.texto" />
				<display:column title="Aforo" property="aforo"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Precio Producto" property="precioProducto"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:column title="Importe" property="importe"
					decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
				<display:footer>
					<th>Saldo inmovilizado:</th>
					<td colspan="4"><fmt:formatNumber pattern="0.00">
		  		${ProcessForm.process.saldo}
		  	</fmt:formatNumber></td>
				</display:footer>
			</display:table>
		</div>
	</logic:equal>
</html:form>
<script>
var form = $('oForm');
var accion = $('accionStr');
var oGarantiaId = $('oGarantiaId');
function liberar(id){
	if(confirm('Está seguro de querer liberar la garantía?')){
		accion.value = 'LIBERAR_GARANTIA';
		oGarantiaId.value = id; 
		form.submit();
	}
}
</script>
