<%@ page language="java"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt.tld" prefix="fmt"%>

<div class="title">Consulta de Lote </div> 
<br>

<html:form action="/actions/process.do?do=process&processName=${param.processName}">
<html:hidden property="process.nroLote" value="${ProcessForm.process.nroLote}" styleId="nroLoteId"/>
<html:hidden property="process.idCaratula" value="${ProcessForm.process.idCaratula}"/>

<logic:notEmpty name="ProcessForm" property="process.resultado[1]">
<br/>
	
	<display:table name="ProcessForm" property="process.resultado[1]" export="true" id="row" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.idCaratula=${ProcessForm.process.idCaratula}&process.nroLote=${ProcessForm.process.nroLote}">
		<display:caption >
			Ente Recaudador: ${ProcessForm.process.caratula.banco.detaBa}
		</display:caption>
		<display:column title="Recibo" media="pdf excel">${row[1]}</display:column>
		<display:column title="Recibo" media="html">
			<%if(((Object[])row)[2]!=null){%>
				<a href="javascript:void(0);" onclick="imprimir(${row[0]}, 'medioPago', ${ProcessForm.process.idCaratula});">${row[1]}</a>
			<%}%>
			</display:column>
		<display:column title="Proy.">${row[6]}</display:column>
		<display:column title="Titular">${row[5]}</display:column>
		<display:column title="CUIT/CUIL">${row[16]}</display:column>
		<display:column title="Expediente">${row[17]}</display:column>
		<display:column title="L�nea">${row[18]}</display:column>
		<display:column title="Moneda">${row[19]}</display:column>
		<display:column title="Cotiz." class="right">
			<fmt:formatNumber value="${row[20]}" minFractionDigits="2" maxFractionDigits="2" />
		</display:column>
		<display:column title="Fecha Pago">
			<fmt:formatDate value="${row[7]}" pattern="dd/MM/yyyy"/>
		</display:column>
		<display:column title="Capital" class="right"> <%= String.format("%,.2f",((Object[])row)[8])%> </display:column>
		<display:column title="Comp." class="right"><%= String.format("%,.2f",((Object[])row)[9])%></display:column>
		<display:column title="Mor." class="right"><%= String.format("%,.2f",((Object[])row)[10])%></display:column>
		<display:column title="Pun." class="right"><%= String.format("%,.2f",((Object[])row)[11])%></display:column>
		<display:column title="Bon." class="right"><%= String.format("%,.2f",((Object[])row)[12])%></display:column>
		<display:column title="Gas." class="right"><%= String.format("%,.2f",((Object[])row)[13])%></display:column>
		<display:column title="Mul." class="right"><%= String.format("%,.2f",((Object[])row)[14])%></display:column>
		<display:column title="Gas.Rec." class="right"><%= String.format("%,.2f",((Object[])row)[15])%></display:column>
		<display:column title="Importe" class="right">
			<fmt:formatNumber value="${row[4]}" minFractionDigits="2" maxFractionDigits="2" />
		</display:column>
	</display:table>
</logic:notEmpty>
<br>
<%-- <logic:empty name="ProcessForm" property="process.resultado[0]"> --%>
<!-- 	No hay datos para mostrar -->
<%-- </logic:empty>  --%>
<%-- <logic:notEmpty name="ProcessForm" property="process.resultado[0]"> --%>
<%-- 	<display:table name="ProcessForm" property="process.resultado[0]" export="true" id="row" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.idCaratula=${ProcessForm.process.idCaratula}&process.nroLote=${ProcessForm.process.nroLote}"> --%>
<%-- 	<display:setProperty name="report.title" value="Consulta de Lote" /> --%>
<%-- 	<display:caption> --%>
<%-- 		   <logic:iterate id="bole" name="ProcessForm" property="process.resultado[1]"> --%>
<%-- 		   		${bole[0]}-${bole[1]}-${bole[2]}  ${bole[3]}</logic:iterate> --%>
<%-- 	</display:caption> --%>
	
<%-- 	<display:column title="Medio" property="mediopago.denominacion" sortable="true" /> --%>
<%-- 	<display:column title="Entidad Emisora" property="entidadEmisora" sortable="true" /> --%>
<%-- 	<display:column title="Nro. Comprobante" property="numeroComprobante" sortable="true" /> --%>
<%-- 	<display:column title="Fecha Emisi�n" property="fechaEmisionStr" sortable="true" /> --%>
<%-- 	<display:column title="Fecha Vencimiento" property="fechaVencimientoStr" sortable="true" /> --%>
<%-- 	<display:column title="Cuotas" property="cuotas" sortable="true" /> --%>
<%-- 	<display:column title="Plan" property="plan" sortable="true" /> --%>
<%-- 	<display:column title="Autorizaci�n" property="autorizacion" sortable="true" /> --%>
<%-- 	<display:column title="Importe" property="importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" /> --%>
<%-- 	</display:table> --%>
<%-- </logic:notEmpty> --%>
</html:form>
<div align="center">
<c:set var="cons" scope="page">
<logic:present parameter="consultaLotes">&consultaLotes</logic:present>
</c:set>
<button onclick="window.location='${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.metodoCarga=${ProcessForm.process.metodoCarga}&process.idCaratula=${ProcessForm.process.idCaratula}${cons}';">Volver</button>
<script type="text/javascript" language="javascript">
function imprimir(idBoleto, medioPago, idCaratula) 
{
    window.location =         
    '${pageContext.request.contextPath}/actions/process.do?do=process'+
    '&processName=${param.processName}'+
    '&process.accion=imprimir'+
    '&process.idBoleto='+idBoleto+
    '&process.idCaratula='+idCaratula+
    '&process.nroLote='+$('nroLoteId').value+
    '&process.metodoCarga='+${ProcessForm.process.metodoCarga}+'${cons}';             
}
</script>    
</div>