<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" /> 
	<logic:empty name="ProcessForm" property="process.creditos">
		No hay datos
	</logic:empty>
    <logic:notEmpty name="ProcessForm" property="process.creditos">
    	<display:table name="ProcessForm" property="process.creditos" id="credito">
            <display:column title="Nro Solicitud" media="html" sortable="false" >
                <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${credito.idObjetoi}&process.action=buscar&process.personaId=${ProcessForm.process.persona.idpersona}" target="_parent">${credito.numeroAtencion}</a>
            </display:column>
            <display:column title="L�nea" property="linea" sortable="false"/>
            <display:column title="Expediente N�" property="expediente" sortable="true"/>
            <display:column title="Etapa" property="estado" sortable="flase"/>
			<display:column title="Comportamiento Pago" property="comportamiento" sortable="false"/>            
            <display:column title="Deuda" property="totalAdeudadoStr" sortable="false"/>
    	</display:table>
    </logic:notEmpty>
</html:form>

	
 