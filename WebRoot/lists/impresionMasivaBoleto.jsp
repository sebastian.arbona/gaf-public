<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

<div class="title" style="margin-bottom: 30px">Emisi�n Masiva de Boletos</div>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
<html:hidden property="process.accion" styleId="accion"/>
<html:hidden property="process.seleccionados" styleId="seleccionados"/>
<html:hidden property="process.emision.id" styleId="emision_id"/>

<html:errors/>

<input type="button" value="Listar Liquidaciones" onclick="listar();">
<br/>
<br/>
<div class="grilla">
	<logic:notEmpty name="ProcessForm" property="process.emisiones">
	<display:table name="ProcessForm" property="process.emisiones" id="reportTable">
		<display:column title="Orden">
			<a href="javascript:void(0);" onclick="seleccionar(${reportTable.id});">
				${reportTable.ordenEmision}
			</a>
		</display:column>
		<display:column property="fechaEmisionStr" title="Fecha"/>
		<display:column property="cantidad" title="Cant. Cuotas"/>		
	    <display:column property="importe" title="Importe"/>
	    <display:column title="Impactado">
	    	<logic:equal property="facturada" name="reportTable"  value="true">
	    		SI
	    	</logic:equal>
	    	<logic:equal property="facturada" name="reportTable" value="false">
	    		NO
	    	</logic:equal>
	    </display:column>
	    <display:column property="criterio" title="Liquida Por"/>
	    <display:column property="moneda" title="Moneda"/>
	    <display:column property="fechaHasta" title="Fecha Hasta"/>
	    <display:column property="valorDesde" title="Valor Desde"/>
	    <display:column property="valorHasta" title="Valor Hasta"/>
	    <display:column property="usuario" title="Usuario"/>
	    
    </display:table>
    </logic:notEmpty>
</div>
<div class="grilla">
	<logic:notEmpty name="ProcessForm" property="process.emidetas">
	<table>
	<tr>
	     <th>
			Ordenar por:
	   	 </th>
	   	 <td>
			<html:select property="process.ordenarPor" name="ProcessForm">
				<html:option value="0">&lt;&lt;Sin orden&gt;&gt;</html:option>
				<html:option value="1">Localidad</html:option>
				<html:option value="2">Departamento</html:option>
				<html:option value="3">L�nea de Cr�dito</html:option>
			</html:select>			
	   	 </td>
   	 </tr>
   	 <tr>
	     <th>
			Tipo de orden:
	   	 </th>
	   	 <td>
			<html:select property="process.tipoOrden" name="ProcessForm">
				<html:option value="1">Ascendente</html:option>
				<html:option value="2">Descendente</html:option>
			</html:select>			
	   	 </td>
   	 </tr>
	</table>
	<input type="button" value="Ordenar" onclick="ordenar();">
	<br/>
	<br/>
	<table>
	<tr>
      <th>Leyenda:</th>
		<td>
			<asf:select entityName="com.nirven.creditos.hibernate.Leyenda" listCaption="nombre"
			listValues="id" orderBy="nombre" name="process.leyenda"
			nullValue="true" nullText="&lt;&lt;Seleccione Leyenda&gt;&gt;"
			value="${ProcessForm.process.emision.leyenda.id}"
			/>
		</td>
	</tr>
	</table>
	<input type="button" value="Imprimir" onclick="imprimir();">
	<input type="button" value="Informe Entes Recaudadores" onclick="emisiones();">
	<br/>
	<br/>
	<table>
	<tr>
      <th>Seleccionar notificaci�n mail:</th>
		<td>
			<asf:select entityName="com.nirven.creditos.hibernate.ConfiguracionNotificacion" listCaption="denominacion"
			listValues="id" orderBy="denominacion" name="process.idCn"
			nullValue="true" nullText="&lt;&lt;Seleccione Notificaci�n&gt;&gt;"
			value="${ProcessForm.process.idCn}"
			/>
		</td>
	</tr>
	</table>
	<br/>
	<br/>
    <div class="grilla">
        <table>
            <tr>
                <td align="left" >Seleccionar Todos</td>
                <td align="left" ><input id="seleccionarTodos" type="checkbox" title="Seleccionar Todos" onclick="checkAll()"/></td>
            </tr>
        </table>
    </div>
	<display:table name="ProcessForm" property="process.emidetas" id="reportTable" export="true"
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
		<display:column media="excel xml pdf" property="id" title="ID"/>
        <display:column title="Enviar Mail" align="center" media="html">
            <input type="checkbox" name="emideta-${reportTable.id}"
            value="${reportTable.id}" class="chkSelect" id="chk${reportTable.id}" onclick="agregarContacto(${reportTable.id})"/>
        </display:column>
		<display:column property="credito.numeroAtencion" title="Proyecto"/>
		<display:column property="numero" title="N.Cuota"/>
<%--		<display:column property="boleto.numeroBoleto" title="Nro."/>--%>
<%--		<display:column property="boleto.verificadorBoleto" title="Verif."/>--%>
<%--		<display:column property="boleto.periodoBoleto" title="Periodo"/>--%>
		<display:column title="Titular"  property="credito.persona.nomb12"/>
		<display:column property="credito.linea.nombre" title="Linea"/>
		<display:column title="Localidad"  property="localidad" />
		<display:column title="Departamento"  property="departamento" />
		<display:column title="Domicilio"  property="obtenerDomicilioEspecial"/>
		<display:column title="C.P"  property="codigoPostal"/>
		<display:column title="Contacto"  property="credito.persona.emailPrincipalNotificacion"/>
    </display:table>
    <br/>
    <input type="button" value="Enviar Mail" onclick="enviarMails();">
    </logic:notEmpty>
</div>
</html:form>
<script>
var formulario = $('oForm');
var accion = $('accion');
var emision_id = $('emision_id');
var url = "";
var array = new Object();
var seleccionados = ' ';
var elementos = 0;

function listar() {
	accion.value = 'listar';
	formulario.submit();
}
function seleccionar(id) {
	accion.value = 'seleccionar';
	emision_id.value = id;
	formulario.submit();	
}
function ordenar() {
	accion.value = 'ordenar';
	formulario.submit();
}
function imprimir() {
	accion.value = 'imprimir';
	formulario.submit();
}
function emisiones(){
       url += "${pageContext.request.contextPath}/actions/process.do?processName=EmisionesGeneradasProcess&do=process";
       window.location = url;
}

function enviarMails(){

    
	if(elementos == 0){
		
		alert("Debe seleccionar al menos un boleto");
	
	}else{
	
			for (var i = 0; i < getElem('oForm').elements.length; i++) {
		  		getElem('oForm').elements[i].checked = false;
			}
	
			convertirSeleccionados();
			accion.value = 'enviarMails';	
			getElem('seleccionados').value = seleccionados; 
	  		formulario.submit();
	
         }
	
	}




function agregarContacto(id){

    var codigo = 'chk'+id;
   
    if(getElem(codigo) != null){
		  if(getElem(codigo).checked){
				array[codigo] = id;	
				elementos++;
				
			}else{
				
				delete array[codigo];
				elementos--;
				getElem('seleccionarTodos').checked = false;
		  }
	}
	 
}

function convertirSeleccionados(){

	for (var indice in array){
		
			if(seleccionados == ' '){
				seleccionados = array[indice];
			}else{
				seleccionados = seleccionados + ', '+array[indice];
			}			
	}	

}

function checkAll(){

	if(getElem('seleccionarTodos').checked){
		for (var i = 7; i < getElem('oForm').elements.length; i++) {
	  		getElem('oForm').elements[i].checked = true;
	  		var id = getElem('oForm').elements[i].value;
	  		if(!isNaN(id) && id != ''){
	  			agregarContacto(id);
	  		}
		}
		
	}else {
		for (var i = 7; i < getElem('oForm').elements.length; i++) {
	  		getElem('oForm').elements[i].checked = false;
	  		var id = getElem('oForm').elements[i].value;
	  		if(!isNaN(id) && id != ''){
	  			agregarContacto(id);
	  		}
		}
	}

}


</script>