<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<br>
<html:form
	action="/actions/process.do?do=process&processName=BonTasaRequisitos"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />
	<html:hidden property="process.idBonTasa" styleId="idBT" />
	<html:hidden property="process.esPersona" styleId="esPer" />
<br>
<br>
	<div class="grilla">
			<display:table name="ProcessForm" property="process.requisitos"
				defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.CargaRequisitoBon" export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
				<display:column media="excel xml pdf" property="id" title="ID"/>				
				<display:column property="id" title="ID" sortProperty="id" paramId="process.idRequisito" paramProperty="id" sortable="true"
					href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaRequisitos&process.accion=cargarRequisito"/>
				<display:column property="requisito.nombre" title="Requisito"
					sortable="false" />
				<display:column property="fechaCumplimientoStr" title="Cumplimiento"
					sortable="false" />
				<display:column property="observaciones" title="Observaciones"
					sortable="false" />
				<display:column property="archivo" title="Archivo" sortable="false" />
				<display:column title="Eliminar" media="html">
				<logic:notEmpty property="fechaCumplimiento" name="reportTable"> 
                	<a href="javascript: eliminar( ${reportTable.id} );" >Eliminar</a>
 				</logic:notEmpty>
            </display:column>
				<display:column title="Selecci�n">
					<logic:empty name="reportTable" property="fechaCumplimientoStr">
						<input type="checkbox" name="requisito-${reportTable.requisito.id}" value="${reportTable.requisito.id}" />
					</logic:empty>
					<logic:notEmpty name="reportTable" property="fechaCumplimientoStr">
						<html:checkbox name="reportTable" property="chec" value="chec" disabled="true"/>
					</logic:notEmpty>
				</display:column>
				
		</display:table>		
		<br>
			<html:submit value="Guardar" styleId="bGuardar" onclick="guardar();" />
		<br>
		<br>
		<br>
		<br/>
		<br/>
		<br/>
			
		
	</div>
</html:form>
<script type="text/javascript">
	function eliminar(id) 
    {
    	if(confirmDelete())
    	{
			var url = '';
	        url += '${pageContext.request.contextPath}/actions/process.do?';
	        url += 'do=process&';
	        url += 'processName=${param.processName}&';
	        url += 'process.accion=eliminar&process.idBonTasa=${ProcessForm.process.idBonTasa}&';	       
	        url += 'process.idRequisito=' + id;
	        window.location = url;
    	}
    }//fin eliminar.-
	function guardar(){
		var accion = document.getElementById("accion");		
		var idBonTasa = document.getElementById("idBT");
		var formulario = document.getElementById("oForm");
		accion.value="guardar";
		idBonTasa = '${ProcessForm.process.idBonTasa}';
		formulario.submit();
	}
	
</script>