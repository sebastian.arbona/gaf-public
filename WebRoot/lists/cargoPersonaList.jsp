<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" value="asignar" />
  <html:hidden property="process.personaTitular.idpersona" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.idpersona}" />
  <div style="width: 70%" align="left">
    <html:errors />
  </div>
  <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=asignar">
    <html:submit>
      <bean:message key="abm.button.new" />
    </html:submit>
	</asf:security>
  <br>
  <br>
  <logic:notEmpty name="ProcessForm" property="process.listaCargos">
    <display:table name="ProcessForm" property="process.listaCargos" pagesize="5" id="reportTable" export="true"
      requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar" defaultsort="4" defaultorder="descending">
      <display:caption>Contactos de: ${ProcessForm.process.personaTitular.id} - ${ProcessForm.process.personaTitular.nomb12}</display:caption>
      <display:column title="ID" sortProperty="id" sortable="true" media="html">
        <a href="javascript: load( ${reportTable.id} );">${reportTable.id}</a>
      </display:column>
      <display:column property="cargo.nombreCargo" title="Cargo"/>
      <display:column property="normaLegal" title="Norma Legal Cargo Alta"/>
      <display:column property="normaLegalBaja" title="Norma Legal Cargo Baja"/>
      <display:column property="fechaStr" title="Fecha Inicio" sortable="true" />
      <display:column property="fechaHastaStr" title="Fecha Fin" sortable="true" />
      <display:column property="observacion" title="Observaciones"/>
      <display:column title="Eliminar" media="html">
        <a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>
      </display:column>
    </display:table>
   
  </logic:notEmpty>
  <logic:empty name="ProcessForm" property="process.listaCargos">
        La Persona: ${ProcessForm.process.personaTitular.id} - ${ProcessForm.process.personaTitular.nomb12} no tiene cargos asignados.
    </logic:empty>
</html:form>

<script type="text/javascript">

function eliminar(id) {
	if (confirmDelete()) {
		var url = '';

		

		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=CargosProcess&';
		url += 'process.accion=elimnarCargoPersona&';
		url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
		url += 'process.idCargoPersona=' + id;
		
		window.location = url;

	}
}//fin eliminar.-

function load(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
	url += 'process.idCargoPersona=' + id;

	window.location = url;
}//fin load.-

Event.observe(window, 'load', function() {
	this.name = "CargoPersona";
	parent.pop(this);
});
</script>