<%@ page language="java"%>
<%@page import="org.jbpm.taskmgmt.exe.TaskInstance"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/jbpm.tld" prefix="jbpm"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.7.1.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-ui-1.8.18.custom.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.ui.datepicker-es.js"></script>

<div class="title">Lista de Tareas Pendientes</div>
<br/><br/>
		        
    <html:form styleId="fExport" method="post" action="actions/jbpm.do?do=showTaskInstanceList">		        
        <html:hidden property="reportName" value="${reportResult.reportName}" />
        <html:hidden property="params" value="${reportResult.params}" />
        <html:hidden property="export" value="${reportResult.export}" />
        <html:hidden property="classBeanName" value="${reportResult.classBeanName}" />	
        
        <table>
		<tr>
			<th>
				Proceso:
			</th>
			<td>
				 <asf:select name="processName" value="${JBPMForm.processName}" entityName="com.nirven.jbpm.persistance.Tramite" listCaption="nombre" listValues="processDefinitionName" filter="processDefinitionName!=''" nullValue="true" nullText="TODOS"></asf:select>
			</td>
		</tr>
		<tr>
			<th>
				Fecha Desde:
			</th>
			<td>
				<asf:calendar property="JBPMForm.fechaDesdeStr" />
				
			</td>
		</tr>
		<tr>
			<th>
				Fecha Hasta:
			</th>
			<td>
				<asf:calendar property="JBPMForm.fechaHastaStr" />
			</td>
		</tr>
		<tr>
			<th>Vencimiento:</th>
			<td colspan="1">
				<html:checkbox styleId="eAce" property="est_vencida" value="true">Vencidas</html:checkbox>
			</td>
		</tr>
		<tr>
			<th>
				Expresión Proceso:
			</th>
			<td>
				<asf:text property="JBPMForm.expresion" 
			type="Text" maxlength="20" value="${JBPMForm.expresion}" />
			</td>	
		</tr>		
	</table>
	<br>
	
	<html:submit>Consultar</html:submit>
	
	<br>
	
        	        
    </html:form>
    


<div class="grilla">
	<display:table name="TaskInstanceList" export="true" id="task" requestURI="${pageContext.request.contextPath}/actions/jbpm.do?do=showTaskInstanceList"
		defaultsort="1" >
		<display:column property="priority" title="Prioridad" sortable="true"/>
		<display:column property="name" title="Tarea"  sortable="true" />
		<display:column  title="Proceso">
			<jbpm:taskTitle name="task"/>
		</display:column>
		<display:column property="actorId" title="Responsable" sortable="true" />
		<display:column property="create" title="Inicio" decorator="com.nirven.jbpm.displayDecorators.DateDecorator" sortable="true"/>
		<display:column property="dueDate" title="Vencimiento" decorator="com.nirven.jbpm.displayDecorators.DueDateDecorator" sortable="true"/>

		<display:column title="Acciones" media="html" sortable="true">
			<a href="${pageContext.request.contextPath}/actions/jbpm.do?do=showTaskInstance&id=${task.id}">Abrir</a>
		</display:column>
	</display:table>
</div>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
