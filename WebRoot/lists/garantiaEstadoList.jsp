<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>

<div class="title">Historico de Estados de Garant�as</div>
<br>

<html:form action="actions/garantiaEstadoAction.do?entityName=${param.entityName}&filter=true" >
	<html:hidden property="do" styleId="accion" value="newEntity" />
	<html:hidden styleId="objetoiGarantia" property="objetoiGarantia" value="${paramValue[0]}"/>
	
	<html:hidden property="paramValue[0]" styleId="paramValue"	value="${paramValue[0]}" />
	<html:hidden property="paramName[0]" styleId="paramName" value="${paramName[0]}" />
	<html:hidden property="paramComp[0]" styleId="paramComp" value="${paramComp[0]}" />
	<html:hidden property="filter" value="true" />
	<asf:security action="/actions/garantiaEstadoAction.do" access="do=newEntity&entityName=${param.entityName}" >
		<button type="submit" ><bean:message key="abm.button.new"/></button>
	</asf:security>
	

</html:form>

<br>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable"  requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}" defaultsort="3" defaultorder="descending">
<display:setProperty name="report.title" value="Historico de Estados de Garant�as"></display:setProperty>
	<display:column title="Estado" property="estado.nombreEstado"></display:column>
	<display:column title="Fecha Cambio Estado" property="fechaEstado"  decorator="com.asf.displayDecorators.DateDecorator"></display:column>
	<display:column title="Fecha Proceso" property="fechaProceso"  decorator="com.asf.displayDecorators.DateDecorator"></display:column>
	<display:column title="Usuario" property="usuario"></display:column>
	<display:column title="Importe" property="importe" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
    
</display:table>
<input type="button" onclick="window.close();" value="Cerrar"/>
</div>

