<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/FCKeditor.tld" prefix="FCK" %>
<div class="title" style="margin-bottom: 15px">Consulta General de Etapas</div>
</div>
<br>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm" enctype="multipart/form-data">
	<html:hidden property="process.idSolicitud" styleId="id" />
	<html:hidden property="process.accion" styleId="accion" />
  <br/>
   <display:table name="ProcessForm" property="process.listaEtapas" export="true" id="reportTable" defaultsort="1" class="com.asf.cred.business"
   requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
		<display:caption>Consulta General de Etapas</display:caption>		
      
      <display:column property="nombreEtapa" title="Etapa" sortable="true" />
      <display:column property="tarea" title="Tarea" sortable="true" />
       <display:column property="sector" title="Sector" sortable="true" />		
      <display:column property="responsable" title="Responsable" sortable="true" />
      <display:column property="fechaIngreso" title="Fecha Ingreso" sortable="true" />	
      <display:column property="estado" title="Estado" sortable="true" />
      <display:column property="fechaInicio" title="Fecha Inicio" sortable="true" />	
      <display:column property="fechaFinal" title="Fecha Fin" sortable="true" />		               
    </display:table>
</html:form>
<iframe width=174 height=220 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<br>
<br>
<script type="text/javascript">
var action = $('accion');
var formulario =$('oForm');
	function Nuevo(){
		action.value='nuevo';
		formulario.submit();
	}
	function Guardar(){
		action.value='guardar';
		formulario.submit();
	}
	function Cancelar(){	
		action.value='cancelar';
		formulario.submit();
	}
</script>