<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<div class="title">
	Recaudaciones
</div>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<html:form action="/actions/process.do?do=process&processName=Recaudaciones" >
	<br/>
	<html:hidden property="process.idRecaudacion" styleId="idRecaudacion"/>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.recaudaciones"
			export="true" id="reportTable" width="100%"
			requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process">
			<display:column title="N�mero Comprobante" media="html"
				sortable="true">
				<logic:equal value="0" name="reportTable" property="saldo">
					${reportTable.nromovimiento}
				</logic:equal>
				<logic:greaterThan value="0" name="reportTable" property="saldo">
					<html:link href="javascript: cargar( ${reportTable.id}, ${reportTable.idMoneda}, ${reportTable.cotizacion},${reportTable.importeMo}, ${reportTable.nromovimiento}, ${reportTable.tipoComprobante}, '${reportTable.fechamovimientoStr}', ${reportTable.saldoMo});">
					${reportTable.nromovimiento}</html:link>
				</logic:greaterThan>
				
			</display:column>
			<display:column title="N�mero Comprobante" property="nromovimiento" media="pdf excel xml" />
			<display:column title="Fecha Movimiento" property="fechamovimientoStr" sortable="true" />
			<display:column title="Fecha de Ingreso" property="fechavencimientoStr" sortable="true" />
			<display:column title="N� Expediente" property="nroExpediente" sortable="true" />
			<display:column title="N� Operaci�n" property="nroOperacion" sortable="true" />
			<display:column title="Cod. Operaci�n" property="codigoOperacion" sortable="true" />
			<display:column title="Ente Recaudador" property="bancos.proveedor.fantasia" sortable="true" />
			<display:column property="tipoComprobanteStr" title="Tipo de Comprobante" sortable="true"  align="right"/>
			<display:column property="estadoComprobanteStr" title="Estado de Comprobante" sortable="true" align="right" />
			<display:column title="Moneda" sortable="false" align="center">
				<logic:notEqual value="1" name="reportTable" property="idMoneda">
					${reportTable.valorOriginalStr } a ${reportTable.cotizacionStr }
				</logic:notEqual>
				<logic:equal value="1" name="reportTable" property="idMoneda">
					${reportTable.moneda.simbolo }
				</logic:equal>
			</display:column>
			
			
				
			<display:column title="Saldo" sortProperty="saldo" sortable="true"			
				align="right" style="text-align:right;" >
				${reportTable.saldoStr}
				<logic:greaterThan value="0.005" name="reportTable" property="saldo">
					<a href="#" onclick="pp('${reportTable.id}');return false;">-&gt;</a>
				</logic:greaterThan> 
			</display:column>
				
			<display:column title="Saldo Partidas Pendientes"  sortable="true" sortProperty="saldoPP"
				align="right" style="text-align:right;" >
				<logic:greaterThan value="0.005" name="reportTable" property="saldoPP">
					<a href="#" onclick="saldo('${reportTable.id}');return false;">&lt;-</a>
				</logic:greaterThan> ${reportTable.saldoPPStr}
			</display:column>
				
				
			<display:column title="Saldo Moneda Original" property="saldoMo" sortable="true"
				align="right" style="text-align:right;" />
				
					
			<display:column title="Importe" property="importe" sortable="true"
				align="right" style="text-align:right;" />
			<display:column title="Comisi�n" property="comision" sortable="true" style="text-align:right;"  align="right" />
			<display:column title="I.V.A." property="iva" sortable="true"  style="text-align:right;"  align="right" />
			<display:column title="Neto" property="neto" sortable="true" style="text-align:right;" align="right" />
			<display:column property="fechaPaseContabilidadStr" title="Fec. Cont." sortable="true" align="right" />
			<display:column property="fechaAnuladoStr" title="Anulada" sortable="true" align="right" />
			<display:column property="observacion" title="Observaci�n" sortable="true" align="center" />
		</display:table>
	</div>
</html:form>
<script type="text/javascript" language="javascript">
function cargar( id, idMoneda, cotizacion, importe, nromovimiento,tipoComprobante, fechaMov, saldo){
	var moneda = window.opener.document.getElementById("process.idMoneda");
	var mPago = window.opener.document.getElementById("process.idmediopago");
	var cotiza = window.opener.document.getElementById("cotizacion");
	var aCobrar = window.opener.document.getElementById("aCobrar");
	var recaudId = window.opener.document.getElementById("recauId");
	var recaudFecha = window.opener.document.getElementById("recauFecha");
	var recaudImporte = window.opener.document.getElementById("recauImporte");
	var recaudSaldo = window.opener.document.getElementById("recauSaldo");
	var importeMP = window.opener.document.getElementById("importe");
	moneda.value = idMoneda;
	cotiza.value = new Number(cotizacion);
	mPago.value = new Number(51);
	recaudId.value = id;
	recaudFecha.value = fechaMov;
	recaudImporte.value = new Number(importe);
	recaudSaldo.value = new Number(saldo);
	aCobrar.value = new Number("0.00");
	importeMP.value = new Number(0);
	window.close();
}
function pp(id) {
	if(!confirm("Est� seguro de pasar el saldo seleccionado a Partidas Pendientes?"))
		return ;
	var url = "";
	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=Recaudaciones&process.action=pp";
	url += "&process.idRecaudacion=" + id;
	url += "&process.idEnteRecaudador=" + ${ProcessForm.process.idEnteRecaudador};
	url += "&process.fecha=" + '${ProcessForm.process.fecha}';
	url += "&process.metodoCarga=" + ${ProcessForm.process.metodoCarga};
	

	window.location = url;		
}
function saldo(id) {
	if(!confirm("Est� seguro de pasar el valor en Partidas Pendientes a saldo aplicable?"))
		return ;
	var url = "";
	url += "${pageContext.request.contextPath}/actions/process.do?do=process";
	url += "&processName=Recaudaciones&process.action=saldo";
	url += "&process.idRecaudacion=" + id;
	url += "&process.idEnteRecaudador=" + ${ProcessForm.process.idEnteRecaudador};
	url += "&process.fecha=" + '${ProcessForm.process.fecha}';
	url += "&process.metodoCarga=" + ${ProcessForm.process.metodoCarga};

	window.location = url;
}
</script>