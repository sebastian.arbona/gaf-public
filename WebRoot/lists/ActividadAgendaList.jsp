<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Actividades de Agenda</div>
<p>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.idObjetoi" styleId="id" />
	<html:hidden property="process.action" styleId="action" />
	<html:hidden property="process.pestana" />
	<br/>
	<html:checkbox property="process.mostrarTodos" styleId="mostrarTodos" onchange="formulario.submit();"/><label for="mostrarTodos"> Mostrar Todas </label><br/>
	<logic:equal value="true" name="ProcessForm" property="process.pestana">
		<input type="button" onclick="Nuevo();" value="Nuevo" align="middle">
	</logic:equal>

  <br/>
  <br/>

	<div class="grilla">
		
		<display:table name="result" export="true"  id="reportTable" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:column property="estado" title="Estado" paramId="process.actividadAgenda.id" paramProperty="id"
	        	href="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=${param.processName}&process.action=load&process.idObjetoi={ProcessForm.process.idObjetoi}&process.pestana=${ProcessForm.process.pestana}"></display:column>
			<logic:equal value="responsable" name="ProcessForm" property="process.listBy" >
				<display:column property="objetoi.titulo" title="Proyecto"></display:column>
			</logic:equal>
			<display:column property="analisis.tipoStr" title="Analisis" sortable="true"></display:column>
			<display:column property="tipoActividadAgenda.nombre"  title="Tipo"></display:column>
			<display:column property="responsable.id" title="Responsable"></display:column>
			<display:column property="fechaCreacionStr" title="Creaci�n"></display:column>
			<display:column property="fechaVencimientoStr" title="Vencimiento"></display:column>
			<display:column property="fechaCumplimientoStr" title="Cumplimiento"></display:column>
			<display:column property="detalle" title="Detalle"></display:column>
			<display:column title="Ver Archivo" media="html">
				
		      	<logic:notEmpty property="objetoiArchivo" name="reportTable">
				  <html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${reportTable.objetoiArchivo.id}')" property="process.idAnalisis">Descargar</html:button>
				</logic:notEmpty>  
			  </display:column>
			
		</display:table>
	</div>
</html:form>
<script type="text/javascript">
var action = $('action');
var formulario =$('oForm');
function Nuevo(){
	action.value='new';
	formulario.submit();
}

</script>