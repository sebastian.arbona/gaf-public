<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="title" style="margin-bottom: 30px">Emisiones</div>

<html:form action="/actions/abmAction.do?do=load&entityName=Emision">

<html:errors/>
	
<div class="grilla">
	<display:table name="ListResult" property="result" export="true" id="reportTable" 
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
		<display:setProperty name="report.title" value="Emisiones sin generar"></display:setProperty>
	    <display:column property="fechaEmisionStr" title="Fecha Emisi�n" href="${pageContext.request.contextPath}/actions/emidetaAction.do?do=list&entityName=Emideta" paramProperty="id" paramId="idEmision"/>
	    <display:column property="ordenEmision" title="Orden"/>
	    <display:column property="cantidad" title="Cant. Cuotas"/>
	    <display:column property="importe" title="Importe" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	    <display:column title="Impactado">
	    	<logic:equal property="facturada" name="reportTable"  value="true">
	    		SI
	    	</logic:equal>
	    	<logic:equal property="facturada" name="reportTable" value="false">
	    		NO
	    	</logic:equal>
	    </display:column>
	    <display:column property="criterio" title="Liquida Por"/>
	    <display:column property="moneda" title="Moneda"/>
	    <display:column property="fechaHasta" title="Fecha Hasta"/>
	    <display:column property="valorDesde" title="Valor Desde"/>
	    <display:column property="valorHasta" title="Valor Hasta"/>
	    <display:column property="usuario" title="Usuario"/>
	    <display:footer>
			<tr>
				<td><b>TOTAL</b></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>
					${totalImporte}
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
	    </display:footer>

    </display:table>
</div>

</html:form>