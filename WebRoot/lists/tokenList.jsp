<%@ page language="java"%>
<%@page import="org.jbpm.taskmgmt.exe.TaskInstance"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/jbpm.tld" prefix="jbpm"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %> 


<div class="title">Lista de Tareas En Espera</div>
<br/><br/>
<html:form action="actions/jbpm.do?do=showPausedTokensList">
	Proceso: <asf:select name="processName" value="${JBPMForm.processName}" entityName="com.nirven.jbpm.persistance.Tramite" listCaption="nombre" listValues="processDefinitionName" filter="processDefinitionName!=''"></asf:select>
	<%-- html:text property="stateName"></html:text --%>
	<html:submit>Consultar</html:submit>

</html:form>

<div class="grilla">
	<display:table name="PausedTokensList" export="true" id="token" requestURI="${pageContext.request.contextPath}/actions/jbpm.do?do=showPausedTokensList"
		defaultsort="1" >
		<display:column  property="node.name" title="Estado"   />
		<display:column  title="Proceso">
			<jbpm:taskTitle name="token" />
		</display:column>
		<display:column property="start" title="Desde" decorator="com.nirven.jbpm.displayDecorators.DateDecorator" />	
		<display:column title="Acciones" media="html">
			<a href="${pageContext.request.contextPath}/actions/jbpm.do?do=signalToken&id=${token.id}&processName=${JBPMForm.processName}&stateName=${JBPMForm.stateName}" onclick="return confirm('Est� seguro de retomar el tr�mite?');">Retomar</a>
		</display:column>
	</display:table>
</div>
