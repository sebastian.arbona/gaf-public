<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Confirmar Liberaciones Pendientes</div>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
  	<br/>
    <input type="button" value="Listar" onclick="listar();">
    <br/>
    <br/>
    <input type="button" value="Confirmar" onclick="confirmar();">
    <div class="error" style="color: red;">${ProcessForm.process.msg}</div>
    <table align="right">
    	<tr>
	    	<th><input type="checkbox" onclick="activar();" id="activacion" align="right"></th>
    	</tr>
    </table>
    <br/>
    <br/>
	<display:table name="ProcessForm" property="process.creditosDTO" defaultsort="1" id="credito" class="com.asf.cred.business.ObjetoiDTO">					
		<display:column class="right" title="Proyecto" >
			<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${credito.idObjetoi}&process.action=buscar&process.personaId=${credito.titular.idpersona}">${credito.numeroAtencion}</a>
		</display:column>
		<display:column class="right" title="Titular" >
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.titular.nomb12}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.titular.nomb12}</div>
		   	</logic:equal>
		</display:column>
		<display:column class="right" title="Expediente" >
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.expediente}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.expediente}</div>
		   	</logic:equal>
		</display:column>
		<%-- Se oculta columna. Ticket 9157 
		<display:column render="false" class="right" title="Total Inmovilizado" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.totalInmovilizadoStr}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.totalInmovilizadoStr}</div>
		   	</logic:equal>
		</display:column>
		--%>
		<display:column class="right" title="Producto" >
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.producto}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.producto}</div>
		   	</logic:equal>
		</display:column>						
		<display:column class="right" title="Liberado a la Fecha" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.liberadoALaFechaStr}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.liberadoALaFechaStr}</div>
		   	</logic:equal>
		</display:column>										
		<display:column class="right" title="Última Liberacion" >
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.ultimaLiberacion}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.ultimaLiberacion}</div>
		   	</logic:equal>
		</display:column>				
		<display:column class="right" title="Volumen Actual Inmovilizado" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.saldoInmovilizadoStr}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.saldoInmovilizadoStr}</div>
		   	</logic:equal>
		</display:column>
		<display:column class="right" title="Deuda Actual" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" >
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.deudaActualStr}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.deudaActualStr}</div>
		   	</logic:equal>
		</display:column>
		<display:column class="right" title="Nuevo Volumen" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
			<logic:equal value="false" name="credito" property="banderaFiltrosColor">
		   				<div style="color: black">${credito.nuevoVolumenStr}</div>
		   	</logic:equal>
		   	<logic:equal value="true" name="credito" property="banderaFiltrosColor">
		   				<div style="color: red">${credito.nuevoVolumenStr}</div>
		   	</logic:equal>
		</display:column>
<%--	<display:column property="liberacionSolicitada" title="Liberación Solicitada" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
		<display:column property="liberacionSolicitar" title="Liberación a Solicitar" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
 --%>
		<display:column media="html">
			<input type="checkbox" name="credito-${credito.idObjetoi}" value="${credito.idObjetoi}" align="middle"/>
		</display:column>
	</display:table>
	<br/>
    <input type="button" value="Confirmar" onclick="confirmar();">
    <table align="right">
    	<tr>
	    	<th><input type="checkbox" onclick="activar2();" id="activacion2" align="left"></th>
    	</tr>
    </table>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
<script type="text/javascript">
var accion = $('accion');
var form = $('oForm');
function listar() {
	accion.value = "listar";
	form.submit();
}
function marcarTodos() {
	var nodoCheck = document.getElementsByTagName("input");
	for (i = 0; i < nodoCheck.length; i++) {
		if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
				&& nodoCheck[i].disabled == false) {
			nodoCheck[i].checked = true;
		}
	}
}

function desmarcarTodos() {
	var nodoCheck = document.getElementsByTagName("input");
	for (i = 0; i < nodoCheck.length; i++) {
		if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
				&& nodoCheck[i].disabled == false) {
			nodoCheck[i].checked = false;
		}
	}
}
function activar() {
	var check = document.getElementById('activacion');
	if (check.checked == true) {
		marcarTodos();
	} else {
		desmarcarTodos();
	}
}
function activar2() {
	var check = document.getElementById('activacion2');
	if (check.checked == true) {
		marcarTodos();
	} else {
		desmarcarTodos();
	}
}
function confirmar(){
	accion.value = "confirmar";
	form.submit();
}
</script>