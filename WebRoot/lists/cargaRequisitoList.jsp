<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<html:hidden property="paramValue[0]" value="${paramValue[0]}" />
<html:hidden property="paramName[0]" value="credito.id" />
<html:hidden property="paramComp[0]" value="=" />
<html:hidden property="requisitosCumplidos"
	value="${RequisitosForm.requisitosCumplidos}" styleId="reqCumpl" />
<br>
<html:form
	action="actions/RequisitosAction.do?do=list&entityName=${param.entityName}&idSolicitud=${RequisitosForm.credito.id}"
	styleId="requisitosForm">
	<html:hidden property="do" styleId="do" value="list" />

	<br>
	<br>
	<div class="grilla">
		<table>
			<tr>
				<th>Ver requisitos especificos para</th>
				<td>
					<span>
						<html:checkbox property="personasFisicas" onchange="submitCheck();"></html:checkbox> Personas F�sicas
					</span>
					<span>
						<html:checkbox property="personasJuridicas" onchange="submitCheck();"></html:checkbox> Personas Jur�dicas
					</span>
				</td>
			</tr>
		</table>
		
		<table>
			<logic:iterate id="req" name="requisitosForm" property="cargaRqList">
				<logic:notPresent name="familiaActual">
					<bean:define id="familiaActual" name="req" property="requisito.familiaNombre"/>
					<tr>
						<th colspan="10" style="background-color: #888785">${req.requisito.familiaNombre}</th>
					</tr>
					<tr>
						<th>ID</th>
						<th>Requisito</th>
						<th>Orden</th>
						<th>Cumplimiento</th>
						<th>Observaciones</th>
						<th>Archivo</th>
						<th>Ver</th>
						<th>Eliminar</th>
						<th>Selecci&oacute;n</th>
						<th>N/C</th>
					</tr>
				</logic:notPresent>
				<logic:present name="familiaActual">
					<logic:notEqual value="${req.requisito.familiaNombre}" name="familiaActual">
						<tr>
							<th colspan="10" style="background-color: #888785">${req.requisito.familiaNombre}</th>
						</tr>
						<tr>
							<th>ID</th>
							<th>Requisito</th>
							<th>Orden</th>
							<th>Cumplimiento</th>
							<th>Observaciones</th>
							<th>Archivo</th>
							<th>Ver</th>
							<th>Eliminar</th>
							<th>Selecci&oacute;n</th>
							<th>N/C</th>
						</tr>
						<bean:define id="familiaActual" name="req" property="requisito.familiaNombre"></bean:define>
					</logic:notEqual>
				</logic:present>
				
				<tr>
					<td>
						<a href="${pageContext.request.contextPath}/actions/RequisitosAction.do?do=load&idSolicitud=${RequisitosForm.credito.id}&idEntity=${req.requisito.id}">
							${req.requisito.id}
						</a>
					</td>
					<td>
						<logic:equal value="true" name="req"
							property="requisito.obligatorio">
							<div style="color: red">
								${req.requisito.nombre}
							</div>
						</logic:equal>
						<logic:notEqual value="true" name="req"
							property="requisito.obligatorio">
							${req.requisito.nombre}
						</logic:notEqual>
					</td>
					<td>${req.requisito.orden}</td>
					<td>${req.fechaCumplimientoStr}</td>
					<td>${req.observaciones}</td>
					<td>${req.objetoiArchivo.nombre}</td>
					<td>
						<logic:notEmpty name="req" property="objetoiArchivo">
							<html:button onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${req.objetoiArchivo.id}')" 
							property="process.objetoiArchivo.id">Bajar</html:button>
						</logic:notEmpty>
					</td>
					<td>
						<asf:security action="/actions/RequisitosAction.do" access="do=delete&entityName=${param.entityName}">
							<logic:notEmpty name="req" property="fechaCumplimientoStr">
								<a href="${pageContext.request.contextPath}/actions/RequisitosAction.do?do=delete&entityName=CargaRequisito&entity.id=${req.id}" onclick="return confirmDelete();">
									<bean:message key="abm.button.delete" />
								</a>
							</logic:notEmpty>
						</asf:security>
					</td>
					<td>
						<logic:empty name="req" property="fechaCumplimientoStr">
							<html:checkbox property="seleccion(${req.requisito.id})"
								value="true" disabled="false"></html:checkbox>
						</logic:empty>
						<logic:notEmpty name="req" property="fechaCumplimientoStr">
							<html:checkbox property="seleccion(${req.requisito.id})"
								value="false" disabled="true"></html:checkbox>
						</logic:notEmpty>
					</td>
					<td>
						<logic:empty name="req" property="fechaCumplimientoStr">
							<html:checkbox property="nc(${req.requisito.id})"
								value="true" disabled="false"></html:checkbox>
						</logic:empty>
						<logic:notEmpty name="req" property="fechaCumplimientoStr">
							<html:checkbox property="nc(${req.requisito.id})"
								value="false" disabled="false"></html:checkbox>
						</logic:notEmpty>
					</td>
				</tr>
			</logic:iterate>
		</table>
		
		
		<table>
			<tr>
			<logic:equal name="requisitosForm" value="ESPERANDO DOCUMENTACION" property="estadoObjetoi">
				<td>
					<html:submit value="Guardar" styleId="bGuardar" 
						onclick="guardar();"/>
				</td>
			</logic:equal>
			<logic:notEqual name="requisitosForm" value="ESPERANDO DOCUMENTACION" property="estadoObjetoi">
				<td>
					<html:submit value="Guardar" styleId="bGuardar" 
						onclick="guardar();" disabled="true"/>
				</td>
			</logic:notEqual>
				<td>
					<html:submit value="Comprobante requisitos" styleId="comprobarId"
						onclick="comprobar();" />
				</td>
				<td>

					
					<asf:security action="/actions/omitirRequisitosObligatorios.do" access="">
						<logic:equal name="requisitosForm" value="ESPERANDO DOCUMENTACION" property="estadoObjetoi">
						<html:submit value="Omitir Requisitos Obligatorios"
							styleId="omitirId" onclick="omitir();" />
						</logic:equal>
						<logic:notEqual name="requisitosForm" value="ESPERANDO DOCUMENTACION" property="estadoObjetoi">
						<html:submit value="Omitir Requisitos Obligatorios"
							styleId="omitirId" onclick="omitir();" disabled="true"/>
						</logic:notEqual>						
					</asf:security>
					
				</td>
				<td>
					<input type="button" id="seleccionarTodos"
						value="Seleccionar todos" onclick="marcarTodos();" />
				</td>
				<td>
					<input type="button" id="deseleccionarTodos"
						value="Deseleccionar todos" onclick="desmarcarTodos()" />
				</td>
				<%--			<td>--%>
				<%--				<html:submit value="Refrescar" styleId="refrescarId" onclick="volver();"/>--%>
				<%--			</td>		--%>
			</tr>
		</table>


	</div>
</html:form>
<script type="text/javascript">

var req = ${'reqCumpl'};

requisitosCumplidos1();

	function requisitosCumplidos1(){
		if(req.value=="true"){
			var iniciarAprob = window.parent.document.getElementById("iniciarAprob");
			iniciarAprob.style.display="block";
			var continuar = window.parent.document.getElementById("continuar");
			continuar.disabled=false;
		}else{
			var iniciarAprob = window.parent.document.getElementById("iniciarAprob");
			iniciarAprob.style.display="none";
			var continuar = window.parent.document.getElementById("continuar");
			continuar.disabled=true;
		}
	}

	function guardar() {
		$("requisitosForm").action = "${pageContext.request.contextPath}/actions/RequisitosAction.do?do=saveCumplimiento&idSolicitud=${RequisitosForm.credito.id}";
		requisitosCumplidos1();
	}
	
	function comprobar() {
		$("requisitosForm").action = "${pageContext.request.contextPath}/actions/RequisitosAction.do?do=comprobarRequisito&idSolicitud=${RequisitosForm.credito.id}";
		requisitosCumplidos1();
}
	
	function omitir() {
		$("requisitosForm").action = "${pageContext.request.contextPath}/actions/RequisitosAction.do?do=omitirRequisitosObligatorios&idSolicitud=${RequisitosForm.credito.id}";
		requisitosCumplidos1();
	}
	
	function volver(){			
		var url = '';
        url += '${pageContext.request.contextPath}/actions/process.do?';
        url += 'do=process&';
        url += 'processName=SolicitudesHandler&';
        url += 'process.accion=show&';
        url += 'process.idSolicitud=${RequisitosForm.credito.id}';
        parent.location = url;
	}
	
	function submitCheck() {
		$("requisitosForm").action = "${pageContext.request.contextPath}/actions/RequisitosAction.do?do=list&entityName=${param.entityName}&idSolicitud=${RequisitosForm.credito.id}"
		$("requisitosForm").submit();
	}
	
	function marcarTodos(){
		var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" &&
	        		nodoCheck[i].name.substr(0, 2) != "nc" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = true;
	        }
	    }
	}

	function desmarcarTodos(){
		var nodoCheck = document.getElementsByTagName("input");
		for (i=0; i<nodoCheck.length; i++){
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" &&
				nodoCheck[i].name.substr(0, 2) != "nc" && nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}
</script>






