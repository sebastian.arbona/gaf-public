<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administración de Nacionalidades</div>
<p>

<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
	<display:setProperty name="report.title" value="Administración de Nacionalidades"></display:setProperty>
    <display:column property="id" title="Código" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" sortable="true"  />
    <display:column property="nacionalidad" title="Nombre" sortable="true" />    
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete"/></display:column>
</asf:security>
</display:table>
</div>