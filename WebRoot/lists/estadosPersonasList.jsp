<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="nuevo" />
	<html:hidden property="process.persona.id" styleId="idPersonaTitular"
		value="${ProcessForm.process.persona.id}" />
	<html:hidden property="process.agrega" styleId="agrega" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<asf:security action="/actions/process.do"
		access="do=process&processName=${param.processName}&process.accion=nuevo">
		<html:submit>
			<bean:message key="abm.button.new" />
		</html:submit>
	</asf:security>
	<br>
	<br>
	<logic:notEmpty name="ProcessForm" property="process.estadosPersonas">
		<display:table name="ProcessForm" property="process.estadosPersonas"
			pagesize="10" id="reportTable" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
			<display:caption>Estados y Observaciones de la Persona:  ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12}</display:caption>
			<display:column title="Estado" property="estaobs" sortable="true" />
			<display:column title="Color" media="html">
				<div style="background:${reportTable.color};">
					&nbsp;
				</div>
			</display:column>
			<display:column title="Desde" property="fechaStr" sortable="true" />
			<display:column title="Hasta" property="fechaHastaStr"
				sortable="true" />
			<display:column title="Observaciones" property="observaciones"
				sortable="true" />
		</display:table>
	</logic:notEmpty>
	<logic:empty name="ProcessForm" property="process.estadosPersonas">
        La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no tiene Estados ni Observaciones.
    </logic:empty>
</html:form>

<script language='javascript'>
Event.observe(window, 'load', function() {
	this.name = "Estados y Observaciones";
	parent.pop(this);
});
</script>