<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<link type="text/css"
		href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
		rel="stylesheet" />
	<link type="text/css"
		href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
		rel="stylesheet" />
<body class=" yui-skin-sam">
	<div class="title">
		Administración de Sub Tipos de Resoluciones
	</div>
	<br>
	<br>
	
		
<div><html:errors /></div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
</script>
	
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="nuevo" />
		<asf:security action="/actions/process.do?do=process&processName=${param.processName}" access="accion=nuevo">
			<html:submit onclick="getElem('accion').value='nuevo';">
				<bean:message key="abm.button.new" />
			</html:submit>
		</asf:security>
	</html:form>
	<br>
	<display:table name="ProcessForm" property="process.subTiposResoluciones" export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
		<display:setProperty name="report.title"
			value="Administración de Sub Tipos de Resoluciones"></display:setProperty>
		<display:column title="ID" sortProperty="id" sortable="true" media="html">
		<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=nuevo&process.idSubTipoResolucion=${reportTable.id}"
		paramId="entity.id" paramProperty="id" sortable="true">${reportTable.id}</a>
		</display:column>
		<display:column property="nombre" title="Nombre" sortable="true" />
		<display:column property="descripcion" title="Descripcion" />
		<asf:security action="/actions/process.do" access="do=process&entityName=${param.entityName}">       
				<display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/process.do?processName=SubTipoResolucionProcess&do=process&process.accion=eliminar"
				      paramId="process.idSubTipoResolucion" paramProperty="id"><bean:message key="abm.button.delete" /></display:column>    
			</asf:security>
	</display:table>

	<script type="text/javascript">

function load(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.idSolicitud=' + id;

	window.location = url;
}// fin modificar.-

function show(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=show&';
	url += 'process.idSolicitud=' + id;

	window.location = url;
}

function eliminar(id) {
	var url = '';

	if (confirmDelete()) {
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=eliminar&';
		url += 'process.idSolicitud=' + id;

		window.location = url;
	}
}// fin modificar.-   

//fin irARequisitos.-

</script>