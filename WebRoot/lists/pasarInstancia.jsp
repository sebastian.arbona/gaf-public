<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Instancias Judiciales</div>
<br>
  <div style="width: 70%" align="left">
    <html:errors />
  </div>
<br>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
<html:hidden property="process.accion" styleId="accion" name="ProcessForm"/>
<div>Seleccione instancia para editar las observaciones</div>
<div class="grilla">
<display:table name="ProcessForm" property="process.instancias" defaultsort="1" id="instancia">
	<display:column title="Orden" media="html" sortable="true">
	    <a href="javascript:void(0);" onclick="seleccionar(${instancia_rowNum - 1});">
	    	${instancia.instancia.orden}
	    </a>
    </display:column>
    <display:column title="Nombre" property="instancia.nombre" sortable="true"/>
    <display:column title="Gasto Asociado"  property="instancia.gastoAsociado"  sortable="true"/>
    <display:column title="Observaciones" property="observaciones"/>
    <display:column title="Fecha Desde"  property="fechaStr"  sortable="true"/>
	<display:column title="Fecha Hasta"  property="fechaHastaStr"  sortable="true"/>
</display:table>
</div>
<logic:notEqual name="ProcessForm" value="-1" property="process.indice">
	<br/>
	<table>
	<tr>
		<th>Orden</th>
		<td>
			<html:text size="2" name="ProcessForm" property="process.instanciaObjetoi.instancia.orden" value="${ProcessForm.process.instanciaObjetoi.instancia.orden}" readonly="true"/>
		</td>
	</tr>
	<tr>
		<th>Observaciones</th>
		<td>
			<html:textarea name="ProcessForm" property="process.instanciaObjetoi.observaciones" value="${ProcessForm.process.instanciaObjetoi.observaciones}" styleId="observaciones" rows="5" cols="60"/>
		</td>
	</tr>
	</table>
	<br/>
	<input type="button" value="Guardar Observaciones" onclick="guardar();" id="guardarId">
	<input type="button" value="Cancelar" onclick="cancelar();">
	<br/>
</logic:notEqual>
<br/>
<input type="button" value="Pasar de Instancia" onclick="pasarInstancia();" id="pasarInstanciaId">
</html:form>
<script language='javascript' >
function seleccionar(fila) {
	window.location = 
		'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=seleccionar' +
        '&process.objetoi.id=${ProcessForm.process.objetoi.id}' +
        '&process.indice='+fila;	
}

function guardar() {
	var observaciones = $('observaciones').value;
	window.location = 
		'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=guardar' +
        '&process.objetoi.id=${ProcessForm.process.objetoi.id}' +
        '&process.instanciaObjetoi.id=${ProcessForm.process.instanciaObjetoi.id}' +
        '&process.instanciaObjetoi.observaciones=' + observaciones;
} 

function pasarInstancia() {
	var actual = "${ProcessForm.process.instanciaActual}";
	var confirmacion = confirm("La instancia actual es de orden " + actual 
							+ " �Est� seguro que desea pasar el proyecto a la instancia de orden " 
							+ ++actual + "?");
   	if(confirmacion) {
		window.location = 
		'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=pasarInstancia' +
        '&process.objetoi.id=${ProcessForm.process.objetoi.id}';						
	}   
}
function cancelar() {
	window.location = 
		'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.objetoi.id=${ProcessForm.process.objetoi.id}' +
        '&process.indice=-1';
}
</script>