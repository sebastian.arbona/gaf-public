<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>

<div class="title">Ejecuci&oacute;n Desembolsos Masivos</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.importeGastosCosecha" styleId="gastos" />
	<html:hidden property="process.accion" styleId="accion" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<logic:empty name="ProcessForm" property="process.creditos">
    	No hay desembolsos para ejecutar
    	<br />
		<br />
	</logic:empty>
	<logic:equal value="true" property="process.ejecutado"
		name="ProcessForm">
    	Desembolsos solicitados correctamente
    	<br />
		<br />
	</logic:equal>
	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<div>
			<br> <br> <input type="checkbox" onclick="activar();"
				id="activacion" align="left">
		</div>
		<display:table name="ProcessForm" property="process.creditos"
			id="credito" class="com.asf.cred.business.ObjetoiDTO">
			<display:column title="Selecci�n">
				<input type="checkbox" name="requisito-${credito.idObjetoi}"
					value="${credito.idObjetoi}" />
			</display:column>
			<display:column title="Nro Solicitud" property="numeroAtencion"
				sortable="true" />
			<display:column title="Titular" property="titular.nomb12"
				sortable="true" />
			<display:column title="Destino" sortable="true">
				<logic:equal value="CRDC" property="destino" name="credito">
				Cosecha
				</logic:equal>
				<logic:equal value="CRDA" property="destino" name="credito">
				Elaboraci&oacute;n
				</logic:equal>
			</display:column>
			<display:column title="Monto Aprobado" property="financiamientoStr"
				sortable="true" />
			<display:column title="Fecha Solicitud" property="fechaSolicitudStr"
				sortable="true" />
			<display:column title="Fecha Contrato" property="fechaContratoStr"
				sortable="true" />
			<display:column title="Tipo Garant&iacute:a" property="tipoGarantia"
				sortable="true" />
			<logic:equal value="false" property="process.ejecucion2"
				name="ProcessForm">
				<logic:equal value="false" property="process.primeraEtapa"
					name="ProcessForm">
					<display:column title="Monto a Desembolsar"
						property="desembolsoStr" />
					<display:column title="Gastos" property="gastosStr" />
				</logic:equal>
			</logic:equal>
			<logic:equal value="true" property="process.ejecucion2"
				name="ProcessForm">
				<logic:equal value="false" property="process.primeraEtapa"
					name="ProcessForm">
					<display:column title="Monto a Desembolsar"
						property="desembolsoStr" />
				</logic:equal>
			</logic:equal>
			<logic:equal value="true" property="process.ejecucion2"
				name="ProcessForm">
				<display:column title="Tipo Desembolso" property="tipoDesembolso"
					sortable="true" />
			</logic:equal>
		</display:table>
		<logic:equal value="false" property="process.ejecucion2"
			name="ProcessForm">
			<br />
			<br />
			<logic:equal value="false" property="process.primeraEtapa"
				name="ProcessForm">
				<input type="button" value="Solicitar a GAF Desembolso 1"
					id="solicitarId" onclick="solicitar1();" />
				<br />
				<br />
				<logic:equal value="true" property="process.ocultarGastosMultas"
					name="ProcessForm">
					<table>
						<tr>
							<th>Importe de Gastos:</th>
							<td><asf:text name="ProcessForm"
									property="process.importeGastosIngresado" maxlength="15"
									type="decimal" readonly="false"
									value="${ProcessForm.process.importeGastosIngresado}" /></td>
						</tr>
						<tr>
							<th>Importe de Multas:</th>
							<td><asf:text name="ProcessForm"
									property="process.importeMultasIngresado" maxlength="15"
									type="decimal" readonly="false"
									value="${ProcessForm.process.importeMultasIngresado}" /></td>
						</tr>
					</table>
				</logic:equal>
			</logic:equal>
		</logic:equal>
		<logic:equal value="true" property="process.ejecucion2"
			name="ProcessForm">
			<br />
			<br />
			<logic:equal value="false" property="process.primeraEtapa"
				name="ProcessForm">
				<input type="button" value="Solicitar a GAF Desembolso 2"
					id="solicitarId2" onclick="solicitar2();" />
				<br />
				<br />
				<logic:equal value="true" property="process.ocultarGastosMultas"
					name="ProcessForm">
					<table>
						<tr>
							<th>Importe de Gastos:</th>
							<td><asf:text name="ProcessForm"
									property="process.importeGastosIngresado" maxlength="15"
									type="decimal" readonly="false"
									value="${ProcessForm.process.importeGastosIngresado}" /></td>
						</tr>
						<tr>
							<th>Importe de Multas:</th>
							<td><asf:text name="ProcessForm"
									property="process.importeMultasIngresado" maxlength="15"
									type="decimal" readonly="false"
									value="${ProcessForm.process.importeMultasIngresado}" /></td>
						</tr>
					</table>
				</logic:equal>
			</logic:equal>
		</logic:equal>
		<br />
		<br />
	</logic:notEmpty>

	<div class="grilla">
		<table>
			<tr>
				<th>L&iacute;neas</th>
				<td><html:select property="process.seleccion"
						name="ProcessForm" multiple="true" size="8">
						<html:optionsCollection name="ProcessForm"
							property="process.lineas" label="nombre" value="id" />
					</html:select></td>
			</tr>
		</table>
	</div>

	<input type="button" value="Listar Cr&eacute;ditos con Desembolso 1"
		id="listarId" onclick="listar1();" />
	<br />
	<br />
	<input type="button" value="Listar Cr&eacute;ditos con Desembolso 2"
		id="listarId2" onclick="listar2();" />
	<br />
	<br />
	<table>
		<tr>
			<th>Fecha de Lote:</th>
			<td><asf:calendar property="ProcessForm.process.fechaLoteStr" />
				<input type="button" value="Buscar Lotes" id="buscarLotes"
				onclick="buscarL();" /></td>

		</tr>
		<tr>
			<th>Nro de Lote:</th>
			<td><asf:select name="process.nroLoteElegido"
					entityName="com.nirven.creditos.hibernate.LoteDesembolso"
					listCaption="getNroLote" listValues="getId" nullText="No hay Lotes"
					value="${ProcessForm.process.nroLoteElegido}"
					filter="fecha = '${ProcessForm.process.fechaActual}'" /></td>

		</tr>
	</table>
	<div style="clear: both"></div>
	<br />
	<input type="button" value="Imprimir Solicitud" id="solicitarId3"
		onclick="imprimirReporte();" />
	<input type="button" value="Imprimir Volante" id="solicitarId4"
		onclick="imprimirVolante();" />
</html:form>
<script language="javascript" type="text/javascript">
	var accion = $('accion');
	var formulario = $('oForm');
	function listar1() {
		accion.value = 'listar1';
		formulario.submit();
	}
	function listar2() {
		accion.value = 'listar2';
		formulario.submit();
	}

	function buscarL() {
		accion.value = 'buscarLotes';
		formulario.submit();
	}

	function solicitar1() {
		accion.value = 'solicitar1';
		formulario.submit();
	}

	function solicitar2() {
		accion.value = 'solicitar2';
		formulario.submit();
	}

	function imprimirReporte() {
		accion.value = 'imprimirReporte';
		formulario.submit();
	}

	function imprimirVolante() {
		accion.value = 'imprimirVolante';
		formulario.submit();
	}

	function marcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = true;
			}
		}
	}

	function desmarcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}

	function activar() {
		var check = document.getElementById('activacion');
		if (check.checked == true) {
			marcarTodos();
		} else {
			desmarcarTodos();
		}
	}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>