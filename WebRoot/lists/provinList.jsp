<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript">
	
    function nuevo(){
        if('${paramValue[0]}'!='')
        {
            getElem('do').value='newEntity';
            window.location.href="${pageContext.request.contextPath}/actions/abmAction.do?entityName=${param.entityName}&filter=true";
        }
    }
</script>

<div class="title">Administraci�n de Provincias</div>
<html:form action="/actions/abmAction.do?entityName=${param.entityName}&filter=true&entity.idpais=${paramValue[0]}">
    <br><br>
    <b>Pa�s:</b>
    <asf:select name="paramValue[0]" attribs="onchange=AbmForm.submit();"  nullValue="true" entityName="com.civitas.hibernate.persona.Pais" listCaption="nombrePais" listValues="idPais" value="${paramValue[0]}" filter="nombrePais like '%' order by nombrePais"/>
    <br><br>
    <html:hidden property="paramName[0]" styleId="paramName" value="idpais"/>
    <html:hidden property="paramComp[0]" styleId="paramComp" value="="/>
    <html:hidden property="filter" value="true"/>
    <html:hidden property="do" styleId="do" value="list"/>

    <asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
        <html:submit onclick="nuevo();">Nuevo</html:submit>
    </asf:security>
</html:form>
<br><br>
<div class="grilla">

<display:table name="ListResult" property="result" defaultsort="1" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
<display:setProperty name="report.title" value="Administraci�n Provincias y Pa�ses"></display:setProperty>
    
    <display:column media="html" property="id" title="C�digo" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.id" paramProperty="id" sortable="true"/>

    <display:column media="excel pdf xml" title="C�digo" property="codi08"/>
    <display:column title="Detalle" property="deta08" sortable="true"/>
    <display:column title="Abreviatura" property="abre08" sortable="true"/>
    <display:column title="C�digo D.G.I." property="cdgi08" sortable="true"/>
        
    <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
        <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&filter=true" paramId="entity.id" paramProperty="id">Eliminar</display:column>
    </asf:security>
 
</display:table>
</div>
