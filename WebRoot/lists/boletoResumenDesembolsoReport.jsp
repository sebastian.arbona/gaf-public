<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="grilla">
<div class="title">Alerta de Boletos sin Liquidaci&oacute;n</div>
<br>
<html:form action="/actions/process.do?do=process&processName=BoletoResumenDesembolsoReport">
	<table>
		<tr>
			<th>Fecha desembolso desde</th>
			<td><asf:calendar property="ProcessForm.process.fechaDesde" value="${process.fechaDesde}"/></td>
		</tr>
		<tr>
			<th>Fecha desembolso hasta</th>
			<td><asf:calendar property="ProcessForm.process.fechaHasta" value="${process.fechaHasta}"/></td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Buscar"></html:submit>
			</th>
		</tr>
	</table>
	<logic:notEmpty name="ProcessForm" property="process.boletos">
		<display:table name="ProcessForm" property="process.boletos" export="true" id="boletos" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BoletoResumenDesembolsoReport">
			<display:caption>Alerta de Boletos sin Liquidaci&oacute;n</display:caption>
			<display:column title="N�mero" sortable="true">
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BoletosDePago&process.accion=imprimirBoleto&process.objetoi.id=${boletos.objetoi.id}&process.boleto.id=${boletos.id}" target="_blank">${boletos.numeroBoleto}</a>
			</display:column>
			<display:column property="objetoi.numeroAtencion" title="Proyecto"/>
			<display:column property="objetoi.persona.nomb12" title="Titular"/>
			<display:column property="objetoi.linea.nombre" title="Linea"/>
			<display:column property="fechaVencimiento" title="Fecha Vto." decorator="com.asf.displayDecorators.DateDecorator"/>
		</display:table>
	</logic:notEmpty>
</html:form>
</div>

<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>