<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<div class="title">
  Administraci�n de Subtipos de L�neas de Cr�dito
</div>
<br>
<html:form action="actions/abmAction.do?entityName=${param.entityName}">
  <html:hidden property="do" styleId="accion" value="newEntity" />
  <asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
    <button type="submit">
      <bean:message key="abm.button.new" />
    </button>
  </asf:security>
</html:form>
<br>
<br>
<div class="grilla">
  <display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1"
    requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}"
  >
    <display:setProperty name="report.title" value="Administraci�n de Subtipos de L�neas de Credito"></display:setProperty>
    <display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
      href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}"
    />
    <display:column property="nombre" title="Nombre" sortable="true" />
    <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
      <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
        href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id"
        paramProperty="id"
      >
        <bean:message key="abm.button.delete" />
      </display:column>
    </asf:security>
  </display:table>
</div>
<!-- Raul Varela -->