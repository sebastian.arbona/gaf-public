<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@page import="com.asf.cred.business.inv.AccionINV"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div class="title">
	${ProcessForm.process.titulo}
</div>
<logic:equal name="ProcessForm" property="process.INV" value="0">
	Los siguientes vi�edos poseen alg�n dato err�neo seg�n el INV, por favor verifique los datos de cada uno y modifique en caso de ser necesario"
</logic:equal>
<logic:equal name="ProcessForm" property="process.INV" value="1">
	Los siguientes vi�edos poseen alg�n dato err�neo seg�n FTyC, por favor verifique los datos de cada uno y modifique en caso de ser necesario" 
</logic:equal>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br>
<html:form
	action="actions/process.do?processName=${param.processName}&do=process"
	styleId="formulario">

	<html:hidden property="process.accionStr"
		value="<%=AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE.toString()%>"
		styleId="accion" />
	<html:hidden property="process.idVinedo" styleId="vinedoId"/>

	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

	<div class="grilla">
		<display:table name="ProcessForm" property="process.vinedos"
			export="true" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.action=listar"
			width="100%">

			<display:setProperty name="report.title"
				value="Solicitud de Verificaci�n T�cnica Para el INV" />
			<display:column property="credito.expediente" title="Expediente" sortable="false"
				align="center" />
			<display:column property="credito.persona.nomb12" title="Titular" sortable="false"
				align="center" />
			<display:column property="vinedo.hectareas" title="Hect."
				sortable="true" align="right" />
			<display:column property="qqEstimados" title="QQ Estimados"
				sortable="true" align="right" />
			<display:column property="qqAprobado" title="QQ Aprobados"
				sortable="true" align="right" />
			<display:column property="vinedo.departamento" title="Depto."
				sortable="true" align="left" />
			<display:column property="vinedo.localidad.nombre" title="Localidad"
				sortable="true" align="left" />
			<logic:equal name="ProcessForm" property="process.INV" value="0">
				<display:column property="observacionINV" title="Observaciones INV"
					sortable="true" align="left" />
			</logic:equal>
			<logic:equal name="ProcessForm" property="process.INV" value="1">
	 			<display:column property="observaciones" title="Observaciones FTyC"
					sortable="true" align="left" />
			</logic:equal>
			<display:column title="Revisar" align="center">
				<a
					href="javascript:evaluar('${reportTable.id}', '${reportTable.vinedo.codigo}', '${reportTable.vinedo.hectareas}', '${reportTable.qqEstimados}', '${reportTable.qqAprobado}', '${reportTable.observaciones}','${reportTable.observacionINV}', '${reportTable.credito.expediente}', '${reportTable.credito.persona.nomb12}');">Modificar</a>
			</display:column>
			<display:column title="Eliminar" align="center">
				<a
					href="javascript:eliminar('${reportTable.id}');">Eliminar</a>
			</display:column>
		</display:table>
	</div>
	<br>
	<br>
	<logic:equal name="ProcessForm" property="process.INV" value="0">
		<div class="title">
			Ingrese Quintales Aprobados para el Vi�edo Seleccionado
		</div>
		<table class="tabla">
		
<%--		<asf:text name="ProcessForm" property="process.idVinedo"--%>
<%--		type="Long" maxlength="100" readonly="true"--%>
<%--		value="${ProcessForm.process.idVinedo}" id="idVinedo" />--%>
				
			<tr>
				<th>
					Expediente:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.expediente"
						type="Text" maxlength="100" readonly="true"
						value="${process.expediente}" id="expediente" />
				</td>
			</tr>
			<tr>
				<th>
					Titular:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.titular"
						type="Text" maxlength="300" readonly="true"
						value="${process.titular}" id="titular"/>
				</td>
			</tr>
			
			<tr>
				<th>
					C.I.V.:
				</th>
				<td>
					<html:text property="process.codigo" value="0" styleId="codigo" />
				</td>
			</tr>
			<tr>
				<th>
					Hectareas:
				</th>
				<td>
					<html:text property="process.hectareas" value="0"
						styleId="hectareas" />
				</td>
			</tr>
			<tr>
				<th>
					QQ Estimados:
				</th>
				<td>
					<html:text property="process.estimados" value="0"
						styleId="estimados" />
				</td>
			</tr>
			<tr>
				<th>
					QQ Aprobados:
				</th>
				<td>
					<html:text property="process.qq" value="0" styleId="qq"
						disabled="true" />
				</td>
			</tr>
			<tr>
				<th>
					Observaciones Propias:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.observacion"
						type="textarea" maxlength="250" readonly="false"
						value="${process.observacion}" />
				</td>
			</tr>
			<tr>
				<th>
					Observaciones INV:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.observacionINV"
						type="textarea" maxlength="250" readonly="true"
						value="${process.observacionINV}" />
				</td>
			</tr>
		</table>
		<html:submit onclick="return rectificarF();" value="Guardar" />
	</logic:equal>
	<logic:equal name="ProcessForm" property="process.INV" value="1">
		<div class="title">
			Ingrese Quintales Aprobados para el Vi�edo Seleccionado
		</div>
		<table class="tabla">		
			<tr>
				<th>
					Expediente:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.expediente"
						type="Text" maxlength="100" readonly="true"
						value="${process.expediente}" id="expediente" />
				</td>
			</tr>
			<tr>
				<th>
					Titular:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.titular"
						type="Text" maxlength="300" readonly="true"
						value="${process.titular}" id="titular"/>
				</td>
			</tr>
			<tr>
				<th>
					C.I.V.:
				</th>
				<td>
					<html:text property="process.codigo" value="0" styleId="codigo"
						disabled="true" />
				</td>
			</tr>
			<tr>
				<th>
					Hectareas:
				</th>
				<td>
					<html:text property="process.hectareas" value="0"
						styleId="hectareas" disabled="true" />
				</td>
			</tr>
			<tr>
				<th>
					QQ Estimados:
				</th>
				<td>
					<html:text property="process.estimados" value="0"
						styleId="estimados" disabled="true" />
				</td>
			</tr>
			<tr>
				<th>
					QQ Aprobados:
				</th>
				<td>
					<html:text property="process.qq" styleId="qq" />
				</td>
			</tr>
			<tr>
				<th>
					Observaciones Propias:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.observacion"
						type="textarea" maxlength="250" readonly="true"
						value="${process.observacion}" />
				</td>
			</tr>
			<tr>
				<th>
					Observaciones INV:
				</th>
				<td>
					<asf:text name="ProcessForm" property="process.observacionINV"
						type="textarea" maxlength="250" readonly="false"
						value="${process.observacionINV}" />
				</td>
			</tr>
		</table>
		<html:submit onclick="return rectificarINV();" value="Guardar" />
	</logic:equal>
</html:form>

<script type="text/javascript">

var ctrlFormulario = $('formulario');
var ctrlAccion = $('accion');
var ctrlId = $('id');
var ctrlIndex = $('vinedoId');
var ctrlCodigo = $('codigo');
var ctrlHectareas = $('hectareas');
var ctrlEstimados = $('estimados');
var ctrlQQ = $('qq');
var ctrlObservacion = $('process.observacion');
var ctrlObservacionINV = $('process.observacionINV');
var ctrlExpediente = $('expediente');
var ctrlTitular = $('titular');
ctrlIndex.style.display='none';


function evaluar(id, codigo, hectareas, estimados, qq, obs, obsINV, expediente,titular) {
	ctrlIndex.value = id;
	ctrlIndex.style.display='none';
	ctrlCodigo.value = codigo;
	ctrlHectareas.value = hectareas;
	ctrlEstimados.value = estimados;
	ctrlExpediente.value = expediente;
	ctrlTitular.value = titular;

	if(qq==null || qq == " "){
		qq = 0;
	}
	ctrlQQ.value = qq;
	ctrlObservacion.value = obs;
	ctrlObservacionINV.value = obsINV;
	//ctrlFormulario.submit();
}

function rectificacion() {
	errores = "";
	ctrlAccion.value = "<%=AccionINV.INV_RECTIFICACION_VINEDO.toString()%>";
	if (ctrlIndex.value == null || ctrlIndex.value == 0) {
		errores = errores + " No se ha seleccionado ning�n vi�edo";
	}
	if (errores == "") {
		msg = "Desea usted que el Fondo modifique los datos de este vi�edo por las siguientes razones:\n";
		msg = msg + " - " + ctrlObservacion.value;
		return confirm(msg);
	}
	alert(errores);

	return false;
}

function rectificarF() {
	errores = "";
	ctrlAccion.value = "<%=AccionINV.SAVE_RECTIFICACIONF_PENDIENTE.toString()%>";
	if (ctrlIndex.value == null || ctrlIndex.value == 0) {
		errores = errores + " No se ha seleccionado ning�n vi�edo\n";
	}
	if (ctrlHectareas.value == null || ctrlHectareas.value == 0) {
		errores = errores + " No se han ingresado hect�reas\n";
	}
	if (ctrlEstimados.value == null || ctrlEstimados.value == 0) {
		errores = errores + " No se ha ingresado la cantidad estimada\n";
	}
	if (errores == "") {
		msg = "Desea usted que el modificar los datos ingresados:\n";
		msg += " - C.I.V.: " + ctrlCodigo.value + "\n";
		msg += " - Hect�reas.: " + ctrlHectareas.value + "\n";
		msg += " - QQ Estimados: " + ctrlEstimados.value + "\n";
		msg += " - Observaciones: " + ctrlObservacion.value;
		return confirm(msg);
	}
	alert(errores);

	return false;
}
function rectificarINV() {
	errores = "";
	ctrlAccion.value = "<%=AccionINV.SAVE_RECTIFICACION_INV_PENDIENTE.toString()%>";
	if (ctrlIndex.value == null || ctrlIndex.value == 0) {
		errores = errores + " No se ha seleccionado ning�n vi�edo\n";
	}
	if (ctrlQQ.value == null || ctrlQQ.value == 0) {
		errores = errores + " No se ha ingresado cantidad aprobada\n";
	}
	if (errores == "") {
		msg = "Desea usted que el modificar los datos ingresados:\n";
		msg += " - QQ Aprobados: " + ctrlQQ.value + "\n";
		msg += " - Observaciones: " + ctrlObservacionINV.value;
		return confirm(msg);
	}
	alert(errores);

	return false;
}

function eliminar(id) {
	ctrlIndex.value = id;
	ctrlAccion.value = "<%=AccionINV.DELETE_RECTIFICACIONF_PENDIENTE.toString()%>";
	ctrlFormulario.submit();
}
</script>
