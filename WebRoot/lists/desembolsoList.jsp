<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<html:form action="actions/abmAction.do?entityName=${param.entityName}">
	<html:hidden property="paramValue[0]" value="${paramValue[0]}" />
	<html:hidden property="paramName[0]" value="credito.id" />
	<html:hidden property="paramComp[0]" value="=" />
	<html:hidden property="do" styleId="accion" value="newEntity" />
	<asf:security action="/actions/abmAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		<button type="submit">
			<bean:message key="abm.button.new" />
		</button>
	</asf:security>
</html:form>
<br>
<br>
<div class="grilla">
	<display:table name="ListResult" property="result" export="true"
		id="reportTable" defaultsort="1"
		requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
		<display:setProperty name="report.title"
			value="Administración Cronograma Desembolso"></display:setProperty>
		<display:column property="id" title="ID" sortProperty="id"
			paramId="entity.id" paramProperty="id" sortable="true"
			href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" />
		<display:column property="numero" title="Numero" sortable="true" />
		<display:column property="fechaStr" title="Fecha" sortable="true" />
		<display:column property="importe" title="Importe" sortable="true" />
		<display:column property="observacion" title="Observación"
			sortable="true" />
		<asf:security action="/actions/abmAction.do"
			access="do=delete&entityName=${param.entityName}">
			<display:column media="html" honClick="return confirmDelete();"
				title="Eliminar"
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&paramValue[0]=${paramValue[0]}&paramName[0]=credito.id&paramComp[0]=%3d&filter=true"
				paramId="entity.id" paramProperty="id">
				<bean:message key="abm.button.delete" />
			</display:column>
		</asf:security>
	</display:table>
</div>
