<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Administración de Funcionarios Responsables</div>
<br><br>
<html:form action="/actions/FuncionarioResponsableAction.do?entityName=${param.entityName}&filter=false">
<html:hidden property="do" value="list" styleId="do" />

<asf:security action="/actions/FuncionarioResponsableAction.do" access="do=newEntity&entityName=${param.entityName}">
	<html:submit onclick="nuevo();" ><bean:message key="abm.button.new"/></html:submit>
</asf:security>
</html:form>
<br><br>

<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable"
	requestURI="${pageContext.request.contextPath}/actions/FuncionarioResponsableAction.do?do=list&entityName=${param.entityName}">
	<display:setProperty name="report.title" value="Administración de Funcionarios Responsables"/>

    <display:column title="ID" media="pdf excel xml" property="id" />
    <display:column title="ID" media="html" sortable="true" >
    	<a href="javascript: modificar( ${ reportTable.id });" >${ reportTable.id }</a>
    </display:column>
    <display:column title="Estado" property="estado.nombreEstado" sortable="true"/>
    <display:column title="Comportamiento de Pago" property="comportamientoNombre" sortable="true"/>
    
    <display:column title="Fecha Desde" property="fechaDesdeStr" sortable="true"/>
    <display:column title="Fecha Hasta" property="fechaHastaStr" sortable="true"/>
    
    <display:column title="Nombre Responsable" property="nombreResponsable" sortable="true"/>
    <display:column title="Usuario Responsable" property="usuario.nombre" sortable="true"></display:column>
    
	<asf:security action="/actions/FuncionarioResponsableAction.do" access="do=delete&entityName=${param.entityName}">        
    	<display:column media="html" title="Eliminar" >
    		<a href="javascript: eliminar( ${ reportTable.id } );">
    			<bean:message key="abm.button.delete" />
    		</a>
    	</display:column>
	</asf:security>
</display:table>
</div>
<script language='javascript' >
//VARIABLES GLOBALES.-
var action = $( 'do' );

//MÉTODOS
function nuevo()
{
	action.value = 'newEntity';
}

function eliminar( id )
{
	var url = "";

	if( confirmDelete() )
	{
		url += "${pageContext.request.contextPath}/actions/FuncionarioResponsableAction.do?";
		url += "do=delete";
		url += "&entityName=${param.entityName}";
		url += "&entity.id=" + id;
		window.location = url;
	}
}

function modificar( id )
{
	var url = "";

	url += '${pageContext.request.contextPath}/actions/FuncionarioResponsableAction.do?';
	url += 'do=load';
	url += '&entityName=${param.entityName}';
	url += '&entity.id=' + id;
	window.location = url;
}

</script>
<logic:equal parameter="reset" value="true" >
	<script language="javascript">
	    $( "do" ).value = "list";
    	document.funcionarioResponsableForm.submit();
    </script>
</logic:equal>