<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript">
	function nuevo() {
		if ('${paramValue[0]}' != '') {
			getElem('do').value = 'newEntity';
			window.location.href = "${pageContext.request.contextPath}/actions/abmAction.do?entityName=${param.entityName}&filter=true";
		}
	}
</script>

<div class="title">Administración de Requisitos de Desembolso</div>
<html:form
	action="/actions/abmAction.do?entityName=${param.entityName}&filter=true&entity.desembolsoId=${paramValue[0]}&idObjetoi=${param.idObjetoi}">
	<html:hidden property="paramValue[0]" styleId="paramValue"
		value="${paramValue[0]}" />
	<html:hidden property="paramName[0]" styleId="paramName"
		value="desembolsoId" />
	<html:hidden property="paramComp[0]" styleId="paramComp" value="=" />
	<html:hidden property="filter" value="true" />
	<html:hidden property="do" styleId="do" value="list" />
	<html:hidden property="idObjetoi" styleId="idObjetoi"
		value="${param.idObjetoi}" />
	<asf:security action="/actions/abmAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		<html:submit onclick="nuevo();">Nuevo</html:submit>
	</asf:security>
</html:form>
<br>

<br>
<div class="grilla">
	<html:form
		action="/actions/process.do?do=process&processName=CreditoRequisitos">
		<html:hidden property="idDesembolso" styleId="idDesembolso"
			value="${paramValue[0]}" />
		<html:hidden property="idObjetoi" styleId="idObjetoi"
			value="${param.idObjetoi}" />
		<display:table name="ListResult" property="result" defaultsort="1"
			export="true" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
			<display:setProperty name="report.title"
				value="Administración de Requisitos de Desembolso"></display:setProperty>
			<display:column media="html" property="id" title="Código"
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load&idObjetoi=${param.idObjetoi}"
				paramId="entity.id" paramProperty="id" sortable="true" />
			<display:column media="excel pdf xml" title="Código" property="id" />
			<display:column title="Nombre" property="requisito.nombre"
				sortable="true" />
			<display:column title="Detalle" property="requisito.descripcion" />
			<display:column title="Fecha Cumplimiento"
				property="fechaCumplidoStr" sortable="true" />
			<display:column title="Observaciones" property="observaciones" />
			<asf:security action="/actions/abmAction.do"
				access="do=delete&entityName=${param.entityName}">
				<display:column media="html" honClick="return confirmDelete();"
					title="Eliminar"
					href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&filter=true&idObjetoi=${param.idObjetoi}"
					paramId="entity.id" paramProperty="id">Eliminar</display:column>
			</asf:security>
			<display:column title="Cumplido">
				<logic:empty name="reportTable" property="fechaCumplido">
					<input type="checkbox" name="requisito-${reportTable.id}"
						value="${reportTable.id}" />
				</logic:empty>
				<logic:notEmpty name="reportTable" property="fechaCumplido">
					<input checked="checked" disabled="disabled" type="checkbox"
						name="requisito-${reportTable.id}" value="${reportTable.id}" />
				</logic:notEmpty>
			</display:column>
		</display:table>
		<br />
		<html:submit>Guardar</html:submit>
	</html:form>
</div>

<html:form
	action="/actions/process.do?do=process&processName=CreditoDesembolsos&process.idObjetoi=${param.idObjetoi}">
	<html:submit>Volver</html:submit>
</html:form>