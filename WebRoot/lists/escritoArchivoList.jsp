<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">
	Administración de Escritos
</div>
<br>
<br>
<html:form
	action="/actions/EscritoArchivoAction.do?entityName=${param.entityName}">
	<html:hidden property="do" styleId="do" value="list" />
	<asf:security action="/actions/EscritoArchivoAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		<input type="submit" onclick="getElem('do').value='newEntity';"
			value="<bean:message key="abm.button.new"/>" />
	</asf:security>
</html:form>
<display:table name="ListResult" property="result" defaultsort="1"
	pagesize="5" id="reportTable"
	requestURI="${pageContext.request.contextPath}/actions/EscritoArchivoAction.do?do=list&entityName=${param.entityName}">
	<display:setProperty name="report.title"
		value="Administración de Escritos"></display:setProperty>
	<display:column media="html" sortProperty="id" property="id"
		title="ID Archivo"
		href="${pageContext.request.contextPath}/actions/EscritoArchivoAction.do?do=load&entityName=${param.entityName}&load"
		paramId="entity.id" paramProperty="id" />
	<display:column property="nombre" title="Nombre del Escrito" />
	<display:column property="detalle" title="Detalle Escrito" />
	<display:column property="tipoStr" title="Tipo" />
	<display:column title="Descargar">
		<html:button property="" value="Bajar Documento"
			onclick="window.open('${pageContext.request.contextPath}/actions/EscritoArchivoAction.do?id=${reportTable.id}&do=descargar')" />
	</display:column>
	<asf:security action="/actions/EscritoArchivoAction.do"
		access="do=delete&entityName=${param.entityName}">
		<display:column media="html" honClick="return confirmDelete();"
			title="Eliminar"
			href="${pageContext.request.contextPath}/actions/EscritoArchivoAction.do?do=delete&entityName=${param.entityName}"
			paramId="entity.id" paramProperty="id">Eliminar</display:column>
	</asf:security>
</display:table>
<script language='javascript'>
Event.observe(window, 'load', function() {
	this.name = "Documentacion";
	parent.pop(this);
});
</script>