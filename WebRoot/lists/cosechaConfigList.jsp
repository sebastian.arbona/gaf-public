<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div class="title">
	Cosecha y Acarreo
</div>
<p>

<button onclick="window.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=CosechaConfigProcess';"><bean:message key="abm.button.new"/></button>

<div class="grilla">
<br>
<display:table name="ListResult" property="result" export="true" defaultsort="1" id="result" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">

    <display:column title="Temporada" property="temporada" sortable="true"/>
    <display:column title="Varietales">
    	<logic:equal value="true" name="result" property="varietales">
    		Si
    	</logic:equal>
    	<logic:equal value="false" name="result" property="varietales">
    		No
    	</logic:equal>
    </display:column>
    <display:column title="Costo por QQ a cosechar" property="costoQQCosechaStr" sortable="true"/>
    <display:column title="Costo por QQ a elaborar" property="costoQQElabStr" sortable="true"/>
    
	<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
		<display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete" /></display:column>
	</asf:security>
		<display:column title="Modificar"><a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CosechaConfigProcess&process.action=cargar&process.idCosechaConfig=${result.id}">MODIFICAR</a></display:column>
    
</display:table>
</div>