<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Administraci�n de Formatos de �tems</div>
<p>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
</asf:security>
</br></br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" defaultsort="1"  id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
<display:setProperty name="report.title" value="Administraci�n de Formatos de �tems"></display:setProperty>

    <display:column title="Formato de Importaci�n" sortable="true"  >
        <a href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load=load&formatoImportacion=${ reportTable.formatoimportacion.detalle }&entity.id.idformato=${ reportTable.id.idformato }&entity.id.item=${ reportTable.id.item}&entity.id.posicion=${ reportTable.id.posicion}">${ reportTable.formatoimportacion.detalle}</a>
    </display:column>
        
    <display:column title="Item" property="id.itemTpf(Item_Importacion_GAT)" sortable="true" />
    <display:column title="Posici�n" property="id.posicion" sortable="true" />
    <display:column title="Longitud" property="longitud" sortable="true"/>
    <display:column title="C�digo Detalle" property="codigodetalle" sortable="true" />
    <display:column title="C�digo Controlador" property="codigocontrolador" sortable="true" />
       
	<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
		<display:column media="html" title="Eliminar" >                    
                    <a onClick="return confirmDelete();" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&load&entity.id.idformato=${ reportTable.id.idformato }&entity.id.item=${ reportTable.id.item}&entity.id.posicion=${ reportTable.id.posicion}"><bean:message key="abm.button.delete" /></a>
                </display:column>
	</asf:security>
    
</display:table>
</div>
