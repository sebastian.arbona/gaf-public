<%@page import="com.nirven.creditos.hibernate.InformeDeuda"%>
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script type="text/javascript">
	function verInforme(id) {
		var action = document.getElementById("action");
		action.value = "verInforme";
		var idInforme = document.getElementById("idInforme");
		idInforme.value = id;
		document.forms["ProcessForm"].submit();
	}
	
	function ejecutarInforme(id) {
		var action = document.getElementById("action");
		action.value = "ejecutarInforme";
		var idInforme = document.getElementById("idInforme");
		idInforme.value = id;
		document.forms["ProcessForm"].submit();
	}
	
	function nuevoInforme() {
		var action = document.getElementById("action");
		action.value = "nuevoInforme";
		document.forms["ProcessForm"].submit();
	}
</script>

<div class="title">Informes de Deuda</div>
<br>

<html:form action="/actions/process.do?do=process&processName=InformeDeudaProcess" styleId="ProcessForm">
    <html:hidden property="process.cid"/>
    <html:hidden property="process.action" styleId="action" value="buscar"/>
    <html:hidden property="process.idInforme" styleId="idInforme"/>

	<table border="0">
		<tr>
			<th>Creado desde</th>
			<td><asf:calendar property="ProcessForm.process.fechaDesde" value="${process.fechaDesde}"/></td>
		</tr>
		<tr>
			<th>Hasta</th>
			<td><asf:calendar property="ProcessForm.process.fechaHasta" value="${process.fechaHasta}"/></td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Buscar"></html:submit>
				<html:submit value="Nuevo Informe" onclick="nuevoInforme();"></html:submit>
			</th>
		</tr>
	</table>
	
	<display:table name="ProcessForm" property="process.informes" export="true" id="informe">
		<display:column title="Fecha Creacion" property="fechaCreacion" decorator="com.asf.displayDecorators.DateDecorator"/>
		<display:column title="Lineas">
			<span title="${informe.lineasId}">${informe.lineasDesc}</span>
		</display:column>
		<display:column title="Fecha Calculo" property="fechaHasta" decorator="com.asf.displayDecorators.DateDecorator"/>
		<display:column title="Fecha Ejecucion" property="fechaEjecucion" decorator="com.asf.displayDecorators.DateDecorator"/>
		<display:column title="Estado" property="estado.texto"/>
		<display:column title="Total Creditos" property="creditos"/>
		<display:column title="Procesados" property="procesados"/>
		<display:column title="Progreso">
			<table style="border: 1px solid; width: 100%; border-spacing: 0">
				<tr>
					<td style="padding: 0;">
						<hr style="border: none; margin: 0; background-color: <%= ((InformeDeuda)informe).getProcesados() == ((InformeDeuda)informe).getCreditos() && ((InformeDeuda)informe).getCreditos() > 0 ? "#AAD72F" : "#AA272F" %>; height: 10px; width: <%= Math.round(((InformeDeuda)informe).getProcesados() * 1.0 / ((InformeDeuda)informe).getCreditos() * 100) %>%" align="left"/>
					</td>
				</tr>
			</table>
		</display:column>
		<display:column title="Ejecutar">
			<logic:equal name="informe" property="estado" value="LISTO">
				<a href="javascript:void(0);" onclick="ejecutarInforme(${informe.id});">Ejecutar</a>
			</logic:equal>
			<logic:equal name="informe" property="estado" value="ERROR">
				<a href="javascript:void(0);" onclick="ejecutarInforme(${informe.id});">Ejecutar</a>
			</logic:equal>
		</display:column>
		<display:column title="Ver Informe">
			<logic:equal name="informe" property="estado" value="TERMINADO">
				<a href="javascript:void(0);" onclick="verInforme(${informe.id});">Ver</a>
			</logic:equal>
		</display:column>
	</display:table>
</html:form>


