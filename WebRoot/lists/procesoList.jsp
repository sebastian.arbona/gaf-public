<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmldom.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlsax.js"></script>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/xmlw3cdom.js"></script>

<script language="javascript" type="text/javascript">

/*$(document).ready(function(){
	
		//Se ocultan todas los elementos
		   $('#nombre').hide();
		   $('#expediente').hide();
		   $('#caratula').hide();
		   $('#juzgado').hide();
		   $('#definitivo').hide();
		   $('#fecha').hide();
		   $('#tipo').hide();
		   $('#monto').hide();
	   });*/
	   
	/*function ocultar(value){
			//Se muestran los elementos que corresponden al proceso seleccionado
		   alert(value);
		   $j = jQuery.noConflict();
		   //var idTipoProceso = ${paramValue[0]};
			 $j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=getElementosAMostrar', 
				'&idTipoProceso='+idTipoProceso,
				function(data) {
				var elem = data.split('-');
					for (x=0;x<elem.length;x++){
						$('#'+elem[x]).show();
					}
				}); 
		
	
	}*/
	   
	   


	function filtrar()
    {
		 getElem("AbmForm").submit();
    }

	function newEntity(){
        window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}";
    }
</script>

<div class="title">Administración de Procesos</div>
<br><br>

<html:form action="/actions/abmAction.do?entityName=${param.entityName}&do=list&filter=true" styleId="AbmForm">
<html:hidden property="paramName[0]" value="tipoProceso.id" styleId="idTipoProceso"/>
<html:hidden property="paramComp[0]" value="=" />
<br>
<table>
	<tr>
		<th>Tipo de Proceso: </th>
		<td>
			<asf:select name="paramValue[0]" entityName="TipoProceso" listCaption="id,nombreTipoProceso"
			listValues="id"
			orderBy="id" value="${paramValue[0]}" attribs="onchange='filtrar();'" nullText="Seleccione un Tipo de Proceso" nullValue="true" />
		</td>
		
	</tr>
</table>
</html:form>
<br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
    <button type="button" onclick="javascript:newEntity();"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1"
	requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}" >
	<display:setProperty name="report.title" value="Administración de Procesos"></display:setProperty>  

	<display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
		href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" />
	<display:column property="nombreProceso" title="Nombre del Proceso" sortable="true"/>
	<div id="nombre"><display:column property="nombre" title="Nombre" sortable="true" /></div>
	<div id="expediente"><display:column property="expediente" title="Expediente" sortable="true" /></div>
	<div id="caratula"><display:column property="caratula" title="Carátula" sortable="true" /></div>
	<div id="juxgado"><display:column property="juzgado" title="Juzgado" sortable="true" /></div>
	<div id="definitivo"><display:column property="definitivo" title="Definitivo" sortable="true" /></div>
	<display:column property="fechaAltaStr" title="Fecha Alta" sortable="true" />
	<div id="fecha"><display:column property="fechaStr" title="Fecha" sortable="true" /></div>
	<display:column property="fechaVencimientoStr" title="Fecha Vencimiento" sortable="true" />
	<div id="tipo"><display:column property="tipo" title="Tipo" sortable="true" /></div>
	<div id="monto"><display:column property="monto" title="Monto" sortable="true" /></div>
	
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
	<display:column media="html" honClick="return confirmDelete();"
		title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}"
		paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete" />
	</display:column>
</asf:security>

</display:table>
</div>