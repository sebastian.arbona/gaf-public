<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div class="title">Hist&oacute;rico de Movimientos Instrumento de Garant&iacute;a</div>
<br><br>
<html:form action="/actions/process.do?do=process&processName=GarantiaCustodiaProcess">
	<html:hidden property="process.cid"/>
	
	<div class="grilla">
		<table>
			<tr>
				<th>Garant&iacute;a</th>
				<td>
					${ProcessForm.process.objetoiGarantia.garantia.identificadorUnico}
				</td>
			</tr>
			<tr>
				<th>Tipo</th>
				<td>
					${ProcessForm.process.objetoiGarantia.garantia.tipo.nombre}
				</td>
			</tr>
		</table>
		
		<display:table name="ProcessForm" property="process.custodias" export="true" id="reportTable">
			<display:setProperty name="report.title" value="Hist&oacute;rico de Movimientos Instrumento de Garant&iacute;a"></display:setProperty>
			<display:column title="Orden" property="orden" />
		    <display:column title="Fecha Mov" property="fechaIngreso" decorator="com.asf.displayDecorators.DateDecorator"/>
		    <display:column title="Sector" property="sector.nombre"/>
		    <display:column title="Responsable" property="responsable.nomb12"/>
		    <display:column title="Ubicaci&oacute;n F&iacute;sica" property="ubicacion"/>
		    <display:column title="Fecha L&iacute;mite Pr&eacute;stamo Instrumento" property="fechaDevolucion" decorator="com.asf.displayDecorators.DateDecorator"/>
		    <display:column title="Expediente" property="expediente"/>
		</display:table>
		
		<div style="margin: 10 auto; width: 400px; font-size: 10pt; font-weight: bold; text-transform: uppercase;">
			<a href="${pageContext.request.contextPath}/actions/process.do?processName=GarantiaProcess&do=process&process.accionStr=MODIFICAR_GARANTIA&process.idSolicitud=${ProcessForm.process.objetoiGarantia.objetoi.id}&process.oGarantiaId=${ProcessForm.process.objetoiGarantia.id}">
				Volver a la Garant&iacute;a
			</a>
		</div>
	</div>

</html:form>