<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
<script language="javascript" type="text/javascript">
	/**
	 * @inputIn: control sobre el input que dispara la consulta ajax.-
	 * @defecto: para parametro=valor que tiene,
	 * a la izquierda del igual el id del input que dispara la consulta ajax.-
	 * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
	 */
	function actualizarLista(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&persona=" + inputIn.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}
	function actualizar(inputIn, defecto) {
		var urlDinamica = "";
		urlDinamica += "&expediente=" + inputIn.value;
		urlDinamica += "&" + defecto;
		return urlDinamica;
	}
</script>

<div class="title">Gastos a Recuperar a Cargo del Inversor</div>
<br />
<div align="left" style="width: 70%">
	<html:errors />
</div>
<br />
<html:form
	action="/actions/process.do?do=process&processName=GastosRecuperarProcess"
	styleId="ProcessForm">
	<html:hidden property="process.accion" styleId="accion" />
	<html:submit onclick="crear();" value="Nueva Factura"
		style="margin: 20px 0;"></html:submit>
	<table>
		<tr>
			<th>Desde:</th>
			<td><asf:calendar property="ProcessForm.process.desde"
					maxlength="10"></asf:calendar></td>
			<th>Hasta:</th>
			<td><asf:calendar property="ProcessForm.process.hasta"
					maxlength="10"></asf:calendar></td>
			<th>Condicion:</th>
			<td colspan="1"><asf:select name="process.condicion"
					entityName="com.asf.hibernate.mapping.Tipificadores"
					value="${ProcessForm.process.condicion}"
					listCaption="getCodigo,getDescripcion" listValues="getCodigo"
					filter="categoria='detalleFactura.condicion' order by codigo"
					nullValue="true" nullText="&lt;&lt; Todas &gt;&gt;" /></td>
			<td></td>
		</tr>
		<tr>
			<th>Proyecto:</th>
			<td><html:text property="process.proyecto" name="ProcessForm" /></td>
			<th>L�nea</th>
			<td colspan="3"><asf:select
					entityName="com.nirven.creditos.hibernate.Linea"
					attribs="class='Linea'" listCaption="id,nombre" listValues="id"
					name="process.lineaId" value="${process.lineaId}"
					filter="activa=true" nullText="&lt;&lt;Seleccione&gt;&gt;"
					nullValue="true" /></td>
			<td></td>
		</tr>
		<tr>
			<th>Pagada</th>
			<td><asf:select name="process.pagada"
					value="${ProcessForm.process.pagada}" listCaption="Todas,Si,No"
					listValues="Todas,Si,No" /></td>
			<th>Impactada</th>
			<td colspan="4"><asf:select name="process.impactada"
					value="${ProcessForm.process.impactada}" listCaption="Todas,Si,No"
					listValues="Todas,Si,No" /></td>
		</tr>
		<tr>
			<th>Expediente Tomador</th>
			<td colspan="6"><asf:selectpop name="process.expediente"
					entityName="DocumentoADE" title="Expediente"
					value="${empty ProcessForm.process.expediente ? '' : ProcessForm.process.expediente}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI"
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					cantidadDescriptors="2" /></td>
		</tr>
		<tr>
			<th>Tomador</th>
			<td><asf:selectpop name="process.idPersona" title="Persona"
					columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
					captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
					entityName="com.civitas.hibernate.persona.Persona"
					value="${ProcessForm.process.idPersona}" /> (Codigo-Apellido y
				Nombre-Documento)</td>
			<td colspan="5"><html:submit onclick="listar();" value="Buscar"></html:submit>
			</td>
		</tr>
	</table>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.listado"
			export="true" id="reportTable" style="margin: 20px 0;"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=GastosRecuperarProcess">
			<display:setProperty name="report.title"
				value="Gastos a Recuperar a Cargo del Inversor" />
			<display:column class="center" title="Fecha" property="factura.fecha"
				decorator="com.asf.displayDecorators.DateDecorator"
				paramId="process.idFactura" paramProperty="factura.id"
				href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=GastosRecuperarProcess&process.accion=modificar" />
			<display:column title="Proveedor"
				property="factura.proveedor.persona.nomb12" sortable="true" />
			<display:column title="Pagado">
				<logic:empty name="reportTable" property="factura.fechaPago">
      					No
      				</logic:empty>
				<logic:notEmpty name="reportTable" property="factura.fechaPago">
      					S�
      				</logic:notEmpty>
			</display:column>
			<display:column title="Sucursal" property="factura.sucursal" />
			<display:column title="Nro Factura" property="factura.numero"
				sortable="true" />
			<display:column title="Expte Pago" property="factura.expepago" />
			<display:column title="Total" property="factura.importe"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"
				align="right" />
			<display:column title="Moneda" property="factura.moneda.abreviatura" />
			<display:column title="Tomador" property="credito.persona.nomb12" />
			<display:column title="Proyecto" property="credito.numeroAtencion"
				sortable="true" />
			<display:column title="Estado Proyecto"
				property="credito.estadoActual.estado" sortable="true" />
			<display:column title="Expediente" property="expediente" />
			<display:column title="Importe" property="importe" sortable="true"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"
				align="right" />
			<display:column title="Observaciones" property="observaciones" />
			<display:column title="Motivo" property="factura.motivo" />
			<display:column class="center" title="Fecha Cobranza Boleto"
				property="deudoresVarios.fechaPago"
				decorator="com.asf.displayDecorators.DateDecorator" sortable="true" />
			<display:column title="Nro Boleto" property="deudoresVarios.numero"
				sortable="true" />
			<display:column class="center" title="Fecha Emision Boleto"
				property="deudoresVarios.fechaEmision"
				decorator="com.asf.displayDecorators.DateDecorator" sortable="true" />
			<display:column title="Impactado">
				<logic:notEqual name="reportTable" property="impactado" value="true">
                        No
                    </logic:notEqual>
				<logic:equal name="reportTable" property="impactado" value="true">
                        S�
                    </logic:equal>
			</display:column>
			<display:column title="Eliminar">
				<a
					href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=GastosRecuperarProcess&process.accion=delete&process.idFactura=${reportTable.factura.id}"
					onclick="return confirm('Est� seguro?');">Eliminar</a>
			</display:column>
			<display:footer>
				<tr>
					<td colspan="11"><div align="right">
							<strong>TOTAL:</strong>
						</div></td>
					<td align="right">${ProcessForm.process.totalStr}</td>
					<td colspan="3"></td>
				</tr>
			</display:footer>
		</display:table>
	</div>
</html:form>

<script>
	//var form = $('ProcessForm');
	var accion = $('accion');

	function listar() {
		accion.value = 'listar';
		return true;
	}

	function crear() {
		accion.value = 'crear';
		return true;
	}
</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>