<%@ page language="java"%>
<%@page import="org.jbpm.taskmgmt.exe.TaskInstance"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 


<div class="title">Historial de Versiones</div>
<br/><br/>


<div class="grilla">
	<display:table name="versionList" export="true" id="version" requestURI="${pageContext.request.contextPath}/actions/jbpm.do?do=getVariableVersions"
		defaultsort="1">
		<display:column property="version" title="Version" />
		<display:column property="name" title="Archivo" />		
		<display:column property="fecha" title="Fecha" decorator="com.nirven.jbpm.displayDecorators.DateDecorator" />		
		<display:column title="Descargar" media="html">
			<a href="${pageContext.request.contextPath}/actions/jbpm.do?do=getVariableContent&id=${version.id}">descargar</a>
		</display:column>
	</display:table>
</div>
