<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<script type="text/javascript">
	
    function nuevo(){
        if('${paramValue[0]}'!='')
        {
            getElem('do').value='newEntity';
            window.location.href="${pageContext.request.contextPath}/actions/bonificacionesAction.do?entityName=${param.entityName}&filter=true";
        }
    }
</script>

<div class="title">Bonificaciones de Cr�dito</div>
<html:form action="/actions/abmAction.do?entityName=${param.entityName}&filter=true&entity.idCredito=${paramValue[0]}" >
    <html:hidden property="paramName[0]" styleId="paramName" value="idCredito"/>
    <html:hidden property="paramComp[0]" styleId="paramComp" value="="/>
    <html:hidden property="paramValue[0]" styleId="paramValue" value="${paramValue[0]}"/>
    <input type="hidden" name="idIndice" value="${param.idIndice}"/>
    <html:hidden property="filter" value="true"/>
    <html:hidden property="do" styleId="do" value="list"/>

<html:errors/>

    <asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
        <html:submit onclick="nuevo();">Nuevo</html:submit>
    </asf:security>
</html:form>
<br><br>
<div class="grilla">

<display:table name="ListResult" property="result" defaultsort="1" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/bonificacionesAction.do?do=list&entityName=${param.entityName}">
<display:setProperty name="report.title" value="Bonificaciones de Cr�dito"></display:setProperty>
	<display:column media="html" property="bonificacion.convenio.nombre" title="Convenio"/>
    <display:column media="html" property="ente.detaBa" title="Entidad bonificadora"/>
    <display:column media="html" property="bonificacion.tasaBonificadaStr" title="Bonificaci�n"/>
    <display:column media="html" property="bonificacion.tipoBonificacionStr" title="Tipo Bonif."/>
    
	<%-- <asf:security action="/actions/bonificacionesAction.do" access="do=load&entityName=${param.entityName}">--%>	   
    <%-- </asf:security> --%>
    <%-- <asf:security action="/actions/bonificacionesAction.do" access="do=delete&entityName=${param.entityName}"> --%>
        <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/bonificacionesAction.do?do=delete&idIndice=${param.idIndice}&entityName=${param.entityName}&filter=true" paramId="entity.id" paramProperty="id">Eliminar</display:column>
    <%-- </asf:security> --%>
 
</display:table>
</div>

<div>
	<table>
		<tr>
			<th>Tasa Neta:</th>
			<td><input type="text" id="tasaNeta" /></td>
		</tr>
		<tr>
			<th>Bonificaci�n Total Estimada:</th>
			<td><input type="text" id="bonificacionEstimada" /></td>
		</tr>
	</table>
</div>

<div style="width: 100%; text-align: center">
	<input type="button" value="Volver" onclick="volver();"/>
</div>
<script type="text/javascript">


		$j = jQuery.noConflict();

		$j(document).ready(function() {
			var idCredito = $j("#paramValue").val();
			$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularTasaNeta', 
					'idCredito='+idCredito,
					function(data) {
						$j("#tasaNeta").val(data);
					});
			$j.get('${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=calcularBonificacionEstimada', 
					'idCredito='+idCredito,
					function(data) {
						$j("#bonificacionEstimada").val(data);
					});
		});

		function volver() {
			var url = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoDatosFinancieros&process.action=loadIndice&process.idObjetoiIndice=${param.idIndice}";
    		window.location = url;
    	}
</script>