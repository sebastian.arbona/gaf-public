<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
    <br/>   
	<br/>
	<input type="button" value="Nueva Bonificacion de Tasa" onclick="nueva();" />
    <br/>
	<br/>
    <html:select property="process.filtro" name="ProcessForm" value="${ProcessForm.process.filtro}" onchange="cargarFiltro();" styleId="idFiltro">
		<html:option value="ninguno">Ning�n Filtro</html:option>
		<html:option value="numero">Numero</html:option>
		<html:option value="expediente">Expediente</html:option>
		<html:option value="titular">Titular</html:option>
	</html:select>
	<br/>
	<br/>
	<logic:equal value="numero" property="process.filtro" name="ProcessForm">
	<table>
	<tr>
		<th>Nro Bonificaci�n</th>
		<td>
			<asf:text maxlength="40" name="ProcessForm" type="long" property="process.bonTasa.numeroBonificacion"/>
		</td>
	</tr>
	</table>	
	<br/>
	<br/>	
	</logic:equal>
	<logic:equal value="expediente" property="process.filtro" name="ProcessForm">
	<table>
	<tr>
		<th>Expediente N�</th>
		<td>
			<asf:text maxlength="40" name="ProcessForm" type="text" property="process.bonTasa.expediente"/>
		</td>
	</tr>
	</table>
	<br/>
	<br/>		
	</logic:equal>
	<logic:equal value="titular" property="process.filtro" name="ProcessForm">
	<table>
		<tr>
		<th>C�digo de Persona:</th>
		<td><asf:selectpop name="process.bonTasa.persona.id" title="Persona"
			columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
			captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
			entityName="com.civitas.hibernate.persona.Persona" value="${ProcessForm.process.bonTasa.persona.id}" />
		</td>
		</tr>	
	</table>		
	<br/>
	<br/>
	</logic:equal>
	<logic:notEqual name="ProcessForm" value="ninguno" property="process.filtro">
		<input type="button" value="Filtrar" onclick="filtrar();">
		<br/>
		<br/>
	</logic:notEqual>
	<logic:empty name="ProcessForm" property="process.bonificaciones">
    	No hay Bonificaciones de Tasa
    	<br/>
    	<br/>
    </logic:empty>
    <logic:notEmpty name="ProcessForm" property="process.bonificaciones">
    	<display:table name="ProcessForm" property="process.bonificaciones" id="bonificacion">    		
            <display:column title="ID" media="html" sortable="true" >
                <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaProcess&process.idBonTasa=${bonificacion.id}&process.accion=verGeneral&process.idPersona=${process.idPersona}" >${bonificacion.id}</a>
            </display:column>
            <display:column title="Nro Bonificaci�n" property="numeroBonificacion" sortable="true"/>
            <display:column title="Expediente N�" property="expediente" sortable="true"/>
            <display:column title="Titular" property="persona.nomb12"/>
            <display:column title="Fecha Contrato" property="fechaMutuo" sortable="true"/>
            <display:column title="Destino de Fondos" property="objeto" sortable="true"/>
          	<display:column title="Agente Financiero" property="banco.detaBa" sortable="true"/>
            <display:column title="Estado" property="estadoActual.estado.nombreEstado" sortable="true" />
    	</display:table>
    </logic:notEmpty>   
</html:form>
<script language="javascript" type="text/javascript">
    function nueva(){
		window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=NuevaBonTasaGeneral&process.accion=nueva&esPersona=false";	
	}
	function cargarFiltro() {
		var filtro = $('idFiltro').value;
		window.location="${pageContext.request.contextPath}/actions/process.do?&do=process" +
						"&processName=BonTasaProcess" +
						"&process.accion=general" +
						"&process.filtro=" + filtro;
	}
	function filtrar(){
		var form = $('oForm');
		var accion = $('accion');
		accion.value = 'filtrar';
		form.submit();
	}	
</script>    