<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />

<body class=" yui-skin-sam">
	<div class="title">
		Administración de Movimientos de Instancias
	</div>
	<br>
	<br>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
		
<div><html:errors /></div>
</br>
</body>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="nuevoMovimiento" />
		<html:hidden property="process.sid"/>
		
		<html:hidden name="ProcessForm" property="process.procesoResolucion.id"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.resolucion.numero"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.resolucion.id"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.resolucion.proyecto.id" styleId="idProyecto"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.resolucion.id" styleId="idSolicitud"/>
		<html:hidden property="process.idproyectoPop"/>	
			
		<asf:security action="/actions/process.do?do=process&processName=${param.processName}" access="accion=nuevoMovimiento">
			<html:submit onclick="getElem('accion').value='nuevoMovimientoBlanco';" value="Nuevo Movimiento"/>
		</asf:security>
	</html:form>
	<br>
	
	
	<b>Número de resolución:
		${ProcessForm.process.procesoResolucion.resolucion.numero} </b>

	<br>
	<b>Proyecto:
		${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion} </b>

	<br>
	<b>Titular:
		${ProcessForm.process.procesoResolucion.resolucion.proyecto.persona.nomb12Str} </b>

	<br>
	<br>

	<display:table name="ProcessForm" property="process.movimientosInstancia" export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listarMovimientos">
		<display:setProperty name="report.title" value="Administración de Movimientos - Resolucion:${ProcessForm.process.procesoResolucion.resolucion.numero} - Proyecto: ${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion} - Titular: ${ProcessForm.process.procesoResolucion.resolucion.proyecto.persona.nomb12Str}"></display:setProperty>
		<display:column title="Nro Movimiento" media="html" sortable="true">
			<a href="javascript: show( ${ reportTable.id}  );">${reportTable.id}</a>
		</display:column>
		<display:column property="instancia.resolucion.proyecto.numeroAtencion" title="Número Proyecto" />
		<display:column property="tipoMovimientoInstancia.nombre" title="Tipo de Movimiento" />
		<display:column property="fechaMovimientoStr" title="Fecha Movimiento" />
		<display:column property="fechaCargaMovimientoStr" title="Fecha Carga Movimiento" />
		<display:column property="observacion" title="Observacion"/>
		<display:column title="Eliminar" media="html">
			<a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>	
		</display:column>
	</display:table>
	
	<div style="margin-top: 10px">
			
			<input type="button" value="Volver" onclick="volver();"/>
		</div>

	<script type="text/javascript">


		
		function show(id) {
		
			var url = '';
		
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=showMovimientoBlanco&';
			url += 'process.movimientoInstancia.id=' + id
			url += '&process.idproyectoPop=' + document.getElementById('idProyecto').value;
			url += '&process.idSolicitud=' + document.getElementById('idSolicitud').value;

			window.location = url;
		}
		
		function eliminar(id) {
			var url = '';
		
			if (confirmDelete()) {
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}&';
				url += 'process.accion=eliminarMovimientoBlanco&';
				url += 'process.movimientoInstancia.id=' + id
				url += '&process.idproyectoPop=' + document.getElementById('idProyecto').value;

		
				window.location = url;
			}
		}	
		function instancias(id){
				var url = '';
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}&';
				url += 'process.accion=listarInstancias&';
				url += 'process.idSolicitud=' + id;
		
				window.location = url;
		}
		
		function volver(){

			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}';
			url += '&process.accion=listarInstanciasFiltroInicio';
			url += '&process.idproyectoPop=' + document.getElementById('idProyecto').value;
			url += '&process.idSolicitud=' + document.getElementById('idSolicitud').value; 

			window.location = url;
		}
		// fin modificar.-   
		
		function movimientos(id){
			
			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=listarMovimientos&';
			url += 'process.procesoResolucion.id=' + id;
			url += '&process.procesoResolucion.resolucion.proyecto.id=' + id;
			url += '&process.idproyectoPop=' + document.getElementById('idProyecto').value;
	       
			window.location = url;
}

</script>