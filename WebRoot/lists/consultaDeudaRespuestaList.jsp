<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="grilla">

	<display:table name="result" id="respuesta" export="false" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.consulta_id=${ProcessForm.process.consulta_id}">
		<display:column sortProperty="persona.persona.nomb12" title="Nombre y Apellido">
			<logic:equal name="ProcessForm" property="process.readOnly" value="true">
				${respuesta.persona.persona.nomb12}
			</logic:equal>
			<logic:equal name="ProcessForm" property="process.readOnly" value="false">
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.consulta_id=${ProcessForm.process.consulta_id}&process.entidadStr=${ProcessForm.process.entidadStr}&process.consultaPersona_id=${respuesta.persona.id}">${respuesta.persona.persona.nomb12}</a>
			</logic:equal>
		</display:column>
		<display:column property="persona.persona.cuil12" title="CUIT/CUIL"></display:column>
		<display:column property="persona.caracter" title="Car�cter"></display:column>
		<display:column property="fechaConsultaStr" title="Fecha de Consulta"></display:column>		
		<display:column property="monto" title="Monto"></display:column>
		<display:column property="fechaVencimientoStr" title="Fecha de Vencimiento"></display:column>
        
	</display:table>
	
	
</div>
<script>
var bs=window.parent.document.getElementsByTagName("input");

for(i = 0; i < bs.length; i++){
	if(bs[i].type === 'submit')
		bs[i].disabled=false;
}
</script>
    