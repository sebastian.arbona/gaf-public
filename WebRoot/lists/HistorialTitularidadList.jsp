<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<link rel="stylesheet" href="/CREDITOS/style/frames.css" type="text/css">

<div style="width:70%" align="left"><html:errors/></div>
<div class="title">
	Historial de Titularidad
</div>

<asf:security action="actions/process.do?processName=${param.processName}&do=process" access="do=new">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/process.do?do=process&processName=HistorialTitularidadProcess&process.action=new&process.idSolicitud=${ProcessForm.process.idSolicitud}';" value="Nuevo Titular">Nuevo Titular</button>
</asf:security>
<html:form
	action="/actions/process.do?processName=${param.processName}&do=process">
	<html:hidden property="process.action" value="list" styleId="action" />
	<html:hidden property="process.idSolicitud" value="${ProcessForm.process.idSolicitud}" styleId="action" />
	<div class="grilla">
		<br>
		<display:table name="ProcessForm" property="process.historialList"
			export="true" defaultsort="1" id="historialList">
			<display:setProperty name="report.title" value="Historial de Titularidad"></display:setProperty>
			<display:column title="Titular" property="titularAnterior.nomb12" sortable="true" />
			<display:column title="Fecha Hasta" property="fechaHastaStr" sortable="false" />
			<display:column title="Resolución" property="numeroResolucionCambio" sortable="false" />
			<display:column title="Responsable" property="responsable" sortable="false" />
			<display:column title="Observaciones" property="observaciones" sortable="true" />
		</display:table>
	</div>
</html:form>