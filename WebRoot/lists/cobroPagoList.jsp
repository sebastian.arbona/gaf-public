<%@ page language="java"%>
<%@ page import="com.asf.util.DateHelper" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<script type="text/javascript">
var clickCount = 0;  
function cierre() {
    if(clickCount == 0) {  
        clickCount++;  
        progreso();
        return true;  
    } else {  
    	var e;
    	if (window.event) e = window.event;
        else return false;
        if (e.cancelBubble != null) e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
        if (e.preventDefault) e.preventDefault();
        if (window.event) e.returnValue = false;
        if (e.cancel != null) e.cancel = true;
        return false;  
    }  
}

function Nuevo(val){
    if( val != '' ){
         getElem("cd").action=getElem("cd").action.toString().replace(getElem("cd").action,"actions/abmActionCaratula.do?do=newEntity&entityName=Caratula&cobroPagos&consultaBoletos&paramName[0]=fechaEnvioStr&paramComp[0]==&paramValue[0]="+val);
         getElem("cd").submit();
     }
}

function Consultar(){
 	getElem("cd").action = getElem("cd").action.toString().replace("CierreCaja", "CobroPagos");
}

function progreso(){
	window.open("${pageContext.request.contextPath}/progress.jsp", "Proceso",
	"width=450, height=180");
}
</script>


<div class="title">Administración de Procesamiento de Recaudación</div>
<br><br>

<html:form styleId="cd" action="/actions/process.do?do=process&processName=${param.processName}&filter=true">
<html:errors/>
<table border="0">

<tr><th>Fecha:</th>
<td>
<logic:empty name="ProcessForm" property="process.fenv12Str" >
	<asf:calendar property="ProcessForm.process.fenv12Str" value="${ProcessForm.process.fenv12Str}"/>
</logic:empty>
<logic:notEmpty name="ProcessForm" property="process.fenv12Str" >
	<asf:calendar property="process.fenv12Str" value="${ProcessForm.process.fenv12Str}"/>
</logic:notEmpty>
</td></tr>

</table>
	<input type="button" onclick="Nuevo(getElem('process.fenv12Str').value);" value="Nuevo" />
    <html:submit onclick="Consultar();" value="Consultar"/>
   
</html:form>
<br>
<div class="grilla">
<display:table name="ProcessForm" property="process.result" export="true" id="reportTable" defaultsort="1" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&filter=true">
    <display:setProperty name="report.title" value="Administración de Cobro con Valores"/>
    <display:caption>
     Fecha: ${reportTable.fechaEnvioStr}
    </display:caption>
 
 	<display:column title="Nro de Caja" property="grupo" sortable="true"/>
    <display:column title="Ente Recaudador" property="banco.detaBa" sortable="true" />
    <display:column title="Fecha Cobranza" property="fechaCobranzaStr" sortable="true" />
    <display:column title="Cantidad Lotes" property="comprobantes" sortable="true" />
    <display:column title="Importe Total" property="importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
   	<display:column title="Usuario" property="usuario" sortable="true"/>
   	<display:column title="Método de Carga" property="metodoCargaStr" sortable="true"/>
  	
    <asf:security action="/actions/process.do" access="do=process&processName=CierreCaja">
    <display:column media="html" title="Cierre de Caja" >
    	<logic:equal name="reportTable" property="actualizada" value="0">
       		<logic:equal value="${reportTable.usuario}" name="ProcessForm" property="process.usuarioSesion">
      	 		<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CierreCaja&process.idcaratula=${reportTable.id}&process.fenv12Str=${reportTable.fechaEnvioStr}&filter=true&process.cobro=true" onclick="cierre();">Cierre de Caja</a>
      	 	</logic:equal>
       	</logic:equal>
	   	<logic:notEqual name="reportTable" property="actualizada" value="0">
	  	 	${reportTable.fechaActualizacionStr}
	   	</logic:notEqual>
	</display:column>
    </asf:security> 
<%--    <asf:security action="/actions/process.do" access="do=process&processName=RegistrarPago">--%>
    <display:column media="html" title="Procesar Recaudación" >
        <logic:equal name="reportTable" property="actualizada" value="0">
       		<logic:equal value="${reportTable.usuario}" name="ProcessForm" property="process.usuarioSesion">        	
      	 		<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=RegistrarCobroPagos&process.idcaratula=${reportTable.id}&process.fenv12Str=${reportTable.fechaEnvioStr}&process.metodoCarga=${reportTable.metodoCarga}&filter=true">Procesar Recaudación</a>
			</logic:equal>			      	 		
       </logic:equal>
	</display:column>
<%--    </asf:security> --%>
<%--    <asf:security action="/actions/process.do" access="do=process&processName=ConsultarLote">--%>
    <display:column media="html" title="Consultar Lotes" >
        <logic:equal name="reportTable" property="actualizada" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConsultarLotes&process.idCaratula=${reportTable.id}&process.metodoCarga=${reportTable.metodoCarga}">Consultar Lotes</a>
       </logic:equal>
       <logic:notEqual name="reportTable" property="actualizada" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConsultarLotes&process.idCaratula=${reportTable.id}&process.metodoCarga=${reportTable.metodoCarga}&consultaLotes">Consultar Lotes</a>
       </logic:notEqual>
	</display:column>
<%--    </asf:security> --%>

    <asf:security action="/actions/process.do" access="do=process&processName=ConsultarBoleto">
    <display:column media="html" title="Consultar Boletos" >
        <logic:equal name="reportTable" property="actu12" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=load&entityName=Caratula&entity.id=${reportTable.id}&consultaBoletos">Consultar Boletos</a>
       </logic:equal>
       <logic:notEqual name="reportTable" property="actu12" value="0">
      	 <a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=load&entityName=Caratula&entity.id=${reportTable.id}">Consultar Boletos</a>
       </logic:notEqual>
	</display:column>
    </asf:security>
  
<%--    <asf:security action="/actions/process.do" access="do=delete&entityName=${param.entityName}">--%>
      <display:column media="html" title="Eliminar" >
    	<logic:equal name="reportTable" property="actualizada" value="0">
    		<logic:equal value="${reportTable.usuario}" name="ProcessForm" property="process.usuarioSesion">    		
    			<a href="${pageContext.request.contextPath}/actions/abmActionCaratula.do?do=delete&entityName=Caratula&entity.id=${reportTable.id}&entity.fechaEnvioStr=${reportTable.fechaEnvioStr}&filter=true" onclick="return confirmDelete();" >Eliminar</a>
    		</logic:equal>	
		</logic:equal>
	 </display:column>
<%--	</asf:security>--%>

</display:table>
</div>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
