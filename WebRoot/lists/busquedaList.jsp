<%@ page language="java"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<div class="title">Administración de Documentos</div>
<br><br>

<html:form action="/actions/ArchivoAction.do?entityName=${param.entityName}&filter=true">
    <html:hidden property="paramName[0]" styleId="paramName" value="idpersona" />
    <html:hidden property="paramComp[0]" styleId="paramComp" value="="/>
    <html:hidden property="paramValue[0]" styleId="paramValue" value="${paramValue[0]}"/>
    <html:hidden property="filter" value="true"/>
    <html:hidden property="do" styleId="do" value="list"/>
    <input type="button" value="Volver" onclick="volver();" >
    <asf:security action="/actions/ArchivoAction.do" access="do=newEntity&entityName=${param.entityName}">
        <input type="submit" onclick="getElem('do').value='newEntity';" value="<bean:message key="abm.button.new"/>" />
    </asf:security>
</html:form>

<br>

<display:table name="ListResult" property="result" defaultsort="1" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=list&entityName=${param.entityName}">
    <display:setProperty name="report.title" value="Administración de Documentos"></display:setProperty>

    <display:column media="html" sortProperty="id" property="id" title="ID Archivo"
                    href="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=load&entityName=${param.entityName}&load"
                    paramId="entity.id" paramProperty="id" />
    <display:column property="nombre" title="Nombre del Archivo" />
    <display:column property="tipoArchivo.descripcion" title="Tipo Documento" />
    <display:column title="imagen">
        <logic:equal value="1"  name="reportTable" property="tipoArchivo.imagen">
            <img height="50px" src="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=ver&id=${reportTable.id}" />
        </logic:equal>
        <logic:notEqual  value="1"  name="reportTable"   property="tipoArchivo.imagen">
            <img height="50px"  src="${pageContext.request.contextPath}/images/${reportTable.tipoArchivo.ruta}" />
        </logic:notEqual>
    </display:column>
    <display:column title="Descargar" ><html:button property="" value="Bajar Documento"  onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoAction.do?do=descargar&id=${reportTable.id}')"/></display:column>
    <asf:security action="/actions/ArchivoAction.do" access="do=delete&entityName=${param.entityName}">
        <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=delete&entityName=${param.entityName}&filter=true" paramId="entity.id" paramProperty="id">Eliminar</display:column>
    </asf:security>
</display:table>

<script language='javascript'>
    function volver() {
        var url = '';
        url += '${pageContext.request.contextPath}/actions/PersonaAction.do?';
        url += 'do=list';
        url += '&entityName=Persona';
        url += '&filter=true';
        url += '&paramName[0]=idpersona';
        url += '&paramComp[0]==';
        url += '&paramValue[0]=${paramValue[0]}';
        window.location = url;
    }
</script>