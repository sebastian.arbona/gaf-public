<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.accion" value="asignar" />
    <html:hidden property="process.personaTitular.id" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.id}"/>
    <div style="width:70%" align="left"><html:errors/></div>
    <asf:security action="/actions/process.do?do=process&processName=Domicilio" access="process.accion=guardar">
		<html:submit><bean:message key="abm.button.new"/></html:submit>
	</asf:security>
	<br><br>
    <logic:notEmpty name="ProcessForm" property="process.listaDomicilios" >
        <display:table name="ProcessForm" property="process.listaDomicilios" pagesize="5" id="reportTable" export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar" >
            <display:caption>Domicilios de: ${ProcessForm.process.personaTitular.idpersona} - ${ProcessForm.process.personaTitular.nomb12}</display:caption>
            <display:column media="excel xml pdf" property="id" title="ID"/>
            <display:column title="ID" sortProperty="id" sortable="true" media="html" >
                <a href="javascript: load( ${reportTable.id} );" >${reportTable.id}</a>
            </display:column>
            <display:column property="tipo" title="Tipo" sortable="true" />
            <display:column property="provinciaStr" title="Prov." sortable="true" />
            <display:column property="localidadStr" title="Loc." sortable="true" />
            <display:column property="departamentoStr" title="Departamento" sortable="true" />
            <display:column property="barrioStr" title="Barrio" sortable="true" />
            <display:column property="calleStr" title="Calle" sortable="true" />
            <display:column property="numero" title="Numero" sortable="true" />
            <display:column property="manzana" title="Manz./Piso" sortable="true" />
            <display:column property="lote" title="Lote/Depto." sortable="true" />
            <display:column title="Eliminar" media="html">
                <a href="javascript: eliminar( ${reportTable.id} );" >Eliminar</a>
            </display:column>
        </display:table>
    </logic:notEmpty>
    <logic:empty name="ProcessForm" property="process.listaDomicilios" >
        La Persona: ${ProcessForm.process.personaTitular.idpersona} - ${ProcessForm.process.personaTitular.nomb12} no tiene domicilios vinculados
    </logic:empty>
</html:form>
<script type="text/javascript">
    function eliminar(id) 
    {
    	if(confirmDelete())
    	{
			var url = '';
	        url += '${pageContext.request.contextPath}/actions/process.do?';
	        url += 'do=process&';
	        url += 'processName=${param.processName}&';
	        url += 'process.accion=eliminar&';
	        url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
	        url += 'process.domicilio.id=' + id;
	        window.location = url;
    	}
    }//fin eliminar.-

    function load(id) 
    {
        var url = '';
        url += '${pageContext.request.contextPath}/actions/process.do?';
        url += 'do=process&';
        url += 'processName=${param.processName}&';
        url += 'process.accion=load&';
        url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
        url += 'process.domicilio.id=' + id;
        window.location = url;
    }//fin load.-

    Event.observe(window, 'load', function() 
	{
    	this.name = "Domicilio";
    	parent.pop(this);
    });
</script>