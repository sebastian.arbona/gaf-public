<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.asf.util.BeanHelper"%>
<%@ page import="java.util.List"%>
<%@page import="com.asf.util.StringHelper"%>
<%@page import="com.asf.cred.Constants"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>

<script language="JavaScript">
    var navegador=navigator.appName;
        
            function carga(){
                if(document.getElementById('search').value=='')
                    document.getElementById('search').value='%';
            }
            
            function actualizar(control, valor){
                if (window.opener){
                    if(navegador=='Netscape'){
                        var e = window.opener.document.getElementById( control );
                        if(!e)return;
                        e.value = valor;
                        try{
	                        if(e.onchange){
	                            e.onchange();
	                        }
                        }catch(e){}
                    }else{
                        var e = window.opener.document.all[control];
                        if(!e)return;
                        e.value=valor;
                        if(e.onchange){
                            e.onchange();
                        }
                    }
                    window.close();
                }
            }
            function setFocus(){
            	document.PopUpForm.search.focus();
            } 
        
    </script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/displaytag.css"
	type="text/css">

<TITLE>${param.title}</TITLE>
</head>
<body onload="setFocus();">
	<bean:define id="paramColumns" value="${param.columns}" />
	<bean:define id="paramColumnsEnc"
		value="<%=URLEncoder.encode(URLDecoder.decode(paramColumns,\"ISO-8859-1\"),\"ISO-8859-1\")%>" />

	<html:form
		action="/actions/selectpopup.do?do=list&cantidadDescriptors=${param.cantidadDescriptors}&caseSensitive=${empty param.caseSensitive ? 'false' : param.caseSensitive}">
		<html:hidden property="name" />
		<html:hidden property="entityName" />
		<html:hidden property="captions" />
		<html:hidden property="columns" />
		<html:hidden property="title" />
		<html:hidden property="decorador" />
		<html:hidden property="atributosDecoradores" />
		<html:hidden property="strs" />
		<%
			String filtro = "";
				if (request.getParameter("filtro") != null) {
					filtro = request.getParameter("filtro").toString();
					request.setAttribute("filter", filtro);
				}
		%>
		<html:hidden property="filter" />
		<html:hidden property="filter2" />

		<center>
			<table>
				<tr>
					<TH>Buscar:</TH>
					<td><html:text property="search" styleId="search" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center">(${param.columns})</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><html:submit onclick="carga();">Consultar</html:submit>&nbsp;
						<BUTTON onclick="javascript:window.close();">Cancelar</BUTTON> <%
 	String altaenlinea = request.getParameter("title"); // sirve solo para saber que vengo del selectpop de persona de la pagina proveedor.jsp
 		String entity = request.getParameter("entityName");
 		if (entity.substring(entity.lastIndexOf(".") + 1, entity.length()).equalsIgnoreCase("persona")
 				&& altaenlinea != null && altaenlinea.equals("altaenlinea")) {
 %> <asf:security action="/actions/lineaContr.do?do=newEntity">
							<input type="button"
								onclick="window.location='${pageContext.request.contextPath}/actions/lineaContr.do?do=newEntity&name=${param.name}&descriptor=${param.descriptor}','altaEnLinea','width=900,height=700,scrollbars=yes,screenX=this.screenX,screenY=this.screenY';"
								value="Nuevo">
						</asf:security> <%
 	}
 %></td>
				</tr>
			</table>
		</center>
	</html:form>
	<br>
	<br>

	<bean:define name="popUpList" property="captions" type="String"
		id="captions" />
	<bean:define name="popUpList" property="columns" type="String"
		id="columns" />
	<bean:define name="popUpList" property="strs" type="String" id="strs" />
	<%
		String valor = "";
		Long cant;
		try {
			cant = new Long(request.getAttribute("cantidadDescriptors").toString());
		} catch (Exception e) {
			cant = new Long(1);
		}

		String[] arrCaptions = captions.split(",");
		String[] arrStrs = strs.split(",");

		pageContext.setAttribute("arrStrs", arrStrs);

		pageContext.setAttribute("firstColumn", arrCaptions[0]);
		//pageContext.setAttribute("arrColumns", columns.split(","));
		pageContext.setAttribute("descriptor",
				((String) request.getParameter("name")).replace("[", "_").replace("]", "_").replace(".", "_")
						+ "Descriptor");
		try {
			pageContext.setAttribute("arrCaptions", captions.replace(arrCaptions[0] + ",", "").split(","));
		} catch (Exception e) {
		}

		try {
			String[] columnas = columns.replace(arrCaptions[0] + ", ", " ").split(",");
			for (int i = 0; i < columnas.length; i++)
				columnas[i] = java.net.URLDecoder.decode(columnas[i]);
			pageContext.setAttribute("arrColumns", columnas);
		} catch (Exception e) {
		}
	%>
	<display:table align="center" property="list" pagesize="100"
		name="popUpList" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/selectpopup.do?do=list&name=${param.name}&entityName=${param.entityName}&title=${param.title}&captions=${param.captions}&columns=${paramColumnsEnc}">
		<display:column sortProperty="${firstColumn}" title="${arrColumns[0]}"
			sortable="true">
			<a href="javascript://"
				onclick="actualizar('${param.name}','<bean:write name="reportTable" property="${firstColumn}"/>');actualizar('${descriptor}','<%
                		for(int i=0;i<cant.longValue();i++){        
                			pageContext.setAttribute("iter", new Integer(i));  
                			%><bean:define name="reportTable" property="${arrCaptions[iter]}" id="myVar" type="String"/><%                			
                			out.write(StringHelper.markup(StringHelper.StripHMTL(myVar)));	
                		}
                		%>');"><bean:write
					name="reportTable" property="${firstColumn}" /></a>

		</display:column>

		<%
			boolean bOk = false;
				String[] arrColumns = (String[]) pageContext.getAttribute("arrColumns");
				for (int j = 1; j < arrCaptions.length; j++) {
					for (int k = 0; k < arrStrs.length; k++) {
						if (arrStrs[k].equals(arrCaptions[j])) {
							bOk = true;
							break;
						}
					}
					try {
		%><display:column
			property="<%=bOk ? arrCaptions[j] +\"Str\" : arrCaptions[j]%>"
			title="<%=arrColumns[j]%>" sortable="true" />
		<%
			} catch (Exception e) {
					} finally {
						bOk = false;
					}
				}
		%>
		<%--c:forEach  var="caption" items="${arrCaptions}">
                    <% pageContext.setAttribute("i", new Integer(i++));%>
                    <display:column property="${caption}" title="${arrColumns[i]}" sortable="true"/>
                    <% for( int i=0; i < arrStrs.length; i++ )
                            if( arrStrs[ i ].equals( arr ) )
                    %>                   
                     <display:column property="${caption}${( caption == strs ? 'Str' : '' )}" title="${arrColumns[i]}" sortable="true"/>                    
                    
            </c:forEach--%>
	</display:table>
	<logic:notEmpty name="popUpList" property="list">
		<bean:define name="popUpList" property="list" type="java.util.List"
			id="list" />
		<%=(list.size() >= Constants.MAX_SELECT_POPUP_RECORDS.intValue())
						? "<br>El resultado de la consulta es superior al n�mero de registros que se puede mostrar. Por favor, refine la b�squeda e intente nuevamente."
						: ""%>
	</logic:notEmpty>

</body>
</html>