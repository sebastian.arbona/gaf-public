<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/prototype.js"></script>

<div class="title">Administración de Reportes</div>
<br><br>

<html:form action="/actions/process.do?do=process&processName=${param.processName}">
    <html:hidden property="process.action" styleId="action"/>
    <table>
        <tr><th>Reporte:</th><td><asf:select name="process.idreporte" listCaption="titulo" listValues="idreporte" value="${ProcessForm.process.idreporte}" entityName="com.asf.hibernate.mapping.Reporte" orderBy="titulo" nullValue="true" nullText="--- Nuevo Reporte ---"/></td></tr>
    </table>
    <html:submit value="Nuevo" onclick="return nuevo();"/>
    <html:submit value="Consultar" onclick="return listar();"/>
</html:form>

<br><br>

<div class="grilla">
<style>th {text-align:center;}</style>
<logic:notEmpty name="ProcessForm" property="process.listaReporte">
    <display:table name="ProcessForm" property="process.listaReporte" defaultsort="1" export="true" id="reportes" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=list&process.idreporte=${ProcessForm.process.idreporte}">

		<display:column title="Título" media="html">
            <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=PColumna&process.action=load&process.idreporte=${reportes.idreporte}&process.reporte.tipo=${reportes.tipo}">${reportes.titulo}</a>
        </display:column>
        <display:column property="titulo" title="Título" media="xml excel pdf"/>
        
        <logic:equal name="reportes" property="tipo" value="0">
            <display:column title="Tipo">${'Reporte HTML'}</display:column>
        </logic:equal>
         <logic:equal name="reportes" property="tipo" value="1">
            <display:column title="Tipo">${'Reporte Jasper'}</display:column>
        </logic:equal>
        <logic:equal name="reportes" property="tipo" value="2">
            <display:column title="Tipo">${'Reporte con retoma Jasper'}</display:column>
        </logic:equal>
        <logic:equal name="reportes" property="tipo" value="3">
            <display:column title="Tipo">${'Action Report'}</display:column>
        </logic:equal>

        <logic:equal name="reportes" property="tipo" value="0">
            <display:column property="subtitulo" title="Subtitulo"/> 
        </logic:equal>
        <logic:equal name="reportes" property="tipo" value="1">
            <display:column property="subtitulo" title="Archivo Jasper"/> 
        </logic:equal>
        <logic:equal name="reportes" property="tipo" value="2">
            <display:column property="subtitulo" title="Retoma Jasper"/> 
        </logic:equal>
        <logic:equal name="reportes" property="tipo" value="3">
            <display:column property="subtitulo" title="Action Report"/> 
        </logic:equal>

        <display:column property="sql" title="Consulta SQL"/> 
        <display:column title="Eliminar" media="html" honClick="return confirmDelete();" href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=PColumna&process.action=delete&process.idreporte=${reportes.idreporte}">Eliminar</display:column>

    </display:table>
</logic:notEmpty>
<logic:empty name="ProcessForm" property="process.listaReporte">
    No hay datos para mostrar
</logic:empty>
</div>

<script language="javascript">
    function nuevo(){
        $("process.idreporte").selectedIndex=0;
        $("action").value="new";
        return true;
    }
    
    function listar(){
        $("action").value="list";
        return true;
    }
</script>