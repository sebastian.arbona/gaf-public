<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="nuevo" />
	<html:hidden property="process.objetoi.id" styleId="objetoi.id"
		value="${ProcessForm.process.objetoi.id}" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<asf:security action="/actions/process.do"
		access="do=process&processName=${param.processName}">
		<logic:equal name="ProcessForm" property="process.ocultarCampos" value= "true">
			<html:submit value="Modificar"/>
		</logic:equal>
		
		<logic:notEqual name="ProcessForm" property="process.ocultarCampos" value= "true">
   			<html:submit value="Modificar"/>
		</logic:notEqual>
		
	</asf:security>
	<br/>
	<br/>
	<logic:equal name="ProcessForm" value="DESISTIDO" property="process.estadoActual">
		<a href="${pageContext.request.contextPath}/actions/process.do?do=process
				&processName=${param.processName}
				&process.objetoi.id=${ProcessForm.process.objetoi.id}
				&process.accion=imprimirInforme">
			IMPRIMIR DESESTIMIENTO
		</a>
	</logic:equal>
	<br>
	<br>
	<logic:notEmpty name="ProcessForm" property="process.estadosObservaciones">
		<display:table name="ProcessForm" property="process.estadosObservaciones"
			pagesize="10" id="reportTable" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
			<display:caption>Proyecto ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12}</display:caption>
			<display:column title="Estado" property="estaobs" sortable="true" />
			<display:column title="Color" media="html">
				<div style="background:${reportTable.color};">
					&nbsp;
				</div>
			</display:column>
			<display:column title="Desde" property="fechaStr" sortable="true" />
			<display:column title="Hasta" property="fechaHastaStr"
				sortable="true" />
			<display:column title="Observaciones" property="observaciones"
				sortable="true" />
			<display:column title="Area Responsable" property="areaResponsable"/>
		</display:table>
	</logic:notEmpty>
	<logic:empty name="ProcessForm" property="process.objetoiEstados">
        El Cr�dito : ${ProcessForm.process.objetoi.id} - ${ProcessForm.process.objetoi.numeroCredito} no tiene Estado.
    </logic:empty>
</html:form>

<script language='javascript'>
Event.observe(window, 'load', function() {
	this.name = "Estados";
	parent.pop(this);
});
</script>