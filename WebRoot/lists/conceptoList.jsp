<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<script language="javascript" type="text/javascript">
function filtrar() {
	getElem("AbmForm").submit();
}
function back() {
	window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=CConcepto";
}

function newEntity() {
	window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}";
}
</script>
<div class="title">
  Administración de Conceptos Por Ejercicio 
</div>
<br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
  <button type="button" onclick="javascript:newEntity();">
    <bean:message key="abm.button.new" />
  </button>
</asf:security>
<asf:security action="/actions/abmAction.do" access="do?do=list&entityName=Especialidad">
  <button type="button" onclick="javascript:back();">
    <bean:message key="abm.button.back" />
  </button>
</asf:security>
<br>
<br>
<div class="grilla">
  <display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1"
    requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}"
  >
    <display:setProperty name="report.title" value="Administración de Conceptos"></display:setProperty>
    <display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
      href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}"
    />
    <display:column property="periodo" title="Período" sortable="true" />
    <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
      <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
        href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&filter=true"
        paramId="entity.id" paramProperty="id"
      >
        <bean:message key="abm.button.delete" />
      </display:column>
    </asf:security>
    <asf:security action="/actions/abmAction.do" access="do=list&entityName=ConceptoContable">
    	<display:column media="html" title="Modelos Asiento por Tipo"
    		href="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=ConceptoContable&filter=true&paramName[0]=concepto.id&paramComp[0]=%3D"
    		paramId="paramValue[0]" paramProperty="id">
    		Ver
    	</display:column>
    </asf:security>
  </display:table>
</div>