
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>


<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />


<body class=" yui-skin-sam">
	<div class="title">
		Administración de Resoluciones
	</div>
	<br>
	<br>
		
		
<div><html:errors /></div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
    </script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
</script>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	
		
		<html:hidden property="process.accion" styleId="accion" value="listFiltro" />
		<html:hidden name="ProcessForm" property="process.procesoResolucion.id"/>
		<table>
		<tr>
			   <th>Origen</th>
			   <td>
			   		<asf:select name="process.opcion"
			   		value="${ProcessForm.process.opcion}"
					listCaption="Demandados,Demandantes"
					listValues="1,2" attribs="onchange='seleccion(this);'"/>
			   </td> 
			</tr>
			<tr id="persona" style="display: none;">
				<th>Persona:</th>
				<td>
            		<asf:text name="ProcessForm" property="process.persona"  type="text" maxlength="30" value="${ProcessForm.process.persona}"/>
            		(Apellido o Nombre)
				</td>
			</tr>
			
		
			<tr id="proyecto">
				<th>Nro de Solicitud/Proyecto:</th>
				<td><asf:text name="ProcessForm" property="process.numeroAtencion" type="Long" maxlength="6" value="${ProcessForm.process.numeroAtencion}" /></td>
			</tr>
			<tr>
				<th>Nro de Resolución:</th>
				<td><asf:text name="ProcessForm" property="process.numeroResolucion"  type="text" maxlength="11" value="${ProcessForm.process.numeroResolucion}" /></td>
			</tr>
			<tr>
				<th>Abogado:</th>
				<td>
					<asf:select name="process.idAbogado" entityName="EspecialidadPersona"
                             listCaption="persona.nomb12" listValues="persona.idpersona"
                             nullValue="true" nullText="Seleccione abogado..."
                             value="${ProcessForm.process.idAbogado}"
                             filter=" especialidad_id = 6"
                              /> 
				</td>
			</tr>
			
		
		</table>		
		<br />
		<br />
		<html:submit>Consultar</html:submit>
	</html:form>
	<br>
	<br>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	
		<html:hidden property="process.accion" styleId="accion" value="nuevo" />
		<html:hidden property="process.sid"/>
		<html:hidden property="process.idproyectoPop"/>	
		<asf:security action="/actions/process.do?do=process&processName=${param.processName}" access="accion=nuevo">
			<html:submit onclick="getElem('accion').value='nuevo';">
				<bean:message key="abm.button.new" />
			</html:submit>
		</asf:security>
	</html:form>
	<br>
	<br>
	<display:table name="ProcessForm" property="process.resoluciones" export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
		<display:setProperty name="report.title"
			value="Administración de Resoluciones"></display:setProperty>
		<display:column title="Nro Resolucion" media="html" sortable="true">
			<a href="javascript: show( ${ reportTable.id}  );">${reportTable.numero}</a>
		</display:column>
		
		<logic:equal name="ProcessForm" property="process.opcion" value="1">
			<display:column title="Proyecto" media="html" sortable="true">
					<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.proyecto.id}&process.action=buscar&process.personaId=${reportTable.proyecto.persona.idpersona}">${reportTable.proyecto.numeroAtencion}</a>
			</display:column>
			<display:column property="proyecto.persona.nomb12" title="Titular" sortable="true" />
		</logic:equal>
		<logic:equal name="ProcessForm" property="process.opcion" value="2">
			<display:column property="persona.id" title="Demandate Id" sortable="true" />
			<display:column property="persona.nomb12" title="Demandate Nombre" sortable="true" />
		
		</logic:equal>
		
		<display:column property="especialista.persona.nomb12" title="Abogado" sortable="true" />	
		<display:column property="tipoResolucion.nombre" title="Proceso" />
		<display:column media="excel xml pdf" property="numero"
			title="Nro Resolucion" />
		<display:column property="fechaStr" title="Fecha" />
		<display:column property="observaciones" title="Observación" />
		<display:column title="Instancias" media="html">
				<a href="javascript: instancias( ${reportTable.id} );">Administrar</a>
		</display:column>
		<display:column title="Archivos" media="html">
				<a href="javascript: documentoResolucion( ${reportTable.id} );">Administrar</a>
		</display:column>
	</display:table>

<script type="text/javascript">
$j(document).ready(function(){
	var selector = document.getElementById('process.opcion');
	seleccion(selector);	
});
function load(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.idSolicitud=' + id;

	window.location = url;
}// fin modificar.-

function show(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=show&';
	url += 'process.resolucion.id=' + id;

	window.location = url;
}
	
function instancias(id){
		var url = '';
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=listarInstancias&';
		url += 'process.idSolicitud=' + id + '&';
		url += 'process.opcion=${ProcessForm.process.opcion}&';
		url += 'process.persona=${ProcessForm.process.persona}&';
		url += 'process.idAbogado=${ProcessForm.process.idAbogado}&';
		url += 'process.numeroAtencion=${ProcessForm.process.numeroAtencion}&';
		url += 'process.numeroResolucion=${ProcessForm.process.numeroResolucion}';

		window.location = url;
}


function documentoResolucion(id){
		var url = '';
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=listarDocumentosResolucion&';
		url += 'process.idSolicitud=' + id + '&';
		url += 'process.opcion=${ProcessForm.process.opcion}&';
		url += 'process.persona=${ProcessForm.process.persona}&';
		url += 'process.idAbogado=${ProcessForm.process.idAbogado}&';
		url += 'process.numeroAtencion=${ProcessForm.process.numeroAtencion}&';
		url += 'process.numeroResolucion=${ProcessForm.process.numeroResolucion}';
		
		window.location = url;
}

function seleccion(mySelect) {
	var currentValue = mySelect.value;
	if(currentValue==1){
		document.getElementById('proyecto').style.display = '';
		document.getElementById('persona').style.display = 'none';
    } else{
	    document.getElementById('proyecto').style.display = 'none';
	    document.getElementById('persona').style.display = '';
	}
	
}

function actualizarLista( inputIn, defecto )
{
	var urlDinamica = "";

	urlDinamica += "&persona=" + inputIn.value;
	urlDinamica += "&" + defecto;

	return urlDinamica;
}

</script>