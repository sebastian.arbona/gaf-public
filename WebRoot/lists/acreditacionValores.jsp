<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<div class="title">
	Acreditación de Valores
</div>
<br>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	<logic:notEmpty name="ProcessForm" property="process.mensaje">
	<div style="width: 70%" align="left">
		${ProcessForm.process.mensaje}
	</div>
	</logic:notEmpty>
	<table>
		<tr> 
		    <th>Banco</th>
	    	<td><asf:select entityName="com.asf.gaf.hibernate.Bancos" listCaption="codiBa,detaBa"
	            listValues="codiBa" name="process.banco.codiBa" value="${ProcessForm.process.banco.codiBa}" 
	            filter="codiBa IN(8, 10)"  orderBy="codiBa DESC"/>
            </td>
        </tr>
        <tr>    
	    	<th>Fecha Desde</th>
	    	<td><asf:calendar property="ProcessForm.process.fechaDesdeStr"/></td>
	    </tr>
	    <tr>		    		   	
	    	<th>Titular</th>
			<td><asf:selectpop name="process.titular.id" title="Titular"
				columns="Código, Apellido y Nombres, Nro. Documento, C.U.I.L"
				captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
				entityName="com.civitas.hibernate.persona.Persona" value="${ProcessForm.process.titular.id}" />
			</td>
		</tr>
		<tr>	
	    	<th>Numero Valor</th>
	    	<td><asf:text name="ProcessForm" maxlength="20" property="process.numeroValor" type="text"/></td>
	    </tr>	
    </table>    
    <br />
	<input value="listar" type="button" onclick="listar();">
	<br />		
	<logic:empty name="ProcessForm" property="process.detalles">
    	No hay valores presentados
    </logic:empty>
	<br />
	<logic:notEmpty name="ProcessForm" property="process.detalles">
		<display:table name="ProcessForm" property="process.detalles"
			id="detalle" >
			<display:column title="Selección">
				<input type="checkbox" name="requisito-${detalle.id}" value="${detalle.id}"/>
			</display:column>
			<display:column title="Numero" property="numeroValor"
				sortable="true" />
			<display:column title="Nro Operación" property="numeroOperacion"
				sortable="true" />
			<display:column title="Banco" property="banco.detaBa"
				sortable="true" />
			<display:column title="Fecha" property="fechaRecaudacion"
				sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="Proyecto" property="numeroProyecto"
				sortable="true" />
			<display:column title="Titular" property="objetoi.persona.nomb12"
				sortable="true" />	
			<display:column title="Moneda" property="moneda.denominacion"
				sortable="true" />
			<display:column title="Importe" property="importe" align="right"
				sortable="true" decorator="com.asf.displayDecorators.DoubleDecorator"/>				
		</display:table>
		<br />
		<table>
			<tr>
				<th>Fecha Acreditación</th>
	    		<td><asf:calendar  property="ProcessForm.process.fechaAcreditacionStr"/></td>
	    	</tr>		
		</table>
		<br />
		<input type="button" value="Acreditar valores" onclick="acreditar();" />
	</logic:notEmpty>

</html:form>
<script type="text/javascript">
var accion = $('accion');
var formulario = $('oForm');

function listar() {
	accion.value = 'listar';
	formulario.submit();
}
function acreditar() {
	accion.value = 'acreditar';
	formulario.submit();
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" 
src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" 
style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>