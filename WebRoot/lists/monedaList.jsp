<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<!-- diegobq -->
<div class="title">Administración de Monedas</div>
<br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';">
	<bean:message key="abm.button.new"/>
</button>
</asf:security>

<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable"
	requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}" defaultsort="1">

    <display:setProperty name="report.title" value="Administración de Monedas" />

    <display:column property="id" title="ID" media="html"
    	href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" paramId="entity.id"
    	paramProperty="id" sortable="true" />
    <display:column property="id" title="ID" media="pdf excel xml" />
    <display:column property="denominacion" title="Denominación" sortable="true" />
    <display:column property="abreviatura" title="Abreviatura" sortable="true" />
    <display:column property="simbolo" title="Símbolo" sortable="true" />
	<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    
    <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
    	href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id"
    	paramProperty="id" ><bean:message key="abm.button.delete" />
    </display:column>
    <display:column media="html" 
		title="Cotizacion" href="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=Cotizacion&paramName[0]=moneda.id&paramComp[0]=%3d&filter=true"
		paramId="paramValue[0]" paramProperty="id" >ver
	</display:column>
    
</asf:security>
</display:table>
</div>