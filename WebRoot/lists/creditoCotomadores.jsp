<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.action" styleId="accion"/>
    <html:hidden property="process.idObjetoi"/>
    <html:hidden property="process.personaTitular.id" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.id}" />
    <html:hidden property="process.personaVinculada.personaVinculada_id" styleId="idpersonaVinculada"/>
    <div style="width:70%" align="left"><html:errors/></div>
    <logic:equal value="false" name="ProcessForm" property="process.ocultar">
    <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}">
  		<input type="button" onclick="Nuevo();" value="Nuevo" align="middle">
	</asf:security>
	</logic:equal>
	<br><br>
    <logic:notEmpty name="ProcessForm" property="process.personasVinculadas" >
        <display:table name="ProcessForm" property="process.personasVinculadas" id="reportTable" export="true"
        requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.action=listar">
            <display:caption>Personas Vinculadas al Titular: ${ProcessForm.process.personaTitular.idpersona} - ${ProcessForm.process.personaTitular.nomb12}</display:caption>
            
            <display:column property="id" title="ID"/>
            <display:column title="Persona Vinculada" property="personaVinculada.nomb12" sortable="true" />
            <display:column title="CUIL/CUIT" property="personaVinculada.cuil12" sortable="true" />
            <logic:equal value="false" name="ProcessForm" property="process.ocultar">
            <display:column title="Eliminar"	media="html" >
                <a href="javascript: eliminar( ${reportTable.id} );" >Eliminar</a>
            
            </display:column>
            </logic:equal>
        </display:table>
    </logic:notEmpty>
    <logic:empty name="ProcessForm" property="process.personasVinculadas" >
        La Persona Titular: ${ProcessForm.process.personaTitular.idpersona} - ${ProcessForm.process.personaTitular.nomb12} no tiene Co-Tomadores
    </logic:empty>
</html:form>
<script language='javascript' >
	function eliminar( id )
    {
		if(confirmDelete())
    	{
	        var url = '';
	        url += '${pageContext.request.contextPath}/actions/process.do?';
	        url += 'do=process&';
	        url += 'processName=${param.processName}&';
	        url += 'process.action=eliminar&';
	        url += 'process.idObjetoi=${ProcessForm.process.idObjetoi}&';
	        url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
	        url += 'process.personaVinculada.id=' + id;
	        window.location = url;
    	}
    }//fin eliminar.-

	function Nuevo(){
		var action = $('accion');
		var formulario =$('oForm');
		action.value='new';
		formulario.submit();
	}

</script>