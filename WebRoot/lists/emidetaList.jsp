<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Date"%>
<%@page import="com.asf.util.DateHelper"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<div class="title" style="margin-bottom: 30px">
	Detalle de Emisi&oacute;n
	<center>Fecha de Vencimiento: ${fecha}</center>
</div>

<html:form
	action="/actions/process.do?do=show&processName=Generacion&idEmision=${idEmision}">

	<html:submit disabled="${facturada}">Generar Cargos</html:submit>

	<html:errors />

	<div class="grilla">
		<display:table name="ListResult" property="result" export="true"
			id="reportTable" class="com.nirven.creditos.hibernate.Emideta"
			requestURI="${pageContext.request.contextPath}/actions/emidetaAction.do?do=list&entityName=${param.entityName}">

			<display:setProperty name="report.title" value="Detalle de Emisi&oacute;n"></display:setProperty>
			<display:column property="emideta.boleto.numeroBoleto"
				title="Nro. Resumen" />
			<display:column property="fechaVtoStr" title="Vto. Cuota" />
			<display:column property="emideta.credito.numeroAtencion"
				title="Proyecto" />
			<display:column property="emideta.numero" title="Nro. Cuota" />
			<display:column property="emideta.credito.linea.nombre" title="Linea" />
			<display:column property="auditoriaFinalPos"
				title="Aplicacion de Fondos" />

			<display:column property="emideta.credito.linea.moneda.simbolo"
				title="Moneda" />
			<display:column property="emideta.credito.persona.id"
				title="Nro. Tit." />
			<display:column title="Titular">
				<logic:notEmpty name="reportTable"
					property="emideta.credito.persona.nomb12">
				${reportTable.emideta.credito.persona.nomb12}
			</logic:notEmpty>
				<logic:notEmpty name="reportTable"
					property="emideta.credito.persona.razonSocial">
					<logic:empty name="reportTable"
						property="emideta.credito.persona.nomb12">
					${reportTable.emideta.credito.persona.razonSocial}
				</logic:empty>
				</logic:notEmpty>
			</display:column>
			<display:column title="Emisi&oacute;n/Proceso">
				<%=DateHelper.getString(new Date())%>
			</display:column>
			<display:column property="emideta.importeCuota" title="Importe Cuota"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column bgcolor="#939598">
			</display:column>
			<display:column property="emideta.capital" title="Capital"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column property="emideta.compensatorio"
				title="Interes Comp."
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column property="emideta.tasaCompensatorio"
				title="Tasa Comp."
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column title="Bonif."
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator">
				<logic:equal name="reportTable"
					property="emideta.bajaEstadoBonificacion" value="false">
				${reportTable.emideta.bonificacionStr}
		    </logic:equal>
				<logic:notEqual name="reportTable"
					property="emideta.bajaEstadoBonificacion" value="false">
		    		0,00
		    </logic:notEqual>
			</display:column>
			<display:column property="emideta.gastos" title="Gastos"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column property="emideta.gastosRecuperar"
				title="Gastos Recup."
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:footer>
				<td>TOTALES</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>${totalImporteCuota}</td>
				<td></td>
				<td>${totalCapital}</td>
				<td>${totalIntCompensatorio}</td>
				<td>${totalTasaCompensatorio}</td>
				<td>${totalBonificacion}</td>
				<td>${totalGastos}</td>
				<td>${totalGastosRecuperar}</td>
			</display:footer>
		</display:table>
	</div>

</html:form>