<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" value="nuevo" />
	<html:hidden property="process.objetoi.id" styleId="objetoi.id"
		value="${ProcessForm.process.objetoi.id}" />
	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	
	<br>
	<br>
	<logic:notEmpty name="ProcessForm" property="process.solicitudes">
		<display:table name="ProcessForm" property="process.solicitudes"
			pagesize="10" id="reportTable" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
			<display:caption>Pr�rrogas de solicitud Nro  ${ProcessForm.process.objetoi.numeroAtencion}</display:caption>
			<display:column media="excel xml pdf" property="id" title="Pr�rroga N�"/>
            <display:column title="Pr�rroga N�" sortProperty="id" sortable="true" media="html" >
				 <a href="javascript: list( ${reportTable.id} );" >${reportTable.id}</a>
			</display:column>
			<display:column title="Motivo" property="motivo" sortable="true" />
		</display:table>
	</logic:notEmpty>
	
</html:form>

<script language='javascript'>

function list(id) 
{
    var url = '';
    url += '${pageContext.request.contextPath}/actions/process.do?';
    url += 'do=process&';
    url += 'processName=ProrrogaEstados&';
    url += 'process.accion=list&';
    url += 'process.idSolicitudProrroga=' + id;
    window.location = url;
}//fin load.-

Event.observe(window, 'list', function() {
	this.name = "Prorroga";
	parent.pop(this);
});
</script>
