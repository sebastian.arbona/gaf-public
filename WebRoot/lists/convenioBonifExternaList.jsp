<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<div class="title">
	Convenios de Bonificaci&oacute;n Externa
</div>
<p>
	<asf:security action="/actions/abmAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		<button
			onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';">
			<bean:message key="abm.button.new" />
		</button>
	</asf:security>
	<br></br>
	<div class="grilla">
		<display:table name="ListResult" property="result" export="true"
			id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}"
			defaultsort="1">
			<display:setProperty name="report.title"
				value="Convenio de Bonificacion Externa"></display:setProperty>
			<display:column title="ID" sortProperty="id" sortable="true"
				media="html" paramId="entity.id" paramProperty="id">
					<a href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&entity.id=${reportTable.id}&load">
						${reportTable.id}</a>
			</display:column>
			<display:column media="excel xml pdf" property="id" title="ID" />
			<display:column property="nombre" title="Nombre" />
			<display:column property="linea.nombre" title="Linea" />
			<display:column property="destino" title="Destino" />
			<display:column property="fechaVencimiento" title="Fecha Vencimiento" decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column property="moneda.denominacion" title="Moneda"/>
			<display:column property="monedaPago.denominacion" title="Moneda de Pago"/>
			<asf:security action="/actions/abmAction.do"
				access="do=delete&entityName=${param.entityName}">
				<display:column media="html" honClick="return confirmDelete();"
					title="Eliminar"
					href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}"
					paramId="entity.id" paramProperty="id">
					<bean:message key="abm.button.delete" />
				</display:column>
			</asf:security>
		</display:table>
	</div>
</p>