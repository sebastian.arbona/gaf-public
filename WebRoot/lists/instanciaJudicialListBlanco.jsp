<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />


	<div class="title">
		Administraci�n de Instancias
	</div>
	<br>
	<br>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
		
<div><html:errors /></div>

	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="nuevaInstancia" />
		<html:hidden property="process.sid"/>
		<html:hidden property="process.idSolicitud" styleId="idSolicitud"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.id"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.resolucion.proyecto.id" styleId="idProyecto"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.resolucion.id"  styleId="idResolucion"/>
		<html:hidden property="process.idproyectoPop"/>	
			
		<asf:security action="/actions/process.do?do=process&processName=${param.processName}" access="accion=nuevaInstancia">
			<html:button property="" onclick="nuevo();" value="Cargar Instancia"/>
		</asf:security>
	</html:form>
	<br>
	
	 
	 <b>N�mero de resoluci�n:
		${ProcessForm.process.procesoResolucion.resolucion.numero} </b>

	<br>
	<b>Proyecto:
		${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion} </b>

	<br>
	<b>Titular:
		${ProcessForm.process.procesoResolucion.resolucion.proyecto.persona.nomb12Str} </b>

	<br>
	<br>
	
	<br>
	<br>
	
	<div class="grilla">
		<display:table name="ProcessForm" property="process.instancias" export="true" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listarInstancias">
			<display:setProperty name="report.title" value="Administraci�n de Movimientos - Resolucion:${ProcessForm.process.procesoResolucion.resolucion.numero} - Proyecto: ${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion} - Titular: ${ProcessForm.process.procesoResolucion.resolucion.proyecto.persona.nomb12Str}"></display:setProperty>			
			<display:column title="Nro Instancia" media="html" sortable="true">
				<a href="javascript: show( ${ reportTable.id}  );">${reportTable.id}</a>
			</display:column>
			<display:column property="resolucion.proyecto.numeroAtencion" title="N�mero Proyecto" />
			<display:column property="tipoProceso.nombreTipoProceso" title="Instancia" />
			<display:column property="tipoJuzgadoStr" title="Tipo de Tribunal" />
			<display:column property="juzgadoStr" title="N�mero de Tribunal" />
			<display:column property="expediente" title="N�mero Expediente" />
			<display:column property="resolucion.especialista.persona.nomb12" title="Abogado" />
			<display:column property="observaciones" title="Observaciones" />
			<display:column property="ultimoMovimiento.tipoMovimientoInstancia.nombre" title="�ltimo Movimiento" />
			<display:column title="Movimientos" media="html">
					<a href="javascript: movimientos( ${reportTable.id} );">Administrar</a>
			</display:column>
			<display:column title="Eliminar" media="html">
				<a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>	
			</display:column>
		</display:table>
	</div>
	<div style="margin-top: 10px">
		<input type="button" value="Volver" onclick="volver();"/>
	</div>

	<script type="text/javascript">

		function show(id) {
		
			var url = '';
		
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=showInstanciaBlanco&';
			url += 'process.procesoResolucion.id=' + id
			url += '&process.idSolicitud=' + document.getElementById('idSolicitud').value;
			window.location = url;
			
			
		}
		
		function eliminar(id) {
			var url = '';

			var idproyectoPop = "'${ProcessForm.process.idproyectoPop}'";
		
			if (confirmDelete()) {
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}&';
				url += 'process.accion=eliminarInstanciaBlanco&';
				url += 'process.procesoResolucion.id=' + id
				url += '&process.procesoResolucion.resolucion.id=' + document.getElementById('idResolucion').value;
				url += '&process.idSolicitud=' + document.getElementById('idSolicitud').value;
				url += '&process.idproyectoPop=' +idproyectoPop;
				window.location = url;
			}
		}	
		function instancias(id){
				var url = '';
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}&';
				url += 'process.accion=listarInstancias&';
				url += 'process.idSolicitud=' + id;
		
				window.location = url;
		}
		
		function nuevo(){
		
				var url = '';
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}';
				url += '&process.accion=nuevaInstanciaBlanco';
				url += '&process.idproyectoPop=' +${ProcessForm.process.procesoResolucion.resolucion.proyecto.id};
				url += '&process.idResolucion=' + ${ProcessForm.process.procesoResolucion.resolucion.id};
				window.location = url;
		
		}

		function movimientos(id){
			
			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=listarMovimientosBlanco&';
			url += 'process.procesoResolucion.id=' + id;
			url += '&process.procesoResolucion.resolucion.id=' + document.getElementById('idResolucion').value;
			//url += '&process.procesoResolucion.resolucion.numero=' + ${ProcessForm.process.procesoResolucion.resolucion.numero};
			url += '&process.procesoResolucion.resolucion.proyecto.id=' + ${ProcessForm.process.procesoResolucion.resolucion.proyecto.id};
			url += '&process.procesoResolucion.procesoResolucion.resolucion.proyecto.numeroAtencion=' + ${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion};
	        //alert(url);
			window.location = url;
		}

		function volver(){

			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}';
			url += '&process.accion=listarResolucionFiltroInicio';
			url += '&process.idproyectoPop=' + document.getElementById('idProyecto').value;
			window.location = url;
		}

</script>