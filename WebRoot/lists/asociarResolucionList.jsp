<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ page import="com.nirven.creditos.hibernate.Linea"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@page import="com.asf.gaf.hibernate.Ejercicio"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ page import="com.asf.util.DateHelper"%>
<%@ page import="java.util.Date"%>

<div class="title">Generar Resoluciones</div>
<%
request.setAttribute("URL_ADE", DirectorHelper.getString("URL.ADE"));
request.setAttribute("URL_GAF", DirectorHelper.getString("URL.GAF"));
request.setAttribute("ejercicio", Ejercicio.getEjercicioActual());
%>
<html:form action="actions/asociarResolucion.do"
	styleId="AsociarResolucionExportForm" enctype="multipart/form-data">
	<html:hidden styleId="do" property="do" value="listar" />
	<html:hidden styleId="ocultarGenerar" property="ocultarGenerar"
		name="AsociarResolucionExportForm"
		value="${AsociarResolucionExportForm.ocultarGenerar}" />
	<html:hidden styleId="htmlDocumento" property="htmlDocumento"
		name="AsociarResolucionExportForm"
		value="${AsociarResolucionExportForm.htmlDocumento}" />

	<div align="left" style="position: relative; left: 15px">
		<html:errors />
	</div>

	<logic:empty name="AsociarResolucionExportForm" property="objetoiId">
		<table border="0">
			<tr>
				<th>L�nea de cr�dito:</th>
				<td><asf:select name="linea"
						entityName="com.nirven.creditos.hibernate.Linea"
						listCaption="getId,getNombre" listValues="getId"
						filter="activa = true order by id" nullValue="true"
						nullText="Seleccione l�nea..."
						value="${AsociarResolucionExportForm.linea}" /></td>
			</tr>
			<tr>
				<th>Nro de Resoluci�n:</th>
				<td><asf:text maxlength="40" property="resolucion" type="text"
						name="AsociarResolucionExportForm"
						value="${AsociarResolucionExportForm.resolucion}"></asf:text></td>
			</tr>
		</table>
		<html:submit value="Listar" styleId="bListar" onclick="listar();" />
	</logic:empty>
	<br />
	<br />
	<input type="checkbox" onclick="activar();" id="activacion" />Seleccionar Todos 
	<br />
	<display:table name="AsociarResolucionExportForm" property="result"
		id="oi" offset="0"
		requestURI="${pageContext.request.contextPath}/actions/asociarResolucionExport.do?do=listar();">
		<display:column title="Selecci�n">
			<input type="checkbox" name="seleccion-${oi.id}" value="${oi.id}"
				<logic:notEmpty name="AsociarResolucionExportForm" property="objetoiId">checked="checked"</logic:notEmpty> />
		</display:column>
		<display:column property="persona.cuil12" title="CUIT/CUIL"></display:column>
		<display:column property="persona.nomb12" title="Nombre"></display:column>
		<display:column property="numeroAtencion" title="Proyecto"></display:column>
		<display:column property="fechaExpediente" title="Fecha Expte."></display:column>
		<display:column title="Expediente">
			${oi.expediente}
			<logic:notEmpty name="oi" property="expediente">
				<logic:notEqual name="oi" property="expediente" value="0">
					<br />
					${URL_ADE}
					${URL_GAF}
					<!--  ojo esto no funciona cuando hay datos -->
					<logic:notEmpty name="URL_ADE" scope="page">
						<a
							href="${URL_ADE}/actions/jreport.do?do=exec&reportName=caratula.jasper&params=identificacion=${oi.expediente}&export=pdf"
							target="_blank">Car�tula</a>
					</logic:notEmpty>
					<!--  ojo esto no funciona cuando hay datos -->
					<logic:notEmpty name="URL_GAF" scope="page">
						<a
							href="${URL_GAF }/actions/jreport.do?do=exec&export=PDF&reportName=volanteImputacion.jasper&params=WHERE%3D++WHERE+e.expediente=%27${oi.expediente }%27+and+e.ejercicio=${ejercicio}"
							target="_blank">Imputaciones</a>
					</logic:notEmpty>
				</logic:notEqual>
			</logic:notEmpty>
		</display:column>
	</display:table>
	<br />
	<br />
	<div id="datosGenerales">
		<table>
			<tr>
				<th>Resoluci&oacute;n:</th>
				<td><asf:select name="escrito"
						entityName="com.nirven.creditos.hibernate.Escrito"
						value="${AsociarResolucionExportForm.escrito}"
						listCaption="identificacion" listValues="id"
						filter="tipoEscrito = 'R' order by identificacion"
						nullValue="true" nullText="Seleccione el modelo de resoluci�n..." /></td>
			</tr>
			<tr>
				<th>N&uacute;mero de Resoluci&oacute;n:</th>
				<td><asf:text name="nroResolucion" property="nroResolucion"
						type="text" maxlength="50" /></td>
			</tr>
			<tr>
				<th>Fecha de Resoluci&oacute;n:</th>
				<td><asf:calendar property="fechaRStr" /></td>
			</tr>
			<tr>
				<th>Resoluci&oacute;n Definitiva:</th>
				<td><input type="checkbox" name="chkDefinitivo" value="checked"
					onclick="habilitarAdjuntar();" /></td>
			</tr>
		</table>
	</div>
	<div id="adjuntar" style="display: none">
		<table>
			<tr>
				<th>Tipo de Documento</th>
				<td><asf:select2 name="AsociarResolucionExportForm"
						property="idTipoDeArchivo"
						value="${AsociarResolucionExportForm.idTipoDeArchivo}"
						descriptorLabels=" Nombre" entityProperty="id"
						descriptorProperties="nombre, descripcion"
						entityName="TipoDeArchivo" where2=" padre_id IS NOT NULL "
						accessProperty="id" /></td>
			</tr>
			<tr>
				<th>Adjuntar archivo de Resoluci&oacute;n Definitiva:</th>
				<td><html:file property="formFile" styleId="adjunto"
						accept=".pdf" /></td>
			</tr>
		</table>
	</div>
	<br />
	<logic:equal value="false" name="AsociarResolucionExportForm"
		property="ocultarGenerar">
		<html:submit onclick="generar();" value="Generar Resoluci�n" />
	</logic:equal>
</html:form>

<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
<script type="text/javascript">
	function generar() {
		var msg = "";
		var definitivo = false;
		var nodoCheck = document.getElementsByName("chkDefinitivo");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].checked == true) {
				definitivo = true;
			}
		}
		if ($('nroResolucion').value == null || $('nroResolucion').value == "") {
			msg += "No se ha indicado un n�mero de resoluci�n.\n"
		}
		if ($('fechaRStr').value == null || $('fechaRStr').value == "") {
			msg += "No se ha indicado una fecha de resoluci�n.\n"
		}
		if (definitivo) {
			if ($('adjunto').value == null || $('adjunto').value == "") {
				msg += "No ha adjuntado el archivo.\n";
			}
		}
		if (msg == "") {
			$("AsociarResolucionExportForm").action = "${pageContext.request.contextPath}/actions/asociarResolucion.do?do=generar";
			$("do").value = "generar";
		} else {
			alert(msg);
		}
	}

	function bajarDocumento() {
		$("AsociarResolucionExportForm").action = "${pageContext.request.contextPath}/actions/asociarResolucion.do?do=bajarDocumento";
		$("do").value = "bajarDocumento";
		var form = $('AsociarResolucionExportForm');
		form.submit();
	}

	function listar() {
		$("AsociarResolucionExportForm").action = "${pageContext.request.contextPath}/actions/asociarResolucion.do?do=listar";
		$("do").value = "listar";
	}

	function marcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox"
					&& nodoCheck[i].name != "chkDefinitivo"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = true;
			}
		}
	}

	function desmarcarTodos() {
		var nodoCheck = document.getElementsByTagName("input");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].type == "checkbox"
					&& nodoCheck[i].name != "chkDefinitivo"
					&& nodoCheck[i].disabled == false) {
				nodoCheck[i].checked = false;
			}
		}
	}

	function activar() {
		var check = document.getElementById('activacion');
		if (check.checked == true) {
			marcarTodos();
		} else {
			desmarcarTodos();
		}
	}

	function habilitarAdjuntar() {
		var nodoCheck = document.getElementsByName("chkDefinitivo");
		for (i = 0; i < nodoCheck.length; i++) {
			if (nodoCheck[i].checked == true) {
				$('adjuntar').style.display = "";
			} else {
				$('adjuntar').style.display = "none";
			}
		}
	}
</script>
${AsociarResolucionExportForm.script}
