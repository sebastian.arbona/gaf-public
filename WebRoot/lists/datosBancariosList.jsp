<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  <html:hidden property="process.accion" value="nuevo" />
  <html:hidden property="process.persona.id" styleId="idPersonaTitular" value="${ProcessForm.process.persona.id}" />
  <div style="width: 40%" align="left">
    <html:errors />
  </div>
  <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=nuevo">
    <html:submit>
      <bean:message key="abm.button.new" />
    </html:submit>
  </asf:security>
  <br>
  <br>
  <logic:notEmpty name="ProcessForm" property="process.cuentasBancarias">
    <display:table name="ProcessForm" property="process.cuentasBancarias" pagesize="5" id="reportTable" export="true"
      requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar"
    >
      <display:caption>Cuentas Bancarias de:  ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12}</display:caption>
      <display:column title="ID" media="html" sortable="true">
        <a href="javascript: load( ${ reportTable.id }  );">${ reportTable.id }</a>
      </display:column>
      <display:column title="Tipo de Cuenta" property="tipoCuentaBancariaStr" sortable="true" />
      <display:column title="Banco" property="bancoStr" sortable="true" />
      <display:column title="N�mero de Cuenta" property="nroCuentaBancaria" sortable="true" />
      <display:column title="N�mero de CBU" property="cbu" sortable="true" />
      <display:column title="Fecha de Apertura" property="fechaAperturaStr" sortable="true" />
      <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=eliminar">
        <display:column title="Eliminar" media="html">
          <a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>
        </display:column>
      </asf:security>
    </display:table>
  </logic:notEmpty>
  <logic:empty name="ProcessForm" property="process.cuentasBancarias">
    La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no tiene Cuentas Bancarias.
</logic:empty>
</html:form>
<script language='javascript'>
function load(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.persona.id=${ProcessForm.process.persona.id}&';
	url += 'process.cuentaBancaria.id=' + id;

	window.location = url;
}// fin load.-

function eliminar(id) {
	if (confirmDelete()) {
		var url = '';

		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=eliminar&';
		url += 'process.persona.id=${ProcessForm.process.persona.id}&';
		url += 'process.cuentaBancaria.id=' + id;

		window.location = url;
	}
}//fin eliminar.-

Event.observe(window, 'load', function() {
	this.name = "Bancos";
	parent.pop(this);
});
</script>