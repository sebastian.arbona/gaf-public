<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<div class="title" style="margin-bottom: 30px">Cuotas de Bonificaciones de Tasa</div>

<html:form action="/actions/process.do?do=process&processName=BonTasaCuotas" styleId="formulario">
	<html:hidden name="ProcessForm" property="process.accion" value="save" styleId="accion"/>
	<html:hidden name="ProcessForm" property="process.idBonTasa" value="${ProcessForm.process.idBonTasa}" styleId="idBT"/>
	<div>
	<logic:equal value="true" property="process.error" name="ProcessForm">
		${ProcessForm.process.detalleError}
	</logic:equal>
	</div>
	<logic:notEqual value="Baja" name="ProcessForm" property="process.bonificacion.estadoActual.estado.nombreEstado">
<!-- 		<input type="button" value="Solicitar Imputacion GAF" name="ProcessForm" onclick="imputacion();"> -->
<!-- 		<input type="button" value="Entrega Pago GAF" name="ProcessForm" onclick="entregaPago();"> -->
		<html:submit value="Pagar" onclick="pagar();"/>	
	</logic:notEqual>
<div class="grilla">
	<display:table name="ProcessForm" property="process.cuotas"
	id="reportTable" class="com.nirven.creditos.hibernate.BonTasaCuotas" export="true"
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
		<display:setProperty name="report.title" value="Cuotas de Cr�dito"></display:setProperty>
		<display:column media="excel xml pdf" property="id" title="ID"/>
	    <display:column property="numero" title="Nro"/>
	    <display:column title="Fecha Vto" style="width: 130px" property="fechaVencimiento" />	    	
	    <display:column title="Estado" property="tipoEstadoBonCuota"/>
	    <display:column property="importeCuota" title="Importe Cuota" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" style="border: 10px; border-right-color: #0049FF;" />	    
	    <display:column title="Capital" property="capitalFormat"/>
	    <display:column property="compensatorio" title="Int. Compens." decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	    <display:column property="subsidio" title="Subsidio" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" bgcolor="yellow"/>
	    <display:column property="desembolsoBonTasa.numero" title="Desembolso"/>	    
		<display:column property="observaciones" title="Observaciones"/>
	    <display:column property="nroOrdenPago" title="Nro Orden"/>
	    <display:column property="fechaPagoStr" title="Fecha Pago"/>
	    <display:column title="Selecci�n">
	    	<logic:equal value="Pendiente"  name="reportTable" property="tipoEstadoBonCuota">
				<input type="checkbox" name="cuota-${reportTable.id}" value="${reportTable.id}" />
			</logic:equal>	
		</display:column>
	</display:table>
	<table>	
		<tr>	
			<th>Total Bonificaciones</th>
			<td>
			<html:text property="process.totalBonificaciones" name="ProcessForm" disabled="true"/>
			</td>
		</tr>
	</table>	
</div>

</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
	
<script type="text/javascript">
// 	function imputacion(){
// 	var form = document.getElementById("formulario");
// 	var act = document.getElementById("accion");
// 		var msj = "�Esta seguro de querer solicitar el pago de las cuotas seleccionadas?";
//     	if(confirm(msj)){
//        		act.value="imputar";
// 			form.submit();
//        }
// 	}
// 	function entregaPago(){
// 		var form = document.getElementById("formulario");
// 		var act = document.getElementById("accion");	
// 	  	act.value="entregarPago";
// 		form.submit();	
// 	}

	function pagar(){
		var action = document.getElementById("accion");
		action.value= 'pagar';
	}
</script>