<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<div class="title">
	Actualizaci�n por �ndice
</div>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br/>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" value="calcular" />
	<html:hidden property="process.cid"/>
	
	<table>
	<tr>
        <th>
          Nro Solicitud:
        </th>
        <td>
          <asf:text property="ProcessForm.process.creditoDTO.numeroAtencion" type="Long" maxlength="6" />
        </td>
     </tr>
     <tr>
        <th>
          Titular:
        </th>
        <td>
          <asf:selectpop name="process.creditoDTO.titular.id" title="Persona" columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
            captions="idpersona,nomb12,nudo12,cuil12" values="idpersona" entityName="com.civitas.hibernate.persona.Persona"
            value="${ProcessForm.process.creditoDTO.titular.id}"
          />
        </td>
      </tr>
      
	<tr>
        <th>
          Estado:
        </th>
        <td>
          <asf:select entityName="com.civitas.hibernate.persona.Estado" listCaption="idEstado,nombreEstado" attribs="class='Estado'"
            listValues="idEstado" name="process.creditoDTO.idEstado" value="${ProcessForm.process.creditoDTO.idEstado}" 
            nullValue="true" nullText="&lt;&lt;Ninguno&gt;&gt;"/>
        </td>
      </tr>
      <tr>
      		<th>Expediente:</th>
			
			<td>
				<asf:selectpop name="process.expediente" 
					entityName="DocumentoADE" title="Expediente" 
					value="${empty ProcessForm.process.expediente ? '' : ProcessForm.process.expediente}"
					caseSensitive="true" values="identificacion"
					columns="N�,Asunto,Iniciador,CUIT/DNI" 
					captions="identificacion,asunto,datosIniciador.nombre,datosIniciador.identificacion"
					cantidadDescriptors="2"/>
			</td>
		
		</tr>
      <tr>
        <th>
          L�nea:
        </th>
        <td>
          <asf:select entityName="com.nirven.creditos.hibernate.Linea" attribs="class='Linea'" listCaption="getId,getNombre"
            listValues="getId" name="process.creditoDTO.idLinea" value="${ProcessForm.process.creditoDTO.idLinea}" 
            filter="id in (${ProcessForm.process.lineasCER})"
             nullValue="true" nullText="&lt;&lt;Ninguna&gt;&gt;"
          />
        </td>
      </tr>
	</table>
	<br/>    
	<input type="button" value="Listar" id="listarId" onclick="listar();" />
	<br/>
	<br/>
	<logic:empty name="ProcessForm" property="process.creditos">
    	No hay cr�ditos seg�n filtro
    </logic:empty>
	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<div style="text-align: right">
			<a href="javascript:void(0);" onclick="toggleSeleccion();">Seleccionar Todos</a>
		</div>
		<display:table name="ProcessForm" property="process.creditos" id="credito">
			<display:column title="Nro Solicitud" property="numeroAtencion" sortable="true" />
			<display:column title="Expediente" property="expediente" sortable="true" />
			<display:column title="Titular" property="titular.nomb12" sortable="true" />
			<display:column title="L�nea" property="linea" sortable="true" />
			<display:column title="Estado" property="estado" sortable="true" />
			<display:column title="Actualizar" media="html">
                <input type="checkbox" name="creditoseleccionado" class="creditocheck" value="${credito.idObjetoi}"/>
            </display:column>
		</display:table>
		<div style="text-align: right">
			<a href="javascript:void(0);" onclick="toggleSeleccion();">Seleccionar Todos</a>
		</div>
		<br />
		<table>
			<tr>
				<th>Indice:</th>
				<td>
					<asf:select entityName="com.nirven.creditos.hibernate.Indice" listCaption="id,nombre"
		        	listValues="id" name="process.indice.id" value="${ProcessForm.process.indice.id}" filter="tipo='2'"
		         	nullValue="true" nullText="&lt;&lt;Seleccione Indice&gt;&gt;" />
		    	</td>
			</tr>
			<tr>
				<th>Fecha Actualizaci�n</th>
				<td>
					<asf:calendar property="ProcessForm.process.actualizacion" maxlength="10"></asf:calendar>
				</td>
			</tr>
			<tr>
				<th colspan="2"><html:submit onclick="calcular();" value="Calcular"/></th>
			</tr>
		</table>
	</logic:notEmpty>
</html:form>
<script type="text/javascript">
$j = jQuery.noConflict();

function toggleSeleccion() {
	$j(".creditocheck").each(function() {
		if ($j(this).is(':checked')) {
			$j(this).attr('checked', false);
		} else {
			$j(this).attr('checked', true);
		}
	});
}

var accion = $('accion');
var formulario = $('oForm');

function calcular() {
	window.open("${pageContext.request.contextPath}/progress.jsp?reset=true", "Proceso", "width=450, height=180");
}

function listar() {
	accion.value = 'listar';
	formulario.submit();
}
function actualizarIndice(id, estado) {
	window.location = 
	 	'${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}'+
        '&process.accion=load'+
        '&process.creditoDTO.idObjetoi='+ id +
        '&process.creditoDTO.estado='+ estado;     
}
</script>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>