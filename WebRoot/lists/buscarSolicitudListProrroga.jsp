<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" 
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet"/>
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet"/>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
<script type="text/javascript"  src="${pageContext.request.contextPath}/scripts/jquery-1.4.2.min.js"></script>

<script type="text/javascript">


    var $j = jQuery.noConflict();
 	var selected;

    $j(document).ready(function(){
    	 
 	    
    	    $j('.plantillas').change(function(){	
    	        
    			selected=this.value;
    			
    		});

    	  
    });	

     
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
    }
	
    function actualizar( inputIn, defecto )
    {
    	var urlDinamica = "";

    	
    	urlDinamica += "&expediente=" + inputIn.value;
    	urlDinamica += "&" + defecto;

    	return urlDinamica;
    }

    function imprimir(){
		var action = document.getElementById('action');
		action.value = 'imprimirProrroga';
    }

    
	
    function guardar() {
		var action = document.getElementById('action');
		action.value = 'guardar';
	}

	function registrar(){

		var url = '';
	    url += '${pageContext.request.contextPath}/actions/process.do?';
	    url += 'do=process&';
	    url += 'processName=CreditoProcess&';
	    url += 'process.idObjetoi=${ProcessForm.process.solicitud.id}';
	    window.open(url);
	}


	function eliminar(id) 
	{
		if (confirmDelete()){
			var action = document.getElementById('action');
			action.value = 'eliminarProrroga';
			var idSolicitudProrroga = document.getElementById('idSolicitudProrroga');
			idSolicitudProrroga.value = id;
			document.getElementById('ProcessForm').submit();
			return true;
		}
		return false;
		
	}

    
</script>

<div class="title">
	B�squeda de Solicitudes de Pr�rroga
</div>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="ProcessForm">
	<html:hidden property="process.cid"/>
	<html:hidden property="process.action" styleId="action" value="listar"/>
	<html:hidden property="process.idSolicitudProrroga" styleId="idSolicitudProrroga"/>
	<html:errors />
	
	<br>
	<table>
	
		<tr>
			<th>
				Nro de Proyecto migraci�n:
			</th>
			<td>
				<asf:text name="ProcessForm" property="process.numeroCredito" attribs="size=\"30\""
					type="text" maxlength="15" value="" />
			</td>
		</tr>
		<tr>
			<th>
				Nro de Solicitud / Proyecto:
			</th>
			<td>
				<asf:text name="ProcessForm" property="process.numeroAtencion"
					type="Long" maxlength="6" value=""  attribs="size=\"30\""/>
			</td>
		</tr>
		<tr>
			<th>Nro de Expediente:</th>
			<td>
				<asf:autocomplete name="process.expediente" tipo="AJAX" idDiv="autocomplete_choices"
					idInput="expepago" classDiv="autocomplete" value="${ProcessForm.process.expediente}" options="{callback: actualizar, frequency: '0.8', minChars: '2'}"
   					origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarExpediente" attribs="size=\"30\""/>
			</td>
		</tr>
	
	</table>
	<br>
	<html:submit value="Buscar"></html:submit>
	<br>
	<br>
	<div class="grilla">
	<logic:notEmpty property="process.solicitud" name="ProcessForm">
		<display:table name="ProcessForm" property="process.solicitud"
			export="false" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:setProperty name="report.title"
				value="B�squeda de Solicitudes"></display:setProperty>
			<display:column property="numeroCredito" title="Nro de Cr�dito" />
			<display:column title="Nro Solicitud" media="html" sortable="true">
				${reportTable.numeroAtencion}
			</display:column>
			<display:column media="excel xml pdf" property="numeroAtencion"
				title="Nro solicitud" />
			<display:column property="persona.nomb12Str2" title="Titular" />
			<display:column property="expediente" title="Expediente" />
			<display:column property="linea.nombre" title="L�nea de Cr�dito" />
			<display:column title="Etapa" property="estadoActual.estado.nombreEstado" />
			<display:column title="Comportamiento Pago" property="comportamientoActual" />
			
		</display:table>
		
		<logic:notEmpty name="ProcessForm" property="process.solProrroga">
		
			<table width="100%" style="margin-top: 30px">
				<tr><th>Solicitudes de Pr�rroga</th></tr>
			</table>
		
			<display:table name="ProcessForm" property="process.solProrroga" id="reportTable" width="100%">
			<display:column media="excel xml pdf" property="id" title="Pr�rroga N�"/>
            <display:column title="Pr�rroga N�" sortProperty="id" sortable="true" media="html" >
				 <a href="javascript: load( ${reportTable.id} );" >${reportTable.id}</a>
			</display:column>
			<display:column title="Estado" property="estadoActualProrroga.estado.nombreEstado" />
			<display:column title="Motivo" property="motivo" />
			<display:column title="Fecha Ingreso" property="fechaIngreso" decorator="com.asf.displayDecorators.DateDecorator" align="center"/> 
			<display:column title="Imprimir Pr�rroga" media="html" paramId="process.id" paramProperty="id" align="center">
				<html:submit onclick="return imprimir();">Imprimir</html:submit>
			 </display:column>
			 <display:column title="" media="html" align="center">
			   <logic:equal property="estadoActualProrroga.estado.idEstado" value="29" name="reportTable">
			         <html:submit onclick="return eliminar(${reportTable.id});">Eliminar</html:submit>
			   </logic:equal>

			 </display:column>
			 
			<display:column title="Resoluci�n N�" align="center">
				<logic:notEmpty property="nroResolucion" name="reportTable">
					${reportTable.nroResolucion}
				</logic:notEmpty>
			</display:column>
			<display:column title="Registrar Pr�rroga" align="center">
				<logic:equal  value="APROBADA" property="estadoActualProrroga.estado.nombreEstado" name="reportTable">
					 <html:submit onclick="return registrar();">Registrar</html:submit>
				</logic:equal>
				<logic:equal value="REGISTRADA" property="estadoActualProrroga.estado.nombreEstado" name="reportTable">
					<a href="${pageContext.request.contextPath}/DownloadIt.do?do=pdf&boleto=false&tipoNoti=${reportTable.objetoi.notificacionProrroga.tipoAviso}&ids=${reportTable.objetoi.notificacionProrroga.id}">Ver PDF</a>
				</logic:equal>			
			 </display:column>
		</display:table>
		</logic:notEmpty>
		<br/>
		<br/>
		
		<table width="100%">
	
			<tr>
				<th>Motivo</th>
				
				<td>
					<asf:select name="process.motivoProrroga"
						entityName="com.asf.hibernate.mapping.Tipificadores"
						value="${process.idTipificador}" 
						listCaption="descripcion" listValues="descripcion"
						filter="categoria = 'MotivoProrroga'  order by codigo"
						nullValue="true" nullText="Seleccione motivo..." />
				</td>
			</tr>
			<tr>
				<th>Fecha Ingreso Solicitud</th>
				
				<td>
					<asf:calendar property="ProcessForm.process.fechaIngSolStr" />
				</td>
			</tr>
			<tr>
				<th>Observaciones</th>
				
				<td>
					<html:textarea property="process.observacion" cols="40" rows="4" styleId="Observaciones" onkeypress="caracteres(this,100,event)"/>
				</td>
				
			</tr>
		</table>
		<br>
			<html:submit onclick="return guardar();">Guardar</html:submit>
		<br>
	</logic:notEmpty>	
	</div>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

<script language='javascript'>

function load(id) 
{

		var action = document.getElementById('action');
		action.value = 'load';
		var idSolicitudProrroga = document.getElementById('idSolicitudProrroga');
		idSolicitudProrroga.value = id;
		document.getElementById('ProcessForm').submit();

}//fin load.-

</script>