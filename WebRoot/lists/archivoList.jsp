<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form
	action="/actions/ArchivoAction.do?entityName=${param.entityName}&filter=true">
	<html:hidden property="paramName[0]" styleId="paramName"
		value="${paramName[0]}" />
	<html:hidden property="paramComp[0]" styleId="paramComp"
		value="${paramComp[0]}" />
	<html:hidden property="paramValue[0]" styleId="paramValue"
		value="${paramValue[0]}" />
	<html:hidden property="filter" value="true" />
	<html:hidden property="do" styleId="do" value="list" />
	<asf:security action="/actions/ArchivoAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		<input type="submit" onclick="getElem('do').value='newEntity';"
			value="<bean:message key="abm.button.new"/>" />
	</asf:security>
	<br />
	<br />

	<display:table name="ListResult" property="result" pagesize="5"
		id="reportTable" export="true"
		requestURI="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=list&entityName=${param.entityName}&filter=true">
		<display:setProperty name="report.title"
			value="Administración de Documentos"></display:setProperty>

		<display:column media="html" sortProperty="id" property="id"
			title="ID Archivo"
			href="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=load&entityName=${param.entityName}&load"
			paramId="entity.id" paramProperty="id" />
		<display:column media="pdf xml excel" sortProperty="id" property="id"
			title="ID Archivo" sortable="true" />

		<display:column property="nombre" title="Nombre del Archivo" />
		<display:column property="vigenciaDesdeStr"
			title="Fecha de Vigencia Desde" />
		<display:column property="vigenciaHastaStr"
			title="Fecha de Vigencia Hasta" />
		<display:column property="orden" title="Orden" />
		<display:column title="Ordenar" media="html">
			<html:link
				href="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=subir&entityName=${param.entityName}&filter=true&id=${reportTable.id}">Subir</html:link>
			<html:link
				href="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=bajar&entityName=${param.entityName}&filter=true&id=${reportTable.id}">Bajar</html:link>
		</display:column>

		<display:column media="html" title="Descargar">
			<html:button property="" value="Bajar Documento"
				onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoAction.do?do=descargar&id=${reportTable.id}')" />
		</display:column>
		<asf:security action="/actions/ArchivoAction.do"
			access="do=delete&entityName=${param.entityName}">
			<display:column media="html" honClick="return confirmDelete();"
				title="Eliminar"
				href="${pageContext.request.contextPath}/actions/ArchivoAction.do?do=delete&entityName=${param.entityName}&filter=true"
				paramId="entity.id" paramProperty="id">Eliminar</display:column>
		</asf:security>
	</display:table>
</html:form>

<script language='javascript'>
	Event.observe(window, 'load', function() {
		this.name = "Documentación";
		parent.pop(this);
	});
</script>