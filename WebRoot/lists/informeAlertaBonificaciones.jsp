<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<style>
<!--
	.nivel-caida {
		width: 12px;
		height: 12px;
		box-shadow: 1px 1px 4px;
	}
	
	.nivel-caida.nivel1 {
		background-color: red;
	}
	.nivel-caida.nivel2 {
		background-color: #ff9e00;
	}
	.nivel-caida.nivel3 {
		background-color: #00893a;
	}
-->
</style>

<div class="title">Informe de Alerta de Bonificaciones</div>
<html:form action="/actions/process.do?do=process&processName=InformeAlertaBonificaciones">
	<html:hidden property="process.action" value="generar"/>
	
	<div class="grilla">
		<table style="margin: 20px 0">
			<tr>
				<th>Fecha Emisi&oacute;n</th>
				<td>
					<asf:calendar value="${process.fecha}" property="ProcessForm.process.fecha"/>
				</td>
			</tr>
			<tr>
				<th>D&iacute;as hasta la baja</th>
				<td>
					<html:text property="process.dias" name="ProcessForm"></html:text> o menos
				</td>
			</tr>
			<tr>
	  			<th>Convenio</th>
	  			<td>
	  				<asf:select entityName="com.nirven.creditos.hibernate.ConvenioBonificacion" listCaption="getId,getNombre"
	          			listValues="getId" name="process.idConvenio" value="${ProcessForm.process.idConvenio}" nullText="Todos" nullValue="true"/>
	  			</td>
	  		</tr>
	  		<tr>
	  			<th>L&iacute;nea de cr&eacute;dito</th>
	  			<td>
	  				<asf:select entityName="com.nirven.creditos.hibernate.Linea" listCaption="getId,getNombre"
	          			listValues="getId" name="process.idLinea" value="${ProcessForm.process.idLinea}" nullText="Todas" nullValue="true"/>
	  			</td>
	  		</tr>
			<tr>
				<th colspan="2">
					<html:submit value="Consultar"/>
				</th>
			</tr>
		</table>
		
		<display:table name="ProcessForm" property="process.beans" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
			<display:setProperty name="report.title" value="Informe de Alerta de Bonificaciones"></display:setProperty>
			<display:column media="html">
				<div class="nivel-caida nivel${reportTable.nivel}">
				</div>
			</display:column>		
			
			<display:column title="Proyecto" sortable="true" >
				<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi=${reportTable.idObjetoi}&process.action=buscar">${reportTable.numeroAtencion}</a>
			</display:column>
			<display:column title="Expediente" property="expediente" sortable="true"></display:column>
			<display:column title="Titular" property="titular" sortable="true"></display:column>
			<display:column title="CUIL/CUIT" property="cuit" sortable="true"></display:column>
			<display:column title="L�nea" property="linea" sortable="true"></display:column>
			<display:column title="Estado" property="estado" sortable="true"></display:column>
			<display:column title="Comport. de Pago" property="comportamiento" sortable="true"></display:column>
			<display:column title="Convenio" property="convenio" sortable="true"></display:column>
			<display:column title="Tasa Bonif." property="tasaBonificacion" sortable="true"></display:column>
			<display:column title="Tipo Bonif." property="tipoBonificacion" sortable="true"></display:column>
			<display:column title="D�as Mora" property="diasMora" sortable="true"></display:column>
			<display:column title="D�as Ca�da" property="diasAlerta" sortable="true"></display:column>
			<display:column title="Fecha Notif." property="notifFecha" decorator="com.asf.displayDecorators.DateDecorator" sortable="true"></display:column>
			<display:column title="Obs. Notif." property="notifObservaciones" sortable="true"></display:column>
			<display:column title="Tipo Aviso" property="notifTipoAviso" sortable="true"></display:column>
		</display:table>
	</div>

</html:form>
<iframe width="174" height="189" name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
</iframe>