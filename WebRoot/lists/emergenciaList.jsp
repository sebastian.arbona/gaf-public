<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 30px">Emergencias Agropecuarias</div>

  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	<html:hidden property="process.action" styleId="accion" value="activar"/>
	<html:hidden property="process.idPersona" styleId="persona" value="${ProcessForm.process.idPersona}"/>
	<html:hidden property="process.idEmergencia" styleId="emerg" value="${ProcessForm.process.idEmergencia}"/>
	<logic:equal value="false" property="process.listar" name="ProcessForm">
	<div>
	<center>
	<label>Nueva Emergencia</label>
	</center>
	</div>
	<table border="0" width="90%" align="left" >
		<tr>
			<th>
				Periodo:
			</th>
			<td colspan="5">
				<asf:select name="process.idPeriodo"
							entityName="com.nirven.creditos.hibernate.EmergenciaPeriodo" 
							listCaption="getDescripcionCiclo" 
							listValues="getId"
							value="${ProcessForm.process.idPeriodo}"/>
			</td>
		</tr>
		<tr>
			<th>
				Expediente:
			</th>	
			<td colspan="5">
				<html:text property="process.expediente" name="ProcessForm"/>
			</td>
		</tr>
		<tr>
			<th>
				Observaciones:
			</th>
			<td colspan="3">
				<html:textarea property="process.observaciones" name="ProcessForm" rows="8" cols="60"/>
			</td>			
		</tr>
	</table>
	<div style="clear: both">
		<input type="button" value="Guardar" onclick="Guardar();">	
		<input type="button" value="Cancelar" onclick="Cancelar();">
	</div>	
	</logic:equal>
	<logic:equal value="true" property="process.listar" name="ProcessForm">
		<input type="button" value="Nuevo" onclick="Nueva();">
		<display:table name="ProcessForm" property="process.emergencias"
				defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.Emergencia" export="true">  
			<display:column property="id" title="ID" sortProperty="id" paramProperty="id" paramId="process.idEmergencia" sortable="true" 
   				 href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.action=modificar"/>	  	
			<display:column media="excel xml pdf" property="id" title="ID"/>	          
            <display:column title="Periodo" property="emergenciaPeriodo.descripcionCiclo" sortable="true"/>
            <display:column title="Expediente" property="expediente" sortable="true"/>
            <display:column title="Observaciones" property="observaciones" sortable="true"/>  
                     
    	</display:table>
    </logic:equal>
</html:form>
<script type="text/javascript">
function Guardar(){
var action = document.getElementById('accion');
action.value= 'guardar';
var form = document.getElementById('oForm');
form.submit();
}
function Nueva(){
var action = document.getElementById('accion');
action.value= 'Nueva';
var form = document.getElementById('oForm');
form.submit();
}
function Cancelar(){
var action = document.getElementById('accion');
action.value= 'cancelar';
var form = document.getElementById('oForm');
form.submit();
}
</script>