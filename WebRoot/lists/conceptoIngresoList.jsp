<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>



<div class="title">Parametrizaci&oacute;n de Conceptos de Ingresos</div>
<br><br>


<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" defaultsort="1" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
<display:setProperty name="report.title" value="Parametrizaci�n de Conceptos de Ingresos"></display:setProperty>

    <display:column media="html" property="id" title="C�digo" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.id" paramProperty="id" sortable="true"/>
    <display:column media="excel xml pdf" property="id" title="C�digo"/>
    <display:column property="concepto.detalle" title="Concepto" sortable="true"/>
    <display:column property="tipoConceptoNombre" title="Tipo de Concepto" sortable="true"/>
    <display:column property="codigoIngreso" title="C�digo de Ingreso" sortable="true"/>
    <display:column property="conceptoIngreso" title="Concepto de Ingreso" sortable="true"/>
    
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    <display:column media="html" honClick="return confirmDelete();" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" title="eliminar">Eliminar</display:column>
</asf:security>

</display:table>
</div>
