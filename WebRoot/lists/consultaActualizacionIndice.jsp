<%@page import="com.nirven.creditos.hibernate.InformeDeuda"%>
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Actualizaciones por Indice</div>
<br>

<html:form action="/actions/process.do?do=process&processName=ConsultaActualizacionIndice" styleId="ProcessForm">
    <html:hidden property="process.cid"/>
    
	<table border="0">
		<tr>
			<th>Actualizacion realizada desde</th>
			<td><asf:calendar property="ProcessForm.process.fechaDesde" value="${process.fechaDesde}"/></td>
		</tr>
		<tr>
			<th>Hasta</th>
			<td><asf:calendar property="ProcessForm.process.fechaHasta" value="${process.fechaHasta}"/></td>
		</tr>
		<tr>
			<th colspan="2">
				<html:submit value="Buscar"></html:submit>
			</th>
		</tr>
	</table>
	
	<display:table name="ProcessForm" property="process.resultado" export="true" id="informe"
		requestURI="${pageContext.request.contextPath}/actions/process.do">
		<display:column title="Fecha" property="fecha" decorator="com.asf.displayDecorators.DateDecorator"/>
		<display:column title="Nro Solicitud" property="credito.numeroAtencion" sortable="true" />
		<display:column title="Expediente" property="credito.expediente" sortable="true" />
		<display:column title="Titular" property="credito.persona.nomb12" sortable="true" />
		<display:column title="L�nea" property="credito.linea.nombre" sortable="true" />
		<display:column title="Cuota N�" property="numeroCuota"></display:column>
		<display:column title="Saldo Capital" property="saldoCapital" style="text-align:right" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
		<display:column title="�ndice CER" property="indice" style="text-align:right" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
		<display:column title="Importe CER" property="montoActualizacion" style="text-align:right" decorator="com.asf.displayDecorators.DoubleDecorator"></display:column>
		<display:column title="Usuario" property="usuario" />
	</display:table>
</html:form>


