<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">Historial de Estados</div>
<br>
<br>
<html:form
	action="/actions/process.do?do=process&processName=GarantiaSeguroEstadoProcess"
	styleId="formulario">
	<html:hidden property="process.idObjetoiGarantia"/>
	<html:hidden property="process.action" value="nuevo" styleId="action"/>
	<html:hidden property="process.cid" />

	<p>
	<html:submit>
		Cambiar Estado
	</html:submit>
	</p>
	<display:table name="ProcessForm" property="process.estados" export="true" id="estado" 
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=GarantiaSeguroEstadoProcess&process.cid=${ProcessForm.process.cid}">
		<display:column title="Fecha" property="fechaStr"/>
		<display:column title="Estado" property="estadoStr"/>
		<display:column title="Observaciones" property="observaciones"/>
		<display:column title="Eliminar" media="html">
     			<asf:security action="/actions/process.do" access="do=process&processName=GarantiaSeguroEstadoProcess"> 
    				<a href="${pageContext.request.contextPath}/actions/process.do?processName=GarantiaSeguroEstadoProcess&do=process&process.action=eliminar&process.idGarantiaSeguroEstado=${estado.id}&process.idObjetoiGarantia=${ProcessForm.process.idObjetoiGarantia}" onclick="return">Eliminar</a>
   			       </asf:security> 
    	</display:column>
	</display:table>
	
	<p>
	<html:submit onclick="volver();">
		Volver
	</html:submit>
	</p>
</html:form>
<script type="text/javascript">
	var action = $('action');
	var formulario = $('formulario');

	function volver() {
		action.value = 'volver';
		formulario.submit();
	}
</script>