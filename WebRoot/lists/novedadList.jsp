<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administración de Novedades de Cuenta Corriente</div>
<br>
<br>

<html:form
	action="/actions/process.do?do=process&processName=NovedadesProcess"
	styleId="ProcessForm">
	<html:hidden name="ProcessForm" property="process.cid" />
	<html:hidden name="ProcessForm" property="process.action" value="nuevo" />
	
	<html:submit>
		<bean:message key="abm.button.new" />
	</html:submit>

	<br>
	<br>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.novedades"
			defaultsort="1" export="true" id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&process.action=listar&processName=NovedadesProcess">
			<display:setProperty name="report.title"
				value="Administración de Novedades de Cuenta Corriente"></display:setProperty>

			<display:column media="html" property="id" title="Código"
				sortable="true" />
			<display:column media="excel xml pdf" property="id" title="Código" />
			<display:column property="fecha" title="Fecha" sortable="true"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column property="conceptoDetalle" title="Concepto"
				sortable="true" />
			<display:column property="tipoConceptoStr" title="Tipo de Concepto"
				sortable="true" />
			<display:column property="importe" title="Importe"
				decorator="com.asf.displayDecorators.DoubleDecorator"
				sortable="true" />
			<display:column property="detalle" title="Observaciones" />
			<display:column title="Factura">
				<logic:notEmpty name="reportTable" property="detalleFactura">
    		${reportTable.detalleFactura.factura.numero} - ${reportTable.detalleFactura.factura.proveedor.persona.nomb12}
    	</logic:notEmpty>
			</display:column>
			<display:column title="Impactado Cta Cte">
				<logic:equal name="reportTable" property="impactado" value="true">Si</logic:equal>
				<logic:equal name="reportTable" property="impactado" value="false">No</logic:equal>
			</display:column>
			<display:column media="html" honClick="return confirmDelete();"
				href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=NovedadesProcess&process.cid=${ProcessForm.process.cid}&process.action=eliminar"
				paramId="process.novedad.id" paramProperty="id" title="Eliminar">Eliminar</display:column>
		</display:table>
	</div>
</html:form>