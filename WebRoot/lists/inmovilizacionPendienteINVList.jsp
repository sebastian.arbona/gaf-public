<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html>
	<body>

		<script type="text/javascript">
			
		function enviarInforme() {
				window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=InmovilizacionPendienteINV&process.accion=enviar&process.id=${ProcessForm.process.idStr}";
			}
	
		</script>

		<div class="title">
			Solicitudes Pendientes
		</div>
		<br>
		<br>
		<html:form action="actions/process.do?processName=${param.processName}&do=process">
			
			<div class="grilla">

				<html:hidden property="process.idStr" name="ProcessForm" value="${ProcessForm.process.idStr}" />

				<display:table name="ProcessForm" property="process.creditos" defaultsort="1" id="reportTable">

					<display:column property="persona.nomb12" title="Solicitante" />
					<display:column property="expediente" title="Expediente" />
					<display:column property="productoDeReferencia" title="Producto"/>
					<%-- Oculto columna. Ticket 9159
					<display:column property="depositoMinimo" title="Volumen Segun Contrato" />
					--%>
					<display:column property="depositoMinimo" title="Volumen a Inmovilizar" />
					<display:column property="destinoInm" title="Destino" />
					<display:column media="html" title="Ver Detalle"
						href="${pageContext.request.contextPath}/actions/process.do?processName=InmovilizacionPendienteINV&do=process&process.accion=ver"
						paramId="process.id" paramProperty="id">Ver Detalle</display:column>
				</display:table>
			</div>
		
		<logic:equal name="ProcessForm" property="process.verDetalle" value="true">
			<div class="title">
				Detalle de Expediente
			</div>
			<display:table name="ProcessForm" property="process.seleccionado" defaultsort="1" id="reportTable">
				<display:column property="persona.nomb12" title="Solicitante" />
				<display:column property="expediente" title="Expediente" />
				<%-- Oculto columna. Ticket 9159
				<display:column property="depositoMinimo" title="Volumen Segun Contrato" />
				--%>
				<display:column property="depositoMinimo" title="Volumen a Inmovilizar" />
				<display:column property="destinoInm" title="Destino" />
			</display:table>
			<div class="title">
				Detalle de la Garant�a
			</div>
			<table border="0">
				
				<logic:notEmpty name="ProcessForm" property="process.garantia.valores">
				
				<logic:iterate id="valorGarantia" name="ProcessForm" property="process.garantia.valores">
				<tr>
					<th>	
						<bean:write name="valorGarantia" property="campo.nombre"/>
					</th>
					<td>
						<bean:write name="valorGarantia" property="valorCadena"/>	
					</td>
				</tr>
				
				</logic:iterate>
					
				</logic:notEmpty>
				
				<tr>
					<td>
						Observaciones
					</td>
					<td>
						<html:textarea property="process.garantia.observacion" 
							name="ProcessForm"  disabled="true" />
					</td>
				</tr>
			</table>
			</logic:equal>
			<logic:equal name="ProcessForm" property="process.boton" value="true">
				<input type="button" name="inmovilizacionRealizada" value="Inmovilizacion Realizada" onclick="enviarInforme();">
			</logic:equal>
			
		</html:form>
		
	</body>

</html>
