<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Informe de Liberacion</div>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />

	<div align="right">
	Mendoza, ${ProcessForm.process.fecha}
	</div>
	<br>
	<div>
	Señor: R. Alberto Castro <br>
	Coordinador de Acciones <br>
	Interinstitucionales p/ Fiscalización <br>
	Instituto Nacional de Vitivinicultura <br>
	Presente <br>
	</div>
	<br>
	<div>
	De nuestra consideración:
	</div>
	<br>
	<div>
	&emsp;&emsp;&emsp;&emsp;Por medio de la presente, en el marco del Convenio suscripto entre la Administradora Provincial del Fondo, 
	Mendoza Fiduciaria S.A.(fiduciaria del Fideicomiso de Garantía Cosecha y Acarreo y/o Elaboración de productos vitivinícolas) y el 
	organismo al cual Ud. pertenece, le informamos el <strong><u>nuevo</u> volumen a mantener del producto en dominio fiduciario de 
	Mendoza Fiduciaria SA</strong>, respecto de los expedientes de la "Operatoria Financiamiento gastos de cosecha y acarreo de uva y 
	elaboración de productos vitivinícolas", que  se detallan a continuación:	
	</div>
	<br>
	<br>
    <input type="button" value="Listar" onclick="listar();">
    <br>
    <logic:equal value="true" property="process.lista" name="ProcessForm">
	<display:table name="ProcessForm" property="process.creditosDTO" defaultsort="1" id="credito" class="com.asf.cred.business.ObjetoiDTO">
					<display:column property="numeroAtencion" title="Proyecto" />
					<display:column property="titular.nomb12" title="Titular" />
					<display:column property="expediente" title="Expediente" />
					<display:column property="totalInmovilizado" title="Total Inmovilizado" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column property="producto" title="Producto" />						
					<display:column property="liberadoALaFecha" title="Liberado a la Fecha" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>										
					<display:column property="ultimaLiberacion" title="Última Liberacion" />				
					<display:column property="nuevoVolumen" title="Nuevo Volumen" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
					<display:column title="RECIBIDO" media="html">
						    	<a onclick="this.style.display='none';" href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accion=Recibido&process.id=${credito.idObjetoi}&process.credito.totalInmovilizado=${credito.totalInmovilizado}&process.credito.nuevoVolumen=${credito.nuevoVolumen}">
						    	RECIBIDO
						    	</a>
					</display:column>
	</display:table>
	</logic:equal>

	<br>
	<div>
	Sin otro particular lo saludo atentamente.
	</div>

	<logic:equal value="true" property="process.detalle" name="ProcessForm">
	<div class="title">
				Detalle de la Garantía
			</div>
			<table border="0">
				<logic:notEmpty name="ProcessForm" property="process.valores">
				<logic:iterate id="valorGarantia" name="ProcessForm" property="process.valores">
				<tr>
					<th>	
						<bean:write name="valorGarantia" property="campo.nombre"/>
					</th>
					<td>
						<bean:write name="valorGarantia" property="valorCadena"/>	
					</td>
				</tr>
				
				</logic:iterate>
					
				</logic:notEmpty>
				</table>
	</logic:equal>
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
<script type="text/javascript">
function listar() {
   		var accion = $('accion');
   		accion.value = "listar";
   		var form = $('oForm');
   		form.submit();
    }

</script>