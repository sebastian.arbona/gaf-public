<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<body class=" yui-skin-sam">
	<div class="title">
		Administraci�n de Solicitudes
	</div>
	<br>
	<br>
	<link type="text/css"
		href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
		rel="stylesheet" />
	<link type="text/css"
		href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
		rel="stylesheet" />
		
<div><html:errors /></div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
</script>
	<html:form
		action="/actions/process.do?do=process&processName=${param.processName}"
		styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="list" />		
		<b>C�digo de Persona:</b>
		<asf:selectpop name="process.idPersona" title="Persona"
			columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
			captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
			entityName="com.civitas.hibernate.persona.Persona" value="" />
		<br />
		<b>Nro de Solicitud/Proyecto:</b>
		<asf:text name="ProcessForm" property="process.numeroAtencion"
			type="Long" maxlength="6" value="" />
		<br />
		<html:submit>Consultar</html:submit>
	</html:form>
	<br>
	<br>
	<html:form
		action="/actions/process.do?do=process&processName=${param.processName}"
		styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="nuevo" />
		<asf:security
			action="/actions/process.do?do=process&processName=${param.processName}"
			access="accion=nuevo">
			<html:submit onclick="getElem('accion').value='nuevo';">
				<bean:message key="abm.button.new" />
			</html:submit>
		</asf:security>
		<html:hidden property="process.idSolicitud"
			styleId="process.idSolicitud" value="${process.idSolicitud}" />
		<html:hidden property="process.numeroAtencion"
			styleId="process.numeroAtencion" value="${process.numeroAtencion}" />
		<html:hidden property="filter" value="true" />
	</html:form>
	<br>
	<br>
	<br>
	<display:table name="ProcessForm" property="process.solicitudes"
		export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
		<display:setProperty name="report.title"
			value="Administraci�n de Estados"></display:setProperty>
		<display:column title="Nro Solicitud" media="html" sortable="true">
			<a href="javascript: show( ${ reportTable.id}  );">${
				reportTable.numeroAtencion}</a>
		</display:column>
		<display:column media="excel xml pdf" property="numeroAtencion"
			title="Nro solicitud" />
		<display:column property="persona.nomb12Str2" title="Persona" />
		<display:column property="fechaSolicitudStr" title="Fecha"
			sortable="true" />
		<display:column property="linea.nombre" title="L�nea de Credito" />
		<display:column property="etapa" title="Etapa" />
		<display:column property="objeto" title="Objeto" />
		<display:column title="Eliminar" media="html">
			<logic:equal value="true" property="esperandoDocumentacion" name="reportTable">
			<a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>
			</logic:equal>
		</display:column>
		<display:column title="Modificar" media="html">
			<logic:equal name="reportTable" value="true" property="solicitudEditable">	
				<a href="javascript: load( ${reportTable.id} );">Modificar</a>
			</logic:equal>
		</display:column>
		<logic:present name="reportTable">
			<logic:empty name="reportTable" property="id">
				<display:column property="id" title="ID" sortable="true" />
			</logic:empty>
		</logic:present>
	</display:table>

	<script type="text/javascript">

function load(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.idSolicitud=' + id;

	window.location = url;
}// fin modificar.-

function show(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=show&';
	url += 'process.idSolicitud=' + id;

	window.location = url;
}

function eliminar(id) {
	var url = '';

	if (confirmDelete()) {
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=eliminar&';
		url += 'process.idSolicitud=' + id;

		window.location = url;
	}
}// fin modificar.-   

//fin irARequisitos.-

</script>