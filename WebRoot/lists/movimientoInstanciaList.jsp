<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />

<body class=" yui-skin-sam">
	<div class="title">
		Administración de Movimientos de Instancias
	</div>
	<br>
	<br>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
		
<div><html:errors /></div>
</br>
</br>
</body>
	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
		<html:hidden property="process.accion" styleId="accion" value="nuevoMovimiento" />
		<html:hidden property="process.sid"/>
		<html:hidden property="process.idSolicitud" styleId="idSolicitud"/>
		<html:hidden name="ProcessForm" property="process.procesoResolucion.id"/>
		<html:hidden property="process.idproyectoPop"/>	
		
		<html:hidden property="process.opcion" styleId="opcion" />
		<html:hidden property="process.persona" styleId="persona" />
		<html:hidden property="process.numeroAtencion" styleId="numeroAtencion" />
		<html:hidden property="process.numeroResolucion" styleId="numeroResolucion" />
		<html:hidden property="process.idAbogado" styleId="idAbogado" />
		
		<table>
			<tr>
				<th>Tipo de Movimiento:</th>
				<td>
            		<asf:text name="ProcessForm" property="process.tipoMovimientoIntanciaStr"  type="text" maxlength="30" value="${ProcessForm.process.tipoMovimientoIntanciaStr}"/>
				</td>
			</tr>
		</table>
		
		<html:submit onclick="getElem('accion').value='listarMovimientos';" value="Consultar"/>
		<asf:security action="/actions/process.do?do=process&processName=${param.processName}" access="accion=nuevoMovimiento">
			<html:submit onclick="getElem('accion').value='nuevoMovimiento';" value="Nuevo Movimiento"/>
		</asf:security>		
	</html:form>
	
	<br>
	<b>Número de resolución:
		${ProcessForm.process.procesoResolucion.resolucion.numero} </b>

	<br>
	<b>Proyecto:
		${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion} </b>

	<br>
	<b>Titular:
		${ProcessForm.process.procesoResolucion.resolucion.proyecto.persona.nomb12Str} </b>

	<br>
	
	<br>
<br>
<div class="grilla">
	<display:table name="ProcessForm" property="process.movimientosInstancia" export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listarMovimientos&process.procesoResolucion.id=${ProcessForm.process.procesoResolucion.id}">
		<display:setProperty name="report.title" value="Administración de Movimientos - Resolucion:${ProcessForm.process.procesoResolucion.resolucion.numero} - Proyecto: ${ProcessForm.process.procesoResolucion.resolucion.proyecto.numeroAtencion} - Titular: ${ProcessForm.process.procesoResolucion.resolucion.proyecto.persona.nomb12Str}"></display:setProperty>
		
		
		<display:column title="Nro Movimiento" media="html" sortable="true">
			<a href="javascript: show( ${ reportTable.id}  );">${reportTable.id}</a>
		</display:column>
		<display:column property="instancia.resolucion.proyecto.numeroAtencion" title="Número Proyecto" />
		<display:column property="tipoMovimientoInstancia.nombre" title="Tipo de Movimiento" />
		<display:column property="fechaMovimientoStr" title="Fecha Movimiento" />
		<display:column property="fechaCargaMovimientoStr" title="Fecha Carga Movimiento" />
		<display:column property="observacion" title="Observacion"/>
		
		<display:column title="Eliminar" media="html">
   			<asf:security action="/actions/process.do" access="do=process&processName=EliminarMovimientosInstancia">
   				<a href="javascript: eliminar( ${reportTable.id} );">Eliminar</a>
   			</asf:security>
    	</display:column>
	</display:table>
</div>
	
	<div style="margin-top: 10px">
			
			<input type="button" value="Volver" onclick="volver();"/>
		</div>

	<script type="text/javascript">


		
		function show(id) {
		
			var url = '';
		
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=showMovimiento&';
			url += 'process.movimientoInstancia.id=' + id
			window.location = url;
		}
		
		function eliminar(id) {
			var url = '';
		
			if (confirmDelete()) {
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}&';
				url += 'process.accion=eliminarMovimiento&';
				url += 'process.movimientoInstancia.id=' + id

		
				window.location = url;
			}
		}	
		function instancias(id){
				var url = '';
				url += '${pageContext.request.contextPath}/actions/process.do?';
				url += 'do=process&';
				url += 'processName=${param.processName}&';
				url += 'process.accion=listarInstancias&';
				url += 'process.idSolicitud=' + id;
		
				window.location = url;
		}
		
		function volver(){

			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}';
			url += '&process.accion=listarInstancias';
			url += '&process.idproyectoPop=${ProcessForm.process.procesoResolucion.resolucion.proyecto.id}';
			url += '&process.idSolicitud=${ProcessForm.process.procesoResolucion.resolucion.id}'; 
			url += '&process.numeroAtencion=${ProcessForm.process.numeroAtencion}';
			
			url += '&process.idAbogado=${ProcessForm.process.idAbogado}';
			url += '&process.numeroResolucion=${ProcessForm.process.numeroResolucion}';

			window.location = url;
		}
		// fin modificar.-   
		
		function movimientos(id){
			
			var url = '';
			url += '${pageContext.request.contextPath}/actions/process.do?';
			url += 'do=process&';
			url += 'processName=${param.processName}&';
			url += 'process.accion=listarMovimientos&';
			url += 'process.procesoResolucion.id=' + id;
	
			window.location = url;
}

</script>