<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.asf.util.DateHelper"%>
<%@ page import="java.util.Calendar"%>

<%
	Calendar calendar = Calendar.getInstance(); //Obtiene la fecha de hoy.
calendar.add(Calendar.DATE, -30);

String fechaDesde = DateHelper.getString(calendar.getTime());
String fechaHoy = DateHelper.getString(new Date());

fechaDesde = fechaDesde.substring(fechaDesde.lastIndexOf("/") + 1) + "-"
		+ fechaDesde.substring(fechaDesde.indexOf("/") + 1, fechaDesde.lastIndexOf("/")) + "-"
		+ fechaDesde.substring(0, fechaDesde.indexOf("/"));
pageContext.setAttribute("fechaDesde", fechaDesde);

fechaHoy = fechaHoy.substring(fechaHoy.lastIndexOf("/") + 1) + "-"
		+ fechaHoy.substring(fechaHoy.indexOf("/") + 1, fechaHoy.lastIndexOf("/")) + "-"
		+ fechaHoy.substring(0, fechaHoy.indexOf("/"));
pageContext.setAttribute("fechaHoy", fechaHoy);
%>

<div class="title">Administración de Índices y Tasas</div>
<br>
<html:form action="actions/abmAction.do?entityName=${param.entityName}">
	<html:hidden property="do" styleId="accion" value="newEntity" />

	<asf:security action="/actions/abmAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		<button type="submit">
			<bean:message key="abm.button.new" />
		</button>
	</asf:security>

</html:form>

<br>
<div class="grilla">
	<display:table name="ListResult" property="result" export="true"
		id="reportTable" defaultsort="1"
		requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
		<display:setProperty name="report.title"
			value="Administración de Indices"></display:setProperty>
		<display:column property="tipoStr" title="Tipo" sortable="true" />
		<display:column property="nombre" title="Nombre" sortable="true" />
		<display:column property="valorInicialStr" title="Valor Inicial"
			sortable="true" />
		<display:column property="aplicaEnStr" title="Aplica en"
			sortable="true" />
		<display:column media="html" title="Índice Valor">
			<a
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=IndiceValor&filter=true&paramValue[0]=${reportTable.id}&paramName[0]=indice.id&paramComp[0]==&nombre=${reportTable.nombre}&tipo=${reportTable.tipoStr}
    			&paramName[1]=fecha&paramComp[1]=>=&paramValue[1]=${fechaDesde}&paramName[2]=fecha&paramComp[2]=<=&paramValue[2]=${fechaHoy}">
				Índice Valor </a>
		</display:column>
		<display:column media="html" title="Actualizar Valores">
			<a
				href="${pageContext.request.contextPath}/actions/process.do?processName=CargaIndiceValor&do=process&process.indice=${reportTable.id}">
				Actualizar </a>
		</display:column>
		<asf:security action="/actions/abmAction.do"
			access="do=delete&entityName=${param.entityName}">
			<display:column media="html" honClick="return confirmDelete();"
				title="Eliminar"
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}"
				paramId="entity.id" paramProperty="id">
				<bean:message key="abm.button.delete" />
			</display:column>
		</asf:security>
		<display:column media="html" title="Concepto">
			<a
				href="${pageContext.request.contextPath}/actions/process.do?processName=IndiceConcepto&do=process&process.indiceId=${reportTable.id}">
				Concepto </a>
		</display:column>
	</display:table>
</div>
