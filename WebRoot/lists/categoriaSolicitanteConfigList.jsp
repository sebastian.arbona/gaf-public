<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administraci&oacute;n de Configuraciones de
	Categor&iacute;as de Solicitante</div>
<br>
<html:form
	action="actions/abmAction.do?entityName=CategoriaSolicitanteConfig">
	<html:hidden property="do" styleId="accion" value="newEntity" />
	<asf:security action="/actions/abmAction.do"
		access="do=newEntity&entityName=CategoriaSolicitanteConfig">
		<button type="submit">
			<bean:message key="abm.button.new" />
		</button>
	</asf:security>
</html:form>
<br>
<br>
<div class="grilla">
	<display:table name="ListResult" property="result" export="true"
		id="reportTable" defaultsort="1"
		requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=CategoriaSolicitanteConfig">
		<display:setProperty name="report.title"
			value="Administracion de Configuraciones de Categorias de Solicitante"></display:setProperty>
		<display:column property="id" title="ID" sortProperty="id"
			paramId="entity.id" paramProperty="id" sortable="true"
			href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=CategoriaSolicitanteConfig" />
		<display:column property="linea.nombre" title="Linea" sortable="true" />
		<display:column property="fechaDesdeStr" title="Fecha Desde"
			sortable="true" />
		<display:column property="fechaHastaStr" title="Fecha Hasta"
			sortable="true" />
		<display:column property="categoriaSolicitanteStr"
			title="Cat. Solicitante" sortable="true" />
		<display:column property="tipoTasaStr" title="Tipo Tasa"
			sortable="true" />
		<display:column property="indice.nombre" title="Indice"
			sortable="true" />
		<display:column property="valorMasStr" title="Valor Mas"
			sortable="true" />
		<display:column property="valorPorStr" title="Valor Por"
			sortable="true" />
		<display:column property="topeTasaStr" title="Tope Tasa"
			sortable="true" />
		<display:column property="diasAntes" title="Dias Antes"
			sortable="true" />
		<display:column property="tipoCalculoStr" title="Tipo Calculo"
			sortable="true" />
		<asf:security action="/actions/abmAction.do"
			access="do=delete&entityName=CategoriaSolicitanteConfig">
			<display:column media="html" honClick="return confirmDelete();"
				title="Eliminar"
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=CategoriaSolicitanteConfig"
				paramId="entity.id" paramProperty="id">
				<bean:message key="abm.button.delete" />
			</display:column>
		</asf:security>
	</display:table>
</div>

