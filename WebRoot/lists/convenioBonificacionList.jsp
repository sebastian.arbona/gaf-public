<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div class="title">
	Convenios de Bonificación
</div>
<br>
<br>

<html:form
	action="actions/process.do?processName=${param.processName}&do=process">		
</html:form>
<input type="button" value="Nuevo" onclick="nuevoConvenio();">
<br>
<div class="grilla">

<logic:equal value="true" property="process.error" name="ProcessForm">
El convenio no se puede Eliminar debido a que se usa en una o mas Bonificaciones.
</logic:equal>

<display:table name="ProcessForm" property="process.convenios" export="true" id="reportTable" defaultsort="1"
  requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.idConvenio=${ProcessForm.process.idConvenio}">
  <display:setProperty name="report.title" value="Administración de Garantías"></display:setProperty>
  
  <display:column property="id" title="ID" sortProperty="id" paramId="process.idConvenio" paramProperty="id" sortable="true"
    href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accion=modificar&process.idConvenio=${ProcessForm.process.idConvenio}"/>        
  
  <display:column title="Nombre" property="nombre"/>  
  <display:column property="banco.detaBa" title="Ente Bonificador" sortable="true" />
  <display:column property="maxBonConvenio" title="Bonificacion Maxima" sortable="true" />
  <display:column property="maxFinanciamientoConvenio" title="Financiamiento Máximo" sortable="true" />  	
  <display:column property="fechaVencimientoReal" title="Fecha Vencimiento Real" sortable="true" />
  <display:column property="fechaVencimientoAplicacion" title="Fecha Vencimiento Aplicación" sortable="true" />
  
   <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
      href="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process&process.accion=eliminar"
      paramId="process.idConvenio" paramProperty="id">
      <bean:message key="abm.button.delete" />
    </display:column>       
  </display:table>
  </div>
 <script type="text/javascript">
 function nuevoConvenio(){
 	window.location="${pageContext.request.contextPath}/actions/process.do?do=process&processName=ConvenioBonificacionProcess&process.accion=nuevo";
 }
 
 </script>