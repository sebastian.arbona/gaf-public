<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <html:hidden property="process.accion" value="asignar"/>
    <html:hidden property="process.personaTitular.id" styleId="idPersonaTitular" value="${ProcessForm.process.personaTitular.id}" />
    <html:hidden property="process.personaVinculada.personaVinculada_id" styleId="idpersonaVinculada"/>
    <div style="width:70%" align="left"><html:errors/></div>
    <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=asignar">
		<html:submit><bean:message key="abm.button.new"/></html:submit>
	</asf:security>
	<br><br>
    <logic:notEmpty name="ProcessForm" property="process.personasVinculadas" >
        <display:table name="ProcessForm" property="process.personasVinculadas" pagesize="5" id="reportTable" export="true"
                       requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar" >
            <display:caption>Personas Vinculadas al Titular: ${ProcessForm.process.personaTitular.idpersona} - ${ProcessForm.process.personaTitular.nomb12}</display:caption>
            
            <display:column media="excel xml pdf" property="id" title="ID"/>
            <display:column media="html" title="ID" sortProperty="id" sortable="true" >
                <a href="javascript: load( ${reportTable.id} );" >${reportTable.id}</a>
            </display:column>
            <display:column title="Persona Vinculada" property="personaVinculada.nomb12" sortable="true" />
            <display:column title="Tipo de Relaci�n" property="relacion.tipoRelacion.nombreTipoRelacion" sortable="true" />
            <display:column title="Relaci�n" property="relacion.nombreRelacion" sortable="true" />
            <display:column title="Nro Solicitud" property="numeroAtencion" sortable="true"/>
            <asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=eliminar">
	            <display:column title="Eliminar"	media="html" >
	                <a href="javascript: eliminar( ${reportTable.id} );" >Eliminar</a>
	            </display:column>
            </asf:security>
        </display:table>
    </logic:notEmpty>
    <logic:empty name="ProcessForm" property="process.personasVinculadas" >
        La Persona Titular: ${ProcessForm.process.personaTitular.idpersona} - ${ProcessForm.process.personaTitular.nomb12} no tiene Personas Vinculadas
    </logic:empty>
</html:form>
<script language='javascript' >
	function eliminar( id )
    {
		if(confirmDelete())
    	{
	        var url = '';
	        url += '${pageContext.request.contextPath}/actions/process.do?';
	        url += 'do=process&';
	        url += 'processName=${param.processName}&';
	        url += 'process.accion=eliminar&';
	        url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
	        url += 'process.personaVinculada.id=' + id;
	        window.location = url;
    	}
    }//fin eliminar.-

    function load( id ) 
    {
        var url = '';
        url += '${pageContext.request.contextPath}/actions/process.do?';
        url += 'do=process&';
        url += 'processName=${param.processName}&';
        url += 'process.accion=load&';
        url += 'process.personaTitular.id=${ProcessForm.process.personaTitular.id}&';
        url += 'process.personaVinculada.id=' + id;
        window.location = url;
    }//fin load.-

    Event.observe(window, 'load', function() 
    {
    	this.name = "Persona Vinculada";
    	parent.pop(this);
    });
</script>