<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	enctype="multipart/form-data" styleId="oForm">
	<html:hidden property="process.action" name="ProcessForm"
		styleId="action" />
	<html:hidden property="process.cid" name="ProcessForm" />
	<html:hidden property="process.idArchivo" name="ProcessForm"
		styleId="idArchivo" />
	<input type="button" Value="Adjuntar Nuevo" onclick="adjuntar();">
	<br />
	<br />
	<div id="grilla">
		<display:table name="ProcessForm" property="process.archivos"
			id="archivo" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
			<display:column property="nombre" title="Nombre" />
			<display:column property="fechaAdjuntoStr" title="Fecha Adjunto" />
			<display:column property="usuario" title="Usuario" />
			<display:column property="tipo.descripcion" title="Tipo" />
			<display:column property="detalle" title="Descripcion" />
			<display:column title="Bajar" media="html">
				<html:button
					onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${archivo.id}')"
					property="process.objetoiArchivo.id">Bajar</html:button>
			</display:column>
			<display:column title="Eliminar" media="html">
				<html:button onclick="eliminar(${archivo.id});"
					property="process.id">Eliminar</html:button>
			</display:column>
		</display:table>
	</div>
	<br/>
	<input type="button" value="Volver" onclick="volver();"/>
</html:form>

<script language='javascript'>
	var action = $('action');
	var form = $('oForm');
	function adjuntar(){
		action.value = 'nuevo';
		form.submit();
	}
	function eliminar(id){
		var archivoId = $('idArchivo');
		archivoId.value = id;
		action.value = 'eliminar';
		form.submit();
	}		
	function volver(){
		action.value = 'volver';
		form.submit();
	}
</script>