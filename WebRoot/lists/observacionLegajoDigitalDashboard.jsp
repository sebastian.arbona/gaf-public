<style>
.sticky {
    /* General */
    margin-top: 25px;
    margin-bottom: 25px;
    /*margin: 100px auto;*/
    padding: 8px 24px;
    /*width: 380px;*/
    /* Font */
    font-family: 'Permanent Marker', cursive;
    font-size: 1.2em;
    /* Border */
    border:1px #E8Ds47 solid;
    /* Shadow */
    -moz-box-shadow:0px 0px 6px 1px #333333;
    -webkit-box-shadow:0px 0px 6px 1px #333333;
    box-shadow:0px 0px 6px 1px #333333;
    /* Background */
    background: #fefdca; /* Old browsers */
    background: -moz-linear-gradient(top, #fefdca 0%, #f7f381 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fefdca), color-stop(100%,#f7f381)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top, #fefdca 0%,#f7f381 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top, #fefdca 0%,#f7f381 100%); /* Opera11.10+ */
    background: -ms-linear-gradient(top, #fefdca 0%,#f7f381 100%); /* IE10+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fefdca', endColorstr='#f7f381',GradientType=0 ); /* IE6-9 */
    background: linear-gradient(top, #fefdca 0%,#f7f381 100%); /* W3C; A catch-all for everything else */
    /* Rotate div */
    /*
    -ms-transform: rotate(-8deg); /* IE 9 */
    /*-webkit-transform: rotate(-8deg); /* Chrome, Safari, Opera */
    /*transform: rotate(-8deg);*/
}
.sticky .title{
	font-size: 18px;
	font-weight: bold;
	font-family: 'Open Sans', sans-serif;
}
.sticky h1 {
    font-size: 0.5em;
    text-align: center;
    padding-bottom: 20px;
    } 
 
.sticky p {
    /*text-align: center;*/
    padding-bottom: 20px;
    font-family: 'Permanent Marker', cursive;
}

.sticky .note {
	border-top-style: solid;
	border-top-width: 1px;
	margin-top: 5px;
	border-top-color: khaki;
/*     font-family: 'Permanent Marker', cursive; */
/*     font-size: 18px; */
    padding: 8px 24px;
    
    font-weight: 300;
	font-family: 'Open Sans', sans-serif;
	font-size: 12px;
}

.sticky .footer{
	border-top-style: solid;
	border-top-width: 1px;
	margin-top: 20px;
	border-top-color: khaki;
	font-size: 12px;
	padding-top: 10px;
	font-family: 'Open Sans', sans-serif;
}

</style>

<!-- Panel principal de Observaciones -->
	
<h3 class="title">
	<span class="fa fa-fw fa-comment fa-lg"></span>&nbsp;<span id="title_grilla_observacion">Observaciones Destacadas</span>
</h3>

<div class="sticky" id="o_layout" style="display: none;">
	<div class="row">
		<span class="title"><span class="fa fa-fw fa-comment"></span>&nbsp;Observación&nbsp;<span id="o_tipo_layout"></span>
	</div>
	
	<div class="row note">
		<span id="o_texto_layout"></span>
	</div>
	
	<div class="row footer">
		<div class="col-lg-6">
			<strong>Usuario:&nbsp;</strong><span id="o_usuario_layout"></span>
		</div>
		<div class="col-lg-6">
			<strong>Fecha:&nbsp;</strong><span id="o_fecha_layout"></span>
		</div>
	</div>
</div>
<div class="sticky" id="o_sin_observaciones" style="display: none;">
	<div class="row">
		<span class="title"><span class="fa fa-fw fa-comment"></span>&nbsp;Sin Observaciones</span>
	</div>
	
	<div class="row note">
		<span>No se han registrado observaciones para el Legajo Digital actual.</span>
	</div>
</div>

<script type="text/javascript">
$j(document).ready(function(){

	jQuery.ajax({
		type: "POST",
		async: true,
		url: pathSistema + "/rest/LegajoDigitalAction.do?do=doObservacionVencimiento",
		data: {
			legajoDigital_id: filterLegajoDigital,
		},
		success: function(){

			jQuery.ajax({
		        type: "POST",
		        async: true,
		        url: "rest/ObservacionLegajoDigitalAction.do?do=doGet",
		        data: {
		        	"entidad": "ObservacionLegajoDigital",
		        	"campos": "tipo,causerK,fecha,texto",
		        	"filtro": "legajoDigital = " + filtroLegajoDigital + " AND mostrar = 1"
		      	},
		        success: function (response){
		            var responseJSON = JSON.parse(response);
		            var showDefault = true;
		            var i = 0;
		            for(t = 1; t<=4;t++){
		        		$j.each(responseJSON.data, function( key, value ) {

		            		if(value.tipo == t){
		        				i++;
		        				var tipoStr = getNameTipo(parseInt(value.tipo));
		        				var fechaAr = new Date(value.fecha);
		                		$j("#o_layout")
		                			.clone()
		                			.attr('id', 'o_layout_' + i)
		                			
		                			.find('#o_tipo_layout').attr('id', 'o_tipo_' + i).end()
		                			.find('#o_tipo_' + i).html(tipoStr).end()
		                			
		                			.find('#o_texto_layout').attr('id', 'o_texto_' + i).end()
		                			.find('#o_texto_' + i).html(value.texto).end()        			
		                			        			
		                			.find('#o_usuario_layout').attr('id', 'o_usuario_' + i).end()
		                			.find('#o_usuario_' + i).html(value.causerK).end()
		                			        			
		                			.find('#o_fecha_layout').attr('id', 'o_fecha_' + i).end()
		                			.find('#o_fecha_' + i).html(fechaAr.toLocaleDateString("es-AR")).end()        			
		                			
		                			.insertAfter('.sticky').last()
		                			.fadeIn();            		
		                		showDefault = false;              		
		               		}


		        		});
		            }
		    		if(showDefault == true){
		    			$j('#o_sin_observaciones').fadeIn();
		       		}
		    	},
		        error: function(data) { 
		    	}
		    });
			
			
			},
		error: function(){
			}
	});




	

})
</script>



