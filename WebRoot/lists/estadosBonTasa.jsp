<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title" style="margin-bottom: 30px">Historial de Estados</div>

  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="ProcessForm">
	<html:hidden property="process.cid" />
	<html:hidden property="process.action" styleId="accion" value="nuevo"/>
	
	<html:submit value="Nuevo Estado" onclick="nuevo();"/>	
	<br><br>
	
		<display:table name="ProcessForm" property="process.estados"
				defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.BonTasaEstado" export="true"
				requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">    	
			<display:column media="excel xml pdf" property="id" title="ID"/>	          
            <display:column title="Fecha Cambio" property="fechaCambio" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
            <display:column title="Estado" property="estado.nombreEstado" sortable="true"/>
            <display:column title="Asesor" property="asesor" sortable="true"/>            
    	</display:table>
</html:form>
<script type="text/javascript">
function activar(){
var action = document.getById('accion');
action.value= 'activar';
}
function baja(){
var action = document.getElementById('accion');
action.value= 'baja';
}
function nuevo(){
	var action = document.getById('accion');
	action.value= 'nuevo';
}


</script>