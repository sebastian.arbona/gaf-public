<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
    <div style="width:70%" align="left"><html:errors/></div>
    <logic:empty name="ProcessForm" property="process.boletos">
    El Cr�dito: ${ProcessForm.process.objetoi.numeroAtencion} no tiene Boletos Resumen.
    </logic:empty>
    <logic:notEmpty name="ProcessForm" property="process.boletos">
    	<display:table name="ProcessForm" property="process.boletos" id="boleto" 
    	export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">
    		<display:caption>Boletos de Pago del cr�dito: ${ProcessForm.process.objetoi.numeroAtencion} - ${ProcessForm.process.objetoi.linea.nombre} - ${ProcessForm.process.objetoi.persona.nomb12}</display:caption>
            <display:column title="Nro de Boleto" media="html" sortable="true">
                <a href="javascript:void(0);" onclick="imprimir(${boleto.objetoi.id}, ${boleto.id});">${boleto.numeroBoleto}</a>
            </display:column>
            <display:column title="Nro de Boleto" media="pdf excel xml" property="numeroBoleto"/>
            <display:column title="Verificador" property="verificadorBoleto" sortable="true"/>
            <display:column title="Fecha de Emisi�n" property="fechaEmisionCuota" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
			<display:column title="Nro Cuota" property="numeroCuota" sortable="true"/>            
            <display:column title="Fecha de Vto" property="fechaVencimientoStr" sortable="true"/>
<%--            <display:column title="Monto" property="importe" sortable="true"/>--%>
    	</display:table>
    </logic:notEmpty>   
</html:form>
<script language="javascript" type="text/javascript">
    function imprimir(id, idBoleto) 
    {
        window.location =         
        '${pageContext.request.contextPath}/actions/process.do?do=process' +
        '&processName=${param.processName}' +
        '&process.accion=imprimirBoleto' +
        '&process.objetoi.id='+id+
        '&process.boleto.id='+idBoleto;               
    }
</script>    