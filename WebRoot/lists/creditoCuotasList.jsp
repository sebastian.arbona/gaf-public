<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="write"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script language="JavaScript" type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<script type="text/javascript">
	$j = jQuery.noConflict();
	$j(document).ready(
			function() {
				$j("input[id^=ProcessForm\\.process\\.montos]").change(
						function() {
							var id = this.id;
							var indice = id.substring(id.indexOf("[") + 1,
									id.length - 1);
							var desembolsado = $j("#desembolsado").val();
							var valor = this.value.replace(',', '.');
							$j(
									"#ProcessForm\\.process\\.porcentajes\\["
											+ indice + "\\]").val(
									valor / desembolsado * 100);
						});
				$j("input[id^=ProcessForm\\.process\\.porcentajes]").change(
						function() {
							var id = this.id;
							var indice = id.substring(id.indexOf("[") + 1,
									id.length - 1);
							var desembolsado = $j("#desembolsado").val();
							var valor = this.value.replace(',', '.');
							$j(
									"#ProcessForm\\.process\\.montos\\["
											+ indice + "\\]").val(
									valor / 100 * desembolsado);
						});
			});
	function imputacion() {
		var form = $('formulario');
		var act = $('accion');
		var msj = "&iquest;Esta seguro de querer solicitar el pago de las cuotas seleccionadas?";
		if (confirmMsg(msj)) {
			act.value = "imputar";
			form.submit();
		}
	}
	function entregaPago() {
		var form = $('formulario');
		var act = $('accion');
		if (confirmMsg(msj)) {
			act.value = "entregarPago";
			form.submit();
		}
	}
	function actualizarCapital(indice) {
		var form = $('formulario');
		var act = $('accion');
		var monto = $('monto-' + indice).value.replace(',', '.');
		act.value = "actualizarCapital";
		$('indice').value = indice;
		$('capitalModificado').value = monto;
		form.submit();
	}
	function cancelar() {
		var form = $('formulario');
		var act = $('accion');
		act.value = "cancelar";
		form.submit();
	}
	function guardarPorcentajes() {
		var act = $('accion');
		act.value = "savePorcentajes";
		return true;
	}
	function nuevaCuota() {
		var url = '${pageContext.request.contextPath}/actions/process.do?'
				+ 'do=process&processName=NuevaCuotaProcess&process.idCredito='
				+ '${ProcessForm.process.idObjetoi}';
		window.location = url;
		return true;
	}
</script>

<div class="title" style="margin-bottom: 30px">Cronograma inicial
	de cuotas</div>
<html:form
	action="/actions/process.do?do=process&processName=CreditoCuotas"
	styleId="formulario">

	<html:hidden name="ProcessForm" property="process.action" value="save"
		styleId="accion" />
	<html:hidden name="ProcessForm" property="process.idObjetoi" />
	<html:hidden name="ProcessForm" property="process.desembolsado"
		styleId="desembolsado" value="${ProcessForm.process.desembolsado}" />
	<html:hidden name="ProcessForm" property="process.indice"
		styleId="indice" />
	<html:hidden name="ProcessForm" property="process.capitalModificado"
		styleId="capitalModificado" />

	<logic:notEmpty name="ProcessForm" property="process.cuotas">
		<a style="font-size: 16; position: absolute; left: 180; top: 32"
			href="actions/process.do?do=process&processName=CreditoCuotas&process.action=imprimirReporte&process.idObjetoi=${ProcessForm.process.idObjetoi}">
			Informe de Cuotas </a>
	</logic:notEmpty>
	<html:errors />

	<div class="grilla">
		<display:table name="ProcessForm" property="process.cuotas"
			id="reportTable">
			<display:setProperty name="report.title" value="Cuotas de Cr&eacute;dito"></display:setProperty>
			<display:column title="Cuota">
				<logic:notEqual value="0" name="reportTable" property="numero">
	    		${reportTable.numero}
	    	</logic:notEqual>
				<logic:equal value="0" name="reportTable" property="numero">
					<div style="font-size: 14; font-weight: bold">Total</div>
				</logic:equal>
			</display:column>
			<display:column title="Resumen">
				<a
					href="actions/process.do?do=process&processName=CreditoCuotas&process.action=imprimirBoletoResumen&process.nroComprobante=${reportTable.idBoletoResumen}&process.idCuota=${reportTable.id}">
					${reportTable.numeroBoletoResumen} </a>
			</display:column>
			<logic:equal name="ProcessForm" property="process.ocultarCampos"
				value="true">
				<display:column title="Fecha Vto. ">
					<html:hidden name="ProcessForm"
						property="process.ids[${reportTable_rowNum - 1}]" />
					<logic:notEqual value="0" name="reportTable" property="numero">
						<div style="width: 140px">
							<asf:calendar
								property="ProcessForm.process.fechas[${reportTable_rowNum - 1}]"
								maxlength="10" attribs=" style=\"width: 90px\" " readOnly="true"></asf:calendar>
						</div>
					</logic:notEqual>
				</display:column>
			</logic:equal>
			<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
				value="true">
				<display:column title="Fecha Vto. ">
					<html:hidden name="ProcessForm"
						property="process.ids[${reportTable_rowNum - 1}]" />
					<logic:notEqual value="0" name="reportTable" property="numero">
						<logic:equal name="ProcessForm"
							property="process.editables[${reportTable_rowNum - 1}]"
							value="true">
							<div style="width: 140px">
								<asf:calendar
									property="ProcessForm.process.fechas[${reportTable_rowNum - 1}]"
									maxlength="10" attribs=" style=\"width: 90px\" "
									readOnly="${ProcessForm.process.acuerdoPago}"></asf:calendar>
							</div>
						</logic:equal>
						<logic:equal name="ProcessForm"
							property="process.editables[${reportTable_rowNum - 1}]"
							value="false">
							<div style="width: 90px">
								<asf:calendar
									property="ProcessForm.process.fechas[${reportTable_rowNum - 1}]"
									maxlength="10" attribs=" style=\"width: 90px\" "
									readOnly="true"></asf:calendar>
							</div>
						</logic:equal>
					</logic:notEqual>
				</display:column>
			</logic:notEqual>
			<display:column property="importeCuota" title="Importe Cuota"
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"
				style="border: 10px; border-right-color: #0049FF;" />
			<display:column bgcolor="#939598">
			</display:column>
			<display:column title="Capital">
				<logic:equal name="ProcessForm" property="process.ocultarCampos"
					value="true">
					<asf:text
						property="ProcessForm.process.montos[${reportTable_rowNum - 1}]"
						type="decimal" maxlength="15" attribs="size=10px" readonly="true" />
				</logic:equal>
				<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
					value="true">
					<logic:equal name="ProcessForm"
						property="process.editables[${reportTable_rowNum - 1}]"
						value="true">
						<asf:text
							property="ProcessForm.process.montos[${reportTable_rowNum - 1}]"
							type="decimal" maxlength="15"
							attribs="onchange=actualizarCapital(${reportTable_rowNum - 1}); size=10px"
							readonly="${ProcessForm.process.acuerdoPago}"
							id="monto-${reportTable_rowNum - 1}" />
					</logic:equal>
					<logic:equal name="ProcessForm"
						property="process.editables[${reportTable_rowNum - 1}]"
						value="false">
						<asf:text
							property="ProcessForm.process.montos[${reportTable_rowNum - 1}]"
							type="decimal" maxlength="15" attribs="size=10px" readonly="true" />
					</logic:equal>
				</logic:notEqual>
			</display:column>
			<display:column title="Porcentaje">
				<logic:equal name="ProcessForm" property="process.ocultarCampos"
					value="true">
					<asf:text
						property="ProcessForm.process.porcentajes[${reportTable_rowNum - 1}]"
						type="decimal" maxlength="15" attribs=" style=\"width: 60px\" "
						readonly="true" />
				</logic:equal>
				<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
					value="true">
					<logic:equal name="ProcessForm"
						property="process.editables[${reportTable_rowNum - 1}]"
						value="true">
						<asf:text
							property="ProcessForm.process.porcentajes[${reportTable_rowNum - 1}]"
							type="decimal" maxlength="15" attribs=" style=\"width: 60px\" "
							readonly="${ProcessForm.process.acuerdoPago}" />
					</logic:equal>
					<logic:equal name="ProcessForm"
						property="process.editables[${reportTable_rowNum - 1}]"
						value="false">
						<asf:text
							property="ProcessForm.process.porcentajes[${reportTable_rowNum - 1}]"
							type="decimal" maxlength="15" attribs=" style=\"width: 60px\" "
							readonly="true" />
					</logic:equal>
				</logic:notEqual>
			</display:column>
			<display:column property="compensatorio" title="Int. Compens."
				decorator="com.asf.cred.decorator.SeparatorDoubleDecorator" />
			<display:column property="bonificacion" title="Bonificaci&oacute;n"
				decorator="com.asf.cred.decorator.NegativeDoubleDecorator" />
			<display:column title="Multas" width="55px">-</display:column>
			<display:column title="Gastos" width="55px">-</display:column>
		</display:table>
	</div>
	<table style="margin-top: 30px">
		<tr>
			<th>Desembolsado:</th>
			<td><html:text name="ProcessForm"
					property="process.desembolsadoStr" styleId="desembolsado"
					readonly="true" /></td>
		</tr>
	</table>

	<div style="text-align: center; margin-top: 20px">
		<logic:equal name="ProcessForm" property="process.ocultarCampos"
			value="true">
			<asf:security action="/actions/guardarCuotas.do" access="">
				<html:submit disabled="true">Guardar</html:submit>
			</asf:security>
			<input type="button" value="Cancelar" onclick="cancelar();"
				disabled="disabled">
		</logic:equal>
		<logic:notEqual name="ProcessForm" property="process.ocultarCampos"
			value="true">
			<asf:security action="/actions/guardarCuotas.do" access="">
				<html:submit>Guardar</html:submit>
				<html:submit onclick="guardarPorcentajes();">Guardar porcentajes</html:submit>
			</asf:security>
			<input type="button" value="Cancelar" onclick="cancelar();" />
			<input type="button" value="Nueva Cuota" onclick="nuevaCuota();" />
		</logic:notEqual>

	</div>

	<logic:notEmpty name="ProcessForm" property="process.prorrogas">
		<h2 style="magin-top: 20px">Pr&oacute;rrogas</h2>
		<display:table name="ProcessForm" property="process.prorrogas"
			id="reportTable">
			<display:column title="Numero de cuota" property="cuota.numero" />
			<display:column title="Fecha de vencimiento original"
				property="cuota.fechaVencimientoOriginal"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Fecha de vencimiento prorrogado"
				property="cuota.fechaVencimiento"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Numero de resolucion" property="resolucion" />
			<display:column title="Observaciones" property="observaciones" />
			<display:column title="Usuario" property="usuario.id" />
		</display:table>
	</logic:notEmpty>
</html:form>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
