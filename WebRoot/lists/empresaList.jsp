<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administración de Empresas</div>
<br><br>

<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	<logic:empty name="ListResult" property="result">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
	</logic:empty>
</asf:security>

<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" defaultsort="1" id="reportTable">

    <display:column property="nomb01" title="Nombre" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.id" paramProperty="id"/>

    <display:column property="domi01" title="Domicilio"/>
    <display:column property="cuit01" title="C.U.I.T."/>

<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id">Eliminar</display:column>
</asf:security>

</display:table>
</div>

<!-- Alejandro Bargna -->
