<br><br><%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<script language="javascript" type="text/javascript">
    function newEntity(){
        window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}";
    }
</script>
<div class="title">Administración de Usuarios</div>
<br><br>
<html:form action="/actions/abmAction.do?filter=true&entityName=${param.entityName}">
	<table border="0">
		<tr>
			<th>Nombre de Usuario:</th>
			<td>
				<html:text property="paramValue[0]" value="${paramValue[0]}"/>
				<html:hidden property="paramName[0]" styleId="paramName" value="id" />
    			<html:hidden property="paramComp[0]" styleId="paramComp" value=" like "/>
    			<html:hidden property="filter" value="true"/>
    			<html:hidden property="do" styleId="do" value="list"/>
  			</td>
  		</tr>
	</table> 
	<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
		<button type="button" onclick="javascript:newEntity();"><bean:message key="abm.button.new"/></button>
	</asf:security>
    <html:submit>Consultar</html:submit>
</html:form>

<br/><br/>

<display:table name="ListResult" property="result" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}"  pagesize="50">
    <display:setProperty name="report.title" value="Administración de Usuarios"></display:setProperty>  
    <display:column property="id" title="Usuario" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" sortable="true"  />
    <display:column property="nombre" title="Nombre" sortable="true" />
    <display:column property="activo" title="Activo" sortable="true" decorator="com.asf.displayDecorators.BooleanDecorator" />
    <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
		<display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id" >Eliminar</display:column>
	</asf:security>
</display:table>