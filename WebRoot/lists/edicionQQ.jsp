<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>



<div class="title">Modificaci&oacute;n de datos INV</div>
<br><br>
<div class="grilla">
<display:table name="ProcessForm" property="process.detalles" defaultsort="1" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process">

    <display:column property="codigoVariedad" title="C�digo Variedad" sortable="true"/>
    <display:column property="nombreVariedad" title="Nombre Variedad" sortable="true"/>
    <display:column property="hectareas" title="Hectareas" sortable="true"/>
    <display:column property="qq" title="QQ" sortable="true"/>
    
	<display:column media="html" href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=EdicionQQ&process.action=editar&process.cid=${ProcessForm.process.cid}" paramId="process.idDetalle" paramProperty="id" title="Modificar">
		Modificar
	</display:column>
</display:table>
<br/><br/>

<input type="button" value="Volver" onclick="volver();"/>
</div>

<script type="text/javascript">
function volver() {
	var url = '${pageContext.request.contextPath}/actions/process.do?processName=VinedoProcess&do=process' +
		'&process.idSolicitud=${ProcessForm.process.idObjetoi}';
	window.location = url;
}
</script>