<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administración de Directores</div>
<br><br>

<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>

<div class="grilla">
<display:table name="ListResult" property="result" defaultsort="1" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
    <display:column property="codigo" title="Código" sortable="true" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.id" paramProperty="id"/>

    <display:column property="nombre" title="Nombre" sortable="true"/>
    <display:column property="descripcion" title="Descripción" sortable="true"/>
    <display:column property="valor" title="Valor" sortable="true"/>

<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id" paramProperty="id">Eliminar</display:column>
</asf:security>

</display:table>
</div>


