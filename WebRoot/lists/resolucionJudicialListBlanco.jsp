
<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet" />
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet" />


<body class=" yui-skin-sam">
	<div class="title">
		Administración de Resoluciones
	</div>
	<br>
	<br>
		
<div><html:errors /></div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js">
    </script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js">
</script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js">
</script>

	<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
	
		<html:hidden property="process.accion" styleId="accion" value="nuevo" />
		<html:hidden property="process.sid"/>
		<html:hidden property="process.idproyectoPop"/>	

	</html:form>
	<br>
	<br>
	<display:table name="ProcessForm" property="process.resoluciones" export="true" id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar">
		<display:setProperty name="report.title"
			value="Administración de Resoluciones"></display:setProperty>

		 <display:column title="Nro Resolucion" media="html" sortable="true">
			  ${reportTable.numero}
		</display:column>
		<display:column property="proyecto.numeroAtencion" title="Proyecto" sortable="true" />
		<display:column property="proyecto.persona.nomb12" title="Titular" sortable="true" />
		<display:column property="especialista.persona.nomb12" title="Abogado" sortable="true" />	
		<display:column property="tipoResolucion.nombre" title="Proceso" />
		<display:column media="excel xml pdf" property="numero"
			title="Nro Resolucion" />
		<display:column property="fechaStr" title="Fecha" />
		<display:column property="observaciones" title="Observación" />
		<display:column title="Instancias" media="html">
				<a href="javascript: instancias( ${reportTable.id} );">Administrar</a>
		</display:column>
	</display:table>

	<script type="text/javascript">

function load(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=load&';
	url += 'process.idSolicitud=' + id;

	window.location = url;
}// fin modificar.-

function show(id) {
	var url = '';

	url += '${pageContext.request.contextPath}/actions/process.do?';
	url += 'do=process&';
	url += 'processName=${param.processName}&';
	url += 'process.accion=show&';
	url += 'process.resolucion.id=' + id;

	window.location = url;
}
	
function instancias(id){
		var url = '';
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=listarInstanciasFiltroInicio&';
		url += 'process.idSolicitud=' + id;

		window.location = url;
}


function documentoResolucion(id){
		var url = '';
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&';
		url += 'processName=${param.processName}&';
		url += 'process.accion=listarDocumentosResolucion&';
		url += 'process.idSolicitud=' + id;
		
		window.location = url;
}
</script>