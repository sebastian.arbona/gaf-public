<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>

<div id="table-container" class="contenedor-dtable grilla-datatable">
	<asf:security action="/actions/process.do"
		access="do=process&processName=LegajoDigital&process.accion=doPost">
		<button type="button" class="btn btn-default new-usuario"
			id="addElement" onclick="add_LegajoDigital();">
			<i class="fa fa-user-plus" aria-hidden="true"></i> Nuevo Legajo
		</button>
	</asf:security>

	<div class="contenedor-dtable grilla-datatable style-datatable">
		<table id="dt-legajoDigital" class="table table-hover user-list"
			cellspacing="0">
		</table>
	</div>
</div>

<script type="text/javascript">
	$j(document).ready(
			function() {
				dTable_legajoDigital = inicializarDatatable("dt-legajoDigital",
						6, null, dt_personalizacion_legajoDigital);
			})
</script>



