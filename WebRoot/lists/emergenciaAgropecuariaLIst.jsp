<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

 <script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>

<div class="title">
	Emergencia Agropecuaria
</div>

<html:form action="/actions/process.do?do=process&processName=EmergenciaAgropecuariaProcess">
	<html:hidden property="process.action" value="search"/>
	<table>
		<tr>
			<th style="padding-left: 41px; padding-right: 41px">Persona:</th>
			<td>
				<asf:autocomplete name="process.idPersona" tipo="AJAX" idDiv="autocomplete_persona"
                                  idInput="idpersona" classDiv="autocomplete"
                                  origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
                                  options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
                                  attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'"/>
                                  <span id="indicadorPersona" style="display: none"><img src="images/loader.gif" alt="Trabajando..." /></span>
                                  <input value="..." onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=paramValue&amp;entityName=Persona&amp;title=Selección de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');" type="button">&nbsp;
                                  (Código - Apellido y Nombre - Número de Documento)

			</td>
		</tr>
	</table>
	
	<input type="submit" value="Consultar"/>
	
	<display:table name="ProcessForm" property="process.creditosEmergencia" id="credito">
   		<display:column title="Crédito Nº" property="numeroAtencion"/>
        <display:column title="Línea" property="linea.nombre"/>
        <display:column title="Expediente Nº" property="expediente"/>
        <display:column title="Estado" property="estadoActual.estado.nombreEstado"/>
    </display:table>
    
    <html:submit>Generar Acuerdo de Pago</html:submit>
</html:form>