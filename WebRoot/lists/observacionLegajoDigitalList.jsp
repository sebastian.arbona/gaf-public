<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div id="table-container" class="contenedor-dtable grilla-datatable">
	
	<h3 class="title">
		<span class="fa fa-fw fa-comment fa-lg"></span>&nbsp;<span id="title_grilla_observacion"></span>
	</h3>
	
	<asf:security action="/actions/process.do"
	access="do=process&processName=ObservacionLegajoDigital&process.accion=doPost">		
				
		<button type="button" class="btn btn-default new-usuario" id="addElement" onclick="add_ObservacionLegajoDigital();">
			<i class="fa fa-user-plus" aria-hidden="true"></i> Nueva Observaci&oacute;n
		</button>
		
		<button type="button" class="btn btn-default" id="btnDescargar" onclick="Descarga();">
			<i class="dtBotones dtDownload" aria-hidden="true"></i>&nbsp;Descargar
		</button>

	</asf:security>
	

			
	<div class="contenedor-dtable grilla-datatable style-datatable">	
		<table id="dt-observacionLegajoDigital" class="table table-hover user-list" cellspacing="0">
        </table>
    </div>
</div>
<script type="text/javascript">
$j(document).ready(function(){
	format_dt_observacionLegajoDigital();
	dTable_observacionLegajoDigital = inicializarDatatable( "dt-observacionLegajoDigital", 1, null, dt_personalizacion_observacionLegajoDigital );
})
</script>



