<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />
    <br/>   
	<br/>
    <logic:empty name="ProcessForm" property="process.bonificaciones">
    La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no tiene Bonificaciones Externas.
    </logic:empty>
     <a href="javascript:void(0);" onclick="nueva();">
		 Nueva Bonificaci�n de Tasa;
    </a>
    
    <logic:notEmpty name="ProcessForm" property="process.bonificaciones">
    	<display:table name="ProcessForm" property="process.bonificaciones" id="bonificacion">
    		<display:caption>Bonificaciones Externas de:  ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12}</display:caption>
            <display:column title="ID" media="html" sortable="true" >
                <a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=BonTasaProcess&process.idBonTasa=${bonificacion.id}&process.accion=ver&process.idPersona=${process.idPersona}&process.esPersona=${process.esPersona}" >${bonificacion.id}</a>
            </display:column>
            <display:column title="Nro Bonificaci�n" property="numeroBonificacion" sortable="true"/>
            <display:column title="Expediente N�" property="expediente" sortable="true"/>
            <display:column title="Fecha Contrato" property="fechaMutuo" sortable="true"/>
            <display:column title="Destino de Fondos" property="objeto" sortable="true"/>
            <display:column title="Estado" property="tipoEstadoBonTasa" sortable="true" />
    	</display:table>
    </logic:notEmpty>   
</html:form>

	
<script language="javascript" type="text/javascript">
    
    function nueva(){
				window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=NuevaBonTasa&process.idPersona=${ProcessForm.process.idPersona}&process.accion=nueva&process.esPersona=${ProcessForm.process.esPersona}";
				
			}
</script>    