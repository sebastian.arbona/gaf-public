<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

<div class="title">
	Titulares de Credito
</div>
<br>
<br>
	<logic:empty name="ProcessForm" property="process.titulares" >
        La Bonificacion de Tasa no tiene titulares vinculados
    </logic:empty>
<html:form 
	action="actions/process.do?processName=${param.processName}&do=process"
	styleId="formulario">
	<h3><html:errors /></h3><ul>
		<tr>
		<b>C�digo de Persona:</b>
		<asf:selectpop name="process.idPersona" title="Persona" 
			columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
			captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
			entityName="com.civitas.hibernate.persona.Persona" value="" onChange="selecPersona();"/>
		<input type="button" value="Agregar" onclick="Agregar();"/>
		</tr>
		<display:table name="ProcessForm" property="process.titulares"
				defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.TitularesBonTasa" export="true" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=TitularesBonTasa&process.accion=listar">	
				<display:column media="excel xml pdf" property="id" title="ID"/>
				<display:column property="persona.nomb12" title="Nombre y Apellido/ Razon Social"/>
			    					
				<display:column  property="persona.cuil12" title="CUIL" />
				<display:column media="html" honClick="return confirmDelete();" title="Eliminar"
	      href="${pageContext.request.contextPath}/actions/process.do?processName=TitularesBonTasa&do=process&process.accion=eliminar&process.idBonTasa=${ProcessForm.process.idBonTasa}"
	      paramId="process.idTitular" paramProperty="id">
	      <bean:message key="abm.button.delete" />
	    </display:column>    
				
		  		
		</display:table>
		</html:form>
<script type="text/javascript">
var persona=false;
function selecPersona(){
	persona= true;
}

function Agregar(){
	var idPer = document.getElementById('process.idPersona').value;
	window.location="${pageContext.request.contextPath}/actions/process.do?&do=process&processName=TitularesBonTasa&process.accion=Agregar&process.idBonTasa=${ProcessForm.process.idBonTasa}&process.idPersona="+idPer;
}
</script>