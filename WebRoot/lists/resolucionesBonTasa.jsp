<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<script language="JavaScript" type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.3.2.min.js"></script>


<div class="title" style="margin-bottom: 30px">Datos de Resolución</div>

  <html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="ProcessForm">
	<html:hidden property="process.cid"  styleId="cid"/>
	<html:hidden property="process.idBonTasa" styleId="idBonTasa"/>
	
	<br><br>
		
	<display:table name="ProcessForm" property="process.estados"
			defaultsort="1" id="reportTable" class="com.nirven.creditos.hibernate.BonTasaEstado" export="true"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">    	
		   <display:column property="id" title="ID"/>
		   <display:column title="Tipo" property="tipoResolucion.texto" sortable="true"/>	          
           <display:column title="Numero" property="numeroResolucion" sortable="true"/>
           <display:column title="Fecha" property="fechaResolucion" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
           <display:column title="Ubicación Exp." property="ubicacionExpediente" sortable="true"/> 
           <display:column title="Archivos" media="html">
				<a href="javascript: documentoResolucion( ${reportTable.id} );">Administrar</a>
		   </display:column>     
   	</display:table>
</html:form>


<script type="text/javascript">
$j = jQuery.noConflict();
function documentoResolucion(id){
		var url = '';
        var idBonTasa =getElem('idBonTasa').value;
		url += '${pageContext.request.contextPath}/actions/process.do?';
		url += 'do=process&processName=ResolucionesBonTasa';
		url += '&process.estado_id='+ id + '&process.accion=listResolucionArchivo&process.idBonTasa='+idBonTasa;
		window.location = url;
}
</script>