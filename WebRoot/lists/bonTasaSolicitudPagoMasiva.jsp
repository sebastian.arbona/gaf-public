<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Solicitud de Pago Masiva</div>
<html:form action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">
  	<html:hidden name="ProcessForm" property="process.accion" styleId="accion"/>
  	<br/>  	
  	<table>
  		<tr>
	  		<th>Agente Financiero</th>
	  		<td>
	  			<asf:select name="process.idEnte"
				listValues="codiBa" listCaption="codiBa,detaBa"
				entityName="com.asf.gaf.hibernate.Bancos"
				value="${ProcessForm.process.idEnte}"  
				nullValue="true"					
				nullText="&lt;&lt;Todos&gt;&gt;" 
				filter="financiador=true"/>
			</td>
	    </tr>
	    <tr>
  			<th>Fecha</th>
    		<td><asf:calendar property="ProcessForm.process.fechaStr" value="${ProcessForm.process.fechaStr}" /></td>
  		</tr>
  	</table>
  	<br/>
  	<input type="button" value="Listar" onclick="listar();">
  	<br/>
  	<br/>
	<logic:empty name="ProcessForm" property="process.bonificaciones">
    	No hay Bonificaciones de Tasa
    	<br/>
    </logic:empty>
    <logic:notEmpty name="ProcessForm" property="process.bonificaciones">
    	<input type="button" value="Solicitar Imputaci�n GAF" name="ProcessForm" onclick="solicitarImputacion();">
    	<br/>
    	<br/>
    	<display:table name="ProcessForm" property="process.bonificaciones" id="bonificacion">    		
            <display:column title="ID" property="id" sortable="true" />
            <display:column title="Nro Bonificaci�n" property="numeroBonificacion" sortable="true"/>
            <display:column title="Expediente N�" property="expediente" sortable="true"/>
            <display:column title="Fecha Contrato" property="fechaMutuo" sortable="true"/>
            <display:column title="Destino de Fondos" property="objeto" sortable="true"/>
          	<display:column title="Agente Financiero" property="banco.detaBa" sortable="true"/>
            <display:column title="Estado" property="estadoActual.tipoEstadoBonTasa" sortable="true" />
            <display:column title="Importe Cuotas" property="importeSolicitudPagoMasiva" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
            <display:column title="Subsidio" property="subsidioSolicitudPagoMasiva" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>            
            <display:column title="Selecci�n">
				<input type="checkbox" name="bonTasa-${bonificacion.id}" value="${bonificacion.id}" />
			</display:column>
    	</display:table>
    </logic:notEmpty>   
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:fixed; top:-500px; left:-500px;"></iframe>
<script language="javascript" type="text/javascript">
		
	function listar(){
		var form = $('oForm');
		var accion = $('accion');
		accion.value = 'listar';
		form.submit();
	}
		
	function solicitarImputacion(){
		var msj = '�Esta seguro de querer solicitar el pago de las bonificaciones seleccionadas?';
    	if(confirm(msj)){
			var form = $('oForm');
			var accion = $('accion');
       		accion.value = 'solicitarImputacion';
			form.submit();
       	}
	}
<%--	function entregarPago(){--%>
<%--		var form = document.getElementById("oForm");--%>
<%--		var accion = document.getElementById("accion");	--%>
<%--	  	accion.value = "entregarPago";--%>
<%--		form.submit();	--%>
<%--	}--%>
</script>    