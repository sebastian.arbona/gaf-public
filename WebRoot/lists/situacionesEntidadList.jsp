<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<html:form action="/actions/process.do?do=process&processName=${param.processName}">
    <html:hidden property="process.accion" value="nuevo"/>
    <html:hidden property="process.persona.id" value="${ProcessForm.process.persona.id}"/>
    <div style="width:70%" align="left"><html:errors/></div>
	<asf:security action="/actions/process.do" access="do=process&processName=${param.processName}&process.accion=nuevo">
		<html:submit><bean:message key="abm.button.new"/></html:submit>
	</asf:security>
	<br><br>
    <logic:notEmpty name="ProcessForm" property="process.situaciones" >
        <display:table name="ProcessForm" property="process.situaciones" pagesize="5" id="reportTable" export="true"
                       requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=listar" >
            <display:caption>Situaci�n frente a Entidades Financieras de:  ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12}</display:caption>
            <display:column title="Entidad" property="categoria.entidad.nombreEntidad" sortable="true"/>
            <display:column title="Categor�a" property="categoria.nombreCategoria" sortable="true"/>
            <display:column title="Fecha" property="fechaDesdeStr" sortable="true"/>
            <display:column title="Observaciones" property="observaciones" sortable="true"/>
        </display:table>
    </logic:notEmpty>
    <logic:empty name="ProcessForm" property="process.situaciones" >
        La Persona : ${ProcessForm.process.persona.idpersona} - ${ProcessForm.process.persona.nomb12} no est� registrada en ninguna Entidad Financiera.
    </logic:empty>
</html:form>

<script language='javascript'>
Event.observe(window, 'load', function() 
{
	this.name = "Situaci�n Entidades";
	parent.pop(this);
});
</script>