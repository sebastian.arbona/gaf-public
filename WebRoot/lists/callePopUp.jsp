<%@ page language="java"%>
<%@ page import="java.util.List" %>
<%@ page import="com.asf.util.Constants" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

<html> 
    <head>
    <title>Selecci�n de Calles</title>
        
        <script language="JavaScript">
        var navegador=navigator.appName;
            
            function carga(){
                if(document.getElementById('calle').value=='' && document.getElementById('codigo').value=='')
                    document.getElementById('codigo').value='%';
            }
            
            function actualizar(control, valor){
                if (window.opener){
                    if(navegador=='Netscape'){
                        var e = window.opener.document.getElementById( control );
                        e.value = valor;
                        if(e.onchange){
                            e.onchange();
                        }
                    }else{
                        var e = window.opener.document.all[control];
                        e.value=valor;
                        if(e.onchange){
                            e.onchange();
                        }
                    }
                    window.close();
                }
            } 

            function setFocus(){
            	document.CallePopUpForm.calle.focus();
            }
        </script>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/style/displaytag.css" type="text/css">
    </head>
    <body onload="setFocus();">
    <center>
    <h3><u>Selecci�n de Calles</u></h3>
    <html:form action="/actions/callePopUp.do?do=list&name=${param.name}&property=${param.property}">
    <table border="0">
    <tr><th>Nombre de Calle:</th><td><html:text property="calle" styleId="calle"/></td>
    <tr><th>C�digo de Calle:</th><td><html:text property="codigo" styleId="codigo"/></td>
    </table>
    <br>
    <div align="center"><html:submit onclick="carga();">Buscar</html:submit>
    <input type="button" onclick="window.close();" value="Cancelar">
    </html:form>
    <br><br>
	<div class="grilla">
	<display:table name="CallePopUpForm" pagesize="100" property="calleList" export="false" id="calles"
		requestURI="${pageContext.request.contextPath}/actions/callePopUp.do?do=list&property=${param.property}&filter=null">
	    <display:column title="C�digo">
	    	<a href="javascript://" onclick="actualizar('${CallePopUpForm.name}Descriptor','${calles[1]}');actualizar('${CallePopUpForm.name}Codigo','${calles[0]}');actualizar('${CallePopUpForm.property}','${calles[2]}');">
	    	${calles[0]}</a>
	    </display:column>
	    <display:column title="Calle" sortable="true">${calles[1]}</display:column>
	</display:table>

	</div>
    </center>
    </body>
</html>
