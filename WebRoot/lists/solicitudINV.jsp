 <%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@page import="com.asf.cred.business.inv.AccionINV"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<link rel="stylesheet" href="${pageContext.request.contextPath}/style/autocomplete.css" type="text/css">
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css" rel="stylesheet"/>
<link type="text/css" href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css" rel="stylesheet"/>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>
<script language="javascript" type="text/javascript">
	/**
     * @inputIn: control sobre el input que dispara la consulta ajax.-
     * @defecto: para parametro=valor que tiene,
     * a la izquierda del igual el id del input que dispara la consulta ajax.-
     * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
     */
    function actualizarLista( inputIn, defecto )
    {
    	var urlDinamica = "";

    	urlDinamica += "&persona=" + inputIn.value;
    	urlDinamica += "&" + defecto;

		return urlDinamica;
  }
</script>

<div class="title">
	${ProcessForm.process.titulo}
</div>
<br>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<br>
<html:form
	action="actions/process.do?processName=${param.processName}&do=process"
	styleId="formulario">

	<html:hidden property="process.INV" value="${ProcessForm.process.INV}"
		styleId="inv" />

	<html:hidden property="process.accionStr"
		styleId="accion" />
	
	<html:hidden property="process.accionReal"
		styleId="accionReal" />
		
	<html:hidden property="process.accionAnterior" 
		styleId="accionAnterior" />

	<html:hidden property="process.idPersona"
		styleId="idPersonaBusqueda" />
			
	<logic:equal name="ProcessForm" property="process.accionStr"
		value="<%= AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()  %>"> 

		<table class="tabla">
			<tr>
				<th colspan="2">
					Verificaci�n Realizada
				</th>
			</tr>
			<tr>
				<th>
					Desde:
				</th>
				<td>
					<asf:calendar property="process.desdeStr"
						value="${ProcessForm.process.desdeStr}" />
				</td>
			</tr>
			<tr>
				<th>
					Hasta:
				</th>
				<td>
					<asf:calendar property="process.hastaStr"
						value="${ProcessForm.process.hastaStr}" />
				</td>
			</tr>
		</table>
	</logic:equal>

	<logic:equal name="ProcessForm" property="process.accionStr"
		value="<%= AccionINV.LISTAR_VINEDOS_PENDIENTE.toString()  %>">
		<asf:security
			action="/actions/process.do?do=save&processName=${param.processName}">
			<button type="button" onclick='javascript: crear();'>
				Solicitar Verificaci�n
			</button>
		</asf:security>
	</logic:equal>

<%--	<logic:equal name="ProcessForm" property="process.accionStr"--%>
<%--		value="<%= AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()  %>">--%>
<%----%>
<%--		<button type="button" onclick='javascript: listarInformados();'>--%>
<%--			Consultar--%>
<%--		</button>--%>
<%--	</logic:equal>--%>

	<iframe width=174 height=189 name="gToday:normal:agenda.js"
		id="gToday:normal:agenda.js"
		src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
		scrolling="no" frameborder="0"
		style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>

	<table>
    	<tr>
    		<th style="padding-left: 41px; padding-right: 41px" >Persona:</th>
    		<td>
            	<asf:autocomplete name="" tipo="AJAX" idDiv="autocomplete_persona"
                                  idInput="idpersona" classDiv="autocomplete" 
                                  origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
                                  options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
                                  attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de documento'"/>
                                  <span id="indicadorPersona" style="display: none"><img src="images/loader.gif" alt="Trabajando..." /></span>
<%--                                  <input value="..." onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=paramValue&amp;entityName=Persona&amp;title=Selecci�n de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C%F3digo%2C+Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');" type="button">&nbsp;--%>
                                  (C�digo - Apellido y Nombre - N�mero de Documento)
            </td>
        </tr>
    </table>
	<input type="button" value="Consultar" onclick="consultar();">
	<br/>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.vinedos"
			export="true" id="reportTable" class="com.nirven.creditos.hibernate.objetoiVinedo"
			requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}"
			width="100%">

			<display:setProperty name="report.title"
				value="Solicitud de Verificaci�n T�cnica Para el INV" />
				<display:column media="excel xml pdf" property="id" title="ID"/>
			<display:column property="credito.persona.nomb12" title="Titular" sortable="true"
				align="left" />
			<display:column property="credito.numeroAtencion" title="Nro Solicitud" sortable="true"
				align="left" />
			<display:column property="vinedo.codigo" title="C.I.V." sortable="true"
				align="center" />
		   <display:column property="vinedo.hectareas" title="Has"
				sortable="true" align="right" />
			<display:column property="qqEstimados" title="QQ Estimados"
				sortable="true" align="right" />
			<display:column property="vinedo.departamento" title="Departamento"
				sortable="true" align="left" />
			<display:column property="vinedo.localidad.nombre" title="Localidad"
				sortable="true" align="left" />
<%--			<logic:equal value="true" property="process.min" name="ProcessForm">--%>
<%--			<display:column property="gtiaMin" title="Deposito en Garantia Minimo"--%>
<%--				sortable="true" align="left" />	--%>
<%--			</logic:equal>--%>
			
			<display:column property="tieneGarantia" title="Garantia"
				sortable="true" align="left" />				
			
			<logic:equal name="ProcessForm" property="process.accionStr"
				value="<%= AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()  %>">
				<display:column property="fechaInformeINVStr"
					title="Fecha Informe" sortable="true" align="center" />
				<display:column title="QQ Aprobados (INV)" property="qqAprobado"
					sortable="true" align="right" />
				<display:column title="Observaci�n (INV)" property="observacionINV"
					sortable="true" align="left" />
			</logic:equal>
			<logic:equal name="ProcessForm" property="process.accionStr"
				value="<%= AccionINV.LISTAR_VINEDOS_SOLICITADO.toString()  %>">
				<logic:equal name="ProcessForm" property="process.INV" value="1">
					<display:column property="fechaSolicitadoStr"
						title="Fecha Solicitud" sortable="true" align="center" />
					<display:column property="credito.expediente" title="Expediente" sortable="true"
						align="left" />
					<display:column title="Evaluar" align="center">
						<asf:security
							action="/actions/process.do?do=save&processName=${param.processName}">
							<a
								href="javascript:evaluar('${reportTable.id}', '${reportTable.vinedo.codigo}', '${reportTable.vinedo.hectareas}', '${reportTable.qqEstimados}', ' ${reportTable.qqAprobado}', ' ${reportTable.observaciones}','${reportTable.observacionINV}' );">Ingresar</a>
						</asf:security>
					</display:column>
				</logic:equal>
			</logic:equal>
			<logic:equal name="ProcessForm" property="process.accionStr"
				value="<%= AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE.toString()  %>">
				<logic:equal name="ProcessForm" property="process.INV" value="0">
					<display:column title="Revisar" align="center">
						<a
							href="javascript:evaluar('${reportTable.id}', '${reportTable.vinedo.codigo}', '${reportTable.vinedo.hectareas}', '${reportTable.qqEstimados}', ' ${reportTable.qqAprobado}', ' ${reportTable.observaciones}','${reportTable.observacionINV}' );">Revisar</a>
					</display:column>
				</logic:equal>
			</logic:equal>
			<logic:equal name="ProcessForm" property="process.accionStr"
				value="<%= AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()  %>">
				<logic:equal name="ProcessForm" property="process.INV" value="0">
					<display:column title="Revisar" align="center">
						<a
							href="javascript:evaluar('${reportTable.id}', '${reportTable.vinedo.codigo}', '${reportTable.vinedo.hectareas}', '${reportTable.qqEstimados}', '${reportTable.qqAprobado}', '${reportTable.observaciones}','${reportTable.observacionINV}' );">Verificar	datos</a>
					</display:column>
				</logic:equal>
			</logic:equal>
		</display:table>
	</div>
	<logic:equal name="ProcessForm" property="process.accionStr"
		value="<%= AccionINV.LISTAR_VINEDOS_SOLICITADO.toString()  %>">
		<logic:equal name="ProcessForm" property="process.INV" value="1">
			<div class="title">
				Ingrese Quintales Aprobados para el Vi�edo Seleccionado
			</div>
			<table class="tabla">
				<tr>
					<th>
						Id Vi�edo:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.idVinedo"
							type="Long" maxlength="100" readonly="true"
							value="${process.idVinedo}" id="idVinedo" />
					</td>
				</tr>
				<tr>
					<th>
						C.I.V.:
					</th>
					<td>
						<html:text property="process.codigo" value="0" styleId="codigo"
							disabled="true" />
					</td>
				</tr>
				<tr>
					<th>
						Hectareas:
					</th>
					<td>
						<html:text property="process.hectareas" value="0"
							styleId="hectareas" disabled="true" />
					</td>
				</tr>
				<tr>
					<th>
						QQ Estimados:
					</th>
					<td>
						<html:text property="process.estimados" value="0"
							styleId="estimados" disabled="true" />
					</td>
				</tr>
				<tr>
					<th>
						QQ Aprobados: ${process.qq}
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.qq" type="Long"
							maxlength="10" value="${process.qq}" readonly="false" id="qq"/>
					</td>
				</tr>
				<tr>
					<th>
						Observaciones FTyC:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.observacion"
							type="textarea" maxlength="250" value="${process.observacion}" 
							readonly="true"	/>
					</td>
				</tr>
				<tr>
					<th>
						Observaciones INV:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.observacionINV"
							type="textarea" maxlength="250" value="${process.observacionINV}"
							readonly="false" />
					</td>
				</tr>
			</table>
			<button type="button" onclick="javascript: aprobar();">
				Aprobar
			</button>
			<html:submit onclick="return rectificacion();"
				value="Datos err�neos, solicitar rectificaci�n" />
		</logic:equal>
	</logic:equal>
	<logic:equal name="ProcessForm" property="process.accionStr"
		value="<%= AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()  %>">
		<logic:equal name="ProcessForm" property="process.INV" value="0">
<%--			<div class="title">--%>
<%--				Ingrese Quintales Aprobados para el Vi�edo Seleccionado--%>
<%--			</div>--%>
			<table class="tabla">
				<tr>
					<th>
						Id Vi�edo:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.idVinedo"
							type="Long" maxlength="100" readonly="true"
							value="${process.idVinedo}" id="idVinedo" />
					</td>
				</tr>
				<tr>
					<th>
						C.I.V.:
					</th>
					<td>
						<html:text property="process.codigo" value="0" styleId="codigo"
							disabled="true" />
					</td>
				</tr>
				<tr>
					<th>
						Hectareas:
					</th>
					<td>
						<html:text property="process.hectareas" value="0"
							styleId="hectareas" disabled="true" />
					</td>
				</tr>
				<tr>
					<th>
						QQ Estimados:
					</th>
					<td>
						<html:text property="process.estimados" value="0"
							styleId="estimados" disabled="true" />
					</td>
				</tr>
				<tr>
					<th>
						QQ Aprobados:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.qq" type="Long"
							maxlength="10" value="${process.qq}" readonly="true" id="qq"/>
					</td>
				</tr>
				<tr>
					<th>
						Observaciones FTyC:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.observacion"
							type="textarea" maxlength="250" 
							value="${process.observacion}" readonly="false"/>
					</td>
				</tr>
				<tr>
					<th>
						Observaciones INV:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.observacionINV"
							type="textarea" maxlength="250" 
							value="${process.observacionINV}" readonly="true"/>
					</td>
				</tr>
			</table>
<%--			<button type="button" onclick='javascript: aprobar();'>--%>
<%--				Aprobar--%>
<%--			</button>--%>
			<html:submit onclick="return rectificacionParaINV();"
				value="Datos err�neos, solicitar rectificaci�n" />
		</logic:equal>
	</logic:equal>
	<logic:equal name="ProcessForm" property="process.accionStr"
		value="<%= AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE.toString()  %>">
		<logic:equal name="ProcessForm" property="process.INV" value="0">

			<div class="title">
				Ingrese Quintales Aprobados para el Vi�edo Seleccionado
			</div>
			<table class="tabla">
				<tr>
					<th>
						Id Vi�edo:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.idVinedo"
							type="Long" maxlength="100" readonly="true"
							value="${process.idVinedo}" id="idVinedo" />
					</td>
				</tr>
				<tr>
					<th>
						C.I.V.:
					</th>
					<td>
						<html:text property="process.codigo" value="0" styleId="codigo" />
					</td>
				</tr>
				<tr>
					<th>
						Hectareas:
					</th>
					<td>
						<html:text property="process.hectareas" value="0"
							styleId="hectareas" />
					</td>
				</tr>
				<tr>
					<th>
						QQ Estimados:
					</th>
					<td>
						<html:text property="process.estimados" value="0"
							styleId="estimados" />
					</td>
				</tr>
				<tr>
					<th>
						QQ Aprobados:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.qq" type="Decimal"
							maxlength="10" value="${process.qq}" readonly="false" id="qq"/>
					</td>
				</tr>
				<tr>
					<th>
						Observaciones FTyC:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.observacion"
							type="textarea" maxlength="250" 
							value="${process.observacion}" readonly="false"/>
					</td>
				</tr>
				<tr>
					<th>
						Observaciones INV:
					</th>
					<td>
						<asf:text name="ProcessForm" property="process.observacionINV"
							type="textarea" maxlength="250" 
							value="${process.observacionINV}" readonly="true"/>
					</td>
				</tr>
			</table>
			<html:submit onclick="return rectificar();" value="Rectificar" />
		</logic:equal>
	</logic:equal>
</html:form>

<script type="text/javascript">

var ctrlFormulario = $('formulario');
var ctrlAccion = $('accion');
var ctrlId = $('id');
var ctrlIndex = $('idVinedo');
var ctrlCodigo = $('codigo');
var ctrlHectareas = $('hectareas');
var ctrlEstimados = $('estimados');
var ctrlQQ = $('qq');
var ctrlObservacion = $('process.observacion');
var ctrlObservacionINV = $('process.observacionINV');
var ctrlPersona = $('idpersona');
var accionReal = $('accionReal');
var idPersona = $('idPersonaBusqueda');
var ctrlAccionAnterior = $('accionAnterior');
//AL INICIAR.-

function listar() {
	ctrlAccion.value = "<%=AccionINV.LISTAR_VINEDOS_PENDIENTE.toString()%>";
	ctrlFormulario.submit();
}

function listarInformados() {
	ctrlAccion.value = "<%=AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()%>";
	ctrlFormulario.submit();
}

function evaluar(id, codigo, hectareas, estimados, qq, obs, obsINV) {
	ctrlIndex.value = id;
	ctrlCodigo.value = codigo;
	ctrlHectareas.value = hectareas;
	ctrlEstimados.value = estimados;
	ctrlObservacion.value = obs;
	if(qq==null || qq == " "){
		qq = 0;
	}
	ctrlQQ.value = qq;
	ctrlObservacionINV.value = obsINV;
	//ctrlFormulario.submit();
}

function aprobar() {
	ctrlAccion.value = "<%=AccionINV.INFORMAR_VINEDO.toString()%>";
	ctrlFormulario.submit();
}

function crear() {
	ctrlAccion.value = "<%=AccionINV.ENVIAR_PENDIENTES.toString()%>";
	ctrlFormulario.submit();
}

function rectificacion() {
	errores = "";
	ctrlAccion.value = "<%=AccionINV.INV_RECTIFICACION_VINEDO.toString()%>";
	if (ctrlIndex.value == null || ctrlIndex.value == 0) {
		errores = errores + " No se ha seleccionado ning�n vi�edo";
	}
	if (errores == "") {
		msg = "Desea usted que el Fondo modifique los datos de este vi�edo por las siguientes razones:\n";
		msg = msg + " - " + ctrlObservacionINV.value;
		return confirm(msg);
	}
	alert(errores);

	return false;
}

function rectificar() {
	errores = "";
	ctrlAccion.value = "<%=AccionINV.SAVE_RECTIFICACIONF_PENDIENTE.toString()%>";
	if (ctrlIndex.value == null || ctrlIndex.value == 0) {
		errores = errores + " No se ha seleccionado ning�n vi�edo\n";
	}
	if (ctrlHectareas.value == null || ctrlHectareas.value == 0) {
		errores = errores + " No se han ingresado hect�reas\n";
	}
	if (ctrlEstimados.value == null || ctrlEstimados.value == 0) {
		errores = errores + " No se ha ingresado la cantidad estimada\n";
	}
	if (errores == "") {
		msg = "Desea usted que el modificar los datos ingresados:\n";
		return confirm(msg);
	}
	alert(errores);

	return false;
}

function rectificacionParaINV() {
	errores = "";
	ctrlAccion.value = "<%=AccionINV.FONDO_RECTIFICACION_VINEDO.toString()%>";
	if (ctrlIndex.value == null || ctrlIndex.value == 0) {
		errores = errores + " No se ha seleccionado ning�n vi�edo";
	}
	if (errores == "") {
		msg = "Desea usted que el INV modifique los datos de este vi�edo por las siguientes razones:\n";
		msg = msg + " - " + ctrlObservacion.value;
		return confirm(msg);
	}
	alert(errores);

	return false;
}

function consultar()
{
	var idTitular = trim(ctrlPersona.value.split("-")[0]);
	if(ctrlAccionAnterior.value == "<%=AccionINV.LISTAR_VINEDOS_SOLICITADO_PRE_AP.toString()%>"){
		accionReal.value = "LISTAR_VINEDOS_SOLICITADO_PRE_AP";
		idPersona.value = idTitular;
	}else if(ctrlAccionAnterior.value == "<%=AccionINV.LISTAR_VINEDOS_SOLICITADO.toString()%>"){
		accionReal.value = "LISTAR_VINEDOS_SOLICITADO";
		idPersona.value = idTitular;
	} else if(ctrlAccionAnterior.value == "<%=AccionINV.LISTAR_VINEDOS_INFORMADOS.toString()%>"){
		accionReal.value = "LISTAR_VINEDOS_INFORMADOS";
		idPersona.value = idTitular;
	} else{
		accionReal.value = "LISTAR_VINEDOS_PENDIENTE";
	}		
	ctrlFormulario.submit();
}

</script>
