<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<div class="title">Administración de Categorías de Entidades Financieras</div>
<br><br>
<html:form action="/actions/abmAction.do?entityName=${param.entityName}&filter=true" styleId="formulario">
<html:hidden property="paramName[0]" value="entidad.id"/>
<html:hidden property="paramComp[0]" value="="/>
<html:hidden property="do" styleId="do" value="list"/>
<br>
<table>
	<tr>
		<th>Entidad Financiera: </th>
		<td>
			<asf:select name="paramValue[0]" entityName="EntidadFinanciera" listCaption="codigoEntidad,nombreEntidad" listValues="id"
			            orderBy="codigoEntidad" value="${paramValue[0]}" attribs="onchange='filtrar();'" nullText="Seleccione Entidad Financiera" nullValue="true"/>
		</td>
	</tr>
</table>
</html:form>
<br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
    <button type="button" onclick="javascript:newEntity();"><bean:message key="abm.button.new"/></button>
</asf:security>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1"
	requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}" >
	<display:setProperty name="report.title" value="Administración de Categorías de Entidades Financieras"></display:setProperty>  

	<display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
		href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}"/>
	<display:column property="codigoCategoria"      title="Código"      sortable="true"/>
	<display:column property="nombreCategoria"      title="Categoría"   sortable="true"/>
	<display:column property="descripcionCategoria" title="Descripción" sortable="true"/>
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
	<display:column media="html" honClick="return confirmDelete();"
		title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}"
		paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete"/>
	</display:column>
</asf:security>
</display:table>
</div>
<script language="javascript" type="text/javascript">
	var formulario = $('formulario');
	var accion     = $('do');
	
	function filtrar()
    {
		 formulario.submit();
    }

	function newEntity()
	{
		accion.value = "newEntity";
		formulario.submit();
        //window.location = "${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}";
    }
</script>