<%@ page language="java"%>
<%@page import="java.util.Date"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>
<div class="title">Auditoria Final</div>

<div>
	<button onclick="nuevaAuditoria();" value="Nuevo">Nueva
		Auditoria</button>
</div>
<br />
<br />

<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden name="ProcessForm" property="process.accion"
		styleId="accion" />
	<html:hidden name="ProcessForm" property="process.idCredito"
		styleId="idCred" value="${ProcessForm.process.idCredito}" />
	<br />

	<div>

		<display:table name="ProcessForm" property="process.auditoriafin"
			id="auditoria" export="true" defaultsort="2"
			requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}&process.accion=verNotif">
			<display:caption>Auditoria Final</display:caption>
			<display:setProperty name="report.title" value="Auditoria Final"></display:setProperty>
			<display:column media="excel xml pdf" property="id" title="ID" />

			<display:column title="ID" sortProperty="id" sortable="true"
				media="html">
				<a href="javascript: load(${auditoria.id});">${auditoria.id}</a>
			</display:column>

			<display:column title="Fecha Proceso" property="fechaProceso"
				sortable="true" decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Observaciones" property="observaciones"
				sortable="true" />
			<display:column title="Aplica Fondo" property="aplicaFondo"
				sortable="true" />
			<display:column title="Fecha No Aplica Fondo"
				property="fechanoaplica" sortable="true"
				decorator="com.asf.displayDecorators.DateDecorator" />
			<display:column title="Caducidad Plazo" property="caducidadplazo"
				sortable="true" />
			<display:column title="Multa Porcentaje" property="multaporcentaje"
				sortable="true" />
			<display:column title="Importe" property="importeaudi"
				sortable="true" />
			<display:column title="Int. Esp Tasa" property="interestasa"
				sortable="true" />
			<display:column title="Base Calculo" property="basecalculo"
				sortable="true" />
			<display:column title="Bajar" media="html">
				<html:button
					onclick="window.open('${pageContext.request.contextPath}/actions/ArchivoCreditoAction.do?do=descargar&id=${auditoria.objetoiArchivo.id}')"
					property="process.objetoiArchivo.id">Bajar</html:button>
			</display:column>
			<display:column title="Eliminar" media="html">
				<asf:security action="/actions/process.do"
					access="do=process&processName=EliminarAuditoriaFinal">
					<a
						href="${pageContext.request.contextPath}/actions/process.do?processName=EliminarAuditoriaFinal&do=process&process.idAuditoriaFinal=${auditoria.id}"
						onclick="return confirmDelete();">Eliminar</a>
				</asf:security>
			</display:column>
		</display:table>
	</div>

</html:form>
<script type="text/javascript">
	function nuevaAuditoria() {
		window.location = "${pageContext.request.contextPath}/actions/process.do?&do=process&processName=NuevaAuditoriaFinal&process.idObjetoi=${ProcessForm.process.idCredito}";
	}

	function load(id) {
		var url = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=NuevaAuditoriaFinal&process.accion=load&process.idauditoriafinal="
				+ id;
		window.location = url;
	}//fin load.-
</script>