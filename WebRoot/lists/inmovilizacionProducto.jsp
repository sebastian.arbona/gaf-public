<%@ page language="java"%>
<%@page import="com.nirven.creditos.hibernate.Vinedo"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> 

		
<html>
	<body>
	
	<script type="text/javascript">
	
	  
	  function marcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = true;
	        }
	    }
	  }

	  function desmarcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = false;
	        }
	    }
	  }
	
	
	function enviarInforme(){
		var accion = document.getElementById("accion");		
		var formulario = document.getElementById("oForm");
		var confir = confirm("�Seguro que desea enviar informe?");
		if(confir){
			//window.location = "${pageContext.request.contextPath}/actions/process.do?do=process&processName=InmovilizacionProductoProcess&process.accion=enviar";
		accion.value="enviar";
		formulario.submit();
		}
	}
		</script>	
		
		<div class="title">
			Solicitud de Inmovilizaci�n de Producto en Garant�a
		</div>
		<br>
		<br>

		<html:form action="actions/process.do?processName=${param.processName}&do=process" styleId="oForm">
			<html:hidden property="process.accion" styleId="accion" />
		
		<div class="grilla">
			<display:table name="ProcessForm" property="process.creditos"
				defaultsort="1" id="vinedo" class="com.nirven.creditos.hibernate.objetoi">

				<display:column property="persona.nomb12"
					title="Solicitante" />
				<display:column property="expediente" title="Expediente" />
				<display:column property="depositoMinimo"
					title="Deposito en garant�a m�nimo" />
				<display:column property="destinoInm"
					title="Destino" />
				<display:column media="html" title="Ver Detalle"
					href="${pageContext.request.contextPath}/actions/process.do?processName=InmovilizacionProductoProcess&do=process&process.accion=ver"
					paramId="process.id" paramProperty="id">Ver Detalle</display:column>
				<display:column title="Selecci�n">
					<input type="checkbox" name="vinedo-${vinedo.id}" value="${vinedo.id}" />
				</display:column>
	
			</display:table>
			</div>
			<input type="button"  id="seleccionarTodos" value="Seleccionar todos" onclick="marcarTodos();" />
			<input type="button"  id="deseleccionarTodos" value="Deseleccionar todos" onclick="desmarcarTodos()" />
			
			<logic:equal value="true" name= "ProcessForm" property= "process.detalle">
			<div class="title">
				Detalle del Titular
			</div>
			<table border="0">				
				<tr>
					<td>
						Solicitante
					</td>
					<td>
						<html:text property="process.credito.persona.nomb12"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
					<td>
						Expediente
					</td>
					<td>
						<html:text property="process.credito.expediente"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
					<td>
						Nro Proyecto
					</td>
					<td>
						<html:text property="process.credito.numeroAtencion"
							name="ProcessForm" maxlength="30" disabled="true" />
					</td>
				</tr>
			</table>
			<div class="title">
				Detalle de la Garant�a
			</div>
			<table border="0">
				
				<logic:notEmpty name="ProcessForm" property="process.garantia.valores">
				
				<logic:iterate id="valorGarantia" name="ProcessForm" property="process.garantia.valores">
				<tr>
					<th>	
						<bean:write name="valorGarantia" property="campo.nombre"/>
					</th>
					<td>
						<bean:write name="valorGarantia" property="valorCadena"/>	
					</td>
				</tr>
				
				</logic:iterate>
					
				</logic:notEmpty>
				
				<tr>
					<td>
						Observaciones
					</td>
					<td>
						<html:textarea property="process.garantia.observacion" 
							name="ProcessForm"  disabled="true" />
					</td>
				</tr>
			</table>
			</logic:equal>
			<input type="button"  id="enviarSol" value="Enviar Informe" onclick="enviarInforme();" />
 			</html:form>		
		</body>
	</html>

