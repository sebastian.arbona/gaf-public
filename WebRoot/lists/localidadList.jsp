<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<script type="text/javascript">
	
    function nuevo(){
        if('${paramValue[0]}'!=''){
            getElem('do').value='newEntity';
            window.location.href="${pageContext.request.contextPath}/actions/abmAction.do?entityName=${param.entityName}&filter=true";
        }
    }
	
</script>

<div class="title">Administración de Localidades</div>

<html:form action="/actions/abmAction.do?entityName=${param.entityName}&filter=true&entity.codi08=${paramValue[0]}" >
    <br><br>
    <b>Provincia:</b>

    <asf:select name="paramValue[0]" attribs="onchange=AbmForm.submit();"  nullValue="true" entityName="com.civitas.hibernate.persona.Provin" listCaption="getDeta08" listValues="getId" value="${paramValue[0]}" filter="deta08 like '%' order by deta08"/>
    <br/>
    <br/>
    <html:hidden property="paramName[0]" styleId="paramName" value="codi08"/>
    <html:hidden property="paramComp[0]" styleId="paramComp" value="="/>
    <html:hidden property="filter" value="true"/>
    <html:hidden property="do" styleId="do" value="list"/>

    <asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
        <html:submit onclick="nuevo();">Nuevo</html:submit>
    </asf:security>

</html:form>

<div class="grilla">
    <display:table name="ListResult" property="result" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">

        <display:setProperty name="report.title" value="Administración de Localidades"/> 
        <display:caption>
            Provincia: ${paramValue[0]} - ${reportTable.provin.deta08}
        </display:caption>
        <display:column property="idlocalidad" title="ID" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&load" paramId="entity.idlocalidad" paramProperty="id" sortable="true"/>
        <display:column property="nombre" title="Localidad" sortable="true"/>    
        <display:column property="abrev" title="Abreviatura" sortable="true"/>
        <display:column property="cp" title="Código Postal" sortable="true"/>

        <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
            <display:column media="html" honClick="return confirmDelete();" title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&filter=true" paramId="entity.idlocalidad" paramProperty="id">Eliminar</display:column>
        </asf:security>
    </display:table>
</div>
