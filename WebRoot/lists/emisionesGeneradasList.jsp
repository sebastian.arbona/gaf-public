<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div class="title" style="margin-bottom: 30px">Emisiones Generadas</div>

<html:form
 action="/actions/informeEnte.do?do=generar"
	styleId="oForm"
	>
<html:errors/>

<script type="text/javascript">
	  function marcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = true;
	        }
	    }
	  }

	  function desmarcarTodos(){
		  var nodoCheck = document.getElementsByTagName("input");
	    for (i=0; i<nodoCheck.length; i++){
	        if (nodoCheck[i].type == "checkbox" && nodoCheck[i].name != "all" && nodoCheck[i].disabled == false) {
	            nodoCheck[i].checked = false;
	        }
	    }
	  }
	  
	 function desmarcarGenerar(){
		 setTimeout("desmarcarTodos()",1000);
	 }
		</script>	

	
<div class="grilla">
	<display:table name="ProcessForm" property="process.emisiones" export="true" id="reportTable"
	requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=${param.processName}">			
		<display:setProperty name="report.title" value="Emisiones sin generar"></display:setProperty>
	    <display:column property="fechaEmisionStr" title="Fecha" href="${pageContext.request.contextPath}/actions/emidetaAction.do?do=list&entityName=Emideta" paramProperty="id" paramId="idEmision"/>
	    <display:column property="ordenEmision" title="Orden"/>
	    <display:column property="cantidad" title="Cantidad"/>
	    <display:column property="importe" title="Importe" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
	    <display:column title="Selecci�n">
			<input type="checkbox" name="emision-${reportTable.id}" value="${reportTable.id}" />
		</display:column>
	    
    </display:table>
    <div>
		<input type="submit" value="Generar" onclick="desmarcarGenerar()">
		<input type="button"  id="seleccionarTodos" value="Seleccionar todos" onclick="marcarTodos();" />
		<input type="button"  id="deseleccionarTodos" value="Deseleccionar todos" onclick="desmarcarTodos()" />
	</div>
</div>



</html:form>
