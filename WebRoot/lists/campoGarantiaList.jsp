<%@ page language="java"%>
<%@ page buffer="16kb"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">
	Administración de Campos - ${param.nombre}
</div>
<br>
<html:form
	action="actions/abmAction.do?do=newEntity&entityName=${param.entityName}">
	<html:hidden property="paramValue[0]" value="${paramValue[0]}" />
	<html:hidden property="paramName[0]" value="tipoGarantia.id" />
	<html:hidden property="paramComp[0]" value="=" />
	<html:hidden property="do" styleId="accion" value="newEntity" />
	
	<html:hidden property="nombre" value="${param.nombre}" />
	
	<asf:security action="/actions/abmAction.do"
		access="do=newEntity&entityName=${param.entityName}">
		
		<button type="submit">
			<bean:message key="abm.button.new" />
		</button>
		<button type="button" onclick="window.location='${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=TipoGarantia';">
			Volver
 		</button>
	</asf:security>

</html:form>

<br>
<br>
<div class="grilla">
	<display:table name="ListResult" property="result" export="true"
		id="reportTable" defaultsort="1"
		requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
		<display:setProperty name="report.title"
			value="Administración de Tipos de Garantía"></display:setProperty>

		<display:column property="id" title="ID" sortProperty="id"
			paramId="entity.id" paramProperty="id" sortable="true"
			href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" />

		<display:column property="nombre" title="Nombre" sortable="true" />
		<display:column property="obligatorioStr" title="Obligatorio"
			sortable="true" />
		<display:column property="tipoDeCampoStr" title="Tipo de Campo"
			sortable="true" />
		<display:column property="maximoCaracteres" title="Tamaño"
			sortable="true" />
		<asf:security action="/actions/abmAction.do"
			access="do=delete&entityName=${param.entityName}&filter=true&paramValue[0]=${paramValue[0]}&paramName[0]=${paramName[0]}&paramComp[0]=%3D">

			<display:column media="html" honClick="return confirmDelete();"
				title="Eliminar"
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&paramName[0]=${paramName[0]}&nombre=${param.nombre}&filter=true&paramName[0]=tipoGarantia.id&paramComp[0]=%3d"
				paramId="entity.id" paramProperty="id">
				<bean:message key="abm.button.delete" />
			</display:column>



		</asf:security>
	</display:table>
</div>
