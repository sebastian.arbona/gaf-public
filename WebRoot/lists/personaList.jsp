<%@ page language="java"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%
	request.setAttribute("URL_ADE", DirectorHelper.getString("URL.ADE"));
%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/style/autocomplete.css"
	type="text/css">
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/fonts/fonts-min.css"
	rel="stylesheet" />
<link type="text/css"
	href="${pageContext.request.contextPath}/scripts/yui/build/tabview/assets/skins/sam/tabview.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/element/element-beta-min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/scripts/yui/build/tabview/tabview-min.js"></script>

<script language="javascript" type="text/javascript">
	/**
	 * @inputIn: control sobre el input que dispara la consulta ajax.-
	 * @defecto: para parametro=valor que tiene,
	 * a la izquierda del igual el id del input que dispara la consulta ajax.-
	 * a la derecha del igual el valor que tomo el input que dispara la consulta ajax.-
	 */
	function actualizarLista(inputIn, defecto) {
		var urlDinamica = "";

		urlDinamica += "&persona=" + inputIn.value;
		urlDinamica += "&" + defecto;

		return urlDinamica;
	}
</script>

<body class=" yui-skin-sam">
	<div class="title">Administraci&oacute;n de Personas</div>
	<html:form
		action="/actions/PersonaAction.do?entityName=${param.entityName}&filter=true">
		<html:hidden property="paramValue" styleId="paramValue"
			onchange="consultar2();" value="" />
		<table>
			<tr>
				<th style="padding-left: 41px; padding-right: 41px">Persona:</th>
				<td><asf:autocomplete name="" tipo="AJAX"
						idDiv="autocomplete_persona" idInput="idpersona"
						classDiv="autocomplete"
						origenDatos="${pageContext.request.contextPath}/actions/creditosAjaxHelperAction.do?do=buscarPersona"
						options="{callback: actualizarLista, frequency: '0.8', minChars: '5',indicator:'indicadorPersona'}"
						attribs="size='50%' title='Se puede buscar por Apellido y Nombre o Nro de Documento'" />
					<span id="indicadorPersona" style="display: none"><img
						src="images/loader.gif" alt="Trabajando..." /></span> <input value="..."
					onclick="popUp('${pageContext.request.contextPath}/actions/selectpopup.do?do=list&amp;name=paramValue&amp;entityName=Persona&amp;title=Selecci�n de Persona&amp;captions=idpersona,nomb12,cuil12,nudo12&amp;columns=C�digo,Apellido y Nombre,CUIL-CUIT,Documento&amp;values=idpersona&amp;cantidadDescriptors=0&amp');"
					type="button">&nbsp; (C&oacute;digo - Apellido y Nombre -
					CUIL/CUIT - N&uacute;mero de Documento)</td>
			</tr>
		</table>
		<html:hidden property="paramName[0]" styleId="paramName"
			value="idpersona" />
		<html:hidden property="paramComp[0]" styleId="paramComp" value="=" />
		<html:hidden property="filter" value="true" />
		<html:hidden property="do" styleId="do" value="list" />
		<asf:security action="/actions/PersonaAction.do"
			access="do=newEntity&entityName=${param.entityName}">
			<input type="button" onclick="nuevo();"
				value="<bean:message key="abm.button.new"/>" />
		</asf:security>
		<input type="button" value="Consultar" onclick="consultar();">
		<br>
		<br>
		<display:table name="ListResult" property="result" defaultsort="1"
			id="reportTable"
			requestURI="${pageContext.request.contextPath}/actions/PersonaAction.do?do=list&entityName=${param.entityName}">
			<display:setProperty name="report.title"
				value="Administraci�n de Personas"></display:setProperty>
			<display:column media="html" sortProperty="id" property="id"
				title="ID"
				href="${pageContext.request.contextPath}/actions/PersonaAction.do?do=load&entityName=${param.entityName}&load"
				paramId="entity.id" paramProperty="id" />
			<display:column property="nomb12" title="Apellido y Nombre" />
			<display:column property="cuil12" title="CUIT" />
			<display:column property="razonSocial" title="Raz�n Social" />
			<display:column title="Documento">
    			${reportTable.tipodoc.tido47} ${reportTable.nudo12}
    		</display:column>
			<display:column property="estadoActual.estado.nombreEstado"
				title="Estado" />
			<display:column title="Color">
				<div style="background:${reportTable.estadoActual.estado.color};">&nbsp;</div>
			</display:column>
			<display:column property="fechaBajaStr" title="Fecha de Baja" />
			<asf:security action="/actions/PersonaAction.do?do=delete">
				<logic:notEmpty name="ListResult" property="result">
					<logic:empty name="reportTable" property="fechaBaja">
						<display:column media="html" honClick="return confirmDelete();"
							title="Baja"
							href="${pageContext.request.contextPath}/actions/PersonaAction.do?do=loadBaja&entityName=${param.entityName}&filter=true"
							paramId="entity.id" paramProperty="id">Baja</display:column>
					</logic:empty>
				</logic:notEmpty>
				<logic:notEmpty name="ListResult" property="result">
					<logic:notEmpty name="reportTable" property="fechaBaja">
						<display:column media="html" title="Alta"
							href="${pageContext.request.contextPath}/actions/PersonaAction.do?do=loadAlta&entityName=${param.entityName}&filter=true"
							paramId="entity.id" paramProperty="id">Alta</display:column>
					</logic:notEmpty>
				</logic:notEmpty>
			</asf:security>
			<logic:notEmpty name="ListResult" property="result">
				<logic:notEmpty name="reportTable" property="legajoDigital">
					<display:column title="Legajo Digital" honClick="irLegajoDigital()"
						property="legajoDigital" href="#" />
				</logic:notEmpty>
			</logic:notEmpty>
		</display:table>
		<logic:notEmpty name="ListResult" property="result">
			<logic:notEmpty name="reportTable" property="legajoDigital">
				<input type="hidden" value="${reportTable.legajoDigital}"
					name="process.id" index="process.id" />
				<input type="hidden" value="1" name="process.idTipo" />
			</logic:notEmpty>
		</logic:notEmpty>
		<logic:notEqual parameter="paramValue[0]" value="0">
			<div id="pestanias" class="yui-navset"
				style="width: 100%; display: none;">
				<ul class="yui-nav">
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasDomicilio">
						<li class="selected"><a href="#domicilios"><em>Domicilios</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasContactoPersona">
						<li><a href="#contactos" onclick="irAContactos();"><em>Contactos</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasVinculadas">
						<li><a href="#personasVinculadas"
							onclick="irAPersonasVinculadas();"><em>Personas
									Vinculadas</em></a></li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasImpuestos">
						<li><a href="#datosImpositivos"
							onclick="irADatosImpositivos();"><em>Impuestos</em></a></li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasSolicitudes_Proyectos">
						<li><a href="#datosCrediticios"
							onclick="irADatosCrediticios();"><em>Solicitudes/Proyectos</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasBancos">
						<li><a href="#datosBancarios" onclick="irADatosBancarios();"><em>Bancos</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasDatos_Laborales">
						<li><a href="#datosLaborales" onclick="irADatosLaborales();"><em>Datos
									Laborales</em></a></li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasRoles">
						<li><a href="#roles" onclick="irARoles();"><em>Roles</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasEstados_Observaciones">
						<li><a href="#estadosObservaciones" onclick="irAEstaObs();"><em>Estados
									y Observaciones</em></a></li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasEntidades">

						<li><a href="#situacionEntidades"
							onclick="irASitEntidades();"><em>Entidades</em></a></li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasDocumentacion">
						<li><a href="#documentacion" onclick="irADocumentacion();"><em>Documentaci&oacute;n</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasBonificaciones">
						<li><a href="#bonificacionesTasa" onclick="irABonTasa();"><em>Bonificaciones
									Tasa</em></a></li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasCargos">
						<li><a href="#cargos" onclick="irACargos();"><em>Cargos</em></a>
						</li>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasHistorialNoti">
						<li><a href="#noti" onclick="irANoti();"><em>Historial
									de Notificaciones</em></a></li>
					</asf:security>
				</ul>
				<div class="yui-content">
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasDomicilio">
						<div id="domicilios">
							<iframe
								src='${pageContext.request.contextPath}/actions/process.do?do=process&processName=Domicilio&process.personaTitular.id=${paramValue[0]}'
								width="100%" height="400" scrolling="auto" style="border: none"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasContactoPersona">
						<div id="contactos">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameContactos"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasVinculadas">
						<div id="personasVinculadas">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="framePersonasVinculadas"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasImpuestos">
						<div id="datosImpositivos">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDatosImpositivos"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasSolicitudes_Proyectos">
						<div id="datosCrediticios">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDatosCrediticios"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasBancos">
						<div id="datosBancarios">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDatosBancarios"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasDatos_Laborales">
						<div id="datosLaborales">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDatosLaborales"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasRoles">
						<div id="roles">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameRoles"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasEstados_Observaciones">
						<div id="estadosObservaciones">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameEstaObs"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasEntidades">
						<div id="situacionEntidades">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameSitEntidades"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasDocumentacion">
						<div id="documentacion">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameDocumentacion"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasBonificaciones">
						<div id="bonificacionesTasa">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameBonTasa"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasCargos">
						<div id="cargos">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameCargos"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
					<asf:security action="/actions/process.do"
						access="processName=pestaniasPersonasHistorialNoti">
						<div id="noti">
							<iframe width="100%" height="400" scrolling="auto"
								style="border: none;" id="frameNoti"
								allowtransparency="allowtransparency"> </iframe>
						</div>
					</asf:security>
				</div>
			</div>
		</logic:notEqual>
	</html:form>

	<script language="javascript" type="text/javascript">
		var ctrlPersona = $('idpersona');

		//pesta�as.-
		var tabView = new YAHOO.widget.TabView('pestanias');
		var pestanias = $('pestanias');

		//Frames
		var framePersonasVinculadas = $('framePersonasVinculadas');
		var frameDatosImpositivos = $('frameDatosImpositivos');
		var frameContactos = $('frameContactos');
		var frameDatosCrediticios = $('frameDatosCrediticios');
		var frameDatosBancarios = $('frameDatosBancarios');
		var frameDatosLaborales = $('frameDatosLaborales');
		var frameRoles = $('frameRoles');
		var frameEstaObs = $('frameEstaObs');
		var frameDocumentacion = $('frameDocumentacion');
		var frameSitEntidades = $('frameSitEntidades');
		var frameBonTasa = $('frameBonTasa');
		var frameCargos = $('frameCargos');
		var frameNoti = $('frameNoti');

		if ('${paramValue[0]}' != "") {
			pestanias.style.display = "";
		}

		function consultar() {
			if (frames.length == 0) {
				var idPersona = trim(ctrlPersona.value.split("-")[0]);
				var url = "";
				url += '${pageContext.request.contextPath}/actions/PersonaAction.do?';
				url += 'do=list';
				url += "&entityName=${param.entityName}";
				url += '&paramName[0]=idpersona';
				url += '&paramComp[0]==';
				url += '&paramValue[0]=' + idPersona;
				url += "&filter=true";
				window.location = url;
			} else {
				var mensaje = "Antes de realizar una nueva b�squeda debe TERMINAR o CANCELAR la carga de los siguientes datos:\n";
				for (var i = 0; i < frames.length; i++) {
					mensaje += frames[i].name + "\n";
				}
				alert(mensaje);
			}
		}//fin consultar.-

		function nuevo() {
			if (frames.length == 0) {
				var url = "";
				url += '${pageContext.request.contextPath}/actions/PersonaAction.do?'
				url += 'do=newEntity';
				url += "&entityName=${param.entityName}";
				window.location = url;
			} else {
				var mensaje = "Antes de cargar una nueva persona debe TERMINAR o CANCELAR la carga de los siguientes datos:\n";
				for (var i = 0; i < frames.length; i++) {
					mensaje += frames[i].name + "\n";
				}

				alert(mensaje);
			}
		}// fin nuevo.-

		function irAContactos() {
			var src = '';
			if (frameContactos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=Contacto';
				src += '&process.personaTitular.id=${paramValue[0]}';
				frameContactos.src = src;
			}
		}//fin irAContactos.-

		function irAPersonasVinculadas() {
			var src = '';
			if (framePersonasVinculadas.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=PersonaVinculada';
				src += '&process.personaTitular.id=${paramValue[0]}';
				framePersonasVinculadas.src = src;
			}
		}//fin irAPersonasVinculadas.-

		function irADatosImpositivos() {
			var src = '';
			if (frameDatosImpositivos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=DatosImpositivos';
				src += '&process.persona_id=${paramValue[0]}';
				frameDatosImpositivos.src = src;
			}
		}//fin irADatosImpositivos.-

		function irADatosCrediticios() {
			var src = '';
			if (frameDatosCrediticios.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=DatosCrediticios';
				src += '&process.persona.id=${paramValue[0]}';
				frameDatosCrediticios.src = src;
			}
		}//fin irADatosCrediticios.-

		function irADatosBancarios() {
			var src = '';
			if (frameDatosBancarios.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=DatosBancarios';
				src += '&process.persona.id=${paramValue[0]}';
				frameDatosBancarios.src = src;
			}
		}//fin irADatosBancarios.-

		function irADatosLaborales() {
			var src = '';
			if (frameDatosLaborales.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=DatosLaborales';
				src += '&process.persona.id=${paramValue[0]}';
				frameDatosLaborales.src = src;
			}
		}//fin irADatosLaborales.-

		function irARoles() {
			var src = '';
			if (frameRoles.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=Roles';
				src += '&process.persona.id=${paramValue[0]}';
				frameRoles.src = src;
			}
		}//fin irADatosLaborales.-

		function irAEstaObs() {
			var src = '';
			if (frameEstaObs.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=EstadosPersonas';
				src += '&process.persona.id=${paramValue[0]}';
				frameEstaObs.src = src;
			}
		}//fin irADatosLaborales.-

		function irADocumentacion() {
			var src = '';
			if (frameDocumentacion.src == "") {
				src += '${pageContext.request.contextPath}/actions/ArchivoAction.do?';
				src += 'do=list';
				src += '&entityName=Archivo';
				src += '&paramName=persona.id';
				src += '&paramComp==&[0]';
				src += '&paramValue[0]=${paramValue[0]}';
				src += '&filter=true';
				frameDocumentacion.src = src;
			}
		}//fin irADocumentacion.-

		function irASitEntidades() {
			var src = '';
			if (frameSitEntidades.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CargarSituacionEntidad';
				src += '&process.persona.id=${paramValue[0]}';
				frameSitEntidades.src = src;
			}
		}//fin irASitEntidades.-

		function irABonTasa() {
			var src = '';
			if (frameBonTasa.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=BonTasaProcess';
				src += '&process.idPersona=${paramValue[0]}';
				frameBonTasa.src = src;
			}
		}

		function irACargos() {
			var src = '';
			if (frameCargos.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=CargosProcess';
				src += '&process.personaTitular.id=${paramValue[0]}';
				frameCargos.src = src;
			}
		}

		function irANoti() {
			var src = '';
			if (frameNoti.src == "") {
				src += '${pageContext.request.contextPath}/actions/process.do?';
				src += 'do=process&processName=NotificacionProcess';
				src += '&process.idPersona=${paramValue[0]}&process.accion=verNotifPersona&process.pestania=true';
				frameNoti.src = src;
			}
		}

		//Funciones para control de los frames.-
		var frames = new Array();

		function push(frame) {
			frames[frames.length] = frame;
		}//fin push.-

		function pop(frame) {
			var continuar = true;
			for (var i = 0; i < frames.length && continuar; i++) {
				if (frames[i].name == frame.name) {
					//si no es el �ltimo.-
					if (i < (frames.length - 1)) {
						for (var j = i; j < frames.length; j++) {
							try {
								frames[j] = frames[(j + 1)];
							} catch (e) {
							}
						}
						try {
							frames.splice((frames.length - 1), 1);
						} catch (e) {
						}
					}
					//si es el �ltimo.-
					else {
						try {
							frames.splice(i, 1);
						} catch (e) {
						}
					}
					continuar = false;
				}
			}
		}//fin pop.-

		function consultar2() {
			if (frames.length == 0) {
				var paramValue = $('paramValue');
				var url = "";
				url += '${pageContext.request.contextPath}/actions/PersonaAction.do?'
				url += 'do=list';
				url += "&entityName=${param.entityName}";
				url += '&paramName[0]=idpersona';
				url += '&paramComp[0]==';
				url += '&paramValue[0]=' + paramValue.value;
				url += "&filter=true";
				window.location = url;
			} else {
				var mensaje = "Antes de realizar una nueva b�squeda debe TERMINAR o CANCELAR la carga de los siguientes datos:\n";
				for (var i = 0; i < frames.length; i++) {
					mensaje += frames[i].name + "\n";
				}
				alert(mensaje);
			}
		}//fin consultar2.-

		function irLegajoDigital() {
			document.personaForm.target = '_blank';
			document.personaForm.action = "${URL_ADE}/actions/process.do?processName=DocumentoHelper&process.id=${reportTable.legajoDigital}&do=process&process.action=identificador"
			document.personaForm.submit();
		}
	</script>