<%@ page language="java"%>
<%@page import="com.asf.cred.business.ObjetoiDTO"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/asf-gat-taglib.tld" prefix="asfgat"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>

<div class="title">
	Desembolsos Masivos
</div>
<br>
<html:form
	action="/actions/process.do?do=process&processName=${param.processName}"
	styleId="oForm">
	<html:hidden property="process.accion" styleId="accion" />

	<div style="width: 70%" align="left">
		<html:errors />
	</div>
	
	<table>
	<logic:notEmpty name="ProcessForm" property="process.mensajes">
		<logic:iterate id="mensajesId" name="ProcessForm" property="process.mensajes">
			<tr>
				<td><bean:write name="mensajesId" property="value"/></td>
			</tr>								
		</logic:iterate>
	</logic:notEmpty>
	</table>
	
	<logic:notEmpty name="ProcessForm" property="process.mensajes">
		<br/>
	</logic:notEmpty>
	<table>
	
	<logic:notEmpty name="ProcessForm" property="process.mensajesGarantia">
		<logic:iterate id="mensajesGarantiaId" name="ProcessForm" property="process.mensajesGarantia">
			<tr>
				<td><bean:write name="mensajesGarantiaId" property="value"/></td>
			</tr>								
		</logic:iterate>    
	</logic:notEmpty>
   	</table> 
	<logic:notEmpty name="ProcessForm" property="process.mensajesGarantia">   	   
    	<br/>
    </logic:notEmpty>	
    
	<logic:empty name="ProcessForm" property="process.creditos">
    	No hay creditos en estado An�lisis
    </logic:empty>

	<logic:notEmpty name="ProcessForm" property="process.creditos">
		<display:table name="ProcessForm" property="process.creditos"
			id="credito" class="com.asf.cred.business.ObjetoiDTO">
			<display:column title="Nro Solicitud" property="numeroAtencion"
				sortable="true" />
			<display:column title="Titular" property="titular.nomb12"
				sortable="true" />
			<display:column title="Destino"sortable="true" >
				<logic:equal value="CRDC" property="destino" name="credito">
				Cosecha
				</logic:equal>
				<logic:equal value="CRDA" property="destino" name="credito">
				Elaboracion
				</logic:equal>
			</display:column>
			<display:column title="Monto Aprobado" property="financiamiento"
				sortable="true" />
			<display:column title="Fecha Solicitud" property="fechaSolicitudStr"
				sortable="true" />
			<display:column title="Selecci�n">
				<input type="checkbox" name="requisito-${credito.idObjetoi}"
					value="${credito.idObjetoi}" />
			</display:column>
		</display:table>
		<br />
		<input type="button" value="Generar" id="generarId"
			onclick="generar();" />
	</logic:notEmpty>

</html:form>
<script type="text/javascript">
var accion = $('accion');
var formulario = $('oForm');

function generar() {
	accion.value = 'generar';
	formulario.submit();
}
</script>
