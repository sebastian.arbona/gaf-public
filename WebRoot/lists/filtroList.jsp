<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Administración de Filtros</div>
<p>
    <asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
        <button	onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';"><bean:message key="abm.button.new" /></button>
    </asf:security>
</p>
<div class="grilla">
    <display:table name="ListResult" property="result" defaultsort="1" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
        <display:setProperty name="report.title" value="Administración de Filtros"/>
        <display:column media="html" title="ID" sortable="true" href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" paramId="entity.idfiltro" paramProperty="idfiltro" property="idfiltro"/>
        <display:column title="ID" property="idfiltro" media="xml excel pdf"/>
        <display:column title="Nombre" property="nombre" sortable="true" />
        <display:column title="Tipo" property="tipo" sortable="true" />
        <display:column title="Tabla" property="tabla" sortable="true" />
        <asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
            <display:column title="Eliminar" media="html" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.idfiltro" paramProperty="idfiltro" honClick="return confirmDelete();">Elminar</display:column>
        </asf:security>
    </display:table>
</div>