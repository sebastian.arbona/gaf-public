<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div id="table-container" class="contenedor-dtable grilla-datatable">

	<h3 class="title">
		<span class="fa fa-fw fa-file"></span>&nbsp;<span
			id="title_grilla_archivo"></span>
	</h3>

	<asf:security action="/actions/process.do"
		access="do=process&processName=ArchivoLegajoDigital&process.accion=doPost">

		<button type="button" class="btn btn-default new-usuario"
			id="addElement" onclick="add_ArchivoLegajoDigital();">
			<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Subir Archivo
		</button>
		<button type="button" class="btn btn-default new-usuario"
			id="addElement" onclick="search_Archivo();">
			<i class="fa fa-search" aria-hidden="true"></i>&nbsp;Seleccionar
			Existente
		</button>
		<button type="button" class="btn btn-default" id="btnDescargar"
			onclick="Descarga();">
			<i class="dtBotones dtDownload" aria-hidden="true"></i>&nbsp;Descargar
		</button>

	</asf:security>



	<div class="contenedor-dtable grilla-datatable style-datatable">
		<table id="dt-archivoLegajoDigital"
			class="table table-hover user-list" cellspacing="0">
		</table>
	</div>
</div>

<script type="text/javascript">
	$j(document).ready(
			function() {
				format_dt_archivoLegajoDigital();
				dTable_archivoLegajoDigital = inicializarDatatable(
						"dt-archivoLegajoDigital", 1, null,
						dt_personalizacion_archivoLegajoDigital);
				var table = $j('#dt-archivoLegajoDigital').DataTable();
				table.on('preDraw', function() {
					selectallRevision();
				}).on('draw.dt', function() {
					//aca agarra los evento en la paginacion que se redirecciono
					selectallRevision();
				}).on('search.dt', function() {
					selectallRevision();
				});

			})
	function selectallRevision() {
		seleccionoCheckRevision(this);
	}
	function seleccionoCheckRevision(source) {
		var sumaCheck = 0;
		//meto en una variable el elemento cuyo nombre es check
		checkboxes = document.getElementsByName('check');

		// dTable_archivoLegajoDigital.rows().deselect();
		if (variosId == "") {
			for (var i = 0, n = checkboxes.length; i < n; i++) {
				checkboxes[i].checked = 0
			}
		} else {
			for (var i = 0, n = checkboxes.length; i < n; i++) {
				if (checkboxes[i].checked == true) {
					sumaCheck++;
				}
			}
		}
		if (sumaCheck == checkboxes.length) {
			checkAllCheck.checked = 1;
		} else {
			checkAllCheck.checked = 0;
		}
	}
</script>
