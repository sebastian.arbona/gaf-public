<%@ page language="java"%>
<%@ page buffer = "16kb" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<!-- diegobq -->
<div class="title">Administración de Unidades</div>
<br>
<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
<button onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';">
	<bean:message key="abm.button.new"/>
</button>
</asf:security>

<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable"
	requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}" defaultsort="1">

    <display:setProperty name="report.title" value="Administración de Unidades" />

    <display:column property="id" title="ID" media="html"
    	href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}" paramId="entity.id"
    	paramProperty="id" sortable="true" />
    <display:column property="id" title="ID" media="pdf excel xml" />
    
    <display:column property="codigo" title="Código" sortable="true" />
    <display:column property="nombre" title="Nombre" sortable="true" />
    <display:column property="observaciones" title="Observaciones" sortable="true" />
    <display:column property="unidadSuperior_id" title="Unidad Superior" sortable="true" />
    <display:column property="activoStr" title="Ativo" sortable="true" />
    <display:column property="nivelStr" title="Nivel" sortable="true" />
    
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
    <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
    	href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}" paramId="entity.id"
    	paramProperty="id" ><bean:message key="abm.button.delete" />
    </display:column>
</asf:security>
</display:table>
</div>