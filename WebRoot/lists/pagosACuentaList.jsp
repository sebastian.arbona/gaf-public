<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<div class="title">
	Pagos a Cuenta
</div>
<div style="width: 70%" align="left">
	<html:errors />
</div>
<html:form action="/actions/process.do?do=process&processName=PagosACuenta" >
	<br/>
<%-- 	<html:hidden property="process.idRecaudacion" styleId="idRecaudacion"/> --%>
	<div class="grilla">
		<display:table name="ProcessForm" property="process.arrlMovIngresosVarios"
 			export="true" id="reportTable" width="100%"
			requestURI="${pageContext.request.contextPath}/actions/process.do?processName=${param.processName}&do=process">
			
			<display:column title="N�mero Comprobante" media="html"	sortable="true">
				<html:link href="javascript: cargar( ${reportTable.id}, ${reportTable.deudoresVarios.moneda.idMoneda}, ${reportTable.cotizacion},${reportTable.saldo}, null, null, '${reportTable.fechaProceso}', ${reportTable.saldo});">
				Seleccionar</html:link>
			</display:column>
			
			<display:column title="Saldo" property="saldo" sortable="true" align="right" style="text-align:right;" />
			<display:column title="Importe" property="importe" sortable="true" align="right" style="text-align:right;" />
			<display:column title="Persona" property="deudoresVarios.persona.nomb12" sortable="true" />	
			<display:column title="Fecha Proceso" property="fechaProceso" sortable="true" />
			<display:column title="Fecha Generaci�n" property="fechaProceso" sortable="true" />	
			<display:column title="Moneda" property="deudoresVarios.moneda.denominacion" sortable="true" />
			<display:column title="Id Moneda" property="deudoresVarios.moneda.idMoneda" sortable="true" />	
			<display:column title="Cotizaci�n" property="cotizacion" sortable="true" />	
			
 		</display:table>
	</div>
</html:form>
<script type="text/javascript" language="javascript">

function cargar( id, idMoneda, cotizacion, importe, nromovimiento,tipoComprobante, fechaMov, saldo){
	var moneda = window.opener.document.getElementById("process.idMoneda"); 
	var mPago = window.opener.document.getElementById("process.idmediopago");
	var cotiza = window.opener.document.getElementById("cotizacion"); 
	var aCobrar = window.opener.document.getElementById("aCobrar");
	var movIngresosVariosId = window.opener.document.getElementById("movIngresosVariosId");
	var recaudFecha = window.opener.document.getElementById("recauFecha");
	var recaudImporte = window.opener.document.getElementById("recauImporte");
	var recaudSaldo = window.opener.document.getElementById("recauSaldo");
	var importeMP = window.opener.document.getElementById("importe");
	moneda.value = idMoneda;
	cotiza.value = new Number(cotizacion);
	mPago.value = new Number(51);
	movIngresosVariosId.value = id;
	recaudFecha.value = fechaMov; // FechaMov
	recaudImporte.value = new Number(importe);
	recaudSaldo.value = new Number(saldo);
	aCobrar.value = new Number("0.00");
	importeMP.value = new Number(0);
	window.close();
}

</script>