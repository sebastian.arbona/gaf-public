<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<div class="title">Administración de Etapas</div>

<asf:security action="/actions/abmAction.do"
	access="do=newEntity&entityName=${param.entityName}">
	<button
		onclick="window.location = '${pageContext.request.contextPath}/actions/abmAction.do?do=newEntity&entityName=${param.entityName}';">
		<bean:message key="abm.button.new" />
	</button>
</asf:security>
<br></br>
<div class="grilla">
	<display:table name="ListResult" property="result" export="true"
		id="reportTable"
		requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}"
		defaultsort="1">
		<display:setProperty name="report.title"
			value="Administración de Etapas"></display:setProperty>
		<display:column title="ID" sortProperty="idEstado" sortable="true"
			media="html">
			<a
				href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}&entity.idEstado=${reportTable.id}&load"
				paramId="entity.idEstado" paramProperty="idEstado" sortable="true">${reportTable.id}</a>
		</display:column>
		<display:column media="excel xml pdf" property="id" title="ID" />
		<display:column property="nombreEstado" title="Nombre de la Etapa" />
		<display:column title="Color" media="html">
			<div style="background:${reportTable.color};">&nbsp;</div>
		</display:column>
		<display:column property="tipo" title="Tipo" />
		<display:column property="manual" title="Manual"
			decorator="com.asf.displayDecorators.BooleanDecorator" />
	</display:table>
</div>
