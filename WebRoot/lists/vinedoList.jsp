<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<link rel="stylesheet" href="/CREDITOS/style/frames.css" type="text/css">

<html:form action="actions/abmAction.do?entityName=${param.entityName}">
  <html:hidden property="paramValue[0]" value="${paramValue[0]}" />
  <html:hidden property="paramName[0]" value="credito.id" />
  <html:hidden property="paramComp[0]" value="=" />
  <html:hidden property="entity.credito.id" value="${paramValue[0]}" />
  <html:hidden property="do" styleId="accion" value="newEntity" />
  <asf:security action="/actions/abmAction.do"
    access="do=newEntity&entityName=${param.entityName}&filter=true&paramValue[0]=${paramValue[0]}"
  >
    <button type="submit">
      <bean:message key="abm.button.new" />
    </button>
  </asf:security>
</html:form>
<br>
<br>
<display:table name="ListResult" property="result" export="false" id="reportTable" defaultsort="1"
  requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
  <display:setProperty name="report.title" value="Administración de Viñedos"></display:setProperty>
  <display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
    href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}"/>
  <display:column property="codigo" title="C.I.V." sortable="true" />
  <display:column property="vinedo.hectareas" title="Has. Afectadas" sortable="true" />
  <display:column property="qqestimados" title="QQ Estimados" sortable="true" />
  <display:column property="observaciones" title="Observaciones" sortable="true" />
  <display:column property="departamento" title="Departamento" sortable="true" />
  <display:column property="localidad.nombre" title="Localidad" sortable="true" />
  <display:column media="html" honClick="return confirmDelete();" title="Eliminar"
      href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}&filter=true&paramValue[0]=${paramValue[0]}&paramName[0]=${paramName[0]}&paramComp[0]=%3D"
      paramId="entity.id" paramProperty="id">
      <bean:message key="abm.button.delete" />
  </display:column>


</display:table>
