<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>

<div class="title">Consulta de Deudores Varios--.</div>
<br><br>
<html:form action="/actions/process.do?do=process&processName=DeudoresVariosList" styleId="ProcessForm">
<html:hidden property="process.cid"/>

<table>
  <tr>
    <th>Persona</th>
    <td><asf:selectpop name="process.idPersona" title="Persona"
				columns="C�digo, Apellido y Nombres, Nro. Documento, C.U.I.L"
				captions="idpersona,nomb12,nudo12,cuil12" values="idpersona"
				entityName="com.civitas.hibernate.persona.Persona" 
				value="${ProcessForm.process.idPersona}"/></td>
  </tr>
  
  <tr> 
		<th>Concepto</th>
	     <td><asf:select entityName="com.nirven.creditos.hibernate.CConcepto" listCaption="id,detalle"
	            listValues="id" name="process.concepto.id" value="${ProcessForm.process.concepto.id}" 
	            orderBy="detalle ASC" nullValue="true" nullText="Todos"/>
         </td>
  </tr>
  
  <tr> 
		<th>Tipo de Concepto</th>
	    <td><asf:select entityName="com.asf.hibernate.mapping.Tipificadores" listCaption="codigo,descripcion"
	            listValues="codigo" name="process.tipificadores.codigo" value="${ProcessForm.process.tipificadores.codigo}" 
	            filter="categoria IN('ctacte.detalle')"  orderBy="descripcion ASC" nullValue="true" nullText="Todos"/>
        </td>
  </tr>
  
   
  <tr>
    <th>Numero de atenci&oacute;n</th>
    <td><html:text property="process.numeroAtencion"/></td>
  </tr>
  <tr>
    <th>Numero de boleto</th>
    <td><html:text property="process.numero"/></td>
  </tr>
  <tr>
    <th>Pagados</th>
    <td>
    	<html:select property="process.pagados">
    		<html:option value="false">No</html:option>
    		<html:option value="true">Si</html:option>
    	</html:select>
    </td>
  </tr>
  <tr>
  	<th colspan="2"><html:submit>Buscar</html:submit></th>
  </tr>
</table>

<br><br>
<div class="grilla">
<display:table name="ProcessForm" property="process.resultado" export="true" id="reportTable" requestURI="${pageContext.request.contextPath}/actions/process.do?do=process&processName=DeudoresVariosList">
	<display:setProperty name="report.title" value="Consulta de Deudores Varios"></display:setProperty>

    <logic:equal name="ProcessForm" property="process.pagados" value="true">
    	<display:column title="Numero">
    		${reportTable.numero }
    	</display:column>
    </logic:equal>
    
    <logic:equal name="ProcessForm" property="process.pagados" value="false">           	
    	<display:column title="Numero">
    		<a href="${pageContext.request.contextPath}/actions/process.do?do=process&processName=DeudoresVariosProcess&process.accion=reimprimir&process.idDeudoresVarios=${reportTable.id}">
	   		${reportTable.numero }
	   		</a> 
    	</display:column>
    </logic:equal>
   	
    <display:column property="tipoEmision.texto" title="Origen"/>
    <display:column property="nombreApellido" title="Nombre o Razon Social" sortable="true"/>
    <display:column property="descCuit" title="CUIT" sortable="true"/>
    <display:column property="descEmail" title="Email" sortable="true"/>
    <display:column property="descTelefono" title="Telefono" sortable="true"/>
    <display:column property="numeroAtencion" title="Numero de Atencion" sortable="true"/>
    <display:column property="conceptoNombre" title="Concepto" sortable="true"/>
    <display:column property="tipoConceptoNombre" title="Tipo de Concepto" sortable="true"/>
    <display:column property="expediente" title="Expediente" sortable="true"/>
    <display:column property="fechaEmision" title="Fecha de Emision" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
    <display:column property="fechaVto" title="Fecha de Vencimiento" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
    <display:column property="moneda.denominacion" title="Moneda" sortable="true"/>
    <display:column property="importe" title="Importe" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
    <display:column property="fechaPago" title="Fecha Pago" sortable="true" decorator="com.asf.displayDecorators.DateDecorator"/>
    <display:column property="importePagado" title="Importe Pago" sortable="true" decorator="com.asf.cred.decorator.SeparatorDoubleDecorator"/>
    
</display:table>
</div>
</html:form>