<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>

<div class="title">Bonificaciones - Reporte de Alta de Cr�ditos Bonificados</div>

<div style="overflow: auto;" class="grilla"> 

<html:form	action="/actions/process.do?do=process&processName=${param.processName}" styleId="oForm">

	<div style="overflow: auto;" class="grilla"> 
	
		<table border="0">
			<tr>
				<th>Ente Bonificador:</th>
				<td><asf:select entityName="com.asf.gaf.hibernate.Bancos"
						name="process.bancoId" value="${ProcessForm.process.bancoId}"
						listCaption="getDetaBa" listValues="getCodiBa"
						filter="bonificador = 1  order by codiBa" nullValue="true" nullText="Todos" />
				</td>
			</tr>
			
			<tr>
				<th>Convenio:</th>
				<td><asf:select entityName="com.nirven.creditos.hibernate.ConvenioBonificacion"
						listCaption="nombre" listValues="id" nullValue="true" nullText="Todos"
						name="process.convenioBonificacionId" value="${ProcessForm.process.convenioBonificacionId}" />
				</td>
			</tr>
			
			<tr>
				<th>Fecha Desde</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaDesde"></asf:calendar>
				</td>
			</tr>
			
			<tr>
				<th>Fecha Hasta</th>
				<td>
					<asf:calendar property="ProcessForm.process.fechaHasta"></asf:calendar>
				</td>
			</tr>
	
		</table>		
		
		<br />
			<input type="button" value="Buscar" onclick="buscar();"/>
		<br />
	
	</div>
	<html:hidden name="ProcessForm" property="process.accion" styleId="accion" />

</html:form>



</div>

<script language='javascript'>
	function buscar() {
		var form = $('oForm');
		accion.value = 'buscar';
		form.submit();	
	}

</script>

<iframe width=174 height=189 name="gToday:normal:agenda.js"
	id="gToday:normal:agenda.js"
	src="${pageContext.request.contextPath}/calendar/ipopeng.htm"
	scrolling="no" frameborder="0"
	style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;"></iframe>
	
