<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c"%>
<%@ taglib uri="/WEB-INF/fmt-1_0.tld" prefix="fmt"%>

<div class="title">Administración de Entidades Financieras</div>
<br><br>

<html:form action="/actions/abmAction.do?entityName=${param.entityName}" styleId="formulario">
	<html:hidden property="do" styleId="do" value="list"/>

	<asf:security action="/actions/abmAction.do" access="do=newEntity&entityName=${param.entityName}">
	    <input type="button" onclick="nuevo();" value="Nuevo"/>
	</asf:security>
</html:form>
<br><br>
<div class="grilla">
<display:table name="ListResult" property="result" export="true" id="reportTable" defaultsort="1"
	requestURI="${pageContext.request.contextPath}/actions/abmAction.do?do=list&entityName=${param.entityName}">
	<display:setProperty name="report.title" value="Administración de Entidades Financieras"></display:setProperty>  

	<display:column property="id" title="ID" sortProperty="id" paramId="entity.id" paramProperty="id" sortable="true"
		href="${pageContext.request.contextPath}/actions/abmAction.do?do=load&entityName=${param.entityName}"/>
	<display:column property="codigoEntidad"      title="Código"      sortable="true"/>
	<display:column property="nombreEntidad"      title="Nombre"      sortable="true"/>
	<display:column property="descripcionEntidad" title="Descripción" sortable="true"/>
<asf:security action="/actions/abmAction.do" access="do=delete&entityName=${param.entityName}">
	<display:column media="html" honClick="return confirmDelete();"
		title="Eliminar" href="${pageContext.request.contextPath}/actions/abmAction.do?do=delete&entityName=${param.entityName}"
		paramId="entity.id" paramProperty="id" ><bean:message key="abm.button.delete" />
	</display:column>
</asf:security>
</display:table>
</div>

<script language="javascript">
	var accion     = $('do');	
	var formulario = $('formulario');
	
	function nuevo()
	{
		accion.value = "newEntity";
		formulario.submit();
	}
</script>