/*
 * Para ocultar la grilla y entrar al perfil
 */
function verSeleccion() {	
	$j('.beta-cuerpo').empty();		
	$j('.contenedor-menu-lateral').removeClass('oculto');
	$j('.contenedor-dtable').addClass('oculto');
}

/*
 * Para volver a la grilla
 */
function volverBusqueda() {
	location.reload();
	/*$j('.contenedor-menu-lateral').addClass('oculto');
	$j('.contenedor-dtable').removeClass('oculto');*/
	
}

/*
 * Retorna referencia a la foto de la persona
 */
function fotoArchivo(idArchivo) {	
	 return "<img class='img-responsive' id='img' src='"+pathSistema+"/actions/ArchivoAction.do?do=ver&id="+idArchivo+"'/>";
}

function fotoArchivoDocumento(idArchivo,idTipoArchivo) {	
	if(idTipoArchivo==3){
	 return "<img class='img-responsive' id='img' src='"+pathSistema+"/actions/ArchivoAction.do?do=ver&id="+idArchivo+"'/>";
	}else{
	 return "<img class='img-responsive' img-dtable' src='"+pathSistema+"/images/file.png'>";	
	}
}

/*
 * Retorna una imagen generica dependiendo del sexo de la persona (M/F)
 */
function addImageTable (sexo) {
	var img = "<img class='img-responsive img-dtable' src='"+pathSistema+"/images/";
	if (sexo != null && sexo != "" && sexo == 'F'){
		img += "user-f.png";
	}
	else {
		img += "user-m.png";
	}
	img += "'/>";
	return img;
}

/*
 * Definimos los botones en la grilla
 
function addButtonDt(idElemento,idArchivo,sexo) {
    var dtConsultar = "<div class=\"campo-btn\"><span class='dtBotones dtConsultar' onclick='verSeleccion(); actualizarPerfil("+idArchivo+",\""+sexo+"\",\""+idElemento+"\");'></span>";
    var dtEliminar = "<span class='dtBotones dtEliminar' onclick='eliminarSeleccion("+idElemento+")'></span></div>";
    return dtConsultar+dtEliminar;
}
*/
function addButtonDt(fuConsultar, fuEliminar) {
	var dtConsultar = "<div class=\"campo-btn\"><span class='dtBotones dtConsultar' onclick='"+fuConsultar+";'></span>";
    var dtEliminar = "<span class='dtBotones dtEliminar' onclick='"+fuEliminar+";'></span></div>";
    return dtConsultar+dtEliminar;
}

function addButtonDtc(fuConsultar) {
	var dtConsultar = "<div class=\"campo-btn\"><span class='dtBotones dtConsultar' onclick='"+fuConsultar+";'></span>";    
    return dtConsultar;
}

function addButtonDocum(fuConsultar, fuEliminar, fuDescargar){
	var dtConsultar = "<div class=\"campo-btn\"><span class='dtBotones dtConsultar' onclick='"+fuConsultar+";'></span>";
    var dtEliminar = "<span class='dtBotones dtEliminar' onclick='"+fuEliminar+";'></span>";
    var dtDescargar = "<span class='dtBotones dtDescargar' onclick='"+fuDescargar+";'></span></div>";
    return dtConsultar+dtEliminar+dtDescargar;
}

/*
 * Definimos una etiqueta de estado (activo/inactivo)
 */
function addStatus(activo) {
	if(activo) {
		return "<h5><span class='label label-success'>Activo</span></h5>";
	}
	else{
		return "<h5><span class='label label-warning'>Inactivo</span></h5>";
	}		
}

function calificacionStars(valor) {
	var promedio = 0;
	if(Number(valor) >= 0 && Number(valor) <= 100) {
		promedio = Number(valor);
	}		
	var rating = "<div class=\"star-rating\">";
		rating += "<div class=\"star-rating-top\" style=\"width: "+promedio+"%\"> <span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>";
		rating += "<div class=\"star-rating-bottom\"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>";
		rating += "</div>";		
	return rating;
}

// function actualizarDataTable(nombreVariable, datosJson)
function actualizarDataTable(datosJson) {
	dataTable.clear();
    dataTable.rows.add(datosJson);
    dataTable.draw();
}

function actualizarGrillaDataTable(dataTable, datosJson) {
	dataTable.clear();
	if( datosJson != null && datosJson != "" ) {
		dataTable.rows.add(datosJson);
	}    
    dataTable.draw();
}

function validarSiNo(valor){	
	if(valor == 1 || valor == "1" || valor == true || valor == "true") {
		return "SI";
	}
	else {
		return "NO";
	}
}

function limpiarFormulario(idForm) {
	debugger
	var formulario = "#"+idForm+" :input";
	var $inputs = $j(formulario);
	$inputs.each(function(){
		if ( $j(this).prop("tagName") == "SELECT") {
			debugger
			var idCombo = "#"+$j(this).attr('id')+" option:first";
			$j(this).val( $j(idCombo).val() ).trigger("change");
		}
		else{
			debugger
			if($j(this).prop("type") == "checkbox"){
				$j(this).val(0);
				$j(this).lcs_off();
			}else{
			  $j(this).val("");
			}
		}
	});
	
    
	/*$inputs.each(function() {
    	if ( $j(this).prop("tagName") == "SELECT") {
    		debugger
    		var idCombo = "#"+$j(this).attr('id')+" option:first";
    		//$j(this).val( $j(idCombo).val() ).trigger("change");para componente select2
    		$j(this).val($j(idCombo).val());
    	}
		else{
			debugger
    		$j(this).val("");
    	}    	        
    });*/
}