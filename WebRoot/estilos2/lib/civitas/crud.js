crud = {
	
	errors: function(responseText){
		var responseJSON = JSON.parse(responseText);		
		$j.each(responseJSON.errors, function( index, value ) {	
			$j( '<span class="error">' + value + '</span>' ).insertAfter( "#" + index );
		});
	},
	
	success: function () {
		alert('Se han guardado los cambios correctamente');
	},
	
	saveBefore: function(button){
		button.attr("disabled", true);
		$j(".error").remove();
	},
	
	save: function(params, callback, prepare){
		var defaults = {
			url: null,
			saveButtonId: "#btn-save",
			formId: "#form",
		};
		param = $j.extend(defaults, params);
    	if($j.isFunction(prepare)){
    		prepare.call();
    	}
		
		var form = $j(param.formId);
		var button = $j(param.saveButtonId);
		var slzData = form.serializeArray();
		
	    jQuery.each( slzData, function( i, field ) {
	    	var uname = field.name.replace("_", ".");
	    	slzData[i].name = uname;
	      });	
		crud.saveBefore(button);
		HoldOn.open();
		jQuery.ajax({
	        type: "POST",
	        async: true,
	        url: param.url,
	        data:  slzData,
	        success: function (data){ 
	        	crud.saveAfter(button);
	        	if($j.isFunction(callback)){
	        		callback.call();
	        	}
	        	HoldOn.close();
        	},
	        error: function(data) {
	        	crud.errors(data.responseText);
	        	crud.saveAfter(button);
	        	HoldOn.close();
        	}
	    });
	},
	
	saveAfter: function(button) {
		button.prop("disabled", false);
	},
	
	deleteBefore: function () {},
	
	deleteP: function () {},
	
	deleteL: function (params, callback) {

		var defaults = {
		url: null,
		id: 0,
		dTable: null,
		title: "Eliminar elemento",
		html: "&iquest;Desea eliminar el registro?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#3eaf23',
		cancelButtonColor: '#d33',
		confirmButtonText: 'ACEPTAR',
		cancelButtonText: 'CANCELAR'
		};
		param = $j.extend(defaults, params);
		
		swal({
			  title: param.title,
			  html: param.html,
			  type: param.type,
			  showCancelButton: param.showCancelButton,
			  confirmButtonColor: param.confirmButtonColor,
			  cancelButtonColor: param.cancelButtonColor,
			  confirmButtonText: param.confirmButtonText,
			  cancelButtonText: param.cancelButtonText
			}).then((value) => {
				
				HoldOn.open();
				jQuery.ajax({
			        type: "POST",
			        async: true,
			        url: param.url,
			        data:  {id : param.id},
			        success: function (data){
			        	if(param.dTable != null){
			        		param.dTable.ajax.reload();
			        	}
			        	HoldOn.close();
		        	},
			        error: function(data) { 
			        	/*
			        	crud.errors(data.responseText);
			        	crud.saveAfter(button);
			        	*/
			        	HoldOn.close();
		        	}
			    });  
			})
	},
	
	deleteAfter: function () {},	
	
	populate: function(params, callback){
		
		var defaults = {
			url: null,
			id: null,
			entidad: null,
			formId: "#form"
		};
		
		HoldOn.open();
		
		param = $j.extend(defaults, params);
		var form = $j(param.formId);
		var button = $j(param.saveButtonId);
		var slzData = form.serializeArray();
		var strCampos = "";
		var firstElement = true;
		
	    jQuery.each( slzData, function( i, field ) {
	    	if(firstElement == true){
	    		firstElement = false;
	    	}else{
	    		strCampos += ",";
	    	}
	    	strCampos += field.name.replace("_", ".");
	      });
		jQuery.ajax({
	        type: "POST",
	        async: true,
	        url: param.url,
	        data: {
	        	id: param.id,
	        	entidad: param.entidad,
	        	campos: strCampos,
	        	filtro: "id=" + param.id
	        },
	        success: function (response){	        	
	    		response = JSON.parse(response);
	    		if(typeof(response.data) != "undefined"){
		    		$j.each(response.data[0], function( key, value ) {
			    		$j(param.formId +' input, ' + param.formId +' select, ' + param.formId + ' textarea').each(function(index){  
			    			var field = $j(this);
			    			if(field.attr('id') === key){		    							    			
				    			if(field.is(':checkbox')){
				    				if(field.attr('data-crud-populate') != 'disabled'){
				    					field.prop('checked', (value == 'true'));
				    				}				    				
				    			}else if(field.is('textarea')){
				    				if(field.attr('data-crud-populate') != 'disabled'){
				    					field.text(value);
				    				}				    				
				    			}else{
				    				console.log(field.attr('id'));
				    				console.log(field.attr('data-crud-populate'));
				    				if(field.attr('data-crud-populate') != 'disabled'){
				    					field.val(value);
				    				}				    				
				    			}
			    			}
			    		});	
		    		});	    			
	    		}
	    		HoldOn.close();
	    		
	        	if($j.isFunction(callback)){
	        		callback.call();
	        	}
        	},
	        error: function(data) { 
	        	HoldOn.close();
        	}
	    });
	},	
	
    _getDataForm: function() {

        var data = {};
        var form = document.getElementById('saveForm');

        Array.prototype.forEach.call(form.children, function(child) {
            var name = child.getAttribute('name');
            if ('value' in child && child.type !== 'submit' && name) {
                data[name] = child.value;
            }
        });

        return data;

    },

    saveResult: function(response, successCallBack, errorCallBack) {
        var response = JSON.parse(response);

        //Status result OK
        if (response.status === 200) {

            if ($j("#objectID").length > 0) {
                $j('#objectID').val(response.data.objectID);
            }

            if (successCallBack !== null && successCallBack !== undefined) {
                successCallBack(response);
            }
        } else {

            if (errorCallBack !== null && errorCallBack !== undefined)
                errorCallBack(response);

        }
    },
    
    get: function (url, dataTable){
    	dataTable.clear();
    	$j.post(url, function(response){
        	if( response != null && response != "") {
        		dataTable.rows.add(JSON.parse(response));
        	}
        	dataTable.draw();
    	}).fail(function(response) {
        });    	
    },    

    goTo: function go(url){
        window.location = url;
    }
};