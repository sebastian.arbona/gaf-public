/*
    <input id="forzado" type="checkbox" name="entity.forz30"  class="lcs_check" value="${AbmForm.entity.forz30}">
*/

/**
*	Manejador de Componente LC Switch
*/
function inicializarLCSwitch() {
	$j('body').delegate('.lcs_check', 'lcs-statuschange', function() {
		try {
			var status = ($j(this).is(':checked')) ? 'checked' : 'unchecked';
		    $j(this).val( (status == 'checked') ? 1 : 0 );
		} catch (e) {
			console.log("ERROR CHECK-LCS: "+$j(this).attr('name'));
		}		    
	});
}

function defineCheckboxLcs( idElement ) {
	var valor = $j('#'+idElement).val();
	
	$j('#'+idElement).lc_switch('SI','NO');
	
	if( valor == "1" || Number(valor) == Number(1) || valor == true || valor == "true" ) {
		$j('#'+idElement).lcs_on();
	}
	else {
		$j('#'+idElement).lcs_off();
	}
}

function validarForm( formulario ) {
	try {
		var $inputs = $j(formulario.getElementsByTagName("INPUT"));	
	    $inputs.each(function() {
	    	if($j(this).prop("type") == "checkbox"){	    		
				$j(this).attr('type', 'text');
			}
	    });
	    return true;
	} catch (e) {
		console.log("ERROR AL VALIDAR FORMULARIO");
		console.log(e);
		return false;
	}
}
/**
*
*/