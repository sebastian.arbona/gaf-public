/**
 * Definimos la estructura base por default para los botones de exportacion
 * title: "Nombre del titulo"
 * orientation: 'landscape' 
 */
var dt_botonesDefault = [	   
	{
		extend: 'excel',
		text: 'Imprimir XLS',
		exportOptions: {
			//columns: [ 0,1,2,3,4]
		}
	},
	{
		extend: 'pdfHtml5',
		text: 'Imprimir PDF',
		pageSize: 'A4',
		exportOptions: {
			//columns: ':visible'
			//columns: [ 0,1,2,3,4]
		}                 
	}
];

/**
 * Funcion donde definimos diferentes estructuras segun nuestras necesidades
 * @param {Number} tipo
 * searching: true			->	Visualiza campo Search
 * bLengthChange: true		->	Visualiza combo Mostrar X registros
 * iDisplayLength: 5		->	Cantidad de campos por defecto al iniciar tabla
 * lengthMenu: [5, 10]		->	Opciones de cantidad de registros a ver
 * dom: 'Bfrtip'			->	Utilizamos <'class'> como contenedores para ubicar los botones
 */
function definirEstructura( tipo, datos, personalizados ) {
	var estructura = personalizados;
	estructura.language = languajedt_es;
	estructura.data = (datos != null &&  datos != "") ? datos : "";
	estructura.responsive = true;
	switch( Number (tipo) ) {
		case Number (1):
			estructura.searching = true;
			estructura.bLengthChange = true;
            if (typeof estructura.iDisplayLength == "undefined") {
            	estructura.iDisplayLength = 5;
            }
            if (typeof estructura.lengthMenu == "undefined") {
            	estructura.lengthMenu = [ [5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100 , "Todos"] ];
            }
			estructura.dom = "<'row'<'col-sm-6'f><'col-sm-6 print_datatable'Bl>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";
			break;
		case Number (2):
	        estructura.searching = false;
			estructura.bLengthChange = false;
			estructura.dom = "<'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";
			break;
		case Number (3):
			estructura.searching = true;
			estructura.bLengthChange = true;
            if (typeof estructura.iDisplayLength == "undefined") {
            	estructura.iDisplayLength = 5;
            }
            if (typeof estructura.lengthMenu == "undefined") {
            	estructura.lengthMenu = [ [5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100 , "Todos"] ];
            }
			estructura.dom = "<'row'<'col-sm-6'f><'col-sm-6 print_datatable'Bl>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";
			estructura.createdRow = function(row, data, dataIndex){initRating(row);}; /* Necesita libreria jquery.raty */
			break;			
		case Number (4): /* Para ventanas modal (faltan prop definir)*/
			estructura.searching = true;
			estructura.bLengthChange = true;
			estructura.scrollY = "500px";
			estructura.scrollCollapse = true;
			estructura.paging = false;
			estructura.dom ="<'top'f>rt<'bottom'i><'clear'>";
			break;
		case Number (6): /* Para recargas ajax */
            estructura.processing = true;
            estructura.bServerSide = true;
            estructura.searching = true;
            estructura.bLengthChange = true;
            if (typeof estructura.iDisplayLength == "undefined") {
            	estructura.iDisplayLength = 5;
            }
            if (typeof estructura.lengthMenu == "undefined") {
            	estructura.lengthMenu = [ [5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100 , "Todos"] ];
            }
            
            estructura.dom = "<'row'<'col-sm-6'f><'col-sm-6 print_datatable'Bl>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>";
            estructura.sServerMethod = "POST";
            break;
		default:
			estructura = estructura;
	}

	//console.log(estructura);

	return estructura;
}

/**
 * Inicializacion de una grilla DataTable.js base
 * @param idElemento, tipo, datos, columns, columnDefs, buttons
 * @return object
*/
function inicializarDatatable( idElemento, tipo, datos, personalizados ) {
	var ejecuta = definirEstructura( tipo, datos, personalizados );
	return $j('#'+idElemento).DataTable( ejecuta );
}

/**
 * Funcion Generica que refresca los datos de una grilla en el componente DataTable.js
 * @param object
 * @param json
 * @return
*/
function refrescar_grilla_dataTable( dataTable, datos ) {
	dataTable.clear();
	if( datos != null && datos != "") {
		dataTable.rows.add( datos );
	}
	dataTable.draw();
}

function getDatosDatatable( dataTable ) {
	try {		
		return dataTable.rows().data();
	} catch (error) {
		return null;
	}
}

/**
 * 
 * @param arrayAcciones
 * @example [ ["editar","funcionEditar"], ["eliminar","funcionEliminar"] ]
 */
function acciones_dataTable( arrayAcciones ) {	
	var acciones = "<div class=\"campo-btn-cuadricula\">";
	$j.each( arrayAcciones, function( i, valor ){
		acciones += addAccion(arrayAcciones[i][0],arrayAcciones[i][1]);
	});
	acciones += "</div>";
	return acciones;
}

/**
 * Funcion que retorna estructura html de la accion que se indica
 * @param accion 
 * @param funcion 
 */
function addAccion (accion, funcion) {	
	var def_accion = "";
	switch(accion) {
		case "editar":
			def_accion = "<span class='dtBotones dtEditar' onclick='"+funcion+";'></span>";
			break;
		case "eliminar":
			def_accion = "<span class='dtBotones dtEliminar' onclick='"+funcion+";'></span>";
			break;
		case "restar":
			def_accion = "<span class='dtBotones dtRestar' onclick='"+funcion+";'></span>";
			break;
		case "agregar":
			def_accion = "<span class='dtBotones dtAgregar' onclick='"+funcion+";'></span>";
			break;
		case "siguiente":
			def_accion = "<span class='dtBotones dtSiguiente' onclick='"+funcion+";'></span>";
			break;
		case "documento":
			def_accion = "<span class='dtBotones dtDocument' onclick='"+funcion+";'></span>";
			break;
		case "pdf":
			def_accion = "<span class='dtBotones dtPdf' onclick='"+funcion+";'></span>";
			break;
		case "consultar":
			def_accion = "<span class='dtBotones dtConsultar' onclick='"+funcion+";'></span>";
			break;
		case "bajar":
		case "descargar":
		case "download":
			def_accion = "<span class='dtBotones dtDownload' onclick='"+funcion+";'></span>";
			break;
		case "up":
			def_accion = "<span class='dtBotones dtUp' onclick='"+funcion+";'></span>";
			break;
		case "down":
			def_accion = "<span class='dtBotones dtDown' onclick='"+funcion+";'></span>";
			break;
		case "select":
			def_accion = "<span class='dtBotones dtSelect' onclick='"+funcion+";'></span>";
			break;
		default:
			def_accion = "";
	}
	return def_accion;
}

/*
 * Definimos una etiqueta de estado (activo/inactivo)
 */
function addStatus(activo) {
	if(activo) {
		return "<h5><span class='label label-success'>Activo</span></h5>";
	}
	else{
		return "<h5><span class='label label-warning'>Inactivo</span></h5>";
	}		
}

/*
 * Carga de estrellas
 */
function calificacionStars(valor) {
	var promedio = 0;
	if(Number(valor) >= 0 && Number(valor) <= 100) {
		promedio = Number(valor);
	}		
	var rating = "<div class=\"star-rating\">";
		rating += "<div class=\"star-rating-top\" style=\"width: "+promedio+"%\"> <span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>";
		rating += "<div class=\"star-rating-bottom\"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>";
		rating += "</div>";		
	return rating;
}

/**
 * Funcion que retorna la palabra SI / NO en funcion de otros valores representativos
 * @param {*} valor 
 */
function validarSiNo(valor){	
	if(valor == 1 || valor == "1" || valor == true || valor == "true") {
		return "SI";
	}
	else {
		return "NO";
	}
}

/**
 * Initializes jQuery Raty control
 */
function initRating(container) {
    $j('span.rating', container).raty({
        half: true,
        starHalf: 'https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images/star-half.png',
        starOff: 'https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images/star-off.png',
        starOn: 'https://cdnjs.cloudflare.com/ajax/libs/raty/2.7.0/images/star-on.png',
        score: function(){
            return $j(this).data('score');
        },
        click: function(score){
            $j(this).data('score', score);
        }
	});
}

/*
 * Retorna una imagen generica dependiendo del sexo de la persona (M/F)
 */
function addImageTable (sexo) {
	var img = "<img class='img-responsive img-dtable' src='"+pathSistema+"/images/";
	if (sexo != null && sexo != "" && sexo == 'F'){
		img += "user-f.png";
	}
	else {
		img += "user-m.png";
	}
	img += "'/>";
	return img;
}