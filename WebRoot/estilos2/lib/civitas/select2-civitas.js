/**
 * Funcion Generica Simple para cargar los componentes Select2 via Ajax.-
 * @param string, string, string, array, string, string, string.-
 * @return
 * Ejemplo de uso: 
 * cargarSelect2( "test", "Institucional", "id", ["codigo","denominacion"], "imputable,ejercicio", "imputable=1 and ejercicio=2018", "ejercicio desc" );
*/
function cargarSelect2( elementoClass, entidad, identificador, descripcion, campos, filtro, orden, conBusqueda ) {
	var estructura_campos = identificador;
	$j.each( descripcion, function( i, valor ){
		estructura_campos += ","+valor;
	});
	if( campos!=null && campos!="" ) {
		estructura_campos += ","+campos;
	}
	
	var jsonObject = {
			"entidad": entidad ,
			"campos": estructura_campos,
			"filtro": filtro ,
			"orden": orden
	};
	
	if( entidad!=null && entidad!="" && identificador!=null && identificador!="" &&  descripcion!=null && descripcion!="" && estructura_campos!=null && estructura_campos!="") 
	{	
		return $j.ajax({
			url : pathSistema+"/actions/gafAjaxHelperAction.do?do=getListJson",
			type: 'POST',
			data: jsonObject,
	 		dataType : 'json'		
	    })
	    .done(function(json) {	    	
	    	if(json != null && json != ""){
	    		var datosModif = $j.map(json, function (obj) {	    			
	    			obj.id = obj.id || obj[identificador];
	    			
	    			var desc_opcion = "";	    			
	    			$j.each( descripcion, function( i, valor ){
	    				desc_opcion += obj[valor] + " - ";
	    			});
	    			desc_opcion = desc_opcion.slice(0, -3);
	    				    			
	    			obj.text = obj.text || desc_opcion;
	    			
	    			return obj;
	    		});
	    		$j("."+elementoClass).select2({
	    			minimumResultsForSearch: ( conBusqueda ? 0 : -1 ),
	    			allowClear: true,
	    			data : datosModif
	    		});	
			}
		})
		.fail(function(xhr) {
			mensajeModal('Error!', 'Se prudujo un error en la consulta.', 'error');
		});
	}	
}

/**
 * Funcion que vacia combo Select2
 */
function vaciarClassSelect(classElemento) {
	$j("."+classElemento+" option").each(function() {
	    $(this).remove();
	});
	$j("."+classElemento).append('<option></option>');
}

function vaciarIdSelect(idElemento) {
	$j("#"+idElemento+" option").each(function() {
	    $(this).remove();
	});
	$j("#"+idElemento).append('<option></option>');
}

function cargarClassSelect(classElemento, datosJson) {
	$j("."+classElemento).html('').select2({data: datosJson});
}

function inicializarSelect2() {
	try {
		$j(".ini-select2").select2({
			minimumResultsForSearch: -1
		});
	} catch (e) {
		
	}
	
}

/**
 * Funcion que a partir de un combo ya cargado, obtengo el objeto seleccionado
 * @param classElemento
 * @returns
 */
function getObjectSelect2( classElemento ) {
	var objecto = null;
	try {
		objecto = $j("."+classElemento).select2('data')[0];
	} catch (e) {
		objecto = null;
		console.log("Error al obtener el elemento seleccionado del combo: "+classElemento);
	}
	return objecto;
}

/************************************************************************************************************************
 * **********************************************************************************************************************
 * Funciones de carga de combo en GAF
 * @param json.-
 * @return
 * Ejemplo de uso: 
 * cargarSelect2( "class_elemento", "Entidad", "id", ["detalle1","detalle2"], "detalle3,detalle4", "filtro", "orden", conBusqueda );
*/

function cargarSelectEjercicio() {
	return cargarSelect2( "select-ejercicio", "Ejercicio", "id", ["denominacion"], null, null, "ejercicio desc", false );
}

function cargarSelectPlanCta( ejercicio ) {
	return cargarSelect2( "select-plancta", "Plancta", "idplancta", ["codigo","denominacion"], null, "ejercicio="+validarEjercicio(ejercicio) , null, true );
}

function cargarSelectPlanCtaCodigo( ejercicio ) {
	return cargarSelect2( "select-plancta", "Plancta", "codigo", ["codigo","denominacion"], null, "ejercicio="+validarEjercicio(ejercicio) , null, false );
}

function cargarSelectEtapa( ejercicio ) {
	return cargarSelect2( "select-etapa", "Etapa", "idetapa", ["orden","denominacion"], "abreviatura", "ejercicio="+validarEjercicio(ejercicio), null, false );
}

function cargarSelectTipoExpediente( filtro ) {
	return cargarSelect2( "select-tipoexpediente", "Tipoexpediente", "id", ["id","denominacion"], null, filtro, null, false );
}

function cargarSelectInstitucional( filtro ) {
	return cargarSelect2( "select-institucional", "Institucional", "idinstitucional", ["codigo","denominacion"], null, filtro, null, true );
}

function cargarSelectIegreso( filtro ) {
	return cargarSelect2( "select-iegreso", "Iegreso", "idiegreso", ["codigo","denominacion"], null, filtro, null, true );
}

function cargarSelectNomenclador( filtro ) {
	return cargarSelect2( "select-nomenclador", "Nomenclador", "idnomenclador", ["codigo","denominacion"], null, filtro, null, true );
}

function cargarSelectProveedor() {
	return cargarSelect2( "select-proveedor", "Proveedor", "idproveedor", ["codigo","fantasia"], null, null, null, false );
}

function cargarSelectTipoComprobanteExt() {
	return cargarSelect2( "select-compexterno", "Tipificadores", "codigo", ["codigo","descripcion"], null, "categoria='TipoComprobanteExt'", null, false );
}

function cargarPartidaEgreinsumo( idInsumo ) {
	return cargarSelect2( "select-iegreso", "Egreinsumo", "idiegreso", ["Iegreso.codigo","Iegreso.denominacion"], null, "Iegreso.imputable=1 and idinsumo="+idInsumo+" and Iegreso.ejercicio="+periodoActual, null, true );
}

function cargarSelectBanco( filtro ) {
	return cargarSelect2( "select-banco", "Bancue", "idbancue", ["idbancue","cuentabancaria"], null, filtro, null, true );
}

/**
 * 
 */
function inicializarSelectDoc() {	
	$j(".select-docade").select2({		
		ajax : {
			url : pathSistema+"/actions/ajaxHelperAction.do?do=getDocumentosAde",
			dataType : 'json',
			delay : 250,
			data : function(params) {
				return {
					filtro : params.term,
					entidad : "Expegasto",
					campos : "expediente,denominacion",
					cantidad : 30,
					tipoExpediente : $j(".select-tipoexpediente").val()
				};
			},
			processResults : function(data, params) {
				return {
					results : data
				};
			},
			cache : true
		},
		escapeMarkup : function(markup) {
			return markup;
		},
		minimumInputLength : 4,
		allowClear: true
	});
}

function formatoExpedienteAde (option) {   
	if (!option.id) { return option.text; }
   	var $option;
   	if(option.imputado) {
   		$option = $j('<span class="bold color_expegasto">' + option.text + '</span>');
   	}
   	else {
   		$option = option.text;
   	}	
	return $option;
}

function inicializarSelectComprobante() {
	$j(".select-comprobante").select2({
		ajax : {
			url : pathSistema+"/actions/gafAjaxHelperAction.do?do=getComprobantes",
			dataType : 'json',
			delay : 250,
			data : function(params) {
				return {
					numero : params.term,
					entidad : "Comprobante",
					idtipocomprobante : 5,
					cantidad : 30					
				};
			},
			processResults : function(data, params) {				
				return {
					results : data
				};
			},
			cache : true
		},
		escapeMarkup : function(markup) {
			return markup;
		},
		minimumInputLength : 1,
		allowClear: true
	});
}

function buscarUltimosComprobante( classElemento, jsonObject ) {
	return $j.ajax({
		url : pathSistema+"/actions/gafAjaxHelperAction.do?do=getComprobantesPorEjercicio",
		type: 'POST',
		data: jsonObject,
 		dataType : 'json'		
    })
    .done(function(json) {	    	
    	if(json != null && json != ""){    		
    		//$j("."+classElemento).html('').select2({data: json});
    		$j("."+classElemento).select2({
    			minimumResultsForSearch: 0,
    			allowClear: true,
    			data : json
    		});
		}
	})
	.fail(function(xhr) {
		mensajeModal('Error!', 'Se prudujo un error en la consulta.', 'error');
	});
}

function inicializarMesSelect2() {	
	var meses = [
		{id:1,text:"01 - Enero"},
		{id:2,text:"02 - Febrero"}, 
		{id:3,text:"03 - Marzo"},
		{id:4,text:"04 - Abril"}, 
		{id:5,text:"05 - Mayo"},
		{id:6,text:"06 - Junio"}, 
		{id:7,text:"07 - Julio"},
		{id:8,text:"08 - Agosto"}, 
		{id:9,text:"09 - Septiembre"},
		{id:10,text:"10 - Octubre"}, 
		{id:11,text:"11 - Noviembre"},
		{id:12,text:"12 - Diciembre"}
		];
		
	$j(".select-meses").select2({
		data: meses,
		minimumResultsForSearch: -1
	});
}

function inicializarSelectInsumo() {	
	$j(".select-insumo").select2({		
		ajax : {
			url : pathSistema+"/actions/gafAjaxHelperAction.do?do=getInsumos",
			dataType : 'json',
			delay : 250,
			data : function(params) {
				return {
					filtro : params.term,
					entidad : "Insumo",
					campos : "codigo,denominacion",
					cantidad : 30
				};
			},
			processResults : function(data, params) {
				return {
					results : data
				};
			},
			cache : true
		},
		escapeMarkup : function(markup) {
			return markup;
		},
		minimumInputLength : 4,
		allowClear: true
	});
}
