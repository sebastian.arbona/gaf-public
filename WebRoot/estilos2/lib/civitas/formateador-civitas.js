/**
 * Defino constantes para el formato de importe
 */
var simbolo_moneda = "$ ";

var FormatM = wNumb({
    mark: ',',
    prefix: simbolo_moneda,
    thousand: '.',
    decimals: 2,
});

var FormatD = wNumb({
    mark: ',',
    prefix: '',
    thousand: '.'
});

/**
 * Funcion que determina mascara de Moneda
 * @param 
 * @return 
*/
function inicializarMaskMoney() {
    $j(".moneda").maskMoney({
        prefix: simbolo_moneda+'', 
        allowNegative: true, 
        thousands:'.', 
        decimal:',', 
        affixesStay: true
    });

    inicializarValoresMaskMoney();
}

/**
 * Funcion que determina mascara de Decimal
 * @param 
 * @return 
*/
function inicialiarMaskDecimal() {
    $j(".decimal").maskMoney({
        prefix:'', 
        allowNegative: true, 
        thousands:'.', 
        decimal:',', 
        affixesStay: true
    });
}

/**
 * Funcion que define una mascara a todos los campos que contengan dicha clase
 * @param String, int
 * @return 
*/
function setMaskNumber(classElemento, opcion) {
    var seleccion = "";

	switch ( Number(opcion) ) {
	    case 1:
	        seleccion = "0#"; break;
	    case 2:
	        seleccion = "0#";
	}

	if(seleccion != "") {
		$j('.'+classElemento).mask(seleccion, {
            reverse: true
        });
	}
	else {
		alert("Opcion ingresada no corresponde");
	}
}

/**
 * Funcion que retorna un valor con formato
 * @param 
 * @return 
*/
function formatMoney(monto) {    
    return FormatM.to( Number( monto ) );
};

function inicializarMaskMoneyForm( idForm ) {
    var formulario = "#"+idForm;
	var form_input = formulario +" :input"
	var $inputs = $j(form_input);

    $inputs.each(function() {
    	if ( $j(this).prop("tagName") == "INPUT") {
            if( $j(this).attr('class').indexOf("moneda") >= 0 ) {
                //$j(this).trigger('mask.maskMoney');
                $j(this).val( formatMoney( $j(this).val() ));
    		}    		
    	}    	
    });
}

function inicializarValoresMaskMoney() {
    var $inputs = $j(":input");
    $inputs.each(function() {
    	if ( $j(this).prop("tagName") == "INPUT") {
            if( $j(this).attr('class').indexOf("moneda") >= 0 ) {
                //$j(this).trigger('mask.maskMoney');
                $j(this).val( formatMoney( $j(this).val() ));
    		}    		
    	}    	
    });
}