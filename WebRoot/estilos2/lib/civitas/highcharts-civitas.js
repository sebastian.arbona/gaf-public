/*
Doc:
Referencias de una serie a otra => series:[{id: 'idelem', ...}, {linkedTo: 'idelem', ...}]
Iconos:
- Columna           => <i class="fa fa-bar-chart"></i>
- Spline            => <i class="fa fa-line-chart"></i>
- Columna Apilada 1 => <i class="glyphicon glyphicon-menu-hamburger"></i>
- Columna Apilada 2 => <i class="glyphicon glyphicon-equalizer"></i>
*/


/**
 * Funcion que vacia las categorias
 * @param {*} chart 
 */
function h_remover_categorias( chart ) {
	chart.xAxis[0].setCategories(new Array());
}

/**
 * Funcion que vacia series ya definidas en la estructura
 * @param serie
 * @returns
 */
function h_remover_series( chart ) {
	let seriesLength = chart.series.length;			
    for(var i = seriesLength -1; i > -1; i--) {
    	chart.series[i].setData([]);
    }
}

/**
 * Funcion que elimina series
 * @param chart
 * @returns
 */
function h_eliminar_series( chart ) {
	let seriesLength = chart.series.length;
    for(var i = seriesLength -1; i > -1; i--) {
    	chart.series[i].remove();        	
    }
}

/**
 * Funcion que cambia el tipo de renderizado del grafico
 * @param {*} chart 
 * @param {*} tipo 
 */
function h_cambiarOpcionRenderizar( chart, tipoGrafica, tipoSeries ) {
    let chart_cust = new Object();
    
    if( tipoGrafica != null ) {
        switch ( tipoGrafica ) {
            case 0: //Plain
                chart_cust.inverted     = false;
                chart_cust.polar        = false;
                break;
            case 1: //Inverted
                chart_cust.inverted     = true;
                chart_cust.polar        = false;
                break;
            case 2: //Polar
                chart_cust.inverted     = true;
                chart_cust.polar        = true;
                break;            
        }  
    }

    if( tipoSeries != null ) {
        switch ( tipoSeries ) {
            case 0:
                chart_cust.type =       'column';
                break;
            case 1:
                chart_cust.type =       'spline';
                break;            
        }
    }

    chart.update({
        chart: chart_cust
    });
}

/**
 * Funcion que agrega una serie al grafico y refresca automaticamente
 * @param {*} chart 
 * @param {*} tipo 
 * @param {*} nombre 
 * @param {*} datos 
 */
function h_agregarSerie( chart, tipo, nombre, datos ) {
    try {
        if( chart != null && nombre != null  ) {
            let chart_cust = new Object();
            chart_cust.name = nombre;            
            if( datos != null ) chart_cust.data = datos;
            if( tipo != null ) chart_cust.type = tipo;
            
            chart.addSeries(chart_cust);
        }
    } catch (error) {
        console.log("Error al agregar serie");
    }
}

/**
 * Funcion que verifica y agrega una serie que no exista
 * @param {*} chart 
 * @param {*} nombre_serie 
 */
function h_agregarSerieNoRepetida( chart, nombre_serie ) {
    try {    	
        if( chart != null ) {
        	let seriesLength = chart.series.length;
        	
        	if( seriesLength > 0 ) {
        		let estaRepetida = false;
        		for(let i = 0; i < seriesLength; i++) {        	        
        			if( chart.series[i].name == nombre_serie ) {
        				estaRepetida = true;
        			}
        	    }
        		
        		if( !estaRepetida ) {
        			h_agregarSerie( chart, null, nombre_serie, null );
        		}       		
        	}
        	else {
        		h_agregarSerie( chart, null, nombre_serie, null );
        	}
        }
    } catch (error) {
        console.log("Error al agregar serie");
    }
}

/**
 * Funcion que agrega una serie con valores sobre si misma para las categorias existetes
 * @param {*} chart 
 * @param {*} nombre_categoria 
 * @param {*} array_valores 
 */
function h_agregarCategoriaSerie( chart, nombre_categoria, array_valores ) {    
    var categories = chart.xAxis[0].categories;
    categories.push(nombre_categoria);
    chart.xAxis[0].setCategories(categories);
    let seriesLength = chart.series.length;
    for(var i = 0; i < seriesLength; i++) {
        chart.series[i].addPoint(array_valores[i]);
    }
    chart.redraw();
}

/**
 * Funcion que agrega una serie con valores sobre si misma para las categorias existetes
 * @param {*} chart 
 * @param {*} nombre_categoria 
 * @param {*} array_valores [nombre, valor]
 */
function h_agregarCategorieSerieDinamico( chart, nombre_categoria, array_valores ) {
	let categories = chart.xAxis[0].categories;
    categories.push(nombre_categoria);
    chart.xAxis[0].setCategories(categories);
    let seriesLength = chart.series.length;
    let nuevosValores = array_valores.length;    
    for(let i = 0; i < seriesLength; i++) {
    	for(let j = 0; j < nuevosValores; j++) {    		
    		if( chart.series[i].name == array_valores[j][0] ) {
    			chart.series[i].addPoint( array_valores[j][1] );
			}    		
    	}
    }
    chart.redraw();
    h_equilibrarSeries( chart );
}

/**
 * Funcion 
 * @param {*} chart 
 */
function h_equilibrarSeries( chart ) {
    var categories = chart.xAxis[0].categories;
    let categoriasLength = chart.xAxis[0].categories.length;
    let seriesLength = chart.series.length;

    for(var i = 0; i < seriesLength; i++) {
        let valoresXserie = chart.series[i].data.length;
        while ( valoresXserie < categoriasLength ) {
            chart.series[i].addPoint(0);
            valoresXserie++;
        }         
    }
}

/**
 * Funcion donde defino por medio de Highcharts, el color a utilizar
 * colors: ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce',
    '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a']
 * @param {*} numero 
 */
function h_colorTraza( numero ) {    
    return Highcharts.getOptions().colors[numero];
}

function h_mesesAbrev() {
    return ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
}

function h_mesesCompletos() {
    return ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
}

function h_poinFormatImporte() {
    return '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>$ {point.y:.2f}</b></td></tr>';
}

Math.easeOutBounce = function (pos) {
    if ((pos) < (1 / 2.75)) {
        return (7.5625 * pos * pos);
    }
    if (pos < (2 / 2.75)) {
        return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
    }
    if (pos < (2.5 / 2.75)) {
        return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
    }
    return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
};