// Referencia: https://eonasdan.github.io/bootstrap-datetimepicker/Functions/#defaultdate
/*
Options format:
yyyy-mm-dd
yyyy-mm-dd hh:ii
yyyy-mm-ddThh:ii
yyyy-mm-dd hh:ii:ss
yyyy-mm-ddThh:ii:ssZ

String. Default: 'mm/dd/yyyy'

The date format, combination of p, P, h, hh, i, ii, s, ss, d, dd, m, mm, M, MM, yy, yyyy.
p : meridian in lower case ('am' or 'pm') - according to locale file
P : meridian in upper case ('AM' or 'PM') - according to locale file
s : seconds without leading zeros
ss : seconds, 2 digits with leading zeros
i : minutes without leading zeros
ii : minutes, 2 digits with leading zeros
h : hour without leading zeros - 24-hour format
hh : hour, 2 digits with leading zeros - 24-hour format
H : hour without leading zeros - 12-hour format
HH : hour, 2 digits with leading zeros - 12-hour format
d : day of the month without leading zeros
dd : day of the month, 2 digits with leading zeros
m : numeric representation of month without leading zeros
mm : numeric representation of the month, 2 digits with leading zeros
M : short textual representation of a month, three letters
MM : full textual representation of a month, such as January or March
yy : two digit representation of a year
yyyy : full numeric representation of a year, 4 digits
*/

const personalizacion_icono_datetimepicker = {
							                time: "fa fa-clock-o",
							                date: "fa fa-calendar",
							                up: "fa fa-arrow-up",
							                down: "fa fa-arrow-down"
							        		};

/**
 * Funcion que a partir de la clase definida en los elementos inicializa el datetimepicker
 * @param 
 * @return 
*/
function inicializarDatetimepickerFecha() {	
    return $j('.dp-fecha').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        showClose:true,
		showClear: true,
		showTodayButton: true,
		toolbarPlacement: 'bottom'
        
        //collapse:false,
		//sideBySide:true
        //useCurrent: true

        /* Defino la fecha por defencto mes/dia/año */
        //defaultDate: "5/24/1986"

        /* Personalizar iconos del calendario */
        //icons: personalizacion_icono_datetimepicker	

        /* Deshabilitar dias del calendario, ej. sab y dom */
        //daysOfWeekDisabled: [0, 6]

    });    
};



/**
 * Funcion que define una fecha en el campo
 * @param String, Array.-
 * @return 
*/
function setFechaDatetimepicker(idElemento, fecha) {	// fecha:"15/01/2013"
	$j('#'+idElemento).data('DateTimePicker').defaultDate(fecha);
}

function setArrayFechaDatetimepicker(arrayElementos, fecha) {	// arrayElementos:["datapik1", "datapik2"]; fecha:"15/01/2013"	
	$j.each( arrayElementos, function( i, value ){
		setFechaDatetimepicker(value, fecha)
	});
}

/**
 * Funcion que deshabilita DIAS en el calendario
 * @param String, Array.-
 * @return 
*/
function setDeshabilitarDiasDatetimepicker(idElemento, rango) {		// rango: [0, 6]
	$j('#'+idElemento).data('DateTimePicker').daysOfWeekDisabled(rango);
}

function setArrayDeshabilitarDiasDatetimepicker(arrayElementos, rango) {	// arrayElementos:["datapik1", "datapik2"]; fecha:"15/01/2013"	
	$j.each( arrayElementos, function( i, value ){
		setDeshabilitarDiasDatetimepicker(value, rango)
	});
}

/**
 * Funcion que deshabilita FECHAS en el calendario
 * @param String, Array.-
 * @return 
*/
function setDeshabilitarFechasDatetimepicker(idElemento, rango) {	// 3 formas de indicar fechas en rango: [moment("12/25/2013"), new Date(2013, 11 - 1, 21), "11/22/2013 00:53"]
	$j('#'+idElemento).data('DateTimePicker').disabledDates(rango);
}

function setArrayDeshabilitarFechasDatetimepicker(arrayElementos, rango) {	// arrayElementos:["datapik1", "datapik2"]; fecha:"15/01/2013"	
	$j.each( arrayElementos, function( i, value ){
		setDeshabilitarFechasDatetimepicker(value, rango)
	});
}

/**
 * Funcion que determina la fecha Desde la que puedo seleccionar
 * @param String, String.-
 * @return 
*/
function setFechaDesdeDatetimepicker(idElemento, fecha) {
	$j('#'+idElemento).data('DateTimePicker').minDate(fecha);
}

function setArrayFechaDesdeDatetimepicker(arrayElementos, fecha) {	// arrayElementos:["datapik1", "datapik2"]; fecha:"15/01/2013"	
	$j.each( arrayElementos, function( i, value ){
		setFechaDesdeDatetimepicker(value, fecha)
	});
}

/**
 * Funcion que determina la fecha Hasta la que puedo seleccionar
 * @param String, String.-
 * @return 
*/
function setFechaHastaDatetimepicker(idElemento, fecha) {
	$j('#'+idElemento).data('DateTimePicker').maxDate(fecha);
}

function setArrayFechaHastaDatetimepicker(arrayElementos, fecha) {	// arrayElementos:["datapik1", "datapik2"]; fecha:"15/01/2013"	
	$j.each( arrayElementos, function( i, value ){
		setFechaHastaDatetimepicker(value, fecha)
	});
}

/**
 * Funcion que determina la propiedad useCurrent
 * @param String, boolean.-
 * @return 
*/
function setUseCurrentDatetimepicker(idElemento, booleano) {
	$j('#'+idElemento).data('DateTimePicker').useCurrent(booleano);
}

/**
 * Funcion que retorna indica el formato de ingreso de fecha
 * @param String, int.-
 * @return 
*/
function setViewMode(idElemento, opcion) {	
	var seleccion = "";

	switch ( Number(opcion) ) {
	    case 1:
	        seleccion = "days"; break;
	    case 2:
	        seleccion = "months"; break;
	    case 3:
	        seleccion = "years"; break;
	    case 4:
	        seleccion = "decades"; break;
	    case 5:
	    	seleccion = "LT"; break;
	}

	if(seleccion != "") {		
		try {
			$j('#'+idElemento).data('DateTimePicker').viewMode(seleccion);
		}
		catch(err) {
			console.log("Error elemento id: "+idElemento);
		}
	}
	else {
		alert("Opcion ingresada no corresponde");
	}	
}

function setArrayViewMode(arrayElementos, opcion) {	// arrayElementos:["datapik1", "datapik2"];
	$j.each( arrayElementos, function( i, value ){
		setViewMode(value, opcion)
	});
}

/**
 * Funcion que retorna la fecha en un formato indicado
 * @param String, int.-
 * @return String
*/
function getFormatoFechaDatetimepicker(idElemento, opcion) { // opcion: int
	var seleccion = "";

	switch ( Number(opcion) ) {
	    case 1:
	        seleccion = 'YYYY-MM-DD'; break;
	    case 2:
	        seleccion = 'DD/MM/YYYY'; break;
	    case 3:
	        seleccion = 'DD-MM-YYYY HH:mm'; break;
        case 4:
	        seleccion = 'yyyy-mm-dd hh:ii'; break;
        case 5:
	        seleccion = 'dd MM yyyy - hh:ii'; break;
	    case 6:
	        seleccion = 'MM/YYYY'; break;
	    case 7:
	        seleccion = 'LT';
	}

	if(seleccion != "") {
		return $j('#'+idElemento).data('DateTimePicker').date().format(seleccion);
	}
	else {
		return null;
	}	
}

function validarFormulario(idForm) {	
	var formulario = "#"+idForm;
	var form_input = formulario +" :input"
	var $inputs = $j(form_input);


	var alServidor = "Enviar al Servidor";

    $inputs.each(function() {
    	console.log("#"+$j(this).attr('id') +": "+$j(this).val());

    	if ( $j(this).prop("tagName") == "INPUT") {

    		if( $j(this).attr('class').indexOf("dp-date") >= 0 ) {    			
    			alServidor += "\nname: "+$j(this).attr('name')+" ; value: "+ moment($j(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD');

    			$j(this).val(moment($j(this).val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));

    		}
    		else {
    			alServidor += "\nname: "+$j(this).attr('name')+" ; value: "+$j(this).val();
    		}
    		
    		alServidor += "\n";
    	}
    	
    });

    alert (alServidor)

    $j(formulario).submit();
}

//dp-date 