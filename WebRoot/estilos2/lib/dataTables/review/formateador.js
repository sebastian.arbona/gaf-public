/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var FormatM = wNumb({
    mark: ',',
    prefix: '$',
    thousand: '.'
});

var FormatD = wNumb({
    mark: ',',
    prefix: '',
    thousand: '.'
});

jQuery(document).ready(function() {
    
    $("#decimalAR").maskMoney({
        prefix:'', 
        allowNegative: true, 
        thousands:'.', 
        decimal:',', 
        affixesStay: true
    });

    $("#monedaAR").maskMoney({
        prefix:'$ ', 
        allowNegative: true, 
        thousands:'.', 
        decimal:',', 
        affixesStay: true
    });

    $("#fecha").mask("00/00/0000", {placeholder: "__/__/____"});    
    
    $("#entero2").mask("0#", {
        reverse: true
    });    
   
});

function formatMoney(monto) {
    return FormatM.to( monto );
};

function validarForma(forma) {
    var nombre = forma.nombre;
    var importe = "" ; 
       
    if (nombre.value === "" || nombre.value === "Escribir nombre") {
        alert("Debe proporcionar un nombre");
        nombre.focus();
        nombre.select();
        return false;
    }

    // saca la máscara y deja el valor compatible con el modelo
    importe = FormatM.from( forma.saldo.value );
    forma.saldo.value = importe;
    console.log( "nombre: " + nombre.value );
    console.log( "saldo: " + forma.saldo.value );
    console.log( "importe: " + importe );
    
    //Formulario validado
    alert("Formulario validado! Actualizando datos" );
    return true;
};


