var editor; // use a global for the submit and return data rendering in the examples

var datosJson = null;
var tabla = null;

// CUERPO PRINCIPAL
$(document).ready(function() {
 
    // EDITOR Y DATATABLES
    var promise_datos = cargarGrilla();

    $.when( promise_datos ).done(function() {
        refrescar_grilla_dataTable(tabla, datosJson);
    });
    // Set up the editor
    editor = new $.fn.dataTable.Editor({
        table: "#example",
        fields: [
            { label: "Id:", name: "id"}, 
            { label: "Nombre:", name: "nombre"},
            { label: "Precio:", name: "precio",  format: "#.###.##9,99"},
            { label: "Cantidad:", name: "cantidad"}
        ],
        idSrc:  'id',
        ajax: function(method, url, d, successCallback, errorCallback) {
            var output = { data: [] };

            if (d.action === 'create') {
                // Create new row(s), using the current time and loop index as
                // the row id
                var dateKey = +new Date();

                $.each(d.data, function(key, value) {
                    var id = dateKey + '' + key;
                    value.DT_RowId = id;
                    datosJson[id] = value;
                    console.log('create', value);
                    console.log('datosJson', datosJson[id]);
                    output.data.push(value);
                });
            } else if (d.action === 'edit') {
                // Update each edited item with the data submitted
                $.each(d.data, function(id, value) {
                    value.DT_RowId = id;
                    $.extend(datosJson[id], value);
                    console.log('edit', value);
                    console.log('datosJson', datosJson[id]);
                    output.data.push(datosJson[id]);
                });
            } else if (d.action === 'remove') {
                // Remove items from the object
                $.each(d.data, function(id) {
                    console.log('remove', id);
                    console.log('datosJson', datosJson[id]);
                    delete datosJson[id];
                });
            }
            successCallback(output);
        },
        i18n: {
            create: {
                button: "Nuevo",
                title:  "Crear un NUEVO registro",
                submit: "Crear"
            },
            edit: {
                button: "Editar",
                title:  "MODIFICAR el registro actual",
                submit: "Actualizar"
            },
            remove: {
                button: "Eliminar",
                title:  "BORRAR el registro actual",
                submit: "eliminar",
                confirm: {
                    _: "Está seguro de ELIMINAR %d registros?",
                    1: "Está seguro de ELIMINAR el registro?"
                }
            },
            error: {
                system: "Se ha producido un error, capture la pantalla y avise al Administrador del sistema."
            },
            multi: {
                title: "Seleción múltiple",
                info: "Los elementos seleccionados contienen diferentes valores para esta entrada. Para editar y poner todos los registros para esta entrada por el mismo valor, haga clic o toque aquí, de lo contrario mantendrán sus valores individuales.",
                restore: "Cancelar las modificaciones"
            },
            datetime: {
                previous: 'Anterior',
                next:     'Siguiente',
                months:   [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
                weekdays: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ]
            }
        }
    });

//    $.extend( true, $.fn.dataTable.Editor.defaults, {

//    } );

//  Para terminar, usamos dos comandos de jQuery para asignarle clases 
//  de bootstrap a las label de los campos de la parte superior, así cómo a los
//   propios campos    
    $('label').addClass('form-inline');
    $('select, input[type="search"]').addClass('form-control input-sm');
    
    // Initialise the DataTable
    tabla = $('#example').DataTable({
        dom: 'Bfrtipl',
//        "ajax": JSON.stringify(ajaxCargaDataSource(todo)),
//        "ajax": {
//            "data": ajaxCargaDataSource() 
//        },
        data:"",
//  se desactiva JqueryUI cuando se usa Boostrap        
        "bJQueryUI":    false,
        "lengthMenu":	[[5, 10, 20, -1], [5, 10, 20, "Todos"]],
	"iDisplayLength":	5,
        columns: [
//  no funciona utilizar el parametro 0 1 2 3 en data:            
            { data: "id" },
            { data: "nombre" },
            { data: "precio",  className: "text-right" },
            { data: "cantidad", className: "text-right", 'orderable': false, 'searchable': false }      
        ],
	"columnDefs": [
            {
		'targets': 2, // define la columna 2 = precio 
		"render": function (data) {                   
                    return formatMoney(data); // aplica la mascara moneda
		}
            }
	],        
        select: true,
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit", editor: editor },
            { extend: "remove", editor:  editor }
        ],
        language: {
            "url": "scripts/DataTableSpanish.json"
        }
    });
});

function cargarGrilla() {
    
    return $.ajax({
            url : "catalogo/productosJsonServicio",
            type: 'POST',
            data: null,
            dataType : 'json'
        })
        .done(function(json) {
            console.log('success', json);
            datosJson = json;
        })
        .fail(function(xhr) {
            console.log('error', xhr);
        });
   
}

function refrescar_grilla_dataTable( dataTable, datos ) {
    dataTable.clear();

    if( datos !== null && datos !== "") {
        dataTable.rows.add( datos );
        console.log('refrescar', datos);
    } else { console.log('falso', datos); }
    
    dataTable.draw();
}

function formatMoney(data) {

    var FormatM = wNumb({
        mark: ',',
        prefix: '$',
        decimals: 2,
        thousand: '.'
    });    
    return FormatM.to( data );
};

function unFormatMoney(data) {

    var FormatM = wNumb({
        mark: ',',
        prefix: '$',
        decimals: 2,
        thousand: '.'
    });      
    return FormatM.from( data );
};
function FormatDecimal(data) {

    var FormatD = wNumb({
        mark: ',',
        prefix: '',
        decimals: 2,
        thousand: '.'
    });    
    return FormatD.to( data );
};

function unFormatDecimal(data) {

    var FormatD = wNumb({
        mark: ',',
        prefix: '',
        decimals: 2,    
        thousand: '.'
    });      
    return FormatD.from( data );
};


