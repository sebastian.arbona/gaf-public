/* 
 * Plantilla CRUD DataTable Editor con Tablas asociadas
 * CIVITAS SA
 * Versión2.0 - mPalma
 */

// Variables Globales
var editor = null;
var tabla = null;
var datosJson = null;
var listaCategorias = '';

// CUERPO PRINCIPAL
$(document).ready(function() {
     
    // EDITOR
    editor = new $.fn.dataTable.Editor({
        table: "#dataTable", // DataTable asociada al Editor
        fields: [ // Datos que gestiona el Form del CRUD del Editor
            { label: "Id:", name: 'id', attr: {class:'form-control'}}, 
            { label: "Nombre:", name: 'producto.nombre', attr: {class:'form-control'}},
            {
              label: "Categoría:", name: "producto.categoria", attr: {class:'form-control'},
              type: 'select', fieldInfo: 'Seleccione la Categoría'
            },
            { label: "Precio:", name: 'producto.precio', attr: {class:'form-control'}, 
              fieldInfo: 'El precio debe ir con dos decimales (incluso si son 00), separados por un punto.'},
            { label: "Cantidad:", name: 'producto.cantidad', attr: {class:'form-control'}}
        ],
        idSrc:  'id', // Indica el Id del registro
        // Personaliza la funcionalidad del CRUD del Editor
        ajax: "catalogo/productosJsonServiciov2.action",
        // Configuración en español
        i18n: {
            create: {
                button: "Nuevo",
                title:  "Crear un NUEVO registro",
                submit: "Crear"
            },
            edit: {
                button: "Editar",
                title:  "MODIFICAR el registro actual",
                submit: "Actualizar"
            },
            remove: {
                button: "Eliminar",
                title:  "BORRAR el registro actual",
                submit: "eliminar",
                confirm: {
                    _: "Está seguro de ELIMINAR %d registros?",
                    1: "Está seguro de ELIMINAR el registro?"
                }
            },
            error: {
                system: "Se ha producido un error, capture la pantalla y avise al Administrador del sistema."
            },
            multi: {
                title: "Seleción múltiple",
                info: "Los elementos seleccionados contienen diferentes valores para esta entrada. Para editar y poner todos los registros para esta entrada por el mismo valor, haga clic o toque aquí, de lo contrario mantendrán sus valores individuales.",
                restore: "Cancelar las modificaciones"
            },
            datetime: {
                previous: 'Anterior',
                next:     'Siguiente',
                months:   [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
                weekdays: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ]
            }
        }
    });

    editor.on('preOpen', function(e, mode, action){
        console.log("preOPEN");
        console.log(e);
        console.log(mode);
        console.log(action);
        
	if (action == "remove") return;
	$.ajax({
            url:"catalogo/categoriaJsonServicio.action", 
            async: false,
            dataType: "JSON",
            complete:function(datosRecibidos) {              
                listaCategorias = datosRecibidos.responseText;
            }
	});
        console.log("lista Categoria " + JSON.parse(listaCategorias) );  
	editor.field('producto.categoria').update(JSON.parse(listaCategorias));
    });
    
    editor.on( 'preSubmit' , function( e, data, action ) {
        console.log("preSUBMIT");
        console.log(e);
        console.log(data);
        console.log(action);
        
	if (action === 'remove') return true;
 
        // en Create pone en 0 el ID para que funcione bien el BACK
	var dato_id = editor.field('id').val();
        if (!editor.field('id').isMultiValue()) dato_id = dato_id.trim();
	if (action === 'create') {      
            console.log(action, editor.field('id'));
            editor.field('id').set("0");
        }
        
	// Se comprueba que el nombre tenga contenido, si no es multiedición.
	var dato_nombre = editor.field('producto.nombre').val();
	if (!editor.field('producto.nombre').isMultiValue()) dato_nombre = dato_nombre.trim();
	if (dato_nombre === "" && !editor.field('producto.nombre').isMultiValue()){
            editor.field('producto.nombre').error('Debe ingresar el NOMBRE.');
            editor.field('producto.nombre').focus();
            return false;
	} else {
            editor.field('producto.nombre').error('');
	}

	// Se comprueba que el precio tenga contenido, si no es multiedición. 
	var dato_precio = editor.field('producto.precio').val();
	if (dato_precio === "" && !editor.field('producto.precio').isMultiValue()){
            editor.field('producto.precio').error('Debe ingresar el PRECIO.');
            editor.field('producto.precio').focus();
            return false;
	} else {
            editor.field('producto.precio').error('');
	}
	
        // Se comprueba que el precio se ajuste a formato.
	if (dato_precio !== parseFloat(dato_precio).toFixed(2) && !editor.field('producto.precio').isMultiValue()){
            editor.field('producto.precio').error('El PRECIO no tiene el formato correcto.');
            editor.field('producto.precio').focus();
            return false;
	} else {
            editor.field('producto.precio').error('');
	}
 
	return true;
    });    
    
    //  DATATABLE
    tabla = $('#dataTable').DataTable({
        dom: 'Bfrtipl',
    //  se carga el DataTable mediante el Servlet Action Java (Struts)   
        ajax: {
            method: "POST",
            url: "catalogo/productosJsonServiciov2.action",
            data: null,
            dataType : 'json'
        },
    //  se desactiva JqueryUI cuando se usa Boostrap        
        "bJQueryUI":    false,
    //  Configuración del paginado    
        "lengthMenu":	[[5, 10, 20, -1], [5, 10, 20, "Todos"]],
	"iDisplayLength":	5,
    //  Mapeo de las columnas del DataTable
    //  no funciona utilizar el parametro 0 1 2 3 en data: 
        columns: [           
            { data: 'id' },
            { data: 'producto.nombre' },
            { data: 'producto.precio',  className: "text-right" },
            { data: 'producto.cantidad', className: "text-right", 'orderable': false, 'searchable': false },
            { data: 'categoria.categoria'}
        ],
    //  Configuración personalizada de cada columna    
	"columnDefs": [
            {
		'targets': 2, // define la columna 2 = precio 
		"render": function (data) {                   
                    return formatMoney(data); // aplica la mascara moneda
		}
            }
	],        
    //  Selección múltiple    
        select: true,
    //  Habilita los botones CRUD del Editor    
        buttons: [
            { extend: "create", editor: editor },
            { extend: "edit", editor: editor },
            { extend: "remove", editor:  editor }
        ],
    //  Configuración de Idioma del DataTable    
        language: {
            "url": "../scripts/DataTableSpanish.json"
        }
    });

    //  Usamos dos comandos de jQuery para asignarle clases de bootstrap
    //  a los labels de los campos del encabezado, así cómo a los datos   
    $('label').addClass('form-inline');
    $('select, input[type="search"]').addClass('form-control input-sm');    
 
});

 


//  Gestión de Máscaras
function formatMoney(data) {
    var FormatM = wNumb({
        mark: ',',
        prefix: '$',
        decimals: 2,
        thousand: '.'
    });    
    return FormatM.to( data );
};

function unFormatMoney(data) {
    var FormatM = wNumb({
        mark: ',',
        prefix: '$',
        decimals: 2,
        thousand: '.'
    });      
    return FormatM.from( data );
};

function FormatDecimal(data) {
    var FormatD = wNumb({
        mark: ',',
        prefix: '',
        decimals: 2,
        thousand: '.'
    });    
    return FormatD.to( data );
};

function unFormatDecimal(data) {
    var FormatD = wNumb({
        mark: ',',
        prefix: '',
        decimals: 2,    
        thousand: '.'
    });      
    return FormatD.from( data );
};


