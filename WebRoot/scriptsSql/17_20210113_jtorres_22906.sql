-- Creo las vistas q seran entidades

CREATE VIEW Digesto AS SELECT * FROM DBDIGESTO.dbo.Digesto d 

CREATE VIEW Categoria_Digesto AS SELECT * FROM DBDIGESTO.dbo.Categoria_Digesto cd 

CREATE VIEW Categoria AS SELECT * FROM DBDIGESTO.dbo.Categoria c 

CREATE VIEW Archivo_Digesto AS SELECT * FROM DBDIGESTO.dbo.Archivo_Digesto ad 


-- agrego columna a tabla linea

ALTER TABLE dbo.Linea 
  ADD idCategoria numeric(19,0)
  
-- Creo permisos para visualizar componente

INSERT INTO MENU (IDMENU,NOMBRE,RUTA,IDPADRE,IDMODULO,ACTIVO,TIPO,ORDEN)
	VALUES ('Linea.confDigesto','L�nea Modificar Configuracion Categoria.Digesto','actions/modificarLinea&form=confDigesto','RV027092010MENULINEA',10,0,0,2)

INSERT INTO permiso (idmenu,causer_k,TIPO)
	VALUES ('Linea.confDigesto','ADMINISTRADORES',2) 
	

INSERT INTO versionbdd (nombrescript) VALUES('17_20210113_jtorres_22906.sql');
