-- colocar aqui las modificaciones
UPDATE MENU SET RUTA='actions/process.do?do=process&processName=ProrrogaMasivaProcess' WHERE IDMENU='RV05042011MENUPRORROGA';

INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'Normal','prorroga.tipoProceso','Prorroga Normal');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'Covid19','prorroga.tipoProceso','Prorroga Covid19');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'157','ctacte.detalle','Prorroga Covid19');

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('15_20200413_cnoguerol_21482.sql');