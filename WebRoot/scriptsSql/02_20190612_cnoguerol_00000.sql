-- para SqlServer
CREATE TABLE versionbdd (
    id numeric(20,0) identity(1,1) NOT NULL,
    fechaejecucion datetime2(7) DEFAULT getdate() NOT NULL,
    nombrescript varchar(100) COLLATE Modern_Spanish_CI_AS NOT NULL,
    CONSTRAINT versionbdd_PK PRIMARY KEY (id)
);

INSERT INTO versionbdd (nombrescript) VALUES('02_20190612_cnoguerol_00000.sql');