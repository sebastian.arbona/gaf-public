-- colocar aqui las modificaciones
USE CRED_FTYC;
ALTER TABLE NovedadCtaCte ADD desembolso_id numeric(19,0) NULL;
ALTER TABLE NovedadCtaCte ADD CONSTRAINT FK6E37B1FB94E2570E FOREIGN KEY (desembolso_id) REFERENCES Desembolso(id);
ALTER TABLE DetalleFactura ADD desembolso_id numeric(19,0) NULL;
ALTER TABLE DetalleFactura ADD CONSTRAINT FKB996EA2194E2570E FOREIGN KEY (desembolso_id) REFERENCES Desembolso(id);

USE GAF_FTYC;
DROP VIEW DetalleFactura;
CREATE VIEW DetalleFactura AS SELECT * FROM CRED_FTYC.dbo.DetalleFactura;
DROP VIEW Novedadctacte;
CREATE VIEW NovedadCtacte AS SELECT	* FROM CRED_FTYC.dbo.NovedadCtaCte; 
DROP VIEW Linea;
CREATE VIEW Linea AS SELECT * FROM CRED_FTYC.dbo.Linea;
DROP VIEW desembolso;
CREATE VIEW Desembolso AS SELECT * FROM CRED_FTYC.dbo.Desembolso;

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('12_20191218_cnoguerol_20111.sql');