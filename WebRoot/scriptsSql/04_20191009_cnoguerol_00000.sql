-- colocar aqui las modificaciones
-- integracion de common_persistence mdzf en develop

ALTER TABLE CRED_FTYC.dbo.TIPODOC ADD escuit tinyint NULL GO

ALTER TABLE CRED_FTYC.dbo.CondicionIva ADD codigoafip varchar(20) NULL GO
ALTER TABLE CRED_FTYC.dbo.CondicionIva ADD columna varchar(20) NULL GO

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('04_20191009_cnoguerol_00000.sql');