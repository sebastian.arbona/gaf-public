-- colocar aqui las modificaciones
-- inserta la nueva opcion de menu
INSERT INTO CRED_FTYC.dbo.MENU
(IDMENU, NOMBRE, RUTA, IDPADRE, IDMODULO, ACTIVO, TIPO, AYUDA, ORDEN, parametro, idProperty, valoresAlta, valoresBaja, valoresModificacion, valoresConsulta)
VALUES(N'NotificacionMora', N'Proyectos a Notificar(NUEVO)', N'actions/process.do?do=process&processName=NotificacionMora', N'IDMENUREPORTESCREDITOSMORA', 10, 1, 0, N'', 4, NULL, NULL, NULL, NULL, NULL, NULL);
-- inserta los permisos
INSERT INTO CRED_FTYC.dbo.permiso (idmenu, causer_k, TIPO) VALUES(N'NotificacionMora', N'ADMINISTRADORES', 2);

-- reordena todo el submenu
UPDATE CRED_FTYC.dbo.MENU	SET ORDEN=1	WHERE IDMENU=N'IdInformeCreditosMora';
UPDATE CRED_FTYC.dbo.MENU	SET ORDEN=2	WHERE IDMENU=N'IDMENUREPORTECOMPORTAMIENTODELINEAS';
UPDATE CRED_FTYC.dbo.MENU	SET ORDEN=3	WHERE IDMENU=N'IDMENUREPORTEPROYECTOSANOTIFICAR';
UPDATE CRED_FTYC.dbo.MENU	SET ORDEN=4	WHERE IDMENU=N'NotificacionMora';
UPDATE CRED_FTYC.dbo.MENU	SET ORDEN=5	WHERE IDMENU=N'IDMENUREPORTERESUMENCOMPORTAMIENTO';

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('19_20210416_cnoguerol_23450.sql');