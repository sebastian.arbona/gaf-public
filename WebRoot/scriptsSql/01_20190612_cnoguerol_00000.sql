-- para MariaDB
CREATE TABLE `versionbdd` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechaejecucion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nombrescript` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO versionbdd (nombrescript) VALUES('01_20190612_cnoguerol_00000.sql');