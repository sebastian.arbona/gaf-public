-- colocar aqui las modificaciones
-- a todos los usuarios con acceso a la consulta de creditos RV06122010MENUCREDITOS otorgarles acceso a estas pesta�as
-- Creditos 
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Acuerdo de Pago' WHERE IDMENU='pestaniaAcuerdodePago';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Agenda' WHERE IDMENU='pestaniaAgenda';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Analisis' WHERE IDMENU='pestaniaAnalisis';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Auditoria Final' WHERE IDMENU='pestaniaAuditoriaFinal';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Cesion Titularidad' WHERE IDMENU='pestaniaCesiondeTitularidad';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Comportamientos' WHERE IDMENU='pestaniaComportamientosdePago';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Composicion' WHERE IDMENU='pestaniaComposicion';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Cotomadores' WHERE IDMENU='pestaniaCotomadores';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Cta Cte Ampliada' WHERE IDMENU='pestaniaCtaCteAmpliada';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Cta Cte' WHERE IDMENU='pestaniaCuentaCorriente';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Cuotas' WHERE IDMENU='pestaniaCuotas';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Datos Financieros' WHERE IDMENU='pestaniaDatFinancieros';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Datos Generales' WHERE IDMENU='pestaniadatosgenerales';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Desembolsos' WHERE IDMENU='pestaniaDesembolsos';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Documentacion' WHERE IDMENU='pestaniaDocumentacion';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Domicilios' WHERE IDMENU='pestaniadomicilioObjetoi';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Emergencia' WHERE IDMENU='pestaniaEmergencias';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Garantias' WHERE IDMENU='pestaniaGarantias';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Gastos a Recuperar' WHERE IDMENU='pestaniaGastosaRecuperar';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Historial' WHERE IDMENU='pestaniaHistorial';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Notificaciones' WHERE IDMENU='pestaniaHistorialdeNotificaciones';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Instancias Judiciales' WHERE IDMENU='pestaniaInstanciasJudiciales';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Cosecha' WHERE IDMENU='pestanialineaCyA';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Novedades' WHERE IDMENU='pestaniaNovedades';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Credito Original' WHERE IDMENU='pestaniaOriginal';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Prorroga' WHERE IDMENU='pestaniaProrroga';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Recibos' WHERE IDMENU='pestaniaRecibos';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Resumenes' WHERE IDMENU='pestaniaResumenesdeCuenta';
UPDATE CRED_FTYC.dbo.MENU SET IDPADRE='RV06122010MENUCREDITOS', NOMBRE='Credito Pesta�a Vi�edos' WHERE IDMENU='pestaniavinedos';

insert into permiso 
select m.idmenu, p.causer_k, p.tipo 
from menu m, permiso p
where m.IDPADRE = 'RV06122010MENUCREDITOS' and p.idmenu = 'RV06122010MENUCREDITOS' and p.tipo > 0;

-- Personas
UPDATE CRED_FTYC.dbo.MENU 	SET NOMBRE='Persona Pesta�a Bancos',IDPADRE='11',ORDEN=1	WHERE IDMENU='pestaniasPersonasBancos' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Bonificaciones',IDPADRE='11',ORDEN=2	WHERE IDMENU='pestaniasPersonasBonificaciones' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Cargos',IDPADRE='11',ORDEN=3	WHERE IDMENU='pestaniasPersonasCargos' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Datos Laborales',IDPADRE='11',ORDEN=4	WHERE IDMENU='pestaniasPersonasDatos_Laborales' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Documentaci�n',IDPADRE='11',ORDEN=5	WHERE IDMENU='pestaniasPersonasDocumentacion' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Domicilio',IDPADRE='11',ORDEN=6	WHERE IDMENU='pestaniasPersonasDomicilio' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Entidades',IDPADRE='11',ORDEN=7	WHERE IDMENU='pestaniasPersonasEntidades' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Estados',IDPADRE='11',ORDEN=8	WHERE IDMENU='pestaniasPersonasEstados_Observaciones' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Notificaciones',IDPADRE='11',ORDEN=9	WHERE IDMENU='pestaniasPersonasHistorialNoti' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Impuestos',IDPADRE='11',ORDEN=10	WHERE IDMENU='pestaniasPersonasImpuestos' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Roles',IDPADRE='11',ORDEN=11	WHERE IDMENU='pestaniasPersonasRoles' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Proyectos',IDPADRE='11',ORDEN=12	WHERE IDMENU='pestaniasPersonasSolicitudes_Proyectos' GO
UPDATE CRED_FTYC.dbo.MENU	SET NOMBRE='Persona Pesta�a Personas Vinculadas',IDPADRE='11',ORDEN=13	WHERE IDMENU='pestaniasPersonasVinculadas' GO

insert into permiso 
select m.idmenu, p.causer_k, p.tipo 
from menu m, permiso p
where m.IDPADRE = '11' and p.idmenu = '11' and p.tipo > 0;

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('09_20191206_cnoguerol_20611.sql');