-- colocar aqui las modificaciones
insert into menu
(IDMENU,NOMBRE,RUTA,IDPADRE,IDMODULO,ACTIVO,TIPO,AYUDA,ORDEN,parametro,idProperty,valoresAlta,valoresBaja,valoresModificacion,valoresConsulta)values
('Linea.DatosGenerales','L�nea Modificar Datos Generales','actions/modificarLinea&form=datosGenerales','RV027092010MENULINEA',10,0,0,null,1,null,null,null,null,null,null),
('Linea.GastosAdministrativos','L�nea Modificar Gastos Administrativos','actions/modificarLinea&form=gastosAdministrativos','RV027092010MENULINEA',10,0,0,null,2,null,null,null,null,null,null),
('Linea.GastosDesembolso','L�nea Modificar Gastos Desembolso','actions/modificarLinea&form=gastosDesembolso','RV027092010MENULINEA',10,0,0,null,3,null,null,null,null,null,null),
('Linea.ConfiguracionContable','L�nea Modificar Configuracion Contable','actions/modificarLinea&form=configuracionContable','RV027092010MENULINEA',10,0,0,null,4,null,null,null,null,null,null),
('Linea.ConfiguracionPresupuesto','L�nea Modificar Configuracion Presupuesto','actions/modificarLinea&form=configuracionPresupuesto','RV027092010MENULINEA',10,0,0,null,5,null,null,null,null,null,null);

-- a todos los usuarios que tengan acceso a RV027092010MENULINEA darles acceso a cada porcion
-- ftyc no tiene limitada la modificacion de datos
insert into permiso 
select m.idmenu, p.causer_k, p.tipo 
from menu m, permiso p
where m.IDPADRE = 'RV027092010MENULINEA' and p.idmenu = 'RV027092010MENULINEA' and p.tipo > 0;

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('11_20191206_cnoguerol_20483.sql');