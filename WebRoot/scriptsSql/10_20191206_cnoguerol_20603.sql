-- colocar aqui las modificaciones
ALTER TABLE objetoiindice ADD tipoCalculo varchar(5) NULL;

update objetoiindice set tipoCalculo = 'IC' where tasaVariable = 0;
update objetoiindice set tipoCalculo = 'ICV' where tasaVariable = 1;


ALTER TABLE linea ADD tipoCalculoCompensatorio varchar(5) NULL;
ALTER TABLE linea ADD tipoCalculoMoratorio varchar(5) NULL;
ALTER TABLE linea ADD tipoCalculoPunitorio varchar(5) NULL;

update linea set tipoCalculoPunitorio = 'IC',tipoCalculoMoratorio = 'IC',tipoCalculoCompensatorio = 'IC';

-- Auto-generated SQL script #201912090944
UPDATE TIPIFICADORES SET TF_DESCRIPCION='Compuesto' WHERE TF_ID=900358 ;
UPDATE TIPIFICADORES SET TF_DESCRIPCION='Compuesto Variable' WHERE TF_ID=900359 ;
UPDATE TIPIFICADORES SET TF_DESCRIPCION='Directo' WHERE TF_ID=900360 ;

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('10_20191206_cnoguerol_20603.sql');