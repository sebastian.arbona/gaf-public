-- para MariaDB
INSERT INTO MENU
(IDMENU, NOMBRE, RUTA, IDPADRE, IDMODULO, ACTIVO, TIPO, AYUDA, ORDEN, parametro, idProperty, valoresAlta, valoresBaja, valoresModificacion, valoresConsulta)
VALUES('IdInformeCreditosMora', 'Informe Creditos Mora', 'actions/process.do?do=process&processName=InformeCreditosMoraProcess', 'IDMENUREPORTESCREDITOSMORA', 10, 1, 0, '', 8, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO permiso
(idmenu, causer_k, TIPO)
VALUES('IdInformeCreditosMora', 'ADMINISTRADORES', 2);

INSERT INTO versionbdd (nombrescript) VALUES('04_20190910_JuanT_18695.sql');