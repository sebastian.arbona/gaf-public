-- nuevos atributos para la relacion
ALTER TABLE ObjetoiIndice ADD diasTope int NULL GO
ALTER TABLE ObjetoiIndice ADD tasaVariable tinyint NULL GO

-- valores por defecto para los registros existentes
update objetoiindice set diastope = 0 where diastope is null;
update objetoiindice set tasavariable = 0 where tasavariable is null;

-- dejan de existir las tasas de tipo variable ya que ahora se configura en la relacion de la tasa con el objetoi
update indice set tipo = 1 where tipo = 4;

-- nueva version
INSERT INTO versionbdd (nombrescript) VALUES('05_20190909_cnoguerol_19445.sql');