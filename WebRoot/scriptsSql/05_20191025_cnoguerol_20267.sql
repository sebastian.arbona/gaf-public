-- colocar aqui las modificaciones
ALTER TABLE CRED_FTYC.dbo.ObjetoiArchivo ADD archivoRepo_id numeric NULL;

use ade_ftyc;
ALTER VIEW dbo.ArchivoVinculado AS 
SELECT a1.id, 'GEDO' AS Tipo, a1.urlGEDO AS urlGEDO, null AS documento_id, null AS idPersona, null AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a1
WHERE a1.urlGEDO IS NOT NULL
UNION
SELECT a2.id, 'Expediente' AS Tipo, null AS urlGEDO, c.documento_id AS documento_id, null AS idPersona, null AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a2
INNER JOIN ADE_FTYC.dbo.Contenido c ON  a2.id = c.archivo_id
UNION
SELECT a3.id, 'Persona' AS Tipo, null AS urlGEDO, null AS documento_id, j.idPersona AS idPersona, null AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a3
INNER JOIN CRED_FTYC.dbo.ArchivoJoin j
ON a3.id = j.archivoRepo_id
UNION
SELECT a4.id, 'Proyecto' AS Tipo, null AS urlGEDO, null AS documento_id, o.persona_IDPERSONA AS idPersona, j.credito_id AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a4
INNER JOIN CRED_FTYC.dbo.ObjetoiArchivo j oN a4.id = j.archivoRepo_id
INNER JOIN CRED_FTYC.dbo.Objetoi o on o.id = j.credito_id

use cred_ftyc;
CREATE VIEW dbo.ArchivoVinculado AS 
SELECT a1.id, 'GEDO' AS Tipo, a1.urlGEDO AS urlGEDO, null AS documento_id, null AS idPersona, null AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a1
WHERE a1.urlGEDO IS NOT NULL
UNION
SELECT a2.id, 'Expediente' AS Tipo, null AS urlGEDO, c.documento_id AS documento_id, null AS idPersona, null AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a2
INNER JOIN ADE_FTYC.dbo.Contenido c ON  a2.id = c.archivo_id
UNION
SELECT a3.id, 'Persona' AS Tipo, null AS urlGEDO, null AS documento_id, j.idPersona AS idPersona, null AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a3
INNER JOIN CRED_FTYC.dbo.ArchivoJoin j
ON a3.id = j.archivoRepo_id
UNION
SELECT a4.id, 'Proyecto' AS Tipo, null AS urlGEDO, null AS documento_id, o.persona_IDPERSONA AS idPersona, j.credito_id AS idObjetoi
FROM ADE_FTYC.dbo.Archivo a4
INNER JOIN CRED_FTYC.dbo.ObjetoiArchivo j oN a4.id = j.archivoRepo_id
INNER JOIN CRED_FTYC.dbo.Objetoi o on o.id = j.credito_id


-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('05_20191025_cnoguerol_20267.sql');