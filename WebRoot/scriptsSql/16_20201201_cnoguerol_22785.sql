-- colocar aqui las modificaciones
UPDATE CRED_FTYC.dbo.MENU SET ORDEN=1 WHERE IDMENU='RV027092010MENULINEA';
UPDATE CRED_FTYC.dbo.MENU SET ORDEN=3 WHERE IDMENU='RV03092010MENUSUBTIPO';
UPDATE CRED_FTYC.dbo.MENU SET ORDEN=2 WHERE IDMENU='RV03092010MENUTIPOLINEA';

INSERT INTO menu 
(IDMENU                      , NOMBRE                 , RUTA                                                                , IDPADRE                           , IDMODULO, ACTIVO, TIPO, AYUDA, ORDEN, parametro, idProperty, valoresAlta, valoresBaja, valoresModificacion, valoresConsulta) VALUES
('CategoriaSolicitanteConfig', 'Categoria Solicitante', 'actions/abmAction.do?do=list&entityName=CategoriaSolicitanteConfig', '402880862d278571012d3699d47b0005',       10,      1,    1,    '',     4,      null,       null,        null,        null,                null,            null);
INSERT INTO permiso (idmenu, causer_k, TIPO) VALUES('CategoriaSolicitanteConfig', 'ADMINISTRADORES', 2);

INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'1','Categoria.Solicitante','Micro Agro');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'2','Categoria.Solicitante','Micro');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'3','Categoria.Solicitante','Pequea Tramo 1');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'4','Categoria.Solicitante','Pequea Tramo 2');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'5','Categoria.Solicitante','Mediana Tramo 1');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'6','Categoria.Solicitante','Mediana Tramo 2');

ALTER TABLE Objetoi ADD categoriaSolicitante varchar(10) NULL;

ALTER TABLE CRED_FTYC.dbo.Linea ADD topeTasaCompensatorio float NULL GO
ALTER TABLE CRED_FTYC.dbo.Linea ADD topeTasaMoratorio float NULL GO
ALTER TABLE CRED_FTYC.dbo.Linea ADD topeTasaPunitorio float NULL GO
update linea set topeTasaCompensatorio =0,topeTasaMoratorio =0,topeTasaPunitorio=0;

ALTER TABLE CRED_FTYC.dbo.ObjetoiIndice ADD tasaTope float NULL GO
update ObjetoiIndice set tasaTope = 0;

CREATE TABLE CategoriaSolicitanteConfig (
	id numeric(19,0) IDENTITY(1,1) NOT NULL,
	categoriaSolicitante varchar(10) COLLATE Modern_Spanish_CI_AS NULL,
	diasAntes int NULL,
	tipoCalculo varchar(10) COLLATE Modern_Spanish_CI_AS NULL,
	tipoTasa varchar(10) COLLATE Modern_Spanish_CI_AS NULL,
	valorMas float NULL,
	valorPor float NULL,
	indice_id numeric(19,0) NULL,
	linea_id numeric(19,0) NULL,
	fechaDesde date NULL,
	fechaHasta date NULL,
	topeTasa float NULL,
	CONSTRAINT PK__Categori__3213E83F4EF7F14A PRIMARY KEY (id)
) GO;

-- CRED_FTYC.dbo.CategoriaSolicitanteConfig foreign keys
ALTER TABLE CRED_FTYC.dbo.CategoriaSolicitanteConfig ADD CONSTRAINT FKA790A6824FC75926 FOREIGN KEY (linea_id) REFERENCES Linea(id) GO
ALTER TABLE CRED_FTYC.dbo.CategoriaSolicitanteConfig ADD CONSTRAINT FKA790A682E4D3A52E FOREIGN KEY (indice_id) REFERENCES Indice(id) GO;

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('16_20201201_cnoguerol_22785.sql');
