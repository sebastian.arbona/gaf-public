-- colocar aqui las modificaciones
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'NORTE','Departamento.oasisProductivo','Oasis Norte');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'SUR','Departamento.oasisProductivo','Oasis Sur');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'ESTE','Departamento.oasisProductivo','Oasis Este');
INSERT INTO TIPIFICADORES (TF_ID,TF_CODIGO,TF_CATEGORIA,TF_DESCRIPCION) VALUES ((select max(tf_id) + 1 from TIPIFICADORES) ,'VALLE_UCO','Departamento.oasisProductivo','Oasis Valle de Uco');

ALTER TABLE CRED_FTYC.dbo.Departamento ADD oasis varchar(20) NULL;

UPDATE CRED_FTYC.dbo.Departamento SET oasis='NORTE'	WHERE id=1;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='NORTE'	WHERE id=2;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='NORTE'	WHERE id=3;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='SUR'	WHERE id=5;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='NORTE'	WHERE id=6;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='ESTE'	WHERE id=7;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='ESTE'	WHERE id=8;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='NORTE'	WHERE id=9;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='NORTE'	WHERE id=10;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='SUR'	WHERE id=11;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='ESTE'	WHERE id=12;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='VALLE_UCO'	WHERE id=13;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='ESTE'	WHERE id=14;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='SUR'	WHERE id=15;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='ESTE'	WHERE id=16;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='VALLE_UCO'	WHERE id=17;
UPDATE CRED_FTYC.dbo.Departamento SET oasis='VALLE_UCO'	WHERE id=18;

-- modificar el nombre de la script
INSERT INTO versionbdd (nombrescript) VALUES('14_20200326_cnoguerol_21397.sql');