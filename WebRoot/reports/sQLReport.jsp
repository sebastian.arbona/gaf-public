<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<logic:equal name="SQLReport" property="estilo" value="1">
    <div align="right">
        <input type="button" value="Cerrar" onclick="window.close();">
    </div>
</logic:equal>

<div class="title"><bean:write name="SQLReport" property="title"/></div>
<br>

<div class="grilla">
    <display:table name="SQLReport.result" pagesize="100" export="true" requestURI="/actions/SQLReport.do?do=list" id="row">
        <display:setProperty name="report.title"><bean:write name="SQLReport" property="title"/></display:setProperty>
        <display:caption><bean:write name="SQLReport" property="subtitle"/></display:caption>
        <logic:iterate name="SQLReport" property="titles" id="column">
            <display:column title="${column.nombre}" property="${column.nombre}" sortable="true" align="${column.align}"/>
        </logic:iterate>
        <display:footer>${SQLReport.footer}</display:footer>
    </display:table>
    <br><br>
    <logic:equal name="SQLReport" property="estilo" value="">
        <div align="center">
            <input type="button" value="Volver" onclick="window.location='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/actions/SQLReport.do?do=show';">
        </div>
    </logic:equal>
</div>