<%@ page language="java"%>
<%@ page import="java.util.List"%>
<jsp:directive.page import="com.asf.security.SessionHandler"/>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>


<div class="title">Administración de Reportes</div>
<br>

<html:form action="/actions/SQLReport.do?do=select">
    
    <TABLE border="0">
        <TR><TH>Reporte:</TH><TD>
        <%
        
        List reports=SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getByFilter("FROM Reporte r ORDER BY r.titulo");
        request.setAttribute("reports", reports);
        %>
        
        <select name="idReporte" id="idReporte" onchange="opciones();">
            <script>var hideValue=new Array();</script>
            <logic:iterate id="reporte" scope="request" name="reports">
                <asf:security  action="/actions/SQLReport.do?do=select&idReporte=${reporte.id}">
                    <option value="${reporte.id}">${reporte.titulo}</option>
                    <script>hideValue[hideValue.length] = new Number(${reporte.tipo});</script>
                </asf:security>
            </logic:iterate>
        </select>
        
    </TABLE>
    
    <TABLE id="tabla" border="0">
        <TR>
            <TH>Destino:</TH>
            <TD align="left">
                <INPUT type="radio" id="destino1" name="destino" value="0" checked="true">Navegador WEB (HTML)</INPUT>
            </TD>
        </TR>
        <tr>
            <th>&nbsp;</th>
            <td align="left"><INPUT type="radio" name="destino" value="1">Archivo de texto (TXT)</INPUT></td>
        </tr>
    </TABLE>
    <table id="tablaJASPER" boder="0">
        <tr>
            <th>Destino:</th>
            <td align="left">            
                <INPUT type="radio" id="destino2" name="destino" value="2" checked="true">Visualizador</INPUT>                               
            </td>
        </tr>
        <tr>
            <th>&nbsp;</th>
            <td align="left">
                <INPUT type="radio" name="destino" value="3">PDF</INPUT>
            </td>
        </tr>        
    </table>
 
        <div align="center"><html:submit value="Continuar" onclick="return validarReportes();"/></div>
    
</html:form>

<script language="javascript">
    function validarReportes(){
        var cmbSelect = $('idReporte');
        if (cmbSelect.value != ''){
            return true;
        }else{
            alert('No se selecciono ningun reporte');
            return false;}

    }
    opciones();

    function opciones(){
        var i=getElem('idReporte').selectedIndex;
        if (hideValue[i]!=0 && hideValue[i]!=1 ){
            ocultar("tabla", "tablaJASPER" );            
            getElem("destino1").checked=true;
        }else{
            if( hideValue[ i ] != 1 ){
                mostrar("tabla");
                ocultar("tablaJASPER" );            
                getElem("destino1").checked=true;
            }else{
                ocultar( "tabla" );
                mostrar( "tablaJASPER" );
                getElem("destino2").checked=true;
            }
        }
    }
</script>