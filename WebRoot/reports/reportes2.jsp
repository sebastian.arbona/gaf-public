<%@ page language="java"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<logic:notEmpty name="result" property="export">
    <bean:define id="export" name="result" property="export" type="String" />    
    <div><b>Exportación a <%= export.toUpperCase() %> en progreso. Por favor aguarde unos instantes...</b></div>    
    <html:form styleId="fExport" method="post" action="actions/jreport.do?do=exec" target="_blank">
        
        <html:hidden property="reportName" value="${result.reportName}" />
        <html:hidden property="params" value="${result.params}" />
        <html:hidden property="export" value="${result.export}" />
        <html:hidden property="classBeanName" value="${result.classBeanName}" />        
        
    </html:form>
</logic:notEmpty>
<logic:empty name="result" property="export">
    <logic:equal name="result" property="print" value="true" >
        <APPLET CODE="Imprimir.class" ID="applet"
                CODEBASE="${pageContext.request.contextPath}/applets"
                ARCHIVE="jasperreports-3.1.2-applet.jar" WIDTH="1" HEIGHT="1">
            <PARAM NAME=CODE VALUE="Imprimir.class">
            <PARAM NAME=CODEBASE VALUE="${pageContext.request.contextPath}/applets">
            <PARAM NAME=ARCHIVE VALUE="jasperreports-3.1.2-applet.jar">
            <PARAM NAME="type" VALUE="application/x-java-applet;version=1.2.2">
            <PARAM NAME="scriptable" VALUE="false">
            <PARAM NAME="REPORT_URL"
                   VALUE="${pageContext.request.contextPath}/actions/jreport.do?do=exec&reportName=<bean:write name="result" property="reportName"/>&params=<bean:write name="result" property="params"/>&classBeanName=<bean:write name="result" property="classBeanName"/>"> 
            <PARAM NAME="PRINT"
                   VALUE="<bean:write name="result" property="print"/>">
        </APPLET>  
        
        <div><b>Proceso de impresión en progreso. Por favor aguarde unos instantes ...</b></div> 
    </logic:equal>
    <logic:notEqual name="result" property="print" value="true" >
        <APPLET CODE="AppletNG.class" ID="applet"
                CODEBASE="${pageContext.request.contextPath}/applets"
                ARCHIVE="jasperreports-3.1.2-applet.jar" WIDTH="1000" HEIGHT="400">
            <PARAM NAME=CODE VALUE="AppletNG.class">
            <PARAM NAME=CODEBASE VALUE="${pageContext.request.contextPath}/applets">
            <PARAM NAME=ARCHIVE VALUE="jasperreports-3.1.2-applet.jar">
            <PARAM NAME="type" VALUE="application/x-java-applet;version=1.2.2">
            <PARAM NAME="scriptable" VALUE="false">
            <PARAM NAME="REPORT_URL"
                   VALUE="${pageContext.request.contextPath}/actions/jreport.do?do=exec&reportName=<bean:write name="result" property="reportName"/>&params=<bean:write name="result" property="params"/>&classBeanName=<bean:write name="result" property="classBeanName"/>">
            <PARAM NAME="PRINT"
                   VALUE="<bean:write name="result" property="print"/>">
        </APPLET>
        <script type="text/javascript" language="javascript">
            var applet = getElem("applet");
            if( screen.width == 800 ){    
                applet.setAttribute('width',500);
                applet.setAttribute('height',250);
            }
            if( screen.width > 800 ){    
                applet.setAttribute('width',1000);
                applet.setAttribute('height',700);
            }
        </script>
    </logic:notEqual>    
</logic:empty>
<br>
<br>
<logic:notEmpty name="result" property="export">
    <script type="text/javascript" language="javascript">
        getElem("fExport").submit();
    </script>
</logic:notEmpty>

<button onclick="location.href='${pageContext.request.contextPath}/actions/SQLReport.do?do=show'">Volver</button>

<!-- Alejandro Bargna -->
