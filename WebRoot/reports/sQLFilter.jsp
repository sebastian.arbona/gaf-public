<%@ page language="java"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%> 
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<div class="title"><bean:write name="SQLReport" property="reporte.titulo"/>
<br>Filtros</div>
<br>
<html:form action="/actions/SQLReport.do?do=list">
    <html:hidden name="SQLReport" property="idReporte" />
    <bean:define id="leyenda" value=""/>
    
    <table border="0">
        <logic:equal value="0" name="SQLReport" property="reporte.tipo">
            <tr>
                <th>Destino:</th>
                <td>
                    <html:radio property="destino" value="0" disabled="" />Navegador WEB (HTML)
                    <br>
                    <html:radio property="destino" value="1" disabled="" />Archivo de texto (TXT)
                </td>
            </tr>
        </logic:equal>
        <logic:equal value="1" name="SQLReport" property="reporte.tipo">            
            <html:hidden name="SQLReport" property="destino" value="${ SQLReport.destino }" />
        </logic:equal>
        <logic:iterate id="columna" name="SQLReport" property="filterColumns">
            <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                <bean:define id="leyenda" value="(*) Campo Obligatorio."/>
            </logic:equal>
            
            <tr><th>${columna.etiqueta}:</th><td>
                    <%-- TEXTO   --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="0" scope="page">
                        <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                            <html:text property="f${columna.id}" value="${columna.valor}" maxlength="30"/>
                        </logic:equal>
                        <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                            <html:text  property="f${columna.id}" value="${columna.valor}" maxlength="30"/> *
                        </logic:equal>
                    </logic:equal>
                    <%-- TEXTO Num�rico  --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="6" scope="page">
                        <bean:define id="etiqueta" name="columna" property="etiqueta"/>
                        <%
                        Long largo=null;
                        if(etiqueta.equals("Per�odo")){
                            largo=new Long(4);
                        }else if(etiqueta.equals("Mes")){
                            largo=new Long(2);
                        }else{
                            largo=new Long(30);
                        }
                        %>
                        <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                            <asf:text name="sQLReportForm" property="f${columna.id}" type="long" value="${columna.valor}" maxlength="<%=largo.toString()%>"/>
                        </logic:equal>
                        
                        <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                            <asf:text name="sQLReportForm" property="f${columna.id}" type="long" value="${columna.valor}" maxlength="<%=largo.toString()%>"/> *
                        </logic:equal>
                    </logic:equal>
                    <%-- FECHA   --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="1" scope="page">
                        <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                            <asf:calendar property="f${columna.id}" />
                        </logic:equal>
                        <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                            <asf:calendar property="f${columna.id}" /> *
                        </logic:equal>
                    </logic:equal>	
                    <%-- Tipificador   --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="2" scope="page">
                        <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                            <asf:select name="f${columna.id}" entityName="com.asf.hibernate.mapping.Tipificadores" listCaption="codigo,descripcion" listValues="codigo" nullValue="${columna.filtro.nullvalueTpf}" nullText="${columna.filtro.nulltext}" value="${columna.valor}" filter="categoria='${columna.filtro.tabla}' ORDER BY codigo" />
                        </logic:equal>
                        <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                            <asf:select name="f${columna.id}" entityName="com.asf.hibernate.mapping.Tipificadores" listCaption="codigo,descripcion" listValues="codigo" value="${columna.valor}" filter="categoria='${columna.filtro.tabla}' ORDER BY codigo" /> *
                        </logic:equal>
                    </logic:equal>			
                    <%-- Combo   --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="3" scope="page">
                        <logic:empty name="columna" property="filtro.tabla">
                            <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                                <asf:select name="f${columna.id}" listValues="${columna.filtro.valores}" listCaption="${columna.filtro.etiquetas}" nullValue="${columna.filtro.nullvalueTpf}" nullText="${columna.filtro.nulltext}" value="${columna.valor}" filter="${columna.filtro.columnas}" />
                            </logic:equal>
                            <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                                <asf:select name="f${columna.id}" nullValue="${columna.filtro.nullvalueTpf}" listValues="${columna.filtro.valores}" listCaption="${columna.filtro.etiquetas}" value="${columna.valor}" filter="${columna.filtro.columnas}"/> *
                            </logic:equal>
                        </logic:empty>
                        
                        <logic:notEmpty name="columna" property="filtro.tabla">
                            <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                                <asf:select name="f${columna.id}" value="${columna.valor}" entityName="${columna.filtro.tabla}" nullValue="${columna.filtro.nullvalueTpf}" nullText="${columna.filtro.nulltext}" 
                                            listValues="${columna.filtro.valores}" listCaption="${columna.filtro.etiquetas}" filter="${columna.filtro.columnas}"/>
                            </logic:equal>
                            <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                                <asf:select name="f${columna.id}" value="${columna.valor}"  
                                            entityName="${columna.filtro.tabla}" 
                                            listValues="${columna.filtro.valores}" listCaption="${columna.filtro.etiquetas}" filter="${columna.filtro.columnas}"/> *
                            </logic:equal>
                        </logic:notEmpty>
                    </logic:equal>
                    
                    <%-- PopUp   --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="4" scope="page">
                        <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                            <asf:selectpop name="f${columna.id}" columns="${columna.filtro.columnas}" captions="${columna.filtro.etiquetas}" values="${columna.filtro.valores}" entityName="${columna.filtro.tabla}" />
                        </logic:equal>
                        <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                            <asf:selectpop name="f${columna.id}" columns="${columna.filtro.columnas}" captions="${columna.filtro.etiquetas}" values="${columna.filtro.valores}" entityName="${columna.filtro.tabla}" /> *
                        </logic:equal>
                    </logic:equal>	
                    <%-- Si/No   --%>	
                    <logic:equal name="columna" property="filtro.tipo" value="5" scope="page">
                        <logic:equal name="columna" property="obligatorio" value="0" scope="page">
                            <asf:select name="f${columna.id}" value="${columna.valor}" listValues="0,1" listCaption="No,Si" nullText="${columna.filtro.nulltext}" nullValue="${columna.filtro.nullvalueTpf}"/>
                        </logic:equal>
                        <logic:equal name="columna" property="obligatorio" value="1" scope="page">
                            <asf:select name="f${columna.id}" value="${columna.valor}" listValues="0,1" listCaption="No,Si"/> *
                        </logic:equal>
                    </logic:equal>	
                </td>
            </tr>
        </logic:iterate>
    </table>
    <div align="center">
        <p style="{font-size:9pt}"><bean:write name="leyenda"/></p>
    </div>
    
    <div align="center">
        <input type="button" value="Volver" onclick="window.location='http://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/actions/SQLReport.do?do=show';">
    <html:submit value="Continuar"/></div>
    
    
</html:form>
<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="${pageContext.request.contextPath}/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>

