import java.awt.BorderLayout;
import java.io.PrintWriter;
import java.net.URL;

import javax.swing.*;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JasperPrintFactory;
import net.sf.jasperreports.view.*;

public class Applet extends JApplet{
		
	private JasperPrint jrprint = null;
	private JPanel panel = null; 

	public Applet(){
		panel = new JPanel();
		
		panel.setLayout(new BorderLayout());
		getContentPane().add(panel,BorderLayout.CENTER);
		//panel.remove();
		
	}
	
	public void init(){
		
		String reporte = getParameter("REPORT_URL").toString();
		
		if(reporte != null){
			
			try{
				jrprint = (JasperPrint)JRLoader.loadObject(new URL( getCodeBase(), reporte));
				
				if( getParameter("PRINT").equalsIgnoreCase("true") )					
					JasperPrintManager.printReport(jrprint,true);
				else{
					JRViewer jrview = new JRViewer(jrprint);
					panel.add(jrview, BorderLayout.CENTER);
				}
				
			}catch(Exception e){}	
			
		}
		
	}

}


