<%@ page language="java"%>
<%@ page import="com.asf.grh.hibernate.*"%>
<%@ page import="com.asf.security.BusinessPersistance"%>
<%@ page import="com.asf.security.SessionHandler"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html"
	prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic"
	prefix="logic"%>
<%@ taglib uri="/WEB-INF/asf-taglib.tld" prefix="asf"%>
<%@page import="com.asf.util.DirectorHelper"%>
<%
	request.setAttribute("URL_ADE", DirectorHelper.getString("URL.ADE"));
%>
<div class="row">
	<div class="col-lg-9 col-sm-9">
		<h1 class="title">Legajo Digital</h1>
	</div>
	<div class="col-lg-3 col-sm-3" style="text-align: right;">
		<button type="button" class="btn btn-default new-usuario"
			id="addElement" onclick="go_MainPage();">
			<i class="fa fa-home" aria-hidden="true"></i>&nbsp;Home
		</button>
	</div>
</div>


<div id="module-grid" class="template-style"></div>
<div id="module-form"></div>
<div id="module-details"></div>

<script>
	var allowArchivoVisualizar = false;
	var allowLegajoDigitalDelete = false;
	var allowArchivoLegajoDigitalEdit = false;
	var allowObservacionLegajoDigitalEdit = false;
	var allowArchivoLegajoDigitalDelete = false;
	var allowObservacionLegajoDigitalDelete = false;
</script>
<asf:security
	action="/actions/process.do?do=process&processName=ArchivoLegajoDigitalVisualizar">
	<script>
		allowArchivoVisualizar = true;
	</script>
</asf:security>

<asf:security action="/actions/process.do"
	access="do=process&processName=LegajoDigital&process.accion=doDelete">
	<script>
		allowLegajoDigitalDelete = true;
	</script>
</asf:security>

<asf:security action="/actions/process.do"
	access="do=process&processName=ArchivoLegajoDigital&process.accion=doPost">
	<script>
		allowArchivoLegajoDigitalEdit = true;
	</script>
</asf:security>

<asf:security action="/actions/process.do"
	access="do=process&processName=ObservacionLegajoDigital&process.accion=doPost">
	<script>
		allowObservacionLegajoDigitalEdit = true;
	</script>
</asf:security>

<asf:security action="/actions/process.do"
	access="do=process&processName=ArchivoLegajoDigital&process.accion=doDelete">
	<script>
		allowArchivoLegajoDigitalDelete = true;
	</script>
</asf:security>

<asf:security action="/actions/process.do"
	access="do=process&processName=ObservacionLegajoDigital&process.accion=doDelete">
	<script>
		allowObservacionLegajoDigitalDelete = true;
	</script>
</asf:security>

<script type="text/javascript">
	/**
	 * Objetos dTable globales
	 */
	var dTable_legajoDigital = null;
	var dTable_observacionLegajoDigital = null;
	var dTable_archivoLegajoDigital = null;
	var dTable_searchArchivo = null;

	var dt_personalizacion_observacionLegajoDigital = {};
	var dt_personalizacion_archivoLegajoDigital = {};
	/**
	 * Objetos String para filtros (filter)
	 */
	var filterTipo = "";
	var filterLegajoDigital = "";

	/**
	 * Carga de Contenedores de Grillas (grid_)
	 * ** Oculta-descarga secciones del formulario
	 * ** Carga la p�gina [nombreEntidad]List, que muestra la grilla
	 */

	function grid_LegajoDigital() {
		HoldOn.open();
		$j('#module-form').empty();
		$j('#module-details').empty();

		$j('#module-grid').load(pathSistema + '/lists/legajoDigitalList.jsp',
				function() {
					HoldOn.close();
				}).fadeIn();
	}

	function grid_ObservacionLegajoDigital(idTipo) {
		HoldOn.open();
		if (idTipo != -1) {
			filterTipo = idTipo;
		}
		$j('#form_LegajoDigital > #tipo').val(filterTipo);
		$j('#beta-form').empty();
		$j('#beta-grid').load(
				pathSistema + '/lists/observacionLegajoDigitalList.jsp?idTipo='
						+ filterTipo + '&idLegajoDigital='
						+ filterLegajoDigital, function() {
					var sw_Title;
					switch (idTipo) {
					case 1:
						sw_Title = "Observaciones Generales";
						break;
					case 2:
						sw_Title = "Observaciones T&eacute;cnicas";
						break;
					case 3:
						sw_Title = "Observaciones Contables";
						break;
					case 4:
						sw_Title = "Observaciones Jur&iacute;dicas";
						break;
					}
					$j("#title_grilla_observacion").html(sw_Title);
					HoldOn.close();
				}).fadeIn();
	}

	function grid_ArchivoLegajoDigital(idTipo) {
		HoldOn.open();
		if (idTipo != -1) {
			filterTipo = idTipo;
		}
		$j('#form_LegajoDigital > #tipo').val(filterTipo);
		$j('#beta-form').empty();
		$j('#beta-grid').load(
				pathSistema + '/lists/archivoLegajoDigitalList.jsp?idTipo='
						+ filterTipo + '&idLegajoDigital='
						+ filterLegajoDigital, function() {
					var sw_Title;
					switch (filterTipo) {
					case 1:
						sw_Title = "Documentaci&oacute;n General";
						break;
					case 2:
						sw_Title = "Documentaci&oacute;n T&eacute;cnica";
						break;
					case 3:
						sw_Title = "Documentaci&oacute;n Contable";
						break;
					case 4:
						sw_Title = "Documentaci&oacute;n Jur&iacute;dica";
						break;
					}
					$j("#title_grilla_archivo").html(sw_Title);
					HoldOn.close();

				}).fadeIn();
		variosId = "";
	}

	function dashboard_ObservacionLegajoDigital() {

		HoldOn.open();
		$j('#beta-grid').empty();
		$j('#beta-form')
				.load(
						pathSistema
								+ '/lists/observacionLegajoDigitalDashboard.jsp?idLegajoDigital='
								+ filterLegajoDigital, function() {
							HoldOn.close();
						}).fadeIn();
	}

	function back_Details_Principal() {

	}

	/* Definicion de Grillas (dt_personalizacion) */

	var dt_personalizacion_legajoDigital = {
		"iDisplayLength" : 50,
		"lengthMenu" : [ [ 5, 10, 25, 50, 100, 500, -1 ],
				[ 5, 10, 25, 50, 100, 500, "Todos" ] ],
		"sAjaxSource" : pathSistema + "/rest/LegajoDigitalAction.do?do=doGet",
		"dom" : 'Bfrtip',
		"columns" : [ {
			"title" : ""
		}, {
			"title" : "Apellido y Nombre / Raz&oacute;n Social",
			"data" : "p_NOMB_12"
		}, {
			"title" : "CUIT",
			"data" : "p_CUIL_12"
		}, {
			"title" : "Expediente"
		}, {
			"title" : "Doc. Vencida"
		}, {
			"title" : "Obs."
		},

		],
		"columnDefs" : [
				{
					"render" : function(data, type, row) {
						return printProfileGrid(row.l_id);
					},
					"className" : "",
					"visible" : true,
					"searchable" : false,
					"targets" : 0
				},
				{
					"render" : function(data, type, row) {
						return goExpediente(row.l_expediente);
					},
					"className" : "",
					"visible" : true,
					"searchable" : true,
					"targets" : 3
				},
				{
					"render" : function(data, type, row) {
						return getDocumentacionVencida(row.l_id);
					},
					"className" : "",
					"visible" : true,
					"searchable" : true,
					"targets" : 4
				},
				{
					"render" : function(data, type, row) {
						return getObservacionesCount(row.l_id);
					},
					"className" : "",
					"visible" : true,
					"searchable" : true,
					"targets" : 5
				},

				{
					"title" : "Acciones",
					"render" : function(data, type, row, meta) {
						var acciones = [];
						acciones.push([ "consultar",
								"details_LegajoDigital(" + row.l_id + ")" ]);

						if (allowLegajoDigitalDelete == true) {
							acciones.push([ "eliminar",
									"delete_LegajoDigital(" + row.l_id + ")" ]);
						}

						return acciones_dataTable(acciones);
					},
					"className" : "",
					"visible" : true,
					"searchable" : false,
					"targets" : 6
				}, ],
		"buttons" : [ {
			extend : 'excel',
			title : 'Imputaciones',
			text : 'Imprimir XLS',
			exportOptions : {
				columns : [ 0, 1, 2, 3, 4, 5 ]
			}
		}, {
			extend : 'pdfHtml5',
			title : 'Imputaciones',
			text : 'Imprimir PDF',
			pageSize : 'A4',
			exportOptions : {
				columns : [ 0, 1, 2, 3, 4, 5 ]
			}
		} ]
	};

	function format_dt_observacionLegajoDigital() {
		dt_personalizacion_observacionLegajoDigital = {
			ajax : {
				method : "POST",
				url : "rest/ObservacionLegajoDigitalAction.do?do=doGet",
				data : {
					"entidad" : "ObservacionLegajoDigital",
					"campos" : "id,texto,causerK,fecha,mostrar,tipo,legajoDigital.id",
					"filtro" : "tipo=" + filterTipo + " AND legajoDigital = "
							+ filterLegajoDigital,
				},
				dataType : 'json'
			},
			"columns" : [
					{
						"title" : "<input type=\"checkbox\" id=\"checkAllCheck\" value=\"Seleccion\"  onclick=\"selectall()\">"
					}, {
						"title" : "Id",
						"data" : "id",
						"className" : "bold"
					}, {
						"title" : "Observaci�n",
						"data" : "texto"
					}, {
						"title" : "Usuario",
						"data" : "causerK"
					}, {
						"title" : "Fecha",
						"data" : "fecha"
					}, {
						"title" : "Destacada",
						"data" : "mostrar"
					}, ],
			"columnDefs" : [
					{
						"title" : "Acciones",
						"render" : function(data, type, row, meta) {
							var acciones = [];

							if (allowObservacionLegajoDigitalEdit == true) {
								acciones.push([
										"editar",
										"dt_edit_ObservacionLegajoDigital("
												+ row.id + ")" ]);
							}

							if (allowObservacionLegajoDigitalDelete == true) {
								acciones.push([
										"eliminar",
										'dt_delete_ObservacionLegajoDigital('
												+ row.id + ', "' + row.texto
												+ '")' ]);
							}

							return acciones_dataTable(acciones);
						},
						"className" : "",
						"visible" : true,
						"searchable" : false,
						"targets" : 6
					},
					{
						"title" : "Fecha",
						"render" : function(data, type, row, meta) {
							var fechaAr = new Date(row.fecha);
							return fechaAr.toLocaleDateString("es-AR")
						},
						"targets" : 4
					},
					{
						"render" : function(data, type, row, meta) {
							if (row.mostrar == "true") {
								return "Si";
							} else {
								return "No";
							}
						},
						"targets" : 5
					},
					{
						"title" : "Seleccionar",
						data : null,
						className : 'text-center',
						"render" : function(data, type, row, meta) {
							return '<input type="checkbox" id="check_'
									+ data.id
									+ '" class="check" name="check" value="'
									+ data.id + '" onclick=\"selectCheck()\">';
						},
						"targets" : 0,
						"orderable" : false
					} ],
			"buttons" : [ {
				extend : 'excel',
				title : 'Imputaciones',
				text : 'Imprimir XLS',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5 ]
				}
			}, {
				extend : 'pdfHtml5',
				title : 'Imputaciones',
				text : 'Imprimir PDF',
				pageSize : 'A4',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5 ]
				}
			} ]
		};
	}

	function format_dt_archivoLegajoDigital() {
		HoldOn.open();
		dt_personalizacion_archivoLegajoDigital = {

			ajax : {
				method : "POST",
				url : "rest/ArchivoLegajoDigitalAction.do?do=doGet",
				data : {
					//envia los campos que seran las columnas, estas se envian al action
					"entidad" : "ArchivoLegajoDigital",
					"campos" : "id,legajoDigital.id,archivo.id,archivo.tipoDeArchivo.nombre,archivo.nombre,archivo.descripcion,archivo.fecha,archivo.mimetype,orden,desde,hasta,visualizar",
					"filtro" : "tipo = " + filterTipo + " AND legajoDigital = "
							+ filterLegajoDigital,
					"orden" : "orden",
				},
				dataType : 'json'
			},
			"iDisplayLength" : 100,
			"lengthMenu" : [ [ 5, 10, 25, 50, 100, 500, -1 ],
					[ 5, 10, 25, 50, 100, 500, "Todos" ] ],
			"order" : [ [ 1, "asc" ] ],
			"columns" : [
					{
						"title" : "<input type=\"checkbox\" id=\"checkAllCheck\" value=\"Seleccion\"  onclick=\"selectall()\">"
					}, {
						"title" : "Orden",
						"data" : "orden"
					}, {
						"title" : "Tipo de Documento",
						"data" : "archivo_tipoDeArchivo_nombre"
					}, {
						"title" : "Nombre",
						"data" : "archivo_nombre"
					}, {
						"title" : "Descripci&oacute;n",
						"data" : "archivo_descripcion"
					}, {
						"title" : "Fecha Hasta"
					}, {
						"title" : "Visualizar",
						"data" : "visualizar"
					}

			],
			"columnDefs" : [
					{
						"title" : "Seleccionar",
						data : null,
						className : 'text-center',
						"render" : function(data, type, row, meta) {
							return '<input type="checkbox" id="check_'
									+ data.id
									+ '" class="check" name="check" value="'
									+ data.id + '" onclick=\"selectCheck()\">';
						},
						"targets" : 0,
						"orderable" : false
					},
					{
						"title" : "Fecha Hasta",
						"render" : function(data, type, row, meta) {
							var d = new Date(row.hasta);
							var t = new Date();
							if (d < t) {
								return '<span style="color: red">'
										+ getGridDate(row.hasta) + '</span>';
							} else {
								return getGridDate(row.hasta);
							}
						},
						"targets" : 5
					},
					{
						"render" : function(data, type, row, meta) {
							if (row.visualizar == "true") {
								return "Si";
							} else {
								return "No";
							}
						},
						"targets" : 6
					},
					{
						"title" : "Acciones",
						"render" : function(data, type, row, meta) {
							var acciones = [];
							if (allowArchivoLegajoDigitalEdit == true) {
								acciones.push([
										"up",
										"move_archivoLegajoDigital(" + row.id
												+ ", -1)" ]);
								acciones.push([
										"down",
										"move_archivoLegajoDigital(" + row.id
												+ ", 1)" ]);
							}
							var visualizar = false;
							if (row.visualizar == 'true') {
								visualizar = true;
							} else {
								if (allowArchivoVisualizar == true) {
									visualizar = true;
								}
							}
							if (visualizar == true) {
								acciones.push([
										"download",
										"dt_download_ArchivoLegajoDigital("
												+ row.id + ")" ]);
							}
							if (allowArchivoLegajoDigitalEdit == true) {
								acciones.push([
										"editar",
										"dt_edit_ArchivoLegajoDigital("
												+ row.id + ")" ]);
							}
							if (allowArchivoLegajoDigitalDelete == true) {
								acciones.push([
										"eliminar",
										'dt_delete_ArchivoLegajoDigital('
												+ row.id + ', "'
												+ row.archivo_nombre + '")' ]);
							}
							return acciones_dataTable(acciones);
						},
						"className" : "",
						"visible" : true,
						"searchable" : false,
						"targets" : 7
					} ],
			"buttons" : [ {
				extend : 'excel',
				title : 'Imputaciones',
				text : 'Imprimir XLS',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			}, {
				extend : 'pdfHtml5',
				title : 'Imputaciones',
				text : 'Imprimir PDF',
				pageSize : 'A4',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4, 5, 6, 7 ]
				}
			} ]
		};

		var table = $j('#archivoLegajoDigital').DataTable();

		$j('#archivoLegajoDigital')
				.on(
						'page.dt',
						function() {
							var info = table.page.info();
							$j('#pageInfo').html(
									'Showing page: ' + info.page + ' of '
											+ info.pages);
							alert("toco p�ginacion");
						});

		var startTime;

		table.on('preDraw', function() {
			startTime = new Date().getTime();
		}).on(
				'draw.dt',
				function() {
					console.log('Redraw took at: '
							+ (new Date().getTime() - startTime) + 'mS');
					alert(redibujar);
				});

		var eventFired = function(type) {
			var n = $('#demo_info')[0];
			n.innerHTML += '<div>' + type + ' event - ' + new Date().getTime()
					+ '</div>';
			n.scrollTop = n.scrollHeight;
		}

		$j('#archivoLegajoDigital').on('order.dt', function() {
			eventFired('Order');
		}).on('search.dt', function() {
			eventFired('Search');
		}).on('page.dt', function() {
			eventFired('Page');
		}).DataTable();

		HoldOn.close();
	}

	function format_dt_searchArchivo() {
		dt_personalizacion_searchArchivo = {
			"sAjaxSource" : pathSistema
					+ "/rest/ArchivoLegajoDigitalAction.do?do=doSearchArchivo",
			"columns" : [ {
				"title" : "Id",
				"data" : "a_id",
				"className" : "bold"
			}, {
				"title" : "Descripcion",
				"data" : "a_descripcion"
			}, {
				"title" : "Fecha"
			}, {
				"title" : "Nombre Archivo",
				"data" : "a_nombre"
			}, {
				"title" : "Usuario",
				"data" : "a_usuario_CAUSER_K"
			}, {
				"title" : "Nombre Persona",
				"data" : "p_NOMB_12"
			}, {
				"title" : "Documento",
				"data" : "p_NUDO_12"
			}, {
				"title" : "CUIL",
				"data" : "p_CUIL_12"
			}, ],
			"columnDefs" : [
					{
						"title" : "Acciones",
						"render" : function(data, type, row, meta) {
							var acciones = [];
							acciones.push([ "select",
									"select_Archivo(" + row.a_id + ")" ]);
							return acciones_dataTable(acciones);
						},
						"className" : "",
						"visible" : true,
						"searchable" : false,
						"targets" : 8
					}, {
						"title" : "Fecha",
						"render" : function(data, type, row, meta) {
							var fechaAr = new Date(row.a_fecha);
							return fechaAr.toLocaleDateString("es-AR")
						},
						"targets" : 2
					} ],
			"buttons" : [ {
				extend : 'excel',
				title : 'Imputaciones',
				text : 'Imprimir XLS',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4 ]
				}
			}, {
				extend : 'pdfHtml5',
				title : 'Imputaciones',
				text : 'Imprimir PDF',
				pageSize : 'A4',
				exportOptions : {
					columns : [ 0, 1, 2, 3, 4 ]
				}
			} ]
		};
	}

	/* Nuevo formulario en blanco (add_) 
	 * ** Oculta-descarga secciones de las grillas
	 * ** Carga la p�gina [nombreEntidad], que muestra el formulario
	 */

	function add_LegajoDigital() {
		HoldOn.open();
		$j('#module-grid').empty();
		$j('#module-details').empty();

		$j('#module-form').load(pathSistema + '/forms/legajoDigital.jsp',
				function() {
					HoldOn.close();
				}).fadeIn();
	}

	function add_ObservacionLegajoDigital() {
		HoldOn.open();
		$j("#beta-grid").empty();
		$j("#beta-form").load(
				pathSistema + '/forms/observacionLegajoDigital.jsp',
				function() {
					$j('#title_ObservacionLegajoDigital').html(
							"Nueva Observaci&oacute;n "
									+ getNameTipo(filterTipo));
					$j('#form_ObservacionLegajoDigital > #tipo')
							.val(filterTipo);
					HoldOn.close();
				}).fadeIn();
	}

	function add_ArchivoLegajoDigital() {
		HoldOn.open();
		$j("#beta-grid").empty();
		$j(
				'<iframe width="100%" scrolling="no" style="border: none; height: 1500px;" id="frameUploadFile"></iframe>')
				.appendTo('#beta-form');
		frameUploadFile.src = pathSistema
				+ "/actions/ArchivoAction.do?do=legajoDigitalForm&idLegajoDigital="
				+ filterLegajoDigital + "&tipoLegajoDigital=" + filterTipo;
		HoldOn.close();

		$j('#frameUploadFile').on(
				'load',
				function(e) {
					var iFrameContent = $j("#frameUploadFile").contents().find(
							"body").html();
					if (iFrameContent.indexOf("<done></done>") >= 0) {
						grid_ArchivoLegajoDigital(-1);
					}
				});
	}

	function add_LegajoDigitalFoto() {
		HoldOn.open();
		$j("#beta-grid").empty();
		$j("#beta-form").empty();
		$j(
				'<iframe width="100%" height="800" scrolling="no" style="border: none;" id="frameUploadPicture"></iframe>')
				.appendTo('#beta-form');
		frameUploadPicture.src = pathSistema
				+ "/actions/ArchivoAction.do?do=legajoDigitalFotoForm&idLegajoDigital="
				+ filterLegajoDigital;

		$j('#frameUploadPicture')
				.on(
						'load',
						function(e) {
							var iFrameContent = $j("#frameUploadPicture")
									.contents().find("body").html();
							if (iFrameContent.indexOf("<done></done>") >= 0) {
								$j("#img-perfil-user")
										.attr(
												"src",
												pathSistema
														+ "/actions/ArchivoAction.do?do=legajoDigitalPrintProfile&legajoDigital_id="
														+ filterLegajoDigital
														+ '&rnd='
														+ (new Date())
																.getTime());
								dashboard_ObservacionLegajoDigital();
							}
							HoldOn.close();
						});
	}

	function search_Archivo() {
		HoldOn.open();
		$j("#beta-grid").empty();
		$j("#beta-form").load(pathSistema + '/forms/searchArchivo.jsp',
				function() {
					HoldOn.close();
				}).fadeIn();
	}

	/* Guardar formularios (save_) */
	function save_LegajoDigital() {
		crud.save({
			url : pathSistema + '/rest/LegajoDigitalAction.do?do=doPost',
			formId : '#form_LegajoDigital'
		}, function() {
			grid_LegajoDigital();
		});
	}

	function save_ObservacionLegajoDigital() {
		crud.save({
			url : pathSistema
					+ '/rest/ObservacionLegajoDigitalAction.do?do=doPost',
			formId : '#form_ObservacionLegajoDigital',
			saveButtonId : '#btn-observacion'
		}, function() {
			grid_ObservacionLegajoDigital(filterTipo);
		}, function() {
			$j('#form_ObservacionLegajoDigital > #legajoDigital_id').val(
					filterLegajoDigital);
			$j('#form_ObservacionLegajoDigital > #tipo').val(filterTipo);
		});
	}

	function select_Archivo(idArchivo) {
		HoldOn.open();
		jQuery
				.ajax({
					type : "POST",
					async : true,
					url : pathSistema
							+ '/rest/ArchivoLegajoDigitalAction.do?do=doPost',
					data : {
						idArchivo : idArchivo,
						idLegajoDigital : filterLegajoDigital,
						tipo : filterTipo,
					},
					success : function(data) {
						grid_ArchivoLegajoDigital(-1)
					},
					error : function(data) {
						HoldOn.close();
					}
				});
	}

	function save_ArchivoLegajoDigital() {
		crud.save(
				{
					url : pathSistema
							+ '/rest/ArchivoLegajoDigitalAction.do?do=doPost',
					formId : '#form_ArchivoLegajoDigital',
					saveButtonId : '#btn-archivo',
					contentType : 'multipart/form-data',
					enctype : 'multipart/form-data',

				}, function() {
					grid_ArchivoLegajoDigital(filterTipo);
				}, function() {
					$j('#form_ArchivoLegajoDigital > #legajoDigital_id').val(
							filterLegajoDigital);
					$j('#form_ArchivoLegajoDigital > #tipo').val(filterTipo);
				});
	}

	function move_archivoLegajoDigital(id, step) {

		jQuery.ajax({
			type : "POST",
			async : true,
			url : pathSistema
					+ '/rest/ArchivoLegajoDigitalAction.do?do=doMoveOrder',
			data : {
				id : id,
				step : step,
			},
			success : function() {
				grid_ArchivoLegajoDigital(-1)
			}
		});
	}

	function report_archivoLegajoDigital() {
		window.open(pathSistema
				+ '/rest/LegajoDigitalAction.do?do=doReport&id='
				+ filterLegajoDigital, "_blank");
	}

	/* Detalles de la p�gina (details_) */

	function details_LegajoDigital(id) {
		HoldOn.open();
		$j('#module-form').empty();
		$j('#module-grid').empty();

		$j('#module-details')
				.load(
						pathSistema + '/forms/legajoDigitalDetails.jsp?id='
								+ id,
						function() {
							filtroLegajoDigital = id;
							dashboard_ObservacionLegajoDigital();
							var imgSrc = pathSistema
									+ "/actions/ArchivoAction.do?do=legajoDigitalPrintProfile&legajoDigital_id="
									+ id + "&rnd=" + (new Date()).getTime();
							var imgDefault = pathSistema + "/images/avatar.jpg";

							var imagen = new Image();
							imagen.src = imgSrc;
							imagen.onload = function() {
								$j("#img-perfil-user").attr("src", imgSrc);
							}
							imagen.onerror = function() {
								$j("#img-perfil-user").attr("src", imgDefault);
							}
							imagen.onabort = function() {
								$j("#img-perfil-user").attr("src", imgDefault);
							}

							HoldOn.close();
						}).fadeIn();
	}

	/* Carga de formulario para su edici�n (dt_edit) */
	function dt_edit_ObservacionLegajoDigital(id) {
		$j("#beta-grid").empty();
		$j("#beta-form")
				.load(
						pathSistema + '/forms/observacionLegajoDigital.jsp',
						function() {
							crud
									.populate(
											{
												url : pathSistema
														+ "/rest/ObservacionLegajoDigitalAction.do?do=doGet",
												id : id,
												formId : '#form_ObservacionLegajoDigital',
												entidad : 'ObservacionLegajoDigital'
											},
											function() {
												$j(
														'#title_ObservacionLegajoDigital')
														.html(
																"Editar Observaci&oacute;n "
																		+ getNameTipo(filterTipo));
											});
						}).fadeIn();
	}

	function dt_edit_ArchivoLegajoDigital(id) {
		HoldOn.open();
		$j("#beta-grid").empty();
		$j(
				'<iframe width="100%" height="800" scrolling="no" style="border: none;" id="frameUploadFile"></iframe>')
				.appendTo('#beta-form');
		frameUploadFile.src = pathSistema
				+ "/actions/ArchivoAction.do?do=legajoDigitalForm&idArchivoLegajoDigital="
				+ id;
		HoldOn.close();

		$j('#frameUploadFile').on(
				'load',
				function(e) {
					var iFrameContent = $j("#frameUploadFile").contents().find(
							"body").html();
					if (iFrameContent.indexOf("<done></done>") >= 0) {
						grid_ArchivoLegajoDigital(-1);
					}
				});
	}

	/* Delete de elementos (delete_) */
	function delete_LegajoDigital(id) {
		crud.deleteL({
			url : pathSistema + "/rest/LegajoDigitalAction.do?do=doDelete",
			id : id,
			html : "&iquest;Desea eliminar este Legajo Digital?",
			dTable : dTable_legajoDigital,
		});
	}

	function dt_delete_ObservacionLegajoDigital(id, texto) {
		crud.deleteL({
			url : pathSistema
					+ "/rest/ObservacionLegajoDigitalAction.do?do=doDelete",
			id : id,
			title : "Eliminar Observaci&oacute;n",
			html : "<p>" + texto + "</p>",
			dTable : dTable_observacionLegajoDigital
		});
	}

	function dt_delete_ArchivoLegajoDigital(id, texto) {
		crud.deleteL({
			url : pathSistema
					+ "/rest/ArchivoLegajoDigitalAction.do?do=doDelete",
			id : id,
			title : "Eliminar Archivo Digital",
			html : "<p>" + texto + "</p>",
			dTable : dTable_archivoLegajoDigital
		});
	}

	/*
	 * Funciones a�adidas espec�ficas del M�dulo
	 */
	function getNameTipo(tipo) {
		switch (tipo) {
		case 1:
			return "General";
			break;
		case 2:
			return "T&eacute;cnica";
			break;
		case 3:
			return "Contable";
			break;
		case 4:
			return "Jur&iacute;dica";
			break;
		}
	}
	function go_MainPage() {
		window.location.href = pathSistema;
	}

	function printProfileGrid(id) {

		var imgSrc = pathSistema
				+ "/actions/ArchivoAction.do?do=legajoDigitalPrintProfile&legajoDigital_id="
				+ id + "&rnd=" + (new Date()).getTime();
		var imgDefault = pathSistema + "/images/avatar.jpg";

		var imagen = new Image();
		imagen.src = imgSrc;
		imagen.onload = function() {
			$j("#img_" + id).attr("src", imgSrc);
		}
		imagen.onerror = function() {
			$j("#img_" + id).attr("src", imgDefault);
		}
		imagen.onabort = function() {
			$j("#img_" + id).attr("src", imgDefault);

		}
		return "<img style='height: 50px; width: 50px;' class='img-responsive' id='img_" + id + "' src='" + imgDefault + "'/>";
	}

	function getDocumentacionVencida(legajoDigitalId) {
		loadDocumentacionVencida(legajoDigitalId);
		return '<h6 style="text-align: center; font-weight: normal;" id="documentos_vencidos_' + legajoDigitalId + '"></h6>';

	}

	function getObservacionesCount(legajoDigitalId) {
		loadObservacionesCount(legajoDigitalId);
		return '<h6 style="text-align: center; font-weight: normal;" id="observaciones_count_' + legajoDigitalId + '"></h6>';

	}

	function loadDocumentacionVencida(legajoDigitalId) {
		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth() + 1; //Months are zero based
		var curr_year = d.getFullYear();
		if (curr_month < 10) {
			var sql_date = curr_year + '-0' + curr_month + '-' + curr_date;
		} else {
			var sql_date = curr_year + '-' + curr_month + '-' + curr_date;
		}

		var htmlInner;
		jQuery
				.ajax({
					type : "POST",
					async : true,
					url : pathSistema
							+ '/rest/ArchivoLegajoDigitalAction.do?do=doGet',
					dataType : 'json',
					data : {
						"entidad" : "ArchivoLegajoDigital",
						"campos" : "id",
						"filtro" : "legajoDigital = " + legajoDigitalId
								+ " AND hasta < '" + sql_date + "T00:00:00'",
					},
					success : function(response) {
						var l = response.data.length;
						if (l > 0) {
							htmlInner = '<span class="badge badge-pill badge-danger">'
									+ '<span style="font-weight: bold;">'
									+ l
									+ '</span>'
									+ '<span style="font-weight: initial">&nbsp;Vencidos</span>'
									+ '</span>';
							$j('#documentos_vencidos_' + legajoDigitalId).html(
									htmlInner);
						}
					},
				});

	}

	function loadObservacionesCount(legajoDigitalId) {
		var htmlInner;
		jQuery
				.ajax({
					type : "POST",
					async : true,
					url : pathSistema
							+ '/rest/ObservacionLegajoDigitalAction.do?do=doGet',
					dataType : 'json',
					data : {
						"entidad" : "ObservacionLegajoDigital",
						"campos" : "id",
						"filtro" : "legajoDigital = " + legajoDigitalId,
					},
					success : function(response) {
						var l = response.data.length;
						if (l > 0) {
							htmlInner = '<span class="badge badge-pill badge-warning">'
									+ '<span style="font-weight: bold;">'
									+ l
									+ '</span>'
									+ '<span style="font-weight: initial">&nbsp;Observaciones</span>'
									+ '</span>';
							$j('#observaciones_count_' + legajoDigitalId).html(
									htmlInner);
						}
					},
				});

	}

	function getGridDate(d) {
		var d = new Date(d);
		if (Object.prototype.toString.call(d) === "[object Date]") {
			if (isNaN(d.getTime())) { // d.valueOf() could also work
				return '';
			} else {
				return d.toLocaleDateString("es-AR");
			}
		} else {
			return '';
		}
	}

	function goExpediente(codigoExpediente) {

		if (codigoExpediente != null) {
			jQuery
					.ajax({
						type : "GET",
						url : pathSistema
								+ '/rest/LegajoDigitalAction.do?do=doGetDocumentoAdeId',
						dataType : 'json',
						data : {
							"codigoExpediente" : codigoExpediente,
						},
						success : function(response) {
							if (response.data.length > 0) {
								url_ade = "${URL_ADE}";
								url = url_ade
										+ "/actions/process.do?processName=DocumentoHelper&process.id="
										+ response.data[0].id
										+ "&do=process&process.action=show";
								r = '<a onclick="window.open(\''
										+ url
										+ '\', \'_blank\');" style="cursor: pointer;">'
										+ codigoExpediente + '</a>';
								$j('#link_ade_' + codigoExpediente).html(r);

							}
						},
					});
		}
		return '<div id="link_ade_' + codigoExpediente + '"></div>';
	}

	function Descarga() {
		//saco la coma al final
		if (variosId.charAt(variosId.length - 1) == ","
				|| variosId.charAt(variosId.length - 1) == ",,") {
			variosId = variosId.substr(0, variosId.length - 1);
			variosId = variosId.substr(1);

		}
		var selecciones;
		if (dTable_archivoLegajoDigital != null) {
			selecciones = dTable_archivoLegajoDigital.tables().rows().data();
		} else {
			if (dTable_observacionLegajoDigital.valueOf != null) {
				selecciones = dTable_observacionLegajoDigital.tables().rows()
						.data();
			}
		}
		if (selecciones != null && selecciones.length > 0) {
			try {
				var envioId = "";
				envioId = variosId;

				if (envioId != "") {
					dt_download_ArchivoLegajoDigitalAll(envioId);
				} else {
					alert("Debe Selecionar Items a Descargar");
				}

			} catch (e) {
				console
						.error("EXPORTADOR MASIVO: Error al ejecutar CONSULTAR.");
				HoldOn.close();
			}
		}
		variosId = "";
		checkAllCheck.checked = 0;
		checkboxes = document.getElementsByName('check');
		for (var i = 0, n = checkboxes.length; i < n; i++) {
			checkboxes[i].checked = 0;
		}

	}

	function dt_download_ArchivoLegajoDigitalAll(id) {
		HoldOn.open();
		window.location = pathSistema
				+ "/actions/ArchivoAction.do?do=legajoDigitalDownloadAll&id="
				+ id;
		HoldOn.close();
	}
	function dt_download_ArchivoLegajoDigital(id) {
		window.location = pathSistema
				+ "/actions/ArchivoAction.do?do=legajoDigitalDownload&id=" + id;
	}

	function toggle(source) {
		checkboxes = document.getElementsById('checkall');
		for ( var i in checkboxes)
			checkboxes[i].checked = source.checked;
	}
	/*seleciona todos los check de la tabla */
	var variosId;
	function seleccionoCheck(source) {
		//meto en una variable el elemento cuyo nombre es check
		checkboxes = document.getElementsByName('check');

		if (checkAllCheck.checked == false) {
			for (var i = 0, n = checkboxes.length; i < n; i++) {
				checkboxes[i].checked = 0;
			}
		}

		if (checkAllCheck.checked == true) {
			for (var i = 0, n = checkboxes.length; i < n; i++) {
				//pongo todos los check en 1
				checkboxes[i].checked = 1;
			}
		}
		selectCheck();
	}

	function selectall() {
		seleccionoCheck(this);
	}

	var variosId = "";
	function selectCheck() {
		/*variable q me dara el tama�o del arreglo */
		var idsOp = (variosId != null && variosId != "" ? variosId.toString()
				.split(",") : null);
		var selecciones;
		if (dTable_archivoLegajoDigital != null) {
			selecciones = dTable_archivoLegajoDigital.tables().rows().data();
		} else {
			if (dTable_observacionLegajoDigital.valueOf != null) {
				selecciones = dTable_observacionLegajoDigital.tables().rows()
						.data();
			}
		}
		checkboxes = document.getElementsByName('check');

		//verifico si el ultimo caracter es una coma y lo elimino	 
		if (checkAllCheck.checked == true || checkAllCheck.checked == false) {
			for (var i = 0; i < checkboxes.length; i++) {
				//verifico si esta checkeado en false
				if (checkboxes[i].checked == false) {
					checkAllCheck.checked = 0;
					//compardo para saber contiene el valor en el string concatenado			
					if ((variosId.search(checkboxes[i].value.toString)) != 0) {
						var busco = "," + checkboxes[i].value.toString() + ",";
						var re = new RegExp(busco, 'g');
						var buscoElimino = checkboxes[i].value.toString() + ",";
						if (variosId.includes(busco) == true) {
							variosId = variosId.replace(buscoElimino, "");
						}
					}
				}
				if (checkboxes[i].checked == true) {
					//comparo q no exista
					var busco = "," + checkboxes[i].value.toString() + ",";
					var busco1 = checkboxes[i].value.toString() + ",";
					if ((variosId.search(busco)) == -1
							|| (variosId.search(busco1)) == -1) {
						if (variosId == "") {
							//revisar que valor trae. por q conviene ponerle el id
							variosId = "," + checkboxes[i].value.toString()
									+ ",";
						} else {
							variosId = variosId
									+ checkboxes[i].value.toString() + ",";
						}
					}
				}
			}
		}
	}

	$j(document).ready(function() {
		grid_LegajoDigital();
	});
</script>