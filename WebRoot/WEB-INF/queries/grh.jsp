<%@ page session="true" contentType="text/html; charset=ISO-8859-1" %>
<jsp:directive.page import="com.asf.security.SessionHandler"/>
<%@ taglib uri="/WEB-INF/jpivot/jpivot-tags.tld" prefix="jp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>


<jp:mondrianQuery id="query01" jdbcDriver="oracle.jdbc.driver.OracleDriver" jdbcUrl="jdbc:oracle:thin:@zeus:1521:civitas" jdbcUser="grh_roca" jdbcPassword="grh" catalogUri="/WEB-INF/queries/grh.mondrian.xml">
select
  {[Measures].[Total Pagado], [Measures].[Cantidad de Legajos]} on 0,
  {[Periodo].[Todos Periodos]} on 1
from grh
	WHERE ([Concepto].[999])
</jp:mondrianQuery>

<c:set var="title01" scope="session">An�lisis de Liquidaciones</c:set>
