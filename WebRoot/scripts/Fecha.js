/**
 * autor: diegobq version: 1.0
 */

// ==============ATRIBUTOS=============
var fecha;
var separador = "/";
var formato = "d/m/a";

var msjError;
// =============CONSTRUCTOR=============
function Fecha(separador) {
	this.msjError = "";

	this.separador = (separador != null || separador == "") ? separador : "/";
	// funcionalidades.-
	this.toString = toString; // Convierte una Date js a string.-
	this.toDate = toDate; // Convierte una fecha string en un Date js.-
	this.validaStr = validaStr; // Valida que un string de la fecha sea
								// correcta.-
	this.validaDate = validaDate; // Valida que un date de js sea correcto.-
	this.equals = equals; // Valida si dos objetos Date js son iguales.-
	this.compareTo = compareTo; // Compara las fechas de dos objetos Date js sin
								// tener en cuenta horas, minutos y segundos.-
	this.compareToStr = compareToStr; // Compara las fechas de dos objetos
										// Date js sin tener en cuenta horas,
										// minutos y segundos.-
	this.getYearStr = getYearStr; // retorna el a�o de una fecha que recibe
									// como string.-
	this.getYear = getYear; // retorna el a�o de una fecha que recibe como date
							// js.-

	// getters y setters.-
	this.setFecha = setFecha;
	this.setFechaStr = setFechaStr;
	this.getError = getError;
}

// ===========FUNCIONALIDAD=============
/**
 * @funcionalidad: Convierte una Date js a string.-
 * 
 * @descripci�n: Si el Date js no es correcto retorna vac�o ( "" ) .-
 */
function toString(fecha) {
	var fechaStr = "";

	if (fecha != null) {
		// armamos el d�a.-
		if (fecha.getDate() < 10) {
			fechaStr += "0";
		}
		fechaStr += fecha.getDate();
		fechaStr += this.separador;

		// armamos el mes.-
		if ((fecha.getMonth() + 1) < 10) {
			fechaStr += "0";
		}
		fechaStr += fecha.getMonth() + 1;
		fechaStr += this.separador;

		// armamos el a�o.-
		fechaStr += fecha.getFullYear();
	}

	return fechaStr;
}// fin toString.-

/**
 * @funcionalidad: Convierte una fecha string en un Date js.-
 * 
 * @descripci�n: Si la fecha est� mal armada o es incorrecta, retorna null.-
 */
function toDate(fechaStr) {
	var fecha = null;

	this.msjError = "";

	// si se envi� algo por par�metro
	if (fechaStr != null && fechaStr != "") {
		var arrayFecha = fechaStr.split(this.separador); // parseamos el
															// string de la
															// fecha.-
		if (arrayFecha.length == 3) {
			var anio = new Number(arrayFecha[2]); // cargamos el a�o.-
			var mes = new Number(arrayFecha[1] - 1); // cargamos el mes.-
			var dia = new Number(arrayFecha[0]); // cargamos el d�a.-
			fecha = new Date(anio, mes, dia);

			if (parseInt(fecha.getDate()) != dia) {
				fecha = null;
				this.msjError += "El d�a ingresado es incorrecto\n";
			} else if (parseInt(fecha.getMonth()) != mes) {
				fecha = null;
				this.msjError += "El mes ingresado es incorrecto\n";
			} else if (parseInt(fecha.getFullYear()) != anio) {
				fecha = null;
				this.msjError += "El a�o ingresado es incorrecto\n";
			}
		}
	}

	return fecha;
}// fin toDate.-

/**
 * @funcionalidad: Valida que una fecha sea correcta.-
 * 
 * @param fechaStr:
 *            string de la fecha.-
 */
function validaStr(fechaStr) {
	return this.toDate(fechaStr) != null;
}// fin validaStr.-

/**
 * @funcionalidad: Valida que una fecha sea correcta.-
 * 
 * @param fecha:
 *            date de js.-
 */
function validaDate(fecha) {
	return this.toString(fecha) != "";
}// fin validaDate.-

/**
 * @funcionalidad: Valida si dos objetos Date js son iguales.-
 * 
 * @descripci�n: Compara dos objetos de tipo Date js El primer objeto Date lo
 *               obtiene de this.fecha, o sea que hay que setearlo antes de
 *               llamar este m�todo.- El segundo objeto Date lo obtiene del
 *               par�metro, o sea que el par�metro fechaAComparar tiene que ser
 *               un Date.-
 * 
 * @resultados Si son iguales (sin considerar horas, minutos y segundos) retorna
 *             true, sino false.- Si ocurre alg�n problema retorna false.-
 * 
 */
function equals(fechaAComparar) {
	var iguales = false;

	iguales = parseInt(this.compareTo(fechaAComparar)) == parseInt(0);

	return iguales != null ? iguales : false;
}// fin equals.-

/**
 * @func: Compara las fechas de dos objetos Date js sin tener en cuenta horas,
 *        minutos y segundos.-
 * 
 * @descripci�n: Compara dos objetos de tipo Date js El primer objeto Date lo
 *               obtiene de this.fecha, o sea que hay que setearlo antes de
 *               llamar este m�todo.- El segundo objeto Date lo obtiene del
 *               par�metro, o sea que el par�metro fechaAComparar tiene que ser
 *               un Date.-
 * 
 * @resultados Realiza la diferencia entre los objetos, retorna la diferencia en
 *             milisegundos: Si this.fecha = fechaAComparar => retorna 0; Si
 *             this.fecha > fechaAComparar => retorna positivo; Si this.fecha <
 *             fechaAComparar => retorna negativo;
 * 
 */
function compareTo(fechaAComparar) {
	var diferencia = null;

	fechaAComparar = getDate(fechaAComparar);

	if (this.fecha != null && fechaAComparar != null) {
		diferencia = this.fecha - fechaAComparar;
	}

	return diferencia;
}// fin compareTo.-

/**
 * @func: Compara las fechas de dos objetos Date js sin tener en cuenta horas,
 *        minutos y segundos.-
 * 
 * @descripci�n: Compara dos fechas El primer objeto Date lo obtiene de
 *               this.fecha, o sea que hay que setearlo antes de llamar este
 *               m�todo.- El segundo objeto String lo obtiene del par�metro, o
 *               sea que el par�metro fechaACompararStr tiene que ser un
 *               String.-
 */
function compareToStr(fechaACompararStr) {
	var diferencia = null;
	var fechaAComparar = toDate(fechaACompararStr);

	diferencia = this.compareTo(fechaAComparar);

	return diferencia;
}// fin compareToStr.-

/**
 * Retorna el a�o de la fecha que recibe como par�metro.- Recibe la fecha como
 * un string con formato dd/mm/aaaa
 */
function getYearStr(fechaStr) {
	var year = null;
	var fecha = this.toDate(fechaStr);

	year = this.getYear(fecha);

	return year;
}// fin getYearStr.-

/**
 * Retorna el a�o de la fecha que recibe como par�metro.- Recibe la fecha como
 * date de js.-
 */
function getYear(fecha) {
	var year = null;

	if (fecha != null) {
		year = fecha.getFullYear();
	}

	return year;
}// fin getYear.-

// ==================FUNCIONALIDAD PRIVADA=======================
/**
 * Retorna un objeto de tipo Date js Para crearlo toma el d�a, el mes y el a�o
 * del par�metro fecha.- La hora, los minutos y los segundos van en cero.-
 */
function getDate(fecha) {
	var nuevaFecha = null;

	if (fecha != null && fecha instanceof Date) {
		nuevaFecha = new Date(fecha.getFullYear(), fecha.getMonth(), fecha
				.getDate());
	}

	return nuevaFecha;
}// fin getDate.-

// ================GETTERS Y SETTERS==============
function getError() {
	var errorStr = this.msjError;

	this.msjError = "";

	return errorStr;
}

function setFecha(fecha) {
	this.fecha = getDate(fecha);
}

function setFechaStr(fechaStr) {
	this.fecha = getDate(this.toDate(fechaStr));
}