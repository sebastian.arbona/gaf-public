/**
 * Parsea el xml que recibe por par�metros.- Retorna un objeto donde los
 * atributos son los elementos del xml.-
 */
function parseXml(xml) {
	var dom = null;
	if (window.DOMParser) {
		try {
			dom = (new DOMParser()).parseFromString(xml, "text/xml");
		} catch (e) {
			dom = null;
		}
	} else if (window.ActiveXObject) {
		try {
			dom = new ActiveXObject('Microsoft.XMLDOM');
			dom.async = false;
			if (!dom.loadXML(xml)) // parse error ..
			{
				dom.loadXML("<root>No anduvo!!!</root>")
			}
			// window.alert(dom.parseError.reason + dom.parseError.srcText);
		} catch (e) {
			dom = null;
		}
	} else {
		alert("cannot parse xml string!");
	}
	return eval('(' + xml2json(dom, "") + ')');
}

function leftTrim(str) {
	while (str.substring(0, 1) == ' ')
		str = str.substring(1, str.length);

	return str;
}
function rightTrim(str) {
	while (str.substring(str.length - 1, str.length) == ' ')
		str = str.substring(0, str.length - 1);

	return str;
}

function trim(str) {
	return leftTrim(rightTrim(str));
}

function num(e) {
	var key;
	if (e.keyCode)
		key = e.keyCode;
	else
		key = e.which;

	if (key != 8 && key != 9 && key != 44 && key != 46) {
		if (key < 48 || key > 57) {
			if (e.which)
				e.preventDefault(true);
			else
				e.returnValue = false;
		}
	}
}

function mostrar() {
	for (var args = 0; args < mostrar.arguments.length; args++) {
		getElem(mostrar.arguments[args]).style.display = "";
	}

}

/*
 * Descripci�n: Oculta un n�mero variable de tags div. Requisitos: tag div
 * deber� de tener la propiedad id asignada con el id que corresponda al
 * elemento del combo. @Autor: aromeo.
 */
function ocultar() {
	for (var args = 0; args < ocultar.arguments.length; args++)
		getElem(ocultar.arguments[args]).style.display = "none";
}

function borrar() {
	for (var args = 0; args < borrar.arguments.length; args++)
		getElem(borrar.arguments[args]).value = "";
}

/*
 * Descripci�n: Controla la cantidad de car�cteres en un TextArea. Requisitos:
 * Se deber� de colocar la propiedad del TextArea StyleId="Nombre Del TextArea"
 * Par�metros: - text: Valor del StyleId. - Longuitud: Cantidad M�xima permitida
 * de car�cteres.
 */
/*
 * function caracteres(text, longuitud){ if (longuitud == null) longuitud = 40;
 * 
 * if (text.value.length > longuitud) { alert ("La longitud maxima del campo \"" +
 * text.id +"\" es de " + longuitud + " caracteres. "); text.value =
 * text.value.substring(0,longuitud); } }
 */

function formaFecha(nombre, e) {
	var key;
	if (e.which)
		key = e.which;
	else
		key = e.keyCode;

	if (key != 8) {
		if (key >= 48 && key <= 57) {
			var texto = getElem(nombre).value;
			var valor = getElem(nombre).value.length;
			if ((valor == 2 && texto.charAt(valor - 1) != '/')
					|| (valor == 5 && texto.charAt(valor - 2) != '/' && texto
							.charAt(valor - 1) != '/'))
				getElem(nombre).value = getElem(nombre).value + "/";
		} else {
			if (e.which)
				e.preventDefault(true);
			else
				e.returnValue = false;
		}
	}
}

function completaFecha(nombre) {
	var fecha = getElem(nombre);
	if (fecha.value != "") {
		var partes = fecha.value.split("/");
		if (partes[0] < 1 || partes[0] > 31) {
			alert("La Fecha est� mal escrita. Por favor, intente nuevamente.");
			fecha.focus();
			return;
		}

		if (partes[1] < 1 || partes[1] > 12) {
			alert("La Fecha est� mal escrita. Por favor, intente nuevamente.");
			fecha.focus();
			return;
		}

		if (partes[2].length == 2 && partes[2] >= 30 && partes[2] <= 99) {
			partes[2] = "19" + partes[2];
		} else if (partes[2].length == 2 && partes[2] >= 00 && partes[2] <= 29) {
			partes[2] = "20" + partes[2];
		}

		if (partes[2] < 1900 || partes[2] > 2050) {
			alert("La Fecha est� mal escrita. Por favor, intente nuevamente.");
			fecha.focus();
			return;
		}
		fecha.value = partes[0] + "/" + partes[1] + "/" + partes[2];
	}
}

function getElem(nombre) {
	if (document.getElementById) {
		return document.getElementById(nombre);
	} else if (document.all) {
		return document.all[nombre];
	}
}

function getChar(cod) {

	return String.fromCodeChar(cod);
}

function getCharToEvent(e) {

	if (e.which)
		return String.fromCharCode(e.which);
	else
		return String.fromCharCode(e.keyCode);
}

function getAsciiToEvent(e) {

	if (e.which)
		return e.which;
	else
		return e.keyCode;
}

function getAscii(car) {

	return String.charCode(car);
}

function Nuevo(control) {
	if (getElem(control).value == '') {
		return false;
	}
	getElem('do').value = 'newEntity';
	document.AbmForm.submit();
	return true;
}

function Proceso(control) {
	if (getElem(control).value == '') {
		alert("El LEGAJO est� vací­o, debe ingresar un n�mero de legajo v�lido o realizar una b�squeda. Por favor, intente nuevamente.");
		return false;
	}
	document.ProcessForm.submit();
	return true;
}

function openSearch(destino) {
	window.location.href = 'search/' + destino;
	window.location.reload();
}

var hijo;
function popUp(destino) {
	hijo = window
			.open(
					destino,
					"Titulo1",
					"titlebar=no,toolbar=no,dependent=yes,scrollbars=yes,status=yes,resizable=no,width=600,height=400");
	hijo.moveTo(screen.width * 0.5 - 300, screen.height * 0.5 - 200);
	hijo.opener = self;
}

function popUpBig(destino) {
	hijo = window
			.open(
					destino,
					"Titulo1",
					"titlebar=no,toolbar=no,dependent=yes,scrollbars=no,status=no,resize=no,width=700,height=500");
	hijo.moveTo(screen.width * 0.5 - 300, screen.height * 0.5 - 200);
	hijo.opener = self;
}

function confirmMsg(msg) {
	if (msg == '' || typeof (window.opera) != 'undefined') {
		return true;
	}
	return confirm(msg);
}

function confirmDelete() {
	return confirmMsg("�Est� seguro que desea eliminar el registro?");
}

function actualizarPopUp(control, valor) {
	if (window.opener) {
		// window.opener.getElem(control).value=valor;

		if (window.opener.document.getElementById) {
			// window.opener.document.getElementById( control ).value = valor;
			var e = window.opener.document.getElementById(control);
			if (e)
				e.value = valor;
		} else if (window.opener.document.all) {
			window.opener.document.all[control].value = valor;
		}
		window.close();
	}
}

var IFrameObj;
function callToServer(URL) {
	if (!document.createElement) {
		return true
	}
	;
	var IFrameDoc;
	if (!IFrameObj && document.createElement) {
		try {
			var tempIFrame = document.createElement('iframe');
			tempIFrame.setAttribute('id', 'RSIFrame');
			tempIFrame.style.border = '0px';
			tempIFrame.style.width = '0px';
			tempIFrame.style.height = '0px';
			IFrameObj = document.body.appendChild(tempIFrame);

			if (document.frames) {
				IFrameObj = document.frames['RSIFrame'];
			}
		} catch (exception) {
			iframeHTML = '<iframe id="RSIFrame" style="';
			iframeHTML += 'border:0px;';
			iframeHTML += 'width:0px;';
			iframeHTML += 'height:0px;';
			iframeHTML += '"><\/iframe>';
			document.body.innerHTML += iframeHTML;
			IFrameObj = new Object();
			IFrameObj.document = new Object();
			IFrameObj.document.location = new Object();
			IFrameObj.document.location.iframe = document
					.getElementById('RSIFrame');
			IFrameObj.document.location.replace = function(location) {
				this.iframe.src = location;
			}
		}
	}

	if (navigator.userAgent.indexOf('Gecko') != -1
			&& !IFrameObj.contentDocument) {
		setTimeout('callToServer()', 10);
		return false;
	}

	if (IFrameObj.contentDocument) {
		IFrameDoc = IFrameObj.contentDocument;
	} else if (IFrameObj.contentWindow) {
		IFrameDoc = IFrameObj.contentWindow.document;
	} else if (IFrameObj.document) {
		IFrameDoc = IFrameObj.document;
	} else {
		return true;
	}
	IFrameDoc.location.replace(URL);
	return false;
}

function addEvent(pControl, pEvent, pFunction) {
	var oCon = getElem(pControl);
	if (document.attachEvent) // IE
		oCon.attachEvent('on' + pEvent, pFunction);
	else
		oCon.addEventListener(pEvent, pFunction, false);
}

function removeEvent(pControl, pEvent, pFunction) {
	var oCon = getElem(pControl);
	if (document.detachEvent) // IE
		oCon.detachEvent('on' + pEvent, pFunction);
	else
		oCon.removeEventListener(pEvent, pFunction, false)
}

var _signoL = "-1";

var _puntoD = "-1";
var _signoD = "-1";

function validate(c, e, t) {
	var key = getAsciiToEvent(e);
	var n = getCharToEvent(e);

	if (t.toString() == '1') {
		// long value
		if ((isNaN(n) && key != 8) || key == 32) {
			if ((n.toString() != '-' || _signoL.toString() != "-1")
					|| (c.value.length > 0 && n.toString() == "-")
					|| n.toString() == '.') {
				if (e.which)
					e.preventDefault(true)
				else
					e.returnValue = false;
			}
		}
	} else {
		// decimal value
		if ((isNaN(n) && key != 8) || key == 32) {
			if ((n.toString() != '.' || _puntoD.toString() != "-1")
					&& (n.toString() != '-' || _signoD.toString() != "-1")
					&& (n.toString() != ',' || _puntoD.toString() != "-1")
					|| (c.value.length > 0 && n.toString() == "-")
					|| (isNaN(c.value) && n.toString() == "." || (c.value.length == 0 && n
							.toString() == "."))) {
				if (e.which)
					e.preventDefault(true)
				else
					e.returnValue = false;
			}

			if (n.toString() == ',') {
				if (e.which)
					e.preventDefault(true)
				else
					e.returnValue = false;

				if (c.value.indexOf(".") == -1)
					c.value += ".";
			}
		}
	}
}

function updateNumeros(c, t) {

	if (t.value == 1)
		_signoL = c.value.indexOf("-");
	else {
		_puntoD = c.value.indexOf(".");
		if (_puntoD.toString() == "-1")
			_puntoD = c.value.indexOf(",");

		_signoD = c.value.indexOf("-");
	}
}

var _numSaltoLinea = 0;

function caracteres(text, longuitud, e) {
	var num = 0;
	var oText = "";
	try {
		if (longuitud == null)
			longuitud = 40;

		if (typeof (text) != 'object')
			text = getElem(text);

		oText = text.value + getCharToEvent(e);

		while (oText != '' && oText.indexOf("\n") != '-1') {

			if (oText.indexOf("\n") != '-1') {
				num++;
				oText = oText.substring(oText.indexOf("\n") + 1, oText.length);
			}
		}

		if (num != 0 && num > _numSaltoLinea)
			_numSaltoLinea = num;

		if (getAsciiToEvent(e) == 13)
			_numSaltoLinea++;

		if ((text.value.length + parseInt(_numSaltoLinea)) >= longuitud) {
			if (e.which && e.which != 8)
				e.preventDefault(true);
			else
				e.returnValue = false;
		}

	} catch (error) {
		alert(error);
	}

}
/*
 * decodifica utf-8 desde java
 */

function decodeURL(decodedTxt) {
	var tempTxt = decodedTxt.replace(/\+/g, ' ');
	return decodeURIComponent(tempTxt);
}

// ************** Scripts para el popup de Alta en l�nea de Personas.-
function popUpBigBig(destino, id) {
	if (id == '' || id == undefined) {
		id = "Titulo2";
	}
	hijo = window
			.open(
					destino,
					id,
					"titlebar=no,toolbar=no,dependent=yes,scrollbars=yes,status=no,resize=yes,width=800,height=600");
	hijo.moveTo(screen.width * 0.5 - 400, screen.height * 0.5 - 300);
	hijo.opener = self;
}

/**
 * Mensaje generico (reemplazo de alert).-
 * @param string, string, string.-
 * @return popup
*/
function mensajeModal(title, text, type) {
    swal({
          title: title,
          text: text,
          type: type,
          showCloseButton: false,
          confirmButtonColor: '#AA272F',
          confirmButtonText: 'ACEPTAR'
        })
}

function mensajeModalHtml(title, html, type) {
    swal({
        title: title,
        html:html,
        type: type,
        showCloseButton: true,
        confirmButtonColor: '#AA272F',
        confirmButtonText: 'ACEPTAR'
      })
}

function confirmarModal(title, text, type, valor,funcion) {
	swal({
		  title: title,
		  text: text,
		  type: type,
		  showCancelButton: true,
		  confirmButtonColor: '#3eaf23',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'ACEPTAR',
		  cancelButtonText: 'CANCELAR'
		}).then(function () {
			funcion(valor);
		})
}

function confirmarModalAvanzado(title, text, type, valor, funcion1, funcion2, msjBtnSusses, msjBtnCancel) {
		swal({
		  title: title,
		  text: text,
		  type: type,
		  showCancelButton: true,
		  confirmButtonText: msjBtnSusses,
		  cancelButtonText: msjBtnCancel,
		  confirmButtonColor: '#3eaf23',
		  cancelButtonColor: '#d33',
		  reverseButtons: true
		}).then((result) =>{
			funcion1(valor);		
		}, function(dismiss) {			
		  if (dismiss === 'cancel') {
			funcion2(valor);
		  } else {
		    throw dismiss;
		  }
		})				
}

function armarListHtml(vector) {
	var resultado = "<ul class='cont-error' align='left'>";
	for(var i=0;i<vector.length;i++){
		resultado += "<li><p>"+vector[i]+"</p></li>";
	}	
	resultado += "<ul>";
	return resultado;
}
