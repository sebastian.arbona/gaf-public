function cuil_cuit(genero, documento) {
	/*
	 * Verifico que el documento tenga exactamente ocho números y que la cadena
	 * no contenga letras.
	 */
	var AB = '';
	var C = '';
	if ((documento.length != 8) || (isNaN(documento))) {
		// Muestro un error en caso de no serlo.
	} else {
		// Defino el valor del prefijo.
		if (genero == 'M') {
			AB = '20';
		} else if (genero == 'F') {
			AB = '27';
		} else {
			AB = '30';
		}
		/*
		 * Los números (excepto los dos primeros) que le tengo que multiplicar a
		 * la cadena formada por el prefijo y por el número de documento los
		 * tengo almacenados en un arreglo.
		 */
		var multiplicadores = new Array('3', '2', '7', '6', '5', '4', '3', '2');
		// Realizo las dos primeras multiplicaciones por separado.
		var calculo = ((parseInt(AB.charAt(0)) * 5) + (parseInt(AB.charAt(1)) * 4));
		/*
		 * Recorro el arreglo y el número de documento para realizar las
		 * multiplicaciones.
		 */
		for (var i = 0; i < 8; i++) {
			calculo += (parseInt(documento.charAt(i)) * parseInt(multiplicadores[i]));
		}
		// Calculo el resto.
		var resto = (parseInt(calculo)) % 11;
		var resultado = 11 - parseInt(resto);

		if (parseInt(resultado) == 11) {
			C = '0';
		} else if (parseInt(resultado) == 10) {
			switch (AB) {
			case '20':
				AB = '23';
				C = '9';
				break;
			case '27':
				AB = '23';
				C = '4';
				break;
			case '24':
				AB = '23';
				C = '3';
				break;
			case '30':
				AB = '33';
				C = '9';
				break;
			case '34':
				AB = '33';
				C = '3';
				break;
			}
		} else {
			C = resultado;
		}
		// Almaceno el CUIL o CUIT en una variable.
		var cuil_cuit = AB + documento + C;
		return cuil_cuit;
	}
}