/**
 * Get the contents of the URL via an Ajax call
 * url - to get content from (e.g. /struts-ajax/sampleajax.do?ask=COMMAND_NAME_1) 
 * nodeToOverWrite - when callback is made
 * nameOfFormToPost - which form values will be posted up to the server as part 
 *					of the request (can be null)
 */
 
 var fcName;
 
function retrieveURL(url, vfcName) {
    //Do the Ajax call
    fcName=vfcName;

	if (window.XMLHttpRequest) { // Non-IE browsers
		req = new XMLHttpRequest();
		req.onreadystatechange = processStateChange;
		req.timeout = 3600000;
		
		try {
			req.open("GET", url, true); //was get
		} catch (e) {
			alert("Error de comunicación con el servidor\n" + e);
		}
		req.send(null);
	} else if (window.ActiveXObject) { // IE
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			req.onreadystatechange = processStateChange;
			req.open("GET", url, true);
			req.send();
		}
	}
}

/**
 * Set as the callback method for when XmlHttpRequest State Changes 
 * used by retrieveUrl
 */
function processStateChange() {
	if (req.readyState == 4) { // Complete
		if (req.status == 200) { // OK response
			fcName(req.responseText);
      	} else {
			alert("Problemas con la respuesta del servidor: \n " + req.statusText);
		}
	}
}



