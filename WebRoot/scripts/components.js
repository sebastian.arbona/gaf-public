components = {
	SelectPersona: function(params){
		var defaults = {
			selectId: ".select-persona",
			url: pathSistema + "/actions/ajaxHelperAction.do?do=getBuscPersona",
			delay: 250,
			minimumInputLength: 4,
			size: 30
		};
		param = $j.extend(defaults, params);	
	
		$j(param.selectId).select2({		
			ajax : {
				url : param.url,
				dataType : 'json',
				delay : param.delay,
				data : function(params) {
					return {
						filtro : params.term,
						entidad : "Persona",
						campos : "idpersona,nomb_12",
						cantidad : param.size
						
					};
				},
				processResults : function(data, params) {
					return {						
						results : data						
					};
					
				},
				cache : true
			},
			escapeMarkup : function(markup) {
				return markup;
			},
			templateSelection: function(selection){

			},
			minimumInputLength : param.minimumInputLength,
			allowClear: false
		});	
	},
}