var isIE = false;

if (window.ActiveXObject)
    isIE = true;


function addHidden( pCell, pName, pValue, pText ){
    oInput = document.createElement( "input" );
    oInput.name = pName;
    oInput.id = pName;
    oInput.type = "hidden";
    oInput.value = pValue;
    pCell.appendChild( oInput );
    if ( pText || pText != "" )
        pCell.appendChild( document.createTextNode( pText ) );
}

function addButtons( pCell, pFuncModif, pFuncBorrar, pIndex ){
    if( pFuncModif!="" ){
        oInput = document.createElement( "img" );
        //if( !isIE ) oInput.type = "button";
        //oInput.style.width = "15px";
        oInput.src = "images/b_edit.png";
        oInput.onclick = pFuncModif;
        oInput.value = pIndex;
        //oInput.appendChild( document.createTextNode( "M" ) );
        pCell.appendChild( oInput );
    }
    
    oInput = document.createElement( "img" );
    //if( !isIE ) oInput.type = "button";
    //oInput.style.width = "15px";
    oInput.onclick = pFuncBorrar;
    oInput.value = pIndex;
    oInput.src = "images/deleteT.png";
    //oInput.appendChild( document.createTextNode( "X" ) );
    pCell.appendChild( oInput );
}
 
function setSelectOption( pControl, pValue ){
    for ( i = 0 ; i < pControl.options.length; i++ )
    {
        if ( pControl.options[i].value == pValue )
            pControl.selectedIndex = i;
    }
}

function borrarItem( evt  ){
        if ( !evt ) evt = window.event;
        var elem = (evt.target) ? evt.target : evt.srcElement;
        
        var respuesta=confirm('�Est� seguro de eliminar este �tem?');
        if(respuesta==true){
            elem.parentNode.parentNode.parentNode.removeChild(  elem.parentNode.parentNode );
        }
}

function verificarInsumo( pId ){
    var oTabla = getElem( "detalle" );
    
    for ( i = 0; i < oTabla.childNodes.length; i++)
    {
        oTr = oTabla.childNodes[i];
        for ( r = 0; r < oTr.childNodes.length; r++)
        {
            oTd = oTr.childNodes[r];
            for ( d = 0; d < oTd.childNodes.length; d++)
            {
                if ( oTd.childNodes[d].tagName == "INPUT"  )
                {
                    if ( oTd.childNodes[d].name.lastIndexOf( "idinsumo" ) > 0 )
                        if ( oTd.childNodes[d].value == pId )
                            return false;
                }
            }
        }
    }
    return true;
}