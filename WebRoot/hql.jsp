<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List"%>
<%@ page import="com.asf.security.BusinessPersistance"%>
<%@ page import="com.asf.security.SessionHandler"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/displaytag-12.tld" prefix="display"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
<head>
<html:base />

<title>Consultas HQL</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
</head>

<body>
	<form>
		query
		<TEXTAREA name="query" cols="50" rows="5">${param.query}</TEXTAREA>
		<input type="submit">
	</form>
	<%
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		out.println(BusinessPersistance.getSchema());
		out.println("<br>");
		out.println(BusinessPersistance.getConnectionUrl());
	if (request.getParameter("query") != null) {
		List l = bp.getByFilter(request.getParameter("query"));
		pageContext.setAttribute("l", l);
	%>
	<display:table name="l" scope="page" />
	<%
		}
	%>

</body>
</html:html>
