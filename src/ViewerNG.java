import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JRViewer;

public class ViewerNG extends JRViewer  implements ActionListener
{
   private static final long serialVersionUID = 1L;
   private static JasperPrint jrprint = null;
      
    public ViewerNG( JasperPrint jrp ) 
    {
        super( jrp );
        this.jrprint = jrp;
        
        this.initButton();
    }

    public void initButton()
    {
    	super.tlbToolBar.remove( super.btnSave );   
    }
    
        
    public void actionPerformed(ActionEvent e)
    {
         
    }
}