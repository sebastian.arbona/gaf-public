import java.awt.BorderLayout;
import java.net.URL;
import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.util.JRLoader;

public class AppletNG extends JApplet
{
   	private static final long serialVersionUID = 1L;
	private static JasperPrint jrprint = null;
    private String reporte = null;
    private JPanel panel = new JPanel();
    private JOptionPane dialog = null;
    public AppletNG() {
        
        panel.setLayout( new BorderLayout() );
        getContentPane().add( panel, BorderLayout.CENTER );
            
     }

    public void init(){
     try {
        this.reporte = this.getParameter("REPORT_URL").toString();         
                      
         if( this.reporte != null ){
            this.jrprint = ( JasperPrint ) JRLoader.loadObject( new URL( getCodeBase(), this.reporte ));
        
            if( getParameter( "PRINT" ).equalsIgnoreCase( "true" ) ){
                if( this.jrprint.getPages().size() > 0 )
                    JasperPrintManager.printReport( this.jrprint, true );
                else
                    dialog.showMessageDialog(this,"The document has no pages.");
            }else
                this.panel.add( new ViewerNG( this.jrprint ), BorderLayout.CENTER );                
            
         }
     } catch ( Exception ex ) {ex.printStackTrace();}
     
    }
    
   
    
}
