package com.civitas.migracion;

import java.io.Serializable;

public class BeanMigraGarantia implements Serializable {
	private static final long serialVersionUID = -7642383477678040444L;
	private String dominio;
	private String marca;
	private String version;
	private Long modelo;
	private Long preSegInfo;
	private Long porcentaje;

	public String getDominio() {
		return (dominio == null || dominio.isEmpty()) ? "" : dominio.trim();
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

	public String getMarca() {
		return (marca == null || marca.isEmpty()) ? "" : marca.trim();
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getVersion() {
		return (version == null || version.isEmpty()) ? "" : version.trim();
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Long getModelo() {
		return modelo;
	}

	public String getModeloStr() {
		return (modelo == null || modelo == 0) ? "" : modelo.toString().trim();
	}

	public void setModelo(Long modelo) {
		this.modelo = modelo;
	}

	public Long getPreSegInfo() {
		return preSegInfo;
	}
	public String getPreSegInfoStr() {
		return (preSegInfo == null || preSegInfo == 0) ? "" : preSegInfo.toString().trim();
	}

	public void setPreSegInfo(Long preSegInfo) {
		this.preSegInfo = preSegInfo;
	}

	public Long getPorcentaje() {
		return porcentaje;
	}
	public String getPorcentajeStr() {
		return (porcentaje == null || porcentaje == 0) ? "" : porcentaje.toString().trim();
	}


	public void setPorcentaje(Long porcentaje) {
		this.porcentaje = porcentaje;
	}

	/**
	 * si el dominio es invalido, la garantia es invalida
	 * 
	 * @return
	 */
	public boolean isValido() {
		boolean valido = true;
		valido &= !(this.dominio == null || this.dominio.isEmpty());
		return valido;
	}

	@Override
	public String toString() {
		return "BeanGarantia [dominio=" + dominio + ", marca=" + marca + ", version=" + version 
				+ ", modelo=" + modelo + ", preSegInfo=" + preSegInfo + ", porcentaje=" + porcentaje + "]";
	}

}
