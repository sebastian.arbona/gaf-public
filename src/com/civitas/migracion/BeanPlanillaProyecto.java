package com.civitas.migracion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.asf.util.DateHelper;
import com.asf.util.DoubleHelper;
import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.cred.beans.BeanDesembolso;
import com.civitas.cred.metodos.MetodoCalculo;
import com.civitas.cred.metodos.MetodoCalculoAleman;
import com.civitas.cred.metodos.MetodoCalculoDirecto;

public class BeanPlanillaProyecto implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;
	private Long numero;
	private String metodoAmortizacion;
	private Long objetoiId;
	private Long titularId;

	private String nombreTitular; // b2
	private String apellidoTitular; // b2
	private Long cuitTitular; // c3
	private Integer cuotasTotales;
	private BigDecimal importePagado;
	private Integer periodicidadCuotas;
	private BigDecimal valorCuota;
	private BigDecimal tasaCompensatorio; // j5
	private BigDecimal tasaMoratorio; // j6
	private BigDecimal tasaPunitorio; // j7
	private Date firmaContrato; // f11

	private String estadoInicial;

	private ArrayList<BeanMigraContacto> contactoList;
	private ArrayList<BeanMigraDesembolso> desembolsoList;
	private ArrayList<BeanMigraCuota> cuotaList;
	private ArrayList<BeanMigraPago> pagoList;
	private ArrayList<BeanMigraNotificacion> notificacionList;
	private ArrayList<BeanMigraDomicilio> domicilioList;
	private ArrayList<BeanMigraGarantia> garantiaList;

	public BeanPlanillaProyecto() {
		this.contactoList = new ArrayList<BeanMigraContacto>();
		this.desembolsoList = new ArrayList<BeanMigraDesembolso>();
		this.cuotaList = new ArrayList<BeanMigraCuota>();
		this.pagoList = new ArrayList<BeanMigraPago>();
		this.notificacionList = new ArrayList<BeanMigraNotificacion>();
		this.domicilioList = new ArrayList<BeanMigraDomicilio>();
		this.garantiaList = new ArrayList<BeanMigraGarantia>();

	}

	public BeanPlanillaProyecto(String metodoAmortizacion, String estadoInicial) {
		this();
		this.metodoAmortizacion = metodoAmortizacion;
		this.estadoInicial = estadoInicial;
	}

	public BeanPlanillaProyecto(Long idPersona, Date fechaDesembolso, BigDecimal importeDesembolso,
			Integer cuotasTotales, BigDecimal importePagado, Integer periodicidadCuotas, BigDecimal tasaCompensatorio,
			String metodoAmortizacion, String estadoInicial) {
		this(metodoAmortizacion, estadoInicial);
		this.titularId = idPersona;
		this.firmaContrato = fechaDesembolso;
		this.tasaCompensatorio = tasaCompensatorio;
		this.cuotasTotales = cuotasTotales;
		this.importePagado = importePagado;
		this.periodicidadCuotas = periodicidadCuotas;
		BeanMigraDesembolso beanDesembolso = new BeanMigraDesembolso();
		beanDesembolso.setNumero(1);
		beanDesembolso.setOrden(1);
		beanDesembolso.setFecha(fechaDesembolso);
		beanDesembolso.setImporte(importeDesembolso);
		this.desembolsoList.add(beanDesembolso);
	}

	public void setTasaCompensatorioStr(String tasaCompensatorio) {
		this.tasaCompensatorio = new BigDecimal(
				tasaCompensatorio.replace("%", "").replace(".", "").replace(",", ".").trim()).setScale(2,
						RoundingMode.HALF_EVEN);
	}

	public void setTasaMoratorioStr(String tasaMoratorio) {
		this.tasaMoratorio = new BigDecimal(tasaMoratorio.replace("%", "").replace(".", "").replace(",", ".").trim())
				.setScale(2, RoundingMode.HALF_EVEN);
	}

	public void setTasaPunitorioStr(String tasaPunitorio) {
		this.tasaPunitorio = new BigDecimal(tasaPunitorio.replace("%", "").replace(".", "").replace(",", ".").trim())
				.setScale(2, RoundingMode.HALF_EVEN);
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = (nombreTitular == null) ? "" : nombreTitular.trim();
	}

	public String getApellidoTitular() {
		return apellidoTitular;
	}

	public void setApellidoTitular(String apellidoTitular) {
		this.apellidoTitular = (apellidoTitular == null) ? "" : apellidoTitular.trim();
	}

	public String getNombreCompleto() {
		String nombreCompleto = apellidoTitular.trim() + " " + nombreTitular.trim();
		return nombreCompleto.trim();
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getMetodoAmortizacion() {
		return metodoAmortizacion;
	}

	public void setMetodoAmortizacion(String metodoAmortizacion) {
		this.metodoAmortizacion = metodoAmortizacion;
	}

	public Long getObjetoiId() {
		return objetoiId;
	}

	public void setObjetoiId(Long objetoiId) {
		this.objetoiId = objetoiId;
	}

	public Long getTitularId() {
		return titularId;
	}

	public void setTitularId(Long titularId) {
		this.titularId = titularId;
	}

	public Long getCuitTitular() {
		return cuitTitular;
	}

	public void setCuitTitular(Long cuitTitular) {
		this.cuitTitular = cuitTitular;
	}

	public Integer getCuotasTotales() {
		return cuotasTotales;
	}

	public void setCuotasTotales(Integer cuotasTotales) {
		this.cuotasTotales = cuotasTotales;
	}

	public BigDecimal getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}

	public Integer getPeriodicidadCuotas() {
		return periodicidadCuotas;
	}

	public void setPeriodicidadCuotas(Integer periodicidadCuotas) {
		this.periodicidadCuotas = periodicidadCuotas;
	}

	public BigDecimal getValorCuota() {
		return valorCuota;
	}

	public void setValorCuota(BigDecimal valorCuota) {
		this.valorCuota = valorCuota;
	}

	public BigDecimal getTasaCompensatorio() {
		return tasaCompensatorio;
	}

	public void setTasaCompensatorio(BigDecimal tasaCompensatorio) {
		this.tasaCompensatorio = tasaCompensatorio;
	}

	public BigDecimal getTasaMoratorio() {
		return tasaMoratorio;
	}

	public void setTasaMoratorio(BigDecimal tasaMoratorio) {
		this.tasaMoratorio = tasaMoratorio;
	}

	public BigDecimal getTasaPunitorio() {
		return tasaPunitorio;
	}

	public void setTasaPunitorio(BigDecimal tasaPunitorio) {
		this.tasaPunitorio = tasaPunitorio;
	}

	public Date getFirmaContrato() {
		return firmaContrato;
	}

	public void setFirmaContrato(Date firmaContrato) {
		this.firmaContrato = firmaContrato;
	}

	public String getEstadoInicial() {
		return estadoInicial;
	}

	public void setEstadoInicial(String estadoInicial) {
		this.estadoInicial = estadoInicial;
	}

	public ArrayList<BeanMigraContacto> getContactoList() {
		return contactoList;
	}

	public void setContactoList(ArrayList<BeanMigraContacto> contactoList) {
		this.contactoList = contactoList;
	}

	public ArrayList<BeanMigraDesembolso> getDesembolsoList() {
		return desembolsoList;
	}

	public void setDesembolsoList(ArrayList<BeanMigraDesembolso> desembolsoList) {
		this.desembolsoList = desembolsoList;
	}

	public ArrayList<BeanMigraCuota> getCuotaList() {
		return cuotaList;
	}

	public void setCuotaList(ArrayList<BeanMigraCuota> cuotaList) {
		this.cuotaList = cuotaList;
	}

	public ArrayList<BeanMigraPago> getPagoList() {
		return pagoList;
	}

	public void setPagoList(ArrayList<BeanMigraPago> pagoList) {
		this.pagoList = pagoList;
	}

	public ArrayList<BeanMigraNotificacion> getNotificacionList() {
		return notificacionList;
	}

	public void setNotificacionList(ArrayList<BeanMigraNotificacion> notificacionList) {
		this.notificacionList = notificacionList;
	}

	public ArrayList<BeanMigraDomicilio> getDomicilioList() {
		return domicilioList;
	}

	public void setDomicilioList(ArrayList<BeanMigraDomicilio> domicilioList) {
		this.domicilioList = domicilioList;
	}

	public ArrayList<BeanMigraGarantia> getGarantiaList() {
		return garantiaList;
	}

	public void setGarantiaList(ArrayList<BeanMigraGarantia> garantiaList) {
		this.garantiaList = garantiaList;
	}

	public boolean ejecutar() {
		boolean status = true;
		Calendar calendario = Calendar.getInstance();
		ArrayList<BeanDesembolso> desembolsos = new ArrayList<BeanDesembolso>();
		for (BeanMigraDesembolso beanDesembolso : desembolsoList) {
			BeanDesembolso desembolso = new BeanDesembolso();
			desembolso.setFecha(beanDesembolso.getFecha());
			desembolso.setImporte(beanDesembolso.getImporte().doubleValue());
			desembolso.setNumero(beanDesembolso.getNumero());
			desembolsos.add(desembolso);
			if (beanDesembolso.getNumero() == 1) {
				calendario.setTime(beanDesembolso.getFecha());
			}
		}
		calendario.add(Calendar.MONTH, 1);
		Date primerVencimiento = calendario.getTime();
		ArrayList<Date> feriados = new ArrayList<Date>();
		primerVencimiento = DateHelper.getDiaHabil(feriados, primerVencimiento);

		double[] bonificaciones = new double[cuotasTotales];
		BeanCalculo beanCalculo = new BeanCalculo(primerVencimiento, primerVencimiento,
				DoubleHelper.redondear(tasaCompensatorio.doubleValue() * 100d), bonificaciones, cuotasTotales,
				cuotasTotales, periodicidadCuotas, periodicidadCuotas, desembolsos);
		beanCalculo.setIvaAlicuota(0d);
		MetodoCalculo metodo = null;
		if (metodoAmortizacion.equalsIgnoreCase("A")) {
			metodo = new MetodoCalculoAleman();
		} else if (metodoAmortizacion.equalsIgnoreCase("F")) {
			// MetodoCalculoFrances metodo = new MetodoCalculoFrances();
			// this.cuotas = metodo.amortizar(this.getDesembolsos(), this.tasaAmortizacion,
			// this.plazoAmortizacion, this.prestamo, this.periodicidadCuotas,
			// this.plazoGracia);
		} else if (metodoAmortizacion.equalsIgnoreCase("D")) {
			metodo = new MetodoCalculoDirecto();
		}
		if (metodo == null) {
			status = false;
		} else {
			// limpio cuotas
			cuotaList.clear();
			// amortiza
			for (BeanCuota cuota : metodo.amortizar(beanCalculo)) {
				BeanMigraCuota beanCuota = new BeanMigraCuota();
				beanCuota.setCapital(new BigDecimal(cuota.getCapital()));
				beanCuota.setCompensatorio(new BigDecimal(cuota.getCompensatorio()));
				beanCuota.setFechaVencimiento(cuota.getFechaVencimiento());
				beanCuota.setGasto(false);
				beanCuota.setGastos(BigDecimal.ZERO);
				beanCuota.setNumero(cuota.getNumero());
				cuotaList.add(beanCuota);
			}
			BigDecimal saldoAplicable = importePagado;
			// limpio pagos
			pagoList.clear();
			for (BeanMigraCuota beanCuota : cuotaList) {
				BeanMigraPago beanPago = new BeanMigraPago();
				beanPago.setNumero(beanCuota.getNumero());
				beanPago.setGasto(beanCuota.isGasto());
				if (saldoAplicable.compareTo(BigDecimal.ZERO) > 0) {
					beanPago.setCompensatorio(saldoAplicable.min(beanCuota.getCompensatorio()));
					saldoAplicable = saldoAplicable.add(beanPago.getCompensatorio().negate());
				}
				if (saldoAplicable.compareTo(BigDecimal.ZERO) > 0) {
					beanPago.setCapital(saldoAplicable.min(beanCuota.getCapital()));
					saldoAplicable = saldoAplicable.add(beanPago.getCapital().negate());
				}
				beanPago.setIvaCompensatorio(BigDecimal.ZERO);
				beanPago.setMoratorio(BigDecimal.ZERO);
				beanPago.setIvaMoratorio(BigDecimal.ZERO);
				beanPago.setPunitorio(BigDecimal.ZERO);
				beanPago.setIvaPunitorio(BigDecimal.ZERO);
				beanPago.setGastoRecuperar(BigDecimal.ZERO);
				beanPago.setIvaPagado(BigDecimal.ZERO);
				beanPago.setImportePagado(
						beanPago.getCapital().add(beanPago.getCompensatorio()).setScale(2, RoundingMode.HALF_EVEN));
				beanPago.setFechaPago(beanCuota.getFechaVencimiento());
				if (beanPago.isValido()) {
					pagoList.add(beanPago);
				} else { // no agrego el pago
				}
				if (saldoAplicable.compareTo(BigDecimal.ZERO) <= 0) {
					break;
				}
			}
		}
		return status;
	}

	@Override
	public String toString() {
		return "BeanPlanillaProyecto [objetoiId=" + objetoiId + ", titularId=" + titularId + ", nombreTitular="
				+ nombreTitular + ", cuitTitular=" + cuitTitular + "]";
	}

}
