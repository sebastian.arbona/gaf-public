package com.civitas.migracion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

import com.civitas.importacion.excel.XLSCellBean;

public class BeanMigraDesembolso implements Serializable, Comparable<BeanMigraDesembolso> {
	private static final long serialVersionUID = -1779405245616306547L;
	private Integer numero;
	private Date fecha; // f12
	private BigDecimal importe; // c12

	private Integer orden;

	public BeanMigraDesembolso() {
	}

	public BeanMigraDesembolso(ArrayList<XLSCellBean> celdas) throws IllegalArgumentException {
		for (XLSCellBean celda : celdas) {
			switch (celda.getNumeroColumna()) {
			case 0:
				try {
					String valor = (String) celda.getValor();
					valor = valor.replaceAll("\\D+", "");
					this.numero = new Integer(valor);
				} catch (Exception e) {
					throw new IllegalArgumentException("el numero del desembolso no se puede determinar");
				}
				break;
			case 1:
				try {
					Double valor = (Double) celda.getValor();
					this.importe = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					throw new IllegalArgumentException("el monto del desembolso no se puede determinar");
				}
				break;
			case 4:
				try {
					Date valor = (Date) celda.getValor();
					this.fecha = valor;
				} catch (Exception e) {
					throw new IllegalArgumentException("la fecha de desembolso no se puede determinar");
				}
				break;
			default:
				break;
			}
		}
		if (!this.isValido()) {
			throw new IllegalArgumentException("los datos del desembolso no estan completos");
		}
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public boolean isValido() {
		boolean valido = true;
		valido &= !(this.numero == null || this.numero == 0);
		valido &= !(this.importe == null || this.importe.compareTo(BigDecimal.ZERO) <= 0);
		valido &= !(this.fecha == null);
		return valido;
	}

	@Override
	public String toString() {
		return "BeanDesembolso [numero=" + numero + ", importe=" + importe + ", fecha=" + fecha + ", orden=" + orden
				+ "]";
	}

	@Override
	public int compareTo(BeanMigraDesembolso o) {
		return this.fecha.compareTo(o.getFecha());
	}

}
