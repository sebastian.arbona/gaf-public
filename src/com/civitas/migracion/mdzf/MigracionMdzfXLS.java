package com.civitas.migracion.mdzf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.asf.util.DoubleHelper;
import com.civitas.importacion.excel.XLSCellBean;
import com.civitas.importacion.excel.XLSRowBean;
import com.civitas.importacion.excel.XLSSheetBean;
import com.civitas.importacion.excel.XLSWorkbookBean;
import com.civitas.migracion.BeanMigraContacto;
import com.civitas.migracion.BeanMigraCuota;
import com.civitas.migracion.BeanMigraDesembolso;
import com.civitas.migracion.BeanMigraNotificacion;
import com.civitas.migracion.BeanMigraPago;
import com.civitas.migracion.BeanPlanillaProyecto;
import com.civitas.migracion.MigracionXLSX;

public class MigracionMdzfXLS extends MigracionXLSX {

	public MigracionMdzfXLS(XLSWorkbookBean libro) {
		super(libro);
	}

	@SuppressWarnings("unchecked")
	@Override
	public BeanLibroMDZF importar() {
		ArrayList<XLSSheetBean> hojas = libro.getHojas();
		BeanLibroMDZF libro = new BeanLibroMDZF();
		ArrayList<BeanPlanillaDomicilio> domicilioList = new ArrayList<BeanPlanillaDomicilio>();
		ArrayList<BeanPlanillaMora> moraList = new ArrayList<BeanPlanillaMora>();
		ArrayList<BeanPlanillaProyecto> proyectoList = new ArrayList<BeanPlanillaProyecto>();
		String nombreHoja = "";
		for (XLSSheetBean hoja : hojas) {
			nombreHoja = "Hoja: " + hoja.getNumeroHoja() + " " + hoja.getNombreHoja();
			ArrayList<XLSRowBean> filas = hoja.getFilas();
			if (hoja.getNombreHoja().toLowerCase().contains("domicilio")) {
				BeanPlanillaDomicilio beanPlanillaDomicilio;
				for (XLSRowBean fila : filas) {
					if (fila.getNumeroFila() >= 1) {
						ArrayList<XLSCellBean> celdas = fila.getCeldas();
						System.out.println("Procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
						try {
							beanPlanillaDomicilio = new BeanPlanillaDomicilio(celdas);
							domicilioList.add(beanPlanillaDomicilio);
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			} else if (hoja.getNombreHoja().toLowerCase().contains("mora")) {
				BeanPlanillaMora beanPlanillaMora = new BeanPlanillaMora();
				for (XLSRowBean fila : filas) {
					if (fila.getNumeroFila() >= 3) {
						ArrayList<XLSCellBean> celdas = fila.getCeldas();
						System.out.println("Procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
						try {
							beanPlanillaMora = new BeanPlanillaMora(celdas);
							moraList.add(beanPlanillaMora);
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			} else if (hoja.getNombreHoja().toLowerCase().contains("flujo")) {
				// de esta hoja no se extrae nada, agrego condicion para que no intente
				// procesarla como proyecto
			} else {
				try {
					BeanPlanillaProyecto beanPlanillaProyecto = new BeanPlanillaProyecto("A", "EJECUCION");
					filas: for (XLSRowBean fila : filas) {
						System.out.println("Procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
						try {
							ArrayList<XLSCellBean> celdas = fila.getCeldas();
							if (fila.getNumeroFila() == 0) {
								for (XLSCellBean celda : celdas) {
									if (celda.getNumeroColumna() == 0) {
										beanPlanillaProyecto.setNombreTitular((String) celda.getValor());
									}
								}
							} else {
								// el indice de cada item en la lista de celdas no es equivalente al numero de
								// columna
								int columnas = 0;
								for (XLSCellBean celda : celdas) {
									columnas = celda.getNumeroColumna();
								}
								columnas++;
								String[] identificador = new String[columnas];
								Object[] valor = new Object[columnas];
								for (XLSCellBean celda : celdas) {
									Object temporal = celda.getValor();
									identificador[celda.getNumeroColumna()] = (temporal == null) ? ""
											: temporal.toString().toLowerCase().trim();
									valor[celda.getNumeroColumna()] = temporal;
								}
								if (identificador[0] != null && !identificador[0].isEmpty()) {
									/**
									 * acordado con usuario el 28/03/2019
									 */
									if (identificador[0].equalsIgnoreCase("info adicional")) {
										break filas;
									}
									if (identificador[0].contains("cuit")) {
										// el valor del cuit puede estar incluido con el label
										String valorString = "";
										Double valorDouble = 0d;
										if (valor[1] instanceof String) {
											valorString = (String) valor[1];
										} else if (valor[1] instanceof Double) {
											valorDouble = (Double) valor[1];
											valorString = new Long(valorDouble.longValue()).toString();
										}
										Long cuitTitular = new Long(
												identificador[0].concat(valorString).replaceAll("\\D+", ""));
										beanPlanillaProyecto.setCuitTitular(cuitTitular);
									} else if (identificador[0].contains("tel") || identificador[0].contains("mail")) {
										// medios de contacto
										// telefonos y mails puede estar incluidos con el label
										ArrayList<BeanMigraContacto> contactoList = beanPlanillaProyecto
												.getContactoList();
										try {
											if (contactoList == null) {
												contactoList = new ArrayList<BeanMigraContacto>();
											}
											contactoList.addAll(BeanMigraContacto.extraerContactos(celdas));
											beanPlanillaProyecto.setContactoList(contactoList);
										} catch (IllegalArgumentException e) {
											System.out.println(e.getMessage());
										}
									} else if (identificador[0].contains("firma contrato")) {
										try {
											beanPlanillaProyecto.setFirmaContrato((Date) valor[4]);
										} catch (Exception e) {
											beanPlanillaProyecto.setFirmaContrato(null);
										}
									} else if (identificador[0].contains("desembolso")) {
										// desembolsos
										ArrayList<BeanMigraDesembolso> desembolsoList = beanPlanillaProyecto
												.getDesembolsoList();
										try {
											if (desembolsoList == null) {
												desembolsoList = new ArrayList<BeanMigraDesembolso>();
											}
											desembolsoList.add(new BeanMigraDesembolso(celdas));
											beanPlanillaProyecto.setDesembolsoList(desembolsoList);
										} catch (IllegalArgumentException e) {
											System.out.println(e.getMessage());
										}
									} else if (identificador[0].contains("gracia")) {
										// fecha fin periodo gracia, no se hace nada
									} else if (identificador[0].contains("cuota")) {
										// cuotas, para determinar cantidad, periodicidad, primeras cuotas
										// captura tambien los gastos a recuperar en cuota 1
										ArrayList<BeanMigraCuota> cuotaList = beanPlanillaProyecto.getCuotaList();
										try {
											if (cuotaList == null) {
												cuotaList = new ArrayList<BeanMigraCuota>();
											}
											cuotaList.add(new BeanMigraCuota(celdas));
											beanPlanillaProyecto.setCuotaList(cuotaList);
										} catch (IllegalArgumentException e) {
											// los gastos a recuperar deben salir por aca siempre ya que no tienen fecha
											// de vencimiento propio ni capital ni compensatorio
											System.out.println(e.getMessage());
										}
										// pagos, para generar deb/cre en la cuenta corriente
										// en el caso de gastos a recuperar podria ser solo el debito ya que debe quedar
										// impactado en ctacte
										ArrayList<BeanMigraPago> pagoList = beanPlanillaProyecto.getPagoList();
										try {
											if (pagoList == null) {
												pagoList = new ArrayList<BeanMigraPago>();
											}
											pagoList.add(new BeanMigraPago(celdas));
											beanPlanillaProyecto.setPagoList(pagoList);
										} catch (IllegalArgumentException e) {
											System.out.println(e.getMessage());
										}
									}
								} else if (identificador.length >= 2 && identificador[0] == null
										&& identificador[1] != null && !identificador[1].trim().isEmpty()) {
									// notificaciones
									ArrayList<BeanMigraNotificacion> notificacionList = beanPlanillaProyecto
											.getNotificacionList();
									try {
										if (notificacionList == null) {
											notificacionList = new ArrayList<BeanMigraNotificacion>();
										}
										notificacionList.add(new BeanMigraNotificacion(celdas));
										beanPlanillaProyecto.setNotificacionList(notificacionList);
									} catch (IllegalArgumentException e) {
										System.out.println(e.getMessage());
									}
								}
								if (identificador.length >= 6 && valor.length >= 8 && identificador[6] != null
										&& !identificador[6].isEmpty()) {
									if (identificador[6].contains("tna")) {
										// linea 4, columna 9, tasa compensatorio
										beanPlanillaProyecto.setTasaCompensatorio(
												new BigDecimal(DoubleHelper.redondear((Double) valor[8])).setScale(2,
														RoundingMode.HALF_EVEN));
									} else if (identificador[6].contains("morat")) {
										// linea 5, columna 9, tasa moratorio
										beanPlanillaProyecto.setTasaMoratorio(
												new BigDecimal(DoubleHelper.redondear((Double) valor[8])).setScale(2,
														RoundingMode.HALF_EVEN));
									} else if (identificador[6].contains("punit")) {
										// linea 4, columna 9, tasa punitorio
										beanPlanillaProyecto.setTasaPunitorio(
												new BigDecimal(DoubleHelper.redondear((Double) valor[8])).setScale(2,
														RoundingMode.HALF_EVEN));
									}
								}
							}
						} catch (Exception e) {
							System.out.println(
									"Error procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
							e.printStackTrace();
						}
					}
					// ordena desembolsos por fecha
					ArrayList<BeanMigraDesembolso> desembolsoList = beanPlanillaProyecto.getDesembolsoList();
					if (desembolsoList != null && !desembolsoList.isEmpty()) {
						Collections.sort(desembolsoList);
						int orden = 0;
						for (BeanMigraDesembolso beanDesembolso : desembolsoList) {
							beanDesembolso.setOrden(++orden);
						}
						beanPlanillaProyecto.setDesembolsoList(desembolsoList);
					}
					// ordena cuotas por vencimiento
					ArrayList<BeanMigraCuota> cuotaList = beanPlanillaProyecto.getCuotaList();
					if (cuotaList != null && !cuotaList.isEmpty()) {
						// Collections.sort(cuotaList);
						beanPlanillaProyecto.setCuotaList(cuotaList);
					}
					// ordena pagos por fecha de pago
					ArrayList<BeanMigraPago> pagoList = beanPlanillaProyecto.getPagoList();
					if (pagoList != null && !pagoList.isEmpty()) {
						// Collections.sort(pagoList);
						beanPlanillaProyecto.setPagoList(pagoList);
					}
					proyectoList.add(beanPlanillaProyecto);
				} catch (Exception e) {
					System.out.println("Error procesando hoja: " + nombreHoja);
					e.printStackTrace();
				}
			}
		}
		libro.setDomicilioList(domicilioList);
		libro.setMoraList(moraList);
		libro.setProyectoList(proyectoList);
		return libro;
	}

}
