package com.civitas.migracion.mdzf;

import java.io.Serializable;
import java.util.ArrayList;

import com.civitas.migracion.BeanPlanillaProyecto;

public class BeanLibroMDZF implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;
	private ArrayList<BeanPlanillaDomicilio> domicilioList;
	private ArrayList<BeanPlanillaMora> moraList;
	private ArrayList<BeanPlanillaProyecto> proyectoList;

	public ArrayList<BeanPlanillaDomicilio> getDomicilioList() {
		return domicilioList;
	}

	public void setDomicilioList(ArrayList<BeanPlanillaDomicilio> domicilioList) {
		this.domicilioList = domicilioList;
	}

	public ArrayList<BeanPlanillaMora> getMoraList() {
		return moraList;
	}

	public void setMoraList(ArrayList<BeanPlanillaMora> moraList) {
		this.moraList = moraList;
	}

	public ArrayList<BeanPlanillaProyecto> getProyectoList() {
		return proyectoList;
	}

	public void setProyectoList(ArrayList<BeanPlanillaProyecto> proyectoList) {
		this.proyectoList = proyectoList;
	}

}
