package com.civitas.migracion.mdzf;

import java.io.Serializable;
import java.util.ArrayList;

import com.civitas.importacion.excel.XLSCellBean;
import com.civitas.migracion.BeanMigraDomicilio;

public class BeanPlanillaDomicilio implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;
	private String deudor;
	private Long deudorCuit;
	private Long deudorDocumento;

	private String cotomador;
	private String cotomadorSexo;
	private Long cotomadorDocumento;

	private String fiador;
	private String fiadorSexo;
	private Long fiadorDocumento;

	private BeanMigraDomicilio domicilio;

	/**
	 * Este constructor permite importar datos desde una planilla XLSX, donde a cada
	 * fila es un array de celdas a procesar
	 * 
	 * @param celdas
	 * @throws IllegalArgumentException
	 */
	public BeanPlanillaDomicilio(ArrayList<XLSCellBean> celdas) throws IllegalArgumentException {
		String calle = "", departamento = "", provincia = "";
		for (XLSCellBean celda : celdas) {
			switch (celda.getNumeroColumna()) {
			case 0:
				this.deudor = (String) celda.getValor();
				break;
			case 1:
				this.deudorCuit = ((Double) celda.getValor()).longValue();
				break;
			case 4:
				this.cotomador = celda.getValor() == null ? null : ((String) celda.getValor());
				break;
			case 5:
				this.cotomadorSexo = celda.getValor() == null ? null : ((String) celda.getValor());
				break;
			case 6:
				this.cotomadorDocumento = celda.getValor() == null ? null : ((Double) celda.getValor()).longValue();
				break;
			case 7:
				this.fiador = celda.getValor() == null ? null : ((String) celda.getValor());
				break;
			case 8:
				this.fiadorSexo = celda.getValor() == null ? null : ((String) celda.getValor());
				break;
			case 9:
				this.fiadorDocumento = celda.getValor() == null ? null : ((Double) celda.getValor()).longValue();
				break;
			case 10:
				calle = (String) celda.getValor();
				break;
			case 11:
				departamento = (String) celda.getValor();
				break;
			case 12:
				provincia = (String) celda.getValor();
				break;
			default:
				break;
			}
		}
		this.domicilio = new BeanMigraDomicilio(calle, "", departamento, provincia);
	}

	/**
	 * Este constructor permite importar datos desde una planilla CSV, donde a cada
	 * linea se le realiza SPLIT con el separador con lo que se genera el vector que
	 * recibo como parametro
	 * 
	 * @param valores
	 * @throws IllegalArgumentException
	 */
	public BeanPlanillaDomicilio(String[] valores) throws IllegalArgumentException {
		// TODO parsear los valores que vienen en el vector
	}

	public String getDeudor() {
		return deudor;
	}

	public void setDeudor(String deudor) {
		this.deudor = deudor;
	}

	public Long getDeudorCuit() {
		return deudorCuit;
	}

	public void setDeudorCuit(Long deudorCuit) {
		this.deudorCuit = deudorCuit;
	}

	public Long getDeudorDocumento() {
		return deudorDocumento;
	}

	public void setDeudorDocumento(Long deudorDocumento) {
		this.deudorDocumento = deudorDocumento;
	}

	public String getCotomador() {
		return cotomador;
	}

	public void setCotomador(String cotomador) {
		this.cotomador = cotomador;
	}

	public String getCotomadorSexo() {
		return cotomadorSexo;
	}

	public void setCotomadorSexo(String cotomadorSexo) {
		this.cotomadorSexo = cotomadorSexo;
	}

	public Long getCotomadorDocumento() {
		return cotomadorDocumento;
	}

	public void setCotomadorDocumento(Long cotomadorDocumento) {
		this.cotomadorDocumento = cotomadorDocumento;
	}

	public String getFiador() {
		return fiador;
	}

	public void setFiador(String fiador) {
		this.fiador = fiador;
	}

	public String getFiadorSexo() {
		return fiadorSexo;
	}

	public void setFiadorSexo(String fiadorSexo) {
		this.fiadorSexo = fiadorSexo;
	}

	public Long getFiadorDocumento() {
		return fiadorDocumento;
	}

	public void setFiadorDocumento(Long fiadorDocumento) {
		this.fiadorDocumento = fiadorDocumento;
	}

	public BeanMigraDomicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(BeanMigraDomicilio domicilio) {
		this.domicilio = domicilio;
	}

	@Override
	public String toString() {
		return "BeanPlanillaDomicilio [deudor=" + deudor + ", deudorCuit=" + deudorCuit + ", deudorDocumento="
				+ deudorDocumento + "]";
	}

}
