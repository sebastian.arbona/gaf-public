package com.civitas.migracion.mdzf;

import java.io.Serializable;
import java.util.ArrayList;

import com.civitas.importacion.excel.XLSCellBean;

public class BeanPlanillaMora implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;
	private Long titularCuit;
	private String nombreLinea;

	public BeanPlanillaMora() {
	}

	/**
	 * Este constructor permite importar datos desde una planilla XLSX, donde a cada
	 * fila es un array de celdas a procesar
	 * 
	 * @param celdas
	 * @throws IllegalArgumentException
	 */
	public BeanPlanillaMora(ArrayList<XLSCellBean> celdas) throws IllegalArgumentException {
		for (XLSCellBean celda : celdas) {
			switch (celda.getNumeroColumna()) {
			case 2:
				try {
					this.titularCuit = ((Double) celda.getValor()).longValue();
				} catch (Exception e) {
					throw new IllegalArgumentException("el numero de cuit no se puede determinar");
				}
				break;
			case 5:
				try {
					this.nombreLinea = (String) celda.getValor();
				} catch (Exception e) {
					throw new IllegalArgumentException("el nombre de la linea no se puede determinar");
				}
				break;
			default:
				break;
			}
		}
		if (!this.isValido()) {
			throw new IllegalArgumentException("no se pueden recuperar los datos del titular y la linea");
		}

	}

	public Long getTitularCuit() {
		return titularCuit;
	}

	public void setTitularCuit(Long titularCuit) {
		this.titularCuit = titularCuit;
	}

	public String getNombreLinea() {
		return nombreLinea;
	}

	public void setNombreLinea(String nombreLinea) {
		this.nombreLinea = nombreLinea;
	}

	private boolean isValido() {
		boolean valido = true;
		if (this.titularCuit == null || this.nombreLinea == null) {
			valido = false;
		}
		return valido;
	}

	@Override
	public String toString() {
		return "BeanPlanillaMora [titularCuit=" + titularCuit + ", nombreLinea=" + nombreLinea + "]";
	}

}
