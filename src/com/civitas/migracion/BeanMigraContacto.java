package com.civitas.migracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.civitas.importacion.excel.XLSCellBean;

public class BeanMigraContacto implements Serializable {
	private static final long serialVersionUID = -9161698341383328043L;
	private String medio; // mail,telefono
	private String tipo; // particular,laboral
	private String detalle;

	public BeanMigraContacto() {

	}

	public BeanMigraContacto(String medio, String tipo, String detalle) {
		this.medio = medio;
		this.tipo = tipo;
		this.detalle = detalle;
	}

	public String getMedio() {
		return medio;
	}

	public void setMedio(String medio) {
		this.medio = medio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	/**
	 * Analiza las celdas recibidas y usando expresiones regulares extra mails y
	 * telefonos
	 * 
	 * @param celdas lista de las celdas no vacias en la fila, los numeros de
	 *               columna pueden no ser consecutivos
	 * @return una lista de contactos
	 */
	public static ArrayList<BeanMigraContacto> extraerContactos(ArrayList<XLSCellBean> celdas) {
		String regexTelefono = "[0-9-]+";
		String regexMail = "[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+";
		Pattern pattern;
		Matcher matcher;
		ArrayList<BeanMigraContacto> contactos = null;
		XLSCellBean celda;
		String identificador = "", valor = "";
		if (celdas.size() < 1 || celdas.get(0).getValor() == null) {
			// conserva el valor preasignado a identificador
		} else {
			celda = celdas.get(0);
			// uso la celda siempre que sea una de las 3 primeras
			if (celda.getNumeroColumna() <= 2) {
				identificador = celda.getValor().toString().toLowerCase();
			}
		}
		if (celdas.size() < 2 || celdas.get(1).getValor() == null) {
			// conserva el valor preasignado a valor
		} else {
			celda = celdas.get(1);
			// uso la celda siempre que sea una de las 3 primeras
			if (celda.getNumeroColumna() <= 2) {
				valor = celdas.get(1).getValor().toString().trim();
			}
		}
		valor = identificador.concat(valor);
		String tipo = "Principal";
		String medio;
		if (identificador.contains("tel")) {
			medio = "Tel�fono";
			pattern = Pattern.compile(regexTelefono);
		} else if (identificador.contains("mail")) {
			medio = "E-Mail";
			pattern = Pattern.compile(regexMail);
		} else {
			medio = null;
			pattern = null;
		}
		if (pattern != null) {
			matcher = pattern.matcher(valor);
			contactos = new ArrayList<BeanMigraContacto>();
			while (matcher.find()) {
				BeanMigraContacto contacto = new BeanMigraContacto(medio, tipo, matcher.group(0));
				contactos.add(contacto);
			}
		}
		return contactos;
	}

	/**
	 * si alguno de los componentes es invalido, el contacto es invalido
	 * 
	 * @return
	 */
	public boolean isValido() {
		boolean valido = true;
		valido &= !(this.medio == null || this.medio.isEmpty());
		valido &= !(this.tipo == null || this.tipo.isEmpty());
		valido &= !(this.detalle == null || this.detalle.isEmpty());
		return valido;
	}

	@Override
	public String toString() {
		return "BeanContacto [medio=" + medio + ", tipo=" + tipo + ", detalle=" + detalle + "]";
	}

}
