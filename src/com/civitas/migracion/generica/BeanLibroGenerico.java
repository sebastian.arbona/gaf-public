package com.civitas.migracion.generica;

import java.util.ArrayList;

import com.civitas.migracion.BeanPlanillaProyecto;

public class BeanLibroGenerico {

	private ArrayList<BeanPlanillaProyecto> proyectoList = new ArrayList<BeanPlanillaProyecto>();

	public ArrayList<BeanPlanillaProyecto> getProyectoList() {
		return proyectoList;
	}

	public void setProyectoList(ArrayList<BeanPlanillaProyecto> proyectoList) {
		this.proyectoList = proyectoList;
	}

}
