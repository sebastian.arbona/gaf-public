package com.civitas.migracion.generica;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

import com.civitas.importacion.excel.XLSCellBean;
import com.civitas.importacion.excel.XLSRowBean;
import com.civitas.importacion.excel.XLSSheetBean;
import com.civitas.importacion.excel.XLSWorkbookBean;
import com.civitas.migracion.BeanMigraContacto;
import com.civitas.migracion.BeanMigraCuota;
import com.civitas.migracion.BeanMigraDesembolso;
import com.civitas.migracion.BeanMigraDomicilio;
import com.civitas.migracion.BeanMigraGarantia;
import com.civitas.migracion.BeanMigraNotificacion;
import com.civitas.migracion.BeanMigraPago;
import com.civitas.migracion.BeanPlanillaProyecto;
import com.civitas.migracion.MigracionXLSX;

public class MigracionGenericaXLS extends MigracionXLSX {

	public MigracionGenericaXLS(XLSWorkbookBean libro) {
		super(libro);
	}

	@SuppressWarnings("unchecked")
	@Override
	public BeanLibroGenerico importar() {
		ArrayList<XLSSheetBean> hojas = libro.getHojas();
		BeanLibroGenerico libro = new BeanLibroGenerico();
		ArrayList<BeanPlanillaProyecto> proyectoList = new ArrayList<BeanPlanillaProyecto>();
		String nombreHoja = "";
		for (XLSSheetBean hoja : hojas) {
			nombreHoja = "Hoja: " + hoja.getNumeroHoja() + " " + hoja.getNombreHoja();
			ArrayList<XLSRowBean> filas = hoja.getFilas();
			if (hoja.getNombreHoja().toLowerCase().contains("datos")) {
				for (XLSRowBean fila : filas) {
					if (fila.getNumeroFila() >= 2) {
						ArrayList<XLSCellBean> celdas = fila.getCeldas();
						System.out.println("Procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
						try {
							BeanPlanillaProyecto beanPlanillaProyecto = new BeanPlanillaProyecto("D", "EJECUCION");
							ArrayList<BeanMigraContacto> contactoList = beanPlanillaProyecto.getContactoList();
							ArrayList<BeanMigraDesembolso> desembolsoList = beanPlanillaProyecto.getDesembolsoList();
							ArrayList<BeanMigraCuota> cuotaList = beanPlanillaProyecto.getCuotaList();
							ArrayList<BeanMigraPago> pagoList = beanPlanillaProyecto.getPagoList();
							ArrayList<BeanMigraNotificacion> notificacionList = beanPlanillaProyecto
									.getNotificacionList();
							ArrayList<BeanMigraDomicilio> domicilioList = beanPlanillaProyecto.getDomicilioList();
							ArrayList<BeanMigraGarantia> garantiaList = beanPlanillaProyecto.getGarantiaList();
							String valorString;
							Double valorDouble;
							Long valorLong;
							Date valorDate;
							@SuppressWarnings("unused")
							Boolean valorBoolean;
							Object valorObject;
							for (XLSCellBean celda : celdas) {
								switch (celda.getNumeroColumna()) {
								case 0: // orden
									valorDouble = (Double) celda.getValor();
									beanPlanillaProyecto.setNumero(valorDouble.longValue());
									break;
								case 1: // nombre
									valorString = (String) celda.getValor();
									beanPlanillaProyecto.setNombreTitular(valorString);
									break;
								case 2: // apellido
									valorString = (String) celda.getValor();
									beanPlanillaProyecto.setApellidoTitular(valorString);
									beanPlanillaProyecto.setNombreTitular(beanPlanillaProyecto.getNombreCompleto());
									break;
								case 3: // cuil
									valorString = (String) celda.getValor();
									beanPlanillaProyecto.setCuitTitular(new Long(valorString.replaceAll("\\D+", "")));
									break;
								case 4: // direccion
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraDomicilio beanDomicilio = new BeanMigraDomicilio();
										beanDomicilio.setCalle(valorString);
										domicilioList.add(beanDomicilio);
									}
									break;
								case 5: // localidad
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraDomicilio beanDomicilio = domicilioList.get(0);
										beanDomicilio.setLocalidad(valorString);
										domicilioList.set(0, beanDomicilio);
									}
									break;
								case 6: // departamento
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraDomicilio beanDomicilio = domicilioList.get(0);
										beanDomicilio.setDepartamento(valorString);
										domicilioList.set(0, beanDomicilio);
									}
									break;
								case 7: // provincia
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraDomicilio beanDomicilio = domicilioList.get(0);
										beanDomicilio.setProvincia(valorString);
										if (beanDomicilio.isValido()) {
											domicilioList.set(0, beanDomicilio);
										} else {
											domicilioList.remove(0);
										}
									}
									break;
								case 8: // codigo area
									valorLong = ((Double) celda.getValor()).longValue();
									if (valorLong != null) {
										valorString = valorLong.toString() + "-";
										BeanMigraContacto beanContacto = new BeanMigraContacto("Tel�fono", "Principal",
												valorString);
										contactoList.add(beanContacto);
									}
									break;
								case 9: // numero telefono
									valorLong = ((Double) celda.getValor()).longValue();
									if (valorLong != null) {
										valorString = valorLong.toString();
										BeanMigraContacto beanContacto = contactoList.get(0);
										valorString = beanContacto.getDetalle() + valorString;
										beanContacto.setDetalle(valorString);
										if (beanContacto.isValido()) {
											contactoList.set(0, beanContacto);
										} else {
											contactoList.remove(0);
										}
									}
									break;
								case 10: // dominio
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraGarantia beanGarantia = new BeanMigraGarantia();
										beanGarantia.setDominio(valorString.replace(" ", ""));
										garantiaList.add(beanGarantia);
									}
									break;
								case 11: // marca
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraGarantia beanGarantia = garantiaList.get(0);
										beanGarantia.setMarca(valorString);
										garantiaList.set(0, beanGarantia);
									}
									break;
								case 12: // modelo/version
									// el valor puede ser un numero (fiat 500, peugeot 307), verificar
									valorObject = celda.getValor();
									if (valorObject instanceof String) {
										valorString = (String) valorObject;
									} else if (valorObject instanceof Double) {
										valorDouble = (Double) valorObject;
										valorString = String.valueOf(valorDouble.intValue());
									} else {
										valorString = null;
									}
									if (valorString != null && !valorString.isEmpty()) {
										BeanMigraGarantia beanGarantia = garantiaList.get(0);
										beanGarantia.setVersion(valorString);
										garantiaList.set(0, beanGarantia);
									}
									break;
								case 13: // modelo/anio
									valorDouble = (Double) celda.getValor();
									if (valorDouble != null) {
										BeanMigraGarantia beanGarantia = garantiaList.get(0);
										beanGarantia.setModelo(valorDouble.longValue());
										if (beanGarantia.isValido()) {
											garantiaList.set(0, beanGarantia);
										} else {
											garantiaList.remove(0);
										}
									}
									break;
								case 15: // fecha desembolso
									valorDate = (Date) celda.getValor();
									if (valorDate != null) {
										beanPlanillaProyecto.setFirmaContrato(valorDate);
										BeanMigraDesembolso beanDesembolso = new BeanMigraDesembolso();
										beanDesembolso.setNumero(1);
										beanDesembolso.setOrden(1);
										beanDesembolso.setFecha(valorDate);
										desembolsoList.add(beanDesembolso);
									}
									break;
								case 17: // monto desembolso
									valorDouble = (Double) celda.getValor();
									if (valorDouble != null) {
										BeanMigraDesembolso beanDesembolso = desembolsoList.get(0);
										beanDesembolso.setImporte(
												new BigDecimal(valorDouble).setScale(2, RoundingMode.HALF_EVEN));
										if (beanDesembolso.isValido()) {
											desembolsoList.set(0, beanDesembolso);
										} else {
											desembolsoList.remove(0);
										}
									}
									break;
								case 18: // cantidad cuotas
									valorDouble = (Double) celda.getValor();
									if (valorDouble != null) {
										beanPlanillaProyecto.setCuotasTotales(valorDouble.intValue());
									}
									break;
								case 19: // tasa interes
									valorDouble = (Double) celda.getValor();
									if (valorDouble != null && valorDouble > 0d) {
										beanPlanillaProyecto.setTasaCompensatorio(
												new BigDecimal(valorDouble).setScale(2, RoundingMode.HALF_EVEN));
									}
									break;
								case 20: // importe cobrado
									valorDouble = (Double) celda.getValor();
									if (valorDouble != null) {
										beanPlanillaProyecto.setImportePagado(
												new BigDecimal(valorDouble).setScale(2, RoundingMode.HALF_EVEN));
									}
									break;
								default:
									break;
								}
							}
							beanPlanillaProyecto.setContactoList(contactoList);
							beanPlanillaProyecto.setDesembolsoList(desembolsoList);
							beanPlanillaProyecto.setCuotaList(cuotaList);
							beanPlanillaProyecto.setPagoList(pagoList);
							beanPlanillaProyecto.setNotificacionList(notificacionList);
							beanPlanillaProyecto.setDomicilioList(domicilioList);
							beanPlanillaProyecto.setGarantiaList(garantiaList);
							boolean status = beanPlanillaProyecto.ejecutar();
							System.out.println("Estado: " + status + " Datos: " + beanPlanillaProyecto.toString());
							if (status) {
								proyectoList.add(beanPlanillaProyecto);
							}
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
		}
		libro.setProyectoList(proyectoList);
		return libro;
	}

}
