package com.civitas.migracion.cld;

import java.io.Serializable;

public class BeanGastosSeguro implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;

	private Double cuotaSeguro;
	private String dominio;

	public BeanGastosSeguro() {
	}

	public Double getCuotaSeguro() {
		return cuotaSeguro;
	}

	public void setCuotaSeguro(Double cuotaSeguro) {
		this.cuotaSeguro = cuotaSeguro;
	}

	public String getDominio() {
		return (dominio == null || dominio.isEmpty()) ? "" : dominio.trim();
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}

}
