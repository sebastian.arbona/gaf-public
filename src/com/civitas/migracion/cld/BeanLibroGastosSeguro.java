package com.civitas.migracion.cld;

import java.util.ArrayList;

public class BeanLibroGastosSeguro {
	ArrayList<BeanGastosSeguro> gastosSeguroList = new ArrayList<BeanGastosSeguro>();

	public ArrayList<BeanGastosSeguro> getGastosSeguroList() {
		return gastosSeguroList;
	}

	public void setGastosSeguroList(ArrayList<BeanGastosSeguro> gastosSeguroList) {
		this.gastosSeguroList = gastosSeguroList;
	}
}
