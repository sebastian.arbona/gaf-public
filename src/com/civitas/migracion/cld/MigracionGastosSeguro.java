package com.civitas.migracion.cld;

import java.util.ArrayList;

import com.civitas.importacion.excel.XLSCellBean;
import com.civitas.importacion.excel.XLSRowBean;
import com.civitas.importacion.excel.XLSSheetBean;
import com.civitas.importacion.excel.XLSWorkbookBean;
import com.civitas.migracion.MigracionXLSX;

public class MigracionGastosSeguro extends MigracionXLSX {

	public MigracionGastosSeguro(XLSWorkbookBean libro) {
		super(libro);
	}

	@SuppressWarnings("unchecked")
	@Override
	public BeanLibroGastosSeguro importar() {
		ArrayList<XLSSheetBean> hojas = libro.getHojas();
		BeanLibroGastosSeguro libro = new BeanLibroGastosSeguro();
		ArrayList<BeanGastosSeguro> gastosSeguroList = new ArrayList<BeanGastosSeguro>();
		String nombreHoja = "";
		for (XLSSheetBean hoja : hojas) {
			nombreHoja = "Hoja: " + hoja.getNumeroHoja() + " " + hoja.getNombreHoja();
			ArrayList<XLSRowBean> filas = hoja.getFilas();
			if (hoja.getNombreHoja().toLowerCase().contains("datos")) {
				for (XLSRowBean fila : filas) {
					if (fila.getNumeroFila() >= 1) {
						ArrayList<XLSCellBean> celdas = fila.getCeldas();
						System.out.println("Procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
						try {
							String valorString;
							Double valorDouble;
							@SuppressWarnings("unused")
							Boolean valorBoolean;
							BeanGastosSeguro beanGarantiaSeguro = new BeanGastosSeguro();
							for (XLSCellBean celda : celdas) {
								switch (celda.getNumeroColumna()) {
								case 4: // dominio
									valorString = (String) celda.getValor();
									if (valorString != null && !valorString.isEmpty()) {
										beanGarantiaSeguro.setDominio(valorString);
									}
									break;
								case 5: // cuota garantia seguro
									valorDouble = (Double) celda.getValor();
									if (valorDouble != null) {
										beanGarantiaSeguro.setCuotaSeguro(valorDouble);
									}
									break;
								}
							}
							gastosSeguroList.add(beanGarantiaSeguro);
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
		}
		libro.setGastosSeguroList(gastosSeguroList);
		return libro;
	}

}
