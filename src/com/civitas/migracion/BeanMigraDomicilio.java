package com.civitas.migracion;

import java.io.Serializable;

public class BeanMigraDomicilio implements Serializable {
	private static final long serialVersionUID = 6486500247023010305L;
	private String calle;
	private String localidad;
	private String departamento;
	private String provincia;

	public BeanMigraDomicilio() {
	}

	public BeanMigraDomicilio(String calle, String localidad, String departamento, String provincia) {
		this.calle = calle;
		this.localidad = localidad;
		this.departamento = departamento;
		this.provincia = provincia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	/**
	 * si todos los componentes del domicilio son invalidos, el domicilio es
	 * invalido
	 * 
	 * @return boolean
	 */
	public boolean isValido() {
		boolean valido = true;

		valido &= !((this.calle == null || this.calle.isEmpty()) && (this.localidad == null || this.localidad.isEmpty())
				&& (this.departamento == null || this.departamento.isEmpty())
				&& (this.provincia == null || this.provincia.isEmpty()));
		return valido;
	}

	@Override
	public String toString() {
		return "BeanDomicilio [calle=" + calle + ", localidad=" + localidad + ", departamento=" + departamento
				+ ", provincia=" + provincia + "]";
	}

}
