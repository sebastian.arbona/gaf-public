package com.civitas.migracion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

import com.civitas.importacion.excel.XLSCellBean;

public class BeanMigraCuota implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;

	private Integer numero;
	private boolean isGasto;
	private Date fechaVencimiento;
	private BigDecimal capital;
	private BigDecimal compensatorio;
	private BigDecimal gastos;

	public BeanMigraCuota() {
	}

	public BeanMigraCuota(ArrayList<XLSCellBean> celdas) throws IllegalArgumentException {
		for (XLSCellBean celda : celdas) {
			switch (celda.getNumeroColumna()) {
			case 0:
				try {
					String valor = (String) celda.getValor();
					this.isGasto = valor.toLowerCase().contains("gtos a recup");
					valor = valor.replaceAll("\\D+", "");
					this.numero = new Integer(valor);
				} catch (Exception e1) {
					throw new IllegalArgumentException("el numero de cuota no se puede determinar");
				}
				break;
			case 4:
				try {
					Date valor = (Date) celda.getValor();
					this.fechaVencimiento = valor;
				} catch (Exception e1) {
					throw new IllegalArgumentException("la fecha de vencimiento no se puede determinar");
				}
				break;
			case 7:
				try {
					Double valor = (Double) celda.getValor();
					this.compensatorio = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e1) {
					this.compensatorio = BigDecimal.ZERO;
				}
				break;
			case 9:
				try {
					Double valor = (Double) celda.getValor();
					this.capital = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e1) {
					this.capital = BigDecimal.ZERO;
				}
				break;
			case 10:
				try {
					Double valor = (Double) celda.getValor();
					this.gastos = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e1) {
					this.gastos = BigDecimal.ZERO;
				}
				break;
			default:
				break;
			}
		}
		if (!this.isValido()) {
			throw new IllegalArgumentException("los datos de la cuota no estan completos");
		}
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public boolean isGasto() {
		return isGasto;
	}

	public void setGasto(boolean isGasto) {
		this.isGasto = isGasto;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public BigDecimal getCapital() {
		return capital;
	}

	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}

	public BigDecimal getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(BigDecimal compensatorio) {
		this.compensatorio = compensatorio;
	}

	public BigDecimal getGastos() {
		return gastos;
	}

	public void setGastos(BigDecimal gastos) {
		this.gastos = gastos;
	}

	public boolean isValido() {
		boolean valido = true;
		/**
		 * el capital puede ser nulo si tiene periodo de gracia
		 */

		/**
		 * el numero de cuota es obligatorio
		 */
		valido &= !(this.numero == null);
		if (this.isGasto) {
			valido &= !(this.gastos == null || this.gastos.compareTo(BigDecimal.ZERO) > 0);
		} else {
			/**
			 * si no es gasto corresponde validar vencimiento y compensatorio
			 */
			valido &= !(this.compensatorio == null || this.compensatorio.compareTo(BigDecimal.ZERO) > 0);
			valido &= !(this.fechaVencimiento == null);
		}
		return valido;

	}

	@Override
	public String toString() {
		return "BeanCuota [numero=" + numero + ", fechaVencimiento=" + fechaVencimiento + ", capital=" + capital
				+ ", compensatorio=" + compensatorio + ", gastos=" + gastos + "]";
	}

}
