package com.civitas.migracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.civitas.importacion.excel.XLSCellBean;

public class BeanMigraNotificacion implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;
	private Date fechaNotificacion; // fecha en columna c o d
	private String observaciones; // texto en columna d o e, adyacente a la fecha anterior

	public BeanMigraNotificacion() {
	}

	public BeanMigraNotificacion(ArrayList<XLSCellBean> celdas) throws IllegalArgumentException {
		for (XLSCellBean celda : celdas) {
			switch (celda.getNumeroColumna()) {
			case 1:
				try {
					this.fechaNotificacion = (Date) celda.getValor();
				} catch (Exception e) {
					this.fechaNotificacion = null;
				}
				break;
			case 2:
				try {
					this.observaciones = ((String) celda.getValor()).trim().toLowerCase();
				} catch (Exception e) {
					this.observaciones = null;
				}
				break;
			default:
				break;
			}
		}

		if (!this.isValido()) {
			throw new IllegalArgumentException("los datos de la notificacion no estan completos");
		}

	}

	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}

	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean isValido() {
		boolean valido = true;
		if (this.fechaNotificacion == null || this.observaciones == null || this.observaciones.isEmpty()) {
			valido = false;
		}
		return valido;
	}

	@Override
	public String toString() {
		return "BeanNotificacion [fechaNotificacion=" + fechaNotificacion + ", observaciones=" + observaciones + "]";
	}

}
