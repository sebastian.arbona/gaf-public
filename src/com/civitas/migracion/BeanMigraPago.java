package com.civitas.migracion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

import com.civitas.importacion.excel.XLSCellBean;

public class BeanMigraPago implements Serializable {
	private static final long serialVersionUID = -1779405245616306547L;

	private Integer numero;
	private boolean isGasto;
	private BigDecimal capital = BigDecimal.ZERO;
	private BigDecimal compensatorio = BigDecimal.ZERO;
	private BigDecimal ivaCompensatorio = BigDecimal.ZERO;
	private BigDecimal moratorio = BigDecimal.ZERO;
	private BigDecimal ivaMoratorio = BigDecimal.ZERO;
	private BigDecimal punitorio = BigDecimal.ZERO;
	private BigDecimal ivaPunitorio = BigDecimal.ZERO;
	private BigDecimal gastoRecuperar = BigDecimal.ZERO;
	private Date fechaPago;
	private BigDecimal importePagado = BigDecimal.ZERO;
	private BigDecimal ivaPagado = BigDecimal.ZERO;

	public BeanMigraPago() {
	}

	public BeanMigraPago(ArrayList<XLSCellBean> celdas) throws IllegalArgumentException {
		for (XLSCellBean celda : celdas) {
			switch (celda.getNumeroColumna()) {
			case 0:
				try {
					String valor = (String) celda.getValor();
					this.isGasto = valor.toLowerCase().contains("gtos a recup");
					valor = valor.replaceAll("\\D+", "");
					this.numero = new Integer(valor);
				} catch (Exception e) {
					throw new IllegalArgumentException("el numero de cuota pagado no se puede determinar");
				}
				break;
			case 7:
				try {
					/**
					 * en caso de multiples desembolsos antes de la cuota 1, la cuota 1 suele tener
					 * este valor en 2 filas distintas. analizar la posibilidad de obtenerlo por
					 * diferencia <br>
					 * este valor solo sirve como bandera para determinar si la cuota tiene o no
					 * compensatorio. el valor real lo calcula el proceso de desembolso
					 */
					Double valor = (Double) celda.getValor();
					this.compensatorio = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.compensatorio = BigDecimal.ZERO;
				}
				break;
			case 8:
				try {
					/**
					 * en caso de multiples desembolsos antes de la cuota 1, la cuota 1 suele tener
					 * este valor en 2 filas distintas. analizar la posibilidad de obtenerlo por
					 * diferencia <br>
					 * este valor solo sirve como bandera para determinar si la cuota tiene o no
					 * compensatorio. el valor real lo calcula el proceso de desembolso
					 */
					Double valor = (Double) celda.getValor();
					this.ivaCompensatorio = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.ivaCompensatorio = BigDecimal.ZERO;
				}
				break;
			case 10:
				try {
					Double valor = 0d;
					if (this.isGasto) {
						valor = (Double) celda.getValor();
					}
					this.gastoRecuperar = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.gastoRecuperar = BigDecimal.ZERO;
					;
				}
				break;

			case 11:
				try {
					Double valor = (Double) celda.getValor();
					this.capital = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.capital = BigDecimal.ZERO;
				}
				break;
			case 12:
				try {
					Double valor = (Double) celda.getValor();
					this.importePagado = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.importePagado = BigDecimal.ZERO;
				}
				break;
			case 13:
				try {
					Date valor = (Date) celda.getValor();
					this.fechaPago = valor;
				} catch (Exception e1) {
					this.fechaPago = null;
				}
				break;
			case 17:
				try {
					Double valor = (Double) celda.getValor();
					this.moratorio = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.moratorio = BigDecimal.ZERO;
				}
				break;
			case 18:
				try {
					Double valor = (Double) celda.getValor();
					this.punitorio = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.punitorio = BigDecimal.ZERO;
				}
				break;
			case 19:
				try {
					Double valor = (Double) celda.getValor();
					this.ivaPagado = new BigDecimal(valor).setScale(2, RoundingMode.HALF_EVEN);
				} catch (Exception e) {
					this.ivaPagado = BigDecimal.ZERO;
				}
				break;
			default:
				break;
			}
		}
		if (this.ivaPagado != null && this.ivaPagado.compareTo(BigDecimal.ZERO) > 0) {
			BigDecimal ivaAlicuota = this.ivaPagado.divide(this.moratorio.add(this.punitorio), 2,
					RoundingMode.HALF_EVEN);
			this.ivaMoratorio = this.moratorio.multiply(ivaAlicuota).setScale(2, RoundingMode.HALF_EVEN);
			this.ivaPunitorio = ivaPagado.add(ivaMoratorio.negate());
		}
		if (!this.isValido()) {
			throw new IllegalArgumentException("los datos del pago no estan completos");
		}
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public BigDecimal getCapital() {
		return capital;
	}

	public void setCapital(BigDecimal capital) {
		this.capital = capital;
	}

	public BigDecimal getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(BigDecimal compensatorio) {
		this.compensatorio = compensatorio;
	}

	public BigDecimal getIvaCompensatorio() {
		return ivaCompensatorio;
	}

	public void setIvaCompensatorio(BigDecimal ivaCompensatorio) {
		this.ivaCompensatorio = ivaCompensatorio;
	}

	public BigDecimal getMoratorio() {
		return moratorio;
	}

	public void setMoratorio(BigDecimal moratorio) {
		this.moratorio = moratorio;
	}

	public BigDecimal getIvaMoratorio() {
		return ivaMoratorio;
	}

	public void setIvaMoratorio(BigDecimal ivaMoratorio) {
		this.ivaMoratorio = ivaMoratorio;
	}

	public BigDecimal getPunitorio() {
		return punitorio;
	}

	public void setPunitorio(BigDecimal punitorio) {
		this.punitorio = punitorio;
	}

	public BigDecimal getIvaPunitorio() {
		return ivaPunitorio;
	}

	public void setIvaPunitorio(BigDecimal ivaPunitorio) {
		this.ivaPunitorio = ivaPunitorio;
	}

	public BigDecimal getGastoRecuperar() {
		return gastoRecuperar;
	}

	public void setGastoRecuperar(BigDecimal gastoRecuperar) {
		this.gastoRecuperar = gastoRecuperar;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public BigDecimal getImportePagado() {
		return importePagado;
	}

	public void setImportePagado(BigDecimal importePagado) {
		this.importePagado = importePagado;
	}

	public BigDecimal getIvaPagado() {
		return ivaPagado;
	}

	public void setIvaPagado(BigDecimal ivaPagado) {
		this.ivaPagado = ivaPagado;
	}

	public boolean isGasto() {
		return isGasto;
	}

	public void setGasto(boolean isGasto) {
		this.isGasto = isGasto;
	}

	public boolean isValido() {
		boolean valido = true;
		/**
		 * el numero de cuota es obligatorio
		 */
		valido &= !(this.numero == null);
		if (this.isGasto) {
			valido &= !(this.gastoRecuperar == null || this.gastoRecuperar.compareTo(BigDecimal.ZERO) == 0);
		} else {
			/**
			 * si no es gasto corresponde validar importe pagado y fecha
			 */
			valido &= !(this.importePagado == null || this.importePagado.compareTo(BigDecimal.ZERO) == 0);
			valido &= !(this.fechaPago == null);
		}
		return valido;

	}

	@Override
	public String toString() {
		return "BeanCuota [numero=" + numero + ", capital=" + capital + ", compensatorio=" + compensatorio
				+ ", ivaCompensatorio=" + ivaCompensatorio + ", moratorio=" + moratorio + ", ivaMoratorio="
				+ ivaMoratorio + ", punitorio=" + punitorio + ", ivaPunitorio=" + ivaPunitorio + ", fechaPago="
				+ fechaPago + ", importePagado=" + importePagado + ", ivaPagado=" + ivaPagado + "]";
	}

}
