package com.civitas.migracion;

import com.civitas.importacion.excel.XLSWorkbookBean;

public abstract class MigracionXLSX {
	protected XLSWorkbookBean libro;

	public MigracionXLSX(XLSWorkbookBean libro) {
		this.libro = libro;
	}

	public abstract <T> T importar();
}
