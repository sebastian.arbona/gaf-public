package com.civitas.migracion;

import java.io.Serializable;

import com.nirven.creditos.hibernate.CtacteKey;

public class BeanMigraCtaCte implements Serializable {
	private static final long serialVersionUID = 6889353802628729237L;
	private Long objetoiId;
	private Long periodoCtacte;
	private Long movimientoCtacte;
	private Long verificadorCtacte;
	private Long itemCtacte;

	public BeanMigraCtaCte() {

	}

	public BeanMigraCtaCte(CtacteKey cck) {
		this.objetoiId = cck.getObjetoi_id();
		this.periodoCtacte = cck.getPeriodoCtacte();
		this.movimientoCtacte = cck.getMovimientoCtacte();
		this.verificadorCtacte = cck.getVerificadorCtacte();
		this.itemCtacte = cck.getItemCtacte();
	}

	public Long getObjetoiId() {
		return objetoiId;
	}

	public void setObjetoiId(Long objetoiId) {
		this.objetoiId = objetoiId;
	}

	public Long getPeriodoCtacte() {
		return periodoCtacte;
	}

	public void setPeriodoCtacte(Long periodoCtacte) {
		this.periodoCtacte = periodoCtacte;
	}

	public Long getMovimientoCtacte() {
		return movimientoCtacte;
	}

	public void setMovimientoCtacte(Long movimientoCtacte) {
		this.movimientoCtacte = movimientoCtacte;
	}

	public Long getVerificadorCtacte() {
		return verificadorCtacte;
	}

	public void setVerificadorCtacte(Long verificadorCtacte) {
		this.verificadorCtacte = verificadorCtacte;
	}

	public Long getItemCtacte() {
		return itemCtacte;
	}

	public void setItemCtacte(Long itemCtacte) {
		this.itemCtacte = itemCtacte;
	}

	@Override
	public String toString() {
		return "CtacteKey [objetoi=" + objetoiId + ", periodo=" + periodoCtacte + ", movimiento=" + movimientoCtacte
				+ ", verificador=" + verificadorCtacte + ", item=" + itemCtacte + "]";
	}

}
