package com.civitas.migracion.ftyc;

import java.util.ArrayList;

public class BeanLibroFTyC {

	private ArrayList<BeanPlanillaPersonas> personasList;

	public ArrayList<BeanPlanillaPersonas> getPersonasList() {
		return personasList;
	}

	public void setPersonasList(ArrayList<BeanPlanillaPersonas> personasList) {
		this.personasList = personasList;
	}

}
