package com.civitas.migracion.ftyc;

import java.io.Serializable;
import java.util.ArrayList;

import com.civitas.migracion.BeanMigraContacto;

public class BeanPlanillaPersonas implements Serializable {

	private static final long serialVersionUID = -3042455546477438408L;
	private Long numero;
	private String tipoPersona;
	private String apellido;
	private String nombre;
	private Long cuit;
	private String tipoDocumento;
	private Long numeroDocumento;
	private String sexo;
	private String nacionalidad;
	private String razonSocial;
	private String tipoSociedad;

	private ArrayList<BeanMigraContacto> contactoList;

	public BeanPlanillaPersonas() {
		this.contactoList = new ArrayList<BeanMigraContacto>();
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public String getTipoPersonaStr() {
		return tipoPersona.contains("H") ? "F" : (tipoPersona.contains("J") ? "J" : "");
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCuit() {
		return cuit;
	}

	public void setCuit(Long cuit) {
		this.cuit = cuit;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTipoDocumentoStr() {
		String tipoPersona = getTipoPersonaStr();
		return tipoPersona.equalsIgnoreCase("J") ? "RPPJ" : this.tipoDocumento;
	}

	public Long getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(Long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Long getNumeroDocumentoStr() {
		String tipoPersona = getTipoPersonaStr();
		return tipoPersona.equalsIgnoreCase("J") ? 0 : this.numeroDocumento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSexoStr() {
		String tipoPersona = getTipoPersonaStr();
		String sexoStr;
		if (tipoPersona.equalsIgnoreCase("J")) {
			sexoStr = null;
		} else {
			sexoStr = this.sexo.equalsIgnoreCase("hombre") ? "M" : (this.sexo.equalsIgnoreCase("mujer") ? "F" : null);
		}
		return sexoStr;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getNacionalidadStr() {
		String tipoPersona = getTipoPersonaStr();
		return tipoPersona.equalsIgnoreCase("J") ? null : this.nacionalidad;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public String getRazonSocialStr() {
		String tipoPersona = getTipoPersonaStr();
		return tipoPersona.equalsIgnoreCase("J") ? this.razonSocial : null;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getTipoSociedad() {
		return tipoSociedad;
	}

	public void setTipoSociedad(String tipoSociedad) {
		this.tipoSociedad = tipoSociedad;
	}

	public String getTipoSociedadStr() {
		String tipoPersona = getTipoPersonaStr();
		return tipoPersona.equalsIgnoreCase("J") ? this.tipoSociedad : null;
	}

	public ArrayList<BeanMigraContacto> getContactoList() {
		return contactoList;
	}

	public void setContactoList(ArrayList<BeanMigraContacto> contactoList) {
		this.contactoList = contactoList;
	}

	public String getNombreCompleto() {
		String tipoPersona = getTipoPersonaStr();
		String nombreCompleto = null;
		if (tipoPersona.equalsIgnoreCase("J")) {
			nombreCompleto = razonSocial;
			nombreCompleto = nombreCompleto.trim().toUpperCase();
		} else if (tipoPersona.equalsIgnoreCase("F")) {
			nombreCompleto = apellido.trim() + ", " + nombre.trim();
			nombreCompleto = nombreCompleto.trim().toUpperCase();
		}
		return nombreCompleto;
	}

	@Override
	public String toString() {
		return "BeanPlanillaPersonas [cuit=" + cuit + ", getNombreCompleto()=" + getNombreCompleto() + "]";
	}

}
