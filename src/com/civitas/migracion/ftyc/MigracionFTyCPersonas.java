package com.civitas.migracion.ftyc;

import java.util.ArrayList;

import com.civitas.importacion.excel.XLSCellBean;
import com.civitas.importacion.excel.XLSRowBean;
import com.civitas.importacion.excel.XLSSheetBean;
import com.civitas.importacion.excel.XLSWorkbookBean;
import com.civitas.migracion.BeanMigraContacto;
import com.civitas.migracion.MigracionXLSX;

public class MigracionFTyCPersonas extends MigracionXLSX {

	public MigracionFTyCPersonas(XLSWorkbookBean libro) {
		super(libro);
	}

	@SuppressWarnings("unchecked")
	@Override
	public BeanLibroFTyC importar() {
		ArrayList<XLSSheetBean> hojas = libro.getHojas();
		BeanLibroFTyC libroFTyC = new BeanLibroFTyC();
		ArrayList<BeanPlanillaPersonas> listaPersonas = new ArrayList<BeanPlanillaPersonas>();
		String nombreHoja = "";
		for (XLSSheetBean hoja : hojas) {
			nombreHoja = "Hoja: " + hoja.getNumeroHoja() + " " + hoja.getNombreHoja();
			ArrayList<XLSRowBean> filas = hoja.getFilas();
			if (hoja.getNombreHoja().toLowerCase().contains("civitas")) {
				for (XLSRowBean fila : filas) {
					if (fila.getNumeroFila() >= 1) {
						ArrayList<XLSCellBean> celdas = fila.getCeldas();
						System.out.println("Procesando hoja: " + nombreHoja + " - fila: " + fila.getNumeroFila());
						try {
							BeanPlanillaPersonas beanPersona = new BeanPlanillaPersonas();
							ArrayList<BeanMigraContacto> contactoList = beanPersona.getContactoList();
							String valorString;
							Double valorDouble;
							Object valorObject;
							for (XLSCellBean celda : celdas) {
								switch (celda.getNumeroColumna()) {
								case 0: // orden
									valorDouble = (Double) celda.getValor();
									beanPersona.setNumero(valorDouble.longValue());
									break;
								case 1: // tipo persona
									valorString = (String) celda.getValor();
									beanPersona.setTipoPersona(valorString);
									break;
								case 2: // apellido
									valorString = (String) celda.getValor();
									beanPersona.setApellido(valorString);
									break;
								case 3: // nombre
									valorString = (String) celda.getValor();
									beanPersona.setNombre(valorString);
									break;
								case 4: // cuil
									valorDouble = (Double) celda.getValor();
									beanPersona.setCuit(valorDouble == null ? null : valorDouble.longValue());
									break;
								case 5: // tipo de documento
									valorString = (String) celda.getValor();
									beanPersona.setTipoDocumento(valorString);
									break;
								case 6: // numero de documento
									valorDouble = (Double) celda.getValor();
									beanPersona
											.setNumeroDocumento(valorDouble == null ? null : valorDouble.longValue());
									break;
								case 7: // sexo
									valorString = (String) celda.getValor();
									beanPersona.setSexo(valorString);
									break;
								case 8: // nacionalidad
									valorString = (String) celda.getValor();
									beanPersona.setNacionalidad(valorString);
									break;
								case 9: // razon social
									valorString = (String) celda.getValor();
									beanPersona.setRazonSocial(valorString);
									break;
								case 10: // tipo de sociedad
									valorString = (String) celda.getValor();
									beanPersona.setTipoSociedad(valorString);
									break;
								case 11: // telefono
									valorObject = celda.getValor();
									if (valorObject instanceof String) {
										valorString = (String) valorObject;
									} else if (valorObject instanceof Double) {
										valorDouble = (Double) valorObject;
										valorString = String.valueOf(valorDouble.longValue());
									} else {
										valorString = null;
									}
									if (valorString != null) {
										BeanMigraContacto beanContacto = new BeanMigraContacto("Tel�fono", "Principal",
												valorString.replaceAll("\\D+", ""));
										contactoList.add(beanContacto);
									}
									break;
								case 12: // celular
									valorObject = celda.getValor();
									if (valorObject instanceof String) {
										valorString = (String) valorObject;
									} else if (valorObject instanceof Double) {
										valorDouble = (Double) valorObject;
										valorString = String.valueOf(valorDouble.longValue());
									} else {
										valorString = null;
									}
									if (valorString != null) {
										BeanMigraContacto beanContacto = new BeanMigraContacto("Celular", "Principal",
												valorString.replaceAll("\\D+", ""));
										contactoList.add(beanContacto);
									}
									break;
								case 13: // mail
									valorString = (String) celda.getValor();
									if (valorString != null) {
										BeanMigraContacto beanContacto = new BeanMigraContacto("E-Mail", "Notificaci�n",
												valorString);
										contactoList.add(beanContacto);
									}
									break;
								default:
									break;
								}
							}
							beanPersona.setContactoList(contactoList);
							System.out
									.println("Fila: " + beanPersona.getNumero() + " Datos: " + beanPersona.toString());
							listaPersonas.add(beanPersona);
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}

				}
			}
		}
		libroFTyC.setPersonasList(listaPersonas);
		return libroFTyC;
	}

}
