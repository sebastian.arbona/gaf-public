package com.civitas.process;

import java.util.HashMap;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;

import com.asf.cred.business.GarantiasActivas;
import com.asf.cred.business.GarantiasAltaContable;
import com.asf.cred.business.GarantiasCanceladas;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;

public class ActualizacionGarantias implements IExecuteExternalClass {

	private HashMap<String, Object> errores;
	private BusinessPersistance bp = null;

	@Override
	public boolean execute() {
		boolean seActualizo = false;
		int interval = SessionHandler.getCurrentSessionHandler().getRequest().getSession().getMaxInactiveInterval();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setMaxInactiveInterval(-1);
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		try {
			new GarantiasActivas(bp).ejecutar();
			new GarantiasAltaContable(bp).ejecutar();
			new GarantiasCanceladas(bp).ejecutar();
			seActualizo = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.bp.getCurrentSession().setFlushMode(FlushMode.AUTO);
			this.bp.getCurrentSession().setCacheMode(CacheMode.NORMAL);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setMaxInactiveInterval(interval);
		}

		if (seActualizo) {
			errores.put("actualizacionGarantias.seActualizo", "actualizacionGarantias.seActualizo");
		} else {
			errores.put("actualizacionGarantias.noSeActualizo", "actualizacionGarantias.noSeActualizo");
		}
		return true;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

}
