package com.civitas.cred.beans;

import java.io.Serializable;
import java.util.Date;

import com.asf.util.DateHelper;

public class BeanCreditosBonificados implements Serializable {
	private static final long serialVersionUID = 5L;
	
	private Long idBanco;
	private Long idConvenio;
	private Date fechaDesembolso;
	private Date fechaDesembolsoHasta;
	private String linea;
	private String enteBonificador;
	private String convenio;
	private String bonificacion;
	private String tipoBonificacion;
	private Long proyectoId;
	private String nroCredito;
	private String titular;
	private String cuit;
	private Long codTitular;
	private String cotomadores;
	private String cuitCotomadores;
	private String destino;
	private String nroExpediente;
	private String nroResolucion;
	private Date fechaResolucion;
	private Double montoCredito;
	private Long numeroDesembolso;
	private Double montoDesembolso;
	private String tasaInteres;
	private String tasaBonificacion;
	private Long plazoTotal;
	private Long plazoAmortizacionCapital;
	private Date primerVencimientoInteres;
	private Date primerVencimientoCapital;
	private String periodoCapital;
	private String periodoInteres;
	private String estadoSolicitud;
	private String sectorEconomico;
	private String actividadAfipId;
	private String actividadAfipDescripcion;
	private Date fechaInicioActividad;
	private Long personalOcupado;
	private Double promedioVentasAnuales;
	private String domicilioProyecto;
	private String departamentoProyecto;	
	
	public BeanCreditosBonificados() {
	}
	
	public Long getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}
	public Long getIdConvenio() {
		return idConvenio;
	}
	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}
	public Date getFechaDesembolso() {
		return fechaDesembolso;
	}
	public void setFechaDesembolso(Date fechaDesembolso) {
		this.fechaDesembolso = fechaDesembolso;
	}
	public void setFechaDesembolso(Object fechaDesembolso) {
		
		this.fechaDesembolso = fechaDesembolso == null 
				? null 
						: DateHelper.getDate( (String) fechaDesembolso );
	}
	
	public Date getFechaDesembolsoHasta() {
		return fechaDesembolsoHasta;
	}
	public void setFechaDesembolsoHasta(Date fechaDesembolsoHasta) {
		this.fechaDesembolsoHasta = fechaDesembolsoHasta;
	}
	public void setFechaDesembolsoHasta(Object fechaDesembolsoHasta) {
		
		this.fechaDesembolsoHasta = fechaDesembolsoHasta == null 
				? null 
						: DateHelper.getDate( (String) fechaDesembolsoHasta );
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getEnteBonificador() {
		return enteBonificador;
	}
	public void setEnteBonificador(String enteBonificador) {
		this.enteBonificador = enteBonificador;
	}
	public String getConvenio() {
		return convenio;
	}
	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	public String getBonificacion() {
		return bonificacion;
	}
	public void setBonificacion(String bonificacion) {
		this.bonificacion = bonificacion;
	}
	public String getTipoBonificacion() {
		return tipoBonificacion;
	}
	public void setTipoBonificacion(String tipoBonificacion) {
		this.tipoBonificacion = tipoBonificacion;
	}
	public Long getProyectoId() {
		return proyectoId;
	}
	public void setProyectoId(Long proyectoId) {
		this.proyectoId = proyectoId;
	}
	public String getNroCredito() {
		return nroCredito;
	}
	public void setNroCredito(String nroCredito) {
		this.nroCredito = nroCredito;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public Long getCodTitular() {
		return codTitular;
	}
	public void setCodTitular(Long codTitular) {
		this.codTitular = codTitular;
	}
	public String getCotomadores() {
		return cotomadores;
	}
	public void setCotomadores(String cotomadores) {
		this.cotomadores = cotomadores;
	}
	public String getCuitCotomadores() {
		return cuitCotomadores;
	}
	public void setCuitCotomadores(String cuitCotomadores) {
		this.cuitCotomadores = cuitCotomadores;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getNroExpediente() {
		return nroExpediente;
	}
	public void setNroExpediente(String nroExpediente) {
		this.nroExpediente = nroExpediente;
	}
	public String getNroResolucion() {
		return nroResolucion;
	}
	public void setNroResolucion(String nroResolucion) {
		this.nroResolucion = nroResolucion;
	}

	public Double getMontoCredito() {
		return montoCredito;
	}
	public void setMontoCredito(Double montoCredito) {
		this.montoCredito = montoCredito;
	}
	public Long getNumeroDesembolso() {
		return numeroDesembolso;
	}
	public void setNumeroDesembolso(Long numeroDesembolso) {
		this.numeroDesembolso = numeroDesembolso;
	}
	public Double getMontoDesembolso() {
		return montoDesembolso;
	}
	public void setMontoDesembolso(Double montoDesembolso) {
		this.montoDesembolso = montoDesembolso;
	}
	public String getTasaInteres() {
		return tasaInteres;
	}
	public void setTasaInteres(String tasaInteres) {
		this.tasaInteres = tasaInteres;
	}
	public String getTasaBonificacion() {
		return tasaBonificacion;
	}
	public void setTasaBonificacion(String tasaBonificacion) {
		this.tasaBonificacion = tasaBonificacion;
	}
	public Long getPlazoTotal() {
		return plazoTotal;
	}
	public void setPlazoTotal(Long plazoTotal) {
		this.plazoTotal = plazoTotal;
	}
	public Long getPlazoAmortizacionCapital() {
		return plazoAmortizacionCapital;
	}
	public void setPlazoAmortizacionCapital(Long plazoAmortizacionCapital) {
		this.plazoAmortizacionCapital = plazoAmortizacionCapital;
	}

	public String getPeriodoCapital() {
		return periodoCapital;
	}
	public void setPeriodoCapital(String periodoCapital) {
		this.periodoCapital = periodoCapital;
	}
	public String getPeriodoInteres() {
		return periodoInteres;
	}
	public void setPeriodoInteres(String periodoInteres) {
		this.periodoInteres = periodoInteres;
	}
	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}
	public void setEstadoSolicitud(String estadoSolicitud) {
		this.estadoSolicitud = estadoSolicitud;
	}
	public String getSectorEconomico() {
		return sectorEconomico;
	}
	public void setSectorEconomico(String sectorEconomico) {
		this.sectorEconomico = sectorEconomico;
	}
	public String getActividadAfipId() {
		return actividadAfipId;
	}
	public void setActividadAfipId(String actividadAfipId) {
		this.actividadAfipId = actividadAfipId;
	}	
	public String getActividadAfipDescripcion() {
		return actividadAfipDescripcion;
	}
	public void setActividadAfipDescripcion(String actividadAfipDescripcion) {
		this.actividadAfipDescripcion = actividadAfipDescripcion;
	}
	public Long getPersonalOcupado() {
		return personalOcupado;
	}
	public void setPersonalOcupado(Long personalOcupado) {
		this.personalOcupado = personalOcupado;
	}
	public Double getPromedioVentasAnuales() {
		return promedioVentasAnuales;
	}
	public void setPromedioVentasAnuales(Double promedioVentasAnuales) {
		this.promedioVentasAnuales = promedioVentasAnuales;
	}
	public void setPromedioVentasAnuales(Object promedioVentasAnuales) {
		this.promedioVentasAnuales = promedioVentasAnuales == null 
				? 0 
						: ((Number) promedioVentasAnuales).doubleValue();				
	}
	public String getDomicilioProyecto() {
		return domicilioProyecto;
	}
	public void setDomicilioProyecto(String domicilioProyecto) {
		this.domicilioProyecto = domicilioProyecto;
	}
	public String getDepartamentoProyecto() {
		return departamentoProyecto;
	}
	public void setDepartamentoProyecto(String departamentoProyecto) {
		this.departamentoProyecto = departamentoProyecto;
	}

	public Date getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(Date fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}
	public void setFechaResolucion(Object fechaResolucion) {
		
		this.fechaResolucion = fechaResolucion == null 
				? null 
						: DateHelper.getDate( (String) fechaResolucion );
	}

	public Date getPrimerVencimientoInteres() {
		return primerVencimientoInteres;
	}

	public void setPrimerVencimientoInteres(Date primerVencimientoInteres) {
		this.primerVencimientoInteres = primerVencimientoInteres;
	}
	public void setPrimerVencimientoInteres(Object primerVencimientoInteres) {
		
		this.primerVencimientoInteres = primerVencimientoInteres == null 
				? null 
						: DateHelper.getDate( (String) primerVencimientoInteres );
	}
	public Date getPrimerVencimientoCapital() {
		return primerVencimientoCapital;
	}
	public void setPrimerVencimientoCapital(Date primerVencimientoCapital) {
		this.primerVencimientoCapital = primerVencimientoCapital;
	}
	public void setPrimerVencimientoCapital(Object primerVencimientoCapital) {
		
		this.primerVencimientoCapital = primerVencimientoCapital == null 
				? null 
						: DateHelper.getDate( (String) primerVencimientoCapital );
	}

	public Date getFechaInicioActividad() {
		return fechaInicioActividad;
	}

	public void setFechaInicioActividad(Date fechaInicioActividad) {		
		this.fechaInicioActividad = fechaInicioActividad;
	}

	public void setFechaInicioActividad(Object fechaInicioActividad) {
		
		this.fechaInicioActividad = fechaInicioActividad == null 
				? null 
						: DateHelper.getDate( (String) fechaInicioActividad );
	}	
}
