package com.civitas.cred.beans;

import java.util.Date;

import com.asf.cred.business.CreditoCalculoDeuda;

public class BeanResumenComportamiento {
	private com.nirven.creditos.hibernate.Objetoi credito;

	private double deudaTotal;
	private double deudaExigible;
	private double montoValorCartera;
	private String linea;
	private String comportamientoPago;
	private String clasificacionMora;
	
	public com.nirven.creditos.hibernate.Objetoi getCredito() {
		return credito;
	}
	
	public void setCredito(com.nirven.creditos.hibernate.Objetoi credito) {
		this.credito = credito;
		CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(credito.getId(), new Date());
		ccd.calcular();
		deudaTotal = ccd.getDeudaTotal();
		deudaExigible = ccd.getDeudaVencidaFechaEstricta();
		montoValorCartera = credito.getMontoValoresCartera();
		linea = credito.getLinea().getNombre();
		comportamientoPago = credito.getComportamientoActual();
		clasificacionMora = credito.getClasificacionMora();
	}
	
	public double getDeudaTotal() {
		return deudaTotal;
	}
	
	public void setDeudaTotal(double deudaTotal) {
		this.deudaTotal = deudaTotal;
	}
	
	public double getDeudaExigible() {
		return deudaExigible;
	}
	
	public void setDeudaExigible(double deudaExigible) {
		this.deudaExigible = deudaExigible;
	}

	public double getMontoValorCartera() {
		return montoValorCartera;
	}

	public void setMontoValorCartera(double montoValorCartera) {
		this.montoValorCartera = montoValorCartera;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getComportamientoPago() {
		return comportamientoPago;
	}

	public void setComportamientoPago(String comportamientoPago) {
		this.comportamientoPago = comportamientoPago;
	}

	public String getClasificacionMora() {
		return clasificacionMora;
	}

	public void setClasificacionMora(String clasificacionMora) {
		this.clasificacionMora = clasificacionMora;
	}
	
}
