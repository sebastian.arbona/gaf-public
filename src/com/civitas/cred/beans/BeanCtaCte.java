package com.civitas.cred.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.ManyToOne;

import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;

public class BeanCtaCte implements Serializable {

	private static final long serialVersionUID = 4722266014586810622L;

	private Integer numero;
	private Date fechaEmision;
	private String detalle;
	private Date fechaGeneracion;
	private Date fechaVencimiento;
	private String estado;
	private Double capital;
	private Double compensatorio;
	private Double punitorio;
	private Double moratorio;
	private Double gastosSimples;
	private Double cer;

	// private Double ivaGastos;
	// private Double ivaCompensatorio;
	// private Double ivaPunitorio;
	// private Double ivaMoratorio;
	// private Double ivaGastosRec;
	// private Double ivaMultas;

	private Double gastosRec;
	private Double multas;
	private Long idCuota;
	private Boleto boleto;
	private Long caratulaId;
	private Double saldoCuota;
	private Double importe;
	private String concepto;
	private boolean manual;
	private String observaciones;
	private Double compensatorioBruto;
	private String motivo;
	private Tipomov tipoMov;

	public BeanCtaCte() {
		capital = 0.0;
		compensatorio = 0.0;
		// ivaCompensatorio = 0.0;
		punitorio = 0.0;
		// ivaPunitorio = 0.0;
		moratorio = 0.0;
		// ivaMoratorio = 0.0;
		gastosSimples = 0.0;
		// ivaGastos = 0.0;
		multas = 0.0;
		// ivaMultas = 0.0;
		gastosRec = 0.0;
		// ivaGastosRec = 0.0;
		cer = 0.0;
	}

	public BeanCtaCte(Integer numero, Date fechaEmision, Date fechaVencimiento, String estado, String detalle,
			Long idCuota) {
		this();
		this.numero = numero;
		this.fechaEmision = fechaEmision;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
		this.detalle = detalle;
		this.idCuota = idCuota;
		validarDetalle();
	}

	public BeanCtaCte(Integer numero, Date fechaEmision, Date fechaVencimiento, String estado, String detalle,
			Long idCuota, Double importe, String concepto, String observaciones) {
		this();
		this.numero = numero;
		this.fechaEmision = fechaEmision;
		this.fechaVencimiento = fechaVencimiento;
		this.estado = estado;
		this.detalle = detalle;
		this.idCuota = idCuota;
		this.importe = importe;
		this.concepto = concepto;
		this.observaciones = observaciones;
		validarDetalle();
	}

	public BeanCtaCte(Integer numero, Date fechaEmision, Date fechaVencimiento, String estado, String detalle,
			Long idCuota, Double saldoCuota, Boleto boleto) {
		this(numero, fechaEmision, fechaVencimiento, estado, detalle, idCuota);
		this.saldoCuota = saldoCuota;
		this.boleto = boleto;
	}

	public BeanCtaCte(Integer numero, Date fechaEmision, Date fechaVencimiento, String estado, String detalle,
			Long idCuota, Double saldoCuota, Boleto boleto, Double importe, String concepto, String Observaciones) {
		this(numero, fechaEmision, fechaVencimiento, estado, detalle, idCuota);
		this.saldoCuota = saldoCuota;
		this.boleto = boleto;
		this.importe = importe;
		this.concepto = concepto;
		this.observaciones = Observaciones;
	}

	public BeanCtaCte(Integer numero, Date fechaEmision, Date fechaVencimiento, String estado, String detalle,
			Long idCuota, Double saldoCuota, Boleto boleto, Long caratulaId) {
		this(numero, fechaEmision, fechaVencimiento, estado, detalle, idCuota, saldoCuota, boleto);
		this.caratulaId = caratulaId;
	}

	public BeanCtaCte(Integer numero, Date fechaEmision, Date fechaVencimiento, String estado, String detalle,
			Long idCuota, Double saldoCuota, Boleto boleto, Long caratulaId, Double importe, String concepto,
			String observaciones) {
		this(numero, fechaEmision, fechaVencimiento, estado, detalle, idCuota, saldoCuota, boleto);
		this.caratulaId = caratulaId;
		this.importe = importe;
		this.concepto = concepto;
		this.observaciones = observaciones;
	}

	public void addCtaCte(Ctacte cc) {
		cc.redondear();
		Concepto concepto = cc.getAsociado();
		double alicuotaIva = cc.getId().getObjetoi().getPersona().getAlicuotaIva();
		double importeIva = cc.getImporte() * alicuotaIva;
		if (concepto != null && concepto.getConcepto() != null) {
			String tipo = concepto.getConcepto().getConcepto();
			if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_CAPITAL)) {
				this.capital += cc.getImporte();
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_COMPENSATORIO)) {
				this.compensatorio += cc.getImporte();
				// this.ivaCompensatorio += importeIva;
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_MORATORIO)) {
				this.moratorio += cc.getImporte();
				// this.ivaMoratorio += importeIva;
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_PUNITORIO)) {
				this.punitorio += cc.getImporte();
				// this.ivaPunitorio += importeIva;
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_GASTOS)) {
				this.gastosSimples += cc.getImporte();
				// this.ivaGastos = importeIva;
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_GASTOS_A_RECUPERAR)) {
				this.gastosRec += cc.getImporte();
				// this.ivaGastosRec += importeIva;
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_MULTAS)) {
				this.multas += cc.getImporte();
				// this.ivaMultas += importeIva;
			} else if (tipo.equalsIgnoreCase(Concepto.CONCEPTO_CER)) {
				this.cer += cc.getImporte();
			}
		}
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getFechaStr() {
		/*
		 * Ticket 9318 if(detalle != null && (detalle.equalsIgnoreCase("Pago") ||
		 * detalle.equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_REIMPUTACION))){
		 */
		if (detalle != null && (detalle.equalsIgnoreCase("Pago") || detalle.equalsIgnoreCase("Reimputacion Pago"))) {
			return DateHelper.getString(fechaGeneracion);
		} else if (detalle != null && detalle.toLowerCase().replace("\u00c9", "e").replace("\u00e9", "e")
				.equalsIgnoreCase("Debito Manual")) {
			return DateHelper.getString(fechaGeneracion);
		} else if (detalle != null && detalle.toLowerCase().replace("\u00c9", "e").replace("\u00e9", "e")
				.equalsIgnoreCase("Credito Manual")) {
			return DateHelper.getString(fechaGeneracion);
		} else if (fechaVencimiento != null) {
			return DateHelper.getString(fechaVencimiento);
		} else {
			return DateHelper.getString(fechaEmision);
		}
	}

	public String getDetalle() {
		return this.detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Date getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getFechaVencimientoStr() {
		return DateHelper.getString(fechaVencimiento);
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Double getCapital() {
		return this.capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

	public String getCapitalStr() {
		if (this.capital == null) {
			return "";
		}
		return String.format("%,.2f", this.capital);
	}

	public Double getCompensatorio() {
		return this.compensatorio;
	}

	public void setCompensatorio(Double compensatorio) {
		this.compensatorio = compensatorio;
	}

	public String getCompensatorioStr() {
		if (this.compensatorio == null) {
			return "";
		}
		return String.format("%,.2f", this.compensatorio);
	}

	public Double getPunitorio() {
		return this.punitorio;
	}

	public void setPunitorio(Double punitorio) {
		this.punitorio = punitorio;
	}

	public String getPunitorioStr() {
		if (this.punitorio == null) {
			return "";
		}
		return String.format("%,.2f", this.punitorio);
	}

	public Double getMoratorio() {
		return moratorio;
	}

	public void setMoratorio(Double moratorio) {
		this.moratorio = moratorio;
	}

	public String getMoratorioStr() {
		if (this.moratorio == null) {
			return "";
		}
		return String.format("%,.2f", this.moratorio);
	}

	public Double getGastos() {
		return ((gastosSimples != null ? gastosSimples.doubleValue() : 0.0)
				+ (gastosRec != null ? gastosRec.doubleValue() : 0.0));
	}

	public String getGastosStr() {
		if (getGastos() == null)
			return "";
		return String.format("%,.2f", getGastos());
	}

	public Double getMultas() {
		return multas;
	}

	public void setMultas(Double multas) {
		this.multas = multas;
	}

	public String getMultasStr() {
		if (this.multas == null) {
			return "";
		}
		return String.format("%,.2f", this.multas);
	}

	public Long getIdCuota() {
		return idCuota;
	}

	public void setIdCuota(Long idCuota) {
		this.idCuota = idCuota;
	}

	@ManyToOne
	public Boleto getBoleto() {
		return boleto;
	}

	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	public Long getCaratulaId() {
		return caratulaId;
	}

	public void setCaratulaId(Long caratulaId) {
		this.caratulaId = caratulaId;
	}

	public Double getSaldoCuota() {
		try {
			return Math.max(saldoCuota, 0);// evitar saldo negativo
		} catch (Exception e) {
		}
		return 0.0;
	}

	public void setSaldoCuota(Double saldoCuota) {
		this.saldoCuota = saldoCuota;
	}

	public String getSaldoCuotaStr() {
		if (getSaldoCuota() == null) {
			return "";
		}
		return String.format("%,.2f", getSaldoCuota());
	}

	public boolean isCuota() {
		if (detalle == null || detalle.equals("Cuota") || detalle.equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_CADUCIDAD)) {
			return true;
		}
		return false;

	}

	public boolean isBonificacion() {
		return detalle != null && detalle.equalsIgnoreCase("bonificacion");
	}

	public boolean isDebito() {
		if (detalle == null) {
			return true;
		}

		if (detalle.equalsIgnoreCase("Cuota")) {
			return true;
		} else if (detalle.equalsIgnoreCase("Bonificacion")) {
			return false;
		} else if (detalle.equalsIgnoreCase("Pago")) {
			return false;
		} else if (detalle.equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_REIMPUTACION)) {
			return false;
		} else if (detalle.equalsIgnoreCase("Debito Manual")) {
			return true;
		} else if (detalle.equalsIgnoreCase("credito manual")) {
			return false;
		} else if (detalle.equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_CADUCIDAD)) {
			return true;
		}

		return true;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	// se utiliza para ver si la ctacte es un credito o debito manual
	private void validarDetalle() {
		if (this.detalle != null && (this.detalle.equalsIgnoreCase("debito manual")
				|| this.detalle.equalsIgnoreCase("credito manual"))) {
			manual = true;
		}
	}

	public Double getCompensatorioBruto() {
		return compensatorioBruto;
	}

	public void setCompensatorioBruto(Double compensatorioBruto) {
		this.compensatorioBruto = compensatorioBruto;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getMotivo() {
		return motivo;
	}

	public Double getGastosRec() {
		return gastosRec;
	}

	public void setGastosRec(Double gastosRec) {
		this.gastosRec = gastosRec;
	}

	public String getGastosRecStr() {
		if (gastosRec == null) {
			return "";
		}
		return String.format("%,.2f", gastosRec);
	}

	public Double getGastosSimples() {
		return gastosSimples;

	}

	public void setGastosSimples(Double gastosSimples) {
		this.gastosSimples = gastosSimples;
	}

	public String getGastosSimplesStr() {
		if (gastosSimples == null) {
			return "";
		}
		return String.format("%,.2f", gastosSimples);
	}

	public Tipomov getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(Tipomov tipoMov) {
		this.tipoMov = tipoMov;
	}

	public Double getCer() {
		return cer;
	}

	public void setCer(Double cer) {
		this.cer = cer;
	}

	public String getCerStr() {
		if (this.cer == null) {
			return "";
		}
		return String.format("%,.2f", this.cer);
	}

}
