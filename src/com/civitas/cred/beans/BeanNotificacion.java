package com.civitas.cred.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.Objetoi;

public class BeanNotificacion {
	private Objetoi credito;
	private double deudaTotal;
	private double deudaExigible;
	private boolean bonificado;
	private boolean garantiaSGR;
	private boolean seguroVencido;
	private String auditoriaFinalPosee;
	private String departamento;

	public Objetoi getCredito() {
		return credito;
	}

	@SuppressWarnings("unchecked")
	public void setCredito(Objetoi credito) {
		this.credito = credito;
		CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(credito.getId(), new Date());
		ccd.calcular();
		deudaTotal = ccd.getDeudaTotal();
		deudaExigible = ccd.getDeudaVencidaFechaEstricta();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Object[]> departamentos = new ArrayList<Object[]>();
		String consulta = "SELECT dm.* FROM Domicilio dm "
				+ "INNER JOIN DomicilioObjetoi dmoi ON dmoi.domicilio_id = dm.id "
				+ "WHERE dm.tipo = 'Dom. Proyecto' AND dmoi.objetoi_id = " + this.credito.getId();
		departamentos = (List<Object[]>) bp.createSQLQuery(consulta).list();
		if (departamentos.size() > 0) {
			Object[] dep = departamentos.get(0);
			this.departamento = (String) dep[3];
		}

		this.setGarantiaSGR(credito.getTieneGarantiaSGR());
		this.setSeguroVencido(credito.getTieneSeguroVencido());
		this.setAuditoriaFinalPosee(credito.getAplicaFondos());

	}

	public Double getDeudaTotal() {
		return deudaTotal;
	}

	public boolean isBonificado() {
		return bonificado;
	}

	public void setBonificado(boolean bonificado) {
		this.bonificado = bonificado;
	}

	public double getDeudaExigible() {
		return deudaExigible;
	}

	public boolean isGarantiaSGR() {
		return garantiaSGR;
	}

	public void setGarantiaSGR(boolean garantiaSGR) {
		this.garantiaSGR = garantiaSGR;
	}

	public boolean isSeguroVencido() {
		return seguroVencido;
	}

	public void setSeguroVencido(boolean seguroVencido) {
		this.seguroVencido = seguroVencido;
	}

	public String getAuditoriaFinalPosee() {
		return auditoriaFinalPosee;
	}

	public void setAuditoriaFinalPosee(String auditoriaFinalPosee) {
		this.auditoriaFinalPosee = auditoriaFinalPosee;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

}
