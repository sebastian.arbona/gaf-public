package com.civitas.cred.beans;

import java.io.Serializable;
import java.util.Date;

import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.SolicitudProrroga;

public class BeanNotificacionMoraDetalle implements Serializable {
	private static final long serialVersionUID = 6829207888370855990L;

	private Long idObjetoi;
	private Long numeroAtencion;
	private String numeroCredito;
	private String departamento;
	private Long titularId;
	private String titularNombre;

	private String titularCuit;
	private String lineaNombre;
	private String comportamientoNombre;
	private String estadoNombre;
	private Date fechaMora;
	private Integer diasAtraso;
	private Double deudaTotal;
	private Double deudaExigible;
	private Date fechaUltimaCobranza;
	private String monedaNombre;
	private Double monedaCotizacion;
	private Boolean bonificado;
	private Boolean valorCartera;
	private Double valoresCartera;
	private String ultimaNotificacionTipo;
	private Date ultimaNotificacionFecha;
	private String ultimaNotificacionObservacion;
	private Boolean garantiaSGR;
	private Date fechaCaidaBonificacion;
	private Boolean seguroVencido;
	private Long ultimaProrroga;
	private String ultimaProrrogaEstado;
	private String aplicoFondos;

	public BeanNotificacionMoraDetalle() {

	}

	public BeanNotificacionMoraDetalle(BeanNotificacion beanNotificacion) {

		Objetoi credito = beanNotificacion.getCredito();
		Linea linea = credito.getLinea();
		Notificacion ultimaNotificacion = credito.getUltimaNotificacion();
		SolicitudProrroga prorroga = credito.getUltimaProrroga();
		this.idObjetoi = credito.getId();
		this.numeroAtencion = credito.getNumeroAtencion();
		this.numeroCredito = credito.getNumeroCredito();
		this.departamento = beanNotificacion.getDepartamento();
		this.titularId = credito.getPersona_id();
		this.titularNombre = credito.getTitularNombre();
		this.titularCuit = credito.getTitularCuit();
		this.lineaNombre = linea.getNombre();
		this.comportamientoNombre = credito.getObjetoiComportamientoActual().getNombreComportamiento();
		this.estadoNombre = credito.getEstadoActual().getEstado().getNombreEstado();
		this.fechaMora = credito.getFechaUltimoVencimientoDate();
		this.diasAtraso = credito.getDiasDeAtraso();
		this.deudaTotal = beanNotificacion.getDeudaTotal();
		this.deudaExigible = beanNotificacion.getDeudaExigible();
		this.fechaUltimaCobranza = credito.getFechaUltimoPago();

		this.monedaNombre = linea.getMoneda().getDenominacion();
		this.monedaCotizacion = linea.getMoneda().getCotizacion();
		this.bonificado = beanNotificacion.isBonificado();
		this.valorCartera = credito.getValorCartera();
		this.valoresCartera = credito.getMontoValoresCartera();

		if (ultimaNotificacion != null) {
			this.ultimaNotificacionTipo = ultimaNotificacion.getTipoAviso();
			this.ultimaNotificacionFecha = ultimaNotificacion.getFechaCreada();
			this.ultimaNotificacionObservacion = ultimaNotificacion.getObservaciones();
		}

		this.garantiaSGR = beanNotificacion.isGarantiaSGR();
		this.fechaCaidaBonificacion = credito.getFechaBajaBonificacion();
		this.seguroVencido = beanNotificacion.isSeguroVencido();

		if (prorroga != null) {
			this.ultimaProrroga = prorroga.getId();
			this.ultimaProrrogaEstado = prorroga.getNombreEstado();
		}

		this.aplicoFondos = beanNotificacion.getAuditoriaFinalPosee();
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public Long getTitularId() {
		return titularId;
	}

	public void setTitularId(Long titularId) {
		this.titularId = titularId;
	}

	public String getTitularNombre() {
		return titularNombre;
	}

	public void setTitularNombre(String titularNombre) {
		this.titularNombre = titularNombre;
	}

	public String getTitularCuit() {
		return titularCuit;
	}

	public void setTitularCuit(String titularCuit) {
		this.titularCuit = titularCuit;
	}

	public String getLineaNombre() {
		return lineaNombre;
	}

	public void setLineaNombre(String lineaNombre) {
		this.lineaNombre = lineaNombre;
	}

	public String getComportamientoNombre() {
		return comportamientoNombre;
	}

	public void setComportamientoNombre(String comportamientoNombre) {
		this.comportamientoNombre = comportamientoNombre;
	}

	public String getEstadoNombre() {
		return estadoNombre;
	}

	public void setEstadoNombre(String estadoNombre) {
		this.estadoNombre = estadoNombre;
	}

	public String getFechaMora() {
		return DateHelper.getString(fechaMora);
	}

	public void setFechaMora(String fechaMora) {
		this.fechaMora = DateHelper.getDate(fechaMora);
	}

	public Integer getDiasAtraso() {
		return diasAtraso;
	}

	public void setDiasAtraso(Integer diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	public Double getDeudaTotal() {
		return deudaTotal;
	}

	public void setDeudaTotal(Double deudaTotal) {
		this.deudaTotal = deudaTotal;
	}

	public Double getDeudaExigible() {
		return deudaExigible;
	}

	public void setDeudaExigible(Double deudaExigible) {
		this.deudaExigible = deudaExigible;
	}

	public String getFechaUltimaCobranza() {
		return DateHelper.getString(fechaUltimaCobranza);
	}

	public void setFechaUltimaCobranza(String fechaUltimaCobranza) {
		this.fechaUltimaCobranza = DateHelper.getDate(fechaUltimaCobranza);
	}

	public String getMonedaNombre() {
		return monedaNombre;
	}

	public void setMonedaNombre(String monedaNombre) {
		this.monedaNombre = monedaNombre;
	}

	public Double getMonedaCotizacion() {
		return monedaCotizacion;
	}

	public void setMonedaCotizacion(Double monedaCotizacion) {
		this.monedaCotizacion = monedaCotizacion;
	}

	public Boolean getBonificado() {
		return bonificado;
	}

	public void setBonificado(Boolean bonificado) {
		this.bonificado = bonificado;
	}

	public Double getValoresCartera() {
		return valoresCartera;
	}

	public void setValoresCartera(Double valoresCartera) {
		this.valoresCartera = valoresCartera;
	}

	public Boolean getValorCartera() {
		return valorCartera;
	}

	public void setValorCartera(Boolean valorCartera) {
		this.valorCartera = valorCartera;
	}

	public String getUltimaNotificacionTipo() {
		return ultimaNotificacionTipo;
	}

	public void setUltimaNotificacionTipo(String ultimaNotificacionTipo) {
		this.ultimaNotificacionTipo = ultimaNotificacionTipo;
	}

	public String getUltimaNotificacionFecha() {
		return DateHelper.getString(ultimaNotificacionFecha);
	}

	public void setUltimaNotificacionFecha(String ultimaNotificacionFecha) {
		this.ultimaNotificacionFecha = DateHelper.getDate(ultimaNotificacionFecha);
	}

	public String getUltimaNotificacionObservacion() {
		return ultimaNotificacionObservacion;
	}

	public void setUltimaNotificacionObservacion(String ultimaNotificacionObservacion) {
		this.ultimaNotificacionObservacion = ultimaNotificacionObservacion;
	}

	public Boolean getGarantiaSGR() {
		return garantiaSGR;
	}

	public void setGarantiaSGR(Boolean garantiaSGR) {
		this.garantiaSGR = garantiaSGR;
	}

	public String getFechaCaidaBonificacion() {
		return DateHelper.getString(fechaCaidaBonificacion);
	}

	public void setFechaCaidaBonificacion(String fechaCaidaBonificacion) {
		this.fechaCaidaBonificacion = DateHelper.getDate(fechaCaidaBonificacion);
	}

	public Boolean getSeguroVencido() {
		return seguroVencido;
	}

	public void setSeguroVencido(Boolean seguroVencido) {
		this.seguroVencido = seguroVencido;
	}

	public Long getUltimaProrroga() {
		return ultimaProrroga;
	}

	public void setUltimaProrroga(Long ultimaProrroga) {
		this.ultimaProrroga = ultimaProrroga;
	}

	public String getUltimaProrrogaEstado() {
		return ultimaProrrogaEstado;
	}

	public void setUltimaProrrogaEstado(String ultimaProrrogaEstado) {
		this.ultimaProrrogaEstado = ultimaProrrogaEstado;
	}

	public String getAplicoFondos() {
		return aplicoFondos;
	}

	public void setAplicoFondos(String aplicoFondos) {
		this.aplicoFondos = aplicoFondos;

	}

}