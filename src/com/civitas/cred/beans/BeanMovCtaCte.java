package com.civitas.cred.beans;

import java.io.Serializable;

import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Pagos;

public class BeanMovCtaCte implements Serializable {

	private static final long serialVersionUID = -6010498451246752287L;
	
	private Ctacte ctacte;
	private Pagos pago;
	
	public BeanMovCtaCte() {
	}
	
	public BeanMovCtaCte(Ctacte ctacte) {
		this.ctacte = ctacte;
	}

	private String getConcepto() {
		return ctacte.getAsociado().getConcepto().getAbreviatura();
	}
	
	public double getCapital() {
		if (getConcepto().equals("cap")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getCompensatorio() {
		if (getConcepto().equals("com")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getBonificacion() {
		if (getConcepto().equals("bon")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getMoratorio() {
		if (getConcepto().equals("mor")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getPunitorio() {
		if (getConcepto().equals("pun")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getMultas() {
		if (getConcepto().equals("mul")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getGastos() {
		if (getConcepto().equals("gas")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public double getGastosRecuperar() {
		if (getConcepto().equals("rec")) {
			return ctacte.getImporte();
		}
		return 0;
	}
	
	public Ctacte getCtacte() {
		return ctacte;
	}

	public void setCtacte(Ctacte ctacte) {
		this.ctacte = ctacte;
	}

	public Pagos getPago() {
		return pago;
	}

	public void setPago(Pagos pago) {
		this.pago = pago;
	}
	
}
