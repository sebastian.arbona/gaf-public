package com.civitas.cred.beans;

import java.math.BigDecimal;

public class BeanDesembolsoParcial {

	private BigDecimal numeroAtencion;
	private String expediente;
	private String nombre;
	private Double qqFinal;
	private Double porcentaje;

	public BigDecimal getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(BigDecimal numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getQqFinal() {
		return qqFinal;
	}

	public void setQqFinal(Double qqFinal) {
		this.qqFinal = qqFinal;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getPorcentajeStr() {
		return String.format("%.2f", porcentaje);
	}

}
