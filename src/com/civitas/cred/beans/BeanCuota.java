/**
 * 
 */
package com.civitas.cred.beans;

import java.util.Date;

import com.asf.util.DateHelper;

/**
 * @author cnoguerol
 */
public class BeanCuota {
	private Integer desembolso = 0;
	private Integer numero = 0;
	private Double capital = 0d;
	private Double compensatorio = 0d;
	private Double ivaCompensatorio = 0d;
	private Double tasaAmortizacion = 0d;
	private Double saldoCapital = 0d;
	private Double punitorio = 0d;
	private Double ivaPunitorio = 0d;
	private Double moratorio = 0d;
	private Double ivaMoratorio = 0d;
	private Double tasaBonificacion = 0d;
	private Double bonificacion = 0d;
	private Double ivaBonificacion = 0d;
	private Date fechaVencimiento;
	private Date fechaVencimientoAnterior;
	private Integer diasPeriodo = 0;
	private Integer diasTransaccion = 0;
	private Double interes;
	private Double subsidio;

	public Integer getDesembolso() {
		return desembolso;
	}

	public void setDesembolso(Integer desembolso) {
		this.desembolso = desembolso;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Double getCapital() {
		return capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

	public Double getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(Double compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Double getIvaCompensatorio() {
		return ivaCompensatorio;
	}

	public void setIvaCompensatorio(Double ivaCompensatorio) {
		this.ivaCompensatorio = ivaCompensatorio;
	}

	public Double getTasaAmortizacion() {
		return tasaAmortizacion;
	}

	public void setTasaAmortizacion(Double tasaAmortizacion) {
		this.tasaAmortizacion = tasaAmortizacion;
	}

	public Double getSaldoCapital() {
		return saldoCapital;
	}

	public void setSaldoCapital(Double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	public Double getPunitorio() {
		return punitorio;
	}

	public void setPunitorio(Double punitorio) {
		this.punitorio = punitorio;
	}

	public Double getIvaPunitorio() {
		return ivaPunitorio;
	}

	public void setIvaPunitorio(Double ivaPunitorio) {
		this.ivaPunitorio = ivaPunitorio;
	}

	public Double getMoratorio() {
		return moratorio;
	}

	public void setMoratorio(Double moratorio) {
		this.moratorio = moratorio;
	}

	public Double getIvaMoratorio() {
		return ivaMoratorio;
	}

	public void setIvaMoratorio(Double ivaMoratorio) {
		this.ivaMoratorio = ivaMoratorio;
	}

	public Double getTasaBonificacion() {
		return tasaBonificacion;
	}

	public void setTasaBonificacion(Double tasaBonificacion) {
		this.tasaBonificacion = tasaBonificacion;
	}

	public Double getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(Double bonificacion) {
		this.bonificacion = bonificacion;
	}

	public Double getIvaBonificacion() {
		return ivaBonificacion;
	}

	public void setIvaBonificacion(Double ivaBonificacion) {
		this.ivaBonificacion = ivaBonificacion;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getFechaVencimientoStr() {
		return DateHelper.getString(this.getFechaVencimiento());
	}

	public void setFechaVencimientoStr(String fechaVencimientoStr) {
		this.setFechaVencimiento(DateHelper.getDate(fechaVencimientoStr));
	}

	public Date getFechaVencimientoAnterior() {
		return fechaVencimientoAnterior;
	}

	public void setFechaVencimientoAnterior(Date fechaVencimientoAnterior) {
		this.fechaVencimientoAnterior = fechaVencimientoAnterior;
	}

	public String getFechaVencimientoAnteriorStr() {
		return DateHelper.getString(this.getFechaVencimientoAnterior());
	}

	public void setFechaVencimientoAnteriorStr(String fechaVencimientoAnteriorStr) {
		this.setFechaVencimientoAnterior(DateHelper.getDate(fechaVencimientoAnteriorStr));
	}

	public Integer getDiasPeriodo() {
		return diasPeriodo;
	}

	public void setDiasPeriodo(Integer diasPeriodo) {
		this.diasPeriodo = diasPeriodo;
	}

	public Integer getDiasTransaccion() {
		return diasTransaccion;
	}

	public void setDiasTransaccion(Integer diasTransaccion) {
		this.diasTransaccion = diasTransaccion;
	}

	public Double getInteres() {
		return interes;
	}

	public void setInteres(Double interes) {
		this.interes = interes;
	}

	public Double getSubsidio() {
		return subsidio;
	}

	public void setSubsidio(Double subsidio) {
		this.subsidio = subsidio;
	}

	public Double getValorCuota() {
		Double valorCuota = capital + compensatorio + ivaCompensatorio + punitorio + ivaPunitorio + moratorio
				+ ivaMoratorio - bonificacion - ivaBonificacion;
		return valorCuota;
	}

	public void agregar(BeanCuota beanCuota) {
		this.capital += beanCuota.getCapital();
		this.compensatorio += beanCuota.getCompensatorio();
		this.ivaCompensatorio += beanCuota.getIvaCompensatorio();
		this.bonificacion += beanCuota.getBonificacion();
		this.ivaBonificacion += beanCuota.getIvaBonificacion();
		this.saldoCapital += beanCuota.getSaldoCapital();
	}

	@Override
	public String toString() {
		return "BeanCuota [numero=" + this.numero + ", desembolso=" + desembolso + ", fechaVencimiento="
				+ this.fechaVencimiento + ",capital=" + this.capital + ", diasPeriodo=" + diasPeriodo
				+ ", diasTransaccion=" + diasTransaccion + "]";
	}

}
