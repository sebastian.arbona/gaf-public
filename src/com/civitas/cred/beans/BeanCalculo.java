package com.civitas.cred.beans;

import java.util.ArrayList;
import java.util.Date;

import com.nirven.creditos.hibernate.ObjetoiIndice;

public class BeanCalculo {
	private Date primerVencimientoCapital;
	private Date primerVencimientoCompensatorio;
	private Date segundoVencimientoCapital;
	private Date segundoVencimientoCompensatorio;
	private Double tasaAmortizacion;
	private double[] tasasBonificacion;
	private Integer plazoCapital;
	private Integer plazoCompensatorio;
	private Integer periodicidadCapital;
	private Integer periodicidadCompensatorio;
	private ArrayList<BeanDesembolso> desembolsos;
	private Date fechaDesembolso;
	private Double montoFinanciamiento;
	private ObjetoiIndice tasaCompensatorio;
	private Double ivaAlicuota;

	public BeanCalculo(Date primerVencimientoCapital, Date primerVencimientoCompensatorio, Double tasaAmortizacion,
			double[] tasasBonificacion, Integer plazoCapital, Integer plazoCompensatorio, Integer periodicidadCapital,
			Integer periodicidadCompensatorio, ArrayList<BeanDesembolso> desembolsos) {
		this.primerVencimientoCapital = primerVencimientoCapital;
		this.primerVencimientoCompensatorio = primerVencimientoCompensatorio;
		this.tasaAmortizacion = tasaAmortizacion;
		this.tasasBonificacion = tasasBonificacion;
		this.plazoCapital = plazoCapital;
		this.plazoCompensatorio = plazoCompensatorio;
		this.periodicidadCapital = periodicidadCapital;
		this.periodicidadCompensatorio = periodicidadCompensatorio;
		this.desembolsos = desembolsos;
	}

	public BeanCalculo(Date primerVencimientoCapital, Date primerVencimientoCompensatorio, Double tasaAmortizacion,
			Integer plazoCapital, Integer plazoCompensatorio, Integer periodicidadCapital,
			Integer periodicidadCompensatorio, Date fechaDesembolso, Double montoDesembolso) {
		this.primerVencimientoCapital = primerVencimientoCapital;
		this.primerVencimientoCompensatorio = primerVencimientoCompensatorio;
		this.tasaAmortizacion = tasaAmortizacion;
		this.plazoCapital = plazoCapital;
		this.plazoCompensatorio = plazoCompensatorio;
		this.periodicidadCapital = periodicidadCapital;
		this.periodicidadCompensatorio = periodicidadCompensatorio;
		this.fechaDesembolso = fechaDesembolso;
		this.montoFinanciamiento = montoDesembolso;
	}

	public Date getPrimerVencimientoCapital() {
		return primerVencimientoCapital;
	}

	public void setPrimerVencimientoCapital(Date primerVencimientoCapital) {
		this.primerVencimientoCapital = primerVencimientoCapital;
	}

	public Date getPrimerVencimientoCompensatorio() {
		return primerVencimientoCompensatorio;
	}

	public void setPrimerVencimientoCompensatorio(Date primerVencimientoCompensatorio) {
		this.primerVencimientoCompensatorio = primerVencimientoCompensatorio;
	}

	public Double getTasaAmortizacion() {
		return tasaAmortizacion;
	}

	public void setTasaAmortizacion(Double tasaAmortizacion) {
		this.tasaAmortizacion = tasaAmortizacion;
	}

	public Integer getPlazoCapital() {
		return plazoCapital;
	}

	public void setPlazoCapital(Integer plazoCapital) {
		this.plazoCapital = plazoCapital;
	}

	/**
	 * Si los creditos tienen tasa cero, deber� indicarse toda la configuracion de
	 * compensatorio y el sistema se encargar� de calcular el valor cero.<br>
	 * Es distinto no tener tasa a tener tasa cero.
	 * 
	 * @return la cantidad de cuotas que incluyen interes compensatorio
	 */
	public Integer getPlazoCompensatorio() {
		return plazoCompensatorio;
	}

	public void setPlazoCompensatorio(Integer plazoCompensatorio) {
		this.plazoCompensatorio = plazoCompensatorio;
	}

	public Integer getPeriodicidadCapital() {
		return periodicidadCapital;
	}

	public void setPeriodicidadCapital(Integer periodicidadCapital) {
		this.periodicidadCapital = periodicidadCapital;
	}

	public Integer getPeriodicidadCompensatorio() {
		return periodicidadCompensatorio;
	}

	public void setPeriodicidadCompensatorio(Integer periodicidadCompensatorio) {
		this.periodicidadCompensatorio = periodicidadCompensatorio;
	}

	public ArrayList<BeanDesembolso> getDesembolsos() {
		return desembolsos;
	}

	public void setDesembolsos(ArrayList<BeanDesembolso> desembolsos) {
		this.desembolsos = desembolsos;
	}

	public Double getTasaBonificacion(int numeroCuota) {
		if (numeroCuota >= 0 && numeroCuota < tasasBonificacion.length) {
			return tasasBonificacion[numeroCuota];
		}
		return 0.0;
	}

	public double[] getTasasBonificacion() {
		return tasasBonificacion;
	}

	public void setTasasBonificacion(double[] tasasBonificacion) {
		this.tasasBonificacion = tasasBonificacion;
	}

	public Date getFechaDesembolso() {
		return fechaDesembolso;
	}

	public void setFechaDesembolso(Date fechaDesembolso) {
		this.fechaDesembolso = fechaDesembolso;
	}

	public Double getMontoFinanciamiento() {
		return montoFinanciamiento;
	}

	public void setMontoFinanciamiento(Double montoFinanciamiento) {
		this.montoFinanciamiento = montoFinanciamiento;
	}

	public ObjetoiIndice getTasaCompensatorio() {
		return tasaCompensatorio;
	}

	public void setTasaCompensatorio(ObjetoiIndice tasaCompensatorio) {
		this.tasaCompensatorio = tasaCompensatorio;
	}

	public Date getSegundoVencimientoCapital() {
		return segundoVencimientoCapital;
	}

	public void setSegundoVencimientoCapital(Date segundoVencimientoCapital) {
		this.segundoVencimientoCapital = segundoVencimientoCapital;
	}

	public Date getSegundoVencimientoCompensatorio() {
		return segundoVencimientoCompensatorio;
	}

	public void setSegundoVencimientoCompensatorio(Date segundoVencimientoCompensatorio) {
		this.segundoVencimientoCompensatorio = segundoVencimientoCompensatorio;
	}

	public Double getIvaAlicuota() {
		return ivaAlicuota;
	}

	public void setIvaAlicuota(Double ivaAlicuota) {
		this.ivaAlicuota = ivaAlicuota;
	}

}
