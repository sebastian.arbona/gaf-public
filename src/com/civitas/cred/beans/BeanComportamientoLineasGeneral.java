package com.civitas.cred.beans;

import java.util.ArrayList;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.Linea;

public class BeanComportamientoLineasGeneral {
	private double deudaTotal;
	private double deudaExigible;
	private double montoValorCartera;
	private String linea;
	private String comportamientoPago;
	private int cantidad;
	private double montoDesembolsado;
	private double totalRecaudado;
	private double capitalRecaudado;
	private double compensatorioRecaudado;
	private double moratorioRecaudado;
	private double punitorioRecaudado;
	private double gastoRecaudado;
	private int anioFirmaContrato;
	private double porCapSobDes;
	
	
	public List<BeanComportamientoLineasGeneral> armarReporteComportamientoLineas(
			List<BeanComportamientoLineas> creditosBean, List<Long> lineasSeleccionadas, Integer anio) {
		if(creditosBean != null && creditosBean.size() > 0){
			double deudaTotalSN = 0, deudaExigibleSN = 0, montoValorCarteraSN = 0, montoDesembolsadoSN = 0, totalRecaudadoSN = 0, capitalRecaudadoSN = 0, compensatorioRecaudadoSN = 0, moratorioRecaudadoSN = 0, punitorioRecaudadoSN = 0, gastoRecaudadoSN = 0;
			double deudaTotalICA = 0, deudaExigibleICA = 0, montoValorCarteraICA = 0, montoDesembolsadoICA = 0, totalRecaudadoICA = 0, capitalRecaudadoICA = 0, compensatorioRecaudadoICA = 0, moratorioRecaudadoICA = 0, punitorioRecaudadoICA = 0, gastoRecaudadoICA = 0;
			double deudaTotalPA = 0, deudaExigiblePA = 0, montoValorCarteraPA = 0, montoDesembolsadoPA = 0, totalRecaudadoPA = 0, capitalRecaudadoPA = 0, compensatorioRecaudadoPA = 0, moratorioRecaudadoPA = 0, punitorioRecaudadoPA = 0, gastoRecaudadoPA = 0;
			double deudaTotalARA = 0, deudaExigibleARA = 0, montoValorCarteraARA = 0, montoDesembolsadoARA = 0, totalRecaudadoARA = 0, capitalRecaudadoARA = 0, compensatorioRecaudadoARA = 0, moratorioRecaudadoARA = 0, punitorioRecaudadoARA = 0, gastoRecaudadoARA = 0;
			double porcSN = 0, porcICA = 0, porcPA = 0, porcARCA = 0;
			int countSN = 0, countICA = 0, countPA = 0, countARCA = 0;
			for (BeanComportamientoLineas beanComportamientoLineas : creditosBean) {
				if(beanComportamientoLineas.getComportamientoPago().equalsIgnoreCase("SITUACI�N NORMAL")){
					deudaTotalSN += beanComportamientoLineas.getDeudaTotal();
					deudaExigibleSN += beanComportamientoLineas.getDeudaExigible();
					montoValorCarteraSN += beanComportamientoLineas.getMontoValorCartera();
					montoDesembolsadoSN += beanComportamientoLineas.getMontoDesembolsado();
					totalRecaudadoSN += beanComportamientoLineas.getTotalRecaudado();
					capitalRecaudadoSN += beanComportamientoLineas.getCapitalRecaudado();
					compensatorioRecaudadoSN = beanComportamientoLineas.getCompensatorioRecaudado();
					moratorioRecaudadoSN += beanComportamientoLineas.getMoratorioRecaudado();
					punitorioRecaudadoSN += beanComportamientoLineas.getPunitorioRecaudado();
					gastoRecaudadoSN += beanComportamientoLineas.getGastoRecaudado();
					countSN += 1;
										
				}else if(beanComportamientoLineas.getComportamientoPago().equalsIgnoreCase("INADECUADO CON ATRASO")){
					deudaTotalICA += beanComportamientoLineas.getDeudaTotal();
					deudaExigibleICA += beanComportamientoLineas.getDeudaExigible();
					montoValorCarteraICA += beanComportamientoLineas.getMontoValorCartera();
					montoDesembolsadoICA += beanComportamientoLineas.getMontoDesembolsado();
					totalRecaudadoICA += beanComportamientoLineas.getTotalRecaudado();
					capitalRecaudadoICA += beanComportamientoLineas.getCapitalRecaudado();
					compensatorioRecaudadoICA += beanComportamientoLineas.getCompensatorioRecaudado();
					moratorioRecaudadoICA += beanComportamientoLineas.getMoratorioRecaudado();
					punitorioRecaudadoICA += beanComportamientoLineas.getPunitorioRecaudado();
					gastoRecaudadoICA += beanComportamientoLineas.getGastoRecaudado();
					countICA += 1;
									
				}else if(beanComportamientoLineas.getComportamientoPago().equalsIgnoreCase("CON PROBLEMAS ATRASO")){
					deudaTotalPA += beanComportamientoLineas.getDeudaTotal();
					deudaExigiblePA += beanComportamientoLineas.getDeudaExigible();
					montoValorCarteraPA += beanComportamientoLineas.getMontoValorCartera();
					montoDesembolsadoPA += beanComportamientoLineas.getMontoDesembolsado();
					totalRecaudadoPA += beanComportamientoLineas.getTotalRecaudado();
					capitalRecaudadoPA += beanComportamientoLineas.getCapitalRecaudado();
					compensatorioRecaudadoPA += beanComportamientoLineas.getCompensatorioRecaudado();
					moratorioRecaudadoPA += beanComportamientoLineas.getMoratorioRecaudado();
					punitorioRecaudadoPA += beanComportamientoLineas.getPunitorioRecaudado();
					gastoRecaudadoPA += beanComportamientoLineas.getGastoRecaudado();
					countPA += 1;
										
				}else if(beanComportamientoLineas.getComportamientoPago().equalsIgnoreCase("ALTO RIESGO CON ATRASO")){
					deudaTotalARA += beanComportamientoLineas.getDeudaTotal();
					deudaExigibleARA += beanComportamientoLineas.getDeudaExigible();
					montoValorCarteraARA += beanComportamientoLineas.getMontoValorCartera(); 
					montoDesembolsadoARA += beanComportamientoLineas.getMontoDesembolsado();
					totalRecaudadoARA += beanComportamientoLineas.getTotalRecaudado();
					capitalRecaudadoARA += beanComportamientoLineas.getCapitalRecaudado(); 
					compensatorioRecaudadoARA += beanComportamientoLineas.getCompensatorioRecaudado();
					moratorioRecaudadoARA += beanComportamientoLineas.getMoratorioRecaudado();
					punitorioRecaudadoARA += beanComportamientoLineas.getPunitorioRecaudado();
					gastoRecaudadoARA += beanComportamientoLineas.getGastoRecaudado();
					countARCA += 1;
										
				}
			}
			
			if(!lineasSeleccionadas.isEmpty()) {
				BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
				Linea l = (Linea) bp.getById(Linea.class, lineasSeleccionadas.get(0));
				linea = l != null ? l.getNombre() : "";
			}else{
				linea = "";
			}
			
			List<BeanComportamientoLineasGeneral> list =  new ArrayList<BeanComportamientoLineasGeneral>();
			
			if(deudaTotalSN>0){
				porcSN = (capitalRecaudadoSN*100)/montoDesembolsadoSN;
				BeanComportamientoLineasGeneral beanSN = new BeanComportamientoLineasGeneral();
				beanSN.setComportamientoPago("SITUACI�N NORMAL");
				beanSN.setLinea(linea);
				beanSN.setDeudaTotal(deudaTotalSN);
				beanSN.setDeudaExigible(deudaExigibleSN);
				beanSN.setMontoValorCartera(montoValorCarteraSN);
				beanSN.setMontoDesembolsado(montoDesembolsadoSN);
				beanSN.setTotalRecaudado(totalRecaudadoSN);
				beanSN.setCapitalRecaudado(capitalRecaudadoSN);
				beanSN.setCompensatorioRecaudado(compensatorioRecaudadoSN);
				beanSN.setMoratorioRecaudado(moratorioRecaudadoSN);
				beanSN.setPunitorioRecaudado(punitorioRecaudadoSN);
				beanSN.setGastoRecaudado(gastoRecaudadoSN);
				beanSN.setAnioFirmaContrato(anio);
				beanSN.setPorCapSobDes(porcSN);
				beanSN.setCantidad(countSN);
				
				list.add(beanSN);
			}
			
			if(deudaTotalICA>0){
				porcICA = (capitalRecaudadoICA*100)/montoDesembolsadoICA;
				BeanComportamientoLineasGeneral beanICA = new BeanComportamientoLineasGeneral();
				beanICA.setComportamientoPago("INADECUADO CON ATRASO");
				beanICA.setLinea(linea);
				beanICA.setDeudaTotal(deudaTotalICA);
				beanICA.setDeudaExigible(deudaExigibleICA);
				beanICA.setMontoValorCartera(montoValorCarteraICA);
				beanICA.setMontoDesembolsado(montoDesembolsadoICA);
				beanICA.setTotalRecaudado(totalRecaudadoICA);
				beanICA.setCapitalRecaudado(capitalRecaudadoICA);
				beanICA.setCompensatorioRecaudado(compensatorioRecaudadoICA);
				beanICA.setMoratorioRecaudado(moratorioRecaudadoICA);
				beanICA.setPunitorioRecaudado(punitorioRecaudadoICA);
				beanICA.setGastoRecaudado(gastoRecaudadoICA);
				beanICA.setAnioFirmaContrato(anio);
				beanICA.setPorCapSobDes(porcICA);
				beanICA.setCantidad(countICA);
				
				list.add(beanICA);
			}
			
			if(deudaTotalPA>0){
				porcPA = (capitalRecaudadoPA*100)/montoDesembolsadoPA;
				BeanComportamientoLineasGeneral beanPA = new BeanComportamientoLineasGeneral();
				beanPA.setComportamientoPago("CON PROBLEMAS ATRASO");
				beanPA.setLinea(linea);
				beanPA.setDeudaTotal(deudaTotalPA);
				beanPA.setDeudaExigible(deudaExigiblePA);
				beanPA.setMontoValorCartera(montoValorCarteraPA);
				beanPA.setMontoDesembolsado(montoDesembolsadoPA);
				beanPA.setTotalRecaudado(totalRecaudadoPA);
				beanPA.setCapitalRecaudado(capitalRecaudadoPA);
				beanPA.setCompensatorioRecaudado(compensatorioRecaudadoPA);
				beanPA.setMoratorioRecaudado(moratorioRecaudadoPA);
				beanPA.setPunitorioRecaudado(punitorioRecaudadoPA);
				beanPA.setGastoRecaudado(gastoRecaudadoPA);
				beanPA.setAnioFirmaContrato(anio);
				beanPA.setPorCapSobDes(porcPA);
				beanPA.setCantidad(countPA);
				
				list.add(beanPA);
			}
			
			if(deudaTotalARA>0){
				porcARCA = (capitalRecaudadoARA*100)/montoDesembolsadoARA;
				BeanComportamientoLineasGeneral beanARA = new BeanComportamientoLineasGeneral();
				beanARA.setComportamientoPago("ALTO RIESGO CON ATRASO");
				beanARA.setLinea(linea);
				beanARA.setDeudaTotal(deudaTotalARA);
				beanARA.setDeudaExigible(deudaExigibleARA);
				beanARA.setMontoValorCartera(montoValorCarteraARA);
				beanARA.setMontoDesembolsado(montoDesembolsadoARA);
				beanARA.setTotalRecaudado(totalRecaudadoARA);
				beanARA.setCapitalRecaudado(capitalRecaudadoARA);
				beanARA.setCompensatorioRecaudado(compensatorioRecaudadoARA);
				beanARA.setMoratorioRecaudado(moratorioRecaudadoARA);
				beanARA.setPunitorioRecaudado(punitorioRecaudadoARA);
				beanARA.setGastoRecaudado(gastoRecaudadoARA);
				beanARA.setAnioFirmaContrato(anio);
				beanARA.setPorCapSobDes(porcARCA);
				beanARA.setCantidad(countARCA);
				
				list.add(beanARA);
			}
			
			
			BeanComportamientoLineasGeneral beanTotales = new BeanComportamientoLineasGeneral();
			beanTotales.setComportamientoPago("Total General");
			beanTotales.setDeudaTotal(deudaTotalSN+deudaTotalICA+deudaTotalPA+deudaTotalARA);
			beanTotales.setDeudaExigible(deudaExigibleSN+deudaExigibleICA+deudaExigiblePA+deudaExigibleARA);
			beanTotales.setMontoValorCartera(montoValorCarteraSN+montoValorCarteraICA+montoValorCarteraPA+montoValorCarteraARA);
			beanTotales.setMontoDesembolsado(montoDesembolsadoSN+montoDesembolsadoICA+montoDesembolsadoPA+montoDesembolsadoARA);
			beanTotales.setTotalRecaudado(totalRecaudadoSN+totalRecaudadoICA+totalRecaudadoPA+totalRecaudadoARA);
			beanTotales.setCapitalRecaudado(capitalRecaudadoSN+capitalRecaudadoICA+capitalRecaudadoPA+capitalRecaudadoARA);
			beanTotales.setCompensatorioRecaudado(compensatorioRecaudadoSN+compensatorioRecaudadoICA+compensatorioRecaudadoPA+compensatorioRecaudadoARA);
			beanTotales.setMoratorioRecaudado(moratorioRecaudadoSN+moratorioRecaudadoICA+moratorioRecaudadoPA+moratorioRecaudadoARA);
			beanTotales.setPunitorioRecaudado(punitorioRecaudadoSN+punitorioRecaudadoICA+punitorioRecaudadoPA+punitorioRecaudadoARA);
			beanTotales.setGastoRecaudado(gastoRecaudadoSN+gastoRecaudadoICA+gastoRecaudadoPA+gastoRecaudadoARA);
			beanTotales.setPorCapSobDes(porcSN+porcICA+porcPA+porcARCA);
			beanTotales.setCantidad(countSN+countICA+countPA+countARCA);
			
			list.add(beanTotales);
			
			return list;
		}else{
			return null;
		}
	}
	
	
	
	public double getDeudaTotal() {
		return deudaTotal;
	}
	public void setDeudaTotal(double deudaTotal) {
		this.deudaTotal = deudaTotal;
	}
	public double getDeudaExigible() {
		return deudaExigible;
	}
	public void setDeudaExigible(double deudaExigible) {
		this.deudaExigible = deudaExigible;
	}
	public double getMontoValorCartera() {
		return montoValorCartera;
	}
	public void setMontoValorCartera(double montoValorCartera) {
		this.montoValorCartera = montoValorCartera;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getComportamientoPago() {
		return comportamientoPago;
	}
	public void setComportamientoPago(String comportamientoPago) {
		this.comportamientoPago = comportamientoPago;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getMontoDesembolsado() {
		return montoDesembolsado;
	}
	public void setMontoDesembolsado(double montoDesembolsado) {
		this.montoDesembolsado = montoDesembolsado;
	}
	public double getTotalRecaudado() {
		return totalRecaudado;
	}
	public void setTotalRecaudado(double totalRecaudado) {
		this.totalRecaudado = totalRecaudado;
	}
	public double getCapitalRecaudado() {
		return capitalRecaudado;
	}
	public void setCapitalRecaudado(double capitalRecaudado) {
		this.capitalRecaudado = capitalRecaudado;
	}
	public double getCompensatorioRecaudado() {
		return compensatorioRecaudado;
	}
	public void setCompensatorioRecaudado(double compensatorioRecaudado) {
		this.compensatorioRecaudado = compensatorioRecaudado;
	}
	public double getMoratorioRecaudado() {
		return moratorioRecaudado;
	}
	public void setMoratorioRecaudado(double moratorioRecaudado) {
		this.moratorioRecaudado = moratorioRecaudado;
	}
	public double getPunitorioRecaudado() {
		return punitorioRecaudado;
	}
	public void setPunitorioRecaudado(double punitorioRecaudado) {
		this.punitorioRecaudado = punitorioRecaudado;
	}
	public double getGastoRecaudado() {
		return gastoRecaudado;
	}
	public void setGastoRecaudado(double gastoRecaudado) {
		this.gastoRecaudado = gastoRecaudado;
	}
	public int getAnioFirmaContrato() {
		return anioFirmaContrato;
	}
	public void setAnioFirmaContrato(int anioFirmaContrato) {
		this.anioFirmaContrato = anioFirmaContrato;
	}
	public double getPorCapSobDes() {
		return porCapSobDes;
	}
	public void setPorCapSobDes(double porCapSobDes) {
		this.porCapSobDes = porCapSobDes;
	}

	
	
}
