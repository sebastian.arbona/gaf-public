package com.civitas.cred.beans;

import java.util.ArrayList;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.Linea;

public class BeanResumenComportamientoGeneral {
	
	private double deudaTotal;
	private double deudaExigible;
	private double montoValorCartera;
	private double porcMonto;
	private double porcCantidad;
	private double porcDeuTot;
	private double total;
	private String linea;
	private String comportamientoPago;
	private String clasificacionMora;
	private int cantidad;
	
	
	public List<BeanResumenComportamientoGeneral> armarReporteResumenComportamiento(
			List<BeanResumenComportamiento> creditosBean, List<Long> lineasSeleccionadas) {
		
		if(creditosBean != null && creditosBean.size() > 0){
			double deudaTotalSNMoroso = 0, deudaExigibleSNMoroso = 0, deudaTotalSNPremora = 0, deudaExigibleSNPremora = 0, deudaTotalSNVtoFuturo = 0, deudaExigibleSNVtoFuturo = 0, monValCarSNMoroso = 0, monValCarSNPremora = 0, monValCarSNVtoFuturo = 0;
			double deudaTotalICAMoroso = 0, deudaExigibleICAMoroso = 0, monValCarICAMoroso = 0;
			double deudaTotalPAMoroso = 0, deudaExigiblePAMoroso = 0, monValCarPAMoroso = 0;
			double deudaTotalARAMoroso = 0, deudaExigibleARAMoroso = 0, monValCarARAMoroso = 0;
			double deudaTotalGral = 0, deudaExigibleGral = 0, monValCarGral = 0;
			int countSNMoroso = 0, countSNPremora = 0, countSNVtoFuturo = 0;
			int countICA = 0, countPA = 0, countARCA = 0, countGral = 0;
			boolean haySN = false, hayICA = false, hayPA = false, hayARCA = false;
			for (BeanResumenComportamiento beanResumenComportamiento : creditosBean) {
				if(beanResumenComportamiento.getComportamientoPago().equalsIgnoreCase("SITUACI�N NORMAL")){
					if(beanResumenComportamiento.getClasificacionMora().equals("Moroso")){
						deudaTotalSNMoroso += beanResumenComportamiento.getDeudaTotal();
						deudaExigibleSNMoroso += beanResumenComportamiento.getDeudaExigible();
						monValCarSNMoroso += beanResumenComportamiento.getMontoValorCartera();
						countSNMoroso += 1;
					}else if (beanResumenComportamiento.getClasificacionMora().equals("Premora")){
						deudaTotalSNPremora += beanResumenComportamiento.getDeudaTotal();
						deudaExigibleSNPremora += beanResumenComportamiento.getDeudaExigible();
						monValCarSNPremora += beanResumenComportamiento.getMontoValorCartera();
						countSNPremora += 1;
					}else{
						deudaTotalSNVtoFuturo += beanResumenComportamiento.getDeudaTotal();
						deudaExigibleSNVtoFuturo += beanResumenComportamiento.getDeudaExigible();
						monValCarSNVtoFuturo += beanResumenComportamiento.getMontoValorCartera();
						countSNVtoFuturo += 1;
					}
				}else if(beanResumenComportamiento.getComportamientoPago().equalsIgnoreCase("INADECUADO CON ATRASO")){
					if(beanResumenComportamiento.getClasificacionMora().equals("Moroso")){
						deudaTotalICAMoroso += beanResumenComportamiento.getDeudaTotal();
						deudaExigibleICAMoroso += beanResumenComportamiento.getDeudaExigible();
						monValCarICAMoroso += beanResumenComportamiento.getMontoValorCartera();
						countICA += 1;
					}
				}else if(beanResumenComportamiento.getComportamientoPago().equalsIgnoreCase("CON PROBLEMAS ATRASO")){
					if(beanResumenComportamiento.getClasificacionMora().equals("Moroso")){
						deudaTotalPAMoroso += beanResumenComportamiento.getDeudaTotal();
						deudaExigiblePAMoroso += beanResumenComportamiento.getDeudaExigible();
						monValCarPAMoroso += beanResumenComportamiento.getMontoValorCartera();
						countPA += 1;
					}
				}else{
					if(beanResumenComportamiento.getClasificacionMora().equals("Moroso")){
						deudaTotalARAMoroso += beanResumenComportamiento.getDeudaTotal();
						deudaExigibleARAMoroso += beanResumenComportamiento.getDeudaExigible();
						monValCarARAMoroso += beanResumenComportamiento.getMontoValorCartera();
						countARCA += 1;
					}
				}
			}
			
			if(!lineasSeleccionadas.isEmpty()) {
				BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
				Linea l = (Linea) bp.getById(Linea.class, lineasSeleccionadas.get(0));
				linea = l != null ? l.getNombre() : "";
			}else{
				linea = "";
			}
			
			List<BeanResumenComportamientoGeneral> list =  new ArrayList<BeanResumenComportamientoGeneral>();
			
			if(deudaTotalSNMoroso>0){
				BeanResumenComportamientoGeneral beanSitNorMor = new BeanResumenComportamientoGeneral();
				beanSitNorMor.setComportamientoPago("SITUACI�N NORMAL");
				beanSitNorMor.setLinea(linea);
				beanSitNorMor.setClasificacionMora("Moroso");
				beanSitNorMor.setDeudaTotal(deudaTotalSNMoroso);
				beanSitNorMor.setDeudaExigible(deudaExigibleSNMoroso);
				beanSitNorMor.setMontoValorCartera(monValCarSNMoroso);
				beanSitNorMor.setTotal(deudaExigibleSNMoroso-monValCarSNMoroso);
				beanSitNorMor.setCantidad(countSNMoroso);	
				
				list.add(beanSitNorMor);
				haySN = true;
			
			}
			
			if(deudaTotalSNPremora>0){
				BeanResumenComportamientoGeneral beanSitNorPM = new BeanResumenComportamientoGeneral();
				beanSitNorPM.setComportamientoPago("SITUACI�N NORMAL");
				beanSitNorPM.setLinea(linea);
				beanSitNorPM.setClasificacionMora("Premora");
				beanSitNorPM.setDeudaTotal(deudaTotalSNPremora);
				beanSitNorPM.setDeudaExigible(deudaExigibleSNPremora);
				beanSitNorPM.setMontoValorCartera(monValCarSNPremora);
				beanSitNorPM.setTotal(deudaExigibleSNPremora-monValCarSNPremora);
				beanSitNorPM.setCantidad(countSNPremora);
				
				list.add(beanSitNorPM);
				haySN = true;

			}
			
			if(deudaTotalSNVtoFuturo>0){
				BeanResumenComportamientoGeneral beanSitNorVF = new BeanResumenComportamientoGeneral();
				beanSitNorVF.setComportamientoPago("SITUACI�N NORMAL");
				beanSitNorVF.setLinea(linea);
				beanSitNorVF.setClasificacionMora("Vto Futuro");
				beanSitNorVF.setDeudaTotal(deudaTotalSNVtoFuturo);
				beanSitNorVF.setDeudaExigible(deudaExigibleSNVtoFuturo);
				beanSitNorVF.setMontoValorCartera(monValCarSNVtoFuturo);
				beanSitNorVF.setTotal(deudaExigibleSNVtoFuturo-monValCarSNVtoFuturo);
				beanSitNorVF.setCantidad(countSNVtoFuturo);
	
				
				list.add(beanSitNorVF);
				haySN = true;

			}
			
			if(haySN){
				BeanResumenComportamientoGeneral beanTotalSN = new BeanResumenComportamientoGeneral();
				beanTotalSN.setComportamientoPago("Total SITUACI�N NORMAL");
				beanTotalSN.setDeudaTotal(deudaTotalSNMoroso+deudaTotalSNPremora+deudaTotalSNVtoFuturo);
				beanTotalSN.setDeudaExigible(deudaExigibleSNMoroso+deudaExigibleSNPremora+deudaExigibleSNVtoFuturo);
				beanTotalSN.setMontoValorCartera(monValCarSNMoroso+monValCarSNPremora+monValCarSNVtoFuturo);
				beanTotalSN.setTotal(beanTotalSN.getDeudaExigible()-beanTotalSN.getMontoValorCartera());
				beanTotalSN.setCantidad(countSNMoroso+countSNPremora+countSNVtoFuturo);
				
				
				list.add(beanTotalSN);
				deudaTotalGral += beanTotalSN.getDeudaTotal();
				deudaExigibleGral += beanTotalSN.getDeudaExigible();
				monValCarGral += beanTotalSN.getMontoValorCartera();
				countGral += beanTotalSN.getCantidad();
			}
			
			if(deudaTotalICAMoroso>0){
				BeanResumenComportamientoGeneral beanInaConAtrMor = new BeanResumenComportamientoGeneral();
				beanInaConAtrMor.setComportamientoPago("INADECUADO CON ATRASO");
				beanInaConAtrMor.setLinea(linea);
				beanInaConAtrMor.setClasificacionMora("Moroso");
				beanInaConAtrMor.setDeudaTotal(deudaTotalICAMoroso);
				beanInaConAtrMor.setDeudaExigible(deudaExigibleICAMoroso);
				beanInaConAtrMor.setMontoValorCartera(monValCarICAMoroso);
				beanInaConAtrMor.setTotal(deudaExigibleICAMoroso-monValCarICAMoroso);
				beanInaConAtrMor.setCantidad(countICA);
				
				
				list.add(beanInaConAtrMor);
				hayICA = true;

			}
			
			if(hayICA){
				BeanResumenComportamientoGeneral beanTotalICA = new BeanResumenComportamientoGeneral();
				beanTotalICA.setComportamientoPago("Total INADECUADO CON ATRASO");
				beanTotalICA.setDeudaTotal(deudaTotalICAMoroso);
				beanTotalICA.setDeudaExigible(deudaExigibleICAMoroso);
				beanTotalICA.setMontoValorCartera(monValCarICAMoroso);
				beanTotalICA.setTotal(deudaExigibleICAMoroso-monValCarICAMoroso);
				beanTotalICA.setCantidad(countICA);
			
				
				list.add(beanTotalICA);
				deudaTotalGral += beanTotalICA.getDeudaTotal();
				deudaExigibleGral += beanTotalICA.getDeudaExigible();
				monValCarGral += beanTotalICA.getMontoValorCartera();
				countGral += beanTotalICA.getCantidad();
			}
			
			if(deudaTotalPAMoroso>0){
				BeanResumenComportamientoGeneral beanProAtrMor = new BeanResumenComportamientoGeneral();
				beanProAtrMor.setComportamientoPago("CON PROBLEMAS ATRASO");
				beanProAtrMor.setLinea(linea);
				beanProAtrMor.setClasificacionMora("Moroso");
				beanProAtrMor.setDeudaTotal(deudaTotalPAMoroso);
				beanProAtrMor.setDeudaExigible(deudaExigiblePAMoroso);
				beanProAtrMor.setMontoValorCartera(monValCarPAMoroso);
				beanProAtrMor.setTotal(deudaExigiblePAMoroso-monValCarPAMoroso);
				beanProAtrMor.setCantidad(countPA);
				
				
				list.add(beanProAtrMor);
				hayPA = true;

			}
			
			if(hayPA){
				BeanResumenComportamientoGeneral beanTotalPA = new BeanResumenComportamientoGeneral();
				beanTotalPA.setComportamientoPago("Total CON PROBLEMAS ATRASO");
				beanTotalPA.setDeudaTotal(deudaTotalPAMoroso);
				beanTotalPA.setDeudaExigible(deudaExigiblePAMoroso);
				beanTotalPA.setMontoValorCartera(monValCarPAMoroso);
				beanTotalPA.setTotal(deudaExigiblePAMoroso-monValCarPAMoroso);
				beanTotalPA.setCantidad(countPA);
								
				list.add(beanTotalPA);
				deudaTotalGral += beanTotalPA.getDeudaTotal();
				deudaExigibleGral += beanTotalPA.getDeudaExigible();
				monValCarGral += beanTotalPA.getMontoValorCartera();
				countGral += beanTotalPA.getCantidad();
			}
			
			if(deudaTotalARAMoroso>0){
				BeanResumenComportamientoGeneral beanAltRieConAtr = new BeanResumenComportamientoGeneral();
				beanAltRieConAtr.setComportamientoPago("ALTO RIESGO CON ATRASO");
				beanAltRieConAtr.setLinea(linea);
				beanAltRieConAtr.setClasificacionMora("Moroso");
				beanAltRieConAtr.setDeudaTotal(deudaTotalARAMoroso);
				beanAltRieConAtr.setDeudaExigible(deudaExigibleARAMoroso);
				beanAltRieConAtr.setMontoValorCartera(monValCarARAMoroso);
				beanAltRieConAtr.setTotal(deudaExigibleARAMoroso-monValCarARAMoroso);
				beanAltRieConAtr.setCantidad(countARCA);
		
				
				list.add(beanAltRieConAtr);
				hayARCA = true;

			}
			
			if(hayARCA){
				BeanResumenComportamientoGeneral beanTotalARCA = new BeanResumenComportamientoGeneral();
				beanTotalARCA.setComportamientoPago("Total ALTO RIESGO CON ATRASO");
				beanTotalARCA.setDeudaTotal(deudaTotalARAMoroso);
				beanTotalARCA.setDeudaExigible(deudaExigibleARAMoroso);
				beanTotalARCA.setMontoValorCartera(monValCarARAMoroso);
				beanTotalARCA.setTotal(deudaExigibleARAMoroso-monValCarARAMoroso);
				beanTotalARCA.setCantidad(countARCA);
				
				
				list.add(beanTotalARCA);
				deudaTotalGral += beanTotalARCA.getDeudaTotal();
				deudaExigibleGral += beanTotalARCA.getDeudaExigible();
				monValCarGral += beanTotalARCA.getMontoValorCartera();
				countGral += beanTotalARCA.getCantidad();
			}
			
			BeanResumenComportamientoGeneral beanTotalGral = new BeanResumenComportamientoGeneral();
			beanTotalGral.setComportamientoPago("Total General");
			beanTotalGral.setDeudaTotal(deudaTotalGral);
			beanTotalGral.setDeudaExigible(deudaExigibleGral);
			beanTotalGral.setMontoValorCartera(monValCarGral);
			beanTotalGral.setTotal(deudaExigibleGral-monValCarGral);
			beanTotalGral.setCantidad(countGral);
						
			list.add(beanTotalGral);
			
			for (BeanResumenComportamientoGeneral b : list) {
				b.setPorcMonto(b.getDeudaExigible()*100/deudaExigibleGral);
				b.setPorcCantidad((double) b.getCantidad()*100/countGral);
				b.setPorcDeuTot(b.getDeudaTotal()*100/deudaTotalGral);
			}
			
			return list;
		}else{
			return null;
		}
		
	}
	

	public double getDeudaTotal() {
		return deudaTotal;
	}
	public void setDeudaTotal(double deudaTotal) {
		this.deudaTotal = deudaTotal;
	}
	public double getDeudaExigible() {
		return deudaExigible;
	}
	public void setDeudaExigible(double deudaExigible) {
		this.deudaExigible = deudaExigible;
	}
	public double getMontoValorCartera() {
		return montoValorCartera;
	}
	public void setMontoValorCartera(double montoValorCartera) {
		this.montoValorCartera = montoValorCartera;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getComportamientoPago() {
		return comportamientoPago;
	}
	public void setComportamientoPago(String comportamientoPago) {
		this.comportamientoPago = comportamientoPago;
	}
	public String getClasificacionMora() {
		return clasificacionMora;
	}
	public void setClasificacionMora(String clasificacionMora) {
		this.clasificacionMora = clasificacionMora;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}	
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}

	public double getPorcMonto() {
		return porcMonto;
	}

	public double getPorcCantidad() {
		return porcCantidad;
	}

	public double getPorcDeuTot() {
		return porcDeuTot;
	}


	public void setPorcMonto(double porcMonto) {
		this.porcMonto = porcMonto;
	}


	public void setPorcCantidad(double porcCantidad) {
		this.porcCantidad = porcCantidad;
	}


	public void setPorcDeuTot(double porcDeuTot) {
		this.porcDeuTot = porcDeuTot;
	}
}
