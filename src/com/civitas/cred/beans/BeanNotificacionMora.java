package com.civitas.cred.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.asf.util.DateHelper;

public class BeanNotificacionMora implements Serializable {
	private static final long serialVersionUID = 6829207888370855990L;

	private String comportamientoPago;
	private String[] idsLinea;
	private Long idPersona;
	private Long numeroAtencionDesde;
	private Long numeroAtencionHasta;
	private Date fechaMora;
	private Long idTipoGarantia;
	private String identificadorUnico;
	private ArrayList<BeanNotificacionMoraDetalle> beanNotificacionMoraDetalleList;

	public BeanNotificacionMora() {
	}

	public BeanNotificacionMora(String comportamientoPago, String[] idsLinea, Long idPersona, Long numeroAtencionDesde,
			Long numeroAtencionHasta, Date fechaMora, Long idTipoGarantia, String identificadorUnico,
			ArrayList<BeanNotificacionMoraDetalle> beanNotificacionMoraDetalleList) {
		this.comportamientoPago = comportamientoPago;
		this.idsLinea = idsLinea;
		this.idPersona = idPersona;
		this.numeroAtencionDesde = numeroAtencionDesde;
		this.numeroAtencionHasta = numeroAtencionHasta;
		this.fechaMora = fechaMora;
		this.idTipoGarantia = idTipoGarantia;
		this.identificadorUnico = identificadorUnico;
		this.beanNotificacionMoraDetalleList = beanNotificacionMoraDetalleList;
	}

	public String getComportamientoPago() {
		return comportamientoPago;
	}

	public void setComportamientoPago(String comportamientoPago) {
		this.comportamientoPago = comportamientoPago;
	}

	public String[] getIdsLinea() {
		return idsLinea;
	}

	public void setIdsLinea(String[] idsLinea) {
		this.idsLinea = idsLinea;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getNumeroAtencionDesde() {
		return numeroAtencionDesde;
	}

	public void setNumeroAtencionDesde(Long numeroAtencionDesde) {
		this.numeroAtencionDesde = numeroAtencionDesde;
	}

	public Long getNumeroAtencionHasta() {
		return numeroAtencionHasta;
	}

	public void setNumeroAtencionHasta(Long numeroAtencionHasta) {
		this.numeroAtencionHasta = numeroAtencionHasta;
	}

	public String getFechaMora() {
		return DateHelper.getString(fechaMora);
	}

	public void setFechaMora(String fechaMora) {
		this.fechaMora = DateHelper.getDate(fechaMora);
	}

	public Long getIdTipoGarantia() {
		return idTipoGarantia;
	}

	public void setIdTipoGarantia(Long idTipoGarantia) {
		this.idTipoGarantia = idTipoGarantia;
	}

	public String getIdentificadorUnico() {
		return identificadorUnico;
	}

	public void setIdentificadorUnico(String identificadorUnico) {
		this.identificadorUnico = identificadorUnico;
	}

	public ArrayList<BeanNotificacionMoraDetalle> getBeanNotificacionMoraDetalleList() {
		return beanNotificacionMoraDetalleList;
	}

	public void setBeanNotificacionMoraDetalleList(
			ArrayList<BeanNotificacionMoraDetalle> beanNotificacionMoraDetalleList) {
		this.beanNotificacionMoraDetalleList = beanNotificacionMoraDetalleList;
	}

}
