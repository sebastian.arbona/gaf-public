package com.civitas.cred.beans;

import java.util.Date;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Concepto;

public class BeanComportamientoLineas {
	private com.nirven.creditos.hibernate.Objetoi credito;

	private double deudaTotal;
	private double deudaExigible;
	private double montoValorCartera;
	private double montoDesembolsado;
	private double totalRecaudado;
	private double capitalRecaudado;
	private double compensatorioRecaudado;
	private double moratorioRecaudado;
	private double punitorioRecaudado;
	private double gastoRecaudado;
	private String linea;
	private String comportamientoPago;
	private int anioFirmaContrato;
	private Date fecha;

	public BeanComportamientoLineas(Date fechaHasta) {
		if (fechaHasta != null) {
			this.fecha = fechaHasta;
		} else {
			this.fecha = new Date();
		}

	}

	public com.nirven.creditos.hibernate.Objetoi getCredito() {
		return credito;
	}

	public void setCredito(com.nirven.creditos.hibernate.Objetoi credito) {
		this.credito = credito;
		CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(credito.getId(), new Date());
		ccd.calcular();
		deudaTotal = ccd.getDeudaTotal();
		deudaExigible = ccd.getDeudaVencidaFechaEstricta();
		montoValorCartera = credito.getMontoValoresCartera();
		linea = credito.getLinea().getNombre();
		comportamientoPago = credito.getComportamientoActual();
		anioFirmaContrato = credito.getFechaFirmaContrato() != null
				? DateHelper.getAnio(credito.getFechaFirmaContrato())
				: 0;
		montoDesembolsado = credito.getMontoDesembolsado();
		capitalRecaudado = credito.getTotalConceptoRecaudado(fecha, Concepto.CONCEPTO_CAPITAL);
		compensatorioRecaudado = credito.getTotalConceptoRecaudado(fecha, Concepto.CONCEPTO_COMPENSATORIO);
		moratorioRecaudado = credito.getTotalConceptoRecaudado(fecha, Concepto.CONCEPTO_MORATORIO);
		punitorioRecaudado = credito.getTotalConceptoRecaudado(fecha, Concepto.CONCEPTO_PUNITORIO);
		gastoRecaudado = credito.getTotalConceptoRecaudado(fecha, Concepto.CONCEPTO_GASTOS)
				+ credito.getTotalConceptoRecaudado(new Date(), Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
		totalRecaudado = capitalRecaudado + compensatorioRecaudado + moratorioRecaudado + punitorioRecaudado
				+ gastoRecaudado;
	}

	public double getDeudaTotal() {
		return deudaTotal;
	}

	public void setDeudaTotal(double deudaTotal) {
		this.deudaTotal = deudaTotal;
	}

	public double getDeudaExigible() {
		return deudaExigible;
	}

	public void setDeudaExigible(double deudaExigible) {
		this.deudaExigible = deudaExigible;
	}

	public double getMontoValorCartera() {
		return montoValorCartera;
	}

	public void setMontoValorCartera(double montoValorCartera) {
		this.montoValorCartera = montoValorCartera;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getComportamientoPago() {
		return comportamientoPago;
	}

	public void setComportamientoPago(String comportamientoPago) {
		this.comportamientoPago = comportamientoPago;
	}

	public int getAnioFirmaContrato() {
		return anioFirmaContrato;
	}

	public void setAnioFirmaContrato(int anioFirmaContrato) {
		this.anioFirmaContrato = anioFirmaContrato;
	}

	public double getMontoDesembolsado() {
		return montoDesembolsado;
	}

	public void setMontoDesembolsado(double montoDesembolsado) {
		this.montoDesembolsado = montoDesembolsado;
	}

	public double getTotalRecaudado() {
		return totalRecaudado;
	}

	public void setTotalRecaudado(double totalRecaudado) {
		this.totalRecaudado = totalRecaudado;
	}

	public double getCapitalRecaudado() {
		return capitalRecaudado;
	}

	public void setCapitalRecaudado(double capitalRecaudado) {
		this.capitalRecaudado = capitalRecaudado;
	}

	public double getCompensatorioRecaudado() {
		return compensatorioRecaudado;
	}

	public void setCompensatorioRecaudado(double compensatorioRecaudado) {
		this.compensatorioRecaudado = compensatorioRecaudado;
	}

	public double getMoratorioRecaudado() {
		return moratorioRecaudado;
	}

	public void setMoratorioRecaudado(double moratorioRecaudado) {
		this.moratorioRecaudado = moratorioRecaudado;
	}

	public double getPunitorioRecaudado() {
		return punitorioRecaudado;
	}

	public void setPunitorioRecaudado(double punitorioRecaudado) {
		this.punitorioRecaudado = punitorioRecaudado;
	}

	public double getGastoRecaudado() {
		return gastoRecaudado;
	}

	public void setGastoRecaudado(double gastoRecaudado) {
		this.gastoRecaudado = gastoRecaudado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
