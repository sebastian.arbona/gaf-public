package com.civitas.cred.metodos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCuota;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DesembolsoBonTasa;

public class MetodoCalculoFrances {

	private ArrayList<Date> feriados;
	private int correr;
	private BusinessPersistance bp;

	public MetodoCalculoFrances() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	public List<Cuota> amortizar(List<Desembolso> desembolsos, Double tasaAmortizacion, Integer totalCuotas,
			Double saldoCapital, Integer periodicidad, Integer periodoGracia) {
		return null;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BeanCuota> amortizarBonTasaBice(List<DesembolsoBonTasa> desembolsos, Double tasa,
			Double bonificacion, Double plazo, int contar, int correr, Date primerVencimiento, Double perioricidad) {
//    	tasa = 0.09;
//    	bonificacion = 0.03;
		feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");
		this.correr = correr;
		ArrayList<BeanCuota> cuotasTasaCompensatorio = calcularCuotasBice(desembolsos, tasa, plazo, contar, correr,
				primerVencimiento, perioricidad);
		ArrayList<BeanCuota> cuotasTasaCompensatorioMasBonificacion = calcularCuotasBice(desembolsos,
				tasa + bonificacion, plazo, contar, correr, primerVencimiento, perioricidad);
		BeanCuota cuotaCompensatorioMasBonificacion;
		Double subsidio;
		for (BeanCuota cuotaCompensatorio : cuotasTasaCompensatorio) {
			cuotaCompensatorioMasBonificacion = cuotasTasaCompensatorioMasBonificacion
					.get(cuotaCompensatorio.getNumero() - 1);
			subsidio = cuotaCompensatorioMasBonificacion.getInteres() - cuotaCompensatorio.getInteres();
			cuotaCompensatorio.setSubsidio(subsidio);
		}
		return cuotasTasaCompensatorio;
	}

	private ArrayList<BeanCuota> calcularCuotasBice(List<DesembolsoBonTasa> desembolsos, Double tasa, Double plazo,
			int contar, int correr, Date primerVencimiento, Double perioricidad) {
		Double capital;
		Double cuotaPura;
		Double saldoCapital;
		Double capitalCuota;
		Double interes;
		Integer numero;
		Double periodos;
		Calendar vencimiento = Calendar.getInstance();
		vencimiento.setTime(primerVencimiento);
		Calendar vencimientoAnterior;
		ArrayList<BeanCuota> beanCuotas = new ArrayList<BeanCuota>();
		BeanCuota beanCuota;
		for (DesembolsoBonTasa desembolso : desembolsos) {
			vencimientoAnterior = Calendar.getInstance();
			vencimientoAnterior.setTime(desembolso.getFecha());
			vencimientoAnterior.set(Calendar.HOUR, 0);
			vencimientoAnterior.set(Calendar.MINUTE, 0);
			vencimientoAnterior.set(Calendar.SECOND, 0);
			vencimientoAnterior.set(Calendar.MILLISECOND, 0);
			capital = desembolso.getImporte();
			saldoCapital = capital;
			beanCuotas = new ArrayList<BeanCuota>();
			numero = 1;
			periodos = 30.0 * perioricidad;
			while (numero.intValue() <= plazo.intValue()) {
				beanCuota = new BeanCuota();
				beanCuota.setFechaVencimiento(determinarVencimiento(vencimiento));
				if (contar == 1) {
					periodos = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
				}
				cuotaPura = (capital * ((Math.pow(1.0 + tasa / 365.0 * periodos, plazo) * tasa / 365.0 * periodos)))
						/ (Math.pow(1.0 + tasa / 365.0 * periodos, plazo) - 1.0);
				interes = saldoCapital * tasa * periodos / 365.0;
				capitalCuota = cuotaPura - interes;
				saldoCapital -= capitalCuota;
				beanCuota.setSaldoCapital(saldoCapital);
				beanCuota.setCapital(capitalCuota);
				beanCuota.setInteres(interes);
				beanCuota.setNumero(numero);
				beanCuotas.add(beanCuota);
				numero++;
				vencimiento.add(Calendar.MONTH, perioricidad.intValue());
				vencimientoAnterior.setTime(beanCuota.getFechaVencimiento());
			}
		}
		return beanCuotas;
	}

	private Date determinarVencimiento(Calendar vencimiento) {
		Calendar auxiliar = Calendar.getInstance();
		auxiliar.setTime(vencimiento.getTime());
		if (correr == 1) {
			return DateHelper.getDiaHabil(feriados, auxiliar.getTime());
		} else {
			Date diaHabil = DateHelper.getDiaHabil(feriados, auxiliar.getTime());
			while (diaHabil.after(auxiliar.getTime())) {
				auxiliar.add(Calendar.DATE, -1);
				diaHabil = DateHelper.getDiaHabil(feriados, auxiliar.getTime());
			}
			return auxiliar.getTime();
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BeanCuota> amortizarBonTasaCredicoop(List<DesembolsoBonTasa> desembolsos, Double tasa,
			Double bonificacion, Double plazo, int contar, int correr, Date primerVencimiento, Double perioricidad) {
//    	tasa = 0.0925;
//    	bonificacion = 0.03;
		feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");
		this.correr = correr;
		Double capital;
		Double i;
		Double periodos;
		Double periodoFijo;
		Integer numero;
		Integer cuotasRestantes;
		Double saldoCapital;
		Double cuotaPura;
		Double interes;
		Double iSubsidio;
		Double impSubsidio;
		Calendar vencimiento = Calendar.getInstance();
		vencimiento.setTime(primerVencimiento);
		Calendar vencimientoAnterior;
		ArrayList<BeanCuota> beanCuotas = new ArrayList<BeanCuota>();
		BeanCuota beanCuota;
		for (DesembolsoBonTasa desembolso : desembolsos) {
			vencimientoAnterior = Calendar.getInstance();
			vencimientoAnterior.setTime(desembolso.getFecha());
			vencimientoAnterior.set(Calendar.HOUR, 0);
			vencimientoAnterior.set(Calendar.MINUTE, 0);
			vencimientoAnterior.set(Calendar.SECOND, 0);
			vencimientoAnterior.set(Calendar.MILLISECOND, 0);
			saldoCapital = desembolso.getImporte();
			beanCuotas = new ArrayList<BeanCuota>();
			numero = 1;
			periodos = 30.0 * perioricidad;
			periodoFijo = 30.0 * perioricidad;
			cuotasRestantes = plazo.intValue();
			// tasa = new Double(0.15);
			while (numero.intValue() <= plazo.intValue()) {
				beanCuota = new BeanCuota();
				beanCuota.setFechaVencimiento(determinarVencimiento(vencimiento));

				/*
				 * periodos = new
				 * Double(CreditoCalculoDeuda.getDiasEntreFechas(vencimientoAnterior.getTime(),
				 * beanCuota.getFechaVencimiento())); periodoFijo = new
				 * Double(CreditoCalculoDeuda.getDiasEntreFechas(vencimientoAnterior.getTime(),
				 * beanCuota.getFechaVencimiento()));
				 */

				if (contar == 0) {
					periodos = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
					periodoFijo = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
				}

				i = Math.pow(1 + tasa * periodoFijo / 365, periodos / periodoFijo) - 1;
				cuotaPura = saldoCapital * i * Math.pow(1 + i, cuotasRestantes)
						/ (-1 + Math.pow(1 + i, cuotasRestantes));

				interes = saldoCapital * i;
				capital = cuotaPura - interes;
				iSubsidio = Math.pow(1 + bonificacion * periodoFijo / 365, periodos / periodoFijo) - 1;
				impSubsidio = saldoCapital * iSubsidio;
				beanCuota.setSaldoCapital(saldoCapital);
				beanCuota.setCapital(capital);
				beanCuota.setInteres(interes);
				beanCuota.setNumero(numero);
				beanCuota.setSubsidio(impSubsidio);
				beanCuotas.add(beanCuota);
				numero++;
				vencimiento.add(Calendar.MONTH, perioricidad.intValue());
				vencimientoAnterior.setTime(beanCuota.getFechaVencimiento());
				cuotasRestantes--;
				saldoCapital -= capital;
			}
		}
		return beanCuotas;
	}

	public ArrayList<BeanCuota> amortizarBonTasaFB(List<DesembolsoBonTasa> desembolsos, Double tasa,
			Double bonificacion, Double plazo, int contar, int correr, Date primerVencimiento, Double perioricidad) {
		this.correr = correr;
		Double capital;
		Double i;
		Double iCapital;
		Double periodos;
		Double periodoFijo;
		Integer numero;
		Integer cuotasRestantes;
		Double saldoCapital;
		Double cuotaPura;
		Double interes;
		Double iSubsidio;
		Double impSubsidio;
		Calendar vencimiento = Calendar.getInstance();
		vencimiento.setTime(primerVencimiento);
		Calendar vencimientoAnterior;
		boolean primeraCuota = true;
		ArrayList<BeanCuota> beanCuotas = new ArrayList<BeanCuota>();
		BeanCuota beanCuota;
		for (DesembolsoBonTasa desembolso : desembolsos) {
			vencimientoAnterior = Calendar.getInstance();
			vencimientoAnterior.setTime(desembolso.getFecha());
			vencimientoAnterior.set(Calendar.HOUR, 0);
			vencimientoAnterior.set(Calendar.MINUTE, 0);
			vencimientoAnterior.set(Calendar.SECOND, 0);
			vencimientoAnterior.set(Calendar.MILLISECOND, 0);
			saldoCapital = desembolso.getImporte();
			beanCuotas = new ArrayList<BeanCuota>();
			numero = 1;
			periodos = 30.0 * perioricidad;
			periodoFijo = 30.0 * perioricidad;
			cuotasRestantes = plazo.intValue();
			// tasa = new Double(0.15);
			while (numero.intValue() <= plazo.intValue()) {
				beanCuota = new BeanCuota();
				beanCuota.setFechaVencimiento(vencimiento.getTime());

				/*
				 * periodos = new
				 * Double(CreditoCalculoDeuda.getDiasEntreFechas(vencimientoAnterior.getTime(),
				 * beanCuota.getFechaVencimiento())); periodoFijo = new
				 * Double(CreditoCalculoDeuda.getDiasEntreFechas(vencimientoAnterior.getTime(),
				 * beanCuota.getFechaVencimiento()));
				 */

				if (primeraCuota) {
					periodos = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
					periodoFijo = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
					primeraCuota = false;
					i = Math.pow(1 + tasa * periodoFijo / 365, periodos / periodoFijo) - 1;
					iCapital = Math.pow(1 + tasa * (30.0 * perioricidad) / 365,
							(30.0 * perioricidad) / (30.0 * perioricidad)) - 1;
					cuotaPura = saldoCapital * iCapital * Math.pow(1 + iCapital, cuotasRestantes)
							/ (-1 + Math.pow(1 + iCapital, cuotasRestantes));

				} else {
					periodos = 30.0 * perioricidad;
					periodoFijo = 30.0 * perioricidad;
					iCapital = i = Math.pow(1 + tasa * periodoFijo / 365, periodos / periodoFijo) - 1;
					cuotaPura = saldoCapital * i * Math.pow(1 + i, cuotasRestantes)
							/ (-1 + Math.pow(1 + i, cuotasRestantes));
				}

				interes = saldoCapital * i;
				capital = cuotaPura - interes;
				iSubsidio = Math.pow(1 + bonificacion * periodoFijo / 365, periodos / periodoFijo) - 1;
				impSubsidio = saldoCapital * iSubsidio;
				beanCuota.setSaldoCapital(saldoCapital);
				beanCuota.setCapital(capital);
				beanCuota.setInteres(interes);
				beanCuota.setNumero(numero);
				beanCuota.setSubsidio(impSubsidio);
				beanCuotas.add(beanCuota);
				numero++;
				vencimiento.add(Calendar.MONTH, perioricidad.intValue());
				vencimientoAnterior.setTime(beanCuota.getFechaVencimiento());
				cuotasRestantes--;
				saldoCapital -= capital;
			}
		}
		return beanCuotas;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BeanCuota> amortizarBonTasaBICE(List<DesembolsoBonTasa> desembolsos, Double tasa,
			Double bonificacion, Double plazo, int contar, int correr, Date primerVencimiento, Double perioricidad) {
//    	tasa = 0.0925;
//    	bonificacion = 0.03;
		feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");
		this.correr = correr;
		Double capital;
		Double i;
		Double periodos;
		Double periodoFijo;
		Integer numero;
		Integer cuotasRestantes;
		Double saldoCapital;
		Double cuotaPura;
		Double interes;
		Double iSubsidio;
		Double impSubsidio;
		Calendar vencimiento = Calendar.getInstance();
		vencimiento.setTime(primerVencimiento);
		Calendar vencimientoAnterior;
		ArrayList<BeanCuota> beanCuotas = new ArrayList<BeanCuota>();
		BeanCuota beanCuota;
		for (DesembolsoBonTasa desembolso : desembolsos) {
			vencimientoAnterior = Calendar.getInstance();
			vencimientoAnterior.setTime(desembolso.getFecha());
			vencimientoAnterior.set(Calendar.HOUR, 0);
			vencimientoAnterior.set(Calendar.MINUTE, 0);
			vencimientoAnterior.set(Calendar.SECOND, 0);
			vencimientoAnterior.set(Calendar.MILLISECOND, 0);
			saldoCapital = desembolso.getImporte();
			beanCuotas = new ArrayList<BeanCuota>();
			numero = 1;
			periodos = 30.0 * perioricidad;
			periodoFijo = 30.0 * perioricidad;
			cuotasRestantes = plazo.intValue();
			// tasa = new Double(0.15);
			while (numero.intValue() <= plazo.intValue()) {
				beanCuota = new BeanCuota();
				beanCuota.setFechaVencimiento(determinarVencimiento(vencimiento));
				// CristianAnton
				if (contar == 0) {
					periodos = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
					periodoFijo = new Double(
							DateHelper.getDiffDates(vencimientoAnterior.getTime(), beanCuota.getFechaVencimiento(), 2));
				}

				i = Math.pow(1 + tasa * periodoFijo / 365, periodos / periodoFijo) - 1;
				cuotaPura = saldoCapital * i * Math.pow(1 + i, cuotasRestantes)
						/ (-1 + Math.pow(1 + i, cuotasRestantes));

				interes = saldoCapital * i;
				capital = cuotaPura - interes;
				iSubsidio = Math.pow(1 + bonificacion * periodoFijo / 365, periodos / periodoFijo) - 1;
				impSubsidio = saldoCapital * iSubsidio;
				beanCuota.setSaldoCapital(saldoCapital);
				beanCuota.setCapital(capital);
				beanCuota.setInteres(interes);
				beanCuota.setNumero(numero);
				beanCuota.setSubsidio(impSubsidio);
				beanCuotas.add(beanCuota);
				numero++;
				vencimiento.add(Calendar.MONTH, perioricidad.intValue());
				vencimientoAnterior.setTime(beanCuota.getFechaVencimiento());
				cuotasRestantes--;
				saldoCapital -= capital;
			}
		}
		return beanCuotas;
	}

}
