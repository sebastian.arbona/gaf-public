package com.civitas.cred.metodos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DoubleHelper;
import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.cred.beans.BeanDesembolso;

public class MetodoCalculoDirecto extends MetodoCalculo {

	public MetodoCalculoDirecto() {
		cuotas = new ArrayList<BeanCuota>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<BeanCuota> amortizar(BeanCalculo beanCalculo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ArrayList<BeanDesembolso> desembolsos = beanCalculo.getDesembolsos();
		ArrayList<Date> feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");
		Calendar vencCapital = Calendar.getInstance(), vencAnterior = Calendar.getInstance();
		int cantCuotas = beanCalculo.getPlazoCapital();
		BigDecimal tasa = new BigDecimal(beanCalculo.getTasaAmortizacion() / 100d).setScale(3, RoundingMode.HALF_EVEN);
		BigDecimal capitalCuota = BigDecimal.ZERO, capitalTotal = BigDecimal.ZERO, interesCuota = BigDecimal.ZERO,
				interesTotal = BigDecimal.ZERO, saldoCapital = BigDecimal.ZERO;
		for (BeanDesembolso beanDesembolso : desembolsos) {
			capitalTotal = capitalTotal.add(new BigDecimal(beanDesembolso.getImporte())).setScale(2,
					RoundingMode.HALF_EVEN);
			vencAnterior.setTime(beanDesembolso.getFecha());
		}
		interesTotal = new BigDecimal(DoubleHelper.redondear(
				(((tasa.doubleValue() * cantCuotas) + 1) * capitalTotal.doubleValue()) - capitalTotal.doubleValue()))
						.setScale(2, RoundingMode.HALF_EVEN);
		capitalCuota = new BigDecimal(DoubleHelper.redondear(capitalTotal.doubleValue() / cantCuotas)).setScale(2,
				RoundingMode.HALF_EVEN);
		interesCuota = new BigDecimal(DoubleHelper.redondear(interesTotal.doubleValue() / cantCuotas)).setScale(2,
				RoundingMode.HALF_EVEN);
		saldoCapital = capitalTotal;
		if (beanCalculo.getPrimerVencimientoCapital() != null) {
			// ingresado por el usuario
			vencCapital.setTime(beanCalculo.getPrimerVencimientoCapital());
		}
		for (int nroCuota = 1; nroCuota <= cantCuotas; nroCuota++) {
			BeanCuota beanCuota = new BeanCuota();
			beanCuota.setNumero(nroCuota);
			beanCuota.setSaldoCapital(saldoCapital.doubleValue());
			beanCuota.setCapital((nroCuota == cantCuotas) ? saldoCapital.doubleValue() : capitalCuota.doubleValue());
			saldoCapital = new BigDecimal(
					DoubleHelper.redondear(saldoCapital.doubleValue() - beanCuota.getCapital().doubleValue()))
							.setScale(2, RoundingMode.HALF_EVEN);
			beanCuota.setCompensatorio(interesCuota.doubleValue());
			// viene desde el bean o calculado en iteracion anterior
			beanCuota.setFechaVencimiento(DateHelper.getDiaHabil(feriados, vencCapital.getTime()));
			beanCuota.setFechaVencimientoAnterior(vencAnterior.getTime());
			vencAnterior.setTime(beanCuota.getFechaVencimiento());
			vencCapital.add(Calendar.MONTH, 1);
			cuotas.add(beanCuota);
		}
		return ordenarCuotas();
	}

}
