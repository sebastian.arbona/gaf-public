package com.civitas.cred.metodos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.asf.cred.business.CalculoInteresHelper;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.asf.util.DoubleHelper;
import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.cred.beans.BeanDesembolso;

public class MetodoCalculoAleman extends MetodoCalculo {

	public MetodoCalculoAleman() {
		cuotas = new ArrayList<BeanCuota>();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BeanCuota> amortizar(BeanCalculo beanCalculo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ArrayList<BeanDesembolso> desembolsos = beanCalculo.getDesembolsos();
		ArrayList<Date> feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");
		for (BeanDesembolso desembolso : desembolsos) {
			Calendar vencCapital = Calendar.getInstance();
			Calendar primerVencCapital = Calendar.getInstance();
			primerVencCapital.setTime(beanCalculo.getPrimerVencimientoCapital());
			vencCapital.setTime(DateHelper.getDiaHabil(feriados, primerVencCapital.getTime()));

			Calendar vencCompensatorio = Calendar.getInstance();
			Calendar primerVencCompensatorio = Calendar.getInstance();
			primerVencCompensatorio.setTime(beanCalculo.getPrimerVencimientoCompensatorio());
			vencCompensatorio.setTime(DateHelper.getDiaHabil(feriados, primerVencCompensatorio.getTime()));

			Calendar segundoCapital = null, segundoCapitalOriginal = null;
			if (beanCalculo.getSegundoVencimientoCapital() != null) {
				segundoCapital = Calendar.getInstance();
				segundoCapital.setTime(DateHelper.getDiaHabil(feriados, beanCalculo.getSegundoVencimientoCapital()));
				segundoCapitalOriginal = Calendar.getInstance();
				segundoCapitalOriginal.setTime(beanCalculo.getSegundoVencimientoCapital());
			}

			Calendar segundoCompensatorio = null, segundoCompensatorioOriginal = null;
			if (beanCalculo.getSegundoVencimientoCompensatorio() != null) {
				segundoCompensatorio = Calendar.getInstance();
				segundoCompensatorio
						.setTime(DateHelper.getDiaHabil(feriados, beanCalculo.getSegundoVencimientoCompensatorio()));
				segundoCompensatorioOriginal = Calendar.getInstance();
				segundoCompensatorioOriginal.setTime(beanCalculo.getSegundoVencimientoCompensatorio());
			}

			Calendar vencAnterior = null;
			Date fechaDesembolso = desembolso.getFecha();

			Integer nroCtaCap = 0;
			Double valorCuota = 0D, saldoCapital = desembolso.getImporte();

			for (int nroCta = 1; nroCta <= beanCalculo.getPlazoCompensatorio(); nroCta++) {
				BeanCuota cuota = new BeanCuota();
				cuota.setNumero(nroCta);
				cuota.setDesembolso(desembolso.getNumero());

				if (segundoCompensatorio != null && nroCta == 2) {
					vencCompensatorio.setTime(segundoCompensatorio.getTime());
				}
				if (segundoCapital != null && nroCta == 2) {
					vencCapital.setTime(segundoCapital.getTime());
				}

				cuota.setFechaVencimiento(DateHelper.getDiaHabil(feriados, vencCompensatorio.getTime()));
				if (vencAnterior != null) {
					cuota.setFechaVencimientoAnterior(DateHelper.getDiaHabil(feriados, vencAnterior.getTime()));
				} else {
					cuota.setFechaVencimientoAnterior(DateHelper.getDiaHabil(feriados, fechaDesembolso));
				}

				double tasaAmortizacion;
				if (beanCalculo.getTasaCompensatorio() != null) {
					tasaAmortizacion = beanCalculo.getTasaCompensatorio()
							.calcularTasaFinal(cuota.getFechaVencimiento());
				} else {
					tasaAmortizacion = beanCalculo.getTasaAmortizacion();
				}
				cuota.setTasaAmortizacion(tasaAmortizacion);
				cuota.setTasaBonificacion(beanCalculo.getTasaBonificacion(nroCta - 1));
				cuota.setDiasPeriodo((int) DateHelper.getDiffDates(cuota.getFechaVencimientoAnterior(),
						cuota.getFechaVencimiento(), 2));
				cuota.setDiasTransaccion((int) DateHelper.getDiffDates(cuota.getFechaVencimientoAnterior(),
						cuota.getFechaVencimiento(), 2));
				cuota.setSaldoCapital(saldoCapital);
				cuota.setCompensatorio(this.calcularCompensatorio(cuota));
				cuota.setBonificacion(this.calcularBonificacion(cuota));
				if (cuota.getDiasPeriodo() >= 0) {
					if (vencAnterior == null) {
						vencAnterior = Calendar.getInstance();
						vencAnterior.setTime(desembolso.getFecha());
					}
				}
				if (vencCompensatorio.getTime().compareTo(vencCapital.getTime()) == 0) {
					nroCtaCap++;
					if (vencCompensatorio.getTime().compareTo(desembolso.getFecha()) >= 0) {
						if (valorCuota == 0D) {
							valorCuota = DoubleHelper.redondear(
									desembolso.getImporte() / (beanCalculo.getPlazoCapital() - nroCtaCap + 1));
						}
						// si ya se ocupo todo el saldo, no seguir asignando capital
						if (saldoCapital > 0) {
							valorCuota = DoubleHelper.redondear(
									nroCtaCap.equals(beanCalculo.getPlazoCapital()) ? saldoCapital : valorCuota);
							cuota.setCapital(valorCuota);
							saldoCapital = DoubleHelper.redondear(saldoCapital - valorCuota);
						}
					}
					if (segundoCapitalOriginal != null && nroCta >= 2) {
						vencCapital.setTime(segundoCapitalOriginal.getTime());
						// empieza de la 2
						vencCapital.add(Calendar.MONTH, beanCalculo.getPeriodicidadCapital() * (nroCtaCap - 1));
					} else {
						vencCapital.setTime(primerVencCapital.getTime());
						vencCapital.add(Calendar.MONTH, beanCalculo.getPeriodicidadCapital() * nroCtaCap);
					}
					vencCapital.setTime(DateHelper.getDiaHabil(feriados, vencCapital.getTime()));
				}
				if (vencAnterior != null) {
					vencAnterior.setTime(DateHelper.getDiaHabil(feriados, vencCompensatorio.getTime()));
				}
				if (segundoCompensatorioOriginal != null && nroCta >= 2) {
					vencCompensatorio.setTime(segundoCompensatorioOriginal.getTime());
					vencCompensatorio.add(Calendar.MONTH, beanCalculo.getPeriodicidadCompensatorio() * (nroCta - 1));
				} else {
					vencCompensatorio.setTime(primerVencCompensatorio.getTime());
					vencCompensatorio.add(Calendar.MONTH, beanCalculo.getPeriodicidadCompensatorio() * nroCta);
				}
				vencCompensatorio.setTime(DateHelper.getDiaHabil(feriados, vencCompensatorio.getTime()));
				cuotas.add(cuota);
			}
		}

		return ordenarCuotas();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<BeanCuota> amortizarBonTasa(BeanCalculo beanCalculo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ArrayList<Date> feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");

		Calendar vencCapital = Calendar.getInstance();
		Calendar primerVencCapital = Calendar.getInstance();
		primerVencCapital.setTime(beanCalculo.getPrimerVencimientoCapital());
		vencCapital.setTime(DateHelper.getDiaHabil(feriados, primerVencCapital.getTime()));

		Calendar vencCompensatorio = Calendar.getInstance();
		Calendar primerVencCompensatorio = Calendar.getInstance();
		primerVencCompensatorio.setTime(beanCalculo.getPrimerVencimientoCompensatorio());
		vencCompensatorio.setTime(DateHelper.getDiaHabil(feriados, primerVencCompensatorio.getTime()));

		Integer nroCtaCap = 0;
		Double valorCuota = 0D, saldoCapital = beanCalculo.getMontoFinanciamiento();

		Calendar vencAnterior = null;

		for (int nroCta = 1; nroCta <= beanCalculo.getPlazoCompensatorio(); nroCta++) {
			BeanCuota cuota = new BeanCuota();
			cuota.setNumero(nroCta);
			cuota.setFechaVencimiento(vencCompensatorio.getTime());

			if (vencAnterior != null) {
				cuota.setFechaVencimientoAnterior(DateHelper.getDiaHabil(feriados, vencAnterior.getTime()));
			} else {
				cuota.setFechaVencimientoAnterior(DateHelper.getDiaHabil(feriados, beanCalculo.getFechaDesembolso()));
			}

			cuota.setTasaAmortizacion(beanCalculo.getTasaAmortizacion());
			cuota.setTasaBonificacion(beanCalculo.getTasaBonificacion(0));
			cuota.setSaldoCapital(saldoCapital);
			cuota.setDiasPeriodo(
					(int) DateHelper.getDiffDates(cuota.getFechaVencimientoAnterior(), cuota.getFechaVencimiento(), 2));
			cuota.setDiasTransaccion(
					(int) DateHelper.getDiffDates(cuota.getFechaVencimientoAnterior(), cuota.getFechaVencimiento(), 2));
			cuota.setInteres(this.calcularCompensatorio(cuota));
			cuota.setSubsidio(this.calcularBonificacion(cuota));

			if (cuota.getDiasPeriodo() >= 0) {
				if (vencAnterior == null) {
					vencAnterior = Calendar.getInstance();
					vencAnterior.setTime(beanCalculo.getFechaDesembolso());
				}
			}

			if (cuota.getFechaVencimiento().compareTo(vencCapital.getTime()) == 0) {
				nroCtaCap++;
				if (valorCuota == 0D) {
					valorCuota = DoubleHelper.redondear(
							beanCalculo.getMontoFinanciamiento() / (beanCalculo.getPlazoCapital() - nroCtaCap + 1));

				}
				valorCuota = DoubleHelper
						.redondear(nroCtaCap.equals(beanCalculo.getPlazoCapital()) ? saldoCapital : valorCuota);
				cuota.setCapital(valorCuota);
				saldoCapital = Math.round((saldoCapital - valorCuota) * 100D) / 100D;
				vencCapital.add(Calendar.MONTH, beanCalculo.getPeriodicidadCapital());
				vencCapital.set(Calendar.DATE, primerVencCapital.get(Calendar.DATE));
			}
			if (vencAnterior != null) {
				vencAnterior.setTime(DateHelper.getDiaHabil(feriados, vencCompensatorio.getTime()));
			}
			vencCompensatorio.add(Calendar.MONTH, beanCalculo.getPeriodicidadCompensatorio());
			vencCompensatorio.set(Calendar.DATE, primerVencCompensatorio.get(Calendar.DATE));
			cuotas.add(cuota);
		}
		return ordenarCuotas();

	}

	private Double calcularCompensatorio(BeanCuota cuota) {
		return CalculoInteresHelper.calcularInteresCompuesto(cuota.getSaldoCapital(), cuota.getTasaAmortizacion(),
				cuota.getDiasPeriodo(), cuota.getDiasTransaccion());
	}

	private Double calcularBonificacion(BeanCuota cuota) {
		return CalculoInteresHelper.calcularInteresCompuesto(cuota.getSaldoCapital(), cuota.getTasaBonificacion(),
				cuota.getDiasPeriodo(), cuota.getDiasTransaccion());
	}

}
