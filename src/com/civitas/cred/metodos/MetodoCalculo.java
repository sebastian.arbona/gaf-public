package com.civitas.cred.metodos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCuota;

public abstract class MetodoCalculo {

	protected ArrayList<BeanCuota> cuotas;

	public abstract ArrayList<BeanCuota> amortizar(BeanCalculo beanCalculo);

	protected final ArrayList<BeanCuota> ordenarCuotas() {
		ArrayList<BeanCuota> cuotasOrdenadas = new ArrayList<BeanCuota>();
		if (cuotas != null && !cuotas.isEmpty()) {
			HashMap<Integer, BeanCuota> hashCuotas = new HashMap<Integer, BeanCuota>();
			for (BeanCuota beanCuota : cuotas) {
				BeanCuota cuota;
				if (hashCuotas.containsKey(beanCuota.getNumero())) {
					cuota = hashCuotas.get(beanCuota.getNumero());
					cuota.agregar(beanCuota);
				} else {
					cuota = beanCuota;
				}
				hashCuotas.put(cuota.getNumero(), cuota);
			}
			cuotasOrdenadas.clear();
			TreeMap<Integer, BeanCuota> ordenado = new TreeMap<Integer, BeanCuota>();
			ordenado.putAll(hashCuotas);
			for (Iterator<Integer> iterator = ordenado.keySet().iterator(); iterator.hasNext();) {
				Integer nroCta = (Integer) iterator.next();
				cuotasOrdenadas.add(nroCta - 1, hashCuotas.get(nroCta));
			}
		}
		return cuotasOrdenadas;
	}

	public final ArrayList<BeanCuota> getCuotas() {
		return cuotas;
	}

	public final void setCuotas(ArrayList<BeanCuota> cuotas) {
		this.cuotas = cuotas;
	}

}
