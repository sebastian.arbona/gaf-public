package com.civitas.cred.jasper;

import com.asf.util.NumberHelper;

import it.businesslogic.ireport.IReportScriptlet;

public class SolicitudDesembolsoScriptlet extends IReportScriptlet {

	public SolicitudDesembolsoScriptlet() {
	}

	public String getDinero(Double dinero) {
		return NumberHelper.getAsDinero(dinero.doubleValue());
	}

	public String moneda(String s) {
		if (s.equals("Dolar"))
			return "D�lares";
		else if (s.equals("Euro"))
			return "Euros";
		else if (s.equals("Peso Argentino"))
			return "Pesos";
		else
			return "";
	}

	public String importe(String s, Double importe, Double importeReal) {
		if (importeReal != null) {
			return s + String.format("%.2f", importeReal);
		} else {
			return s + String.format("%.2f", importe);
		}
	}

}
