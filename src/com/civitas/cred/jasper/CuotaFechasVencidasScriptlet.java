package com.civitas.cred.jasper; 

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;

import it.businesslogic.ireport.IReportScriptlet;

public class CuotaFechasVencidasScriptlet  extends IReportScriptlet{
	
	BusinessPersistance bp;
	
	public CuotaFechasVencidasScriptlet() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}
	
	public String getFecha(Long numeroNotificacion) {
		String output = "";
		String sql = " select a.fecha from Objetoi o , AcuerdoPago a, Notificacion n";
		sql += " where a.creditoAcuerdo_id = o.id ";
		sql += " and n.credito_id = o.id ";
		sql += " and n.id = :id";
		@SuppressWarnings("unchecked")
		ArrayList<Object> fechas = (ArrayList<Object>) bp.createSQLQuery(sql).setLong("id", numeroNotificacion).list();
		for(Object fecha : fechas) {
 		   output = this.FormatFechaLarga((Date)fecha);
		}
		return output;
	}
	
	
	public String getResolucion(Long numeroNotificacion) {
		String output = "";
		String sql = " select o.resolucion, o.fechaResolucion from Objetoi o, Notificacion n";
		sql += " where n.credito_id = o.id ";
		sql += " and n.id = :id";
		@SuppressWarnings("unchecked")
		ArrayList<Object> fechas = (ArrayList<Object>) bp.createSQLQuery(sql).setLong("id", numeroNotificacion).list();
		for(Object fecha : fechas) {
		   Object[] valor = (Object[]) fecha;	
 		   output = "  " + valor[0]==null?"":valor[0] + " de fecha " + this.FormatFechaLarga((Date)valor[1]) + " del D.E.. ";
		}
		return output;
	}
	
	
	public String getFechas(Long numeroNotificacion) {
		 SimpleDateFormat formatter;
		 String output = "";
		 String sql = " select fechaVencimiento from Cuota c, Objetoi o, Notificacion n ";
		 sql += " where c.credito_id = o.id and n.credito_id = o.id ";
		 sql += " and c.fechaVencimiento <= :fecha";
		 sql += " and estado = 1 and";
		 sql += " n.id = :id";
		 
		 ArrayList<Object> fechas = (ArrayList<Object>) bp.createSQLQuery(sql).setDate("fecha", new Date()).setLong("id", numeroNotificacion).list();
		 int cont = 0;
		 for(Object fecha : fechas) {
			 
			 if (fecha!=null) {
				 Date fec = (Date) fecha;
				 formatter = new SimpleDateFormat("dd/MM/yyyy");
				 output = formatter.format(fec);
			 }
			 cont++;
			 if(cont == fechas.size() )
				 output += " y ";
			 if (cont < fechas.size() - 1 ) 
				 output += ", ";

		 } 
		 if (output.length()>0) output = output.substring(0, output.length()-3); 
	     return output;	
	}
	
	private String FormatFechaLarga(Date fecha) {
		
		 if (fecha == null) {
			 return "";
		 } 
	     SimpleDateFormat formatter;
		 Date fec = (Date) fecha;
		 formatter = new SimpleDateFormat("dd.MMMM.yyyy");
		 return formatter.format(fec).replace(".", " de ");
		 
	}

	
}
