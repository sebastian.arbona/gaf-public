package com.civitas.cred.jasper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.CConcepto;
import com.nirven.creditos.hibernate.Concepto;

import it.businesslogic.ireport.IReportScriptlet;

public class BoletosEmisionScriptlet extends IReportScriptlet {
	private Double importeTotal;
	private List<CConcepto> conceptos;
	private HashMap<String, Double> deudas;
	private HashMap<String, String> cuotas;
	private int contador;
	private String concepto_id;
	private Float capital;
	private Float compensatorio;
	private Float moratorioMasPunitorio;

	public String getFechaStr() {
		return new SimpleDateFormat("dd - MMM - yyyy  HH:mm").format(new Date());
	}

	public String sumarImportes(Double importe) {
		importeTotal += importe;
		return null;
	}

	public Double getImporteTotal() {
		return importeTotal;
	}

	public String moneda(String s) {
		if (s.equals("Dolar"))
			return "D�lares:";
		else if (s.equals("Euro"))
			return "Euros:";
		else if (s.equals("Peso Argentino"))
			return "Pesos:";
		else
			return ":";
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}
		// agrego el c�digo del ente fijo: 2696
		codigo = "2696" + codigo;
		// agrego el numero de cuota fijo: 01
		codigo += "01";
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha;
		try {
			fecha = formato.parse(vencimiento);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		// agrego el periodo
		formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(fecha);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(fecha);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int digito;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String emisionStr, String vencimientoStr) {
		Date emision = DateHelper.getDate(emisionStr);
		Date vencimiento = DateHelper.getDate(vencimientoStr);
		return calcularDigitoVerificador(numeroAtencion, emision, vencimiento);
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, Date emision, Date vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}
		// agrego el c�digo del ente fijo: 2696
		codigo = "2696" + codigo;
		// agrego el numero de cuota fijo: 01
		codigo += "01";
		// agrego el periodo
		SimpleDateFormat formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(emision);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(vencimiento);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		int digito;
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularDeuda(String objetoiIdStr, String vencimientoStr) {
		Date vencimiento = DateHelper.getDate(vencimientoStr);
		SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long objetoiId = Long.parseLong(objetoiIdStr);

		CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(objetoiId, vencimiento);
		calculo.calcular();
		BeanCtaCte totalBean = calculo.getTotal();
		deudas = new HashMap<String, Double>();
		deudas.put("com", totalBean.getCompensatorioBruto());
		deudas.put("pun", totalBean.getPunitorio());
		deudas.put("mor", totalBean.getMoratorio());
		deudas.put("gas", totalBean.getGastos());
		deudas.put("mul", totalBean.getMultas());
		double sumaCapital = 0;
		for (int i = 0; i < calculo.getBeans().size() - 1; i++) {
			BeanCtaCte bean = calculo.getBeans().get(i);
			if (bean.getFechaEmision() != null && bean.getFechaEmision().getTime() <= vencimiento.getTime()) {
				sumaCapital += bean.getCapital();
			} else if (bean.getFechaVencimiento().getTime() <= vencimiento.getTime()) {
				sumaCapital += bean.getCapital();
			}
		}
		deudas.put("cap", sumaCapital);

		// Armo las cadenas con las cuotas correspondientes a cada concepto

		Integer nro = 0;
		Double cap = 0.0;
		String ccap = "";
		Double com = 0.0;
		String ccom = "";
		Double pun = 0.0;
		String cpun = "";
		Double mor = 0.0;
		String cmor = "";
		Double gas = 0.0;
		String cgas = "";
		Double mul = 0.0;
		String cmul = "";
		Double bon = 0.0;
		String cbon = "";
		Double bonTotal = 0.0;

		List<BeanCtaCte> beans = calculo.getBeans();
		for (BeanCtaCte bean : calculo.getBeans()) {
			if (bean.getNumero() != 0 && bean.getNumero().compareTo(nro) != 0) {
				if (nro > 0) {
					if (cap > 0.0) {
						if (ccap.equalsIgnoreCase("")) {
							ccap = nro.toString();
						} else {
							ccap = ccap + "-" + nro.toString();
						}
					}
					if (com > 0.0) {
						if (ccom.equalsIgnoreCase("")) {
							ccom = nro.toString();
						} else {
							ccom = ccom + "-" + nro.toString();
						}
					}
					if (pun > 0.0) {
						if (cpun.equalsIgnoreCase("")) {
							cpun = nro.toString();
						} else {
							cpun = cpun + "-" + nro.toString();
						}
					}
					if (mor > 0.0) {
						if (cmor.equalsIgnoreCase("")) {
							cmor = nro.toString();
						} else {
							cmor = cmor + "-" + nro.toString();
						}
					}
					if (gas > 0.0) {
						if (cgas.equalsIgnoreCase("")) {
							cgas = nro.toString();
						} else {
							cgas = cgas + "-" + nro.toString();
						}
					}
					if (mul > 0.0) {
						if (cmul.equalsIgnoreCase("")) {
							cmul = nro.toString();
						} else {
							cmul = cmul + "-" + nro.toString();
						}
					}
					if (bon > 0.0) {
						if (cbon.equalsIgnoreCase("")) {
							cbon = nro.toString();
						} else {
							cbon = cbon + "-" + nro.toString();
						}
					}
					cap = 0.0;
					com = 0.0;
					mor = 0.0;
					pun = 0.0;
					mul = 0.0;
					gas = 0.0;
					bonTotal += bon;
					bon = 0.0;
				}
				nro++;
			}
			if (bean.getNumero().compareTo(nro) == 0 && (bean.getDetalle().equalsIgnoreCase("cuota")
					|| bean.getDetalle().equalsIgnoreCase("debito manual"))) {
				if (bean.getFechaEmision() != null && bean.getFechaEmision().getTime() <= vencimiento.getTime()) {
					sumaCapital += bean.getCapital();
					cap += bean.getCapital();

				} else if (bean.getFechaVencimiento().getTime() <= vencimiento.getTime()) {
					sumaCapital += bean.getCapital();
					cap += bean.getCapital();
				}
				com += bean.getCompensatorio();
				pun += bean.getPunitorio();
				mor += bean.getMoratorio();
				mul += bean.getMultas();
				gas += bean.getGastos();
			} else if (bean.getNumero().compareTo(nro) == 0 && !(bean.getDetalle().equalsIgnoreCase("bonificacion"))) {
				if (bean.getFechaEmision() != null && bean.getFechaEmision().getTime() <= vencimiento.getTime()) {
					sumaCapital -= bean.getCapital();
				} else if (bean.getFechaVencimiento().getTime() <= vencimiento.getTime()) {
					sumaCapital -= bean.getCapital();
				}
				com -= bean.getCompensatorio();
				pun -= bean.getPunitorio();
				mor -= bean.getMoratorio();
				mul -= bean.getMultas();
				gas -= bean.getGastos();
			} else if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("bonificacion")) {
				bon += bean.getCompensatorio();
			}
		}

		// Al ser el ultimo bean compruebo si hay que agregar la ultima cuota a alguno
		// de los conceptos
		if (nro > 0) {
			if (cap > 0.0) {
				if (ccap.equalsIgnoreCase("")) {
					ccap = nro.toString();
				} else {
					ccap = ccap + "-" + nro.toString();
				}
			}
			if (com > 0.0) {
				if (ccom.equalsIgnoreCase("")) {
					ccom = nro.toString();
				} else {
					ccom = ccom + "-" + nro.toString();
				}
			}
			if (pun > 0.0) {
				if (cpun.equalsIgnoreCase("")) {
					cpun = nro.toString();
				} else {
					cpun = cpun + "-" + nro.toString();
				}
			}
			if (mor > 0.0) {
				if (cmor.equalsIgnoreCase("")) {
					cmor = nro.toString();
				} else {
					cmor = cmor + "-" + nro.toString();
				}
			}
			if (gas > 0.0) {
				if (cgas.equalsIgnoreCase("")) {
					cgas = nro.toString();
				} else {
					cgas = cgas + "-" + nro.toString();
				}
			}
			if (mul > 0.0) {
				if (cmul.equalsIgnoreCase("")) {
					cmul = nro.toString();
				} else {
					cmul = cmul + "-" + nro.toString();
				}
			}
			if (bon > 0.0) {
				if (cbon.equalsIgnoreCase("")) {
					cbon = nro.toString();
				} else {
					cbon = cbon + "-" + nro.toString();
				}
			}
		}

		// Le seteo al mapa las cadenas con las cuotas correspondientes
		cuotas = new HashMap<String, String>();
		cuotas.put("com", ccom);
		cuotas.put("cap", ccap);
		cuotas.put("mor", cmor);
		cuotas.put("pun", cpun);
		cuotas.put("gas", cgas);
		cuotas.put("mul", cmul);
		cuotas.put("bon", cbon);

		deudas.put("bon", bonTotal);

		return "";
	}

	public String nombreConcepto() {
		if (contador >= conceptos.size()) {
			contador = 0;
			concepto_id = conceptos.get(contador).getId();
			return conceptos.get(contador++).getDetalle();
		}
		concepto_id = conceptos.get(contador).getId();
		return conceptos.get(contador++).getDetalle();
	}

	public Boolean hayConcepto() {
		double total = 0.0;
		for (CConcepto c : conceptos) {
			total += deudas.get(c.getId());
		}
		if (total > 0.0)
			return true;
		else
			return false;
	}

	public String cuotas() {
		return cuotas.get(concepto_id);
	}

	public String cuotas(String concepto_id) {
		return cuotas.get(concepto_id);
	}

	public Double getSubtotal() {
		return deudas.get(concepto_id);
	}

	public Double getSubtotal(String concepto_id) {
		return deudas.get(concepto_id);
	}

	public Double getTotal() {
		double total = 0.0;
		for (Double subTotal : deudas.values()) {
			total += subTotal;
		}
		total = total - (2 * deudas.get("bon"));
		return total;
	}

	private int talon;

	public String talon() {
		if (talon <= 8) {
			talon++;
			if (talon == 5)
				return "Tal�n 1 para el Contribuyente";
			else if (talon >= 7) {
				return "Tal�n 1 para el Contribuyente";
			} else
				return "" + talon;
		}
		return "Tal�n 2 para el FTyC";
	}

	@SuppressWarnings("unchecked")
	public String calcularDetalles(BigDecimal numeroBoleto, Long idBoleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b " + "WHERE b.id.boleto.id = :boleto")
				.setParameter("boleto", idBoleto).list();
		capital = 0F;
		compensatorio = 0F;
		moratorioMasPunitorio = 0F;
		for (Bolcon bolcon : bolcons) {
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
				capital = bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
				compensatorio = bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
				moratorioMasPunitorio += bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
				moratorioMasPunitorio += bolcon.getImporte().floatValue();
			}
		}
		return "";
	}

	public Float getCapital() {
		return capital;
	}

	public void setCapital(Float capital) {
		this.capital = capital;
	}

	public Float getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(Float compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Float getMoratorioMasPunitorio() {
		return moratorioMasPunitorio;
	}

	public void setMoratorioMasPunitorio(Float moratotioMasPunitorio) {
		this.moratorioMasPunitorio = moratotioMasPunitorio;
	}

	public Float getImporteTotal_2() {
		return compensatorio + capital + moratorioMasPunitorio;
	}

}
