package com.civitas.cred.jasper;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.asf.cred.business.CreditoHandler;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.civitas.hibernate.persona.Domicilio;
import com.civitas.hibernate.persona.creditos.Contacto;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObjetoiIndice;

import it.businesslogic.ireport.IReportScriptlet;

public class OtorgamientoScriptlet extends IReportScriptlet {

	private BusinessPersistance bp;
	private Linea linea;
	private Objetoi objetoi;
	private List<Desembolso> desembolsos;
	private int cantidadCuotas;
	private NumberFormat formatoNumero;

	public OtorgamientoScriptlet() {
		if (SessionHandler.getCurrentSessionHandler() != null)
			bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		formatoNumero = NumberFormat.getInstance();
		formatoNumero.setMaximumFractionDigits(2);
		formatoNumero.setMinimumFractionDigits(2);
	}

	public String getLinea(long id) {
		linea = (Linea) bp.getById(Linea.class, id);
		if (linea != null)
			return linea.getNombre();
		else
			return null;
	}

	public double calcularTotal(String idObjetoi) {
		if (objetoi == null) {
			Long id = Long.parseLong(idObjetoi);
			objetoi = (Objetoi) bp.getById(Objetoi.class, id);
		}
		Double total = 0.0;
		total += objetoi.getFinanciamiento() == null ? 0.0 : objetoi.getFinanciamiento();
		total += objetoi.getAportePropio() == null ? 0.0 : objetoi.getAportePropio();
		total += objetoi.getAporteCofinanciador() == null ? 0.0 : objetoi.getAporteCofinanciador();
		total += objetoi.getOtrosAportes() == null ? 0.0 : objetoi.getOtrosAportes();
		return total;
	}

	public String getPorcentaje(String idObjetoi, Double cantidad) {
		if (calcularTotal(idObjetoi) == 0.0 || cantidad == null) {
			return NumberFormat.getInstance().format(0.0);
		}
		Double porcentaje = cantidad / calcularTotal(idObjetoi) * 100;
		return NumberFormat.getInstance().format(porcentaje.doubleValue());
	}

	public String getDomicilio(String tipo) {
		Domicilio domicilio = objetoi.getDomicilio(tipo);
		String domicilioStr = domicilio == null ? "" : domicilio.getDomicilioCompleto2();
		return domicilioStr;
	}

	public String getImporteDesembolso(String idObjetoi) {
		Double importeDesembolsos;
		long id = Long.parseLong(idObjetoi);
		importeDesembolsos = (Double) bp
				.createSQLQuery("SELECT SUM(importe) FROM Desembolso d WHERE d.credito_id = :credito_id")
				.setParameter("credito_id", id).uniqueResult();
		if (importeDesembolsos != null) {
			return getImporte(importeDesembolsos);
		} else {
			return null;
		}

	}

	public String getRequisitoDesembolso(String idObjetoi, int orden) {
		if (desembolsos == null) {
			long id = Long.parseLong(idObjetoi);
			String consulta = "SELECT d  FROM Desembolso d WHERE d.credito = " + id + " ORDER BY d.fecha";
			desembolsos = bp.getByFilterCast(consulta);
		}
		if (desembolsos.size() > orden && desembolsos.get(orden).getRequisito() != null)
			return desembolsos.get(orden).getRequisito().getDescripcion();
		else
			return null;
	}

	public String vencimientosCuotasInteres(String idObjetoi) {
		long id = Long.parseLong(idObjetoi);
		String consulta = "SELECT c  FROM Cuota c WHERE c.credito = " + id + " AND c.compensatorio > 0.0 "
				+ " ORDER BY c.fechaVencimiento";
		List<Cuota> cuotas = bp.getByFilterCast(consulta);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String vencimientos = "";
		for (Cuota cuota : cuotas) {
			vencimientos = vencimientos + "    " + format.format(cuota.getFechaVencimiento());
			cantidadCuotas++;
		}
		return vencimientos;
	}

	public String vencimientosCuotasCapital(String idObjetoi) {
		long id = Long.parseLong(idObjetoi);
		String consulta = "SELECT c  FROM Cuota c WHERE c.credito = " + id + " AND c.capital > 0.0 "
				+ " ORDER BY c.fechaVencimiento";
		List<Cuota> cuotas = bp.getByFilterCast(consulta);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String vencimientos = "  ";
		cantidadCuotas = 0;
		for (Cuota cuota : cuotas) {
			vencimientos = vencimientos + "    " + format.format(cuota.getFechaVencimiento());
			cantidadCuotas++;
		}
		return vencimientos;
	}

	public Integer cuotas() {
		return cantidadCuotas;
	}

	public String tna(String idObjetoi, String tipo) {
		if (objetoi == null) {
			Long id = Long.parseLong(idObjetoi);
			objetoi = (Objetoi) bp.getById(Objetoi.class, id);
		}
		CreditoHandler creditoHandler = new CreditoHandler();
		ObjetoiIndice objetoiIndice;
		Long idIndice;
		if (tipo.equals("compensatorio")) {
			objetoiIndice = objetoi.findTasaCompensatorio();
			if (objetoiIndice != null) {
				idIndice = objetoiIndice.getIndice().getId();
				return NumberFormat.getInstance().format(creditoHandler.buscarValorIndice(idIndice) * 100)
						+ "% de la tasa "
						+ "de inter�s nominal anual variable vencida en Pesos de la Cartera General del Banco de la Naci�n Argentina.";
			} else {
				return null;
			}
		}
		if (tipo.equals("moratorio")) {
			objetoiIndice = objetoi.findTasaMoratorio();
			if (objetoiIndice != null) {
				idIndice = objetoiIndice.getIndice().getId();
				return creditoHandler.buscarValorIndice(idIndice)
						+ " veces la tasa fijada para los intereses compensatorios.";
			} else {
				return null;
			}
		}
		if (tipo.equals("punitorio")) {
			objetoiIndice = objetoi.findTasaPunitorio();
			if (objetoiIndice != null) {
				idIndice = objetoiIndice.getIndice().getId();
				return NumberFormat.getInstance().format(creditoHandler.buscarValorIndice(idIndice) * 100)
						+ "% de la tasa prevista para los intereses moratorios.";
			} else {
				return null;
			}
		}
		return null;
	}

	public String telefono_email(String idPersona) {
		if (idPersona != null) {
			long id = Long.parseLong(idPersona);
			String consulta = "SELECT C FROM Contacto C WHERE C.persona = " + id;
			List<Contacto> lista = bp.getByFilterCast(consulta);
			if (lista.isEmpty()) {
				return null;
			} else {
				String telefono = telefono(lista);
				String email = email(lista);
				if (telefono == null && email == null) {
					return null;
				}
				if (telefono == null && email != null) {
					return email;
				}
				if (email == null && telefono != null) {
					return telefono;
				}
				return telefono + " - " + email;
			}
		}
		return "";
	}

	public String garantias(String idCredito, String tipo) {
		String ConsultaGarantia;
		if (idCredito != null) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			long id = Long.parseLong(idCredito);
			ConsultaGarantia = "Select og FROM ObjetoiGarantia og where og.objetoi.id = " + id + " AND og.baja IS NULL";
			List<ObjetoiGarantia> listaObjetoGarantia = bp.getByFilterCast(ConsultaGarantia);
			if (tipo == "tipoGarantia") {
				for (ObjetoiGarantia objetoiGarantia : listaObjetoGarantia) {
					return objetoiGarantia.getGarantia().getTipoProducto();
				}
			}
		} else {
			ConsultaGarantia = "";
		}
		return "";
	}

	public String telefono_celular_email(String idPersona) {
		if (idPersona != null) {
			long id = Long.parseLong(idPersona);
			String consulta = "SELECT C FROM Contacto C WHERE C.persona = " + id;
			List<Contacto> lista = bp.getByFilterCast(consulta);
			if (lista.isEmpty()) {
				return null;
			} else {
				String telefono = telefono(lista);
				String celular = celular(lista);
				String email = email(lista);

				String r = (telefono != null ? telefono + " - " : "") + (celular != null ? celular + " - " : "")
						+ (email != null ? email : "");
				if (r.endsWith(" - ")) {
					r = r.substring(0, r.length() - 3);
				}
				return r;
			}
		}
		return "";
	}

	public String telefono(List<Contacto> lista) {
		for (Contacto contacto : lista) {
			if (contacto.getMedio().equals("Tel�fono") && contacto.getTipo().equals("Principal")) {
				return contacto.getDetalle();
			}
		}
		return null;
	}

	public String celular(List<Contacto> lista) {
		for (Contacto contacto : lista) {
			if (contacto.getMedio().equals("Celular") && contacto.getTipo().equals("Principal")) {
				return contacto.getDetalle();
			}
		}
		return null;
	}

	public String email(List<Contacto> lista) {
		for (Contacto contacto : lista) {
			if (contacto.getMedio().equals("E-Mail") && contacto.getTipo().equals("Notificaci�n")) {
				return contacto.getDetalle();
			}
		}
		return null;
	}

	public String getFechaStr() {
		return new SimpleDateFormat("dd - MMM - yyyy  hh:mm:ss").format(new Date());
	}

	public String getEstadoActual(String idObjetoi) {
		objetoi = (Objetoi) bp.getById(Objetoi.class, new Long(idObjetoi));
		return objetoi.getEstadoActual().getEstado().getNombreEstado();
	}

	public String tna2(String idObjetoi, String tipo) {
		if (objetoi == null) {
			Long id = Long.parseLong(idObjetoi);
			objetoi = (Objetoi) bp.getById(Objetoi.class, id);
		}
		ObjetoiIndice objetoiIndice = null;
		Long idIndice;
		String nombreIndice;
		String valorMas;
		String valorPor;
		String diasAntes;
		String tipoCalculo;
		if (tipo.equals("compensatorio")) {
			objetoiIndice = objetoi.findTasaCompensatorio();
		} else if (tipo.equals("moratorio")) {
			objetoiIndice = objetoi.findTasaMoratorio();
		} else if (tipo.equals("punitorio")) {
			objetoiIndice = objetoi.findTasaPunitorio();
		}
		if (objetoiIndice != null) {
			idIndice = objetoiIndice.getIndice().getId();
			nombreIndice = objetoiIndice.getIndice().getNombre();
			valorMas = objetoiIndice.getValorMasStr();
			valorPor = objetoiIndice.getValorPorStr();
			diasAntes = objetoiIndice.getDiasAntes() != null ? objetoiIndice.getDiasAntes().toString() : null;
			tipoCalculo = objetoiIndice.getTipoCalculoStr();
			return idIndice + " - " + nombreIndice + " Mas: " + valorMas + " Por: " + valorPor
					+ (diasAntes != null ? (" Dias antes: " + diasAntes) : "") + " Calculo: " + tipoCalculo;
		}
		return null;
	}

	public double getBonificacionTotal(String idObjetoi) {
		if (objetoi == null) {
			objetoi = (Objetoi) bp.getById(Objetoi.class, new Long(idObjetoi));
		}
		double[] bonificaciones = objetoi.calcularTasaNeta(new Date(), false);
		double tasaNeta;
		if (bonificaciones.length > 0) {
			tasaNeta = bonificaciones[0];
		} else {
			tasaNeta = 0.0;
		}
		return objetoi.findTasaCompensatorio().getValorFinal() - tasaNeta;
	}

	public String getImporte(Double importe) {
		if (importe != null) {
			return objetoi.getLinea().getMoneda().getSimbolo() + " " + formatoNumero.format(importe);
		} else {
			return objetoi.getLinea().getMoneda().getSimbolo() + " " + formatoNumero.format(0.0);
		}
	}

	public String getImporte(Double importe, String moneda) {
		if (importe != null) {
			return moneda + " " + formatoNumero.format(importe);
		} else {
			return moneda + " " + formatoNumero.format(0.0);
		}
	}

	public String getFrecuenciaCapitalStr() {
		cantidadCuotas = objetoi.getPlazoCapital();
		if (objetoi.getFrecuenciaCapital() != null) {
			return objetoi.getFrecuenciaCapitalStr();
		} else {
			return null;
		}
	}

	public String getFrecuenciaInteresStr() {
		cantidadCuotas = objetoi.getPlazoCompensatorio();
		if (objetoi.getFrecuenciaInteres() != null) {
			return objetoi.getFrecuenciaInteresStr();
		} else {
			return null;
		}
	}

}
