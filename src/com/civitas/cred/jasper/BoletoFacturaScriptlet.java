package com.civitas.cred.jasper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.asf.util.SQLHelper;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.CConcepto;
import com.nirven.creditos.hibernate.Concepto;

import it.businesslogic.ireport.IReportScriptlet;

public class BoletoFacturaScriptlet extends IReportScriptlet {

	private Double importeTotal;
	private List<CConcepto> conceptos;
	private HashMap<String, Double> deudas;
	private HashMap<String, String> cuotas;
	private int contador;
	private String concepto_id;
	private Float capital;
	private Float compensatorio;
	private Float moratorioMasPunitorio;
	private Double total = 0.0;
	private Integer numCuota = 0;
	private String gastosAdministrativos;

	public BoletoFacturaScriptlet() {
		importeTotal = 0.0;
	}

	public String getFechaStr() {
		return new SimpleDateFormat("dd - MMM - yyyy  HH:mm").format(new Date());
	}

	public String sumarImportes(Double importe) {
		importeTotal += importe;
		return null;
	}

	public Double getImporteTotal() {
		return importeTotal;
	}

	public String moneda(String s) {
		if (s.equals("Dolar"))
			return "D�lares:";
		else if (s.equals("Euro"))
			return "Euros:";
		else if (s.equals("Peso Argentino"))
			return "Pesos:";
		else
			return ":";
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}
		// agrego el c�digo del ente fijo: 2696
		codigo = "2696" + codigo;
		// agrego el numero de cuota fijo: 01
		codigo += "01";
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha;
		try {
			fecha = formato.parse(vencimiento);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		// agrego el periodo
		formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(fecha);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(fecha);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int digito;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String emisionStr, String vencimientoStr) {
		Date emision = DateHelper.getDate(emisionStr);
		Date vencimiento = DateHelper.getDate(vencimientoStr);
		return calcularDigitoVerificador(numeroAtencion, emision, vencimiento);
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, Date emision, Date vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}
		// agrego el c�digo del ente fijo: 2696
		codigo = "2696" + codigo;
		// agrego el numero de cuota fijo: 01
		codigo += "01";
		// agrego el periodo
		SimpleDateFormat formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(emision);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(vencimiento);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		int digito;
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	@SuppressWarnings("unchecked")
	public String calcularDeuda(String objetoiIdStr, String vencimientoStr, String idBoleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b " + "WHERE b.id.boleto.id=:boleto")
				.setParameter("boleto", new Long(idBoleto)).list();

		Double capital = 0.0;
		Double compensatorio = 0.0;
		Double punitorio = 0.0;
		Double moratorio = 0.0;
		Double gastos = 0.0;
		Double gastosRec = 0.0;
		Double bonificaciones = 0.0;
		for (Bolcon bolcon : bolcons) {
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
				capital += bolcon.getImporte();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
				compensatorio += bolcon.getImporte();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
				punitorio += bolcon.getImporte();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
				moratorio += bolcon.getImporte();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_BONIFICACION)) {
				bonificaciones += bolcon.getImporte();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_GASTOS)) {
				gastos += bolcon.getImporte();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_GASTOS_A_RECUPERAR)) {
				gastosRec += bolcon.getImporte();
			}
		}
		Bolcon bolc = bolcons.get(0);

		numCuota = bolc.getCuota().getNumero();

		deudas = new HashMap<String, Double>();
		deudas.put("com", new Double(compensatorio));
		deudas.put("pun", new Double(punitorio));
		deudas.put("mor", new Double(moratorio));
		deudas.put("cap", new Double(capital));
		deudas.put("gas", new Double(gastos));
		deudas.put("rec", new Double(gastosRec));
		deudas.put("bon", new Double(bonificaciones));
		// guardo el total
		total += new Double(compensatorio);
		total += new Double(punitorio);
		total += new Double(moratorio);
		total += new Double(capital);
		total += new Double(gastos);
		total += new Double(gastosRec);
		total -= new Double(bonificaciones);

		String consultaBasica = "SELECT cc.detalle FROM Ctacte cc "
				+ "JOIN concepto c ON cc.facturado_id = c.id JOIN cconcepto cco ON c.concepto_concepto = cco.concepto WHERE cc.boleto_id = :boleto_id "
				+ "AND tipomov_id = 2 AND cco.abreviatura = 'gas' AND c.periodo = :periodo";
		String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
		Object detalle = bp.createSQLQuery(consultaLimitada).setLong("boleto_id", new Long(idBoleto))
				.setLong("periodo", Calendar.getInstance().get(Calendar.YEAR)).uniqueResult();
		gastosAdministrativos = detalle == null ? "Gastos Administrativos:"
				: "Gastos Administrativos: " + (String) detalle;
		return "";
	}

	public String nombreConcepto() {
		if (contador >= conceptos.size()) {
			contador = 0;
			concepto_id = conceptos.get(contador).getId();
			return conceptos.get(contador++).getDetalle();
		}
		concepto_id = conceptos.get(contador).getId();
		return conceptos.get(contador++).getDetalle();
	}

	public Boolean hayConcepto() {
		double total = 0.0;
		for (CConcepto c : conceptos) {
			total += deudas.get(c.getId());
		}
		if (total > 0.0)
			return true;
		else
			return false;
	}

	public String cuotas() {
		return cuotas.get(concepto_id);
	}

	public String cuotas(String concepto_id) {
		return cuotas.get(concepto_id);
	}

	public Double getSubtotal() {
		return deudas.get(concepto_id);
	}

	public Double getSubtotal(String concepto_id) {
		return deudas.get(concepto_id);
	}

	public Double getTotal() {
		return total;
	}

	private int talon;

	public String talon() {
		if (talon <= 8) {
			talon++;
			if (talon == 5)
				return "Tal�n 1 para el Contribuyente";
			else if (talon >= 7) {
				return "Tal�n 1 para el Contribuyente";
			} else
				return "" + talon;
		}
		return "Tal�n 2 para el FTyC";
	}

	@SuppressWarnings("unchecked")
	public String calcularDetalles(BigDecimal numeroBoleto, Long idBoleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b " + "WHERE b.id.boleto.id = :boleto")
				.setParameter("boleto", idBoleto).list();
		capital = 0F;
		compensatorio = 0F;
		moratorioMasPunitorio = 0F;

		for (Bolcon bolcon : bolcons) {
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
				capital = bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
				compensatorio = bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
				moratorioMasPunitorio += bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
				moratorioMasPunitorio += bolcon.getImporte().floatValue();
			}
		}
		return "";
	}

	public Float getCapital() {
		return capital;
	}

	public void setCapital(Float capital) {
		this.capital = capital;
	}

	public Float getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(Float compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Float getMoratorioMasPunitorio() {
		return moratorioMasPunitorio;
	}

	public void setMoratorioMasPunitorio(Float moratotioMasPunitorio) {
		this.moratorioMasPunitorio = moratotioMasPunitorio;
	}

	public Float getImporteTotal_2() {
		return compensatorio + capital + moratorioMasPunitorio;
	}

	@SuppressWarnings("unchecked")
	public Integer getNumCuota(String idBoleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b " + "WHERE b.id.boleto.id=:boleto")
				.setParameter("boleto", new Long(idBoleto)).list();
		Bolcon bolc = bolcons.get(0);

		numCuota = bolc.getCuota().getNumero();
		return numCuota;
	}

	public String getDomicilio(String manzana, String lote, String barrio) {
		String domicilio = "";
		domicilio += manzana != null && manzana.trim() != "" ? "Manzana: " + manzana : "Manzana:";
		domicilio += lote != null && lote.trim() != "" ? " - Lote: " + lote : " - Lote:";
		domicilio += barrio != null && barrio.trim() != "" ? " - Barrio: " + barrio : " - Barrio:";
		return domicilio;
	}

	public String getGastosAdministrativos() {
		return gastosAdministrativos;
	}
}
