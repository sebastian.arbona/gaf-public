package com.civitas.cred.jasper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.asf.cred.business.BoletoResumenHelper;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.NumberHelper;
import com.nirven.creditos.hibernate.Objetoi;

import it.businesslogic.ireport.IReportScriptlet;

public class BoletoResumenScriptlet extends IReportScriptlet {

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}

		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Objetoi o = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setParameter("nroAtencion", numeroAtencion.longValue()).uniqueResult();

			if (o != null && o.getLinea().getMoneda_id() == 1L) { // 1 = pesos
				// agrego el c�digo del ente fijo: 2696
				codigo = "2696" + codigo;
			} else {
				codigo = "4540" + codigo;
			}
		} catch (Exception e) {
			System.err.println("Error creando codigo de barras: " + e.getMessage());
			System.err.println("Usando default.");
			codigo = "2696" + codigo;
		}

		// agrego el numero de cuota fijo: 01
		codigo += "01";
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha;
		try {
			fecha = formato.parse(vencimiento);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		// agrego el periodo
		formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(fecha);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(fecha);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int digito;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String emisionStr, String vencimientoStr) {
		Date emision = DateHelper.getDate(emisionStr);
		Date vencimiento = DateHelper.getDate(vencimientoStr);
		return calcularDigitoVerificador(numeroAtencion, emision, vencimiento);
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, Date emision, Date vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}

		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Objetoi o = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setParameter("nroAtencion", numeroAtencion.longValue()).uniqueResult();

			if (o != null && o.getLinea().getMoneda_id() == 1L) { // 1 = pesos
				// agrego el c�digo del ente fijo: 2696
				codigo = "2696" + codigo;
			} else {
				codigo = "4540" + codigo;
			}
		} catch (Exception e) {
			System.err.println("Error creando codigo de barras: " + e.getMessage());
			System.err.println("Usando default.");
			codigo = "2696" + codigo;
		}

		// agrego el numero de cuota fijo: 01
		codigo += "01";
		// agrego el periodo
		SimpleDateFormat formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(emision);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(vencimiento);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		int digito;
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String getDineroEnLetras(double dinero) {
		dinero = Math.rint(dinero * 100D) / 100D;
		return NumberHelper.getAsDinero(dinero);
	}

	public String getDineroStr(double dinero) {
		dinero = Math.rint(dinero * 100D) / 100D;
		return String.format("%,.2f", dinero);
	}

	public String getFechaStr(Date fecha) {
		return DateHelper.getString(fecha);
	}

	public String getDomicilio(String manzana, String lote, String barrio) {
		String domicilio = "";
		domicilio += manzana != null && manzana.trim() != "" ? "Manzana: " + manzana : "Manzana:";
		domicilio += lote != null && lote.trim() != "" ? " - Lote: " + lote : " - Lote:";
		domicilio += barrio != null && barrio.trim() != "" ? " - Barrio: " + barrio : " - Barrio:";
		return domicilio;
	}

	public String calcularMoratorio(String idReporte, String idObjetoi) {
		return BoletoResumenHelper.getInstance(idReporte).calcularMoratorio(new Long(idObjetoi)).toString();
	}

	public String calcularPunitorio(String idReporte, String idObjetoi) {
		return BoletoResumenHelper.getInstance(idReporte).calcularPunitorio(new Long(idObjetoi)).toString();
	}

	public String calcularCuotasMoratorio(String idReporte, String idObjetoi) {
		return BoletoResumenHelper.getInstance(idReporte).calcularCuotasMoratorio(new Long(idObjetoi));
	}

	public String calcularCuotasPunitorio(String idReporte, String idObjetoi) {
		return BoletoResumenHelper.getInstance(idReporte).calcularCuotasMoratorio(new Long(idObjetoi));
	}

}
