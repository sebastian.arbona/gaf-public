package com.civitas.cred.jasper;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.asf.util.NumberHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.CConcepto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.DeudoresVarios;
import com.nirven.creditos.hibernate.Objetoi;

import it.businesslogic.ireport.IReportScriptlet;

public class BoletosDePagoScriptlet extends IReportScriptlet {

	private Double importeTotal;
	private List<CConcepto> conceptos;
	private HashMap<String, Double> deudas;
	private HashMap<String, String> cuotas;
	private int contador;
	private String concepto_id;
	private Float capital;
	private Float compensatorio;
	private Float moratorioMasPunitorio;

	private BusinessPersistance bp;

	public BoletosDePagoScriptlet() {
		importeTotal = 0.0;
	}

	public String getFechaStr() {
		return new SimpleDateFormat("dd - MMM - yyyy  HH:mm").format(new Date());
	}

	public String sumarImportes(Double importe) {
		importeTotal += importe;
		return null;
	}

	public Double getImporteTotal() {
		return importeTotal;
	}

	public String moneda(String s) {
		if (s.equals("Dolar"))
			return "D�LARES:";
		else if (s.equals("Euro"))
			return "EUROS:";
		else if (s.equals("Peso Argentino"))
			return "PESOS:";
		else
			return ":";
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}

		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Objetoi o = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setParameter("nroAtencion", numeroAtencion.longValue()).uniqueResult();

			if (o != null && o.getLinea().getMoneda_id() == 1L) { // 1 = pesos
				// agrego el c�digo del ente fijo: 2696
				codigo = "2696" + codigo;
			} else {
				codigo = "4540" + codigo;
			}
		} catch (Exception e) {
			System.err.println("Error creando codigo de barras: " + e.getMessage());
			System.err.println("Usando default.");
			codigo = "2696" + codigo;
		}

		// agrego el numero de cuota fijo: 01
		codigo += "01";
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha;
		try {
			fecha = formato.parse(vencimiento);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		// agrego el periodo
		formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(fecha);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(fecha);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int digito;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, String emisionStr, String vencimientoStr) {
		Date emision = DateHelper.getDate(emisionStr);
		Date vencimiento = DateHelper.getDate(vencimientoStr);
		return calcularDigitoVerificador(numeroAtencion, emision, vencimiento);
	}

	public String calcularDigitoVerificador(BigDecimal numeroAtencion, Date emision, Date vencimiento) {
		String codigo = numeroAtencion.toString();
		// relleno con ceros a la izquierda el numeroAtencion
		while (codigo.length() < 10) {
			codigo = "0" + codigo;
		}

		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Objetoi o = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setParameter("nroAtencion", numeroAtencion.longValue()).uniqueResult();

			if (o != null && o.getLinea().getMoneda_id() == 1L) { // 1 = pesos
				// agrego el c�digo del ente fijo: 2696
				codigo = "2696" + codigo;
			} else {
				codigo = "4540" + codigo;
			}
		} catch (Exception e) {
			System.err.println("Error creando codigo de barras: " + e.getMessage());
			System.err.println("Usando default.");
			codigo = "2696" + codigo;
		}

		// agrego el numero de cuota fijo: 01
		codigo += "01";
		// agrego el periodo
		SimpleDateFormat formato = new SimpleDateFormat("MMyyyy");
		String cadena = formato.format(emision);
		codigo += cadena;
		// agrego el vencimiento
		formato = new SimpleDateFormat("ddMMyyyy");
		cadena = formato.format(vencimiento);
		codigo += cadena;
		// inicio el calculo del d�gito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		int digito;
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularCodigoBarraDeudoresVarios(String id) {
		Long idDeudoresVarios = new Long(id);
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		DeudoresVarios dv = (DeudoresVarios) bp.getById(DeudoresVarios.class, idDeudoresVarios);

		String codigo;
		if (dv.getMoneda() != null && dv.getMoneda().getId() != 1L) {
			codigo = "4540";
		} else {
			codigo = "2696";
		}

		codigo += "99";
		codigo += String.format("%08d", dv.getNumero());
		codigo += "0000000000000000";

		// digito verificador
		String[] arreglo = codigo.split("");
		boolean esTres = true;
		int suma = 10;// inicio el acumulador en 10 por si la sumatoria es menor a 10
		int digito;
		for (int i = arreglo.length - 1; i > 0; i--) {
			digito = Integer.parseInt(arreglo[i]);
			if (esTres) {
				suma += digito * 3;
				esTres = false;
			} else {
				suma += digito;
				esTres = true;
			}
		}
		boolean seguir = true;
		int digitoVerificador = 0;
		while (seguir) {
			if (suma % 10 == 0) {
				seguir = false;
			} else {
				suma++;
				digitoVerificador++;
			}
		}
		return codigo + digitoVerificador;
	}

	public String calcularDeuda(String objetoiIdStr, String vencimientoStr) {
		return calcularDeuda(objetoiIdStr, vencimientoStr, "false");
	}

	public String calcularDeuda(String objetoiIdStr, String vencimientoStr, String deudaParcialStr) {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Date vencimiento = DateHelper.getDate(vencimientoStr);
		Long objetoiId = Long.parseLong(objetoiIdStr);
		CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(objetoiId, vencimiento);
		calculo.calcular();

		boolean deudaParcial = Boolean.parseBoolean(deudaParcialStr);

		BeanCtaCte totalBean = calculo.getTotal();
		deudas = new HashMap<String, Double>();
		if (!deudaParcial) {
			deudas.put("com", totalBean.getCompensatorioBruto());
			deudas.put("pun", totalBean.getPunitorio());
			deudas.put("mor", totalBean.getMoratorio());
			deudas.put("gas", totalBean.getGastosSimples());
			deudas.put("mul", totalBean.getMultas());
			deudas.put("cap", totalBean.getCapital());
			deudas.put("rec", totalBean.getGastosRec());
			deudas.put("cer", totalBean.getCer());

		} else {
			deudas.put("com", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_COMPENSATORIO));
			deudas.put("pun", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_PUNITORIO));
			deudas.put("mor", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_MORATORIO));
			deudas.put("gas", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_GASTOS));
			deudas.put("mul", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_MULTAS));
			deudas.put("cap", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_CAPITAL));
			deudas.put("rec", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR));
			deudas.put("bon", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_BONIFICACION));
			deudas.put("cer", calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_CER));
		}

		double sumaCapital = 0;

		for (int i = 0; i < calculo.getBeans().size() - 1; i++) {
			BeanCtaCte bean = calculo.getBeans().get(i);
			if (bean.getDetalle().equals("Cuota")) {
				sumaCapital += bean.getCapital();

			}
		}

		// Armo las cadenas con las cuotas correspondientes a cada concepto
		if (!deudaParcial) {
			Integer nro = 0;
			Double cap = 0.0;
			String ccap = "";
			Double com = 0.0;
			String ccom = "";
			Double pun = 0.0;
			String cpun = "";
			Double mor = 0.0;
			String cmor = "";
			Double gas = 0.0;
			String cgas = "";
			Double mul = 0.0;
			String cmul = "";
			Double bon = 0.0;
			String cbon = "";
			Double bonTotal = 0.0;
			Double capPar = 0.0;
			String ccapPar = "";
			Double rec = 0.0;
			String crec = "";
			Double cer = 0.0;
			String ccer = "";
			for (BeanCtaCte bean : calculo.getBeans()) {
				if (bean.getNumero() != 0 && bean.getNumero().compareTo(nro) != 0) {
					if (nro > 0) {
						if (com > 0.0) {
							if (ccom.equalsIgnoreCase("")) {
								ccom = nro.toString();
							} else {
								ccom = ccom + "-" + nro.toString();
							}
						}
						if (pun > 0.0) {
							if (cpun.equalsIgnoreCase("")) {
								cpun = nro.toString();
							} else {
								cpun = cpun + "-" + nro.toString();
							}
						}
						if (mor > 0.0) {
							if (cmor.equalsIgnoreCase("")) {
								cmor = nro.toString();
							} else {
								cmor = cmor + "-" + nro.toString();
							}
						}
						if (gas > 0.0) {
							if (cgas.equalsIgnoreCase("")) {
								cgas = nro.toString();
							} else {
								cgas = cgas + "-" + nro.toString();
							}
						}
						if (mul > 0.0) {
							if (cmul.equalsIgnoreCase("")) {
								cmul = nro.toString();
							} else {
								cmul = cmul + "-" + nro.toString();
							}
						}
						if (bon > 0.0) {
							if (cbon.equalsIgnoreCase("")) {
								cbon = nro.toString();
							} else {
								cbon = cbon + "-" + nro.toString();
							}
						}
						if (rec > 0.0) {
							if (crec.equalsIgnoreCase("")) {
								crec = nro.toString();
							} else {
								crec = crec + "-" + nro.toString();
							}
						}
						if (cer > 0.0) {
							if (ccer.equalsIgnoreCase("")) {
								ccer = nro.toString();
							} else {
								ccer = ccer + "-" + nro.toString();
							}
						}
						cap = 0.0;
						com = 0.0;
						mor = 0.0;
						pun = 0.0;
						mul = 0.0;
						gas = 0.0;
						bon = 0.0;
						rec = 0.0;
					}
					nro++;
				}

				if (bean.getNumero().compareTo(nro) == 0 && (bean.getDetalle().equalsIgnoreCase("cuota")
						|| bean.getDetalle().equalsIgnoreCase("debito manual"))) {
					if (bean.getFechaEmision() != null && bean.getFechaEmision().getTime() <= vencimiento.getTime()) {
						sumaCapital += bean.getCapital();
						cap += bean.getCapital();
					} else if (bean.getFechaVencimiento().getTime() <= vencimiento.getTime()) {
						sumaCapital += bean.getCapital();
						cap += bean.getCapital();
					}
					com += bean.getCompensatorio();
					pun += bean.getPunitorio();
					mor += bean.getMoratorio();
					mul += bean.getMultas();
					gas += bean.getGastosSimples();
					rec += bean.getGastosRec();
					cer += bean.getCer();
				} else if (bean.getNumero().compareTo(nro) == 0
						&& !(bean.getDetalle().equalsIgnoreCase("bonificacion"))) {
					if (bean.getFechaEmision() != null && bean.getFechaEmision().getTime() <= vencimiento.getTime()) {
						sumaCapital -= bean.getCapital();
						cap = Math.abs(sumaCapital);

						// sumaCapitalParcial-=bean.getCapital();
						// capPar = Math.abs(sumaCapitalParcial);

					} else if (bean.getFechaVencimiento().getTime() <= vencimiento.getTime()) {
						sumaCapital -= bean.getCapital();
						cap = Math.abs(sumaCapital);

						// sumaCapitalParcial-=bean.getCapital();
						// capPar = Math.abs(sumaCapitalParcial);
					}
					com -= bean.getCompensatorio();
					pun -= bean.getPunitorio();
					mor -= bean.getMoratorio();
					mul -= bean.getMultas();
					gas -= bean.getGastosSimples();
					rec -= bean.getGastosRec();
					cer -= bean.getCer();
				} else if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("bonificacion")) {
					bon += bean.getCompensatorio();
					bonTotal += bon;
				}
			}

			// Al ser el ultimo bean compruebo si hay que agregar la ultima cuota a alguno
			// de los conceptos
			if (nro > 0) {
				ccap = "1";
				ccapPar = "1";
				for (int i = 2; i <= nro.intValue(); i++) {
					ccap += "-" + i;

				}
				// for (int i = 2; i <= cantCap; i++) {
				// ccapPar += "-" + i;
				// }
				if (com > 0.0) {
					if (ccom.equalsIgnoreCase("")) {
						ccom = nro.toString();
					} else {
						ccom = ccom + "-" + nro.toString();
					}
				}
				if (pun > 0.0) {
					if (cpun.equalsIgnoreCase("")) {
						cpun = nro.toString();
					} else {
						cpun = cpun + "-" + nro.toString();
					}
				}
				if (mor > 0.0) {
					if (cmor.equalsIgnoreCase("")) {
						cmor = nro.toString();
					} else {
						cmor = cmor + "-" + nro.toString();
					}
				}
				if (gas > 0.0) {
					if (cgas.equalsIgnoreCase("")) {
						cgas = nro.toString();
					} else {
						cgas = cgas + "-" + nro.toString();
					}
				}
				if (rec > 0.0) {
					if (crec.equalsIgnoreCase("")) {
						crec = nro.toString();
					} else {
						crec = crec + "-" + nro.toString();
					}
				}
				if (cer > 0.0) {
					if (ccer.equalsIgnoreCase("")) {
						ccer = nro.toString();
					} else {
						ccer = ccer + "-" + nro.toString();
					}
				}
				if (mul > 0.0) {
					if (cmul.equalsIgnoreCase("")) {
						cmul = nro.toString();
					} else {
						cmul = cmul + "-" + nro.toString();
					}
				}
				if (bon > 0.0) {
					if (cbon.equalsIgnoreCase("")) {
						cbon = nro.toString();
					} else {
						cbon = cbon + "-" + nro.toString();
					}
				}
			}

			// Le seteo al mapa las cadenas con las cuotas correspondientes
			cuotas = new HashMap<String, String>();
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, objetoiId);
			if (credito.getCaducidadPlazo() != null && credito.getCaducidadPlazo().getCuota() != null) { // no mostrar
																											// cuota en
																											// caducidad
				String numero = ""; // credito.getCaducidadPlazo().getCuota().getNumero().toString();
				cuotas.put("com", numero);
				cuotas.put("cap", numero);
				cuotas.put("capPar", numero);
				cuotas.put("mor", numero);
				cuotas.put("pun", numero);
				cuotas.put("gas", numero);
				cuotas.put("rec", numero);
				cuotas.put("mul", numero);
				cuotas.put("bon", numero);
				cuotas.put("cer", numero);
			} else {
				cuotas.put("com", ccom);
				cuotas.put("cap", ccap);
				cuotas.put("capPar", ccapPar);
				cuotas.put("mor", cmor);
				cuotas.put("pun", cpun);
				cuotas.put("gas", cgas);
				cuotas.put("rec", crec);
				cuotas.put("mul", cmul);
				cuotas.put("bon", cbon);
				cuotas.put("cer", ccer);
			}
			deudas.put("bon", bonTotal);
		} else {
			// deuda parcial
			String cuotasStr = "";
			for (BeanCtaCte bean : calculo.getBeans()) {
				if (bean.isCuota() && bean.getNumero() != null) {
					if (bean.getFechaVencimiento() != null
							&& bean.getFechaVencimiento().getTime() <= vencimiento.getTime()) {
						cuotasStr += bean.getNumero() + "-";
					}
				}
			}

			if (!cuotasStr.isEmpty()) {
				cuotasStr = cuotasStr.substring(0, cuotasStr.length() - 1);
			}

			cuotas = new HashMap<String, String>();
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, objetoiId);
			if (credito.getCaducidadPlazo() != null && credito.getCaducidadPlazo().getCuota() != null) { // caducidad
																											// tiene una
																											// sola
																											// cuota con
																											// deuda
				String numero = credito.getCaducidadPlazo().getCuota().getNumero().toString();
				cuotas.put("com", numero);
				cuotas.put("cap", numero);
				cuotas.put("capPar", numero);
				cuotas.put("mor", numero);
				cuotas.put("pun", numero);
				cuotas.put("gas", numero);
				cuotas.put("rec", numero);
				cuotas.put("mul", numero);
				cuotas.put("bon", numero);
				cuotas.put("cer", numero);
			} else {
				cuotas.put("com", cuotasStr);
				cuotas.put("cap", cuotasStr);
				cuotas.put("capPar", cuotasStr);
				cuotas.put("mor", cuotasStr);
				cuotas.put("pun", cuotasStr);
				cuotas.put("gas", cuotasStr);
				cuotas.put("rec", cuotasStr);
				cuotas.put("mul", cuotasStr);
				cuotas.put("bon", cuotasStr);
				cuotas.put("cer", cuotasStr);
			}
		}

		return "";
	}

	public String nombreConcepto() {
		if (contador >= conceptos.size()) {
			contador = 0;
			concepto_id = conceptos.get(contador).getId();
			return conceptos.get(contador++).getDetalle();
		}
		concepto_id = conceptos.get(contador).getId();
		return conceptos.get(contador++).getDetalle();
	}

	public Boolean hayConcepto() {
		double total = 0.0;
		for (CConcepto c : conceptos) {
			total += deudas.get(c.getId());
		}
		if (total > 0.0)
			return true;
		else
			return false;
	}

	public String cuotas() {
		return cuotas.get(concepto_id);
	}

	public String cuotas(String concepto_id) {
		return cuotas.get(concepto_id);
	}

	public Double getSubtotal() {
		return deudas.get(concepto_id);
	}

	public Double getSubtotal(String concepto_id) {
		return deudas.get(concepto_id);
	}

	public Double getTotal() {
		double total = 0.0;
		for (Double subTotal : deudas.values()) {
			total += subTotal;
		}
		total = total - (2 * deudas.get("bon"));
		return total;
	}

	private int talon;

	public String talon() {
		if (talon <= 8) {
			talon++;
			if (talon == 5)
				return "Tal�n 1 para el Contribuyente";
			else if (talon >= 7) {
				return "Tal�n 1 para el Contribuyente";
			} else
				return "" + talon;
		}
		return "Tal�n 2 para el FTyC";
	}

	@SuppressWarnings("unchecked")
	public String calcularDetalles(BigDecimal numeroBoleto, Long idBoleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b WHERE b.id.boleto.id = :boleto")
				.setParameter("boleto", idBoleto).list();
		capital = 0F;
		compensatorio = 0F;
		moratorioMasPunitorio = 0F;
		for (Bolcon bolcon : bolcons) {
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
				capital = bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
				compensatorio = bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
				moratorioMasPunitorio += bolcon.getImporte().floatValue();
			}
			if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
				moratorioMasPunitorio += bolcon.getImporte().floatValue();
			}
		}
		return "";
	}

	public Float getCapital() {
		return capital;
	}

	public void setCapital(Float capital) {
		this.capital = capital;
	}

	public Float getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(Float compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Float getMoratorioMasPunitorio() {
		return moratorioMasPunitorio;
	}

	public void setMoratorioMasPunitorio(Float moratotioMasPunitorio) {
		this.moratorioMasPunitorio = moratotioMasPunitorio;
	}

	public Float getImporteTotal_2() {
		return compensatorio + capital + moratorioMasPunitorio;
	}

	public String getDomicilio(String manzana, String lote, String barrio) {
		String domicilio = "";
		domicilio += manzana != null && manzana.trim() != "" && !manzana.trim().equals("null") ? "Manzana: " + manzana
				: "Manzana:";
		domicilio += lote != null && lote.trim() != "" && !lote.trim().equals("null") ? " - Lote: " + lote : " - Lote:";
		domicilio += barrio != null && barrio.trim() != "" && !barrio.trim().equals("null") ? " - Barrio: " + barrio
				: " - Barrio:";
		return domicilio;
	}

	public String getDineroEnLetras(double dinero) {
		dinero = Math.rint(dinero * 100D) / 100D;
		return NumberHelper.getAsDinero(dinero);
	}

	public String getNumero(double numero) {
		numero = Math.rint(numero * 100D) / 100D;
		return String.format("%,.2f", numero);
	}

	public String getDinero(Double dinero) {
		dinero = Math.rint(dinero.doubleValue() * 100D) / 100D;
		return NumberHelper.getAsDinero(dinero.doubleValue());
	}

	public String getDoubleStr(Double numero) {
		numero = Math.rint(numero.doubleValue() * 100D) / 100D;
		return String.format("%,.2f", numero.doubleValue());
	}

	public String monedaStr(String s) {
		if (s.equals("Dolar"))
			return "d�lares";
		else if (s.equals("Euro"))
			return "euros";
		else if (s.equals("Peso Argentino"))
			return "pesos";
		else
			return "";
	}

	public String monedaStr2(String s) {
		if (s.equals("2"))
			return "d�lares";
		else if (s.equals("3"))
			return "euros";
		else if (s.equals("1"))
			return "pesos";
		else
			return "";
	}

	public String convertirTipoCuenta(String s) {
		if (s.equals("CC"))
			return "Cuenta Corriente";
		else if (s.equals("CA"))
			return "Caja de Ahorro";
		else
			return "";
	}

}
