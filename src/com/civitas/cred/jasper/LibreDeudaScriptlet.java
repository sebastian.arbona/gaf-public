package com.civitas.cred.jasper;

import java.text.SimpleDateFormat;
import java.util.Date;
import it.businesslogic.ireport.IReportScriptlet;

public class LibreDeudaScriptlet extends IReportScriptlet {

	private SimpleDateFormat dia = new SimpleDateFormat("d");
	private SimpleDateFormat mes = new SimpleDateFormat("MMMMMMMMM");
	private SimpleDateFormat anio = new SimpleDateFormat("yyyy");
	private Date fecha = new Date();
	
	public String getDia(){
		return dia.format(fecha);
	}
	public String getMes(){
		return mes.format(fecha);
	}
	public String getA�o(){
		return anio.format(fecha);
	}

}
