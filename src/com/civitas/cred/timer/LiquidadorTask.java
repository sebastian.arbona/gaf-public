package com.civitas.cred.timer;

import java.util.Calendar;
import java.util.TimerTask;

import com.asf.cred.business.ActualizacionCreditos;
import com.asf.cred.business.CancelarCreditosProcess;
import com.asf.cred.business.GarantiasVencidas;
import com.asf.cred.business.SegurosVencidos;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;

public class LiquidadorTask extends TimerTask {
	String usuario;
	String password;

	public LiquidadorTask(String usuario, String password) {
		super();
		this.usuario = usuario;
		this.password = password;
	}

	@Override
	public void run() {
		try {
			String menu = "actions/process.do?do=process&processName=ActualizacionCreditos";
			try {
				SessionHandler sh = new com.asf.cred.security.SessionHandler();
				sh.register(null);
				// Verifico si tiene permiso para loguearse en el sistema
				if (!sh.login(null, usuario, password)) {
					throw new Exception("Usuario o clave inv�lido");
				}
				// Verifico si tiene permiso para ejecutar la opción de menu.
				if (!sh.isAllowed(menu)) {
					throw new Exception("El usuario no tiene permiso para ejecutar este WebService");
				}
			} catch (Exception e) {
				System.out.println(Calendar.getInstance().getTime().toString()
						+ " Falla al validar permisos de usuario para proceso batch");
				e.printStackTrace();
			}
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			System.out.println(
					Calendar.getInstance().getTime().toString() + " Comienza proceso actualizaci�n de Creditos");
			new ActualizacionCreditos(bp).actualizar();
			System.out.println(Calendar.getInstance().getTime().toString()
					+ " Comienza proceso actualizaci�n de Garantias Vencidas");
			new GarantiasVencidas(bp).ejecutar();
			System.out.println(
					Calendar.getInstance().getTime().toString() + " Comienza proceso de cancelaci�n de Creditos");
			CancelarCreditosProcess cancelarCreditos = new CancelarCreditosProcess(bp);
			cancelarCreditos.cancelar();
			System.out.println(Calendar.getInstance().getTime().toString()
					+ " Comienza proceso actualizaci�n de Seguros Vencidos");
			new SegurosVencidos(bp).ejecutar();
		} catch (Exception e) {
			System.out
					.println(Calendar.getInstance().getTime().toString() + " Falla en proceso batch de actualizaci�n");
		} finally {
			System.out.println(
					Calendar.getInstance().getTime().toString() + " Termina el proceso batch de actualizaci�n");
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			sh.unRegister();
			sh.logout();
		}
	}
}
