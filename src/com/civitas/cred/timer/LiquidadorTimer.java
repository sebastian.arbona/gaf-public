package com.civitas.cred.timer;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import javax.servlet.FilterConfig;

import com.asf.util.iInitiated;

/**
 * Dispara la ejecucion de procesos batch
 * 
 * @author Cesar
 * 
 */
public class LiquidadorTimer implements iInitiated {
	public static Timer timer;

	@Override
	public void destroy() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	/**
	 * Analiza la hora y la configuracion asociada e inicia la ejecucion batch si
	 * corresponde
	 * 
	 * @see com.asf.util.iInitiated#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) {
		if (timer == null) {
			String usr = config.getInitParameter("LiquidadorTask.usr");
			String pass = config.getInitParameter("LiquidadorTask.pass");
			Boolean runNow = new Boolean(config.getInitParameter("LiquidadorTask.runNow"));

			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			if (runNow) {
				c.add(Calendar.SECOND, 30);
			} else {
				if (c.get(Calendar.HOUR_OF_DAY) >= 19)
					c.add(Calendar.DAY_OF_YEAR, 1);
				c.set(Calendar.HOUR_OF_DAY, 19);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
			}
			Timer timer = new Timer();
			Long repetirEn = new Long(3600 * 24 * 1000);
			timer.schedule(new LiquidadorTask(usr, pass), c.getTime(), repetirEn);
		}

	}

}
