/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.civitas.cred.expertos;

/**
 *
 * @author aromeo
 */
public interface IExperto{

    public Object crear( BeanParametros parametros ) throws Exception;
    public Object buscar( BeanParametros parametros );
    public Object modificar( BeanParametros parametros );

}
