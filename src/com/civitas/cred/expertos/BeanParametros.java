/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.civitas.cred.expertos;

/**
 *
 * @author aromeo
 */
public abstract class BeanParametros{

    protected Object[] parametros;

    public BeanParametros( Object... parametros ){
        this.parametros = parametros;
    }

    public Object[] getParametros(){
        return parametros;
    }
}
