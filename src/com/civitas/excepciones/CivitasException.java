package com.civitas.excepciones;

import com.civitas.logger.Log4jConstants;
import com.civitas.logger.LoggerService;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

public abstract class CivitasException extends RuntimeException {

    protected String messageString = "";
    protected String completeMessageString = "";
    protected LoggerService loggerService;

    protected CivitasException() {
        loggerService = LoggerService.getInstance();
        loggerService.log(Log4jConstants.ERROR, "Excepcion No Manejada", this);
    }

    public CivitasException(String message) {
        this.messageString = message;
        this.completeMessageString = message;
        loggerService = LoggerService.getInstance();
        loggerService.log(Log4jConstants.ERROR, completeMessageString);
    }

    public CivitasException(Throwable cause) {
        this("");
        unwrapException(cause);
        loggerService.log(Log4jConstants.ERROR, completeMessageString);
    }

    public CivitasException(String message, Throwable cause) {
        this(message);
        unwrapException(cause);
        loggerService.log(Log4jConstants.ERROR, completeMessageString);
    }

    /**
     * Este metodo se ocupa de llegar hasta la exception causa real. Si no es
     * null, me quedo con el throwableCause que tenia como mi throwableCause. Si
     * es null, me quedo con el mensaje, y el throwableCause queda en null.
     */
    private void unwrapException(Throwable throwableCause) {
        if (throwableCause instanceof InvocationTargetException
                || throwableCause instanceof RemoteException
                || throwableCause instanceof ExceptionInInitializerError
                || throwableCause instanceof CivitasException) {
            this.completeMessageString = this.messageString + "\n" + throwableCause.getMessage();
            initCause(throwableCause.getCause());
        } else {
            initCause(throwableCause);
        }

    }

    @Override
    public synchronized Throwable initCause(Throwable cause) {
        Throwable ret = super.initCause(cause);
        if (cause != null) {
            this.completeMessageString = this.messageString + "\n" + cause.getMessage();
        }
        return ret;
    }

    /**
     * este metodo devuelve el propio mensaje, sin perder si lo hubiera el
     * mensaje de la superclase
     *
     * @return
     */
    public String getCompleteMessage() {
        StringBuffer _stringBuffer = new StringBuffer();
        if (super.getMessage() != null) {
            _stringBuffer.append(super.getMessage() + "\n\n");
        }
        _stringBuffer.append(completeMessageString);
        return _stringBuffer.toString();
    }

    @Override
    public String getMessage() {
        return messageString;
    }
}
