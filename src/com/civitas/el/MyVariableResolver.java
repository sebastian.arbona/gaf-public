package com.civitas.el;

import com.civitas.cred.jasper.OtorgamientoScriptlet;

/*
 * esta es la clase que contiene los objetos que se reemplazan
 * en grh, persona, legajo, ausencia, sistema
 */
public class MyVariableResolver implements VariableResolver {

    public Object resolveVariable(String name) throws ELException {
        ExecutionContext executionContext = ExecutionContext.currentExecutionContext();

        Object value = null;

        if ("persona".equals(name)) {
            value = executionContext.getPersona();
        } else if ("sistema".equals(name)) {
            value = executionContext.getSistema();
        }
        return value;
    }
}
