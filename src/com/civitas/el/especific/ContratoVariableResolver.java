package com.civitas.el.especific;

import com.civitas.el.ELException;
import com.civitas.el.VariableResolver;

public class ContratoVariableResolver implements VariableResolver {

	@Override
	public Object resolveVariable(String name) throws ELException {
		ContratoExecutionContext executionContext = ContratoExecutionContext
				.currentExecutionContext();
		Object value = null;
		if ("fechaContrato".equals(name)) {
			value = executionContext.getFechaStr();
		} else if ("escrito".equals(name)) {
			value = executionContext.getEscrito();
		} else if ("fechaStr".equals(name)) {
			value = executionContext.getFechaStr();
		} else if ("objetoi".equals(name)) {
			value = executionContext.getObjetoi();
		}
		return value;
	}
}
