package com.civitas.el.especific;

import java.util.ArrayList;

import com.civitas.cred.jasper.OtorgamientoScriptlet;
import com.civitas.el.ELException;
import com.civitas.el.VariableResolver;
import com.nirven.creditos.hibernate.Objetoi;

public class ProrrogaVariableResolver implements VariableResolver {

	@Override
	public Object resolveVariable(String name) throws ELException {
		ProrrogaContextoEjecucion executionContext = ProrrogaContextoEjecucion
				.currentExecutionContext();
		Object value = null;
		
		if ("objetoi".equals(name)) {
			value = executionContext.getObjetoi();
		} else if ("persona".equals(name)) {
			value = executionContext.getPersona();
		}else if("telefonoPrincipal".equals(name)){ 	
        	OtorgamientoScriptlet utilidad= new OtorgamientoScriptlet();
        	value=utilidad.telefono(executionContext.getPersona().getContactos());
        	
        }else if("celularPrincipal".equals(name)){
        	OtorgamientoScriptlet utilidad= new OtorgamientoScriptlet();
        	value=utilidad.celular(executionContext.getPersona().getContactos());
        	
        }else if("emailPrincipal".equals(name)){
        	OtorgamientoScriptlet utilidad= new OtorgamientoScriptlet();
        	value=utilidad.email(executionContext.getPersona().getContactos());
        }
		return value;
	}

	
}
