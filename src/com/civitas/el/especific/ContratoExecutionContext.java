package com.civitas.el.especific;

import java.util.List;
import java.util.Stack;

import com.nirven.creditos.hibernate.Escrito;
import com.nirven.creditos.hibernate.Objetoi;

@SuppressWarnings("unchecked")
public class ContratoExecutionContext {

	List<Objetoi> objetosi;
	Objetoi objetoi;
	Escrito escrito;
	String fechaStr;

	static ThreadLocal<Stack<ContratoExecutionContext>> threadLocalContextStack = new ThreadLocal();

	static Stack getContextStack() {
		Stack stack = (Stack) threadLocalContextStack.get();
		if (stack == null) {
			stack = new Stack();
			threadLocalContextStack.set(stack);
		}
		return stack;
	}

	public static void pushCurrentContext(
			ContratoExecutionContext executionContext) {
		getContextStack().push(executionContext);
	}

	public static void popCurrentContext(
			ContratoExecutionContext executionContext) throws Exception {
		if (getContextStack().pop() != executionContext) {
			throw new Exception(
					"current execution context mismatch.  make sure that every pushed context gets popped");
		}
	}

	public static ContratoExecutionContext currentExecutionContext() {
		ContratoExecutionContext executionContext = null;
		Stack stack = getContextStack();
		if (!stack.isEmpty()) {
			executionContext = (ContratoExecutionContext) stack.peek();
		}
		return executionContext;
	}

	public void setObjetosi(List<Objetoi> objetoi) {
		this.objetosi = objetoi;
	}

	public List<Objetoi> getObjetosi() {
		return objetosi;
	}

	public void setEscrito(Escrito escrito1) {
		this.escrito = escrito1;
	}

	public Escrito getEscrito() {
		return this.escrito;
	}

	public void setFechaStr(String fecha) {
		this.fechaStr = fecha;
	}

	public String getFechaStr() {
		return fechaStr;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}
}
