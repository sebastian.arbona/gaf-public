package com.civitas.el.especific;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.civitas.el.ELException;
import com.civitas.el.VariableResolver;
import com.civitas.el.impl.ExpressionEvaluatorImpl;

public class ResolucionExpressionEvaluator {
	private static Log log = LogFactory
			.getLog(ResolucionExpressionEvaluator.class);
	static ExpressionEvaluatorImpl evaluator = new ExpressionEvaluatorImpl();
	static VariableResolver variableResolver = new ResolucionVariableResolver();

	public static Object evaluate(String expression,
			ResolucionExecutionContext ec) {
		Object result = null;
		log.debug("expresion:" + expression == null ? "" : expression);
		ResolucionExecutionContext.pushCurrentContext(ec);
		try {
			VariableResolver usedResolver = null;

			if (variableResolver != null) {
				usedResolver = variableResolver;
			} else {
				usedResolver = new ResolucionVariableResolver();
			}
			expression = findCicles(expression, ec);
			// reemplazo comillas de html por si debo comparar cadenas
			expression = expression.replaceAll("&quot;", "\"");
			String dollarExpression = translateExpressionToDollars(expression);
			result = evaluator.evaluate(dollarExpression, Object.class,
					usedResolver, null);
			//reemplazo comillas de string por comillas de html
			result = ((String)result).replaceAll("\"","&quot;");
		} catch (ELException e) {
			log.info("Error al evaluar la expresion:" + expression);
			e.printStackTrace();

		} finally {
			try {
				ResolucionExecutionContext.popCurrentContext(ec);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@SuppressWarnings("static-access")
	private static String findCicles(String dollarExpression,ResolucionExecutionContext eC) {
		int indexS = 0;
		int indexF = 0;
		int lenghInt = dollarExpression.length();
		if(!dollarExpression.contains("#{")){
			return dollarExpression;
		}
		for (int i = 0; i < lenghInt; i++) {
			if (dollarExpression.indexOf("#{") >=0) {
				indexS = dollarExpression.indexOf("#{", indexS);
				indexF = dollarExpression.indexOf("#}", indexF);
				String subString = dollarExpression.substring(indexS + 2,
						indexF);
				int rep = repeticiones(subString);
				String str = new String();
				for (int j = 0; j < rep; j++) {
					String armado;
					{
						ResolucionExecutionContext eCo = new ResolucionExecutionContext();
						ResolucionExpressionEvaluator mEv = new ResolucionExpressionEvaluator();
						eCo.setObjetoi(eC.getObjetosi().get(j));
						eCo.setEscrito(eC.getEscrito());
						eCo.setFechaStr(eC.getFechaStr());
						eCo.setResolucion(eC.getResolucion());
						armado = (String) mEv.evaluate(subString, eCo);	
					}
					str = str.concat(armado);
				}
				String expresion = new String();
				expresion = dollarExpression.substring(0, indexS).concat(str)
						.concat(dollarExpression.substring(indexF + 2, dollarExpression.length()));
				dollarExpression = expresion;
			}else{
				break;				
			}
		}
		return dollarExpression;
	}

	private static int repeticiones(String subString) {
		int rep = 0;
		String name = subString.substring(subString.indexOf("${") + 2,
				subString.indexOf(".", subString.indexOf("${") + 2));
		ResolucionVariableResolver vr = new ResolucionVariableResolver();
		try {
			rep = vr.resolveLength(name, subString);
		} catch (ELException e) {
			e.printStackTrace();
		}
		return rep;
	}

	static String translateExpressionToDollars(String expression) {
		char[] chars = expression.toCharArray();
		int index = 0;
		while (index != -1) {
			index = expression.indexOf("#{", index);
			if (index != -1) {
				chars[index] = '$';
				index++;
			}
		}
		return new String(chars);
	}
}
