package com.civitas.el.especific;

import java.util.ArrayList;

import com.civitas.el.ELException;
import com.civitas.el.VariableResolver;
import com.nirven.creditos.hibernate.Objetoi;

public class ResolucionVariableResolver implements VariableResolver {

	@Override
	public Object resolveVariable(String name) throws ELException {
		ResolucionExecutionContext executionContext = ResolucionExecutionContext
				.currentExecutionContext();
		Object value = null;

		if ("fechaResolucion".equals(name)) {
			value = executionContext.getFechaStr();
		} else if ("escrito".equals(name)) {
			value = executionContext.getEscrito();
		} else if ("objetoi".equals(name)) {
			value = executionContext.getObjetoi();
		} else if ("resolucion".equals(name)) {
			value = executionContext.getResolucion();
		}
		return value;
	}

	/*
	 * Este metodo sirve para traer la cantidad de objetos que tiene un array
	 * list en en el execution context a partir de un nombre
	 */
	public int resolveLength(String name, String substring) throws ELException {
		ResolucionExecutionContext executionContext = ResolucionExecutionContext
				.currentExecutionContext();
		int value = 0;
		try {
			if ("objetoi".equals(name)) {
				value = ((ArrayList<Objetoi>) executionContext.getObjetosi())
						.size();
			} else {
				value = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

}
