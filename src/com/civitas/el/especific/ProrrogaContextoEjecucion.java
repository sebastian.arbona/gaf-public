package com.civitas.el.especific;

import java.util.Stack;



import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Escrito;
import com.nirven.creditos.hibernate.Objetoi;
import com.civitas.hibernate.persona.Persona;
 

public class ProrrogaContextoEjecucion {
	

    Persona persona;
    Objetoi objetoi;
   
    public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    static ThreadLocal<Stack<ProrrogaContextoEjecucion>> threadLocalContextStack = new ThreadLocal();

	static Stack getContextStack() {
		Stack stack = (Stack) threadLocalContextStack.get();
		if (stack == null) {
			stack = new Stack();
			threadLocalContextStack.set(stack);
		}
		return stack;
	}

	public static void pushCurrentContext(
			ProrrogaContextoEjecucion executionContext) {
		getContextStack().push(executionContext);
	}

	public static void popCurrentContext(
			ProrrogaContextoEjecucion executionContext) throws Exception {
		if (getContextStack().pop() != executionContext) {
			throw new Exception(
					"current execution context mismatch.  make sure that every pushed context gets popped");
		}
	}

	public static ProrrogaContextoEjecucion currentExecutionContext() {
		ProrrogaContextoEjecucion executionContext = null;
		Stack stack = getContextStack();
		if (!stack.isEmpty()) {
			executionContext = (ProrrogaContextoEjecucion) stack.peek();
		}
		return executionContext;
	}


}
