package com.civitas.el.especific;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.civitas.el.ELException;
import com.civitas.el.VariableResolver;
import com.civitas.el.impl.ExpressionEvaluatorImpl;

public class ContratoExpressionEvaluator {
	private static Log log = LogFactory
			.getLog(ContratoExpressionEvaluator.class);
	static ExpressionEvaluatorImpl evaluator = new ExpressionEvaluatorImpl();
	static VariableResolver variableResolver = new ContratoVariableResolver();

	public static Object evaluate(String expression, ContratoExecutionContext ec) {
		Object result = null;
		log.debug("expresion:" + expression == null ? "" : expression);
		ContratoExecutionContext.pushCurrentContext(ec);
		try {
			VariableResolver usedResolver = null;
			if (variableResolver != null) {
				usedResolver = variableResolver;
			} else {
				usedResolver = new ContratoVariableResolver();
			}
			// reemplazo comillas de html por si debo comparar cadenas
			expression = expression.replaceAll("&quot;", "\"");
			String dollarExpression = translateExpressionToDollars(expression);
			result = evaluator.evaluate(dollarExpression, Object.class,
					usedResolver, null);
			//reemplazo comillas de string por comillas de html
			result = ((String)result).replaceAll("\"","&quot;");
		} catch (ELException e) {
			log.info("Error al evaluar la expresion:" + expression);
			e.printStackTrace();
		} finally {
			try {
				ContratoExecutionContext.popCurrentContext(ec);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	static String translateExpressionToDollars(String expression) {
		char[] chars = expression.toCharArray();
		int index = 0;
		while (index != -1) {
			index = expression.indexOf("#{", index);
			if (index != -1) {
				chars[index] = '$';
				index++;
			}
		}
		return new String(chars);
	}
}
