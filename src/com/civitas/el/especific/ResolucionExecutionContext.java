package com.civitas.el.especific;

import java.util.ArrayList;
import java.util.Stack;

import com.nirven.creditos.hibernate.Escrito;
import com.nirven.creditos.hibernate.Objetoi;

@SuppressWarnings("unchecked")
public class ResolucionExecutionContext {

	private ArrayList<Objetoi> objetosi;
	Objetoi objetoi;
	Escrito escrito;
	String fechaStr;
	String resolucion;
	String resolucionDigesto;
	

	static ThreadLocal<Stack<ResolucionExecutionContext>> threadLocalContextStack = new ThreadLocal();

	static Stack getContextStack() {
		Stack stack = (Stack) threadLocalContextStack.get();
		if (stack == null) {
			stack = new Stack();
			threadLocalContextStack.set(stack);
		}
		return stack;
	}

	public static void pushCurrentContext(
			ResolucionExecutionContext executionContext) {
		getContextStack().push(executionContext);
	}

	public static void popCurrentContext(
			ResolucionExecutionContext executionContext) throws Exception {
		if (getContextStack().pop() != executionContext) {
			throw new Exception(
					"current execution context mismatch.  make sure that every pushed context gets popped");
		}
	}

	public static ResolucionExecutionContext currentExecutionContext() {
		ResolucionExecutionContext executionContext = null;
		Stack stack = getContextStack();
		if (!stack.isEmpty()) {
			executionContext = (ResolucionExecutionContext) stack.peek();
		}
		return executionContext;
	}

	public void setEscrito(Escrito escrito1) {
		this.escrito = escrito1;
	}

	public Escrito getEscrito() {
		return this.escrito;
	}

	public void setFechaStr(String fecha) {
		this.fechaStr = fecha;
	}

	public String getFechaStr() {
		return fechaStr;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetosi(ArrayList<Objetoi> objetosi) {
		this.objetosi = objetosi;
	}

	public ArrayList<Objetoi> getObjetosi() {
		return objetosi;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public String getResolucionDigesto() {
		return resolucionDigesto;
	}

	public void setResolucionDigesto(String resolucionDigesto) {
		this.resolucionDigesto = resolucionDigesto;
	}
	
}
