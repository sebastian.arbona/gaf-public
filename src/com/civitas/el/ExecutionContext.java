package com.civitas.el;

import com.civitas.hibernate.persona.Persona;
import java.util.Stack;

public class ExecutionContext {

	Persona persona;
	InformacionSistema sistema;
	static ThreadLocal<Stack<ExecutionContext>> threadLocalContextStack = new ThreadLocal();

	static Stack getContextStack() {
		Stack stack = (Stack) threadLocalContextStack.get();
		if (stack == null) {
			stack = new Stack();
			threadLocalContextStack.set(stack);
		}
		return stack;
	}

	public static void pushCurrentContext(ExecutionContext executionContext) {
		getContextStack().push(executionContext);
	}

	public static void popCurrentContext(ExecutionContext executionContext) throws Exception {
		if (getContextStack().pop() != executionContext) {
			throw new Exception("current execution context mismatch.  make sure that every pushed context gets popped");
		}
	}

	public static ExecutionContext currentExecutionContext() {
		ExecutionContext executionContext = null;
		Stack stack = getContextStack();
		if (!stack.isEmpty()) {
			executionContext = (ExecutionContext) stack.peek();
		}
		return executionContext;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public InformacionSistema getSistema() {
		if (this.sistema == null) {
			sistema = new InformacionSistema();
		}

		return sistema;
	}

	public void setSistema(InformacionSistema sistema) {
		this.sistema = sistema;
	}
}
