/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.civitas.el;

import com.asf.cred.business.ParametrosGralReport;
import com.asf.util.DateHelper;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author aortega
 */
public class InformacionSistema {

    ParametrosGralReport pg = new ParametrosGralReport();

    public InformacionSistema() {
    }

    public String getAnioStr() {
        return Calendar.getInstance().getDisplayName(Calendar.YEAR, Calendar.LONG, Locale.getDefault());
    }

    public String getDiaStr() {
        return Calendar.getInstance().getDisplayName(Calendar.DAY_OF_MONTH, Calendar.SHORT, Locale.getDefault());
    }

    public Date getFecha() {
        return Calendar.getInstance().getTime();
    }

    public String getFechaStr() {
        return DateHelper.getString(Calendar.getInstance().getTime());
    }

    public String getHora() {
        return Calendar.getInstance().getDisplayName(Calendar.HOUR_OF_DAY, Calendar.LONG, Locale.getDefault());
    }

    public String getMesStr() {
        return Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
    }

    public String getEmpresa() {
        return pg.getNombreCliente().replaceAll("EMPRESA=", "");
    }

    public String getCuit() {
        return pg.getCuitCliente();
    }

    public String getDireccionEmpresa() {
        return pg.getDirCliente();
    }

    public String getVariables() {

        String metodosStr = null;
        Method metodos[] = this.getClass().getDeclaredMethods();

        for (int i = 0; i < metodos.length; i++) {
            metodosStr += metodos[i].getName();
        }
        return metodosStr;
    }
}
