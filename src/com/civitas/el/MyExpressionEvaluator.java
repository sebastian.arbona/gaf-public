package com.civitas.el;

import com.civitas.el.impl.ExpressionEvaluatorImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MyExpressionEvaluator {

    private static Log log = LogFactory.getLog(MyExpressionEvaluator.class);
    static ExpressionEvaluatorImpl evaluator = new ExpressionEvaluatorImpl();
    static VariableResolver variableResolver = new MyVariableResolver();

    public static Object evaluate(String expression, ExecutionContext ec) {
        Object result = null;
        log.debug("expresion:" + expression == null ? "" : expression);
        ExecutionContext.pushCurrentContext(ec);

        try {
            VariableResolver usedResolver = null;

            if (variableResolver != null) {
                usedResolver = variableResolver;
            } else {
                usedResolver = new MyVariableResolver();
            }

            String dollarExpression = translateExpressionToDollars(expression);

            result = evaluator.evaluate(dollarExpression, Object.class, usedResolver, null);

        } catch (ELException e) {
            log.info("Error al evaluar la expresion:" + expression);
            e.printStackTrace();

        } finally {
            try {
                ExecutionContext.popCurrentContext(ec);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    static String translateExpressionToDollars(String expression) {
        char[] chars = expression.toCharArray();
        int index = 0;
        while (index != -1) {
            index = expression.indexOf("#{", index);
            if (index != -1) {
                chars[index] = '$';
                index++;
            }
        }
        return new String(chars);
    }
}
