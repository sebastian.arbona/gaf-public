package com.civitas.webservices;

import java.util.ArrayList;

import javax.jws.WebMethod;

import org.codehaus.xfire.XFireException;
import org.codehaus.xfire.fault.XFireFault;

import com.asf.gaf.hibernate.ExtractoBancario;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.civitas.importacion.PagoCreditoBean;

public class ExtractosBancariosGICService {
	
	public ExtractosBancariosGICService() {
	}

	@SuppressWarnings("unchecked")
	@WebMethod
	public String nuevosExtractos() throws XFireException{
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			String consulta = "SELECT e FROM ExtractoBancario e WHERE e.procesoGIC IS NULL OR e.procesoGIC = 0";
			ArrayList<ExtractoBancario> extractos = (ArrayList<ExtractoBancario>) bp.getByFilter(consulta);
			Boolean resultado = procesarExtractos(extractos);
			if (resultado) {
				return "OK";
			} else {
				throw new XFireFault("ERROR", XFireFault.SENDER);
			}
		} finally {
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			sh.unRegister();
			sh.logout();
		}
}

	private Boolean procesarExtractos(ArrayList<ExtractoBancario> extractos) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Boolean resultado = true;
		try {
			ArrayList<PagoCreditoBean> pagos = new ArrayList<PagoCreditoBean>();
			for (ExtractoBancario extractoBancario : extractos) {
				PagoCreditoBean bean = new PagoCreditoBean();
				Double importe = extractoBancario.getImporte();
				bean.setNumeroProyecto(extractoBancario.getCodigoFondoStr());
				bean.setImporte(importe);
				extractoBancario.setProcesoGIC(true); 
				pagos.add(bean);   
				bp.saveOrUpdate(extractoBancario);
			}
			
		} catch (Exception e) {
			resultado = false;
		}
		return resultado;
	}
}
