/**
 * 
 */
package com.civitas.dto;

import com.nirven.creditos.hibernate.Objetoi;

/**
 * @author nretamales
 *
 */
public class EstadoNotificacionDTO {

	private Objetoi proyecto;
	private String estadoEnvio;
	private String relacion;
	private String email;

	public void setEstado(Objetoi proyecto, String estadoEnvio, String relacion, String email) {
		this.setEstadoEnvio(estadoEnvio);
		this.setProyecto(proyecto);
		this.setRelacion(relacion);
		this.setEmail(email);
	}

	public Objetoi getProyecto() {
		return proyecto;
	}

	public void setProyecto(Objetoi proyecto) {
		this.proyecto = proyecto;
	}

	public String getEstadoEnvio() {
		return estadoEnvio;
	}

	public void setEstadoEnvio(String estadoEnvio) {
		this.estadoEnvio = estadoEnvio;
	}

	public String getRelacion() {
		return relacion;
	}

	public void setRelacion(String relacion) {
		this.relacion = relacion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
