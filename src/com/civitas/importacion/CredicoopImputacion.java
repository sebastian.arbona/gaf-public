package com.civitas.importacion;

import java.text.ParseException;
import java.util.Date;

public class CredicoopImputacion {

	private CredicoopSuboperacion suboperacion;
	private Integer secuencia;
	private Double importe;
	private String tipoComprobante;
	private String numeroComprobante;
	private Long numeroCliente;
	private Double montoVencimiento;
	private Date fechaVencimiento;
	
	public static Long parseSuboperacion(String l) {
		return new Long(l.substring(9, 17));
	}
	
	public CredicoopImputacion(String l, CredicoopSuboperacion suboperacion) {
		this.suboperacion = suboperacion;
		this.secuencia = new Integer(l.substring(17, 21));
		this.importe = new Double(l.substring(21, 32)) / 100;
		this.tipoComprobante = l.substring(32, 34);
		this.numeroComprobante = l.substring(34, 50);
		this.numeroCliente = new Long(l.substring(50, 58));
		this.montoVencimiento = new Double(l.substring(58, 69)) / 100;
		try {
			this.fechaVencimiento = ImportarBcoCredicop.DATE_FORMAT.parse(l.substring(69, 77));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public CredicoopSuboperacion getSuboperacion() {
		return suboperacion;
	}

	public Integer getSecuencia() {
		return secuencia;
	}

	public Double getImporte() {
		return importe;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public String getNumeroComprobante() {
		return numeroComprobante;
	}

	public Long getNumeroCliente() {
		return numeroCliente;
	}

	public Double getMontoVencimiento() {
		return montoVencimiento;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	
	
}
