/*
 * Validador.java
 *
 * Created on 24 de abril de 2007, 14:55
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.civitas.importacion;

import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.civitas.importacion.abstracts.AbstractFactory;
import com.civitas.importacion.abstracts.IValidador;
import com.civitas.importacion.errores.Errores;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Objetoi;

public class ValidadorBoleto implements IValidador {

    private static Log log = LogFactory.getLog(ValidadorBoleto.class);
    private Errores error = null;
    @SuppressWarnings("unused")
	private BoletosFactory boletoFactory = null;

    /** Creates a new instance of Validador */
    public ValidadorBoleto(AbstractFactory boletoFactory) {
        this.error = Errores.getInstancia();
        this.boletoFactory = (BoletosFactory) boletoFactory;
    }

    @SuppressWarnings("unchecked")
	public SortedMap validar(SortedMap resultado, HashMap hsVariables) {
        SortedMap validados = new TreeMap();
        
        double importeTotal = 0;
        long totalRegistros = 0;
        for (Object key : resultado.keySet()) {
            PagoCreditoBean bean = (PagoCreditoBean) resultado.get(key);
            if (validar(bean, (Long)key)) {
                validados.put(key, bean);
            }
            importeTotal += bean.getImporte();
            totalRegistros++;
        }
        
        return validados;
    }

    @SuppressWarnings({ "finally", "static-access" })
    private boolean validar(PagoCreditoBean bean, Long linea) {
        boolean ok = true;
        
        BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        try{
        	Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion").setLong("nroAtencion", bean.getNumeroProyectoLong()).uniqueResult();
            
        	if(credito != null){
        	bp.getCurrentSession().evict(credito);
            bean.setCredito(credito);
        	}else{
        		Boleto boleto = (Boleto)bp.createQuery("SELECT b FROM Boleto b WHERE b.id.periodoBoleto = :periodo AND b.id.numeroBoleto = :numero AND b.id.verificadorBoleto = :verificador")
        		.setParameter("periodo", bean.getPeriodoBoleto())
        		.setParameter("numero",bean.getNumeroBoleto())
        		.setParameter("verificador",bean.getVerificadorBoleto())
        		.uniqueResult();
        	if (boleto == null) {
        		return false;
        	}
        	bp.getCurrentSession().evict(boleto);
        	bean.setBoleto(boleto);
        }
        }catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.debug("Excepci�n al cargar el Boleto. "+bean.getNumeroProyecto(), e);
                
            }
            e.printStackTrace();
            return false;
        }

        finally {
            if (!ok) {
                if (!error.containsKey(ID, linea)) {
                    error.put(this.ID, linea, "Los datos no pasaron las validaciones.");
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("Linea: " + linea + " valor: " + ok + " boleto: " + bean.getNumeroProyecto());
            }
            return ok;
        }
    }

}

