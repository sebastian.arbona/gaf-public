package com.civitas.importacion;

import java.text.ParseException;
import java.util.Date;

public class CredicoopOperacion {

	private Long numero;
	private Date fecha;
	private String sucursal;
	private String estado;
	private String canal;
	private String situacion;
	private Long numeroCliente;
	
	public CredicoopOperacion(String l) {
		this.numero = new Long(l.substring(3, 9));
		
		try {
			this.fecha = ImportarBcoCredicop.DATE_FORMAT.parse(l.substring(9, 17));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.sucursal = l.substring(17, 20);
		this.estado = l.substring(20, 22);
		this.canal = l.substring(22, 24);
		this.situacion = l.substring(24, 25);
		this.numeroCliente = new Long(l.substring(25, 33));
	}

	public Long getNumero() {
		return numero;
	}

	public Date getFecha() {
		return fecha;
	}

	public String getSucursal() {
		return sucursal;
	}

	public String getEstado() {
		return estado;
	}

	public String getCanal() {
		return canal;
	}

	public String getSituacion() {
		return situacion;
	}

	public Long getNumeroCliente() {
		return numeroCliente;
	}
	
	
}
