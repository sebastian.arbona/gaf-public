package com.civitas.importacion;

import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.client.Client;
import org.hibernate.SQLQuery;

import com.asf.cred.business.ProgressStatus;
import com.asf.cred.struts.form.ImportacionForm;
import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.Mediopago;
import com.asf.hibernate.mapping.Formatoimportacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DirectorHelper;
import com.civitas.importacion.abstracts.AbstractFactory;
import com.civitas.importacion.abstracts.AbstractFormatos;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.DetalleArchivo;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.DetalleRecaudacionInv;
import com.nirven.creditos.hibernate.EstadoCheque;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Usuario;

/**
 * Realiza la Importación propiamente dicha.
 * 
 */
public class ImportarBcoCredicop extends AbstractFormatos {

	public static final String DIRECTOR_CODIBA_CREDICOOP = "cbcredic";
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("ddMMyyyy");

	private BusinessPersistance bp;

	private Bancos enteRecaudador;
	private Long idBancue;
	private Date fechaActual;
	private Usuario usuario;
	private List<DetalleRecaudacion> detallesRecaudacion;
	private String txt;
	private DetalleArchivo detalleArchivoNuevo;
	private ImportacionForm iForm;
	private int registrosNuevos;
	private int registrosExistentes;
	private int registrosInvalidos;
	private Objetoi objetoi;

	private Map<Long, CredicoopOperacion> operaciones;
	private Map<Long, CredicoopSuboperacion> suboperaciones;
	private List<CredicoopValor> valores;
	private List<CredicoopImputacion> imputaciones;

	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog(ImportarBcoCredicop.class);
	@SuppressWarnings("unused")
	private BoletosFactory boletosFactory = (BoletosFactory) super.fabrica;

	private SQLQuery correlacionQuery;

	public ImportarBcoCredicop(Formatoimportacion formatoImportacion, AbstractFactory fabrica) {
		super(formatoImportacion, fabrica);
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		iForm = (ImportacionForm) SessionHandler.getCurrentSessionHandler().getRequest()
				.getAttribute("ImportacionForm");
	}

	@SuppressWarnings({ "rawtypes" })
	public SortedMap parse(SortedMap<Long, Map<String, String>> hsLineas) {
		if (!hsLineas.isEmpty())
			ProgressStatus.iTotal = hsLineas.size();
		else
			return new TreeMap();

		SortedMap resultado = new TreeMap();
		Map hsValores;
		Collection valores;
		String linea;

		operaciones = new TreeMap<Long, CredicoopOperacion>();
		suboperaciones = new TreeMap<Long, CredicoopSuboperacion>();
		this.valores = new ArrayList<CredicoopValor>();
		imputaciones = new ArrayList<CredicoopImputacion>();

		correlacionQuery = bp
				.createSQLQuery("SELECT NumeroSolicitud FROM CORRELACION_CREDITO_GIC WHERE idProyecto = ?");

		for (Long key : hsLineas.keySet()) {// itero sobre la linea
			hsValores = hsLineas.get(key);
			valores = hsValores.values();
			if (valores.isEmpty()) {
				continue;
			}
			linea = valores.iterator().next().toString();

			if (linea.startsWith("O")) {
				CredicoopOperacion o = new CredicoopOperacion(linea);
				operaciones.put(o.getNumero(), o);
			} else if (linea.startsWith("S")) {
				CredicoopSuboperacion s = new CredicoopSuboperacion(linea,
						operaciones.get(CredicoopSuboperacion.parseOperacion(linea)));
				suboperaciones.put(s.getNumero(), s);
			} else if (linea.startsWith("V")) {
				CredicoopSuboperacion s = suboperaciones.get(CredicoopValor.parseSuboperacion(linea));
				CredicoopValor v = new CredicoopValor(linea, s);
				s.addValor(v);
				this.valores.add(v);
			} else if (linea.startsWith("I")) {
				CredicoopSuboperacion s = suboperaciones.get(CredicoopImputacion.parseSuboperacion(linea));
				CredicoopImputacion i = new CredicoopImputacion(linea, s);
				s.addImputacion(i);
				this.imputaciones.add(i);
			}

//			if (!linea.startsWith("I")) { // solo importan imputaciones
//				if(linea.startsWith("O")){
//					beanRegistroO = new PagoCreditoBean();
//					fechaStr = linea.substring(9, 17);
//					fechaStr = fechaStr.substring(0, 2) 
//								+ "/"
//								+ fechaStr.substring(2, 4)
//								+ "/"
//								+ fechaStr.substring(4, 8);
//					beanRegistroO.setFechaPago(DateHelper.getDate(fechaStr));
//				}
//				continue;
//			} 
//			bean = new PagoCreditoBean();
//			importeStr = linea.substring(21, 32);
//			comprobante = linea.substring(34, 50);
//			importe = Double.parseDouble(importeStr) / 100;
//			bean.setNumeroProyecto(comprobante);
//			bean.setImporte(importe);
//			bean.setFechaPago(beanRegistroO.getFechaPago());
//			resultado.put(key, bean);
		}

		String codiBa = DirectorHelper.getString(DIRECTOR_CODIBA_CREDICOOP);
		String consulta = "SELECT b FROM Bancos b WHERE b.codiBa = :codiBa AND b.recaudador = 1";
		enteRecaudador = (Bancos) bp.createQuery(consulta).setParameter("codiBa", new Long(codiBa)).list().get(0);
		idBancue = new Long(bp.getByFilter("SELECT d.valor FROM Director d WHERE d.id='idBancueCredicoop'").get(0)
				.toString().trim());
		Caratula caratulaActual = (Caratula) bp.getById(com.nirven.creditos.hibernate.Caratula.class,
				iForm.getIdCaratula());

		caratulaActual.setBanco(enteRecaudador);

		fechaActual = new Date();
		usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
//		crearExtractosBancarios(hsLineas);

		bp.begin();
		bp.update(caratulaActual);
		crearDetalleArchivo();

		crearDetalleRecaudacion(hsLineas);

		String resumen = "<br>Cantidad de registros importados: " + (registrosNuevos + registrosExistentes)
				+ "<br>Entidad Recaudadora: " + enteRecaudador.getDetaBa() + "<br>Registros Nuevos: " + registrosNuevos
				+ "<br>Registros Existentes: " + registrosExistentes;
		if (registrosInvalidos > 0) {
			resumen += "<br>Registros Inválidos: " + registrosInvalidos;
		}

		resumen += "<br>Monto Total $: " + detalleArchivoNuevo.getTotalArchivo();

		iForm.setResumen(resumen);
//		if(importado){
		iForm.setIdDetalleArchivo(detalleArchivoNuevo.getId());
		detalleArchivoNuevo.setTxt(txt);
		bp.update(detalleArchivoNuevo);
//		}else{
//			if(detalleArchivoAnterior != null){
//				ImportacionForm iForm = (ImportacionForm)SessionHandler.getCurrentSessionHandler().getRequest().getAttribute("ImportacionForm");
//				iForm.setIdDetalleArchivo(detalleArchivoAnterior.getId());
//			}
//			bp.delete(detalleArchivoNuevo);
//		}
		try {
			bp.commit();
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
		}
		ImportacionForm iForm = (ImportacionForm) SessionHandler.getCurrentSessionHandler().getRequest()
				.getAttribute("ImportacionForm");
		iForm.setTodoOk(!(registrosNuevos == 0 && registrosExistentes == 0 && registrosInvalidos == 0));

//		try {
//			activarExtractosGAF();
//		} catch (Exception e) {
//			System.out.println("***Error al activar la importación en GAF***");
//		}
		return resultado;
	}

//	@SuppressWarnings("unused")
//	private void activarExtractosGAF() {
//		try {
//			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
//			String url = DirectorHelper.getString("URL.GAF")
//					+ "/services/ExtractosBancariosGAFService?wsdl";
//			Client c = WebServiceHelper.getCliente(url);
//			Service model = c.getService();
//			AegisBindingProvider bp = (AegisBindingProvider) model
//					.getBindingProvider();
//			TypeMapping typeMapping = bp.getTypeMapping(model);
//			c.addOutHandler(sh.getAutenticacionClienteHandler());
//			Object[] params = new Object[0];
//			c.invoke("nuevosExtractos", params);
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//			System.out.println("***Error al activar la importación en GAF***");
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println("***Error al activar la importación en GAF***");
//		}
//	}

//	private ArrayList<String> armarlineasParaExtracto(List<String> lineasStr) {
//		ArrayList<String> registros = new ArrayList<String>();
//		ArrayList<String> operaciones = new ArrayList<String>();
//		ArrayList<String> suboperaciones = new ArrayList<String>();
//		ArrayList<String> valores = new ArrayList<String>();
//		ArrayList<String> imputaciones = new ArrayList<String>();
//		for (String linea : lineasStr) {
//			if (linea.startsWith("O")) {
//				operaciones.add(linea);
//			} else if (linea.startsWith("S")) {
//				suboperaciones.add(linea);
//			} else if (linea.startsWith("I")) {
//				imputaciones.add(linea);
//			} else if (linea.startsWith("V")) {
//				valores.add(linea);
//			} else {
//				continue;
//			}
//		}
//		for (String operacion : operaciones) {
//			if (operacion == null) {
//				continue;
//			}
//			String campoC = "";
//			campoC = operacion;
//			String nroOperacion = operacion.substring(1, 9);
//			for (String suboperacion : suboperaciones) {
//				if (suboperacion.substring(1, 9).equals(nroOperacion)) {
//					String campoS = suboperacion;
//					String nroSuboperacion = suboperacion.substring(9, 17);
//					String medioPago = suboperacion.substring(17, 19);
//					// los valores de los tipos de medio de pago estan
//					// harcodeados
//					// 01-Efectivo 06-Transf 07-Retencion
//					if (medioPago.equals("01") || medioPago.equals("06")
//							|| medioPago.equals("07")) {
//						for (String imputacion : imputaciones) {
//							if (imputacion.substring(9, 17).equals(
//									nroSuboperacion)) {
//								String registro = campoC + "-" + campoS + "-"
//										+ imputacion;
//								registros.add(registro);
//							}
//						}
//					}// if medio pago efectivo
//					// 02-Chq propio 03-Chq Tercero 04-Chq Dif Propio 05-Chq Dif
//					// Terceros
//					else if (medioPago.equals("02") || medioPago.equals("03")
//							|| medioPago.equals("04") || medioPago.equals("05")) {
//						for (String valor : valores) {
//							if (valor.substring(9, 17).equals(nroSuboperacion)) {
//								String registro = campoC + "-" + campoS + "-"
//										+ valor;
//								registros.add(registro);
//							}
//						}
//					}// if medio de pago valor
//				}// if misma operacion
//			}// for suboperacion
//		}// for operaciones
//		return registros;
//	}

	@Override
	public boolean isLineaCompleta() {
		return true;
	}

	private void crearDetalleRecaudacion(SortedMap<Long, Map<String, String>> hsLineas) {
//		lineasStr = new ArrayList<String>();
//		txt = "";
//		for (Map<String, String> iterador : hsLineas.values()) {
//			cadena = "";
//			for (String string : iterador.values()) {
//				cadena += string;
//			}
//			lineasStr.add(cadena);
//			txt += cadena;
//		}
//		HashMap<String, String> registrosMap = armarlineasParaDetalleRecaudacion(lineasStr);
//		if(registrosMap.size() > 0)
//			ProgressStatus.iTotal = registrosMap.size();

		ProgressStatus.iTotal = suboperaciones.size();

//		for(Entry<String,String> registro : registrosMap.entrySet()){
		for (CredicoopSuboperacion s : suboperaciones.values()) {
			guardarDetalleRecaudacion(s);
			ProgressStatus.iProgress++;
		}
//		}			
	}

	@SuppressWarnings("unchecked")
	private void guardarDetalleRecaudacion(CredicoopSuboperacion s) {

		if (!s.getValores().isEmpty()) {

			CredicoopImputacion i = s.getImputaciones().get(0); // suponemos que viene una sola I cuando hay V's

			// cheques
			for (CredicoopValor v : s.getValores()) {
				DetalleRecaudacion detalleRecaudacion = new DetalleRecaudacion();
				detalleRecaudacion.setOrigen(DetalleRecaudacion.ORIGEN_TXT);
				detalleRecaudacion.setBanco(enteRecaudador);
				detalleRecaudacion.getCuentaBancaria().setId(idBancue);
				detalleRecaudacion.setDetalleArchivo(detalleArchivoNuevo);

				detalleRecaudacion.setNumeroProyecto(new Long(i.getNumeroComprobante()));

				detalleRecaudacion.setFechaRecaudacion(s.getOperacion().getFecha());
				detalleRecaudacion.setCodigoMovimiento(s.getMedioPago());

				detalleRecaudacion.getMoneda().setId(new Long(s.getMoneda()));

				detalleRecaudacion.setNumeroOperacion(s.getOperacion().getNumero());

				detalleRecaudacion.setNumeroValor(v.getNumeroValor());

				detalleRecaudacion.setImporte(v.getImporte());

				detalleRecaudacion.setFechaAcreditacionValor(detalleRecaudacion.getFechaRecaudacion());

				String estadoChequeStr = v.getEstado();
				estadoChequeStr = estadoChequeStr.equals("  ") ? EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO
						: estadoChequeStr.trim();

				// buscar en tabla correlacion por numero informado
				Number numeroAtencion = (Number) correlacionQuery
						.setParameter(0, new Long(i.getNumeroComprobante()).toString()).uniqueResult();
				if (numeroAtencion != null) {
					objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
							.setLong("nroAtencion", numeroAtencion.longValue()).uniqueResult();
					detalleRecaudacion.setNumeroCorrelacion(new Long(i.getNumeroComprobante()));
					detalleRecaudacion.setNumeroProyecto(numeroAtencion.longValue());
				} else {
					objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
							.setLong("nroAtencion", detalleRecaudacion.getNumeroProyecto()).uniqueResult();
					detalleRecaudacion.setNumeroCorrelacion(detalleRecaudacion.getNumeroProyecto());
				}

				if (objetoi == null) {
					registrosInvalidos++;

					String q = "select d from DetalleRecaudacionInv d where " + "d.banco = :banco and "
							+ "d.cuentaBancaria.id = :idBancue and " + "d.importe = :importe and "
							+ "d.fechaAcreditacionValor = :fechaAcreditacionValor and "
							+ "d.fechaRecaudacion = :fechaRecaudacion and " + "d.numeroProyecto = :numeroProyecto and "
							+ "d.numeroValor = :numeroValor ";

					List<DetalleRecaudacionInv> dinvs = bp.createQuery(q).setParameter("banco", enteRecaudador)
							.setParameter("idBancue", idBancue).setParameter("importe", v.getImporte())
							.setParameter("fechaAcreditacionValor", s.getOperacion().getFecha())
							.setParameter("fechaRecaudacion", s.getOperacion().getFecha())
							.setParameter("numeroProyecto", new Long(i.getNumeroComprobante()))
							.setParameter("numeroValor", v.getNumeroValor()).list();
					if (!dinvs.isEmpty()) {
						for (DetalleRecaudacionInv di : dinvs) {
							di.setDetalleArchivo(detalleArchivoNuevo);
							bp.update(di);
						}
						return;
					}

					DetalleRecaudacionInv dinv = new DetalleRecaudacionInv();
					dinv.setBanco(enteRecaudador);
					dinv.getCuentaBancaria().setId(idBancue);
					dinv.setDetalleArchivo(detalleArchivoNuevo);
					dinv.setTipoValor(DetalleRecaudacion.TIPO_VALOR_CHEQUE);

					List<Mediopago> medio = bp
							.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) = :denominacion")
							.setParameter("denominacion", dinv.getTipoValor().toUpperCase()).list();

					if (medio != null && medio.size() > 0) {
						dinv.setMedioPago(medio.get(0));
					}

					dinv.getMoneda().setId(new Long(s.getMoneda()));
					dinv.setImporte(v.getImporte());
					dinv.setFechaAcreditacionValor(s.getOperacion().getFecha());
					dinv.setFechaRecaudacion(s.getOperacion().getFecha());
					dinv.setNumeroProyecto(detalleRecaudacion.getNumeroProyecto());
					dinv.setNumeroCorrelacion(detalleRecaudacion.getNumeroCorrelacion());
					dinv.setNumeroOperacion(s.getOperacion().getNumero());
					dinv.setNumeroValor(v.getNumeroValor());

					bp.save(dinv);

					continue;
				}

				if (s.getMedioPago().equals("01")) {
					detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_EFECTIVO);
				} else if (s.getMedioPago().matches("02|03|04|05")) {
					detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_CHEQUE);
				} else if (s.getMedioPago().equals("06")) {
					detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_TRANSFERENCIA);
				} else if (s.getMedioPago().equals("07")) {
					detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_RETENCION);
				}

				List<Mediopago> medio = bp
						.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) =:denominacion")
						.setParameter("denominacion", (detalleRecaudacion.getTipoValor()).toUpperCase()).list();

				if (medio != null && medio.size() > 0) {
					detalleRecaudacion.setMedioPago(medio.get(0));
				}

				// buscar si ya existia el registro
				detallesRecaudacion = bp.createQuery(
						"select d from DetalleRecaudacion d where d.numeroValor = :numeroValor and d.numeroProyecto = :numeroProyecto "
								+ "and d.numeroOperacion = :numeroOperacion")
						.setParameter("numeroValor", detalleRecaudacion.getNumeroValor())
						.setParameter("numeroProyecto", detalleRecaudacion.getNumeroProyecto())
						.setParameter("numeroOperacion", detalleRecaudacion.getNumeroOperacion()).list();

//				detallesRecaudacion = bp.getNamedQuery("DetalleRecaudacion.findByBancoANDfechaRecaudacionANDnumeroProyectoANDcodigoMovimiento")
//								.setEntity("banco", enteRecaudador)
//								.setDate("fechaRecaudacion", detalleRecaudacion.getFechaRecaudacion())
//								.setLong("numeroProyecto", detalleRecaudacion.getNumeroProyecto())
//								.setString("codigoMovimiento", detalleRecaudacion.getCodigoMovimiento())
//								.list();

				boolean actualizado = false;

				boolean impactado = false;

				for (DetalleRecaudacion detalleExistente : detallesRecaudacion) {
					if (detalleExistente.getRecaudacion() != null) {
						impactado = true;
						break;
					}

					EstadoCheque estadoChequeAnterior = (EstadoCheque) bp
							.getNamedQuery("EstadoCheque.findByDetalleRecaudacion")
							.setEntity("detalleRecaudacion", detalleExistente).setMaxResults(1).uniqueResult();
					if (estadoChequeAnterior != null) {
						if (!estadoChequeAnterior.getEstado().equals(estadoChequeStr)) {
							EstadoCheque estadoCheque = new EstadoCheque();
							estadoCheque.setEstado(estadoChequeStr);
							estadoCheque.setDetalleRecaudacion(detalleExistente);
							if (estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO)) {

								estadoCheque.setFecha(detalleRecaudacion.getFechaRecaudacion());
								detalleExistente.setFechaAcreditacionValor(detalleRecaudacion.getFechaRecaudacion());

							} else if (estadoCheque.getEstado()
									.equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO)) {

								estadoCheque.setFecha(v.getFechaAcreditacion());
								detalleExistente.setFechaAcreditacionValor(v.getFechaAcreditacion());

							} else {
								estadoCheque.setFecha(fechaActual);
							}

							bp.save(estadoCheque);

							// cambiar estado ValoresCartera en GAF
							String estadoValor = estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO)
									? "Acreditado"
									: estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_RECHAZADO)
											? "Rechazado"
											: null;
							if (estadoValor != null) {
								try {
									invocarCambioEstadoValor(detalleExistente, estadoValor);
								} catch (MalformedURLException e) {
									e.printStackTrace();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}

					detalleExistente.setDetalleArchivo(detalleArchivoNuevo);
					detalleExistente.setTempRecaudacion(null);
					bp.update(detalleExistente);
					actualizado = true;
					break;
				}

				if (actualizado) {
					registrosExistentes++;

					bp.getCurrentSession().flush();
					continue;
				}

				if (impactado) {
					continue;
				}

				bp.save(detalleRecaudacion);

				registrosNuevos++;
				// guardo estado por primera vez
				EstadoCheque estadoCheque = new EstadoCheque();
				estadoCheque.setEstado(estadoChequeStr);
				estadoCheque.setDetalleRecaudacion(detalleRecaudacion);

				if (estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO)) {
					estadoCheque.setFecha(detalleRecaudacion.getFechaRecaudacion());
				} else if (estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO)) {
					estadoCheque.setFecha(detalleRecaudacion.getFechaAcreditacionValor());
					detalleRecaudacion.setFechaAcreditacionValor(v.getFechaAcreditacion());
				} else {
					estadoCheque.setFecha(fechaActual);
				}

				bp.save(estadoCheque);
				bp.update(detalleRecaudacion);

				// cambiar estado ValoresCartera en GAF
				String estadoValor = estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO)
						? "Acreditado"
						: estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_RECHAZADO) ? "Rechazado" : null;
				if (estadoValor != null) {
					try {
						invocarCambioEstadoValor(detalleRecaudacion, estadoValor);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				bp.getCurrentSession().flush();
			} // for valores
		} else {
			// efectivo (no hay registros V)

			for (CredicoopImputacion i : s.getImputaciones()) {
				DetalleRecaudacion detalleRecaudacion = new DetalleRecaudacion();
				detalleRecaudacion.setOrigen(DetalleRecaudacion.ORIGEN_TXT);
				detalleRecaudacion.setBanco(enteRecaudador);
				detalleRecaudacion.getCuentaBancaria().setId(idBancue);
				detalleRecaudacion.setDetalleArchivo(detalleArchivoNuevo);
				detalleRecaudacion.setNumeroProyecto(new Long(i.getNumeroComprobante()));
				detalleRecaudacion.setFechaRecaudacion(s.getOperacion().getFecha());
				detalleRecaudacion.setCodigoMovimiento(s.getMedioPago());
				detalleRecaudacion.getMoneda().setId(new Long(s.getMoneda()));
				detalleRecaudacion.setNumeroOperacion(s.getOperacion().getNumero());
				detalleRecaudacion.setNumeroValor(null); // no es cheque
				detalleRecaudacion.setImporte(i.getImporte());
				detalleRecaudacion.setFechaAcreditacionValor(detalleRecaudacion.getFechaRecaudacion());

				// buscar en tabla correlacion por numero informado
				Number numeroAtencion = (Number) correlacionQuery
						.setParameter(0, new Long(i.getNumeroComprobante()).toString()).uniqueResult();
				if (numeroAtencion != null) {
					objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
							.setLong("nroAtencion", numeroAtencion.longValue()).uniqueResult();
					detalleRecaudacion.setNumeroCorrelacion(new Long(i.getNumeroComprobante()));
					detalleRecaudacion.setNumeroProyecto(numeroAtencion.longValue());
				} else {
					objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
							.setLong("nroAtencion", detalleRecaudacion.getNumeroProyecto()).uniqueResult();
					detalleRecaudacion.setNumeroCorrelacion(detalleRecaudacion.getNumeroProyecto());
				}

				if (objetoi == null) {
					registrosInvalidos++;

					String q = "select d from DetalleRecaudacionInv d where " + "d.banco = :banco and "
							+ "d.cuentaBancaria.id = :idBancue and " + "d.importe = :importe and "
							+ "d.fechaAcreditacionValor = :fechaAcreditacionValor and "
							+ "d.fechaRecaudacion = :fechaRecaudacion and " + "d.numeroProyecto = :numeroProyecto";

					List<DetalleRecaudacionInv> dinvs = bp.createQuery(q).setParameter("banco", enteRecaudador)
							.setParameter("idBancue", idBancue).setParameter("importe", i.getImporte())
							.setParameter("fechaAcreditacionValor", s.getOperacion().getFecha())
							.setParameter("fechaRecaudacion", s.getOperacion().getFecha())
							.setParameter("numeroProyecto", new Long(i.getNumeroComprobante())).list();
					if (!dinvs.isEmpty()) {
						for (DetalleRecaudacionInv di : dinvs) {
							di.setDetalleArchivo(detalleArchivoNuevo);
							bp.update(di);
						}
						return;
					}

					DetalleRecaudacionInv dinv = new DetalleRecaudacionInv();
					dinv.setBanco(enteRecaudador);
					dinv.getCuentaBancaria().setId(idBancue);
					dinv.setDetalleArchivo(detalleArchivoNuevo);
					dinv.setTipoValor(DetalleRecaudacion.TIPO_VALOR_EFECTIVO);

					List<Mediopago> medio = bp
							.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) = :denominacion")
							.setParameter("denominacion", dinv.getTipoValor().toUpperCase()).list();

					if (medio != null && medio.size() > 0) {
						dinv.setMedioPago(medio.get(0));
					}

					dinv.getMoneda().setId(new Long(s.getMoneda()));
					dinv.setImporte(i.getImporte());
					dinv.setFechaAcreditacionValor(s.getOperacion().getFecha());
					dinv.setFechaRecaudacion(s.getOperacion().getFecha());
					dinv.setNumeroProyecto(new Long(i.getNumeroComprobante()));
					dinv.setNumeroOperacion(s.getOperacion().getNumero());

					bp.save(dinv);

					continue;
				}

				detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_EFECTIVO);

				if (s.getMedioPago().equals("06")) {
					detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_TRANSFERENCIA);
				} else if (s.getMedioPago().equals("07")) {
					detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_RETENCION);
				}

				List<Mediopago> medio = bp
						.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) =:denominacion")
						.setParameter("denominacion", (detalleRecaudacion.getTipoValor()).toUpperCase()).list();

				if (medio != null && medio.size() > 0) {
					detalleRecaudacion.setMedioPago(medio.get(0));
				}

				// buscar si ya existia el registro
				detallesRecaudacion = bp.createQuery(
						"select d from DetalleRecaudacion d where d.importe = :importe and d.numeroProyecto = :numeroProyecto "
								+ "and d.numeroOperacion = :numeroOperacion and d.banco = :banco and d.fechaRecaudacion = :fecha")
						.setParameter("importe", detalleRecaudacion.getImporte())
						.setParameter("numeroProyecto", detalleRecaudacion.getNumeroProyecto())
						.setParameter("numeroOperacion", detalleRecaudacion.getNumeroOperacion())
						.setParameter("banco", enteRecaudador)
						.setParameter("fecha", detalleRecaudacion.getFechaRecaudacion()).list();

				boolean actualizado = false;

				boolean impactado = false;

				for (DetalleRecaudacion detalleExistente : detallesRecaudacion) {
					if (detalleExistente.getRecaudacion() != null) {
						impactado = true;
						break;
					}

					detalleExistente.setDetalleArchivo(detalleArchivoNuevo);
					detalleExistente.setTempRecaudacion(null);
					bp.update(detalleExistente);
					actualizado = true;
					break;
				}

				if (actualizado) {
					registrosExistentes++;

					bp.getCurrentSession().flush();
					continue;
				}

				if (impactado) {
					continue;
				}

				bp.save(detalleRecaudacion);

				registrosNuevos++;

				bp.getCurrentSession().flush();
			}
		}

//		
//		
//		String detalleVI = registro.getKey();
//		String subopOperacion = registro.getValue();
//		String[] subops = subopOperacion.split("-");
//		
//		String subop = subops[0];
//		String op = subops[1];
//		
//		
//		
//		

//		// datos de operacion
//		detalleRecaudacion.setFechaRecaudacion(DateHelper.getDate(fecha(op.substring(9, 17))));
//		// datos de suboperacion
//		detalleRecaudacion.setCodigoMovimiento(subop.substring(17, 19));
//		detalleRecaudacion.getMoneda().setId(new Long(subop.substring(19, 22)));

//		String estadoChequeStr = "";
//		String nroAtencion = "";
//		if(op.startsWith("O")){
//			//Numero de Operacion
//			detalleRecaudacion.setNumeroOperacion(new Long(op.substring(4, 9)));
//		}
//		if(detalleVI.startsWith("V")) {
//			detalleRecaudacion.setNumeroValor(detalleVI.substring(31,39));
//			detalleRecaudacion.setImporte(new Double(detalleVI.substring(50,61)) / 100D);
//			//detalleRecaudacion.setFechaAcreditacionValor(DateHelper.getDate(fecha(detalleVI.substring(63, 71))));
//			detalleRecaudacion.setFechaAcreditacionValor(detalleRecaudacion.getFechaRecaudacion());
		// Numero de Operacion
//			detalleRecaudacion.setNumeroOperacion(new Long(detalleVI.substring(4, 9)));

//			estadoChequeStr = detalleVI.substring(61, 63);
//			estadoChequeStr = estadoChequeStr.equals("  ") ? EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO : estadoChequeStr.trim();
//			nroAtencion = detalleVI.substring(39, 50);

//		} else if(detalleVI.startsWith("I")) {
//			if(detalleRecaudacion.getCodigoMovimiento().matches("02|03|04|05") == false){
//				detalleRecaudacion.setImporte(new Double(detalleVI.substring(21, 32)) / 100D);
//			}
//			//Numero de Operacion
//			detalleRecaudacion.setNumeroOperacion(new Long(detalleVI.substring(4, 9)));
//			detalleRecaudacion.setFechaAcreditacionValor(detalleRecaudacion.getFechaRecaudacion());
//			nroAtencion = detalleVI.substring(34, 50);
//		}

//		detalleRecaudacion.setNumeroProyecto(new Long(nroAtencion));
//		objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
//				.setLong("nroAtencion", detalleRecaudacion.getNumeroProyecto())
//				.uniqueResult();
//		if(objetoi == null){
//			registrosInvalidos++;
//			return;
//		}

//		if(detalleRecaudacion.getCodigoMovimiento().equals("01")){
//			detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_EFECTIVO);			
//		}else if(detalleRecaudacion.getCodigoMovimiento().matches("02|03|04|05")){
//			detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_CHEQUE);
//		}else if(detalleRecaudacion.getCodigoMovimiento().equals("06")){
//			detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_TRANSFERENCIA);
//		}else if(detalleRecaudacion.getCodigoMovimiento().equals("07")){
//			detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_RETENCION);			
//		}

//		List<Mediopago> medio = bp.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) =:denominacion")
//		.setParameter("denominacion", (detalleRecaudacion.getTipoValor()).toUpperCase()).list();
//		
//		if(medio!= null && medio.size()>0){
//			detalleRecaudacion.setMedioPago(medio.get(0));
//		}

//		detallesRecaudacion = bp.getNamedQuery("DetalleRecaudacion.findByBancoANDfechaRecaudacionANDnumeroProyectoANDcodigoMovimiento")
//								.setEntity("banco", enteRecaudador)
//								.setDate("fechaRecaudacion", detalleRecaudacion.getFechaRecaudacion())
//								.setLong("numeroProyecto", detalleRecaudacion.getNumeroProyecto())
//								.setString("codigoMovimiento", detalleRecaudacion.getCodigoMovimiento())
//								.list();
//		actualizado = false;
//		
//		boolean impactado = false;

//		for(DetalleRecaudacion detalle : detallesRecaudacion){
//			if(DetalleRecaudacion.TIPO_VALOR_CHEQUE.equals(detalle.getTipoValor())) {
//				if (detalle.getNumeroValor().equals(detalleRecaudacion.getNumeroValor())){
//				
//					if (detalle.getRecaudacion() != null) {
//						impactado = true;
//						break;
//					}
//					
//					EstadoCheque estadoChequeAnterior = (EstadoCheque) bp.getNamedQuery("EstadoCheque.findByDetalleRecaudacion")
//									.setEntity("detalleRecaudacion", detalle)
//									.setMaxResults(1)
//									.uniqueResult();
//					if(estadoChequeAnterior != null){
//						if(!estadoChequeAnterior.getEstado().equals(estadoChequeStr)){
//							EstadoCheque estadoCheque = new EstadoCheque();
//							estadoCheque.setEstado(estadoChequeStr);
//							estadoCheque.setDetalleRecaudacion(detalle);
//							if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO)) {
//								estadoCheque.setFecha(detalleRecaudacion.getFechaRecaudacion());
//								detalle.setFechaAcreditacionValor(new Date());
//							}
//							else if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO)) {
//								estadoCheque.setFecha(detalleRecaudacion.getFechaAcreditacionValor());
//								detalle.setFechaAcreditacionValor(new Date());
//							}
//							else 
//								estadoCheque.setFecha(fechaActual);
//							bp.save(estadoCheque);
//							
//							detalle.setDetalleArchivo(detalleArchivoNuevo);
//							bp.update(detalle);
//						}
//					}
//					detalle.setDetalleArchivo(detalleArchivoNuevo);
//					detalle.setTempRecaudacion(null);
//					bp.update(detalle);
//					actualizado = true;
//					break;
//				}
//			} else {

		// efectivo
//				if(detalle.getImporte().doubleValue() == detalleRecaudacion.getImporte().doubleValue()){
//					if (detalle.getRecaudacion() != null) {
//						impactado = true;
//						break;
//					}
//					detalle.setDetalleArchivo(detalleArchivoNuevo);
//					detalle.setFechaAcreditacionValor(detalleRecaudacion.getFechaRecaudacion());
//					bp.update(detalle);
//					actualizado = true;
//					break;
//				}
//			}
//		}
//		if(actualizado){
//			registrosExistentes++;
//
//			bp.getCurrentSession().flush();
//			return;
//		}
//		
//		if (impactado) {
//			return;
//		}

//		bp.save(detalleRecaudacion);

//		registrosNuevos++;
//		if(DetalleRecaudacion.TIPO_VALOR_CHEQUE.equals(detalleRecaudacion.getTipoValor())){
//			EstadoCheque estadoCheque = new EstadoCheque();
//			estadoCheque.setEstado(estadoChequeStr);
//			estadoCheque.setDetalleRecaudacion(detalleRecaudacion);
//			if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO))
//				estadoCheque.setFecha(detalleRecaudacion.getFechaRecaudacion());
//			else if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO))
//				estadoCheque.setFecha(detalleRecaudacion.getFechaAcreditacionValor());
//			else 
//				estadoCheque.setFecha(fechaActual);
//			bp.save(estadoCheque);	
//		}
//		
//		bp.getCurrentSession().flush();
	}

//	private String fecha(String ddmmaaaa) {
//		return ddmmaaaa.substring(0, 2) + "/" +  
//				ddmmaaaa.substring(2, 4) + "/" +
//				ddmmaaaa.substring(4, 8);
//	}

	/**
	 * estado = Acreditado | Rechazado
	 */
	public static void invocarCambioEstadoValor(DetalleRecaudacion detalle, String estado)
			throws MalformedURLException, Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (detalle.getTipoValor() != null && detalle.getTipoValor().equals(DetalleRecaudacion.TIPO_VALOR_CHEQUE)) {
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			String url = DirectorHelper.getString("URL.GAF") + "/services/Valores?wsdl";
			Client client = WebServiceHelper.getCliente(url);
			client.addOutHandler(sh.getAutenticacionClienteHandler());

			GregorianCalendar c = new GregorianCalendar();
			c.setTime(detalle.getFechaAcreditacionValor());

			client.invoke("cambiarEstadoValor", new Object[] { detalle.getNumeroValor(), detalle.getImporte(),
					DatatypeFactory.newInstance().newXMLGregorianCalendar(c), estado, detalle.getObjetoi().getId() });

			if (estado != null && estado.equals("Acreditado")) {
				detalle.getObjetoi().setValorCartera(false);
				bp.update(detalle.getObjetoi());
			}
		}
	}

	private void crearDetalleArchivo() {
		detalleArchivoNuevo = new DetalleArchivo();
		detalleArchivoNuevo.setFechaImportacion(fechaActual);
		detalleArchivoNuevo.setBanco(enteRecaudador);
		detalleArchivoNuevo.setUsuarioImportacion(usuario);
		detalleArchivoNuevo.getCaratula().setId(iForm.getIdCaratula());
		bp.save(detalleArchivoNuevo);
	}

}
