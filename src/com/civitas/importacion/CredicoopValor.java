package com.civitas.importacion;

import java.text.ParseException;
import java.util.Date;

public class CredicoopValor {

	private CredicoopSuboperacion suboperacion;
	private Integer secuencia;
	private String banco;
	private String sucursal;
	private String codigoPostal;
	private String numeroValor;
	private String numeroCuenta;
	private Double importe;
	private String estado;
	private Date fechaAcreditacion;
	private Date fechaVencimiento;
	
	public static Long parseSuboperacion(String l) {
		return new Long(l.substring(9, 17));
	}
	
	public CredicoopValor(String l, CredicoopSuboperacion suboperacion) {
		this.suboperacion = suboperacion;
		this.secuencia = new Integer(l.substring(17, 21));
		
		this.numeroValor = l.substring(31, 39);
		this.numeroCuenta = l.substring(39, 50);
		this.importe = new Double(l.substring(50, 61)) / 100;
		this.estado = l.substring(61, 63);
		
		try {
			this.fechaAcreditacion = ImportarBcoCredicop.DATE_FORMAT.parse(l.substring(63, 71));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			this.fechaVencimiento = ImportarBcoCredicop.DATE_FORMAT.parse(l.substring(71, 79));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public CredicoopSuboperacion getSuboperacion() {
		return suboperacion;
	}

	public Integer getSecuencia() {
		return secuencia;
	}

	public String getBanco() {
		return banco;
	}

	public String getSucursal() {
		return sucursal;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public String getNumeroValor() {
		return numeroValor;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public Double getImporte() {
		return importe;
	}

	public String getEstado() {
		return estado;
	}

	public Date getFechaAcreditacion() {
		return fechaAcreditacion;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	
	
}
