package com.civitas.importacion;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.datatype.DatatypeFactory;

import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.aegis.type.TypeMapping;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.service.Service;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import com.asf.cred.business.ProgressStatus;
import com.asf.cred.struts.form.ImportacionForm;
import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.Mediopago;
import com.asf.hibernate.mapping.Formatoimportacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.importacion.abstracts.AbstractFactory;
import com.civitas.importacion.abstracts.AbstractFormatos;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.DetalleArchivo;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.DetalleRecaudacionInv;
import com.nirven.creditos.hibernate.EstadoCheque;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Usuario;

public class ImportarBcoNacion extends AbstractFormatos {

	public static final String DIRECTOR_CODIBA_NACION = "cbnacion";
	public static final String DIRECTOR_CODIBA_NACION_DOLARES = "cbnacion.dolares";
	public static final String DIRECTOR_CODIBA_SUPERVIELLE = "cbsuperv";
	public static final String DIRECTOR_CODIBA_FRANCES = "cbfrances";
	
	private List<DetalleRecaudacion> detalles;
	private Date fechaRecaudacion;
	private Date fechaAcreditacionValor;
	private Long numeroProyecto;
	private String codigoMovimiento;
	private String numeroValor;
	private Double importe;
	private boolean actualizado;
	private DetalleRecaudacion detalleRecaudacion;
	private EstadoCheque estadoCheque;
	private EstadoCheque estadoChequeAnterior;
	private Object entidad;
	private Date fechaActual;
	private Usuario usuario;
	private Bancos banco;
	private Long idBancue;
	private BusinessPersistance bp;
	private String estadoChequeStr;	
	private DetalleArchivo detalleArchivoNuevo;
	private DetalleArchivo detalleArchivoAnterior;
	private boolean importado;
	private ImportacionForm iForm;
	private int registrosNuevos;
	private int registrosExistentes;
	private int registrosInvalidos;
	private Objetoi objetoi;
	
	private SQLQuery correlacionQuery;

	public ImportarBcoNacion(Formatoimportacion formatoimportacion,
			AbstractFactory fabrica) {
		super(formatoimportacion, fabrica);
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		iForm = (ImportacionForm)SessionHandler.getCurrentSessionHandler().getRequest().getAttribute("ImportacionForm");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public SortedMap parse(SortedMap<Long, Map<String, String>> hsLineas) {
		if(!hsLineas.isEmpty())
			ProgressStatus.iTotal = hsLineas.size();
		else
			return new TreeMap();
		
		SortedMap resultado = new TreeMap();
		
		fechaActual = new Date();
		usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
		
		correlacionQuery = bp.createSQLQuery("SELECT NumeroSolicitud FROM CORRELACION_CREDITO_GIC WHERE idProyecto = ?");
		
		String txt = "";
		crearDetalleArchivo();
		Map hsValores;
		Collection valores;
		String linea;
//		ExtractoBancario ext;
		PagoCreditoBean bean;
		importado = false;
		
		String simboloMoneda = "$";
		
		for (Long key : hsLineas.keySet()) {
			hsValores = hsLineas.get(key);
			valores = hsValores.values();
			if (valores.isEmpty()) {
				ProgressStatus.iProgress++;
				continue;
			}
			linea = valores.iterator().next().toString();
			
			if (banco == null) {
				if (linea.startsWith("0000002696")) {
					String codiBa = DirectorHelper.getString(DIRECTOR_CODIBA_NACION);
					banco = (Bancos)bp.createQuery("SELECT b FROM Bancos b WHERE b.recaudador = 1 AND b.codiBa = :codiBa").setParameter("codiBa", new Long(codiBa)).list().get(0);
					idBancue = new Long(bp.getByFilter("SELECT d.valor FROM Director d WHERE d.id='idBancueNacion'").get(0).toString().trim());
				} else if (linea.startsWith("0000004540")) {
					String codiBa = DirectorHelper.getString(DIRECTOR_CODIBA_NACION_DOLARES);
					banco = (Bancos)bp.createQuery("SELECT b FROM Bancos b WHERE b.recaudador = 1 AND b.codiBa = :codiBa").setParameter("codiBa", new Long(codiBa)).list().get(0);
					idBancue = new Long(bp.getByFilter("SELECT d.valor FROM Director d WHERE d.id='idBancueNacion'").get(0).toString().trim());
					simboloMoneda = "U$S";
				} else if (linea.startsWith("0000000017")) {
					String codiBa = DirectorHelper.getString(DIRECTOR_CODIBA_FRANCES);
					banco = (Bancos)bp.createQuery("SELECT b FROM Bancos b WHERE b.recaudador = 1 AND b.codiBa = :codiBa").setParameter("codiBa", new Long(codiBa)).list().get(0);
					idBancue = new Long(bp.getByFilter("SELECT d.valor FROM Director d WHERE d.id='idBancueFrances'").get(0).toString().trim());
				} else {
					String codiBa = DirectorHelper.getString(DIRECTOR_CODIBA_SUPERVIELLE);
					banco = (Bancos)bp.createQuery("SELECT b FROM Bancos b WHERE b.recaudador = 1 AND b.codiBa = :codiBa").setParameter("codiBa", new Long(codiBa)).list().get(0);
					idBancue = new Long(bp.getByFilter("SELECT d.valor FROM Director d WHERE d.id='idBancueSupervielle'").get(0).toString().trim());
				}
				detalleArchivoNuevo.setBanco(banco);
				Caratula caratulaActual = (Caratula) bp.getById(Caratula.class,iForm.getIdCaratula());
				caratulaActual.setBanco(banco);
				bp.update(caratulaActual);
			}
			
//			ext = null;
//			try {
//				ext = (ExtractoBancario) bp.getByFilter(
//						"SELECT e FROM ExtractoBancario e WHERE e.registro = '" + linea + "'").get(0);
//			} catch (Exception e) {
//			}
//			if (ext == null) {
//				ext = new ExtractoBancario();
//				ext.setEnteRecaudador(banco);
//				ext.crearExtractoNacion(linea);
//				ext.setIdCuentaBancaria(idBancue);
//				ext.setProcesoGIC(true);
//				bp.save(ext);
//			}
//			try {
//				bp.commit();
//			} catch (HibernateException e) {
//				bp.rollback();
//				e.printStackTrace();
//			}
			
//			if (linea.contains("R")) {
//				ProgressStatus.iProgress++;
//				continue;
//			}
			bean = new PagoCreditoBean();
			bean.setNumeroProyecto(new Long(linea.substring(62, 72).trim()));
			bean.setPeriodoBoleto(new Long (linea.substring(76, 80)));
			bean.setVerificadorBoleto(0L);
			bean.setImporte(new Double(linea.substring(42,57))*(1.0)/(100));	
			bean.setFechaPago(DateHelper.getDate(invertirFecha(linea.substring(18,26))));
			if(bean.getCredito()!=null){
				resultado.put(key, bean);
			}
			crearDetalleRecaudacion(linea);
			txt += linea;
			ProgressStatus.iProgress++;
		}
		String resumen = "<br>Cantidad de registros importados: " + (registrosNuevos + registrosExistentes) +
						"<br>Entidad Recaudadora: " + banco.getDetaBa() +
						"<br>Registros Nuevos: " + registrosNuevos + 
						"<br>Registros Existentes: " + registrosExistentes;
		if(registrosInvalidos > 0){
			resumen += "<br>Registros Inválidos: " + registrosInvalidos;
		}
		
		resumen += "<br>Monto Total " + simboloMoneda +
				": " + String.format("%,.2f", detalleArchivoNuevo.getTotalArchivo());
		
		iForm.setResumen(resumen);
//		if(importado){
			iForm.setIdDetalleArchivo(detalleArchivoNuevo.getId());
			detalleArchivoNuevo.setTxt(txt);
			bp.update(detalleArchivoNuevo);
//		}else{
//			if(detalleArchivoAnterior != null){
//				iForm.setIdDetalleArchivo(detalleArchivoAnterior.getId());
//			}
//			bp.delete(detalleArchivoNuevo);
//		}
		
		ImportacionForm iForm = (ImportacionForm)SessionHandler.getCurrentSessionHandler().getRequest().getAttribute("ImportacionForm");
		iForm.setTodoOk(!(registrosNuevos == 0 && registrosExistentes == 0 && registrosInvalidos == 0));

//		try {
//			activarExtractosGAF();
//		} catch (Exception e) {
//			System.out.println("***Error al activar la importación en GAF***");
//		}
		return resultado;
	}

//	@SuppressWarnings("unused")
//	private void activarExtractosGAF() {
//		try {
//			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
//			String url = DirectorHelper.getString("URL.GAF") + "/services/ExtractosBancariosGAFService?wsdl";
//			Client c = WebServiceHelper.getCliente(url);
//			Service model = c.getService();
//			AegisBindingProvider bp = (AegisBindingProvider) model.getBindingProvider();
//			TypeMapping typeMapping = bp.getTypeMapping(model);
//			c.addOutHandler(sh.getAutenticacionClienteHandler());
//			Object[] params = new Object[0];
//			c.invoke("nuevosExtractos", params);
//		} catch (MalformedURLException e) {
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	@Override
	public boolean isLineaCompleta() {
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private void crearDetalleRecaudacion(String linea){
		fechaRecaudacion = DateHelper.getDate(invertirFecha(linea.substring(18,26)));
		String stringProyecto = new Long(linea.substring(62, 72).trim()).toString();
		numeroProyecto = new Long(stringProyecto);
		Long numeroCorrelacion = null;
		codigoMovimiento = linea.substring(34,36);
		numeroValor = linea.substring(146,154);
		fechaAcreditacionValor = DateHelper.getDate(invertirFecha(linea.substring(26,34)));
		importe = new Double(linea.substring(42,57)).doubleValue() / 100D;
		estadoChequeStr = linea.substring(154,155);
		
		if (!stringProyecto.startsWith("99")) { // 99 = ingresos varios
			// buscar en tabla correlacion por numero informado
			Number numeroAtencion = (Number) correlacionQuery.setParameter(0, stringProyecto).uniqueResult();
			if (numeroAtencion != null) {
				objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setLong("nroAtencion", numeroAtencion.longValue())
					.uniqueResult();
				numeroCorrelacion = numeroProyecto;
				numeroProyecto = numeroAtencion.longValue();
			} else {
				objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setLong("nroAtencion", numeroProyecto)
					.uniqueResult();
				numeroCorrelacion = numeroProyecto;
			}
		
		
			if(objetoi == null){
				registrosInvalidos++;
				
				DetalleRecaudacionInv dinv = new DetalleRecaudacionInv();
				dinv.setBanco(banco);
				dinv.getCuentaBancaria().setId(idBancue);
				dinv.setDetalleArchivo(detalleArchivoNuevo);
				if(codigoMovimiento.equals("50")){
					dinv.setTipoValor(DetalleRecaudacion.TIPO_VALOR_EFECTIVO);
				}else{ 
					dinv.setTipoValor(DetalleRecaudacion.TIPO_VALOR_CHEQUE);
					dinv.setNumeroValor(numeroValor);								
				}
				
				String q = "select d from DetalleRecaudacionInv d where " +
						"d.banco = :banco and " +
						"d.cuentaBancaria.id = :idBancue and " +
						"d.importe = :importe and " +
						"d.fechaAcreditacionValor = :fechaAcreditacionValor and " +
						"d.fechaRecaudacion = :fechaRecaudacion and " +
						"d.numeroProyecto = :numeroProyecto";
				if (dinv.getNumeroValor() != null) {
					q += " and d.numeroValor = :numeroValor ";
				}
				
				Query query = bp.createQuery(q).setParameter("banco", banco).setParameter("idBancue", idBancue)
						.setParameter("importe", importe).setParameter("fechaAcreditacionValor", fechaAcreditacionValor)
						.setParameter("fechaRecaudacion", fechaRecaudacion)
						.setParameter("numeroProyecto", numeroProyecto);
				if (dinv.getNumeroValor() != null) {
					query.setParameter("numeroValor", dinv.getNumeroValor());
				}
				
				List<DetalleRecaudacionInv> dinvs = query.list();
				if (!dinvs.isEmpty()) {
					for (DetalleRecaudacionInv di : dinvs) {
						di.setDetalleArchivo(detalleArchivoNuevo);
						bp.update(di);
					}
					return;
				}
				
				List<Mediopago> medio = bp.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) = :denominacion")
					.setParameter("denominacion", dinv.getTipoValor().toUpperCase()).list();
				
				if(medio!= null && medio.size()>0){
					dinv.setMedioPago(medio.get(0));
				}
				
				dinv.getMoneda().setId(new Long(linea.substring(57,58)));
				dinv.setImporte(importe);
				dinv.setFechaAcreditacionValor(fechaAcreditacionValor);
				dinv.setFechaRecaudacion(fechaRecaudacion);
				dinv.setNumeroProyecto(numeroProyecto);
				dinv.setNumeroCorrelacion(numeroCorrelacion);
				
				bp.save(dinv);
				
				return;
			}
		} else {
			// es ingresos varios
			numeroCorrelacion = numeroProyecto;
		}
		
		detalles = bp.getNamedQuery("DetalleRecaudacion.findByBancoANDfechaRecaudacionANDnumeroProyectoANDcodigoMovimiento")
						.setEntity("banco", banco)
						.setDate("fechaRecaudacion", fechaRecaudacion)
						.setLong("numeroProyecto", numeroProyecto)
						.setString("codigoMovimiento", codigoMovimiento)
						.list();
		actualizado = false;

		boolean impactado = false;
		
		
		for(DetalleRecaudacion detalle : detalles){
			if(detalle.getTipoValor().equals(DetalleRecaudacion.TIPO_VALOR_CHEQUE)) {
			
				if (detalle.getNumeroValor().equals(numeroValor)){
				

					if (detalle.getRecaudacion() != null) {
						impactado = true;
						break;
					}
				
					 entidad = bp.getNamedQuery("EstadoCheque.findByDetalleRecaudacion")
												.setEntity("detalleRecaudacion", detalle)
												.setMaxResults(1)
												.uniqueResult();
					 if(entidad != null){
						 estadoChequeAnterior = (EstadoCheque)entidad;
						 if(!estadoChequeAnterior.getEstado().equals(estadoChequeStr)){
							try{
								estadoCheque = new EstadoCheque();
								estadoCheque.setEstado(estadoChequeStr);
								estadoCheque.setDetalleRecaudacion(detalle);
								if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_NACION_PRESENTADO))
									estadoCheque.setFecha(fechaRecaudacion);
								else if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_NACION_LIBERADO))
									estadoCheque.setFecha(fechaAcreditacionValor);
								else 
									estadoCheque.setFecha(fechaActual);
								bp.save(estadoCheque);
								importado = true;
								detalle.setDetalleArchivo(detalleArchivoNuevo);
								detalle.setFechaAcreditacionValor(fechaAcreditacionValor); // set aca para que tome fecha correcta valor en GAF
								bp.update(detalle);
								
								// cambiar estado ValoresCartera en GAF
								String estadoValor = estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_NACION_LIBERADO) ? "Acreditado" : 
									estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_NACION_RECHAZADO) ? "Rechazado" : null;
								if (estadoValor != null) {
									invocarCambioEstadoValor(detalle, estadoValor);
								}
							} catch(IndexOutOfBoundsException e) {
								//no tiene estado
							} catch (MalformedURLException e) {
								e.printStackTrace();
								
							} catch (Exception e) {
								e.printStackTrace();
							}						
						 }else{
							detalleArchivoAnterior = detalle.getDetalleArchivo();
						 }
					 }
					 detalle.setDetalleArchivo(detalleArchivoNuevo); // siempre pasarlo al archivo nuevo
					 detalle.setTempRecaudacion(null);
					 detalle.setFechaAcreditacionValor(fechaAcreditacionValor);
					 bp.update(detalle);
					 actualizado = true;
					 break; 
				}
			} else {
				if(detalle.getImporte().doubleValue() == importe.doubleValue()){
					if (detalle.getRecaudacion() != null) {
						impactado = true;
						break;
					}
					detalleArchivoAnterior = detalle.getDetalleArchivo();
					detalle.setFechaAcreditacionValor(fechaAcreditacionValor);
					detalle.setDetalleArchivo(detalleArchivoNuevo); // siempre pasarlo al archivo nuevo
					detalle.setTempRecaudacion(null);
					bp.update(detalle);
					actualizado = true;
					break;
				}
			}
		}
		if(actualizado){
			registrosExistentes++;
			return;
		}
		
		if (impactado) {
			return;
		}
		
		detalleRecaudacion = new DetalleRecaudacion();
		detalleRecaudacion.setOrigen(DetalleRecaudacion.ORIGEN_TXT);
		detalleRecaudacion.setBanco(banco);
		detalleRecaudacion.getCuentaBancaria().setId(idBancue);
		detalleRecaudacion.setCodigoMovimiento(codigoMovimiento);
		detalleRecaudacion.setDetalleArchivo(detalleArchivoNuevo);
		if(detalleRecaudacion.getCodigoMovimiento().equals("50")){
			detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_EFECTIVO);
		}else{ 
			detalleRecaudacion.setTipoValor(DetalleRecaudacion.TIPO_VALOR_CHEQUE);
			detalleRecaudacion.setNumeroValor(numeroValor);								
		}
		
		List<Mediopago> medio = bp.createQuery("SELECT m FROM Mediopago m WHERE UPPER(m.denominacion) =:denominacion")
		.setParameter("denominacion", (detalleRecaudacion.getTipoValor()).toUpperCase()).list();
		
		if(medio!= null && medio.size()>0){
			detalleRecaudacion.setMedioPago(medio.get(0));
		}
		
		detalleRecaudacion.getMoneda().setId(new Long(linea.substring(57,58)));
		detalleRecaudacion.setImporte(importe);				
//		fecha de vencimiento valor			
		detalleRecaudacion.setFechaAcreditacionValor(fechaAcreditacionValor);
		detalleRecaudacion.setFechaRecaudacion(fechaRecaudacion);
		detalleRecaudacion.setNumeroProyecto(numeroProyecto);
		detalleRecaudacion.setNumeroCorrelacion(numeroCorrelacion);
//		id interbanking
//		partidas pendientes
//		id deposito
		bp.save(detalleRecaudacion);
		importado = true;
		registrosNuevos++;
		if(detalleRecaudacion.getTipoValor().equals(DetalleRecaudacion.TIPO_VALOR_CHEQUE)){
			try{
				estadoCheque = new EstadoCheque();
				estadoCheque.setEstado(linea.substring(154,155));
				estadoCheque.setDetalleRecaudacion(detalleRecaudacion);
				if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_NACION_PRESENTADO))
					estadoCheque.setFecha(detalleRecaudacion.getFechaRecaudacion());
				else if(estadoCheque.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_NACION_LIBERADO))
					estadoCheque.setFecha(detalleRecaudacion.getFechaAcreditacionValor());
				else 
					estadoCheque.setFecha(fechaActual);
				bp.save(estadoCheque);
				
				// cambiar estado ValoresCartera en GAF
				String estadoValor = estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_NACION_LIBERADO) ? "Acreditado" : 
					estadoChequeStr.equals(EstadoCheque.ESTADO_CHEQUE_NACION_RECHAZADO) ? "Rechazado" : null;
				if (estadoValor != null) {
					invocarCambioEstadoValor(detalleRecaudacion, estadoValor);
				}
			} catch(IndexOutOfBoundsException e) {
				//no tiene estado
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private String invertirFecha(String aaaammdd) {
		return aaaammdd.substring(6, 8) + "/" +  
				aaaammdd.substring(4, 6) + "/" +
				aaaammdd.substring(0, 4);
	}
	
	private void crearDetalleArchivo(){
		detalleArchivoNuevo = new DetalleArchivo();
		detalleArchivoNuevo.setFechaImportacion(fechaActual);
		detalleArchivoNuevo.setUsuarioImportacion(usuario);
		detalleArchivoNuevo.setBanco(banco);
		detalleArchivoNuevo.getCaratula().setId(iForm.getIdCaratula());
		bp.save(detalleArchivoNuevo);
	}

	public static void invocarCambioEstadoValor(DetalleRecaudacion detalle, String estado) throws MalformedURLException, Exception {
	    	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (detalle.getTipoValor() != null && detalle.getTipoValor().equals(DetalleRecaudacion.TIPO_VALOR_CHEQUE) && detalle.getObjetoi() != null) {
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			String url = DirectorHelper.getString("URL.GAF") + "/services/Valores?wsdl";
			Client client = WebServiceHelper.getCliente(url);
			client.addOutHandler(sh.getAutenticacionClienteHandler());
			
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(detalle.getFechaAcreditacionValor());
			
			client.invoke("cambiarEstadoValor", 
					new Object[]{detalle.getNumeroValor(), detalle.getImporte(), DatatypeFactory.newInstance().newXMLGregorianCalendar(c), estado,
					detalle.getObjetoi().getId()});
			
			if (estado != null && estado.equals("Acreditado")) {
				detalle.getObjetoi().setValorCartera(false);
				bp.update(detalle.getObjetoi());
			}
		}
	}
}
