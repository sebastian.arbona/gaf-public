package com.civitas.importacion;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.persistence.NoResultException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.aegis.type.TypeMapping;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.service.Service;
import org.hibernate.HibernateException;

import com.asf.cred.business.ProgressStatus;
import com.asf.cred.struts.form.ImportacionForm;
import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.ExtractoBancario;
import com.asf.gaf.hibernate.Mediopago;
import com.asf.hibernate.mapping.Formatoimportacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.importacion.abstracts.AbstractFactory;
import com.civitas.importacion.abstracts.AbstractFormatos;
import com.civitas.importacion.errores.Errores;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.DetalleArchivo;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.EstadoCheque;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Usuario;

public class ImportarChequeCredicoop extends AbstractFormatos {
	
	private static final String DIRECTOR_CODIBA_CREDICOOP = "cbcredic";
	
	private BusinessPersistance bp;
	private Bancos enteRecaudador;
	private Date fechaActual;
	private Usuario usuario;
	private List<DetalleRecaudacion> detallesRecaudacion;
	private DetalleArchivo detalleArchivoNuevo;
	private ImportacionForm iForm;
	private int nuevosDetalles;
	private int existentesDetalles;
	private int invalidosDetalles;
	private Long idBancue;


	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog(ImportarBcoCredicop.class);
	@SuppressWarnings("unused")
	private BoletosFactory boletosFactory = (BoletosFactory) super.fabrica;

	public ImportarChequeCredicoop(Formatoimportacion formatoImportacion,
			AbstractFactory fabrica) {
		super(formatoImportacion, fabrica);
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		iForm = (ImportacionForm)SessionHandler.getCurrentSessionHandler().getRequest().getAttribute("ImportacionForm");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SortedMap parse(SortedMap<Long, Map<String, String>> hsLineas) {
		if(!hsLineas.isEmpty())
			ProgressStatus.iTotal = hsLineas.size();
		else
			return new TreeMap();
		
		SortedMap resultado = new TreeMap();
		Map hsValores;
		Collection valores;
		String linea;
		PagoCreditoBean bean;
		PagoCreditoBean beanRegistroO = new PagoCreditoBean();
		String importeStr;
		String comprobante;
		String numeroOperacion;
		//String numeroProyecto;
		String fechaRecaudacionStr;
		String estadoCheque ="";
		Double importe;
		String fechaStr;
		List<DetalleRecaudacion> detalles = new ArrayList<DetalleRecaudacion>();
		
		String codiBa = DirectorHelper.getString(DIRECTOR_CODIBA_CREDICOOP);
		String consulta = "SELECT b FROM Bancos b WHERE b.codiBa = :codiBa AND b.recaudador = 1";
		enteRecaudador = (Bancos) bp.createQuery(consulta).setParameter("codiBa", new Long(codiBa)).list().get(0);
		idBancue = new Long(bp.getByFilter("SELECT d.valor FROM Director d WHERE d.id='idBancueCredicoop'").get(0).toString().trim());

		crearDetalleArchivo();
		
		for (Long key : hsLineas.keySet()) {// itero sobre la línea
			try{
				hsValores = hsLineas.get(key);
				valores = hsValores.values();
				if (valores.isEmpty()) {
					continue;
				}
				linea = valores.iterator().next().toString();
				//Proyecto
				//numeroProyecto = linea.substring(1, 9);
				//nro operacion
				numeroOperacion = linea.substring(12, 17);
				//fechaRecaudacion
				fechaRecaudacionStr = linea.substring(17, 25);
				fechaRecaudacionStr = fechaRecaudacionStr.substring(6, 8) 
				+ "/"
				+ fechaRecaudacionStr.substring(4, 6)
				+ "/"
				+ fechaRecaudacionStr.substring(0, 4);
				Date fechaRecaudacion = DateHelper.getDate(fechaRecaudacionStr);
				//nro Valor
				comprobante = linea.substring(35, 43);
				//importe
				importeStr = linea.substring(54, 64);
				importe = Double.parseDouble(importeStr) / 100;
				//estadoCheque
				estadoCheque = linea.substring(64, 65);
				//fecha acreditacion valor
				fechaStr = linea.substring(65, 73);
				fechaStr = fechaStr.substring(6, 8) 
							+ "/"
							+ fechaStr.substring(4, 6)
							+ "/"
							+ fechaStr.substring(0, 4);
				Date fechaAcreditacionValor = DateHelper.getDate(fechaStr);
				
				DetalleRecaudacion detalle = (DetalleRecaudacion) bp.createQuery("SELECT d FROM DetalleRecaudacion d WHERE d.numeroValor =:numero AND " +
							"d.importe = :importe AND d.numeroOperacion =:operacion")
					.setParameter("numero", comprobante)
					.setParameter("importe", importe)
					.setParameter("operacion", new Long(numeroOperacion))
					.uniqueResult();
					
				if(detalle!= null){
					detalle.setFechaAcreditacionValor(fechaAcreditacionValor);
					detalle.setDetalleArchivo(detalleArchivoNuevo);
					
					EstadoCheque estadoNuevo = new EstadoCheque();
					estadoNuevo.setDetalleRecaudacion(detalle);
					estadoNuevo.setEstado(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO);
					estadoNuevo.setFecha(new Date());
					bp.saveOrUpdate(estadoNuevo);
					bp.saveOrUpdate(detalle);
					existentesDetalles++;
				}else{
//						DetalleRecaudacion nuevoDetalle = new DetalleRecaudacion();
//						
//						nuevoDetalle.setFechaAcreditacionValor(fechaAcreditacionValor);
//						nuevoDetalle.setNumeroProyecto(new Long(numeroProyecto));
//						nuevoDetalle.setFechaRecaudacion(fechaRecaudacion);
//						nuevoDetalle.setBanco(enteRecaudador);
//						nuevoDetalle.setImporte(importe);
//						nuevoDetalle.setNumeroOperacion(new Long(numeroOperacion));
//						nuevoDetalle.setNumeroValor(comprobante);
//						nuevoDetalle.setTipoValor(DetalleRecaudacion.TIPO_VALOR_CHEQUE);
//						nuevoDetalle.getCuentaBancaria().setId(idBancue);
//						nuevoDetalle.getMoneda().setId(1L);
//						detalles.add(nuevoDetalle);
					}
			}catch (Exception e) {
				invalidosDetalles++;
			}
		}
		
		fechaActual = new Date();
		usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());

		bp.begin();

		
		for (DetalleRecaudacion detalleRecaudacion : detalles) {
			detalleRecaudacion.setDetalleArchivo(detalleArchivoNuevo);
			bp.saveOrUpdate(detalleRecaudacion);
			nuevosDetalles++;
			EstadoCheque estado = new EstadoCheque();
			estado.setFecha(new Date());
			estado.setDetalleRecaudacion(detalleRecaudacion);
			if(estadoCheque.equalsIgnoreCase("R") || estadoCheque.equalsIgnoreCase("F")) {
				estado.setEstado(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_RECHAZADO);
			}else{
				estado.setEstado(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO);
			}
			bp.save(estado);
			
				
		}
		String resumen = "<br>Cantidad de Detalles de Recaudación Creados: " + nuevosDetalles +
						"<br>Entidad Recaudadora: " + enteRecaudador.getDetaBa() +
						"<br>Registros inválidos: " + invalidosDetalles +
						"<br>Detalles de Recaudación Existentes: " + existentesDetalles; 
		
		resumen += "<br>Monto Total $: " + detalleArchivoNuevo.getTotalArchivo();
		
		iForm.setResumen(resumen);
		try {
			bp.commit();
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
		}
		ImportacionForm iForm = (ImportacionForm)SessionHandler.getCurrentSessionHandler().getRequest().getAttribute("ImportacionForm");
		iForm.setIdDetalleArchivo(detalleArchivoNuevo.getId());
		iForm.setTodoOk(true);
		ProgressStatus.iProgress=ProgressStatus.iTotal;
		return new TreeMap();
	}


	@Override
	public boolean isLineaCompleta() {
		return true;
	}

	
	private void crearDetalleArchivo(){
		detalleArchivoNuevo = new DetalleArchivo();
		detalleArchivoNuevo.setFechaImportacion(fechaActual);
		detalleArchivoNuevo.setBanco(enteRecaudador);
		detalleArchivoNuevo.setUsuarioImportacion(usuario);
		detalleArchivoNuevo.getCaratula().setId(iForm.getIdCaratula());
		bp.save(detalleArchivoNuevo);
	}
	

}
