/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.civitas.importacion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import com.civitas.importacion.abstracts.AbstractFactory;
import com.civitas.importacion.abstracts.AbstractFormatos;
import com.civitas.importacion.abstracts.IValidador;

/**
 * Fabrica Concreta de Boletos.
 * 
 * @author aromeo
 */
public class BoletosFactory extends AbstractFactory {

    private static BoletosFactory boletoFactory = null;
    private static IValidador iValidador = null;
    private static AbstractFormatos importador = null;
    
    private BoletosFactory() {}

    public static final BoletosFactory getInstance() {
        if (boletoFactory == null) {
            boletoFactory = new BoletosFactory();
        }
        return boletoFactory;
    }

    @Override
    public IValidador getValidador() {
        if (iValidador == null) {
            iValidador = new ValidadorBoleto(this);
        }
        return iValidador;
    }

    @Override
    public AbstractFormatos getImportador() {
        	if(formatoImportacion.getDetalle().toUpperCase().trim().contains("CHEQUE")){
        		importador = new ImportarChequeCredicoop(formatoImportacion, boletoFactory);
        	}else if(formatoImportacion.getDetalle().toUpperCase().trim().contains("CREDICOOP")){
        		importador = new ImportarBcoCredicop(formatoImportacion, boletoFactory);	
        	}else{
        		importador = new ImportarBcoNacion(formatoImportacion, boletoFactory);	
        	}
        return importador;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
		InputStream in = new FileInputStream("/home/andres/Descargas/CH120417(1).TXT");
		Scanner txt = new Scanner(in);
        String primeraLinea;
    	 primeraLinea= txt.nextLine();
    	 long idFormato;
    	 if(primeraLinea.startsWith("R")){
 			idFormato = 3L;//idFormato Cheque Credicoop
     	}else{
	        	String segundaLinea = txt.nextLine();
	        	if(segundaLinea.matches("^[A-Z]{1}[\\w\\d ,]+$")){
	        		idFormato = 2L;//idFormato Credicoop
	        	}else{
	        		idFormato = 1L;//idFormato Nacion 
	        	}
     	}
	}
}
