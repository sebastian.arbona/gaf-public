package com.civitas.importacion;

import java.io.Serializable;
import java.util.Date;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Objetoi;

public class PagoCreditoBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long id;
	private String numeroProyecto;
	private Long numeroBoleto;
	private Long verificadorBoleto;
	private Long periodoBoleto;
	private Double importe;
	private Date fechaPago;
	
	// para cuando esta validado
	private Objetoi credito;
	private Boleto boleto;
	
	private void setBoleto(String boleto) {
		// suponer
		// NNNNNNN..NVPPPP (N = numero, V = verif, P = periodo)
		String periodoStr = boleto.substring(boleto.length() - 4);
		String verifStr = boleto.substring(boleto.length() - 5, boleto.length() - 4);
		String numeroStr = boleto.substring(0, boleto.length() - 5);
		numeroBoleto = new Long(numeroStr);
		verificadorBoleto = new Long(verifStr);
		periodoBoleto = new Long(periodoStr);
	}

	public Long getNumeroProyectoLong() {
		return new Long(numeroProyecto);
	}

	public String getNumeroProyecto() {
		return numeroProyecto;
	}

	public void setNumeroProyecto(String numeroProyecto) {
		try {
			this.numeroProyecto = numeroProyecto; // por si es credito
			this.credito = (Objetoi)SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getById(Objetoi.class, new Long(numeroProyecto));
		} catch (Exception e) {
			setBoleto(numeroProyecto); // por si es un boleto
		}
	}

	public Long getNumeroBoleto() {
		return numeroBoleto;
	}

	public void setNumeroBoleto(Long numeroBoleto) {
		this.numeroBoleto = numeroBoleto;
	}

	public Long getVerificadorBoleto() {
		return verificadorBoleto;
	}

	public void setVerificadorBoleto(Long verificadorBoleto) {
		this.verificadorBoleto = verificadorBoleto;
	}

	public Long getPeriodoBoleto() {
		return periodoBoleto;
	}

	public void setPeriodoBoleto(Long periodoBoleto) {
		this.periodoBoleto = periodoBoleto;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public Boleto getBoleto() {
		return boleto;
	}

	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	public void setNumeroProyecto(Long long1) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		try {
			this.credito = (Objetoi) bp.getByFilter("SELECT o FROM Objetoi o WHERE o.numeroAtencion = " + long1).get(0);
		} catch (Exception e) {
		}
		this.numeroProyecto = long1.toString();	
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
	
	public Date getFechaPago() {
		return fechaPago;
	}
	
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

}
