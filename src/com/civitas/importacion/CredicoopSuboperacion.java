package com.civitas.importacion;

import java.util.ArrayList;
import java.util.List;

public class CredicoopSuboperacion {
	
	private CredicoopOperacion operacion;
	private Long numero;
	private String medioPago;
	private String moneda;
	private Double montoOriginal;
	private Double montoAjustado;
	private Integer cantidadValores;
	
	private List<CredicoopValor> valores;
	private List<CredicoopImputacion> imputaciones;
	
	public static Long parseOperacion(String l) {
		return new Long(l.substring(3, 9));
	}
	
	public CredicoopSuboperacion(String l, CredicoopOperacion operacion) {
		this.operacion = operacion;
		
		this.numero = new Long(l.substring(9, 17));
		this.medioPago = l.substring(17, 19);
		this.moneda = l.substring(19, 22);
		this.montoOriginal = new Double(l.substring(22, 33)) / 100;
		this.montoAjustado = new Double(l.substring(33, 44)) / 100;
		this.cantidadValores = new Integer(l.substring(44, 48));
		
		this.valores = new ArrayList<CredicoopValor>();
		this.imputaciones = new ArrayList<CredicoopImputacion>();
	}

	public void addValor(CredicoopValor valor) {
		valores.add(valor);
	}
	
	public void addImputacion(CredicoopImputacion imputacion) {
		imputaciones.add(imputacion);
	}
	
	public CredicoopOperacion getOperacion() {
		return operacion;
	}

	public Long getNumero() {
		return numero;
	}

	public String getMedioPago() {
		return medioPago;
	}

	public String getMoneda() {
		return moneda;
	}

	public Double getMontoOriginal() {
		return montoOriginal;
	}

	public Double getMontoAjustado() {
		return montoAjustado;
	}

	public Integer getCantidadValores() {
		return cantidadValores;
	}

	public List<CredicoopValor> getValores() {
		return valores;
	}

	public void setValores(List<CredicoopValor> valores) {
		this.valores = valores;
	}

	public List<CredicoopImputacion> getImputaciones() {
		return imputaciones;
	}

	public void setImputaciones(List<CredicoopImputacion> imputaciones) {
		this.imputaciones = imputaciones;
	}	

}
