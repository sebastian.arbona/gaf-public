package com.civitas.importacion;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.struts.upload.FormFile;

public class CSVReader {

	private String fieldDelimiter = ";";
	private String stringDelimiter = "\"";
	private FormFile file;
	private List<String> line = new ArrayList<String>();

	public CSVReader() {
		super();
	}

	public CSVReader(FormFile file) {
		super();
		this.file = file;
	}

	public CSVReader config(String fieldDelimiter, String stringDelimiter) {
		this.fieldDelimiter = fieldDelimiter;
		this.stringDelimiter = stringDelimiter;
		return this;
	}

	public CSVReader setFile(FormFile file) {
		this.file = file;
		return this;
	}

	public boolean isValid() {
		ArrayList<String> type = new ArrayList<String>();
		type.add("text/csv");
		type.add("text/plain");
		type.add("text/x-csv");
		type.add("text/plain");
		type.add("application/vnd.ms-excel");
		type.add("text/x-csv");
		type.add("application/csv");
		type.add("application/x-csv");
		type.add("text/comma-separated-values");
		type.add("text/x-comma-separated-values");
		type.add("text/tab-separated-values");

		boolean valid = false;
		try {
			for (String t : type) {
				if ((file.getContentType().contains(t))) {
					valid = true;
				}
			}

		} catch (Exception e) {
			valid = false;
		}
		return valid;
	}

	public List<String> getLine() {
		return this.line;
	}

	@SuppressWarnings("resource")
	public List<String> getListExchange() {

		if (this.isValid()) {

			try {
				Scanner scanner = new Scanner(this.file.getInputStream());
				while (scanner.hasNext()) {
					line.add(scanner.nextLine().toString());
				}
			} catch (Exception e) {

			}
		}
		return this.line;
	}

}
