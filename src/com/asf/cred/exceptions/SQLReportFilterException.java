package com.asf.cred.exceptions;

public class SQLReportFilterException extends Exception {
    private String[] columns;
    private String message="reportes.filtroVacio";

    public String[] getColumns() {
            return columns;
    }

    public void setColumns(String[] columns) {
            this.columns = columns;
    }

    public String getMessage() {
            return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
