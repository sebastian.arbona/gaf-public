package com.asf.cred.tld;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;

public class CallePopUp extends SimpleTagSupport{

    String property;
    String filter;
    String toolTips;
    String id;
    String value;
    String valueId;
    String attribs;
    String onChange;
    Boolean enabled = new Boolean( true );
    String idPopup;

    public void doTag() throws JspException{
        JspWriter out = getJspContext().getOut();
        try{
            JspFragment f = getJspBody();
            if( f != null ){
                f.invoke( out );
            }

            PageContext pageContext = ( PageContext ) getJspContext();
            HttpServletRequest request = ( HttpServletRequest ) pageContext.getRequest();

            String strHidden = "";
            String strInput = "";
            String strButton = "";
            String strDescriptor = "";
            String strCallToServer = "";
            String strName = this.property.replaceAll( "\\[", "_" ).replaceAll( "\\]", "_" ).replaceAll( "\\.", "_" );
            Object[] obj = { "", "", "" };
            List list = new ArrayList();

            BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

            if( this.value != null && this.value.length() > 0 ){
                list = bp.getByFilter( "SELECT c.codi08,c.deta08,c.idcalle FROM Calle c WHERE c.idcalle=" + this.value );
            }

            if( list.size() == 1 ){
                for( Iterator it = list.iterator(); it.hasNext(); ){
                    Object[] elem = ( Object[] ) it.next();
                    obj[0] = elem[0];
                    obj[1] = elem[1];
                    obj[2] = elem[2];
                }
            }

            strHidden = "<input type=\"hidden\" maxlength=\"12\" name=\"" + this.property + "\" id=\"" + this.property + "\" value=\"" + obj[2] + "\">\n";

            strCallToServer = "callToServer('" + request.getContextPath() + "/actions/callePopUp.do?do=get&filter=" +
                    this.filter + "&property=" + this.property + "&name=" + strName + "&value='+this.value);";

            strInput = "<input type=\"text\" maxlength=\"40\" name=\"" + strName + "Codigo\" id=\"" + strName + "Codigo\" size=\"10\"";

            if( attribs != null ){
                strInput += " " + attribs + " ";
            }

            if( value != null ){
                strInput += " value=\"" + obj[0].toString() + "\" ";
            }

            if( !this.enabled.booleanValue() ){
                strInput += " disabled";
            }

            if( this.toolTips != null && this.toolTips.length() != 0 ){
                strInput += " title=\"" + this.toolTips.toString() + "\" ";
            }

            if( onChange == null ){
                onChange = "";
            }

            strInput += " onChange=\"javascript:getElem('" + strName + "Descriptor').value='';" + strCallToServer + onChange + "\">\n";

            if( this.enabled.booleanValue() ){
                strButton += "<input type=\"button\" value=\"...\"";

                strButton += " onClick=\"popUp('" + request.getContextPath() + "/actions/callePopUp.do?do=list&" +
                        "name=" + strName + "&property=" + this.property + "&filter=" + this.filter + "'" +
                        ( idPopup != null && idPopup.length() > 0 ? ",'" + idPopup + "'" : "" ) + ");\">\n";
            }else{
                strButton = "";
            }

            strDescriptor = "<input type=\"text\" name=\"" + strName + "Descriptor\" id=\"" + strName + "Descriptor\" readonly=\"readonly\" " +
                    " size=\"40\" value=\"" + obj[1].toString() + "\"";

            if( this.id != null && !this.id.equals( "" ) ){
                strDescriptor += " id = \"" + this.id + "\">\n ";
            }else{
                strDescriptor += ">\n";
            }

            out.write( strHidden + strInput + strButton + strDescriptor );


        }catch( java.io.IOException ex ){
            throw new JspException( ex.getMessage() );
        }
    }

    public String getIdPopup(){
        return idPopup;
    }

    public void setIdPopup( String idPopup ){
        this.idPopup = idPopup;
    }

    public Boolean getEnabled(){
        return enabled;
    }

    public void setEnabled( Boolean enabled ){
        this.enabled = enabled;
    }

    public String getFilter(){
        return filter;
    }

    public void setFilter( String filter ){
        this.filter = filter;
    }

    public String getToolTips(){
        return toolTips;
    }

    public void setToolTips( String toolTips ){
        this.toolTips = toolTips;
    }

    public String getProperty(){
        return property;
    }

    public void setProperty( String property ){
        this.property = property;
    }

    public String getValue(){
        return value.toString();
    }

    public void setValue( String value ){
        this.value = value;
    }

    public String getId(){
        return id;
    }

    public void setId( String id ){
        this.id = id;
    }

    public String getAttribs(){
        return attribs;
    }

    public void setAttribs( String attribs ){
        this.attribs = attribs;
    }

    public String getOnChange(){
        return onChange;
    }

    public void setOnChange( String onChange ){
        this.onChange = onChange;
    }

    public String getValueId(){
        return valueId;
    }

    public void setValueId( String valueId ){
        this.valueId = valueId;
    }

}
