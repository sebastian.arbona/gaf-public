package com.asf.cred;

public class Constants 
{
	static final public Long CODI_01 = new Long(101);
	
    public static final Integer MAX_SELECT_POPUP_RECORDS = new Integer(1500);
    public static final Integer MAX_LEGAJO_POPUP_RECORDS = new Integer(1500);
    
}
