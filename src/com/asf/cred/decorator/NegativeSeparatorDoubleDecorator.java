package com.asf.cred.decorator;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class NegativeSeparatorDoubleDecorator implements ColumnDecorator{
	
	public static final DecimalFormat format = new DecimalFormat("#,##0.00;(-#,##0.00)");
	
	@Override
	public String decorate(Object columnValue) throws DecoratorException {
		if (columnValue instanceof Number) {
			return format.format(columnValue);
		} else {
			return columnValue != null ? columnValue.toString() : "";
		}
	}

}
