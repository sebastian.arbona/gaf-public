package com.asf.cred.decorator;

import java.text.DecimalFormat;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class DecimalDecorator implements ColumnDecorator {

	public String decorate(Object o) throws DecoratorException {
		DecimalFormat df = new DecimalFormat("0.00###");

		if (o == null || !(o instanceof Double))
			return "0.00";
		else
			return (df.format((Double)o)).toString().replaceAll(",", ".");
	}
}