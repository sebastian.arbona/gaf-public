package com.asf.cred.decorator;

import org.displaytag.exception.DecoratorException;

import com.asf.displayDecorators.DoubleDecorator;

public class NegativeDoubleDecorator extends DoubleDecorator{
	
	@Override
	public String decorate(Object columnValue) throws DecoratorException {
		String valor = super.decorate(columnValue);
		return "(" + valor + ")";
	}

}
