package com.asf.cred.decorator;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class ColorDecorator implements ColumnDecorator {
	public String decorate(Object value) throws DecoratorException {
		if (value != null) {
			return "<table border=\"1\" bgcolor=\"#"
					+ value
					+ "\"><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</tr></td></table>";
		} else {
			return "";
		}
	}

}
