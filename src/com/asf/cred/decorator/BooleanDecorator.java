package com.asf.cred.decorator;

import org.displaytag.decorator.ColumnDecorator;
import org.displaytag.exception.DecoratorException;

public class BooleanDecorator implements ColumnDecorator {
	public String decorate(Object value) throws DecoratorException {
		if (value != null) {
			return (value.toString().equals("true") ? "S�" : "No");
		} else {
			return "";
		}
	}

}
