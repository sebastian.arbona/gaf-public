package com.asf.cred;

import java.util.List;

import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;

public class TipificadorHandler
{
//===================ATRIBUTOS====================
	//atributos utilitarios del proceso.-
	private static BusinessPersistance bp;
	//atributos del proceso.-

//=================CONSTRUCTORES==================
	public TipificadorHandler()
	{
		TipificadorHandler.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

//===============FUNCIONALIDADES==================

	/**
	 * Retorna un �nico tipificador seg�n la categor�a y el c�digo.-
	 * Si no encuentra tipificador, explota.-
	 * Si encuentra m�s de un tipificador, explota.-
	 */
	public static Tipificadores getTipificador( String categoria, String codigo ) throws Exception
	{
		Tipificadores tipificador	= null;
		String consulta				= "";

		try
		{
			TipificadorHandler.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			consulta += "FROM Tipificadores tf WHERE tf.categoria = '" + categoria.toString() + "'";
			consulta += " AND tf.codigo = '" + codigo.toString() + "'";
			tipificador = (Tipificadores)TipificadorHandler.bp.getByFilter( consulta ).get( 0 );
			tipificador.getId();
		}
		catch( Exception e )
		{
			tipificador = null;
			throw e;
		}

		return tipificador;
	}//fin getTipificador.-

	/**
	 * Retorna un listado de tipificadores seg�n una determinada categor�a.-
	 * 
	 * @param categoria
	 * @return
	 * @throws Exception
	 */
	public static List<Tipificadores> getTipificadores( String categoria ) throws Exception
	{
		List<Tipificadores> tipificadores	= null;
		String consulta						= "";

		try
		{
			TipificadorHandler.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			consulta += "FROM Tipificadores tf WHERE tf.categoria = '" + categoria.toString() + "'";
			tipificadores = TipificadorHandler.bp.getByFilter( consulta );
		}
		catch( Exception e )
		{
			tipificadores = null;
			throw e;
		}

		return tipificadores;
	}//fin getTipificadores.-
//=============UTILIDADES PRIVADAS================
//==============GETTERS Y SETTERS=================
}