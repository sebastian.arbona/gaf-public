package com.asf.cred;

public class StringHelper {

	public static final String completarCeros(String cadena, int digitos) {
		int inicial = 0;
		if (cadena != null) {
			inicial = cadena.length();
		}
		StringBuffer sb = new StringBuffer();
		for (int n = inicial; n < digitos; n++) {
			sb.append("0");
		}
		sb.append(cadena);

		return sb.toString();
	}

	public static final String completarConCaracter(String cadena, int longitud, String relleno) {
		int inicial = 0;

		if (!cadena.isEmpty()) {
			inicial = cadena.length();

		}
		for (int n = inicial; n < longitud; n++) {
			cadena = cadena.concat(relleno);
		}

		return cadena;
	}
}
