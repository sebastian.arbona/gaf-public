package com.asf.cred.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.asf.util.BeanHelper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class RestResponse {
	
	private JsonObject errors = new JsonObject();
	private JsonArray dataJsonArray = new JsonArray();

	
	private HttpServletResponse response;
	
	public RestResponse(HttpServletResponse response) throws IOException {
		this.response = response;
	}
	
	public void print() throws IOException {
		JsonObject output = new JsonObject();

		output.add("data",dataJsonArray);
		output.add("errors", errors);
		
		if(errors.size() > 0) {
			response.setStatus(422);
		}
		response.getWriter().print(output.toString());
	}
	
	public RestResponse setData(List<Object[]> listData, String[] listFields) throws Exception {
      
        for (Iterator<Object[]> it = listData.iterator(); it.hasNext();) {
        	Object objectIterance = it.next();                
        	JsonObject jsonObjectIterance = new JsonObject();            	
        	for (int i = 0; i < listFields.length; i++) {
        		jsonObjectIterance.addProperty(listFields[i].replaceAll("\\.", "_"), BeanHelper.parseGet(objectIterance, listFields[i]));            		
            }
        	dataJsonArray.add(jsonObjectIterance);
        }
        return this;	
	}
	
	public RestResponse setErrors(HashMap<String, String> errors) {
		
		Iterator it = errors.entrySet().iterator();
		JsonObject jsonObjectIterance = new JsonObject();
		while(it.hasNext()) {
			Map.Entry e = (Map.Entry) it.next();
			this.errors.addProperty(e.getKey().toString(), e.getValue().toString());
		}
		return this;
	}

}

