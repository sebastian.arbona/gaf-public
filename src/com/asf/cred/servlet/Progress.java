package com.asf.cred.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asf.cred.business.ProgressStatus;

public class Progress extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public Progress() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request  the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException      if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/xml");
		response.setCharacterEncoding("iso-8859-1");
		PrintWriter out = response.getWriter();

		String strXML = "<root>";
		strXML += "<total>" + com.asf.cred.business.ProgressStatus.iTotal + "</total>";
		strXML += "<progreso>" + com.asf.cred.business.ProgressStatus.iProgress + "</progreso>";
		strXML += "<status>" + com.asf.cred.business.ProgressStatus.sStatus + "</status>";
		strXML += "<abort>" + ProgressStatus.abort + "</abort>";
		strXML += "</root>";

		response.setContentType("text/xml");

		out.println(strXML);
		out.flush();
		out.close();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		super.doPost(request, resp);
		if (request.getParameter("abort") != null) {
			ProgressStatus.abort = true;
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
