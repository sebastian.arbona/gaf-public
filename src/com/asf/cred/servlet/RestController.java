package com.asf.cred.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.CastRequestHelper;
import com.nirven.expedientes.persistencia.Archivo;

public abstract class RestController extends DispatchAction {
	
	protected HashMap<String, String> errors = new HashMap<String, String>();
	
	protected RestController() {}
	
	protected String getListQueryString(HttpServletRequest req) {
		return null;
	}
	
	protected List<String> getListMap() {
		return null;		
	}
	
	protected RestResponse getObjectMapped(RestResponse restResponse) {
		return null;
	}
	
	protected boolean validate(HttpServletRequest request) {
		return false;
	}
	
	protected void save(HttpServletRequest request) {}
	
	protected void save(HttpServletRequest request, Archivo archivo) {}
	
	public void doGet(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		printResponseFromMappedEntity(request, response);
	}
	
	public void doPost(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		RestResponse restResponse = new RestResponse(response);
		errors = new HashMap<String, String>();
		if(validate(request)) {
			save(request);
		}
		restResponse.setErrors(errors).print();
	}
	
	public void doDelete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		RestResponse restResponse = new RestResponse(response);
		Object o = new Object();
		o = getMainObject(request);
		bp.delete(o);
		restResponse.setErrors(errors).print();
	}
	
	protected void printResponseFromMappedEntity(HttpServletRequest request, HttpServletResponse response) throws IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		RestResponse rest = new RestResponse(response);			
		
		String entityName = null;
		String filtro = null;
		String orden = null;
		String campos = null;
		String listaCampos[] = null;
        List<Object[]> datos = null;
        errors = new HashMap<String, String>();
       
		try {
			entityName 	= CastRequestHelper.toString("entidad", request);
			filtro 		= CastRequestHelper.toString("filtro", request);
			orden 		= CastRequestHelper.toString("orden", request);
			campos 		= CastRequestHelper.toString("campos", request);
			
			if (entityName == null || entityName.isEmpty()) {
				errors.put("entidad", "No se ha especificado el parámetro: Entidad");
			}
			else if (campos == null || campos.isEmpty() || campos.equals("*")) {
				errors.put("campos", "No se ha especificado el parámetro: Campos");
			}
			if (errors.isEmpty()) {
				listaCampos = campos.split(",");
				
				String consulta = "FROM " + entityName;
	            if (filtro != null && !"".equals(filtro) && !"null".equals(filtro)) {
	                consulta += " WHERE " + filtro;
	            }
	            if (orden != null && !"".equals(orden) && !"null".equals(orden)) {
	                consulta += " ORDER BY " + orden;
	            }
	            datos = bp.getByFilter(consulta);
	            rest.setData(datos, listaCampos);            
			}			
			rest.setErrors(errors).print();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected Object initMainObject (Object o, Class pClass, String pKey, HttpServletRequest request) {		
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if(o == null) {			
			Long idObject = CastRequestHelper.toLong(pKey, request);
			if(idObject != null) {
				o = bp.getById(pClass, idObject);
			}else {
				o = pClass.cast(o);
			}
		}
		return o;
	}
	
	protected Object getMainObject(HttpServletRequest request) {return null;}
	
	protected RestResponse buildJsonObject(RestResponse restResponse) {
		return restResponse;
	}
	
	protected Object getObjectByRequestId (Class pClass, String pKey, HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Object o;
		Long idObject = CastRequestHelper.toLong(pKey, request);
		if(idObject == null) {
			return null;
		}else {
			o = bp.getById(pClass, idObject);
			return o;
		}		
	}	
}
