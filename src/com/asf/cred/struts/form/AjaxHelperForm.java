/*
 * AjaxHelperForm.java
 *
 * Created on 21 de julio de 2006, 14:49
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.asf.cred.struts.form;

import org.apache.struts.action.ActionForm;

/**
 *
 * @author aromeo
 */
public class AjaxHelperForm extends ActionForm
{
	private static final long serialVersionUID = 5;
//================ATRIBUTOS========================
	String listValues;
	String entityName;
	String filter;
	String filter2;
//================CONSTRUCTORES======================
    public AjaxHelperForm() 
    {
    }

	public String getEntityName() 
	{
		return this.entityName;
	}
//================GETTERS y SETTERS===================
	public String getListValues() 
	{
		return this.listValues;
	}
	public void setListValues(String listValues) 
	{
		this.listValues = listValues;
	}
	public void setEntityName(String entityName) 
	{
		this.entityName = entityName;
	}
	public String getFilter() 
	{
		return this.filter;
	}
	public void setFilter(String filter) 
	{
		this.filter = filter;
	}
	public String getFilter2()
	{
		return this.filter2;
	}
	public void setFilter2(String filter2) 
	{
		this.filter2 = filter2;
	}
}