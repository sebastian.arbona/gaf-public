package com.asf.cred.struts.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.action.BeanDeuda;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.Linea;

public class InformeEnteRecaudadorForm extends ActionForm {
	private static final long serialVersionUID = 5576551454617071889L;
	
	private String fechaHasta;
	private String fechaVigencia;
	private String tipoRegistros;
	private String tipoInforme;
	
	private List<BeanDeuda> listado;
	private String nombreArchivo;
	
	private String ultimaActualizacion;
	private String ultimaRecaudacion;
	private String ultimaCobranza;
	private String ultimaActualizacionEstado;
	
	private String cid;
	
	private String[] lineasSeleccionadas;
	private List<Linea> lineas;
	private Long idInforme;
	
	@SuppressWarnings("unchecked")
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lineas = bp.createQuery("select l from Linea l order by l.nombre").list();
	}
	
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getFechaVigencia() {
		return fechaVigencia;
	}
	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}
	public List<BeanDeuda> getListado() {
		return listado;
	}
	public void setListado(List<BeanDeuda> listado) {
		this.listado = listado;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getUltimaActualizacion() {
		return ultimaActualizacion;
	}
	public void setUltimaActualizacion(String ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}
	public String getUltimaRecaudacion() {
		return ultimaRecaudacion;
	}
	public void setUltimaRecaudacion(String ultimaRecaudacion) {
		this.ultimaRecaudacion = ultimaRecaudacion;
	}
	public String getUltimaCobranza() {
		return ultimaCobranza;
	}
	public void setUltimaCobranza(String ultimaCobranza) {
		this.ultimaCobranza = ultimaCobranza;
	}
	public String getUltimaActualizacionEstado() {
		return ultimaActualizacionEstado;
	}
	public void setUltimaActualizacionEstado(String ultimaActualizacionEstado) {
		this.ultimaActualizacionEstado = ultimaActualizacionEstado;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getTipoRegistros() {
		return tipoRegistros;
	}
	public void setTipoRegistros(String tipoRegistros) {
		this.tipoRegistros = tipoRegistros;
	}
	public String getTipoInforme() {
		return tipoInforme;
	}
	public void setTipoInforme(String tipoInforme) {
		this.tipoInforme = tipoInforme;
	}
	public String[] getLineasSeleccionadas() {
		return lineasSeleccionadas;
	}
	public void setLineasSeleccionadas(String[] lineasSeleccionadas) {
		this.lineasSeleccionadas = lineasSeleccionadas;
	}

	public String getIdsLineas() {
		String ids = "";
		if (lineasSeleccionadas != null) {
			for (String l : lineasSeleccionadas) {
				ids += l + ",";
			}
			if (!ids.isEmpty()) {
				ids = ids.substring(0, ids.length() - 1);
			}
		}
		return ids;
	}
	public List<Linea> getLineas() {
		return lineas;
	}
	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public Long getIdInforme() {
		return idInforme;
	}

	public void setIdInforme(Long idInforme) {
		this.idInforme = idInforme;
	}
	
}
