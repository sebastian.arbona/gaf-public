package com.asf.cred.struts.form;

import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import com.nirven.creditos.hibernate.Objetoi;

public class AsociarContratoExportForm extends ActionForm {

	private static final long serialVersionUID = 1L;

	private Long linea = new Long(0);
	private List<Objetoi> result;
	private Long escrito;
	private Long idObjetoi;

	private FormFile formFile;
	public Long idTipoDeArchivo;

	public AsociarContratoExportForm() {
	}

	public void setLinea(Long linea) {
		this.linea = linea;
	}

	public Long getLinea() {
		return linea;
	}

	public void setResult(List<Objetoi> result) {
		this.result = result;
	}

	public List<Objetoi> getResult() {
		return result;
	}

	public void setEscrito(Long escrito) {
		this.escrito = escrito;
	}

	public Long getEscrito() {
		return escrito;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public Long getIdTipoDeArchivo() {
		return idTipoDeArchivo;
	}

	public void setIdTipoDeArchivo(Long idTipoDeArchivo) {
		this.idTipoDeArchivo = idTipoDeArchivo;
	}

}
