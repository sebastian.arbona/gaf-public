package com.asf.cred.struts.form;

import org.apache.struts.upload.FormFile;

public class EscritoForm extends  com.asf.cred.struts.form.AbmForm{

	private static final long serialVersionUID = 1L;
	
	private FormFile theFile;
	
	public EscritoForm() {
	}
	
	public FormFile getTheFile() {
		return theFile;
	}
	
	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}
}