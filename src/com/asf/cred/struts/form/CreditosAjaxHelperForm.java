package com.asf.cred.struts.form;

import org.apache.struts.action.ActionForm;

/**
 *
 * @author eselman
 */
public class CreditosAjaxHelperForm extends ActionForm
{
	private static final long serialVersionUID = 5;
//===============ATRIBUTOS=================================
	String listValues;
	String entityName;
	String filter;
//===============CONSTRUCTORES==============================    
    public CreditosAjaxHelperForm() 
    {
    	
    }
//===============GETTERS Y SETTERS==========================
	public String getEntityName() 
	{
		return this.entityName;
	}
	public void setEntityName(String entityName) 
	{
		this.entityName = entityName;
	}
	public String getFilter() 
	{
		return this.filter;
	}
	public void setFilter(String filter) 
	{
		this.filter = filter;
	}
	public String getListValues() 
	{
		return this.listValues;
	}
	public void setListValues(String listValues) 
	{
		this.listValues = listValues;
	}
}