
package com.asf.cred.struts.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import com.asf.security.SessionHandler;
import com.civitas.importacion.PagoCreditoBean;
import com.nirven.creditos.hibernate.Boleto;

/**
 *
 * @author lfonolla
 */
public class AbmFormCaratula extends com.asf.struts.form.AbmForm {
	private static final long serialVersionUID = -7879557872971819586L;
	public static final String PAGOS_IMPORTADOS = "PAGOS_IMPORTADOS";
	private String[] lista = new String[] {};

	public AbmFormCaratula() {
		super();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String[] getLista() {
		List lstLista = new ArrayList();
		String[] asBoletos = getBoletos();

		for (int i = 0; i < this.lista.length; i++) {
			lstLista.add(this.lista[i]);
		}

		for (int i = 0; i < asBoletos.length; i++) {
			lstLista.add(asBoletos[i]);
		}

		return (String[]) lstLista.toArray(new String[] {});
	}

	public void setLista(String[] lista) {
		this.lista = lista;
	}

	@SuppressWarnings("rawtypes")
	public String[] getBoletos() {
		Map hsBoletos = null;
		String[] asBoletos = new String[] {};
		int iIndice = 0;

		hsBoletos = (SortedMap) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute(PAGOS_IMPORTADOS);

		if (hsBoletos != null) { // Si existe en la sesion
			asBoletos = new String[hsBoletos.size()];

			for (Iterator it = hsBoletos.keySet().iterator(); it.hasNext();) {
				Long lKey = (Long) it.next();

				Boleto boleto = ((PagoCreditoBean) hsBoletos.get(lKey)).getBoleto();

				asBoletos[iIndice++] = boleto.getNumeroBoleto() + "-" + boleto.getVerificadorBoleto() + "-"
						+ boleto.getPeriodoBoleto();
			}
		}

		return asBoletos;
	}

	@SuppressWarnings("unchecked")
	public Date getFechaPago(Boleto boleto) {
		Map<Long, PagoCreditoBean> hsBoletos = (SortedMap<Long, PagoCreditoBean>) SessionHandler
				.getCurrentSessionHandler().getRequest().getSession().getAttribute(PAGOS_IMPORTADOS);
		if (hsBoletos != null) { // Si existe en la sesion
			for (PagoCreditoBean bean : hsBoletos.values()) {
				if (boleto.getId().equals(bean.getBoleto().getId())) {
					return bean.getFechaPago();
				}
			}
		}
		return null;
	}

}
