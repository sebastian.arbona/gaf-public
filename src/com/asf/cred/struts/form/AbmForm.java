package com.asf.cred.struts.form;

import com.asf.security.SessionHandler;


public class AbmForm extends com.asf.struts.form.AbmForm{
	
	@Override
	public String getFullClassName( String entityName ){
		 if( "Director,Menu,Tipificadores,Claves,ClavesEsquema,Columna,Filtro,Permiso,Reporte,Formatoimportacion,Formatoitem,Errorimportacion".contains( entityName ) ){
			 return "com.asf.hibernate.mapping." + entityName;
		 }
		 
		 if( "Moneda,Cotizacion,Bancos".contains( entityName ) ){
			 return "com.asf.gaf.hibernate." + entityName;
		 }
		
		 if( "Archivo,Barrio,Calle,CondicionIva,Empresa,Domicilio,Estado,EstaPer,Localidad,Nacionalidad,Pais,Persona,PersonaVinculada,Provin,Relacion,TipoArchivo,Tipodoc,TipoRelacion,Tsoci,Zonasgeo".contains( entityName ) ){
			 return "com.civitas.hibernate.persona." + entityName;
		 }		
		 
		 if( "CategoriaEntidad,Contacto,EntidadFinanciera,Observacion,RefLabProf,SituacionEntidad,TipoRelacionLaboral".contains( entityName ) ){
			 return "com.civitas.hibernate.persona.creditos." + entityName;
		 }
		
		 if( "Unidad".contains( entityName ) ){
			 return "com.nirven.expedientes.persistencia." + entityName;
		 }
		 
		 return SessionHandler.getCurrentSessionHandler().getPERSISTANCE_PACKAGE() + "." + entityName;
	 }

}
