package com.asf.cred.struts.form;

import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import com.nirven.creditos.hibernate.Objetoi;

public class AsociarResolucionExportForm extends ActionForm {

	private static final long serialVersionUID = 1L;

	private Long linea;
	private List<Objetoi> result;
	private Long escrito;
	public Long objetoiId;
	private String resolucion;
	private Boolean ocultarGenerar;
	private String script;
	private String htmlDocumento;
	private FormFile formFile;
	public Long idTipoDeArchivo;

	public AsociarResolucionExportForm() {
		ocultarGenerar = false;
		script = "";
	}

	public void setLinea(Long linea) {
		this.linea = linea;
	}

	public Long getLinea() {
		return linea;
	}

	public void setResult(List<Objetoi> result) {
		this.result = result;
	}

	public List<Objetoi> getResult() {
		return result;
	}

	public void setEscrito(Long escrito) {
		this.escrito = escrito;
	}

	public Long getEscrito() {
		return escrito;
	}

	public Long getObjetoiId() {
		return objetoiId;
	}

	public void setObjetoiId(Long objetoiId) {
		this.objetoiId = objetoiId;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public Boolean getOcultarGenerar() {
		return ocultarGenerar;
	}

	public void setOcultarGenerar(Boolean ocultarGenerar) {
		this.ocultarGenerar = ocultarGenerar;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getHtmlDocumento() {
		return htmlDocumento;
	}

	public void setHtmlDocumento(String htmlDocumento) {
		this.htmlDocumento = htmlDocumento;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public Long getIdTipoDeArchivo() {
		return idTipoDeArchivo;
	}

	public void setIdTipoDeArchivo(Long idTipoDeArchivo) {
		this.idTipoDeArchivo = idTipoDeArchivo;
	}

}
