package com.asf.cred.struts.form;

import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import com.nirven.creditos.hibernate.ConsultaDeudaPersona;

public class ImportacionForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private FormFile file = null;
	private Long idFormato = 1L;
	private Long totalBoletos = 0L;
	private Double totalPagado = 0.0;
	private String script = "";
	private Long nroReporte;
	private Object result;
	private Boolean deshabilitado = false;
	private Long idCaratula;
	private Long idDetalleArchivo;
	private String resumen;
	private boolean todoOk;
	private List<ConsultaDeudaPersona> consultaDeudaPersona;

	public ImportacionForm() {
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

	public Long getIdFormato() {
		return idFormato;
	}

	public void setIdFormato(Long idFormato) {
		this.idFormato = idFormato;
	}

	public Long getTotalBoletos() {
		return totalBoletos;
	}

	public void setTotalBoletos(Long totalBoletos) {
		this.totalBoletos = totalBoletos;
	}

	public Double getTotalPagado() {
		return totalPagado;
	}

	public void setTotalPagado(Double totalPagado) {
		this.totalPagado = totalPagado;
	}

	public Long getNroReporte() {
		return nroReporte;
	}

	public void setNroReporte(Long nroReporte) {
		this.nroReporte = nroReporte;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
	public Boolean getDeshabilitado() {
		return deshabilitado;
	}
	
	public void setDeshabilitado(Boolean deshabilitado) {
		this.deshabilitado = deshabilitado;
	}
	
	public Long getIdCaratula() {
		return idCaratula;
	}
	
	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}
	
	public Long getIdDetalleArchivo() {
		return idDetalleArchivo;
	}
	
	public void setIdDetalleArchivo(Long idDetalleArchivo) {
		this.idDetalleArchivo = idDetalleArchivo;
	}
	
	public String getResumen() {
		return resumen;
	}
	
	public void setResumen(String resumen) {
		this.resumen = resumen;
	}
	
	public boolean isTodoOk() {
		return todoOk;
	}
	
	public void setTodoOk(boolean todoOk) {
		this.todoOk = todoOk;
	}
	

	public List<ConsultaDeudaPersona> getConsultaDeudaPersona() {
		return consultaDeudaPersona;
	}

	public void setConsultaDeudaPersona(List<ConsultaDeudaPersona> consultaDeudaPersona) {
		this.consultaDeudaPersona = consultaDeudaPersona;
	}

}
