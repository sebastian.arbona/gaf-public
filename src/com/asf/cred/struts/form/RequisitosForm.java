package com.asf.cred.struts.form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import com.nirven.creditos.hibernate.CargaRequisito;
import com.nirven.creditos.hibernate.Objetoi;

public class RequisitosForm extends ActionForm {
	private static final long serialVersionUID = 1L;

	private List<CargaRequisito> result;
	
    private Objetoi credito;
    private String idEntity;
    private String sol;
    private Boolean requisitosCumplidos;
    private Boolean obligatorio;
    
    private Boolean personasFisicas;
    private Boolean personasJuridicas;

    private List<CargaRequisito> cargaRqList = new ArrayList<CargaRequisito>();
    private HashMap<String, String> requisitos = new HashMap<String, String>();
    private HashMap<String, String> observaciones = new HashMap<String, String>();
    private HashMap<String, String> ids = new HashMap<String, String>();
    private HashMap<String, String> fechas = new HashMap<String, String>();
    private HashMap<String, String> archivos = new HashMap<String, String>();
    private HashMap<String, FormFile> formFiles = new HashMap<String, FormFile>();
    
    private HashMap<String, Boolean> selecciones = new HashMap<String, Boolean>();
    private HashMap<String, Boolean> nc = new HashMap<String, Boolean>();
    
    private FormFile f;

	public RequisitosForm() {
	}

	public void setResult(List<CargaRequisito> result) {
		this.result = result;
	}

	public List<CargaRequisito> getResult() {
		return result;
	}

	public List<CargaRequisito> getCargaRqList() {
		return cargaRqList;
	}

	public void setCargaRqList(List<CargaRequisito> cargaRqList) {
		this.cargaRqList = cargaRqList;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public String getSol() {
		return sol;
	}

	public void setSol(String sol) {
		this.sol = sol;
	}

	public void setTestFile(String iIndex, FormFile formFile) {
		f = formFile;
		this.formFiles.put(iIndex, formFile);
	}

	public FormFile getTestFile(String iIndex) {
		while (this.formFiles.get(iIndex) == null) {
			this.formFiles.put(iIndex, f);
		}
		return (FormFile) formFiles.get(iIndex);
	}

	public HashMap<String, FormFile> getTestFiles() {
		return this.formFiles;
	}

	public HashMap<String, String> getRequisitos() {
		return requisitos;
	}

	public void setRequisitos(HashMap<String, String> requisitos) {
		this.requisitos = requisitos;
	}

	public HashMap<String, String> getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(HashMap<String, String> observaciones) {
		this.observaciones = observaciones;
	}

	public HashMap<String, String> getIds() {
		return ids;
	}

	public void setIds(HashMap<String, String> ids) {
		this.ids = ids;
	}

	public HashMap<String, String> getFechas() {
		return fechas;
	}

	public void setFechas(HashMap<String, String> fechas) {
		this.fechas = fechas;
	}

	public HashMap<String, String> getArchivos() {
		return archivos;
	}

	public void setArchivos(HashMap<String, String> archivos) {
		this.archivos = archivos;
	}

	public void setId(String iIndex, String id) {
		this.ids.put(iIndex, id);
	}

	public String getId(String iIndex) {
		while (this.ids.get(iIndex) == null) {
			this.ids.put(iIndex, new String());
		}
		return ids.get(iIndex);
	}

	public void setObservacion(String iIndex, String obs) {
		this.observaciones.put(iIndex, obs);
	}

	public String getObservacion(String iIndex) {
		while (this.observaciones.get(iIndex) == null) {
			this.observaciones.put(iIndex, new String());
		}
		return observaciones.get(iIndex);
	}

	public void setArchivo(String iIndex, String obs) {
		this.archivos.put(iIndex, obs);
	}

	public String getArchivo(String iIndex) {
		while (this.archivos.get(iIndex) == null) {
			this.archivos.put(iIndex, new String());
		}
		return archivos.get(iIndex);
	}

	public void setRequisito(String iIndex, String re) {
		this.requisitos.put(iIndex, re);
	}

	public String getRequisito(String iIndex) {
		while (this.requisitos.get(iIndex) == null) {
			this.requisitos.put(iIndex, new String());
		}
		return requisitos.get(iIndex);
	}

	public void setFecha(String iIndex, String fe) {
		this.fechas.put(iIndex, fe);
	}

	public String getFecha(String iIndex) {
		while (this.fechas.get(iIndex) == null) {
			this.fechas.put(iIndex, new String());
		}
		return fechas.get(iIndex);
	}

	public String getIdEntity() {
		return idEntity;
	}

	public void setIdEntity(String idEntity) {
		this.idEntity = idEntity;
	}

	public void setSeleccion(String iIndex, Boolean sel) {
		this.selecciones.put(iIndex, sel);
	}

	public Boolean getSeleccion(String iIndex) {
		while (this.selecciones.get(iIndex) == null) {
			this.selecciones.put(iIndex, new Boolean(false));
		}
		return selecciones.get(iIndex);
	}
	
	public void setNC(String iIndex, Boolean sel) {
		this.nc.put(iIndex, sel);
	}

	public Boolean getNC(String iIndex) {
		while (this.nc.get(iIndex) == null) {
			this.nc.put(iIndex, new Boolean(false));
		}
		return nc.get(iIndex);
	}
	
	public HashMap<String, Boolean> getNc() {
		return nc;
	}
	
	public void setNc(HashMap<String, Boolean> nc) {
		this.nc = nc;
	}
	
	public HashMap<String, Boolean> getSelecciones() {
		return selecciones;
	}

	public void setSelecciones(HashMap<String, Boolean> seleccion) {
		this.selecciones = seleccion;
	}

	public void setRequisitosCumplidos(Boolean requisitosCumplidos) {
		this.requisitosCumplidos = requisitosCumplidos;
	}

	public Boolean getRequisitosCumplidos() {
		if(requisitosCumplidos==null){
			requisitosCumplidos = false;
		}
		return requisitosCumplidos;
	}
	
	public Boolean getTieneExpediente() {
		if(credito == null || obligatorio == null){
			return false;
		}
		if(obligatorio == false){
			return false;
		}
		if(credito.getExpediente() == null || credito.getExpediente().isEmpty() || credito.getExpediente().equals("0")){
			return false;
		}else{
			return true;
		}
	}	
	
	public void setTieneExpediente(Boolean b){
		
	}
	
	public void setObligatorio(Boolean obligatorio) {
		this.obligatorio = obligatorio;
	}

	public Boolean getObligatorio() {
		return obligatorio;
	}
	
	public String getEstadoObjetoi(){
		if(credito != null)
			return credito.getEstadoActual().getEstado().getNombreEstado().trim();
		else
			return null;
	}

	public Boolean getPersonasFisicas() {
		return personasFisicas;
	}

	public void setPersonasFisicas(Boolean personasFisicas) {
		this.personasFisicas = personasFisicas;
	}

	public Boolean getPersonasJuridicas() {
		return personasJuridicas;
	}

	public void setPersonasJuridicas(Boolean personasJuridicas) {
		this.personasJuridicas = personasJuridicas;
	}
	
	
}
