/*
 * AbmForm.java
 *
 * Created on 4 de noviembre de 2005, 14:52
 */

package com.asf.cred.struts.form;
import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import java.util.List;

/**
 *
 * @author cgarcia
 * @version
 */

public class CallePopUpForm extends ActionForm {
    private String calle;
    private String codigo;

    private String property;
    private String filter;
    private String name;
    private String value;
    private String valueId;
    
    private List calleList=new ArrayList();

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List getCalleList() {
        return calleList;
    }

    public void setCalleList(List calleList) {
        this.calleList = calleList;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}