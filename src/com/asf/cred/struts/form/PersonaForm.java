/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asf.cred.struts.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author cnoguerol
 */
public class PersonaForm extends com.asf.cred.struts.form.AbmForm {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5120699346682629316L;
	private Long idPais;

    public PersonaForm() {
    }

    @Override
    public ActionErrors validate(ActionMapping pMapping, HttpServletRequest pRequest) {
        return super.validate(pMapping, pRequest);
    }

    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }
}
