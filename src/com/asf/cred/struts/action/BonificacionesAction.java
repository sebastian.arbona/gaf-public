package com.asf.cred.struts.action;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.asf.cred.business.CreditoHandler;
import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.AbmForm;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.AbmAction;
import com.asf.util.DateHelper;
import com.civitas.importacion.errores.Errores;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.Tipomov;

public class BonificacionesAction extends AbmAction {

	private String error = "";

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward save(ActionMapping arg0, ActionForm arg1, HttpServletRequest arg2, HttpServletResponse arg3)
			throws Exception {
		Errores.getInstancia().resetErrores();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		bp.begin();
		ActionForward forward = super.save(arg0, arg1, arg2, arg3);
		ObjetoiBonificacion objetoiBonificacion = (ObjetoiBonificacion) ((AbmForm) arg1).getEntity();
		String idCredito = String.valueOf(objetoiBonificacion.getIdObjetoi());
		List<Cuota> listaCuotas = (List<Cuota>) bp.getNamedQuery("Cuota.findByObjetoi")
				.setString("idCredito", idCredito).list();
		CreditoHandler creditoHandler = new CreditoHandler();
		Tipomov tipoMovimiento = null;
		double bonificacionNueva = 0.0;
		for (Cuota cuota : listaCuotas) {
			if (cuota.getFechaVencimiento().after(new Date())) {
				bonificacionNueva = calcularBonificacion(bp, cuota);
				// Si la nueva bonificacion es mayor a la anterior, la cuota no
				// esta vencida, y ya esta cancelada
				if (creditoHandler.calcularPagos(cuota) > 0) {
					if (bonificacionNueva > cuota.getBonificacion()) {
						tipoMovimiento = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
								.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
					} else {
						tipoMovimiento = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
								.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
					}
					Ctacte cc = creditoHandler.crearCtaCte(cuota, "bon", tipoMovimiento,
							bonificacionNueva - cuota.getBonificacion());
					cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_BONIFICACION);
					bp.update(cc);
				}
			}
			cuota.setBonificacion(bonificacionNueva);
			bp.update(cuota);
		}
		if (!(validarBonificacion(objetoiBonificacion.getIdObjetoi(), objetoiBonificacion))) {
			ActionMessages actionmsg = new ActionMessages();
			actionmsg.add("errors.detail", new ActionMessage("errors.detail", error));
			saveErrors(arg2, actionmsg);
			bp.rollback();
		} else {
			bp.commit();
		}
		this.list(arg0, arg1, arg2, arg3);
		return forward;
	}

	private boolean validarBonificacion(Long id, ObjetoiBonificacion bon) {
		if (bonMaxConvenio(id, bon) && noVencido(bon) && financiamientoMaxConvenio(bon)
				&& financiamientoMaxCredito(id, bon) && bonMaxCredito(bon) && tasaNetaInferior(bon)
				&& financiamientoMaxPersona(bon.getObjetoi().getPersona_id(), bon)
				&& bonMaxPersona(bon.getObjetoi().getPersona_id(), bon)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean bonMaxConvenio(Long id, ObjetoiBonificacion bon) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long convenio_id = bon.getBonificacion().getConvenio().getId();
		Double bonificacionEstimada = (Double) bp
				.createSQLQuery("SELECT SUM(bd.monto) " + "FROM ObjetoiBonificacion ob "
						+ "JOIN Bonificacion b ON ob.idBonificacion = b.id "
						+ "JOIN ConvenioBonificacion c ON b.convenio_id = c.id "
						+ "JOIN BonDetalle bd ON bd.objetoiBonificacion_id = ob.id " + "WHERE c.id = :convenio_id")
				.setLong("convenio_id", convenio_id).uniqueResult();
		if (bonificacionEstimada == null)
			bonificacionEstimada = 0.0;
		if (bon.getBonificacion().getConvenio().getMaxBonConvenio() >= bonificacionEstimada) {
			return true;
		} else
			error = "No se puede asociar la Bonificaci�n, supera el monto m�ximo del Convenio";
		return false;
	}

	public boolean IsNaN(Double d) {
		return (d != d);
	}

	private boolean bonMaxCredito(ObjetoiBonificacion bon) {
		CreditoHandler handler = new CreditoHandler();
		Double bonificacionEstimada = 0.0;
		handler.calcularBonificacionEstimada(bon.getObjetoi().getId());
		for (BonDetalle bonDetalle : handler.getBonDetalles()) {
			if (bonDetalle.getBonificacion().getConvenio().getId() == bon.getBonificacion().getConvenio().getId()) {
				if (Double.isNaN(bonDetalle.getMonto())) {
					bonificacionEstimada += 0.0;
				} else {
					bonificacionEstimada += bonDetalle.getMonto();
				}
			}
		}
		if (bon.getBonificacion().getConvenio().getMaxBonCredito() >= bonificacionEstimada) {
			return true;
		} else {
			error = "No se puede asociar la Bonificaci�n, supera el monto m�ximo por Cr�dito";
			return false;
		}
	}

	private boolean bonMaxPersona(Long idPersona, ObjetoiBonificacion bon) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Double bonificacionEstimada = (Double) bp.createSQLQuery(
				"SELECT SUM(bd.monto) " + "FROM Objetoi o " + "JOIN ObjetoiBonificacion ob ON ob.idCredito = o.id "
						+ "JOIN BonDetalle bd ON bd.objetoiBonificacion_id = ob.id "
						+ "WHERE o.persona_IDPERSONA = :persona_id")
				.setLong("persona_id", idPersona).uniqueResult();
		if (bonificacionEstimada == null)
			bonificacionEstimada = 0.0;
		if (bon.getBonificacion().getConvenio().getMaxBonPersona() >= bonificacionEstimada) {
			return true;
		} else
			error = "No se puede asociar la Bonificaci�n, supera el monto m�ximo por Persona";
		return false;
	}

	private boolean tasaNetaInferior(ObjetoiBonificacion bon) {
		CreditoHandler handler = new CreditoHandler();
		double[] arreglo = handler.calcularTasaNeta(bon.getObjetoi().getId());
		double tasaNeta = (arreglo.length == 0 ? 0 : arreglo[0]);
		if (tasaNeta >= bon.getBonificacion().getMinimoInteres()) {
			return true;
		} else {
			error = "No se puede asociar la Bonificaci�n, la tasa Neta es inferior a la permitida por el Convenio";
			return false;
		}
	}

	private boolean financiamientoMaxConvenio(ObjetoiBonificacion bon) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Objetoi> creditos = bp
				.createQuery(
						"Select distinct ob.objetoi from ObjetoiBonificacion ob where ob.bonificacion.convenio=:conv")
				.setParameter("conv", bon.getBonificacion().getConvenio()).list();
		Double totalFinanciamiento = 0.0;
		for (Objetoi cred : creditos) {
			totalFinanciamiento += cred.getFinanciamientoReal();
		}
		if (totalFinanciamiento <= bon.getBonificacion().getConvenio().getMaxFinanciamientoConvenio()) {
			return true;
		} else {
			error = "No se puede asociar la Bonificaci�n, supera el monto m�ximo de financiamiento del Convenio";
			return false;
		}
	}

	private boolean noVencido(ObjetoiBonificacion bon) {

		GregorianCalendar calendar1 = new GregorianCalendar();
		calendar1.setTime(new Date());

		calendar1.set(GregorianCalendar.MINUTE, 0);
		calendar1.set(GregorianCalendar.HOUR, 0);
		calendar1.set(GregorianCalendar.SECOND, 0);
		calendar1.set(GregorianCalendar.MILLISECOND, 0);
		calendar1.set(GregorianCalendar.HOUR_OF_DAY, 0);

		Date fecha1 = calendar1.getTime();
		Date fecha2 = bon.getBonificacion().getConvenio().getFechaVencimientoAplicacion();
		if (!fecha1.after(fecha2)) {
			return true;
		} else {
			error = "No se puede asociar la Bonificaci�n, el Convenio esta vencido";
			return false;
		}
	}

	private boolean financiamientoMaxCredito(Long id, ObjetoiBonificacion bon) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Objetoi cred = (Objetoi) bp.getById(Objetoi.class, id);
		if (bon.getBonificacion().getConvenio().getMaxFinanciamientoCredito() >= cred.getFinanciamientoReal()) {
			return true;
		} else {
			error = "No se puede asociar la Bonificaci�n, supera el monto m�ximo de financiamiento por Cr�dito permitido por el Convenio";
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private boolean financiamientoMaxPersona(Long idPersona, ObjetoiBonificacion bon) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Objetoi> creditos = bp
				.createSQLQuery("SELECT * " + "FROM Objetoi o " + "WHERE o.persona_IDPERSONA = :id_persona")
				.addEntity(Objetoi.class).setLong("id_persona", idPersona).list();
		double financiamiento = 0.0;
		for (Objetoi credito : creditos) {
			financiamiento += credito.getFinanciamientoReal();
		}
		Double maxFinanciamientoPersona = bon.getBonificacion().getConvenio().getMaxFinanciamientoPersona();
		if (maxFinanciamientoPersona == null)
			maxFinanciamientoPersona = 0.0;
		if (maxFinanciamientoPersona >= financiamiento) {
			return true;
		} else {
			error = "No se puede asociar la Bonificaci�n, supera el monto m�ximo de financiamiento por Persona permitido por el Convenio";
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private double calcularBonificacion(BusinessPersistance bp, Cuota cuota) {
		Calendar vencimientoCompensatorio = Calendar.getInstance();
		// buscar saldo capital de la cuota
		double saldoCapital = calcularSaldoCapital(cuota, bp);
		// buscar tasa compensatorio
		double tasaAmortizacion = 0;
		ObjetoiIndice objetoiIndice = findTasaCompensatorio(cuota.getCredito_id());
		if (objetoiIndice != null) {
			// tasa desde vencimiento anterior
			Date vtoAnterior = null;
			if (cuota.getNumero() > 1) {
				List<Cuota> cuotasAnteriores = bp.createQuery(
						"select c from Cuota c where c.credito.id = :idCredito and c.numero < :nroCuota order by c.numero desc")
						.setParameter("idCredito", cuota.getCredito_id()).setParameter("nroCuota", cuota.getNumero())
						.setMaxResults(1).list();
				if (!cuotasAnteriores.isEmpty()) {
					Cuota anterior = (Cuota) cuotasAnteriores.get(0); // la anterior
					vtoAnterior = anterior.getFechaVencimiento();
				}
			} else {
				try {
					List<Desembolso> desembolsos = (List<Desembolso>) bp
							.getNamedQuery("Desembolso.findByCreditoYNumero")
							.setParameter("idCredito", cuota.getCredito_id()).setParameter("numero", cuota.getNumero())
							.list();
					Desembolso desembolso = null;
					if (desembolsos.size() != 0) {
						desembolso = desembolsos.get(0);
					}
					if (desembolso == null) {
						log.error("Error al buscar desembolso del credito " + cuota.getCredito_id());
					} else {
						vtoAnterior = desembolso.getFechaReal() != null ? desembolso.getFechaReal()
								: desembolso.getFecha();
					}
				} catch (NoResultException e) {
					log.error("Error al buscar desembolso del credito " + cuota.getCredito_id());
				}
			}

			if (vtoAnterior == null) {
				// fallback, usar el vencimiento
				vencimientoCompensatorio.setTime(cuota.getFechaVencimiento());
			} else {
				vencimientoCompensatorio.setTime(vtoAnterior);
			}

			vencimientoCompensatorio.add(Calendar.DATE, -objetoiIndice.getDiasAntes()); // mover dias antes desde el
																						// vencimiento
			tasaAmortizacion = objetoiIndice.calcularTasaFinal(vencimientoCompensatorio.getTime()); // calcular al dia
																									// de vencimiento -
																									// dias antes
		}
		int diasPeriodo = 0;
		// buscar dias periodo
		if (cuota.getNumero() == 1) {
			// para la primera cuota se calcula con el primer desembolso
			List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
					.setLong("idCredito", cuota.getCredito_id()).setMaxResults(1).list();
			Desembolso primerDesembolso = null;
			if (!desembolsos.isEmpty()) {
				primerDesembolso = desembolsos.get(0);
			}
			if (primerDesembolso != null && primerDesembolso.getFechaReal() != null) {
				Date fechaDesembolso = primerDesembolso.getFechaReal();
				Calendar c = Calendar.getInstance();
				c.setTime(fechaDesembolso);
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				fechaDesembolso = c.getTime();
				diasPeriodo = DateHelper.getDiasEntreFechas(fechaDesembolso, cuota.getFechaVencimiento());
			}
		} else {
			// para el resto de las cuotas se calcula con la cuota anterior
			List<Cuota> cuotasAnteriores = bp.createQuery(
					"select c from Cuota c where c.credito.id = :idCredito and c.numero < :nroCuota order by c.numero desc")
					.setParameter("idCredito", cuota.getCredito_id()).setParameter("nroCuota", cuota.getNumero())
					.setMaxResults(1).list();
			if (!cuotasAnteriores.isEmpty()) {
				Cuota cuotaAnterior = (Cuota) cuotasAnteriores.get(0); // la anterior
				diasPeriodo = DateHelper.getDiasEntreFechas(cuotaAnterior.getFechaVencimiento(),
						cuota.getFechaVencimiento());
			}
		}
		int diasTransaccion = diasPeriodo; // por ahora es igual al diasPeriodo
		double compensatorioActualizado = calcularCompensatorio(saldoCapital, tasaAmortizacion, diasPeriodo,
				diasTransaccion);
		CreditoHandler ch = new CreditoHandler();
		double tasaBonificacion = ch.calcularTasaNeta(cuota.getCredito_id(), vencimientoCompensatorio.getTime(),
				cuota.getNumero());
		double compensatorioBonificado = calcularCompensatorio(saldoCapital, tasaBonificacion, diasPeriodo,
				diasTransaccion);
		return compensatorioActualizado - compensatorioBonificado;
	}

	private double calcularSaldoCapital(Cuota cuota, BusinessPersistance bp) {
		Double capitalDevengado = (Double) bp.getNamedQuery("Cuota.findCapitalDevengado")
				.setLong("idCredito", cuota.getCredito().getId()).setInteger("nroCuota", cuota.getNumero())
				.uniqueResult();
		Double totalDesembolsado = (Double) bp.getNamedQuery("Cuota.findTotalDesembolsado")
				.setLong("idCuota", cuota.getId()).uniqueResult();
		return ((totalDesembolsado != null) ? totalDesembolsado : 0)
				- ((capitalDevengado != null) ? capitalDevengado : 0);
	}

	public ObjetoiIndice findTasa(String codigo, Long objetoiId) {
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			return (ObjetoiIndice) bp
					.createQuery(
							"select i from ObjetoiIndice i where i.credito.id = :idObjetoi and i.tipoTasa = :codigo")
					.setString("codigo", codigo).setLong("idObjetoi", objetoiId).uniqueResult();
		} catch (NonUniqueResultException nue) {
			return null;
		} catch (NoResultException nre) {
			return null;
		}
	}

	public ObjetoiIndice findTasaCompensatorio(Long objetoiId) {
		return findTasa("1", objetoiId);
	}

	public static Double calcularCompensatorio(double saldoCapital, double tasaAmortizacion, int diasPeriodo,
			int diasTransaccion) {
		if (diasPeriodo == 0) {
			diasPeriodo = 1;
		}
		Double compensatorio = Math.round(saldoCapital
				* Math.pow((1 + (tasaAmortizacion / 100) * diasPeriodo / 365), (diasTransaccion / diasPeriodo)) * 100D)
				/ 100D;
		compensatorio = Math.round((compensatorio - saldoCapital) * 100D) / 100D;
		return Math.max(0D, compensatorio);
	}

	/*
	 * @SuppressWarnings("unchecked") public double calcularTasaNeta(Date fecha,Long
	 * objetoiId) {
	 * 
	 * ObjetoiIndice compensatorio = findTasaCompensatorio(objetoiId); if
	 * (compensatorio != null) { double tasaFinal =
	 * compensatorio.calcularTasaFinal();
	 * 
	 * BusinessPersistance bp =
	 * SessionHandler.getCurrentSessionHandler().getBusinessPersistance(); Query
	 * query = bp.
	 * createQuery("select b from ObjetoiBonificacion b where b.idCredito = :idCredito and b.fechaCaducidad > :fecha and b.tipoBonificacion = :tipo"
	 * );
	 * 
	 * double tasaNeta = tasaFinal;
	 * 
	 * // buscar bonificaciones de porcentual fijo List<ObjetoiBonificacion>
	 * bonificacionesFijo = query .setLong("idCredito", objetoiId).setDate("fecha",
	 * fecha).setString("tipo", "3") .list(); if (!bonificacionesFijo.isEmpty()) {
	 * // si hay bonificacion para mantener la tasa fija, se calcula la tasa neta
	 * con esta bonificacion ObjetoiBonificacion bonificacionFijo =
	 * bonificacionesFijo.get(0); tasaNeta = Math.min(tasaNeta,
	 * bonificacionFijo.getValor()); } else { // si no, se buscan los otros tipos de
	 * bonificaciones y se aplican
	 * 
	 * // buscar bonificaciones de tasa bonificada List<ObjetoiBonificacion>
	 * bonificacionesTasa = query .setLong("idCredito", objetoiId).setDate("fecha",
	 * fecha).setString("tipo", "1") .list(); // buscar bonificaciones de porcentual
	 * de tasa List<ObjetoiBonificacion> bonificacionesPorcentual = query
	 * .setLong("idCredito", objetoiId).setDate("fecha", fecha).setString("tipo",
	 * "2") .list();
	 * 
	 * for (ObjetoiBonificacion bonificacion : bonificacionesTasa) { tasaNeta =
	 * tasaNeta - bonificacion.getValor(); }
	 * 
	 * for (ObjetoiBonificacion bonificacion : bonificacionesPorcentual) { tasaNeta
	 * = tasaNeta - (tasaFinal * bonificacion.getValor() / 100); } }
	 * 
	 * return tasaNeta; } return 0; }
	 */
}
