package com.asf.cred.struts.action;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.SQLGrammarException;

import com.asf.cred.struts.form.FuncionarioResponsableForm;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.AbmAction;
import com.asf.struts.form.ListForm;
import com.nirven.creditos.hibernate.FuncionarioResponsable;

public class FuncionarioResponsableAction extends AbmAction {

	@Override
	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		FuncionarioResponsableForm oForm = (FuncionarioResponsableForm) form;

		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		FuncionarioResponsable nuevo = (FuncionarioResponsable) oForm.getEntity();

		// if(nuevo.getComportamiento().length() == 0){
		// nuevo.setComportamiento("NULL");
		// }

		try {
			FuncionarioResponsable anterior = (FuncionarioResponsable) oBp
					.createQuery(
							"FROM FuncionarioResponsable f WHERE (f.comportamiento IS NULL OR f.comportamiento = :comportamiento) AND f.estado.id = :estado AND f.fechaHasta IS NULL")
					.setParameter("comportamiento", nuevo.getComportamiento())
					.setParameter("estado", nuevo.getIdEstado()).uniqueResult();
			if (anterior != null && !anterior.equals(nuevo) && !isCancelled(request)) {
				Calendar fechaHasta = Calendar.getInstance();
				fechaHasta.setTime(nuevo.getFechaDesde());
				fechaHasta.add(Calendar.DATE, -1);
				anterior.setFechaHasta(fechaHasta.getTime());
				oBp.saveOrUpdate(anterior);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!isCancelled(request)) {
			try {
				oBp.saveOrUpdate(nuevo);
			} catch (ConstraintViolationException e) {
				ActionMessages messages = new ActionMessages();
				messages.add("constraint.error", new ActionMessage("constraint.error"));
				saveErrors(request, messages);
				return mapping.findForward("errorPage");
			}
		}
		return list(mapping, oForm, request, response);
	}

	@Override
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		FuncionarioResponsableForm oAForm = null;
		ListForm oLForm = null;

		String strFilter = "";
		boolean bolFilter = false;

		if (form == null) {
			oLForm = new ListForm();
		} else {
			if (form instanceof FuncionarioResponsableForm) {
				oAForm = (FuncionarioResponsableForm) form;
				oLForm = new ListForm();
				oLForm.setEntityName(oAForm.getEntityName());
				bolFilter = oAForm.getFilter();
			} else {
				oLForm = (ListForm) form;
			}
		}

		if (oLForm.getEntityName().equals("")) {
			oLForm.setEntityName(request.getParameter("entityName"));
		}

		if (bolFilter) {
			strFilter = setFilter(request, oAForm);
		}

		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		java.util.List oList = new java.util.ArrayList();

		String query;

		if (bolFilter) {
			if (strFilter != null && !strFilter.equals("")) {
				try {
					oList = oBp.getByFilter("FROM " + oLForm.getEntityName() + " x " + strFilter
							+ " order by x.estado.id, x.comportamiento, x.fechaDesde asc");

				} catch (SQLGrammarException e) {
					request.getSession().setAttribute("paramName", null);
					request.getSession().setAttribute("paramComp", null);
					request.getSession().setAttribute("paramValue", null);

				}
			}
		} else {
			oList = oBp.createQuery(
					"FROM " + oLForm.getEntityName() + " x order by x.estado.id, x.comportamiento, x.fechaDesde asc")
					.list();
		}

		oLForm.setResult(oList);
		request.setAttribute("ListResult", oLForm);
		return mapping.findForward(oLForm.getEntityName() + "List");

	}
}
