package com.asf.cred.struts.action;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.GarantiaEstadoForm;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.AbmAction;
import com.asf.struts.form.AbmForm;
import com.asf.util.CastRequestHelper;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class GarantiaEstadoAction extends AbmAction {

	@Override
	public ActionForward newEntity(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		Long objetoiGarantia_Id = CastRequestHelper.toLong("objetoiGarantia", request);
		GarantiaEstadoForm garantiaEstadoForm = (GarantiaEstadoForm) form;
		GarantiaEstado garantiaEstado = new GarantiaEstado();
		ObjetoiGarantia objetoiGarantia = new ObjetoiGarantia();
		objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, objetoiGarantia_Id);

		String pHQL = "SELECT ge FROM GarantiaEstado ge WHERE ge.objetoiGarantia.id = :objetoiGarantia ORDER BY ge.fechaEstado DESC";
		List<GarantiaEstado> listGarantiaEstado = bp.createQuery(pHQL)
				.setParameter("objetoiGarantia", objetoiGarantia.getId()).list();
		if (!listGarantiaEstado.isEmpty()) {
			GarantiaEstado ultimoEstado = listGarantiaEstado.get(0);
			garantiaEstado.setImporte(ultimoEstado.getImporte());
		}		
		garantiaEstado.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		garantiaEstado.setObjetoiGarantia(objetoiGarantia);
		garantiaEstado.setId((long) 0);
		garantiaEstadoForm.setEntity(garantiaEstado);

		request.setAttribute("garantiaEstadoForm", garantiaEstadoForm);
		return mapping.findForward("GarantiaEstado");

	}

}
