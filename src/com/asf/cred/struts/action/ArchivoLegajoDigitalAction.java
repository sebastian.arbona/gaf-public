package com.asf.cred.struts.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.servlet.RestController;
import com.asf.cred.servlet.RestResponse;
import com.asf.security.BusinessPersistance;
import com.asf.util.CastRequestHelper;
import com.asf.util.DataTableHelper;
import com.nirven.creditos.hibernate.ArchivoLegajoDigital;
import com.nirven.creditos.hibernate.LegajoDigital;
import com.nirven.expedientes.persistencia.Archivo;

public class ArchivoLegajoDigitalAction extends RestController {

	ArchivoLegajoDigital archivo = new ArchivoLegajoDigital();

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void doGet(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		RestResponse rest = new RestResponse(response);

		String entityName = null;
		String filtro = null;
		String orden = null;
		String campos = null;
		String listaCampos[] = null;
		List<Object[]> datos = null;
		errors = new HashMap<String, String>();

		try {
			entityName = CastRequestHelper.toString("entidad", request);
			filtro = CastRequestHelper.toString("filtro", request);
			orden = CastRequestHelper.toString("orden", request);
			campos = CastRequestHelper.toString("campos", request);

			if (entityName == null || entityName.isEmpty()) {
				errors.put("entidad", "No se ha especificado el parametro: Entidad");
			} else if (campos == null || campos.isEmpty() || campos.equals("*")) {
				errors.put("campos", "No se ha especificado el parametro: Campos");
			}
			if (errors.isEmpty()) {

				listaCampos = campos.split(",");

				String consulta = "FROM " + entityName + " WHERE 1=1 ";
				com.asf.security.SessionHandler sh = SessionHandler.getCurrentSessionHandler();
				if (sh.isAllowed("/actions/process.do?do=process&processName=ArchivoLegajoDigitalVisualizar")) {
					// condicion si tiene permiso, que visualice todas
					consulta += "AND visualizar <> 5 ";
				} else {
					// Si no tiene permiso, solo las permitidas
					consulta += "AND visualizar = 1 ";
				}

				if (filtro != null && !"".equals(filtro) && !"null".equals(filtro)) {
					consulta += "AND " + filtro;
				}

				if (orden != null && !"".equals(orden) && !"null".equals(orden)) {
					consulta += " ORDER BY " + orden;
				}

				String rconsulta = consulta.replaceAll("legajoDigital =", "legajoDigital_id =");
				String ids = "-1";
				List<Object[]> resultSet = bp.createSQLQuery("SELECT * " + rconsulta).list();
				for (Object[] row : resultSet) {

					List archivo = bp.createSQLQuery("SELECT * FROM Archivo WHERE id = " + row[3].toString()).list();
					if (archivo.size() > 0) {
						ids += ", " + row[3];
					}
				}
				datos = bp.getByFilter("FROM ArchivoLegajoDigital WHERE archivo.id IN (" + ids + ")");
				rest.setData(datos, listaCampos);
			}
			rest.setErrors(errors).print();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doMoveOrder(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		getMainObject(request);
		int step = CastRequestHelper.toInteger("step", request);
		int newOrdenObjeto = archivo.getOrden() + step;
		if (newOrdenObjeto < 1)
			newOrdenObjeto = 1;
		String pHQL = "SELECT a FROM ArchivoLegajoDigital a WHERE a.legajoDigital.id = :legajoDigitalId AND a.tipo = :tipo ORDER BY a.orden ASC";
		@SuppressWarnings("unchecked")
		List<ArchivoLegajoDigital> archivos = (List<ArchivoLegajoDigital>) bp.createQuery(pHQL)
				.setParameter("legajoDigitalId", archivo.getLegajoDigital().getId())
				.setParameter("tipo", archivo.getTipo()).list();

		int nextOrden = 1;
		for (Iterator<ArchivoLegajoDigital> it = archivos.iterator(); it.hasNext();) {
			ArchivoLegajoDigital a = it.next();
			if (nextOrden == newOrdenObjeto)
				nextOrden++;
			if (a.getId() != archivo.getId()) {
				a.setOrden(nextOrden);
				bp.save(a);
				nextOrden++;
			}
		}
		if (newOrdenObjeto > nextOrden)
			newOrdenObjeto = nextOrden;

		archivo.setOrden(newOrdenObjeto);
		bp.save(archivo);
	}

	public void doSearchArchivo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String resultado = null;
		try {
			DataTableHelper dtable = new DataTableHelper();
			String[] columnas = { "a.id", "a.descripcion", "a.fecha", "a.nombre", "a.usuario_CAUSER_K", "p.NOMB_12",
					"p.CUIL_12", "p.NUDO_12" };
			String tabla_ppal = "Archivo a";
			String idArchivo_ppal = "a.id";
			String filtro = "";
			String join = "LEFT JOIN ArchivoJoin j ON a.id = j.archivoRepo_id LEFT JOIN Persona p ON p.IDPERSONA = J.idPersona";
			resultado = dtable.getDataTableResponse(request, tabla_ppal, idArchivo_ppal, columnas, filtro, join);

		} catch (Exception e) {
			e.printStackTrace();
		}
		response.getWriter().print(resultado);
		response.getWriter().flush();

	}

	@Override
	protected Object getMainObject(HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long id = CastRequestHelper.toLong("id", request);
		if (id == null) {
			archivo = new ArchivoLegajoDigital();
		} else {
			archivo = (ArchivoLegajoDigital) bp.getById(ArchivoLegajoDigital.class, id);
		}
		return archivo;
	}

	public void doPost(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		RestResponse restResponse = new RestResponse(response);

		Long idArchivo = CastRequestHelper.toLong("idArchivo", request);
		Long idLegajoDigital = CastRequestHelper.toLong("idLegajoDigital", request);
		int tipo = CastRequestHelper.toInteger("tipo", request);
		archivo = new ArchivoLegajoDigital();
		Archivo a = (Archivo) bp.getById(Archivo.class, idArchivo);
		LegajoDigital legajoDigital = (LegajoDigital) bp.getById(LegajoDigital.class, idLegajoDigital);

		archivo.setArchivo(a);
		archivo.setLegajoDigital(legajoDigital);
		archivo.setOrden(archivo.getNextOrderValue(idLegajoDigital, tipo));
		archivo.setDesde(CastRequestHelper.toDate("desde", request));
		archivo.setHasta(CastRequestHelper.toDate("hasta", request));
		archivo.setVisualizar(CastRequestHelper.toBool("visualizar", request));
		archivo.setTipo(tipo);

		bp.save(archivo);
		restResponse.setErrors(errors).print();
	}

}
