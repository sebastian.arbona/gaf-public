package com.asf.cred.struts.action;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.servlet.RestController;
import com.asf.security.BusinessPersistance;
import com.asf.util.CastRequestHelper;
import com.nirven.creditos.hibernate.LegajoDigital;
import com.nirven.creditos.hibernate.ObservacionLegajoDigital;

public class ObservacionLegajoDigitalAction extends RestController {
	
	private ObservacionLegajoDigital observacion;
	private LegajoDigital legajoDigital;

	@Override
	protected boolean validate(HttpServletRequest request) {
		observacion		= (ObservacionLegajoDigital) super.getObjectByRequestId(ObservacionLegajoDigital.class, "id", request);
		legajoDigital 	= (LegajoDigital) super.getObjectByRequestId(LegajoDigital.class, "legajoDigital.id", request);
		String valTexto = CastRequestHelper.toString("texto", request);		
		if(legajoDigital == null) {
			super.errors.put("legajoDigital.id", "No se ha seleccionado el Legajo Digital. Debe iniciar su sesi&oacute;n nuevamente.");
		}
		if(valTexto == null || "".equals(valTexto)) {
			super.errors.put("texto", "El contenido de la observaci&oacute;n es obligatorio.");
		}
		return super.errors.isEmpty();
	}

	@Override
	protected void save(HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		getMainObject(request);
		observacion.setTipo				(CastRequestHelper.toInteger("tipo", request));
		observacion.setCauserK			(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		observacion.setFecha			(new Date());
		observacion.setTexto			(CastRequestHelper.toString("texto", request));
		observacion.setLegajoDigital	(legajoDigital);
		observacion.setMostrar			(CastRequestHelper.toBool("mostrar", request));

		bp.save(observacion);
	}

	@Override
	protected Object getMainObject(HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long id = CastRequestHelper.toLong("id", request);
		if(id == null) {
			observacion = new ObservacionLegajoDigital();
		}else {
			observacion = (ObservacionLegajoDigital) bp.getById(ObservacionLegajoDigital.class, id);
		}
		return observacion;
	}
}
