package com.asf.cred.struts.action;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.ObjectNotFoundException;

import com.asf.cred.business.CierreCaja;
import com.asf.cred.business.GenerarPagos;
import com.asf.cred.business.RegistraCobropago;
import com.asf.cred.business.StrutsArray;
import com.asf.cred.struts.form.AbmFormCaratula;
import com.asf.gaf.hibernate.Mediopago;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.AbmAction;
import com.asf.util.DirectorHelper;
import com.nirven.creditos.hibernate.Bolepago;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Movpagos;
import com.nirven.creditos.hibernate.Pagos;

/**
 *
 * @author lfonolla
 */
public class AbmActionCaratula extends AbmAction {

	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AbmFormCaratula caratulaForm = (AbmFormCaratula) form;
		ActionErrors errores = new ActionErrors();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Caratula caratula = (Caratula) caratulaForm.getEntity();
		String[] list = caratulaForm.getLista(); // lista de boletos.-
		boolean esModificacion = caratula.getId() != null && caratula.getId().longValue() != 0; // true; si es una
																								// modificaci�n.-
		String pagosActuales = ""; // los pagos actuales de una modificaci�n.-
		String[] listB = new String[list.length]; // lista de boletos no pagados (cuando es modificaci�n).

		NumberFormat n = NumberFormat.getInstance();
		n.setMaximumFractionDigits(2);
		n.setMinimumFractionDigits(2);
		n.setGroupingUsed(false);

		if (!isCancelled(request)) {
			try {

				Double total = new Double(0);
				List boletos = new ArrayList();

				for (int i = 0; i < list.length; i++) {
					String[] lista = list[i].split("-");
					try {
						Boleto boleto = (Boleto) bp.createQuery(
								"Select b from Boleto b where b.numeroBoleto=:num and b.verificadorBoleto = :ver and b.periodoBoleto=:per")
								.setParameter("num", new Long(lista[0])).setParameter("ver", new Long(lista[1]))
								.setParameter("per", new Long(lista[2].substring(0, 4))).uniqueResult();

						if (boleto.getFechaVencimiento() != null) {
							// si NO es un libre de deuda
							total += boleto.getImporte().doubleValue();
						} else {
							// total+=boleto.getImp109().doubleValue();
							total += new Double(lista[2].substring(6, lista[2].length()).replace(",", "."));
						}

						// if(esModificacion){
						List lstPagos = bp.getByFilter("FROM Pagos p WHERE p.id.idcaratula=" + caratula.getId()
								+ " AND p.id.boleto.id=" + boleto.getId());
						if (lstPagos.size() > 0)
							continue;
						else
							listB[i] = list[i];

						// }

						boletos.add(boleto);

					} catch (ObjectNotFoundException e) {
					}
				}
				if (caratula.getComprobantes() != null) {
					if (caratula.getComprobantes().longValue() != list.length) {
						errores.add("caratula.totalcomp", new ActionMessage("caratula.totalcomp"));
					}
				}
				if (caratula.getImporte() != null) {
					if (caratula.getImporte().doubleValue() != new Double(n.format(Math.abs(total)).replace(",", "."))
							.doubleValue()) {
						errores.add("caratula.total", new ActionMessage("caratula.total"));
					}
				}
				if (!errores.isEmpty()) {
					saveErrors(request, errores);
					return mapping.findForward("errorPage");
				} else {
					resetBoletos(null, null, null, null);
				}
				Long ultimoGrupo;
				try {
					ultimoGrupo = (Long) bp
							.createQuery("SELECT MAX(c.grupo) FROM Caratula c WHERE c.fechaEnvio = :fecha")
							.setDate("fecha", caratula.getFechaEnvio()).uniqueResult();
				} catch (NoResultException e) {
					ultimoGrupo = 0L;
				}
				if (ultimoGrupo == null)
					ultimoGrupo = 0L;
				caratula.setGrupo(++ultimoGrupo);
				caratula.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());

				bp.saveOrUpdate(caratula);

				for (Iterator iter = boletos.iterator(); iter.hasNext();) {
					Boleto boleto = (Boleto) iter.next();

					/* Boletos Anuales o Semestrales */

					// Tipo Boleto(Tipificador 'TPO_VTO'): 0=Mensual, 1=Semestral, 2=Anual,
					// 3=Anticipo.
					/*
					 * if(boleto.getTipo()!=null && !boleto.getTipo().equals(0L) &&
					 * !boleto.getTipo().equals(3L)){ String codi = ""; String sql = "";
					 * 
					 * // Consulta que el boleto MENSUAL corresp. al bimestre 1(Enero) o 7(Julio) no
					 * est� pagado... List lp = bp.getByFilter("FROM Pagos p ");
					 * 
					 * if(lp.size()>0){ for (int i = 0; i < lp.size(); i++) { Pagos p =
					 * (Pagos)lp.get(i); codi += i < lp.size() - 1 ? p.getId().getCodi09() + "," :
					 * p.getId().getCodi09(); } }
					 * 
					 * sql = "FROM Boleto b WHERE b.idoi=" + boleto.getIdoi() +
					 * " AND b.bime09 IN (1,7) AND b.tipo=0"; sql += !codi.equals("") ?
					 * " AND b.id.codi09 NOT IN (" + codi + ")" : "" ;
					 * 
					 * List lb = bp.getByFilter(sql);
					 * 
					 * //Si el boleto Mensual no est� pagado.. if(lb.size() > 0){ Boleto b =
					 * (Boleto) lb.get(0); //Crea el pago del boleto... GenerarPagos generarPagos =
					 * new GenerarPagos(); generarPagos.generar(b, caratula); pagosActuales += ",'"
					 * + generarPagos.getPagos().getId().getIdcaratula() + "-" +
					 * generarPagos.getPagos().getId().getCodi09() + "-" +
					 * generarPagos.getPagos().getId().getDigi09() + "-" +
					 * generarPagos.getPagos().getId().getPeri09() + "'"; }
					 * 
					 * }
					 */

					GenerarPagos generarPagos = new GenerarPagos();
					generarPagos.generar(boleto, caratula, boleto.getImporte());
					pagosActuales += ",'" + generarPagos.getPagos().getId().getCaratula() + "-"
							+ generarPagos.getPagos().getNumeroBoleto() + "-"
							+ generarPagos.getPagos().getVerificadorBoleto() + "-"
							+ generarPagos.getPagos().getPeriodoBoleto() + "'";

					// Registra el pago del boleto en Cobropago, Bolepago y Detallebolepago..
					RegistraCobropago rC = new RegistraCobropago(bp);
					StrutsArray cobroPagos = new StrutsArray(Cobropago.class);
					Cobropago c = new Cobropago();
					c.setMediopago(
							(Mediopago) bp.getById(Mediopago.class, new Long(DirectorHelper.getString("EFECTIVO"))));
					c.setCaratula(caratula);
					c.setImporte(new Double(generarPagos.getPagos().getImporte()));
					cobroPagos.add(c);
					rC.registraCobro(listB, cobroPagos);
				}

				// Si es una modificaci�n...
				if (esModificacion)
					borraBoleto(list, caratula.getId());

				bp.commit();
			} catch (Exception e) {
				bp.rollback();
				e.printStackTrace();
				return mapping.findForward("errorPage");
			}
		}

		if (caratula.isCobro()) {
			response.sendRedirect(
					"process.do?do=process&processName=CobroPagos&process.fenv12Str=" + caratula.getFechaEnvioStr());
		}

		return list(mapping, form, request, response);
	}

	public ActionForward load(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		resetBoletos(mapping, form, request, response);
		// atributos
		ActionForward actionForward = super.load(mapping, form, request, response);
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List boletos = null; // lista de boletos para java.-
		String[] list = new String[] {}; // lista de boletos para la p�gina.-
		AbmFormCaratula caratula = (AbmFormCaratula) form; // AbmFormCaratula; para agregar el list.-
		Caratula caratulaForm = (Caratula) caratula.getEntity(); // Caratula, para obtener el idcaratula.-

		try {
			// traemos todos los pagos asociados a la car�tula.-
			// boletos = bp.getByFilter("FROM Pagos p WHERE p.Caratula.id = " +
			// caratulaForm.getId().longValue() );
			boletos = bp
					.createSQLQuery("select b.* FROM " + BusinessPersistance.getSchema() + ".Boleto b INNER JOIN "
							+ BusinessPersistance.getSchema() + ".Pagos p ON b.id = p.boleto_id "
							+ "WHERE p.caratula_id = " + caratulaForm.getId().longValue())
					.addEntity(Boleto.class).list();
		} catch (Exception e) {
			ActionMessages messages = new ActionMessages();
			messages.add("pagos.load", new ActionMessage("pagos.load"));
			saveErrors(request, messages);
			return mapping.findForward("errorPage");
		}
		if (boletos != null && boletos.size() != 0) {
			list = new String[boletos.size()];
			int i = 0;
			Date fecha = caratulaForm.getFechaCobranza();
			// por cada pago; agregamos un boleto a la lista con el formato:
			// codigo-digito-periodo.-
			for (Iterator iter = boletos.iterator(); iter.hasNext(); i++) {
				Boleto oBol = (Boleto) iter.next();
				// Double impo = new Double(oBol.getVto209() != null &&
				// (fecha.after(oBol.getVto209())) ? oBol.getImp309() : (oBol.getVto209() !=
				// null && fecha.before(oBol.getVto209()) ? oBol.getImp209() :
				// oBol.getImp109()));
				Double impo = new Double(oBol.getImporte());

				list[i] = "" + oBol.getNumeroBoleto().longValue() + "-" + oBol.getVerificadorBoleto().longValue() + "-"
						+ oBol.getPeriodoBoleto().longValue() + "-"
						+ new DecimalFormat("0.00").format(impo.doubleValue()).toString();

			}
		}
		caratula.setLista(list); // cargamos la lista al form.-
		return actionForward;
	}

	public ActionForward newEntity(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		resetBoletos(null, null, null, null);
		return super.newEntity(mapping, form, request, response);
	}

	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		AbmFormCaratula caratula = (AbmFormCaratula) form;
		Caratula caratulaForm = (Caratula) caratula.getEntity();
		List pagos = null;
		List cobros = null;

		try {

			// Traemos todos los pagos asociados a la car�tula..
			pagos = bp
					.createSQLQuery("SELECT m.* FROM " + BusinessPersistance.getSchema() + ".Pagos m INNER JOIN "
							+ BusinessPersistance.getSchema() + ".Boleto b ON b.id = m.boleto_id "
							+ "WHERE m.caratula_id = " + caratulaForm.getId().longValue())
					.addEntity(Pagos.class).list();
			cobros = bp
					.createSQLQuery("SELECT b.* FROM " + BusinessPersistance.getSchema() + ".Bolepago b INNER JOIN "
							+ BusinessPersistance.getSchema() + ".Cobropago c ON c.id = b.cobropago_id "
							+ " WHERE c.caratula_id = " + caratulaForm.getId().longValue())
					.addEntity(Bolepago.class).list();
		} catch (Exception e) {
			ActionMessages messages = new ActionMessages();
			messages.add("pagos.load", new ActionMessage("pagos.load"));
			saveErrors(request, messages);
			return mapping.findForward("errorPage");
		}

		// Borra los cobros asociados a la car�tula..
		for (Iterator iter = cobros.iterator(); iter.hasNext();) {
			Bolepago bolepago = (Bolepago) iter.next();
			Cobropago cobropago = bolepago.getId().getCobropago();
			cobropago.setImporte(cobropago.getImporte().doubleValue() - bolepago.getImporte().doubleValue());
			bp.saveOrUpdate(cobropago);
			bp.delete(bolepago);
			if (cobropago.getImporte().doubleValue() <= 0D) {
				bp.delete(cobropago);
			}

		}
		// Borra los MovPagos asociados a los pagos de la caratula
		for (Iterator iter = pagos.iterator(); iter.hasNext();) {
			Pagos p = (Pagos) iter.next();
			List<Movpagos> movPagos = bp.createQuery("Select m from Movpagos m where m.pagos=:pago")
					.setParameter("pago", p).list();
			for (Movpagos mov : movPagos) {
				bp.delete(mov);
			}
		}

		// Borra los pagos asociados a la car�tula..
		for (Iterator iter = pagos.iterator(); iter.hasNext();) {
			Pagos p = (Pagos) iter.next();
			bp.delete(p);
		}

		// Borra las ctasctes de la caratula
		List<Ctacte> ctas = bp.createQuery("Select cta from Ctacte cta where cta.caratula=:caratula")
				.setParameter("caratula", caratulaForm).list();
		for (Ctacte ctacte : ctas) {
			bp.delete(ctacte);
		}

		super.delete(mapping, form, request, response);
		response.sendRedirect(
				"process.do?do=process&processName=CobroPagos&process.fenv12Str=" + caratulaForm.getFechaEnvioStr());
		return list(mapping, form, request, response);

	}

	public void resetBoletos(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute(AbmFormCaratula.PAGOS_IMPORTADOS, null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.removeAttribute(AbmFormCaratula.PAGOS_IMPORTADOS);
	}

	public ActionForward cerrarCaja(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		/*
		 * processName=CierreCaja&process.idcaratula=${reportTable.idcaratula}&process.
		 * fenv12Str=${reportTable.fenv12Str}&
		 * entityName=${param.entityName}&filter=true
		 */
		AbmFormCaratula caratulaForm = (AbmFormCaratula) form;
		Caratula caratula = (Caratula) caratulaForm.getEntity();

		CierreCaja cierre = new CierreCaja();
		cierre.setIdcaratula(caratula.getId());
		cierre.setFenv12(caratula.getFechaEnvio());

		if (!cierre.doProcess()) {
			ActionMessages oMessages = new ActionMessages();
			for (Object oKey : cierre.getErrors().keySet()) {
				String sError = (String) cierre.getErrors().get(oKey);
				oMessages.add(oKey.toString(), new ActionMessage(sError));
			}
			saveErrors(request, oMessages);
		}

		return super.list(mapping, form, request, response);
	}
	/*
	 * private class CrearCtaCte{
	 * 
	 * public static final int MENSUAL = 1; public static final int SEMESTRAL = 6;
	 * public static final int ANUAL = 12; private Boleto boleto = null; private
	 * BusinessPersistance bp = null;
	 * 
	 * public CrearCtaCte(Boleto boleto){ this.boleto = boleto; this.bp =
	 * SessionHandler.getCurrentSessionHandler().getBusinessPersistance(); }
	 * 
	 * public int getTipoBolcon(Boleto boleto){ return MENSUAL; }
	 */
	/**
	 * Crea la ctacte a partir de bolcones.
	 */
	/*
	 * public void crearCtaCte_Bolcon(){ List lBolcones = getBolcones(boleto);
	 * crearCtaCte(lBolcones, true); crearCtaCte(lBolcones, false); }
	 */

	/**
	 * Crea la ctacte a partir de una lista de Bolcones.
	 *
	 * @param bDebitos El tipo de movimiento a crear, true -> debitos, false ->
	 *                 cr�ditos.
	 */
	/*
	 * private void crearCtaCte(List lBolcones, boolean bDebitos){
	 * 
	 * try{ for(Iterator it = lBolcones.iterator(); it.hasNext();){ Bolcon bolcon =
	 * (Bolcon) it.next(); MovimientoManual movimientoManual = new
	 * MovimientoManual(); Ctacte ctacte = null;
	 * 
	 * movimientoManual.setConfirma(new Long(1));
	 * movimientoManual.setGenerarBoleto(false); movimientoManual.setEmite(new
	 * Long(0));
	 * 
	 * movimientoManual.setIdoi(bolcon.getIdoi());
	 * movimientoManual.setPeriodoCtacte(bolcon.getId().getPeriodoCtacte());
	 * movimientoManual.setOfic99(bolcon.getOfic99());
	 * movimientoManual.setCodi11(bolcon.getCodi11());
	 * movimientoManual.setFech10(boleto.getFemi09());
	 * 
	 * movimientoManual.setImpo10(Double.valueOf(bolcon.getImpo16().toString()));
	 * 
	 * movimientoManual.setExpe10(null); movimientoManual.setDbcr(bDebitos ? new
	 * Long(1) : new Long(2)); movimientoManual.setBime10(bolcon.getBime10());
	 * movimientoManual.setConc(boleto.getConcem() != null ?
	 * boleto.getConcem().getIdconcem().toString() : "");
	 * 
	 * movimientoManual.doProcess();
	 * 
	 * ctacte = movimientoManual.getCtacte();
	 * 
	 * if(bDebitos){
	 * 
	 * bp.getCurrentSession().evict(bolcon);
	 * 
	 * borrarBolcon(bolcon);
	 * 
	 * bolcon.getId().setItemCtacte(ctacte.getId().getItemCtacte());
	 * bolcon.getId().setVerificadorCtacte(ctacte.getId().getVerificadorCtacte());
	 * bolcon.getId().setMovimientoCtaCte(ctacte.getId().getMovimientoCtaCte());
	 * 
	 * bp.saveOrUpdate(bolcon); }
	 * 
	 * ctacte.setBoleto(boleto);
	 * 
	 * bp.saveOrUpdate(ctacte); } }catch(Exception e){ e.printStackTrace();
	 * bp.rollback(); } }
	 */
	/**
	 * Borra el bolcon.
	 *
	 * @param Bolcon El bolcon que se quiere eliminar.
	 */

	/*
	 * private void borrarBolcon(Bolcon bolcon){ BolconKey bKey = new BolconKey();
	 * 
	 * bKey.setNumeroBoleto(bolcon.getId().getNumeroBoleto());
	 * bKey.setItemCtacte(bolcon.getId().getItemCtacte());
	 * bKey.setVerificadorBoleto(bolcon.getId().getVerificadorBoleto());
	 * bKey.setVerificadorCtacte(bolcon.getId().getVerificadorCtacte());
	 * bKey.setMovimientoCtacte(bolcon.getId().getMovimientoCtacte());
	 * bKey.setPeriodoBoleto(bolcon.getId().getPeriodoBoleto());
	 * bKey.setPeriodoCtacte(bolcon.getId().getPeriodoCtacte());
	 * 
	 * bolcon.setId(bKey);
	 * 
	 * bp.delete(bolcon); }
	 */
	/**
	 * Obtiene los bolcones de un boleto.
	 *
	 * @param Boleto el Boleto el cual se usar� para obtener sus bolcones.
	 *
	 * @return Una Lista con los bolcones.
	 */
	/*
	 * private List getBolcones(Boleto boleto){ return
	 * bp.getByFilter("FROM Bolcon b WHERE b.id.peri09 = " +
	 * boleto.getId().getPeriodoBoleto() + " AND b.id.codi09 = " + boleto.getId().
	 * getNumeroBoleto() + " AND b.id.digi09 = " +
	 * boleto.getId().getVerificadorBoleto() + " ORDER BY b.bime10 "); } }
	 */

	private void borraBoleto(String[] list, Long idcaratula) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String lst = "";
		// String[] lst2 = new String[list.length];

		for (int i = 0; i < list.length; i++) {
			String[] lstBoletos = list[i].split(" ");
			lst += (i > 0 ? "," : "") + "'" + lstBoletos[0] + "'";
			// lst2[i] = lstBoletos[1].substring(1, lstBoletos[1].length()).replace(",",
			// ".");
		}
		// borro Pagos (y Movpagos) que se quitaron de la lista en la modificaci�n..
		String sql = !lst.equals("")
				? " AND p.id.numeroBoleto ||'-'||p.id.verificadorBoleto||'-'||p.id.periodoBoleto NOT IN (" + lst + ")"
				: "";
		List borrarP = bp.getByFilter("FROM Pagos p WHERE p.id.idcaratula=" + idcaratula + sql);
		for (Iterator iter = borrarP.iterator(); iter.hasNext();) {
			bp.delete(iter.next());
		}

		// borro Cobropago (Consulta de lotes)..
		List borrarC = bp.getByFilter("FROM Bolepago p WHERE p.Cobropago.idcaratula=" + idcaratula + sql);
		// List borrarC = bp.getByFilter("FROM Cobropago p WHERE p.idcaratula="+
		// idcaratula + sql );
		for (int i = 0; i < borrarC.size(); i++) {
			Bolepago b = (Bolepago) borrarC.get(i);
			Cobropago c = b.getId().getCobropago();
			c.setImporte(c.getImporte().doubleValue() - b.getImporte().doubleValue());
			bp.saveOrUpdate(c);
			bp.delete(b);
			if (c.getImporte().doubleValue() <= 0D)
				bp.delete(c);

		}

	}
}