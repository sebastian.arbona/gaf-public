package com.asf.cred.struts.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

public class Anexo28Action extends DispatchAction {

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map<String, Object> conversation = (HashMap<String, Object>) request.getSession()
				.getAttribute(request.getParameter("cid"));

		String textResult = (String) conversation.get("textResult");
		byte[] bytes = textResult.getBytes("utf-8");

		response.setContentType("text/plain; charset=UTF-8");
		response.setContentLength(bytes.length);
		response.setHeader("Content-Disposition", "attachment; filename=\"StoCreEvoAcuFinAPFTC.txt\"");
		response.getOutputStream().write(bytes);
		response.getOutputStream().flush();
		response.getOutputStream().close();

		return null;
	}

}
