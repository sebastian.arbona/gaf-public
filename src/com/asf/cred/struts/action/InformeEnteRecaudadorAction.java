package com.asf.cred.struts.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

import com.asf.cred.StringHelper;
import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.cred.business.ProgressStatus;
import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.InformeEnteRecaudadorForm;
import com.asf.gaf.DirectorHandler;
import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Persona;
import com.civitas.hibernate.persona.Tipodoc;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.DetalleInformeDeuda;
import com.nirven.creditos.hibernate.Emideta;
import com.nirven.creditos.hibernate.Emision;
import com.nirven.creditos.hibernate.InformeDeuda;
import com.nirven.creditos.hibernate.Linea;

public class InformeEnteRecaudadorAction extends DispatchAction {

	private List<Emision> emisiones = new ArrayList<Emision>();
	private List<Emision> eSeleccionadas = new ArrayList<Emision>();
	private String text;
	private int cant;
	private List<Emideta> emidetas = new ArrayList<Emideta>();

	@SuppressWarnings("unchecked")
	public ActionForward show(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		cargarFechas((InformeEnteRecaudadorForm) form);

		String cid = UUID.randomUUID().toString();
		((InformeEnteRecaudadorForm) form).setCid(cid);

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Linea> lineas = bp.createQuery("select l from Linea l order by l.nombre").list();
		((InformeEnteRecaudadorForm) form).setLineas(lineas);

		return mapping.findForward("GenerarInformeDeuda");
	}

	private void cargarFechas(InformeEnteRecaudadorForm form) {
		try {
			form.setUltimaActualizacion(
					DirectorHandler.getDirector("comportamientoPago.ultimaActualizacion").getValor());
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Caratula c = (Caratula) bp
				.createQuery("select c  from Caratula c where c.actualizada = 1 ORDER BY c.fechaEnvio DESC")
				.setMaxResults(1).uniqueResult();

		if (c != null) {
			form.setUltimaRecaudacion(c.getFechaEnvioStr());
			form.setUltimaCobranza(c.getFechaCobranzaStr());
		}
	}

	@SuppressWarnings("unchecked")
	public ActionForward generar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		eSeleccionadas.clear();
		text = "";
		cant = 0;
		emidetas.clear();
		double sumTotal = 0.0;
		HashSet<String> seleccionados = new HashSet<String>();

		Enumeration<String> params = request.getParameterNames();
		String param;
		String[] p;
		while (params.hasMoreElements()) {
			param = params.nextElement();
			if (param.startsWith("emision-")) {
				p = param.split("-");
				seleccionados.add(p[1]);
			}
		}

		emisiones = (List<Emision>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("EmisionesGeneradas.emision");

		if (emisiones != null) {
			for (Emision emisionSel : emisiones) {
				for (String id : seleccionados) {
					if (id.equalsIgnoreCase(emisionSel.getId().toString())) {
						eSeleccionadas.add(emisionSel);
					}
				}
			}
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		List<Emideta> e;
		for (Emision em : eSeleccionadas) {
			em = (Emision) bp.getById(Emision.class, em.getId());
			em.setFacturada(true);
			bp.update((Emision) em);
			bp.getCurrentSession().flush();
			e = bp.createQuery("SELECT e FROM Emideta e WHERE e.emision = :emision").setParameter("emision", em).list();
			if (!e.isEmpty()) {
				emidetas.addAll(e);
			}

		}

		bp.commit();

		if (emidetas != null) {
			for (Emideta emideta : emidetas) {
				if (emideta.getBoleto() != null && emideta.getBoleto().getImporte() != null)
					sumTotal += emideta.getBoleto().getImporte();
			}
		}
		text = "C";
		String d = "" + DirectorHandler.getDirector("credicoop.numeroEmpresa").getValor();

		// Id Empresa
		if (d.equals("")) {
			text = text + "00000000";
		} else {
			text = text + StringHelper.completarCeros(d, 8);
		}
		// Cant Registros
		int reg = eSeleccionadas.size();
		String c = StringHelper.completarCeros("" + reg, 6);

		text = text + c;

		// Monto Total
		String imp = convertirImporte(sumTotal, 11);
		text += imp;

		// Accion
		text = text + "DT";

		// Ultima Rendicion
		String i = DirectorHandler.getDirector("ultimaRendicion.numero").getValor();
		text = text + StringHelper.completarCeros(i, 8);

		if (emidetas != null) {

			for (Emideta emideta : emidetas) {

				text = text + "\r\n";
				cant++;
				// Tipo Registro
				text = text + "R";

				// Nro Registro
				String n = StringHelper.completarCeros("0" + cant, 8);
				text = text + n;

				// Id Empresa
				text = text + "00000033";

				// Nro Cliente
				Long cli = emideta.getCredito().getPersona_id();
				text = text + StringHelper.completarCeros(cli.toString(), 11);

				// Tipo Comprobante
				text = text + "CU";

				// Nro Comprobante
				Ctacte cta = buscarCtaCte(emideta);
				String nro = "";
				if (cta != null) {
					nro = cta.getBoleto().getPeriodoBoleto().toString() + cta.getBoleto().getNumeroBoleto().toString()
							+ cta.getBoleto().getVerificadorBoleto().toString();

					nro = StringHelper.completarCeros(nro, 16);
				} else {
					nro = StringHelper.completarCeros(nro, 16);
				}
				//
				text = text + nro;

				// Detalle
				text = text + StringHelper.completarConCaracter((emideta.getCredito().getPersona().getNomb12() + "-"
						+ emideta.getCredito().getLinea().getNombre()), 50, " ");

				// Monto del comprobante
				String importe = "00000000000";

				if (cta != null) {
					importe = convertirImporte(cta.getBoleto().getImporte(), 11);
				}

				text += importe;

				String moneda = "";
				// Moneda
				if (cta != null) {
					if (cta.getBoleto().getObjetoi().getLinea().getMoneda().getDenominacion().equals("Pesos")) {
						moneda = "001";
					} else if (cta.getBoleto().getObjetoi().getLinea().getMoneda().getDenominacion().equals("Dolar")) {
						moneda = "002";
					} else if (cta.getBoleto().getObjetoi().getLinea().getMoneda().getDenominacion().equals("Euro")) {
						moneda = "003";
					}
				} else {
					moneda = "001";
				}
				text = text + moneda;

				// Perfil
				text = text + "0000000000";
				// Monto vigencia 1
				text += importe;

				// Vto 1
				if (cta != null) {
					SimpleDateFormat vto = new SimpleDateFormat("ddMMyyyy");
					text = text + vto.format(cta.getBoleto().getFechaVencimiento());
				} else {
					text = text + "00000000";
				}
				text = text + "000000000000000000000000000000000000000000000000000000000000000000000000000000000000A";
			}
		}
		bp.begin();
		Director director = DirectorHandler.getDirector("ultimaRendicion.numero");

		int ult = new Integer(director.getValor());
		ult = ult + 1;

		director.setValor("" + ult);

		bp.saveOrUpdate(director);
		bp.commit();

		response.setContentType("text/plain");
		response.setContentLength(text.length());
		response.setHeader("Content-Disposition", "attachment;filename=informe.txt");
		response.getWriter().write(text);
		response.getWriter().flush();
		response.getWriter().close();

		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward generarClientes(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Generando TXT de Clientes";
		ProgressStatus.iProgress = 0;

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		InformeEnteRecaudadorForm informeForm = (InformeEnteRecaudadorForm) form;

		List<BigDecimal> ids = bp.createSQLQuery(
				"SELECT DISTINCT O.persona_idpersona FROM Objetoi O INNER JOIN Ctacte C ON C.objetoi_id = O.id "
						+ "INNER JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id AND fechaHasta IS NULL "
						+ "WHERE OE.estado_idEstado IN(6,7,8)")
				.list();

		if (ids.isEmpty())
			ProgressStatus.iTotal = 1;
		else
			ProgressStatus.iTotal = ids.size();

		char cabecera = 'C';
		int numeroEmpresa = Integer.parseInt(DirectorHandler.getDirector("credicoop.numeroEmpresa").getValor());
		int cantRegistros = ids.size();
		int ultimaRendicion = Integer.parseInt(DirectorHandler.getDirector("ultimaRendicion.numero").getValor());
		String accion = "DT";

		String cabeceraFormat = "%c%08d%06d%08d%s\r\n";
		String textCabecera = String.format(cabeceraFormat, cabecera, numeroEmpresa, cantRegistros, ultimaRendicion,
				accion);

		String format = "%c%08d1%010d%-50.50s%02d%011d%011d%-50.50s%-5.5s%-5.5s%-5.5s%-50.50s%-2.2s%-8.8s%-20.20s%-50.50s%-1.1s%-1.1s%-10.10s%08d%c\r\n";

		String text = "";
		int nroRegistro = 1;
		for (BigDecimal id : ids) {
			Persona p = (Persona) bp.getById(Persona.class, id.longValue());

			char tipoRegistro = 'R';
			long numeroCliente = p.getId();
			String nombre = p.getNomb12();
			if (nombre != null) {
				nombre = nombre.replace('\t', ' ');
			}

			int tipoDoc = convertirTipoDoc(p.getTipodoc());

			long nroDoc = p.getNudo12() != null ? p.getNudo12() : 0;
			long cuit = p.getCuil12() != null ? p.getCuil12() : 0;

			String calle = "";
			String numero = "";
			String piso = "";
			String depto = "";
			String localidad = "";
			String provincia = "";
			String codigoPostal = "";
			String telefono = "";
			String email = "";
			String recibeEmail = "";
			String agenteRetencion = "";
			String perfil = "";

			int empresa = 33;

			char accionRegistro = 'A';

			if (informeForm.getTipoRegistros() != null) {
				if (informeForm.getTipoRegistros().equalsIgnoreCase("A") || informeForm.getTipoRegistros().equals("B")
						|| informeForm.getTipoRegistros().equalsIgnoreCase("M")) {
					accionRegistro = informeForm.getTipoRegistros().toUpperCase().charAt(0);
				}
			}

			text += String.format(format, tipoRegistro, nroRegistro, numeroCliente, nombre, tipoDoc, nroDoc, cuit,
					calle, numero, piso, depto, localidad, provincia, codigoPostal, telefono, email, recibeEmail,
					agenteRetencion, perfil, empresa, accionRegistro);

			nroRegistro++;

			ProgressStatus.iProgress++;
		}

		bp.begin();
		Director director = DirectorHandler.getDirector("ultimaRendicion.numero");
		int ult = new Integer(director.getValor());
		ult = ult + 1;
		director.setValor("" + ult);
		bp.saveOrUpdate(director);
		bp.commit();

		ProgressStatus.onProcess = false;

		String body = textCabecera + text;

		String nombreArchivo = String.format("clientes_33_%1$tY%1$tm%1$td.txt", new Date());

		response.setContentType("text/plain");
		response.setContentLength(body.length());
		response.setHeader("Content-Disposition", "attachment;filename=" + nombreArchivo);
		response.getWriter().write(body);
		response.getWriter().flush();
		response.getWriter().close();

		return null;
	}

	private int convertirTipoDoc(Tipodoc t) {
		if (t.getTido47().equals("D.N.I.")) {
			return 1;
		} else if (t.getTido47().equals("C.I.")) {
			return 4;
		} else if (t.getTido47().equals("L.E.")) {
			return 3;
		} else if (t.getTido47().equals("L.C.")) {
			return 2;
		} else if (t.getTido47().equals("C.E.")) {
			return 11;
		}
		return 1;
	}

	@SuppressWarnings("unchecked")
	public ActionForward mostrarDeuda(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Generando TXT de Deuda";
		ProgressStatus.iProgress = 0;

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		InformeEnteRecaudadorForm informeForm = (InformeEnteRecaudadorForm) form;

		if (informeForm.getCid() != null && !informeForm.getCid().isEmpty()) {
			String fh = (String) request.getSession().getAttribute(informeForm.getCid() + "fechaHasta");
			String idl = (String) request.getSession().getAttribute(informeForm.getCid() + "idsLineas");
			if (fh != null && informeForm.getFechaHasta().equals(fh)
					&& (idl != null && informeForm.getIdsLineas().equals(idl))) {

				List<BeanDeuda> listado = (List<BeanDeuda>) request.getSession()
						.getAttribute(informeForm.getCid() + "listado");
				if (listado != null && !listado.isEmpty()) {
					informeForm.setListado(listado);
					String nombreArchivo = String.format("comprobantes_33_%1$tY%1$tm%1$td.xls", new Date());
					informeForm.setNombreArchivo(nombreArchivo);

					ProgressStatus.abort = true;
					ProgressStatus.onProcess = false;

					cargarFechas(informeForm);

					return mapping.findForward("GenerarInformeDeuda");
				}
			}
		}

		String sql = "SELECT O.id FROM Objetoi O INNER JOIN Ctacte C ON C.objetoi_id = O.id "
				+ "INNER JOIN Cuota CT ON CT.id = C.cuota_id "
				+ "INNER JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id AND fechaHasta IS NULL "
				+ "WHERE OE.estado_idEstado IN(6,7,8) AND CT.fechaVencimiento <= :fecha ";

		if (informeForm.getIdsLineas() != null && !informeForm.getIdsLineas().isEmpty()) {
			sql += " AND O.linea_id IN (" + informeForm.getIdsLineas() + ") ";
		}

		sql += "GROUP BY O.id HAVING SUM(CASE WHEN C.tipomov_id = 2 THEN C.importe ELSE -C.importe END) >= 0.01";

		List<BigDecimal> ids = bp.createSQLQuery(sql).setDate("fecha", DateHelper.getDate(informeForm.getFechaHasta()))
				.list();

		ProgressStatus.iTotal = ids.size();

		CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda();
		calculoDeuda.setHastaFecha(DateHelper.getDate(informeForm.getFechaHasta()));

		List<BeanDeuda> listado = new ArrayList<BeanDeuda>();

		for (BigDecimal id : ids) {
			calculoDeuda.setIdObjetoi(id.longValue());
			calculoDeuda.calcular();

			BeanDeuda bean = new BeanDeuda();

			bean.setCompensatorio(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_COMPENSATORIO)
					- calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_BONIFICACION));
			bean.setGastos(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_GASTOS));
			bean.setMoratorioPunitorio(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_MORATORIO)
					+ calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_PUNITORIO));
			bean.setCapital(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_CAPITAL));

			if (bean.getTotalFondo() >= 0.01) {
				Object[] cabecera = (Object[]) bp
						.createSQLQuery("SELECT O.numeroAtencion, P.nomb_12, P.cuil_12, P.IDPERSONA, E.nombreEstado, "
								+ "CASE WHEN E.nombreEstado = 'EJECUCION' OR E.nombreEstado = 'PRIMER DESEMBOLSO' "
								+ "OR E.nombreEstado = 'PENDIENTE SEGUNDO DESEMBOLSO' "
								+ "THEN TFOC.TF_DESCRIPCION ELSE 'N/A' END AS Comportamiento, L.nombre, MAX(C.fechaVencimiento) "
								+ "FROM Cuota C JOIN Objetoi O ON O.id = C.credito_id "
								+ "JOIN PERSONA P ON O.persona_IDPERSONA = P.IDPERSONA "
								+ "JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id AND fechaHasta IS NULL "
								+ "JOIN Estado E ON E.idEstado = OE.estado_idEstado "
								+ "LEFT JOIN ObjetoiComportamiento OC ON OC.objetoi_id = O.id AND OC.fechaHasta IS NULL "
								+ "LEFT JOIN Tipificadores TFOC ON TFOC.TF_CATEGORIA = 'comportamientoPago' AND TFOC.TF_CODIGO = OC.comportamientoPago "
								+ "JOIN Linea L ON L.id = O.linea_id "
								+ "WHERE O.id = :id AND C.fechaVencimiento <= :fecha "
								+ "GROUP BY O.numeroAtencion, P.nomb_12, P.cuil_12, P.IDPERSONA, E.nombreEstado, L.nombre, "
								+ "CASE WHEN E.nombreEstado = 'EJECUCION' OR E.nombreEstado = 'PRIMER DESEMBOLSO' "
								+ "OR E.nombreEstado = 'PENDIENTE SEGUNDO DESEMBOLSO' "
								+ "THEN TFOC.TF_DESCRIPCION ELSE 'N/A' END")
						.setLong("id", id.longValue()).setDate("fecha", DateHelper.getDate(informeForm.getFechaHasta()))
						.uniqueResult();

				if (cabecera != null) {
					bean.setNumeroAtencion(((Number) cabecera[0]).longValue());
					bean.setNomb12((String) cabecera[1]);
					bean.setCuil12(((Number) cabecera[2]).toString());
					bean.setIdPersona(((Number) cabecera[3]).longValue());
					bean.setEstado((String) cabecera[4]);
					bean.setComportamiento((String) cabecera[5]);
					bean.setLinea((String) cabecera[6]);
					bean.setFechaVencimiento((Date) cabecera[7]);
					bean.setFechaCalculo(DateHelper.getDate(informeForm.getFechaHasta()));

					listado.add(bean);
				}
			}

			bp.getCurrentSession().clear();

			ProgressStatus.iProgress++;
		}

		informeForm.setListado(listado);

		request.getSession().setAttribute(informeForm.getCid() + "listado", listado);
		request.getSession().setAttribute(informeForm.getCid() + "fechaHasta", informeForm.getFechaHasta());
		request.getSession().setAttribute(informeForm.getCid() + "idsLineas", informeForm.getIdsLineas());

		String nombreArchivo = String.format("comprobantes_33_%1$tY%1$tm%1$td.xls", new Date());
		informeForm.setNombreArchivo(nombreArchivo);

		ProgressStatus.onProcess = false;

		cargarFechas(informeForm);

		request.setAttribute("InformeEnteRecaudadorForm", form);

		return mapping.findForward("GenerarInformeDeuda");
	}

	public ActionForward generarDeudaTotal(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Generando TXT de Deuda";
		ProgressStatus.iProgress = 0;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		InformeEnteRecaudadorForm informeForm = (InformeEnteRecaudadorForm) form;

		String fv = informeForm.getFechaVigencia();
		if (fv == null) {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("informedeuda.fechavigencia", new ActionMessage("informedeuda.fechavigencia"));
			saveErrors(request, actionMessages);
			request.setAttribute("InformeEnteRecaudadorForm", form);
			return mapping.findForward("GenerarInformeDeuda");
		}

		SimpleDateFormat vto = new SimpleDateFormat("ddMMyyyy");
		Calendar hoy = Calendar.getInstance();
		hoy.set(Calendar.HOUR_OF_DAY, 0);
		hoy.set(Calendar.MINUTE, 0);
		hoy.set(Calendar.SECOND, 0);
		hoy.set(Calendar.MILLISECOND, 0);

		Date fechaVigencia = DateHelper.getDate(fv);
		if (fechaVigencia.before(hoy.getTime())) {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("informedeuda.fechavigencia.pasado",
					new ActionMessage("informedeuda.fechavigencia.pasado"));
			saveErrors(request, actionMessages);
			request.setAttribute("InformeEnteRecaudadorForm", form);
			return mapping.findForward("GenerarInformeDeuda");
		}

		Long idInforme = informeForm.getIdInforme();
		InformeDeuda informe = (InformeDeuda) bp.getById(InformeDeuda.class, idInforme);
		List<DetalleInformeDeuda> detalles = informe.getDetalles();

		double sumTotal = 0;
		for (DetalleInformeDeuda det : detalles) {
			double deuda = det.getDeudaVencida();
			deuda = (int) (deuda * 100 + 0.5) / 100.0;
			det.setDeudaVencida(deuda);
			sumTotal += deuda;
		}

		text = "C";
		String d = "" + DirectorHandler.getDirector("credicoop.numeroEmpresa").getValor();
		// Id Empresa
		if (d.equals("")) {
			text = text + "00000000";
		} else {
			text = text + StringHelper.completarCeros(d, 8);
		}
		// Cant Registros
		// int reg = credito_deuda.size();
		int reg = detalles.size();
		String c = StringHelper.completarCeros("" + reg, 6);
		text = text + c;
		// Monto Total
		String imp = convertirImporte(sumTotal, 11);
		text += imp;
		// Accion
		if (informeForm.getTipoInforme() != null
				&& (informeForm.getTipoInforme().equals("DT") || informeForm.getTipoInforme().equals("AC"))) {
			text += informeForm.getTipoInforme();
		} else {
			text += "DT";
		}
		// Ultima Rendicion
		String i = DirectorHandler.getDirector("ultimaRendicion.numero").getValor();
		text = text + StringHelper.completarCeros(i, 8);
		cant = 0;
		String n;
		Long cli;
		String nro;
		String importe;
		String moneda;

		sumTotal = 0;

		for (DetalleInformeDeuda det : detalles) {
			text = text + "\r\n";
			cant++;
			// Tipo Registro
			text = text + "R";
			// Nro Registro
			n = StringHelper.completarCeros("0" + cant, 8);
			text = text + n;
			// Id Empresa
			text = text + "00000033";
			// Nro Cliente
			cli = det.getCredito().getPersona_id();
			text = text + "1" + StringHelper.completarCeros(cli.toString(), 10);
			// Tipo Comprobante
			text = text + "CU";
			// Nro Comprobante, no hay número de comprobante
			nro = det.getCredito().getNumeroAtencion() != null ? det.getCredito().getNumeroAtencionStr() : "";
			nro = StringHelper.completarCeros(nro, 16);
			text = text + nro;
			// Detalle
			String nombre = StringHelper.completarConCaracter(
					((det.getCredito().getPersona().getNomb12()) + "-" + (det.getCredito().getLinea().getNombre())), 50,
					" ");
			if (nombre != null) {
				if (nombre.length() > 50) {
					nombre = nombre.substring(0, 50);
				}
				nombre = nombre.replace('\t', ' ');
			}
			text = text + nombre;
			sumTotal += det.getDeudaVencida();
			// Monto del comprobante
			importe = convertirImporte(det.getDeudaVencida(), 11);
			text += importe;
			// Moneda
			String monDenom = (String) det.getCredito().getLinea().getMoneda().getDenominacion();
			if (monDenom.equals("Peso Argentino")) {
				moneda = "001";
			} else if (monDenom.equals("Dolar")) {
				moneda = "002";
			} else if (monDenom.equals("Euro")) {
				moneda = "003";
			} else {
				moneda = "001";
			}
			text = text + moneda;
			// Perfil
			text = text + "          ";
			// Monto vigencia 1
			text += importe;
			// Vto 1
			text = text + vto.format(fechaVigencia);

			text = text
					+ "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
			if (informeForm.getTipoRegistros() != null) {
				if (informeForm.getTipoRegistros().equalsIgnoreCase("A") || informeForm.getTipoRegistros().equals("B")
						|| informeForm.getTipoRegistros().equalsIgnoreCase("M")) {
					text += informeForm.getTipoRegistros().toUpperCase();
				} else {
					text += "A";
				}
			} else {
				text += "A";
			}
			ProgressStatus.iProgress++;
		}
		bp.begin();
		Director director = DirectorHandler.getDirector("ultimaRendicion.numero");
		int ult = new Integer(director.getValor());
		ult = ult + 1;
		director.setValor("" + ult);
		bp.saveOrUpdate(director);
		bp.commit();
		ProgressStatus.onProcess = false;
		response.setContentType("text/plain");
		response.setContentLength(text.length());
		String nombreArchivo = String.format("comprobantes_33_%1$tY%1$tm%1$td.txt", new Date());
		response.setHeader("Content-Disposition", "attachment;filename=" + nombreArchivo);
		response.getWriter().write(text);
		response.getWriter().flush();
		response.getWriter().close();
		return null;
	}

	private static String convertirImporte(double importe, int lugares) {
		long entera = Math.round(importe * 100);
		String result = "" + entera;
		if (result.length() > lugares) {
			return result.substring(0, lugares);
		} else {
			return com.asf.cred.StringHelper.completarCeros(result, lugares);
		}
	}

	@SuppressWarnings("unchecked")
	private Ctacte buscarCtaCte(Emideta emideta) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Ctacte> ctacte = new ArrayList<Ctacte>();
		ctacte = bp.createQuery("SELECT c FROM Ctacte c WHERE c.emideta = :emideta").setParameter("emideta", emideta)
				.list();
		if (ctacte.isEmpty()) {
			return null;
		}
		return ctacte.get(0);
	}

}
