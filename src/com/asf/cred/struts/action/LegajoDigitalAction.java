package com.asf.cred.struts.action;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.servlet.RestController;
import com.asf.cred.servlet.RestResponse;
import com.asf.security.BusinessPersistance;
import com.asf.util.CastRequestHelper;
import com.asf.util.DataTableHelper;
import com.asf.util.PersonalizacionHelper;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.ArchivoLegajoDigital;
import com.nirven.creditos.hibernate.LegajoDigital;
import com.nirven.creditos.hibernate.ObservacionLegajoDigital;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class LegajoDigitalAction extends RestController {

	private LegajoDigital legajoDigital;
	private Persona persona;

	@Override
	protected void save(HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		legajoDigital = (LegajoDigital) getMainObject(request);
		legajoDigital.setDeleted(false);
		legajoDigital.setNumeroLegajo(CastRequestHelper.toString("numeroLegajo", request));
		legajoDigital.setPersona(persona);
		legajoDigital.setExpediente(CastRequestHelper.toString("expediente", request));
		bp.save(legajoDigital);
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (CastRequestHelper.toLong("persona.id", request) == null) {
			super.errors.put("LegajoDigital_Persona_Nombre", "Debe seleccionar una persona");
		} else {
			persona = (Persona) super.getObjectByRequestId(Persona.class, "persona.id", request);
		}
		String q = "SELECT l FROM LegajoDigital l WHERE l.persona.id = "
				+ CastRequestHelper.toString("persona.id", request) + " AND l.deleted = 0";
		List<LegajoDigital> rsLegajoDigital = bp.createQuery(q).list();
		if (!rsLegajoDigital.isEmpty()) {
			super.errors.put("LegajoDigital_Persona_Nombre", "La persona seleccionada ya posee un legajo.");
		}
		return super.errors.isEmpty();
	}

	@Override
	public void doDelete(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		getMainObject(request);
		legajoDigital.setDeleted(true);
		bp.save(legajoDigital);
	}

	@Override
	protected Object getMainObject(HttpServletRequest request) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long id = CastRequestHelper.toLong("id", request);
		if (id == null) {
			legajoDigital = new LegajoDigital();
		} else {
			legajoDigital = (LegajoDigital) bp.getById(LegajoDigital.class, id);
		}
		return legajoDigital;
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "deprecation" })
	public void doReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, SQLException {
		// getMainObject(request);
		JasperReport reporte;
		String legajoDigitalId = CastRequestHelper.toString("id", request);
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

		URL url = new URL(u.toString() + "/reports/legajoDigital.jasper");

		try {

			reporte = (JasperReport) JRLoader.loadObject(url.openStream());
			response.setHeader("Content-Disposition", "attachment;filename=\"LegajoDigital.pdf\"");
			String consultaArchivos = "SELECT t.nombre AS Tipo, a.nombre AS NombreArchivo, e.orden AS Orden, e.desde AS Desde, e.hasta AS hasta\n"
					+ "FROM ArchivoLegajoDigital e\n" + "INNER JOIN Archivo a ON a.id = e.archivo_id\n"
					+ "INNER JOIN tipoDearchivo t ON t.id = a.tipoDeArchivo_id\n" + "WHERE e.legajoDigital_id = "
					+ legajoDigitalId + "\n" + "AND e.tipo = $P{tipo}\n";

			// creo condicion de permiso
			com.asf.security.SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			if (sh.isAllowed("/actions/process.do?do=process&processName=ArchivoLegajoDigitalVisualizar")) {
				// condicion si tiene permiso, que visualice todas
				consultaArchivos += "AND visualizar <> 5 ";
			} else {
				// Si no tiene permiso, solo las permitidas
				consultaArchivos += "AND visualizar = 1 ";
			}
			consultaArchivos += "ORDER BY e.orden ASC";
			// "and e.visualizar = 1" +
			HashMap param = new HashMap();
			// param.put("legajoDigitalId", legajoDigitalId); original
			param.put("legajoDigitalId", legajoDigitalId);
			param.put("consultaArchivos", consultaArchivos);
			param.put("SCHEMA", BusinessPersistance.getSchema());
			param.put("REPORTS_PATH", u.toString());
			param = (HashMap) PersonalizacionHelper.getPersonalizacionReporte(param);

			Connection conn = oBp.getCurrentSession().connection();

			JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, param, conn);
			byte[] pdfNotificacion = JasperExportManager.exportReportToPdf(jasperPrint);
			response.setContentType("application/pdf");
			response.setContentLength(pdfNotificacion.length);
			response.getOutputStream().write(pdfNotificacion);
			response.getOutputStream().close();

		} catch (JRException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void doGet(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Long id = CastRequestHelper.toLong("id", request);
		if (id == null) {
			String resultado = null;
			try {
				DataTableHelper dtable = new DataTableHelper();

				String[] columnas = { "l.id", "l.numeroLegajo", "p.NOMB_12", "p.CUIL_12", "p.NUDO_12", "l.deleted",
						"l.expediente" };
				String tabla_ppal = "LegajoDigital l";
				String idArchivo_ppal = "l.id";
				String filtro = "l.deleted = 0";
				String join = "JOIN Persona p ON p.IDPERSONA = l.persona_IDPERSONA";
				resultado = dtable.getDataTableResponse(request, tabla_ppal, idArchivo_ppal, columnas, filtro, join);

			} catch (Exception e) {
				e.printStackTrace();
			}
			response.getWriter().print(resultado);
			response.getWriter().flush();
		} else {
			super.doGet(mapping, form, request, response);
		}
	}

	public void doGetDocumentoAdeId(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Object[]> resultSet = null;
		RestResponse rr = new RestResponse(response);
		String codigoExpediente = CastRequestHelper.toString("codigoExpediente", request);
		String q = "FROM DocumentoADE WHERE identificacion = '" + codigoExpediente + "'";
		resultSet = bp.getByFilter(q);
		rr.setData(resultSet, new String[] { "id" });
		rr.print();

	}

	public void doObservacionVencimiento(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// ALD = Alias para ArchivoLegajoDigital
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idLegajoDigital = CastRequestHelper.toLong("legajoDigital_id", request);
		String sHQL = "FROM ArchivoLegajoDigital WHERE legajoDigital_id = " + idLegajoDigital;
		List<ArchivoLegajoDigital> listALD = bp.getByFilter(sHQL);
		for (ArchivoLegajoDigital ALD : listALD) {
			Date hoy = new Date();
			if (ALD.getHasta() == null) {
				deleteObservacionesPorVencimiento(ALD);
			} else {
				if (ALD.getHasta().compareTo(hoy) < 0) {
					updateObservacionesPorVencimiento(ALD);
				} else {
					deleteObservacionesPorVencimiento(ALD);
				}
			}
		}
	}

	private void deleteObservacionesPorVencimiento(ArchivoLegajoDigital ALD) {
		// OLD = ObservacionLegajoDigital
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String sHQL = "FROM ObservacionLegajoDigital WHERE archivoLegajoDigital_id = " + ALD.getId();
		List<ObservacionLegajoDigital> listOLD = bp.getByFilter(sHQL);
		bp.delete(listOLD);
	}

	private void updateObservacionesPorVencimiento(ArchivoLegajoDigital ALD) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String sHQL = "FROM ObservacionLegajoDigital WHERE archivoLegajoDigital_id = " + ALD.getId();
		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
		String textoObservacion = ALD.getArchivo().getNombre() + " DOCUMENTACION VENCIDA "
				+ DATE_FORMAT.format(ALD.getHasta());
		List<ObservacionLegajoDigital> listOLD = bp.getByFilter(sHQL);
		if (listOLD.isEmpty()) {

			ObservacionLegajoDigital OLD = new ObservacionLegajoDigital();
			OLD.setArchivoLegajoDigital(ALD);
			OLD.setCauserK(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			OLD.setFecha(new Date());
			OLD.setLegajoDigital(ALD.getLegajoDigital());
			OLD.setMostrar(true);
			OLD.setTexto(textoObservacion);
			OLD.setTipo(ALD.getTipo());
			bp.save(OLD);
		} else {

			for (ObservacionLegajoDigital OLD : listOLD) {
				OLD.setTexto(textoObservacion);
				bp.save(OLD);
			}
		}
	}

}
