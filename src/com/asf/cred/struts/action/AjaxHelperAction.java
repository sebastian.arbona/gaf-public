package com.asf.cred.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.asf.cred.business.TipoProcesoDTO;
import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.AjaxHelperForm;
import com.asf.gaf.hibernate.Cotizacion;
import com.asf.gaf.hibernate.Moneda;
import com.asf.security.BusinessPersistance;
import com.asf.util.BeanHelper;
import com.asf.util.EscapeHelper;
import com.asf.util.SQLHelper;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nirven.creditos.hibernate.Cargo;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Modelo;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObservacionRelevante;
import com.nirven.creditos.hibernate.ProcesoResolucion;
import com.nirven.creditos.hibernate.ProcesoTipoMovimiento;
import com.nirven.creditos.hibernate.ProcesoTipoResolucion;
import com.nirven.creditos.hibernate.SubTipoResolucion;
import com.nirven.creditos.hibernate.TipoMovimientoInstancia;
import com.nirven.creditos.hibernate.TipoProceso;
import com.nirven.creditos.hibernate.TipoResolucion;

public class AjaxHelperAction extends DispatchAction {
	// atributos de logueo.-
	// constructor por defecto.-
	public AjaxHelperAction() {

	}// fin constructor.-

	// este metodo es usado por el select2 para cargar los datos. es generico
	// pero esta particular en cada uno
	@SuppressWarnings("rawtypes")
	public ActionForward ajaxQuery(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		AjaxHelperForm ajaxHelperForm = (AjaxHelperForm) form;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String[] arr = ajaxHelperForm.getListValues().split(";");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setCharacterEncoding("ISO-8859-1");
		Object o = null;
		PrintWriter out = null;
		String xml = "<Objects>";
		try {
			out = response.getWriter();
			String consulta = "FROM " + EscapeHelper.escapeQL(ajaxHelperForm.getEntityName());
			consulta += " WHERE " + ajaxHelperForm.getFilter();
			if (ajaxHelperForm.getFilter2() != null && ajaxHelperForm.getFilter2().length() > 0) {
				consulta += " and " + ajaxHelperForm.getFilter2();
			}
			List l = bp.getByFilter(consulta);
			for (Iterator iter = l.iterator(); iter.hasNext();) {

				xml += "<" + ajaxHelperForm.getEntityName() + ">";
				o = iter.next();
				for (int i = 0; i < arr.length; i++) {
					if (xml.contains("<" + arr[i].replace(".", "_") + ">"))
						continue;
					xml += "<" + arr[i].replace(".", "_") + ">" + EscapeHelper.escapeXML(BeanHelper.parseGet(o, arr[i]))
							+ "</" + arr[i].replace(".", "_") + ">";
				}
				xml += "</" + ajaxHelperForm.getEntityName() + ">";
			}
			xml += "</Objects>";
		} catch (Exception e) {
			xml = "<Objects></Objects>";
		}
		out.print(xml);
		out.flush();
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward ajaxTipificadorDeLinea(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idLinea = new Long(request.getParameter("idLinea"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Linea linea = (Linea) bp.getById(Linea.class, idLinea);
		List<Cotizacion> cotizaciones = bp
				.createQuery("Select c from Cotizacion c where c.moneda=:moneda order by c.fechaDesde")
				.setParameter("moneda", linea.getMoneda()).list();
		Cotizacion coti = new Cotizacion();
		if (cotizaciones.size() != 0) {
			coti = cotizaciones.get(0);
		}
		for (Cotizacion cotizacion : cotizaciones) {
			if (cotizacion.getFechaHasta() == null) {
				coti = cotizacion;
			} else {
				if (coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
					coti = cotizacion;
				}
			}
		}
		try {
			response.getWriter().print(linea.getTipoLinea() + "-" + coti.getVenta().toString());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward ajaxTipificadorDeLineaSolicitud(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		Long idLinea = new Long(request.getParameter("idLinea"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Linea linea = (Linea) bp.getById(Linea.class, idLinea);
		List<Cotizacion> cotizaciones = bp
				.createQuery("Select c from Cotizacion c where c.moneda=:moneda order by c.fechaDesde")
				.setParameter("moneda", linea.getMoneda()).list();
		Cotizacion coti = new Cotizacion();
		if (cotizaciones.size() != 0) {
			coti = cotizaciones.get(0);
		}
		for (Cotizacion cotizacion : cotizaciones) {
			if (cotizacion.getFechaHasta() == null) {
				coti = cotizacion;
			} else {
				if (coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
					coti = cotizacion;
				}
			}
		}
		try {
			JsonArray jsonArr = new JsonArray();
			jsonArr.add(linea.getTipoLinea());
			jsonArr.add(linea.getFecovitaTipo());
			response.getWriter().print(jsonArr.toString());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ActionForward ajaxFecovitaTipoSolicitud(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idLinea = new Long(request.getParameter("idLinea"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Linea linea = (Linea) bp.getById(Linea.class, idLinea);
		linea.getFecovitaTipo();
		try {
			response.getWriter().print(linea.getFecovitaTipo());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	public ActionForward ajaxLineaMonedaCotizacion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idLinea = new Long(request.getParameter("idLinea"));
		Linea linea = (Linea) bp.getById(Linea.class, idLinea);
		try {
			response.getWriter().print(linea.getMoneda().getDenominacion() + "-" + linea.getMoneda().getCotizacion());
			response.getWriter().flush();
		} catch (IOException e) {
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward ajaxMonedaCotizacion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idMoneda = new Long(request.getParameter("idMoneda"));
		Moneda moneda = (Moneda) bp.getById(Moneda.class, idMoneda);
		List<Cotizacion> cotizaciones = bp
				.createQuery("Select c from Cotizacion c where c.moneda=:moneda order by c.fechaDesde")
				.setParameter("moneda", moneda).list();
		Cotizacion coti = new Cotizacion();
		if (cotizaciones.size() != 0) {
			coti = cotizaciones.get(0);
		}
		for (Cotizacion cotizacion : cotizaciones) {
			if (cotizacion.getFechaHasta() == null) {
				coti = cotizacion;
			} else {
				if (coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
					coti = cotizacion;
				}
			}
		}
		try {
			response.getWriter().print(coti.getVenta());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward ajaxCotizacionFecha(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idMoneda = new Long(request.getParameter("idMoneda"));
		// String fecha = request.getParameter("fecha");
//		Cotizacion coti = (Cotizacion) bp.createQuery(
//				"SELECT c FROM Cotizacion c WHERE c.moneda.id = " + idMoneda
//						+ " AND c.fechaDesde = (SELECT max(d.fechaDesde) FROM Cotizacion d WHERE d.moneda.id = " + idMoneda 
//						+ " AND d.fechaDesde < :fecha) ORDER BY c.fechaDesde DESC")
//						.setParameter("fecha", DateHelper.getDate(fecha)).list().get(0);
//		
		// Busco el ultimo valor de la cotizacion para la moneda
		List<Cotizacion> cotis = bp
				.createQuery("Select c from Cotizacion c where c.moneda.id=:moneda order by c.fechaDesde")
				.setParameter("moneda", idMoneda).list();
		Cotizacion coti = new Cotizacion();
		if (cotis.size() != 0) {
			coti = cotis.get(0);
		}
		for (Cotizacion cotizacion : cotis) {
			if (cotizacion.getFechaHasta() == null) {
				coti = cotizacion;
			} else {
				if (coti.getFechaHasta() != null && coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
					coti = cotizacion;
				}
			}
		}

		try {
			response.getWriter().print(coti.getVenta());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ActionForward ajaxTraerBody(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idModelo = new Long(Long.parseLong(request.getParameter("modeloid")));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Modelo modelo = (Modelo) bp.getById(Modelo.class, idModelo);
		try {
			response.getWriter().print(modelo.getBodyStr());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward mostrarFuncion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		try {
			String idCargo = request.getParameter("idCargo");

			Cargo cargo = (Cargo) bp.getById(Cargo.class, new Long(idCargo));

			response.getWriter().print("Funci�n: " + cargo.getFuncion());

			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward ajaxTraerTipoProceso(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
		Long idSubTipoResolucion = new Long(request.getParameter("idSubTipoResolucion"));

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<ProcesoTipoResolucion> list = (List<ProcesoTipoResolucion>) bp.createQuery(
				"Select ptr from ProcesoTipoResolucion ptr where ptr.subTipoResolucion.id = :idSubTipoResolucion")
				.setParameter("idSubTipoResolucion", idSubTipoResolucion).list();
		List<TipoProcesoDTO> listaProceso = new ArrayList<TipoProcesoDTO>();

		for (ProcesoTipoResolucion pTR : list) {

			listaProceso.add(new TipoProcesoDTO(pTR.getProceso().getId(), pTR.getProceso().getNombreTipoProceso()));
		}

		Gson gson = new Gson();
		String json = gson.toJson(listaProceso);
		// String a = "\"hola\"";

		try {
			PrintWriter out = response.getWriter();
			out.println(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward ajaxTraerCamposTipoProceso(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
		String nombreTipoProceso = new String(request.getParameter("nombreTipoProceso"));

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<TipoProceso> list = (List<TipoProceso>) bp
				.createQuery("Select tpr from TipoProceso tpr where tpr.nombreTipoProceso = :nombreTipoProceso")
				.setParameter("nombreTipoProceso", nombreTipoProceso).list();

		String camposAMostrar = "";
		TipoProceso tpr = null;

		if (!list.isEmpty()) {

			tpr = (TipoProceso) list.get(0);

			camposAMostrar = camposAMostrar + "caratulaDiv:" + tpr.isCaratula();
			camposAMostrar = camposAMostrar + "-definitivoDiv:" + tpr.isDefinitivo();
			camposAMostrar = camposAMostrar + "-expedienteDiv:" + tpr.isExpediente();
			camposAMostrar = camposAMostrar + "-fechaPresentacionDiv:" + tpr.isFechaPresentacion();
			camposAMostrar = camposAMostrar + "-fechaSentenciaDiv:" + tpr.isFechaSentencia();
			camposAMostrar = camposAMostrar + "-fechaEmbargoDiv:" + tpr.isFechaEmbargo();
			camposAMostrar = camposAMostrar + "-juzgadoDiv:" + tpr.isJuzgado();
			camposAMostrar = camposAMostrar + "-montoDiv:" + tpr.isMonto();
			camposAMostrar = camposAMostrar + "-nombreDiv:" + tpr.isNombre();
			camposAMostrar = camposAMostrar + "-tipoDiv:" + tpr.isTipo();
			camposAMostrar = camposAMostrar + "-personaDiv:" + tpr.isPersona();
			camposAMostrar = camposAMostrar + "-plantillaMailDiv:" + tpr.isPlantillaMail();
			camposAMostrar = camposAMostrar + "-destinatarioMailDiv:" + tpr.isPlantillaMail();
			camposAMostrar = camposAMostrar + "-NotificacionDiv:" + tpr.isNotificacion();
			camposAMostrar = camposAMostrar + "-tipoRegistroDiv:" + tpr.isTipoRegistro();
			camposAMostrar = camposAMostrar + "-tipoEmbargoDiv:" + tpr.isTipoEmbargo();
			camposAMostrar = camposAMostrar + "-circunscripcionDiv:" + tpr.isCircunscripcion();
			camposAMostrar = camposAMostrar + "-tipoTribunalDiv:" + tpr.isTipoTribunal();
			camposAMostrar = camposAMostrar + "-tipoMedidaDiv:" + tpr.isTipoMedida();
			camposAMostrar = camposAMostrar + "-tipoInstanciaDiv:" + tpr.isTipoInstancia();
			camposAMostrar = camposAMostrar + "-camaraDiv:" + tpr.isCamara();

		}

		Gson gson = new Gson();
		String json = gson.toJson(camposAMostrar);

		try {
			PrintWriter out = response.getWriter();
			out.println(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward ajaxTraerTipoResolucion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setContentType("text/html;charset=UTF-8");

		Long idSubTipoResolucion = new Long(request.getParameter("idSubTipoResolucion"));

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		SubTipoResolucion str = (SubTipoResolucion) bp
				.createQuery("Select ptr from SubTipoResolucion ptr where ptr.id = :idSubTipoResolucion")
				.setParameter("idSubTipoResolucion", idSubTipoResolucion).uniqueResult();
		TipoResolucion tr = (TipoResolucion) bp
				.createQuery("Select ptr from TipoResolucion ptr where ptr.id = :idSubTipoResolucion")
				.setParameter("idSubTipoResolucion", str.getTipoResolucion().getId()).uniqueResult();

		try {
			PrintWriter out = response.getWriter();
			out.println(tr.getNombre());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward ajaxTraerDatosCredito(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		response.setContentType("text/html;charset=UTF-8");

		Long numeroAtencion = new Long(request.getParameter("numeroAtencion"));
		String result = "No existe el credito ingresado.";

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Objetoi str = (Objetoi) bp.createQuery("Select ptr from Objetoi ptr where ptr.numeroAtencion = :numeroAtencion")
				.setParameter("numeroAtencion", numeroAtencion).uniqueResult();

		if (str != null) {

			result = str.getPersona().getNomb12() + " - " + str.getPersona().getCuil12() + " - "
					+ str.getLinea().getNombre();
		}

		try {
			PrintWriter out = response.getWriter();
			out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward populateObservacionRelevante(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		// AjaxHelperForm ajaxHelperForm = (AjaxHelperForm) form;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		response.setContentType("application/json;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setCharacterEncoding("ISO-8859-1");
//		Object o = null;
		PrintWriter out = null;

		try {

			out = response.getWriter();
			List<ObservacionRelevante> observacionesRelevante = (List<ObservacionRelevante>) bp
					.createQuery("from ObservacionRelevante").list();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			mapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

			mapper.writeValue(out, observacionesRelevante);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		out.flush();

		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward populateTipoMovimiento(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		AjaxHelperForm ajaxHelperForm = (AjaxHelperForm) form;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		response.setContentType("application/json;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setCharacterEncoding("ISO-8859-1");
//		Object o = null;
		PrintWriter out = null;
//		List<Rubroeconomico> listaRubroEconomico = new ArrayList<Rubroeconomico>();
		List<TipoMovimientoInstancia> listaTipoMovimientoInstancia = new ArrayList<TipoMovimientoInstancia>();

//		String listaIdsRubroEconomico = "";

		try {

			out = response.getWriter();

			Long idProcesoResolucion = new Long(ajaxHelperForm.getFilter());

			ProcesoResolucion procesoResolucion = (ProcesoResolucion) bp.getById(ProcesoResolucion.class,
					idProcesoResolucion);

			List<ProcesoTipoMovimiento> listaProcesoTipoMovimiento = (List<ProcesoTipoMovimiento>) bp
					.createQuery("From ProcesoTipoMovimiento pTM where pTM.proceso.id = "
							+ procesoResolucion.getTipoProceso().getId())
					.list();

			for (ProcesoTipoMovimiento ua : listaProcesoTipoMovimiento) {

				listaTipoMovimientoInstancia.add(ua.getTipoMovimientoInstancia());
			}

			if (listaTipoMovimientoInstancia.isEmpty()) {

				TipoMovimientoInstancia areaNueva = new TipoMovimientoInstancia();
				areaNueva.setId(-1L);
				areaNueva.setNombre("La instancia no tiene tipos de movimientos asociados");
				listaTipoMovimientoInstancia.add(areaNueva);

			}

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
			mapper.configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

			mapper.writeValue(out, listaTipoMovimientoInstancia);

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		out.flush();

		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward getBuscPersona(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String resultado = null;
		String filtro = null;
		String campos = null;
		String cantidad = null;
		List<Object[]> datos = null;

		boolean continuar = true;

		try {
			filtro = request.getParameter("filtro");
			campos = request.getParameter("campos");
			cantidad = request.getParameter("cantidad");

			if (campos == null || campos.isEmpty() || campos.equals("*")) {
				continuar = false;
			}

			if (continuar) {

				String consulta = "SELECT  p.idpersona, p.nomb_12, p.nudo_12, p.CUIL_12" + " FROM "
						+ BusinessPersistance.getSchema() + ".Persona p";

				if (filtro != null && !"".equals(filtro) && !"null".equals(filtro)) {
					consulta += " WHERE (p.nomb_12 LIKE '%" + filtro + "%'";
					consulta += " OR p.nudo_12 LIKE '%" + filtro + "%')";
				}

				if (cantidad != null && !"".equals(cantidad) && !"null".equals(cantidad)) {
					consulta = SQLHelper.getLimitedQuery(consulta, new Integer(cantidad));
				}

				datos = bp.createSQLQuery(consulta).list();

				JsonArray jsons = new JsonArray();

				for (Object[] item : datos) {
					JsonObject jsonObj = new JsonObject();
					Number idpersona = (Number) item[0];
					String nombre = (String) item[1];
					Number dni = (Number) item[2];
					Number cuil = (Number) item[3];
					String formatDNI = "<span class='ocultardni'>" + dni + " - </span>";
					jsonObj.addProperty("id", idpersona);
					jsonObj.addProperty("text", formatDNI + nombre);
					jsonObj.addProperty("cuil", cuil);
					jsonObj.addProperty("dni", dni);
					jsonObj.addProperty("idPersona", idpersona);
					jsonObj.addProperty("nombre", nombre);
					jsons.add(jsonObj);
				}
				resultado = jsons.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		response.getWriter().print(resultado);
		response.getWriter().flush();
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward getDocumentosAde(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String resultado = null;
		String filtro = null;
		String campos = null;
		String cantidad = null;
		Long periodo = null;
		Long tipoexp = null;
		List<Object[]> datos = null;

		boolean continuar = true;

		try {
			filtro = request.getParameter("filtro");
			campos = request.getParameter("campos");
			cantidad = request.getParameter("cantidad");
			periodo = (request.getParameter("periodo") != null && request.getParameter("periodo").length() > 0)
					? new Long(request.getParameter("periodo"))
					: null;
			tipoexp = (request.getParameter("tipoexp") != null && request.getParameter("tipoexp").length() > 0)
					? new Long(request.getParameter("tipoexp"))
					: null;

			if (campos == null || campos.isEmpty() || campos.equals("*")) {
				continuar = false;
			}

			if (continuar) {
				String consulta = "SELECT da.identificacion, da.asunto, e.idexpegasto, e.ejercicio, e.idtipoexpe"
						+ " FROM " + BusinessPersistance.getSchema() + ".DocumentoADE da" + " LEFT OUTER JOIN "
						+ BusinessPersistance.getSchema() + ".Expegasto e on e.EXPEDIENTE = da.identificacion";

				if (filtro != null && !"".equals(filtro) && !"null".equals(filtro)) {
					consulta += " WHERE (da.identificacion LIKE '%" + filtro + "%'";
					consulta += " OR da.asunto LIKE '%" + filtro + "%')";
				}

				if (periodo != null && periodo > 0L) {
					consulta += " AND (e.ejercicio = " + periodo + " OR e.ejercicio is null)";
				}

				if (tipoexp != null && tipoexp > 0L) {
					consulta += " AND (e.idtipoexpe = " + tipoexp + " OR e.idtipoexpe is null)";
				}

				consulta += " ORDER BY e.ejercicio DESC";

				if (cantidad != null && !"".equals(cantidad) && !"null".equals(cantidad)) {
					consulta = SQLHelper.getLimitedQuery(consulta, new Integer(cantidad));
				}

				datos = bp.createSQLQuery(consulta).list();

				JsonArray jsons = new JsonArray();

				for (Object[] item : datos) {
					JsonObject jsonObj = new JsonObject();
					String expediente = (String) item[0];
					String asunto = (item[1] != null) ? (String) item[1] : "";
					boolean imputado = (item[2] != null) ? true : false;
					Long ejercicio = (item[3] != null) ? new Long(item[3].toString()) : null;
					jsonObj.addProperty("id", expediente);
					jsonObj.addProperty("text",
							((ejercicio != null) ? (ejercicio + " - ") : "") + expediente + " - " + asunto);
					jsonObj.addProperty("imputado", imputado);
					jsonObj.addProperty("periodo", ejercicio);
					jsons.add(jsonObj);
				}
				resultado = jsons.toString();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		response.getWriter().print(resultado);
		response.getWriter().flush();
		return null;
	}
}
