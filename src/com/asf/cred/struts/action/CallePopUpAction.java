/*
 * AjaxPopUp.java
 *
 * Created on 22 de noviembre de 2005, 11:28
 */

package com.asf.cred.struts.action;

import com.asf.cred.struts.form.CallePopUpForm;
import com.asf.util.StringHelper;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.actions.DispatchAction;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.asf.security.BusinessPersistance;

import com.asf.security.SessionHandler;

import com.asf.util.Constants;

/**
 *
 * @author cgarcia
 * @version
 */

public class CallePopUpAction extends DispatchAction {
    
   
    public ActionForward get(ActionMapping mapping, ActionForm  form,HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        CallePopUpForm calleForm = (CallePopUpForm)form;
        BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        PrintWriter out = response.getWriter();
        List list=new ArrayList();

        String strName = calleForm.getName().replaceAll( "\\[", "_").replaceAll( "\\]", "_").replaceAll("\\.", "_");
        Object[] obj={"","",""};
        
        out.println( "<html><title></title><body><script language=\"javascript\">\n" );
        out.println( "function actualizar(control, valor){\n " +
                     "if (window.parent)\n{ " +
                     "if(window.parent.document.getElementById){\n" +
                     "var e = window.parent.document.getElementById( control );\n" +
                     "if (e)\n " +
                     "e.value = valor;\n" +
                     "}\n " +
                     "else\n" +
                     "if (window.parent.document.all){\n " +
                     "window.parent.document.all[control].value = valor;\n" +
                     "}\n " +
                     "}\n " +
                     "}\n" +
                     "</script>\n");
        out.println( "<script language=\"javascript\">\n" );

        if(calleForm.getValue()!=null && calleForm.getValue().length()>0){
            list=bp.getByFilter("SELECT DISTINCT c.codi08,c.deta08,c.idcalle FROM Calle c WHERE c.codi08='" + StringHelper.replaceSQL( calleForm.getValue().toUpperCase() )+"'");
        }

        if(list.size()==1){
            for (Iterator it = list.iterator(); it.hasNext();) {
                Object[] elem = (Object[]) it.next();
                obj[0]=elem[0];
                obj[1]=elem[1];
                obj[2]=elem[2];
            }
            

            out.println( "actualizar('" + strName +"Descriptor', '" + obj[1].toString() + "');\n" );
            out.println( "actualizar('" + calleForm.getProperty() +"','" + obj[2].toString() + "');\n" );
         }else{
            out.println( "actualizar('" + calleForm.getProperty() + "','');\n" );
            out.println( "actualizar('" + strName + "Codigo','');\n" );
            out.println( "actualizar('" + strName + "Descriptor','');\n" );
            out.println( "alert('La CALLE no se encontr�. Por favor, intente nuevamente.');\n" );
            out.println( "window.parent.document.getElementById('" + strName + "Codigo').focus();\n");
        }
        
        out.println("</script></body></html>");
        return null;
    }
    
    public ActionForward list(ActionMapping mapping, ActionForm  form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
    	CallePopUpForm calleForm = (CallePopUpForm)form;
        String strName = calleForm.getName().replaceAll( "\\[", "_").replaceAll( "\\]", "_").replaceAll("\\.", "_");
    	
    	if((calleForm.getCalle()!=null && calleForm.getCalle().length()>0) || (calleForm.getCodigo()!=null && calleForm.getCodigo().length()>0)){
    		
    		String where ="";
    		
    		if(calleForm.getCalle().length()>0){
                    where="upper(c.deta08) LIKE '%"+calleForm.getCalle().toUpperCase()+"%'";
    		}
    		
    		try{
                    if(calleForm.getCodigo()!=null){
                        if(where.length()>0){
                            where=where+" AND upper(c.codi08) LIKE '%"+calleForm.getCodigo().toUpperCase()+"%'";
                        }else{
                            where=" upper(c.codi08) LIKE '"+calleForm.getCodigo().toUpperCase()+"%'";
                        }
                    }
    		}catch(NumberFormatException e){
    			
    		}
    		
    		if (where.length()>0){
                    BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();	
                    List lstCalles = oBp.getByFilter("SELECT c.codi08,c.deta08,c.idcalle FROM Calle c WHERE "+where, Constants.MAX_SELECT_POPUP_RECORDS);
                    calleForm.setCalleList(lstCalles);                    
    		}
    	}
        request.setAttribute( "CallePopUpForm", calleForm );
        return mapping.findForward( "callePopUp" );
    }
}
