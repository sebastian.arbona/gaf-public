package com.asf.cred.struts.action;

import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.struts.action.*;
import org.apache.struts.actions.*;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.LoginForm;

/**
 * MyEclipse Struts
 * Creation date: 11-04-2005
 *
 * XDoclet definition:
 * @struts.action path="/action/login" name="loginForm" input="/form/login.jsp" scope="request" validate="true"
 * @struts.action-forward name="index" path="/index.jsp" redirect="true"
 */
public class LoginAction extends DispatchAction {

    // --------------------------------------------------------- Instance Variables
    // --------------------------------------------------------- Methods
    /**
     * Method execute
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward login(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        LoginForm loginForm = (LoginForm) form;
        SessionHandler sh = new SessionHandler();
        if (sh.login(request, loginForm.getName(), loginForm.getPassword())) {

            if (loginForm.getUrlDestino() == null || "".equals(loginForm.getUrlDestino().trim())) {
                return mapping.findForward("index");
            } else {
                try {
                    response.sendRedirect(loginForm.getUrlDestino());
                } catch (Exception e) {
                    return mapping.findForward("index");
                }
                return null;
            }

        } else {
            ActionMessages messages = new ActionMessages();

            //ORA-01017- pass / user invalido
            //ORA-28000- usuario bloqueado

            if (sh.getError() != null && sh.getError().contains("login.bloqueado")) {
                messages.add("login.bloqueado", new ActionMessage("login.bloqueado"));
            } else {
                messages.add("login.error", new ActionMessage("login.error"));
            }

            saveErrors(request, messages);
            loginForm.setPassword("");
            return mapping.findForward("login");
        }
    }

    public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            SessionHandler.getCurrentSessionHandler().logout();
        } catch (Exception e) {
        }

        return mapping.findForward("login");
    }
}

