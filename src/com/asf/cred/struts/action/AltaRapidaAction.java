package com.asf.cred.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AltaRapidaAction extends PersonaAction
{

    @Override
    public ActionForward newEntity( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws Exception
    {
        super.newEntity( mapping, form, request, response );
        return mapping.findForward( ( String ) request.getParameter( "forward" ) );
    }

    @Override
    public ActionForward save( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws Exception
    {
        String forward = ( String ) request.getParameter( "forward" );
        super.save( mapping, form, request, response );
        request.setAttribute( "CERRAR", "<script>window.close();</script>" );
        return mapping.findForward( forward );
    }

    @Override
    public ActionForward load( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws Exception
    {
        String forward = ( String ) request.getParameter( "forward" );
        super.load( mapping, form, request, response );
        return mapping.findForward( forward );
    }
}