package com.asf.cred.struts.action;

import java.util.Date;

public class BeanDeuda {

	private Date fechaVencimiento;
	private double capital;
	private double cer;
	private double compensatorio;
	private double moratorioPunitorio;
	private double gastos;
	private double comision;
	private double envio;
	private double iva;
	private Date fechaCalculo;
	private String linea;
	private String nomb12;
	private String cuil12;
	private String estado;
	private String comportamiento;
	private Long idPersona;
	private Long numeroAtencion;
	
	public String getDetalle() {
		return linea + " - " + nomb12;
	}
	
	public double getTotalFondo() {
		return capital + cer + compensatorio + moratorioPunitorio + gastos;
	}
	
	public double getTotalInv() {
		return getTotalFondo() + comision + envio + iva;
	}
	
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public double getCapital() {
		return capital;
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	public double getCer() {
		return cer;
	}
	public void setCer(double cer) {
		this.cer = cer;
	}
	public double getCompensatorio() {
		return compensatorio;
	}
	public void setCompensatorio(double compensatorio) {
		this.compensatorio = compensatorio;
	}
	public double getMoratorioPunitorio() {
		return moratorioPunitorio;
	}
	public void setMoratorioPunitorio(double moratorioPunitorio) {
		this.moratorioPunitorio = moratorioPunitorio;
	}
	public double getGastos() {
		return gastos;
	}
	public void setGastos(double gastos) {
		this.gastos = gastos;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public double getEnvio() {
		return envio;
	}
	public void setEnvio(double envio) {
		this.envio = envio;
	}
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public Date getFechaCalculo() {
		return fechaCalculo;
	}
	public void setFechaCalculo(Date fechaCalculo) {
		this.fechaCalculo = fechaCalculo;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getNomb12() {
		return nomb12;
	}
	public void setNomb12(String nomb12) {
		this.nomb12 = nomb12;
	}
	public String getCuil12() {
		return cuil12;
	}
	public void setCuil12(String cuil12) {
		this.cuil12 = cuil12;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getComportamiento() {
		return comportamiento;
	}
	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public Long getNumeroAtencion() {
		return numeroAtencion;
	}
	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}
	
}
