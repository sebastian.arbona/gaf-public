package com.asf.cred.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.hibernate.exception.ConstraintViolationException;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.AbmAction;
import com.asf.struts.form.AbmForm;
import com.asf.struts.form.ListForm;
import com.civitas.hibernate.persona.Calle;

public class CalleAction extends AbmAction {
	
	 @Override
	    public ActionForward newEntity( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) throws Exception
	    {
		 
		 String forward = ( String ) request.getParameter( "forward" );
		 
		 if(forward != null){
			 super.newEntity( mapping, form, request, response );
             return mapping.findForward( forward );
		 }else{
			 return super.newEntity(mapping, form, request, response);
		 }        
	        
	    }
	
	
	public ActionForward save( ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response )
	throws Exception{
		AbmForm oForm = ( AbmForm ) form;
		ListForm oLForm = null;
		String forward = ( String ) request.getParameter( "forward" );

		if( !isCancelled( request ) ){
			BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			try{
				oBp.saveOrUpdate( oForm.getEntity() );
				Calle ca = (Calle) oForm.getEntity();
				ca.setCodi08(ca.getId().toString());
				oForm.setEntity(ca);
				oBp.saveOrUpdate( oForm.getEntity() );
			}catch( ConstraintViolationException e ){
				ActionMessages messages = new ActionMessages();
				messages.add( "constraint.error", new ActionMessage( "constraint.error" ) );
				saveErrors( request, messages );
				return mapping.findForward( "errorPage" );
			}

		}
		 
		 if(forward != null){
			 request.setAttribute( "CERRAR", "<script>window.close();</script>" );
		     return mapping.findForward( forward );
		 }else{
			 return list( mapping, oForm, request, response );
		 }

	}
	
}
