package com.asf.cred.struts.action;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.AsociarContratoExportForm;
import com.asf.gaf.hibernate.Cotizacion;
import com.asf.hibernate.mapping.Claves;
import com.asf.security.BusinessPersistance;
import com.asf.util.StringHelper;
import com.civitas.el.especific.ContratoExecutionContext;
import com.civitas.el.especific.ContratoExpressionEvaluator;

import com.civitas.hibernate.persona.Persona;
import com.civitas.logger.Log4jConstants;
import com.civitas.logger.LoggerService;
import com.civitas.util.PdfHelper;
import com.nirven.creditos.hibernate.Documento;
import com.nirven.creditos.hibernate.Escrito;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.expedientes.persistencia.Archivo;
import com.nirven.expedientes.persistencia.TipoDeArchivo;

public class AsociarContratoExportAction extends DispatchAction {
	public Long lineaL = new Long(0);
	LoggerService logger = LoggerService.getInstance();

	@SuppressWarnings("unchecked")
	public ActionForward listar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AsociarContratoExportForm asociarContratoForm = (AsociarContratoExportForm) form;
		setLineaL(asociarContratoForm.getLinea());
		if ((getLineaL() != null && getLineaL() != 0L)
				|| (asociarContratoForm.getIdObjetoi() != null && asociarContratoForm.getIdObjetoi() != 0L)) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			String consulta = "SELECT DISTINCT c.objetoi FROM ObjetoiEstado c WHERE c.estado.nombreEstado = :estado "
					+ ((getLineaL() != null && getLineaL() != 0L) ? "AND c.objetoi.linea.id = :linea "
							: "AND c.objetoi.id = :idObjetoi ")
					+ "AND c.fecha = (SELECT max(e.fecha) FROM ObjetoiEstado e WHERE e.objetoi.id = c.objetoi.id) AND c.fechaHasta IS NULL";
			List<Objetoi> result = null;
			if (getLineaL() != null && getLineaL() != 0L) {
				result = (List<Objetoi>) bp.createQuery(consulta).setLong("linea", getLineaL())
						.setString("estado", "PENDIENTE CONTRATO").list();
			} else {
				result = (List<Objetoi>) bp.createQuery(consulta)
						.setLong("idObjetoi", asociarContratoForm.getIdObjetoi())
						.setString("estado", "PENDIENTE CONTRATO").list();
			}
			asociarContratoForm.setResult(result);
			asociarContratoForm.setLinea(lineaL);
		}
		if ((asociarContratoForm.getIdObjetoi() != null && asociarContratoForm.getIdObjetoi() != 0L)) {
			return mapping.findForward("AsociarContratoListNF");
		} else {
			return mapping.findForward("AsociarContratoList");
		}
	}

	@SuppressWarnings("unchecked")
	public ActionForward generar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AsociarContratoExportForm asociarContratoExportForm = (AsociarContratoExportForm) form;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long escritoid = asociarContratoExportForm.getEscrito();
		if (getLineaL() == null || getLineaL() == 0L) {
			return null;
		}
		Enumeration<String> paramNames = request.getParameterNames();
		String ids = "";
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("seleccion-") && request.getParameter(param) != null
					&& !request.getParameter(param).equals("")) {
				String[] p = param.split("-");
				ids += p[1] + ",";
			}
		}
		if (ids.length() < 1) {
			return mapping.findForward("AsociarContratoList");
		}
		ArrayList<Objetoi> objetosi = (ArrayList<Objetoi>) bp
				.createQuery("SELECT o FROM Objetoi o WHERE o.id IN (" + ids.substring(0, ids.length() - 1) + ")")
				.list();
		Escrito escrito = (Escrito) bp.createQuery("SELECT e FROM Escrito e WHERE e.id = :escritoid")
				.setLong("escritoid", escritoid).uniqueResult();
		bp.begin();
		try {
			ArrayList<String> docs = new ArrayList<String>();
			for (Objetoi objetoi : objetosi) {
				docs.add(generarDocumento(objetoi, escrito, request.getSession().getServletContext(), response, bp,
						request, asociarContratoExportForm));
			}
			ArrayList<File> archs = armarHTMLs(docs, objetosi); // archivos html
			ByteArrayOutputStream baos = crearZip(archs);
			byte[] bytesZip = baos.toByteArray();
			response.addHeader("Content-Disposition",
					"attachment; filename=contrato" + escrito.getIdentificacion().replace(" ", "") + ".zip");
			response.setContentType("application/zip");
			response.setContentLength(bytesZip.length);
			OutputStream out = response.getOutputStream();
			out.write(bytesZip);
			out.close();
			bp.commit();
		} catch (Exception e) {
			bp.rollback();
			logger.log(Log4jConstants.ERROR, "Error al generar documentos y adjuntar en ZIP.", e);
		}
		return mapping.findForward("AsociarContratoList");
	}

	private ByteArrayOutputStream crearZip(ArrayList<File> archs) {
		ZipOutputStream zip;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		zip = new ZipOutputStream(baos);
		try {
			for (File arch : archs) {
				InputStream is = new FileInputStream(arch);
				ZipEntry zipEntry = new ZipEntry(arch.getName());
				zip.putNextEntry(zipEntry);
				byte[] buffer = new byte[(int) (arch.length() % (5 * 1024 * 1024))];
				int byteCount;
				while (-1 != (byteCount = is.read(buffer))) {
					zip.write(buffer, 0, byteCount);
				}
				zip.closeEntry();
				is.close();
				arch.delete();
			}
			zip.close();
		} catch (IOException e) {
			logger.log(Log4jConstants.ERROR, "Error al generar adjunto ZIP.", e);
		}
		return baos;
	}

	private ArrayList<File> armarHTMLs(ArrayList<String> docs, ArrayList<Objetoi> objs) {
		// Variables
		String extension = ".html";
		String nombreArchivoConExtencion;
		ArrayList<File> archs = new ArrayList<File>();
		try {
			for (int i = 0; i < docs.size(); i++) {
				String nombreArchivo = "contratoSolicNro" + objs.get(i).getNumeroAtencion();
				nombreArchivoConExtencion = nombreArchivo + extension;
				File archivo = new File(nombreArchivoConExtencion);
				FileWriter fw = new FileWriter(archivo);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter salida = new PrintWriter(bw);
				// Estructura HTML
				salida.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
						+ "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
				salida.println("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\" dir=\"ltr\">");
				salida.println("<head>");
				salida.println("<title>Contrato Generado - " + objs.get(i).getPersona().getNomb12() + "</title>");
				salida.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">");
				salida.println("<head>");
				salida.println("<body>");
				salida.println(docs.get(i));
				salida.println("</body>");
				salida.println("</html>");
				salida.close();
				archs.add(archivo);
			}
		} catch (java.io.IOException ioex) {
			logger.log(Log4jConstants.ERROR, "Error al salida HTML.", ioex);
		}
		return archs;
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private String generarDocumento(Objetoi objetoi, Escrito escrito, ServletContext servletContext,
			HttpServletResponse response, BusinessPersistance bp, HttpServletRequest request,
			AsociarContratoExportForm asociarContratoExportForm) {
		String htmlDocumento;
		// Armo execution context y evaluo la expresion htmlDocumento tiene el codigo de
		// html de la expresion resuelta
		ContratoExecutionContext eC = new ContratoExecutionContext();
		ContratoExpressionEvaluator mE = new ContratoExpressionEvaluator();
		String fecha = request.getParameter("fechaCStr");
		eC.setObjetoi(objetoi);
		eC.setEscrito(escrito);
		eC.setFechaStr(fecha);
		htmlDocumento = (String) mE.evaluate(escrito.getBodyStr(), eC);
		htmlDocumento = htmlDocumento.replaceAll("&quot;", "\"");
		// armo el documento
		Documento documento = new Documento();
		documento.setNumero(escrito.getIdentificacion());
		documento.setFecha(new Date(System.currentTimeMillis()));
		documento.setEsEditable(false);
		ArrayList<Objetoi> objetosi = new ArrayList<Objetoi>();
		objetosi.add(objetoi);
		documento.setObjetosi(objetosi);
		documento.setTipoEscrito(escrito.getTipoEscrito());
		try {
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			BufferedOutputStream bufferOut = new BufferedOutputStream(byteOut);
			ObjectOutputStream objectOut = new ObjectOutputStream(bufferOut);
			objectOut.writeObject(htmlDocumento);
			objectOut.flush();
			byte[] bytes = byteOut.toByteArray();
			documento.setArchivo(bytes);
			objectOut.close();
		} catch (IOException e) {
			logger.log(Log4jConstants.ERROR, "Error al escribir documento HTML.", e);
		}
		if (!(objetoi.getLinea().getMoneda().getAbreviatura().contains("Peso"))) {
			List<Cotizacion> cotis = bp
					.createQuery("Select c from Cotizacion c where c.moneda = :moneda order by c.fechaDesde")
					.setParameter("moneda", objetoi.getLinea().getMoneda()).list();
			Cotizacion coti = new Cotizacion();
			if (cotis.size() != 0) {
				coti = cotis.get(0);
			}
			for (Cotizacion cotizacion : cotis) {
				if (cotizacion.getFechaHasta() == null) {
					coti = cotizacion;
				} else {
					if (coti.getFechaHasta() != null && coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
						coti = cotizacion;
					}
				}
			}
			objetoi.setCotizaContrato(coti.getVenta());
			bp.saveOrUpdate(objetoi);
		}
		String check = request.getParameter("chkDefinitivo");
		if (check != null && check.equals("checked")) {

			String usuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
			ObjetoiArchivo objetoiArchivo;
			FormFile formFile = asociarContratoExportForm.getFormFile();
			Claves currentUser = new Claves(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			TipoDeArchivo tipoDeArchivo = new TipoDeArchivo(asociarContratoExportForm.getIdTipoDeArchivo());
			String origen = SessionHandler.getCurrentSessionHandler().getIDMODULO();
			String observacionesEstado = "Contrato generado automaticamente - "
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser();
			String detalleArchivo = escrito.getTipoEscrito() + " " + escrito.getIdentificacion();
			objetoi.setEstadoActual("CONTRATO EMITIDO", observacionesEstado);
			// armo el archivo generado
			try {
				objetoiArchivo = new ObjetoiArchivo();
				objetoiArchivo.setCredito(objetoi);
				objetoiArchivo.setArchivo(formFile.getFileData());
				objetoiArchivo.setMimetype(formFile.getContentType());
				objetoiArchivo.setNombre(formFile.getFileName());
				objetoiArchivo.setFechaAdjunto(new Date());
				objetoiArchivo.setUsuario(usuario);
				objetoiArchivo.setDetalle(detalleArchivo);
				objetoiArchivo.setFuncionalidadOrigen("2");
				Persona titular = objetoi.getPersona();
				Integer paginas = PdfHelper.getPdfCantidadPaginas(formFile.getFileData());
				String descripcion = "Descripcion: " + StringHelper.StripHMTL(objetoiArchivo.getDetalle()) + ";"
						+ "Proyecto: " + objetoi.getNumeroAtencionStr() + ";" + "Titular: " + titular.getNomb12() + ";"
						+ "CUIT: " + titular.getCuil12Str();
				Archivo archivoRepo = new Archivo(formFile.getFileName(), formFile.getContentType(), tipoDeArchivo,
						currentUser, descripcion, origen, paginas);
				archivoRepo.setArchivo(formFile.getFileData());
				bp.save(archivoRepo);
				objetoiArchivo.setArchivoRepo(archivoRepo);
				bp.save(objetoiArchivo);
			} catch (IOException e) {
				logger.log(Log4jConstants.ERROR, "Error al vincular el contrato adjunto.", e);
			}
			bp.save(documento);
			objetoi.setFechaMutuoStr(fecha);
			objetoi.setFechaFirmaContratoStr(fecha);
			bp.saveOrUpdate(objetoi);
		}
		return htmlDocumento;
	}

	public void setLineaL(Long linea) {
		this.lineaL = linea;
	}

	public Long getLineaL() {
		return lineaL;
	}

}
