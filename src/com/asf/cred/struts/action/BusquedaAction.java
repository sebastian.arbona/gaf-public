/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asf.cred.struts.action;

import com.asf.cred.business.BusquedaBusiness;
import com.civitas.cred.expertos.BeanParametros;
import com.civitas.cred.expertos.IExperto;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author cnoguerol
 */
public class BusquedaAction extends DispatchAction {

    public void buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String buscarPor = request.getParameter("buscar");
        BeanParametros parametros = null;

        //Cdo se busca por dominio.
        String criterio = request.getParameter("criterio");

        //Cdo se busca por persona (conductores o testigos)
        if (criterio == null) {
            criterio = request.getParameter("criterio");
        }

        //Cdo se buscan calles.
        if (criterio == null) {
            criterio = request.getParameter("calle");
        }

        IExperto expertoBuscador = new BusquedaBusiness();
        parametros = new BeanParametros(buscarPor, criterio) {
        };

        if (buscarPor != null && criterio != null) {
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            response.setCharacterEncoding("ISO-8859-1");
            PrintWriter out = response.getWriter();
            String xml = (String) expertoBuscador.buscar(parametros);
            out.println(xml != null ? xml.replace("<null/>", "</>") : "");
            out.flush();
        }
    }
}
