package com.asf.cred.struts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.asf.cred.struts.form.AbmForm;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.AbmAction;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Requisito;
import com.nirven.creditos.hibernate.RequisitoDesembolso;

public class DesembolsoAction extends AbmAction {
	@Override
	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		AbmForm abmForm = (AbmForm) form;
		Desembolso desembolso = (Desembolso) abmForm.getEntity();
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, desembolso.getCredito_id());

		boolean guardar = true;
		if (desembolso.getId()==null&&credito.getLinea() != null && credito.getLinea().getMonto() != 0.0) {
			Double sumFinanciamiento = (Double) bp.createSQLQuery("SELECT SUM(FINANCIAMIENTO) FROM Objetoi O "
					+ "INNER JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id "
					+ "INNER JOIN Estado E ON OE.estado_idEstado = E.idEstado AND E.nombreEstado NOT IN ('ESPERANDO DOCUMENTACION') "
					+ "WHERE linea_id = :linea_id AND OE.fechaHasta IS NULL AND o.id <> :objetoi_id")
					.setLong("linea_id", credito.getLinea().getId()).setLong("objetoi_id", credito.getId())
					.uniqueResult();
			if (sumFinanciamiento == null) {
				sumFinanciamiento = 0.0;
			}
			if ((sumFinanciamiento + desembolso.getImporte() + desembolso.getFinanciamientoSolicitado()) > credito
					.getLinea().getMonto()) {
				ActionMessages actionMessages = new ActionMessages();
				actionMessages.add("Solicitud.error.financiamiento",
						new ActionMessage("Solicitud.error.financiamiento"));
				addErrors(request, actionMessages);
				guardar = false;
			}
		}
		if (desembolso.getId()==null&&credito.getLinea() != null && credito.getLinea().getMontoObjetoi() != null
				&& credito.getLinea().getMontoObjetoi() != 0.0) {
			if ((desembolso.getImporte() + desembolso.getFinanciamientoSolicitado()) > credito.getLinea()
					.getMontoObjetoi()) {
				ActionMessages actionMessages = new ActionMessages();
				actionMessages.add("Solicitud.error.montoObjetoi", new ActionMessage("Solicitud.error.montoObjetoi"));
				addErrors(request, actionMessages);
				guardar = false;
			}
		}
		if (guardar) {
			if(desembolso.getId()==null) {
				desembolso.setEstado("4");
				desembolso.setImporteSolicitado(desembolso.getImporte());
			}
			abmForm.setEntity(desembolso);
			super.save(mapping, abmForm, request, response);

			@SuppressWarnings("unused")
			Desembolso desembolso1 = (Desembolso) abmForm.getEntity();

			if (credito.getEstadoActual().getEstado().getNombreEstado().equals(Estado.ANALISIS)) {
				Double financiamiento = desembolso.getFinanciamientoSolicitado();
				credito.setFinanciamiento(financiamiento);
			}
			
			credito.setExpedientePago(desembolso.getExpedientePago());
			bp.saveOrUpdate(credito);

			@SuppressWarnings("unchecked")
			ArrayList<Requisito> requisitoList = (ArrayList<Requisito>) bp.getByFilter(Requisito.class, "tipo = 'R'");

			for (Requisito requisito : requisitoList) {
				RequisitoDesembolso rDesembolso = new RequisitoDesembolso();
				rDesembolso.setRequisito(requisito);
				rDesembolso.setDesembolso(desembolso);
				bp.save(rDesembolso);
			}
			request.setAttribute("redirectDesembolso", "true");
		}
		request.setAttribute("idObjetoi", ((Desembolso) ((AbmForm) form).getEntity()).getCredito_id());
		return mapping.findForward("CreditoDesembolso");
	}

	@Override
	protected ActionForward cancelled(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		super.cancelled(mapping, form, request, response);

		request.setAttribute("redirectDesembolso", "true");
		request.setAttribute("idObjetoi", ((Desembolso) ((AbmForm) form).getEntity()).getCredito_id());

		return mapping.findForward("CreditoDesembolso");
	}

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idDesembolso = ((Desembolso) ((AbmForm) form).getEntity()).getId();
		Desembolso d = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		Long idCredito = d.getCredito_id();
		boolean eliminar = true;
		if (d.getEstado() != null && !d.getEstado().equals(Desembolso.NUEVO)) {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("desembolso.eliminar.estado", new ActionMessage("desembolso.eliminar.estado"));
			saveErrors(request, actionMessages);
			eliminar = false;
		} else if (d.getEstado() == null) {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("desembolso.eliminar.estadoNulo", new ActionMessage("desembolso.eliminar.estadoNulo"));
			saveErrors(request, actionMessages);
			eliminar = false;
		}
		if (eliminar) {
			
			bp.createQuery("delete FROM RequisitoDesembolso r WHERE r.desembolso = :desembolso").setParameter("desembolso", d).executeUpdate();
			super.delete(mapping, form, request, response);
			// corregir numeracion para mantener correlativa
			List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
					.setParameter("idCredito", idCredito).list();
			int numero = 1;
			for (Desembolso desembolso : desembolsos) {
				desembolso.setNumero(numero++);
				bp.update(desembolso);
			}
			request.setAttribute("redirectDesembolso", "true");
		}
		request.setAttribute("idObjetoi", idCredito);
		return mapping.findForward("CreditoDesembolso");
	}

	@Override
	public ActionForward newEntity(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		AbmForm abmForm=(AbmForm)form;
		Desembolso d=(Desembolso)abmForm.getEntity();		
		
		
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, new Long(request.getParameter("paramValue[0]").toString()));
		
		d.setExpedientePago(credito.getExpedientePago());
		
		
		
		return super.newEntity(mapping, form, request, response);
	}
}
