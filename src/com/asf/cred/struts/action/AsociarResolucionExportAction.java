package com.asf.cred.struts.action;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;
import org.hibernate.Query;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.AsociarResolucionExportForm;
import com.asf.gaf.hibernate.Cotizacion;
import com.asf.hibernate.mapping.Claves;
import com.asf.security.BusinessPersistance;
import com.asf.util.DirectorHelper;
import com.asf.util.StringHelper;
import com.civitas.el.especific.ResolucionExecutionContext;
import com.civitas.el.especific.ResolucionExpressionEvaluator;
import com.civitas.hibernate.persona.Persona;
import com.civitas.logger.Log4jConstants;
import com.civitas.logger.LoggerService;
import com.civitas.util.PdfHelper;
import com.nirven.creditos.bpm.ImputacionesHelper;
import com.nirven.creditos.hibernate.ConvenioBonificacion;
import com.nirven.creditos.hibernate.Documento;
import com.nirven.creditos.hibernate.Escrito;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.expedientes.persistencia.Archivo;
import com.nirven.expedientes.persistencia.TipoDeArchivo;

public class AsociarResolucionExportAction extends DispatchAction {
	public Long lineaL = new Long(0);
	LoggerService logger = LoggerService.getInstance();

	@SuppressWarnings("unchecked")
	public ActionForward listar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AsociarResolucionExportForm asociarResolucionForm = (AsociarResolucionExportForm) form;
		setLineaL(asociarResolucionForm.getLinea());
		if ((getLineaL() != null && getLineaL() != 0L) || (asociarResolucionForm.getResolucion() != null
				&& !asociarResolucionForm.getResolucion().trim().isEmpty())) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			String consulta = "SELECT DISTINCT c.objetoi FROM ObjetoiEstado c WHERE c.estado.nombreEstado LIKE :estado "
					+ " AND c.fecha = (SELECT max(e.fecha) FROM ObjetoiEstado e WHERE e.objetoi.id = c.objetoi.id) AND c.fechaHasta IS NULL";
			if (getLineaL() != null && getLineaL() != 0L) {
				consulta += " AND c.objetoi.linea.id = :linea ";
			}
			if (asociarResolucionForm.getResolucion() != null
					&& !asociarResolucionForm.getResolucion().trim().isEmpty()) {
				consulta += "AND c.objetoi.resolucion = :resolucion ";
			}
			Query query = bp.createQuery(consulta);
			query.setString("estado", "PENDIENTE RESOLUCION");
			if (getLineaL() != null && getLineaL().longValue() != 0L) {
				query.setLong("linea", getLineaL());
			}
			if (asociarResolucionForm.getResolucion() != null
					&& !asociarResolucionForm.getResolucion().trim().isEmpty()) {
				query.setString("resolucion", asociarResolucionForm.getResolucion());
			}
			List<Objetoi> result = query.list();
			asociarResolucionForm.setResult(result);
			asociarResolucionForm.setLinea(lineaL);
		}
		if (asociarResolucionForm.getObjetoiId() != null && asociarResolucionForm.getObjetoiId().longValue() > 0) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			List<Objetoi> result = (List<Objetoi>) bp.createQuery(
					"SELECT DISTINCT c.objetoi FROM ObjetoiEstado c WHERE c.estado.nombreEstado LIKE :estado AND c.objetoi.id = :id AND c.fecha = "
							+ "(SELECT max(e.fecha) FROM ObjetoiEstado e WHERE e.objetoi.id = c.objetoi.id) AND c.fechaHasta IS NULL")
					.setLong("id", asociarResolucionForm.getObjetoiId()).setString("estado", "PENDIENTE RESOLUCION")
					.list();
			asociarResolucionForm.setResult(result);
			return mapping.findForward("AsociarResolucionListFrame");
		}
		asociarResolucionForm.setOcultarGenerar(false);
		return mapping.findForward("AsociarResolucionList");
	}

	@SuppressWarnings("unchecked")
	public ActionForward generar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AsociarResolucionExportForm as = (AsociarResolucionExportForm) form;
		if (as.getEscrito() == null) {
			return mapping.findForward("AsociarResolucionList");
		} else {
			AsociarResolucionExportForm asociarResolucionForm = (AsociarResolucionExportForm) form;
			Long escritoid = asociarResolucionForm.getEscrito();
			String check = (String) request.getParameter("chkDefinitivo");
			if (check != null && check.equals("checked")) {
				String numero = (String) request.getParameter("nroResolucion");
				if (numero == null || numero.trim().isEmpty()) {
					ActionMessages mensajes = new ActionMessages();
					mensajes.add("error", new ActionMessage("asociarResolucion.numero", "asociarResolucion.numero"));
					saveErrors(request, mensajes);
					return null;
				}
			}
			Enumeration<String> paramNames = request.getParameterNames();
			String ids = "";
			while (paramNames.hasMoreElements()) {
				String param = paramNames.nextElement();
				if (param.startsWith("seleccion-") && request.getParameter(param) != null
						&& !request.getParameter(param).equals("")) {
					String[] p = param.split("-");
					ids += p[1] + ",";
				}
			}
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			List<Objetoi> objetosi;
			if (!ids.isEmpty()) {
				objetosi = (List<Objetoi>) bp.getByFilter(
						"SELECT o FROM Objetoi o WHERE o.id IN (" + ids.substring(0, ids.length() - 1) + ")");
			} else {
				return mapping.findForward("AsociarResolucionList");
			}
			Escrito escrito = (Escrito) bp.getById(Escrito.class.getName(), escritoid);
			// genero el documento y lo guardo
			// Hago la imputación, si está ok, sigo
			if (check != null && check.equals("checked")) {
				try {
					this.generarImputacionesDefinitivas(objetosi);
				} catch (Exception e) {
					e.printStackTrace();
					ActionMessages mensajes = new ActionMessages();
					mensajes.add("error", new ActionMessage("asociarResolucion.error.imputacion",
							"asociarResolucion.error.imputacion"));
					saveErrors(request, mensajes);
					return mapping.findForward("AsociarResolucionList");
				}
			}
			bp.begin();
			String htmlDocumento = "";
			try {
				htmlDocumento = generarDocumento(objetosi, escrito, request.getSession().getServletContext(), response,
						bp, request, asociarResolucionForm);
				asociarResolucionForm.setOcultarGenerar(true);
				asociarResolucionForm.setScript("<script>bajarDocumento();</script>");
				asociarResolucionForm.setHtmlDocumento(htmlDocumento);
				bp.commit();
			} catch (Exception e) {
				bp.rollback();
				ActionMessages mensajes = new ActionMessages();
				mensajes.add("error", new ActionMessage("asociarResolucion.error", "asociarResolucion.error"));
				saveErrors(request, mensajes);
				e.printStackTrace();
			}
			return mapping.findForward("AsociarResolucionList");
		}
	}

	private void generarImputacionesDefinitivas(List<Objetoi> objetosi) throws Exception {
		ImputacionesHelper ih = new ImputacionesHelper();
		ih.setEtapa(2l);
		ih.setTipoExpediente(15l);
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		for (Objetoi o : objetosi) {
			ih.imputar(o);
			try {
				ConvenioBonificacion convenio = (ConvenioBonificacion) bp.createQuery(
						"select c from ObjetoiBonificacion o join o.bonificacion b join b.convenio c where o.objetoi.id = :idObjetoi")
						.setParameter("idObjetoi", o.getId()).setMaxResults(1).uniqueResult();
				if (convenio != null) {
					double importeBonificaciones = o.getBonificacion() != null ? o.getBonificacion() : 0;
					if (importeBonificaciones >= 0.01) {
						String expediente = DirectorHelper.getString("imputacion.bonif.expediente");
						if (expediente != null) {
							Calendar c = Calendar.getInstance();
							c.setTime(o.getFechaSolicitud());
							int periodo = c.get(Calendar.YEAR);
							Number idIEgreso = (Number) bp.createSQLQuery(
									"select idiegreso from iegreso where codigo = :codigo and ejercicio = :ejercicio")
									.setParameter("codigo", convenio.getCodigoIegreso())
									.setParameter("ejercicio", periodo).setMaxResults(1).uniqueResult();
							Number idInstitucional = (Number) bp.createSQLQuery(
									"select idinstitucional from institucional where codigo = :codigo and ejercicio = :ejercicio")
									.setParameter("codigo", convenio.getCodigoInstitucional())
									.setParameter("ejercicio", periodo).setMaxResults(1).uniqueResult();
							Number idNomenclador = (Number) bp.createSQLQuery(
									"select idnomenclador from nomenclador where codigo = :codigo and ejercicio = :ejercicio")
									.setParameter("codigo", convenio.getCodigoNomenclador())
									.setParameter("ejercicio", periodo).setMaxResults(1).uniqueResult();
							if (idIEgreso != null && idInstitucional != null && idNomenclador != null) {
								log.debug("Llamo a imputacion bonificaciones WS");
								ih.imputar(2l, 2l, 15l, o.getId(), expediente, importeBonificaciones,
										idIEgreso.longValue(), idInstitucional.longValue(), idNomenclador.longValue());
								log.debug("Imputacion bonificaciones WS");
							}
						}
					}
				}
			} catch (Exception e) {
				logger.log(Log4jConstants.ERROR, "Error al imputar bonificaciones en GAF.");
			}
		}
	}

	@SuppressWarnings({ "static-access", "unchecked" })
	private String generarDocumento(List<Objetoi> objetosi, Escrito escrito, ServletContext servletContext,
			HttpServletResponse response, BusinessPersistance bp, HttpServletRequest request,
			AsociarResolucionExportForm asociarResolucionExportForm) {
		String htmlDocumento;
		// Armo execution context y evaluo la expresion
		// htmlDocumento tiene el codigo de html de la expresion resuelta
		ResolucionExecutionContext eC = new ResolucionExecutionContext();
		ResolucionExpressionEvaluator mE = new ResolucionExpressionEvaluator();
		String fecha = request.getParameter("fechaRStr");
		String numero = request.getParameter("nroResolucion");

		eC.setObjetosi(new ArrayList<Objetoi>(objetosi));
		eC.setEscrito(escrito);
		eC.setFechaStr(fecha);
		eC.setResolucion(numero);

		htmlDocumento = (String) mE.evaluate(escrito.getBodyStr(), eC);
		htmlDocumento = htmlDocumento.replaceAll("&quot;", "\"");
		// armo documento
		Documento documento = new Documento();
		documento.setNumero(escrito.getIdentificacion());
		documento.setFecha(new Date(System.currentTimeMillis()));
		documento.setEsEditable(false);
		documento.setObjetosi(objetosi);
		documento.setTipoEscrito(escrito.getTipoEscrito());
		try {
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			BufferedOutputStream bufferOut = new BufferedOutputStream(byteOut);
			ObjectOutputStream objectOut = new ObjectOutputStream(bufferOut);
			objectOut.writeObject(htmlDocumento);
			objectOut.flush();
			byte[] bytes = byteOut.toByteArray();
			documento.setArchivo(bytes);
			objectOut.close();
		} catch (IOException e) {
			logger.log(Log4jConstants.ERROR, "Error al escribir la resolucion HTML.");
		}
		String check = (String) request.getParameter("chkDefinitivo");
		if (check != null && check.equals("checked")) {
			String usuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
			ObjetoiArchivo objetoiArchivo;
			FormFile formFile = asociarResolucionExportForm.getFormFile();
			Claves currentUser = new Claves(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			TipoDeArchivo tipoDeArchivo = new TipoDeArchivo(asociarResolucionExportForm.getIdTipoDeArchivo());
			String origen = SessionHandler.getCurrentSessionHandler().getIDMODULO();
			String observacionesEstado = "Resolución generada automáticamente - "
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser();
			String detalleArchivo = escrito.getTipoEscrito() + " " + escrito.getIdentificacion() + " " + numero;
			for (Objetoi oi : objetosi) {
				oi.setEstadoActual("PENDIENTE CONTRATO", observacionesEstado);
				// Busca la cotizacion si la moneda de la linea es != a Peso
				if (!(oi.getLinea().getMoneda().getAbreviatura().contains("Peso"))) {
					List<Cotizacion> cotis = bp
							.createQuery("Select c from Cotizacion c where c.moneda = :moneda order by c.fechaDesde")
							.setParameter("moneda", oi.getLinea().getMoneda()).list();
					Cotizacion coti = new Cotizacion();
					if (cotis.size() != 0) {
						coti = cotis.get(0);
					}
					for (Cotizacion cotizacion : cotis) {
						if (cotizacion.getFechaHasta() == null) {
							coti = cotizacion;
						} else {
							if (coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
								coti = cotizacion;
							}
						}
					}
					oi.setCotizaResol(coti.getVenta());
				}
				oi.setFechaResolucionStr(fecha);
				oi.setResolucion(numero);
				bp.saveOrUpdate(oi);
				try {
					objetoiArchivo = new ObjetoiArchivo();
					objetoiArchivo.setCredito(oi);
					objetoiArchivo.setArchivo(formFile.getFileData());
					objetoiArchivo.setMimetype(formFile.getContentType());
					objetoiArchivo.setNombre(formFile.getFileName());
					objetoiArchivo.setFechaAdjunto(new Date());
					objetoiArchivo.setUsuario(usuario);
					objetoiArchivo.setDetalle(detalleArchivo);
					objetoiArchivo.setFuncionalidadOrigen("2");
					Persona titular = oi.getPersona();
					Integer paginas = PdfHelper.getPdfCantidadPaginas(formFile.getFileData());
					String descripcion = "Descripcion: " + StringHelper.StripHMTL(objetoiArchivo.getDetalle()) + ";"
							+ "Proyecto: " + oi.getNumeroAtencionStr() + ";" + "Titular: " + titular.getNomb12() + ";"
							+ "CUIT: " + titular.getCuil12Str();
					Archivo archivoRepo = new Archivo(formFile.getFileName(), formFile.getContentType(), tipoDeArchivo,
							currentUser, descripcion, origen, paginas);
					archivoRepo.setArchivo(formFile.getFileData());
					bp.save(archivoRepo);
					objetoiArchivo.setArchivoRepo(archivoRepo);
					bp.save(objetoiArchivo);
				} catch (IOException e) {
					logger.log(Log4jConstants.ERROR, "Error al guardar el contrato adjunto.");
				}

			}
			bp.save(documento);
		} else {
			for (Objetoi objetoi : objetosi) {
				objetoi.setResolucion(numero);
				bp.saveOrUpdate(objetoi);
			}
		}
		return htmlDocumento;
	}

	public ActionForward bajarDocumento(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		AsociarResolucionExportForm asociarResolucionForm = (AsociarResolucionExportForm) form;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Escrito escrito = (Escrito) bp.getById(Escrito.class.getName(), asociarResolucionForm.getEscrito());
		response.setContentType("text/html");
		response.addHeader("Content-Disposition",
				"attachment; filename=resolucion" + escrito.getIdentificacion() + ".html");
		response.getWriter().write(asociarResolucionForm.getHtmlDocumento());
		response.getWriter().flush();
		response.getWriter().close();
		return null;
	}

	public void setLineaL(Long linea) {
		this.lineaL = linea;
	}

	public Long getLineaL() {
		return lineaL;
	}

}
