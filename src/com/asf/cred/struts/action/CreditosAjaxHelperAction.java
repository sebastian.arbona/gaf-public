package com.asf.cred.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;

import com.asf.cred.business.BoletoPagoHandler;
import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.cred.business.CreditoHandler;
import com.asf.cred.business.ProgressStatus;
import com.asf.gaf.TipificadorHandler;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Localidad;
import com.civitas.hibernate.persona.Persona;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nirven.creditos.hibernate.ActividadAfip;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Bonificacion;
import com.nirven.creditos.hibernate.CConcepto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.ConceptoIngresosVarios;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.DetalleVinedoINV;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.LocalidadDepartamento;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.PeriodoCosechaConfig;
import com.nirven.creditos.hibernate.Requisito;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.creditos.hibernate.TipoProceso;

/**
 * 
 * @author eselman
 */
public class CreditosAjaxHelperAction extends DispatchAction {

	private static final DecimalFormat NUMBERFORMAT = new DecimalFormat("#,##0.00;(-#,##0.00)");

	public CreditosAjaxHelperAction() {
	}

	/**
	 * 
	 * Busca las personas, por nombre, apellido o nro de documento.-
	 * 
	 */
	@SuppressWarnings("unchecked")
	public ActionForward buscarPersona(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String xml = "";
		String personaIngresada = request.getParameter("persona");
		Long nroDoc = null;
		try {
			nroDoc = new Long(personaIngresada);
		} catch (NumberFormatException ne) {
			nroDoc = null;
		}
		String consulta = "SELECT p FROM Persona p WHERE p.fechaBaja is null ";
		// Si el dato ingresado es un numero, busco por numero de documento o CUIT.-
		if (nroDoc != null) {
			consulta += " AND p.nudo12 LIKE '" + nroDoc + "%' OR p.cuil12 LIKE '" + nroDoc + "%'";
			consulta += " ORDER BY p.nudo12";
		}
		// Sino busco por nombre u Apellido.-
		else {
			consulta += "AND p.nomb12 LIKE '" + personaIngresada + "%'";
			consulta += " ORDER BY p.nomb12";
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Persona> personas = bp.getByFilter(consulta);
		xml += "<ul>";
		if (personas.size() > 0) {
			for (Persona persona : personas) {
				xml += "<li>";
				xml += "<id>";
				xml += persona.getIdpersona() + "-";
				xml += "</id>";
				xml += "<nombre>";
				xml += persona.getNomb12() + "-";
				xml += "</nombre>";
				xml += "<nroDoc>";
				xml += persona.getNudo12Str();
				xml += "</nroDoc>";
				xml += "</li>";
			}
		} else {
			xml += "<li>";
			xml += "No se encontraron coincidencias";
			xml += "</li>";
		}
		xml += "</ul>";
		this.escribir(response, xml);
		return null;
	}// fin buscarPersona.-

	/**
	 * 
	 * Busca las localidades por Provincia.-
	 * 
	 */
	@SuppressWarnings("unchecked")
	public ActionForward buscarLocalidades(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// atributos.-
		String xml = "";
		String localidadStr = null;
		Long idprovincia = null;
		try {
			idprovincia = new Long(request.getParameter("idprovincia"));
			localidadStr = request.getParameter("idlocalidad");
		} catch (NumberFormatException ne) {
			idprovincia = null;
			localidadStr = null;
		}
		String consulta = "SELECT l FROM Localidad l WHERE 1=1";
		consulta += idprovincia != null ? " AND l.codi08=" + idprovincia.longValue() : "";
		consulta += localidadStr != null
				? " AND (UPPER( l.nombre ) like '" + localidadStr.toUpperCase() + "%' OR UPPER( l.cp ) like '"
						+ localidadStr.toUpperCase() + "%')"
				: "";
		consulta += " ORDER BY l.cp";
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Localidad> localidades = bp.getByFilter(consulta);
		xml += "<ul>";
		if (localidades.size() > 0) {
			for (Localidad localidad : localidades) {
				xml += "<li>";
				xml += "<id>";
				xml += localidad.getIdlocalidad() + "-";
				xml += "</id>";
				xml += "<cp>";
				xml += localidad.getCp() + "-";
				xml += "</cp>";
				xml += "<localidad>";
				xml += localidad.getNombre();
				xml += "</localidad>";
				xml += "</li>";
			}
		} else {
			xml += "<li>";
			xml += "No se encontraron coincidencias";
			xml += "</li>";
		}
		xml += "</ul>";
		this.escribir(response, xml);
		return null;
	}// fin buscarPersona.-

	public ActionForward buscarValorIndicePromedio(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			Long idIndice = Long.parseLong(request.getParameter("idIndice"));
			double resultado = new CreditoHandler().buscarValorIndicePromedio(idIndice) * 100;
			response.getWriter().print(String.format("%3.2f%%", resultado));
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward buscarValorIndice(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			Long idIndice = Long.parseLong(request.getParameter("idIndice"));
			double resultado = new CreditoHandler().buscarValorIndice(idIndice) * 100;
			response.getWriter().print(String.format("%3.2f%%", resultado));
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward calcularValorFinal(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String tasaStr = request.getParameter("tasa");
			String masStr = request.getParameter("mas");
			String porStr = request.getParameter("por");
			String topeTasaStr = request.getParameter("topeTasa");
			double tasa = Double.parseDouble(tasaStr == null ? "0" : tasaStr.replaceAll(",", ".").replaceAll("%", ""));
			double mas = Double.parseDouble(masStr == null ? "0" : masStr.replaceAll(",", ".").replaceAll("%", ""));
			double por = Double.parseDouble(porStr == null ? "1" : porStr.replaceAll(",", ".").replaceAll("%", ""));
			double topeTasa = Double
					.parseDouble(topeTasaStr == null ? "0" : topeTasaStr.replaceAll(",", ".").replaceAll("%", ""));
			double resultado = (tasa + mas) * por;
			resultado = (topeTasa > 0d) ? Math.min(topeTasa, resultado) : resultado;
			response.getWriter().print(String.format("%.2f%%", resultado));
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward calcularValorFinalPromedio(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			Long idIndice = Long.parseLong(request.getParameter("idIndice"));
			String tasaStr = request.getParameter("tasa");
			String masStr = request.getParameter("mas");
			String porStr = request.getParameter("por");
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Double valorMaximo = ((Indice) bp.getById(Indice.class, idIndice)).getValorMaximo();
			valorMaximo = valorMaximo == null ? 0d : valorMaximo;
			double tasa = Double.parseDouble(tasaStr.replaceAll(",", ".").replaceAll("%", ""));
			double mas = Double.parseDouble(masStr.replaceAll(",", ".").replaceAll("%", ""));
			double por = Double.parseDouble(porStr.replaceAll(",", ".").replaceAll("%", ""));
			double resultado = (tasa + mas) * por;
			if (resultado > valorMaximo * 100) {
				resultado = valorMaximo * 100;
			}
			response.getWriter().print(String.format("%.2f%%", resultado));
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward buscarPersona2(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String personaid = request.getParameter("personaid");
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Persona persona = (Persona) bp.createQuery("select p from Persona p where p.id = :id")
					.setString("id", personaid).uniqueResult();
			if (persona != null) {
				String resultado = persona.getNomb12().toUpperCase() + "#" + persona.getCuil12Str() + "#"
						+ persona.getNudo12Str();
				response.getWriter().print(resultado);
				response.getWriter().flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward calcularTasaNeta(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idCredito = new Long(request.getParameter("idCredito"));
		CreditoHandler handler = new CreditoHandler();
		// tomo la inicial del credito = primera cuota
		double[] arreglo = handler.calcularTasaNeta(idCredito);
		double tasaNeta = (arreglo.length == 0 ? 0 : arreglo[0]);
		try {
			response.getWriter().print(String.format("%.2f", tasaNeta));
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward calcularBonificacionEstimada(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		Long idCredito = new Long(request.getParameter("idCredito"));
		CreditoHandler handler = new CreditoHandler();
		Double bonificacionEstimada = 0D;
		bonificacionEstimada = new Double(handler.calcularBonificacionEstimada(idCredito));
		if (bonificacionEstimada.isNaN()) {
			bonificacionEstimada = 0D;
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		credito.setBonificacion(bonificacionEstimada);
		bp.saveOrUpdate(credito);
		try {
			response.getWriter().print(String.format("$ %.2f", bonificacionEstimada));
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward buscarDescripcionRequisito(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idRequisito = new Long(request.getParameter("idReq"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Requisito requisito = (Requisito) bp.getById(Requisito.class, idRequisito);
		if (requisito != null) {
			try {
				response.getWriter().print(requisito.getDescripcion());
				response.getWriter().flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * A partir del monto a acreditar determina los montos y conceptos a aplicar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward consultarPreaplicacion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		synchronized (this) {
			Long nroAtencion = new Long(request.getParameter("nroProyecto"));
			double importe = new Double(request.getParameter("importe")).doubleValue();
			String fechaCobranza = request.getParameter("fechaCobranza");
			String fechaCalculo = request.getParameter("fechaCalculo");
			Long idMoneda = 1L;
			try {
				idMoneda = new Long(request.getParameter("idMoneda"));
			} catch (Exception e) {
			}
			Date hastaFecha = new Date();
			if (fechaCalculo != null) {
				hastaFecha = DateHelper.getDate(fechaCalculo);
			}
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			List<Objetoi> lista = bp.getByFilter("select o from Objetoi o where o.numeroAtencion = " + nroAtencion);
			if (lista.isEmpty()) {
				return null;
			}
			Objetoi credito = lista.get(0);
			CreditoCalculoDeuda calculo = new CreditoCalculoDeuda();
			calculo.setHastaFecha(hastaFecha);
			calculo.setIdObjetoi(credito.getId());
			calculo.setConsiderarLiquidacion(true);
			calculo.calcular();
			Long idMonedaCredito = credito.getMoneda_id() != null ? credito.getMoneda_id()
					: credito.getLinea().getMoneda_id();
			if (idMonedaCredito == null) {
				idMonedaCredito = 1L;
			}
			CreditoHandler ch = new CreditoHandler();
			double cotizacion = ch.getCotizacion(idMoneda, idMonedaCredito, hastaFecha);
			double importeConvertido = importe;
			if (idMoneda != null && !idMoneda.equals(idMonedaCredito)) {
				importeConvertido = importeConvertido / cotizacion;
			}
			String xml = "<Objects>";
			xml += generarXmlPreaplicacion(calculo, credito, importeConvertido, fechaCobranza, null, null, null);
			xml += "</Objects>";
			escribir(response, xml);
			return null;
		}
	}

	private String generarXmlPreaplicacion(CreditoCalculoDeuda calculo, Objetoi credito, double importe,
			String fechaCobranza, String idDetalleRec, String idDetallePadre, Long idBoleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Query query = bp.createSQLQuery("select estado from Cuota c where c.numero = :numero and c.credito_id = :id")
				.setLong("id", credito.getId());
		String xml = "<Objetoi>";
		xml += "<Persona>";
		xml += "<nombre>"
				+ (credito.getPersona().getNomb12() != null ? credito.getPersona().getNomb12().replace("&", "&amp;")
						: "")
				+ "</nombre>";
		xml += "<cuil>" + credito.getPersona().getCuil12() + "</cuil>";
		xml += "</Persona>";
		xml += "<numeroAtencion>" + credito.getNumeroAtencion() + "</numeroAtencion>";
		xml += "<importePago>" + NUMBERFORMAT.format(importe) + "</importePago>";
		xml += "<importeRaw>" + importe + "</importeRaw>";
		xml += "<fechaPago>" + fechaCobranza + "</fechaPago>";
		if (idDetalleRec != null) {
			xml += "<idDetalleRecaudacion>" + idDetalleRec + "</idDetalleRecaudacion>";
		}
		if (idDetallePadre != null) {
			xml += "<idDetallePadre>" + idDetallePadre + "</idDetallePadre>";
		}
		if (idBoleto != null) {
			xml += "<idBoleto>" + idBoleto + "</idBoleto>";
		}
		double saldoPendiente = importe;
		boolean hayCuotas = false;

		for (int cuota = 1; cuota <= credito.getPlazoCompensatorio(); cuota++) {
			String estado = (String) query.setInteger("numero", cuota).setMaxResults(1).uniqueResult();

			if (estado == null || estado.equals("2")) {
				continue;
			}
			double capital = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_CAPITAL, cuota);
			double compensatorio = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_COMPENSATORIO, cuota);
			double bonificaciones = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_BONIFICACION, cuota);
			compensatorio -= bonificaciones;
			double moratorio = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_MORATORIO, cuota);
			double punitorio = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_PUNITORIO, cuota);
			double gastos = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_GASTOS, cuota);
			double gastosRec = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, cuota);
			double multas = calculo.getSaldoCuotaConcepto(Concepto.CONCEPTO_MULTAS, cuota);

			double aplicado = 0;
			if (saldoPendiente - gastosRec >= 0.01) {
				saldoPendiente -= gastosRec;
			} else {
				gastosRec = saldoPendiente;
				saldoPendiente = 0;
			}
			if (saldoPendiente - gastos >= 0.01) {
				saldoPendiente -= gastos;
			} else {
				gastos = saldoPendiente;
				saldoPendiente = 0;
			}
			if (saldoPendiente - multas >= 0.01) {
				saldoPendiente -= multas;
			} else {
				multas = saldoPendiente;
				saldoPendiente = 0;
			}
			if (saldoPendiente - punitorio >= 0.01) {
				saldoPendiente -= punitorio;
			} else {
				punitorio = saldoPendiente;
				saldoPendiente = 0;
			}
			if (saldoPendiente - moratorio >= 0.01) {
				saldoPendiente -= moratorio;
			} else {
				moratorio = saldoPendiente;
				saldoPendiente = 0;
			}
			if (saldoPendiente - compensatorio >= 0.01) {
				saldoPendiente -= compensatorio;
			} else {
				compensatorio = saldoPendiente;
				saldoPendiente = 0;
			}
			if (saldoPendiente - capital >= 0.01) {
				saldoPendiente -= capital;
			} else {
				capital = saldoPendiente;
				saldoPendiente = 0;
			}

			aplicado = capital + compensatorio + moratorio + punitorio + gastos + gastosRec + multas;
			if (aplicado >= 0.01) {
				hayCuotas = true;
				xml += "<Cuota>";
				xml += "<numero>" + cuota + "</numero>";
				xml += "<aplicado>" + NUMBERFORMAT.format(aplicado) + "</aplicado>";
				xml += "<capital>" + NUMBERFORMAT.format(capital) + "</capital>";
				xml += "<compensatorio>" + NUMBERFORMAT.format(compensatorio) + "</compensatorio>";
				xml += "<moratorio>" + NUMBERFORMAT.format(moratorio) + "</moratorio>";
				xml += "<punitorio>" + NUMBERFORMAT.format(punitorio) + "</punitorio>";
				xml += "<gastos>" + NUMBERFORMAT.format(gastos) + "</gastos>";
				xml += "<gastosRec>" + NUMBERFORMAT.format(gastosRec) + "</gastosRec>";
				xml += "<multas>" + NUMBERFORMAT.format(multas) + "</multas>";
				xml += "<aplicadoRaw>" + aplicado + "</aplicadoRaw>";
				xml += "<capitalRaw>" + capital + "</capitalRaw>";
				xml += "<compensatorioRaw>" + compensatorio + "</compensatorioRaw>";
				xml += "<moratorioRaw>" + moratorio + "</moratorioRaw>";
				xml += "<punitorioRaw>" + punitorio + "</punitorioRaw>";
				xml += "<gastosRaw>" + gastos + "</gastosRaw>";
				xml += "<gastosRecRaw>" + gastosRec + "</gastosRecRaw>";
				xml += "<multasRaw>" + multas + "</multasRaw>";
				xml += "<saldoPendiente>";
				/**
				 * se acabo el importe
				 */
				if (saldoPendiente < 0.01) {
					xml += "0,00";
				}
				/**
				 * se acabaron las cuotas
				 */
				else if (cuota == credito.getPlazoCompensatorio()) {
					xml += NUMBERFORMAT.format(saldoPendiente);
				}
				xml += "</saldoPendiente>";
				xml += "<saldoPendienteRaw>";
				if (saldoPendiente < 0.01) {
					xml += "0";
				} else if (cuota == credito.getPlazoCompensatorio()) {
					xml += saldoPendiente;
				}
				xml += "</saldoPendienteRaw>";
				xml += "</Cuota>";
			}

			/**
			 * se acabo el importe a aplicar
			 */
			if (saldoPendiente < 0.01) {
				break;
			}
		}
		if (!hayCuotas) {
			xml += "<Cuota>";
			xml += "<numero/>";
			xml += "<aplicado/>";
			xml += "<capital/>";
			xml += "<compensatorio/>";
			xml += "<moratorio/>";
			xml += "<punitorio/>";
			xml += "<gastos/>";
			xml += "<gastosRec/>";
			xml += "<multas/>";
			xml += "<aplicadoRaw/>";
			xml += "<capitalRaw/>";
			xml += "<compensatorioRaw/>";
			xml += "<moratorioRaw/>";
			xml += "<punitorioRaw/>";
			xml += "<gastosRaw/>";
			xml += "<gastosRecRaw/>";
			xml += "<multasRaw/>";
			xml += "<saldoPendiente>";
			if (saldoPendiente < 0.01) {
				xml += "0,00";
			} else {
				xml += NUMBERFORMAT.format(saldoPendiente);
			}
			xml += "</saldoPendiente>";
			xml += "<saldoPendienteRaw>";
			if (saldoPendiente < 0.01) {
				xml += "0";
			} else {
				xml += saldoPendiente;
			}
			xml += "</saldoPendienteRaw>";
			xml += "</Cuota>";
		}
		xml += "</Objetoi>";
		return xml;
	}

	@SuppressWarnings("unchecked")
	public ActionForward generarBoleto(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Long nroAtencion = new Long(request.getParameter("nroProyecto"));
		String fechaCobranza = request.getParameter("fechaCobranza");
		Date hastaFecha = new Date();
		if (fechaCobranza != null) {
			hastaFecha = DateHelper.getDate(fechaCobranza);
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		bp.begin();
		try {
			List<Objetoi> lista = bp.getByFilter("select o from Objetoi o where o.numeroAtencion = " + nroAtencion);
			if (lista.isEmpty()) {
				return null;
			}
			Objetoi credito = lista.get(0);
			Number count = (Number) bp
					.createQuery(
							"select count(*) from Pagos p where p.objetoi.id = :idCredito and p.fechaPago > :fecha")
					.setParameter("idCredito", credito.getId()).setParameter("fecha", hastaFecha).uniqueResult();
			if (count != null && count.intValue() > 0) {
				String xml = "<error>Existe un pago en una fecha posterior para este proyecto.</error>";
				escribir(response, xml);
				bp.rollback();
				return null;
			}
			BoletoPagoHandler bh = new BoletoPagoHandler();
			Boleto boleto;
			boleto = bh.generarBoleto(credito, new Date(), log, bp);
			/*
			 * Boleto boleto; if(metodoCarga != null && metodoCarga == 3) { boleto =
			 * bh.generarBoletoParcial(deudaPagoACuenta, credito, new Date(), log, bp);
			 * }else { boleto = bh.generarBoleto(credito, new Date(), log, bp); }
			 */
			bp.commit();
			Objetoi objetoi = boleto.getObjetoi();
			Persona persona = objetoi.getPersona();
			CreditoCalculoDeuda calculo = new CreditoCalculoDeuda();
			calculo.setHastaFecha(hastaFecha);
			calculo.setIdObjetoi(credito.getId());
			calculo.calcular();
			BeanCtaCte beanTotal = calculo.getBeans().remove(calculo.getBeans().size() - 1);
			double deuda = beanTotal.getSaldoCuota().doubleValue();
			double capital = beanTotal.getCapital().doubleValue();
			double compensatorio = beanTotal.getCompensatorio().doubleValue();
			double moratorio = beanTotal.getMoratorio().doubleValue();
			double punitorio = beanTotal.getPunitorio().doubleValue();
			double gastos = beanTotal.getGastos().doubleValue();
			double multas = beanTotal.getMultas().doubleValue();

			Integer[] cuotas = new Integer[calculo.getBeans().get(calculo.getBeans().size() - 1).getNumero()
					.intValue()];
			for (BeanCtaCte bean : calculo.getBeans()) {
				if (bean.getSaldoCuota() != null && bean.getSaldoCuota().doubleValue() > 0.0) {
					cuotas[bean.getNumero().intValue() - 1] = bean.getNumero();
				}
			}
			String cuotasStr = "";
			for (Integer numero : cuotas) {
				if (numero != null)
					cuotasStr += numero + ", ";
			}
			if (cuotasStr.length() > 2) {
				cuotasStr = cuotasStr.substring(0, cuotasStr.length() - 2);
			}

			String xml = "<Objects>";
			xml += "<Boleto>";
			xml += "<id>" + boleto.getId() + "</id>";
			xml += "<id_numeroBoleto>" + boleto.getNumeroBoleto() + "</id_numeroBoleto>";
			xml += "<id_verificadorBoleto>" + boleto.getVerificadorBoleto() + "</id_verificadorBoleto>";
			xml += "<id_periodoBoleto>" + boleto.getPeriodoBoleto() + "</id_periodoBoleto>";
			xml += "<importe>" + boleto.getImporte() + "</importe>";
			xml += "<nroProyecto>" + credito.getNumeroAtencion() + "</nroProyecto>";

			xml += "<expediente>" + credito.getExpediente() + "</expediente>";
			xml += "<deuda>" + deuda + "</deuda>";
			xml += "<capital>" + capital + "</capital>";
			xml += "<compensatorio>" + compensatorio + "</compensatorio>";
			xml += "<moratorio>" + moratorio + "</moratorio>";
			xml += "<punitorio>" + punitorio + "</punitorio>";
			xml += "<gastos>" + gastos + "</gastos>";
			xml += "<multas>" + multas + "</multas>";
			xml += "<cuotas>" + cuotasStr + "</cuotas>";
			xml += "<moneda>" + credito.getLinea().getMoneda_id() + "</moneda>";

			xml += "</Boleto>";
			xml += "<Persona>";
			xml += "<NOMB_12>" + (persona.getNomb12() != null ? persona.getNomb12().replace("&", "&amp;") : "")
					+ "</NOMB_12>";
			xml += "<CUIL_12>" + persona.getCuil12() + "</CUIL_12>";
			xml += "</Persona>";

			xml += "</Objects>";

			escribir(response, xml);
		} catch (Exception e) {
			bp.rollback();
			throw e;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward generarBoletosFecovita(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Cargando Boletos Fecovita";
		ProgressStatus.iProgress = 0;
		Double saldo = new Double(request.getParameter("saldo"));
		String fechaCobaranzaStr = request.getParameter("fechaCobranza");
		String fechaCalculoStr = request.getParameter("fechaCalculo");
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String fecovitaTipo = request.getParameter("fecovitaTipo");
		String sqlObjetos = "SELECT O.id, O.numeroAtencion, P.NOMB_12 FROM Objetoi O JOIN PERSONA P ON P.IDPERSONA = O.persona_IDPERSONA JOIN ObjetoiEstado OE ON O.id = OE.objetoi_id "
				+ "JOIN Estado E ON E.idEstado = OE.estado_idEstado WHERE OE.fechaHasta IS NULL AND (E.nombreEstado = 'EJECUCION' "
				+ "OR E.nombreEstado = 'PENDIENTE SEGUNDO DESEMBOLSO') AND O.fecovitaTipo = '" + fecovitaTipo
				+ "' ORDER BY O.id";
		List<Object[]> objetos = bp.createSQLQuery(sqlObjetos).list();
		if (objetos.isEmpty()) {
			ProgressStatus.iTotal = 1;
		} else {
			ProgressStatus.iTotal = objetos.size();
		}
		Boleto boleto;
		Objetoi credito = new Objetoi();
		BoletoPagoHandler bh = new BoletoPagoHandler();
		Date fechaCobranza = DateHelper.getDate(fechaCobaranzaStr);
		Date fechaCalculo = fechaCobranza;
		if (!fechaCalculoStr.isEmpty()) {
			fechaCalculo = DateHelper.getDate(fechaCalculoStr);
		}
		CreditoCalculoDeuda calculo = new CreditoCalculoDeuda();
		List<Cuota> cuotasPeriodo;
		Cuota cuota;
		// BeanCtaCte beanTotal;
		double deuda;
		double capital;
		double compensatorio;
		double moratorio;
		double punitorio;
		double gastos;
		double multas;
		Integer[] cuotas;
		String cuotasStr;
		String xml = "<Objects>";
		try {
			bp.begin();
			for (Object[] objeto : objetos) {
				try {
					if (saldo <= 0.0) {
						ProgressStatus.iProgress = ProgressStatus.iTotal;
						break;
					}
					credito.setId(((BigDecimal) objeto[0]).longValue());
					calculo.setIdObjetoi(credito.getId());
					cuotasPeriodo = bp
							.createSQLQuery("SELECT * FROM Cuota C WHERE C.credito_id = :credito_id "
									+ "AND C.fechaVencimiento = (SELECT MIN(CU.fechaVencimiento) FROM Cuota CU "
									+ "WHERE CU.fechaVencimiento >= :fecha AND CU.credito_id = :credito_id) "
									+ "AND C.numero = (SELECT MIN(CUO.numero) FROM Cuota CUO "
									+ "WHERE CUO.fechaVencimiento >= :fecha AND CUO.credito_id = :credito_id)")
							.addEntity(Cuota.class).setLong("credito_id", credito.getId())
							.setDate("fecha", fechaCalculo).list();
					if (cuotasPeriodo.isEmpty()) {
						cuota = credito.getUltimaCuota();
					} else {
						cuota = cuotasPeriodo.get(0);
					}
					if (cuota.getEmision() != null) {
						calculo.setHastaFecha(cuota.getFechaVencimiento());
					} else {
						calculo.setHastaFecha(fechaCalculo);
					}
					calculo.calcular();
					// beanTotal =
					// calculo.getBeans().get(calculo.getBeans().size() - 1); //
					// no uso remove porque el metodo siguiente itera hasta -1
					// deuda = beanTotal.getSaldoCuota().doubleValue();
					deuda = calculo.getDeudaVencidaFechaEstricta();
					// calculo.getBeans().remove(calculo.getBeans().size() - 1);
					// // ahora si lo remuevo
					if (deuda < 0.1) {
						ProgressStatus.iProgress++;
						continue;
					}
					multas = calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_MULTAS);
					if (saldo >= multas) {
						multas = 0.0;
						saldo -= multas;
					} else {
						multas = multas - saldo;
						saldo = 0.0;
					}
					gastos = calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_GASTOS);
					if (saldo >= gastos) {
						gastos = 0;
						saldo -= gastos;
					} else {
						gastos = gastos - saldo;
						saldo = 0.0;
					}
					punitorio = calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_PUNITORIO);
					if (saldo >= punitorio) {
						punitorio = 0;
						saldo -= punitorio;
					} else {
						punitorio = punitorio - saldo;
						saldo = 0.0;
					}
					moratorio = calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_MORATORIO);
					if (saldo >= moratorio) {
						moratorio = 0;
						saldo -= moratorio;
					} else {
						moratorio = moratorio - saldo;
						saldo = 0.0;
					}
					compensatorio = calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_COMPENSATORIO);
					compensatorio -= calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_BONIFICACION);
					if (saldo >= compensatorio) {
						compensatorio = 0;
						saldo -= compensatorio;
					} else {
						compensatorio = compensatorio - saldo;
						saldo = 0.0;
					}
					// double capitalParcial = calculo.getCapitalParcial();
					double capitalParcial = calculo.getSaldoVencidoConcepto(Concepto.CONCEPTO_CAPITAL);
					if (saldo >= capitalParcial) {
						capital = 0;
						saldo -= capitalParcial;
					} else {
						capital = capitalParcial - saldo;
						saldo = 0.0;
					}
					// ahora si lo remuevo
					calculo.getBeans().remove(calculo.getBeans().size() - 1);
					cuotas = new Integer[calculo.getBeans().get(calculo.getBeans().size() - 1).getNumero().intValue()];

					for (BeanCtaCte bean : calculo.getBeans()) {
						if (bean.getSaldoCuota() != null && bean.getSaldoCuota().doubleValue() > 0.0)
							cuotas[bean.getNumero().intValue() - 1] = bean.getNumero();
					}
					cuotasStr = "";
					for (Integer numero : cuotas) {
						if (numero != null) {
							cuotasStr += numero + ", ";
						}
					}
					cuotasStr = cuotasStr.substring(0, cuotasStr.length() - 2);
					boleto = bh.generarBoletoParcial(deuda, credito, fechaCalculo, log, bp);
					xml += "<Credito>";
					xml += "<id>" + boleto.getId() + "</id>";
					xml += "<id_numeroBoleto>" + boleto.getNumeroBoleto() + "</id_numeroBoleto>";
					xml += "<id_verificadorBoleto>" + boleto.getVerificadorBoleto() + "</id_verificadorBoleto>";
					xml += "<id_periodoBoleto>" + boleto.getPeriodoBoleto() + "</id_periodoBoleto>";
					xml += "<importe>" + boleto.getImporte() + "</importe>";
					xml += "<nroProyecto>" + ((BigDecimal) objeto[1]).longValue() + "</nroProyecto>";
					xml += "<NOMB_12>" + (objeto[2] != null ? ((String) objeto[2]).replace("&", "&amp;") : "")
							+ "</NOMB_12>";
					xml += "<deuda>" + deuda + "</deuda>";
					xml += "<capital>" + capital + "</capital>";
					xml += "<compensatorio>" + compensatorio + "</compensatorio>";
					xml += "<moratorio>" + moratorio + "</moratorio>";
					xml += "<punitorio>" + punitorio + "</punitorio>";
					xml += "<gastos>" + gastos + "</gastos>";
					xml += "<multas>" + multas + "</multas>";
					xml += "<cuotas>" + cuotasStr + "</cuotas>";
					xml += "</Credito>";
					ProgressStatus.iProgress++;
				} catch (Exception e) {
					ProgressStatus.iProgress++;
					if (e.getMessage().equals("El credito no tiene cuotas generadas."))
						continue;
					else
						throw e;
				}
			}
			xml += "</Objects>";
			escribir(response, xml);
			bp.commit();
			ProgressStatus.onProcess = false;
			return null;
		} catch (Exception e) {
			ProgressStatus.onProcess = false;
			e.printStackTrace();
			bp.rollback();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public ActionForward generarBoletosMetodoAutomatico(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Cargando Proyectos";
		ProgressStatus.iProgress = 0;
		Long idCaratula = new Long(request.getParameter("idCaratula"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Object[]> objetos = bp.createSQLQuery(
				"SELECT O.id, O.numeroAtencion, P.NOMB_12, P.CUIL_12, O.expediente, D.importe, D.id 'idDetalleRecaudacion', D.detallePadre_id 'idDetallePadre', D.moneda_idmoneda FROM Objetoi O "
						+ "JOIN PERSONA P ON P.IDPERSONA = O.persona_IDPERSONA JOIN DetalleRecaudacion D ON D.numeroProyecto = O.numeroAtencion WHERE D.caratula_id = :caratula_id "
						+ "AND D.tempRecaudacion_IDRECAUDACION IS NOT NULL ORDER BY D.numeroProyecto, D.fechaAcreditacionValor")
				.setLong("caratula_id", idCaratula).list();
		if (objetos.isEmpty()) {
			ProgressStatus.iTotal = 1;
			ProgressStatus.iProgress = ProgressStatus.iTotal;
		} else
			ProgressStatus.iTotal = objetos.size();
		Boleto boleto;
		BoletoPagoHandler bh = new BoletoPagoHandler();
		DetalleRecaudacion detalleRecaudacion;
		CreditoCalculoDeuda calculo = new CreditoCalculoDeuda();
		// BeanCtaCte beanTotal;
		// String cuotasStr;
		String xml = "<Objects>";
		try {
			bp.begin();
			CreditoHandler ch = new CreditoHandler();
			for (int i = 0; i < objetos.size(); i++) {
				Object[] objeto = objetos.get(i);
				try {
					Objetoi credito = (Objetoi) bp.getById(Objetoi.class, new Long(((Number) objeto[0]).longValue()));
					calculo.setIdObjetoi(credito.getId());
					detalleRecaudacion = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class,
							((BigDecimal) objeto[6]).longValue());
					Date fechaCobranza = detalleRecaudacion.getFechaAcreditacionValor();
					double sumaImportes = (Double) objeto[5];
					Long idMoneda = objeto[8] != null ? ((Number) objeto[8]).longValue() : null;
					if (idMoneda == null) {
						idMoneda = 1L;
					}
					Long idMonedaCredito = credito.getMoneda_id() != null ? credito.getMoneda_id()
							: credito.getLinea().getMoneda_id();
					if (idMonedaCredito == null) {
						idMonedaCredito = 1L;
					}
					// devuelve 1 si es la misma moneda
					double cotizacion = ch.getCotizacion(idMoneda, idMonedaCredito, fechaCobranza);
					double importeConvertido = sumaImportes;
					if (idMoneda != null && !idMoneda.equals(idMonedaCredito)) {
						importeConvertido = importeConvertido / cotizacion;
					}
					// veo hacia adelante los detalleRecaudacion que sean del
					// mismo numeroProyecto
					for (int j = i + 1; j < objetos.size(); j++) {
						Long idDetalleSiguiente = ((BigDecimal) objetos.get(j)[6]).longValue();
						DetalleRecaudacion detalleSiguiente = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class,
								idDetalleSiguiente);
						if (credito.getNumeroAtencion().equals(detalleSiguiente.getNumeroProyecto())) {
							sumaImportes += detalleSiguiente.getImporte();
							Long idMonedaDetalle = detalleSiguiente.getMoneda() != null
									? detalleSiguiente.getMoneda().getId()
									: 1L;
							if (!idMonedaDetalle.equals(idMonedaCredito)) {
								importeConvertido += detalleSiguiente.getImporte() / ch.getCotizacion(idMonedaDetalle,
										idMonedaCredito, detalleSiguiente.getFechaRecaudacion());
							} else {
								importeConvertido += detalleSiguiente.getImporte();
							}
							i++;
							ProgressStatus.iProgress++;
						} else {
							break;
						}
					}
					calculo.setHastaFecha(fechaCobranza);
					calculo.setConsiderarLiquidacion(true);
					calculo.calcular();
					boleto = bh.generarBoleto(credito, fechaCobranza, log, bp);
					// usar suma de importes
					boleto.setImporte(importeConvertido);
					String fechaCobStr;
					if (detalleRecaudacion.getFechaAcreditacionValor() != null) {
						fechaCobStr = DateHelper.getString(detalleRecaudacion.getFechaAcreditacionValor());
					} else {
						fechaCobStr = detalleRecaudacion.getFechaRecaudacionStr();
					}
					xml += generarXmlPreaplicacion(calculo, credito, importeConvertido, fechaCobStr,
							"" + ((BigDecimal) objeto[6]).longValue(), objeto[7] != null ? objeto[7].toString() : null,
							boleto.getId());
					ProgressStatus.iProgress++;
				} catch (Exception e) {
					ProgressStatus.iProgress++;
					if (e.getMessage() != null && e.getMessage().equals("El credito no tiene cuotas generadas.")) {
						continue;
					} else {
						throw e;
					}
				}
			}
			xml += "</Objects>";
			escribir(response, xml);
			bp.commit();
			ProgressStatus.onProcess = false;
			return null;
		} catch (Exception e) {
			ProgressStatus.onProcess = false;
			e.printStackTrace();
			bp.rollback();
			throw e;
		}
	}

	public ActionForward calcularDeuda(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String idCredito = request.getParameter("idCredito");
		String vencida = request.getParameter("deudaVencida");
		CreditoCalculoDeuda c = new CreditoCalculoDeuda(new Long(idCredito), new Date());
		c.calcular();
		double deuda = vencida != null ? c.getDeudaVencidaFechaEstricta() : c.getDeudaTotal();
		String xml = "<Credito>";
		xml += "<id>" + idCredito + "</id>";
		xml += "<deuda>" + NUMBERFORMAT.format(deuda) + "</deuda>";
		xml += "</Credito>";
		escribir(response, xml);
		return null;
	}

	// =====================UTILIDADES=========================
	/**
	 * Encargado de escribir texto en html para que retorne el servidor.-
	 * 
	 * @param response
	 * @param xml      , xml que escribira para que retorne el servidor.-
	 * @return true si todo salio bien.-
	 */
	private boolean escribir(HttpServletResponse response, String xml) {
		boolean retorno = true;
		try {
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setCharacterEncoding("ISO-8859-1");
			PrintWriter out = response.getWriter();
			out.print(xml.replace("<null/>", "</>"));
			out.flush();
		} catch (Exception e) {
			retorno = false;
		}
		return retorno;
	}// fin escribir.-

	public ActionForward completarDatosAfip(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String codigo = new String(request.getParameter("codigo"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ActividadAfip afip = (ActividadAfip) bp.createQuery("Select a from ActividadAfip a where a.codigo=:codigo")
				.setParameter("codigo", codigo).uniqueResult();
		if (afip != null) {
			response.getWriter().print(afip.getNombre() + "@");
			response.getWriter().print(afip.getDescripcion() + "@");
		}
		return null;
	}

	public ActionForward completarDatosBonificacion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idBonificacion = new Long(request.getParameter("idBonificacion"));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Bonificacion bonificacion = (Bonificacion) bp.getById(Bonificacion.class, idBonificacion);
		try {
			response.getWriter().print(bonificacion.getTasaBonificadaStr() + "@");
			response.getWriter().print(bonificacion.getTipoBonificacionStr() + "@");
			response.getWriter().print(bonificacion.getConvenio().getBancoId() + "@");
			response.getWriter().print(bonificacion.getConvenio().getFechaVencimientoRealStr() + "@");
			response.getWriter().print(bonificacion.getConvenio().getFechaVencimientoAplicacionStr() + "@");
			response.getWriter().print(bonificacion.getConvenio().getDiasMora() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxDiasBon() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxBonCredito() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxFinanciamientoCredito() + "@");
			response.getWriter().print(bonificacion.getMinimoInteresStr() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxBonConvenio() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxFinanciamientoConvenio() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxBonPersona() + "@");
			response.getWriter().print(bonificacion.getConvenio().getMaxFinanciamientoPersona() + "@");
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Transient
	public ActionForward buscarSiguienteDesembolso(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String idCredito = request.getParameter("idCredito");
		BusinessPersistance bp = com.asf.security.SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Number numero = (Number) bp.getNamedQuery("Desembolso.findSiguienteNumero")
				.setParameter("idCredito", new Long(idCredito)).uniqueResult();
		if (numero == null) {
			numero = new Integer(1);
		}
		try {
			response.getWriter().print(numero.intValue());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ActionForward ajaxModificarRecaudacion(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Long idRecaudacion = new Long(Long.parseLong(request.getParameter("idrecaudacion")));
		String imp = request.getParameter("importeSaldo");
		if (!imp.contains(".")) {
			imp += ".00";
		}
		Double importe = new Double(Double.parseDouble(imp));
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Recaudacion recaudacion = (Recaudacion) bp.getById(Recaudacion.class, idRecaudacion);
		Double saldo = recaudacion.getSaldo().doubleValue() - (importe.doubleValue() * recaudacion.getCotizacion());

		if (saldo.doubleValue() < 0.0) {
			return null;
		}
		try {
			recaudacion.setSaldo(saldo);
			if (recaudacion.getImporte().doubleValue() <= importe.doubleValue()) {
				recaudacion.setProcesado(null);
			} else {

				recaudacion.setProcesado(true);
				recaudacion.setEstadoComprobante("2");
			}
			bp.saveOrUpdate(recaudacion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward buscarExpediente(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// atributos.-
		String xml = "";
		String expediente = null;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<String> expedientes = null;
		try {
			expediente = request.getParameter("expediente");
			expedientes = bp.createQuery(
					"SELECT DISTINCT(c.expediente) FROM Objetoi c WHERE c.expediente LIKE :expediente ORDER BY c.expediente")
					.setString("expediente", expediente + "%").list();
			xml += "<ul>";
			if (expedientes != null && expedientes.size() > 0) {
				for (Iterator<String> iterator = expedientes.iterator(); iterator.hasNext();) {
					String expedienteTemp = iterator.next();
					xml += "<li>";
					xml += expedienteTemp;
					xml += "</li>";
				}
			} else {
				xml += "<li>";
				xml += "No se encontraron coincidencias";
				xml += "</li>";
			}
			xml += "</ul>";
		} catch (Exception e) {
			expediente = "<ul></ul>";
		}
		escribir(response, xml);
		return null;
	}

	public ActionForward buscarExpedientePorProyecto(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String nroAtencion = request.getParameter("numeroAtencion");
		if (!nroAtencion.equals("")) {
			Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setParameter("nroAtencion", new Long(nroAtencion)).uniqueResult();
			if (credito != null && credito.getExpediente() != null) {
				escribir(response, credito.getExpediente());
			}
		}
		return null;
	}

	public ActionForward buscarIdPersonaPorProyecto(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String nroAtencion = request.getParameter("numeroAtencion");
		Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
				.setParameter("nroAtencion", new Long(nroAtencion)).uniqueResult();
		if (credito != null && credito.getPersona_id() != null) {
			escribir(response, credito.getPersona_id().toString());
		}
		return null;
	}

	public ActionForward buscarConfigIngresosVarios(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String concepto = request.getParameter("concepto");
		String tipoConcepto = request.getParameter("tipoConcepto");
		ConceptoIngresosVarios ing = (ConceptoIngresosVarios) bp.createQuery(
				"select c from ConceptoIngresosVarios c where c.concepto.concepto = :concepto and c.tipoConcepto = :tipoConcepto and c.aplicacion = 'INGRESOS_VARIOS'")
				.setParameter("concepto", concepto).setParameter("tipoConcepto", tipoConcepto).uniqueResult();
		if (concepto.equalsIgnoreCase(DirectorHelper.getString("ftyc.deudoresVarios.conceptoGastosVarios"))
				&& tipoConcepto
						.equalsIgnoreCase(DirectorHelper.getString("ftyc.deudoresVarios.tipoConceptoGastosVarios"))) {
			Double importeTotal = (double) 0;
			String nroAtencion = request.getParameter("numeroAtencion");
			if (nroAtencion != null && !nroAtencion.equals("")) {
				Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
						.setParameter("nroAtencion", new Long(nroAtencion)).uniqueResult();
				if (credito != null) {
					Double importe = (Double) bp.createQuery(
							"select sum(d.importe) from DetalleFactura d where d.credito.id = :credito and d.impactado = 0")
							.setParameter("credito", credito.getId()).uniqueResult();
					if (importe != null && importe > 0) {
						importeTotal += importe;
					}
					String xml = "<config>";
					xml += "<vencimiento>" + DateHelper.getString(DateHelper.addDiasHabiles(new Date(), 15))
							+ "</vencimiento>";
					xml += "<importe>" + importeTotal + "</importe>";
					xml += "<manual>" + ing.isManual() + "</manual>";
					xml += "</config>";
					escribir(response, xml);
				}
			}
		} else {
			if (ing != null) {
				String xml = "<config>";
				xml += "<vencimiento>"
						+ DateHelper.getString(DateHelper.addDiasHabiles(new Date(), ing.getPlazoVtoCon()))
						+ "</vencimiento>";
				xml += "<importe>" + ing.getImporte() + "</importe>";
				xml += "<manual>" + ing.isManual() + "</manual>";
				xml += "</config>";
				escribir(response, xml);
			}
		}
		return null;
	}

	public ActionForward buscarConfigNovedades(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String concepto = request.getParameter("concepto");
		String tipoConcepto = request.getParameter("tipoConcepto");
		ConceptoIngresosVarios ing = (ConceptoIngresosVarios) bp.createQuery(
				"select c from ConceptoIngresosVarios c where c.concepto.concepto = :concepto and c.tipoConcepto = :tipoConcepto and c.aplicacion = 'NOVEDADES_CTACTE'")
				.setParameter("concepto", concepto).setParameter("tipoConcepto", tipoConcepto).uniqueResult();
		if (ing != null) {
			String xml = "<config>";
			xml += "<vencimiento>" + DateHelper.getString(DateHelper.addDiasHabiles(new Date(), ing.getPlazoVtoCon()))
					+ "</vencimiento>";
			xml += "<importe>" + ing.getImporte() + "</importe>";
			xml += "</config>";
			escribir(response, xml);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward buscarConceptosIngresosVarios(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<CConcepto> ings = bp.createQuery(
				"select distinct c.concepto from ConceptoIngresosVarios c where c.aplicacion = 'INGRESOS_VARIOS'")
				.list();
		String xml = "<conceptos>";
		for (CConcepto c : ings) {
			xml += "<concepto>" + "<detalle>" + c.getDetalle() + "</detalle>" + "<id>" + c.getId() + "</id>"
					+ "</concepto>";
		}
		xml += "</conceptos>";
		escribir(response, xml);
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward buscarTiposConceptosIngresosVarios(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String concepto = request.getParameter("concepto");
		List<String> ings = bp.createQuery(
				"select c.tipoConcepto from ConceptoIngresosVarios c where c.concepto.concepto = :concepto and c.aplicacion = 'INGRESOS_VARIOS'")
				.setParameter("concepto", concepto).list();
		String xml = "<tipos>";
		for (String c : ings) {
			try {
				xml += "<tipo>" + "<text>" + TipificadorHandler.getDescripcion("ctacte.detalle", c) + "</text>"
						+ "<value>" + c + "</value>" + "</tipo>";
			} catch (Exception e) {
			}
		}
		xml += "</tipos>";
		escribir(response, xml);
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward buscarConceptosNovedades(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<CConcepto> ings = bp.createQuery(
				"select distinct c.concepto from ConceptoIngresosVarios c where c.aplicacion = 'NOVEDADES_CTACTE'")
				.list();
		String xml = "<conceptos>";
		for (CConcepto c : ings) {
			xml += "<concepto>" + "<detalle>" + c.getDetalle() + "</detalle>" + "<id>" + c.getId() + "</id>"
					+ "</concepto>";
		}
		xml += "</conceptos>";
		escribir(response, xml);
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward buscarTiposConceptosNovedades(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String concepto = request.getParameter("concepto");
		List<String> ings = bp.createQuery(
				"select c.tipoConcepto from ConceptoIngresosVarios c where c.concepto.concepto = :concepto and c.aplicacion = 'NOVEDADES_CTACTE'")
				.setParameter("concepto", concepto).list();
		String xml = "<tipos>";
		for (String c : ings) {
			try {
				xml += "<tipo>" + "<text>" + TipificadorHandler.getDescripcion("ctacte.detalle", c) + "</text>"
						+ "<value>" + c + "</value>" + "</tipo>";
			} catch (Exception e) {
			}
		}
		xml += "</tipos>";
		escribir(response, xml);
		return null;
	}

	public ActionForward verificarCambioTasa(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idObjetoiGarantia = new Long(request.getParameter("idObjetoiGarantia"));
		String tipoProd = request.getParameter("tipoProd");
		ObjetoiGarantia og = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);
		String oldTipoProd = og.getGarantia().getTipoProducto();
		if (!tipoProd.equals(oldTipoProd)) {
			Date fechaSolicitud = og.getObjetoi().getFechaSolicitud();
			if (fechaSolicitud == null) {
				fechaSolicitud = new Date();
			}
			ObjetoiIndice oic = og.getObjetoi().findTasaCompensatorio();
			CreditoHandler ch = new CreditoHandler();
			// lleno parametros para buscar el periodo
			Garantia newGarantia = new Garantia();
			newGarantia.setTipo(og.getGarantia().getTipo());
			newGarantia.setTipoProducto(tipoProd);
			PeriodoCosechaConfig periodo = ch.buscarPeriodoCosechaConfig(fechaSolicitud, newGarantia,
					og.getObjetoi().getVarietales());
			if (periodo != null) {
				if (!periodo.getIdIndiceGar().equals(oic.getIndice().getId())
						|| !periodo.getValorMasGar().equals(oic.getValorMas())
						|| !periodo.getValorPorGar().equals(oic.getValorPor())) {
					escribir(response, "true");
					return null;
				}
			}
		}
		escribir(response, "false");
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward buscarDepartamento(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idLocalidad = new Long(request.getParameter("idLocalidad"));
		List<LocalidadDepartamento> lds = bp.getNamedQuery("LocalidadDepartamento.findByLocalidad")
				.setParameter("idLocalidad", idLocalidad).setMaxResults(1).list();
		if (lds != null && !lds.isEmpty()) {
			escribir(response, lds.get(0).getId().getDepartamento().getNombre());
		}
		return null;
	}

	public ActionForward getElementosAMostrar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		try {
			String idTipoProceso = request.getParameter("idTipoProceso");
			TipoProceso tipoProceso = (TipoProceso) bp.getById(TipoProceso.class, new Long(idTipoProceso));
			response.getWriter().print(tipoProceso.getCamposAMostrar());
			response.getWriter().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * Busca las personas, por nombre, apellido o nro de documento.-
	 * 
	 */
	@SuppressWarnings("unchecked")
	public ActionForward buscarProyecto(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		// atributos.-
		String xml = "";

		String ignorarLineas = request.getParameter("ignorarLineas");
		String expedienteIngresado = request.getParameter("proyecto");
		Long nroProy = null;

		try {
			nroProy = new Long(expedienteIngresado);
		} catch (NumberFormatException ne) {
			nroProy = null;
		}

		String consulta = "SELECT o FROM Objetoi o WHERE ";

		if (ignorarLineas != null && !ignorarLineas.isEmpty()) {
			consulta += "o.linea.id NOT IN (" + ignorarLineas + ") AND ";

		}

		// Si el dato ingresado es un numero, busco por numero de documento o CUIT.-
		if (nroProy != null) {
			consulta += "o.numeroAtencion LIKE '" + nroProy + "%'";
			consulta += " ORDER BY o.numeroAtencion";
		}
		// Sino busco por expediente
		else {
			consulta += "o.expediente LIKE '" + expedienteIngresado + "%'";
			consulta += " ORDER BY o.expediente";
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Objetoi> proyectos = bp.getByFilter(consulta);

		xml += "<ul>";

		if (proyectos.size() > 0) {
			for (Objetoi proyecto : proyectos) {
				xml += "<li>";
				xml += "<id>";
				xml += "Credito:" + proyecto.getNumeroAtencionStr() + "-";
				xml += "</id>";
				xml += "<nombre>";
				xml += "Expediente:" + proyecto.getExpediente() + "-";
				xml += "</nombre>";
				xml += "<nroDoc>";
				xml += "Dni:" + proyecto.getPersona().getNudo12Str();
				xml += "</nroDoc>";
				xml += "</li>";
			}
		} else {
			xml += "<li>";
			xml += "No se encontraron coincidencias";
			xml += "</li>";
		}
		xml += "</ul>";
		this.escribir(response, xml);
		return null;
	}// fin buscarPersona.-

	@SuppressWarnings("unchecked")
	public ActionForward consultaInv(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String xml = "<consultainv>";
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Long idVinedo = new Long(request.getParameter("v"));
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			Objetoi credito = vinedo.getCredito();
			boolean cargaPorDefecto = false;

			String urlString = DirectorHelper.getString("inv.consulta.url");
			URL url = new URL(urlString.replace("{vinedo}", vinedo.getVinedo().getCodigo()));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			int responseCode = conn.getResponseCode();
			if (responseCode == HttpsURLConnection.HTTP_OK) {
				String responseText = IOUtils.toString(conn.getInputStream(), "utf-8");
				xml += "<success>true</success>";
				if (responseText != null && !responseText.trim().isEmpty()) {
					double hectareas = 0;
					double qq = 00;
					bp.createQuery("delete from DetalleVinedoINV where vinedo.id = :id")
							.setParameter("id", vinedo.getId()).executeUpdate();
					String[] registros = responseText.split("\\|");
					for (String registro : registros) {
						if (registro.trim().isEmpty()) {
							continue;
						}
						DetalleVinedoINV detalle = new DetalleVinedoINV(registro);
						detalle.setVinedo(vinedo);
						bp.save(detalle);
						hectareas += detalle.getHectareas();
						qq += detalle.getQq();
					}
					vinedo.getVinedo().setHectareas(hectareas);
					if (credito.getDestino().equalsIgnoreCase("CRDA")) {
						vinedo.getObjetoiVinedoElab().setQqAprobadoElab(qq);
						vinedo.getObjetoiVinedoElab().setFechaInformeINV(new Date());
					} else {
						vinedo.setQqAprobado(qq);
						vinedo.setFechaInformeINV(new Date());
					}
					bp.update(vinedo);
					xml += "<hectareas>" + DoubleHelper.getString(hectareas) + "</hectareas>";
					xml += "<qq>" + DoubleHelper.getString(qq) + "</qq>";
				} else {
					cargaPorDefecto = true;
				}
			} else {
				cargaPorDefecto = true;
			}
			if (cargaPorDefecto) {
				List<DetalleVinedoINV> detalles = bp
						.createQuery("select d from DetalleVinedoINV d where d.vinedo.id = :id")
						.setParameter("id", vinedo.getId()).list();
				// si hay detalles cargados, no hacer nada
				if (detalles.isEmpty()) {
					// siempre es exitoso porque si no devuelve nada, hay que
					// llenar con valores por defecto
					xml += "<success>true</success>";
					DetalleVinedoINV detalle = new DetalleVinedoINV();
					detalle.setVinedo(vinedo);
					detalle.setCodigoVariedad("-");
					detalle.setNombreVariedad("-");
					detalle.setHectareas(0);
					detalle.setQq(0);
					bp.save(detalle);
					xml += "<hectareas>0</hectareas>";
					xml += "<qq>0</qq>";
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			xml += "<success>false</success>";
			xml += "<error>La URL configurada es incorrecta</error>";
		} catch (IOException e) {
			e.printStackTrace();
			xml += "<success>false</success>";
			xml += "<error>Error de conexion con INV</error>";
		}
		xml += "</consultainv>";
		escribir(response, xml);
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward consultaDetalleVinedo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long idVinedo = new Long(request.getParameter("v"));

		String xml = "<consultavinedo>";

		xml += "<id>" + idVinedo + "</id>";

		List<DetalleVinedoINV> detalles = bp
				.createQuery("select d from DetalleVinedoINV d where d.vinedo.id = :id order by d.id")
				.setParameter("id", idVinedo).list();
		for (DetalleVinedoINV detalle : detalles) {
			xml += "<detalle>";
			xml += "<codvariedad>" + detalle.getCodigoVariedad() + "</codvariedad>";
			xml += "<nombrevariedad>" + detalle.getNombreVariedad() + "</nombrevariedad>";
			xml += "<hectareas>" + DoubleHelper.getString(detalle.getHectareas()) + "</hectareas>";
			xml += "<qq>" + DoubleHelper.getString(detalle.getQq()) + "</qq>";
			xml += "</detalle>";
		}

		xml += "</consultavinedo>";

		escribir(response, xml);

		return null;
	}

	public ActionForward calcularFechaVencimientoGarantia(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		try {
			Long idTipo = new Long(request.getParameter("t"));
			Date fechaInsc = DateHelper.getDate(request.getParameter("fi"));

			TipoGarantia tipo = (TipoGarantia) bp.getById(TipoGarantia.class, idTipo);
			if (tipo.getValidezAnios() != null && tipo.getValidezAnios().intValue() > 0) {
				Calendar c = Calendar.getInstance();
				c.setTime(fechaInsc);
				c.add(Calendar.YEAR, tipo.getValidezAnios().intValue());
				Date fechaVenc = c.getTime();
				escribir(response, DateHelper.getString(fechaVenc));
				return null;
			}
		} catch (NumberFormatException e) {
			System.err.println("Error con id tipo de garantia al calcular fecha vencimiento");
		} catch (Exception e) {
			e.printStackTrace();
		}
		escribir(response, "");
		return null;
	}

	@SuppressWarnings("unchecked")
	public ActionForward consultarPeriodoProyecto(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		JsonObject resultado = new JsonObject();

		Long idLinea = null;
		Long mes = null;
		Long anio = null;
		String fecha = null;

		try {
			idLinea = validarParamLong("idlinea", request);
			fecha = validarParamString("fecha", request);

			String[] parts = fecha.split("/");
			mes = new Long(parts[0]);
			anio = new Long(parts[1]);

			JsonArray list = new JsonArray();

			if (mes != null && anio != null) {
				String query = "SELECT COUNT(*), l.nombre FROM " + BusinessPersistance.getSchema()
						+ ".Objetoi oi LEFT JOIN " + BusinessPersistance.getSchema()
						+ ".Linea l on l.id = oi.linea_id WHERE 1 = 1 AND MONTH(oi.fechaSolicitud) = " + mes
						+ " AND YEAR(oi.fechaSolicitud) = " + anio
						+ (idLinea != null ? (" AND oi.linea_id = " + idLinea) : "") + " GROUP BY l.nombre";

				List<Object[]> valor = bp.createSQLQuery(query).list();

				if (valor != null) {
					for (Object[] l : valor) {
						JsonObject obj = new JsonObject();
						String nombreLinea = (l[1] != null ? ((String) l[1]) : "");
						obj.addProperty("linea", nombreLinea);
						obj.addProperty("cantidad", (Number) l[0]);
						list.add(obj);
					}
				}

				resultado.addProperty("exito", true);
				resultado.addProperty("periodo", mes + "/" + anio);
				resultado.addProperty("resultado", list.toString());
			}

		} catch (Exception e) {
			resultado.addProperty("exito", false);
			resultado.addProperty("mensaje", "Error en el proceso.");
			e.printStackTrace();
		}
		response.getWriter().print(resultado.toString());
		response.getWriter().flush();
		return null;
	}

	private Long validarParamLong(String valor, HttpServletRequest request) {
		Long res = (request.getParameter(valor) != null && !request.getParameter(valor).isEmpty())
				? new Long(request.getParameter(valor))
				: null;
		return res;
	}

	private String validarParamString(String valor, HttpServletRequest request) {
		String res = (request.getParameter(valor) != null && !request.getParameter(valor).isEmpty())
				? (String) request.getParameter(valor)
				: null;
		// this.log.addTextoDebugLn(valor + ": " + res);
		return res;
	}
}
