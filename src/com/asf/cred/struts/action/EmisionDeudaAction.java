package com.asf.cred.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.asf.cred.business.FabricaGeneradorArchivoBanco;
import com.asf.cred.business.GeneradorArchivoBancoAbstract;
import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.EmisionDeudaForm;
import com.asf.gaf.hibernate.Bancos;
import com.asf.security.BusinessPersistance;

public class EmisionDeudaAction extends DispatchAction {

	public ActionForward generar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Long idBanco = ((EmisionDeudaForm) form).getIdBanco();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Bancos banco = (Bancos) bp.createQuery("SELECT b FROM Bancos b where b.codiBa = :id")
				.setParameter("id", idBanco).uniqueResult();
		GeneradorArchivoBancoAbstract generador = FabricaGeneradorArchivoBanco.getInstance().getGenerador(banco);
		boolean resultado = generador.generar();
		if (resultado) {
			String mensaje = "Se gener� correctamente el archivo de Emisi�n de Deuda para el " + banco.getDetaBa();
		}
		return super.execute(mapping, form, request, response);
	}

	public ActionForward cargar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return mapping.findForward("EmisionDeuda");
	}

}
