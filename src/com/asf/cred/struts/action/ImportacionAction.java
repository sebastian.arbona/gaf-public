/*
 * UploadAction.java
 *
 * Created on 19 de junio de 2007, 15:58
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.asf.cred.struts.action;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.SortedMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.ObjectNotFoundException;

import com.asf.cred.business.BoletoPagoHandler;
import com.asf.cred.business.ConsultaDeuda;
import com.asf.cred.business.ProgressStatus;
import com.asf.cred.struts.form.AbmFormCaratula;
import com.asf.cred.struts.form.ImportacionForm;
import com.asf.hibernate.mapping.Claves;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.civitas.importacion.BoletosFactory;
import com.civitas.importacion.CSVReader;
import com.civitas.importacion.Importacion;
import com.civitas.importacion.PagoCreditoBean;
import com.civitas.importacion.errores.Errores;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.ConsultaDeudaPersona;
import com.nirven.creditos.hibernate.DeudorMF;
import com.nirven.creditos.hibernate.SubidaMF;

public class ImportacionAction extends DispatchAction {

	public ImportacionAction() {
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public ActionForward importar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ImportacionForm iForm = (ImportacionForm) form;
		Importacion importacion = new com.civitas.importacion.Importacion(iForm.getIdFormato(),
				BoletosFactory.getInstance());
		ActionErrors aErrores = new ActionErrors();
		// Reciclo el Singleton.
		Errores.getInstancia().resetErrores();
		importacion.setArchivo(iForm.getFile().getInputStream());
		importacion.importar();
		if (importacion.getObjetosValidados() == null) {
			aErrores.add("error",
					new ActionMessage("importacion.error", "Debe cargar nuevamente un archivo para importar"));
			saveErrors(request, aErrores);
			return mapping.findForward("Importacion");
		}
		List<PagoCreditoBean> beans = new ArrayList<PagoCreditoBean>();
		try {
			importacion.setArchivo(iForm.getFile().getInputStream());
			if (!importacion.importar()) {
				iForm.setNroReporte(importacion.getNroReporte());
				for (Iterator it = Errores.getInstancia().iterator(); it.hasNext();) {
					String sID = it.next().toString();
					List lstErrores = Errores.getInstancia().getErrores(sID);
					for (Iterator it2 = lstErrores.iterator(); it2.hasNext();) {
						com.civitas.importacion.errores.Error error = (com.civitas.importacion.errores.Error) it2
								.next();
						String sError = "";
						if (!error.getLinea().equals(Errores.GENERICOS)) {
							sError += "L�nea N�: " + error.getLinea() + " ";
						}
						if (!error.getPosicion().equals(Errores.GENERICOS)) {
							sError += "Posici�n: " + error.getPosicion() + " ";
						}
						sError += error.getError();
						aErrores.add("error", new ActionMessage("importacion.error", sError));
					}
				}
				saveErrors(request, aErrores);
			} else {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute(AbmFormCaratula.PAGOS_IMPORTADOS, importacion.getObjetosValidados());
				StringBuilder sbScript = new StringBuilder();
				sbScript.append("<script>\nvar i = 0;\n");
				sbScript.append(" var objetos = null;\n");
				BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
				BoletoPagoHandler bh = new BoletoPagoHandler();
				if (importacion.getObjetosValidados().isEmpty()) {
					aErrores.add("error", new ActionMessage("importacion.error", "No hay registros nuevos validados"));
				}
				Long nroProyecto;
				for (Object obj : importacion.getObjetosValidados().values()) {
					PagoCreditoBean bean = (PagoCreditoBean) obj;
					try {
						Boleto boleto = null;
						if (bean.getCredito() != null) {
							try {
								boleto = bh.generarBoleto(bean.getCredito(), bean.getFechaPago(), log, bp);
							} catch (Exception e) {
								e.printStackTrace();
								ActionMessages mensajes = new ActionMessages();
								mensajes.add("error", new ActionMessage("procesamientoRecaudaciones.importacion.cuotas",
										"procesamientoRecaudaciones.importacion.cuotas"));
								saveErrors(request, mensajes);
								return mapping.findForward("Importacion");
							}
						} else if (bean.getBoleto() != null) {
							try {
								boleto = bean.getBoleto();
							} catch (Exception e) {
								e.printStackTrace();
								continue;
							}
						}
						nroProyecto = bean.getNumeroProyectoLong();
						bean.setBoleto(boleto);
						boleto.getId();
						bean.setNumeroBoleto(boleto.getNumeroBoleto());
						bean.setPeriodoBoleto(boleto.getPeriodoBoleto());
						bean.setVerificadorBoleto(boleto.getVerificadorBoleto());
						String xml = "<Objects>";
						xml += "<Boleto>";
						xml += "<id>" + boleto.getId() + "</id>";
						xml += "<numeroBoleto>" + boleto.getNumeroBoleto() + "</numeroBoleto>";
						xml += "<verificadorBoleto>" + boleto.getVerificadorBoleto() + "</verificadorBoleto>";
						xml += "<periodoBoleto>" + boleto.getPeriodoBoleto() + "</periodoBoleto>";
						xml += "<importe>" + bean.getImporte() + "</importe>";
						xml += "</Boleto>";
						xml += "</Objects>";
						sbScript.append("objetos = parseXml(\"").append(xml).append("\");\n");
						sbScript.append("window.parent.boleto = objetos.Objects.Boleto;\n");
						if (bean.getCredito() != null && bean.getCredito().getPersona() != null
								&& bean.getCredito().getPersona().getNomb12() != null
								&& bean.getCredito().getPersona().getCuil12() != null) {
							sbScript.append("window.parent.boletoResponseImportado(\"<Objects></Objects>\");\n");
							String titular = bean.getCredito().getPersona().getNomb12();
							String cuit = bean.getCredito().getPersona().getCuil12().toString();
							sbScript.append("window.parent.agregarBoleto(" + "\"" + titular + "\"" + ", " + "\"" + cuit
									+ "\"" + ", \"" + nroProyecto + "\"" + ");\n");
						} else {
							sbScript.append("window.parent.boletoResponse2(\"<Objects></Objects>\");\n");
							sbScript.append("window.parent.agregarBoleto();\n");
						}
						beans.add(bean);
					} catch (ObjectNotFoundException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				sbScript.append("</script>\n");
				iForm.setScript(sbScript.toString());
				iForm.setDeshabilitado(true);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		saveErrors(request, aErrores);
		cargarTotales(importacion.getObjetosValidados(), iForm);
		iForm.setResult(beans);
		ActionMessages mensajes = new ActionMessages();
		mensajes.add("mensaje",
				new ActionMessage("procesamientoRecaudaciones.importacion", "procesamientoRecaudaciones.importacion"));
		saveErrors(request, mensajes);
		return mapping.findForward("Importacion");
	}

	public ActionForward load(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		request.setAttribute("ImportacionForm", new ImportacionForm());
		return mapping.findForward("Importacion");
	}

	public ActionForward loadImportarCobranza(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ImportacionForm iForm = new ImportacionForm();
		iForm.setIdCaratula(new Long(request.getParameter("idCaratula")));
		request.setAttribute("ImportacionForm", iForm);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("ImportacionForm.idCaratula",
				iForm.getIdCaratula());
		return mapping.findForward("ImportacionCobranza");
	}

	public ActionForward loadImportarConsultaDeuda(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO: Deber�a ser el primero en la vista. Revisar
		ImportacionForm iForm = new ImportacionForm();
		// iForm.setIdCaratula(new Long(request.getParameter("idCaratula")));
		request.setAttribute("ImportacionForm", iForm);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("ImportacionForm.idCaratula",
				iForm.getIdCaratula());
		return mapping.findForward("ImportacionConsultaDeuda");
	}

	public ActionForward eliminar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		StringBuilder sbScript = new StringBuilder();
		sbScript.append("<script>\n");
		sbScript.append("window.parent.quitarTodos();\n");
		sbScript.append("</script>\n");
		ImportacionForm iForm = (ImportacionForm) form;
		iForm.setScript(sbScript.toString());
		return mapping.findForward("Importacion");
	}

	@SuppressWarnings("unchecked")
	private void cargarTotales(SortedMap resultado, ImportacionForm iForm) {
		double totalPagado = 0;
		long cantidadPagados = 0;
		for (Object obj : resultado.values()) {
			PagoCreditoBean bean = (PagoCreditoBean) obj;

			totalPagado += (bean.getImporte() != null) ? bean.getImporte() : 0;
			cantidadPagados++;
		}
		iForm.setTotalBoletos(cantidadPagados);
		iForm.setTotalPagado(totalPagado);
	}

	@SuppressWarnings("unchecked")
	public ActionForward importarCobranza(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Importando Archivo";
		ProgressStatus.iProgress = 0;
		ActionMessages mensajes = new ActionMessages();
		ImportacionForm iForm = (ImportacionForm) form;
		Scanner txt = new Scanner(iForm.getFile().getInputStream());
		String primeraLinea;
		try {
			primeraLinea = txt.nextLine();
		} catch (NoSuchElementException e) {
			ProgressStatus.iProgress = ProgressStatus.iTotal;
			mensajes.add("error", new ActionMessage("procesamientoRecaudaciones.importacion.archivo",
					"procesamientoRecaudaciones.importacion.archivo"));
			saveErrors(request, mensajes);
			txt.close();
			return mapping.findForward("ImportacionCobranza");
		}
		Long idFormato;
		try {
			if (primeraLinea.startsWith("R")) {
				idFormato = 3L;// idFormato Cheque Credicoop
			} else {
				String segundaLinea = txt.nextLine();
				if (segundaLinea.matches("^[A-Z]{1}[\\w\\d ,]+$")) {
					idFormato = 2L;// idFormato Credicoop
				} else {
					idFormato = 1L;// idFormato Nacion
				}
			}
		} catch (NoSuchElementException e) {
			idFormato = 1L;// idFormato Nacion
		}
		txt.close();
		Importacion importacion = new com.civitas.importacion.Importacion(idFormato, BoletosFactory.getInstance());
		importacion.setArchivo(iForm.getFile().getInputStream());
		Errores.getInstancia().resetErrores();
		if (!importacion.importar()) {
			String sID;
			List lstErrores;
			String sError;
			for (Iterator it = Errores.getInstancia().iterator(); it.hasNext();) {
				sID = it.next().toString();
				lstErrores = Errores.getInstancia().getErrores(sID);
				for (Iterator it2 = lstErrores.iterator(); it2.hasNext();) {
					com.civitas.importacion.errores.Error error = (com.civitas.importacion.errores.Error) it2.next();
					sError = "";
					if (!error.getLinea().equals(Errores.GENERICOS)) {
						sError += "L�nea N�: " + error.getLinea() + " ";
					}
					if (!error.getPosicion().equals(Errores.GENERICOS)) {
						sError += "Posici�n: " + error.getPosicion() + " ";
					}
					sError += error.getError();
					mensajes.add("error", new ActionMessage("importacion.error", sError));
				}
			}
			saveErrors(request, mensajes);
			return mapping.findForward("ImportacionCobranza");
		} else {
			String script = "<script>\nparent.document.getElementById('idDetalleArchivo').value = "
					+ iForm.getIdDetalleArchivo() + ";\n</script>";
			if (!iForm.isTodoOk()) {
				script += "<script>\nparent.document.getElementById('idVerResultado').style.display = 'none';\n</script>";
			} else {
				script += "<script>\nparent.document.getElementById('idVerResultado').style.display = 'inline';\n</script>";
			}
			iForm.setScript(script);
			return mapping.findForward("ImportacionCobranza");
		}
	}

	@SuppressWarnings("unchecked")
	public ActionForward importarConsultaDeuda(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO: Ya existe en otro archivo, determinar en cu�l vas
		ProgressStatus.onProcess = false;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Importando Archivo";
		ProgressStatus.iProgress = 0;
		// TODO: implementar los mensajes
		ActionMessages mensajes = new ActionMessages();
		ImportacionForm iForm = (ImportacionForm) form;
		CSVReader reader = new CSVReader(iForm.getFile());

		if (reader.isValid()) {
			Iterator<String> itr = reader.getListExchange().iterator();

			String predicate = "(1";
			while (itr.hasNext()) {
				String element = itr.next().toString();
				if (this.validCUITPredicate(element)) {
					predicate += ", " + element;
					ProgressStatus.iProgress++;
				}
			}
			predicate += ")";
			ConsultaDeuda consultaDeuda = new ConsultaDeuda();
			consultaDeuda.agregarPersonasMasivo(predicate);
			iForm.setConsultaDeudaPersona(consultaDeuda.getConsultaDeudaPersonaArray());
			HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
			if (consultaDeuda.getConsultaDeudaPersonaArray().isEmpty()) {
				ProgressStatus.iProgress = ProgressStatus.iTotal;
				mensajes.add("error", new ActionMessage("consultaDeuda.importacion.archivo.incorrecto",
						"consultaDeuda.importacion.archivo.incorrecto"));
				saveErrors(request, mensajes);
			} else {
				session.setAttribute("consultaDeudaPersonaArray", consultaDeuda.getConsultaDeudaPersonaArray());
				ProgressStatus.iProgress = ProgressStatus.iTotal;
			}
		} else {
			ProgressStatus.iProgress = ProgressStatus.iTotal;
			mensajes.add("error", new ActionMessage("consultaDeuda.importacion.archivo.incorrecto",
					"consultaDeuda.importacion.archivo.incorrecto"));
			saveErrors(request, mensajes);
		}

		String script = "<script>\nparent.document.getElementById('idDetalleArchivo').value = "
				+ iForm.getIdDetalleArchivo() + ";\n</script>";
		if (!iForm.isTodoOk()) {
			script += "<script>\nparent.document.getElementById('idVerResultado').style.display = 'none';\n</script>";
		} else {
			script += "<script>\nparent.document.getElementById('idVerResultado').style.display = 'inline';\n</script>";
		}
		iForm.setScript(script);
		return mapping.findForward("ImportacionConsultaDeuda");
	}

	@SuppressWarnings("unchecked")
	public ActionForward importarDeudoresMF(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ProgressStatus.onProcess = false;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Importando Archivo";
		ProgressStatus.iProgress = 0;
		// TODO: implementar los mensajes
		ActionMessages mensajes = new ActionMessages();
		ImportacionForm iForm = (ImportacionForm) form;
		if (iForm.getFile() != null) {
			CSVReader reader = new CSVReader(iForm.getFile());
			if (reader.isValid()) {
				Iterator<String> itr = reader.getListExchange().iterator();
				SubidaMF subida = new SubidaMF();
				subida.setFecha(new Date());
				subida.setUsuario(new Claves(SessionHandler.getCurrentSessionHandler().getCurrentUser()));
				bp.save(subida);
				int i = 0;
				while (itr.hasNext()) {
					String element = itr.next().toString();
					if (this.validCUITPredicate(element)) {
						DeudorMF d = new DeudorMF();
						d.setCuit(element);
						d.setSubidaMF(subida);
						bp.save(d);
						i++;
					}
				}
				iForm.setResumen("Se importaron " + i + " registros.");
			} else {
				ProgressStatus.iProgress = ProgressStatus.iTotal;
				mensajes.add("error", new ActionMessage("consultaDeuda.importacion.archivo.incorrecto",
						"consultaDeuda.importacion.archivo.incorrecto"));
				saveErrors(request, mensajes);
			}
		}
		return mapping.findForward("ImportacionDeudoresMF");

	}

//	private static boolean isNumeric(String cadena) {
//		try {
//			Integer.parseInt(cadena);
//			return true;
//		} catch (NumberFormatException nfe) {
//			return false;
//		}
//	}

	private boolean validCUITPredicate(String cadena) {
		try {
			if (cadena == null || cadena.isEmpty()) {
				return false;
			} else if (cadena.matches("[0-9]*")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

}
