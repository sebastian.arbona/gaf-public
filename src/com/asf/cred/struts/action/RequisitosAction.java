package com.asf.cred.struts.action;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.upload.FormFile;

import com.asf.cred.business.CreditoHandler;
import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.RequisitosForm;
import com.asf.security.BusinessPersistance;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.CargaRequisito;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.Requisito;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class RequisitosAction extends DispatchAction {

	public Long lineaL = new Long(0);
	@SuppressWarnings("unused")
	private List<CargaRequisito> ListCargaRq = null;
	private String idSolicitud;
	private JasperReport reporte;

	public RequisitosAction() {
		ListCargaRq = new ArrayList<CargaRequisito>();
	}

	@SuppressWarnings("unchecked")
	private List<CargaRequisito> mergeOfCargaRequisitos(BusinessPersistance oBp, Objetoi solicitud,
			RequisitosForm form) {

		List<CargaRequisito> lstCargaRq = oBp
				.getByFilter("SELECT r FROM CargaRequisito r WHERE r.credito.id = " + solicitud.getId());

		String q = "SELECT r FROM Requisito r WHERE r.linea.id  = " + solicitud.getLinea_id();
		if (form.getPersonasFisicas() != null && form.getPersonasFisicas().booleanValue()
				&& form.getPersonasJuridicas() != null && form.getPersonasJuridicas().booleanValue()) {
			q += " ORDER BY r.familia, r.orden, r.tipoPersona ";
		} else {
			q += " AND (";
			if (form.getPersonasFisicas() != null && form.getPersonasFisicas().booleanValue()) {
				q += "r.tipoPersona = 'F' OR ";
			}
			if (form.getPersonasJuridicas() != null && form.getPersonasJuridicas().booleanValue()) {
				q += "r.tipoPersona = 'J' OR ";
			}
			q += "r.tipoPersona = 'A') ORDER BY r.familia, r.orden, r.tipoPersona ";
		}

		List<Requisito> requisitosPosibles = oBp.getByFilter(q);

		List<Requisito> requisitoCargados = new ArrayList<Requisito>();
		List<Requisito> requisitosFaltantes = new ArrayList<Requisito>();

		for (CargaRequisito cargaRequisito : lstCargaRq) {
			requisitoCargados.add(cargaRequisito.getRequisito());
		}

		for (Requisito requisito : requisitosPosibles) {
			if (!requisitoCargados.contains(requisito)) {
				requisitosFaltantes.add(requisito);
			}
		}

		for (Requisito requisito : requisitosFaltantes) {
			CargaRequisito carga = new CargaRequisito();
			carga.setRequisito(requisito);
			lstCargaRq.add(carga);
		}

		Collections.sort(lstCargaRq, new Comparator<CargaRequisito>() {

			@Override
			public int compare(CargaRequisito o1, CargaRequisito o2) {

				String t1 = o1.getRequisito().getFamilia();
				String t2 = o2.getRequisito().getFamilia();
				int comparacion = t1.compareTo(t2);
				if (comparacion == 0) {
					return o1.getRequisito().getOrden().compareTo(o2.getRequisito().getOrden());
				}
				return comparacion;
			}
		});

		return lstCargaRq;
	}

	// public List<CargaRequisito> ordenarListaPorId(List<CargaRequisito> lista){
	// CargaRequisito ini = lista.get(0);
	// List<CargaRequisito> listaOrdenada = new ArrayList<CargaRequisito>();
	//
	// listaOrdenada.add(ini);
	//
	// for (CargaRequisito cargaRequisito : lista) {
	// if (LIST.getRequisito().getId()<cargaRequisito.getRequisito().getId()){
	// listaOrdenada.add(ini);
	// }else{
	// listaOrdenada.add(cargaRequisito);
	// }
	// ini = cargaRequisito;
	// }
	// }

	@SuppressWarnings("unchecked")
	public ActionForward load(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		RequisitosForm requisitosForm = (RequisitosForm) form;
		idSolicitud = request.getParameter("idSolicitud");
		String idRequisito = request.getParameter("idEntity");
		Requisito requisito = (Requisito) oBp.getById(Requisito.class, new Long(idRequisito));
		Objetoi solicitud = (Objetoi) oBp.getByFilter("SELECT o FROM Objetoi o WHERE o.id = " + idSolicitud).get(0);
		List<CargaRequisito> requisitoList = oBp.getByFilter("SELECT c FROM CargaRequisito c WHERE c.credito.id = "
				+ idSolicitud + " AND c.requisito.id = " + idRequisito);
		if (requisitoList.isEmpty()) {
			CargaRequisito carga = new CargaRequisito();
			carga.setRequisito(requisito);
			requisitoList.add(carga);
		}
		requisitosForm.setObligatorio(requisito.getObligatorio());
		requisitosForm.setCargaRqList(requisitoList);
		requisitosForm.setCredito(solicitud);
		String estadoActual = solicitud.getEstadoActual().getEstado().getNombreEstado();
		if (estadoActual == null || estadoActual.equalsIgnoreCase("ESPERANDO DOCUMENTACION")) {
			requisitosForm.setRequisitosCumplidos(new Boolean(false));
		} else {
			requisitosForm.setRequisitosCumplidos(new Boolean(true));
		}
		request.setAttribute("requisitosForm", requisitosForm);
		request.setAttribute("idSolicitud", idSolicitud);
		return mapping.findForward("CargaRequisito");
	}

	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		RequisitosForm requisitosForm = (RequisitosForm) form;
		HashMap<String, String> requisitos = requisitosForm.getRequisitos();
		HashMap<String, String> observaciones = requisitosForm.getObservaciones();
		HashMap<String, String> ids = requisitosForm.getIds();
		HashMap<String, String> fechas = requisitosForm.getFechas();
		Objetoi solicitud = null;
		Requisito requisito = null;
		HashMap<String, FormFile> formFiles = requisitosForm.getTestFiles();
		if (!isCancelled(request)) {
			for (String requisitoStr : requisitos.values()) {

				CargaRequisito cargaRequisito = null;
				if (ids.get(requisitoStr) != null && !ids.get(requisitoStr).isEmpty()) {
					cargaRequisito = (CargaRequisito) oBp.getById(CargaRequisito.class,
							new Long(ids.get(requisitoStr)));
				} else {
					cargaRequisito = new CargaRequisito();
					cargaRequisito.setId(null);
				}
				if (idSolicitud != null && !idSolicitud.equals("")) {
					solicitud = (Objetoi) oBp.getById(Objetoi.class, new Long(idSolicitud));
					cargaRequisito.setCredito(solicitud);
				} else {
					cargaRequisito.setCredito(null);
				}
				if (requisitos.get(requisitoStr) != null) {
					requisito = (Requisito) oBp.getById(Requisito.class, new Long(requisitos.get(requisitoStr)));
					cargaRequisito.setRequisito(requisito);
				} else {
					cargaRequisito.setRequisito(null);
				}
				if (observaciones.get(requisitoStr) != null) {
					cargaRequisito.setObservaciones(observaciones.get(requisitoStr));
				} else {
					cargaRequisito.setObservaciones(null);
				}

				if (fechas.get(requisitoStr) != null) {
					cargaRequisito.setFechaCumplimientoStr(fechas.get(requisitoStr));
				} else {
					cargaRequisito.setFechaCumplimiento(null);
				}

				ObjetoiArchivo objetoiArchivo = null;
				FormFile theFile = formFiles.get(requisitoStr);
				if (theFile != null && theFile.getFileSize() != 0) {
					if (cargaRequisito.getObjetoiArchivo() == null) {
						cargaRequisito.setObjetoiArchivo(new ObjetoiArchivo());
						cargaRequisito.getObjetoiArchivo().setCredito(solicitud);
					}
					objetoiArchivo = cargaRequisito.getObjetoiArchivo();

					try {
						objetoiArchivo.setArchivo(theFile.getFileData());
						objetoiArchivo.setMimetype(theFile.getContentType());
						objetoiArchivo.setNombre(theFile.getFileName());
						objetoiArchivo.setFechaAdjunto(new Date());
						objetoiArchivo.setNoBorrar(true);
						objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					oBp.save(objetoiArchivo);

				}

				// //validar Carga de Requisito
				ActionMessages rta = validateAll(cargaRequisito);
				if (!rta.isEmpty()) {
					saveErrors(request, rta);
					return mapping.findForward("errorPage");
				}

				oBp.saveOrUpdate(cargaRequisito);
			}
		}
		if (solicitud != null) {
			if (solicitud.getEstadoActual() != null) {
				if (solicitud.getEstadoActual().getEstado().getNombreEstado()
						.equalsIgnoreCase("ESPERANDO DOCUMENTACION")) {
					requisitosForm.setRequisitosCumplidos(false);
				} else {
					requisitosForm.setRequisitosCumplidos(true);
				}
			}
		}
		request.setAttribute("requisitosForm", form);
		request.setAttribute("idSolicitud", idSolicitud);
		return list(mapping, form, request, response);
	}

	public ActionMessages validateAll(CargaRequisito cargaRequisito) {

		ActionMessages actionMessages = new ActionMessages();
		return actionMessages;
	}

	public String addTimeMark(String name) {
		String rta = "";
		String[] aux = name.split("\\.");
		int lastposition = aux.length;
		GregorianCalendar calendar = new GregorianCalendar();
		for (String string : aux) {
			if (string.equalsIgnoreCase(aux[lastposition - 1])) {
				rta += calendar.getTimeInMillis() + ".";
			}
			rta += string;
		}
		return rta;
	}

	@SuppressWarnings("unchecked")
	public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String id = request.getParameter("entity.id");
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		ArrayList<CargaRequisito> cargaRqs = (ArrayList<CargaRequisito>) oBp
				.getByFilter("SELECT s from CargaRequisito s where s.id= " + id);

		if (cargaRqs.isEmpty() || cargaRqs.size() == 0) {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("CargaRequisito.delete",
					new ActionMessage("CargaRequisito.delete", "CargaRequisito.delete"));
			saveErrors(request, actionMessages);
			return mapping.findForward("errorPage");
		}

		CargaRequisito cargaRq = cargaRqs.get(0);
		Objetoi credito = cargaRq.getCredito();
		if (cargaRq.getRequisito().getObligatorio() && credito.getExpediente() != null
				&& !credito.getExpediente().isEmpty() && !credito.getExpediente().equals("0")) {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("CargaRequisito.deleteObligatorioConExpediente", new ActionMessage(
					"CargaRequisito.deleteObligatorioConExpediente", "CargaRequisito.deleteObligatorioConExpediente"));
			saveErrors(request, actionMessages);
			return mapping.findForward("errorBlancoPage");
		}
		request.setAttribute("idSolicitud", cargaRq.getCredito().getId().toString());
		boolean rtaDeletion = false;
		try {
			cargaRq.setFechaCumplimiento(null);
			cargaRq.setObservaciones(null);
			ObjetoiArchivo oa = cargaRq.getObjetoiArchivo();
			cargaRq.setObjetoiArchivo(null);
			oBp.update(cargaRq);
			oBp.delete(oa);
			verificarEstado(cargaRq.getCredito(), "delete");
			rtaDeletion = true;
			request.setAttribute("requisitosCumplidos", new Boolean(false));
			((RequisitosForm) form).setRequisitosCumplidos(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (rtaDeletion) {
			return list(mapping, form, request, response);
		} else {
			ActionMessages actionMessages = new ActionMessages();
			actionMessages.add("CargaRequisito.delete",
					new ActionMessage("CargaRequisito.delete", "CargaRequisito.delete"));
			saveErrors(request, actionMessages);
			return mapping.findForward("errorPage");
		}
	}

	// -----------------------------------------------------------------------------------------------
	private void verificarEstado(Objetoi solicitud, String tipo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Long requisitos = (Long) bp.createQuery(
				"SELECT COUNT (r) from Requisito r where linea = :linea AND r.obligatorio = true and (r.tipoPersona = :tipoPersona OR r.tipoPersona = 'A')")
				.setParameter("linea", solicitud.getLinea())
				.setString("tipoPersona", solicitud.getPersona().getTipo12()).uniqueResult();
		Long cumplidos = (Long) bp.createQuery(
				"SELECT COUNT(c) FROM CargaRequisito c WHERE c.requisito.obligatorio = true AND c.credito = :solicitud AND (c.fechaCumplimiento IS NOT NULL or c.observaciones='N/C')")
				.setParameter("solicitud", solicitud).uniqueResult();
		Long cant = requisitos - cumplidos;
		Estado estado = null;
		CreditoHandler creditoHandler = new CreditoHandler();
		ObjetoiEstado estadoAnterior = creditoHandler.findObjetoiEstado(solicitud);
		ObjetoiEstado objetoiEstado = new ObjetoiEstado();
		if (cant <= 0) {
			try {
				estado = (Estado) bp
						.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = 'ANALISIS' and tipo='Objetoi' ")
						.uniqueResult();
			} catch (NonUniqueResultException nue) {
				objetoiEstado = null;
			} catch (NoResultException nre) {
				objetoiEstado = null;
			}
		} else if (cant > 0) {
			try {
				estado = (Estado) bp
						.createQuery("SELECT e FROM Estado e "
								+ "WHERE e.nombreEstado = 'ESPERANDO DOCUMENTACION'  and tipo='Objetoi' ")
						.uniqueResult();
			} catch (NonUniqueResultException nue) {
				objetoiEstado = null;
			} catch (NoResultException nre) {
				objetoiEstado = null;
			}
		}
		objetoiEstado.setEstado(estado);
		objetoiEstado.setObjetoi(solicitud);
		objetoiEstado.setObservacion("Modificado por " + SessionHandler.getCurrentSessionHandler().getCurrentUser());
		if (estadoAnterior != null && estadoAnterior.getEstado() != objetoiEstado.getEstado()) {
			if (tipo.equals("save") && estadoAnterior.getEstado().getNombreEstado().equals("ANALISIS")) {

			} else {
				estadoAnterior.setFechaHasta(new Date());
				objetoiEstado.setFecha(new Date());
				bp.update(estadoAnterior);
				bp.save(objetoiEstado);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		idSolicitud = (String) request.getAttribute("idSolicitud");
		if (idSolicitud == null || idSolicitud.equals(""))
			idSolicitud = request.getParameter("idSolicitud");

		Objetoi solicitud = (Objetoi) oBp.getById(Objetoi.class, new Long(idSolicitud));
		RequisitosForm requisitosForm = (RequisitosForm) form;

		requisitosForm.setCredito(solicitud);
		requisitosForm.setCargaRqList(mergeOfCargaRequisitos(oBp, solicitud, requisitosForm));
		if (solicitud.getEstadoActual() == null || solicitud.getEstadoActual().getEstado() == null
				|| solicitud.getEstadoActual().getEstado().getNombreEstado() == null
				|| !solicitud.getEstadoActual().getEstado().getNombreEstado().equals("ANALISIS")) {
			requisitosForm.setRequisitosCumplidos(false);
		} else {
			requisitosForm.setRequisitosCumplidos(true);
		}

		request.setAttribute("requisitosForm", requisitosForm);
		request.setAttribute("idSolicitud", idSolicitud);
		return mapping.findForward("CargaRequisitoList");
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public ActionForward comprobarRequisito(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

		URL url = new URL(u.toString() + "/reports/requisitos.jasper");
		reporte = (JasperReport) JRLoader.loadObject(url.openStream());

		idSolicitud = (String) request.getAttribute("idSolicitud");
		if (idSolicitud == null || idSolicitud.equals("")) {
			idSolicitud = request.getParameter("idSolicitud");
		}
		Objetoi solicitud = (Objetoi) oBp.getById(Objetoi.class, new Long(idSolicitud));
		RequisitosForm requisitosForm = (RequisitosForm) form;
		requisitosForm.setCredito(solicitud);
		requisitosForm.setCargaRqList(mergeOfCargaRequisitos(oBp, solicitud, requisitosForm));
		request.setAttribute("requisitosForm", requisitosForm);
		request.setAttribute("idSolicitud", idSolicitud);

		if (solicitud != null) {
			generarInformeRequisitos(solicitud);
		}
		response.setHeader("Content-Disposition", "attachment;filename=\"ComprobarRequisitos.pdf\"");

		HashMap param = new HashMap();

		param.put("OBJETOI_ID", solicitud.getId().intValue());
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		param.put("relacionCotomador_id", relacionCotomadorId);
		param.put("SCHEMA", BusinessPersistance.getSchema());
		param.put("REPORTS_PATH", u.toString());

		Connection conn = oBp.getCurrentSession().connection();

		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, param, conn);
		byte[] pdfNotificacion = JasperExportManager.exportReportToPdf(jasperPrint);
		if (solicitud.getEstadoActual().getEstado().getNombreEstado().equals("ESPERANDO DOCUMENTACION")) {
			requisitosForm.setRequisitosCumplidos(new Boolean(false));
		}
		response.setContentType("application/pdf");
		response.setContentLength(pdfNotificacion.length);
		response.getOutputStream().write(pdfNotificacion);
		response.getOutputStream().close();

		return null;
	}

	public ActionForward saveCumplimiento(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		idSolicitud = (String) request.getAttribute("idSolicitud");
		if (idSolicitud == null || idSolicitud.equals(""))
			idSolicitud = request.getParameter("idSolicitud");
		Objetoi solicitud = (Objetoi) oBp.getById(Objetoi.class, new Long(idSolicitud));
		RequisitosForm requisitosForm = (RequisitosForm) form;
		ArrayList<CargaRequisito> crs = (ArrayList<CargaRequisito>) mergeOfCargaRequisitos(oBp, solicitud,
				requisitosForm);
		// verifica los ANALISIS y les asigna la feha de
		// cumplimiento
		for (CargaRequisito cr : crs) {
			if (requisitosForm.getSelecciones().containsKey(cr.getRequisito().getId().toString())) {
				Boolean ac = requisitosForm.getSelecciones().get(cr.getRequisito().getId().toString());
				if (ac.booleanValue()) {
					if (cr.getFechaCumplimiento() == null) { // agregado, si tiene fecha de cumplimiento, no lo
						// actualizo
						cr.setFechaCumplimiento(new Date());
					}
					cr.setCredito(solicitud);
					if (cr.getObservaciones() == null || !cr.getObservaciones().equals("N/C")) {
						cr.setObservaciones("");
					}
					// cr.setArchivo("");
					oBp.saveOrUpdate(cr);
				}
			}

			Boolean bol = requisitosForm.getNc().containsKey(cr.getRequisito().getId().toString());
			if (bol != null && bol.booleanValue()) {
				cr.setObservaciones("N/C");
				cr.setCredito(solicitud);
				oBp.saveOrUpdate(cr);
			}
		}

		requisitosForm.setCargaRqList(crs);
		request.setAttribute("requisitosForm", requisitosForm);
		request.setAttribute("idSolicitud", idSolicitud);
		verificarEstado(solicitud, "save");
		if (solicitud.getEstadoActual().getEstado().getNombreEstado().equals("ANALISIS")) {
			requisitosForm.setRequisitosCumplidos(true);
		} else {
			requisitosForm.setRequisitosCumplidos(false);
		}
		return list(mapping, form, request, response);
	}

	public ActionForward omitirRequisitosObligatorios(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		idSolicitud = (String) request.getAttribute("idSolicitud");
		if (idSolicitud == null || idSolicitud.equals(""))
			idSolicitud = request.getParameter("idSolicitud");
		Objetoi solicitud = (Objetoi) oBp.getById(Objetoi.class, new Long(idSolicitud));
		CreditoHandler creditoHandler = new CreditoHandler();
		ObjetoiEstado estadoAnterior = creditoHandler.findObjetoiEstado(solicitud);
		ObjetoiEstado objetoiEstado = new ObjetoiEstado();
		objetoiEstado.setFecha(new Date());
		try {
			if (!estadoAnterior.getEstado().getNombreEstado().equals("ANALISIS")) {
				Estado estado = (Estado) oBp
						.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = 'ANALISIS' and tipo='Objetoi'")
						.uniqueResult();
				objetoiEstado.setEstado(estado);
				objetoiEstado.setObservacion("Omisi�n de requisitos obligatorios. Otorgado por "
						+ SessionHandler.getCurrentSessionHandler().getCurrentUser());
				if (estadoAnterior != null) {
					estadoAnterior.setFechaHasta(new Date());
					oBp.update(estadoAnterior);
				}
				((RequisitosForm) form).setRequisitosCumplidos(true);
				objetoiEstado.setObjetoi(solicitud);
				oBp.save(objetoiEstado);
			}
		} catch (NonUniqueResultException nue) {
			objetoiEstado = null;
		} catch (NoResultException nre) {
			objetoiEstado = null;
		}
		return list(mapping, form, request, response);
	}

	public void generarInformeRequisitos(Objetoi solicitud) throws JRException, IOException {

	}

	public String getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
}
