package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.RolesPersona;

public class Roles implements IProcess {
	private String forward = "Roles";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	private Persona persona;
	private List<RolesPersona> rolesPersonas;

	public Roles() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.persona = new Persona();
	}

	@SuppressWarnings({ "unchecked" })
	public boolean doProcess() {
		this.persona = (Persona) this.bp.getById(Persona.class, this.persona.getId());
		try {
			this.rolesPersonas = bp
					.createQuery("FROM " + RolesPersona.class.getCanonicalName() + " rp WHERE rp.persona = :persona")
					.setEntity("persona", this.persona).list();

		} catch (HibernateException e) {
			this.errores.put("error.persona.roles", "error.persona.roles");
			e.printStackTrace();
		}
		return this.errores.isEmpty();
	}

	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Object getResult() {
		return null;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public boolean validate() {
		return this.errores.isEmpty();
	}

	public void setRolesPersonas(List<RolesPersona> rolesPersonas) {
		this.rolesPersonas = rolesPersonas;
	}

	public List<RolesPersona> getRolesPersonas() {
		return rolesPersonas;
	}
}