package com.asf.cred.business;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Liberaciones;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;

public class InmovilizacionRealizada implements IProcess {

	protected ObjetoiVinedo vinedo = new ObjetoiVinedo();
	protected ArrayList<ObjetoiVinedo> vinedos;

	protected Objetoi credito;
	protected List<Objetoi> inmovilizaciones;

	private String accion;
	private String forward;
	private Long id;
	private String ruta;
	private Garantia garantia = new Garantia();
	private boolean detalle = false;

	private Long idCosechaConfig;
	private List<Liberaciones> liberaciones;

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (accion == null) {
			detalle = false;
			forward = "InmovilizacionRealizadaList";
		} else if (accion.equalsIgnoreCase("listar")) {
			if (idCosechaConfig == null) {
				return false;
			}
			CosechaConfig config = (CosechaConfig) bp.getById(CosechaConfig.class, idCosechaConfig);
			inmovilizaciones = bp
					.createQuery("SELECT i FROM Inmovilizaciones i WHERE i.estado = 'REALIZADA' "
							+ "AND i.objetoi.fechaSolicitud >= :fechaDesde AND i.objetoi.fechaSolicitud <= :fechaHasta "
							+ "ORDER BY i.fechaAceptacion DESC, i.fecha DESC")
					.setParameter("fechaDesde", config.getFechaInicioPer())
					.setParameter("fechaHasta", config.getFechaFinPer()).list();
			detalle = false;
			forward = "InmovilizacionRealizadaList";
		} else if (accion.equalsIgnoreCase("ver")) {
			credito = (Objetoi) bp.getById(Objetoi.class, id);
			comprobarVinedo();
			setGarantia(buscarGarantia());

			liberaciones = bp
					.createQuery("SELECT l FROM Liberaciones l "
							+ "WHERE l.objetoi.id = :objetoi_id AND l.estado = 'REALIZADA' " + "ORDER BY fecha")
					.setLong("objetoi_id", id).list();

			/* Agrego query de inmovilizaciones para mostrar el detalle. Ticket 9156 */
			inmovilizaciones = bp.createQuery(
					"SELECT i FROM Inmovilizaciones i WHERE i.objetoi.id = :objetoi_id AND i.estado = 'REALIZADA' "
							+ "ORDER BY i.fechaAceptacion DESC, i.fecha DESC")
					.setLong("objetoi_id", id).list();

			detalle = true;
			forward = "InmovilizacionRealizadaList";
		}
		return true;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	public boolean isDetalle() {
		return detalle;
	}

	public void setDetalle(boolean detalle) {
		this.detalle = detalle;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public List<Liberaciones> getLiberaciones() {
		return liberaciones;
	}

	public ObjetoiVinedo getVinedo() {
		return vinedo;
	}

	public void setVinedo(ObjetoiVinedo vinedo) {
		this.vinedo = vinedo;
	}

	public ArrayList<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(ArrayList<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public Objetoi comprobarVinedo() {
		if (credito.getPersona() == null) {
			credito.setPersona(new Persona());
		}
		return credito;
	}

	public Garantia buscarGarantia() {
		List<Garantia> garantias = new ArrayList<Garantia>();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		garantias = bp
				.createQuery("SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja is NULL")
				.setEntity("credito", credito).list();
		if (garantias.isEmpty()) {
			garantia = new Garantia();
		} else {
			garantia = garantias.get(0);
		}
		return garantia;
	}

	public Garantia setGarantia(Garantia garantia) {
		this.garantia = garantia;
		return garantia;
	}

	public Garantia getGarantia() {
		return garantia;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public List<Objetoi> getInmovilizaciones() {
		return inmovilizaciones;
	}

	public void setInmovilizaciones(List<Objetoi> inmovilizaciones) {
		this.inmovilizaciones = inmovilizaciones;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		List<String> lineas = IOUtils.readLines(new FileReader("/home/andres/Descargas/ValoresDeTasas.txt"));

		PrintWriter out = new PrintWriter(
				new BufferedWriter(new FileWriter("/home/andres/Descargas/ValoresDeTasas.sql")));

		for (String linea : lineas) {
			TasaBean tasa = new TasaBean(linea);
			if (tasa.getTasa() == 1) {
				out.println(tasa.getSQL());
			}
		}

		out.flush();
		out.close();
	}

	public void setIdCosechaConfig(Long idCosechaConfig) {
		this.idCosechaConfig = idCosechaConfig;
	}

	public Long getIdCosechaConfig() {
		return idCosechaConfig;
	}

	private static class TasaBean {
		private static SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy hh:mm:ss");
		private static SimpleDateFormat sqlFormat = new SimpleDateFormat("yyyy-MM-dd");
		private static String insert = "INSERT INTO IndiceValor (cantidad,fecha,importe,observaciones,valor,indice_id) "
				+ "VALUES (0, '@FECHA', 0, '', @VALOR, @TASA);";

		private Date fecha;
		private int tasa;
		private double valor;

		public TasaBean(String linea) throws ParseException {
			String[] partes = linea.split("\\|");
			fecha = format.parse(partes[0]);
			tasa = Integer.parseInt(partes[1]);
			valor = Double.parseDouble(partes[2].replace(',', '.'));
		}

		public String getSQL() {
			return insert.replace("@FECHA", sqlFormat.format(fecha)).replace("@VALOR", Double.toString(valor))
					.replace("@TASA", Integer.toString(tasa));
		}

		public int getTasa() {
			return tasa;
		}
	}
}
