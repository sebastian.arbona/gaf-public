package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.gaf.hibernate.CuentaBancaria;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;

/**
 * @author eselman
 */

public class DatosBancarios implements IProcess
{
//=========================ATRIBUTOS===========================
	//atributos utilitarios del proceso.-
	private String forward = "DatosBancariosList";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	//atributos del proceso.-
	private Persona persona;
	private String idBanco;
	private String idTipoCuentaBancaria;
	private CuentaBancaria cuentaBancaria;
	private List<CuentaBancaria> cuentasBancarias;
	private String titularCuenta;
	private String cuitTitular;
//=======================CONSTRUCTORES=========================
	public DatosBancarios()
	{
		this.errores        = new HashMap<String, Object>();
		this.bp		        = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.persona        = new Persona();
		this.cuentaBancaria = new CuentaBancaria();
	}
//======================FUNCIONALIDADES========================
	public boolean doProcess()
	{
		if(this.getCuentaBancaria().getFechaApertura() == null)
		{
			this.getCuentaBancaria().setFechaApertura(new Date());
		}
		
		if(this.accion.equals("nuevo"))
		{
			this.forward = "DatosBancarios";
		}
		else if(this.accion.equals("guardar"))
		{
			this.validarCuentaBancaria();
			if(this.errores.isEmpty())
			{
				this.guardarCuentaBancaria();
				if(this.errores.isEmpty())
				{
					this.forward = "DatosBancariosList";
				}
				else
				{
					this.forward = "DatosBancarios";
				}
			}
			else
			{
				this.forward = "DatosBancarios";
			}
		}
		else if(this.accion.equals("cancelar"))
		{
			this.forward = "DatosBancariosList";
		}
		else if(this.accion.equals("load"))
		{
			this.cuentaBancaria = (CuentaBancaria) this.bp.getById(CuentaBancaria.class, this.cuentaBancaria.getId());
			this.idBanco = cuentaBancaria.getBanco();
			this.idTipoCuentaBancaria = cuentaBancaria.getTipoCuentaBancaria();
			this.titularCuenta = cuentaBancaria.getTitularCuenta();
			this.cuitTitular = cuentaBancaria.getCuitTitular();
			this.forward = "DatosBancarios";
		}
		else if(this.accion.equals("eliminar"))
		{
			this.eliminarCuentaBancaria();
		}
		
		this.listarCuentasBancarias();
		
		return this.errores.isEmpty();
	}

//=====================UTILIDADES PRIVADAS=====================
	private void listarCuentasBancarias()
	{
		if( this.persona != null)
		{
			try
			{
				this.persona = (Persona)this.bp.getById(Persona.class, this.persona.getId());
				
				String consulta = "";
				consulta += "SELECT cb FROM CuentaBancaria cb WHERE";
				consulta += " cb.persona.id=" + this.persona.getId();
								
				this.cuentasBancarias = this.bp.getByFilter(consulta);
			}
			catch( Exception e )
			{
				this.cuentasBancarias = new ArrayList<CuentaBancaria>();
				e.printStackTrace();
			}
		}
	}
	
	private void validarCuentaBancaria()
	{
		this.cuentaBancaria.validate();
		this.errores.putAll(this.cuentaBancaria.getChErrores());
	}
	
	private void guardarCuentaBancaria()
	{
		try
		{
			this.persona                     = (Persona)this.bp.getById(Persona.class, this.persona.getId());
			this.cuentaBancaria.setPersona(this.persona);
			this.cuentaBancaria.setBanco(idBanco);
			this.cuentaBancaria.setTipoCuentaBancaria(idTipoCuentaBancaria);
			this.cuentaBancaria.setTitularCuenta(titularCuenta);
			this.cuentaBancaria.setCuitTitular(cuitTitular);
			this.bp.saveOrUpdate(this.cuentaBancaria);
			this.cuentaBancaria = new CuentaBancaria();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void eliminarCuentaBancaria()
	{
		try {
			
			this.cuentaBancaria = (CuentaBancaria) this.bp.getById(CuentaBancaria.class, this.cuentaBancaria.getId());
			this.bp.delete(this.cuentaBancaria);
			this.cuentaBancaria = new CuentaBancaria();
			
		}catch(Exception e) {
			
			if(e.getMessage().contains("could not delete")) {
				this.errores.put("cuentaBancaria.delete.constraint", "cuentaBancaria.delete.constraint");
			}else {
				this.errores.put("cuentaBancaria.delete.unknow", "cuentaBancaria.delete.unknow");
			}
		}
		
	}
//======================GETTERS Y SETTERS======================
	public HashMap<String, Object> getErrors()
	{
		return this.errores;
	}
	public String getForward()
	{
		return this.forward;
	}
	public String getInput()
	{
		return this.forward;
	}
	public Object getResult()
	{
		return null;
	}
	public Persona getPersona() 
	{
		return this.persona;
	}
	public void setPersona(Persona persona) 
	{
		this.persona = persona;
	}
	public String getAccion()
	{
		return this.accion;
	}
	public void setAccion(String accion)
	{
		this.accion = accion;
	}
	public CuentaBancaria getCuentaBancaria() 
	{
		return this.cuentaBancaria;
	}
	public String getIdBanco() {
		return idBanco;
	}
	public void setIdBanco(String idBanco) {
		this.idBanco = idBanco;
	}
	public String getIdTipoCuentaBancaria() {
		return idTipoCuentaBancaria;
	}
	public void setIdTipoCuentaBancaria(String idTipoCuentaBancaria) {
		this.idTipoCuentaBancaria = idTipoCuentaBancaria;
	}
	public void setCuentaBancaria(CuentaBancaria cuentaBancaria) 
	{
		this.cuentaBancaria = cuentaBancaria;
	}
	public List<CuentaBancaria> getCuentasBancarias() 
	{
		return this.cuentasBancarias;
	}
	public void setCuentasBancarias(List<CuentaBancaria> cuentasBancarias) 
	{
		this.cuentasBancarias = cuentasBancarias;
	}
	
	
	public String getTitularCuenta() {
		return titularCuenta;
	}
	public void setTitularCuenta(String titularCuenta) {
		this.titularCuenta = titularCuenta;
	}
	public String getCuitTitular() {
		return cuitTitular;
	}
	public void setCuitTitular(String cuitTitular) {
		this.cuitTitular = cuitTitular;
	}
	//========================VALIDACIONES=========================
	public boolean validate()
	{
		return this.errores.isEmpty();
	}
	
}