package com.asf.cred.business;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.struts.upload.FormFile;
import org.hibernate.Query;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.TipificadorHelper;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class ImportacionMovimientosProcess extends ConversationProcess {

	private static final String CAMPO_PROYECTO = "Proyecto";
	private static final String CAMPO_FECHA_PROCESO = "Fecha proceso";
	private static final String CAMPO_FECHA_VENCIMIENTO = "Fecha Vencimiento";
	private static final String CAMPO_FECHA_GENERACION = "Fecha";
	private static final String CAMPO_NUMERO_CUOTA = "Numero Cuota";
	private static final String CAMPO_IMPORTE = "Importe";
	private static final String CAMPO_CONCEPTO = "Concepto";
	private static final String CAMPO_TIPO_CONCEPTO = "Tipo Concepto";

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

	private FormFile archivo;
	@Save
	private List<BeanMovimiento> movimientos;
	@Save
	private List<Ctacte> resultado;
	private String separadorCampos = ";";
	private String separadorDecimal = ",";
	private String charset = "iso-8859-1";
	private String tipoBoleto = Tipomov.TIPOMOV_DEBITO;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean importar() {
		if (archivo == null) {
			errors.put("importacionMovimientos.archivo", "importacionMovimientos.archivo");
			return false;
		}

		try {
			resultado = new ArrayList<Ctacte>();
			movimientos = new ArrayList<ImportacionMovimientosProcess.BeanMovimiento>();

			byte[] data = archivo.getFileData();

			List<String> contenido = IOUtils.readLines(new ByteArrayInputStream(data), charset);

			String sep = separadorCampos;
			if (separadorCampos.equals("tab")) {
				sep = "\t";
			}

			String header = contenido.get(0);
			List<String> campos = Arrays.asList(header.split(sep));

			for (int i = 1; i < contenido.size(); i++) {
				try {
					String linea = contenido.get(i);
					String[] valores = linea.split(sep);

					BeanMovimiento bean = new BeanMovimiento();
					bean.setNumeroAtencion(new Long(valores[campos.indexOf(CAMPO_PROYECTO)]));
					String fp = valores[campos.indexOf(CAMPO_FECHA_PROCESO)];
					if (fp.isEmpty())
						bean.setFechaProceso(new Date());
					else
						bean.setFechaProceso(DATE_FORMAT.parse(fp));
					bean.setFechaVencimiento(DATE_FORMAT.parse(valores[campos.indexOf(CAMPO_FECHA_VENCIMIENTO)]));
					bean.setFechaGeneracion(DATE_FORMAT.parse(valores[campos.indexOf(CAMPO_FECHA_GENERACION)]));
					bean.setNumeroCuota(new Integer(valores[campos.indexOf(CAMPO_NUMERO_CUOTA)]));
					bean.setImporte(new Double(valores[campos.indexOf(CAMPO_IMPORTE)].replace(separadorDecimal, ".")));
					bean.setConcepto(valores[campos.indexOf(CAMPO_CONCEPTO)]);
					bean.setTipoConcepto(valores[campos.indexOf(CAMPO_TIPO_CONCEPTO)]);

					movimientos.add(bean);
				} catch (ArrayIndexOutOfBoundsException ex) {
					System.err.println("Error linea " + i + ": " + ex.getClass() + ": " + ex.getMessage());
				} catch (NumberFormatException ex) {
					System.err.println("Error linea " + i + ": " + ex.getClass() + ": " + ex.getMessage());
				} catch (ParseException ex) {
					System.err.println("Error linea " + i + ": " + ex.getClass() + ": " + ex.getMessage());
				}
			}

			return true;
		} catch (IOException e) {
			e.printStackTrace();
			errors.put("importacionMovimientos.error", "importacionMovimientos.error");
			return false;
		}
	}

	@ProcessMethod
	public boolean aplicar() {
		Query q = bp.getNamedQuery("Objetoi.findByNroAtencion");
		Query queryCuota = bp.getNamedQuery("Cuota.findByNumero");

		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();

		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtacte = 1;

		resultado = new ArrayList<Ctacte>();

		bp.begin();
		try {
			for (BeanMovimiento b : movimientos) {
				Ctacte cc = new Ctacte();
				CtacteKey ck = new CtacteKey();
				Objetoi credito = (Objetoi) q.setParameter("nroAtencion", b.getNumeroAtencion()).uniqueResult();
				ck.setObjetoi(credito);
				ck.setPeriodoCtacte(new Long(periodo));
				ck.setVerificadorCtacte(0L);
				ck.setMovimientoCtacte(movimientoCtaCte);
				ck.setItemCtacte(itemCtacte++);

				cc.setId(ck);

				Concepto concepto = Concepto.buscarConcepto(b.getConcepto(), periodo);

				cc.setAsociado(concepto);
				cc.setFacturado(concepto);

				Cuota cuota = (Cuota) queryCuota.setParameter("idCredito", credito.getId())
						.setParameter("nroCuota", b.getNumeroCuota()).uniqueResult();
				cc.setCuota(cuota);

				cc.setDetalle(b.getDetalle());

				cc.setFechaGeneracion(b.getFechaGeneracion());
				cc.setFechaVencimiento(b.getFechaVencimiento());
				cc.setImporte(b.getImporte());
				cc.setTipoConcepto(b.getTipoConcepto());
				cc.setUsuario(usuario);

				if (tipoBoleto.equals(Tipomov.TIPOMOV_DEBITO)) {
					cc.setTipomov(tipoDebito);
					cc.setTipoMovimiento("movManualDeb");
				} else {
					cc.setTipomov(tipoCredito);
					cc.setTipoMovimiento("movManualCred");
				}

				CreditoHandler.cargarCotizacion(cc);

				bp.save(cc);

				Boleto boleto = new Boleto();
				boleto.setFechaEmision(cc.getFechaGeneracion());
				boleto.setFechaVencimiento(cc.getFechaGeneracion());
				if (tipoBoleto.equals(Tipomov.TIPOMOV_DEBITO)) {
					boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				} else {
					boleto.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				}
				boleto.setUsuario(usuario);
				boleto.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna cuota
				boleto.setObjetoi(credito);
				boleto.setPeriodoBoleto((long) periodo);
				boleto.setVerificadorBoleto(0L);
				boleto.setImporte(cc.getImporte());
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));

				bp.save(boleto);

				cc.setBoleto(boleto);
				bp.update(cc);

				Bolcon bolcon = new Bolcon();
				BolconKey bk = new BolconKey();
				bk.setBoleto(boleto);
				bk.setItemCtacte(cc.getId().getItemCtacte());
				bk.setMovimientoCtacte(movimientoCtaCte);
				bk.setPeriodoCtacte((long) periodo);
				bk.setVerificadorCtacte(0L);
				bolcon.setId(bk);

				bolcon.setCuota(cuota);
				bolcon.setFacturado(concepto);
				bolcon.setImporte(cc.getImporte());
				bolcon.setTipomov(cc.getTipomov());

				bp.save(bolcon);

				resultado.add(cc);
			}
			bp.commit();

			movimientos = new ArrayList<ImportacionMovimientosProcess.BeanMovimiento>();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			bp.rollback();
			errors.put("importacionMovimientos.errorAplicacion", "importacionMovimientos.errorAplicacion");
			resultado.clear();
			return false;
		}
	}

	@ProcessMethod
	public boolean exportar() {
		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "ImportacionMovimientos";
	}

	public FormFile getArchivo() {
		return archivo;
	}

	public void setArchivo(FormFile archivo) {
		this.archivo = archivo;
	}

	public List<BeanMovimiento> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<BeanMovimiento> movimientos) {
		this.movimientos = movimientos;
	}

	public List<Ctacte> getResultado() {
		return resultado;
	}

	public void setResultado(List<Ctacte> resultado) {
		this.resultado = resultado;
	}

	public String getSeparadorCampos() {
		return separadorCampos;
	}

	public void setSeparadorCampos(String separadorCampos) {
		this.separadorCampos = separadorCampos;
	}

	public String getSeparadorDecimal() {
		return separadorDecimal;
	}

	public void setSeparadorDecimal(String separadorDecimal) {
		this.separadorDecimal = separadorDecimal;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getTipoBoleto() {
		return tipoBoleto;
	}

	public void setTipoBoleto(String tipoBoleto) {
		this.tipoBoleto = tipoBoleto;
	}

	public class BeanMovimiento implements Serializable {

		private static final long serialVersionUID = -1779405245616306547L;

		private Long numeroAtencion;
		private Date fechaProceso;
		private Date fechaVencimiento;
		private Date fechaGeneracion;
		private Integer numeroCuota;
		private Double importe;
		private String concepto;
		private String tipoConcepto;
		private String detalle;

		public String getTipoConceptoNombre() {
			return TipificadorHelper.getString("ctacte.detalle", tipoConcepto);
		}

		public String getConceptoNombre() {
			if (concepto != null)
				return Concepto.buscarConcepto(concepto).getDescripcion();
			return null;
		}

		public Long getNumeroAtencion() {
			return numeroAtencion;
		}

		public void setNumeroAtencion(Long numeroAtencion) {
			this.numeroAtencion = numeroAtencion;
		}

		public Date getFechaProceso() {
			return fechaProceso;
		}

		public void setFechaProceso(Date fechaProceso) {
			this.fechaProceso = fechaProceso;
		}

		public Date getFechaVencimiento() {
			return fechaVencimiento;
		}

		public void setFechaVencimiento(Date fechaVencimiento) {
			this.fechaVencimiento = fechaVencimiento;
		}

		public Date getFechaGeneracion() {
			return fechaGeneracion;
		}

		public void setFechaGeneracion(Date fechaGeneracion) {
			this.fechaGeneracion = fechaGeneracion;
		}

		public Integer getNumeroCuota() {
			return numeroCuota;
		}

		public void setNumeroCuota(Integer numeroCuota) {
			this.numeroCuota = numeroCuota;
		}

		public Double getImporte() {
			return importe;
		}

		public void setImporte(Double importe) {
			this.importe = importe;
		}

		public String getConcepto() {
			return concepto;
		}

		public void setConcepto(String concepto) {
			this.concepto = concepto;
		}

		public String getTipoConcepto() {
			return tipoConcepto;
		}

		public void setTipoConcepto(String tipoConcepto) {
			this.tipoConcepto = tipoConcepto;
		}

		public String getDetalle() {
			return detalle;
		}

		public void setDetalle(String detalle) {
			this.detalle = detalle;
		}
	}
}
