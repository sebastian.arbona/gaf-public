package com.asf.cred.business;

import java.util.ArrayList;
import java.util.List;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.ProrrogaMasiva;

public class ProrrogaMasivaResult extends ConversationProcess {

	private List<Cuota> cuotasProrrogadas;
	private List<Cuota> cuotasFechaAnterior;
	private List<Cuota> cuotasFechaSuperpuesta;
	private List<Cuota> cuotasCaducidad;
	private ProrrogaMasiva prorroga;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscarResultado() {
		cuotasProrrogadas = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("cuotasProrrogadas");
		cuotasFechaAnterior = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("cuotasFechaAnterior");
		cuotasFechaSuperpuesta = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("cuotasFechaSuperpuesta");
		cuotasCaducidad = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("cuotasCaducidad");
		prorroga = (ProrrogaMasiva) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("prorroga");

		cuotasProrrogadas = load(cuotasProrrogadas);
		cuotasFechaAnterior = load(cuotasFechaAnterior);
		cuotasFechaSuperpuesta = load(cuotasFechaSuperpuesta);
		cuotasCaducidad = load(cuotasCaducidad);
		prorroga = (ProrrogaMasiva) bp.getById(ProrrogaMasiva.class, prorroga.getId());

		return true;
	}

	@SuppressWarnings("unchecked")
	private List<Cuota> load(List<Cuota> cuotas) {
		List<Cuota> retorno = cuotas;
		if (cuotas != null && !cuotas.isEmpty()) {
			List<Long> ids = new ArrayList<Long>();
			for (Cuota c : cuotas) {
				ids.add(c.getId());
			}
			if (!ids.isEmpty()) {
				retorno = bp.createQuery("select c from Cuota c where c.id in (:ids)").setParameterList("ids", ids)
						.list();
			}
		}
		return retorno;
	}

	@Override
	protected String getDefaultForward() {
		return "ProrrogaMasivaResult";
	}

	public List<Cuota> getCuotasProrrogadas() {
		return cuotasProrrogadas;
	}

	public List<Cuota> getCuotasFechaAnterior() {
		return cuotasFechaAnterior;
	}

	public List<Cuota> getCuotasFechaSuperpuesta() {
		return cuotasFechaSuperpuesta;
	}

	public List<Cuota> getCuotasCaducidad() {
		return cuotasCaducidad;
	}

	public ProrrogaMasiva getProrroga() {
		return prorroga;
	}
}
