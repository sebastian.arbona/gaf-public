package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Persona;

public class DatosImpositivos implements IProcess {
	private String forward = "DatosImpositivos";
	private HashMap<String, Object> errores;
	private String accion = "";
	private Persona persona;
	private ArrayList<DatosImpositivosPersona> impuestos;
	private String persona_id;

	public DatosImpositivos() {
		this.errores = new HashMap<String, Object>();
		this.persona = new Persona();
	}

	@Override
	@SuppressWarnings({ "unchecked", "static-access" })
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String schema = bp.getSchema();
		try {
			List<Object[]> objetos = bp
					.createSQLQuery("SELECT act.descripcion, imp.codigo, fi.fechaAlta, fi.fechaBaja FROM " + schema
							+ ".FInscripcion fi JOIN " + schema
							+ ".proveedor pro ON fi.idproveedor = pro.idproveedor JOIN " + schema
							+ ".ActividadGAF act ON fi.idactividad = act.idactividad JOIN " + schema
							+ ".impuesto imp ON imp.idimpuesto = act.idimpuesto WHERE pro.idpersona = "
							+ this.persona.getId())
					.list();
			this.impuestos = new ArrayList<DatosImpositivosPersona>();
			for (Object[] object : objetos) {
				DatosImpositivosPersona impuesto = new DatosImpositivosPersona();
				impuesto.setActividad(object[0].toString());
				impuesto.setImpuesto(object[1].toString());
				impuesto.setFechaAlta(DateHelper.getString((Date) object[2]));
				impuesto.setFechaBaja(DateHelper.getString((Date) object[3]));
				this.impuestos.add(impuesto);
			}
		} catch (HibernateException e) {
			this.errores.put("error.persona.impuesto", "error.persona.impuesto");
			e.printStackTrace();
		}
		return this.errores.isEmpty();
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

	public void setImpuestos(ArrayList<DatosImpositivosPersona> impuestos) {
		this.impuestos = impuestos;
	}

	public ArrayList<DatosImpositivosPersona> getImpuestos() {
		return impuestos;
	}

	public String getPersona_id() {
		return persona_id;
	}

	public void setPersona_id(String id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		persona = (Persona) bp.createQuery("select p from Persona p where p.id  = :id")
				.setLong("id", Long.parseLong(id)).uniqueResult();
		this.persona_id = (persona != null) ? id : null;
	}
}