package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Query;

import com.asf.cred.business.garantias.AccionGarantia;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.nirven.creditos.hibernate.CampoGarantia;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.EstadoInmovilizacion;
import com.nirven.creditos.hibernate.EstadoLiberacion;
import com.nirven.creditos.hibernate.FamiliaCampoGarantia;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.GarantiaCustodia;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.GarantiaSeguro;
import com.nirven.creditos.hibernate.GarantiaSeguroEstado;
import com.nirven.creditos.hibernate.GarantiaTasacion;
import com.nirven.creditos.hibernate.GarantiaTasacionInmueble;
import com.nirven.creditos.hibernate.GarantiaTasacionMueble;
import com.nirven.creditos.hibernate.GarantiaUso;
import com.nirven.creditos.hibernate.Inmovilizaciones;
import com.nirven.creditos.hibernate.Liberaciones;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.creditos.hibernate.ValorCampoGarantia;

/**
 * Proceso para administracion de Garantias
 * 
 * @author Cesar
 *
 */
public class GarantiaProcess implements IProcess {

	private Garantia garantia;

	private ObjetoiGarantia oGarantia;
	private Long oGarantiaId;

	private GarantiaCustodia custodiaActual;
	private GarantiaEstado estado = new GarantiaEstado();
	private GarantiaSeguro seguro = new GarantiaSeguro();
	private GarantiaTasacion tasacion = new GarantiaTasacion();

	private GarantiaTasacionMueble tasacionMueble = new GarantiaTasacionMueble();
	private GarantiaTasacionInmueble tasacionInmueble = new GarantiaTasacionInmueble();

	private String eFechaModificacion;
	private String fechaSustitucionStr;
	private String productoReferencia;
	String estadoAcuerdo;
	private boolean inmovilizacion = false;
	private HashMap<String, String> errores = new HashMap<String, String>();
	private String seleccion;
	private AccionGarantia accion;

	private boolean modifica = false;

	private boolean modTitular = false;
	private String forward = "ListarGarantia";

	private List<ValorCampoGarantia>[] valores;
	private static final int GENERAL = 0;
	private static final int INMUEBLE = 1;
	private static final int CUSTODIA = 2;
	private static final int SEGURO = 3;
	private static final int GRAVAMEN = 4;
	private static final int MUEBLE = 5;

	private Long idSolicitud;

	private Object result;

	private Long idTipo;
	private Double porcentaje;
	private String tipoProducto;
	private String cuit;

	private String mensajeError;
	private boolean habilitarCampos;

	private String nombreCampoIdentificador;

	private String idGarantiaInmovilizacion;

	private boolean ocultarCampos;
	private boolean campoId;
	private Objetoi credito;

	private boolean garantiaCompartidaOtrosProyectos;

	private ArrayList<Liberaciones> liberaciones;
	private ArrayList<Inmovilizaciones> inmovilizaciones;
	private Double saldo;
	private Double volumen;
	private boolean hayFiduciaria;

	private GarantiaSeguroEstado estadoSeguro;
	private Double aforo;

	private boolean modifValorCons = true;

	/**
	 * Constructor por defecto que inicializa las variables de instancia
	 */
	@SuppressWarnings("unchecked")
	public GarantiaProcess() {
		garantia = new Garantia();
		oGarantia = new ObjetoiGarantia();
		valores = (List[]) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("GARANTIA_VALORES");
		custodiaActual = new GarantiaCustodia();
		if (valores == null) {
			resetValores();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.asf.struts.action.IProcess#doProcess()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		mensajeError = null;
		if (accion == null) {
			accion = AccionGarantia.LISTAR_GARANTIAS;
		}

		try {
			estadoAcuerdo = DirectorHandler.getDirector("estado.ObjetoOriginal").getValor();
		} catch (Exception e) {
			e.printStackTrace();
		}

		credito = buscarObjetoi(this.getIdSolicitud());

		List<String> estadosCerrados = Arrays.asList("EJECUCION", "CANCELADO", "DESISTIDO", "FINALIZADO");
		List<String> estadosNoModValConsGtia = Arrays.asList("EJECUCION", "GESTION JUDICIAL", "GESTION EXTRAJUDICIAL");

		if (credito != null && credito.getEstadoActual() != null && credito.getEstadoActual().getEstado() != null) {
			String nombreEstado = credito.getEstadoActual().getEstado().getNombreEstado();
			if (credito.getEstadoActual().getEstado().getId().toString().equalsIgnoreCase(estadoAcuerdo)) {
				ocultarCampos = true;
			} else if (estadosCerrados.contains(nombreEstado)) {
				ocultarCampos = true;
				if (nombreEstado.equalsIgnoreCase("EJECUCION") && credito.getTieneGarantiaSGR()) {
					ocultarCampos = false;
				}
			} else if (estadosNoModValConsGtia.contains(nombreEstado)) {
				setModifValorCons(false);
			} else {
				ocultarCampos = false;
			}
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		switch (accion) {
		case NUEVA_GARANTIA:
			estado = new GarantiaEstado();
			seguro = new GarantiaSeguro();
			tasacion = new GarantiaTasacion();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("modifica", false);
			if (aforo != null) {
				oGarantia.setValor(credito.getFinanciamiento() * aforo);
			} else {
				oGarantia.setValor(credito.getFinanciamiento());
			}
			forward = "VerGarantia";
			break;
		case SELECCIONAR_TIPO:
			generarValores();
			oGarantia.setValor(credito.getFinanciamiento() * aforo);
			forward = "VerGarantia";
			break;
		case GUARDAR_GARANTIA:
			if (garantia.getId() != null) {
				idTipo = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("idTipo");
			}
			Double total = 0.0;
			if (garantia != null && garantia.getId() != null && garantia.getId() != 0) {
				total = (Double) bp.createQuery("Select SUM (g.porcentaje) from GarantiaUso g where g.garantia=:gar")
						.setParameter("gar", garantia).uniqueResult();
			}
			boolean modificacion = (Boolean) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("modifica");
			if (modificacion) {
				String idUnico = (String) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("idUnico");
				garantia.setIdentificadorUnico(idUnico);
			}
			boolean validacionOk = true;
			if (!validarTasacionInmueble()) {
				validacionOk = false;
			}
			if (!validarGravamen()) {
				validacionOk = false;
			}
			if (!validarSeguro()) {
				validacionOk = false;
			}
			if (cuit != null && !cuit.trim().isEmpty()) {
				cuit = cuit.replace(" ", "").replace("-", "").replace(".", "");
				if (cuit.length() != 11) {
					errores.put("garantias.cuit", "garantias.cuit");
					validacionOk = false;
				} else {
					try {
						Long.parseLong(cuit);
						garantia.setCuit(cuit);
					} catch (NumberFormatException e) {
						errores.put("garantias.cuit", "garantias.cuit");
						validacionOk = false;
					}
				}
			}
			if (validacionOk) {
				if ((total == null || porcentaje == null || total + porcentaje <= 1.0)) {
					if (garantia.getIdentificadorUnico() != null && !(garantia.getIdentificadorUnico().isEmpty())) {
						if (garantiaUnica(idTipo)) {
							guardar();
						} else {
							mensajeError = "Ya existe una garantia de ese tipo con el mismo Identificador �nico (Toque el bot�n 'Comprobar')";
						}
					} else {
						mensajeError = "El identificador �nico de la Garant��a es Obligatorio (campo a la izquierda del bot�n comprobar)";
					}
				} else {
					mensajeError = "Los porcentajes de titularidad de la Garant��a suman mas del 100%";
				}
			}
			if (mensajeError == null && validacionOk) {
				setAccion(AccionGarantia.LISTAR_GARANTIAS);
				this.result = this.buildList(this.getIdSolicitud());
				this.doProcess();
			} else {
				campoId = true;
				if (idTipo != null && idTipo != 0) {
					TipoGarantia tipoGar = (TipoGarantia) bp.getById(TipoGarantia.class, idTipo);
					nombreCampoIdentificador = tipoGar.getNombreNumeroIdentificacion();
				}
				forward = "VerGarantia";
			}
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("modifica", false);
			break;
		case MODIFICAR_GARANTIA:
			forward = "VerGarantia";
			modTitular = true;
			modifica = true;
			this.cargaroGar(oGarantiaId);
			if (seguro != null && seguro.getId() != null) {
				List<GarantiaSeguroEstado> est = bp.createQuery(
						"select e from GarantiaSeguroEstado e where e.garantiaSeguro.id = :id order by e.fecha desc")
						.setParameter("id", seguro.getId()).setMaxResults(1).list();
				if (!est.isEmpty()) {
					estadoSeguro = est.get(0);
				}
			}

			aforo = (Double) bp.createQuery(
					"select max(o.aforo) from ObjetoiGarantia o where o.aforo is not null and o.id = :idgarantia")
					.setLong("idgarantia", oGarantiaId).uniqueResult();

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idTipo",
					garantia.getTipo().getId());
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idUnico",
					garantia.getIdentificadorUnico());
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("modifica", true);
			break;
		case ELIMINAR_GARANTIA:
			eliminar();
			accion = AccionGarantia.LISTAR_GARANTIAS;
			this.result = this.buildList(this.getIdSolicitud());
			forward = "ListarGarantia";
			break;
		case LISTAR_GARANTIAS:
			this.result = this.buildList(this.getIdSolicitud());
			forward = "ListarGarantia";
			break;
		case VER_RESPONSABLE:
			forward = "verTitulares";
			break;
		case VOLVER_LISTAR:
			this.result = this.buildList(idSolicitud);
			forward = "ListarGarantia";
			break;
		case COMPROBAR:
			Garantia existente = comprobarExistencia();
			campoId = true;
			if (existente != null) {
				cargarGarantia(existente.getId());
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idTipo",
						garantia.getTipo().getId());
				forward = "VerGarantia";
			} else {
				String ident = garantia.getIdentificadorUnico();
				garantia = new Garantia();
				garantia.setIdentificadorUnico(ident);
				if (idTipo == null) {
					idTipo = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.getAttribute("idTipo");
				}
				garantia.setTipo((TipoGarantia) bp.getById(TipoGarantia.class, idTipo));
				setAccion(AccionGarantia.NUEVA_GARANTIA);
				nombreCampoIdentificador = garantia.getTipo().getNombreNumeroIdentificacion();
				aforo = garantia.getTipo().getAforo();
				if (oGarantia.getValor() == null) {
					oGarantia.setValor(new Double(0));
				}
				campoId = true;
				doProcess();
			}
			break;
		case LIBERAR_GARANTIA:
			oGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, oGarantiaId);
			oGarantia.setFechaLiberada(new Date());
			bp.update(oGarantia);
			setAccion(AccionGarantia.LISTAR_GARANTIAS);
			doProcess();
		}

		if (oGarantia != null && oGarantia.getObjetoi() != null) {
			idSolicitud = oGarantia.getObjetoi().getId();
		}

		idGarantiaInmovilizacion = DirectorHelper.getString("tipoGarantia.inmovilizacion");

		return errores.isEmpty();
	}

	private boolean validarTasacionInmueble() {
		return true;
	}

	private boolean validarSeguro() {
		if (seguro != null) {
			if (seguro.getFechaCertifCobertura() != null && seguro.getVencCertifCobertura() != null
					&& seguro.getVencCertifCobertura().before(seguro.getFechaCertifCobertura())) {
				errores.put("garantias.seguro.fechaCertifCoberturaMayor", "garantias.seguro.fechaCertifCoberturaMayor");
				return false;
			}
			if (seguro.getFechaCertifCobertura() != null && seguro.getFechaVencimiento() != null
					&& seguro.getFechaVencimiento().before(seguro.getFechaCertifCobertura())) {
				errores.put("garantias.seguro.fechaCertifCoberturaMayor", "garantias.seguro.fechaCertifCoberturaMayor");
				return false;
			}
			if (seguro.getFechaVencimiento() != null && seguro.getVencCertifCobertura() != null
					&& seguro.getFechaVencimiento().before(seguro.getVencCertifCobertura())) {
				errores.put("garantias.seguro.vencCertifCoberturaMayor", "garantias.seguro.vencCertifCoberturaMayor");
				return false;
			}
		}

		return true;
	}

	private boolean validarGravamen() {
		if (tasacion != null) {
			if (tasacion.isRequiereRegistro() && (tasacion.getGravFechaPresentacionReg() == null
					|| tasacion.getGravInscripcion() == null || tasacion.getGravFechaInscripcion() == null)) {
				errores.put("garantias.tasacion.requiereRegistro", "garantias.tasacion.requiereRegistro");
				return false;
			}
			if (tasacion.isRequiereVencimiento() && tasacion.getGravFechaVencGarantia() == null) {
				errores.put("garantias.tasacion.gravFechaVencGarantia", "garantias.tasacion.gravFechaVencGarantia");
				return false;
			}
			if (tasacion.getGravNroEscritura() != null && tasacion.getGravNroEscritura() > 0
					&& tasacion.getGravFechaEscritura() == null) {
				errores.put("garantias.tasacion.gravFechaEscritura", "garantias.tasacion.gravFechaEscritura");
				return false;
			}
			if (tasacion.getGravFechaInscripcion() != null
					&& (tasacion.getGravInscripcion() == null || tasacion.getGravInscripcion().isEmpty())) {
				errores.put("garantias.tasacion.gravInscripcion", "garantias.tasacion.gravInscripcion");
				return false;
			}
			if (tasacion.getGravFechaPresentacionReg() != null && tasacion.getGravFechaEscritura() != null
					&& tasacion.getGravFechaPresentacionReg().before(tasacion.getGravFechaEscritura())) {
				errores.put("garantias.tasacion.gravFechaPresentacionRegMayor",
						"garantias.tasacion.gravFechaPresentacionRegMayor");
				return false;
			}
			if (tasacion.getGravFechaInscripcion() != null && tasacion.getGravFechaEscritura() != null
					&& tasacion.getGravFechaInscripcion().before(tasacion.getGravFechaEscritura())) {
				errores.put("garantias.tasacion.gravFechaInscripcionMayor",
						"garantias.tasacion.gravFechaInscripcionMayor");
				return false;
			}
			if (tasacion.getGravFechaVencGarantia() != null && tasacion.getGravFechaInscripcion() != null
					&& tasacion.getGravFechaVencGarantia().getTime() <= tasacion.getGravFechaInscripcion().getTime()) {
				errores.put("garantias.tasacion.gravFechaVencGarantiaMayor",
						"garantias.tasacion.gravFechaVencGarantiaMayor");
				return false;
			}
			if (tasacion.getGravFechaVencGarantia() != null && tasacion.getGravFechaReinscripcion() != null && tasacion
					.getGravFechaVencGarantia().getTime() <= tasacion.getGravFechaReinscripcion().getTime()) {
				errores.put("garantias.tasacion.gravFechaVencGarantiaMayor",
						"garantias.tasacion.gravFechaVencGarantiaMayor");
				return false;
			}
			if (tasacion.getGravFechaReinscripcion() != null && tasacion.getGravFechaInscripcion() != null
					&& tasacion.getGravFechaReinscripcion().getTime() <= tasacion.getGravFechaInscripcion().getTime()) {
				errores.put("garantias.tasacion.gravFechaReinscripcionMayor",
						"garantias.tasacion.gravFechaReinscripcionMayor");
				return false;
			}
			if (tasacion.getGravFechaReinscripcion() != null && tasacion.getGravFechaVencGarantia() != null && tasacion
					.getGravFechaReinscripcion().getTime() >= tasacion.getGravFechaVencGarantia().getTime()) {
				errores.put("garantias.tasacion.gravFechaReinscripcionMenor",
						"garantias.tasacion.gravFechaReinscripcionMenor");
				return false;
			}
			if (tasacion.getFechaSustitucion() != null && tasacion.getGravFechaEscritura() != null
					&& tasacion.getFechaSustitucion().getTime() <= tasacion.getGravFechaEscritura().getTime()) {
				errores.put("garantias.tasacion.fechaSustitucionMayor", "garantias.tasacion.fechaSustitucionMayor");
				return false;
			}
			if (tasacion.getFechaSustitucion2() != null && tasacion.getFechaSustitucion() != null
					&& tasacion.getFechaSustitucion2().getTime() <= tasacion.getFechaSustitucion().getTime()) {
				errores.put("garantias.tasacion.fechaSustitucion2Mayor", "garantias.tasacion.fechaSustitucion2Mayor");
				return false;
			}
			if (tasacion.getFechaSustitucion3() != null && tasacion.getFechaSustitucion2() != null
					&& tasacion.getFechaSustitucion3().getTime() <= tasacion.getFechaSustitucion2().getTime()) {
				errores.put("garantias.tasacion.fechaSustitucion3Mayor", "garantias.tasacion.fechaSustitucion3Mayor");
				return false;
			}
			if ((tasacion.getNroSustitucion() == null || tasacion.getNroSustitucion().isEmpty())
					&& tasacion.getFechaSustitucion() != null) {
				errores.put("garantias.tasacion.nroSustitucion", "garantias.tasacion.nroSustitucion");
				return false;
			}
			if ((tasacion.getNroSustitucion2() == null || tasacion.getNroSustitucion2().isEmpty())
					&& tasacion.getFechaSustitucion2() != null) {
				errores.put("garantias.tasacion.nroSustitucion2", "garantias.tasacion.nroSustitucion2");
				return false;
			}
			if ((tasacion.getNroSustitucion3() == null || tasacion.getNroSustitucion3().isEmpty())
					&& tasacion.getFechaSustitucion3() != null) {
				errores.put("garantias.tasacion.nroSustitucion3", "garantias.tasacion.nroSustitucion3");
				return false;
			}
		}
		return true;
	}

	/**
	 * @return el estado de la garantia
	 */
	public GarantiaEstado getEstado() {
		return estado;
	}

	/**
	 * @param estado de la garantia
	 */
	public void setEstado(GarantiaEstado estado) {
		this.estado = estado;
	}

	public GarantiaSeguro getSeguro() {
		return seguro;
	}

	public void setSeguro(GarantiaSeguro seguro) {
		this.seguro = seguro;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public GarantiaTasacion getTasacion() {
		return tasacion;
	}

	public void setTasacion(GarantiaTasacion tasacion) {
		this.tasacion = tasacion;
	}

	public boolean isGarantiaCustodia() {
		return this.getGarantia().getTipo() != null && this.getGarantia().getTipo().getNecesitaCustodia() != null
				&& this.getGarantia().getTipo().getNecesitaCustodia().booleanValue();

	}

	public boolean isGarantiaEstado() {
		boolean garantiaEStado = false;
		if (this.getGarantia().getTipo() != null) {
			if (this.getGarantia().getTipo().getRequiereDatosEstado() != null) {
				if (this.getGarantia().getTipo().getRequiereDatosEstado()) {
					garantiaEStado = true;
				} else {
					garantiaEStado = false;
				}
			} else {
				garantiaEStado = false;
			}
		}

		return garantiaEStado;

	}

	public boolean isMostrarTipo() {
		return idTipo != null && idTipo != 0;
	}

	public boolean isGarantiaSeguro() {
		return this.getGarantia().getTipo() != null && this.getGarantia().getTipo().getRequiereSeguro() != null
				&& this.getGarantia().getTipo().getRequiereSeguro().booleanValue();
	}

	private String parseToPercent(Double percent) {
		return (percent != null ? new String(percent + "%") : new String("0%"));
	}

	private Double parsePercent(String percent) {
		return new Double(percent.replaceAll(",", ".").replaceAll("%", ""));
	}

	public boolean isGarantiaTasaciones() {
		return this.getGarantia().getTipo() != null && this.getGarantia().getTipo().getNecesitaTasacion() != null
				&& this.getGarantia().getTipo().getNecesitaTasacion().booleanValue();

	}

	public boolean isGarantiaTasacionInmueble() {
		return this.getGarantia().getTipo() != null
				&& this.getGarantia().getTipo().getNecesitaTasacionInmueble() != null
				&& this.getGarantia().getTipo().getNecesitaTasacionInmueble().booleanValue();
	}

	public boolean isGarantiaTasacionMueble() {
		return this.getGarantia().getTipo() != null && this.getGarantia().getTipo().getNecesitaTasacionMueble() != null
				&& this.getGarantia().getTipo().getNecesitaTasacionMueble().booleanValue();
	}

	public String getPorcentajeStr() {
		return parseToPercent((porcentaje != null ? porcentaje * 100 : 0));
	}

	public void setPorcentajeStr(String porcentaje) {
		this.porcentaje = parsePercent(porcentaje) / 100;
	}

	public TipoGarantia getTipo() {
		return this.getGarantia().getTipo();
	}

	public Long getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Long idTipo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		this.idTipo = idTipo;
		if (idTipo != null) {
			TipoGarantia tipo = (TipoGarantia) bp.getById(TipoGarantia.class, idTipo);
			garantia.setTipo(tipo);
		}
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getValorDouble(int index) {
		int familia = index / 1000;
		index -= familia * 1000;
		return DoubleHelper.getString(valores[familia].get(index).getValorDouble());
	}

	public void setValorDouble(int index, String valor) {
		int familia = index / 1000;
		index -= familia * 1000;
		valores[familia].get(index).setValorDouble(DoubleHelper.getDouble(valor));
	}

	public String getValorCadena(int index) {
		int familia = index / 1000;
		index -= familia * 1000;
		return valores[familia].get(index).getValorCadena();
	}

	public void setValorCadena(int index, String valor) {
		int familia = index / 1000;
		index -= familia * 1000;
		valores[familia].get(index).setValorCadena(valor);
	}

	public String getNombre(int index) {
		int familia = index / 1000;
		index -= familia * 1000;
		return valores[familia].get(index).getCampo().getNombre();
	}

	public void setNombre(int index, String valor) {
		int familia = index / 1000;
		index -= familia * 1000;
		valores[familia].get(index).getCampo().setNombre(valor);
	}

	public String getTipoDeCampo(int index) {
		int familia = index / 1000;
		index -= familia * 1000;
		return valores[familia].get(index).getCampo().getTipoDeCampo();
	}

	public void setTipoDeCampo(int index, String tipo) {
		int familia = index / 1000;
		index -= familia * 1000;
		valores[familia].get(index).getCampo().setTipoDeCampo(tipo);
	}

	public void setTamano(int index, String tamano) {
		int familia = index / 1000;
		index -= familia * 1000;
		valores[familia].get(index).getCampo().setMaximoCaracteres(new Long(tamano));
	}

	public String getTamano(int index) {
		int familia = index / 1000;
		index -= familia * 1000;
		Long max = valores[familia].get(index).getCampo().getMaximoCaracteres();
		return max != null ? max.toString() : null;
	}

	/**
	 * index llega en rangos de 1000, para diferenciar a que familia pertenece 0 -
	 * 999: general 1000 - 1999: inmuebles etc
	 * 
	 * @param index
	 * @return
	 */
	public ValorCampoGarantia getValor(int index) {
		int familia = index / 1000;
		index -= familia * 1000;
		return valores[familia].get(index);
	}

	public void setValor(int index, ValorCampoGarantia valor) {
		int familia = index / 1000;
		index -= familia * 1000;
		valores[familia].set(index, valor);
	}

	public String getProductoReferencia() {
		return productoReferencia;
	}

	public void setProductoReferencia(String productoReferencia) {
		this.productoReferencia = productoReferencia;
	}

	public boolean isInmovilizacion() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (idTipo != null && idTipo != 0) {
			TipoGarantia tipo = (TipoGarantia) bp.getById(TipoGarantia.class, idTipo);
			if (Arrays.asList(idGarantiaInmovilizacion.split(",")).contains(tipo.getId().toString())) {
				inmovilizacion = true;
			}
		}
		return inmovilizacion;
	}

	public void setInmovilizacion(boolean inmovilizacion) {
		this.inmovilizacion = inmovilizacion;
	}

	public void setTipo(TipoGarantia tipo) {
		this.getGarantia().setTipo(tipo);
	}

	public Garantia getGarantia() {
		return garantia;
	}

	public void setGarantia(Garantia garantia) {
		this.garantia = garantia;
	}

	public void setAccionStr(String a) {
		accion = AccionGarantia.valueOf(a);
	}

	public String getAccionStr() {
		return accion.toString();
	}

	@SuppressWarnings("unchecked")
	private void generarValores() {
		if (idTipo != null && idTipo != 0) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			List<CampoGarantia> campos = bp
					.createQuery(
							"SELECT c FROM CampoGarantia c WHERE c.tipoGarantia.id = :tipo ORDER BY c.familia,c.orden")
					.setParameter("tipo", idTipo).list();
			resetValores();
			for (CampoGarantia campo : campos) {
				int index = 0;
				if (campo.getFamilia() != null) {
					FamiliaCampoGarantia familia = FamiliaCampoGarantia.valueOf(campo.getFamilia());
					if (familia != null) {
						index = familia.ordinal();
					}
				}
				List<ValorCampoGarantia> listValores = valores[index];
				ValorCampoGarantia valor = new ValorCampoGarantia();
				valor.setCampo(campo);
				listValores.add(valor);
			}

			TipoGarantia tipoGar = (TipoGarantia) bp.getById(TipoGarantia.class, idTipo);
			if (tipoGar.getNombreNumeroIdentificacion() != null && tipoGar.getNombreNumeroIdentificacion() != "") {
				nombreCampoIdentificador = tipoGar.getNombreNumeroIdentificacion();
				campoId = true;
				aforo = tipoGar.getAforo();
			}
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("GARANTIA_VALORES",
					valores);
		}
	}

	@SuppressWarnings("unchecked")
	private void resetValores() {
		valores = new List[] { new ArrayList<ValorCampoGarantia>(), new ArrayList<ValorCampoGarantia>(),
				new ArrayList<ValorCampoGarantia>(), new ArrayList<ValorCampoGarantia>(),
				new ArrayList<ValorCampoGarantia>(), new ArrayList<ValorCampoGarantia>() };
	}

	private void extraerValoresPorFamilia(List<ValorCampoGarantia> valoresGar) {
		resetValores();
		for (ValorCampoGarantia valor : valoresGar) {
			int index = 0;
			if (valor.getCampo().getFamilia() != null) {
				FamiliaCampoGarantia familia = FamiliaCampoGarantia.valueOf(valor.getCampo().getFamilia());
				if (familia != null) {
					index = familia.ordinal();
				}
			}
			List<ValorCampoGarantia> listValores = valores[index];
			listValores.add(valor);
		}
	}

	private boolean garantiaUnica(Long id) {
		if (garantia.getId() == null) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Garantia gar = (Garantia) bp
					.createQuery("Select g from Garantia g where g.tipo.id=:tipo and g.identificadorUnico=:id")
					.setParameter("tipo", id).setParameter("id", garantia.getIdentificadorUnico()).uniqueResult();
			if (gar == null) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	private Garantia comprobarExistencia() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Garantia> gar = null;
		if (garantia.getIdentificadorUnico() == null || garantia.getIdentificadorUnico().isEmpty()) {
			return null;
		}
		gar = bp.createQuery("Select g from Garantia g where g.tipo.id=:tipo and g.identificadorUnico=:ident")
				.setParameter("tipo", idTipo).setParameter("ident", garantia.getIdentificadorUnico()).list();
		if (gar != null && gar.size() > 0) {
			return gar.get(0);
		} else {
			return null;
		}
	}

	private void cargaroGar(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		oGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, oGarantiaId);
		garantia = oGarantia.getGarantia();
		idTipo = garantia.getTipoId();

		tipoProducto = garantia.getTipoProducto();
		cuit = garantia.getCuit();

		// guarda los valores separados por familia
		extraerValoresPorFamilia(garantia.getValores());
		tasacion = oGarantia.getTasacion();
		tasacionInmueble = garantia.getTasacionInmueble();
		tasacionMueble = garantia.getTasacionMueble();
		custodiaActual = (GarantiaCustodia) bp
				.createQuery(
						"select c from GarantiaCustodia c where c.objetoiGarantia.id = :ogid order by c.orden desc")
				.setParameter("ogid", oGarantiaId).setMaxResults(1).uniqueResult();
		if (custodiaActual == null) {
			custodiaActual = new GarantiaCustodia();
		}
		seguro = garantia.getSeguro();
		setFechaSustitucionStr(oGarantia.getFechaSustitucionStr());
		TipoGarantia tipoGar = (TipoGarantia) SessionHandler.getCurrentSessionHandler().getBusinessPersistance()
				.getById(TipoGarantia.class, idTipo);
		if (tipoGar.getNombreNumeroIdentificacion() != null && tipoGar.getNombreNumeroIdentificacion() != "") {
			nombreCampoIdentificador = tipoGar.getNombreNumeroIdentificacion();
			campoId = true;
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("GARANTIA_VALORES", valores);
	}

	private void cargarGarantia(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		garantia = (Garantia) bp.getById(Garantia.class, id);

		idTipo = garantia.getTipo().getId();

		Number count = (Number) bp.createSQLQuery("select count(*) from ObjetoiGarantia og where og.garantia_id = :id")
				.setParameter("id", id).uniqueResult();
		garantiaCompartidaOtrosProyectos = count != null && count.intValue() > 0;

		// guarda los valores separados por familia
		extraerValoresPorFamilia(garantia.getValores());

		tasacionInmueble = garantia.getTasacionInmueble();
		tasacionMueble = garantia.getTasacionMueble();

		seguro = garantia.getSeguro();

		if (tasacionInmueble == null) {
			tasacionInmueble = new GarantiaTasacionInmueble();
		}
		if (tasacionMueble == null) {
			tasacionMueble = new GarantiaTasacionMueble();
		}
		if (seguro == null) {
			seguro = new GarantiaSeguro();
		}

		TipoGarantia tipoGar = (TipoGarantia) SessionHandler.getCurrentSessionHandler().getBusinessPersistance()
				.getById(TipoGarantia.class, idTipo);
		if (tipoGar.getNombreNumeroIdentificacion() != null && tipoGar.getNombreNumeroIdentificacion() != "") {
			nombreCampoIdentificador = tipoGar.getNombreNumeroIdentificacion();
			campoId = true;
			aforo = tipoGar.getAforo();
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("GARANTIA_VALORES", valores);
	}

	private Objetoi buscarObjetoi(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return (Objetoi) bp.getById(Objetoi.class, id);
	}

	@SuppressWarnings("unchecked")
	private List<ObjetoiGarantia> buildList(Long idCredito) {
		if (idCredito != null) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Query q = bp.createQuery("FROM ObjetoiGarantia og where og.objetoi.id=? AND og.baja IS NULL");
			q.setLong(0, idCredito);
			liberaciones = (ArrayList<Liberaciones>) bp
					.createSQLQuery("SELECT * FROM Liberaciones l WHERE objetoi_id = :objetoi_id  ORDER BY fecha")
					.addEntity(Liberaciones.class).setLong("objetoi_id", idCredito).list();
			inmovilizaciones = (ArrayList<Inmovilizaciones>) bp
					.createSQLQuery("SELECT * FROM Inmovilizaciones l WHERE objetoi_id = :objetoi_id ORDER BY fecha")
					.addEntity(Inmovilizaciones.class).setLong("objetoi_id", idCredito).list();
			Objetoi objetoi = new Objetoi();
			objetoi.setId(idCredito);
			Double volumenLiberaciones = (Double) bp.getNamedQuery("Liberaciones.sumVolumen")
					.setEntity("objetoi", objetoi).uniqueResult();
			Double volumenInmovilizaciones = (Double) bp.getNamedQuery("Inmovilizaciones.sumVolumen")
					.setEntity("objetoi", objetoi).uniqueResult();
			saldo = (volumenInmovilizaciones != null ? volumenInmovilizaciones : 0D)
					- (volumenLiberaciones != null ? volumenLiberaciones : 0D);
			volumen = (volumenInmovilizaciones != null ? volumenInmovilizaciones : 0D);

			String idTipoGarantia = DirectorHelper.getString("tipoGarantia.inmovilizacion");
			Number count = (Number) bp.createSQLQuery(
					"SELECT COUNT(tg.id) FROM ObjetoiGarantia og JOIN Garantia g ON g.id = og.garantia_id JOIN TipoGarantia tg ON tg.id = g.tipo_id "
							+ "WHERE og.objetoi_id = :objetoi_id AND tg.id in (:tipo_id) AND og.baja IS NULL")
					.setLong("objetoi_id", idCredito).setParameterList("tipo_id", idTipoGarantia.split(",")).uniqueResult();
			hayFiduciaria = !(count == null || count.intValue() == 0);

			return q.list();
		} else {
			return new ArrayList<ObjetoiGarantia>();
		}

	}

	public List<ValorCampoGarantia> getValoresGeneral() {
		return valores[GENERAL];
	}

	public List<ValorCampoGarantia> getValoresInmueble() {
		return valores[INMUEBLE];
	}

	public List<ValorCampoGarantia> getValoresCustodia() {
		return valores[CUSTODIA];
	}

	public List<ValorCampoGarantia> getValoresSeguro() {
		return valores[SEGURO];
	}

	public List<ValorCampoGarantia> getValoresGravamen() {
		return valores[GRAVAMEN];
	}

	public List<ValorCampoGarantia> getValoresMueble() {
		return valores[MUEBLE];
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private boolean esUnico(String id, Long tipo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Garantia> gars = bp
				.createQuery("Select g from Garantia g where g.tipo.id=:tipo and g.identificadorUnico=:id")
				.setParameter("tipo", tipo).setParameter("id", id).list();
		if (gars == null || gars.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public void guardar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String tipoProductoExistente = null;
		ObjetoiGarantia og;
		if (oGarantiaId != null && oGarantiaId != 0L) {
			og = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, oGarantiaId);
			if (garantia.getId() != null && garantia.getId() != 0L) {
				tipoProductoExistente = (String) bp
						.createQuery("select g.tipoProducto from Garantia g where g.id = :id")
						.setParameter("id", garantia.getId()).uniqueResult();
			}
		} else {
			og = new ObjetoiGarantia();
		}
		boolean cambioProducto = false;
		double volumenInmovilizado = 0;
		if (tipoProductoExistente != null && tipoProducto != null && !tipoProductoExistente.equals(tipoProducto)) {
			if (og.getObjetoi() != null) {
				cambioProducto = true;
				Double volumenLiberaciones = (Double) bp.getNamedQuery("Liberaciones.sumVolumen")
						.setEntity("objetoi", og.getObjetoi()).uniqueResult();
				Double volumenInmovilizaciones = (Double) bp.getNamedQuery("Inmovilizaciones.sumVolumen")
						.setEntity("objetoi", og.getObjetoi()).uniqueResult();
				volumenInmovilizado = (volumenInmovilizaciones != null ? volumenInmovilizaciones : 0D)
						- (volumenLiberaciones != null ? volumenLiberaciones : 0D);
			}
		}
		List<ValorCampoGarantia> valoresGuardados = new ArrayList<ValorCampoGarantia>();
		if (idTipo != null) {
			bp.begin();
			if (valores != null) {
				for (List<ValorCampoGarantia> listValores : valores) {
					for (ValorCampoGarantia valor : listValores) {
						boolean cadenaVacia = (valor.getValorCadena() == null || valor.getValorCadena().isEmpty());
						boolean numericoVacio = (valor.getValorDouble() == null);
						if (valor.getCampo().getObligatorio()) {
							if (cadenaVacia && numericoVacio) {
								mensajeError = "El campo " + valor.getCampo().getNombre() + " es obligatorio.";
								bp.rollback();
								return;
							} else {
								bp.saveOrUpdate(valor);
								valoresGuardados.add(valor);
							}
						} else {
							bp.saveOrUpdate(valor);
							valoresGuardados.add(valor);
						}
					}
				}
			}
			garantia.setValores(valoresGuardados);
			List<TipoGarantia> lista = bp.createQuery("SELECT b.tipo FROM Garantia b WHERE b.id = :id")
					.setParameter("id", garantia.getId()).list();

			if (lista != null && lista.size() > 0) {
				TipoGarantia tipo = lista.get(0);
				garantia.setTipo(tipo);
			}

			GarantiaTasacion gt = null;
			try {
				if (oGarantiaId != null && oGarantiaId != 0L) {
					gt = (GarantiaTasacion) bp.createQuery("select o.tasacion from ObjetoiGarantia o where o.id = :id")
							.setParameter("id", oGarantiaId).uniqueResult();
				}
			} catch (NoResultException e) {
				e.printStackTrace();
			}
			if (gt == null) {
				// cuando recien se crea, no tiene ninguna fecha y todas
				// significan cambio de estado
				gt = new GarantiaTasacion();
			}

			GarantiaEstado estado = null;
			try {
				if (oGarantiaId != null && oGarantiaId != 0L) {
					estado = (GarantiaEstado) bp.createQuery(
							"select e from GarantiaEstado e where e.objetoiGarantia.id = :id order by e.fechaProceso desc")
							.setMaxResults(1).setParameter("id", oGarantiaId).uniqueResult();
				}
			} catch (NoResultException e) {
				e.printStackTrace();
			}

			GarantiaEstado nuevoEstado = null;
			if (gt != null) {
				if (gt.getFechaReinscripcion() == null && tasacion.getGravFechaReinscripcion() != null) {
					if (estado.getEstado().equals(ComportamientoGarantiaEnum.VENCIDA.getEstado())) {
						nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.ALTA_CONTABLE,
								tasacion.getGravFechaReinscripcion());
					} else {
						nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.REINSCRIPTA,
								tasacion.getGravFechaReinscripcion());
					}
				} else if (gt.getFechaEjecucion() == null && tasacion.getFechaEjecucion() != null) {
					nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.EJECUTADA, tasacion.getFechaEjecucion());
				} else if (gt.getFechaCesion() == null && tasacion.getFechaCesion() != null) {
					nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.CEDIDA, tasacion.getFechaCesion());
				} else if (estado == null) {
					Double importe = null;
					boolean esCosecha = credito.getLinea().isLineaCosecha();
					if (esCosecha) {
						importe = 0d;
					} else {
						if (credito.getFinanciamiento() == null || credito.getFinanciamiento() == 0) {
							importe = 0d;
						} else {
							importe = null;
						}
					}

					/**
					 * no importa si es o no null el importe ya que al final lo analizo. si es null
					 * se calcula pero si tiene valor no
					 */
					nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.ACTIVA, new Date(), importe);
				}
				if (nuevoEstado != null) {
					if (!(estado == null || estado.getEstado() != nuevoEstado.getEstado())) {
						nuevoEstado = null;
					}
				}
			}

			if (garantia.getTipo().getNecesitaTasacionInmueble() == null
					|| !garantia.getTipo().getNecesitaTasacionInmueble()) {
				garantia.setTasacionInmueble(null);
			} else if (tasacionInmueble != null) {
				garantia.setTasacionInmueble((GarantiaTasacionInmueble) bp.getCurrentSession().merge(tasacionInmueble));
			}

			if (garantia.getTipo().getNecesitaTasacionMueble() == null
					|| !garantia.getTipo().getNecesitaTasacionMueble()) {
				garantia.setTasacionMueble(null);
			} else if (tasacionMueble != null) {
				garantia.setTasacionMueble((GarantiaTasacionMueble) bp.getCurrentSession().merge(tasacionMueble));
			}

			if (garantia.getTipo().getNecesitaTasacion() == null || !garantia.getTipo().getNecesitaTasacion()) {
				og.setTasacion(null);
			} else if (tasacion != null) {
				og.setTasacion((GarantiaTasacion) bp.getCurrentSession().merge(tasacion));
			}

			if (!garantia.getTipo().getRequiereSeguro()) {
				garantia.setSeguro(null);
			} else if (seguro != null) {
				garantia.setSeguro((GarantiaSeguro) bp.getCurrentSession().merge(seguro));
			}

			garantia.setTipoProducto(tipoProducto);
			garantia.setCuit(cuit);

			bp.saveOrUpdate(garantia);

			if (!(Boolean) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("modifica")) {
				// Creo el ObjetoiGarantia
				og.setObservacion(oGarantia.getObservacion());
				og.setValor(oGarantia.getValor());
				og.setGarantia(garantia);
				og.setObjetoi((Objetoi) bp.getById(Objetoi.class, idSolicitud));
				// og.setEstado(ObjetoiGarantia.ESTADO_ACTIVA);

				// Cristian Anton
				// se le agrega el aforo a la relacion para que lo use siempre en los cambios
				og.setAforo(garantia.getTipo().getAforo());
				bp.saveOrUpdate(og);
				oGarantia = og;
				// Creo una garantiaUso si corresponde
				if (garantia.getTipo().getTomadorTitular() != null
						&& garantia.getTipo().getTomadorTitular().booleanValue()) {
					guardarGarantiaUso(bp);
				}
			} else {
				og.setValor(oGarantia.getValor());
				og.setObservacion(oGarantia.getObservacion());
				bp.saveOrUpdate(og);
				oGarantia = og;

				if (cambioProducto && volumenInmovilizado > 0) {
					// buscar si tiene desembolsos ejecutados
					Number count = (Number) bp
							.createSQLQuery(
									"select count(*) from Desembolso d where d.estado = '2' and d.credito_id = :id")
							.setLong("id", og.getObjetoi().getId()).uniqueResult();

					if (count != null && count.intValue() > 0) {
						Date hoy = new Date();
						// Ticket 9949
						CosechaConfig cosecha;
						Liberaciones liberacion = new Liberaciones();
						liberacion.setObjetoi(og.getObjetoi());
						liberacion.setFecha(hoy);
						liberacion.setVolumen(volumenInmovilizado);
						liberacion.setProducto(tipoProductoExistente);
						liberacion.setEstado(EstadoLiberacion.PENDIENTE);
						liberacion.setCambioProducto(true);
						// Ticket 9949
						try {
							cosecha = (CosechaConfig) bp.getNamedQuery("CosechaConfig.findByFecha")
									.setParameter("fecha", liberacion.getFecha())
									.setParameter("varietales", liberacion.getObjetoi().getVarietales());
						} catch (Exception e) {
							cosecha = null;
						}
						if (cosecha != null) {
							liberacion.setAforo(cosecha.getAforo().floatValue());
							switch (liberacion.getProducto()) {
							case "1":
								liberacion.setPrecioProducto(cosecha.getPrecioVinoTinto().floatValue());
							case "2":
								liberacion.setPrecioProducto(cosecha.getPrecioVinoBlanco().floatValue());
							case "3":
								liberacion.setPrecioProducto(cosecha.getPrecioMosto().floatValue());
							case "4":
								liberacion.setPrecioProducto(cosecha.getPrecioOtrosVarietales().floatValue());
							}
							liberacion.setImporte(liberacion.getVolumen().floatValue() * liberacion.getPrecioProducto()
									/ liberacion.getAforo());
						}
						bp.save(liberacion);
						Inmovilizaciones inmovilizacion = new Inmovilizaciones();
						inmovilizacion.setObjetoi(og.getObjetoi());
						inmovilizacion.setFecha(hoy);
						inmovilizacion.setVolumen(og.getObjetoi().getDepositoMinimoDouble());
						inmovilizacion.setProducto(tipoProducto);
						inmovilizacion.setEstado(EstadoInmovilizacion.SOLICITADA);
						inmovilizacion.setCambioProducto(true);
						// Ticket 9949
						if (cosecha != null) {
							inmovilizacion.setAforo(cosecha.getAforo().floatValue());
							switch (inmovilizacion.getProducto()) {
							case "1":
								inmovilizacion.setPrecioProducto(cosecha.getPrecioVinoTinto().floatValue());
							case "2":
								inmovilizacion.setPrecioProducto(cosecha.getPrecioVinoBlanco().floatValue());
							case "3":
								inmovilizacion.setPrecioProducto(cosecha.getPrecioMosto().floatValue());
							case "4":
								inmovilizacion.setPrecioProducto(cosecha.getPrecioOtrosVarietales().floatValue());
							}
							inmovilizacion.setImporte(inmovilizacion.getVolumen().floatValue()
									* inmovilizacion.getPrecioProducto() / inmovilizacion.getAforo());
						}
						bp.save(inmovilizacion);
						Objetoi credito = og.getObjetoi();
						double volumen = credito.getDepositoMinimoDouble();
						credito.setVolumenContrato(volumen);
						bp.update(credito);
					}
				}
			}
			if (nuevoEstado != null) {
				nuevoEstado.setObjetoiGarantia(oGarantia);
				if (nuevoEstado.getImporte() == null) {
					nuevoEstado.determinarImporte();
				}
				bp.save(nuevoEstado);
			}
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("GARANTIA_VALORES", null);
			bp.commit();
			// Ticket 10013
			try {
				GarantiaSeguro newseguro = garantia.getSeguro();
				if (newseguro != null) {
					Number count = null;
					if (newseguro.getId() != null) {
						// Controlo que el seguro no tenga ya un estado
						count = (Number) bp
								.createSQLQuery(
										"select count(*) from GarantiaSeguroEstado g where g.garantiaSeguro_id = :id")
								.setLong("id", newseguro.getId()).uniqueResult();
					}
					if ((count == null || count.intValue() == 0) && newseguro.getFechaVencimiento() != null) {
						GarantiaSeguroEstado gse = new GarantiaSeguroEstado();
						gse.setEstado("VIGENTE");
						gse.setFecha(new Date());
						gse.setGarantiaSeguro(newseguro);
						gse.setObservaciones("Estado autom�tico al registrar cambios en el seguro");
						bp.save(gse);
						bp.commit();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			errores.put("garantias.error.tipo", "garantias.error.tipo");
			bp.rollback();
		}
	}

	@SuppressWarnings("unchecked")
	private void guardarGarantiaUso(BusinessPersistance bp) {
		List<GarantiaUso> gar = bp.createQuery("select gu from GarantiaUso gu where gu.garantia= :garantia ")
				.setEntity("garantia", garantia).list();
		if (gar.isEmpty()) {
			bp.save(new GarantiaUso(garantia, 1.0, "", oGarantia.getObjetoi().getPersona()));
			bp.commit();
		}
	}

	public Long getIdGarantiaTasacion() {
		return tasacion != null ? tasacion.getId() : null;
	}

	public void setIdGarantiaTasacion(Long id) {
		if (tasacion == null) {
			tasacion = new GarantiaTasacion();
		}
		tasacion.setId(id);
	}

	public Long getIdGarantiaTasacionMueble() {
		return tasacionMueble != null ? tasacionMueble.getId() : null;
	}

	public void setIdGarantiaTasacionMueble(Long id) {
		if (tasacionMueble == null) {
			tasacionMueble = new GarantiaTasacionMueble();
		}
		tasacionMueble.setId(id);
	}

	public Long getIdGarantiaTasacionInmueble() {
		return tasacionInmueble != null ? tasacionInmueble.getId() : null;
	}

	public void setIdGarantiaTasacionInmueble(Long id) {
		if (tasacionInmueble == null) {
			tasacionInmueble = new GarantiaTasacionInmueble();
		}
		tasacionInmueble.setId(id);
	}

	public Long getIdSeguro() {
		return seguro != null ? seguro.getId() : null;
	}

	public void setIdSeguro(Long id) {
		if (seguro == null) {
			seguro = new GarantiaSeguro();
		}
		seguro.setId(id);
	}

	public Long getIdGarantia() {
		return garantia != null ? garantia.getId() : null;
	}

	public void setIdGarantia(Long id) {
		if (id != null && id != 0L) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			garantia = (Garantia) bp.getById(Garantia.class, id);
		}
		if (garantia == null) {
			garantia = new Garantia();
			garantia.setId(id);
		}
	}

	public void eliminar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		oGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, oGarantiaId);
		oGarantia.setBaja(new Date());
		bp.update(oGarantia);
		// garantia = (Garantia) bp.getById(Garantia.class, garantia.getId());
		// garantia.setBaja(new Date());
		// bp.saveOrUpdate(garantia);
		this.idSolicitud = oGarantia.getObjetoi().getId();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("GARANTIA_VALORES", null);
	}

	@Override
	public HashMap<String, String> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	public boolean isModTitular() {
		return modTitular;
	}

	public void setModTitular(boolean modTitular) {
		this.modTitular = modTitular;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getIdGarantiaInmovilizacion() {
		return idGarantiaInmovilizacion;
	}

	public void setIdGarantiaInmovilizacion(String idGarantiaInmovilizacion) {
		this.idGarantiaInmovilizacion = idGarantiaInmovilizacion;
	}

	public HashMap<String, String> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, String> errores) {
		this.errores = errores;
	}

	public AccionGarantia getAccion() {
		return accion;
	}

	public void setAccion(AccionGarantia accion) {
		this.accion = accion;
	}

	public boolean isHabilitarCampos() {
		return habilitarCampos;
	}

	public void setHabilitarCampos(boolean habilitarCampos) {
		this.habilitarCampos = habilitarCampos;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	public String getNombreCampoIdentificador() {
		return nombreCampoIdentificador;
	}

	public void setNombreCampoIdentificador(String nombreCampoIdentificador) {
		this.nombreCampoIdentificador = nombreCampoIdentificador;
	}

	public boolean isCampoId() {
		return campoId;
	}

	public void setCampoId(boolean campoId) {
		this.campoId = campoId;
	}

	public ObjetoiGarantia getoGarantia() {
		return oGarantia;
	}

	public void setoGarantia(ObjetoiGarantia oGarantia) {
		this.oGarantia = oGarantia;
	}

	public Long getoGarantiaId() {
		return oGarantiaId;
	}

	public void setoGarantiaId(Long oGarantiaId) {
		this.oGarantiaId = oGarantiaId;
	}

	public GarantiaTasacionMueble getTasacionMueble() {
		return tasacionMueble;
	}

	public void setTasacionMueble(GarantiaTasacionMueble tasacionMueble) {
		this.tasacionMueble = tasacionMueble;
	}

	public GarantiaTasacionInmueble getTasacionInmueble() {
		return tasacionInmueble;
	}

	public void setTasacionInmueble(GarantiaTasacionInmueble tasacionInmueble) {
		this.tasacionInmueble = tasacionInmueble;
	}

	public String getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}

	public boolean isModifica() {
		return modifica;
	}

	public void setModifica(boolean modifica) {
		this.modifica = modifica;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public ArrayList<Liberaciones> getLiberaciones() {
		return liberaciones;
	}

	public void setLiberaciones(ArrayList<Liberaciones> liberaciones) {
		this.liberaciones = liberaciones;
	}

	public ArrayList<Inmovilizaciones> getInmovilizaciones() {
		return inmovilizaciones;
	}

	public void setInmovilizaciones(ArrayList<Inmovilizaciones> inmovilizaciones) {
		this.inmovilizaciones = inmovilizaciones;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Double getVolumen() {
		return volumen;
	}

	public void setVolumen(Double volumen) {
		this.volumen = volumen;
	}

	public boolean isHayFiduciaria() {
		return hayFiduciaria;
	}

	public void setHayFiduciaria(boolean hayFiduciaria) {
		this.hayFiduciaria = hayFiduciaria;
	}

	public String getFechaSustitucionStr() {
		return fechaSustitucionStr;
	}

	public void setFechaSustitucionStr(String fechaSustitucionStr) {
		this.fechaSustitucionStr = fechaSustitucionStr;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public boolean isGarantiaCompartidaOtrosProyectos() {
		return garantiaCompartidaOtrosProyectos;
	}

	public String geteFechaModificacion() {
		return eFechaModificacion;
	}

	public void seteFechaModificacion(String eFechaModificacion) {
		this.eFechaModificacion = eFechaModificacion;
	}

	public GarantiaCustodia getCustodiaActual() {
		return custodiaActual;
	}

	public void setCustodiaActual(GarantiaCustodia custodiaActual) {
		this.custodiaActual = custodiaActual;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public GarantiaSeguroEstado getEstadoSeguro() {
		return estadoSeguro;
	}

	public Double getAforo() {
		return aforo;
	}

	public void setAforo(Double aforo) {
		this.aforo = aforo;
	}

	public boolean isModifValorCons() {
		return modifValorCons;
	}

	public void setModifValorCons(boolean modifValorCons) {
		this.modifValorCons = modifValorCons;
	}
}
