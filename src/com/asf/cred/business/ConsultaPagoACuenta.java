package com.asf.cred.business;

import java.util.ArrayList;

import org.hibernate.SQLQuery;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.MovIngresosVarios;

public class ConsultaPagoACuenta extends ConversationProcess {

	private ArrayList<MovIngresosVarios> arrlMovIngresosVarios;
	private String fechaDesde;
	private String fechaHasta;
	private double totalSaldo = 0;
	private double totalImporte = 0;
	private String saldoCero;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscar() {

		String sqlQuery = "SELECT m.* FROM MovIngresosVarios m "
				+ "INNER JOIN DeudoresVarios d ON d.id = m.deudoresVarios_id "
				+ "INNER JOIN Caratula c ON c.id = d.caratula_id AND m.tipoConcepto = 155 ";

		if (!("on").equals(this.saldoCero)) {
			sqlQuery += " AND m.saldo > 0 ";
		}

		if (this.fechaDesde != null && !this.fechaDesde.isEmpty()) {
			sqlQuery += "AND m.fechaGeneracion >= :fechaDesde ";
		}

		if (this.fechaHasta != null && !this.fechaHasta.isEmpty()) {
			sqlQuery += "AND m.fechaGeneracion <= :fechaHasta ";
		}

		SQLQuery sql = bp.createSQLQuery(sqlQuery).addEntity(MovIngresosVarios.class);

		if (this.fechaDesde != null && !this.fechaDesde.isEmpty()) {
			sql.setDate("fechaDesde", DateHelper.getDate(this.fechaDesde));
		}

		if (this.fechaHasta != null && !this.fechaHasta.isEmpty()) {
			sql.setDate("fechaHasta", DateHelper.getDate(this.fechaHasta));
		}

		arrlMovIngresosVarios = (ArrayList<MovIngresosVarios>) sql.list();

		for (int i = 0; i < arrlMovIngresosVarios.size(); i++) {
			this.totalImporte += arrlMovIngresosVarios.get(i).getImporte();
			this.totalSaldo += arrlMovIngresosVarios.get(i).getImporte();
		}

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "ConsultaPagoACuenta";
	}

	public ArrayList<MovIngresosVarios> getArrlMovIngresosVarios() {
		return arrlMovIngresosVarios;
	}

	public void setArrlMovIngresosVarios(ArrayList<MovIngresosVarios> arrlMovIngresosVarios) {
		this.arrlMovIngresosVarios = arrlMovIngresosVarios;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getSaldoCero() {
		return saldoCero;
	}

	public void setSaldoCero(String saldoCero) {
		this.saldoCero = saldoCero;
	}

	public double getTotalSaldo() {
		return totalSaldo;
	}

	public void setTotalSaldo(double totalSaldo) {
		this.totalSaldo = totalSaldo;
	}

	public double getTotalImporte() {
		return totalImporte;
	}

	public void setTotalImporte(double totalImporte) {
		this.totalImporte = totalImporte;
	}

}
