package com.asf.cred.business;

import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.TipoGarantia;

public class ConsultaObjetoiGarantia extends ConversationProcess {

	@Save private Long idGarantia;
	private String identificador;
	private Long idTipoGarantia;
	
	private List<ObjetoiGarantia> garantias;
	private Garantia garantia;
	
	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction=true)
	public boolean cargar() {
		if (idGarantia == null) {
			return false;
		}
		
		garantia = (Garantia) bp.getById(Garantia.class, idGarantia);
		
		identificador = garantia.getIdentificadorUnico();
		idTipoGarantia = garantia.getTipoId();
		
		garantias = bp.createQuery("select o from ObjetoiGarantia o where o.garantia.id = :id")
			.setParameter("id", idGarantia).list();
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		if (identificador == null || identificador.trim().isEmpty() ||
				idTipoGarantia == null || idTipoGarantia == 0L) {
			return false;
		}
		
		TipoGarantia tipoGarantia = (TipoGarantia) bp.getById(TipoGarantia.class, idTipoGarantia);
		garantia = (Garantia)bp.getNamedQuery("Garantia.findByIdentificadorYTipo")
				.setString("identificador", identificador)
				.setEntity("tipo", tipoGarantia)
				.setMaxResults(1)
				.uniqueResult();
		
		
		garantias = bp.createQuery("select o from ObjetoiGarantia o where o.garantia.id = :id")
				.setParameter("id", garantia.getId()).list();
		
		return true;
	}
	
	@Override
	protected String getDefaultForward() {
		return "ConsultaObjetoiGarantia";
	}

	public List<ObjetoiGarantia> getGarantias() {
		return garantias;
	}
	
	public Garantia getGarantia() {
		return garantia;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Long getIdTipoGarantia() {
		return idTipoGarantia;
	}

	public void setIdTipoGarantia(Long idTipoGarantia) {
		this.idTipoGarantia = idTipoGarantia;
	}

	public Long getIdGarantia() {
		return idGarantia;
	}

	public void setIdGarantia(Long idGarantia) {
		this.idGarantia = idGarantia;
	}

	
}
