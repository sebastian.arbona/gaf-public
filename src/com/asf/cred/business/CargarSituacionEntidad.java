package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.struts.action.ActionMessage;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.*;
import com.civitas.hibernate.persona.creditos.*;

/**
 * @author eselman
 */

public class CargarSituacionEntidad implements IProcess
{
//==========================ATRIBUTOS============================
//=========================ATRIBUTOS===========================
	//atributos utilitarios del proceso.-
	private String forward = "CargarSituacionList";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	//atributos del proceso.-
	private Persona persona;
	private CategoriaEntidad categoria;
	private SituacionEntidad situacionEntidad;
	private List<SituacionEntidad> situaciones;
	//=======================CONSTRUCTORES=========================
	public CargarSituacionEntidad()
	{
		this.errores          = new HashMap<String, Object>();
		this.bp		          = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.persona          = new Persona();
		this.categoria        = new CategoriaEntidad();
		this.situacionEntidad = new  SituacionEntidad();
	}
//======================FUNCIONALIDADES========================
	public boolean doProcess()
	{
		if(this.accion.equals("nuevo"))
		{
			this.forward = "CargarSituacion";
		}
		else if(this.accion.equals("guardar"))
		{
			this.persona = (Persona)this.bp.getById(Persona.class, this.persona.getId());
			this.categoria = (CategoriaEntidad)this.bp.getById(CategoriaEntidad.class, this.categoria.getId());
			
			this.situacionEntidad.setPersona(this.persona);
			
			this.situacionEntidad.setCategoria(this.categoria);
			
			this.validarSituacionEntidad();
            
			if (this.errores.isEmpty()) 
            {
                this.guardarSituacionEntidad();
                if(this.errores.isEmpty())
                {
                	this.forward = "CargarSituacionList";
                }
                else
                {
                	this.forward = "CargarSituacion";
                }
            }
            else
            {
            	this.forward = "CargarSituacion";
            }
		}
		else if(this.accion.equals("cancelar"))
		{
			this.forward = "CargarSituacionList";
		}
		
		this.listarSituaciones();
		
		return this.errores.isEmpty();
	}
//=====================UTILIDADES PRIVADAS=====================
	private void listarSituaciones()
	{
		if( this.persona != null)
		{
			try
			{
				this.persona = (Persona)this.bp.getById(Persona.class, this.persona.getId());
				
				String consulta = "";
				consulta += "SELECT se FROM SituacionEntidad se WHERE";
				consulta += " se.persona.id=" + this.persona.getId();
				consulta += " AND se.fechaHasta IS NULL";
								
				this.situaciones = this.bp.getByFilter(consulta);
			}
			catch( Exception e )
			{
				this.situaciones = new ArrayList<SituacionEntidad>();
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Valida un cambio de estado.-
	 * 
	 * 1) Validaciones generales de SituacionEstado.-
	 * 2) La fecha desde debe ser posterior o igual a la Fecha Desde del �ltimo Estado.-
	 * 
	 *  */
	private void validarSituacionEntidad()
	{
		//1)		
		this.situacionEntidad.validate();
		this.errores.putAll(this.situacionEntidad.getChErrores());
		
		//2)
		String consulta = "";
		consulta += "SELECT se FROM SituacionEntidad se WHERE";
		consulta += " se.persona.id=" + this.persona.getId();
		consulta += " AND se.categoria.entidad.id=" + this.categoria.getEntidad().getId();
		consulta += " AND se.fechaHasta IS NULL";
		
		SituacionEntidad situacionActual = null;
		
		try
		{
			situacionActual = (SituacionEntidad)this.bp.getByFilter(consulta).get(0);
		}
		catch(IndexOutOfBoundsException ie)
		{
			situacionActual = null;
		}
		
		if(situacionActual != null && this.situacionEntidad.getFechaDesde() != null)
		{
			if(this.situacionEntidad.getFechaDesde().before(situacionActual.getFechaDesde()))
			{
				String[] mensaje = {this.situacionEntidad.getCategoria().getEntidad().getNombreEntidad(),situacionActual.getFechaDesdeStr()};
				this.errores.put("CargarSituacionEntidad.fechaDesde.anterior", new ActionMessage("CargarSituacionEntidad.fechaDesde.anterior",mensaje));
			}
		}
	}
	
	/**
	 * Actualiza el estado de situacion de la persona para una entidad.-
	 * 1) Actualizo la fecha Hasta del Estado Anterior.-
	 * 2) Guarda el nuevo estado.-
	 * 
	 */
	private void guardarSituacionEntidad()
	{
		try
		{
			//1)
			String consulta = "";
			consulta += "SELECT se FROM SituacionEntidad se WHERE";
			consulta += " se.persona.id=" + this.persona.getId();
			consulta += " AND se.categoria.entidad.id=" + this.categoria.getEntidad().getId();
			consulta += " AND se.fechaHasta IS NULL";
			
			List<SituacionEntidad> historicoSituaciones = this.bp.getByFilter(consulta);
			
			if(historicoSituaciones.size() > 0)
			{
				SituacionEntidad ultimaSituacion = historicoSituaciones.get(0);
				ultimaSituacion.setFechaHasta(this.situacionEntidad.getFechaDesde());
				this.bp.saveOrUpdate(ultimaSituacion);
			}
			
			//2)
			this.bp.saveOrUpdate(this.situacionEntidad);
			
			this.situacionEntidad = new SituacionEntidad();
			this.categoria = new CategoriaEntidad();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			this.errores.put("CargaSituacionEntidad.error", new ActionMessage("CargaSituacionEntidad.error",this.categoria.getEntidad().getNombreEntidad()));
		}
	}
//======================GETTERS Y SETTERS======================
	public HashMap<String, Object> getErrors()
	{
		return this.errores;
	}
	public String getForward()
	{
		return this.forward;
	}
	public String getInput()
	{
		return this.forward;
	}
	public Object getResult()
	{
		return null;
	}
	public String getAccion() 
	{
		return this.accion;
	}
	public void setAccion(String accion) 
	{
		this.accion = accion;
	}
	public Persona getPersona() 
	{
		return this.persona;
	}
	public void setPersona(Persona persona) 
	{
		this.persona = persona;
	}
	public CategoriaEntidad getCategoria() 
	{
		return this.categoria;
	}
	public void setCategoria(CategoriaEntidad categoria) 
	{
		this.categoria = categoria;
	}
	public List<SituacionEntidad> getSituaciones() 
	{
		return this.situaciones;
	}
	public void setEstadosPersonas(List<SituacionEntidad> situaciones) 
	{
		this.situaciones = situaciones;
	}
	public SituacionEntidad getSituacionEntidad() 
	{
		return this.situacionEntidad;
	}
	public void setSituacionEntidad(SituacionEntidad situacionEntidad) 
	{
		this.situacionEntidad = situacionEntidad;
	}
	//========================VALIDACIONES=========================
	public boolean validate()
	{
		return this.errores.isEmpty();
	}
}