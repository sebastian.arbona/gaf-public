package com.asf.cred.business;

import com.asf.gaf.hibernate.Mediopago;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Pagos;

public class GenerarPagos {
	private BusinessPersistance bp;
	private Pagos pagos;

	public GenerarPagos() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	public GenerarPagos(BusinessPersistance bp) {
		this.bp = bp;
	}

	public boolean generar(Boleto boleto, Caratula caratula, double importe) throws PagoException {
		Number count = (Number) bp.createQuery("select count(*) from Pagos p where p.objetoi.id = :idCredito and p.fechaPago > :fecha")
			.setParameter("idCredito", boleto.getObjetoi_id()).setParameter("fecha", caratula.getFechaCobranza()).uniqueResult();
		if (count != null && count.intValue() > 0) {
			throw new PagoException("pago.posterior");
		}
		
		pagos = new Pagos();
		pagos.getId().setBoleto(boleto);
		pagos.getId().setCaratula(caratula);
		pagos.setActualizado(false);
		pagos.setFechaPago(caratula.getFechaCobranza());
		pagos.setFechaEmision(boleto.getFechaEmision());
		pagos.setFechaVencimiento(boleto.getFechaVencimiento());
		pagos.setImporte(importe);
		pagos.setObjetoi(boleto.getObjetoi());
		bp.saveOrUpdate(pagos);

		/* Comentado porque Movpagos se usa directamente al cerrar caja 
		List<Bolcon> bolcons = bp.getByFilter("FROM Bolcon b WHERE b.id.numeroBoleto="
				+ boleto.getId().getNumeroBoleto()
				+ " AND b.id.verificadorBoleto="
				+ boleto.getId().getVerificadorBoleto()
				+ " AND b.id.periodoBoleto="
				+ boleto.getId().getPeriodoBoleto());
		if (bolcons.size() > 0) {
			for (Iterator<Bolcon> iter = bolcons.iterator(); iter.hasNext();) {
				Bolcon bolcon = iter.next();

				Movpagos movpagos = new Movpagos();

				movpagos.getId().setNumeroBoleto(bolcon.getId().getNumeroBoleto());
				movpagos.getId().setVerificadorBoleto(bolcon.getId().getVerificadorBoleto());
				movpagos.getId().setPeriodoBoleto(bolcon.getId().getPeriodoBoleto());
				movpagos.getId().setCaratula(caratula);
				movpagos.getId().setMovimientoCtacte(bolcon.getId().getMovimientoCtacte());
				movpagos.getId().setPeriodoCtacte(bolcon.getId().getPeriodoCtacte());
				movpagos.getId().setVerificadorCtacte(bolcon.getId().getVerificadorCtacte());
				movpagos.getId().setItemCtacte(bolcon.getId().getItemCtacte());
				movpagos.setCuota(bolcon.getCuota());
				movpagos.setFacturado(bolcon.getFacturado());
				movpagos.setImporte(bolcon.getImporte());
				movpagos.setMes(bolcon.getMes());
				movpagos.setOriginal(bolcon.getOriginal());
				movpagos.setTipomov(bolcon.getTipomov());
				movpagos.setActualizado(false);
				movpagos.setPagos(pagos);
				
				bp.saveOrUpdate(movpagos);
			}
		}
*/
		return true;
	}
	
	
	public boolean generar(Boleto boleto, Caratula caratula, double importe, String medio) throws PagoException {
		Number count = (Number) bp.createQuery("select count(*) from Pagos p where p.objetoi.id = :idCredito and p.fechaPago > :fecha")
			.setParameter("idCredito", boleto.getObjetoi_id()).setParameter("fecha", caratula.getFechaCobranza()).uniqueResult();
		if (count != null && count.intValue() > 0) {
			throw new PagoException("pago.posterior");
		}
		
		pagos = new Pagos();
		pagos.getId().setBoleto(boleto);
		pagos.getId().setCaratula(caratula);
		pagos.setActualizado(false);
		pagos.setFechaPago(caratula.getFechaCobranza());
		pagos.setFechaEmision(boleto.getFechaEmision());
		pagos.setFechaVencimiento(boleto.getFechaVencimiento());
		pagos.setImporte(importe);
		pagos.setMedioPago(medio);
		pagos.setObjetoi(boleto.getObjetoi());
		bp.saveOrUpdate(pagos);

		/* Comentado porque Movpagos se usa directamente al cerrar caja 
		List<Bolcon> bolcons = bp.getByFilter("FROM Bolcon b WHERE b.id.numeroBoleto="
				+ boleto.getId().getNumeroBoleto()
				+ " AND b.id.verificadorBoleto="
				+ boleto.getId().getVerificadorBoleto()
				+ " AND b.id.periodoBoleto="
				+ boleto.getId().getPeriodoBoleto());
		if (bolcons.size() > 0) {
			for (Iterator<Bolcon> iter = bolcons.iterator(); iter.hasNext();) {
				Bolcon bolcon = iter.next();

				Movpagos movpagos = new Movpagos();

				movpagos.getId().setNumeroBoleto(bolcon.getId().getNumeroBoleto());
				movpagos.getId().setVerificadorBoleto(bolcon.getId().getVerificadorBoleto());
				movpagos.getId().setPeriodoBoleto(bolcon.getId().getPeriodoBoleto());
				movpagos.getId().setCaratula(caratula);
				movpagos.getId().setMovimientoCtacte(bolcon.getId().getMovimientoCtacte());
				movpagos.getId().setPeriodoCtacte(bolcon.getId().getPeriodoCtacte());
				movpagos.getId().setVerificadorCtacte(bolcon.getId().getVerificadorCtacte());
				movpagos.getId().setItemCtacte(bolcon.getId().getItemCtacte());
				movpagos.setCuota(bolcon.getCuota());
				movpagos.setFacturado(bolcon.getFacturado());
				movpagos.setImporte(bolcon.getImporte());
				movpagos.setMes(bolcon.getMes());
				movpagos.setOriginal(bolcon.getOriginal());
				movpagos.setTipomov(bolcon.getTipomov());
				movpagos.setActualizado(false);
				movpagos.setPagos(pagos);
				
				bp.saveOrUpdate(movpagos);
			}
		}
*/
		return true;
	}

	

	public Pagos getPagos() {
		return pagos;
	}

}