package com.asf.cred.business;

import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.GarantiaSeguroEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class GarantiaSeguroEstadoProcess extends ConversationProcess {

	@Save
	private Long idObjetoiGarantia;
	private List<GarantiaSeguroEstado> estados;
	private String nuevoEstado;

	private String observaciones;
	private String forwardURL;
	private Long idGarantiaSeguroEstado;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean listar() {
		ObjetoiGarantia objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);
		if (objetoiGarantia == null || objetoiGarantia.getGarantia() == null
				|| objetoiGarantia.getGarantia().getSeguro() == null) {
			return false;
		}

		estados = bp
				.createQuery(
						"select e from GarantiaSeguroEstado e where e.garantiaSeguro.id = :id order by e.fecha desc")
				.setParameter("id", objetoiGarantia.getGarantia().getSeguro().getId()).list();

		forward = "GarantiaSeguroEstadoList";

		return true;
	}

	@ProcessMethod(defaultAction = true)
	public boolean eliminar() {
		try {
			GarantiaSeguroEstado gse = (GarantiaSeguroEstado) bp.getById(GarantiaSeguroEstado.class,
					idGarantiaSeguroEstado);
			if (gse != null) {
				String query = "DELETE FROM GarantiaSeguroEstado WHERE id = " + idGarantiaSeguroEstado.toString();
				bp.createSQLQuery(query).executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		listar();
		return true;

	}

	@ProcessMethod
	public boolean nuevo() {
		return true;
	}

	@ProcessMethod
	public boolean guardar() {
		GarantiaSeguroEstado e = new GarantiaSeguroEstado();
		e.setFecha(new Date());
		e.setEstado(nuevoEstado);
		e.setObservaciones(observaciones);
		ObjetoiGarantia objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);
		e.setGarantiaSeguro(objetoiGarantia.getGarantia().getSeguro());
		bp.save(e);

		volver();

		return true;
	}

	@ProcessMethod
	public boolean volver() {
		ObjetoiGarantia objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);

		forward = "ProcessRedirect";
		forwardURL = "/actions/process.do?processName=GarantiaProcess&process.accionStr=MODIFICAR_GARANTIA"
				+ "&do=process&process.oGarantiaId=" + objetoiGarantia.getId() + "&process.idSolicitud="
				+ objetoiGarantia.getObjetoi().getId();

		return true;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public List<GarantiaSeguroEstado> getEstados() {
		return estados;
	}

	public Long getIdObjetoiGarantia() {
		return idObjetoiGarantia;
	}

	public void setIdObjetoiGarantia(Long idObjetoiGarantia) {
		this.idObjetoiGarantia = idObjetoiGarantia;
	}

	@Override
	protected String getDefaultForward() {
		return "GarantiaSeguroEstado";
	}

	public String getNuevoEstado() {
		return nuevoEstado;
	}

	public void setNuevoEstado(String nuevoEstado) {
		this.nuevoEstado = nuevoEstado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getIdGarantiaSeguroEstado() {
		return idGarantiaSeguroEstado;
	}

	public void setIdGarantiaSeguroEstado(Long idGarantiaSeguroEstado) {
		this.idGarantiaSeguroEstado = idGarantiaSeguroEstado;
	}

}
