package com.asf.cred.business;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;

import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.expedientes.el.impl.StringLiteral;

/**
 * @author eselman
 */

public class DatosCrediticios implements IProcess {
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "DatosCrediticios";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Persona persona;

	private List<ObjetoiDTO> creditos;
	private ReportResult reportResult;
	private Long idObjetoi;
	private Long idPersona;
	private List<StringLiteral> mensajes;

	private double deudaNoExigible;
	private String numeroCredito;
	private List<AuditoriaFinal> auditoriaFinal;
	private String auditoriaFinalPos;
	private boolean hayNroCredito;

	// =======================CONSTRUCTORES=========================
	public DatosCrediticios() {
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		persona = new Persona();
		idPersona = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("idPersona");
		persona.setId(idPersona);
		mensajes = new ArrayList<StringLiteral>();
	}

	// ======================FUNCIONALIDADES========================
	@Override
	public boolean doProcess() {
		if (accion != null && accion.equals("imprimirInforme")) {
			getReportResult();
			if (this.errores.isEmpty()) {
				this.forward = "ReporteManual";
			}
			return errores.isEmpty();
		}
		if (accion != null && accion.equals("imprimirProrroga")) {
			imprimirProrroga();
			if (this.errores.isEmpty()) {
				this.forward = "ReporteManual";
			}
			return errores.isEmpty();
		}
		if (accion != null && accion.equals("verificarLibreDeuda")) {
			if (verificarLibreDeuda()) {
				if (this.errores.isEmpty()) {
					this.forward = "ReporteManual";
					return errores.isEmpty();
				}
			}
		}
		this.listarCreditos();
		return this.errores.isEmpty();
	}
	// =====================UTILIDADES PRIVADAS=====================

	@SuppressWarnings("unchecked")
	private void listarCreditos() {

		if (this.persona != null && this.persona.getId().longValue() > 0) {
			try {
				long idRelacionCotomador = new Long(DirectorHandler.getDirector("relacion.cotomador").getValor())
						.longValue();
				this.persona = (Persona) this.bp.getById(Persona.class, this.persona.getId());
				// buscar los creditos de una persona
				List<Objetoi> objetos = bp.getNamedQuery("Objetoi.findByPersonaCotomador")
						.setLong("idPersona", persona.getId()).setLong("idRelacionCotomador", idRelacionCotomador)
						.list();

				creditos = new ArrayList<ObjetoiDTO>();

				CreditoHandler handler = new CreditoHandler();
				Estado estado;
				ObjetoiDTO credito;
				for (Objetoi o : objetos) {
					try {
						auditoriaFinal = bp.createQuery("Select n from AuditoriaFinal n where n.credito.id = :credito")
								.setParameter("credito", o.getId()).list();
						if (auditoriaFinal.isEmpty()) {
							setAuditoriaFinalPos("");
						} else {
							setAuditoriaFinalPos(auditoriaFinal.get(auditoriaFinal.size() - 1).getAplicaFondo());
						}

						estado = handler.findEstado(o);
						String estadoCredito = estado == null ? "Sin Estado" : estado.getNombreEstado();

						credito = new ObjetoiDTO(o, estadoCredito, 0);
						credito.setIdLinea(o.getLinea_id());
						credito.setComportamiento(o.getComportamientoActual());
						credito.setAuditoriaFinalPosee(getAuditoriaFinalPos());

						CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
						calculoDeuda.calcular();
						BeanCtaCte beanTotal = calculoDeuda.getTotal();
						double deudaTotal = beanTotal.getSaldoCuota();
						credito.setTotalAdeudado(deudaTotal);
						o.setTotalDeudaActual(deudaTotal);
						creditos.add(credito);

						if (!(o.getNumeroCredito() == null || o.getNumeroCredito().equals(""))) {
							hayNroCredito = true;
						}

					} catch (Exception e) {
						/**
						 * si dejamos que la excepcion se capture fuera del bucle el procesamiento de
						 * otros creditos no se realiza de esta forma deberia procesar todos los
						 * creditos que no tengan problemas
						 */
						e.printStackTrace();
					}

				}
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idPersona",
						persona.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void getReportResult() {
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		try {
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName("otorgamiento.jasper");
			reportResult.setParams("idObjetoi=" + idObjetoi + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";idPersona=" + idPersona + ";relacionCotomadorId=" + relacionCotomadorId + ";MONEDA="
					+ objetoi.getLinea().getMoneda().getSimbolo() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "otorgamiento"));
		}
	}

	private void imprimirProrroga() {
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		try {
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName("solicitudProrroga.jasper");
			reportResult.setParams("idObjetoi=" + idObjetoi + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";idPersona=" + idPersona + ";relacionCotomadorId=" + relacionCotomadorId + ";MONEDA="
					+ objetoi.getLinea().getMoneda().getSimbolo() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "solicitudProrroga"));
		}
	}

	@SuppressWarnings("unchecked")
	public boolean verificarLibreDeuda() {
		boolean hayDeudaVencida = false;
		boolean hayDeudaNoVencida = false;
		listarCreditos();
		Objetoi objetoi;
		for (ObjetoiDTO dto : creditos) {
			objetoi = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
			List<Cuota> cuotasVencidas = bp
					.createQuery("SELECT c FROM Cuota c WHERE c.credito = :credito "
							+ "AND c.fechaVencimiento < :hoy ORDER BY c.fechaVencimiento")
					.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();

			for (Cuota cuota : cuotasVencidas) {
				List<Ctacte> ctes = bp.createQuery("SELECT cta FROM Ctacte cta JOIN cta.cuota c WHERE c = :cuota")
						.setParameter("cuota", cuota).list();

				double credito = 0.0;
				double debito = 0.0;
				for (Ctacte cte : ctes) {
					if (cte.getTipomov() != null && cte.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_CREDITO)) {
						credito += cte.getImporte();
					} else if (cte.getTipomov() != null
							&& cte.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
						debito += cte.getImporte();
					}
				}
				BigDecimal deuda = new BigDecimal(debito - credito);
				BigDecimal cero = new BigDecimal(0.0);
				if (!deuda.equals(cero)) {
					mensajes.add(StringLiteral.fromLiteralValue("Credito Nro " + dto.getNumeroAtencion()));
					hayDeudaVencida = true;
				}
			}

			// Itero cuotas no vencidas
			if (!hayDeudaVencida) {
				List<Cuota> cuotasNoVencidas = bp
						.createQuery("SELECT c FROM Cuota c WHERE c.credito = :credito "
								+ "AND c.fechaVencimiento >= :hoy ORDER BY c.fechaVencimiento")
						.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();

				for (Cuota cuota : cuotasNoVencidas) {
					List<Ctacte> ctes = bp.createQuery("SELECT cta FROM Ctacte cta JOIN cta.cuota c WHERE c = :cuota")
							.setParameter("cuota", cuota).list();

					double credito = 0.0;
					double debito = 0.0;
					for (Ctacte cte : ctes) {
						if (cte.getTipomov() != null
								&& cte.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_CREDITO)) {
							credito += cte.getImporte();
						} else if (cte.getTipomov() != null
								&& cte.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
							debito += cte.getImporte();
						}
					}
					BigDecimal deuda = new BigDecimal(debito - credito);

					if (deuda.doubleValue() > 0) {
						deudaNoExigible = deuda.doubleValue();
						hayDeudaNoVencida = true;
					}
				}
			}

		}

		if (hayDeudaVencida) {
			mensajes.add(0,
					StringLiteral.fromLiteralValue(persona.getNomb12() + " tiene pendiente el pago de cuotas de:"));
			mensajes.add(StringLiteral.fromLiteralValue("Por lo tanto no se puede imprimir el comprobante."));
			return false;
		} else if (hayDeudaNoVencida) {
			getDeudaNoExigible(deudaNoExigible);
			return true;
		} else {
			getInformeLibreDeuda();
			return true;
		}

	}

	private void getInformeLibreDeuda() {
		try {
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName("libreDeuda.jasper");
			reportResult.setParams("idPersona=" + idPersona + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "libreDeuda"));
		}
	}

	private void getDeudaNoExigible(double deuda) {
		try {
			DecimalFormat d = new DecimalFormat("#.##");
			String deudaFormateada = d.format(deudaNoExigible);
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName("DeudaNoExigible.jasper");
			reportResult.setParams("idPersona=" + idPersona + ";SCHEMA=" + BusinessPersistance.getSchema() + ";DEUDA="
					+ deudaFormateada + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "DeudaNoExigible"));
		}
	}

	// ======================GETTERS Y SETTERS======================
	@Override
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return reportResult;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public List<StringLiteral> getMensajes() {
		return mensajes;
	}

	public void setMensajes(List<StringLiteral> mensajes) {
		this.mensajes = mensajes;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public boolean isHayNroCredito() {
		return hayNroCredito;
	}

	public void setHayNroCredito(boolean hayNroCredito) {
		this.hayNroCredito = hayNroCredito;
	}

	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}

	// ========================VALIDACIONES=========================
	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

}