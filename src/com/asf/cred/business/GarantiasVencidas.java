package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

/**
 * Proceso que analiza la fecha de vencimiento de una garantia y carga el estado
 * VENCIDA si la fecha actual es posterior
 * 
 * @author Cesar
 *
 */
public class GarantiasVencidas implements Serializable {

	private static final long serialVersionUID = 7946329452841252833L;

	private BusinessPersistance bp;

	/**
	 * @param bp acceso a la persistencia
	 */
	public GarantiasVencidas(BusinessPersistance bp) {
		this.bp = bp;
	}

	/**
	 * Ejecuta el analisis de las garantias y si corresponde crea el nuevo estado
	 * VENCIDA.
	 */
	@SuppressWarnings("unchecked")
	public void ejecutar() {
		// Seteo el estado a VENCIDA de las garant��as de tasaci�n que no est�n
		// en el estado CANCELADA, SUSTITUIDA, VENCIDA ni dadas de baja y la
		// fecha de vencimiento grav no est� vencida
		// de los cr�ditos que no est�n Cancelados ni Finalizados
		List<Number> ids = bp
				.createSQLQuery("select og.id from objetoigarantia og join garantiatasacion t on og.tasacion_id = t.id "
						+ "left join garantiaestado ge on ge.objetoigarantia_id = og.id left join estado eg on eg.idestado = ge.estado_idestado "
						+ "left join objetoiestado oe on oe.objetoi_id = og.objetoi_id and oe.fecha <= :hoy and (oe.fechahasta >= :hoy or oe.fechahasta is null) "
						+ "where eg.comportamiento not in ('cancelada','cancelada_acuerdo','sustituida','vencida','ejecutada') "
						+ "and ge.id = (select max(e2.id) from garantiaestado e2 where e2.objetoigarantia_id = og.id) and t.gravfechavencgarantia <= :hoy and oe.estado_idestado not in (12,13) and og.baja is null ;")
				.setParameter("hoy", new Date()).list();
		for (Number id : ids) {
			ObjetoiGarantia og = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id.longValue());
			GarantiaEstado estadoNuevo = new GarantiaEstado(ComportamientoGarantiaEnum.VENCIDA, new Date());
			estadoNuevo.setObjetoiGarantia(og);
			estadoNuevo.determinarImporte();
			bp.save(estadoNuevo);
		}
	}

}
