package com.asf.cred.business;

import com.asf.gaf.hibernate.Bancos;

public class FabricaGeneradorArchivoBanco {
	
	private static FabricaGeneradorArchivoBanco instance;
	
	private FabricaGeneradorArchivoBanco(){
		
	}
	
	public static FabricaGeneradorArchivoBanco getInstance(){
		if(instance == null){
			instance = new FabricaGeneradorArchivoBanco();
		}
		return instance;
	}
	
	public GeneradorArchivoBancoAbstract getGenerador(Bancos banco){
		if(banco.getDetaBa().toUpperCase().contains("NACION")){
			GeneradorArchivoBancoNacion generador = new GeneradorArchivoBancoNacion();
			generador.setBanco(banco);
			return generador;
		}
		return null;
	}

}
