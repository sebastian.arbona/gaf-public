package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.nirven.creditos.hibernate.BeanCompensatorio;

public class CierreMPBean implements Serializable {

	private Long id;
	private Long numeroAtencion;
	private String titular;
	private String linea;
	private Date fechaCalculo;
	private Map<Long, Interes> interesesPorCuota;
	private List<BeanCompensatorio> detalleMoratorio;
	private List<BeanCompensatorio> detallePunitorio;
	private Date primerVencimientoImpago;
	private int diasPrimerVencimientoImpago;
	
	public static class Interes {
		
		private BeanCompensatorio moratorio;
		private BeanCompensatorio punitorio;
		public Interes() {
			
		}
		public Interes(BeanCompensatorio moratorio, BeanCompensatorio punitorio) {
			this.moratorio = moratorio;
			this.punitorio = punitorio;
		}

		public BeanCompensatorio getMoratorio() {
			return moratorio;
		}

		public BeanCompensatorio getPunitorio() {
			return punitorio;
		}
		
		public void setMoratorio(BeanCompensatorio moratorio) {
			this.moratorio = moratorio;
		}
		
		public void setPunitorio(BeanCompensatorio punitorio) {
			this.punitorio = punitorio;
		}
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getNumeroAtencion() {
		return numeroAtencion;
	}
	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public double getMoratorio() {
		double moratorio = 0;
		for (BeanCompensatorio b : detalleMoratorio) {
			moratorio += b.getMonto();
		}
		return moratorio;
	}
	public double getPunitorio() {
		double punitorio = 0;
		for (BeanCompensatorio b : detallePunitorio) {
			punitorio += b.getMonto();
		}
		return punitorio;
	}
	public Date getFechaCalculo() {
		return fechaCalculo;
	}
	public void setFechaCalculo(Date fechaCalculo) {
		this.fechaCalculo = fechaCalculo;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public List<BeanCompensatorio> getDetalleMoratorio() {
		return detalleMoratorio;
	}
	public void setDetalleMoratorio(List<BeanCompensatorio> detalleMoratorio) {
		this.detalleMoratorio = detalleMoratorio;
	}
	public List<BeanCompensatorio> getDetallePunitorio() {
		return detallePunitorio;
	}
	public void setDetallePunitorio(List<BeanCompensatorio> detallePunitorio) {
		this.detallePunitorio = detallePunitorio;
	}
	public Map<Long, Interes> getInteresesPorCuota() {
		return interesesPorCuota;
	}
	public void setInteresesPorCuota(Map<Long, Interes> interesesPorCuota) {
		this.interesesPorCuota = interesesPorCuota;
	}
	public Date getPrimerVencimientoImpago() {
		return primerVencimientoImpago;
	}
	public void setPrimerVencimientoImpago(Date primerVencimientoImpago) {
		this.primerVencimientoImpago = primerVencimientoImpago;
	}
	public int getDiasPrimerVencimientoImpago() {
		return diasPrimerVencimientoImpago;
	}
	public void setDiasPrimerVencimientoImpago(int diasPrimerVencimientoImpago) {
		this.diasPrimerVencimientoImpago = diasPrimerVencimientoImpago;
	}
}
