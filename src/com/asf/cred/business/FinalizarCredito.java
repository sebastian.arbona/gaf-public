package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class FinalizarCredito implements IProcess {

	private String action;
	private BusinessPersistance bp;
	private List<ObjetoiDTO> listaCreditosDTO;
	private String forward;
	private HashMap<String, Object> errores;

	// Parametros para los filtros
	private String linea_id;
	private String expediente;
	private String credito;
	private Date desde;
	private Date hasta;
	private Estado estadoCancelado;

	// Variables para pantalla de finalizacion
	private Date fechaFirmaResolucion;
	private String numeroResolucion;
	private Date fechaFirmaFinalizacion;

	private String mensajeFinalizado;

	private String idObjetoi;
	private Boolean garantiasLiberadas;
	private Objetoi objetoi;

	public FinalizarCredito() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		garantiasLiberadas = false;
		errores = new HashMap<String, Object>();
		objetoi = new Objetoi();
		objetoi.setPersona(new Persona());
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {

		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();

		mensajeFinalizado = null;

		if (action == null) { // Controlo dos veces la variable action, porque cuando la recibo por submit, se
								// setea en setAction, pero para la accion FINALIZAR, viene por request

			action = request.getParameter("action");
		}

		List<Objetoi> listaCreditos = new ArrayList<Objetoi>();

		estadoCancelado = (Estado) bp.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = :nombre")
				.setParameter("nombre", "CANCELADO").uniqueResult();

		forward = "FinalizarCreditoList";

		if (action == null) {

			listaCreditos = bp.createQuery(
					"SELECT o FROM ObjetoiEstado oe JOIN oe.objetoi o WHERE oe.estado = :estado AND oe.fechaHasta IS NULL")
					.setParameter("estado", estadoCancelado).list();

			crearDTO(listaCreditos);

		} else if (action.equals("FILTRAR")) {

			String query = "SELECT o FROM ObjetoiEstado oe JOIN oe.objetoi o WHERE oe.estado = :estado AND oe.fechaHasta IS NULL ";

			if (!credito.isEmpty()) {

				query += "AND o.numeroAtencion = :numero ";

				Objetoi objetoi = (Objetoi) bp.createQuery(query).setParameter("numero", Long.parseLong(credito))
						.setParameter("estado", estadoCancelado).uniqueResult();

				if (objetoi != null)
					listaCreditos.add(objetoi);

			} else {

				Map<String, Object> parameters = new HashMap<String, Object>();

				if (!linea_id.isEmpty()) {
					query += "AND o.linea = :linea ";
					Linea linea = (Linea) bp.getById(Linea.class, Long.parseLong(linea_id));
					parameters.put("linea", linea);
				}

				if (!expediente.isEmpty()) {
					query += "AND o.expediente = :expediente ";
					parameters.put("expediente", expediente);
				}

				if (desde != null && hasta != null) {
					query += "AND oe.fecha BETWEEN :fechaDesde AND :fechaHasta ";
					parameters.put("fechaDesde", desde);
					parameters.put("fechaHasta", hasta);
				}

				parameters.put("estado", estadoCancelado);

				Query q = bp.createQuery(query);

				q.setProperties(parameters);

				listaCreditos = q.list();

			}

			if (listaCreditos != null && !listaCreditos.isEmpty())
				crearDTO(listaCreditos);

		} else if (action.equals("FINALIZAR")) {

			// idObjetoi = request.getParameter("idObjetoi");
			objetoi = (Objetoi) bp.getById(Objetoi.class, new Long(idObjetoi));
			if (verificarLiberaciones())
				forward = "FinalizarCredito";
			else {
				errores.put("finalizarCredito.error.garantiasNoLiberadas",
						"finalizarCredito.error.garantiasNoLiberadas");
				action = "FILTRAR";
				doProcess();
			}
		} else if (action.equals("GUARDAR")) {
			// if(!garantiasLiberadas){
			// errores.put("finalizarCredito.error.garantiasLiberadas",
			// "finalizarCredito.error.garantiasLiberadas");
			// forward = "FinalizarCredito";
			// return errores.isEmpty();
			// }

			if (!verifica()) {
				forward = "FinalizarCredito";
				return errores.isEmpty();
			}

			Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, Long.parseLong(idObjetoi));

			Estado estadoFinalizado = (Estado) bp.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = :nombre")
					.setParameter("nombre", "FINALIZADO").uniqueResult();

			ObjetoiEstado estadoAnterior = (ObjetoiEstado) bp
					.createQuery(
							"SELECT oe FROM ObjetoiEstado oe WHERE oe.objetoi = :credito AND oe.fechaHasta is null")
					.setParameter("credito", objetoi).uniqueResult();

			estadoAnterior.setFechaHasta(new Date());

			ObjetoiEstado estadoNuevo = new ObjetoiEstado();

			estadoNuevo.setEstado(estadoFinalizado);
			estadoNuevo.setFecha(new Date());
			estadoNuevo.setObjetoi(objetoi);

			objetoi.setFechaResolFin(fechaFirmaResolucion);
			objetoi.setNroResolFin(numeroResolucion);
			objetoi.setFechaMutuoFin(fechaFirmaFinalizacion);

			bp.save(estadoNuevo);
			bp.update(estadoAnterior);
			bp.update(objetoi);

			actualizarEstadoObjetoiGarantia(objetoi, estadoNuevo.getFecha());

			mensajeFinalizado = "El Cr�dito ha sido Finalizado";

			forward = "FinalizarCredito";

		} else if (action.equals("VOLVER")) {

			forward = "FinalizarCreditoList";

		}

		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void actualizarEstadoObjetoiGarantia(Objetoi objetoi2, Date fecha) {
		List<ObjetoiGarantia> og = bp
				.createQuery("SELECT g from ObjetoiGarantia g where g.objetoi = :credito and g.baja is null")
				.setParameter("credito", objetoi2).list();

		if (!og.isEmpty()) {
			for (ObjetoiGarantia objetoiGarantia : og) {
				if (objetoiGarantia != null) {
					GarantiaEstado nuevoEstadoGarantia = new GarantiaEstado(ComportamientoGarantiaEnum.FINALIZADA,
							fecha);
					nuevoEstadoGarantia.setObjetoiGarantia(objetoiGarantia);
					nuevoEstadoGarantia.determinarImporte();
					bp.save(nuevoEstadoGarantia);
				}
			}
		}
	}

	private void crearDTO(List<Objetoi> listaCreditos) {

		ObjetoiDTO dto;

		ObjetoiEstado estado;

		listaCreditosDTO = new ArrayList<ObjetoiDTO>();

		for (Objetoi credito : listaCreditos) {

			dto = new ObjetoiDTO(credito);

			estado = (ObjetoiEstado) bp.createQuery(
					"SELECT oe FROM ObjetoiEstado oe WHERE oe.objetoi = :credito AND oe.estado = :estado AND oe.fechaHasta IS NULL")
					.setParameter("credito", credito).setParameter("estado", estadoCancelado).uniqueResult();

			dto.setFechaCancelacionStr(estado.getFechaStr());

			listaCreditosDTO.add(dto);
		}
	}

	public boolean verifica() {
		boolean verifica = true;
		if (numeroResolucion == null || numeroResolucion.trim().isEmpty()) {
			errores.put("finalizarCredito.error.numeroResolucion", "finalizarCredito.error.numeroResolucion");
			verifica = false;
		}
		return verifica;
	}

	public boolean verificarLiberaciones() {
		Integer garantiasNoLiberadas = (Integer) bp
				.createSQLQuery("SELECT COUNT(*) FROM ObjetoiGarantia WHERE objetoi_id = :objetoi_id "
						+ "AND baja IS NULL AND fechaLiberada IS NULL")
				.setLong("objetoi_id", objetoi.getId()).uniqueResult();
		if (garantiasNoLiberadas == 0)
			return true;
		else
			return false;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return listaCreditosDTO;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public void setCredito(String credito) {
		this.credito = credito;
	}

	public void setDesdeStr(String desde) {
		this.desde = DateHelper.getDate(desde);
	}

	public String getDesdeStr() {
		return DateHelper.getString(desde);
	}

	public String getHastaStr() {
		return DateHelper.getString(hasta);
	}

	public void setHastaStr(String hasta) {
		this.hasta = DateHelper.getDate(hasta);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getExpediente() {
		return expediente;
	}

	public String getCredito() {
		return credito;
	}

	public Date getDesde() {
		return desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public List<ObjetoiDTO> getListaCreditosDTO() {
		return listaCreditosDTO;
	}

	public void setListaCreditosDTO(List<ObjetoiDTO> listaCreditosDTO) {
		this.listaCreditosDTO = listaCreditosDTO;
	}

	public String getLinea_id() {
		return linea_id;
	}

	public void setLinea_id(String linea_id) {
		this.linea_id = linea_id;
	}

	public String getFechaFirmaResolucionStr() {
		return DateHelper.getString(fechaFirmaResolucion);
	}

	public void setFechaFirmaResolucionStr(String fechaFirmaResolucion) {
		this.fechaFirmaResolucion = DateHelper.getDate(fechaFirmaResolucion);
	}

	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public String getFechaFirmaFinalizacionStr() {
		return DateHelper.getString(fechaFirmaFinalizacion);
	}

	public void setFechaFirmaFinalizacionStr(String fechaFirmaFinalizacion) {
		this.fechaFirmaFinalizacion = DateHelper.getDate(fechaFirmaFinalizacion);
	}

	public String getMensajeFinalizado() {
		return mensajeFinalizado;
	}

	public void setMensajeFinalizado(String mensajeFinalizado) {
		this.mensajeFinalizado = mensajeFinalizado;
	}

	public String getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(String idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Boolean getGarantiasLiberadas() {
		return garantiasLiberadas;
	}

	public void setGarantiasLiberadas(Boolean garantiasLiberadas) {
		this.garantiasLiberadas = garantiasLiberadas;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}
}
