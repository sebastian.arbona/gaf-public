package com.asf.cred.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Transient;

import org.apache.struts.upload.FormFile;

import com.asf.cred.estados.EstaObs;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.SolicitudProrroga;
import com.nirven.creditos.hibernate.SolicitudProrrogaEstado;

public class SolicitudProrrogaProcess implements IProcess {
	// ==========================ATRIBUTOS============================
	public static String separador = "@";
	public static Long agregaEstado = 1L;
	public static Long agregaObs = 2L;
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "ProrrogaEstados";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	private Estado estado;
	private boolean ocultarCampos = false;
	private List<EstaObs> estadosObservaciones;
	private SolicitudProrroga solicitudProrroga;
	private Long idSolicitudProrroga;
	private String obsEstado;
	private String detalleObservacion;
	private Long agrega;
	private List<SolicitudProrrogaEstado> solicitudProrrogaEstados;
	private SolicitudProrrogaEstado estadoActual;
	private Long idEstado;
	private Long numeroAtencion;
	private String numeroResolucion;
	private Date fechaResolucion;
	private FormFile formFile;
	private boolean estadoAprobada = false;

	// =======================CONSTRUCTORES=========================
	public SolicitudProrrogaProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	}

	@Override
	public boolean doProcess() {
		if (this.getAccion().equalsIgnoreCase("load")) {

			if (idSolicitudProrroga != null) {
				try {
					solicitudProrroga = (SolicitudProrroga) this.bp.getById(SolicitudProrroga.class,
							this.idSolicitudProrroga);
					this.forward = "ProrrogaEstados";
					return true;
				} catch (Exception e) {
					this.errores.put("SolicitudProrroga.Error.cargar", "SolicitudProrroga.Error.carga");
					e.printStackTrace();
				}
			}

			if (numeroAtencion != null) {

			}

		} else if (this.accion.equals("cancelar")) {
			this.forward = "BuscarSolicitudProrrogaList";

		} else if (this.accion.equals("guardar")) {

			if (idEstado != null) {
				try {
					estado = (Estado) this.bp.getById(Estado.class, this.idEstado);
				} catch (Exception e) {
					this.errores.put("Estado.Error.cargar", "Estado.Error.carga");
					e.printStackTrace();
				}
			}

			if (estado.getIdEstado() == 0 && agrega.equals(new Long(2))) {
				solicitudProrrogaEstados = bp.createQuery(
						"from SolicitudProrrogaEstado where solicitudProrroga.id = :idSolicitudProrroga and fechaHasta IS NULL")
						.setParameter("idSolicitudProrroga", solicitudProrroga.getId()).list();
				if (solicitudProrrogaEstados.size() > 0)
					estado = solicitudProrrogaEstados.get(0).getEstado();
			} else {
				estado = (Estado) bp.getById(Estado.class, estado.getId());
			}

			if (this.agrega.longValue() == agregaEstado.longValue()) {
				this.guardarNuevoEstado();
			} else if (this.agrega.longValue() == agregaObs.longValue()) {
				this.guardarObservacion();
			}
			if (this.errores.isEmpty()) {
				this.forward = "BuscarSolicitudProrrogaList";
			} else {
				this.forward = "ProrrogaEstados";
			}
		}
//		if (this.errores.isEmpty()){
//			SolicitudProrrogaPlantillaProcess sp = new SolicitudProrrogaPlantillaProcess(); 
//			sp.setAccion(null);
//			sp.setExpediente("");
//			return sp.doProcess(); 
//		}
		return this.errores.isEmpty();
	}

	private void guardarNuevoEstado() {
		estado = (Estado) bp.getById(Estado.class, estado.getId());
		solicitudProrroga = (SolicitudProrroga) this.bp.getById(SolicitudProrroga.class, this.idSolicitudProrroga);

		estadoAprobada = (estado.getNombreEstado()).trim().equalsIgnoreCase("APROBADA");

		try {

			SolicitudProrrogaEstado solicitudProrrogaEstado = new SolicitudProrrogaEstado();
			solicitudProrrogaEstado.setEstado(this.estado);
			solicitudProrrogaEstado.setSolicitudProrroga(this.solicitudProrroga);
			solicitudProrrogaEstado.setFechaDesde(new Date(System.currentTimeMillis()));
			solicitudProrrogaEstado.setObservacion(this.obsEstado);
			solicitudProrrogaEstado.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			this.validarNuevoEstado(solicitudProrrogaEstado);
			if (this.errores.isEmpty()) {
				if (estadoActual != null) {
					try {
						estadoActual.setFechaHasta(solicitudProrrogaEstado.getFechaDesde());
						this.bp.saveOrUpdate(estadoActual);
					} catch (IndexOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			}

			this.bp.saveOrUpdate(solicitudProrrogaEstado);
			this.estado = new Estado();
			this.obsEstado = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void validarNuevoEstado(SolicitudProrrogaEstado solicitudProrrogaEstado) {
		solicitudProrrogaEstado.validate();
		this.errores.putAll(solicitudProrrogaEstado.getChErrores());
		SolicitudProrrogaEstado estadoActual = getEstadoActual();
		if (estadoActual != null) {
			if (solicitudProrrogaEstado.getFechaDesde().before(estadoActual.getFechaDesde())) {
				this.errores.put("SolicitudProrrogaEstado.fecha.anterior", "SolicitudProrrogaEstado.fecha.anterior");
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Transient
	public SolicitudProrrogaEstado getEstadoActual() {
		estadoActual = new SolicitudProrrogaEstado();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		try {
			String consulta = "SELECT spe FROM SolicitudProrrogaEstado spe WHERE spe.fechaHasta IS NULL";
			consulta += " AND spe.solicitudProrroga.id=" + this.solicitudProrroga.getId()
					+ " ORDER BY spe.fechaDesde desc";
			List<SolicitudProrrogaEstado> historicoEstados = bp.getByFilter(consulta);
			if (historicoEstados.size() > 0) {
				estadoActual = historicoEstados.get(0);
			}
		} catch (Exception e) {
			estadoActual = null;
		}
		return estadoActual;
	}

	private void guardarObservacion() {

	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	public List<EstaObs> getEstadosObservaciones() {
		return estadosObservaciones;
	}

	public void setEstadosObservaciones(List<EstaObs> estadosObservaciones) {
		this.estadosObservaciones = estadosObservaciones;
	}

	public SolicitudProrroga getSolicitudProrroga() {
		return solicitudProrroga;
	}

	public void setSolicitudProrroga(SolicitudProrroga solicitudProrroga) {
		this.solicitudProrroga = solicitudProrroga;
	}

	public Long getIdSolicitudProrroga() {
		return idSolicitudProrroga;
	}

	public void setIdSolicitudProrroga(Long idSolicitudProrroga) {
		this.idSolicitudProrroga = idSolicitudProrroga;
	}

	public String getObsEstado() {
		return obsEstado;
	}

	public void setObsEstado(String obsEstado) {
		this.obsEstado = obsEstado;
	}

	public String getDetalleObservacion() {
		return detalleObservacion;
	}

	public void setDetalleObservacion(String detalleObservacion) {
		this.detalleObservacion = detalleObservacion;
	}

	public Long getAgrega() {
		return agrega;
	}

	public void setAgrega(Long agrega) {
		this.agrega = agrega;
	}

	public List<SolicitudProrrogaEstado> getSolicitudProrrogaEstados() {
		return solicitudProrrogaEstados;
	}

	public void setSolicitudProrrogaEstados(List<SolicitudProrrogaEstado> solicitudProrrogaEstados) {
		this.solicitudProrrogaEstados = solicitudProrrogaEstados;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public Date getFechaResolucion() {
		return fechaResolucion;
	}

	public void setFechaResolucion(Date fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public String getFechaResolucionStr() {
		return DateHelper.getString(fechaResolucion);
	}

	public void setFechaResolucionStr(String fechaResolucionStr) {
		this.fechaResolucion = DateHelper.getDate(fechaResolucionStr);
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public boolean isEstadoAprobada() {
		return estadoAprobada;
	}

	public void setEstadoAprobada(boolean estadoAprobada) {
		this.estadoAprobada = estadoAprobada;
	}

}
