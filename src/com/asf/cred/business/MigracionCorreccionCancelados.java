package com.asf.cred.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.cred.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class MigracionCorreccionCancelados extends ConversationProcess {

	private List<Ctacte> resultado = new ArrayList<Ctacte>();

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean run() {
		try {
			String sqlCuotas = IOUtils.toString(getClass().getResourceAsStream("migracion-correccion-cancelados.sql"));
			String sqlCuotasCap = IOUtils
					.toString(getClass().getResourceAsStream("migracion-correccion-cancelados-capital.sql"));

			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
			int periodo = Calendar.getInstance().get(Calendar.YEAR);
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;

			List<Object[]> ajustes = bp.createSQLQuery(sqlCuotas).list();
			List<Object[]> ajustesCap = bp.createSQLQuery(sqlCuotasCap).list();

			List<Object[]> rows = new ArrayList<Object[]>();
			rows.addAll(ajustes);
			rows.addAll(ajustesCap);

			Calendar c = Calendar.getInstance();
			c.set(2013, Calendar.DECEMBER, 31);
			Date fechaGen = c.getTime();

			for (Object[] r : rows) {
				Number idCuota = (Number) r[0];
				String concepto = (String) r[1];
				Number saldo = (Number) r[2];

				Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota.longValue());
				Ctacte cc = cuota.getCredito().crearCtacte(concepto, saldo.doubleValue(), fechaGen, null,
						cuota.getCredito(), cuota, null);
				cc.setFechaProceso(fechaGen);

				cc.setTipomov(tipoCredito);
				cc.setTipoMovimiento("movManualCred");
				cc.setDetalle("Ajuste por migracion");
				cc.setUsuario(usuario);
				cc.getId().setMovimientoCtacte(movimientoCtaCte);
				cc.getId().setItemCtacte(itemCtaCte++);

				bp.save(cc);

				resultado.add(cc);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "MigracionCorreccionCancelados";
	}

	public List<Ctacte> getResultado() {
		return resultado;
	}

	public int getCantidadAjustes() {
		if (resultado == null)
			return 0;
		return resultado.size();
	}
}
