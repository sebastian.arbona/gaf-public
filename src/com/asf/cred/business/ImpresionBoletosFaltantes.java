package com.asf.cred.business;

import java.util.HashMap;

import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;

public class ImpresionBoletosFaltantes implements IProcess {

	private String forward;
	private String accion;
	private ReportResult reporte;
	private String moneda;

	public ImpresionBoletosFaltantes() {
		forward = "ImpresionBoletosFaltantes";
	}

	@Override
	public boolean doProcess() {
		if (accion == null)
			return true;
		reporte = new ReportResult();
		reporte.setExport("pdf");
		if (moneda.equals("pesos"))
			reporte.setReportName("boletosFaltantesPesos.jasper");
		else
			reporte.setReportName("boletosFaltantesDolares.jasper");
		String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
		href += "/actions/process.do?do=process&processName=ImpresionBoletosFaltantes";
		href += "&process.moneda=" + moneda;
		reporte.setHref(href);
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		reporte.setParams("RELACION_COTOMADOR_ID=" + relacionCotomadorId);
		forward = "ReportesProcess2";
		return true;
	}

	@Override
	public HashMap<String, String> getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
}
