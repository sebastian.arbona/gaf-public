package com.asf.cred.business;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.hibernate.Query;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;

public class Anexo14Detalle extends ConversationProcess {

	private List<Anexo14DetalleBean> beans;
	private Date fechaDesde;
	private Date fechaHasta;
	private String filtrarFecha;
	private static String sql;
	private Long numeroBoleto;
	private String tipoBoleto;
	private String cuit;
	private String concepto;
	private String cuentaContable;
	private Long idEstado;
	private String comportamiento;
	private String areaResponsable;
	private Long idMoneda = 1L;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		if (fechaDesde == null || fechaHasta == null || filtrarFecha == null) {
			errors.put("anexo14detalle.obligatorios", "anexo14detalle.obligatorios");
			return false;
		}

		String estados = DirectorHelper.getString("Anexo14Estados");

		if (estados == null) {
			errors.put("anexo14.estados", "anexo14.estados");
			return false;
		}

		Calendar c = Calendar.getInstance();
		c.setTime(fechaHasta);
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		fechaHasta = c.getTime(); // condicion es fechaProceso < fechaHasta (hora 0)

		boolean ultimoDia = false;
		if (c.get(Calendar.DATE) == 31 && c.get(Calendar.MONTH) == Calendar.DECEMBER) {
			ultimoDia = true;
		}

		String periodo = Integer.toString(c.get(Calendar.YEAR));

		String[] ids = estados.split(",");
		Long[] idsEstados = new Long[ids.length];
		int i = 0;
		for (String id : ids) {
			idsEstados[i++] = new Long(id);
		}

		String sql = getSql().replace(":fechaDesde", new Timestamp(fechaDesde.getTime()).toString())
				.replace(":fechaHasta", new Timestamp(fechaHasta.getTime()).toString())
				.replace(":filtrarFecha", filtrarFecha)
				.replace(":tipoBoleto", tipoBoleto != null && !tipoBoleto.isEmpty() ? "'" + tipoBoleto + "'" : "null")
				.replace(":numeroBoleto", numeroBoleto != null && numeroBoleto > 0L ? numeroBoleto.toString() : "null")
				.replace(":cuit", cuit != null && !cuit.isEmpty() ? "'" + cuit + "'" : "null")
				.replace(":concepto", concepto != null && !concepto.isEmpty() ? "'" + concepto + "'" : "null")
				.replace(":cuentaContable",
						cuentaContable != null && !cuentaContable.isEmpty() ? "'" + cuentaContable + "'" : "null")
				.replace(":idEstado", idEstado != null && idEstado > 0L ? idEstado.toString() : "null")
				.replace(":comportamiento",
						comportamiento != null && !comportamiento.isEmpty() ? "'" + comportamiento + "'" : "null")
				.replace(":areaResponsable",
						areaResponsable != null && !areaResponsable.isEmpty() ? "'" + areaResponsable + "'" : "null")
				.replace(":idmoneda", idMoneda.toString()).replace(":ultimoDia", (ultimoDia ? "1" : "0"))
				.replace(":periodo", periodo);

		List<Object[]> result = bp.createSQLQuery(sql).setParameterList("idsEstados", idsEstados).list();

		String querySaldoInicial = "SELECT SUM(CASE WHEN con.concepto_concepto = 'cap' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS capital, "
				+ "SUM(CASE WHEN con.concepto_concepto in ('com','bon') THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS compensatorio, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'mor' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS moratorio, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'pun' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS punitorio, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'gas' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS gastos, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'rec' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS gastosRec, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'mul' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS multas, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'CER' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS cer, "
				+ "SUM(CASE WHEN con.concepto_concepto in ('cap','adc','adtc') THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS capitalPesos,  " + "SUM(CASE WHEN con.concepto_concepto in ('com' ,'bon') THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END  "
				+ "ELSE 0 END) AS compensatorioPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'mor' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS moratorioPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'pun' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS punitorioPesos,  " + "SUM(CASE WHEN con.concepto_concepto = 'gas' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS gastosPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'rec' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS gastosrecPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'mul' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS multasPesos " + "FROM Ctacte cc " + "JOIN Concepto con ON cc.asociado_id = con.id "
				+ "WHERE cc.objetoi_id = :idObjetoi ";
		if (filtrarFecha.equals("proceso")) {
			querySaldoInicial += "AND cc.fechaProceso < :fecha";
		} else {
			querySaldoInicial += "AND cc.fechaGeneracion < :fecha";
		}
		Query saldoInicial = bp.createSQLQuery(querySaldoInicial);

		String querySaldoFinal = "SELECT SUM(CASE WHEN con.concepto_concepto = 'cap' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS capital, "
				+ "SUM(CASE WHEN con.concepto_concepto IN ('com','bon') THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS compensatorio, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'mor' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS moratorio, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'pun' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS punitorio, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'gas' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS gastos, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'rec' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS gastosRec, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'mul' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS multas, "
				+ "SUM(CASE WHEN con.concepto_concepto = 'CER' THEN CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END ELSE 0 END) AS cer, "
				+ "SUM(CASE WHEN con.concepto_concepto in ('cap','adc','adtc') THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS capitalPesos,  " + "SUM(CASE WHEN con.concepto_concepto IN ('com','bon') THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END  "
				+ "ELSE 0 END) AS compensatorioPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'mor' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS moratorioPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'pun' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS punitorioPesos,  " + "SUM(CASE WHEN con.concepto_concepto = 'gas' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS gastosPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'rec' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS gastosrecPesos, " + "SUM(CASE WHEN con.concepto_concepto = 'mul' THEN "
				+ "CASE WHEN cc.dtype = 'CtaCteAjuste' THEN (cc.debePesos - cc.haberPesos) ELSE (CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * cc.cotizaMov END "
				+ "ELSE 0 END) AS multasPesos " + "FROM Ctacte cc " + "JOIN Concepto con ON cc.asociado_id = con.id "
				+ "WHERE cc.objetoi_id = :idObjetoi ";
		if (filtrarFecha.equals("proceso")) {
			if (!ultimoDia) {
				querySaldoFinal += "and cc.fechaProceso <= :fecha";
			} else {
				querySaldoFinal += "and cc.periodoCtacte <= :periodo";
			}
		} else {
			querySaldoFinal += "and cc.fechaGeneracion <= :fecha";
		}
		Query saldoFinal = bp.createSQLQuery(querySaldoFinal);

		Long idObjetoiActual = null;
		beans = new ArrayList<Anexo14DetalleBean>();
		for (int j = 0; j < result.size(); j++) {
			Object[] r = result.get(j);

			Long idObjetoi = ((Number) r[24]).longValue();

			if (idObjetoiActual == null || !idObjetoiActual.equals(idObjetoi)) {
				if (idObjetoiActual != null) {
					// saldo final para el registro anterior
					Object[] saldosFinales = null;
					if (filtrarFecha.equals("proceso") && ultimoDia) {
						saldosFinales = (Object[]) saldoFinal.setParameter("idObjetoi", idObjetoiActual)
								.setParameter("periodo", periodo).uniqueResult();
					} else {
						saldosFinales = (Object[]) saldoFinal.setParameter("idObjetoi", idObjetoiActual)
								.setParameter("fecha", fechaHasta).uniqueResult();
					}

					Anexo14DetalleBean sf = new Anexo14DetalleBean(result.get(j - 1), saldosFinales,
							Anexo14DetalleBean.Tipo.FINAL);
					beans.add(sf);
				}

				idObjetoiActual = idObjetoi;
				Object[] saldosIniciales = (Object[]) saldoInicial.setParameter("idObjetoi", idObjetoi)
						.setParameter("fecha", fechaDesde).uniqueResult();

				Anexo14DetalleBean si = new Anexo14DetalleBean(r, saldosIniciales, Anexo14DetalleBean.Tipo.INICIAL);
				beans.add(si);
			}

			if (r[1] != null) { // fecha generacion != null, es un movimiento
				Anexo14DetalleBean b = new Anexo14DetalleBean(r);
				beans.add(b);
			}
		}

		// saldo final para el ultimo registro
		if (idObjetoiActual != null) {
			Object[] saldosFinales = null;
			if (filtrarFecha.equals("proceso") && ultimoDia) {
				saldosFinales = (Object[]) saldoFinal.setParameter("idObjetoi", idObjetoiActual)
						.setParameter("periodo", periodo).uniqueResult();
			} else {
				saldosFinales = (Object[]) saldoFinal.setParameter("idObjetoi", idObjetoiActual)
						.setParameter("fecha", fechaHasta).uniqueResult();
			}

			Anexo14DetalleBean sf = new Anexo14DetalleBean(result.get(result.size() - 1), saldosFinales,
					Anexo14DetalleBean.Tipo.FINAL);
			beans.add(sf);
		}

		return true;
	}

	public List<Anexo14DetalleBean> getBeans() {
		return beans;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(Anexo14Detalle.class.getResourceAsStream("anexo14detalle.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	@Override
	protected String getDefaultForward() {
		return "Anexo14Detalle";
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public String getFechaDesdeStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaDesde);
	}

	public String getFechaHastaStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaHasta);
	}

	public String getFiltrarFecha() {
		return filtrarFecha;
	}

	public void setFiltrarFecha(String filtrarFecha) {
		this.filtrarFecha = filtrarFecha;
	}

	public Long getNumeroBoleto() {
		return numeroBoleto;
	}

	public void setNumeroBoleto(Long numeroBoleto) {
		this.numeroBoleto = numeroBoleto;
	}

	public String getTipoBoleto() {
		return tipoBoleto;
	}

	public void setTipoBoleto(String tipoBoleto) {
		this.tipoBoleto = tipoBoleto;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public String getComportamiento() {
		return comportamiento;
	}

	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}

	public String getAreaResponsable() {
		return areaResponsable;
	}

	public void setAreaResponsable(String areaResponsable) {
		this.areaResponsable = areaResponsable;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
}
