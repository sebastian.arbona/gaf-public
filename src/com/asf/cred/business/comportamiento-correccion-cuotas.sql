update Cuota 
set estado = '2'
where id in (
select c.id
from Ctacte cc
join Cuota c on cc.cuota_id = c.id
where c.emision_id is not null
and c.estado = '1'
group by c.id, c.credito_id, c.numero
having SUM(case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) < 0.005
)