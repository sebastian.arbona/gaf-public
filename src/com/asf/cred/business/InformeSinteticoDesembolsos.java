package com.asf.cred.business;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.cred.decorator.NegativeSeparatorDoubleDecorator;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Linea;

public class InformeSinteticoDesembolsos extends ConversationProcess {

	private List<Linea> lineas;
	private Long[] seleccion;
	private Date fechaDesde;
	private Date fechaHasta;
	private List<InformeSinteticoBean> resultado;
	private double montoAprobados;
	private int cantidadAprobados;

	@SuppressWarnings("unchecked")
	public InformeSinteticoDesembolsos() {
		lineas = bp.createQuery("select l from Linea l order by l.nombre").list();
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		try {
			String lineasIds = "";
			for (Long l : seleccion) {
				lineasIds += l + ",";
			}

			if (lineasIds.isEmpty()) {
				errors.put("informeSintetico.lineas", "informeSintetico.lineas");
				return false;
			}

			lineasIds = lineasIds.substring(0, lineasIds.length() - 1);

			String sql = IOUtils
					.toString(getClass().getResourceAsStream("/com/asf/cred/business/InformeSinteticoCosecha.sql"));

			sql = sql.replace("$P{FECHA_DESDE}", fechaDesde.toString());
			sql = sql.replace("$P{FECHA_HASTA}", fechaHasta.toString());
			sql = sql.replace("$P{LINEAS}", lineasIds);

			InformeSinteticoBean total = new InformeSinteticoBean();

			List<Object[]> rows = bp.createSQLQuery(sql).list();
			resultado = new ArrayList<InformeSinteticoBean>();
			for (Object[] valores : rows) {
				InformeSinteticoBean bean = new InformeSinteticoBean(valores);
				resultado.add(bean);
				total.add(bean);
			}

			total.setCantPendiente1(resultado.get(resultado.size() - 1).getCantPendiente1());
			total.setCantPendiente2(resultado.get(resultado.size() - 1).getCantPendiente2());
			total.setMontoPendiente1(resultado.get(resultado.size() - 1).getMontoPendiente1());
			total.setMontoPendiente2(resultado.get(resultado.size() - 1).getMontoPendiente2());
			total.setMontoPendienteTotal(resultado.get(resultado.size() - 1).getMontoPendienteTotal());
			resultado.add(total);

			// cuadro resumen
			String sqlResumen = "select count(*) as cantidad, sum(case when d.importeReal is null then d.importe else d.importeReal end) as monto from Desembolso d, Objetoi o "
					+ "where d.credito_id = o.id and d.estado <> '3' "
					+ "and o.fechaResolucion >= '$P{FECHA_DESDE}' and o.fechaResolucion <= '$P{FECHA_HASTA}' "
					+ "and d.numero in (1,2) and o.linea_id in ($P{LINEAS})";
			sqlResumen = sqlResumen.replace("$P{FECHA_DESDE}", fechaDesde.toString());
			sqlResumen = sqlResumen.replace("$P{FECHA_HASTA}", fechaHasta.toString());
			sqlResumen = sqlResumen.replace("$P{LINEAS}", lineasIds);

			Object[] valoresResumen = (Object[]) bp.createSQLQuery(sqlResumen).uniqueResult();
			cantidadAprobados = ((Number) valoresResumen[0]).intValue();
			montoAprobados = ((Number) valoresResumen[1]).doubleValue();

			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<InformeSinteticoBean> getResultado() {
		return resultado;
	}

	public Long[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(Long[] seleccion) {
		this.seleccion = seleccion;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	@Override
	protected String getDefaultForward() {
		return "InformeSinteticoDesembolsos";
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = new Date(DateHelper.getDate(fechaDesde).getTime());
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaDesde) {
		this.fechaHasta = new Date(DateHelper.getDate(fechaDesde).getTime());
	}

	public int getCantidadAprobados() {
		return cantidadAprobados;
	}

	public String getMontoAprobados() {
		return NegativeSeparatorDoubleDecorator.format.format(montoAprobados);
	}
}
