package com.asf.cred.business;

import java.io.Serializable;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class GarantiasCanceladas implements Serializable {

	private static final long serialVersionUID = 7946329452841252833L;

	private BusinessPersistance bp;

	public GarantiasCanceladas(BusinessPersistance bp) {
		this.bp = bp;
	}

	@SuppressWarnings("unchecked")
	public void ejecutar() {
		List<Number> ids = bp
				.createSQLQuery("select og.id from objetoigarantia og join objetoi o on og.objetoi_id = o.id "
						+ "join objetoiestado e on e.objetoi_id = o.id where e.estado_idestado = 12 and e.fechahasta is null and not exists ("
						+ "select ge.* from garantiaestado ge join estado eg on eg.idEstado = ge.estado_idEstado where og.id = ge.objetoigarantia_id and eg.comportamiento in ('cancelada','cancelada_acuerdo') and ge.importe >= 0) "
						+ "and og.baja is null")
				.list();

		if (ids != null && ids.size() > 0) {
			int cont = 0;
			for (Number id : ids) {
				ObjetoiGarantia og = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id.longValue());
				if (!og.getEstadoEntStr().equalsIgnoreCase("Ejecutada")) {
					if (og != null) {
						if (!og.getObjetoi().getTieneGarantiaFiduciaria()) {
							// Cancela la Garant�a del cr�dito. No tiene acuerdo de pago
							ComportamientoGarantiaEnum estado = ComportamientoGarantiaEnum.CANCELADA;
							GarantiaEstado estadoNuevo = new GarantiaEstado(estado,
									og.getObjetoi().getEstadoActual().getFecha());
							estadoNuevo.setObjetoiGarantia(og);
							estadoNuevo.determinarImporte();
							bp.save(estadoNuevo);
							if (og.getObjetoi().getLinea().isLineaAcuerdoPago()) {
								// Buscar y Cancelar Garant�a del cr�dito original
								Objetoi creditoOriginal = new Objetoi();
								String sqlCreditoOriginal = "SELECT o FROM Objetoi o WHERE o.acuerdoPago.id = :idAcuerdoPago ";
								creditoOriginal = (Objetoi) bp.createQuery(sqlCreditoOriginal)
										.setLong("idAcuerdoPago", og.getObjetoi().getId()).uniqueResult();
								if (creditoOriginal != null) {
									ObjetoiGarantia ogOriginal = new ObjetoiGarantia();
									String sqlGarantiaOriginal = "SELECT g FROM ObjetoiGarantia g WHERE g.objetoi.id = :idObjetoi AND g.garantia.id = :idGarantia";
									ogOriginal = (ObjetoiGarantia) bp.createQuery(sqlGarantiaOriginal)
											.setLong("idObjetoi", creditoOriginal.getId())
											.setLong("idGarantia", og.getGarantia().getId()).uniqueResult();
									if (ogOriginal != null) {
										GarantiaEstado estadoNuevoOriginal = new GarantiaEstado(
												ComportamientoGarantiaEnum.CANCELADA_ACUERDO,
												og.getObjetoi().getEstadoActual().getFecha());
										estadoNuevoOriginal.setObjetoiGarantia(ogOriginal);
										estadoNuevoOriginal.determinarImporte();
										bp.save(estadoNuevoOriginal);
									}
								}
							}
						}
					}
				}
				cont++;
			}
		}
	}
}
