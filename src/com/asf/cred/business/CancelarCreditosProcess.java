package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.struts.action.ProcessMethod;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;

public class CancelarCreditosProcess implements IProcess {

	private HashMap<String, Object> errores;
	private String mensaje;
	private String accion;
	private BusinessPersistance bp;

	public CancelarCreditosProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	public CancelarCreditosProcess(BusinessPersistance bp) {
		this.errores = new HashMap<String, Object>();
		this.bp = bp;
	}

	public boolean doProcess() {
		if (accion == null) {
			return errores.isEmpty();
		}
		if (accion.equals("cancelar")) {
			cancelar();
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean cancelar() {
		Estado estado = (Estado) bp.getNamedQuery("Estado.findByNombreEstado").setString("nombreEstado", "CANCELADO")
				.setMaxResults(1).uniqueResult();

		// Cancelo los cr�ditos no estan en estado ORIGINAL CON ACUERDO,
		// CANCELADO,
		// FINALIZADO, INCOBRABLE y Acuerdo Caido con saldo menor a 0.01
		List<Object> ids = bp.createSQLQuery(
				"select c.id from Objetoi c join Ctacte cc on cc.objetoi_id = c.id join ObjetoiEstado oe on oe.objetoi_id = c.id and oe.fechaHasta is null "
						+ "where oe.estado_idEstado not in (11,12,13,18,19) group by c.id, c.numeroAtencion, oe.estado_idEstado "
						+ "having SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) <= 0.01 order by c.id")
				.list();
		for (Object id : ids) {
			if (id instanceof Number) {
				Long idCredito = ((Number) id).longValue();
				Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idCredito);
				Date hoy = new Date();
				ObjetoiEstado estadoActual = objetoi.getEstadoActual();
				estadoActual.setFechaHasta(hoy);
				ObjetoiEstado nuevo = new ObjetoiEstado();
				nuevo.setFecha(hoy);
				nuevo.setObjetoi(objetoi);
				nuevo.setEstado(estado);
				bp.update(estadoActual);
				bp.save(nuevo);
			}
		}

		// Cancelo las garant�as que no est�n dadas de baja, cuyo cr�dito est�
		// Cancelado y no existe una garant�a para dicho cr�dito en estado
		// CANCELADA con importe mayor a 0
		GarantiasCanceladas gc = new GarantiasCanceladas(bp);
		gc.ejecutar();
		mensaje = ids.size() + " creditos cancelados";
		return true;
	}

	public String getMensaje() {
		return mensaje;
	}

	@Override
	public String getForward() {
		return "CancelarCreditosProcess";
	}

	@Override
	public String getInput() {
		return "CancelarCreditosProcess";
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

}
