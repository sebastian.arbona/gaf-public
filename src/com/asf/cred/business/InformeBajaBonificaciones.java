package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Cuota;

public class InformeBajaBonificaciones extends ConversationProcess {

	private Date fechaDesde;
	private Date fechaHasta = new Date();

	private Long idLinea;
	private Long idConvenio;

	private List<InformeBonificacionesBean> beans;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean generar() {
		if (fechaDesde == null || fechaHasta == null) {
			return false;
		}

		String sql = "select o.id, o.numeroAtencion, o.expediente, p.nomb_12, p.cuil_12, "
				+ "l.nombre as linea, case when e.nombreEstado = 'EJECUCION' "
				+ "or e.nombreEstado = 'PRIMER DESEMBOLSO' or e.nombreEstado = 'PENDIENTE SEGUNDO DESEMBOLSO' "
				+ "or e.nombreEstado = 'GESTION JUDICIAL' "
				+ "or e.nombreEstado = 'GESTION EXTRAJUDICIAL' then tcomp.tf_descripcion "
				+ "else 'N/A' end as comportamiento, "
				+ "e.nombreEstado, cb.nombre as convenio, cast(b.tasaBonificada as varchar) + '%', tbon.tf_descripcion, o.fechaBajaBonificacion "
				+ "from Objetoi o join Persona p on o.persona_IDPERSONA = p.IDPERSONA "
				+ "join Linea l on l.id = o.linea_id join ObjetoiBonificacion ob on o.id = ob.idCredito "
				+ "join Bonificacion b on b.id = ob.idBonificacion "
				+ "join ConvenioBonificacion cb on cb.id = b.convenio_id "
				+ "join ObjetoiComportamiento oc on oc.objetoi_id = o.id and oc.fechaHasta is null "
				+ "join Tipificadores tcomp on oc.comportamientoPago = tcomp.tf_codigo and tcomp.tf_categoria = 'comportamientoPago' "
				+ "join Tipificadores tbon on b.tipoDeBonificacion = tbon.tf_codigo and tbon.tf_categoria = 'bonificacion.tipo' "
				+ "join ObjetoiEstado oe on oe.objetoi_id = o.id "
				+ "join Estado e on e.idEstado = oe.estado_idEstado and oe.fechaHasta is null "
				+ "where o.fechaBajaBonificacion >= :fechaDesde and o.fechaBajaBonificacion <= :fechaHasta ";

		if (idLinea != null && idLinea != 0L) {
			sql += "and l.id = :idLinea ";
		}
		if (idConvenio != null && idConvenio != 0L) {
			sql += "and cb.id = :idConvenio";
		}

		Query query = bp.createSQLQuery(sql).setParameter("fechaDesde", fechaDesde).setParameter("fechaHasta",
				fechaHasta);

		if (idLinea != null && idLinea != 0L) {
			query.setParameter("idLinea", idLinea);
		}
		if (idConvenio != null && idConvenio != 0L) {
			query.setParameter("idConvenio", idConvenio);
		}

		List<Object[]> rows = query.list();

		Query queryCuota = bp.createQuery(
				"SELECT c FROM Cuota c WHERE c.credito.id = :idCredito AND c.fechaVencimiento <= :fecha AND c.estado = '1' ORDER BY c.fechaVencimiento")
				.setMaxResults(1);

		beans = new ArrayList<InformeBonificacionesBean>(rows.size());

		for (Object[] row : rows) {
			InformeBonificacionesBean bean = new InformeBonificacionesBean(row);

			Long idCredito = bean.getIdObjetoi();

			CreditoCalculoDeuda c = new CreditoCalculoDeuda(idCredito, bean.getFechaBaja());
			c.calcular();
			double deuda = c.getDeudaTotal();
			bean.setDeuda(deuda);

			List<Cuota> cuotas = (List<Cuota>) queryCuota.setParameter("idCredito", idCredito)
					.setParameter("fecha", bean.getFechaBaja()).list();

			if (!cuotas.isEmpty()) { // cuotas vencidas sin cancelar
				Cuota cuota = cuotas.get(0); // mas antigua sin cancelar
				int diasMora = (int) DateHelper.getDiffDates(cuota.getFechaVencimiento(), bean.getFechaBaja(), 2);
				bean.setDiasMora(diasMora);
			}

			beans.add(bean);
		}
		return false;
	}

	@Override
	protected String getDefaultForward() {
		return "InformeBajaBonificaciones";
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public Long getIdConvenio() {
		return idConvenio;
	}

	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}

	public List<InformeBonificacionesBean> getBeans() {
		return beans;
	}

	public void setBeans(List<InformeBonificacionesBean> beans) {
		this.beans = beans;
	}

}
