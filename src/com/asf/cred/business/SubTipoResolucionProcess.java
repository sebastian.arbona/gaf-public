package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.ProcesoTipoResolucion;
import com.nirven.creditos.hibernate.Resolucion;
import com.nirven.creditos.hibernate.SubTipoResolucion;
import com.nirven.creditos.hibernate.TipoProceso;
import com.nirven.creditos.hibernate.TipoResolucion;

public class SubTipoResolucionProcess implements IProcess {
	private String accion = "";
	private String forward = "errorPage";
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;
	// Atributos
	private List<SubTipoResolucion> subTiposResoluciones;
	private String nombre;
	private String observaciones;
	private boolean editable = true;
	private long idTipoResolucion;
	private List<TipoProceso> tipoProcesos;
	private List<TipoProceso> tipoProcesosSeleccionados = new ArrayList<TipoProceso>();
	private String[] seleccion;
	private String[] disponibles;
	private Long idSubTipoResolucion = null;
	SubTipoResolucion subTipoRes = null;

	public SubTipoResolucionProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		tipoProcesos = bp.createQuery("select tp from TipoProceso tp order by tp.nombreTipoProceso").list();
	}

	@Override
	public boolean doProcess() {
		if (this.accion.equals("list")) {
			this.forward = "SubTipoResolucionList";
			this.listarSubTiposResoluciones();
		} else if (this.accion.equals("nuevo")) {
			this.cargarSubTipoResolucion();
			this.forward = "SubTipoResolucion";
		} else if (this.accion.equals("guardar")) {
			this.guardarSubTipoResolucion();
			this.listarSubTiposResoluciones();
			this.forward = "SubTipoResolucionList";
		} else if (this.accion.equals("nuevoSubTipoResolucion")) {
			// this.guardarResolucion();
			this.forward = "SubTipoResolucionList";
		} else if (this.accion.equals("eliminar")) {
			eliminar();
			this.listarSubTiposResoluciones();
			this.forward = "SubTipoResolucionList";
		} else {
			this.listarSubTiposResoluciones();
			this.forward = "SubTipoResolucionList";
		}
		return this.errores.isEmpty();
	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		// Se valida que se haya seleccionado al menos un Proceso
		if (this.accion.equals("guardar") && this.seleccion == null) {
			errores.put("subTipoResolucion.proceso.error", "subTipoResolucion.proceso.error");
		}
		if (this.accion.equals("guardar")) {
			if (this.nombre == null || this.nombre.equals("")) {
				this.errores.put("subtiporesolucion.nombre.error", "subtiporesolucion.nombre.error");
			}
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			String consulta = "";
			consulta += "SELECT e FROM SubTipoResolucion e WHERE UPPER(e.nombre)='" + this.nombre.toUpperCase() + "'";
			consulta += " AND e.id!=" + this.idSubTipoResolucion;
			List<TipoProceso> estados = bp.getByFilter(consulta);
			if (estados.size() > 0) {
				this.errores.put("subtiporesolucion.nombre.repetido", "subtiporesolucion.nombre.repetido");
			}
		}
		return errores.isEmpty();
	}

	// Metodos internos
	private void listarSubTiposResoluciones() {
		String consulta = "";
		consulta = "SELECT r FROM SubTipoResolucion r";
		String filtro = "";
		subTiposResoluciones = bp.createQuery(consulta + filtro).list();
	}

	private void guardarSubTipoResolucion() {
		ArrayList<ProcesoTipoResolucion> listaProcesoTipoResolucion = null;
		// Si se esta modificando un SubTipo
		if (!idSubTipoResolucion.equals(new Long(0))) {
			subTipoRes = (SubTipoResolucion) bp.getById(SubTipoResolucion.class, idSubTipoResolucion);
			subTipoRes.setNombre(this.nombre);
			subTipoRes.setDescripcion(this.observaciones);
			TipoResolucion tipoResolucion = (TipoResolucion) bp.getById(TipoResolucion.class, this.idTipoResolucion);
			subTipoRes.setTipoResolucion(tipoResolucion);
			bp.update(subTipoRes);
			// Busco Los Tipos de Procesos Relacionados
			listaProcesoTipoResolucion = (ArrayList<ProcesoTipoResolucion>) bp.createQuery(
					"Select ptr from ProcesoTipoResolucion ptr where ptr.subTipoResolucion.id = :idSubTipoResolucion")
					.setParameter("idSubTipoResolucion", idSubTipoResolucion).list();
			// Borro las clases asociativas existentes
			for (int i = 0; listaProcesoTipoResolucion.size() > i; i++) {
				bp.delete(listaProcesoTipoResolucion.get(i));
			}
			// Creo las nuevas clases asociativas para los procesos que el usuario
			// selecciono
			for (int i = 0; seleccion.length > i; i++) {
				TipoProceso proceso = (TipoProceso) bp.getById(TipoProceso.class, new Long(seleccion[i]));
				ProcesoTipoResolucion procesoTipoRes = new ProcesoTipoResolucion();
				procesoTipoRes.setProceso(proceso);
				procesoTipoRes.setSubTipoResolucion(subTipoRes);
				bp.save(procesoTipoRes);
			}
		} else {
			subTipoRes = new SubTipoResolucion();
			subTipoRes.setNombre(this.nombre);
			subTipoRes.setDescripcion(this.observaciones);
			TipoResolucion tipoResolucion = (TipoResolucion) bp.getById(TipoResolucion.class, this.idTipoResolucion);
			subTipoRes.setTipoResolucion(tipoResolucion);
			bp.save(subTipoRes);
			for (int i = 0; seleccion.length > i; i++) {
				TipoProceso proceso = (TipoProceso) bp.getById(TipoProceso.class, new Long(seleccion[i]));
				ProcesoTipoResolucion procesoTipoRes = new ProcesoTipoResolucion();
				procesoTipoRes.setProceso(proceso);
				procesoTipoRes.setSubTipoResolucion(subTipoRes);
				bp.save(procesoTipoRes);
			}
		}
	}

	private void cargarSubTipoResolucion() {
		ArrayList<ProcesoTipoResolucion> listaProcesoTipoResolucion = null;
		if (idSubTipoResolucion != null) {
			SubTipoResolucion subTipoRes = (SubTipoResolucion) bp.getById(SubTipoResolucion.class, idSubTipoResolucion);
			this.nombre = subTipoRes.getNombre();
			this.observaciones = subTipoRes.getDescripcion();
			this.setIdTipoResolucion(subTipoRes.getTipoResolucion().getId());
			listaProcesoTipoResolucion = (ArrayList<ProcesoTipoResolucion>) bp.createQuery(
					"Select ptr from ProcesoTipoResolucion ptr where ptr.subTipoResolucion.id = :idSubTipoResolucion")
					.setParameter("idSubTipoResolucion", idSubTipoResolucion).list();
			// Busca los procesos seleccionados
			for (int i = 0; listaProcesoTipoResolucion.size() > i; i++) {
				tipoProcesosSeleccionados.add((TipoProceso) bp.getById(TipoProceso.class,
						listaProcesoTipoResolucion.get(i).getProceso().getId()));
			}
			// Elimina los procesos seleccionados de la lista de disponibles
			for (int i = 0; tipoProcesosSeleccionados.size() > i; i++) {
				for (int j = 0; tipoProcesos.size() > j; j++) {
					if (listaProcesoTipoResolucion.get(i).getProceso().equals(tipoProcesos.get(j))) {
						tipoProcesos.remove(j);
					}
				}
			}
		}
	}

	public void eliminar() {
		List<Resolucion> resolucionList = (List<Resolucion>) bp
				.createQuery("from Resolucion r where r.subTipoResolucion.id = " + this.idSubTipoResolucion).list();
		List<ProcesoTipoResolucion> procesoTipoResolucionList = (List<ProcesoTipoResolucion>) bp
				.createQuery("from ProcesoTipoResolucion r where r.subTipoResolucion.id = " + this.idSubTipoResolucion)
				.list();
		if (resolucionList.size() > 0) {
			errores.put("subTipoResolucion.resolucion.referencia.error",
					"subTipoResolucion.resolucion.referencia.error");
		} else {
			for (ProcesoTipoResolucion ptr : procesoTipoResolucionList) {
				bp.delete(ptr);
			}
			SubTipoResolucion subTipoResolucion = (SubTipoResolucion) bp
					.createQuery("from SubTipoResolucion r where r.id = " + this.idSubTipoResolucion).uniqueResult();
			bp.delete(subTipoResolucion);
		}
	}

	// Getters and Setters
	public List<SubTipoResolucion> getSubTiposResoluciones() {
		return subTiposResoluciones;
	}

	public void setSubTiposResoluciones(List<SubTipoResolucion> subTiposResoluciones) {
		this.subTiposResoluciones = subTiposResoluciones;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public long getIdTipoResolucion() {
		return idTipoResolucion;
	}

	public void setIdTipoResolucion(long idTipoResolucion) {
		this.idTipoResolucion = idTipoResolucion;
	}

	public String[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String[] seleccion) {
		this.seleccion = seleccion;
	}

	public String[] getDisponibles() {
		return disponibles;
	}

	public void setDisponibles(String[] disponibles) {
		this.disponibles = disponibles;
	}

	public List<TipoProceso> getTipoProcesos() {
		return tipoProcesos;
	}

	public void setTipoProcesos(List<TipoProceso> tipoProcesos) {
		this.tipoProcesos = tipoProcesos;
	}

	public List<TipoProceso> getTipoProcesosSeleccionados() {
		return tipoProcesosSeleccionados;
	}

	public void setTipoProcesosSeleccionados(List<TipoProceso> tipoProcesosSeleccionados) {
		this.tipoProcesosSeleccionados = tipoProcesosSeleccionados;
	}

	public Long getIdSubTipoResolucion() {
		return idSubTipoResolucion;
	}

	public void setIdSubTipoResolucion(Long idSubTipoResolucion) {
		this.idSubTipoResolucion = idSubTipoResolucion;
	}
}
