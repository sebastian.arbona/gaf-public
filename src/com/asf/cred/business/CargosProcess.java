/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asf.cred.business;

import com.civitas.hibernate.persona.*;
import com.nirven.creditos.hibernate.Cargo;
import com.nirven.creditos.hibernate.CargoPersona;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author cnoguerol
 */
public class CargosProcess implements IProcess {
    // =========================ATRIBUTOS==================================
    private String forward = "CargoPersona";
    private HashMap<String, Object> errors;
    private String accion = "listar";
    private BusinessPersistance bp;
    private Persona personaTitular;
    private Cargo cargo;
    private Long idCargo;
    private Long idCargoPersona;
    private com.nirven.creditos.hibernate.CargoPersona cargoPersona;
    private List<CargoPersona> listaCargos;

    // =========================CONSTRUCTORES===============================
    public CargosProcess() {
    	
        this.errors = new HashMap<String, Object>();
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        this.cargoPersona = new com.nirven.creditos.hibernate.CargoPersona();
        this.personaTitular = new Persona();
        this.cargo = new Cargo();
    }

    // =========================FUNCIONALIDAD===============================
    public boolean doProcess() {
        if (this.getAccion().equalsIgnoreCase("asignar")) {
            this.forward = "CargoPersona";
        } else if (this.getAccion().equalsIgnoreCase("guardar")) {
            if (this.errors.isEmpty()) {
                this.asignarCargo();
                if (this.errors.isEmpty()) {
                	this.listarCargosPersona();
                    this.forward = "CargoPersonaList";
                } else {
                	this.listarCargosPersona();
                    this.forward = "CargoPersona";
                }
            } else {
            	this.listarCargosPersona();
                this.forward = "CargoPersona";
            }
        } else if (this.accion.equalsIgnoreCase("cancelar")) {
        	this.listarCargosPersona();
            this.forward = "CargoPersonaList";
        } else if (this.getAccion().equalsIgnoreCase("eliminar")) {
            this.eliminarCargo();
            forward = "CargoList";
        } else if (this.getAccion().equalsIgnoreCase("load")) {
            this.cargarCargo();
            this.listarCargosPersona();
            this.forward = "CargoPersona";
        }else if (this.getAccion().equalsIgnoreCase("elimnarCargoPersona")) {
        	this.eliminarCargoPersona();
        	this.listarCargosPersona();
        }
        else if (this.getAccion().equalsIgnoreCase("listar")) {
        	
        	this.listarCargosPersona();
        }

        

        return this.errors.isEmpty();
    }

    // =========================UTILIDADES PRIVADAS=============================

    private void asignarCargo() {
        try {

        	this.setPersonaTitular((Persona) this.bp.getById(Persona.class, this.getPersonaTitular().getIdpersona()));
            this.getCargoPersona().setPersona(this.getPersonaTitular());
            CargoPersona cp = (CargoPersona)bp.createQuery("from CargoPersona cp where cp.persona.id = "+this.personaTitular.getId()+" and fechaHasta is null order by fecha desc").uniqueResult();
            
            //No existen cargos para esa persona
            if(cp != null){
            	if(cp.getFechaHasta() == null){
            		
            		cp.setFechaHasta(cargoPersona.getFecha());
                	this.bp.saveOrUpdate(cp);
            	}
            	
            }
            
            this.setCargo((Cargo) this.bp.getById(Cargo.class, cargoPersona.getCargo().getId()));
            this.getCargoPersona().setCargo(cargo);
            this.bp.saveOrUpdate(cargoPersona);
            
            this.setCargoPersona(new CargoPersona());
        }
        catch (Exception e) {
            this.errors.put("Cargo.Error.guardar", "Cargo.Error.guardar");
            e.printStackTrace();
        }
    }
    
    

    private void eliminarCargo() {
    	
        if (this.idCargo != null) {
            try {
            	ArrayList<CargoPersona> cargoPersonaList = (ArrayList<CargoPersona>)bp.createQuery("from CargoPersona c where c.cargo.idCargo = "+idCargo).list();
            	if(cargoPersonaList.isEmpty()){
            		Cargo cargo= (Cargo)bp.getById(Cargo.class, idCargo);
                    this.bp.delete(cargo);
            	}else{
            		this.errors.put("Cargo.Error.eliminar.vinculado", "Cargo.Error.eliminar.vinculado");
            	}
            	
            }
            catch (Exception e) {
                this.errors.put("Cargo.Error.eliminar", "Cargo.Error.eliminar");
                e.printStackTrace();
            }
        }
    }
    
    private void eliminarCargoPersona() {
        if (this.idCargoPersona != null) {
            try {
            	CargoPersona cargoPersona = (CargoPersona)bp.getById(CargoPersona.class, idCargoPersona);
                this.bp.delete(cargoPersona);
            }
            catch (Exception e) {
                this.errors.put("Cargo.Error.eliminar", "Cargo.Error.eliminar");
                e.printStackTrace();
            }
        }
    }
    
    

    private void cargarCargo() {

    	if (this.getCargoPersona() != null) {
            try {
                
            	String consulta = "from CargoPersona cp where cp.persona.id = "+personaTitular.getId()+" and cp.cargo.idCargo = "+this.getCargo().getId()+" order by fecha desc";
                ArrayList<CargoPersona> listaCargosPersona = (ArrayList<CargoPersona>)this.bp.getByFilter(consulta);
                if(!listaCargosPersona.isEmpty()){
                	this.setCargoPersona(listaCargosPersona.get(0));
                }else{
                	this.setCargoPersona((CargoPersona)bp.getById(CargoPersona.class, this.getIdCargoPersona()));
                }
              
                
         
            }
            catch (Exception e) {
                this.setCargoPersona(new CargoPersona());
                this.errors.put("Contacto.Error.cargar", "Contacto.Error.carga");
                e.printStackTrace();
            }
        }
    }

    private void listarCargosPersona() {
        if (this.personaTitular != null) {
            try {
                this.personaTitular = (Persona) this.bp.getById(Persona.class, this.personaTitular.getId());
                String consulta = "SELECT cp FROM CargoPersona cp";
                consulta += " WHERE cp.persona.id = '" + this.personaTitular.getId().longValue() + "'";
                this.listaCargos = this.bp.getByFilter(consulta);
            }
            catch (Exception e) {
            	e.printStackTrace();
                this.listaCargos = new ArrayList<CargoPersona>();
            }
        }
        
        this.forward = "CargoPersonaList";
    }

    // =========================GETTERS y SETTERS=============================
    public HashMap<String, Object> getErrors() {
        return this.errors;
    }

    public Object getResult() {
        return null;
    }

    public String getInput() {
        return this.forward;
    }

    public String getForward() {
        return this.forward;
    }

    public String getAccion() {
        return this.accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Persona getPersonaTitular() {
        return this.personaTitular;
    }

    public void setPersonaTitular(Persona personaTitular) {
        this.personaTitular = personaTitular;
    }

    
    public com.nirven.creditos.hibernate.CargoPersona getCargoPersona() {
		return cargoPersona;
	}

	public void setCargoPersona(
			com.nirven.creditos.hibernate.CargoPersona cargoPersona) {
		this.cargoPersona = cargoPersona;
	}
	
	

	public List<CargoPersona> getListaCargos() {
		return listaCargos;
	}

	public void setListaCargos(List<CargoPersona> listaCargos) {
		this.listaCargos = listaCargos;
	}
	
	

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}
	
	

	public Long getIdCargoPersona() {
		return idCargoPersona;
	}

	public void setIdCargoPersona(Long idCargoPersona) {
		this.idCargoPersona = idCargoPersona;
	}

	// =========================VALIDACIONES=============================
    public boolean validate() {
    	
    	if(this.getAccion().equalsIgnoreCase("guardar") && (cargoPersona.getCargo().getId() == null || cargoPersona.getCargo().getId().equals(new Long(0)))){
    		this.errors.put("CargoPersona.Error.cargo", "CargoPersona.Error.cargo");
    		
    		
    	}
    	if(this.getAccion().equalsIgnoreCase("guardar") && (cargoPersona.getFecha() == null)){
    		this.errors.put("CargoPersona.Error.fechaDesde", "CargoPersona.Error.fechaDesde");
    	}
    	if(this.getAccion().equalsIgnoreCase("guardar") && (cargoPersona.getNormaLegal() == null || cargoPersona.getNormaLegal().equals(""))){
    		this.errors.put("CargoPersona.Error.normalLegal", "CargoPersona.Error.normalLegal");
    	}
    	
    	
        return this.errors.isEmpty();
        
    }

}
