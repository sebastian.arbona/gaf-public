package com.asf.cred.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.upload.FormFile;
import org.hibernate.transform.Transformers;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.nirven.creditos.hibernate.Analisis;
import com.nirven.creditos.hibernate.Especialidad;
import com.nirven.creditos.hibernate.EspecialidadPersona;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;

public class CreditoAnalisisCrediticio implements IProcess {

	private String forward;
	private String accion = "list";
	private List<Objetoi> creditos;
	private List<EtapaRsaDTO> listaEtapas;
	private Objetoi credito;
	private Long idSolicitud;
	private List<Analisis> analisisList;
	private Long idAnalisis;
	private Analisis analisis;
	private boolean lista;
	private FormFile formFile;
	private BusinessPersistance bp;
	private String esquemaRSA;

	public CreditoAnalisisCrediticio() {
		forward = "AnalisisCred";
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lista = true;
		analisis = new Analisis();
		analisis.setEspecialista(new EspecialidadPersona());
		analisis.getEspecialista().setEspecialidad(new Especialidad());
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion.equals("list")) {
			if (idSolicitud != null && idSolicitud != 0) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("analisis.idSolicitud",
						idSolicitud);
			} else {
				idSolicitud = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("analisis.idSolicitud");
			}
			credito = (Objetoi) bp.getById(Objetoi.class, idSolicitud);
			creditos = new ArrayList<Objetoi>();
			creditos.add(credito);
			analisisList = bp.getNamedQuery("Analisis.findByCredito").setParameter("credito", idSolicitud).list();
			credito = (Objetoi) bp.getById(Objetoi.class, idSolicitud);
			esquemaRSA = DirectorHelper.getString("SRSA");

			if (!(esquemaRSA == null || esquemaRSA.trim().isEmpty())) {
				String consulta = "select t.TF_DESCRIPCION as 'nombreEtapa', a.descripcion as 'tarea', t2.TF_DESCRIPCION as 'sector', e.responsable_causerK as 'responsable', pa.fechaIngreso as 'fechaIngreso', "
						+ "e.estado, a.fechaInicio, a.fechaFinal from " + esquemaRSA + ".proyectoanalisis pa "
						+ "inner join " + esquemaRSA + ".objetoi o on pa.credito_id = o.id inner join " + esquemaRSA
						+ ".etapa e on pa.id = e.proyecto_id and e.orden = (select MAX(e2.orden) from " + esquemaRSA
						+ ".etapa e2 where e2.id = e.id) left join " + esquemaRSA
						+ ".actividad a on e.id = a.etapa_id and a.orden = (select MAX(a2.orden) from " + esquemaRSA
						+ ".actividad a2 where a2.etapa_id = e.id) inner join " + esquemaRSA
						+ ".tipificadores t on t.TF_CODIGO = e.tipoEtapa and t.TF_CATEGORIA = 'etapa.tipo' inner join "
						+ esquemaRSA
						+ ".tipificadores t2 on t2.TF_CODIGO = e.sector and t2.TF_CATEGORIA = 'analisis.tipo' "
						+ "where o.id = :id";
				listaEtapas = bp.createSQLQuery(consulta).setParameter("id", credito.getId())
						.setResultTransformer(Transformers.aliasToBean(EtapaRsaDTO.class)).list();
			} else {
				listaEtapas = null;
			}

		} else if (accion.equalsIgnoreCase("ver")) {
			analisis = (Analisis) bp.getById(Analisis.class, idAnalisis);
			this.setIdSolicitud(analisis.getObjetoi().getId());
			lista = false;
		} else if (accion.equalsIgnoreCase("eliminar")) {
			analisis = (Analisis) bp.getById(Analisis.class, idAnalisis);
			ObjetoiArchivo oa = analisis.getObjetoiArchivo();
			analisis.setObjetoiArchivo(null);
			bp.update(analisis);
			bp.delete(analisis);
			if (oa != null)
				bp.delete(oa);
			accion = "list";
			doProcess();
		} else if (accion.equalsIgnoreCase("nuevo")) {
			lista = false;
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().removeAttribute("analisis.modifica");
		} else if (accion.equalsIgnoreCase("guardar")) {
			Analisis analisisBdd = null;
			if (idSolicitud == null || idSolicitud == 0) {
				idSolicitud = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("analisis.idSolicitud");
			}
			if (idAnalisis != null && idAnalisis.longValue() > 0) {
				analisisBdd = (Analisis) bp.getById(Analisis.class, idAnalisis);
				analisis.setId(analisisBdd.getId());
			} else {
				analisisBdd = new Analisis();
			}
			if (analisis.getId() == null) {
				analisis.setObjetoi((Objetoi) bp.getById(Objetoi.class, idSolicitud));
			} else if (analisisBdd != null) {
				analisis.setObjetoi(analisisBdd.getObjetoi());
			}
			if (analisis.getEspecialista() != null && analisis.getEspecialista().getId() == null) {
				analisis.setEspecialista(null);
			}
			if (formFile != null) {
				ObjetoiArchivo objetoiArchivo = analisisBdd.getObjetoiArchivo();
				if (objetoiArchivo == null) {
					objetoiArchivo = new ObjetoiArchivo();
					objetoiArchivo.setCredito(analisis.getObjetoi());
					analisis.setObjetoiArchivo(objetoiArchivo);
				}
				analisis.setObjetoiArchivo(objetoiArchivo);
				try {
					objetoiArchivo.setArchivo(formFile.getFileData());
					objetoiArchivo.setMimetype(formFile.getContentType());
					objetoiArchivo.setNombre(formFile.getFileName());
					objetoiArchivo.setFechaAdjunto(new Date());
					objetoiArchivo.setNoBorrar(true);
					objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
					bp.saveOrUpdate(objetoiArchivo);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			bp.getCurrentSession().evict(analisisBdd);
			bp.saveOrUpdate(analisis);
			accion = "list";
			doProcess();
		} else if (accion.equalsIgnoreCase("cancelar")) {
			accion = "list";
			doProcess();
		}
		return true;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public List<Analisis> getAnalisisList() {
		return analisisList;
	}

	public void setAnalisisList(List<Analisis> analisisList) {
		this.analisisList = analisisList;
	}

	public Long getIdAnalisis() {
		return idAnalisis;
	}

	public void setIdAnalisis(Long idAnalisis) {
		this.idAnalisis = idAnalisis;
	}

	public Analisis getAnalisis() {
		return analisis;
	}

	public void setAnalisis(Analisis analisis) {
		this.analisis = analisis;
	}

	public boolean isLista() {
		return lista;
	}

	public void setLista(boolean lista) {
		this.lista = lista;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public List<EtapaRsaDTO> getListaEtapas() {
		return listaEtapas;
	}

	public void setListaEtapas(List<EtapaRsaDTO> listaEtapas) {
		this.listaEtapas = listaEtapas;
	}

}
