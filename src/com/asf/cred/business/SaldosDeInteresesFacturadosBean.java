package com.asf.cred.business;

public class SaldosDeInteresesFacturadosBean {
	private String proyecto;
	private String numeroCredito;
	private String linea;
	private String expediente;
	private String resolucion;
	private String titular;
	private String cuil;
	private double totalInteresesCompensatorios;
	private double totalNotasDeCredito;
	private double totalNotasDeDebito;
	private double pagosIntComp;
	private String estado;
	private String comportamiento;
	
	public SaldosDeInteresesFacturadosBean() {
	}

	public String getProyecto() {
		return this.proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public String getNumeroCredito() {
		return this.numeroCredito;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getLinea() {
		return this.linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getExpediente() {
		return this.expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getResolucion() {
		return this.resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public String getTitular() {
		return this.titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getCuil() {
		return this.cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public double getTotalInteresesCompensatorios() {
		return this.totalInteresesCompensatorios;
	}

	public void setTotalInteresesCompensatorios(double totalInteresesCompensatorios) {
		this.totalInteresesCompensatorios = totalInteresesCompensatorios;
	}

	public double getTotalNotasDeCredito() {
		return this.totalNotasDeCredito;
	}

	public void setTotalNotasDeCredito(double totalNotasDeCredito) {
		this.totalNotasDeCredito = totalNotasDeCredito;
	}

	public double getTotalNotasDeDebito() {
		return this.totalNotasDeDebito;
	}

	public void setTotalNotasDeDebito(double totalNotasDeDebito) {
		this.totalNotasDeDebito = totalNotasDeDebito;
	}

	public double getPagosIntComp() {
		return this.pagosIntComp;
	}

	public void setPagosIntComp(double pagosIntComp) {
		this.pagosIntComp = pagosIntComp;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getComportamiento() {
		return this.comportamiento;
	}

	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}
}