package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.ActualizacionIndice;

public class ConsultaActualizacionIndice extends ConversationProcess {

	private List<ActualizacionIndiceBean> resultado;
	private Date fechaDesde;
	private Date fechaHasta;

	public ConsultaActualizacionIndice() {
		fechaHasta = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		fechaDesde = c.getTime();
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscar() {
		if (fechaDesde == null || fechaHasta == null) {
			return false;
		}

		List<ActualizacionIndice> lista = bp
				.createQuery("select a from ActualizacionIndice a " + "where a.fecha >= :desde and a.fecha <= :hasta "
						+ "order by a.fecha desc, a.ctacte.id.objetoi.id asc, a.ctacte.cuota.numero asc")
				.setParameter("desde", fechaDesde).setParameter("hasta", fechaHasta).list();

		resultado = new ArrayList<ActualizacionIndiceBean>();
		for (ActualizacionIndice a : lista) {
			ActualizacionIndiceBean b = new ActualizacionIndiceBean();
			b.setCredito(a.getCtacte().getId().getObjetoi());
			b.setIndice(a.getIndiceValor());
			b.setMontoActualizacion(a.getCtacte().getImporte());
			b.setNumeroCuota(a.getCtacte().getCuota().getNumero());
			b.setSaldoCapital(a.getSaldoCapital());
			b.setFecha(a.getFecha());
			b.setUsuario(a.getUsuario());
			resultado.add(b);
		}

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "ConsultaActualizacionIndice";
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaDesde(String f) {
		this.fechaDesde = DateHelper.getDate(f);
	}

	public void setFechaHasta(String f) {
		this.fechaHasta = DateHelper.getDate(f);
	}

	public List<ActualizacionIndiceBean> getResultado() {
		return resultado;
	}

}
