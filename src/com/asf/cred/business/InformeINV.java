package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.asf.cred.business.inv.AccionINV;
import com.asf.gaf.TipificadorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.importacion.Fecha;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;

public class InformeINV implements IProcess {
	private List<ObjetoiVinedo> vinedos = new ArrayList<ObjetoiVinedo>();
	private Long idVinedo;
	private Double qqAprobado;
	private Date desde;
	private Date hasta;
	private AccionINV accion;
	private String accionAnterior;
	private String accionReal;
	private BusinessPersistance bp;
	private String forward;
	private String estado;
	private HashMap<String, Object> errores;
	private String so;
	private String sn;
	private String re;
	private String rcf;
	private String rcinv;
	private String codigo;
	private double qq;
	private String observacion;
	private String observacionINV;
	private boolean min;
	private double estimados;
	private double hectareas;
	private String titulo;
	private boolean entra;
	private int INV;
	private boolean tieneGarantia;
	private String idPersona;
	private boolean listarSoloPreabrobados;
	private boolean listarDistintosAPreaprobados;
	public final static String E1 = "Solicitar Verificación Técnica (INV)";
	public final static String E2 = "Solicitudes Pendientes de Informe";
	public final static String E3 = "Solicitudes con Verificación Técnica Realizada";
	public final static String E4 = "Rectificación de Verificación Técnica";

	public InformeINV() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.desde = new Date();
		this.hasta = new Date();
		this.errores = new HashMap<String, Object>();
		this.titulo = E1;
		try {
			this.so = TipificadorHandler.getDescripcion("estadoSolicitudINV", "SO");
			this.sn = TipificadorHandler.getDescripcion("estadoSolicitudINV", "SN");
			this.re = TipificadorHandler.getDescripcion("estadoSolicitudINV", "RE");
			this.rcf = TipificadorHandler.getDescripcion("estadoSolicitudINV", "RCF");
			this.rcinv = TipificadorHandler.getDescripcion("estadoSolicitudINV", "RCINV");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean doProcess() {
		if (accionReal == null || accionReal.equals("")) {
			if (accion != null && (accion.equals(AccionINV.LISTAR_VINEDOS_INFORMADOS)
					|| accion.equals(AccionINV.LISTAR_VINEDOS_SOLICITADO)
					|| accion.equals(AccionINV.LISTAR_VINEDOS_SOLICITADO_PRE_AP))) {
				accionAnterior = accion.name();
				forward = "SolicitudINV";
				return true;
			} else if (accion == null) {
				accion = AccionINV.LISTAR_VINEDOS_PENDIENTE;
			} else {
				// seguir
			}
		} else {
			setAccionStr(accionReal);
			accionAnterior = new String(accionReal);
		}
		switch (accion) {
		case LISTAR_VINEDOS_PENDIENTE:
			titulo = E1;
			listarVinedos("SN");
			min = true;
			forward = "SolicitudINV";
			accionReal = null;
			break;
		case LISTAR_VINEDOS_SOLICITADO:
			titulo = E2;
			estado = "Solicitado";
			listarDistintosAPreaprobados = true;
			listarVinedos("SO");
			forward = "SolicitudINV";
			accionReal = null;
			break;
		case LISTAR_VINEDOS_SOLICITADO_PRE_AP:
			titulo = E2;
			estado = "Solicitado";
			listarSoloPreabrobados = true;
			listarVinedos("SO");
			Iterator<ObjetoiVinedo> iterator = vinedos.iterator();
			ObjetoiVinedo objetoiVinedo;
			while (iterator.hasNext()) {
				objetoiVinedo = iterator.next();
				if (objetoiVinedo.getCredito().getEstadoActual() != null && !objetoiVinedo.getCredito()
						.getEstadoActual().getEstado().getNombreEstado().trim().equals("ANALISIS")) {
					iterator.remove();
				}
			}
			setAccionStr("LISTAR_VINEDOS_SOLICITADO");
			forward = "SolicitudINV";
			accionReal = null;
			break;
		case INFORMAR_VINEDO:
			titulo = E2;
			Long id = idVinedo;
			try {
				informarVinedo();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (id != null && id > 0) {
				bp.begin();
				ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, id);
				try {
					if (vinedo.getCredito().getLinea().isLineaCosecha()) {
						setAccionStr("LISTAR_VINEDOS_SOLICITADO_PRE_AP");
						this.doProcess();
						break;
					} else {
						setAccionStr("LISTAR_VINEDOS_SOLICITADO");
						this.doProcess();
						break;
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				listarVinedos("SO");
				INV = 1;
				forward = "SolicitudINV";
				break;
			}
		case LISTAR_VINEDOS_INFORMADOS:
			titulo = E3;
			listarVinedos("RE");
			min = true;
			forward = "SolicitudINV";
			accionReal = null;
			break;
		case ENVIAR_PENDIENTES:
			titulo = E1;
			enviarSolicitudesINV();
			listarVinedos("SN");
			forward = "SolicitudINV";
			break;
		case INV_RECTIFICACION_VINEDO:
			titulo = E2;
			rectificarINV();
			listarDistintosAPreaprobados = true;
			listarVinedos("SO");
			forward = "SolicitudINV";
			break;
		case FONDO_RECTIFICACION_VINEDO:
			titulo = E3;
			rectificarFondo();
			listarVinedos("RE");
			forward = "SolicitudINV";
			break;
		case LISTAR_RECTIFICACIONF_PENDIENTE:
			titulo = E4;
			rectificarINV();
			listarVinedos("RCF");
			forward = "SolicitudINV";
			break;
		case SAVE_RECTIFICACIONF_PENDIENTE:
			titulo = E4;
			rectificarVinedoINV();
			listarVinedos("RCF");
			forward = "SolicitudINV";
			break;
		}
		forward = "SolicitudINV";
		return (errores.size() <= 0);
	}

	private void rectificarFondo() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			vinedo.setFechaInformeINV(null);
			vinedo.setQqAprobado((new Double(qq)));
			vinedo.setObservaciones(observacion);
			vinedo.setObservacionINV(observacionINV);
			try {
				String estado = TipificadorHandler.getDescripcion("estadoSolicitudINV", "RCINV");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
			observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_VINEDOS_INFORMADOS;
		listarVinedos("RE");
	}

	private void rectificarVinedoINV() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			// vinedo.setHectareas((double) hectareas);
			vinedo.getVinedo().setCodigo((String) codigo);
			vinedo.setQqEstimados((double) estimados);
			vinedo.setFechaInformeINV(null);
			vinedo.setObservaciones(observacion);
			vinedo.setObservacionINV(observacionINV);
			try {
				String estado = TipificadorHandler.getDescripcion("estadoSolicitudINV", "SO");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
			observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE;
		listarVinedos("RCF");
	}

	private void rectificarINV() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			vinedo.setFechaInformeINV(null);
			vinedo.setQqAprobado(new Double(qq));
			vinedo.setObservacionINV(observacionINV);
			try {
				String estado = TipificadorHandler.getDescripcion("estadoSolicitudINV", "RCF");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
			observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_VINEDOS_SOLICITADO;
	}

	private void listarVinedos(String estado) {
		StringBuilder sb = new StringBuilder("SELECT o FROM ObjetoiVinedo o WHERE o.estadoSolicitudINV = :estado and "
				+ "o.credito.destino = :cosecha");
		if (idPersona != null) {
			sb.append(" AND o.credito.persona.id =:persona_id");
		}
		if (listarSoloPreabrobados) {
			sb.append(" AND o.credito.linea.id IN (:linea_id)");
		}
		if (listarDistintosAPreaprobados) {
			sb.append(" AND o.credito.linea.id NOT IN (:linea_id)");
		}
		if (estado.equals("SN") || estado.equals("SO")) {
			sb.append(" ORDER BY o.credito.fechaSolicitud");
		} else if (estado.equals("RE")) {
			sb.append(" AND o.fechaInformeINV >= :desde AND o.fechaInformeINV <= :hasta ORDER BY o.fechaInformeINV");
		} else {
			GregorianCalendar gc = new GregorianCalendar();
			int anio = gc.get(GregorianCalendar.YEAR);
			desde = Fecha.sumarDias(new Date(), -45);
			hasta = new Date();
			sb.append(" AND o.fechaInformeINV >= :desde AND o.fechaInformeINV <= :hasta ORDER BY o.fechaInformeINV");
		}
		String estadoAnterior = estado;
		try {
			estado = TipificadorHandler.getDescripcion("estadoSolicitudINV", estado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Query query = bp.createQuery(sb.toString());
		if (estadoAnterior.equals("SN") || estadoAnterior.equals("SO")) {
			query.setParameter("estado", estado).setParameter("cosecha", "CRDC");
		} else {
			query.setParameter("estado", estado).setParameter("desde", desde).setParameter("hasta", hasta)
					.setParameter("cosecha", "CRDC");
		}
		if (listarSoloPreabrobados || listarDistintosAPreaprobados) {
			String lineas[] = DirectorHelper.getString("linea.cosechaPreAprobada").split(",");
			List<Long> lista = new ArrayList<Long>();
			for (int i = 0; i < lineas.length; i++) {
				lista.add(new Long(lineas[i]));
			}
			query.setParameterList("linea_id", lista);
		}
		if (idPersona != null) {
			query.setParameter("persona_id", new Long(idPersona));
		}
		vinedos = query.list();
		// if(estado.equals("SN")){
		for (Iterator iter = vinedos.iterator(); iter.hasNext();) {
			ObjetoiVinedo oVinedo = (ObjetoiVinedo) iter.next();
			List<Garantia> garantias = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
					.setParameter("credito", oVinedo.getCredito()).list();
			Garantia garantia = null;
			if (!garantias.isEmpty()) {
				garantia = garantias.get(0);
			}
			if (garantia != null && garantia.getTipo() != null && garantia.getTipo().getCalculaQQSinVinedo() != null
					&& garantia.getTipo().getCalculaQQSinVinedo()) {
				iter.remove();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void informarVinedo() throws NumberFormatException, HibernateException, Exception {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			vinedo.setFechaInformeINV(new Date());
			vinedo.setQqAprobado((double) qq);
			vinedo.setObservacionINV(observacionINV);
			if (qq < 0) {
				errores.put("solicitud.vinedos.quintales.inv", "solicitud.vinedos.quintales.inv");
			} else {
				try {
					vinedo.setEstadoSolicitudINV(TipificadorHandler.getDescripcion("estadoSolicitudINV", "RE"));
				} catch (Exception e) {
					e.printStackTrace();
				}
				bp.saveOrUpdate(vinedo);
				bp.commit();
				idVinedo = 0L;
				estimados = 0;
				hectareas = 0;
				codigo = "";
				observacion = "";
				observacionINV = "";
				double sumQQAprobados = 0;
				double sumQQEstimados = 0;
				double QQCosto = 0;
				double QQParcial = 0;
				double montoFinal = 0;
				double QQFinal = 0;
				Objetoi credito = vinedo.getCredito();
				List<ObjetoiVinedo> listaVinedos = bp
						.createQuery("SELECT v FROM ObjetoiVinedo v WHERE v.credito = :credito")
						.setParameter("credito", credito).list();
				double qqCredito = 0;
				List<Garantia> garantiasListado = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
						.setParameter("credito", credito).list();
				Garantia gar = null;
				if (!garantiasListado.isEmpty()) {
					gar = garantiasListado.get(0);
				}
				for (ObjetoiVinedo vinedoExistente : listaVinedos) {
					if (vinedoExistente.getQqAprobado() != null) {
						sumQQAprobados += vinedoExistente.getQqAprobado();
					}
					if (vinedoExistente.getQqEstimados() != null) {
						sumQQEstimados += vinedoExistente.getQqEstimados();
					}
				}
				if (credito.getQqsolicitado() != null && credito.getQqsolicitado() != 0) {
					QQParcial = Math.min(sumQQAprobados, Math.min(sumQQEstimados, credito.getQqsolicitado()));
				} else {
					QQParcial = Math.min(sumQQAprobados, sumQQEstimados);
				}
				CosechaConfig cosechaConfig = (CosechaConfig) bp.createQuery(
						"SELECT c FROM CosechaConfig c WHERE c.temporada = (SELECT MAX(co.temporada) FROM CosechaConfig co) "
								+ "AND c.varietales = :varietales")
						.setParameter("varietales", credito.getVarietales()).uniqueResult();
				if (credito.getDestino().equals("CRDC")) {
					QQCosto = QQParcial * cosechaConfig.getCostoQQCosecha();
				} else if (credito.getDestino().equals("CRDA")) {
					QQCosto = QQParcial * cosechaConfig.getCostoQQElab();
				}
				if (credito.getFinanciamientoSol() != null && credito.getFinanciamientoSol() != 0) {
					montoFinal = (Math.min(QQCosto, credito.getFinanciamientoSol()));
				} else {
					montoFinal = QQCosto;
				}
				if (gar != null && gar.getTipo() != null && gar.getTipo().getCalculaQQSinVinedo() != null
						&& gar.getTipo().getCalculaQQSinVinedo().booleanValue() == true
						&& credito.getFinanciamientoSol() != 0) {
					montoFinal = credito.getFinanciamientoSol();
				}
				if (credito.getDestino().equals("CRDC")) {
					QQFinal = montoFinal / cosechaConfig.getCostoQQCosecha();
				} else if (credito.getDestino().equals("CRDA")) {
					QQFinal = montoFinal / cosechaConfig.getCostoQQElab();
				}
				if (credito.getDestino().equals("CRDC")) {
					if (gar != null && gar.getTipo() != null && QQFinal > gar.getTipo().getMaxQQProyecto()) {
						QQFinal = gar.getTipo().getMaxQQProyecto();
					}
					if (cosechaConfig.getMaxQQ() != null && QQFinal > cosechaConfig.getMaxQQ()
							&& (credito.isFecovita() == null || !credito.isFecovita())) {
						QQFinal = cosechaConfig.getMaxQQ();
					}
				}
				credito.setQqFinal(QQFinal);
				if (credito.getDestino().equals("CRDC")) {
					credito.setFinanciamiento(QQFinal * cosechaConfig.getCostoQQCosecha());
				} else if (credito.getDestino().equals("CRDA")) {
					credito.setFinanciamiento(QQFinal * cosechaConfig.getCostoQQElab());
				}
				bp.update(credito);
				// Se generan o actualizan los desembolsos del credito
				List<Desembolso> desembolsos = bp
						.createQuery("Select d from Desembolso d where d.credito=:cred order by d.numero")
						.setParameter("cred", credito).list();
				List<Garantia> garantias = (List<Garantia>) bp
						.createQuery(
								"Select g.garantia from ObjetoiGarantia g where g.objetoi=:cred and g.baja is null")
						.setParameter("cred", credito).list();
				Garantia garantia = new Garantia();
				if (garantias.size() != 0) {
					garantia = garantias.get(0);
				}
				if (desembolsos == null || desembolsos.size() == 0) {
					Desembolso desembolso1 = new Desembolso();
					Desembolso desembolso2 = new Desembolso();
					desembolso1.setFecha(cosechaConfig.getPrimerDesembolso());
					desembolso1.setNumero(1);
					desembolso1.setObservacion("Desembolso generado automaticamente");
					desembolso1.setCredito(credito);
					desembolso1.setEstado("4");
					if (garantia.getId() != null) {
						double importe = credito.getFinanciamiento() * garantia.getTipo().getPorcDesembolso1();
						importe = Math.rint(importe * 100) / 100;
						desembolso1.setImporte(importe);
					}
					if (garantia.getTipo().getPorcDesembolso1() != 1) {
						desembolso2.setFecha(cosechaConfig.getSegundoDesembolso());
						desembolso2.setNumero(2);
						desembolso2.setObservacion("Desembolso generado automaticamente");
						desembolso2.setCredito(credito);
						desembolso2.setEstado("4");
						if (garantia.getId() != null) {
							double importe = credito.getFinanciamiento() * garantia.getTipo().getPorcDesembolso2();
							importe = Math.rint(importe * 100) / 100;
							desembolso2.setImporte(importe);
							importe = Math.rint(importe * 100) / 100;
							desembolso2.setImporte(importe);
						}
						bp.save(desembolso2);
					}
					bp.save(desembolso1);
					bp.getCurrentSession().flush();
				} else if (desembolsos.size() == 1) {
					Desembolso desembolso1 = desembolsos.get(0);
					Desembolso desembolso2 = new Desembolso();
					desembolso1.setFecha(cosechaConfig.getPrimerDesembolso());
					desembolso1.setNumero(1);
					desembolso1.setObservacion("Desembolso generado automaticamente");
					desembolso1.setCredito(credito);
					desembolso1.setEstado("4");
					if (garantia.getId() != null) {
						double importe = credito.getFinanciamiento() * garantia.getTipo().getPorcDesembolso1();
						importe = Math.rint(importe * 100) / 100;
						desembolso1.setImporte(importe);
					}
					if (garantia.getTipo().getPorcDesembolso1() != 1) {
						desembolso2.setFecha(cosechaConfig.getSegundoDesembolso());
						desembolso2.setNumero(2);
						desembolso2.setObservacion("Desembolso generado automaticamente");
						desembolso2.setCredito(credito);
						desembolso2.setEstado("4");
						if (garantia.getId() != null) {
							double importe = credito.getFinanciamiento() * garantia.getTipo().getPorcDesembolso2();
							importe = Math.rint(importe * 100) / 100;
							desembolso2.setImporte(importe);
						}
						bp.save(desembolso2);
					}
					bp.update(desembolso1);
				} else if (desembolsos.size() == 2) {
					Desembolso desembolso1 = desembolsos.get(0);
					Desembolso desembolso2 = desembolsos.get(1);
					desembolso1.setFecha(cosechaConfig.getPrimerDesembolso());
					desembolso1.setNumero(1);
					desembolso1.setObservacion("Desembolso generado automaticamente");
					desembolso1.setCredito(credito);
					desembolso1.setEstado("4");
					if (garantia.getId() != null) {
						double importe = credito.getFinanciamiento() * garantia.getTipo().getPorcDesembolso1();
						importe = Math.rint(importe * 100) / 100;
						desembolso1.setImporte(importe);
					}
					if (garantia.getTipo().getPorcDesembolso1() != 1) {
						desembolso2.setFecha(cosechaConfig.getSegundoDesembolso());
						desembolso2.setNumero(2);
						desembolso2.setObservacion("Desembolso generado automaticamente");
						desembolso2.setCredito(credito);
						desembolso2.setEstado("4");
						if (garantia.getId() != null) {
							double importe = credito.getFinanciamiento() * garantia.getTipo().getPorcDesembolso2();
							importe = Math.rint(importe * 100) / 100;
							desembolso2.setImporte(importe);
						}
						bp.update(desembolso2);
					} else {
						Desembolso existente = (Desembolso) bp.getById(Desembolso.class, desembolso2.getId());
						bp.delete(existente);
					}
					bp.update(desembolso1);
				}
				// Fin creacion y/o actualizacion de Desembolsos para el credito
				// for (ObjetoiVinedo v : listaVinedos) {
				// if (!v.getEstadoSolicitudINV().trim().equals("Realizada")) {
				// bp.saveOrUpdate(credito);
				// return;
				// }
				// }
				// Date hoy = new Date();
				// ObjetoiEstado actual = credito.getEstadoActual();
				// actual.setFechaHasta(hoy);
				// bp.update(actual);
				// PEDIDO POR DAMIAN
				// Estado estadoNuevo = (Estado) bp
				// .createQuery(
				// "SELECT e FROM Estado e WHERE e.nombreEstado = :nombre")
				// .setParameter("nombre", "PENDIENTE RESOLUCION")
				// .uniqueResult();
				// ObjetoiEstado nuevo = new ObjetoiEstado();
				// nuevo.setEstado(estadoNuevo);
				// nuevo.setObjetoi(credito);
				// nuevo.setFecha(hoy);
				// bp.save(nuevo);
				// bp.saveOrUpdate(credito);
			}
		}
	}

	private void enviarSolicitudesINV() {
		listarVinedos("SN");
		if (vinedos != null) {
			bp.begin();
			for (ObjetoiVinedo vinedo : vinedos) {
				// Si no tiene garantias asociadas no puede ejecutar los desembolsos
				if (obtenerGarantia(vinedo.getCredito())) {
					vinedo.setEstadoSolicitudINV(so);
					vinedo.setFechaSolicitado(new Date());
					bp.saveOrUpdate(vinedo);
				}
			}
			bp.commit();
		}
	}

	@SuppressWarnings("unchecked")
	private boolean obtenerGarantia(Objetoi objetoi) {
		List<Garantia> gs = (List<Garantia>) bp
				.createQuery("SELECT g.garantia FROM ObjetoiGarantia g where g.objetoi = :credito AND g.baja IS NULL")
				.setParameter("credito", objetoi).list();
		Garantia g = null;
		if (gs != null && gs.size() > 0) {
			g = gs.get(0);
		}
		if (g != null) {
			return true;
		} else {
			return false;
		}
	}

	public String getObservacionINV() {
		return observacionINV;
	}

	public void setObservacionINV(String observacionINV) {
		this.observacionINV = observacionINV;
	}

	public int getINV() {
		return INV;
	}

	public void setINV(int iNV) {
		INV = iNV;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getEstimados() {
		return estimados;
	}

	public void setEstimados(double estimados) {
		this.estimados = estimados;
	}

	public double getHectareas() {
		return hectareas;
	}

	public void setHectareas(double hectareas) {
		this.hectareas = hectareas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public double getQq() {
		return qq;
	}

	public void setQq(double qq) {
		this.qq = qq;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public String getDesdeStr() {
		if (desde != null) {
			return DateHelper.getString(desde);
		} else {
			return "";
		}
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setDesdeStr(String fecha) {
		if (fecha != null) {
			setDesde(DateHelper.getDate(fecha));
		} else {
			setDesde(null);
		}
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public String getHastaStr() {
		if (hasta != null) {
			return DateHelper.getString(hasta);
		} else {
			return "";
		}
	}

	public void setHastaStr(String fecha) {
		if (fecha != null) {
			setHasta(DateHelper.getDate(fecha));
		} else {
			setHasta(null);
		}
	}

	public boolean isMin() {
		return min;
	}

	public void setMin(boolean min) {
		this.min = min;
	}

	public String getAccionStr() {
		if (accion != null)
			return accion.toString();
		else
			return null;
	}

	public void setAccionStr(String accion) {
		if (accion != null && !accion.equals("")) {
			setAccion(AccionINV.valueOf(accion));
		} else {
			setAccion(AccionINV.LISTAR_VINEDOS_PENDIENTE);
		}
	}

	public void setAccion(AccionINV accion) {
		this.accion = accion;
	}

	public AccionINV getAccion() {
		return accion;
	}

	public Long getIdVinedo() {
		return idVinedo;
	}

	public void setIdVinedo(Long idVinedo) {
		this.idVinedo = idVinedo;
	}

	public Double getQqAprobado() {
		return qqAprobado;
	}

	public void setQqAprobado(Double qqAprobado) {
		this.qqAprobado = qqAprobado;
	}

	public List<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(List<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public String getSo() {
		return so;
	}

	public String getSn() {
		return sn;
	}

	public void setSo(String so) {
		this.so = so;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	// public void informarVinedo() {
	// if (idVinedo != null && idVinedo > 0) {
	// bp.begin();
	// Vinedo vinedo = (Vinedo) bp.getById(Vinedo.class, idVinedo);
	// vinedo.setFechaInformeINV(new Date());
	// vinedo.setQqaprobado((double) qq);
	// vinedo.setObservacionINV(observacion);
	//
	// try {
	// vinedo.setEstadoSolicitudINV(TipificadorHandler.getDescripcion(
	// "estadoSolicitudINV", "RE"));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// bp.saveOrUpdate(vinedo);
	// bp.commit();
	//
	// idVinedo = 0L;
	// estimados = 0;
	// hectareas = 0;
	// codigo = "";
	// observacion = "";
	// }
	// }
	public String getRcf() {
		return rcf;
	}

	public void setRcf(String rcf) {
		this.rcf = rcf;
	}

	public String getRcinv() {
		return rcinv;
	}

	public void setRcinv(String rcinv) {
		this.rcinv = rcinv;
	}

	public boolean isEntra() {
		return entra;
	}

	public void setEntra(boolean entra) {
		this.entra = entra;
	}

	public String getAccionReal() {
		return accionReal;
	}

	public void setAccionReal(String accionReal) {
		this.accionReal = accionReal;
	}

	public String getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(String idPersona) {
		if (idPersona.equals("")) {
			this.idPersona = null;
		} else {
			this.idPersona = idPersona;
		}
	}

	public String getAccionAnterior() {
		return accionAnterior;
	}

	public void setAccionAnterior(String accionAnterior) {
		this.accionAnterior = accionAnterior;
	}
}
