package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.opensymphony.oscache.base.Config;

public class ConfNotificacionProcess implements IProcess {
	private String texto;
	private String denominacion;
	private String asunto;
	private String forward;
	private String action;
	private String eliminar;

	private List<ConfiguracionNotificacion> configuraciones;
	private Long idConfiguracion;
	ConfiguracionNotificacion conf;

	public String getEliminar() {
		return eliminar;
	}

	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	@Override
	public boolean doProcess() {
		if (action == null) {
			setConfiguraciones(listarConfiguraciones());
			forward = "ConfNotificacionesList";
		} else if (action.equalsIgnoreCase("guardar")) {
			guardarConfiguracion();
			setConfiguraciones(listarConfiguraciones());
			forward = "ConfNotificacionesList";
		} else if (action.equalsIgnoreCase("nuevo")) {
			conf = new ConfiguracionNotificacion();
			eliminar = "Guardar";
			forward = "ConfNotificaciones";
		} else if (action.equalsIgnoreCase("modificar")) {
			conf = buscarConfiguracion(idConfiguracion);
			texto = conf.getTexto();
			denominacion = conf.getDenominacion();
			asunto = conf.getAsunto();
			eliminar = "Modificar";

			forward = "ConfNotificaciones";
		} else if (action.equalsIgnoreCase("eliminar")) {
			conf = buscarConfiguracion(idConfiguracion);
			texto = conf.getTexto();
			denominacion = conf.getDenominacion();
			asunto = conf.getAsunto();
			eliminar = "Eliminar";
			forward = "ConfNotificaciones";
		} else if (action.equalsIgnoreCase("cancelar")) {
			setConfiguraciones(listarConfiguraciones());
			forward = "ConfNotificacionesList";
		}

		return true;
	}

	private void guardarConfiguracion() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		if (idConfiguracion != null && !idConfiguracion.equals(0L)) {
			conf = buscarConfiguracion(idConfiguracion);
		} else {
			conf = new ConfiguracionNotificacion();
		}
		if (eliminar != null && eliminar.equals("Eliminar")) {
			conf.setBaja(new Date());
		}
		conf.setAsunto(asunto);
		conf.setDenominacion(denominacion);
		conf.setTexto(texto);
		bp.saveOrUpdate(conf);
	}

	@SuppressWarnings("unchecked")
	private List<ConfiguracionNotificacion> listarConfiguraciones() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<ConfiguracionNotificacion> listado = bp
				.createQuery("SELECT c FROM ConfiguracionNotificacion c WHERE c.baja IS NULL").list();
		return listado;
	}

	private ConfiguracionNotificacion buscarConfiguracion(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		conf = (ConfiguracionNotificacion) bp.createQuery("Select c From ConfiguracionNotificacion c where id = :id")
				.setParameter("id", id).uniqueResult();
		return conf;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setConfiguraciones(List<ConfiguracionNotificacion> configuraciones) {
		this.configuraciones = configuraciones;
	}

	public List<ConfiguracionNotificacion> getConfiguraciones() {
		return configuraciones;
	}

	public Long getIdConfiguracion() {
		return idConfiguracion;
	}

	public void setIdConfiguracion(Long idConfiguracion) {
		this.idConfiguracion = idConfiguracion;
	}

	public ConfiguracionNotificacion getConf() {
		return conf;
	}

	public void setConf(ConfiguracionNotificacion conf) {
		this.conf = conf;
	}

}
