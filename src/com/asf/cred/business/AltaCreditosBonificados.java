package com.asf.cred.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.gaf.hibernate.Bancos;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.struts.action.IReportBean;
import com.asf.struts.form.JReportForm;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.civitas.cred.beans.BeanCreditosBonificados;
import com.nirven.creditos.hibernate.ConvenioBonificacion;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;

public class AltaCreditosBonificados implements IProcess, IReportBean {

	private String forward = "AltaCreditosBonificados";
	private String accion;
	private Long bancoId;
	private Long convenioBonificacionId;
	private Date fechaDesde;
	private Date fechaHasta;
	private List<BeanCreditosBonificados> creditosBonificados;
	private ReportResult result;
	private HashMap<String, String> reportParam = new HashMap<String, String>();
	
	
	@Override
	public boolean doProcess() {
		if("buscar".equals(this.accion)) {
			
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			result = new ReportResult();
			result.setClassBeanName(getClass().getName());
			result.setExport(JReportForm.EXPORT_XLS);
			result.setReportName("altaCreditosBonificados.jasper");
			
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.bancoId", this.bancoId);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.convenioBonificacionId", this.convenioBonificacionId);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.fechaDesde", this.fechaDesde);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.fechaHasta", this.fechaHasta);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String reportParams = ";";
			
			if(this.bancoId != null && this.bancoId > 0) {
				Bancos banco = (Bancos) bp.getById(Bancos.class, this.bancoId);
				reportParams += ";banco=" + banco.getDetaBa();
			}else {
				reportParams += ";banco=Todos";
			}
			
			if(this.convenioBonificacionId != null && this.convenioBonificacionId > 0) {
				ConvenioBonificacion convenioBonificacion = (ConvenioBonificacion) bp.getById(ConvenioBonificacion.class, this.convenioBonificacionId);
				reportParams += ";convenio=" + convenioBonificacion.getNombre();
			}else {
				reportParams += ";convenio=Todos";
			}
			
			if(this.fechaDesde != null) {
				reportParams += ";fechaDesde=" + dateFormat.format(this.fechaDesde);
			}
			
			if(this.fechaHasta != null) {
				reportParams += ";fechaHasta=" + dateFormat.format(this.fechaHasta);
			}	
			
//			
//			result.setParams(					
//					"bancoId=" + this.bancoId
//					+";convenioBonificacionId=" + this.convenioBonificacionId					
//					+";fechaDesde="+dateFormat.format(fechaDesde)					
//					+";fechaHasta="+dateFormat.format(fechaHasta)					
//					+";BEANS=fechaDesde="+DateHelper.getString(fechaDesde)
//					+";fechaHasta="+DateHelper.getString(fechaHasta));
			
			result.setParams(reportParams);
			
			forward = "Reportes";
			
			return true;
		}
		return true;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
	
	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}
	
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}
	
	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}
	
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}
	
	public String getFechaDesdeStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaDesde);
	}
	
	public String getFechaHastaStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaHasta);
	}

	public Long getBancoId() {
		return bancoId;
	}

	public void setBancoId(Long bancoId) {
		this.bancoId = bancoId;
	}

	public Long getConvenioBonificacionId() {
		return convenioBonificacionId;
	}

	public void setConvenioBonificacionId(Long convenioBonificacionId) {
		this.convenioBonificacionId = convenioBonificacionId;
	}		

	public List<BeanCreditosBonificados> getCreditosBonificados() {
		return creditosBonificados;
	}

	public void setCreditosBonificados(List<BeanCreditosBonificados> creditosBonificados) {
		this.creditosBonificados = creditosBonificados;
	}

	private void fetch() {
		
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();		
		creditosBonificados = new ArrayList<BeanCreditosBonificados>();		
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");	
		String whereClause = " WHERE 1=1";
		
		this.bancoId				= (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("AltasCreditosBonificados.bancoId");
		this.convenioBonificacionId	= (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("AltasCreditosBonificados.convenioBonificacionId");
		this.fechaDesde 			= (Date) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("AltasCreditosBonificados.fechaDesde");
		this.fechaHasta 			= (Date) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("AltasCreditosBonificados.fechaHasta");

		
		if(this.bancoId != null && this.bancoId > 0) {
			whereClause += " AND bc.CODI_BA = " + this.bancoId;
		}
		
		if(this.convenioBonificacionId != null && this.convenioBonificacionId > 0) {
			whereClause += " AND cb.id = " + this.convenioBonificacionId;
		}
		
		if(this.fechaDesde != null) {
			whereClause += " AND des.fechaReal >= '" + formatoFecha.format(this.fechaDesde) + "'";
		}
		
		if(this.fechaHasta != null) {
			whereClause += " AND des.fechaReal <= '" + formatoFecha.format(this.fechaHasta) + "'";
		}	
		
		String query = "SELECT bc.CODI_BA as idBanco, cb.id as 'idConvenio', " + 
				" CONVERT(VARCHAR, des.fechaReal, 103) as 'fechaDesembolso'," + 
				" CONVERT(VARCHAR, des.fechaReal, 103) as 'fechaDesembolsoHasta'," + 
				" l.nombre as 'Linea', bc.DETA_BA as 'Ente Bonificador', cb.nombre as Convenio, " + 
				" b.nombre as Bonificacion, tfBon.TF_DESCRIPCION as 'Tipo Bonif'," + 
				" o.numeroAtencion as 'Proyecto', o.numeroCredito as 'Nro Credito', " + 
				" p.NOMB_12 as 'Titular', p.CUIL_12 as CUIT, p.IDPERSONA as 'Cod Titular',  " + 
				" CONVERT(VARCHAR(400), STUFF(" + 
				" (SELECT ', ' + NOMB_12 from PersonaVinculada pv join PERSONA cotom on pv.personaVinculada_IDPERSONA = cotom.IDPERSONA " + 
				" WHERE pv.idObjetoi = o.id AND pv.personaTitular_IDPERSONA = p.IDPERSONA AND pv.relacion_idRelacion = " + 
				" (select valor from Director where CODIGO = 'relacion.cotomador') FOR XML PATH ('')), 1,2,'')) as 'Cotomadores'," + 
				" CONVERT(VARCHAR(400), STUFF(" + 
				" (SELECT ', ' + convert(varchar(20),CUIL_12) from PersonaVinculada pv join PERSONA cotom on pv.personaVinculada_IDPERSONA = cotom.IDPERSONA " + 
				" WHERE pv.idObjetoi = o.id AND pv.personaTitular_IDPERSONA = p.IDPERSONA AND pv.relacion_idRelacion = " + 
				" (select valor from Director where CODIGO = 'relacion.cotomador') FOR XML PATH ('')), 1,2,'')) as 'CUITCotomadores'," + 
				" o.objeto as Destino, o.expediente as 'Nro Expte', o.resolucion as 'Nro Resolucion', " + 
				" CONVERT(VARCHAR, o.fechaResolucion, 103) as 'Fecha Resolucion'," + 
				" REPLACE(CAST(o.financiamiento AS DECIMAL(29,2)) ,'.',',') as 'Monto Credito'," + 
				" des.numero as 'Numero Desembolso', REPLACE(CAST((des.importeReal) AS DECIMAL(29,2)) ,'.',',') as 'Importe Desembolso'," + 
				" REPLACE(CAST(((iv.valor * 100 + oi.valorMas) * oi.valorPor) AS DECIMAL(29,2)) ,'.',',') + ' %' as 'Tasa Int'," + 
				" REPLACE(CAST(" + 
				"	CASE WHEN b.tipoDeBonificacion = 3 " + 
				"		THEN ((iv.valor * 100 + oi.valorMas) * oi.valorPor) - b.tasaBonificada  ELSE b.tasaBonificada END AS DECIMAL(29,2)) ,'.',',') + ' %'  as 'Tasa Bon'," + 
				" CONVERT(integer, o.frecuenciaInteres) * o.plazoCompensatorio as 'Plazo total'," + 
				" CONVERT(integer, o.frecuenciaCapital) * o.plazoCapital as 'Plazo amort capital'," + 
				" CONVERT(VARCHAR, o.primerVencCapital, 103) as 'Primer Venc Cap', CONVERT(VARCHAR, o.primerVencInteres, 103) as 'Primer Venc Int'," + 
				" tfPerCap.TF_DESCRIPCION as 'Period Cap', tfPerCom.TF_DESCRIPCION as 'Period Int'," + 
				" est.nombreEstado as 'Estado de la Solicitud', tfSector.TF_DESCRIPCION as 'Sector Economico'," + 
				" p.ACTTIVIDADAFIP as 'Activ AFIP', af.nombre as 'Desc Activ AFIP'," + 
				" CONVERT(VARCHAR, p.fechaInicioActividad, 103) as 'Fecha Inicio Act'," + 
				" o.cantPersonal as 'Personal Ocupado', o.volumenVtaAnual as 'Promedio de Ventas Anuales'," + 
				" (isnull(D.CALLE_08, D.calleNom) +' '+ isnull(D.numero, '') +' '+ isnull(D.LOCALIDAD, D.departamentoNom)) 'Domicilio Proyecto'," + 
				" isnull(D.LOCALIDAD, D.departamentoNom) as 'Departamento de Localizacion'" + 
				" FROM Objetoi o " + 
				" join ObjetoiBonificacion ob on o.id = ob.idCredito" + 
				" join Bonificacion b on b.id = ob.idBonificacion" + 
				" join TIPIFICADORES tfBon on tfBon.TF_CATEGORIA = 'bonificacion.tipo' and tfBon.TF_CODIGO = b.tipoDeBonificacion" + 
				" join TIPIFICADORES tfPerCap on tfPerCap.TF_CATEGORIA = 'amortizacion.periodicidad' and tfPerCap.TF_CODIGO = o.frecuenciaCapital" + 
				" join TIPIFICADORES tfPerCom on tfPerCom.TF_CATEGORIA = 'amortizacion.periodicidad' and tfPerCom.TF_CODIGO = o.frecuenciaInteres " + 
				" join bancos bc on bc.CODI_BA = b.enteBonificador_CODI_BA" + 
				" join ConvenioBonificacion cb on cb.id = b.convenio_id" + 
				" join Linea l on l.id = o.linea_id" + 
				" join PERSONA p on p.IDPERSONA = o.persona_IDPERSONA " + 
				" left join ActividadAfip af on af.codigo = p.ACTTIVIDADAFIP" + 
				" left join TIPIFICADORES tfSector on tfSector.TF_CATEGORIA = 'persona.sector' and tfSector.TF_CODIGO = p.tiposector " + 
				" join Desembolso des on des.credito_id = o.id" + 
				" join ObjetoiEstado oe on oe.objetoi_id = o.id and oe.fechaHasta is null" + 
				" join Estado est on oe.estado_idEstado = est.idEstado" + 
				" join ObjetoiIndice oi on oi.credito_id = o.id and oi.tipoTasa = '1'" + 
				" join Indice i on i.id = oi.indice_id" + 
				" join IndiceValor iv on iv.indice_id = i.id and iv.fecha = (select max(fecha) from IndiceValor ivalor where ivalor.indice_id = i.id and ivalor.fecha <= o.fechaMutuo)" + 
				" LEFT JOIN (SELECT DO.objetoi_id, DOM.*, PRO.*, c5.DETA_08 AS CALLE_08, LOC.NOMBRE AS LOCALIDAD FROM DomicilioObjetoi DO " + 
				" 	LEFT JOIN DOMICILIO DOM ON DO.domicilio_id = DOM.id" + 
				" 	LEFT JOIN PROVIN PRO ON DOM.provincia_CODI_08 = PRO.CODI_08" + 
				"        LEFT JOIN CALLE c5 ON c5.IDCALLE = DOM.calle_IDCALLE" + 
				"        LEFT JOIN LOCALIDAD LOC ON LOC.IDLOCALIDAD = DOM.localidad_IDLOCALIDAD " + 
				" 	WHERE DOM.tipo='Dom. Proyecto') D" + 
				" 	ON O.id = D.objetoi_id " + 
				whereClause + 
				"	AND des.importeReal IS NOT NULL AND des.fechaReal IS NOT NULL" + 
				"	order by des.fechaReal";
		
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.bancoId", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.convenioBonificacionId", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.fechaDesde", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AltasCreditosBonificados.fechaHasta", null);
		
		List<Object[]> resultSet = bp.createSQLQuery(query).list();
		for(Object[] row: resultSet) {
			BeanCreditosBonificados bean = new BeanCreditosBonificados();
			bean.setIdBanco									(((BigDecimal) row[0]).longValue() );
			bean.setIdConvenio								(((BigDecimal) row[1]).longValue() );
			bean.setFechaDesembolso							(row[2]);
			bean.setFechaDesembolsoHasta					(DateHelper.getDate( (String) row[3]));
			bean.setLinea									((String) row[4]);
			bean.setEnteBonificador							((String) row[5]);
			bean.setConvenio								((String) row[6]);
			bean.setBonificacion							((String) row[7]);
			bean.setTipoBonificacion						((String) row[8]);
			bean.setProyectoId								(((BigDecimal) row[9]).longValue() );
			bean.setNroCredito								((String) row[10]);
			bean.setTitular									((String) row[11]);
			bean.setCuit									(((BigDecimal) row[12]).toString() );
			bean.setCodTitular								(((BigDecimal) row[13]).longValue() );
			bean.setCotomadores								((String) row[14]);
			bean.setCuitCotomadores							((String) row[15]);
			bean.setDestino									((String) row[16]);
			bean.setNroExpediente							((String) row[17]);
			bean.setNroResolucion							((String) row[18]);
			bean.setFechaResolucion							(row[19]);
			String montoCredito = ( (String) row[20] ).replaceAll("\\.", "").replaceAll(",", ".");
			bean.setMontoCredito							(new BigDecimal(montoCredito).doubleValue());	
			bean.setNumeroDesembolso						(((Integer) row[21]).longValue());
			String montoDesembolso = (row[22] == null) ? "0" : ( (String) row[22] ).replaceAll("\\.", "").replaceAll(",", ".");
			bean.setMontoDesembolso							(new BigDecimal(montoDesembolso).doubleValue());
			bean.setTasaInteres								((String) row[23]);
			bean.setTasaBonificacion						((String) row[24]);
			bean.setPlazoTotal								(((Integer) row[25]).longValue() );
			bean.setPlazoAmortizacionCapital				(((Integer) row[26]).longValue() );
			bean.setPrimerVencimientoCapital				(row[27]);
			bean.setPrimerVencimientoInteres				(row[28]);
			bean.setPeriodoCapital							((String) row[29]);
			bean.setPeriodoInteres							((String) row[30]);
			bean.setEstadoSolicitud							((String) row[31]);
			bean.setSectorEconomico							((String) row[32]);
			bean.setActividadAfipId							((String) row[33]);
			bean.setActividadAfipDescripcion				((String) row[34]);
			bean.setFechaInicioActividad					(row[35]);
			bean.setPersonalOcupado							((row[36] == null || "".equals(row[36])) ? 0 :((Integer) row[36]).longValue() );
			bean.setPromedioVentasAnuales					(row[37]);
			bean.setDomicilioProyecto						((String) row[38]);
			bean.setDepartamentoProyecto					((String) row[39]);
			
			this.creditosBonificados.add(bean);
		}
		
//		if(this.creditosBonificados.size() == 0) {
//			BeanCreditosBonificados bean = new BeanCreditosBonificados();
//			this.creditosBonificados.add(bean);
//		}
		
	}
	
	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public Collection getCollection(HashMap hsParametros) {
		this.fetch();
		return this.creditosBonificados;
	}
}
