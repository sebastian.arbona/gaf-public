package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

import com.asf.cred.business.InformeDeudaItemTask.InformeDeudaItemListener;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.DetalleInformeDeuda;
import com.nirven.creditos.hibernate.EstadoInformeDeuda;
import com.nirven.creditos.hibernate.InformeDeuda;

public class InformeDeudaTask implements Runnable, InformeDeudaItemListener {

	private Long idInformeDeuda;
	private InformeDeuda informe;
	private String user;
	private String pass;
	private ExecutorService service;
	private BusinessPersistance bp;

	public InformeDeudaTask(Long idInformeDeuda, String user, String pass, ExecutorService service) {
		this.idInformeDeuda = idInformeDeuda;
		this.user = user;
		this.pass = pass;
		this.service = service;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			if (sh == null) {
				sh = new com.asf.cred.security.SessionHandler();
				sh.register(null);
				if (pass != null) {
					if (!sh.login(null, user, pass)) { // Verifico si tiene permiso para loguearse en el sistema
						System.err.println(getClass() + ": Usuario o clave inv�lido");
						return;
					}
				}
			}
			bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			informe = (InformeDeuda) bp.getById(InformeDeuda.class, idInformeDeuda);
			String sql = "SELECT O.id FROM Objetoi O INNER JOIN Ctacte C ON C.objetoi_id = O.id "
					+ "INNER JOIN Cuota CT ON CT.id = C.cuota_id "
					+ "INNER JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id AND fechaHasta IS NULL "
					+ "WHERE OE.estado_idEstado IN(6,7,8) AND CT.fechaVencimiento <= :fecha ";
			if (informe.getLineasId() != null && !informe.getLineasId().isEmpty()) {
				sql += " AND O.linea_id IN (" + informe.getLineasId() + ") ";
			}
			sql += "GROUP BY O.id HAVING SUM(CASE WHEN C.tipomov_id = 2 THEN C.importe ELSE -C.importe END) >= 0.01";
			List<BigDecimal> ids = bp.createSQLQuery(sql).setDate("fecha", informe.getFechaHasta()).list();
			informe.setEstado(EstadoInformeDeuda.EN_PROCESO);
			informe.setCreditos(ids.size());
			informe.setFechaEjecucion(new Date());
			bp.update(informe);
			bp.getCurrentSession().flush();
			List<InformeDeudaItemTask> tasks = new ArrayList<InformeDeudaItemTask>();
			for (BigDecimal id : ids) {
				InformeDeudaItemTask itemTask = new InformeDeudaItemTask(id.longValue(), informe.getFechaHasta(), user,
						pass);
				itemTask.setListener(this);
				tasks.add(itemTask);
			}
			try {
				service.invokeAll(tasks);
				informe.setEstado(EstadoInformeDeuda.TERMINADO);
			} catch (InterruptedException e) {
				e.printStackTrace();
				informe.setEstado(EstadoInformeDeuda.ERROR);
			}
			bp.update(informe);
			bp.getCurrentSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void detalleNuevo(DetalleInformeDeuda detalle) {
		if (detalle != null) {
			detalle.setInforme(informe);
			bp.update(detalle);
		}
		informe.setProcesados(informe.getProcesados() + 1);
		bp.update(informe);
		bp.getCurrentSession().flush();
	}

}
