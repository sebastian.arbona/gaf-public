package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.hibernate.Bancos;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaConf;
import com.nirven.creditos.hibernate.BonTasaEntidadConf;
import com.nirven.creditos.hibernate.BonTasaEstado;
import com.nirven.creditos.hibernate.CargaRequisitoBon;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Requisito;

public class BonTasaProcess implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	protected Long idPersona;
	private Persona persona;
	private List<BonTasa> bonificaciones;
	protected BonTasa bonificacion;
	private Long idBonTasa;
	private Date fechaHoy = new Date();
	private Linea linea;
	private BonTasaEstado estadoActual;
	private boolean ente = false;
	private boolean requisitosCompletos = false;
	private double maximo;
	private double maximoEnte;
	private double bonMaxPersona;
	private List<BonTasaEntidadConf> configuraciones;
	private Long idEnte;
	private Long idConfig;
	private boolean esPersona;
	private boolean conf;
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	private boolean error;
	private BonTasa bonTasa;
	private String filtro;

	public BonTasaProcess() {
		bonTasa = new BonTasa();
		Persona persona = new Persona();
		bonTasa.setPersona(persona);
		filtro = "ninguno";
	}

	public boolean doProcess() {
		if (accion == null) {
			bonificaciones = bp.createQuery("Select bt from BonTasa bt where bt.persona= :persona")
					.setParameter("persona", bp.getById(Persona.class, idPersona)).list();
			esPersona = true;
			forward = "BonTasaList";
		} else if (accion.equalsIgnoreCase("ver")) {
			bonificaciones = bp.createQuery("Select bt from BonTasa bt where bt.persona= :persona")
					.setParameter("persona", bp.getById(Persona.class, idPersona)).list();
			esPersona = true;
			buscarBonTasa();
			buscarLinea();
			comprobarRequisitos();
			getEstadoActual();
			forward = "VerBonTasa";
		} else if (accion.equalsIgnoreCase("pagos")) {
			buscarBonTasa();
			if (bonificacion.getBanco() != null) {
				ente = true;
			}
			forward = "pagosBonTasa";
		} else if (accion.equalsIgnoreCase("config")) {// Pantalla de las configuraciones de las BONTASA
			buscarConfig();
			forward = "BonTasaConf";
		} else if (accion.equalsIgnoreCase("configAgregar")) {
			agregarConfig();
			buscarConfig();
			forward = "BonTasaConf";
		} else if (accion.equalsIgnoreCase("configGuardar")) {
			guardarConfig();
			buscarConfig();
			forward = "BonTasaConf";
		} else if (accion.equalsIgnoreCase("eliminarConfig")) {
			eliminarConfiguracion();
			buscarConfig();
			forward = "BonTasaConf";
		} else if (accion.equalsIgnoreCase("general")) {// Cuando se llama a la pantalla desde Creditos-->BonTasa
			bonificaciones = bp.createQuery("Select b from BonTasa b").list();
			forward = "BonTasaGeneral";
		} else if (accion.equalsIgnoreCase("verGeneral")) {
//			bonificaciones = bp.createQuery("Select bt from BonTasa bt").list();
			esPersona = false;
			buscarBonTasa();
			buscarLinea();
			comprobarRequisitos();
			getEstadoActual();
			forward = "VerBonTasaGeneral";
		} else if (accion.equalsIgnoreCase("mostrarMod")) {
			conf = true;
			BonTasaEntidadConf configuracion = (BonTasaEntidadConf) bp.getById(BonTasaEntidadConf.class, idConfig);
			idEnte = configuracion.getBanco().getCodiBa();
			maximoEnte = configuracion.getMaxBonEnte();
			bonMaxPersona = configuracion.getBonMaxPersona();
			forward = "BonTasaConf";
		} else if (accion.equalsIgnoreCase("configModificar")) {
			BonTasaEntidadConf con = (BonTasaEntidadConf) bp.getById(BonTasaEntidadConf.class, idConfig);
			con.setMaxBonEnte(maximoEnte);
			con.setBonMaxPersona(bonMaxPersona);
			bp.update(con);
			buscarConfig();
			forward = "BonTasaConf";
		} else if (accion.equals("filtrar")) {
			bonificaciones = filtrar();
			forward = "BonTasaGeneral";
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private List<BonTasa> filtrar() {
		if (bonTasa.getNumeroBonificacion() != null) {
			bonificaciones = bp.getNamedQuery("BonTasa.findByNumeroBonificacion")
					.setLong("numeroBonificacion", bonTasa.getNumeroBonificacion()).list();
		} else if (bonTasa.getExpediente() != null && !bonTasa.getExpediente().isEmpty()) {
			bonificaciones = bp.getNamedQuery("BonTasa.likeByExpediente")
					.setString("expediente", bonTasa.getExpediente() + "%").list();
		} else if (bonTasa.getPersona().getId() != null && bonTasa.getPersona().getId() != 0L) {
			bonificaciones = bp.getNamedQuery("BonTasa.findByPersona").setEntity("persona", bonTasa.getPersona())
					.list();
		}
		return bonificaciones;
	}

	private void eliminarConfiguracion() {
		BonTasaEntidadConf conf = (BonTasaEntidadConf) bp.getById(BonTasaEntidadConf.class, idConfig);
		bp.delete(conf);
	}

	private void guardarConfig() {
		BonTasaConf conf = (BonTasaConf) bp.createQuery("Select c from BonTasaConf c where c.id=:id")
				.setParameter("id", Long.valueOf(1)).uniqueResult();
		if (conf != null) {
			conf.setMaxBonPersona(maximo);
			bp.saveOrUpdate(conf);
		} else {
			BonTasaConf conf2 = new BonTasaConf();
			conf2.setMaxBonPersona(maximo);
			bp.save(conf2);
		}
	}

	private void agregarConfig() {
		if (comprobarEnte()) {
			BonTasaEntidadConf configuracion = new BonTasaEntidadConf();
			configuracion.setBanco((Bancos) bp.getById(Bancos.class, idEnte));
			configuracion.setMaxBonEnte(maximoEnte);
			configuracion.setBonMaxPersona(bonMaxPersona);
			bp.save(configuracion);
		} else {
			error = true;
		}
	}

	private boolean comprobarEnte() {
		Bancos ente = (Bancos) bp.getById(Bancos.class, idEnte);
		buscarConfig();
		for (BonTasaEntidadConf conf : configuraciones) {
			if (conf.getBanco().getCodiBa() == ente.getCodiBa()) {
				return false;
			}
		}
		if (ente.getFinanciador() == null || !ente.getFinanciador()) {
			return false;
		}
		return true;
	}

	private void buscarConfig() {
		configuraciones = bp.createQuery("Select c from BonTasaEntidadConf c ").list();
	}

	private void comprobarRequisitos() {
		List<CargaRequisitoBon> requisitos = listarRequisitos();
		int cumplidos = 0;
		for (CargaRequisitoBon requi : requisitos) {// Compruebo si todos los requisitos estan cumplidos
			if (requi.getFechaCumplimiento() != null) {
				cumplidos += 1;
			}
		}
		if (requisitos.size() == cumplidos) {
			requisitosCompletos = true;
		}
	}

	private List<CargaRequisitoBon> listarRequisitos() {
		// Busco los requisitos de la linea "Bonificaciones de Tasa"
		// Director director = (Director) bp.createQuery("Select d from Director d where
		// d.codigo=:cod").setParameter("cod", "BonTasa.IDLinea").uniqueResult();
		// Linea l= (Linea) bp.getById(Linea.class,
		// bonificacion.getConvenio().getLinea().getId());
		List<Requisito> req = (List<Requisito>) bp.createQuery("Select r from Requisito r where r.linea= :linea")
				.setLong("linea", linea.getId()).list();

		// Busco los requisitos que ya estan cumplidos en CargaRequisitoBon
		List<CargaRequisitoBon> requisitos = (List<CargaRequisitoBon>) bp
				.createQuery("Select c from CargaRequisitoBon c where c.bonTasa =:bonTasa")
				.setParameter("bonTasa", bonificacion).list();

		// comparo los requisitos para armar la lista final
		for (Requisito requisito : req) {
			int i = 0;
			for (CargaRequisitoBon cargaReq : requisitos) {
				if (requisito.equals(cargaReq.getRequisito())) {
					i = 1;
				}
			}
			if (i == 0) {
				CargaRequisitoBon beanCarga = new CargaRequisitoBon();
				beanCarga.setBonTasa(bonificacion);
				beanCarga.setRequisito(requisito);
				if (beanCarga.getId() != null) {
					beanCarga.setId(null);
				}
				requisitos.add(beanCarga);
			}
		}
		return requisitos;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	private void buscarLinea() {
		// linea = (Linea) bp.createQuery("Select l from Linea l where
		// l.nombre=:nombre").setParameter("nombre", "Bonificacion De Tasa
		// Bancaria").uniqueResult();
		linea = (Linea) bp.getById(Linea.class, bonificacion.getConvenio().getLinea().getId());
	}

	protected void buscarBonTasa() {
		bonificacion = (BonTasa) bp.createQuery("select b from BonTasa b where id=:id").setLong("id", idBonTasa)
				.uniqueResult();
	}

	private void guardarBonTasa() {

	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<BonTasa> getBonificaciones() {
		return bonificaciones;
	}

	public void setBonificaciones(List<BonTasa> bonificaciones) {
		this.bonificaciones = bonificaciones;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public Date getFechaHoy() {
		return fechaHoy;
	}

	public void setFechaHoy(Date fechaHoy) {
		this.fechaHoy = fechaHoy;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Linea getLinea() {
		return linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	public BonTasaEstado getEstadoActual() {
		if (estadoActual == null) {
			estadoActual = bonificacion.getEstadoActual();
		}
		return estadoActual;
	}

	public void setEstadoActual(BonTasaEstado estadoActual) {
		this.estadoActual = estadoActual;
	}

	public boolean isRequisitosCompletos() {
		return requisitosCompletos;
	}

	public void setRequisitosCompletos(boolean requisitosCompletos) {
		this.requisitosCompletos = requisitosCompletos;
	}

	public boolean isEnte() {
		return ente;
	}

	public void setEnte(boolean ente) {
		this.ente = ente;
	}

	public double getMaximo() {
		return maximo;
	}

	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}

	public double getMaximoEnte() {
		return maximoEnte;
	}

	public void setMaximoEnte(double maximoEnte) {
		this.maximoEnte = maximoEnte;
	}

	public List<BonTasaEntidadConf> getConfiguraciones() {
		return configuraciones;
	}

	public void setConfiguraciones(List<BonTasaEntidadConf> configuraciones) {
		this.configuraciones = configuraciones;
	}

	public Long getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(Long idEnte) {
		this.idEnte = idEnte;
	}

	public Long getIdConfig() {
		return idConfig;
	}

	public void setIdConfig(Long idConfig) {
		this.idConfig = idConfig;
	}

	public boolean isEsPersona() {
		return esPersona;
	}

	public void setEsPersona(boolean esPersona) {
		this.esPersona = esPersona;
	}

	public boolean isConf() {
		return conf;
	}

	public void setConf(boolean conf) {
		this.conf = conf;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public BonTasa getBonTasa() {
		return bonTasa;
	}

	public void setBonTasa(BonTasa bonTasa) {
		this.bonTasa = bonTasa;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	public double getBonMaxPersona() {
		return bonMaxPersona;
	}

	public void setBonMaxPersona(double bonMaxPersona) {
		this.bonMaxPersona = bonMaxPersona;
	}

}
