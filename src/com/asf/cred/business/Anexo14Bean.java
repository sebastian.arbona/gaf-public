package com.asf.cred.business;

import java.io.Serializable;

public class Anexo14Bean implements Serializable {

	private static final long serialVersionUID = -7411335442900642721L;
	
	private String periodo;
	private String concepto;
	private double saldoInicial;
	private double debitos;
	private double creditos;
	private double creditosPagos;
	
	public Anexo14Bean(Object[] r, String periodo) {
		this.periodo = periodo;
		this.concepto = (String) r[0];
		this.saldoInicial = r[1] != null ? ((Number) r[1]).doubleValue() : 0;
		this.debitos = r[2] != null ? ((Number) r[2]).doubleValue() : 0;
		this.creditosPagos = r[3] != null ? ((Number) r[3]).doubleValue() : 0;
		this.creditos = r[4] != null ? ((Number) r[4]).doubleValue() : 0;
	}
	
	public double getSaldoFinal() {
		return saldoInicial + debitos - creditos - creditosPagos;
	}
	
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public double getSaldoInicial() {
		return saldoInicial;
	}
	public void setSaldoInicial(double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}
	public double getDebitos() {
		return debitos;
	}
	public void setDebitos(double debitos) {
		this.debitos = debitos;
	}
	public double getCreditos() {
		return creditos;
	}
	public void setCreditos(double creditos) {
		this.creditos = creditos;
	}
	public double getCreditosPagos() {
		return creditosPagos;
	}
	public void setCreditosPagos(double creditosPagos) {
		this.creditosPagos = creditosPagos;
	}
}
