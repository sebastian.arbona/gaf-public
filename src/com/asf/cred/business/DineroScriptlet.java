package com.asf.cred.business;

import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.NumberHelper;
import net.sf.jasperreports.engine.JRAbstractScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * 
 * 
 * 
 * Funciones relacionadas con el dinero:
 * 
 * (1) - Devuelve un importe en texto.-
 */

public class DineroScriptlet extends JRAbstractScriptlet

{

	// Constructor por defecto.-

	public DineroScriptlet() {
	}// fin DineroScriptlet.-

	/**
	 * 
	 * Recibe un Double, y retorna el valor como texto.-
	 * 
	 * @param total
	 *            , dinero que se quiere representar en texto.-
	 * 
	 * @return String con el dinero expresado en texto.-
	 * 
	 * @throws JRScriptletException
	 */

	public String getDinero(Double total) throws JRScriptletException

	{

		return NumberHelper.getAsDinero(total.doubleValue());

	}// fin getDinero.-

	public String getNombreEncargado(String codigoDirector)
			throws JRScriptletException

	{

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();

		Director director;

		String nombreEncargado = "";

		String consulta = "";

		if (codigoDirector != null && !"".equals(codigoDirector.trim()))

		{

			consulta += "FROM Director d WHERE";

			consulta += " d.codigo = '" + codigoDirector + "'";

			consulta += " ORDER BY d.valor";

			try

			{

				director = (Director) bp.getByFilter(consulta).get(0);

				if (director.getNombre() != null
						&& !"".equals(director.getNombre().trim()))

				{

					nombreEncargado += director.getNombre();

					if (director.getValor() != null
							&& !"".equals(director.getValor().trim()))

					{

						nombreEncargado += "\n" + director.getValor();

					}

				}

			}

			catch (Exception e)

			{

				nombreEncargado = "";

			}

		}

		return nombreEncargado;

	}// fin getDinero.-

	// m�todos no implementados.-

	public void beforeColumnInit() throws JRScriptletException {
	}

	public void beforeReportInit() throws JRScriptletException {
	}

	public void afterReportInit() throws JRScriptletException {
	}

	public void beforePageInit() throws JRScriptletException {
	}

	public void afterPageInit() throws JRScriptletException {
	}

	public void afterColumnInit() throws JRScriptletException {
	}

	public void beforeGroupInit(String string) throws JRScriptletException {
	}

	public void afterGroupInit(String string) throws JRScriptletException {
	}

	public void beforeDetailEval() throws JRScriptletException {
	}

	public void afterDetailEval() throws JRScriptletException {
	}

}// fin DineroScriptlet.-
