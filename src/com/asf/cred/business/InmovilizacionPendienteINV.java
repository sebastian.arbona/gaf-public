package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.EstadoInmovilizacion;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Inmovilizaciones;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;

public class InmovilizacionPendienteINV implements IProcess {

	protected ObjetoiVinedo vinedo = new ObjetoiVinedo();
	protected List<ObjetoiVinedo> vinedos;
	
	protected List<Objetoi> creditos;
	protected Objetoi credito;
	private List<Objetoi> seleccionado = new ArrayList<Objetoi>();	
	private String accion;
	private String forward;
	private boolean boton = false;
	private Long id;
	private Garantia garantia = new Garantia();
	
	private boolean verDetalle;
	

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		
		if (accion != null && accion.equalsIgnoreCase("enviar")) {
			cambiarFecha();
			boton = false;
			verDetalle = false;
		}
		
		creditos = bp.createQuery("select distinct i.objetoi from Inmovilizaciones i where i.estado = :solicitada")
				.setParameter("solicitada", EstadoInmovilizacion.SOLICITADA)
				.list();
		
		/*creditos = bp.createQuery("select distinct c1 " +
									"from ObjetoiVinedo v, Desembolso d " +
									"join v.credito c1 " +
									"join d.credito c2 " +
									"where c1 = c2 " +
									"and v.fechaSolicitudInforme2 is not NULL " +
									"and v.fechaAceptadoInforme2 is NULL " +
									"and d.numero = 1 " +
									"and d.estado = '2'")
									.list();*/
		
		Iterator<Objetoi> creditosIterator = creditos.iterator();
		Objetoi objetoi;
		while(creditosIterator.hasNext()){
			objetoi = creditosIterator.next();
			if(!objetoi.getTieneGarantiaFiduciaria()){
				creditosIterator.remove();
			}
		}
		
		if (accion == null) {
			boton = false;
			verDetalle = false;
		} else if (accion.equalsIgnoreCase("ver")) {
			credito = (Objetoi) bp.getById(Objetoi.class, id);
			seleccionado.add(credito);
			boton = true;
			verDetalle = true;
			setGarantia(buscarGarantia());
		}

		forward = "InmovilizacionPendienteINVList";
		return true;
	}

	public boolean isBoton() {
		return boton;
	}

	public void setBoton(boolean boton) {
		this.boton = boton;
	}
	
	public Garantia buscarGarantia() {
		List<Garantia> garantias = new ArrayList<Garantia>();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		garantias = bp.createQuery(
						"SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja is NULL")
						.setEntity("credito", credito).list();
		if (garantias.isEmpty()) {
			garantia = new Garantia();
		} else {
			garantia= garantias.get(0);
		}
		return garantia;
	}
	
	public Garantia setGarantia(Garantia garantia) {
		this.garantia = garantia;
		return garantia;
	}
	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public ObjetoiVinedo getVinedo() {
		return vinedo;
	}

	public void setVinedo(ObjetoiVinedo vinedo) {
		this.vinedo = vinedo;
	}

	public List<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(List<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	@SuppressWarnings("unchecked")
	public void cambiarFecha() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		credito = (Objetoi) bp.getById(Objetoi.class, id);
		
		vinedos = (List<ObjetoiVinedo>)bp.createQuery("Select c from ObjetoiVinedo c where c.credito=:credito").setParameter("credito", credito).list();
		boolean realizada = false;
		Date fechaEnvio = new Date();
		bp.begin();
		for (ObjetoiVinedo ov : vinedos) {
			ov.setFechaAceptadoInforme2(fechaEnvio);
			bp.update(ov);
			realizada = true;
		}
		if(realizada){
			List<Inmovilizaciones> inms = bp.createQuery("select i from Inmovilizaciones i where i.objetoi.id = :idCredito and i.estado = :solicitada")
				.setParameter("idCredito", id)
				.setParameter("solicitada", EstadoInmovilizacion.SOLICITADA)
				.list();
			
			for (Inmovilizaciones inmovilizacion : inms) {
				inmovilizacion.setFechaAceptacion(fechaEnvio);
				inmovilizacion.setEstado(EstadoInmovilizacion.REALIZADA);
				bp.update(inmovilizacion);
			}
		}
		bp.commit();
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}
	
	public String getIdStr(){
		return (id != null ? String.valueOf(id) : "");
	}
	
	public void setIdStr(String id){
		this.id = (id == null ? 0 : Long.getLong(id));
	}

	public Garantia getGarantia() {
		return garantia;
	}

	public boolean isVerDetalle() {
		return verDetalle;
	}

	public void setVerDetalle(boolean verDetalle) {
		this.verDetalle = verDetalle;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}
	
	public List<Objetoi> getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(List<Objetoi> seleccionado) {
		this.seleccionado = seleccionado;
	}

}
