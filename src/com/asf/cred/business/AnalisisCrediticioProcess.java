package com.asf.cred.business;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.HTTPHelper;
import com.asf.util.ReportResult;
import com.civitas.logger.LoggerService;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.QQIngresados;

public class AnalisisCrediticioProcess implements IProcess {
	private String fechaInhibicion;
	private String fechaVerificacion;
	private String fechaAsesoriaLetrada;
	private String fechaAnalisisPatrimonial;

	private String observacionInhibicion;
	private String observacionVerificacion;
	private String observacionAsesoriaLetrada;
	private String observacionAnalisisPatrimonial;

	private String forward = "AnalisisCrediticio";
	private String action;
	private Objetoi credito = new Objetoi(); // Ticket 9165. Inicializo el credito
	private String fecovita; // Ticket 9165.

	private String id;
	private boolean ocultarCampos;
	private boolean esperandoRequisitos;
	private HashMap<String, Object> errores;

	private ReportResult result;

	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	public AnalisisCrediticioProcess() {
		errores = new HashMap<String, Object>();
	}

	@Override
	public boolean doProcess() {
		if (id != null) {
			credito = (Objetoi) bp.getById(Objetoi.class, new Long(id));
		}
		if (action == null && credito != null) {
			if (credito.getFechaInhibicion() != null) {
				fechaInhibicion = credito.getFechaInhibicion().toString();
			}
			if (credito.getFechaVerificacion() != null) {
				fechaVerificacion = credito.getFechaVerificacion().toString();
			}
			if (credito.getFechaAsesoriaLetrada() != null) {
				fechaAsesoriaLetrada = credito.getFechaAsesoriaLetrada().toString();
			}
			if (credito.getFechaAnalisisPatrimonial() != null) {
				fechaAnalisisPatrimonial = credito.getFechaAnalisisPatrimonial().toString();
			}
			observacionVerificacion = credito.getObservacionVerificacion();
			observacionInhibicion = credito.getObservacionInhibicion();
			observacionAsesoriaLetrada = credito.getObservacionAsesoriaLetrada();
			observacionAnalisisPatrimonial = credito.getObservacionAnalisisPatrimonial();
			esperandoRequisitos = credito.isEsperandoRequisitos();
			fecovita = credito.getFecovitaStr();
		} else if (action.equalsIgnoreCase("guardar")) {
			if (credito != null) {
				credito.setFechaInhibicion(DateHelper.getDate(fechaInhibicion));
				credito.setFechaVerificacion(DateHelper.getDate(fechaVerificacion));
				credito.setFechaAsesoriaLetrada(DateHelper.getDate(fechaAsesoriaLetrada));
				credito.setFechaAnalisisPatrimonial(DateHelper.getDate(fechaAnalisisPatrimonial));
				credito.setObservacionVerificacion(observacionVerificacion);
				credito.setObservacionInhibicion(observacionInhibicion);
				credito.setObservacionAnalisisPatrimonial(observacionAnalisisPatrimonial);
				credito.setObservacionAsesoriaLetrada(observacionAsesoriaLetrada);
				credito.setEsperandoRequisitos(esperandoRequisitos);
				credito.setFecovitaStr(fecovita); // Ticket 9165
				bp.update(credito);
				bp.commit();
			}
		} else if (action.equalsIgnoreCase("actualizarInv")) {
			consultaINV();
		} else if (action.equals("reporte")) {
			result = new ReportResult();
			result.setExport("PDF");
			result.setHref(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ "/actions/process.do?do=process&processName=AnalisisCrediticioProcess&process.id=" + this.id);
			result.setReportName("reporteQQ.jasper");
			result.setParams("SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";idCredito=" + id);
			forward = "ReportesProcess2";
			return errores.isEmpty();
		}

		forward = "AnalisisCrediticio";

		return errores.isEmpty();
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {

		return forward;
	}

	@Override
	public String getInput() {
		return "AnalisisCrediticio";
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getObservacionInhibicion() {
		return observacionInhibicion;
	}

	public void setObservacionInhibicion(String observacionInhibicion) {
		this.observacionInhibicion = observacionInhibicion;
	}

	public String getObservacionVerificacion() {
		return observacionVerificacion;
	}

	public void setObservacionVerificacion(String observacionVerificacion) {
		this.observacionVerificacion = observacionVerificacion;
	}

	public String getObservacionAsesoriaLetrada() {
		return observacionAsesoriaLetrada;
	}

	public void setObservacionAsesoriaLetrada(String observacionAsesoriaLetrada) {
		this.observacionAsesoriaLetrada = observacionAsesoriaLetrada;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFechaInhibicion() {
		return fechaInhibicion;
	}

	public void setFechaInhibicion(String fechaInhibicion) {
		this.fechaInhibicion = fechaInhibicion;
	}

	public String getFechaVerificacion() {
		return fechaVerificacion;
	}

	public void setFechaVerificacion(String fechaVerificacion) {
		this.fechaVerificacion = fechaVerificacion;
	}

	public String getFechaAsesoriaLetrada() {
		return fechaAsesoriaLetrada;
	}

	public void setFechaAsesoriaLetrada(String fechaAsesoriaLetrada) {
		this.fechaAsesoriaLetrada = fechaAsesoriaLetrada;
	}

	public String getFechaAnalisisPatrimonial() {
		return fechaAnalisisPatrimonial;
	}

	public void setFechaAnalisisPatrimonial(String fechaAnalisisPatrimonial) {
		this.fechaAnalisisPatrimonial = fechaAnalisisPatrimonial;
	}

	public String getObservacionAnalisisPatrimonial() {
		return observacionAnalisisPatrimonial;
	}

	public void setObservacionAnalisisPatrimonial(String observacionAnalisisPatrimonial) {
		this.observacionAnalisisPatrimonial = observacionAnalisisPatrimonial;
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	public boolean isEsperandoRequisitos() {
		return esperandoRequisitos;
	}

	public void setEsperandoRequisitos(boolean esperandoRequisitos) {
		this.esperandoRequisitos = esperandoRequisitos;
	}

	public String getVolumenInmovilizado() {
		Object volumen = bp
				.createSQLQuery("SELECT i.volumen " + "FROM Inmovilizaciones i " + "WHERE i.objetoi_id = :objetoi_id "
						+ "AND i.fecha = (SELECT MAX(inmo.fecha) " + "FROM Inmovilizaciones inmo "
						+ "WHERE inmo.objetoi_id = :objetoi_id)")
				.setLong("objetoi_id", getCredito().getId()).setMaxResults(1).uniqueResult();
		if (volumen == null)
			return null;
		else
			return String.format("%.2f", (Double) volumen);
	}

	public String getFechaInmovilizacion() {
		Timestamp fecha = (Timestamp) bp
				.createSQLQuery(
						"SELECT MAX(i.fecha) " + "FROM Inmovilizaciones i " + "WHERE i.objetoi_id = :objetoi_id")
				.setLong("objetoi_id", getCredito().getId()).uniqueResult();
		if (fecha == null)
			return null;
		else
			return DateHelper.getString(new Date(fecha.getTime()));
	}

	public String getVolumenLiberado() {
		Object sumVolumen = bp.getNamedQuery("Liberaciones.sumVolumen").setEntity("objetoi", getCredito())
				.uniqueResult();
		if (sumVolumen == null)
			return null;
		else
			return String.format("%.2f", (Double) sumVolumen);
	}

	public String getFechaUltimaLiberacion() {
		Timestamp fecha = (Timestamp) bp
				.createSQLQuery("SELECT MAX(l.fecha) " + "FROM Liberaciones l " + "WHERE l.objetoi_id = :objetoi_id")
				.setLong("objetoi_id", getCredito().getId()).uniqueResult();
		if (fecha == null)
			return null;
		else
			return DateHelper.getString(new Date(fecha.getTime()));
	}

	public String getTipoProductoGarantia() {
		String query = "select t.tf_descripcion from ObjetoiGarantia og " + "join Garantia g on og.garantia_id = g.id "
				+ "join Tipificadores t on g.tipoProducto = t.tf_codigo and tf_categoria = 'Garantia.tipoProducto' "
				+ "where og.objetoi_id = :id";
		String tipoProducto = (String) bp.createSQLQuery(query).setParameter("id", getCredito().getId())
				.setMaxResults(1).uniqueResult();
		return tipoProducto;
	}

	public String getFecovita() {
		return fecovita;
	}

	public void setFecovita(String fecovita) {
		this.fecovita = fecovita;
	}

	private void consultaINV() {
		try {
			CreditoInvHelper.consultaINV(id);
		} catch (NonUniqueResultException e) {
			errores.put("registrarQQIngresados.errorconsultainterna", "registrarQQIngresados.errorconsultainterna");
			e.printStackTrace();
		} catch (HibernateException e) {
			errores.put("registrarQQIngresados.errorconsultainterna", "registrarQQIngresados.errorconsultainterna");
			e.printStackTrace();
		} catch (IOException e) {
			errores.put("registrarQQIngresados.errorcomunicacion", "registrarQQIngresados.errorcomunicacion");
			e.printStackTrace();
		} catch (NullPointerException e) {
			errores.put("registrarQQIngresados.url", "registrarQQIngresados.url");
			e.printStackTrace();
		} catch (Exception e) {
			errores.put("registrarQQIngresados.errorformato", "registrarQQIngresados.errorformato");
			e.printStackTrace();
		}
	}

}
