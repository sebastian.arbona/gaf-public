declare @fechaDesde datetime
declare @fechaHasta datetime
declare @filtrarFecha varchar(50)
declare @tipoBoleto varchar(50)
declare @numeroBoleto integer
declare @cuit varchar(50)
declare @concepto varchar(50)
declare @cuentaContable varchar(50)
declare @idEstado integer
declare @comportamiento varchar(50)
declare @areaResponsable varchar(50)
declare @idmoneda varchar(50)
declare @ultimoDia integer
declare @periodo integer
set @fechaDesde = CONVERT(datetime, ':fechaDesde', 121)
set @fechaHasta = CONVERT(datetime, ':fechaHasta', 121)
set @filtrarFecha = ':filtrarFecha'
set @tipoBoleto = :tipoBoleto
set @numeroBoleto = :numeroBoleto
set @cuit = :cuit
set @concepto = :concepto
set @cuentaContable = :cuentaContable
set @idEstado = :idEstado
set @comportamiento = :comportamiento
set @areaResponsable = :areaResponsable
set @idmoneda = :idmoneda
set @ultimoDia = :ultimoDia
set @periodo = :periodo
select 
cuentaContable, fechaGeneracion, min(fechaProceso) as fechaProceso, numeroAtencion, --3
IDPERSONA, titular, cuit, linea, --7
MIN(periodoCtacte) as periodo, numeroBoleto, moneda, tipo, detalle, --12
sum(capital) as capital, sum(compensatorio) as compensatorio, sum(moratorio) as moratorio, --15 
sum(punitorio) as punitorio, sum(gastos) as gastos, --17
nombreEstadoInicial, comportamientoPagoInicial, areaResponsableInicial, --20 
nombreEstadoFinal, comportamientoPagoFinal, areaResponsableFinal, --23
idObjetoi, idBoleto, funcionarioResponsableInicial, funcionarioResponsableFinal, --27 
sum(gastosrec) gastosrec, sum(multas) multas, sum(cer) cer, --30
sum(capitalPesos) capitalPesos, sum(compensatorioPesos) compensatorioPesos, --32 
sum(moratorioPesos) moratorioPesos, sum(punitorioPesos) punitorioPesos, --34
sum(gastosPesos) gastosPesos, sum(gastosrecPesos) gastosrecPesos, sum(multasPesos) multasPesos, --37 
codigoIngreso, conceptoIngreso --39
from (
select cc.objetoi_id, 
cc.fechaGeneracion,
cc.fechaProceso, cc.periodoCtacte, bol.tipo, bol.numeroBoleto, bol.id as idBoleto, cc.tipomov_id,  
conc.concepto_concepto, cc.tipoMovimiento,  
case 
when cc.tipomov_id = 2 and conc.concepto_concepto in ('com','gas') and cc.tipoMovimiento = 'cuota' then 'Comp y Gastos'  
when cc.tipomov_id = 2 and conc.concepto_concepto = 'mor' and cc.tipoMovimiento = 'pago' then 'Moratorio'  
when cc.tipomov_id = 2 and conc.concepto_concepto = 'pun' and cc.tipoMovimiento = 'pago' then 'Punitorio'  
when cc.tipomov_id = 1 and conc.concepto_concepto = 'bon' and cc.tipoMovimiento = 'bonificacion' then cc.detalle  
when cc.tipomov_id = 1 and cc.detalle = 'Pago excedente' then 'Cobranza' 
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' and cc.tipoMovimiento = 'cuota' or conc.concepto_concepto = 'rec' and cc.tipoMovimiento = 'cuota') then 'Pago Gastos' 
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'rec') and cc.caratula_id is null and cc.tipoMovimiento like 'pago' then 'Pago Gastos' 
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'pago' then 'Cobranza' 
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'reimputacion' then cc.detalle 
else cc.detalle end as detalle,  
case when conc.concepto_concepto = 'cap' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as capital,  
case when conc.concepto_concepto in ('com','bon') then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as compensatorio,  
case when conc.concepto_concepto = 'mor' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as moratorio,  
case when conc.concepto_concepto = 'pun' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as punitorio,  
case when conc.concepto_concepto = 'gas' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as gastos, 
case when conc.concepto_concepto = 'rec' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as gastosrec,  
case when conc.concepto_concepto = 'mul' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as multas,  
case when conc.concepto_concepto = 'CER' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as cer,  
case when conc.concepto_concepto in ('cap','adc','adtc') then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end 
else 0 end as capitalPesos,  
case when conc.concepto_concepto in ('com','bon') then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end  
else 0 end as compensatorioPesos, 
case when conc.concepto_concepto = 'mor' then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end 
else 0 end as moratorioPesos, 
case when conc.concepto_concepto = 'pun' then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end 
else 0 end as punitorioPesos,  
case when conc.concepto_concepto = 'gas' then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end 
else 0 end as gastosPesos, 
case when conc.concepto_concepto = 'rec' then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end 
else 0 end as gastosrecPesos,  
case when conc.concepto_concepto = 'mul' then 
	case when cc.dtype = 'CtaCteAjuste' then (cc.debePesos - cc.haberPesos) else (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end) * cc.cotizaMov end 
else 0 end as multasPesos,  
case when (cc.tipoMovimiento = 'pago' or cc.tipoMovimiento = 'reimputacion') then 5  
else case conc.concepto_concepto when 'cap' then 1  
	when 'com' then 2  
	when 'gas' then 2  
	when 'mul' then 2  
	when 'bon' then 3  
	when 'mor' then 4  
	when 'pun' then 5  
	else 6 end  
end as orden, 
case when conc.concepto_concepto = 'cap' then l.codigoCtaContablePatrimonioCapital 
	 when conc.concepto_concepto = 'com' then l.codigoCtaContablePatrimonioCompensatorio 
	 when conc.concepto_concepto = 'gas' and cc.tipoMovimiento = 'pago' and cc.tipoConcepto = '101' then l.codigoCtaContableRecursosGasto 
	 when conc.concepto_concepto = 'gas' then l.codigoCtaContablePatrimonioGasto 
	 when conc.concepto_concepto = 'mul' then l.codigoCtaContablePatrimonioMulta 
	 when conc.concepto_concepto = 'bon' then l.codigoCtaContablePatrimonioCompensatorio 
	 when conc.concepto_concepto = 'mor' then l.codigoCtaContablePatrimonioMoratorio 
	 when conc.concepto_concepto = 'pun' then l.codigoCtaContablePatrimonioPunitorio
	 when conc.concepto_concepto = 'rec' then l.codigoCtaContablePatrimonioGastoRec 
	 when conc.concepto_concepto = 'CER' then l.codigoCtaContablePatrimonioCER 
	 else null end as cuentaContable, 
o.id as idObjetoi, o.numeroAtencion, 
p.IDPERSONA, p.NOMB_12 as titular, p.CUIL_12 as cuit, l.nombre as linea,
ei.nombreEstado nombreEstadoInicial, oci.comportamientoPago as comportamientoPagoInicial,
/* Ticket 5675 */ 
--case when ei.nombreEstado in ('GESTION JUDICIAL','GESTION EXTRAJUDICIAL') then tfaei.TF_DESCRIPCION else tfaci.TF_DESCRIPCION end as areaResponsableInicial,
unii.nombre areaResponsableInicial,
ef.nombreEstado nombreEstadoFinal, ocf.comportamientoPago as comportamientoPagoFinal,
--case when ef.nombreEstado in ('GESTION JUDICIAL','GESTION EXTRAJUDICIAL') then tfaef.TF_DESCRIPCION else tfacf.TF_DESCRIPCION end as areaResponsableFinal,
unif.nombre areaResponsableFinal, 
m.DENOMINACION as moneda, fri.nombreResponsable funcionarioResponsableInicial, frf.nombreResponsable funcionarioResponsableFinal, 
case when (cc.tipoMovimiento = 'pago' or bol.tipo = 'Nota Credito' and cc.tipoConcepto = '106') and cc.tipomov_id = 1 then cing.codigoIngreso else null end as codigoIngreso, 
case when (cc.tipoMovimiento = 'pago' or bol.tipo = 'Nota Credito' and cc.tipoConcepto = '106') and cc.tipomov_id = 1 then cing.conceptoIngreso else null end as conceptoIngreso 
from Objetoi o
left join (
select * 
from Ctacte cc2
where (@filtrarFecha <> 'proceso' or cc2.fechaProceso >= @fechaDesde and 
(@ultimoDia = 0 and cc2.fechaProceso <= @fechaHasta or @ultimoDia = 1 and cc2.periodoCtacte = @periodo))
and (@filtrarFecha <> 'valor' or cc2.fechaGeneracion >= @fechaDesde and cc2.fechaGeneracion <= @fechaHasta)
) cc on cc.objetoi_id = o.id
left join Boleto bol on cc.boleto_id = bol.id

inner join ObjetoiEstado oe3 on oe3.id = (select min(id) from ObjetoiEstado oe4 where o.id = oe4.objetoi_id and oe4.fecha <= @fechaHasta  and (oe4.fechaHasta is null or oe4.fechaHasta>=@fechaDesde) and oe4.estado_idEstado in  (:idsEstados))

left join ObjetoiEstado oei on oei.id=isnull((select max(id) from ObjetoiEstado oe5 where o.id = oe5.objetoi_id and oe5.fecha <= @fechaDesde and (oe5.fechaHasta >= @fechaDesde or oe5.fechaHasta is null)), 
(select MIN(id) from ObjetoiEstado oe5 where o.id = oe5.objetoi_id ))

left join Estado ei on oei.estado_idEstado = ei.idEstado
left join ObjetoiEstado oef on o.id = oef.objetoi_id and oef.fecha <= @fechaHasta and (oef.fechaHasta >= @fechaHasta or oef.fechaHasta is null)
left join Estado ef on oef.estado_idEstado = ef.idEstado

 

left join ObjetoiComportamiento oci on o.id = oci.objetoi_id and oci.fecha <= @fechaDesde and (oci.fechaHasta >= @fechaDesde or oci.fechaHasta is null)
left join ObjetoiComportamiento ocf on o.id = ocf.objetoi_id and ocf.fecha <= @fechaHasta and (ocf.fechaHasta >= @fechaHasta or ocf.fechaHasta is null)
/* Ticket 5675
left join TIPIFICADORES tfaei on tfaei.TF_CATEGORIA = 'AreaResponsableEstado' and tfaei.TF_CODIGO = ei.nombreEstado
left join TIPIFICADORES tfaef on tfaef.TF_CATEGORIA = 'AreaResponsableEstado' and tfaef.TF_CODIGO = ef.nombreEstado
left join TIPIFICADORES tfaci on tfaci.TF_CATEGORIA = 'AreaResponsableComportamiento' and tfaci.TF_CODIGO = oci.comportamientoPago 
left join TIPIFICADORES tfacf on tfacf.TF_CATEGORIA = 'AreaResponsableComportamiento' and tfacf.TF_CODIGO = ocf.comportamientoPago 
*/
left join Concepto conc on cc.asociado_id = conc.id 
left join CConcepto cconc on conc.concepto_concepto = cconc.concepto 
left join Linea l on o.linea_id = l.id 
left join PERSONA p on o.persona_IDPERSONA = p.IDPERSONA 
left join moneda m on l.moneda_IDMONEDA = m.IDMONEDA 
left join FuncionarioResponsable fri on ei.idEstado = fri.idEstado and (fri.comportamiento is null or oci.comportamientoPago = fri.comportamiento) and @fechaDesde >= fri.fechaDesde and (@fechaDesde <= fri.fechaHasta OR fri.fechaHasta IS NULL)
left join FuncionarioResponsable frf on ef.idEstado = frf.idEstado and (frf.comportamiento is null or ocf.comportamientoPago = frf.comportamiento) and @fechaHasta >= fri.fechaDesde and (@fechaHasta <= fri.fechaHasta OR fri.fechaHasta IS NULL)
left join ConceptoIngreso cing on cconc.concepto = cing.concepto and (cc.tipoConcepto = cing.tipoConcepto or cc.tipoConcepto is null and cing.tipoConcepto = '106')
/* Ticket 5675*/
left join usuario ui on ui.causerK = fri.causer_K
left join usuario uf on uf.causerK = frf.causer_K
left join unidad unii on unii.id = ui.unidad_id
left join unidad unif on unif.id = uf.unidad_id
where 
m.IDMONEDA = @idmoneda and 
(@tipoBoleto is null or bol.tipo = @tipoBoleto)
and (@numeroBoleto is null or bol.numeroBoleto = @numeroBoleto)
and (@cuit is null or p.CUIL_12 = @cuit)
and (@concepto is null or conc.concepto_concepto = @concepto)
and (@cuentaContable is null 
or conc.concepto_concepto = 'cap' and l.codigoCtaContablePatrimonioCapital = @cuentaContable 
or conc.concepto_concepto = 'com' and l.codigoCtaContablePatrimonioCompensatorio = @cuentaContable 
or conc.concepto_concepto = 'gas' and cc.tipoMovimiento = 'pago' and cc.tipoConcepto = '101' and l.codigoCtaContableRecursosGasto = @cuentaContable 
or conc.concepto_concepto = 'gas' and l.codigoCtaContablePatrimonioGasto = @cuentaContable 
or conc.concepto_concepto = 'mul' and l.codigoCtaContablePatrimonioMulta = @cuentaContable 
or conc.concepto_concepto = 'bon' and l.codigoCtaContablePatrimonioCompensatorio = @cuentaContable 
or conc.concepto_concepto = 'mor' and l.codigoCtaContablePatrimonioMoratorio = @cuentaContable 
or conc.concepto_concepto = 'pun' and l.codigoCtaContablePatrimonioPunitorio = @cuentaContable 
or conc.concepto_concepto = 'rec' and l.codigoCtaContablePatrimonioGastoRec = @cuentaContable 
or conc.concepto_concepto = 'CER' and l.codigoCtaContablePatrimonioCER = @cuentaContable)
and (@idEstado is null or ei.idEstado = @idEstado)
and (@comportamiento is null or oci.comportamientoPago = @comportamiento)
/* Ticket 5675 
and (@areaResponsable is null 
or ei.nombreEstado in ('GESTION JUDICIAL','GESTION EXTRAJUDICIAL') and tfaei.TF_DESCRIPCION = @areaResponsable 
or ei.nombreEstado not in ('GESTION JUDICIAL','GESTION EXTRAJUDICIAL') and tfaci.TF_DESCRIPCION = @areaResponsable)
*/
and (@areaResponsable is null 
	or ei.nombreEstado in ('GESTION JUDICIAL','GESTION EXTRAJUDICIAL') and unii.id = @areaResponsable 
	or ei.nombreEstado not in ('GESTION JUDICIAL','GESTION EXTRAJUDICIAL') and unif.id = @areaResponsable)

and l.id not in (1601,1565, 1701) -- pedido de excluir lineas 1601 y 1565 del anexo 
and not (o.expediente = '0' or o.expediente is  null or o.expediente = '')  -- si no tiene expediente no se trata de un credito todavia
) t  
group by fechaGeneracion, tipo, numeroBoleto,   
detalle, idObjetoi, cast(fechaProceso as date), cuentaContable, numeroAtencion,
titular, IDPERSONA, cuit, linea, nombreEstadoInicial, comportamientoPagoInicial, areaResponsableInicial,
nombreEstadoFinal, comportamientoPagoFinal, areaResponsableFinal, moneda, idBoleto, 
funcionarioResponsableInicial, funcionarioResponsableFinal, codigoIngreso, conceptoIngreso 
order by numeroAtencion, fechaProceso 