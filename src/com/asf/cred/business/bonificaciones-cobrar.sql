declare @fechaVencimientoDesde date
declare @fechaVencimientoHasta date

set @fechaVencimientoDesde = CONVERT(datetime, ':fechaVencimientoDesde', 102)
set @fechaVencimientoHasta = CONVERT(datetime, ':fechaVencimientoHasta', 102)

select
	o.numeroAtencion,
	p.NOMB_12 as 'titularNombre',
	p.CUIL_12 as 'titularCuit',
	t2.cuotasTotales,
	t2.capitalAcreditado,
	-- incluye actual
	t3.cuotasPagadas,
	(t2.capitalAcreditado - case when t3.capitalPagado is null then 0 else t3.capitalPagado end) as 'saldoCapital',
	c.numero as 'numeroCuota',
	c.fechaVencimiento,
	-- case c.estado when 1 then datediff(day,c.fechaVencimiento,CAST(@fechaVencimientoHasta as datetime2)) else 0 end as 'diasVencida',
	case c.estado when 1 then datediff(day,c.fechaVencimiento,getDate()) else 0 end as 'diasVencida',
	datediff(day,c.fechaVencimiento, isnull((select	max(fechaGeneracion) from ctacte where cuota_id=c.id and tipoMovimiento='pago' and c.estado=2 and fechaGeneracion>c.fechaVencimiento), c.fechaVencimiento)) as diasAtraso,
	
	case when t1.bonificacion is null then abs(c.bonificacion) else abs(t1.bonificacion) end as 'bonificacion',
	-- -- excluye actual
	--(select	count(c2.id) as 'cuotas' from cuota c2 where c2.credito_id = c.credito_id and c2.fechaVencimiento < c.fechaVencimiento and c2.estado = 1) as cuotasImpagas
	(select	count(c2.id) as 'cuotas' from cuota c2 where c2.credito_id = c.credito_id and c2.fechaVencimiento < GETDATE() and c2.estado = 1) as cuotasImpagas,
	oe.estado_idEstado as 'estado' 
from cuota c
inner join Objetoi o on	o.id = c.credito_id
inner join ObjetoiBonificacion ob on ob.idCredito = o.id
inner join ObjetoiEstado oe on oe.objetoi_id = o.id -- and oe.fechaHasta is null --(esta modificacion la hago para el ticket 23149)
inner join bonificacion b on b.id = ob.idBonificacion
inner join ConvenioBonificacion cb on cb.id = b.convenio_id
inner join persona p on p.IDPERSONA = o.persona_IDPERSONA
inner join (select cc.cuota_id,	sum(case tm.abreviatura when 'cr' THEN -cc.importe else cc.importe end) as 'bonificacion'
		    from Ctacte cc
			inner join Tipomov tm on tm.id = cc.tipomov_id
			inner join Concepto asoc on asoc.id = cc.asociado_id
			where asoc.concepto_concepto = 'bon'
			group by cc.cuota_id) t1 on t1.cuota_id = c.id
inner join (select c1.credito_id, count(c1.id) as 'cuotasTotales', sum(c1.capital) as 'capitalAcreditado' 
		    from cuota c1 
		    group by c1.credito_id) t2 on t2.credito_id = o.id
inner join (select cp.id as 'cuotaId', 
		           (select count(c1.id) from cuota c1 where c1.credito_id = cp.credito_id and c1.estado = 2 and c1.fechaVencimiento <= cp.fechavencimiento) as 'cuotasPagadas',
	               (select sum(c2.capital) from cuota c2 where c2.credito_id = cp.credito_id and c2.estado = 2 and c2.fechaVencimiento <= cp.fechavencimiento) as 'capitalPagado'
	        from cuota cp) t3 on t3.cuotaId = c.id
where
	c.fechaVencimiento BETWEEN CAST(@fechaVencimientoDesde as datetime2) and CAST(@fechaVencimientoHasta as datetime2)
    and oe.estado_idEstado in (:estados)
    :filtroConvenio
order by o.numeroAtencion,c.numero        