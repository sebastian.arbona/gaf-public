package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Objetoi;

public class BuscarCobranzas implements IProcess {

	private String forward = "cobranzasList";
	private HashMap<String, Object> errores;
	private ArrayList<Objetoi> solicitudes;
	private String accion;
	private String numeroAtencion;

	public BuscarCobranzas() {
		this.errores = new HashMap<String, Object>();
	}
	
	public boolean doProcess() {
		if(accion == null){
			return false;
		}		
		return false;
	}

	
	

	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return errores;
	}

	public String getForward() {
		return forward;
	}

	public String getInput() {
		return forward;
	}

	public Object getResult() {
		return solicitudes;
	}

	public boolean validate() {
		return true;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setSolicitudes(ArrayList<Objetoi> objetos) {
		this.solicitudes = objetos;
	}

	public ArrayList<Objetoi> getSolicitudes() {
		return solicitudes;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(String numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	

	

	

	

}
