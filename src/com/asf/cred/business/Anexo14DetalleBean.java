package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

import com.asf.util.TipificadorHelper;

public class Anexo14DetalleBean implements Serializable {

	private static final long serialVersionUID = 7870599045773629350L;
	
	public enum Tipo { INICIAL, FINAL };
	
	private String codigoIngreso;
	private String conceptoIngreso;
	private String cuentaContable;
	private Date fechaValor;
	private Date fechaProceso;
	private Long numeroAtencion;
	private Long idPersona;
	private String persona;
	private String cuit;
	private String linea;
	private String estado;
	private String comportamiento;
	private String areaResponsable;
	private String funcionarioResponsable;
	private String observaciones;
	private Movimiento mov;
	
	public Anexo14DetalleBean(Object[] r, Object[] saldos, Tipo tipo) {
		if (tipo == Tipo.INICIAL) {
			cuentaContable = "SALDO INICIAL";
			
			estado = r[18] != null ? (String) r[18] : "";
			if(!estado.equals("EJECUCION") && !estado.equals("PRIMER DESEMBOLSO") &&
					!estado.equals("PENDIENTE SEGUNDO DESEMBOLSO")){
				comportamiento = "N/A";
			} else {
				comportamiento = TipificadorHelper.getString("comportamientoPago", (String) r[19]);
			}
			areaResponsable = (String) r[20];
			funcionarioResponsable = (String) r[26];
		} else if (tipo == Tipo.FINAL) {
			cuentaContable = "SALDO FINAL";
			
			estado = (String) r[21];
			if(!estado.equals("EJECUCION") && !estado.equals("PRIMER DESEMBOLSO") &&
					!estado.equals("PENDIENTE SEGUNDO DESEMBOLSO")){
				comportamiento = "N/A";
			} else {
				comportamiento = TipificadorHelper.getString("comportamientoPago", (String) r[22]);
			}
			areaResponsable = (String) r[23];
			funcionarioResponsable = (String) r[27];
		}
		
		numeroAtencion = ((Number) r[3]).longValue();
		idPersona = ((Number) r[4]).longValue();
		persona = (String) r[5];
		cuit = r[6] != null ? ((Number) r[6]).toString() : null;
		linea = (String) r[7];
		
		mov = new Movimiento();
		mov.capital = saldos[0] != null ? ((Number) saldos[0]).doubleValue() : 0;
		mov.compensatorio = saldos[1] != null ? ((Number) saldos[1]).doubleValue() : 0;
		mov.moratorio = saldos[2] != null ? ((Number) saldos[2]).doubleValue() : 0;
		mov.punitorio = saldos[3] != null ? ((Number) saldos[3]).doubleValue() : 0;
		mov.gastos = saldos[4] != null ? ((Number) saldos[4]).doubleValue() : 0;
		mov.gastosRec = saldos[5] != null ? ((Number) saldos[5]).doubleValue() : 0;
		mov.multas = saldos[6] != null ? ((Number) saldos[6]).doubleValue() : 0;
		mov.cer = saldos[7] != null ? ((Number) saldos[7]).doubleValue() : 0;
		mov.capitalPesos = saldos[8] != null ? ((Number) saldos[8]).doubleValue() : 0;
		mov.compensatorioPesos = saldos[9] != null ? ((Number) saldos[9]).doubleValue() : 0;
		mov.moratorioPesos = saldos[10] != null ? ((Number) saldos[10]).doubleValue() : 0;
		mov.punitorioPesos = saldos[11] != null ? ((Number) saldos[11]).doubleValue() : 0;
		mov.gastosPesos = saldos[12] != null ? ((Number) saldos[12]).doubleValue() : 0;
		mov.gastosRecPesos = saldos[13] != null ? ((Number) saldos[13]).doubleValue() : 0;
		mov.multasPesos = saldos[14] != null ? ((Number) saldos[14]).doubleValue() : 0;
	}
	
	public Anexo14DetalleBean(Object[] r) {
		cuentaContable = (String) r[0];
		fechaValor = (Date) r[1];
		fechaProceso = (Date) r[2];
		numeroAtencion = ((Number) r[3]).longValue();
		idPersona = ((Number) r[4]).longValue();
		persona = (String) r[5];
		cuit = r[6] != null ? ((Number) r[6]).toString() : null;
		linea = (String) r[7];
		codigoIngreso = (String) r[38];
		conceptoIngreso = (String) r[39];
		
		mov = new Movimiento();
		mov.numeroBoleto = r[9] != null ? ((Number) r[9]).longValue() : null;
		mov.moneda = (String) r[10];
		mov.tipoBoleto = (String) r[11];
		mov.detalle = (String) r[12];
		mov.capital = r[13] != null ? ((Number) r[13]).doubleValue() : 0;
		mov.compensatorio = r[14] != null ? ((Number) r[14]).doubleValue() : 0;
		mov.moratorio = r[15] != null ? ((Number) r[15]).doubleValue() : 0;
		mov.punitorio = r[16] != null ? ((Number) r[16]).doubleValue() : 0;
		mov.gastos = r[17] != null ? ((Number) r[17]).doubleValue() : 0;
		mov.gastosRec = r[28] != null ? ((Number) r[28]).doubleValue() : 0;
		mov.multas = r[29] != null ? ((Number) r[29]).doubleValue() : 0;
		mov.cer = r[30] != null ? ((Number) r[30]).doubleValue() : 0;
		mov.capitalPesos = r[31] != null ? ((Number) r[31]).doubleValue() : 0;
		mov.compensatorioPesos = r[32] != null ? ((Number) r[32]).doubleValue() : 0;
		mov.moratorioPesos = r[33] != null ? ((Number) r[33]).doubleValue() : 0;
		mov.punitorioPesos = r[34] != null ? ((Number) r[34]).doubleValue() : 0;
		mov.gastosPesos = r[35] != null ? ((Number) r[35]).doubleValue() : 0;
		mov.gastosRecPesos = r[36] != null ? ((Number) r[36]).doubleValue() : 0;
		mov.multasPesos = r[37] != null ? ((Number) r[37]).doubleValue() : 0;
	}
	
	public class Movimiento {
		private Long numeroBoleto;
		private String moneda;
		private String tipoBoleto;
		private String detalle;
		private double capital;
		private double compensatorio;
		private double moratorio;
		private double punitorio;
		private double gastos;
		private double gastosRec;
		private double multas;
		private double capitalPesos;
		private double compensatorioPesos;
		private double moratorioPesos;
		private double punitorioPesos;
		private double gastosPesos;
		private double gastosRecPesos;
		private double multasPesos;
		private double cer;
		
		public double getTotal() {
			return capital + compensatorio + moratorio + punitorio + gastos + gastosRec + multas + cer;
		}
		
		public double getTotalPesos() {
			return capitalPesos + compensatorioPesos + moratorioPesos + punitorioPesos + gastosPesos +
					gastosRecPesos + multasPesos;
		}
		
		public Long getNumeroBoleto() {
			return numeroBoleto;
		}
		public void setNumeroBoleto(Long numeroBoleto) {
			this.numeroBoleto = numeroBoleto;
		}
		public String getMoneda() {
			return moneda;
		}
		public void setMoneda(String moneda) {
			this.moneda = moneda;
		}
		public String getTipoBoleto() {
			return tipoBoleto;
		}
		public void setTipoBoleto(String tipoBoleto) {
			this.tipoBoleto = tipoBoleto;
		}
		public String getDetalle() {
			return detalle;
		}
		public void setDetalle(String detalle) {
			this.detalle = detalle;
		}
		public double getCapital() {
			return capital;
		}
		public void setCapital(double capital) {
			this.capital = capital;
		}
		public double getCompensatorio() {
			return compensatorio;
		}
		public void setCompensatorio(double compensatorio) {
			this.compensatorio = compensatorio;
		}
		public double getMoratorio() {
			return moratorio;
		}
		public void setMoratorio(double moratorio) {
			this.moratorio = moratorio;
		}
		public double getPunitorio() {
			return punitorio;
		}
		public void setPunitorio(double punitorio) {
			this.punitorio = punitorio;
		}
		public double getGastos() {
			return gastos;
		}
		public void setGastos(double gastos) {
			this.gastos = gastos;
		}
		public double getGastosRec() {
			return gastosRec;
		}
		public void setGastosRec(double gastosRec) {
			this.gastosRec = gastosRec;
		}
		public double getMultas() {
			return multas;
		}
		public void setMultas(double multas) {
			this.multas = multas;
		}
		public double getCapitalPesos() {
			return capitalPesos;
		}
		public void setCapitalPesos(double capitalPesos) {
			this.capitalPesos = capitalPesos;
		}
		public double getCompensatorioPesos() {
			return compensatorioPesos;
		}
		public void setCompensatorioPesos(double compensatorioPesos) {
			this.compensatorioPesos = compensatorioPesos;
		}
		public double getMoratorioPesos() {
			return moratorioPesos;
		}
		public void setMoratorioPesos(double moratorioPesos) {
			this.moratorioPesos = moratorioPesos;
		}
		public double getPunitorioPesos() {
			return punitorioPesos;
		}
		public void setPunitorioPesos(double punitorioPesos) {
			this.punitorioPesos = punitorioPesos;
		}
		public double getGastosPesos() {
			return gastosPesos;
		}
		public void setGastosPesos(double gastosPesos) {
			this.gastosPesos = gastosPesos;
		}
		public double getGastosRecPesos() {
			return gastosRecPesos;
		}
		public void setGastosRecPesos(double gastosRecPesos) {
			this.gastosRecPesos = gastosRecPesos;
		}
		public double getMultasPesos() {
			return multasPesos;
		}
		public void setMultasPesos(double multasPesos) {
			this.multasPesos = multasPesos;
		}
		public double getCer() {
			return cer;
		}
		public void setCer(double cer) {
			this.cer = cer;
		}
	}

	public String getCodigoIngreso() {
		return codigoIngreso;
	}

	public void setCodigoIngreso(String codigoIngreso) {
		this.codigoIngreso = codigoIngreso;
	}

	public String getConceptoIngreso() {
		return conceptoIngreso;
	}

	public void setConceptoIngreso(String conceptoIngreso) {
		this.conceptoIngreso = conceptoIngreso;
	}

	public String getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}

	public Date getFechaValor() {
		return fechaValor;
	}

	public void setFechaValor(Date fechaValor) {
		this.fechaValor = fechaValor;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getComportamiento() {
		return comportamiento;
	}

	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}

	public String getAreaResponsable() {
		return areaResponsable;
	}

	public void setAreaResponsable(String areaResponsable) {
		this.areaResponsable = areaResponsable;
	}

	public String getFuncionarioResponsable() {
		return funcionarioResponsable;
	}

	public void setFuncionarioResponsable(String funcionarioResponsable) {
		this.funcionarioResponsable = funcionarioResponsable;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Movimiento getMov() {
		return mov;
	}

	public void setMov(Movimiento mov) {
		this.mov = mov;
	}
	
}
