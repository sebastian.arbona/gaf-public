package com.asf.cred.business;

import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletOutputStream;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.asf.cred.security.SessionHandler;
import com.asf.hibernate.mapping.Tipificadores;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.el.especific.ProrrogaContextoEjecucion;
import com.civitas.el.especific.ProrrogaExpressionEvaluator;
import com.civitas.hibernate.persona.Estado;
import com.itextpdf.text.Document;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;
import com.nirven.creditos.hibernate.Escrito;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.SolicitudProrroga;
import com.nirven.creditos.hibernate.SolicitudProrrogaEstado;

public class SolicitudProrrogaPlantillaProcess extends com.asf.struts.action.ConversationProcess {
// ==========================ATRIBUTOS============================
	public static String separador = "@";
	public static Long agregaEstado = 1L;
	public static Long agregaObs = 2L;
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-

//	private String forward = "BuscarSolicitudProrrogaList";
	private HashMap<String, Object> errores;
	@Save
	private String numeroCredito;
	@Save
	private Long numeroAtencion;
	@Save
	private String expediente;
	private String persona;
	@Save(refresh = true)
	private Objetoi solicitud;
//	private String accion;
	private String miVar;
	private Long idEscrito;
	private Date fechaIngSol;
	private String observacion;
	private Long idTipificador;
	private String motivoProrroga;
	private List<Tipificadores> tipificadores;
	@Save
	private List<SolicitudProrroga> solProrroga;
	private SolicitudProrrogaEstado solProEstado;

	// atributos prorrogaEstados.jsp
	private Long idEstado;
	private Estado estado;
	private Long agrega;
	private List<SolicitudProrrogaEstado> solicitudProrrogaEstados;
	private SolicitudProrroga solicitudProrroga;
	private SolicitudProrrogaEstado estadoActual;
	private Long idSolicitudProrroga;
	private String obsEstado;
	private String numeroResolucion;
	private Date fechaResolucion;
	private FormFile formFile;
	private String detalleObservacion;

	public SolicitudProrrogaPlantillaProcess() {
	}

	@ProcessMethod
	public boolean listar() {
		buscarSolicitudes();
		if (solicitud != null) {
			buscarSolicitudesProrroga();
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	private void buscarSolicitudes() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String consulta;

		if (this.expediente != null && !this.expediente.trim().isEmpty()) {
			consulta = "SELECT o FROM Objetoi o WHERE o.expediente = '" + this.expediente
					+ "' ORDER BY o.numeroAtencion";
		} else if (this.persona != null && !this.persona.trim().isEmpty()) {
			Long idPersona = new Long(persona.split("-")[0]);
			consulta = "SELECT o FROM Objetoi o WHERE o.persona.id = " + idPersona + " ORDER BY o.numeroAtencion";
		} else if (this.numeroCredito != null && !this.numeroCredito.trim().isEmpty()) {
			consulta = "SELECT o FROM Objetoi o WHERE o.numeroCredito = '" + this.numeroCredito
					+ "' ORDER BY o.numeroAtencion";
		} else if (this.numeroAtencion != null && this.numeroAtencion != 0L) {
			consulta = "SELECT o FROM Objetoi o WHERE o.numeroAtencion = " + this.numeroAtencion;
		} else {
			consulta = "SELECT o FROM Objetoi o ORDER BY o.numeroAtencion";
		}
		solicitud = new Objetoi();

		List<Objetoi> obj2 = (List<Objetoi>) bp.getByFilter(consulta);
//		Estado estado;
//		try {
//			estado = (Estado) bp.getByFilter(
//					"SELECT e FROM Estado e WHERE e.nombreEstado = 'ESPERANDO DOCUMENTACIÓN' "
//							+ "OR e.nombreEstado = 'ESPERANDO DOCUMENTACION'")
//					.get(0);
//		} catch (Exception e) {
//			errors.put("errors.detail", new ActionMessage(
//					"errors.detail",
//					"Error al buscar el estado ESPERANDO DOCUMENTACIÓN."));
//			return;
//		}
		for (Objetoi objetoi : obj2) {
			try {
//				if (!objetoi.getEstadoActual().getEstado().equals(estado)
				if (objetoi.getEstadoActual() != null && objetoi.getEstadoActual().getEstado() != null
						&& (objetoi.getEstadoActual().getEstado().getId() >= 6)
						&& (objetoi.getEstadoActual().getEstado().getId() <= 11) && objetoi.getExpediente() != null
						&& !objetoi.getExpediente().isEmpty() && !objetoi.getExpediente().equals("0")) {
					solicitud = objetoi;

				} else {
					errors.put("errors.detail",
							new ActionMessage("errors.detail", "Algunas solicitudes no se encuentran en ejecución."));
				}
			} catch (Exception e) {
				e.printStackTrace();
				if (!errores.containsKey("errors.detail")) {
					errors.put("errors.detail",
							new ActionMessage("errors.detail", "Algunas solicitudes no poseen estado actual."));
				}
			}
		}

	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean eliminarProrroga() {

		if (idSolicitudProrroga != null) {
			try {
				solicitudProrrogaEstados = bp.createQuery(
						"from SolicitudProrrogaEstado where estado.id=29 and solicitudProrroga.id = :idSolicitudProrroga and fechaHasta IS NULL")
						.setParameter("idSolicitudProrroga", idSolicitudProrroga).list();
				if (solicitudProrrogaEstados.size() > 0) {
					SolicitudProrrogaEstado borrar = solicitudProrrogaEstados.get(0);
					bp.delete(borrar);
					solicitudProrroga = (SolicitudProrroga) bp.getById(SolicitudProrroga.class,
							this.idSolicitudProrroga);
					bp.delete(solicitudProrroga);
					bp.commit();

				}

				return cancelar();
			} catch (Exception e) {
				errors.put("SolicitudProrroga.Error.Eliminar", "SolicitudProrroga.Error.Eliminar");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	@ProcessMethod
	public boolean imprimirProrroga() {

		Escrito plantilla = (Escrito) bp.createQuery("select e from Escrito e where e.tipoEscrito = 'P'")
				.uniqueResult();

		if (plantilla != null) {
			ProrrogaExpressionEvaluator expresionEvaluator = new ProrrogaExpressionEvaluator();
			ProrrogaContextoEjecucion ec = new ProrrogaContextoEjecucion();
			Objetoi obji = solicitud;
			ec.setObjetoi(obji);
			ec.setPersona(obji.getPersona());
			String result = (String) expresionEvaluator.evaluate(plantilla.getBodyStr(), ec);
			result = result.replaceAll("&quot;", "\"");
			try {

				ServletOutputStream sos = getResponse().getOutputStream();
				String name = plantilla.getIdentificacion() + "_" + DateHelper.getStringFull2(new Date());
				getResponse().setHeader("Content-Disposition", "attachment;filename=\"" + name + ".pdf\"");
				getResponse().setContentType("application/pdf");
				String html = result;
				Document document = new Document();
				PdfWriter.getInstance(document, sos);
				document.open();
				HTMLWorker htmlWorker = new HTMLWorker(document);
				htmlWorker.parse(new StringReader(html));
				document.close();
				sos.close();

				return true;

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		return false;

	}

	@SuppressWarnings("unchecked")
	private void buscarSolicitudesProrroga() {

		Long idObjetoi = solicitud.getId();

		try {
			List<SolicitudProrroga> listSolPro = bp
					.createQuery("select s from SolicitudProrroga s where s.objetoi.id = :idObjetoi")
					.setParameter("idObjetoi", idObjetoi).list();
			if (listSolPro != null && listSolPro.size() > 0) {
				solProrroga = listSolPro;
			} else {
				solProrroga.clear();
			}
		} catch (Exception e) {
			errors.put("errors.detail",
					new ActionMessage("errors.detail", "Error al buscar las solicitudes de prórroga."));
			return;
		}
	}

	@ProcessMethod
	public boolean guardar() {

		Objetoi obji = solicitud;
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String estadoInicialId = DirectorHelper.getString("Prorroga.EstadoInicial");

		boolean resultado = false;

		try {

			SolicitudProrrogaEstado solProEst = new SolicitudProrrogaEstado();
			Estado est = (Estado) bp.getById(Estado.class, new Long(estadoInicialId.trim()));

			SolicitudProrroga sp = new SolicitudProrroga();
			sp.setFechaIngreso(new Date());
			sp.setFechaProceso(new Date());
			sp.setMotivo(motivoProrroga);
			sp.setObjetoi(obji);
			sp.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			sp.setObservacion(observacion);

			solProEst.setEstado(est);
			solProEst.setFechaDesde(new Date());
			solProEst.setSolicitudProrroga(sp);
			solProEst.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());

			bp.save(sp);
			bp.saveOrUpdate(solProEst);

			resultado = true;

			fechaIngSol = null;

			observacion = "";

			buscarSolicitudesProrroga();

			return resultado;

		} catch (Exception e) {
			errors.put("errors.detail", new ActionMessage("errors.detail", "Error al guardar la prórroga."));
			return resultado;
		}

	}

	@ProcessMethod
	public boolean load() {
		if (idSolicitudProrroga != null) {
			try {
				solicitudProrroga = (SolicitudProrroga) bp.getById(SolicitudProrroga.class, this.idSolicitudProrroga);
				forward = "ProrrogaEstados";
				return true;
			} catch (Exception e) {
				errors.put("SolicitudProrroga.Error.cargar", "SolicitudProrroga.Error.carga");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardarEstadoProrroga() {

		if (idEstado != null) {
			try {
				estado = (Estado) bp.getById(Estado.class, this.idEstado);
			} catch (Exception e) {
				errors.put("Estado.Error.cargar", "Estado.Error.carga");
				e.printStackTrace();
			}
		}

		if (estado.getNombreEstado().equals("APROBADA")) {

			SolicitudProrroga solicitudProrroga = (SolicitudProrroga) bp.getById(SolicitudProrroga.class,
					idSolicitudProrroga);

			List<SolicitudProrroga> listaSolicitudesProrroga = bp
					.createQuery("from SolicitudProrroga sp where sp.objetoi.id = :idObjetoi")
					.setParameter("idObjetoi", solicitudProrroga.getObjetoi().getId()).list();

			for (SolicitudProrroga sp : listaSolicitudesProrroga) {

				if (sp.getEstadoActualProrroga().getEstado().getNombreEstado().equals("APROBADA")) {

					errors.put("prorroga.solicitud.error.estado", "prorroga.solicitud.error.estado");
					forward = "ProrrogaEstados";
					return false;
				}
			}
		}

		if (estado.getIdEstado() == 0 && agrega.equals(new Long(2))) {
			solicitudProrrogaEstados = bp.createQuery(
					"from SolicitudProrrogaEstado where solicitudProrroga.id = :idSolicitudProrroga and fechaHasta IS NULL")
					.setParameter("idSolicitudProrroga", idSolicitudProrroga).list();
			if (solicitudProrrogaEstados.size() > 0)
				estado = solicitudProrrogaEstados.get(0).getEstado();
		} else {
			estado = (Estado) bp.getById(Estado.class, estado.getId());
		}

		if (this.agrega.longValue() == agregaEstado.longValue()) {
			this.guardarNuevoEstado();
		} else if (this.agrega.longValue() == agregaObs.longValue()) {
			this.guardarObservacion();
		}
		if (errors.isEmpty()) {
			buscarSolicitudesProrroga();
			forward = "BuscarSolicitudProrrogaList";
		} else {
			forward = "ProrrogaEstados";
		}
		return true;
	}

	@ProcessMethod
	public boolean cancelar() {
		buscarSolicitudesProrroga();
		forward = "BuscarSolicitudProrrogaList";
		return true;

	}

	private void guardarNuevoEstado() {
		estado = (Estado) bp.getById(Estado.class, estado.getId());
		solicitudProrroga = (SolicitudProrroga) bp.getById(SolicitudProrroga.class, this.idSolicitudProrroga);

		try {

			SolicitudProrrogaEstado solicitudProrrogaEstado = new SolicitudProrrogaEstado();
			solicitudProrrogaEstado.setEstado(this.estado);
			solicitudProrrogaEstado.setSolicitudProrroga(this.solicitudProrroga);
			solicitudProrrogaEstado.setFechaDesde(new Date(System.currentTimeMillis()));
			solicitudProrrogaEstado.setObservacion(this.obsEstado);
			solicitudProrrogaEstado.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			this.validarNuevoEstado(solicitudProrrogaEstado);
			if (errors.isEmpty()) {
				if (estadoActual != null) {
					try {
						estadoActual.setFechaHasta(solicitudProrrogaEstado.getFechaDesde());
						bp.saveOrUpdate(estadoActual);
					} catch (IndexOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			}

			if (estado.getNombreEstado().trim().equals("APROBADA")) {
				if (numeroResolucion != null || !numeroResolucion.trim().equals("")) {
					solicitudProrroga.setNroResolucion(numeroResolucion);
				}

				if (fechaResolucion != null) {
					solicitudProrroga.setFechaResolucion(fechaResolucion);
				}

				if (formFile != null && formFile.getFileSize() > 0) {
					ObjetoiArchivo objetoiArchivo = new ObjetoiArchivo();
					objetoiArchivo.setCredito(solicitud);
					objetoiArchivo.setArchivo(formFile.getFileData());
					objetoiArchivo.setMimetype(formFile.getContentType());
					objetoiArchivo.setNombre(formFile.getFileName());
					objetoiArchivo.setFechaAdjunto(new Date());
					objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
					bp.saveOrUpdate(objetoiArchivo);
				}

				bp.saveOrUpdate(solicitudProrroga);

			} else {
				numeroResolucion = "";
				fechaResolucion = null;
				formFile = null;
			}

			bp.saveOrUpdate(solicitudProrrogaEstado);
			this.estado = new Estado();
			this.obsEstado = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void validarNuevoEstado(SolicitudProrrogaEstado solicitudProrrogaEstado) {
		solicitudProrrogaEstado.validate();
		errors.putAll(solicitudProrrogaEstado.getChErrores());
		estadoActual = solicitudProrroga.getEstadoActualProrroga();
		if (estadoActual != null) {
			if (solicitudProrrogaEstado.getFechaDesde().before(estadoActual.getFechaDesde())) {
				errors.put("SolicitudProrrogaEstado.fecha.anterior", "SolicitudProrrogaEstado.fecha.anterior");
			}
		}
	}

//	@SuppressWarnings("unchecked")
//	@Transient
//	public SolicitudProrrogaEstado getEstadoActual() {
//		estadoActual = new SolicitudProrrogaEstado();
//		bp = SessionHandler.getCurrentSessionHandler()
//				.getBusinessPersistance();
//		try {
//			String consulta = "SELECT spe FROM SolicitudProrrogaEstado spe WHERE spe.fechaHasta IS NULL";
//			consulta += " AND spe.solicitudProrroga.id=" + this.solicitudProrroga.getId()+" ORDER BY spe.fechaDesde desc";
//			List<SolicitudProrrogaEstado> historicoEstados = bp.getByFilter(consulta);
//			if (historicoEstados.size() > 0) {
//				estadoActual = historicoEstados.get(0);
//			}
//		} catch (Exception e) {
//			estadoActual = null;
//		}
//		return estadoActual;
//	}

	private void guardarObservacion() {

	}

	@Override
	protected String getDefaultForward() {
		return "BuscarSolicitudProrrogaList";
	}

//	public Object getResult() {
//		return solicitudes;
//	}
//
//	public boolean validate() {
//		return true;
//	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

//	public void setAccion(String accion) {
//		this.accion = accion;
//	}
//
//	public String getAccion() {
//		return accion;
//	}

	public void setSolicitud(Objetoi objeto) {
		this.solicitud = objeto;
	}

	public Objetoi getSolicitud() {
		return solicitud;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public String getMiVar() {
		return miVar;
	}

	public void setMiVar(String miVar) {
		this.miVar = miVar;
	}

	public Long getIdEscrito() {
		return idEscrito;
	}

	public void setIdEscrito(Long idEscrito) {
		this.idEscrito = idEscrito;
	}

//	public SolicitudProrrogaPlantillaProcess() {
//		errors = new HashMap<String, Object>();
//	}

	public Date getFechaIngSol() {
		return fechaIngSol;
	}

	public void setFechaIngSol(Date fechaIngSol) {
		this.fechaIngSol = fechaIngSol;
	}

	public String getFechaIngSolStr() {
		return DateHelper.getString(fechaIngSol);
	}

	public void setFechaIngSolStr(String fechaIngSolStr) {
		this.fechaIngSol = DateHelper.getDate(fechaIngSolStr);
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getIdTipificador() {
		return idTipificador;
	}

	public void setIdTipificador(Long idTipificador) {
		this.idTipificador = idTipificador;
	}

	public List<Tipificadores> getTipificadores() {
		return tipificadores;
	}

	public void setTipificadores(List<Tipificadores> tipificadores) {
		this.tipificadores = tipificadores;
	}

	public List<SolicitudProrroga> getSolProrroga() {
		return solProrroga;
	}

	public void setSolProrroga(List<SolicitudProrroga> solProrroga) {
		this.solProrroga = solProrroga;
	}

	public SolicitudProrrogaEstado getSolProEstado() {
		return solProEstado;
	}

	public void setSolProEstado(SolicitudProrrogaEstado solProEstado) {
		this.solProEstado = solProEstado;
	}

	public String getMotivoProrroga() {
		return motivoProrroga;
	}

	public void setMotivoProrroga(String motivoProrroga) {
		this.motivoProrroga = motivoProrroga;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Long getAgrega() {
		return agrega;
	}

	public void setAgrega(Long agrega) {
		this.agrega = agrega;
	}

	public List<SolicitudProrrogaEstado> getSolicitudProrrogaEstados() {
		return solicitudProrrogaEstados;
	}

	public void setSolicitudProrrogaEstados(List<SolicitudProrrogaEstado> solicitudProrrogaEstados) {
		this.solicitudProrrogaEstados = solicitudProrrogaEstados;
	}

	public SolicitudProrroga getSolicitudProrroga() {
		return solicitudProrroga;
	}

	public void setSolicitudProrroga(SolicitudProrroga solicitudProrroga) {
		this.solicitudProrroga = solicitudProrroga;
	}

	public Long getIdSolicitudProrroga() {
		return idSolicitudProrroga;
	}

	public void setIdSolicitudProrroga(Long idSolicitudProrroga) {
		this.idSolicitudProrroga = idSolicitudProrroga;
	}

	public String getObsEstado() {
		return obsEstado;
	}

	public void setObsEstado(String obsEstado) {
		this.obsEstado = obsEstado;
	}

	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public String getFechaResolucion() {
		return DateHelper.getString(fechaResolucion);
	}

	public void setFechaResolucion(String f) {
		fechaResolucion = DateHelper.getDate(f);
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public String getDetalleObservacion() {
		return detalleObservacion;
	}

	public void setDetalleObservacion(String detalleObservacion) {
		this.detalleObservacion = detalleObservacion;
	}
}
