package com.asf.cred.business;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.IReportBean;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.struts.form.JReportForm;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;

public class Anexo28 extends ConversationProcess implements IReportBean {

	private Date fechaDesde;
	private Date fechaHasta;
	private List<Anexo28Bean> beans;
	private ReportResult result;
	@Save
	private String textResult;
	private static String sql;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		List<Object[]> result = bp.createSQLQuery(getSql()).setParameter("fechaDesde", fechaDesde)
				.setParameter("fechaHasta", fechaHasta).list();

		beans = new ArrayList<Anexo28Bean>();
		for (Object[] r : result) {
			Anexo28Bean b = new Anexo28Bean(r);
			if (!b.isNulo()) {
				beans.add(b);
			}
		}
		return true;
	}

	@ProcessMethod
	public boolean exportarTexto() {
		buscar();

		textResult = "Ejercicio|Trimestre|Nomenclador|CodCont|CodContDsc|"
				+ "StoCorSalIni|StoCorDes|StoCorRecPor|StoCorRecPes|StoCorSalFin|"
				+ "StoMorSalIni|StoMorAlta|StoMorRecPor|StoMorRecPes|StoMorSalFin|"
				+ "StoGJSalIni|StoGJAlta|StoGJRecPor|StoGJRecPes|StoGJSalFin\r\n";

		Calendar c = Calendar.getInstance();
		c.setTime(fechaDesde);
		int mes = c.get(Calendar.MONTH);
		int trimestre = 1;
		switch (mes) {
		case 0:
		case 1:
		case 2:
			trimestre = 1;
			break;
		case 3:
		case 4:
		case 5:
			trimestre = 2;
			break;
		case 6:
		case 7:
		case 8:
			trimestre = 3;
			break;
		case 9:
		case 10:
		case 11:
			trimestre = 4;
			break;
		}

		String format = "%tY|%d|%6.6s|%s|%s|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f|%.2f%n";

		for (Anexo28Bean b : beans) {
			textResult += String.format(format, fechaDesde, trimestre, "090701", b.getCuentaContable(), b.getLinea(),
					b.getCorriente().getSaldoInicial(), b.getDesembolsado(), b.getCorriente().getPorcentajeRecupero(),
					b.getCorriente().getRecupero(), b.getCorriente().getSaldoFinal(), b.getMorosos().getSaldoInicial(),
					0.0, b.getMorosos().getPorcentajeRecupero(), b.getMorosos().getRecupero(),
					b.getMorosos().getSaldoFinal(), b.getJudicial().getSaldoInicial(), 0.0,
					b.getJudicial().getPorcentajeRecupero(), b.getJudicial().getRecupero(),
					b.getJudicial().getSaldoFinal());
		}

		forward = "ProcessRedirect";
		return true;
	}

	@ProcessMethod
	public boolean exportarXls() {
		result = new ReportResult();
		result.setClassBeanName(getClass().getName());
		result.setExport(JReportForm.EXPORT_XLS);
		result.setReportName("anexo28.jasper");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		result.setParams("fechaDesde=" + dateFormat.format(fechaDesde) + ";fechaHasta=" + dateFormat.format(fechaHasta)
				+ ";BEANS=fechaDesde=" + DateHelper.getString(fechaDesde) + ";fechaHasta="
				+ DateHelper.getString(fechaHasta));

		forward = "Reportes";
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Collection getCollection(HashMap hsParametros) {
		String fechaDesde = (String) hsParametros.get("fechaDesde");
		String fechaHasta = (String) hsParametros.get("fechaHasta");

		setFechaDesde(fechaDesde);
		setFechaHasta(fechaHasta);

		buscar();

		return beans;
	}

	public String getForwardURL() {
		return "/actions/anexo28.do?cid=" + getCid();
	}

	public List<Anexo28Bean> getBeans() {
		return beans;
	}

	@Override
	public Object getResult() {
		return result;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(Anexo28.class.getResourceAsStream("anexo28.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	@Override
	protected String getDefaultForward() {
		return "Anexo28";
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public String getFechaDesdeStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaDesde);
	}

	public String getFechaHastaStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaHasta);
	}
}
