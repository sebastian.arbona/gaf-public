package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.cred.beans.BeanDesembolsoParcial;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;

public class DesembolsosParcialesProcess implements IProcess {

	private String forward;
	private String accion;
	private Double porcentaje1;
	private Double porcentaje2;
	private List<BeanDesembolsoParcial> desembolsos;
	private boolean estadosModificados;

	@Override
	public boolean doProcess() {
		if (accion == null) {
			forward = "desembolsosParciales";
		} else if (accion.equals("listar")) {
			listar();
			forward = "desembolsosParciales";
		} else if (accion.equalsIgnoreCase("cambiarEstado")) {
			cambiarEstado();
			forward = "desembolsosParciales";
		}
		return true;
	}

	// Metodos privados
	private void cambiarEstado() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		HashSet<String> seleccionados = new HashSet<String>();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("desembolso")) {
				String[] p = param.split("-");
				seleccionados.add(new String(p[1]));
			}
		}
		for (String numAt : seleccionados) {
			Objetoi credito = (Objetoi) bp.createQuery("Select o from Objetoi o where o.numeroAtencion=:numero")
					.setParameter("numero", new Long(numAt)).uniqueResult();
			nuevoEstado(credito, bp);
		}
	}

	// Este metodo le asigna la fechaHasta al estado actual y le setea un nuevo
	// estado "PENDIENTE SEGUNDO DESEBOLSO"
	private void nuevoEstado(Objetoi credito, BusinessPersistance bp) {
		ObjetoiEstado estadoAntiguo = (ObjetoiEstado) bp
				.createQuery("Select oe from ObjetoiEstado oe where oe.fechaHasta is NULL AND oe.objetoi=:credito")
				.setParameter("credito", credito).uniqueResult();
		estadoAntiguo.setFechaHasta(new Date());
		bp.update(estadoAntiguo);
		Estado estado = (Estado) bp.createQuery("Select e from Estado e where e.nombreEstado=:nomb")
				.setParameter("nomb", "PENDIENTE SEGUNDO DESEMBOLSO").uniqueResult();
		ObjetoiEstado oe = new ObjetoiEstado();
		oe.setEstado(estado);
		oe.setFecha(new Date());
		oe.setObjetoi(credito);
		bp.save(oe);
	}

	private void listar() {
		desembolsos = new ArrayList<BeanDesembolsoParcial>();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		List<Objetoi> objetos = bp.createQuery(
				"SELECT credito FROM ObjetoiEstado oe JOIN oe.objetoi credito JOIN oe.estado estado JOIN credito.linea l WHERE estado.nombreEstado = 'EJECUCION' "
						+ " AND EXISTS (SELECT 1 FROM Desembolso des WHERE des.credito = oe.objetoi AND des.numero = 1 AND des.fechaReal IS NOT NULL AND des.fechaSolicitud IS NOT NULL) "
						+ " AND EXISTS (SELECT 1 FROM Desembolso des WHERE des.credito = oe.objetoi AND des.numero = 2 AND des.fechaReal IS NULL AND des.fechaSolicitud IS NULL) AND oe.fechaHasta IS NULL AND credito.autorizadoQQManual = 1")
				.list();

		double financiado = 0, ingresadoTotal = 0, ingresadoBodega = 0, porcentaje = 0;
		for (Iterator<Objetoi> iterator = objetos.iterator(); iterator.hasNext();) {
			Objetoi objetoi = (Objetoi) iterator.next();
			if (objetoi.getQqFinal() == null) {
				objetoi.setQqFinal(0.0);
			}
			financiado = objetoi.getQqFinal();
			ingresadoTotal = objetoi.getQqIngresadosTotales();
			ingresadoBodega = objetoi.getQqIngresadosGarantia();
			porcentaje = (ingresadoBodega / financiado) * 100;

			if (porcentaje >= porcentaje1 && porcentaje <= porcentaje2) {
				BeanDesembolsoParcial bean = new BeanDesembolsoParcial();
				bean.setNumeroAtencion(new BigDecimal(objetoi.getNumeroAtencion()));
				bean.setExpediente(objetoi.getExpediente());
				bean.setNombre(objetoi.getPersona().getNomb12());
				bean.setQqFinal(financiado);
				bean.setPorcentaje(porcentaje);
				desembolsos.add(bean);
			}

		}

		// List<Object[]> result;
		// result = bp
		// .createSQLQuery("SELECT o.numeroAtencion, o.expediente, p.nomb_12,
		// o.qqFinal as qqAprobados, SUM(ov.totalQQIngresados)/o.qqFinal*100 as
		// porcentajeQQIngresado "
		// + "FROM ObjetoiVinedo ov INNER JOIN Objetoi o ON ov.credito_id = o.id
		// INNER JOIN ObjetoiEstado oe ON oe.objetoi_id = o.id "
		// + "INNER JOIN estado e ON oe.estado_idEstado = e.idEstado INNER JOIN
		// persona p ON o.persona_IDPERSONA = p.idPersona "
		// + "WHERE e.nombreEstado = 'PRIMER DESEMBOLSO' AND oe.fechaHasta IS
		// NULL GROUP BY o.expediente, o.numeroAtencion, p.nomb_12, o.qqFinal,
		// o.id "
		// + "HAVING SUM(ov.totalQQIngresados)/o.qqFinal*100 >= :par1 AND
		// SUM(ov.totalQQIngresados)/o.qqFinal*100 <= :par2")
		// .setParameter("par1", porcentaje1).setParameter("par2",
		// porcentaje2).list();

		// for (Object[] objects : result) {
		// BeanDesembolsoParcial bean = new BeanDesembolsoParcial();
		// bean.setNumeroAtencion((BigDecimal) objects[0]);
		// bean.setExpediente((String) objects[1]);
		// bean.setNombre((String) objects[2]);
		// bean.setQqFinal((Double) objects[3]);
		// bean.setPorcentaje((Double) objects[4]);
		// desembolsos.add(bean);
		// }
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Double getPorcentaje1() {
		return porcentaje1;
	}

	public void setPorcentaje1(Double porcentaje1) {
		this.porcentaje1 = porcentaje1;
	}

	public Double getPorcentaje2() {
		return porcentaje2;
	}

	public void setPorcentaje2(Double porcentaje2) {
		this.porcentaje2 = porcentaje2;
	}

	public List<BeanDesembolsoParcial> getDesembolsos() {
		return desembolsos;
	}

	public void setDesembolsos(List<BeanDesembolsoParcial> desembolsos) {
		this.desembolsos = desembolsos;
	}

	public boolean isEstadosModificados() {
		return estadosModificados;
	}

	public void setEstadosModificados(boolean estadosModificados) {
		this.estadosModificados = estadosModificados;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getPorcentaje1Str() {
		if (porcentaje1 == null) {
			return String.format("%.2f%%", 0.0);
		} else {
			return String.format("%.2f%%", porcentaje1);
		}
	}

	public void setPorcentaje1Str(String porcentaje) {
		setPorcentaje1(new Double(porcentaje.replace("%", "").replace(",", ".")));
	}

	public String getPorcentaje2Str() {
		if (porcentaje1 == null) {
			return String.format("%.2f%%", 0.0);
		} else {
			return String.format("%.2f%%", porcentaje2);
		}
	}

	public void setPorcentaje2Str(String porcentaje) {
		setPorcentaje2(new Double(porcentaje.replace("%", "").replace(",", ".")));
	}

}
