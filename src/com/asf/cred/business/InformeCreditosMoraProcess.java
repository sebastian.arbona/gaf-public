package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SecurityFilter;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanNotificacion;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.informeCreditosMora;
import com.nirven.creditos.hibernate.informeDetalleCreditosMora;

public class InformeCreditosMoraProcess extends ConversationProcess {

	private List<Linea> lineas;
	private List<informeCreditosMora> informeCredito;
	private informeCreditosMora informeUnCredito;
	private List<informeDetalleCreditosMora> detalleCreditoMora;
	private String[] idsLineas;
	private String codigo;
	private Long numeroAtencion;
	private Date fechaMora;
	private Date fechaMoraDesde;
	private Date fechaMoraHasta;
	private Date fechaProceso;
	private Long idPersona;
	private List<Objetoi> creditos;
	private List<BeanNotificacion> creditosBean;
	private List<AuditoriaFinal> auditoriaFinal;
	private String auditoriaFinalPos;
	private Long idInforme;

	public InformeCreditosMoraProcess() {
		fechaMoraHasta = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -2);
		fechaMoraDesde = c.getTime();
		forward = "informeCreditosMora";
		if (forward == "") {
			forward = "informeCreditosMora";

		}
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		if (fechaMoraDesde == null || fechaMoraHasta == null) {
			errors.put("informedeuda.fechas", "informedeuda.fechas");
			return false;
		}
		SimpleDateFormat FT = new SimpleDateFormat("dd/MM/yyyy");
		String fechaBusquedaDesde = FT.format(fechaMoraDesde).toString();
		String fechaBusquedahasta = FT.format(fechaMoraHasta).toString();
		// inserto datos en la tabla para mostrar nada mas
		// Necesito insertar los id del credito para despues mostrarlos por pantalla
		// recorriendo el

		informeCredito = bp.createQuery(
				"select i from informeCreditosMora i where i.fechaDesde <= :fechaHasta and i.fechaDesde >= :fechaDesde "
						+ "order by i.fechaDesde desc")
				.setParameter("fechaHasta", DateHelper.getDate(fechaBusquedahasta))
				.setParameter("fechaDesde", DateHelper.getDate(fechaBusquedaDesde)).list();
		forward = "informeCreditosMora";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean nuevoInforme() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lineas = bp.getByFilter("select l from Linea l order by l.nombre");
		forward = "NotificacionesInformeCreditosMora";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean generar() {

		java.util.Date fecha = new Date();
		SimpleDateFormat FT = new SimpleDateFormat("dd/MM/yyyy");
		String fechaProceso = FT.format(fecha).toString();
		String fechaDesde = FT.format(fechaMoraDesde).toString();
		String fechaHasta = FT.format(fechaMoraHasta).toString();

		SecurityFilter datos = SecurityFilter.getSecurityFilterInstance();
		String usuario = datos.getConfig().getInitParameter("LiquidadorTask.usr");
		String password = datos.getConfig().getInitParameter("LiquidadorTask.pass");

		// Filtros para la consulta
		String estadosWhere = "('EJECUCION', 'PRIMER DESEMBOLSO', 'PENDIENTE SEGUNDO DESEMBOLSO')";

		// String usr = DirectorHelper.getString("InformeDeuda.usuario");
		// String pass = DirectorHelper.getString("InformeDeuda.password");
		ExecutorService threadPool = (ExecutorService) com.asf.security.SessionHandler.getCurrentSessionHandler()
				.getRequest().getSession().getServletContext().getAttribute("InformeDeudaThreadPool");
		InformeCreditosMoraTask task = new InformeCreditosMoraTask(fechaMora, estadosWhere, numeroAtencion, codigo,
				idPersona, idsLineas, fechaProceso, fechaDesde, fechaHasta, usuario, password, threadPool);
		threadPool.submit(task);
		forward = "informeCreditosMora";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean verInforme() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (idInforme == null) {
			return false;
		}
		informeUnCredito = (informeCreditosMora) bp.getById(informeCreditosMora.class, idInforme);
		detalleCreditoMora = informeUnCredito.getDetallesCreditoMora();
		forward = "informeDetalleCreditosMora";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean volver() {
		forward = "informeCreditosMora";
		return true;
	}

	public informeCreditosMora getInformeUnCredito() {
		return informeUnCredito;
	}

	public void setInformeUnCredito(informeCreditosMora informeUnCredito) {
		this.informeUnCredito = informeUnCredito;
	}

	public Long getIdInforme() {
		return idInforme;
	}

	public void setIdInforme(Long idInforme) {
		this.idInforme = idInforme;
	}

	@Override
	protected String getDefaultForward() {
		return "informeCreditosMora";
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long idObjetoi) {
		this.numeroAtencion = idObjetoi;
	}

	public String[] getIdsLineas() {
		return idsLineas;
	}

	public void setIdsLineas(String[] idsLineas) {
		this.idsLineas = idsLineas;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public void setFechaMora(String fechaMora) {
		this.fechaMora = DateHelper.getDate(fechaMora);
	}

	public String getFechaMora() {
		return DateHelper.getString(fechaMora);
	}

	public String getFechaMoraDesde() {
		return DateHelper.getString(fechaMoraDesde);
	}

	public void setFechaMoraDesde(String fechaMoraDesde) {
		this.fechaMoraDesde = DateHelper.getDate(fechaMoraDesde);
	}

	public String getFechaMoraHasta() {
		return DateHelper.getString(fechaMoraHasta);
	}

	public void setFechaMoraHasta(String fechaMoraHasta) {
		this.fechaMoraHasta = DateHelper.getDate(fechaMoraHasta);
	}

	public String getFechaProceso() {
		return DateHelper.getString(fechaProceso);
	}

	public void setFechaProcesoa(String fechaProceso) {
		this.fechaProceso = DateHelper.getDate(fechaProceso);
	}

	public List<informeCreditosMora> getInformeCredito() {
		return informeCredito;
	}

	public void setInformeCredito(List<informeCreditosMora> informeCredito) {
		this.informeCredito = informeCredito;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public List<BeanNotificacion> getCreditosBean() {
		return creditosBean;
	}

	public void setCreditosBean(List<BeanNotificacion> creditosBean) {
		this.creditosBean = creditosBean;
	}

	public List<AuditoriaFinal> getAuditoriaFinal() {
		return auditoriaFinal;
	}

	public void setAuditoriaFinal(List<AuditoriaFinal> auditoriaFinal) {
		this.auditoriaFinal = auditoriaFinal;
	}

	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}

	public List<informeDetalleCreditosMora> getDetalleCreditoMora() {
		return detalleCreditoMora;
	}

	public void setDetalleCreditoMora(List<informeDetalleCreditosMora> detalleCreditoMora) {
		this.detalleCreditoMora = detalleCreditoMora;
	}

	// =========================VALIDACIONES==================================
	public boolean validate() {
		return this.errors.isEmpty();
	}

}
