package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.asf.util.TipificadorHelper;
import com.nirven.creditos.hibernate.ObjetoiArchivo;

public class BusquedaArchivosProcess extends ConversationProcess {

	private Long numeroAtencion;
	private Long idTipo;
	private Date desde;
	private Date hasta;
	private String detalle;
	private String origen;

	private List<ObjetoiArchivo> archivos;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean listar() {
		if (numeroAtencion != null && numeroAtencion == 0L)
			numeroAtencion = null;

		if (idTipo != null && idTipo == 0L)
			idTipo = null;

		archivos = bp.createQuery("select a from ObjetoiArchivo a left join a.credito c left join a.garantia g where "
				+ "(:numero is null or c.numeroAtencion = :numero "
				+ "or g.id in (select og.garantia.id from ObjetoiGarantia og where og.objetoi.numeroAtencion = :numero)) "
				+ "and (:idTipo is null or a.tipo.id = :idTipo) " + "and (:desde is null or a.fechaAdjunto >= :desde) "
				+ "and (:hasta is null or a.fechaAdjunto < :hasta) "
				+ "and (:detalle is null or a.detalle like :detalle) " + "and (:origen is null or a.origen=:origen) "
				+ "order by a.fechaAdjunto asc").setParameter("numero", numeroAtencion).setParameter("idTipo", idTipo)
				.setParameter("desde", desde != null ? DateHelper.resetHoraToZero(desde) : null)
				.setParameter("hasta",
						hasta != null ? DateHelper.resetHoraToZero(DateHelper.add(hasta, 1, Calendar.DATE)) : null)
				.setParameter("detalle", "%" + detalle + "%")
				.setParameter("origen", origen == null || origen.isEmpty() ? null
						: TipificadorHelper.getString("requisito.familia", origen))
				.list();

		return true;
	}

	public List<ObjetoiArchivo> getArchivos() {
		return archivos;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}

	public String getDesde() {
		return DateHelper.getString(desde);
	}

	public void setDesde(String desde) {
		this.desde = DateHelper.getDate(desde);
	}

	public String getHasta() {
		return DateHelper.getString(hasta);
	}

	public void setHasta(String hasta) {
		this.hasta = DateHelper.getDate(hasta);
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	@Override
	protected String getDefaultForward() {
		return "BusquedaArchivosProcess";
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

}
