package com.asf.cred.business;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.transform.Transformers;

import com.asf.gaf.DirectorHandler;
import com.asf.gaf.business.pagos.GastosDesembolsoDto;
import com.asf.gaf.hibernate.Moneda;
import com.asf.security.BusinessPersistance;
import com.asf.security.ldap.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Emideta;
import com.nirven.creditos.hibernate.Emision;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;

public class Liquidacion implements IProcess {

	private static final String PROGRESO_KEY = "liquidacion.progreso";

	// =========================ATRIBUTOS===========================
	private static Log log = LogFactory.getLog(Liquidacion.class);
	private BusinessPersistance bp = null;
	private String forward = "LiquidacionResult", input = "Liquidacion";
	private HashMap<Object, Object> errores;
	private String accion = "";

	private Boolean liquidacionOK;
	private Emision emision;
	private Integer liquidaPor;
	private String objLiquidar;
	private Long idCredito;
	private Long idTipoLinea;
	private Long idTipoLineaDesde;
	private Long idTipoLineaHasta;
	private String fechaDesde;
	private String fechaHasta;
	private Long idCreditoDesde;
	private Long idCreditoHasta;
	private Long idMoneda;
	private Long nroAtencion;
	private Long idLineaEmergencia;
	private String ultimaActualizacion;
	private String ultimaActualizacionInicio;
	private String ultimaActualizacionFin;
	private String ultimaRecaudacion;
	private String ultimaCobranza;

	private List<Estado> estados;
	private String[] seleccion;
	private String fecovitaTipo;

	// ===================== CONSTRUCTOR =====================
	@SuppressWarnings("unchecked")
	public Liquidacion() {
		this.errores = new HashMap<Object, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.emision = new Emision();
		List<Moneda> m = bp.createQuery("SELECT m FROM Moneda m WHERE m.abreviatura like :peso")
				.setParameter("peso", "Peso").list();
		if (m.size() > 0) {
			idMoneda = m.get(0).getId();
		}
		idLineaEmergencia = new Long(DirectorHelper.getString(Linea.LINEA_EMERGENCIA));

		try {
			setUltimaActualizacion(DirectorHandler.getDirector("comportamientoPago.ultimaActualizacion").getValor());
			setUltimaActualizacionInicio(
					DirectorHandler.getDirector("comportamientoPago.ultimaActualizacionInicio").getValor());
			setUltimaActualizacionFin(DirectorHandler.getDirector("comportamientoPago.ultimaActualizacion").getValor());
			if (ultimaActualizacionInicio != null && !ultimaActualizacionInicio.isEmpty()
					&& ultimaActualizacionFin != null && !ultimaActualizacionFin.isEmpty()) {
				Date i, f;
				DateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
				i = s.parse(ultimaActualizacionInicio);
				f = s.parse(ultimaActualizacionFin);
				if (f.before(i)) {
					ultimaActualizacionFin = "EN PROCESO";
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		Caratula c = (Caratula) bp
				.createQuery("select c from Caratula c where c.actualizada = 1 ORDER BY c.fechaEnvio DESC")
				.setMaxResults(1).uniqueResult();

		if (c != null) {
			setUltimaRecaudacion(c.getFechaEnvioStr());
			setUltimaCobranza(c.getFechaCobranzaStr());
		}
		String ids = DirectorHelper.getString("liquidacion.estado");
		estados = bp.createQuery("select e from Estado e where id in (" + ids + ") order by e.nombreEstado").list();
	}

	// ======================FUNCIONALIDADES========================
	@Override
	public boolean doProcess() {
		ProgressStatus.onProcess = true;
		ProgressStatus.iTotal = 1;
		ProgressStatus.iProgress = 0;
		ProgressStatus.abort = false;

		if (this.errores.isEmpty()) {
			log.info("Proceso de Liquidacion: Iniciando la liquidaci�n");
			ProgressStatus.sStatus = "Iniciando la liquidaci�n";
			ProgressStatus.abort = false;
			int interval = SessionHandler.getCurrentSessionHandler().getRequest().getSession().getMaxInactiveInterval();
			try {
				log.info("Proceso de Liquidacion: Grabar emision");
				try {
					Query qEmision = bp.getNamedQuery("Emision.findByFecha");
					qEmision.setParameter("fechaEmision", Calendar.getInstance().getTime());
					Emision ePrevia = (Emision) qEmision.list().get(0);
					emision = new Emision(ePrevia.getFechaEmision(), ePrevia.getOrdenEmision() + 1,
							this.getFechaHasta());
				} catch (Exception e) {
					log.info("Proceso de Liquidacion: No existe emision previa");
					emision = new Emision(Calendar.getInstance().getTime(), new Integer(1), this.getFechaHasta());
				}
				bp.saveOrUpdate(emision);
				// this.bp.getCurrentSession().flush();

				this.bp.getCurrentSession().setFlushMode(FlushMode.ALWAYS);
				this.bp.getCurrentSession().setCacheMode(CacheMode.IGNORE);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setMaxInactiveInterval(-1);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute(PROGRESO_KEY, 0);
				long l = System.currentTimeMillis();
				switch (this.liquidaPor.intValue()) {
				case 1: // linea de credito
					emision.setCriterio("L�nea");
					this.liquidacionOK = liquidarLinea();
					break;
				case 2: // credito
					emision.setCriterio("Cr�dito");
					this.liquidacionOK = liquidarCredito();
					break;
				case 3: // rango de fechas
					emision.setCriterio("Rango Fechas");
					this.liquidacionOK = liquidarRangoFechas();
					break;
				case 4: // credito
					buscarIdCredito();
					emision.setCriterio("Cr�dito");
					this.liquidacionOK = liquidarCredito();
					break;
				case 5: // rango de lineas
					emision.setCriterio("Rango L�neas");
					this.liquidacionOK = liquidarRangoLineas();
					break;
				case 6:// Moneda
					emision.setCriterio("Moneda");
					this.liquidacionOK = liquidarMoneda();
					break;
				case 7:// fecovita
					emision.setCriterio(fecovitaTipo);
					liquidacionOK = liquidarFecovita();
					break;
				}
				this.errores.put(new Long(this.errores.size()).toString(),
						"Tiempo aproximado: " + (System.currentTimeMillis() - l) / 1000 + "");
				if (liquidacionOK) {
					this.errores.put(new Long(this.errores.size()).toString(), "El c�lculo finaliz� correctamente");
				} else {
					ProgressStatus.abort = true;
					this.errores.put(new Long(this.errores.size()).toString(), "La liquidaci�n NO se pudo realizar.");
				}
				log.info("Proceso de Liquidacion: Tiempo: " + (System.currentTimeMillis() - l) / 1000);
			} catch (Exception ex) {
				log.warn(ex.getMessage(), ex);
				this.errores.put(new Long(this.errores.size()).toString(), ex.getMessage());
				this.errores.put(new Long(this.errores.size()).toString(), "La liquidaci�n NO finaliz� correctamente");
				ProgressStatus.abort = true;
			} finally {
				this.bp.getCurrentSession().setFlushMode(FlushMode.AUTO);
				this.bp.getCurrentSession().setCacheMode(CacheMode.NORMAL);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setMaxInactiveInterval(interval);
				SessionHandler.getCurrentSessionHandler().getRequest().setAttribute("errores", errores);
			}
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setMaxInactiveInterval(-1);
		} else {
			this.errores.put(new Long(this.errores.size()).toString(),
					"Para realizar la liquidaci�n, debe poner 'si' en CONFIRMA");
		}
		this.emision.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		this.emision.setFechaHasta(this.fechaHasta);
		this.bp.saveOrUpdate(emision);
		log.info("Proceso de Liquidacion: Commit Transaction grabar emision");
		ProgressStatus.onProcess = false;
		return true;
	}

	public void buscarIdCredito() {
		try {
			if (nroAtencion == null) {
				idCredito = null;
				return;
			}
			Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
					.setLong("nroAtencion", nroAtencion).setMaxResults(1).uniqueResult();
			if (credito == null) {
				idCredito = null;
			} else {
				idCredito = credito.getId();
			}
		} catch (NoResultException e) {
			idCredito = null;
		}
	}

	@Override
	public HashMap<Object, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.input;
	}

	@Override
	public Object getResult() {
		return null;
	}

	// =====================UTILIDADES PRIVADAS=====================
	private Boolean liquidarLinea() {
		return null;
	}

	private Boolean liquidarCredito() {
		boolean liquidaOK = false;
		this.bp.begin();
		try {
			if (nroAtencion == null || nroAtencion == 0) {
				this.errores.put("liquidacion.credito", "Debe seleccionar el cr�dito.");
				log.error("Liquidar Credito - No hay credito seleccionado.");
				liquidaOK = false;
			} else if (fechaHasta == null || fechaHasta.isEmpty()) {
				this.errores.put("liquidacion.fecha", "Debe ingresar la fecha l�mite.");
				log.error("Liquidar Credito - No ingreso fecha.");
				liquidaOK = false;
			} else if (validarEstado()) {
				this.errores.put("liquidacion.estado",
						"El estado del cr\u00E9dito no permite hacer la liquidaci\u00F3n.");
				log.error("Liquidar Credito - El estado no permite liquidar.");
				liquidaOK = false;
			} else {
				Date hasta = DateHelper.getDate(fechaHasta);
				Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
						.setParameter("nroAtencion", nroAtencion).uniqueResult();
				this.emision.setValorDesde(nroAtencion.toString());
				this.emision.setValorHasta(nroAtencion.toString());
				Moneda m = (Moneda) bp.createQuery("SELECT m FROM Moneda m WHERE m.idMoneda = :idMoneda")
						.setParameter("idMoneda", idMoneda).uniqueResult();
				this.emision.setMoneda(m.getDenominacion());
				CreditoHandler handler = new CreditoHandler();
				Estado estado = handler.findEstado(credito);
				if (estado != null && estado.getTipo().equals("C")) { // Cancelado
					this.errores.put("liquidacion.estado",
							"El estado del cr\u00E9dito no permite hacer la liquidaci\u00F3n.");
					log.error("Liquidar Credito - Estado de credito incorrecto");
					liquidaOK = false;
				} else {
					ProgressStatus.sStatus = "Credito actual: " + credito.getNumeroAtencionStr();
					this.bp.getCurrentSession().flush();
					this.bp.getCurrentSession().clear();
					log.info("Proceso de Liquidacion: Credito: " + credito.getId());
					// buscar la menor y la mayor cuota para liquidar
					Object[] valores = (Object[]) bp.getNamedQuery("Cuota.findByRangoFechas")
							.setLong("idCredito", idCredito).setDate("fechaHasta", hasta).uniqueResult();
					if (valores != null && valores[0] != null && valores[1] != null) {
						Integer minCuota = (Integer) valores[0];
						Integer maxCuota = (Integer) valores[1];
						ProgressStatus.iProgress += 0.5;
						liquidaOK = credito.liquidar(this.bp, log, this.emision, minCuota, maxCuota);
						log.info("Proceso de Liquidacion: Final Credito: " + credito.getId());
						// this.bp.update(credito);
						if (liquidaOK) {
							liquidarGastosRecuperar(credito.getId());
							this.bp.commit();
						} else {
							this.bp.rollback();
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			liquidaOK = false;
			this.bp.rollback();
			this.errores.put(new Long(this.errores.size()).toString(),
					"No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
			log.error("Liquidar Credito - No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
		}
		if (liquidaOK) {
			ProgressStatus.iProgress += 1;
		} else {
			ProgressStatus.abort = true;
		}
		return liquidaOK;
	}

	@SuppressWarnings({ "rawtypes" })
	private boolean liquidarRangoFechas() {
		boolean liquidaOK = false;
		this.bp.begin();

		try {
			if (fechaHasta == null || fechaHasta.isEmpty()) {
				this.errores.put("liquidacion.fecha", "liquidacion.fecha");
				liquidaOK = false;
			} else {
				this.emision.setFechaHasta(fechaHasta);
				Date hasta = DateHelper.getDate(fechaHasta);
				CreditoHandler handler = new CreditoHandler();
				List creditos;
				if (idMoneda != null) {
					creditos = bp.getNamedQuery("Objetoi.findByVencimientoLiquidacionMoneda")
							.setDate("fechaHasta", hasta).setLong("idMoneda", idMoneda).list();
				} else {
					creditos = bp.getNamedQuery("Objetoi.findByVencimientoLiquidacion").setDate("fechaHasta", hasta)
							.list();
				}

				Moneda m = (Moneda) bp.createQuery("SELECT m FROM Moneda m WHERE m.idMoneda = :idMoneda")
						.setParameter("idMoneda", idMoneda).uniqueResult();
				this.emision.setMoneda(m.getDenominacion());

				ProgressStatus.iTotal = creditos.size();
				if (creditos.isEmpty()) {
					ProgressStatus.iTotal = 1;
					ProgressStatus.iProgress = 1;
				}

				for (Object fila : creditos) {
					Object[] valores = (Object[]) fila;
					Long idCredito = (Long) valores[0];
					Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);

					Estado estado = handler.findEstado(credito);
					if (estado == null || !estado.getTipo().equals("C")) {
						// NO Cancelado
						Integer minCuota = (Integer) valores[1];
						Integer maxCuota = (Integer) valores[2];
						ProgressStatus.sStatus = "Credito actual: " + credito.getNumeroAtencionStr();
						// this.bp.getCurrentSession().flush();
						this.bp.getCurrentSession().clear();
						log.info("Proceso de Liquidacion: Credito: " + credito.getId());
						liquidaOK = credito.liquidar(this.bp, log, this.emision, minCuota.intValue(),
								maxCuota.intValue());
						log.info("Proceso de Liquidacion: Final Credito: " + credito.getId());
						// this.bp.update(credito);
						if (liquidaOK) {
							liquidarGastosRecuperar(credito.getId());
							this.bp.commit();
						} else {
							this.bp.rollback();
						}
					}

					ProgressStatus.iProgress = ProgressStatus.iProgress + 1;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			liquidaOK = false;
			this.bp.rollback();
			this.errores.put(new Long(this.errores.size()).toString(),
					"No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
			log.error("Liquidar Credito - No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
		}
		return liquidaOK;
	}

	@SuppressWarnings({ "rawtypes" })
	private boolean liquidarRangoLineas() {
		boolean liquidaOK = false;
		this.bp.begin();
		try {
			if (idTipoLineaDesde == null || idTipoLineaHasta == null || idTipoLineaDesde == 0
					|| idTipoLineaHasta == 0) {
				this.errores.put("liquidacion.rangoLinea", "liquidacion.rangoLinea");
				liquidaOK = false;
			} else if (fechaHasta == null || fechaHasta.isEmpty()) {
				this.errores.put("liquidacion.fecha", "liquidacion.fecha");
				liquidaOK = false;
			} else {
				Date hasta = DateHelper.getDate(fechaHasta);

				CreditoHandler handler = new CreditoHandler();
				Linea l = (Linea) bp.createQuery("SELECT l FROM Linea l WHERE l.id = :linea")
						.setParameter("linea", idTipoLineaDesde).uniqueResult();
				Linea lh = (Linea) bp.createQuery("SELECT l FROM Linea l WHERE l.id = :linea")
						.setParameter("linea", idTipoLineaHasta).uniqueResult();

				this.emision.setValorDesde(l.getNombre());
				this.emision.setValorHasta(lh.getNombre());
				List creditos;

				if (idMoneda != null) {
					creditos = bp.getNamedQuery("Objetoi.findByRangoLineasMoneda")
							.setLong("idLineaDesde", idTipoLineaDesde).setLong("idLineaHasta", idTipoLineaHasta)
							.setDate("fechaHasta", hasta).setLong("idMoneda", idMoneda).list();
				} else {
					creditos = bp.getNamedQuery("Objetoi.findByRangoLineas").setLong("idLineaDesde", idTipoLineaDesde)
							.setLong("idLineaHasta", idTipoLineaHasta).setDate("fechaHasta", hasta).list();
				}

				Moneda m = (Moneda) bp.createQuery("SELECT m FROM Moneda m WHERE m.idMoneda = :idMoneda")
						.setParameter("idMoneda", idMoneda).uniqueResult();
				this.emision.setMoneda(m.getDenominacion());

				ProgressStatus.iTotal = creditos.size();
				if (creditos.isEmpty()) {
					ProgressStatus.iTotal = 1;
					ProgressStatus.iProgress = 1;
				}

				for (Object fila : creditos) {

					Object[] valores = (Object[]) fila;
					Long idCredito = (Long) valores[0];
					Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
					if (validarEstado(credito)) {
						if (errores.get("liquidacion.rangoLineasEstado") == null) {
							errores.put("liquidacion.rangoLineasEstado",
									"No se liquid�/liquidaron el/los cr�dito/s: " + credito.getNumeroAtencion());
						} else {
							String msj = (String) errores.get("liquidacion.rangoLineasEstado");
							msj += ", " + credito.getNumeroAtencion();
							errores.put("liquidacion.rangoLineasEstado", msj);
						}
					} else {
						Estado estado = handler.findEstado(credito);
						/**
						 * NO Cancelado
						 */
						if (estado == null || !estado.getTipo().equals("C")) {
							Integer minCuota = (Integer) valores[1];
							Integer maxCuota = (Integer) valores[2];

							ProgressStatus.sStatus = "Credito actual: " + credito.getNumeroAtencionStr();
							this.bp.getCurrentSession().flush();
							this.bp.getCurrentSession().clear();
							log.info("Proceso de Liquidacion: Credito: " + credito.getId());
							liquidaOK = credito.liquidar(this.bp, log, this.emision, minCuota, maxCuota);
							log.info("Proceso de Liquidacion: Final Credito: " + credito.getId());
							if (liquidaOK) {
								liquidarGastosRecuperar(credito.getId());
								this.bp.commit();
							} else {
								this.bp.rollback();
							}
						}
					}
					ProgressStatus.iProgress = ProgressStatus.iProgress + 1;
				}
				if (errores.get("liquidacion.rangoLineasEstado") != null) {
					String msj = (String) errores.get("liquidacion.rangoLineasEstado");
					msj += ". Por que su/s estado/s no lo permite/n.";
					errores.put("liquidacion.rangoLineasEstado", msj);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			liquidaOK = false;
			this.bp.rollback();
			this.errores.put(new Long(this.errores.size()).toString(),
					"No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
			log.error("Liquidar Credito - No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
		}
		return liquidaOK;
	}

	/*
	 * Buscamos la emideta del menor numero de cuota de cada objetoi y lo asociamos
	 * al detalleFactura si existe.
	 */
	@SuppressWarnings("unchecked")
	public void liquidarGastosRecuperar(Long creditoId) {
		List<Emideta> emidetas = bp.createQuery(
				"SELECT e FROM Emideta e WHERE e.emision.id = :emisionId AND e.credito.id = :creditoId ORDER BY e.credito, e.numero")
				.setParameter("emisionId", this.emision.getId()).setParameter("creditoId", creditoId).list();
		Objetoi ultimoCredito = null;
		for (Emideta emideta : emidetas) {
			if (ultimoCredito == null || ultimoCredito.getId().longValue() != emideta.getCredito_id().longValue()) {
				asociarDetalleFactura(emideta);
				ultimoCredito = emideta.getCredito();
			} else {
				break;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void asociarDetalleFactura(Emideta emideta) {
		List<GastosDesembolsoDto> gastosDesembolso = new ArrayList<GastosDesembolsoDto>();
		/**
		 * aca ignoro emideta_id == null para permitir tomar gastos que se estan
		 * incluyendo en una liquidacion(podria estar volviendo a calcular). las
		 * novedades no deben estar vinculadas a otro desembolso
		 */
		String consulta = "select n.id,case when df.id is null then 'NovedadCtaCte' else 'DetalleFactura' end as origen,n.importe "
				+ "from NovedadCtaCte n left join DetalleFactura df on df.id = n.detalleFactura_id left join Factura f on f.id = df.factura_id "
				+ "where n.credito_id = :creditoId and n.impactado = 0 and (df.id is null or f.fechaPago is not null) and n.desembolso_id is null";
		gastosDesembolso.addAll(bp.createSQLQuery(consulta).setParameter("creditoId", emideta.getCredito_id())
				.setResultTransformer(Transformers.aliasToBean(GastosDesembolsoDto.class)).list());
		NovedadCtaCte novedad;
		for (GastosDesembolsoDto gastosDesembolsoDto : gastosDesembolso) {
			novedad = (NovedadCtaCte) bp.getById(NovedadCtaCte.class, gastosDesembolsoDto.getId().longValue());
			novedad.setEmideta(emideta);
			bp.update(novedad);
		}
	}

	@SuppressWarnings({ "rawtypes" })
	private boolean liquidarMoneda() {
		boolean liquidaOK = false;
		try {
			this.bp.begin();
			if (idMoneda == null || idMoneda == 0) {
				this.errores.put("liquidacion.moneda", "liquidacion.moneda");
				liquidaOK = false;
			} else if (fechaHasta == null || fechaHasta.isEmpty()) {
				this.errores.put("liquidacion.fecha", "liquidacion.fecha");
				liquidaOK = false;
			} else {
				Date hasta = DateHelper.getDate(fechaHasta);
				CreditoHandler handler = new CreditoHandler();
				Moneda m = (Moneda) bp.createQuery("SELECT m FROM Moneda m WHERE m.idMoneda = :idMoneda")
						.setParameter("idMoneda", idMoneda).uniqueResult();
				this.emision.setMoneda(m.getDenominacion());
				List creditos = bp.getNamedQuery("Objetoi.findCreditosLiquidacionCompleta")
						.setLong("idMoneda", idMoneda).setDate("fechaHasta", hasta)
						.setLong("linea_id", idLineaEmergencia).list();
				liquidaOK = true;
				ProgressStatus.iTotal = creditos.size();
				if (creditos.isEmpty()) {
					ProgressStatus.iTotal = 1;
					ProgressStatus.iProgress = 1;
				}

				bp.commit();

				for (Object fila : creditos) {
					Object[] valores = (Object[]) fila;
					Long idCredito = (Long) valores[0];
					Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
					if (validarEstado(credito)) {
						if (errores.get("liquidacion.rangoLineasEstado") == null) {
							errores.put("liquidacion.rangoLineasEstado",
									"No se liquid�/liquidaron el/los cr�dito/s: " + credito.getNumeroAtencion());
						} else {
							String msj = (String) errores.get("liquidacion.rangoLineasEstado");
							msj += ", " + credito.getNumeroAtencion();
							errores.put("liquidacion.rangoLineasEstado", msj);
						}
					} else {
						Estado estado = handler.findEstado(credito);
						if (estado == null || !estado.getTipo().equals("C")) { // NO
																				// Cancelado
							Integer minCuota = (Integer) valores[1];
							Integer maxCuota = (Integer) valores[2];

							ProgressStatus.sStatus = "Credito actual: " + credito.getNumeroAtencionStr();
							this.bp.getCurrentSession().flush();
							this.bp.getCurrentSession().clear();
							this.bp.begin();
							this.bp.getCurrentSession().refresh(this.emision);
							log.info("Proceso de Liquidacion: Credito: " + credito.getId());
							liquidaOK = credito.liquidar(this.bp, log, this.emision, minCuota, maxCuota);
							log.info("Proceso de Liquidacion: Final Credito: " + credito.getId());
							// this.bp.update(credito);
							if (liquidaOK) {
								liquidarGastosRecuperar(credito.getId());
								this.bp.commit();
							} else {
								this.bp.rollback();
							}
						}
					}

					ProgressStatus.iProgress = ProgressStatus.iProgress + 1;
				}
				if (errores.get("liquidacion.rangoLineasEstado") != null) {
					String msj = (String) errores.get("liquidacion.rangoLineasEstado");
					msj += ". Por que su/s estado/s no lo permite/n.";
					errores.put("liquidacion.rangoLineasEstado", msj);
				}
				return liquidaOK;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			liquidaOK = false;
			this.errores.put(new Long(this.errores.size()).toString(),
					"No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
			log.error("Liquidar Credito - No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
		}
		return liquidaOK;
	}

	@SuppressWarnings("unchecked")
	public boolean liquidarFecovita() {
		boolean liquidaOK = false;
		this.bp.begin();
		try {
			if (fechaHasta == null || fechaHasta.isEmpty()) {
				this.errores.put("liquidacion.fecha", "liquidacion.fecha");
				liquidaOK = false;
			} else {
				Date hasta = DateHelper.getDate(fechaHasta);
				CreditoHandler handler = new CreditoHandler();
				Moneda m = (Moneda) bp.createQuery("SELECT m FROM Moneda m WHERE m.idMoneda = :idMoneda")
						.setParameter("idMoneda", idMoneda).uniqueResult();
				this.emision.setMoneda(m.getDenominacion());
				List<Object> creditos = bp.getNamedQuery("Objetoi.findByRangoFecovita").setDate("fechaHasta", hasta)
						.setString("fecovitaTipo", fecovitaTipo).list();
				liquidaOK = true;
				ProgressStatus.iTotal = creditos.size();
				if (creditos.isEmpty()) {
					ProgressStatus.iTotal = 1;
					ProgressStatus.iProgress = 1;
				}
				for (Object fila : creditos) {
					Object[] valores = (Object[]) fila;
					Long idCredito = (Long) valores[0];
					Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
					if (validarEstado(credito)) {
						if (errores.get("liquidacion.rangoLineasEstado") == null) {
							errores.put("liquidacion.rangoLineasEstado",
									"No se liquid�/liquidaron el/los cr�dito/s: " + credito.getNumeroAtencion());
						} else {
							String msj = (String) errores.get("liquidacion.rangoLineasEstado");
							msj += ", " + credito.getNumeroAtencion();
							errores.put("liquidacion.rangoLineasEstado", msj);
						}
					} else {
						Estado estado = handler.findEstado(credito);
						if (estado == null || !estado.getTipo().equals("C")) {
							// NO Cancelado
							Integer minCuota = (Integer) valores[1];
							Integer maxCuota = (Integer) valores[2];
							ProgressStatus.sStatus = "Credito actual: " + credito.getNumeroAtencionStr();
							this.bp.getCurrentSession().flush();
							this.bp.getCurrentSession().clear();
							log.info("Proceso de Liquidacion: Credito: " + credito.getId());
							liquidaOK = credito.liquidar(this.bp, log, this.emision, minCuota, maxCuota);
							log.info("Proceso de Liquidacion: Final Credito: " + credito.getId());
							// this.bp.update(credito);
							if (liquidaOK) {
								liquidarGastosRecuperar(credito.getId());
								this.bp.commit();
							} else {
								this.bp.rollback();
							}
						}
					}

					ProgressStatus.iProgress = ProgressStatus.iProgress + 1;
				}
				if (errores.get("liquidacion.rangoLineasEstado") != null) {
					String msj = (String) errores.get("liquidacion.rangoLineasEstado");
					msj += ". Por que su/s estado/s no lo permite/n.";
					errores.put("liquidacion.rangoLineasEstado", msj);
				}
				return liquidaOK;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			liquidaOK = false;
			this.bp.rollback();
			this.errores.put(new Long(this.errores.size()).toString(),
					"No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
			log.error("Liquidar Credito - No se pudo finalizar la liquidaci�n:\n" + ex.getMessage());
		}
		return liquidaOK;
	}

	// ======================GETTERS Y SETTERS======================
	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Integer getLiquidaPor() {
		return liquidaPor;
	}

	public void setLiquidaPor(Integer liquidaPor) {
		this.liquidaPor = liquidaPor;
	}

	public String getObjLiquidar() {
		return objLiquidar;
	}

	public void setObjLiquidar(String objLiquidar) {
		this.objLiquidar = objLiquidar;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public Long getIdTipoLinea() {
		return idTipoLinea;
	}

	public void setIdTipoLinea(Long idTipoLinea) {
		this.idTipoLinea = idTipoLinea;
	}

	public Boolean getLiquidacionOK() {
		return liquidacionOK;
	}

	public void setLiquidacionOK(Boolean liquidacionOK) {
		this.liquidacionOK = liquidacionOK;
	}

	// ========================VALIDACIONES=========================
	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

	private boolean validarEstado() {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		String nombreEstado = credito.getEstadoActual().getEstado().getNombreEstado().trim();
		if (seleccion != null) {
			for (int i = 0; i < seleccion.length; i++) {
				if (seleccion[i].equals(nombreEstado) || nombreEstado.equals("FINALIZADO")) {
					return false;
				}
			}
			if (validarContraEstadosDirector(nombreEstado)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (validarContraEstadosDirector(nombreEstado)) {
				return true;
			} else {
				return false;
			}

		}

	}

	private boolean validarContraEstadosDirector(String nombre) {
		for (int i = 0; i < estados.size(); i++) {
			if (nombre.equals(estados.get(i).getNombreEstado())) {
				return true;
			}
		}
		return false;
	}

	private boolean validarEstado(Objetoi credito) {
		String nombreEstado = credito.getEstadoActual().getEstado().getNombreEstado().trim();
		if (seleccion != null) {
			for (int i = 0; i < seleccion.length; i++) {
				if (seleccion[i].equals(nombreEstado) || nombreEstado.equals("FINALIZADO")) {
					return false;
				}
			}
			if (validarContraEstadosDirector(nombreEstado)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (validarContraEstadosDirector(nombreEstado)) {
				return true;
			} else {
				return false;
			}

		}
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Long getIdCreditoDesde() {
		return idCreditoDesde;
	}

	public void setIdCreditoDesde(Long idCreditoDesde) {
		this.idCreditoDesde = idCreditoDesde;
	}

	public Long getIdCreditoHasta() {
		return idCreditoHasta;
	}

	public void setIdCreditoHasta(Long idCreditoHasta) {
		this.idCreditoHasta = idCreditoHasta;
	}

	public Long getIdTipoLineaDesde() {
		return idTipoLineaDesde;
	}

	public void setIdTipoLineaDesde(Long idTipoLineaDesde) {
		this.idTipoLineaDesde = idTipoLineaDesde;
	}

	public Long getIdTipoLineaHasta() {
		return idTipoLineaHasta;
	}

	public void setIdTipoLineaHasta(Long idTipoLineaHasta) {
		this.idTipoLineaHasta = idTipoLineaHasta;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public Emision getEmision() {
		return emision;
	}

	public void setEmision(Emision emision) {
		this.emision = emision;
	}

	public Long getNroAtencion() {
		return nroAtencion;
	}

	public void setNroAtencion(Long nroAtencion) {
		this.nroAtencion = nroAtencion;
	}

	public Long getIdLineaEmergencia() {
		return idLineaEmergencia;
	}

	public void setIdLineaEmergencia(Long idLineaEmergencia) {
		this.idLineaEmergencia = idLineaEmergencia;
	}

	public String getUltimaActualizacion() {
		return ultimaActualizacion;
	}

	public void setUltimaActualizacion(String ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}

	public String getUltimaRecaudacion() {
		return ultimaRecaudacion;
	}

	public void setUltimaRecaudacion(String ultimaRecaudacion) {
		this.ultimaRecaudacion = ultimaRecaudacion;
	}

	public String getUltimaCobranza() {
		return ultimaCobranza;
	}

	public void setUltimaCobranza(String ultimaCobranza) {
		this.ultimaCobranza = ultimaCobranza;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public String[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String[] seleccion) {
		this.seleccion = seleccion;
	}

	public String getUltimaActualizacionInicio() {
		return ultimaActualizacionInicio;
	}

	public void setUltimaActualizacionInicio(String ultimaActualizacionInicio) {
		this.ultimaActualizacionInicio = ultimaActualizacionInicio;
	}

	public String getUltimaActualizacionFin() {
		return ultimaActualizacionFin;
	}

	public void setUltimaActualizacionFin(String ultimaActualizacionFin) {
		this.ultimaActualizacionFin = ultimaActualizacionFin;
	}

	public String getFecovitaTipo() {
		return fecovitaTipo;
	}

	public void setFecovitaTipo(String fecovitaTipo) {
		this.fecovitaTipo = fecovitaTipo;
	}

}
