package com.asf.cred.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.struts.upload.FormFile;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.IndiceValor;

public class CargaIndiceValor implements IProcess {
	private String forward = "CargaIndiceValor";
	private HashMap<String,String> errores;
	private ArrayList<Recaudacion> recaudaciones;
	private Long indice;
	private FormFile file = null;
	private Boolean deshabilitado = false;
	private String script = "";
	private String accion = "";
	private int cantidadFilas;
	private int cantidadFilasInsertadas;
	private int cantidadFilasRepetidas;
	private int cantidadFilasErroneas;
	private BusinessPersistance bp;
	
	public CargaIndiceValor() {
		this.errores = new HashMap<String, String>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}
	
	public boolean doProcess() {
		this.script = "";
		this.cantidadFilas = 0;
		this.cantidadFilasInsertadas = 0;
		this.cantidadFilasRepetidas = 0;
		this.cantidadFilasErroneas = 0;

		if(this.accion.equals("importarValores") && this.indice != null) {
			try {
				importarValores();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		if(this.accion.equals("rellenarFechas") && this.indice != null) {
			try {
				rellenarFechas();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return this.errores.isEmpty();
	}

	private boolean existeFecha(Date fecha, List<Object> indiceValor) {
		int i=0;

		while(i < indiceValor.size()) {
			Date filaIndiceValor = (Date) indiceValor.get(i);
			if(fecha.equals(filaIndiceValor))
				return true;
			i++;
		}

		return false;
	}

	private String filtrarCaracteresNoDecimales(String decimal) {
		String dec = "";
		String caracteresDecimales = ".0123456789";

		for(int i=0; i < decimal.length(); i++) {
			String caracterActual = Character.toString(decimal.charAt(i));
			
			if(caracteresDecimales.contains(caracterActual)) {
				dec += decimal.charAt(i);

				//El decimal s�lo debe tener un caracter '.'
				if(caracterActual.contains("."))
					caracteresDecimales = "0123456789";
			}
			else
				i = decimal.length();
		}

		return dec;
	}

	@SuppressWarnings("unchecked")
	public void importarValores() throws FileNotFoundException, IOException, ParseException {
		String fila;
		String fecha;
		String valor;
		String consulta;
		List<Object> indiceValor;
		IndiceValor nuevaFilaIndiceValor;
		List<IndiceValor> listaIndiceValor = new ArrayList<IndiceValor>();
		SimpleDateFormat format;
		Date fechaDateSQL;

		if(this.file.getFileSize() > 0 && this.file.getFileName().matches(".*.csv$")) {
			List<String> lineas = IOUtils.readLines(this.file.getInputStream());
		
			ProgressStatus.iTotal = lineas.size();
			ProgressStatus.iProgress = 0;
			ProgressStatus.onProcess = true;
			ProgressStatus.abort = false;
			ProgressStatus.sStatus = "Importando Archivo";

			consulta = "SELECT ind.fecha FROM IndiceValor ind WHERE ind.indice=" +
						+ this.indice + " ORDER BY ind.fecha";

			indiceValor = this.bp.getByFilterCast(consulta);

			for(int i = 0; i < lineas.size(); i++) {
				ProgressStatus.iProgress++;
				fila = lineas.get(i).trim();

				if(!fila.isEmpty()) {
					this.cantidadFilas++;

					if(fila.matches(".*[0-9]{2}/[0-9]{2}/[0-9]{4};[0-9]+.*")) {
						fecha = fila.substring(fila.indexOf("/")-2, fila.lastIndexOf("/")+5);

						format = new SimpleDateFormat("dd/MM/yyyy");
						fechaDateSQL = new Date((format.parse(fecha)).getTime());

						valor = fila.substring(fila.lastIndexOf("/")+6);
						valor = valor.replace(',', '.');
						valor = filtrarCaracteresNoDecimales(valor);

						if(!existeFecha(fechaDateSQL, indiceValor)) {
							this.cantidadFilasInsertadas++;

							nuevaFilaIndiceValor = new IndiceValor();
							nuevaFilaIndiceValor.setCantidad(0.0);
							nuevaFilaIndiceValor.setFecha(fechaDateSQL);
							nuevaFilaIndiceValor.setImporte(0.0);
							nuevaFilaIndiceValor.setObservaciones("");
							nuevaFilaIndiceValor.setValor(Double.parseDouble(valor) / 100);
							nuevaFilaIndiceValor.setIndice((Indice)bp.getById(Indice.class, this.indice));

							listaIndiceValor.add(nuevaFilaIndiceValor);
						}
						else
							this.cantidadFilasRepetidas++;
					}
					else
						if(!fila.toLowerCase().matches(".*fecha.*")) {
							this.cantidadFilasErroneas++;
						}
						else
							this.cantidadFilas--;
				}
			}

			if(this.cantidadFilasInsertadas > 0) {
				bp.saveOrUpdate(listaIndiceValor);
			}

			this.script = "<script>document.getElementById('informeResultado').style.visibility='visible';</script>\n";
		
			ProgressStatus.onProcess = false;
		}
		else
			this.script = "<script>alert('El archivo debe ser de tipo .csv y no vac�o');</script>\n";
	}

	public void rellenarFechas() throws FileNotFoundException, IOException {
		String consulta;
		List<Object> indiceValor;
		Object[] filaActualIndiceValor;
		Object[] filaSiguienteIndiceValor;
		List<IndiceValor> listaIndiceValor = new ArrayList<IndiceValor>();
		IndiceValor nuevaFilaIndiceValor;
		Calendar calendario = Calendar.getInstance();
		Date siguienteFecha;

		consulta = "SELECT ind.fecha, ind.valor FROM IndiceValor ind WHERE ind.indice=" +
		+ this.indice + " ORDER BY ind.fecha";

		indiceValor = this.bp.getByFilterCast(consulta);

		ProgressStatus.iTotal = indiceValor.size();
		ProgressStatus.iProgress = 0;
		ProgressStatus.onProcess = true;
		ProgressStatus.abort = false;
		ProgressStatus.sStatus = "Rellenando Tabla";

		ProgressStatus.iProgress++;

		for(int i = 0; i < indiceValor.size()-1; i++) {
			filaActualIndiceValor = (Object[]) indiceValor.get(i);
			filaSiguienteIndiceValor = (Object[]) indiceValor.get(i+1);

			calendario.setTime((Date) filaActualIndiceValor[0]); 
			calendario.add(Calendar.DATE, 1);
			siguienteFecha = new Date(calendario.getTimeInMillis());
			
			while(siguienteFecha.before((Date) filaSiguienteIndiceValor[0])) {
				this.cantidadFilasInsertadas++;

				nuevaFilaIndiceValor = new IndiceValor();
				nuevaFilaIndiceValor.setCantidad(0.0);
				nuevaFilaIndiceValor.setFecha(siguienteFecha);
				nuevaFilaIndiceValor.setImporte(0.0);
				nuevaFilaIndiceValor.setObservaciones("");
				nuevaFilaIndiceValor.setValor((Double) filaActualIndiceValor[1]);
				nuevaFilaIndiceValor.setIndice((Indice) bp.getById(Indice.class, this.indice));

				listaIndiceValor.add(nuevaFilaIndiceValor);

				calendario.add(Calendar.DATE, 1);
				siguienteFecha = new Date(calendario.getTimeInMillis());
			}

			ProgressStatus.iProgress++;
		}

		if(this.cantidadFilasInsertadas > 0) {
			bp.saveOrUpdate(listaIndiceValor);
		}

		this.script = "<script>document.getElementById('informeResultadoRelleno').style.visibility='visible';</script>\n";

		ProgressStatus.onProcess = false;
	}

	public FormFile getFile() {
		return file;
	}

	public void setFile(FormFile file) {
		this.file = file;
	}

	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return errores;
	}
	
	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Long getIndice() {
		return this.indice;
	}

	public void setIndice(Long indice) {
		this.indice = indice;
	}

	public Object getResult() {
		return recaudaciones;
	}
	
	public boolean validate() {
		return true;
	}

	public ArrayList<Recaudacion> getRecaudaciones() {
		return recaudaciones;
	}

	public void setRecaudaciones(ArrayList<Recaudacion> recaudaciones) {
		this.recaudaciones = recaudaciones;
	}

	public Boolean getDeshabilitado() {
		return deshabilitado;
	}
	
	public void setDeshabilitado(Boolean deshabilitado) {
		this.deshabilitado = deshabilitado;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public long getCantidadFilas() {
		return cantidadFilas;
	}

	public void setCantidadFilas(int cantidadFilas) {
		this.cantidadFilas = cantidadFilas;
	}

	public long getCantidadFilasInsertadas() {
		return cantidadFilasInsertadas;
	}

	public void setCantidadFilasInsertadas(int cantidadFilasInsertadas) {
		this.cantidadFilasInsertadas = cantidadFilasInsertadas;
	}

	public long getCantidadFilasRepetidas() {
		return cantidadFilasRepetidas;
	}

	public void setCantidadFilasRepetidas(int cantidadFilasRepetidas) {
		this.cantidadFilasRepetidas = cantidadFilasRepetidas;
	}

	public long getCantidadFilasErroneas() {
		return cantidadFilasErroneas;
	}

	public void setCantidadFilasErroneas(int cantidadFilasErroneas) {
		this.cantidadFilasErroneas = cantidadFilasErroneas;
	}
}