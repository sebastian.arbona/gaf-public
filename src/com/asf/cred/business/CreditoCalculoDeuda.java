package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;

import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCtaCte;

import com.nirven.creditos.hibernate.BeanCompensatorio;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.CtaCteAjuste;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Emideta;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.Tipomov;

public class CreditoCalculoDeuda {

	private Date hastaFecha;
	private BusinessPersistance bp;
	private Long idObjetoi;
	private List<BeanCtaCte> beans;
	private Log log = LogFactory.getLog(getClass());
	private BeanCtaCte total;
	private List<BonDetalle> bonDetalles;
	private Double tasaNetaFinal;

	/**
	 * Indica si el calculo se realiza para liquidar o no. Si es para liquidar,
	 * incluye las bonificaciones aunque esten dadas de baja.
	 */
	private boolean liquidacion;
	private Comparator<BeanCtaCte> comparadorFecha;
	/**
	 * Indica si se considera la liquidacion siempre, aunque tenga fecha posterior a
	 * la de consulta
	 */
	private boolean considerarLiquidacion;
	private Map<String, Double> sumasConceptos;
	private Map<String, Double> sumasConceptosManual;

	public CreditoCalculoDeuda() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.bonDetalles = new ArrayList<BonDetalle>();
		this.tasaNetaFinal = 0.0;
	}

	public CreditoCalculoDeuda(Long idObjetoi, Date hastaFecha) {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.idObjetoi = idObjetoi;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(hastaFecha);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		this.hastaFecha = calendar.getTime();
		this.bonDetalles = new ArrayList<BonDetalle>();
		this.tasaNetaFinal = 0.0;
	}

	/**
	 * Calculo de la deuda de un proyecto
	 */
	@SuppressWarnings("unchecked")
	public void calcular() {
		List<Ctacte> ccs = new ArrayList<Ctacte>();
		List<Ctacte> listaccs = new ArrayList<Ctacte>();
		Long objetoiID = getIdObjetoi();

		if (objetoiID == null || objetoiID == 0) {
			return;
		}

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, objetoiID);

		if (credito.getCaducidadPlazo() == null || credito.getCaducidadPlazo().getCuota() == null) {
			if (hastaFecha == null) {
				listaccs = bp.getNamedQuery("Ctacte.findByCredito").setLong("idCredito", objetoiID).list();
			} else {
				listaccs = bp.getNamedQuery("Ctacte.findByCreditoFecha").setParameter("fecha", hastaFecha)
						.setLong("idCredito", objetoiID).list();
			}
		} else {
			Cuota cuotaCad = credito.getCaducidadPlazo().getCuota();
			if (hastaFecha == null) {
				listaccs = bp.getNamedQuery("Ctacte.findByCuota").setLong("idCuota", cuotaCad.getId()).list();
			} else {
				listaccs = bp.getNamedQuery("Ctacte.findByCuotaFecha").setParameter("fecha", hastaFecha)
						.setLong("idCuota", cuotaCad.getId()).list();
			}
		}

		MultiMap creditosMap = new MultiHashMap();

		// fila total
		double capitalTotal = 0.0;
		double compensatorioTotal = 0.0;
		double punitorioTotal = 0.0;
		double moratorioTotal = 0.0;
		double gastosTotal = 0.0;
		double gastosRecTotal = 0.0;
		double multasTotal = 0.0;
		double cerTotal = 0.0;
		double saldoCuotaTotal = 0.0;
		double compensatorioBruto = 0.0;

		Tipomov tipoDebito = (Tipomov) bp.createQuery("SELECT t FROM Tipomov t WHERE t.abreviatura = :abreviatura")
				.setParameter("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();

		/**
		 * esto permite limpiar la lista de cuentas corrientes dejando solo las
		 * necesarias
		 */
		// for (Ctacte ctacte : listaccs) {
		// if (ctacte.getCuota().getNumero() == 2 && ctacte.getAsociado()
		// .getConcepto().getConcepto().equals("gas")) {
		// ccs.add(ctacte);
		// }
		// }

		ccs.addAll(listaccs);

		for (Ctacte cc : ccs) {
			cc.redondear();
			if (cc.getBoleto() != null && cc.getBoleto().getAnulado()) {
				continue;
			}
			if (cc.getBoleto() != null && (cc.getBoleto().getTipo().equals(Boleto.TIPO_BOLETO_NOTACRED_ANUL)
					|| cc.getBoleto().getTipo().equals(Boleto.TIPO_BOLETO_NOTADEB_DESAPL))) {
				continue;
			}

			BeanCtaCte bean = null;
			if (cc.getTipoMovimiento() != null && ("movManualDeb".equalsIgnoreCase(cc.getTipoMovimiento())
					|| "desapManualDeb".equalsIgnoreCase(cc.getTipoMovimiento()))) {
				String detalle = "movManualDeb".equalsIgnoreCase(cc.getTipoMovimiento()) ? "Debito Manual"
						: "Debito Manual Desaplicacion";
				Date fecha = (cc.getCuota().getEmision() == null ? new Date()
						: cc.getCuota().getEmision().getFechaEmision());
				String concepto = cc.getAsociado().getConcepto().getDetalle();
				if (cc.getBoleto() != null && cc.getCaratula() != null) {
					bean = new BeanCtaCte(cc.getCuota().getNumero(), fecha, cc.getCuota().getFechaGeneracion(), "",
							detalle, cc.getCuota_id(), null, cc.getBoleto(), cc.getCaratula().getId(), cc.getImporte(),
							concepto, cc.getComprobante());
				} else if (cc.getBoleto() != null) {
					bean = new BeanCtaCte(cc.getCuota().getNumero(), fecha, cc.getCuota().getFechaGeneracion(), "",
							detalle, cc.getCuota_id(), null, cc.getBoleto(), cc.getImporte(), concepto,
							cc.getComprobante());
				} else {
					bean = new BeanCtaCte(cc.getCuota().getNumero(), fecha, cc.getCuota().getFechaGeneracion(), "",
							detalle, cc.getCuota_id());
				}
				bean.setMotivo(cc.getDetalle());
				bean.setTipoMov(cc.getTipomov());
				bean.setSaldoCuota(null);
				bean.setFechaGeneracion(cc.getFechaGeneracion());
				creditosMap.put(cc.getCuota().getNumero(), bean);
				bean.addCtaCte(cc); // suma el concepto que corresponda
				capitalTotal += bean.getCapital();
				if (cc.getBoleto() != null) {
					bean.setBoleto(new Boleto());
					bean.getBoleto().setNumeroBoleto(cc.getBoleto().getNumeroBoleto());
				}
				compensatorioTotal += bean.getCompensatorio();
				compensatorioBruto += bean.getCompensatorio();
				punitorioTotal += bean.getPunitorio();
				moratorioTotal += bean.getMoratorio();
			} else if (!cc.isDebito()) {
				List<BeanCtaCte> beansCuota = (List<BeanCtaCte>) creditosMap.get(cc.getCuota().getNumero());
				bean = null;
				if (beansCuota != null) {
					for (BeanCtaCte b : beansCuota) {
						// agrupar por caratula para mostrar los ctacte del
						// mismo pago en una fila
						if (cc.getCaratula_id() != null && b.getCaratulaId() != null
								&& b.getCaratulaId().equals(cc.getCaratula_id())) {
							bean = b;
							break;
						} else if (cc.getBoleto() != null && cc.getBoleto().getId() != null && b.getBoleto() != null
								&& b.getBoleto().getId() != null
								&& cc.getBoleto().getId().equals(b.getBoleto().getId())) {
							// si no hay caratula, intentar por id de boleto
							bean = b;
							break;
						}
					}
				}

				if (bean == null) {
					String detalle;
					String tipoMov;
					if (cc.getTipoMovimiento() != null && cc.getTipoMovimiento().equalsIgnoreCase("movManualDeb")) {
						detalle = "Debito Manual";
						tipoMov = Tipomov.TIPOMOV_DEBITO;
					} else if (cc.getTipoMovimiento() != null
							&& cc.getTipoMovimiento().equalsIgnoreCase("movManualCred")) {
						detalle = "Credito Manual";
						tipoMov = Tipomov.TIPOMOV_CREDITO;
					} else if (cc.getTipoMovimiento() != null
							&& cc.getTipoMovimiento().equalsIgnoreCase("bonificacion")) {
						continue; // ignorar bonificaciones porque se calculan
						// abajo junto con compensatorio
					} else if (cc.getTipoMovimiento() != null
							&& cc.getTipoMovimiento().equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_CADUCIDAD)) {
						detalle = Objetoi.TIPO_MOVIMIENTO_CADUCIDAD;
						tipoMov = Objetoi.TIPO_MOVIMIENTO_CADUCIDAD;
					} else if (cc.getTipoMovimiento() != null
							&& cc.getTipoMovimiento().equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_DIF_CAMBIO)) {
						detalle = "Diferencia Cambio";
						CtacteKey key = cc.getId();
						CtaCteAjuste ajuste = (CtaCteAjuste) bp.getById(CtaCteAjuste.class, key);
						tipoMov = ajuste.getDebePesos() > 0 ? Tipomov.TIPOMOV_DEBITO : Tipomov.TIPOMOV_CREDITO;
					} else if (cc.getTipoMovimiento() != null
							&& cc.getTipoMovimiento().equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_REIMPUTACION)) {
						detalle = "Reimputacion Pago";
						tipoMov = Tipomov.TIPOMOV_CREDITO;
					} else {
						detalle = "Pago";
						tipoMov = Tipomov.TIPOMOV_CREDITO;
					}
					Tipomov t = (Tipomov) bp.createQuery("SELECT t FROM Tipomov t WHERE t.abreviatura = :abreviatura")
							.setParameter("abreviatura", tipoMov).uniqueResult();
					Date fecha = (cc.getCuota().getEmision() == null ? new Date()
							: cc.getCuota().getEmision().getFechaEmision());
					String concepto = cc.getAsociado().getConcepto().getDetalle();
					if (cc.getBoleto() != null && cc.getCaratula() != null) {
						bean = new BeanCtaCte(cc.getCuota().getNumero(), fecha, cc.getCuota().getFechaGeneracion(), "",
								detalle, cc.getCuota_id(), null, cc.getBoleto(), cc.getCaratula().getId(),
								cc.getImporte(), concepto, cc.getComprobante());
					} else if (cc.getBoleto() != null) {
						bean = new BeanCtaCte(cc.getCuota().getNumero(), fecha, cc.getCuota().getFechaGeneracion(), "",
								detalle, cc.getCuota_id(), null, cc.getBoleto(), cc.getImporte(), concepto,
								cc.getComprobante());
					} else {
						bean = new BeanCtaCte(cc.getCuota().getNumero(), fecha, cc.getCuota().getFechaGeneracion(), "",
								detalle, cc.getCuota_id(), cc.getImporte(), concepto, cc.getComprobante());
					}
					// Ticket 9318
					if (cc.getTipoMovimiento() != null
							&& cc.getTipoMovimiento().equalsIgnoreCase(Objetoi.TIPO_MOVIMIENTO_REIMPUTACION)) {
						// Ticket 9318
						String strRecibo = cc.getDetalle().replaceAll("\\D+", "").toString();
						if (!strRecibo.isEmpty()) {
							Long numeroRecibo = Long.parseLong(strRecibo);
							Objetoi acuerdo = (Objetoi) bp
									.createQuery("select oi.acuerdoPago from Objetoi oi where oi.id = :idObjetoi")
									.setParameter("idObjetoi", this.idObjetoi).uniqueResult();
							List<Pagos> listaPagos = bp
									.createSQLQuery("select {p.*} from Pagos p where p.recibo_id = ( "
											+ " 	select min(b.id) from Ctacte ccap, Boleto b "
											+ " 		where ccap.boleto_id = b.id "
											+ " 		  and ccap.tipoMovimiento = 'pago' "
											+ " 		  and b.tipo = 'Recibo'	 "
											+ " 		  and b.numeroBoleto = :numrecibo "
											+ " 		  and ccap.objetoi_id in ( "
											+ " 			select ap.id from Objetoi ap "
											+ " 				where ap.id = :idacuerdo ))")
									.addEntity("p", Pagos.class).setParameter("numrecibo", numeroRecibo)
									.setParameter("idacuerdo", acuerdo.getId()).list();
							if (listaPagos != null && !listaPagos.isEmpty()) {
								bean.setFechaGeneracion(listaPagos.get(0).getFechaPago());
							} else {
								bean.setFechaGeneracion(cc.getFechaGeneracion());
							}
						}
					} else {
						bean.setFechaGeneracion(cc.getFechaGeneracion());
					}
					bean.setTipoMov(t);
					bean.setSaldoCuota(null);
					bean.setMotivo(cc.getDetalle());
					creditosMap.put(cc.getCuota().getNumero(), bean);
				}
				// suma el concepto que corresponda
				bean.addCtaCte(cc);
			}
		}

		// sumarizar
		for (Object b : creditosMap.values()) {
			BeanCtaCte bean = (BeanCtaCte) b;
			if (!("Debito Manual".equalsIgnoreCase(bean.getDetalle())
					|| "Debito Manual Desaplicacion".equalsIgnoreCase(bean.getDetalle()))) {
				capitalTotal -= bean.getCapital();
				compensatorioTotal -= bean.getCompensatorio();
				compensatorioBruto -= bean.getCompensatorio();
				punitorioTotal -= bean.getPunitorio();
				moratorioTotal -= bean.getMoratorio();
				gastosTotal -= bean.getGastosSimples();
				gastosRecTotal -= bean.getGastosRec();
				multasTotal -= bean.getMultas();
				cerTotal -= bean.getCer();
			}
		}

		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setLong("idCredito", getIdObjetoi()).list();
		CreditoHandler ch = new CreditoHandler();

		// para cada cuota, agregar primero debito y luego los creditos
		beans = new ArrayList<BeanCtaCte>();
		Desembolso desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", getIdObjetoi()).setInteger("numero", 1).setMaxResults(1).uniqueResult();

		// cargar sumas de conceptos de todas las cuotas
		buscarSumas(credito); // key idcuota-concepto-tipomov

		// cuando hay caducidad, solo trabajar con la ultima cuota
		int i = credito.getCaducidadPlazo() == null ? 1 : cuotas.size();

		for (; i <= cuotas.size(); i++) {
			Cuota cuota = cuotas.get(i - 1);
			Date fechaEmision = null;
			if (cuota.getEmision() != null) {
				fechaEmision = cuota.getEmision().getFechaEmision();
			}
			BeanCtaCte debitoBean = new BeanCtaCte(cuota.getNumero(), fechaEmision, cuota.getFechaVencimiento(), "",
					"Cuota", cuota.getId());

			// CAPITAL, lo que dice la cuota
			debitoBean.setCapital(cuota.getCapital());
			debitoBean.setTipoMov(tipoDebito);
			BeanCtaCte bonifBean = null;

			Emideta liquidacion = null;
			Date fechaCalculo = hastaFecha;

			if (fechaEmision != null && (considerarLiquidacion || fechaEmision.getTime() <= hastaFecha.getTime())) {
				try {
					// debe haber una sola
					liquidacion = (Emideta) bp.getNamedQuery("Emideta.findByCreditoCuota")
							.setParameter("idCredito", cuota.getCredito_id()).setParameter("numero", cuota.getNumero())
							.setMaxResults(1).uniqueResult();

					if (liquidacion == null) {
						log.error("Error al buscar la liquidacion de la cuota " + cuota.getNumero() + " del credito "
								+ cuota.getCredito_id());
					} else {
						if (hastaFecha.getTime() < cuota.getFechaVencimiento().getTime()) {
							fechaCalculo = cuota.getFechaVencimiento();
						}
						if (liquidacion.getBoleto() != null) {
							debitoBean.setBoleto(liquidacion.getBoleto());
						} else {
							Long idBoleto = cuota.getCodigoDeBoletoPago();
							if (idBoleto != null) {
								Boleto boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
								debitoBean.setBoleto(boleto);
							}
						}
					}
				} catch (NoResultException e) {
					log.error("Error al buscar la liquidacion de la cuota " + cuota.getNumero() + " del credito "
							+ cuota.getCredito_id());
				}
			}

			// COMPENSATORIO
			Date fechaVtoAnterior = null;
			// fecha anterior es la de desembolso
			if (cuota.getNumero() == 1) {
				try {
					List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCreditoYNumero")
							.setParameter("idCredito", cuota.getCredito_id()).setParameter("numero", cuota.getNumero())
							.list();
					Desembolso desembolso = new Desembolso();
					if (!desembolsos.isEmpty()) {
						desembolso = desembolsos.get(0);
					}
					if (desembolso == null) {
						log.error("Error al buscar desembolso del credito " + cuota.getCredito_id());
					} else {
						if (desembolso.getFechaReal() != null) {
							fechaVtoAnterior = desembolso.getFechaReal();
						} else {
							fechaVtoAnterior = desembolso.getFecha();
						}
					}
				} catch (NoResultException e) {
					log.error("Error al buscar desembolso del credito " + cuota.getCredito_id());
				}
			} else {
				// fecha anterior es la cuota anterior
				fechaVtoAnterior = cuotas.get(i - 2).getFechaVencimiento();
			}

			double compensatorio = 0;
			double bonificaciones = 0;

			if (desembolso1 != null && desembolso1.getFechaReal() != null) {
				Date fechaReal = DateHelper.resetHoraToZero(desembolso1.getFechaReal());
				Date fechaHasta = DateHelper.resetHoraToZero(fechaCalculo);
				if (DateHelper.getDiffDates(fechaReal, fechaHasta, 2) > 0) {
					List<BeanCompensatorio> comps = calcularCompensatorio(cuota, fechaCalculo, fechaVtoAnterior);
					for (BeanCompensatorio comp : comps) {
						if (comp.getMonto() >= 0) {
							compensatorio += comp.getMonto();
						} else {
							bonificaciones += Math.abs(comp.getMonto());
						}
					}
				}
			}
			debitoBean.setCompensatorio(compensatorio);

			// buscar debitos de Bonificaciones (cuando estan anuladas) y
			// restarlas
			List<Ctacte> ccBonifs = bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha")
					.setParameter("fecha", fechaCalculo).setParameter("idCuota", cuota.getId())
					.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_BONIFICACION }).list();
			for (Ctacte ccBonif : ccBonifs) {
				if (ccBonif.isDebito()) {
					bonificaciones -= ccBonif.getImporte();
				}
			}

			if (bonificaciones > 0) {
				bonifBean = new BeanCtaCte(cuota.getNumero(), fechaEmision, cuota.getFechaVencimiento(), "",
						"Bonificacion", cuota.getId());
				bonifBean.setCompensatorio(bonificaciones);
			}

			// PUNITORIO Y MORATORIO
			// buscar lo ya generado y la ultima fecha de generacion
			List<Ctacte> ctactePunit = ch.listarCtaCteConceptos(cuota, hastaFecha, Concepto.CONCEPTO_PUNITORIO);
			double sumaPunitorio = 0;
			Date fechaUltimoDebitoPunitorio = null;
			Date fechaTasaPunitorio = null;
			for (Ctacte cc : ctactePunit) {
				if (cc.getBoleto() != null && cc.getBoleto().getAnulado()) {
					continue;
				}
				if (cc.getBoleto() != null && (cc.getBoleto().getTipo().equals(Boleto.TIPO_BOLETO_NOTACRED_ANUL)
						|| cc.getBoleto().getTipo().equals(Boleto.TIPO_BOLETO_NOTADEB_DESAPL))) {
					continue;
				}
				if (cc.isDebito()
						&& !(cc.getTipoMovimiento().equalsIgnoreCase("movManualDeb")
								|| cc.getTipoMovimiento().equalsIgnoreCase("desapManualDeb"))
						&& !(cc instanceof CtaCteAjuste)) {
					if (cc.getFechaGeneracion().getTime() <= fechaCalculo.getTime()) {
						sumaPunitorio += cc.getImporte();
						fechaUltimoDebitoPunitorio = cc.getFechaGeneracion();
						/**
						 * Omite los movimientos que se generan a fin de cada ejercicio para fijar los
						 * punitorios devengados y no pagados o por caducidad de plazo
						 */
						if ("117".equalsIgnoreCase(cc.getTipoConcepto())
								|| "104".equalsIgnoreCase(cc.getTipoConcepto())) {
						} else {
							fechaTasaPunitorio = cc.getFechaGeneracion();
						}
					}
				}
			}
			List<Ctacte> ctacteMorat = ch.listarCtaCteConceptos(cuota, hastaFecha, Concepto.CONCEPTO_MORATORIO);
			double sumaMoratorio = 0;
			Date fechaUltimoDebitoMoratorio = null;
			Date fechaTasaMoratorio = null;
			for (Ctacte cc : ctacteMorat) {
				if (cc.getBoleto() != null && cc.getBoleto().getAnulado()) {
					continue;
				}
				if (cc.getBoleto() != null && (cc.getBoleto().getTipo().equals(Boleto.TIPO_BOLETO_NOTACRED_ANUL)
						|| cc.getBoleto().getTipo().equals(Boleto.TIPO_BOLETO_NOTADEB_DESAPL))) {
					continue;
				}
				if (cc.isDebito()
						&& !(cc.getTipoMovimiento().equalsIgnoreCase("movManualDeb")
								|| cc.getTipoMovimiento().equalsIgnoreCase("desapManualDeb"))
						&& !(cc instanceof CtaCteAjuste)) {
					if (cc.getFechaGeneracion().getTime() <= fechaCalculo.getTime()) {
						sumaMoratorio += cc.getImporte();
						fechaUltimoDebitoMoratorio = cc.getFechaGeneracion();
						/**
						 * Omite los movimientos que se generan a fin de cada ejercicio para fijar los
						 * moratorios devengados y no pagados o por caducidad de plazo
						 */
						if ("117".equalsIgnoreCase(cc.getTipoConcepto())
								|| "104".equalsIgnoreCase(cc.getTipoConcepto())) {
						} else {
							fechaTasaMoratorio = cc.getFechaGeneracion();
						}
					}
				}
			}

			double creditosCapital = buscarSumaConcepto(cuota, Concepto.CONCEPTO_CAPITAL, "CR");
			double saldoCuotaGastos = buscarSaldoConcepto(cuota, Concepto.CONCEPTO_GASTOS);
			double saldoCuotaRec = buscarSaldoConcepto(cuota, Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
			double saldoCuotaMultas = buscarSaldoConcepto(cuota, Concepto.CONCEPTO_MULTAS);
			double saldoCuotaCer = buscarSaldoConcepto(cuota, Concepto.CONCEPTO_CER);
			double creditosCompensatorio = buscarSumaConcepto(cuota, Concepto.CONCEPTO_COMPENSATORIO, "CR");
			double creditosPunitorio = buscarSumaConcepto(cuota, Concepto.CONCEPTO_PUNITORIO, "CR");
			double creditosMoratorio = buscarSumaConcepto(cuota, Concepto.CONCEPTO_MORATORIO, "CR");
			double debitosManualCapital = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_CAPITAL, "DB");
			double debitosManualCompensatorio = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_COMPENSATORIO, "DB");
			double debitosManualMoratorio = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_MORATORIO, "DB");
			double debitosManualPunitorio = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_PUNITORIO, "DB");
			double debitosManualMultas = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_MULTAS, "DB");
			double debitosManualGastos = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_GASTOS, "DB");
			double debitosManualRec = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_GASTOS_A_RECUPERAR, "DB");
			double debitosManualCer = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_CER, "DB");
			double creditosManualCapital = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_CAPITAL, "CR");
			double creditosManualCompensatorio = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_COMPENSATORIO, "CR");
			double creditosManualMoratorio = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_MORATORIO, "CR");
			double creditosManualPunitorio = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_PUNITORIO, "CR");
			double creditosManualMultas = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_MULTAS, "CR");
			double creditosManualGastos = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_GASTOS, "CR");
			double creditosManualRec = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_GASTOS_A_RECUPERAR, "CR");
			double creditosManualCer = buscarSumaConceptoManual(cuota, Concepto.CONCEPTO_CER, "CR");

			// no incluye moratorio ni punitorio
			double saldoDeudaImponible = debitoBean.getCapital() - creditosCapital + saldoCuotaGastos + saldoCuotaRec
					+ saldoCuotaMultas + debitoBean.getCompensatorio() - creditosCompensatorio + debitosManualCapital
					+ debitosManualCompensatorio - creditosManualCapital - creditosManualCompensatorio
					+ debitosManualGastos + debitosManualRec + debitosManualMultas - creditosManualGastos
					- creditosManualRec - creditosManualMultas;
			double saldoImponible = debitoBean.getCapital() - creditosCapital + debitoBean.getCompensatorio()
					- creditosCompensatorio + debitosManualCapital + debitosManualCompensatorio - creditosManualCapital
					- creditosManualCompensatorio;
			// restar bonificaciones
			if (bonifBean != null && bonifBean.getCompensatorio() != null) {
				saldoDeudaImponible -= bonifBean.getCompensatorio();
				saldoImponible -= bonifBean.getCompensatorio();
			}

			// si queda saldo a la fecha
			if (saldoImponible > 0) {
				Objetoi proyectoCalculo = cuota.getCredito();
				// punitorio desde la ultima fecha generacion
				log.debug("comienza calculo punitorio cuota " + cuota.getNumero());
				fechaUltimoDebitoPunitorio = fechaUltimoDebitoPunitorio == null ? cuota.getFechaVencimiento()
						: fechaUltimoDebitoPunitorio;
				fechaTasaPunitorio = fechaTasaPunitorio == null ? cuota.getFechaVencimiento() : fechaTasaPunitorio;
				int dias = (int) DateHelper.getDiffDates(fechaUltimoDebitoPunitorio, hastaFecha, 2);
				int diasPeriodo = dias;
				int diasTransaccion = dias;
				double punitorio = proyectoCalculo.calcularPunitorioNew(saldoImponible, diasPeriodo, diasTransaccion,
						fechaTasaPunitorio, fechaUltimoDebitoPunitorio, hastaFecha);
				// moratorio desde la ultima fecha generacion
				log.debug("comienza calculo moratorio cuota " + cuota.getNumero());
				fechaUltimoDebitoMoratorio = fechaUltimoDebitoMoratorio == null ? cuota.getFechaVencimiento()
						: fechaUltimoDebitoMoratorio;
				fechaTasaMoratorio = fechaTasaMoratorio == null ? cuota.getFechaVencimiento() : fechaTasaMoratorio;
				dias = (int) DateHelper.getDiffDates(fechaUltimoDebitoMoratorio, hastaFecha, 2);
				diasPeriodo = dias;
				diasTransaccion = dias;
				double moratorio = proyectoCalculo.calcularMoratorioNew(saldoImponible, diasPeriodo, diasTransaccion,
						fechaTasaMoratorio, fechaUltimoDebitoMoratorio, hastaFecha);

				sumaPunitorio += punitorio;
				sumaMoratorio += moratorio;
			}
			debitoBean.setPunitorio(sumaPunitorio);
			debitoBean.setMoratorio(sumaMoratorio);

			// GASTOS Y MULTAS
			List<Ctacte> gastosConceptos = bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha")
					.setParameter("idCuota", cuota.getId()).setParameter("fecha", fechaCalculo)
					.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_GASTOS }).list();
			double importeGastos = 0;
			double sumGastos = 0;
			for (Ctacte gc : gastosConceptos) {
				if (gc.getTipoMovimiento() != null) {
					if (gc.isDebito() && gc.getTipoMovimiento().equalsIgnoreCase("cuota")) {
						importeGastos += gc.getImporte();
						sumGastos += gc.getImporte();
					} else if (gc.isDebito()) {
						sumGastos += gc.getImporte();
					}
				}
			}
			debitoBean.setGastosSimples(importeGastos);

			// GASTOS A RECUPERAR
			List<Ctacte> gastosRecConceptos = bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha")
					.setParameter("idCuota", cuota.getId()).setParameter("fecha", fechaCalculo)
					.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_GASTOS_A_RECUPERAR }).list();
			double importeGastosRec = 0;
			double sumGastosRec = 0;
			for (Ctacte gc : gastosRecConceptos) {
				if (gc.getTipoMovimiento() != null) {
					if (gc.isDebito() && gc.getTipoMovimiento().equalsIgnoreCase("cuota")) {
						importeGastosRec += gc.getImporte();
						sumGastosRec += gc.getImporte();
					} else if (gc.isDebito()) {
						sumGastosRec += gc.getImporte();
					}
				}
			}

			debitoBean.setGastosRec(importeGastosRec);

			List<Ctacte> multasConceptos = bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha")
					.setParameter("idCuota", cuota.getId()).setParameter("fecha", fechaCalculo)
					.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_MULTAS }).list();
			double importeMultas = 0;
			double sumMultas = 0;
			for (Ctacte mc : multasConceptos) {
				if (mc.isDebito() && mc.getTipoMovimiento().equalsIgnoreCase("cuota")) {
					importeMultas += mc.getImporte();
					sumMultas += mc.getImporte();
				} else if (mc.isDebito()) {
					sumMultas += mc.getImporte();
				}
			}
			debitoBean.setMultas(importeMultas);

			List<Ctacte> cerConceptos = bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha")
					.setParameter("idCuota", cuota.getId()).setParameter("fecha", fechaCalculo)
					.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_CER }).list();
			double importeCer = 0;
			double sumCer = 0;
			for (Ctacte mc : cerConceptos) {
				if (mc.isDebito() && mc.getTipoMovimiento().equalsIgnoreCase("cuota")) {
					importeCer += mc.getImporte();
					sumCer += mc.getImporte();
				} else if (mc.isDebito()) {
					sumCer += mc.getImporte();
				}
			}
			debitoBean.setCer(importeCer);

			double saldoDeuda = saldoDeudaImponible - creditosMoratorio - creditosPunitorio + debitosManualMoratorio
					+ debitosManualPunitorio - creditosManualMoratorio - creditosManualPunitorio;
			saldoDeuda += (debitosManualCer + saldoCuotaCer - creditosManualCer);

			// agregar moratorio y punitorio
			debitoBean.setSaldoCuota(saldoDeuda + debitoBean.getPunitorio() + debitoBean.getMoratorio());

			beans.add(debitoBean);

			// restar bonificacion
			if (bonifBean != null) {
				beans.add(bonifBean);
				compensatorioTotal -= bonifBean.getCompensatorio();
			}

			capitalTotal += debitoBean.getCapital();
			compensatorioTotal += debitoBean.getCompensatorio();
			compensatorioBruto += debitoBean.getCompensatorio();
			punitorioTotal += debitoBean.getPunitorio();
			moratorioTotal += debitoBean.getMoratorio();
			gastosTotal += sumGastos;
			gastosRecTotal += sumGastosRec;
			multasTotal += sumMultas;
			saldoCuotaTotal += debitoBean.getSaldoCuota();
			cerTotal += sumCer;

			List<BeanCtaCte> creditosBeans = (List<BeanCtaCte>) creditosMap.get(cuota.getNumero());
			if (creditosBeans != null) {
				Collections.sort(creditosBeans, getComparadorFecha());
				beans.addAll(creditosBeans);
			}
		}

		total = new BeanCtaCte();
		total.setNumero(0);
		total.setCapital(capitalTotal);
		total.setCompensatorio(compensatorioTotal);
		total.setCompensatorioBruto(compensatorioBruto);
		total.setPunitorio(punitorioTotal);
		total.setMoratorio(moratorioTotal);
		total.setGastosSimples(gastosTotal);
		total.setGastosRec(gastosRecTotal);
		total.setMultas(multasTotal);
		total.setSaldoCuota(saldoCuotaTotal);
		total.setCer(cerTotal);
		beans.add(total);

	}

	@SuppressWarnings("unchecked")
	private void buscarSumas(Objetoi credito) {
		String consulta = "SELECT c.cuota_id, con.concepto_concepto, "
				+ "CASE WHEN c.tipomov_id = 2 THEN 'DB' ELSE 'CR' END AS tipoMov, SUM(c.importe) AS suma, "
				+ "CASE WHEN c.tipoMovimiento IN ('movManualDeb','movManualCred') THEN 'manual' ELSE 'noManual' END AS manual "
				+ "FROM Ctacte c JOIN Concepto con ON con.id = c.asociado_id "
				+ "LEFT JOIN Boleto bol ON c.boleto_id = bol.id WHERE c.objetoi_id = :idCredito "
				+ "AND c.fechaGeneracion <= :fecha AND (bol.id IS NULL OR bol.anulado = 0 OR bol.anulado IS NULL) "
				+ "AND (bol.id IS NULL OR bol.tipo NOT IN ('Nota Debito Desaplicacion', 'Nota Credito Anulacion')) "
				+ "GROUP BY c.cuota_id, con.concepto_concepto, CASE WHEN c.tipomov_id = 2 THEN 'DB' ELSE 'CR' END, "
				+ "CASE WHEN c.tipoMovimiento IN ('movManualDeb','movManualCred') THEN 'manual' ELSE 'noManual' END";
		List<Object[]> valores = bp.createSQLQuery(consulta).setParameter("idCredito", credito.getId())
				.setParameter("fecha", hastaFecha).list();
		sumasConceptos = new HashMap<String, Double>();
		sumasConceptosManual = new HashMap<String, Double>();
		for (Object[] fila : valores) {
			Long idCuota = fila[0] != null ? ((Number) fila[0]).longValue() : null;
			String concepto = (String) fila[1];
			String tipomov = (String) fila[2];
			Double importe = ((Number) fila[3]).doubleValue();
			String manual = (String) fila[4];
			if (idCuota != null) {
				if (!manual.equalsIgnoreCase("manual")) {
					sumasConceptos.put(idCuota + "-" + concepto + "-" + tipomov, importe);
				} else {
					sumasConceptosManual.put(idCuota + "-" + concepto + "-" + tipomov, importe);
				}
			}
		}
	}

	private double buscarSaldoConcepto(Cuota cuota, String concepto) {
		if (sumasConceptos == null) {
			return 0;
		}
		Double debs = sumasConceptos.get(cuota.getId() + "-" + concepto + "-DB");
		Double creds = sumasConceptos.get(cuota.getId() + "-" + concepto + "-CR");
		return (debs != null ? debs : 0) - (creds != null ? creds : 0);
	}

	private double buscarSumaConcepto(Cuota cuota, String concepto, String tipomov) {
		if (sumasConceptos == null) {
			return 0;
		}
		Double suma = sumasConceptos.get(cuota.getId() + "-" + concepto + "-" + tipomov);
		return suma != null ? suma : 0;
	}

	private double buscarSumaConceptoManual(Cuota cuota, String concepto, String tipomov) {
		if (sumasConceptosManual == null) {
			return 0;
		}
		Double suma = sumasConceptosManual.get(cuota.getId() + "-" + concepto + "-" + tipomov);
		return suma != null ? suma : 0;
	}

	/**
	 * Calcula el compensatorio para una cuota y devuelve el detalle de como esta
	 * conformado.
	 * 
	 * @param cuota            la cuota para la cual calcular
	 * @param fechaCalculo     fecha en que se realiza el calculo
	 * @param fechaVtoAnterior fecha de vencimiento anterior o inicio de periodo de
	 *                         devengamiento de la cuota
	 * @return lista de BeanCompensatorio que detallan los items que forman el monto
	 *         total de compensatorio
	 */
	public List<BeanCompensatorio> calcularCompensatorio(Cuota cuota, Date fechaCalculo, Date fechaVtoAnterior) {
		return Objetoi.calcularCompensatorio(cuota, fechaCalculo, fechaVtoAnterior, bonDetalles, liquidacion);
	}

	/**
	 * Consulta las facturas y detalles de facturas que corresponden a gastos a
	 * recuperar que todavia no han sido impactados
	 * 
	 * @return importe de gastos a recuperar
	 */
	public double calcularGastosRecuperar() {
		Number totalGastos = (Number) bp.createQuery(
				"select sum(d.importe) from DetalleFactura d where d.credito.id = :idCredito and d.impactado = false and d.factura.fechaPago is not null")
				.setParameter("idCredito", idObjetoi).uniqueResult();
		return totalGastos != null ? totalGastos.doubleValue() : 0;
	}

	public Date getHastaFecha() {
		return hastaFecha;
	}

	/**
	 * Setea la fecha tope para los calculos. La hora se fija a las 0 de ese dia
	 * 
	 * @param hastaFecha fecha a utilizar como tope
	 */
	public void setHastaFecha(Date hastaFecha) {
		this.hastaFecha = DateHelper.resetHoraToZero(hastaFecha);
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public List<BeanCtaCte> getBeans() {
		return beans;
	}

	public void setBeans(List<BeanCtaCte> beans) {
		this.beans = beans;
	}

	public BeanCtaCte getTotal() {
		return total;
	}

	public void setTotal(BeanCtaCte total) {
		this.total = total;
	}

	/**
	 * @return la deuda vencida hasta la fecha fechaHasta
	 */
	public double getDeudaParcial() {
		double saldoTotal = total.getSaldoCuota();
		for (BeanCtaCte bean : beans) {
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				if (bean.getFechaVencimiento().after(hastaFecha)) {
					saldoTotal -= bean.getSaldoCuota();
				}
			}
		}
		return saldoTotal;
	}

	/**
	 * Obtiene el total de la deuda calculada en <code>beans</code> y le suma gastos
	 * a recuperar no impactados.
	 * 
	 * @return deuda total mas gastos a recuperar no impactados.
	 */
	public double getDeudaTotal() {
		return total.getSaldoCuota() + calcularGastosRecuperar();
	}

	public double getSaldoCuota() {
		return total.getSaldoCuota();
	}

	/**
	 * Obtiene la deuda con capital parcial (solamente vencido) y suma gastos a
	 * recuperar no impactados.
	 * 
	 * @return deuda de capital parcial mas gastos a recuperar no impactados
	 */
	public double getDeudaVencida() {
		return getDeudaCapitalParcial() + calcularGastosRecuperar();
	}

	/**
	 * Calcula la deuda vencida. Resta de la deuda total el concepto de capital y
	 * suma el capital parcial (solo de las cuotas vencidas).
	 * 
	 * @see getCapitalParcial()
	 * @return deuda vencida
	 */
	public double getDeudaCapitalParcial() {
		double deuda = getTotal().getSaldoCuota();
		// restar los debitos de capital
		for (int i = 0; i < getBeans().size(); i++) {
			BeanCtaCte bean = getBeans().get(i);
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				deuda -= bean.getCapital();
			}
		}

		deuda += getCapitalParcial(); // sumo solo el parcial
		return deuda;
	}

	/**
	 * Calcula capital de las cuotas vencidas mas el capital de la cuota que tiene
	 * su periodo de devengamiento en la fecha de calculo.
	 * 
	 * @return capital parcial
	 */
	public double getCapitalParcial() {
		Date vencimientoAnterior = null;
		try {
			Desembolso desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
					.setLong("idCredito", getIdObjetoi()).setInteger("numero", 1).setMaxResults(1).uniqueResult();
			vencimientoAnterior = desembolso1.getFechaReal();
			if (vencimientoAnterior == null) {
				vencimientoAnterior = desembolso1.getFecha();
			}
		} catch (NoResultException e) {
		}

		double sumaCapitalParcial = 0;
		for (int i = 0; i < getBeans().size(); i++) {
			BeanCtaCte bean = getBeans().get(i);
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				if (bean.getFechaEmision() != null && bean.getFechaEmision().getTime() <= hastaFecha.getTime()) {
					sumaCapitalParcial += bean.getCapital();
				} else if (vencimientoAnterior != null && hastaFecha.getTime() > vencimientoAnterior.getTime()) {
					sumaCapitalParcial += bean.getCapital();
				}

				if (!vencimientoAnterior.equals(bean.getFechaVencimiento())) {
					vencimientoAnterior = bean.getFechaVencimiento();
				}
			}
		}

		return sumaCapitalParcial;
	}

	/**
	 * Calcula deuda vencida hasta la fecha de calculo, solo de cuotas que han
	 * vencido hasta esa fecha. Utiliza saldo de cuota, de las que ya han vencido.
	 * 
	 * @return la deuda vencida a una fecha
	 */
	public double getDeudaVencidaFechaEstricta() {
		double deuda = 0;
		for (int i = 0; i < getBeans().size() - 1; i++) {
			BeanCtaCte bean = getBeans().get(i);
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				if (bean.getFechaVencimiento() != null
						&& bean.getFechaVencimiento().getTime() <= hastaFecha.getTime()) {
					deuda += bean.getSaldoCuota();
				}
			}
		}
		return deuda;
	}

	/**
	 * Calcula la fecha del primer vto impago.
	 * 
	 * @return fecha del primer vto impago
	 */
	public Date getPrimerVtoImpago() {
		Date ret = null;
		for (int i = 0; i < getBeans().size() - 1; i++) {
			BeanCtaCte bean = getBeans().get(i);
			if ((bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota"))
					&& (bean.getFechaVencimiento() != null && bean.getSaldoCuota() > 0.01)
					&& (ret == null || ret.after(bean.getFechaVencimiento())))
				ret = bean.getFechaVencimiento();

		}

		return ret;
	}

	public List<BeanCtaCte> getVistaVencida() {
		int desde = 0;
		int hasta = getBeans().size() - 1;
		for (int i = 0; i < getBeans().size() - 1; i++) {
			BeanCtaCte bean = getBeans().get(i);
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				if (bean.getFechaVencimiento() != null && bean.getFechaVencimiento().getTime() > hastaFecha.getTime()) {
					hasta = i;
					break;
				}
			}
		}
		return getBeans().subList(desde, hasta);
	}

	public double getSaldoVencidoConcepto(String concepto) {
		return getSaldo(getVistaVencida(), concepto);
	}

	public List<BeanCtaCte> getVistaCuota(int cuota) {
		List<BeanCtaCte> vistaCuota = new ArrayList<BeanCtaCte>();
		beans: for (int j = 0; j < beans.size(); j++) {
			BeanCtaCte bean = beans.get(j);
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota") && bean.getNumero() != null
					&& bean.getNumero().intValue() == cuota) {
				vistaCuota.add(bean);
				for (int k = j + 1; k < beans.size(); k++) {
					BeanCtaCte beanSig = beans.get(k);
					if ((beanSig.getDetalle() == null || !beanSig.getDetalle().equalsIgnoreCase("Cuota"))
							// 0 = fila total
							&& beanSig.getNumero() != null && beanSig.getNumero().intValue() != 0) {
						vistaCuota.add(beanSig);
						j++;
					} else {
						break beans;
					}
				}
			}
		}
		return vistaCuota;
	}

	public double getSaldoCuotaConcepto(String concepto, int cuota) {
		return getSaldo(getVistaCuota(cuota), concepto);
	}

	public static double getSaldo(List<BeanCtaCte> beans, String concepto) {
		double saldo = 0;
		for (BeanCtaCte b : beans) {
			if (concepto.equals(Concepto.CONCEPTO_CAPITAL)) {
				saldo += b.getCapital() * (b.isDebito() ? 1 : -1);
			} else if (concepto.equals(Concepto.CONCEPTO_BONIFICACION)) {
				if (b.isBonificacion()) {
					saldo += b.getCompensatorio();
				}
			} else if (concepto.equals(Concepto.CONCEPTO_COMPENSATORIO)) {
				if (!b.isBonificacion()) {
					saldo += b.getCompensatorio() * (b.isDebito() ? 1 : -1);
				}
			} else if (concepto.equals(Concepto.CONCEPTO_MORATORIO)) {
				saldo += b.getMoratorio() * (b.isDebito() ? 1 : -1);
			} else if (concepto.equals(Concepto.CONCEPTO_PUNITORIO)) {
				saldo += b.getPunitorio() * (b.isDebito() ? 1 : -1);
			} else if (concepto.equals(Concepto.CONCEPTO_MULTAS)) {
				saldo += b.getMultas() * (b.isDebito() ? 1 : -1);
			} else if (concepto.equals(Concepto.CONCEPTO_GASTOS)) {
				saldo += b.getGastosSimples() * (b.isDebito() ? 1 : -1);
			} else if (concepto.equals(Concepto.CONCEPTO_GASTOS_A_RECUPERAR)) {
				saldo += b.getGastosRec() * (b.isDebito() ? 1 : -1);
			}
		}
		return saldo;
	}

	/**
	 * Calcula el compensatorio que esta despues del ultimo vencimiento, abarcado
	 * por la fecha de calculo.
	 * 
	 * @return compensatorio que esta despues del ultimo vencimiento
	 */
	public double getCompensatorioParcial() {
		double compensatorioParcial = 0;
		boolean sumaCompensatorio = false;
		for (int i = 0; i < getBeans().size() - 1; i++) {
			BeanCtaCte bean = getBeans().get(i);
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				if (bean.getFechaVencimiento() != null && bean.getFechaVencimiento().getTime() > hastaFecha.getTime()) {
					sumaCompensatorio = true;
				}
			}

			if (sumaCompensatorio) {
				compensatorioParcial += bean.getCompensatorio();
			}
		}
		return compensatorioParcial;
	}

	public Double deuda() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		double interesCompensatorio = 0.0;
		double interesMoratoriosPunitorios = 0.0;
		double gastos = 0.0;
		double capitalDevengado = 0.0;
		double totalPagado = 0.0;
		double bonificaciones = 0.0;
		double totalDevengado = 0.0;
		double totalAdeudado = 0.0;
		Date hastaFecha = new Date();
		BeanCtaCte bean;
		for (int i = 0; i < beans.size() - 1; i++) {
			bean = beans.get(i);
			Cuota cuota = (Cuota) bp.getById(Cuota.class, bean.getIdCuota());
			Date fechaComparacion = new Date();
			if (!(hastaFecha == null)) {
				fechaComparacion = hastaFecha;
			} else {
				fechaComparacion = sumaDias(cuota.getFechaVencimiento(), 1);
			}
			if (bean.getDetalle().equalsIgnoreCase("Cuota")) {
				interesCompensatorio += (bean.getCompensatorio() == null ? 0 : bean.getCompensatorio());
				interesMoratoriosPunitorios += ((bean.getMoratorio() == null ? 0 : bean.getMoratorio())
						+ (bean.getPunitorio() == null ? 0 : bean.getPunitorio()));

				gastos += (bean.getGastos() == null ? 0 : bean.getGastos())
						+ (bean.getMultas() == null ? 0 : bean.getMultas());

				if (cuota.getEmision() != null) {
					Emideta liquidacion = (Emideta) bp.getNamedQuery("Emideta.findByCreditoCuota")
							.setParameter("idCredito", cuota.getCredito_id()).setParameter("numero", cuota.getNumero())
							.setMaxResults(1).uniqueResult(); // debe haber una
					// sola
					capitalDevengado += liquidacion.getCapital();
				} else if (cuota.getFechaVencimiento().before(fechaComparacion)) {
					capitalDevengado += (bean.getCapital() == null ? 0 : bean.getCapital());
				}
			} else if (bean.getDetalle().equalsIgnoreCase("Pago")) {
				totalPagado += ((bean.getCapital() == null ? 0 : bean.getCapital())
						+ (bean.getCompensatorio() == null ? 0 : bean.getCompensatorio())
						+ (bean.getMoratorio() == null ? 0 : bean.getMoratorio())
						+ (bean.getPunitorio() == null ? 0 : bean.getPunitorio())
						+ (cuota.getBonificacion() == null ? 0 : cuota.getBonificacion())
						+ (bean.getGastos() == null ? 0 : bean.getGastos()));
			} else if (bean.getDetalle().equalsIgnoreCase("Bonificacion")) {
				bonificaciones += (bean.getCompensatorio() == null ? 0 : bean.getCompensatorio());
			}
		}
		totalDevengado = interesCompensatorio + interesMoratoriosPunitorios - bonificaciones + gastos
				+ capitalDevengado;
		totalAdeudado = totalDevengado - totalPagado;
		if (totalAdeudado >= 0.0) {
			return totalAdeudado;
		} else {
			return 0.0;
		}
	}

	private static Date sumaDias(Date fecha, int dias) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		cal.add(Calendar.DAY_OF_YEAR, dias);
		return cal.getTime();
	}

	public Double getTotalBonificaciones() {
		Double bon = 0.0;
		for (BeanCtaCte bean : beans) {
			if (bean.getDetalle().equalsIgnoreCase("bonificacion")) {
				bon += bean.getCompensatorio();
			}
		}
		return bon;
	}

	public List<BonDetalle> getBonDetalles() {
		return bonDetalles;
	}

	public void setBonDetalles(List<BonDetalle> bonDetalles) {
		this.bonDetalles = bonDetalles;
	}

	public Double getTasaNetaFinal() {
		return tasaNetaFinal;
	}

	public void setTasaNetaFinal(Double tasaNetaFinal) {
		this.tasaNetaFinal = tasaNetaFinal;
	}

	/**
	 * Indica si es liquidacion Si es true, incluye bonificaciones aunque esten
	 * dadas de baja, para poder hacer credito y debito.
	 * 
	 * @param liquidacion
	 */
	public void setLiquidacion(boolean liquidacion) {
		this.liquidacion = liquidacion;
	}

	public boolean isLiquidacion() {
		return liquidacion;
	}

	private Comparator<BeanCtaCte> getComparadorFecha() {
		if (comparadorFecha == null) {
			comparadorFecha = new Comparator<BeanCtaCte>() {

				@Override
				public int compare(BeanCtaCte o1, BeanCtaCte o2) {
					if (o1 != null && o2 != null) {
						// Cristian Anton
						// Parche cuando el campo fecha de generecion viene nulo
						// 14/08/2017
						if (o1.getFechaGeneracion() == null)
							o1.setFechaGeneracion(o2.getFechaGeneracion());
						if (o2.getFechaGeneracion() == null)
							o2.setFechaGeneracion(o1.getFechaGeneracion());
						if (o1.getFechaGeneracion() != null && o2.getFechaGeneracion() != null) {
							return o1.getFechaGeneracion().compareTo(o2.getFechaGeneracion());
						}
						return 0;
						// fin parche

					}
					return 0;
				}
			};
		}
		return comparadorFecha;
	}

	public void setConsiderarLiquidacion(boolean considerarLiquidacion) {
		this.considerarLiquidacion = considerarLiquidacion;
	}

	public boolean isConsiderarLiquidacion() {
		return considerarLiquidacion;
	}
}
