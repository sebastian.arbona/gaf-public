package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;

import com.asf.gaf.hibernate.Ejercicio;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.ReportResult;
import com.asf.util.SQLHelper;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Pagos;

public class ConsultarLotes implements IProcess {
	private Long idCaratula;
	private Long nroLote;
	private HashMap<String, Object> errores = new HashMap<String, Object>();
	private Object result;
	private Caratula caratula;
	private Boolean delete;
	private String forward;
	BusinessPersistance bp;
	private String[] lista;
	private ReportResult reporte;
	private Long idBoleto;
	private String accion;
	private Integer metodoCarga;
	private Long idObjetoi;
	private String esquemaGAF;

	public ConsultarLotes() {
		esquemaGAF = DirectorHelper.getString("SGAF");
	}

	@SuppressWarnings("unchecked")
	public boolean doProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		CobroPagoHandler handler = new CobroPagoHandler();

		if (SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("ConsultarLotes.accion") != null)
			accion = (String) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("ConsultarLotes.accion");
		if (accion != null && accion.equals("volverPDF")) {
			idCaratula = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("ConsultarLotes.idCaratula");
			nroLote = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("ConsultarLotes.nroLote");
			metodoCarga = (Integer) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("ConsultarLotes.metodoCarga");
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("ConsultarLotes.idCaratula", null);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("ConsultarLotes.nroLote",
					null);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("ConsultarLotes.accion",
					null);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("ConsultarLotes.metodoCarga", null);
		}

		if (this.nroLote != null && nroLote.longValue() > 0 && (delete != null && delete.booleanValue())) {
			Caratula caratula = (Caratula) bp.getById(Caratula.class, this.idCaratula);
			String usuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
			if (!caratula.getUsuario().equals(usuario)) {
				errores.put("caratula.eliminarLote.usuario", "caratula.eliminarLote.usuario");
				return false;
			}

			if (handler.delete(this.idCaratula, this.nroLote)) {
				nroLote = null; // Eliminamos, por lo que volvemos al listado;
				delete = null;
			} else {
				// ERRORES???
				return false; // No se pudo eliminar
			}
		}

		// DetalleBolepago..
		if (this.lista != null && this.lista.length > 0) {
			List[] result = new List[1]; // Detalle de Bolepagos para el boleto seleccionado..
			@SuppressWarnings("unused")
			String[] lst = this.lista[0].split("-");
			// result[0] = handler.getDetallebolepagos(this.idCaratula, lst);

			this.result = result;
			forward = "DetalleBolepagos";
			return true;

		}

		this.caratula = (Caratula) bp.getById(Caratula.class, this.idCaratula);
		if ((this.idCaratula != null && idCaratula.longValue() > 0) && (nroLote == null || nroLote.longValue() == 0)) {
			try {
				result = handler.getLotes(idCaratula);
			} catch (HibernateException e) {
				e.printStackTrace();
				return false;
			}
			forward = "ConsultarLotes"; // En PLURAL
			// return true;
		} else if (this.nroLote != null && nroLote.longValue() > 0
				&& (delete == null || delete.booleanValue() == false)) {
			// Consultar Lote
			List[] result = new List[2];
			// Como resultado tengo dos listas que env�o en un array de dos elementos
			// [0] Lista de medios de pago
			// [1] Lista de Boletos

			result[0] = handler.getCobropagos(this.idCaratula, this.nroLote);
			result[1] = handler.getBoletosRecibo(this.nroLote);
			// result[2]=handler.getBoletos(this.idCaratula, this.nroLote);

			forward = "ConsultarLote"; // En SINGULAR

			this.result = result;
			// return true;
		}
		if (accion != null && accion.equals("imprimir")) {
			imprimir();
			if (errores.isEmpty()) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.idCaratula", idCaratula);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.nroLote", nroLote);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.accion", "volverPDF");
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.metodoCarga", metodoCarga);
				forward = "ReportesProcess2";
			}
			return errores.isEmpty();
		} else if (accion != null && accion.equals("imprimirCobranza")) {
			imprimirCobranza();
			if (errores.isEmpty()) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.idCaratula", idCaratula);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.nroLote", nroLote);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.accion", "volverPDF");
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.metodoCarga", metodoCarga);
				forward = "ReportesProcess2";
			}
			return errores.isEmpty();
		} else if (accion != null && accion.equals("resumenCobranza")) {
			resumenCobranza();
			if (errores.isEmpty()) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.idCaratula", idCaratula);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.nroLote", nroLote);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.accion", "volverPDF");
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.metodoCarga", metodoCarga);
				forward = "ReportesProcess2";
			}
			return errores.isEmpty();
		} else if (accion != null && accion.equals("resumenCobranzaAnalitico")) {
			resumenCobranzaAnalitico();
			if (errores.isEmpty()) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.idCaratula", idCaratula);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.nroLote", nroLote);
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.accion", "volverPDF");
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConsultarLotes.metodoCarga", metodoCarga);
				forward = "ReportesProcess2";
			}
			return errores.isEmpty();
		}

		return errores.isEmpty();
	}

	public void imprimirCobranza() {
		reporte = new ReportResult();
		reporte.setExport("PDF");
		reporte.setReportName("cobranza.jasper");
		reporte.setParams("CARATULA_ID=" + getIdCaratula() + ";SCHEMA=" + BusinessPersistance.getSchema()
				+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

	}

	public void resumenCobranza() {
		reporte = new ReportResult();
		reporte.setExport("PDF");
		reporte.setReportName("cobranzaresumen.jasper");
		reporte.setParams("CARATULA_ID=" + getIdCaratula() + ";SCHEMA=" + BusinessPersistance.getSchema()
				+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
	}

	private void resumenCobranzaAnalitico() {
		reporte = new ReportResult();
		reporte.setExport("PDF");
		reporte.setReportName("cobranzaresumenanalitico1.jasper");
		reporte.setParams("CARATULA_ID=" + getIdCaratula() + ";SCHEMA=" + BusinessPersistance.getSchema()
				+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
	}

	@SuppressWarnings("unchecked")
	public void imprimir() {
		Pagos pago = (Pagos) bp
				.createSQLQuery("SELECT * FROM Pagos p WHERE p.recibo_id = :recibo_id AND p.caratula_id = :caratula_id")
				.addEntity(Pagos.class).setLong("recibo_id", getIdBoleto()).setLong("caratula_id", getIdCaratula())
				.setMaxResults(1).uniqueResult();
		try {
			Boleto b = (Boleto) bp.getById(Boleto.class, idBoleto);
			String expediente = b.getObjetoi().getExpediente();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			String consultaBasica = "SELECT d.calleNom, d.numero, d.manzana, b.DESC_BRR, l.CP, l.NOMBRE, d.departamentoNom, d.lote, pr.DETA_08 "
					+ "FROM Objetoi ob LEFT JOIN Persona pe on ob.persona_IDPERSONA = pe.IDPERSONA "
					+ "LEFT JOIN DomicilioObjetoi do on do.objetoi_id = ob.id  "
					+ "LEFT JOIN Domicilio d on do.domicilio_id = d.id "
					+ "LEFT JOIN LOCALIDAD l on l.IDLOCALIDAD = d.localidad_IDLOCALIDAD "
					+ "LEFT JOIN BARRIO b on b.CODI_BRR = d.barrio_CODI_BRR "
					+ "LEFT JOIN PROVIN pr on pr.CODI_08 = d.provincia_CODI_08 WHERE ob.id = :objetoi_id";
			String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> domicilios = bp.createSQLQuery(consultaLimitada)
					.setLong("objetoi_id", b.getObjetoi().getId()).list();
			Object[] domicilio;
			if (!domicilios.isEmpty()) {
				domicilio = domicilios.get(0);
			} else {
				domicilio = null;
			}

			int anioActual = Ejercicio.getEjercicioActual().intValue();

			consultaBasica = "SELECT b.tiponrocuenta, CASE WHEN idmoneda = 1 THEN 'Pesos' WHEN idmoneda = 2 THEN 'D�lares' WHEN idmoneda = 3 THEN 'Euros' ELSE CAST(idmoneda AS varchar) END AS 'moneda', "
					+ "CASE WHEN tipoCta = 'CC' THEN 'Cuenta Corriente' WHEN tipoCta = 'CA' THEN 'Caja de Ahorro' ELSE tipoCta END AS 'tipoCta' "
					+ "FROM " + this.esquemaGAF + ".ParametrizacionBancos p INNER JOIN " + this.esquemaGAF
					+ ".Bancue b ON b.idBancue = p.idBancue WHERE p.codi_Ba = :codiBa AND p.ejercicio = :ejercicio";
			consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> listado = bp.createSQLQuery(consultaLimitada)
					.setParameter("codiBa", pago.getId().getCaratula().getBanco().getCodiBa())
					.setParameter("ejercicio", anioActual).list();

			String calleNom = domicilio == null || domicilio[0] == null ? null : domicilio[0].toString();
			String numero = domicilio == null || domicilio[1] == null ? null : domicilio[1].toString();
			String manzana = domicilio == null || domicilio[2] == null ? null : domicilio[2].toString();
			String barrioNom = domicilio == null || domicilio[3] == null ? "" : domicilio[3].toString();
			String cp = domicilio == null || domicilio[4] == null ? null : domicilio[4].toString();
			String nombre = domicilio == null || domicilio[5] == null ? null : domicilio[5].toString();
			String departamentoNom = domicilio == null || domicilio[6] == null ? null : domicilio[6].toString();
			String lote = domicilio == null || domicilio[7] == null ? null : domicilio[7].toString();
			String provin = domicilio == null || domicilio[8] == null ? null : domicilio[8].toString();

			String medioPago = (String) bp
					.createQuery("SELECT c.mediopago.denominacion "
							+ "FROM Bolepago bp JOIN bp.id.cobropago c WHERE bp.id.boleto = :boleto")
					.setEntity("boleto", pago.getBoleto()).uniqueResult();

			String tipoNumeroCuenta = "";
			String tipoCta = "";
			String banco = "";
			String moneda = "";
			if (!listado.isEmpty()) {
				Object[] object = listado.get(0);
				tipoNumeroCuenta = (String) object[0];
				moneda = (String) object[1];
				tipoCta = (String) object[2];
			}

			boolean cambioMoneda = false;
			Long idMonedaCredito = b.getObjetoi().getMoneda_id() != null ? b.getObjetoi().getMoneda_id()
					: b.getObjetoi().getLinea().getMoneda_id();
			if (idMonedaCredito == null) {
				idMonedaCredito = 1L;
			}
			if (!idMonedaCredito.equals(1L)) {
				// el credito no esta en pesos
				cambioMoneda = pago.getMoneda() != null && !pago.getMoneda().getId().equals(idMonedaCredito);
			}

			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("pago.jasper");
			reporte.setParams("PROYECTO=" + b.getObjetoi().getNumeroAtencionStr() + ";TITULAR="
					+ b.getObjetoi().getPersona().getNomb12Str() + ";BOLETO_ID=" + idBoleto + ";CUIT="
					+ b.getObjetoi().getPersona().getCuil12Str() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId
					+ ";MONEDA="
					+ (pago.getMoneda() != null ? pago.getMoneda().getDenominacion()
							: b.getObjetoi().getLinea().getMoneda().getDenominacion())
					+ ";EXPEDIENTE=" + expediente + ";numeroBoleto=" + b.getNumeroBoleto() + ";medioPago=" + medioPago
					+ ";FECHA_EMISION=" + b.getFechaEmision().getTime() + ";calleNom=" + calleNom + ";numero=" + numero
					+ ";manzana=" + manzana + ";barrioNom=" + barrioNom + ";cp=" + cp + ";nombre=" + nombre
					+ ";departamentoNom=" + departamentoNom + ";lote=" + lote + ";provincia=" + provin + ";idObjetoi="
					+ b.getObjetoi().getId() + ";SCHEMA=" + BusinessPersistance.getSchema() + ";TIPO_NUMERO_CUENTA="
					+ tipoNumeroCuenta + ";TIPO_CTA=" + tipoCta + ";BANCO="
					+ pago.getId().getCaratula().getBanco().getProveedor().getFantasia()
					+ (cambioMoneda && pago.getMontoOriginal() != null
							? ";montoOriginal=" + DoubleHelper.getString(pago.getMontoOriginal())
							: "")
					+ (cambioMoneda && pago.getCotizacion() != null
							? ";cotizacion=" + DoubleHelper.getString(pago.getCotizacion())
							: "")
					+ (cambioMoneda && pago.getMoneda() != null
							? ";monedaOriginal=" + pago.getMoneda().getDenominacion()
							: "")
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

			String cons = SessionHandler.getCurrentSessionHandler().getRequest().getParameter("consultaLotes");

			reporte.setHref(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ "/actions/process.do?do=process&processName=ConsultarLotes&process.metodoCarga=" + metodoCarga
					+ "&process.idCaratula=" + idCaratula + (cons != null ? "&consultaLotes" : ""));
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "pago"));
		}
	}

	public HashMap<String, Object> getErrors() {
		return errores;
	}

	public String getForward() {
		return forward;
	}

	public String getInput() {
		return "ConsultarLotes";
	}

	public Object getResult() {
		return reporte;
	}

	public boolean validate() {
		return true;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}

	public Caratula getCaratula() {
		return this.caratula;
	}

	public void setCaratula(Caratula caratula) {
		this.caratula = caratula;
	}

	public Long getNroLote() {
		return nroLote;
	}

	public void setNroLote(Long nroLote) {
		this.nroLote = nroLote;
	}

	public void setDelete(Boolean delete) {
		this.delete = delete;
	}

	public String[] getLista() {
		return lista;
	}

	public void setLista(String[] lista) {
		this.lista = lista;
	}

	public String getUsuarioSesion() {
		return SessionHandler.getCurrentSessionHandler().getCurrentUser();
	}

	public Object getResultado() {
		return result;
	}

	public Long getIdBoleto() {
		return idBoleto;
	}

	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Integer getMetodoCarga() {
		return metodoCarga;
	}

	public void setMetodoCarga(Integer metodoCarga) {
		this.metodoCarga = metodoCarga;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}
}
