package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.asf.gaf.hibernate.Expegasto;
import com.asf.gaf.hibernate.documentos.DocumentoADE;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;

public class CreditoExpediente extends ConversationProcess {

	private Long idCredito;
	private String expediente;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean guardar() {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);

		List<DocumentoADE> expAde = bp.createQuery("select d from DocumentoADE d where d.identificacion = :exp")
				.setParameter("exp", expediente).list();
		if (expAde.isEmpty()) {
			errors.put("Solicitud.expediente.ade", "Solicitud.expediente.ade");
			return false;
		}

		Number count = (Number) bp.createQuery("select count(o) from Objetoi o where o.expediente = :exp")
				.setParameter("exp", expediente).uniqueResult();
		if (count != null && count.intValue() > 0) {
			errors.put("Solicitud.expediente.existe", "Solicitud.expediente.existe");
			return false;
		}

		List<Expegasto> expGaf = bp
				.createQuery("select e from Expegasto e where e.expediente = :exp order by e.ejercicio desc")
				.setParameter("exp", expediente).list();
		if (!expGaf.isEmpty()) {
			Calendar c = Calendar.getInstance();
			int ejercicio = c.get(Calendar.YEAR);
			Expegasto expEjercicio = null;
			for (Expegasto exp : expGaf) {
				if (exp.getEjercicio().intValue() == ejercicio) { // corresponde al ejercicio actual
					expEjercicio = exp;
					break;
				}
			}
			if (expEjercicio == null) {
				// no hay exp en el ejercicio actual
				errors.put("Solicitud.expediente.ejercicio", "Solicitud.expediente.ejercicio");
			} else {
				// buscar imputaciones de exp actual
				String schema = DirectorHelper.getString("SGAF");
				List<Object[]> imputaciones = bp.createSQLQuery("select i.importe, e.orden from " + schema
						+ ".Imputacion i " + "inner join " + schema + ".Etapa e on i.idetapa = e.idetapa "
						+ "where i.idexpegasto = " + expEjercicio.getId() + " order by e.orden").list();
				for (Object[] imp : imputaciones) {
					if (((Long) imp[1]).longValue() != 1L
							|| credito.getFinanciamiento().doubleValue() != ((Double) imp[0]).doubleValue()) {
						errors.put("Solicitud.expediente.imputaciones", "Solicitud.expediente.imputaciones");
						break;
					}
				}
			}
		}

		bp.begin();

		credito.setExpediente(expediente);

		if (credito.getEstadoActual() == null
				|| !credito.getEstadoActual().getEstado().getNombreEstado().equals(Estado.ANALISIS)) {
			if (credito.getEstadoActual() != null) {
				credito.getEstadoActual().setFechaHasta(new Date());
			}

			ObjetoiEstado estadoAnalisis = new ObjetoiEstado();
			estadoAnalisis.setFecha(new Date());

			List<Estado> estados = bp.createQuery("select e from Estado e where e.nombreEstado = :estado")
					.setParameter("estado", Estado.ANALISIS).list();

			if (!estados.isEmpty()) {
				Estado estado = estados.get(0);
				estadoAnalisis.setEstado(estado);
			}
			estadoAnalisis.setObjetoi(credito);

			bp.save(estadoAnalisis);
		}

		errors.put("Solicitud.expediente.ok", "Solicitud.expediente.ok");

		bp.update(credito);
		bp.commit();
		return false; // false = muestra mensajes
	}

	@Override
	protected String getDefaultForward() {
		return "CreditoExpediente";
	}

	public String getForwardURL() {
		return "/actions/process.do?do=process&processName=SolicitudesHandler&process.accion=show&process.idSolicitud="
				+ idCredito;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

}
