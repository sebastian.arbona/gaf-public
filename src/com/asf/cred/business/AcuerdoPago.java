package com.asf.cred.business;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Domicilio;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.CalculoEmergencia;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DomicilioObjetoi;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.Inmovilizaciones;
import com.nirven.creditos.hibernate.Liberaciones;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;
import com.sun.org.apache.commons.beanutils.BeanUtils;

public class AcuerdoPago implements IProcess {

	private Long idPersona;
	private HashMap<String, Object> errores;
	private String action;
	private BusinessPersistance bp;
	private List<Objetoi> creditosDeuda;
	private String forward;
	private Long cantidadCuotasCapital;
	private Long cantidadCuotasInteres;

	private Date primerVencCapital;
	private Date primerVencInteres;
	private Date segundoVencCapital;
	private Date segundoVencInteres;
	private List<Persona> personas;
	private Long idMoneda;
	private String periodicidadCapital;
	private String periodicidadInteres;
	private boolean falta;
	private ArrayList<Objetoi> creditosSeleccionados;
	private Map<String, Objetoi> creditosSeleccionadosPorId = new HashMap<String, Objetoi>();
	private String expediente;
	private boolean faltaExpediente = false;
	private boolean exito = false;
	private String nroAtencion;
	private String nroResolucion;

	// LOS SIGUIENTES ATRIBUTOS SE UTILIZAN PARA SETEAR LOS INDICES
	private ObjetoiIndice objetoIndice;
	private Long idIndice;
	private Long idCompensatorio;
	private Long idMoratorio;
	private Long idPunitorio;
	private String tipoTasa;
	private String tipoCalculo;
	private String indiceValor;
	private Double valorMas;
	private Double valorPor;
	private Double topeTasa;
	private Integer diasAntes;

	private Double valorFinal;
	private Double tasaCompensatorio;
	private Double tasaPunitorio;
	private Double tasaMoratorio;
	private Date fechaAcuerdo;

	ObjetoiIndice compensatorio;
	ObjetoiIndice moratorio;
	ObjetoiIndice punitorio;
	Objetoi nuevoCredito;
	Desembolso desembolso;
	ObjetoiEstado estadoCreditoNormal;
	ArrayList<Desembolso> desembolsos;
	GarantiaEstado garantiaestado;

	private boolean generada;
	private List<Objetoi> acuerdos;
	private List<Ctacte> movimientos;

	private Long idLinea;
	private List<Linea> lineas;
	private List<String> expedientes;
	private String forwardURL;
	private DomicilioObjetoi domicObjetoi;
	private int tipoJudicial;

	public AcuerdoPago() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();

		fechaAcuerdo = new Date();
		primerVencCapital = fechaAcuerdo;
		primerVencInteres = fechaAcuerdo;
	}

	@Override
	public boolean doProcess() {
		buscarMoneda();
		if (action == null) {
			inicializarTasas();
			ObjetoiIndice indiceComp = new ObjetoiIndice();
			ObjetoiIndice indiceMor = new ObjetoiIndice();
			ObjetoiIndice indicePun = new ObjetoiIndice();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("AcuerdoPago.IndiceCompensatorio", indiceComp);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("AcuerdoPago.IndiceMoratorio", indiceMor);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("AcuerdoPago.IndicePunitorio", indicePun);
			forward = "AcuerdoPago";
			return true;
		} else if (action.equals("search")) {
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("AcuerdoPago.idPersona",
					idPersona);
			inicializarTasas();
			forward = "AcuerdoPago";
			return buscarCreditos();
		} else if (action.equals("save")) {
			if (validarDatos() && !expediente.equals("")) {
				generada = true;
				idPersona = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("AcuerdoPago.idPersona");
				forward = "AcuerdoPago";
				inicializarTasas();
				boolean resultado = generarAcuerdoPago();
				this.action = "search";
				return resultado;
			} else if (expediente.equals("")) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("AcuerdoPago.idPersona", idPersona);
				faltaExpediente = true;
				errores.put("acuerdoPago.faltaExpediente", "acuerdoPago.faltaExpediente");
				forward = "AcuerdoPago";
				buscarCreditos();
				return false;
			} else {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("AcuerdoPago.idPersona", idPersona);
				falta = true;
				forward = "AcuerdoPago";
				buscarCreditos();
				return false;
			}
		} else if (action.equalsIgnoreCase("loadIndice")) {
			if (tipoTasa.equalsIgnoreCase("compensatorio")) {
				objetoIndice = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("AcuerdoPago.IndiceCompensatorio");
			} else if (tipoTasa.equalsIgnoreCase("moratorio")) {
				objetoIndice = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("AcuerdoPago.IndiceMoratorio");
			} else if (tipoTasa.equalsIgnoreCase("punitorio")) {
				objetoIndice = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("AcuerdoPago.IndicePunitorio");
			}
			if (objetoIndice != null && objetoIndice.getIndice() != null) {
				cargarDatosIndice();
			} else {
				inicializarValores();
			}
			buscarCreditosSeleccionados();
			forward = "AcuerdoIndice";
		} else if (action.equalsIgnoreCase("saveIndice")) {
			if (idIndice != null && idIndice != 0) {
				if (tipoTasa.equalsIgnoreCase("compensatorio")) {
					objetoIndice = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.getAttribute("AcuerdoPago.IndiceCompensatorio");
				} else if (tipoTasa.equalsIgnoreCase("moratorio")) {
					objetoIndice = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.getAttribute("AcuerdoPago.IndiceMoratorio");
				} else if (tipoTasa.equalsIgnoreCase("punitorio")) {
					objetoIndice = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.getAttribute("AcuerdoPago.IndicePunitorio");
				}
				if (objetoIndice == null) {
					objetoIndice = new ObjetoiIndice();
				}
				cargarIndice();
			}
			idPersona = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("AcuerdoPago.idPersona");
			buscarCreditosSeleccionados();
			forward = "AcuerdoPago";
			return buscarCreditos();

		} else if (action.equalsIgnoreCase("cancelarIndice")) {

			idPersona = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("AcuerdoPago.idPersona");
			buscarCreditosSeleccionados();
			forward = "AcuerdoPago";
			return buscarCreditos();
		}
		return true;
	}

	private void inicializarTasas() {
		tasaCompensatorio = 0.0;
		tasaMoratorio = 0.0;
		tasaPunitorio = 0.0;
	}

	private void inicializarValores() {
		valorMas = 0.0;
		valorPor = 1.0;
		topeTasa = 0.0;
		diasAntes = 0;
		tipoCalculo = null;
		idIndice = 0L;
		indiceValor = "";
		valorFinal = 0.0;
	}

	private void cargarIndice() {
		objetoIndice.setDiasAntes(diasAntes);
		objetoIndice.setValorMas(valorMas);
		objetoIndice.setValorPor(valorPor);
		objetoIndice.setTasaTope(topeTasa);
		objetoIndice.setTipoTasa(tipoTasa);
		objetoIndice.setTipoCalculo(tipoCalculo);
		objetoIndice.setIndice((Indice) bp.getById(Indice.class, idIndice));

		if (tipoTasa.equalsIgnoreCase("compensatorio")) {
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("AcuerdoPago.IndiceCompensatorio", objetoIndice);
			tasaCompensatorio = objetoIndice.getIndice().getValorInicial();
		} else if (tipoTasa.equalsIgnoreCase("moratorio")) {
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("AcuerdoPago.IndiceMoratorio", objetoIndice);
			tasaMoratorio = objetoIndice.getIndice().getValorInicial();
		} else if (tipoTasa.equalsIgnoreCase("punitorio")) {
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("AcuerdoPago.IndicePunitorio", objetoIndice);
			tasaPunitorio = objetoIndice.getIndice().getValorInicial();
		}
	}

	private void cargarDatosIndice() {
		valorMas = objetoIndice.getValorMas();
		valorPor = objetoIndice.getValorPor();
		setTopeTasa(objetoIndice.getTasaTope());

		diasAntes = objetoIndice.getDiasAntes();
		tipoCalculo = objetoIndice.getTipoCalculo();
		idIndice = objetoIndice.getIndice().getId();
		indiceValor = objetoIndice.getIndice().getValorInicialStr();
		valorFinal = objetoIndice.getValorFinal();
	}

	private boolean buscarCreditos() {
		if (idPersona == null || idPersona == 0) {
			return false;
		}
		Persona persona = (Persona) bp.getById(Persona.class, idPersona);
		personas = new ArrayList<Persona>();
		if (persona != null) {
			personas.add(persona);
		}
		inicializarCreditos();
		cargarIndices();
		buscarMoneda();
		buscarAcuerdos();
		return true;
	}

	@SuppressWarnings("unchecked")
	private void buscarAcuerdos() {
		acuerdos = bp.createQuery("select distinct o.acuerdoPago from Objetoi o where o.persona.id = :idPersona")
				.setParameter("idPersona", idPersona).list();
	}

	private void cargarIndices() {
		ObjetoiIndice comp = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.IndiceCompensatorio");
		ObjetoiIndice mora = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.IndiceMoratorio");
		ObjetoiIndice puni = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.IndicePunitorio");
		if (comp != null) {
			tasaCompensatorio = comp.getValorFinal();
		}
		if (mora != null) {
			tasaMoratorio = mora.getValorFinal();
		}
		if (puni != null) {
			tasaPunitorio = puni.getValorFinal();
		}
	}

	@SuppressWarnings("unchecked")
	private void inicializarCreditos() {
		creditosDeuda = new ArrayList<Objetoi>();
		String consulta = "SELECT c FROM Objetoi c WHERE c.persona.id = :idPersona"
				+ " AND c.id NOT IN (SELECT ce.acuerdo.id FROM CalculoEmergencia ce WHERE ce.acuerdo IS NOT NULL AND ce.persona.id = :idPersona)"
				+ " ORDER BY c.primerVencCapital";
		creditosDeuda = bp.createQuery(consulta).setLong("idPersona", idPersona).list();

		expedientes = new ArrayList<String>();
		for (Objetoi creditoOriginal : creditosDeuda) {
			expedientes.add(creditoOriginal.getExpediente());
		}
	}

	private void buscarMoneda() {
		idMoneda = null; // tomar la del credito anterior
	}

	private boolean generarAcuerdoPago() {
		buscarMoneda();
		buscarCreditos();
		CreditoHandler handler = new CreditoHandler();
		double deudaTotal = 0;
		if (idLinea == null) {
			errores.put("acuerdoPago.error.linea", "acuerdoPago.error.linea");
			return false;
		}
		if (creditosSeleccionados == null || creditosSeleccionados.isEmpty()) {
			buscarCreditosSeleccionados();
		}
		boolean encontradoExp = false;
		for (Objetoi credSelec : creditosSeleccionados) {
			if (credSelec.getExpediente().equals(expediente)) {
				encontradoExp = true;
			}
		}
		if (!encontradoExp) {
			errores.put("acuerdoPago.error.expedienteSeleccionado", "acuerdoPago.error.expedienteSeleccionado");
			return false;
		}
		boolean resultado = false;
		boolean tieneEmergencia = false;
		for (Objetoi credito : creditosSeleccionados) {
			if (credito.getLinea().isLineaEmergencia()) {
				tieneEmergencia = true;
				break;
			}
		}
		compensatorio = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.IndiceCompensatorio");
		moratorio = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.IndiceMoratorio");
		punitorio = (ObjetoiIndice) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.IndicePunitorio");
		if (compensatorio != null && moratorio != null && punitorio != null) {
			if (tieneEmergencia) {
				resultado = crearAcuerdoEmergencia(handler, deudaTotal);
			} else {
				resultado = crearAcuerdoNoEmergencia(handler, deudaTotal);
			}
		} else {
			errores.put("acuerdoPago.error.indices", "acuerdoPago.error.indices");
			resultado = false;
		}
		if (resultado) {
			this.errores.put("errors.detail", new ActionMessage("errors.detail",
					"Se ha creado el Acuerdo de Pago nro" + nuevoCredito.getNumeroAtencionStr() + " correctamente"));
			nroAtencion = nuevoCredito.getNumeroAtencionStr();
			exito = true;

			forward = "ProcessRedirect";
			forwardURL = "/actions/process.do?do=process&processName=CreditoProcess&process.idObjetoi="
					+ nuevoCredito.getId() + "&process.action=buscar&process.personaId=" + nuevoCredito.getPersona_id();
		}
		return resultado;
	}

	private boolean crearAcuerdoEmergencia(CreditoHandler handler, double deudaTotal) {
		nuevoCredito = new Objetoi();
		desembolso = new Desembolso();
		estadoCreditoNormal = new ObjetoiEstado();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		boolean resultado = false;
		try {
			bp.begin();
			String consulta = "SELECT ce FROM CalculoEmergencia ce WHERE ce.persona.id = :idPersona "
					+ " AND ce.acuerdo IS NOT NULL AND ce.fecha >= :fecha AND ce.fecha <= :fecha2";
			CalculoEmergencia calculoEmergencia;
			try {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.set(Calendar.HOUR, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				Calendar calendar2 = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.set(Calendar.HOUR, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				calendar.set(Calendar.MILLISECOND, 999);
				calculoEmergencia = (CalculoEmergencia) bp.createQuery(consulta).setParameter("idPersona", idPersona)
						.setParameter("fecha", calendar.getTime()).setParameter("fecha2", calendar2.getTime())
						.uniqueResult();
				if (calculoEmergencia == null) {
					calculoEmergencia = crearCalculoEmergencia();
				}
			} catch (Exception e) {
				e.printStackTrace();
				calculoEmergencia = crearCalculoEmergencia();
			}
			desembolsos = new ArrayList<Desembolso>();
			if (!creditosDeuda.isEmpty()) {
				// armo los datos del desembolso
				crearDesembolso(new Double(calculoEmergencia.getTotal().replace(".", "").replace(",", ".")));
				desembolso.setImporteReal(new Double(calculoEmergencia.getTotal().replace(".", "").replace(",", ".")));
				// armo los datos del credito
				armarDatosCredito(calculoEmergencia.getPersona());
				// armo los datos de intereses;
				armarDatosComunes();
			}
			guardar();
			handler.ejecutarDesembolso(nuevoCredito, desembolso);
			calculoEmergencia.setAcuerdo(nuevoCredito);
			bp.saveOrUpdate(calculoEmergencia);
			bp.commit();
			resultado = true;
		} catch (NumberFormatException e) {
			errores.put("acuerdoPago.error.crearAcuerdoEmergencia", "acuerdoPago.error.crearAcuerdoEmergencia");
			bp.rollback();
			e.printStackTrace();
		} catch (HibernateException e) {
			errores.put("acuerdoPago.error.crearAcuerdoEmergencia", "acuerdoPago.error.crearAcuerdoEmergencia");
			bp.rollback();
			e.printStackTrace();
		} catch (Exception e) {
			errores.put("acuerdoPago.error.crearAcuerdoEmergencia", "acuerdoPago.error.crearAcuerdoEmergencia");
			bp.rollback();
			e.printStackTrace();
		}

		return resultado;
	}

	private CalculoEmergencia crearCalculoEmergencia() {
		CalculoEmergenciaProcess calculoEmergenciaProcess = new CalculoEmergenciaProcess();
		calculoEmergenciaProcess.setAccion("calcular");
		calculoEmergenciaProcess.setIdPersona(idPersona);
		calculoEmergenciaProcess.setpFecha(new Date());
		calculoEmergenciaProcess.doProcess();
		errores.put("acuerdoPago.calculoEmergencia", "acuerdoPago.calculoEmergencia");
		return calculoEmergenciaProcess.getRd();
	}

	@SuppressWarnings("unchecked")
	private void armarDatosComunes() {

		ObjetoiEstado estadoCreditoAux = new ObjetoiEstado();

		compensatorio.setCredito(nuevoCredito);
		compensatorio.setTipoTasa("1"); // compensatorio
		moratorio.setCredito(nuevoCredito);
		moratorio.setTipoTasa("2"); // moratorio
		punitorio.setCredito(nuevoCredito);
		punitorio.setTipoTasa("3"); // punitorio
		estadoCreditoNormal.setFecha(new Date());
		estadoCreditoAux.setFecha(getPrimerDiaDelMes());
		estadoCreditoAux.setFechaHasta(estadoCreditoNormal.getFecha());
		List<Estado> estadosNormal = bp.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = :estado")
				.setParameter("estado", "EJECUCION").list();
		if (!estadosNormal.isEmpty()) {
			Estado estadoNormal = estadosNormal.get(0);
			estadoCreditoNormal.setEstado(estadoNormal);
			estadoCreditoAux.setEstado(estadoNormal);
		}
		estadoCreditoNormal.setObjetoi(nuevoCredito);
		estadoCreditoAux.setObjetoi(nuevoCredito);
		estadoCreditoAux.setObservacion("estado generado autom�ticamente");
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		Linea lineaAcuerdoPago = (Linea) bp.getById(Linea.class, idLinea);
		nuevoCredito.setLinea_id(idLinea);
		nuevoCredito.setLinea(lineaAcuerdoPago);

		// Creo el comportamientoPago

		ObjetoiComportamiento comportamientoNuevo = new ObjetoiComportamiento();
		comportamientoNuevo.setObjetoi(nuevoCredito);
		comportamientoNuevo.setFecha(new Date());
		comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
		bp.saveOrUpdate(nuevoCredito);
		bp.saveOrUpdate(comportamientoNuevo);
		bp.save(estadoCreditoAux);
	}

	private boolean crearAcuerdoNoEmergencia(CreditoHandler handler, double deudaTotal) {
		nuevoCredito = new Objetoi();
		desembolso = new Desembolso();
		estadoCreditoNormal = new ObjetoiEstado();
		movimientos = new ArrayList<Ctacte>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		boolean resultado = false;
		try {
			bp.begin();
			for (Objetoi creditoOriginal : creditosSeleccionados) {
				double moratorio = handler.calcularMoratorioAcuerdoPago(creditoOriginal, fechaAcuerdo);
				double punitorio = handler.calcularPunitorioAcuerdoPago(creditoOriginal, fechaAcuerdo);
				generarDebitos(creditoOriginal, moratorio, punitorio);
				if (creditoOriginal.getLinea().getMoneda().getId() != 1
						|| creditoOriginal.getMoneda() != null && creditoOriginal.getMoneda().getId() != 1) {
					try {
						handler.realizarAjusteMoneda(creditoOriginal.getId(), fechaAcuerdo);
					} catch (Exception e) {
						System.err.println("Error realizando ajuste cambio en acuerdo de pago: " + e.getMessage());
						e.printStackTrace();
					}
				}
				BeanCtaCte deuda = calcularDeudaImponible(creditoOriginal.getId());
				double deudaImponible = deuda.getCapital() + deuda.getCompensatorio() + deuda.getGastos()
						+ deuda.getMultas() + deuda.getMoratorio() + deuda.getPunitorio();
				generarCreditos(creditoOriginal, deuda);
				deudaTotal += deudaImponible;
				if (idMoneda == null) {
					idMoneda = creditoOriginal.getLinea().getMoneda_id();
				}
			}
			desembolsos = new ArrayList<Desembolso>();
			if (!creditosDeuda.isEmpty()) {
				// armo el desembolso;
				crearDesembolso(deudaTotal);
				// armo los datos del credito
				armarDatosCredito(creditosDeuda.get(0).getPersona());
				// armo datos intereses
				armarDatosComunes();
			}
			guardar();
			handler.setSegundoVencimientoCapital(segundoVencCapital);
			handler.setSegundoVencimientoCompensatorio(segundoVencInteres);
			handler.ejecutarDesembolso(nuevoCredito, desembolso);
			bp.commit();
			resultado = true;
		} catch (NumberFormatException e) {
			bp.rollback();
			e.printStackTrace();
		} catch (HibernateException e) {
			bp.rollback();
			e.printStackTrace();
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
		}

		return resultado;
	}

	@SuppressWarnings("unchecked")
	private void generarCreditos(Objetoi credito, BeanCtaCte deuda) {
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();

		Calendar c = Calendar.getInstance();
		int periodo = c.get(Calendar.YEAR);

		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;

		// boleto tipo nota de credito
		Boleto notaCredito = new Boleto();
		notaCredito.setFechaEmision(fechaAcuerdo);
		notaCredito.setFechaVencimiento(fechaAcuerdo);
		notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
		notaCredito.setUsuario(usuario);
		// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna
		// cuota
		notaCredito.setObjetoi(credito);
		notaCredito.setPeriodoBoleto((long) periodo);
		notaCredito.setVerificadorBoleto(0L);
		notaCredito.setImporte(deuda.getCapital() + deuda.getCompensatorio() + deuda.getMoratorio()
				+ deuda.getPunitorio() + deuda.getGastos() + deuda.getMultas());
		notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
		bp.save(notaCredito);

		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", credito.getId()).list();
		Cuota ultimaCuota = cuotas.get(cuotas.size() - 1);

		if (deuda.getCapital() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_CAPITAL, deuda.getCapital(), fechaAcuerdo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago", ultimaCuota);
			itemCtaCte++;
		}
		if (deuda.getCompensatorio() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_COMPENSATORIO, deuda.getCompensatorio(), fechaAcuerdo,
					notaCredito, movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago",
					ultimaCuota);
			itemCtaCte++;
		}
		if (deuda.getMoratorio() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_MORATORIO, deuda.getMoratorio(), fechaAcuerdo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago", ultimaCuota);
			itemCtaCte++;
		}
		if (deuda.getPunitorio() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_PUNITORIO, deuda.getPunitorio(), fechaAcuerdo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago", ultimaCuota);
			itemCtaCte++;
		}
		if (deuda.getGastosSimples() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_GASTOS, deuda.getGastosSimples(), fechaAcuerdo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago", ultimaCuota);
			itemCtaCte++;
		}
		if (deuda.getGastosRec() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_GASTOS_A_RECUPERAR, deuda.getGastosRec(), fechaAcuerdo,
					notaCredito, movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago",
					ultimaCuota);
			itemCtaCte++;
		}
		if (deuda.getMultas() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_MULTAS, deuda.getMultas(), fechaAcuerdo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, "Nota de cr�dito por Acuerdo de Pago", ultimaCuota);
		}
	}

	@SuppressWarnings("unchecked")
	private void generarDebitos(Objetoi credito, double montoMoratorio, double montoPunitorio) {
		if (montoMoratorio > 0 || montoPunitorio > 0) {
			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
			Calendar c = Calendar.getInstance();
			int periodo = c.get(Calendar.YEAR);
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;
			// boleto tipo nota de debito
			Boleto notaDebito = new Boleto();
			notaDebito.setFechaEmision(fechaAcuerdo);
			notaDebito.setFechaVencimiento(fechaAcuerdo);
			notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			notaDebito.setUsuario(usuario);
			// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna
			// cuota
			notaDebito.setObjetoi(credito);
			notaDebito.setPeriodoBoleto((long) periodo);
			notaDebito.setVerificadorBoleto(0L);
			notaDebito.setImporte(montoMoratorio + montoPunitorio);
			notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
			bp.save(notaDebito);
			List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", credito.getId())
					.list();
			Cuota ultimaCuota = cuotas.get(cuotas.size() - 1);
			if (montoMoratorio > 0) {
				guardarConcepto(credito, Concepto.CONCEPTO_MORATORIO, montoMoratorio, fechaAcuerdo, notaDebito,
						movimientoCtaCte, itemCtaCte, tipoDebito, "Int. Moratorio", ultimaCuota);
				itemCtaCte++;
			}
			if (montoPunitorio > 0) {
				guardarConcepto(credito, Concepto.CONCEPTO_PUNITORIO, montoPunitorio, fechaAcuerdo, notaDebito,
						movimientoCtaCte, itemCtaCte, tipoDebito, "Int. Punitorio", ultimaCuota);
			}
		}
	}

	private void guardarConcepto(Objetoi objetoi, String concepto, double valor, Date fechaEmision, Boleto nota,
			long movimientoCtaCte, long itemCtaCte, Tipomov tipo, String detalle, Cuota cuota) {
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());

		Ctacte cc = objetoi.crearCtacte(concepto, valor, fechaEmision, nota, objetoi, cuota,
				Ctacte.TIPO_CONCEPTO_ACUERDO_PAGO);
		cc.setTipomov(tipo);
		if (tipo.getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
			cc.setTipoMovimiento("movManualDeb");
		} else {
			cc.setTipoMovimiento("movManualCred");
		}
		cc.setUsuario(usuario);
		cc.getId().setMovimientoCtacte(movimientoCtaCte);
		cc.getId().setItemCtacte(itemCtaCte);
		cc.setBoleto(nota);
		cc.setDetalle(detalle);

		// crear nuevo registro bolcon
		Bolcon bc = objetoi.crearBolcon(concepto, valor, nota, cc.getId(), cuota);
		bc.setTipomov(tipo);

		bp.save(cc);
		bp.save(bc);

		movimientos.add(cc);
	}

	@SuppressWarnings("unchecked")
	private BeanCtaCte calcularDeudaImponible(Long idObjetoi) {
		CreditoHandler h = new CreditoHandler();
		double deudaCapital = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_CAPITAL).setParameter("idCredito", idObjetoi).list());
		double deudaCompensatorio = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_COMPENSATORIO).setParameter("idCredito", idObjetoi).list());
		double saldoBonif = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_BONIFICACION).setParameter("idCredito", idObjetoi).list());
		double deudaGastos = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_GASTOS).setParameter("idCredito", idObjetoi).list());
		double deudaGastosRec = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_GASTOS_A_RECUPERAR).setParameter("idCredito", idObjetoi)
				.list());
		double deudaMultas = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_MULTAS).setParameter("idCredito", idObjetoi).list());
		double deudaMoratorio = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_MORATORIO).setParameter("idCredito", idObjetoi).list());
		double deudaPunitorio = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_PUNITORIO).setParameter("idCredito", idObjetoi).list());
		BeanCtaCte deudaTotal = new BeanCtaCte();
		deudaTotal.setCapital(deudaCapital);
		deudaTotal.setCompensatorio(deudaCompensatorio + saldoBonif); // saldoBonif viene negativo
		deudaTotal.setCompensatorioBruto(deudaCompensatorio);
		deudaTotal.setGastosRec(deudaGastosRec);
		deudaTotal.setGastosSimples(deudaGastos);
		deudaTotal.setMultas(deudaMultas);
		deudaTotal.setMoratorio(deudaMoratorio);
		deudaTotal.setPunitorio(deudaPunitorio);

		return deudaTotal;
	}

	@SuppressWarnings("unchecked")
	private void guardar()
			throws IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());

		nuevoCredito.setAsesor(usuario);
		bp.save(nuevoCredito);
		bp.save(estadoCreditoNormal);
		bp.save(desembolso);
		bp.save(compensatorio);
		bp.save(punitorio);
		bp.save(moratorio);
		desembolsos.add(desembolso);
		nuevoCredito.setDesembolsos(desembolsos);
		bp.update(nuevoCredito);

		if (domicObjetoi != null && domicObjetoi.getObjetoi() != null) {
			bp.save(domicObjetoi);
		}
		for (Objetoi creditoOriginal : creditosSeleccionados) {
			creditoOriginal.setAcuerdoPago(nuevoCredito);
			ObjetoiEstado estadoCreditoAcuerdo = new ObjetoiEstado();
			estadoCreditoAcuerdo.setFecha(new Date());
			if (creditoOriginal.getEstadoActual() != null) {
				creditoOriginal.getEstadoActual().setFechaHasta(new Date());
			}
			List<Estado> estadosAcuerdo = bp.createQuery("select e from Estado e where e.nombreEstado = :estado")
					.setParameter("estado", "ORIGINAL CON ACUERDO").list();
			if (!estadosAcuerdo.isEmpty()) {
				Estado estadoAcuerdo = estadosAcuerdo.get(0);
				estadoCreditoAcuerdo.setEstado(estadoAcuerdo);
			}
			estadoCreditoAcuerdo.setObjetoi(creditoOriginal);
			// Copia garantias del proyecto original con acuerdo al acuerdo vigente
			List<ObjetoiGarantia> garantiasOriginales = bp
					.createQuery("SELECT g from ObjetoiGarantia g where g.objetoi = :credito and g.baja is null")
					.setParameter("credito", creditoOriginal).list();
			for (ObjetoiGarantia garantiaOriginal : garantiasOriginales) {
				List<GarantiaEstado> estadosOriginales = bp
						.createQuery(
								"SELECT ge from GarantiaEstado ge where ge.objetoiGarantia.id = :idObjetoiGarantia ")
						.setParameter("idObjetoiGarantia", garantiaOriginal.getId()).list();
				// copia la garantia al acuerdo
				bp.getCurrentSession().evict(garantiaOriginal);
				ObjetoiGarantia objetoiGarantiaCopia;
				objetoiGarantiaCopia = garantiaOriginal;
				objetoiGarantiaCopia.setObjetoi(nuevoCredito);
				objetoiGarantiaCopia.setId(null);
				bp.save(objetoiGarantiaCopia);
				bp.getCurrentSession().refresh(garantiaOriginal);

				// Copia los estados originales de la garantia a la garantia del acuerdo
				Double importe = 0d;
				for (GarantiaEstado garantiaEstado : estadosOriginales) {
					bp.getCurrentSession().evict(garantiaEstado);
					GarantiaEstado garantiaEstadoCopia;
					garantiaEstadoCopia = garantiaEstado;
					garantiaEstadoCopia.setObjetoiGarantia(objetoiGarantiaCopia);
					garantiaEstadoCopia.setId(null);
					importe = garantiaEstadoCopia.getImporte();
					bp.save(garantiaEstadoCopia);
					bp.getCurrentSession().refresh(garantiaEstado);
				}
				// agrego a la nueva garantia del acuerdo un nuevo estado
				GarantiaEstado estadoAcuerdo = new GarantiaEstado(ComportamientoGarantiaEnum.ACUERDO, fechaAcuerdo,
						importe);
				estadoAcuerdo.setObjetoiGarantia(objetoiGarantiaCopia);
				bp.save(estadoAcuerdo);
			}

			// Si el proyecto tiene garantia fiduciaria, copia inmovilizaciones y/o
			// liberaciones del proyecto original con acuerdo al acuerdo vigente
			if (creditoOriginal.getTieneGarantiaFiduciaria()) {
				List<Inmovilizaciones> inmovilizacionesOriginalesList = bp
						.createQuery("SELECT i from Inmovilizaciones i where i.objetoi = :credito")
						.setParameter("credito", creditoOriginal).list();
				for (Inmovilizaciones inmovilizacionOriginal : inmovilizacionesOriginalesList) {
					bp.getCurrentSession().evict(inmovilizacionOriginal);
					Inmovilizaciones inmovilizacionCopia = inmovilizacionOriginal;
					inmovilizacionCopia.setObjetoi(nuevoCredito);
					inmovilizacionCopia.setId(null);
					bp.save(inmovilizacionCopia);
					bp.getCurrentSession().refresh(inmovilizacionOriginal);
				}
				List<Liberaciones> liberacionesOriginalList = bp
						.createQuery("SELECT l from Liberaciones l where l.objetoi = :credito")
						.setParameter("credito", creditoOriginal).list();
				for (Liberaciones liberacionOriginal : liberacionesOriginalList) {
					bp.getCurrentSession().evict(liberacionOriginal);
					Liberaciones liberacionCopia = liberacionOriginal;
					liberacionCopia.setObjetoi(nuevoCredito);
					liberacionCopia.setId(null);
					bp.save(liberacionCopia);
					bp.getCurrentSession().refresh(liberacionOriginal);
				}
			}
			bp.save(estadoCreditoAcuerdo);
			bp.update(creditoOriginal);
		}

		// se copian las auditorias al nuevo credito
		copiarAuditoriaFinal();

		com.nirven.creditos.hibernate.AcuerdoPago acuerdo = new com.nirven.creditos.hibernate.AcuerdoPago();
		acuerdo.setCreditoAcuerdo(nuevoCredito);
		acuerdo.setFecha(fechaAcuerdo);
		acuerdo.setMovimientos(movimientos);
		acuerdo.setJudicial(tipoJudicial);
		bp.save(acuerdo);
	}

//	/**
//	 * se dispara para agregar en el historial de las garantias cuando se hace un
//	 * nuevo acuerdo
//	 * 
//	 * @param objetoiGarantiaCopia
//	 * @param importe
//	 */
//	private void grabarHistorialGarantia(ObjetoiGarantia objetoiGarantiaCopia, double importe) {
//		GarantiaEstado miGarantiaEstado = new GarantiaEstado(ComportamientoGarantiaEnum.ACUERDO, fechaAcuerdo, importe);
//		miGarantiaEstado.setObjetoiGarantia(objetoiGarantiaCopia);
//		bp.save(miGarantiaEstado);
//	}

	/**
	 * Esta funcion copia las auditorias finales del credito original en el nuevo
	 */
	@SuppressWarnings("unchecked")
	private void copiarAuditoriaFinal() {
		// solo se hace la copia si se elije uno solo
		if (creditosSeleccionados.size() == 1) {
			for (Objetoi creditoSeleccionado : creditosSeleccionados) {
				List<AuditoriaFinal> auditoriasOriginales = bp.getNamedQuery("Auditoria.findByCredito")
						.setParameter("objetoiId", creditoSeleccionado.getId()).list();
				for (AuditoriaFinal auditoriaOriginal : auditoriasOriginales) {
					// desconecto la instancia
					bp.getCurrentSession().evict(auditoriaOriginal);
					// genero la copia sin id, hibernate debe asumir un nuevo registro
					AuditoriaFinal auditoriaCopia = auditoriaOriginal;
					auditoriaCopia.setId(null);
					auditoriaCopia.setCredito(nuevoCredito);
					// persiste la nueva copia
					bp.save(auditoriaCopia);
					// reconecta el registro original
					bp.getCurrentSession().refresh(auditoriaOriginal);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void armarDatosCredito(Persona persona) {
		Long numeroAtencion = Numerador.getNext(Objetoi.NUMERADOR_CREDITOS);
		nuevoCredito.setNumeroAtencion(numeroAtencion);
		nuevoCredito.setPrimerVencCapital(primerVencCapital != null ? primerVencCapital : fechaAcuerdo);
		nuevoCredito.setPrimerVencInteres(primerVencInteres != null ? primerVencInteres
				: (primerVencCapital != null ? primerVencCapital : fechaAcuerdo));
		nuevoCredito.setPlazoCapital(new Integer(cantidadCuotasCapital.intValue()));
		nuevoCredito.setPlazoCompensatorio(new Integer(cantidadCuotasInteres.intValue()));
		nuevoCredito.setFrecuenciaCapital(periodicidadCapital);
		nuevoCredito.setFrecuenciaInteres(periodicidadInteres);
		nuevoCredito.setMoneda_id(idMoneda);
		nuevoCredito.setPersona(persona);
		nuevoCredito.setMoneda_id(idMoneda);
		nuevoCredito.setExpediente(expediente);
		nuevoCredito.setCalcularGastosLocal(false);
		nuevoCredito.setResolucion(nroResolucion);

		List<Domicilio> doms = bp.createQuery("Select d from Domicilio d where d.persona=:persona and d.tipo=:tipo")
				.setParameter("persona", persona).setParameter("tipo", "Real").list();
		if (!doms.isEmpty()) {
			try {
				domicObjetoi = new DomicilioObjetoi();
				Domicilio dom = doms.get(0);
				Domicilio nuevoDom;
				nuevoDom = (Domicilio) BeanUtils.cloneBean(dom);
				nuevoDom.setTipo("Especial");
				nuevoDom.setId(null);
				nuevoDom.setPersona(null);
				bp.save(nuevoDom);
				domicObjetoi.setDomicilio(nuevoDom);
				domicObjetoi.setObjetoi(nuevoCredito);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (SessionHandler.getCurrentSessionHandler().getCurrentUser() != null) {
			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			if (usuario != null) {
				nuevoCredito.setDelegacion(usuario.getDelegacion());
			}
		}
	}

	private void crearDesembolso(Double importe) {
		desembolso.setCredito(nuevoCredito);
		desembolso.setFecha(fechaAcuerdo);
		desembolso.setFechaReal(fechaAcuerdo);
		desembolso.setNroOrden(null);
		desembolso.setNumero(1);
		desembolso.setImporte(importe);
		desembolso.setImporteReal(importe);
		desembolso.setImporteGastos(new Double(0));
		desembolso.setImporteMultas(new Double(0));
		desembolso.setEstado("2");
		nuevoCredito.setFinanciamiento(desembolso.getImporte());
	}

	@SuppressWarnings("unchecked")
	private void buscarCreditosSeleccionados() {
		creditosSeleccionados = new ArrayList<Objetoi>();
		buscarCreditos();
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		HashSet<String> seleccionados = new HashSet<String>();

		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("credito")) {
				String[] p = param.split("-");
				seleccionados.add(new String(p[1]));
			}
		}

		if (seleccionados.isEmpty() && SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("AcuerdoPago.creditosSeleccionadosPorId") != null) {
			creditosSeleccionadosPorId = (Map<String, Objetoi>) SessionHandler.getCurrentSessionHandler().getRequest()
					.getSession().getAttribute("AcuerdoPago.creditosSeleccionadosPorId");
		} else {
			for (Objetoi creditoSeleccionado : creditosDeuda) {
				if (seleccionados.contains(creditoSeleccionado.getId().toString())) {
					creditosSeleccionados.add(creditoSeleccionado);
					creditosSeleccionadosPorId.put(creditoSeleccionado.getId().toString(), creditoSeleccionado);
				}
			}
		}

		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("AcuerdoPago.creditosSeleccionadosPorId", creditosSeleccionadosPorId);
	}

	public boolean validarDatos() {
		buscarCreditosSeleccionados();
		if (creditosSeleccionados.isEmpty()) {
			errores.put("acuerdoPago.error.vacio", "acuerdoPago.error.vacio");
			return false;
		} else {
			boolean tieneEmergencia = false;
			boolean noTieneEmergencia = false;
			for (Objetoi credito : creditosSeleccionados) {
				if (credito.getLinea().isLineaEmergencia()) {
					tieneEmergencia = true;
				} else {
					noTieneEmergencia = true;
				}
			}
			if (tieneEmergencia && noTieneEmergencia) {
				errores.put("acuerdoPago.error.emergencia", "acuerdoPago.error.emergencia");
				return false;
			} else if (tieneEmergencia) {
				errores.put("acuerdoPago.error.todos", "acuerdoPago.error.todos");
			}
		}
		if (segundoVencCapital == null || segundoVencInteres == null || periodicidadCapital == null
				|| periodicidadInteres == null) {
			errores.put("acuerdoPago.error.datos", "acuerdoPago.error.datos");
			return false;
		}
		return true;
	}

	public String getIndiceValorStr() {
		if (idIndice != null && idIndice != 0) {
			if (diasAntes == null) {
				diasAntes = 0;
			}
			diasAntes *= -1;
			Date fechaCorrecta = DateHelper.add(new Date(), diasAntes, Calendar.DAY_OF_YEAR);
			return String.format("%3.2f%%", new CreditoHandler().buscarValorIndiceFecha(idIndice, fechaCorrecta) * 100);
		}
		return String.format("%3.2f%%", 0.00);
	}

	private void initLineas() {
		String ids = DirectorHelper.getString(Linea.LINEA_ACUERDO_PAGO, "-1");
		String[] partes = ids.split(",");
		lineas = new ArrayList<Linea>();
		for (String id : partes) {
			Linea linea = (Linea) bp.getById(Linea.class, new Long(id.trim()));
			lineas.add(linea);
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<Objetoi> getCreditosDeuda() {
		return creditosDeuda;
	}

	public Long getCantidadCuotasCapital() {
		return cantidadCuotasCapital;
	}

	public void setCantidadCuotasCapital(Long cantidadCuotasCapital) {
		this.cantidadCuotasCapital = cantidadCuotasCapital;
	}

	public Long getCantidadCuotasInteres() {
		return cantidadCuotasInteres;
	}

	public void setCantidadCuotasInteres(Long cantidadCuotasInteres) {
		this.cantidadCuotasInteres = cantidadCuotasInteres;
	}

	public Long getIdIndice() {
		return idIndice;
	}

	public void setIdIndice(Long idIndice) {
		this.idIndice = idIndice;
	}

	public Double getValorMas() {
		return valorMas;
	}

	public void setValorMas(Double valorMas) {
		this.valorMas = valorMas;
	}

	public Double getValorPor() {
		return valorPor;
	}

	public void setValorPor(Double valorPor) {
		this.valorPor = valorPor;
	}

	public String getValorPorStr() {
		if (getValorPor() != null) {
			return String.format("%5.3f", getValorPor());
		}
		return "1,000";
	}

	public void setValorPorStr(String s) {
		if (s != null) {
			setValorPor(new Double(s.replaceAll(",", ".")));
		} else {
			setValorPor(0.0);
		}
	}

	public String getValorMasStr() {
		if (getValorMas() != null) {
			return String.format("%5.2f%%", getValorMas() * 100);
		}
		return String.format("%5.2f%%", 0.00);
	}

	public void setValorMasStr(String s) {
		if (s != null) {
			setValorMas(new Double(s.replaceAll(",", ".").replaceAll("%", "")) / 100);
		} else {
			setValorMas(0.0);
		}
	}

	public Integer getDiasAntes() {
		return diasAntes;
	}

	public void setDiasAntes(Integer diasAntes) {
		this.diasAntes = diasAntes;
	}

	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public void setCreditosDeuda(List<Objetoi> creditosDeuda) {
		this.creditosDeuda = creditosDeuda;
	}

	public Date getSegundoVencCapital() {
		return segundoVencCapital;
	}

	public void setSegundoVencCapital(Date primerVencCapital) {
		this.segundoVencCapital = primerVencCapital;
	}

	public String getSegundoVencCapitalStr() {
		return DateHelper.getString(segundoVencCapital);
	}

	public void setSegundoVencCapitalStr(String primerVenc) {
		this.segundoVencCapital = DateHelper.getDate(primerVenc);
	}

	public Date getSegundoVencInteres() {
		return segundoVencInteres;
	}

	public void setSegundoVencInteres(Date primerVencInteres) {
		this.segundoVencInteres = primerVencInteres;
	}

	public String getSegundoVencInteresStr() {
		return DateHelper.getString(segundoVencInteres);
	}

	public void setSegundoVencInteresStr(String primerVenc) {
		this.segundoVencInteres = DateHelper.getDate(primerVenc);
	}

	public boolean isGenerada() {
		return generada;
	}

	public void setGenerada(boolean generada) {
		this.generada = generada;
	}

	public Double getTasaCompensatorio() {
		return tasaCompensatorio;
	}

	public void setTasaCompensatorio(Double tasaCompensatorio) {
		this.tasaCompensatorio = tasaCompensatorio;
	}

	public Long getIdMoratorio() {
		return idMoratorio;
	}

	public void setIdMoratorio(Long idMoratorio) {
		this.idMoratorio = idMoratorio;
	}

	public Long getIdPunitorio() {
		return idPunitorio;
	}

	public void setIdPunitorio(Long idPunitorio) {
		this.idPunitorio = idPunitorio;
	}

	public String getPeriodicidadCapital() {
		return periodicidadCapital;
	}

	public void setPeriodicidadCapital(String periodicidadCApital) {
		this.periodicidadCapital = periodicidadCApital;
	}

	public String getPeriodicidadInteres() {
		return periodicidadInteres;
	}

	public void setPeriodicidadInteres(String periodicidadInteres) {
		this.periodicidadInteres = periodicidadInteres;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public boolean isFalta() {
		return falta;
	}

	public void setFalta(boolean falta) {
		this.falta = falta;
	}

	public ObjetoiIndice getObjetoIndice() {
		return objetoIndice;
	}

	public void setObjetoIndice(ObjetoiIndice objetoIndice) {
		this.objetoIndice = objetoIndice;
	}

	public Long getIdCompensatorio() {
		return idCompensatorio;
	}

	public void setIdCompensatorio(Long idCompensatorio) {
		this.idCompensatorio = idCompensatorio;
	}

	public String getIndiceValor() {
		return indiceValor;
	}

	public void setIndiceValor(String indiceValor) {
		this.indiceValor = indiceValor;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getTipoTasa() {
		return tipoTasa;
	}

	public void setTipoTasa(String tipoTasa) {
		this.tipoTasa = tipoTasa;
	}

	public String getValorFinal() {
		return String.format("%.2f%%", valorFinal);
	}

	public Double getTasaPunitorio() {
		return tasaPunitorio;
	}

	public void setTasaPunitorio(Double tasaPunitorio) {
		this.tasaPunitorio = tasaPunitorio;
	}

	public Double getTasaMoratorio() {
		return tasaMoratorio;
	}

	public void setTasaMoratorio(Double tasaMoratorio) {
		this.tasaMoratorio = tasaMoratorio;
	}

	public String getTasaCompensatorioStr() {
		return String.format("%.2f%%", tasaCompensatorio);
	}

	public String getTasaMoratorioStr() {
		return String.format("%.2f%%", tasaMoratorio);
	}

	public String getTasaPunitorioStr() {
		return String.format("%.2f%%", tasaPunitorio);
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public boolean isFaltaExpediente() {
		return faltaExpediente;
	}

	public void setFaltaExpediente(boolean faltaExpediente) {
		this.faltaExpediente = faltaExpediente;
	}

	public boolean isExito() {
		return exito;
	}

	public void setExito(boolean exito) {
		this.exito = exito;
	}

	public Objetoi getNuevoCredito() {
		return nuevoCredito;
	}

	public void setNuevoCredito(Objetoi nuevoCredito) {
		this.nuevoCredito = nuevoCredito;
	}

	public void setErrores(HashMap<String, Object> errors) {
		this.errores = errors;
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public String getNroAtencion() {
		return nroAtencion;
	}

	public void setNroAtencion(String nroAtencion) {
		this.nroAtencion = nroAtencion;
	}

	public String getFechaAcuerdoStr() {
		return DateHelper.getString(fechaAcuerdo);
	}

	public void setFechaAcuerdoStr(String fechaAcuerdo) {
		this.fechaAcuerdo = DateHelper.getDate(fechaAcuerdo);
	}

	public List<Objetoi> getAcuerdos() {
		return acuerdos;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public List<Linea> getLineas() {
		if (lineas == null) {
			initLineas();
		}
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public List<String> getExpedientes() {
		return expedientes;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public ArrayList<Objetoi> getCreditosSeleccionados() {
		return creditosSeleccionados;
	}

	public Objetoi getCreditoSeleccionadoId(String id) {
		return creditosSeleccionadosPorId.get(id);
	}

	public Date getPrimerVencCapital() {
		return primerVencCapital;
	}

	public void setPrimerVencCapital(Date primerVencCapital) {
		this.primerVencCapital = primerVencCapital;
	}

	public Date getPrimerVencInteres() {
		return primerVencInteres;
	}

	public void setPrimerVencInteres(Date primerVencInteres) {
		this.primerVencInteres = primerVencInteres;
	}

	public String getPrimerVencCapitalStr() {
		return DateHelper.getString(primerVencCapital);
	}

	public void setPrimerVencCapitalStr(String primerVenc) {
		this.primerVencCapital = DateHelper.getDate(primerVenc);
	}

	public String getPrimerVencInteresStr() {
		return DateHelper.getString(primerVencInteres);
	}

	public void setPrimerVencInteresStr(String primerVenc) {
		this.primerVencInteres = DateHelper.getDate(primerVenc);
	}

	public String getNroResolucion() {
		return nroResolucion;
	}

	public void setNroResolucion(String nroResolucion) {
		this.nroResolucion = nroResolucion;
	}

	public static Date getPrimerDiaDelMes() {
		Calendar miCalendario = Calendar.getInstance();
		miCalendario.set(Calendar.DAY_OF_MONTH, miCalendario.getActualMinimum(Calendar.DAY_OF_MONTH));
		return DateHelper.resetHoraToZero(miCalendario.getTime());
	}

	public int getTipoJudicial() {
		return tipoJudicial;
	}

	public void setTipoJudicial(int tipoJudicial) {
		this.tipoJudicial = tipoJudicial;
	}

	public Double getTopeTasa() {
		return topeTasa;
	}

	public void setTopeTasa(Double topeTasa) {
		this.topeTasa = topeTasa;
	}

	@Transient
	public String getTopeTasaStr() {
		if (this.topeTasa == null) {
			return String.format("%.2f%%", 0.0);
		} else {
			return String.format("%.2f%%", this.topeTasa);
		}
	}

	public void setTopeTasaStr(String valor) {
		this.topeTasa = new Double(valor.replace(",", ".").replace("%", ""));
	}

}
