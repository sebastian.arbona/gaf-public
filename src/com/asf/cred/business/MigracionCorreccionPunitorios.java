package com.asf.cred.business;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import com.asf.cred.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class MigracionCorreccionPunitorios extends ConversationProcess {

	private int total;
	private int procesados;

	private List<Ctacte> resultado = new ArrayList<Ctacte>();
	private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean run() {
		try {
			Log log = LogFactory.getLog(getClass());

			String sql = IOUtils.toString(getClass().getResourceAsStream("migracion-correccion-punitorio.sql"));

			SQLQuery queryFechaGeneracion = bp.createSQLQuery("SELECT MAX(c.fechaGeneracion) " + "FROM Ctacte c "
					+ "JOIN Concepto con on con.id = c.facturado_id " + "LEFT JOIN DebitosPunitorioBackup db "
					+ "on c.movimientoCtacte = db.movimientoCtacte " + "AND c.itemCtacte = db.itemCtacte "
					+ "AND c.periodoCtacte = db.periodoCtacte " + "WHERE c.cuota_id = :idCuota AND "
					+ "(c.fechaGeneracion < :fecha OR (c.fechaGeneracion = :fecha "
					+ "AND (c.movimientoCtacte <> :mov OR c.itemCtacte <> :item OR c.periodoCtacte <> :per) "
					+ "AND db.movimientoCtacte IS NULL AND db.itemCtacte IS NULL AND db.periodoCtacte IS NULL)) "
					+ "AND con.concepto_concepto = :concepto " + "AND c.tipomov_id = 2");

			Query queryDeuda = bp
					.createSQLQuery("SELECT SUM(CASE WHEN c.tipomov_id = 1 THEN -c.importe ELSE c.importe END) "
							+ "FROM Ctacte c " + "JOIN Concepto con on con.id = c.facturado_id "
							+ "WHERE c.cuota_id = :idCuota AND c.fechaGeneracion < :fecha AND con.concepto_concepto IN (:conceptos)")
					.setParameterList("conceptos",
							new String[] { Concepto.CONCEPTO_CAPITAL, Concepto.CONCEPTO_COMPENSATORIO,
									Concepto.CONCEPTO_GASTOS, Concepto.CONCEPTO_GASTOS_A_RECUPERAR,
									Concepto.CONCEPTO_MULTAS, Concepto.CONCEPTO_BONIFICACION });

			SQLQuery insertBackup = bp.createSQLQuery(
					"insert into DebitosPunitorioBackup " + "select * from Ctacte where movimientoCtacte = :mov "
							+ "and itemCtacte = :item and periodoCtacte = :per and verificadorCtacte = :ver "
							+ "and objetoi_id = :idCredito");

			SQLQuery countBackup = bp.createSQLQuery("select count(*) from DebitosPunitorioBackup "
					+ "where movimientoCtacte = :mov " + "and itemCtacte = :item " + "and periodoCtacte = :per ");

			List<Ctacte> ccs = bp.createSQLQuery(sql).addEntity(Ctacte.class).list();

			total = ccs.size();
			log.info("Registros total: " + ccs.size());

			Long idObjetoiAnterior = null;
			Ctacte ccComplem = null;
			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
			int periodo = Calendar.getInstance().get(Calendar.YEAR);
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;

			procesados = 0;

			for (Ctacte cc : ccs) {
				String concepto = cc.getFacturado().getConcepto().getConcepto();
				boolean punitorio = concepto.equals(Concepto.CONCEPTO_PUNITORIO);
				String conceptoComplem;
				if (punitorio) {
					conceptoComplem = Concepto.CONCEPTO_MORATORIO;
				} else {
					conceptoComplem = Concepto.CONCEPTO_PUNITORIO;
				}

				Number count = (Number) countBackup.setLong("item", cc.getId().getItemCtacte())
						.setLong("mov", cc.getId().getMovimientoCtacte()).setLong("per", cc.getId().getPeriodoCtacte())
						.uniqueResult();
				if (count != null && count.intValue() >= 1) {
					idObjetoiAnterior = cc.getId().getObjetoi_id();
					ccComplem = cc.getId().getObjetoi().crearCtacte(conceptoComplem, 0.0, cc.getFechaGeneracion(), null,
							cc.getId().getObjetoi(), cc.getCuota(), null);
					procesados++;

					continue;
				}

				Number deuda = (Number) queryDeuda.setLong("idCuota", cc.getCuota_id())
						.setDate("fecha", cc.getFechaGeneracion()).uniqueResult();

				Date ultimaFecha = (Date) queryFechaGeneracion.setLong("idCuota", cc.getCuota_id())
						.setDate("fecha", cc.getFechaGeneracion()).setString("concepto", concepto)
						.setLong("mov", cc.getId().getMovimientoCtacte()).setLong("item", cc.getId().getItemCtacte())
						.setLong("per", cc.getId().getPeriodoCtacte()).uniqueResult();

				if (ultimaFecha == null) {
					ultimaFecha = cc.getCuota().getFechaVencimiento();
				}

				if (deuda == null) {
					deuda = new Double(0);
				}
				Objetoi proyectoCalculo = cc.getId().getObjetoi();
				int dias = (int) DateHelper.getDiffDates(ultimaFecha, cc.getFechaGeneracion(), 2);
				double importeP = proyectoCalculo.calcularPunitorioNew(deuda.doubleValue(), dias, dias, ultimaFecha,
						ultimaFecha, cc.getFechaGeneracion());
				double importeM = proyectoCalculo.calcularMoratorioNew(deuda.doubleValue(), dias, dias, ultimaFecha,
						ultimaFecha, cc.getFechaGeneracion());
				double importe = cc.getImporte();

				if (idObjetoiAnterior != null && !idObjetoiAnterior.equals(cc.getId().getObjetoi_id())) {
					ccComplem.setTipomov(tipoDebito);
					ccComplem.setTipoMovimiento("pago");
					ccComplem.setUsuario(usuario);
					ccComplem.getId().setMovimientoCtacte(movimientoCtaCte);
					ccComplem.getId().setItemCtacte(itemCtaCte++);
					bp.save(ccComplem);
					resultado.add(ccComplem);

					log.info(ccComplem.getId().getObjetoi().getNumeroAtencion() + " Cuota "
							+ ccComplem.getCuota().getNumero() + " "
							+ ccComplem.getFacturado().getConcepto().getAbreviatura() + " " + ccComplem.getImporte()
							+ "  (" + ccComplem.getId().getMovimientoCtacte() + ", "
							+ ccComplem.getId().getPeriodoCtacte() + ")");
				}

				int filas = insertBackup.setLong("item", cc.getId().getItemCtacte())
						.setLong("mov", cc.getId().getMovimientoCtacte()).setLong("per", cc.getId().getPeriodoCtacte())
						.setLong("ver", cc.getId().getVerificadorCtacte())
						.setLong("idCredito", cc.getId().getObjetoi_id()).executeUpdate();

				if (filas >= 1) {
					cc.setImporte(importeP + importeM);
					bp.update(cc);
					resultado.add(cc);
					log.info(cc.getId().getObjetoi().getNumeroAtencion() + " Cuota " + cc.getCuota().getNumero() + " "
							+ cc.getFacturado().getConcepto().getAbreviatura() + " " + importe + " -> "
							+ cc.getImporte() + " " + format.format(ultimaFecha) + "/"
							+ format.format(cc.getFechaGeneracion()) + "  (" + cc.getId().getMovimientoCtacte() + ", "
							+ cc.getId().getPeriodoCtacte() + ")");
				} else {
					log.info(cc.getId().getObjetoi().getNumeroAtencion() + " Cuota " + cc.getCuota().getNumero() + " "
							+ cc.getFacturado().getConcepto().getAbreviatura() + " " + " error insert backup  ("
							+ cc.getId().getMovimientoCtacte() + ", " + cc.getId().getPeriodoCtacte() + ")");
				}

				idObjetoiAnterior = cc.getId().getObjetoi_id();
				ccComplem = cc.getId().getObjetoi().crearCtacte(conceptoComplem, 0.0, cc.getFechaGeneracion(), null,
						cc.getId().getObjetoi(), cc.getCuota(), null);

				procesados++;
			}

			if (ccComplem != null) {
				ccComplem.setTipomov(tipoDebito);
				ccComplem.setTipoMovimiento("pago");
				ccComplem.setUsuario(usuario);
				ccComplem.getId().setMovimientoCtacte(movimientoCtaCte);
				ccComplem.getId().setItemCtacte(itemCtaCte++);
				bp.save(ccComplem);
				resultado.add(ccComplem);

				log.info(ccComplem.getId().getObjetoi().getNumeroAtencion() + " Cuota "
						+ ccComplem.getCuota().getNumero() + " "
						+ ccComplem.getFacturado().getConcepto().getAbreviatura() + " " + ccComplem.getImporte() + "  ("
						+ ccComplem.getId().getMovimientoCtacte() + ", " + ccComplem.getId().getPeriodoCtacte() + ")");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "MigracionCorreccionPunitorios";
	}

	public int getTotal() {
		return total;
	}

	public int getProcesados() {
		return procesados;
	}

	public List<Ctacte> getResultado() {
		return resultado;
	}

}
