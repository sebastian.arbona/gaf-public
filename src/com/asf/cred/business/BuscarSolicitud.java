package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.struts.action.ActionMessage;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Objetoi;

public class BuscarSolicitud implements IProcess {

	private String forward = "buscarSolicitudList";
	private HashMap<String, Object> errores;
	private String numeroCredito;
	private Long numeroAtencion;
	private String expediente;
	private String persona;
	private ArrayList<Objetoi> solicitudes;
	private String accion;
	private String miVar;

	public BuscarSolicitud() {
		this.errores = new HashMap<String, Object>();
	}

	public boolean doProcess() {
		if (accion == null) {
			return false;
		} else if (accion.equals("list")) {
			buscarSolicitudes();
			return this.errores.isEmpty();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void buscarSolicitudes() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		String consulta;

		if (this.expediente != null && !this.expediente.trim().isEmpty()) {
			consulta = "SELECT o FROM Objetoi o WHERE o.expediente = '"
					+ this.expediente + "' ORDER BY o.numeroAtencion";
		} else if (this.persona != null && !this.persona.trim().isEmpty()) {
			Long idPersona = new Long(persona.split("-")[0]);
			consulta = "SELECT o FROM Objetoi o WHERE o.persona.id = "
					+ idPersona + " ORDER BY o.numeroAtencion";
		} else if (this.numeroCredito != null
				&& !this.numeroCredito.trim().isEmpty()) {
			consulta = "SELECT o FROM Objetoi o WHERE o.numeroCredito = '"
					+ this.numeroCredito + "' ORDER BY o.numeroAtencion";
		} else if (this.numeroAtencion != null && this.numeroAtencion != 0L) {
			consulta = "SELECT o FROM Objetoi o WHERE o.numeroAtencion = "
					+ this.numeroAtencion;
		} else {
			consulta = "SELECT o FROM Objetoi o ORDER BY o.numeroAtencion";
		}
		solicitudes = new ArrayList<Objetoi>();
		ArrayList<Objetoi> obj2 = (ArrayList<Objetoi>) bp.getByFilter(consulta);
		Estado estado;
		try {
			estado = (Estado) bp.getByFilter(
					"SELECT e FROM Estado e WHERE e.nombreEstado = 'ESPERANDO DOCUMENTACIÓN' "
							+ "OR e.nombreEstado = 'ESPERANDO DOCUMENTACION'")
					.get(0);
		} catch (Exception e) {
			this.errores.put("errors.detail", new ActionMessage(
					"errors.detail",
					"Error al buscar el estado ESPERANDO DOCUMENTACIÓN."));
			return;
		}
		for (Objetoi objetoi : obj2) {
			try {
				if (!objetoi.getEstadoActual().getEstado().equals(estado)
						&& objetoi.getExpediente() != null
						&& !objetoi.getExpediente().isEmpty()
						&& !objetoi.getExpediente().equals("0")) {
					solicitudes.add(objetoi);
				}
			} catch (Exception e) {
				if (!errores.containsKey("errors.detail")) {
					this.errores.put("errors.detail", new ActionMessage(
							"errors.detail",
							"Algunas solicitudes no poseen estado actual."));
				}
			}
		}

	}

	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return errores;
	}

	public String getForward() {
		return forward;
	}

	public String getInput() {
		return forward;
	}

	public Object getResult() {
		return solicitudes;
	}

	public boolean validate() {
		return true;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}
	
	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setSolicitudes(ArrayList<Objetoi> objetos) {
		this.solicitudes = objetos;
	}

	public ArrayList<Objetoi> getSolicitudes() {
		return solicitudes;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public String getMiVar() {
		return miVar;
	}

	public void setMiVar(String miVar) {
		this.miVar = miVar;
	}

	
	
	
}
