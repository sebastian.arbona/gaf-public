package com.asf.cred.business;

import java.util.Calendar;
import java.util.HashMap;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.EspecialidadPersona;
import com.nirven.creditos.hibernate.EspecialidadPersonaAsignacion;

public class AsignacionEspecialista implements IProcess {
	private String forward;
	private String forwardURL;
	private String accion;

	private HashMap<String, Object> errores;
	private EspecialidadPersonaAsignacion asignacion;

	public AsignacionEspecialista() {
		forward = "AsignacionEspecialista";
		errores = new HashMap<String, Object>();
		asignacion = new EspecialidadPersonaAsignacion();
		asignacion.setEspecialista(new EspecialidadPersona());
		asignacion.setFechaProceso(Calendar.getInstance().getTime());
		asignacion.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
	}

	@Override
	public boolean doProcess() {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		// los datos de la asignacion se deberian cargar con los setters de la entidad
		bp.save(asignacion);

		forward = "ProcessRedirect";
		forwardURL = "/actions/abmAction.do?do=list&entityName=Especialidad";

		return this.errores.isEmpty();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return "AsignacionEspecialista";
	}

	@Override
	public boolean validate() {

		if (this.asignacion.getEspecialista() == null) {
			errores.put("asignacion.especialista.error", "asignacion.especialista.error");
		}
		if (this.asignacion.getDescripcion() == null || this.asignacion.getDescripcion().isEmpty()) {
			errores.put("asignacion.descripcion.error", "asignacion.descripcion.error");
		}
		if (this.asignacion.getFechaAceptacion() == null) {
			errores.put("asignacion.fechaaceptacion.error", "asignacion.fechaaceptacion.error");
		}
		if (this.asignacion.getAcepta() == null) {
			errores.put("asignacion.acepta.error", "asignacion.acepta.error");
		}
		/**
		 * si no hay errores entonces se podra ejecutar doProcess {@see ProcessAction}
		 */
		return this.errores.isEmpty();

	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public EspecialidadPersonaAsignacion getAsignacion() {
		return asignacion;
	}

	public void setAsignacion(EspecialidadPersonaAsignacion asignacion) {
		this.asignacion = asignacion;
	}

}
