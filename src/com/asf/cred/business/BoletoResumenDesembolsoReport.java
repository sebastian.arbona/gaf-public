package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Boleto;

public class BoletoResumenDesembolsoReport extends ConversationProcess {

	private String fechaDesde;
	private String fechaHasta;

	private List<Boleto> boletos;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean listar() {
		Date desde = DateHelper.getDate(fechaDesde);
		Date hasta = DateHelper.getDate(fechaHasta);

		if (desde == null || hasta == null) {
			boletos = new ArrayList<Boleto>();
			return false;
		}

		List<Number> idsBoletos = bp.createSQLQuery(
				"select distinct cc.idBoletoResumen from Ctacte cc join Desembolso d on cc.desembolso_id = d.id "
						+ "where cc.idBoletoResumen not in (select distinct boleto_id from Emideta where boleto_id is not null) "
						+ "and d.fechaReal >= :fechaDesde and d.fechaReal <= :fechaHasta")
				.setParameter("fechaDesde", desde).setParameter("fechaHasta", hasta).list();

		List<Long> idsLong = new ArrayList<Long>();
		for (Number id : idsBoletos) {
			idsLong.add(id.longValue());
		}
		boletos = bp.createQuery("select b from Boleto b where b.id in (:ids) order by b.id")
				.setParameterList("ids", idsLong).list();

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "BoletoResumenDesembolsoReport";
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public List<Boleto> getBoletos() {
		return boletos;
	}

	public void setBoletos(List<Boleto> boletos) {
		this.boletos = boletos;
	}

}
