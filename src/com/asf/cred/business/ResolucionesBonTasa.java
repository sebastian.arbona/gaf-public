package com.asf.cred.business;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.Save;
import com.civitas.hibernate.persona.Archivo;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaEstado;
import com.nirven.creditos.hibernate.BonTasaEstadoArchivo;
import com.nirven.expedientes.persistencia.TipoDeArchivo;

public class ResolucionesBonTasa extends ConversationProcess {

	@Save
	private Long idBonTasa;
	private List<BonTasaEstado> estados;
	private Estado estadoActual;
	private BonTasa bonificacion;
	private BonTasaEstado estado;
	private String forward = "ResolucionesBonTasa";
	private HashMap<String, Object> errores;
	private String accion = "";
	private List<BonTasaEstadoArchivo> bontasaestadoarchivos;
	private FormFile theFile;
	private String detalleArchivoResolucion;
	private BonTasaEstadoArchivo bontasaestadoarchivo;

	@SuppressWarnings("unchecked")
	// @ProcessMethod(defaultAction=true)
	private boolean buscarEstados() {
		BonTasa bonTasa = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
		// solo traigo los que tienen resoluciones
		estados = bp
				.createQuery("SELECT eb FROM BonTasaEstado eb " + " WHERE eb.bonTasa=:bon"
						+ " AND eb.tipoResolucion is not null" + " Order by eb.fechaResolucion desc")
				.setParameter("bon", bonTasa).list();
		return true;
	}

	@SuppressWarnings("unchecked")
	private void buscarResucionArchivos() {
		if (estado.getId() != null)
			bontasaestadoarchivos = (List<BonTasaEstadoArchivo>) bp
					.createQuery("select a from BonTasaEstadoArchivo a where a.bontasaestado.id=:id")
					.setLong("id", estado.getId()).list();
		forward = "ResolucionesBonTasaArchivoList";
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public String getForward() {
		return forward;
	}

	@Override
	public void setForward(String forward) {
		this.forward = forward;
	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	@Override
	public boolean validate() {
		return super.validate();
	}

	public List<BonTasaEstado> getEstados() {
		return estados;
	}

	@Override
	public boolean doProcess() {
		if (accion.equals("listResolucion")) {
			buscarEstados();
			forward = "ResolucionesBonTasa";
		}
		if (accion.equals("listResolucionArchivo")) {
			buscarResucionArchivos();
		}
		if (accion.equals("nuevoDocumentoResolucion")) {
			forward = "ResolucionesBonTasaArchivo";
		}
		if (accion.equals("guardarArchivoResolucion")) {
			grabarArchivo();
			buscarResucionArchivos();
		}
		if (accion.equals("eliminarResolucionArchivo")) {
			borrarArchivo();
		}
		return true;
	}

	private void borrarArchivo() {
		if (bontasaestadoarchivo != null) {
			bp.delete(bontasaestadoarchivo);
			buscarResucionArchivos();
		}

	}

	private void grabarArchivo() {
		Archivo archivo = new Archivo();
		if (theFile.getFileSize() != 0) {
			archivo.setMimetype(theFile.getContentType());
			archivo.setNombre(theFile.getFileName());
			// archivo.setFecha(new Date());
			// archivo.setDetalle(this.getDetalleArchivoResolucion());
			// byte[] fileData = theFile.getFileData();
			// guarda los datos del fichero
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream stream = null;
			try {
				stream = theFile.getInputStream();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			byte[] buffer = new byte[40000];
			int bytesLeidos = 0;
			try {
				while ((bytesLeidos = stream.read(buffer, 0, 40000)) != -1) {
					baos.write(buffer, 0, bytesLeidos);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			archivo.setArchivo(baos.toByteArray());
			archivo.getArchivoRepo().setTipoDeArchivo((TipoDeArchivo) bp.getById(TipoDeArchivo.class, -1L));
			archivo.setPersona(estado.getBonTasa().getPersona());

			BonTasaEstadoArchivo bontasaestadoarchi = new BonTasaEstadoArchivo();
			bontasaestadoarchi.setArchivo(archivo);
			bontasaestadoarchi.setBontasaestado(estado);
			bontasaestadoarchi.setDescripcion(this.getDetalleArchivoResolucion());

			bp.save(archivo);
			bp.save(bontasaestadoarchi);
		}

	}

	public void setEstados(List<BonTasaEstado> estados) {
		this.estados = estados;
	}

	public Estado getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(Estado estadoActual) {
		this.estadoActual = estadoActual;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	@Override
	protected String getDefaultForward() {
		return "ResolucionesBonTasa";
	}

	public BonTasaEstado getEstado() {
		return estado;
	}

	public void setEstado(BonTasaEstado estado) {
		this.estado = estado;
	}

	public void setEstado_id(String id) {
		if (id != null)
			estado = (BonTasaEstado) bp.getById(BonTasaEstado.class, Long.parseLong(id));
	}

	public String getEstado_id() {
		if (estado == null) {
			return null;
		} else {
			return estado.getId().toString();
		}
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<BonTasaEstadoArchivo> getBontasEestadoArchivos() {
		return bontasaestadoarchivos;
	}

	public void setBontasaestadoarchivos(List<BonTasaEstadoArchivo> bontasaestadoarchivos) {
		this.bontasaestadoarchivos = bontasaestadoarchivos;
	}

	public FormFile getTheFile() {
		return theFile;
	}

	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}

	public String getDetalleArchivoResolucion() {
		return detalleArchivoResolucion;
	}

	public void setDetalleArchivoResolucion(String detalleArchivoResolucion) {
		this.detalleArchivoResolucion = detalleArchivoResolucion;
	}

	public String getBontasaestadoarchivo_id() {
		if (bontasaestadoarchivo == null)
			return null;
		return bontasaestadoarchivo.getId().toString();
	}

	public void setBontasaestadoarchivo_id(String id) {
		if (id != null)
			bontasaestadoarchivo = (BonTasaEstadoArchivo) bp.getById(BonTasaEstadoArchivo.class, Long.parseLong(id));
	}

	public BonTasaEstadoArchivo getBontasaestadoarchivo() {
		return bontasaestadoarchivo;
	}

	public void setBontasaestadoarchivo(BonTasaEstadoArchivo bontasaestadoarchivo) {
		this.bontasaestadoarchivo = bontasaestadoarchivo;
	}

}
