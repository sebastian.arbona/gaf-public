package com.asf.cred.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.civitas.cred.beans.BeanCuota;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DetalleCuota;

public class ImputacionFixProcess implements IProcess {

	private String accion;

	private List<Object[]> creditos;
	private BusinessPersistance bp;
	private int desembolso;

	private String log;

	public ImputacionFixProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion == null || accion.equals("1D")) {
			buscarPrimerDesembolso();
			desembolso = 1;
		} else if (accion.equals("2D")) {
			buscarSegundoDesembolso();
			desembolso = 2;
		}

		StringWriter sw = new StringWriter();
		PrintWriter out = new PrintWriter(sw);

		if (creditos != null) {
			bp.begin();

			CreditoHandler handler = new CreditoHandler();

			for (Object[] credito : creditos) {

				Long idObjetoi = ((BigDecimal) credito[0]).longValue();
				out.println("CREDITO: " + idObjetoi);

				Date fechaImputacion = (Date) credito[2];
				Date fechaReal = (Date) credito[3];

				Desembolso d1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
						.setParameter("idCredito", idObjetoi).setParameter("numero", desembolso).uniqueResult();

				// eliminar los detalle cuota mal creados
				bp.createSQLQuery("delete from detallecuota where cuota_id in (select id from cuota where credito_id = "
						+ idObjetoi + ") and desembolso = " + desembolso).executeUpdate();

				// beans con importes mal
				List<BeanCuota> beansMal = handler.generarCuotas(idObjetoi, d1.getId());

				d1.setFechaReal(fechaImputacion);

				// beans con importes bien
				List<BeanCuota> beansOk = handler.generarCuotas(idObjetoi, d1.getId());
				for (BeanCuota bean : beansOk) {
					Cuota cuotaNro = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setLong("idCredito", idObjetoi)
							.setInteger("nroCuota", bean.getNumero()).uniqueResult();
					if (cuotaNro != null) {
						out.println("CUOTA: " + bean.getNumero());
						double compRestar = beansMal.get(beansOk.indexOf(bean)).getCompensatorio();
						double bonRestar = beansMal.get(beansOk.indexOf(bean)).getBonificacion();
						double compSumar = bean.getCompensatorio();
						double bonSumar = bean.getBonificacion();
						out.println("  Comp " + cuotaNro.getCompensatorio() + " - " + compRestar + " + " + compSumar
								+ " = " + (cuotaNro.getCompensatorio() - compRestar + compSumar));
						out.println("  Bon  " + cuotaNro.getBonificacion() + " - " + bonRestar + " + " + bonSumar
								+ " = " + (cuotaNro.getBonificacion() - bonRestar + bonSumar));
						cuotaNro.setCompensatorio(cuotaNro.getCompensatorio() - compRestar + compSumar);
						cuotaNro.setBonificacion(cuotaNro.getBonificacion() - bonRestar + bonSumar);
						cuotaNro.setFechaGeneracion(fechaImputacion);
					}

					// guardar nuevo detalle cuota
					DetalleCuota detalle = new DetalleCuota();
					detalle.setBonificacion(bean.getBonificacion());
					detalle.setCapital(bean.getCapital());
					detalle.setCompensatorio(cuotaNro.getCompensatorio());
					detalle.setCuota(cuotaNro);
					detalle.setDesembolso(desembolso);
					detalle.setDesembolsado(d1.getImporteReal());
					detalle.setDiasPeriodo(bean.getDiasPeriodo());
					detalle.setDiasTransaccion(bean.getDiasTransaccion());
					detalle.setMoratorio(bean.getMoratorio());
					detalle.setPunitorio(bean.getPunitorio());
					detalle.setSaldoCapital(bean.getSaldoCapital());
					detalle.setTasaAmortizacion(bean.getTasaAmortizacion());
					detalle.setTasaBonificacion(bean.getTasaBonificacion());
					detalle.setVencimiento(bean.getFechaVencimiento());
					detalle.setVencimientoAnterior(bean.getFechaVencimientoAnterior());
					bp.save(detalle);
				}

				List<Ctacte> ccs = bp.createQuery(
						"SELECT c FROM Ctacte c WHERE c.cuota.credito.id = :idCredito AND c.fechaGeneracion = :fechaGeneracion")
						.setParameter("idCredito", idObjetoi).setParameter("fechaGeneracion", fechaReal).list();

				out.print("CAMBIAR FECHA CC EN: ");
				String in = "(";
				for (Ctacte cc : ccs) {
					cc.setFechaGeneracion(fechaImputacion);
					in += cc.getId().getMovimientoCtacte() + "-" + cc.getId().getItemCtacte() + ", ";
				}
				in = in.substring(0, in.length() - 2);
				in += ")";

				out.println(in);
				out.println();
			}

			out.flush();

			bp.commit();

		}

		out.flush();

		log = sw.toString();

		return true;
	}

	@SuppressWarnings("unchecked")
	private void buscarPrimerDesembolso() {
		try {
			String sql = IOUtils.toString(getClass().getResourceAsStream("fixDesembolso1.sql"));
			String gaf = DirectorHelper.getString("SGAF");
			sql = sql.replace("@GAF_SCHEMA", gaf);
			creditos = bp.createSQLQuery(sql).list();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void buscarSegundoDesembolso() {
		try {
			String sql = IOUtils.toString(getClass().getResourceAsStream("fixDesembolso2.sql"));
			String gaf = DirectorHelper.getString("SGAF");
			sql = sql.replace("@GAF_SCHEMA", gaf);
			creditos = bp.createSQLQuery(sql).list();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return "ImputacionFix";
	}

	@Override
	public String getInput() {
		return "ImputacionFix";
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getLog() {
		return log;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}
