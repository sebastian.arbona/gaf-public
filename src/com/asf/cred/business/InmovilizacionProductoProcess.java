package com.asf.cred.business;

import groovyjarjarasm.asm.tree.TryCatchBlockNode;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.creditos.hibernate.ValorCampoGarantia;
import com.nirven.creditos.hibernate.Vinedo;
import com.tonbeller.jpivot.olap.query.SetExp;

public class InmovilizacionProductoProcess implements IProcess {

	protected ObjetoiVinedo vinedo = new ObjetoiVinedo();
	protected ArrayList<ObjetoiVinedo> vinedos;

	protected List<Objetoi> creditos;
	protected Objetoi credito;

	private CosechaConfig cosechaConfig;
	private double gtiaMinima;
	private String accion; // Que va a hacer el proceso
	private String forward; // Hacia donde va a ir luego de ejecutar el
							// doProcess
	private Long id;
	private Garantia garantia = new Garantia();
	private boolean detalle = false;
	private String producto;

	public InmovilizacionProductoProcess() {

	}

	public CosechaConfig getCosechaConfig() {
		return cosechaConfig;
	}

	public void setCosechaConfig(CosechaConfig cosechaConfig) {
		this.cosechaConfig = cosechaConfig;
	}

	public boolean isDetalle() {
		return detalle;
	}

	public void setDetalle(boolean detalle) {
		this.detalle = detalle;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ObjetoiVinedo getVinedo() {
		return vinedo;
	}

	public void setVinedo(ObjetoiVinedo vinedo) {
		this.vinedo = vinedo;
	}

	public ArrayList<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(ArrayList<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public double getGtiaMinima() {
		return gtiaMinima;
	}

	public void setGtiaMinima(double gtiaMinima) {
		this.gtiaMinima = gtiaMinima;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		String tipoGarantia = null;
		try {
			tipoGarantia = DirectorHandler.getDirector("tipoGarantia.inmovilizacion").getValor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		creditos = new ArrayList<Objetoi>(bp
				.createQuery("select distinct v.credito from ObjetoiVinedo v where v.credito in "
						+ "(Select distinct g.objetoi from ObjetoiGarantia g where g.garantia.tipo.id "
						+ "in  (:tipo)) AND v.estadoSolicitudINV = 'Realizada'")
				.setParameterList("tipo", tipoGarantia.split(",")).list());

		if (accion == null) {
			detalle = false;
			forward = "InmovilizacionList";

		} else if (accion.equalsIgnoreCase("ver")) {

			credito = (Objetoi) bp.getById(Objetoi.class, id);
			setGarantia(buscarGarantia());
			detalle = true;
			forward = "InmovilizacionList";
		} else if (accion.equals("enviar")) {
			actualizarSeleccion();
			detalle = false;
			forward = "InmovilizacionList";
		}

		return true;
	}

	public void seleccionarTodos() {

	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Garantia buscarGarantia() {
		List<Garantia> garantias = new ArrayList<Garantia>();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		garantias = bp
				.createQuery("SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja is NULL")
				.setEntity("credito", credito).list();
		if (garantias.isEmpty()) {
			garantia = new Garantia();
		} else {
			garantia = garantias.get(0);
		}
		return garantia;
	}

	@SuppressWarnings("unchecked")
	public void actualizarSeleccion() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();

		// String codigo = request.getParameter("process.codigo");

		Enumeration<String> paramNames = request.getParameterNames();

		HashSet<String> seleccionados = new HashSet<String>();

		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("vinedo")) {
				String[] p = param.split("-");
				seleccionados.add(new String(p[1]));
			}
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		java.util.Date fecha = new java.util.Date();

		for (Objetoi creditoSeleccionado : creditos) {
			if (seleccionados.contains(creditoSeleccionado.getId().toString())) {
				vinedos = (ArrayList<ObjetoiVinedo>) bp
						.createQuery("Select ov from ObjetoiVinedo ov where ov.credito=:credito")
						.setParameter("credito", creditoSeleccionado).list();
				for (ObjetoiVinedo ov : vinedos) {
					ov.setFechaSolicitudInforme2(fecha);
					ov.setGtiaMin(calcularMin(creditoSeleccionado));
					bp.saveOrUpdate(ov);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Double calcularMin(Objetoi credito) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<CosechaConfig> configs = bp
				.createQuery("Select c from CosechaConfig c where c.varietales = :varietales order by temporada")
				.setParameter("varietales", credito.getVarietales()).list();
		CosechaConfig config = configs.get(0);

		// Busco la garantia
		List<Garantia> garantias = new ArrayList<Garantia>();
		garantias = bp
				.createQuery("SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja is NULL")
				.setEntity("credito", credito).list();
		if (garantias.isEmpty()) {
			garantia = new Garantia();
		} else {
			garantia = garantias.get(0);
		}

		Double precio;
		if (garantia.getTipoProducto() != null) {
			if (garantia.getTipoProductoNombre().equalsIgnoreCase("Mosto")) {
				precio = config.getPrecioMosto();
			} else if (garantia.getTipoProductoNombre().equalsIgnoreCase("Vino Blanco")) {
				precio = config.getPrecioVinoBlanco();
			} else if (garantia.getTipoProductoNombre().equalsIgnoreCase("Vino Tinto")) {
				precio = config.getPrecioVinoTinto();
			} else if (garantia.getTipoProductoNombre().equalsIgnoreCase("Malbec")) {
				precio = config.getPrecioMalbec();
			} else if (garantia.getTipoProductoNombre().equalsIgnoreCase("Otros varietales")) {
				precio = config.getPrecioOtrosVarietales();
			} else {
				precio = 1.0;
			}
		} else {
			precio = 1.0;
		}
		return credito.getFinanciamiento() * config.getAforo() / precio;
	}

	public void enviarInforme() {

	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public Garantia setGarantia(Garantia garantia) {
		this.garantia = garantia;
		return garantia;
	}

	public Garantia getGarantia() {
		return garantia;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

}
