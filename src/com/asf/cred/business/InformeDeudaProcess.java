package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SecurityFilter;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.DetalleInformeDeuda;
import com.nirven.creditos.hibernate.EstadoInformeDeuda;
import com.nirven.creditos.hibernate.InformeDeuda;
import com.nirven.creditos.hibernate.Linea;

public class InformeDeudaProcess extends ConversationProcess {

	private Date fechaDesde;
	private Date fechaHasta;
	@Save
	private List<InformeDeuda> informes;
	private Long idInforme;
	private InformeDeuda informe;
	private List<DetalleInformeDeuda> detalles;
	private String ultimaActualizacion;
	private String ultimaRecaudacion;
	private String ultimaCobranza;
	private String[] lineasSeleccionadas;
	private List<Linea> lineas;
	private String nombreArchivo;

	public InformeDeudaProcess() {
		fechaHasta = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -2);
		fechaDesde = c.getTime();
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		if (fechaDesde == null || fechaHasta == null) {
			errors.put("informedeuda.fechas", "informedeuda.fechas");
			return false;
		}
		informes = bp.createQuery(
				"select i from InformeDeuda i where i.fechaCreacion <= :fechaHasta and i.fechaCreacion >= :fechaDesde "
						+ "order by i.fechaCreacion desc")
				.setParameter("fechaHasta", fechaHasta).setParameter("fechaDesde", fechaDesde).list();
		forward = "InformeDeudaList";
		return true;
	}

	@ProcessMethod
	public boolean verInforme() {
		if (idInforme == null) {
			return false;
		}
		informe = (InformeDeuda) bp.getById(InformeDeuda.class, idInforme);
		detalles = informe.getDetalles();
		nombreArchivo = String.format("comprobantes_33_%1$tY%1$tm%1$td.xls", new Date());
		forward = "InformeDeudaVer";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean nuevoInforme() {
		try {
			ultimaActualizacion = DirectorHandler.getDirector("comportamientoPago.ultimaActualizacion").getValor();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Caratula c = (Caratula) bp
				.createQuery("select c  from Caratula c where c.actualizada = 1 ORDER BY c.fechaEnvio DESC")
				.setMaxResults(1).uniqueResult();
		if (c != null) {
			ultimaRecaudacion = c.getFechaEnvioStr();
			ultimaCobranza = c.getFechaCobranzaStr();
		}
		/**
		 * recupero todas las lineas en pesos y fuerzo la seleccion automatica de todas
		 * ellas
		 */
		lineas = bp.createQuery("select l from Linea l where l.moneda.id = 1 order by l.nombre").list();
		lineasSeleccionadas = new String[lineas.size()];
		for (Linea linea : lineas) {
			lineasSeleccionadas[lineas.indexOf(linea)] = linea.getId().toString();
		}
		forward = "NuevoInformeDeuda";
		return true;
	}

	@ProcessMethod
	public boolean guardarInforme() {
		String lineasId = "";
		if (lineasSeleccionadas != null) {
			for (String l : lineasSeleccionadas) {
				lineasId += l + ",";
			}
			if (!lineasId.isEmpty()) {
				lineasId = lineasId.substring(0, lineasId.length() - 1);
			}
		}

		InformeDeuda inf = new InformeDeuda();
		inf.setEstado(EstadoInformeDeuda.LISTO);
		inf.setFechaHasta(fechaHasta);
		inf.setLineasId(lineasId);
		bp.save(inf);
		ejecutar(inf);
		return true;
	}

	@ProcessMethod
	public boolean ejecutarInforme() {
		if (idInforme != null) {
			informe = (InformeDeuda) bp.getById(InformeDeuda.class, idInforme);
			if (informe != null) {
				ejecutar(informe);
				return true;
			}
		}
		return false;
	}

	private void ejecutar(InformeDeuda inf) {

		if (!inf.getEstado().equals(EstadoInformeDeuda.LISTO))
			return;

		inf.setEstado(EstadoInformeDeuda.EN_PROCESO);
		bp.update(inf);

		SecurityFilter datos = SecurityFilter.getSecurityFilterInstance();
		String usuario = datos.getConfig().getInitParameter("LiquidadorTask.usr");
		String password = datos.getConfig().getInitParameter("LiquidadorTask.pass");

		// String usr = DirectorHelper.getString("InformeDeuda.usuario");
		// String pass = DirectorHelper.getString("InformeDeuda.password");
		ExecutorService threadPool = (ExecutorService) com.asf.security.SessionHandler.getCurrentSessionHandler()
				.getRequest().getSession().getServletContext().getAttribute("InformeDeudaThreadPool");
		InformeDeudaTask task = new InformeDeudaTask(inf.getId(), usuario, password, threadPool);
		threadPool.submit(task);
	}

	public List<InformeDeuda> getInformes() {
		return informes;
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaDesde(String f) {
		this.fechaDesde = DateHelper.getDate(f);
	}

	public void setFechaHasta(String f) {
		this.fechaHasta = DateHelper.getDate(f);
	}

	public InformeDeuda getInforme() {
		return informe;
	}

	public List<DetalleInformeDeuda> getDetalles() {
		return detalles;
	}

	@Override
	protected String getDefaultForward() {
		return "InformeDeudaList";
	}

	public String getUltimaActualizacion() {
		return ultimaActualizacion;
	}

	public void setUltimaActualizacion(String ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}

	public String getUltimaRecaudacion() {
		return ultimaRecaudacion;
	}

	public void setUltimaRecaudacion(String ultimaRecaudacion) {
		this.ultimaRecaudacion = ultimaRecaudacion;
	}

	public String getUltimaCobranza() {
		return ultimaCobranza;
	}

	public void setUltimaCobranza(String ultimaCobranza) {
		this.ultimaCobranza = ultimaCobranza;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String[] getLineasSeleccionadas() {
		return lineasSeleccionadas;
	}

	public void setLineasSeleccionadas(String[] lineasSeleccionadas) {
		this.lineasSeleccionadas = lineasSeleccionadas;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public Long getIdInforme() {
		return idInforme;
	}

	public void setIdInforme(Long idInforme) {
		this.idInforme = idInforme;
	}

}
