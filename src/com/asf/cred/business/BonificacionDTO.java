package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.Bonificacion;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;

public class BonificacionDTO implements Serializable {
	private static final long serialVersionUID = 4428294876046218911L;
	private String linea;
	private String ente;
	private String convenio;
	private Long proyecto;
	private String titular;
	private Long cuil;
	private String resolucionNumero;
	private Date resolucionFecha;
	private Double montoCredito;
	private Double tasaBonificada;
	private Integer cuotaNumero;
	private Date cuotaVencimiento;
	private Double montoBonificado;
	private String cotomadores;

	public BonificacionDTO() {

	}

	public BonificacionDTO(long idCredito, long idBonificacion, long idCuota, long idBonDetalle) {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		Bonificacion bonificacion = (Bonificacion) bp.getById(Bonificacion.class, idBonificacion);
		Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);
		BonDetalle bonDetalle = (BonDetalle) bp.getById(BonDetalle.class, idBonDetalle);

		this.linea = credito.getLinea().getNombre();
		this.proyecto = credito.getNumeroAtencion();
		this.resolucionNumero = credito.getResolucion();
		this.resolucionFecha = credito.getFechaResolucion();
		this.montoCredito = credito.getFinanciamiento();
		this.cotomadores = credito.getCotomadores();
		this.titular = credito.getPersona().getNomb12();
		this.cuil = credito.getPersona().getCuil12();

		this.ente = bonificacion.getNombre();
		this.convenio = bonificacion.getConvenio().getNombre();
		this.tasaBonificada = bonificacion.getTasaBonificada();

		this.cuotaNumero = cuota.getNumero();
		this.cuotaVencimiento = cuota.getFechaVencimiento();

		this.montoBonificado = bonDetalle.getMonto();

	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getEnte() {
		return ente;
	}

	public void setEnte(String ente) {
		this.ente = ente;
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public Long getProyecto() {
		return proyecto;
	}

	public void setProyecto(Long proyecto) {
		this.proyecto = proyecto;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public Long getCuil() {
		return cuil;
	}

	public void setCuil(Long cuil) {
		this.cuil = cuil;
	}

	public String getResolucionNumero() {
		return resolucionNumero;
	}

	public void setResolucionNumero(String resolucionNumero) {
		this.resolucionNumero = resolucionNumero;
	}

	public Date getResolucionFecha() {
		return resolucionFecha;
	}

	public void setResolucionFecha(Date resolucionFecha) {
		this.resolucionFecha = resolucionFecha;
	}

	public Double getMontoCredito() {
		return montoCredito;
	}

	public void setMontoCredito(Double montoCredito) {
		this.montoCredito = montoCredito;
	}

	public Double getTasaBonificada() {
		return tasaBonificada;
	}

	public void setTasaBonificada(Double tasaBonificada) {
		this.tasaBonificada = tasaBonificada;
	}

	public Integer getCuotaNumero() {
		return cuotaNumero;
	}

	public void setCuotaNumero(Integer cuotaNumero) {
		this.cuotaNumero = cuotaNumero;
	}

	public Date getCuotaVencimiento() {
		return cuotaVencimiento;
	}

	public void setCuotaVencimiento(Date cuotaVencimiento) {
		this.cuotaVencimiento = cuotaVencimiento;
	}

	public Double getMontoBonificado() {
		return montoBonificado;
	}

	public void setMontoBonificado(Double montoBonificado) {
		this.montoBonificado = montoBonificado;
	}

	public String getCotomadores() {
		return cotomadores;
	}

	public void setCotomadores(String cotomadores) {
		this.cotomadores = cotomadores;
	}

}
