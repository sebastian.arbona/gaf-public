package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;

public class SaldosDeInteresesFacturados implements IProcess {

	private static final int NO_MOSTRAR_TABLA = 0;
	private static final int MOSTRAR_TABLA = 1;
	
	private Date fechaVencimientoHasta;
	private Date fechaDeCalculo;
	private int mostrar;

	private List<SaldosDeInteresesFacturadosBean> saldosDeInteresesFacturadosBeanList;

	private String numeroAtencion;
	private ReportResult reporte;
	private BusinessPersistance bp;
	private String accion;
	private HashMap<String,String> errores;
	private String forward = "SaldosDeInteresesFacturados";
	
	@SuppressWarnings("unchecked")
	public SaldosDeInteresesFacturados() {
		this.accion = "";
		this.mostrar = NO_MOSTRAR_TABLA;
		this.errores = new HashMap<String, String>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@SuppressWarnings("unchecked")
	public boolean doProcess() {
		if(this.accion.equals("cargarTabla")) {
			String consulta;
			List<Object> objectSaldosDeInteresesFacturadosBean;
			Object[] filaObjectSaldosDeInteresesFacturadosBean;
			SaldosDeInteresesFacturadosBean saldosDeInteresesFacturadosBean;
			String fechaVencimientoHasta;
			String fechaDeCalculo;

			this.mostrar = MOSTRAR_TABLA;

			fechaVencimientoHasta = this.getFechaVencimientoHastaStr();
			fechaVencimientoHasta = fechaVencimientoHasta.substring(fechaVencimientoHasta.lastIndexOf("/")+1) + 
				"-" + fechaVencimientoHasta.substring(fechaVencimientoHasta.indexOf("/")+1, fechaVencimientoHasta.lastIndexOf("/")) + 
				"-" + fechaVencimientoHasta.substring(0, fechaVencimientoHasta.indexOf("/")) + " 00:00:00";

			fechaDeCalculo = this.getFechaDeCalculoStr();
			fechaDeCalculo = fechaDeCalculo.substring(fechaDeCalculo.lastIndexOf("/")+1) + 
						"-" + fechaDeCalculo.substring(fechaDeCalculo.indexOf("/")+1, fechaDeCalculo.lastIndexOf("/")) + 
						"-" + fechaDeCalculo.substring(0, fechaDeCalculo.indexOf("/")) + " 23:59:59";

			
			consulta = 
				"SELECT proyecto, numeroCredito, " + 
				"linea, expediente, Resolucion, Titular, CUIL_12, " +
				"SUM(totalIntComp) 'totalIntComp', SUM(totalNC) 'totalNC', " +
				"SUM(totalND) 'totalND', SUM(pagos) 'pagosIntComp', EstadoCredito 'Estado', Comportamiento FROM ( " +
				"SELECT O.numeroAtencion proyecto, O.numeroCredito, " +
				"L.nombre linea, O.Expediente,O.Resolucion, P.NOMB_12 Titular, P.CUIL_12, " + 
				"SUM(CASE WHEN C.tipomov_id = 2 AND (b.id is null or b.tipo = 'Factura') THEN C.importe ELSE 0 END) AS totalIntComp, " +
				"SUM(CASE WHEN C.tipomov_id = 1 AND b.id is not null and b.tipo = 'Nota Credito' THEN C.importe ELSE 0 END) AS totalNC, " +
				"SUM(CASE WHEN C.tipomov_id = 2 AND b.id is not null and b.tipo = 'Nota Debito' THEN C.importe ELSE 0 END) AS totalND, " +
				"SUM(CASE WHEN C.tipomov_id = 1 AND C.tipomovimiento like 'pago' THEN C.importe ELSE 0 END) AS pagos, " +
				"estado.nombreEstado EstadoCredito, TC.TF_DESCRIPCION Comportamiento, cuota.fechaVencimiento " +
				"FROM CTACTE C " +
				"INNER JOIN Concepto con ON con.id = C.facturado_id " +
				"INNER JOIN Objetoi O ON C.objetoi_id = O.id " +
				"INNER JOIN Linea L ON O.linea_id = L.id " +
				"INNER JOIN Moneda M ON L.moneda_IDMONEDA = M.IDMONEDA " + 
				"INNER JOIN cotizacion CO ON M.IDMONEDA = CO.moneda_IDMONEDA AND fechaHasta IS NULL " +
				"INNER JOIN PERSONA P ON O.persona_IDPERSONA = P.IDPERSONA " +
				"LEFT JOIN ObjetoiComportamiento OC ON O.id = OC.objetoi_id " +
				"AND OC.fecha = (SELECT MAX(objc2.fecha) FROM ObjetoiComportamiento objc2 WHERE O.id = objc2.objetoi_id " +
				"AND objc2.fecha <= '" + fechaDeCalculo + "') " +
				"LEFT JOIN TIPIFICADORES TC ON OC.comportamientoPago = TC.TF_CODIGO AND TC.TF_CATEGORIA = 'comportamientoPago' " +
				"INNER JOIN Cuota cuota ON cuota.id = C.cuota_id " +
				"LEFT JOIN Boleto b ON C.boleto_id = b.id " +
				"INNER JOIN ObjetoiEstado oe ON oe.objetoi_id = O.id " +
				"AND oe.fecha = (SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE O.id = obje2.objetoi_id " +
				"AND obje2.fecha <= '" + fechaDeCalculo + "') " + 
				"INNER JOIN Estado estado ON oe.estado_idEstado = estado.idEstado " +
				"WHERE cuota.fechaVencimiento <='" + fechaVencimientoHasta + "' " +
				"AND C.fechaGeneracion <='" + fechaDeCalculo + "' " +
				"AND con.concepto_concepto IN ('com','bon') " +
				"GROUP BY " +
				"O.numeroAtencion, O.numeroCredito, O.Expediente,O.Resolucion, P.NOMB_12 , P.CUIL_12, " + 
				"L.nombre , TC.TF_DESCRIPCION, estado.nombreEstado,cuota.fechaVencimiento " +
				"HAVING SUM(CASE WHEN C.tipomov_id = 2 THEN C.importe ELSE -C.importe END) >= 0.01 " +
				") t " +
				"group by proyecto, numeroCredito, " +
				"linea, expediente, Resolucion, Titular, CUIL_12, EstadoCredito , Comportamiento " +
				"ORDER BY t.proyecto ";
			
			
			objectSaldosDeInteresesFacturadosBean = this.bp.createSQLQuery(consulta).list();
			this.saldosDeInteresesFacturadosBeanList = new ArrayList<SaldosDeInteresesFacturadosBean>();
			
			for(int i=0; i<objectSaldosDeInteresesFacturadosBean.size(); i++) {
				filaObjectSaldosDeInteresesFacturadosBean = (Object[]) objectSaldosDeInteresesFacturadosBean.get(i);

				saldosDeInteresesFacturadosBean = new SaldosDeInteresesFacturadosBean();
				saldosDeInteresesFacturadosBean.setProyecto(((BigDecimal) filaObjectSaldosDeInteresesFacturadosBean[0]).toString());
				saldosDeInteresesFacturadosBean.setNumeroCredito((String) filaObjectSaldosDeInteresesFacturadosBean[1]);
				saldosDeInteresesFacturadosBean.setLinea((String) filaObjectSaldosDeInteresesFacturadosBean[2]);
				saldosDeInteresesFacturadosBean.setExpediente((String) filaObjectSaldosDeInteresesFacturadosBean[3]);
				saldosDeInteresesFacturadosBean.setResolucion((String) filaObjectSaldosDeInteresesFacturadosBean[4]);
				saldosDeInteresesFacturadosBean.setTitular((String) filaObjectSaldosDeInteresesFacturadosBean[5]);
				saldosDeInteresesFacturadosBean.setCuil(((BigDecimal) filaObjectSaldosDeInteresesFacturadosBean[6]).toString());
				saldosDeInteresesFacturadosBean.setTotalInteresesCompensatorios((Double) filaObjectSaldosDeInteresesFacturadosBean[7]);
				saldosDeInteresesFacturadosBean.setTotalNotasDeCredito((Double) filaObjectSaldosDeInteresesFacturadosBean[8]);
				saldosDeInteresesFacturadosBean.setTotalNotasDeDebito((Double) filaObjectSaldosDeInteresesFacturadosBean[9]);
				saldosDeInteresesFacturadosBean.setPagosIntComp((Double) filaObjectSaldosDeInteresesFacturadosBean[10]);
				saldosDeInteresesFacturadosBean.setEstado((String) filaObjectSaldosDeInteresesFacturadosBean[11]);
				saldosDeInteresesFacturadosBean.setComportamiento((String) filaObjectSaldosDeInteresesFacturadosBean[12]);

				this.saldosDeInteresesFacturadosBeanList.add(saldosDeInteresesFacturadosBean);
			}
		}
		else
			this.mostrar = NO_MOSTRAR_TABLA;

		return true;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public int getMostrar() {
		return mostrar;
	}

	public void setMostrar(int mostrar) {
		this.mostrar = mostrar;
	}

	public void setSaldosDeInteresesFacturadosBeanList(List<SaldosDeInteresesFacturadosBean> saldosDeInteresesFacturadosBeanList) {
		this.saldosDeInteresesFacturadosBeanList = saldosDeInteresesFacturadosBeanList;
	}

	public List<SaldosDeInteresesFacturadosBean> getSaldosDeInteresesFacturadosBeanList() {
		return this.saldosDeInteresesFacturadosBeanList;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	protected String getDefaultForward() {
		return forward;
	}
	
	public Date getFechaVencimientoHasta() {
		return this.fechaVencimientoHasta;
	}
	
	public void setFechaVencimientoHasta(Date fechaVencimientoHasta) {
		this.fechaVencimientoHasta = fechaVencimientoHasta;
	}

	public String getFechaVencimientoHastaStr() {
		return DateHelper.getString(this.fechaVencimientoHasta);
	}

	public void setFechaVencimientoHastaStr(String fechaVencimientoHasta) {
		this.fechaVencimientoHasta = DateHelper.getDate(fechaVencimientoHasta);
	}

	public Date getFechaDeCalculo() {
		return this.fechaDeCalculo;
	}

	public void setFechaDeCalculo(Date fechaDeCalculo) {
		this.fechaDeCalculo = fechaDeCalculo;
	}

	public String getFechaDeCalculoStr() {
		return DateHelper.getString(this.fechaDeCalculo);
	}

	public void setFechaDeCalculoStr(String fechaDeCalculo) {
		this.fechaDeCalculo = DateHelper.getDate(fechaDeCalculo);
	}

	@SuppressWarnings("unchecked")
	public Collection getCollection(HashMap arg0) {
		return null;
	}

	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}