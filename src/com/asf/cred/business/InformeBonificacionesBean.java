package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

import com.asf.util.DirectorHelper;

public class InformeBonificacionesBean implements Serializable {

	private static final long serialVersionUID = 2389998227362599781L;
	
	private static final String DIRECTOR_NIVEL_1 = "CaidaBonificaciones.nivel1";
	private static final String DIRECTOR_NIVEL_2 = "CaidaBonificaciones.nivel2";
	
	private Long idObjetoi;
	private Long numeroAtencion;
	private String expediente;
	private String titular;
	private String cuit;
	private String linea;
	private String comportamiento;
	private String estado;
	private String convenio;
	private String tasaBonificacion;
	private String tipoBonificacion;
	private int diasMora;
	private double deuda;
	private Date fechaBaja;
	
	private int diasAlerta;
	private Date notifFecha;
	private String notifObservaciones;
	private String notifTipoAviso;
	private int nivel;
	
	private int diasExcedido;
	private Long idBonificacion;
	
	public InformeBonificacionesBean() {
	}
	
	public InformeBonificacionesBean(Object[] r) {
		idObjetoi = ((Number) r[0]).longValue();
		numeroAtencion = ((Number) r[1]).longValue();
		expediente = (String) r[2];
		titular = (String) r[3];
		cuit = ((Number) r[4]).toString();
		linea = (String) r[5];
		comportamiento = (String) r[6];
		estado = (String) r[7];
		convenio = (String) r[8];
		tasaBonificacion = (String) r[9];
		tipoBonificacion = (String) r[10];
		
		if (r.length > 12) {
			diasMora = ((Number) r[11]).intValue();
			diasAlerta = ((Number) r[12]).intValue();
			notifFecha = (Date) r[13];
			notifObservaciones = (String) r[14];
			notifTipoAviso = (String) r[15];
			
			String nivel1 = DirectorHelper.getString(DIRECTOR_NIVEL_1);
			String nivel2 = DirectorHelper.getString(DIRECTOR_NIVEL_2);
			if (nivel1 != null && nivel2 != null) {
				if (diasAlerta <= Integer.parseInt(nivel1)) {
					nivel = 1;
				} else if (diasAlerta <= Integer.parseInt(nivel2)) {
					nivel = 2;
				} else {
					nivel = 3;
				}
			}
		} else {
			fechaBaja = (Date) r[11];
		}
	}
	
	public Long getIdObjetoi() {
		return idObjetoi;
	}
	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}
	public Long getNumeroAtencion() {
		return numeroAtencion;
	}
	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}
	public String getExpediente() {
		return expediente;
	}
	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getComportamiento() {
		return comportamiento;
	}
	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getConvenio() {
		return convenio;
	}
	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}
	public String getTasaBonificacion() {
		return tasaBonificacion;
	}
	public void setTasaBonificacion(String tasaBonificacion) {
		this.tasaBonificacion = tasaBonificacion;
	}
	public String getTipoBonificacion() {
		return tipoBonificacion;
	}
	public void setTipoBonificacion(String tipoBonificacion) {
		this.tipoBonificacion = tipoBonificacion;
	}
	public int getDiasMora() {
		return diasMora;
	}
	public void setDiasMora(int diasMora) {
		this.diasMora = diasMora;
	}
	public double getDeuda() {
		return deuda;
	}
	public void setDeuda(double deuda) {
		this.deuda = deuda;
	}
	public Date getFechaBaja() {
		return fechaBaja;
	}
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	public int getDiasAlerta() {
		return diasAlerta;
	}
	public void setDiasAlerta(int diasAlerta) {
		this.diasAlerta = diasAlerta;
	}
	public Date getNotifFecha() {
		return notifFecha;
	}
	public void setNotifFecha(Date notifFecha) {
		this.notifFecha = notifFecha;
	}
	public String getNotifObservaciones() {
		return notifObservaciones;
	}
	public void setNotifObservaciones(String notifObservaciones) {
		this.notifObservaciones = notifObservaciones;
	}
	public String getNotifTipoAviso() {
		return notifTipoAviso;
	}
	public void setNotifTipoAviso(String notifTipoAviso) {
		this.notifTipoAviso = notifTipoAviso;
	}
	public int getNivel() {
		return nivel;
	}
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	public Long getIdBonificacion() {
		return idBonificacion;
	}
	public void setIdBonificacion(Long idBonificacion) {
		this.idBonificacion = idBonificacion;
	}
	public int getDiasExcedido() {
		return diasExcedido;
	}
	public void setDiasExcedido(int diasExcedido) {
		this.diasExcedido = diasExcedido;
	}	
}
