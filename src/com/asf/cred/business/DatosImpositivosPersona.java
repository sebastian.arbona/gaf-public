package com.asf.cred.business;

public class DatosImpositivosPersona {

	private String actividad;
	private String impuesto;
	private String fechaAlta;
	private String fechaBaja;

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaBaja(String fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getFechaBaja() {
		return fechaBaja;
	}
}
