package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.struts.action.ActionMessage;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.TipificadorHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.CaducidadPlazo;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CaducidadPlazoProcess extends ConversationProcess {

	private static final int CADUCIDAD_NUMERO_CUOTA = 999;
	@Save
	private String nroAtencion;
	@Save
	private Long idObjetoi;
	@Save
	private Objetoi objetoi;
	@Save
	private double montoGastos;
	@Save
	private Date fechaCaducidad;
	@Save
	private Date fechaCalculo = new Date();
	@Save
	private Cuota cuotaCaducidad;
	@Save
	private Cuota siguienteCuota;
	@Save
	private BeanCtaCte deudaTotal;
	@Save
	private BeanCtaCte deudaCaducidad;
	@Save
	private List<CuentaCorrienteAmpliadaBean> movimientos;
	@Save
	private double montoPunitorio;
	@Save
	private double montoMoratorio;
	@Save
	private double deudaExigible;
	@Save
	private double deudaMontoTotal;
	@Save
	private String criterioFecha;

	private Cuota cuotaAfectada;
	private Usuario usuario;
	private Tipomov tipoCredito;
	private Tipomov tipoDebito;
	private List<Ctacte> movsCaducidad;
	private String fechaCaducidadStr;
	private String fechaCalculoStr;
	private boolean permitido;
	private boolean guardado;

	// administracion de gastos
	@Save
	private List<GastoBean> gastos;
	private String motivoGasto;
	private double montoGasto;
	private String idGastoEliminar;
	@Save
	private List<BeanCtaCte> deudaTotalPorCuota;

	@ProcessMethod
	public boolean buscar() {
		objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
				.setParameter("nroAtencion", new Long(nroAtencion)).uniqueResult();

		idObjetoi = objetoi.getId();

		movimientos = new ArrayList<CuentaCorrienteAmpliadaBean>(); // no mostrar la vista previa
		fechaCaducidad = null;
		fechaCaducidadStr = null;

		if (objetoi == null) {
			errors.put("credito.caducidadplazo.existe", "credito.caducidadplazo.existe");
			return false;
		}

		if (objetoi.getCaducidadPlazo() != null) {
			errors.put("credito.caducidadplazo.existe", "credito.caducidadplazo.existe");
			return false;
		}

		return true;
	}

	protected void buscarObjetoi() {
		if (idObjetoi != null && idObjetoi != 0) {
			objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		}
	}

	@ProcessMethod
	public boolean vistaPrevia() {
		if (objetoi.getCaducidadPlazo() != null) {
			errors.put("credito.caducidadplazo.existe", "credito.caducidadplazo.existe");
			return false;
		}

		validarBonificaciones();

		if (fechaCalculoStr != null) {
			fechaCalculo = DateHelper.getDate(fechaCalculoStr);
		}
		if (fechaCalculo == null) {
			fechaCalculo = new Date();
		}

		buscarCuotaCaducidad();
		calcularFechaCaducidad();
		calcularDeuda();

		// capital + compensatorio + gastos + multas
		// no incluye moratorio ni punitorio
		deudaMontoTotal = deudaTotal.getCapital() + deudaTotal.getCompensatorio() + deudaTotal.getGastos()
				+ deudaTotal.getMultas();
		/*
		 * Ticket 10036 deudaExigible = deudaCaducidad.getCapital() +
		 * deudaCaducidad.getCompensatorio() + deudaCaducidad.getGastos() +
		 * deudaCaducidad.getMultas();
		 */
		deudaExigible = deudaCaducidad.getCapital() + deudaCaducidad.getCompensatorio();

		if (gastos == null) {
			gastos = new ArrayList<CaducidadPlazoProcess.GastoBean>();
		}

		resetSimulacion();

		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean guardar() {
		try {
			bp.begin();
			usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
			tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
			tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();

			objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoi);

			cuotaAfectada = new Cuota();
			cuotaAfectada.setCredito_id(objetoi.getId());
			cuotaAfectada.setEstado(Cuota.SIN_CANCELAR);
			cuotaAfectada.setFechaGeneracion(fechaCaducidad);
			cuotaAfectada.setFechaVencimiento(fechaCaducidad);
			cuotaAfectada.setNumero(CADUCIDAD_NUMERO_CUOTA);
			cuotaAfectada.setGastos(0.0);
			cuotaAfectada.setMoratorio(0.0);
			cuotaAfectada.setPunitorio(0.0);
			cuotaAfectada.setMultas(0.0);
			cuotaAfectada.setBonificacion(0.0);
			bp.save(cuotaAfectada);

			movsCaducidad = new ArrayList<Ctacte>();

			crearNotaCredito();
			crearNotaDebitoDeuda();
			crearNotaDebitoGastos();
			crearNotaDebitoIntereses();

			Estado estadoCaducidad = (Estado) bp.getNamedQuery("Estado.findByNombreEstado")
					.setParameter("nombreEstado", Estado.CADUCIDAD_PLAZOS).uniqueResult();
			if (estadoCaducidad != null) {
				ObjetoiEstado actual = objetoi.getEstadoActual();
				actual.setFechaHasta(new Date());

				bp.update(actual);

				ObjetoiEstado caducidad = new ObjetoiEstado();
				caducidad.setFecha(new Date());
				caducidad.setObjetoi(objetoi);
				caducidad.setEstado(estadoCaducidad);

				bp.save(caducidad);
			}

			CreditoHandler ch = new CreditoHandler();
			ch.actualizarComportamiento(objetoi, fechaCaducidad, fechaCalculo);

			CaducidadPlazo cad = new CaducidadPlazo();
			cad.setUsuario(usuario);
			cad.setFecha(fechaCaducidad);
			cad.setMovimientos(movsCaducidad);
			cad.setCuota(cuotaAfectada);
			objetoi.setCaducidadPlazo(cad);
			bp.save(cad);
			bp.update(objetoi);

			SessionHandler.getCurrentSessionHandler().getRequest().setAttribute("credito.caducidadplazo.ok",
					new ActionMessage("credito.caducidadplazo.ok", true));
			// errors.put("credito.caducidadplazo.ok", "credito.caducidadplazo.ok");
			guardado = true; // oculta el boton de guardar

			bp.commit();
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("credito.caducidadplazo.error", "credito.caducidadplazo.error");
			bp.rollback();
		}

		return guardado || errors.isEmpty();
	}

	@ProcessMethod
	public boolean agregar() {
		String motivoDesc = TipificadorHelper.getString("ctacte.detalle", motivoGasto);
		GastoBean bean = new GastoBean(UUID.randomUUID().toString(), montoGasto, motivoDesc);
		gastos.add(bean);

		validarBonificaciones();

		resetSimulacion();
		return true;
	}

	@ProcessMethod
	public boolean eliminar() {
		GastoBean bean = new GastoBean(idGastoEliminar, 0, null); // remover comparando por id
		gastos.remove(bean);

		validarBonificaciones();

		resetSimulacion();
		return true;
	}

	private void validarBonificaciones() {
		if (objetoi.getFechaBajaBonificacion() == null) {
			Number bonifs = (Number) bp
					.createQuery("select count(o) from ObjetoiBonificacion o where o.objetoi.id = :idCredito")
					.setParameter("idCredito", objetoi.getId()).uniqueResult();
			if (bonifs != null && bonifs.intValue() > 0) {
				errors.put("credito.caducidadplazo.caidabonif", "credito.caducidadplazo.caidabonif");
				permitido = false;
			} else {
				permitido = true;
			}
		} else {
			permitido = true;
		}
	}

	private void sumarGastos() {
		double totalGastos = 0;
		for (GastoBean gasto : gastos) {
			totalGastos += gasto.getMonto();
		}
		montoGastos = totalGastos;
	}

	private void resetSimulacion() {
		sumarGastos();

		movimientos = new ArrayList<CuentaCorrienteAmpliadaBean>();
		simularNotaCredito();
		simularNotaDebito();
		simularGastos();
		simularIntereses();

		if (fechaCalculoStr != null) {
			fechaCalculo = DateHelper.getDate(fechaCalculoStr);
		}
	}

	private void normalizarImportes(CuentaCorrienteAmpliadaBean cc) {
		cc.setCapital(normalizarImporte(cc.getCapital()));
		cc.setCompensatorio(normalizarImporte(cc.getCompensatorio()));
		cc.setGastos(normalizarImporte(cc.getGastos()));
		cc.setMoratorio(normalizarImporte(cc.getMoratorio()));
		cc.setPunitorio(normalizarImporte(cc.getPunitorio()));
		cc.setSaldo(normalizarImporte(cc.getSaldo()));
	}

	private double normalizarImporte(double m) {
		if (m >= -0.009 && m <= 0) {
			return 0;
		} else {
			return m;
		}
	}

	private void simularNotaCredito() {
		CuentaCorrienteAmpliadaBean cc = new CuentaCorrienteAmpliadaBean();
		cc.setCapital(-deudaTotal.getCapital());
		cc.setCompensatorio(-deudaTotal.getCompensatorio());
		cc.setGastos(-deudaTotal.getGastos() - deudaTotal.getMultas());
		cc.setMoratorio(-deudaTotal.getMoratorio());
		cc.setPunitorio(-deudaTotal.getPunitorio());
		cc.setFechaGeneracion(fechaCalculo);
		cc.setFechaVencimiento(fechaCalculo);
		cc.setTipoBoleto("Nota de Credito");
		cc.setDetalle(Ctacte.DETALLE_CADUCIDAD);
		cc.setSaldo(0); // deja sin saldo
		normalizarImportes(cc);
		movimientos.add(cc);
	}

	private void simularNotaDebito() {
		CuentaCorrienteAmpliadaBean cc = new CuentaCorrienteAmpliadaBean();
		cc.setCapital(deudaCaducidad.getCapital());
		cc.setCompensatorio(deudaCaducidad.getCompensatorio());
		cc.setGastos(deudaCaducidad.getGastos() + deudaCaducidad.getMultas());
		cc.setFechaGeneracion(fechaCalculo);
		cc.setFechaVencimiento(fechaCaducidad);
		cc.setTipoBoleto("Nota de Debito");
		cc.setDetalle(Ctacte.DETALLE_DEUDA_EXIGIBLE);
		cc.setSaldo(deudaExigible);
		normalizarImportes(cc);
		movimientos.add(cc);
	}

	private void simularGastos() {
		double montoAcumulado = 0;
		for (GastoBean gasto : gastos) {
			montoAcumulado += gasto.getMonto();

			CuentaCorrienteAmpliadaBean cc = new CuentaCorrienteAmpliadaBean();
			cc.setGastos(gasto.getMonto());
			cc.setFechaGeneracion(fechaCalculo);
			cc.setFechaVencimiento(fechaCalculo);
			cc.setTipoBoleto("Nota de Debito");
			cc.setDetalle(gasto.getMotivo());
			cc.setSaldo(deudaExigible + montoAcumulado);
			normalizarImportes(cc);
			movimientos.add(cc);
		}
	}

	private void simularIntereses() {
		int dias = (int) DateHelper.getDiffDates(fechaCaducidad, fechaCalculo, 2);

		montoPunitorio = objetoi.calcularPunitorioNew(deudaExigible, dias, dias, fechaCaducidad, fechaCaducidad,
				fechaCalculo);

		montoMoratorio = objetoi.calcularMoratorioNew(deudaExigible, dias, dias, fechaCaducidad, fechaCaducidad,
				fechaCalculo);

		if (montoPunitorio > 0 || montoMoratorio > 0) {
			CuentaCorrienteAmpliadaBean cc = new CuentaCorrienteAmpliadaBean();
			cc.setMoratorio(montoMoratorio);
			cc.setPunitorio(montoPunitorio);
			cc.setFechaGeneracion(fechaCalculo);
			cc.setFechaVencimiento(fechaCalculo);
			cc.setTipoBoleto("Nota de Debito");
			cc.setDetalle("Interes Moratorio y Punitorio");
			cc.setSaldo(deudaExigible + montoGastos + montoPunitorio + montoMoratorio);
			normalizarImportes(cc);
			movimientos.add(cc);
		}
	}

	/**
	 * Busca la primera cuota impaga
	 */
	@SuppressWarnings("unchecked")
	private void buscarCuotaCaducidad() {
		List<Cuota> cuotasSinCancelar = bp.getNamedQuery("Cuota.findSinCancelar").setParameter("idCredito", idObjetoi)
				.list();
		cuotasSinCancelar = new ArrayList<Cuota>(cuotasSinCancelar);
		cuotaCaducidad = cuotasSinCancelar.remove(0); // saco la primera con deuda, dejo el resto en la lista

		siguienteCuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idObjetoi)
				.setParameter("nroCuota", cuotaCaducidad.getNumero() + 1).uniqueResult();

	}

	/**
	 * Calcula la fecha de caducidad segun criterios:<br/>
	 * -Si no tiene pago parcial de capital, fecha de vencimiento de la cuota
	 * impaga<br/>
	 * -Si tiene pago parcial de capital, fecha del pago parcial<br/>
	 * -Si el pago se pasa de la siguiente cuota, fecha de vencimiento de la
	 * siguiente cuota
	 */
	@SuppressWarnings("unchecked")
	private void calcularFechaCaducidad() {
		Date fechaUsuario = null;
		if (fechaCaducidadStr != null) {
			fechaUsuario = DateHelper.getDate(fechaCaducidadStr);
		}

		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();

		// buscar pagos parciales de esta cuota, que cubran capital
		List<Ctacte> pagosParciales = bp.createQuery(
				"SELECT c FROM Ctacte c WHERE c.facturado.concepto.concepto LIKE :concepto AND c.cuota.id = :idCuota AND c.tipoMovimiento = 'pago'"
						+ " AND c.tipomov = :tipoCredito ORDER BY c.fechaGeneracion DESC")
				.setParameter("concepto", Concepto.CONCEPTO_CAPITAL).setParameter("idCuota", cuotaCaducidad.getId())
				.setParameter("tipoCredito", tipoCredito).list();

		if (!pagosParciales.isEmpty()) {
			// el ultimo pago que cubre capital
			fechaCaducidad = pagosParciales.get(0).getFechaGeneracion();

			criterioFecha = "Fecha de ultimo pago de capital";

			// si se pasa del siguiente vencimiento
			// dejar el siguiente vencimiento
			if (siguienteCuota != null && fechaCaducidad.after(siguienteCuota.getFechaVencimiento())) {
				fechaCaducidad = siguienteCuota.getFechaVencimiento();

				criterioFecha = "Fecha de vencimiento de la siguiente cuota impaga";
			}
		} else {
			// no hay pagos parciales que cubran capital
			// usar fecha de vencimiento
			fechaCaducidad = cuotaCaducidad.getFechaVencimiento();

			criterioFecha = "Fecha de vencimiento de cuota impaga";
		}

		// validar la fecha que ingreso el usuario
		if (fechaUsuario != null) {
			if (fechaUsuario.before(fechaCaducidad)) {
				errors.put("credito.caducidadplazo.fechaUsuario", "credito.caducidadplazo.fechaUsuario");
			} else {
				fechaCaducidad = fechaUsuario;
			}
		}

		fechaCaducidadStr = DateHelper.getString(fechaCaducidad);
	}

	/**
	 * Calcula la deuda total a hoy, para hacer la NC y calcula la deuda total a la
	 * fecha de caducidad (que puede ser menor) para hacer la ND.<br/>
	 * Guarda los beans que detallan como esta formada la deuda por conceptos
	 */
	@SuppressWarnings("unchecked")
	private void calcularDeuda() {
		System.out.println("--Caducidad de plazo, calcular deuda");
		CreditoHandler h = new CreditoHandler();

		// armar lista de deuda por cuota
		// para luego generar creditos separados por cuota
		Number count = (Number) bp.createQuery("select count(c) from Cuota c where c.credito.id = :idCredito")
				.setParameter("idCredito", objetoi.getId()).uniqueResult();
		int cantCuotas = count.intValue();
		double deudaCapital = 0, deudaCompensatorio = 0, saldoBonif = 0, deudaGastos = 0, deudaGastosRec = 0,
				deudaMultas = 0, deudaMoratorio = 0, deudaPunitorio = 0;

		deudaTotalPorCuota = new ArrayList<BeanCtaCte>(cantCuotas);
		for (int c = 1; c <= cantCuotas; c++) {
			Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idObjetoi)
					.setParameter("nroCuota", c).uniqueResult();
			double deudaCapitalCuota = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setParameter("concepto", Concepto.CONCEPTO_CAPITAL).setParameter("idCuota", cuota.getId()).list());
			double deudaCompensatorioCuota = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setParameter("concepto", Concepto.CONCEPTO_COMPENSATORIO).setParameter("idCuota", cuota.getId())
					.list());
			double saldoBonifCuota = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setParameter("concepto", Concepto.CONCEPTO_BONIFICACION).setParameter("idCuota", cuota.getId())
					.list());
			double deudaGastosCuota = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setParameter("concepto", Concepto.CONCEPTO_GASTOS).setParameter("idCuota", cuota.getId()).list());
			double deudaGastosRecCuota = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setParameter("concepto", Concepto.CONCEPTO_GASTOS_A_RECUPERAR)
					.setParameter("idCuota", cuota.getId()).list());
			double deudaMultasCuota = h.calcularSaldo(bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setParameter("concepto", Concepto.CONCEPTO_MULTAS).setParameter("idCuota", cuota.getId()).list());
			double deudaMoratorioCuota = h.calcularSaldo(
					bp.getNamedQuery("Ctacte.findByConceptoCuota").setParameter("concepto", Concepto.CONCEPTO_MORATORIO)
							.setParameter("idCuota", cuota.getId()).list());
			double deudaPunitorioCuota = h.calcularSaldo(
					bp.getNamedQuery("Ctacte.findByConceptoCuota").setParameter("concepto", Concepto.CONCEPTO_PUNITORIO)
							.setParameter("idCuota", cuota.getId()).list());
			BeanCtaCte deudaCuota = new BeanCtaCte();
			deudaCuota.setCapital(deudaCapitalCuota);
			deudaCuota.setCompensatorio(deudaCompensatorioCuota + saldoBonifCuota); // saldoBonif viene negativo
			deudaCuota.setCompensatorioBruto(deudaCompensatorioCuota);
			deudaCuota.setGastosRec(deudaGastosRecCuota);
			deudaCuota.setGastosSimples(deudaGastosCuota);
			deudaCuota.setMultas(deudaMultasCuota);
			deudaCuota.setMoratorio(deudaMoratorioCuota);
			deudaCuota.setPunitorio(deudaPunitorioCuota);
			deudaCuota.setNumero(cuota.getNumero());
			deudaCuota.setIdCuota(cuota.getId());
			deudaTotalPorCuota.add(deudaCuota); // quedan en esta lista

			deudaCapital += deudaCapitalCuota;
			deudaCompensatorio += deudaCompensatorioCuota;
			saldoBonif += saldoBonifCuota;
			deudaGastos += deudaGastosCuota;
			deudaGastosRec += deudaGastosRecCuota;
			deudaMultas += deudaMultasCuota;
			deudaMoratorio += deudaMoratorioCuota;
			deudaPunitorio += deudaPunitorioCuota;
		}

		deudaTotal = new BeanCtaCte();
		deudaTotal.setCapital(deudaCapital);
		deudaTotal.setCompensatorio(deudaCompensatorio + saldoBonif); // saldoBonif viene negativo
		deudaTotal.setCompensatorioBruto(deudaCompensatorio);
		deudaTotal.setGastosRec(deudaGastosRec);
		deudaTotal.setGastosSimples(deudaGastos);
		deudaTotal.setMultas(deudaMultas);
		deudaTotal.setMoratorio(deudaMoratorio);
		deudaTotal.setPunitorio(deudaPunitorio);

		CreditoCalculoDeuda deudaTotalCaducidad = new CreditoCalculoDeuda(idObjetoi, fechaCalculo);
		deudaTotalCaducidad.calcular();

		System.out.println("  fecha calculo " + fechaCalculo);
		System.out.println("  fecha caducidad " + fechaCaducidad);

		deudaCaducidad = deudaTotalCaducidad.getTotal();

		// compensatorio calculado hasta la fecha de caducidad
		// cuando cae en una fecha de vencimiento -> es el calculado por
		// CreditoCalculoDeuda
		// cuando cae en el medio de un periodo de devengamiento -> calcular
		// proporcional con periodo = transaccion
		// (los dias de periodo son solo hasta la fecha de caducidad
		// aca se resta el que viene calculado y se suma el correcto)

		// busco si cae en un vencimiento o entre medio
		BeanCtaCte beanAnteriorCaducidad = null;
		BeanCtaCte beanPosteriorCaducidad = null;
		BeanCtaCte beanVencimiento = null;

		for (BeanCtaCte bean : deudaTotalCaducidad.getBeans()) {
			if (bean.isCuota()) {
				System.out.println("  bean " + bean.getNumero() + " " + bean.getFechaVencimiento());
				if (bean.getFechaVencimiento() != null
						&& bean.getFechaVencimiento().getTime() == fechaCaducidad.getTime()) {
					// coincide con un vencimiento
					beanVencimiento = bean;
				} else if (beanPosteriorCaducidad == null && bean.getFechaVencimiento() != null
						&& bean.getFechaVencimiento().getTime() > fechaCaducidad.getTime()) {
					// busco el primero que tenga la fecha de vencimiento posterior a la caducidad
					beanPosteriorCaducidad = bean;
				} else if (bean.getFechaVencimiento() != null
						&& bean.getFechaVencimiento().getTime() < fechaCaducidad.getTime()) {
					// voy guardando el ultimo que sea anterior a la caducidad
					beanAnteriorCaducidad = bean;
				}
			}
		}

		System.out.println();
		System.out.println(
				"  bean coincide vencimiento " + (beanVencimiento != null ? beanVencimiento.getNumero() : "NO"));
		System.out.println("  bean anterior caducidad   "
				+ (beanAnteriorCaducidad != null ? beanAnteriorCaducidad.getNumero() : "NO"));
		System.out.println("  bean posterior caducidad  "
				+ (beanPosteriorCaducidad != null ? beanPosteriorCaducidad.getNumero() : "NO"));
		System.out.println();

		// reseteo el que viene calculado
		deudaCaducidad.setCompensatorio(0.0);

		// cae entre medio, hay que calcular proporcional
		if (beanVencimiento == null && beanPosteriorCaducidad != null && beanAnteriorCaducidad != null) {
			System.out.println("  cae en el medio - proporcional");
			// calculo de compensatorio proporcional con diasperiodo = diastransaccion
			int numeroCuotaCaducidad = beanPosteriorCaducidad.getNumero();
			Cuota cuotaCaducidad = (Cuota) bp.getNamedQuery("Cuota.findByNumero")
					.setParameter("idCredito", getIdObjetoi()).setParameter("nroCuota", numeroCuotaCaducidad)
					.uniqueResult();
			int numeroAnteriorCaducidad = beanAnteriorCaducidad.getNumero();
			Cuota cuotaAnterior = (Cuota) bp.getNamedQuery("Cuota.findByNumero")
					.setParameter("idCredito", getIdObjetoi()).setParameter("nroCuota", numeroAnteriorCaducidad)
					.uniqueResult();
			double saldoCapital = Objetoi.calcularSaldoCapital(cuotaCaducidad, fechaCaducidad);

			System.out.println("  saldo capital a fechaCaducidad " + saldoCapital);
			Objetoi proyectoCalculo = cuotaCaducidad.getCredito();
			int diasTransaccion = (int) DateHelper.getDiffDates(cuotaAnterior.getFechaVencimiento(), fechaCaducidad, 2);
			int diasPeriodo = (int) DateHelper.getDiffDates(cuotaAnterior.getFechaVencimiento(), fechaCaducidad, 2);
			double compensatorio = proyectoCalculo.calcularCompensatorioNew(saldoCapital, diasPeriodo, diasTransaccion,
					cuotaAnterior.getFechaVencimiento(), cuotaAnterior.getFechaVencimiento(), fechaCaducidad);
			deudaCaducidad.setCompensatorio(deudaCaducidad.getCompensatorio() + compensatorio);
		}

		System.out.println("  --compensatorio devengado no pagado");

		// saldo de compensatorio devengado no pagado
		// busco saldo de compensatorio/bonificacion por cada cuota
		// solo para cuotas que tengan fechaVencimiento <= fechaCaducidad
		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", getIdObjetoi()).list();
		for (Cuota cuota : cuotas) {
			System.out.println("	cuota " + cuota.getNumero());
			if (cuota.getFechaVencimiento().getTime() > fechaCaducidad.getTime()) {
				break;
			}

			System.out.println("	cuota vencida");
			// saldo de comp/bonif
			List<Ctacte> ccsComp = h.listarCtaCteConceptos(cuota, fechaCalculo, Concepto.CONCEPTO_COMPENSATORIO);
			List<Ctacte> ccsBonif = h.listarCtaCteConceptos(cuota, fechaCalculo, Concepto.CONCEPTO_BONIFICACION);
			double saldoComp = h.calcularSaldo(ccsComp);
			double saldoBon = h.calcularSaldo(ccsBonif);

			System.out.println("	saldo compensatorio " + saldoComp);
			System.out.println("	saldo bonificaciones " + saldoBon);

			deudaCaducidad.setCompensatorio(deudaCaducidad.getCompensatorio() + saldoComp + saldoBon);

			System.out.println("	saldo acumulado " + deudaCaducidad.getCompensatorio());
		}

	}

	private void crearNotaDebitoIntereses() {
		if (montoMoratorio > 0 || montoPunitorio > 0) {
			Calendar c = Calendar.getInstance();
			int periodo = c.get(Calendar.YEAR);

			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;

			// boleto tipo nota de credito
			Boleto notaDebito = new Boleto();
			notaDebito.setFechaEmision(fechaCalculo);
			notaDebito.setFechaVencimiento(fechaCalculo);
			notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			notaDebito.setUsuario(usuario);
			// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna
			// cuota
			notaDebito.setObjetoi(objetoi);
			notaDebito.setPeriodoBoleto((long) periodo);
			notaDebito.setVerificadorBoleto(0L);
			notaDebito.setImporte(montoMoratorio + montoPunitorio);
			notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
			bp.save(notaDebito);

			if (montoMoratorio > 0) {
				Ctacte cc = guardarConcepto(Concepto.CONCEPTO_MORATORIO, montoMoratorio, fechaCalculo, notaDebito,
						movimientoCtaCte, itemCtaCte, tipoDebito, "Int. Moratorio", cuotaAfectada, "cuota");
				cc.setFechaVencimiento(fechaCalculo); // por defecto es fechaCaducidad
				bp.saveOrUpdate(cc);
				itemCtaCte++;
			}
			if (montoPunitorio > 0) {
				Ctacte cc = guardarConcepto(Concepto.CONCEPTO_PUNITORIO, montoPunitorio, fechaCalculo, notaDebito,
						movimientoCtaCte, itemCtaCte, tipoDebito, "Int. Punitorio", cuotaAfectada, "cuota");
				cc.setFechaVencimiento(fechaCalculo); // por defecto es fechaCaducidad
				bp.saveOrUpdate(cc);
			}
		}
	}

	private void crearNotaDebitoGastos() {
		if (!gastos.isEmpty()) {
			Calendar c = Calendar.getInstance();
			int periodo = c.get(Calendar.YEAR);

			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;

			// boleto tipo nota de credito
			Boleto notaDebito = new Boleto();
			notaDebito.setFechaEmision(fechaCalculo);
			notaDebito.setFechaVencimiento(fechaCalculo);
			notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			notaDebito.setUsuario(usuario);
			// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna
			// cuota
			notaDebito.setObjetoi(objetoi);
			notaDebito.setPeriodoBoleto((long) periodo);
			notaDebito.setVerificadorBoleto(0L);
			notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
			bp.save(notaDebito);

			double totalGastos = 0;
			for (GastoBean gasto : gastos) {
				guardarConcepto(Concepto.CONCEPTO_GASTOS, gasto.getMonto(), fechaCalculo, notaDebito, movimientoCtaCte,
						itemCtaCte++, tipoDebito, gasto.getMotivo());
				totalGastos += gasto.getMonto();
			}

			notaDebito.setImporte(totalGastos);
			bp.update(notaDebito);
		}
	}

	private void crearNotaDebitoDeuda() {
		Calendar c = Calendar.getInstance();
		int periodo = c.get(Calendar.YEAR);

		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;

		// boleto tipo nota de credito
		Boleto notaDebito = new Boleto();
		notaDebito.setFechaEmision(fechaCalculo);
		notaDebito.setFechaVencimiento(fechaCaducidad);
		notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
		notaDebito.setUsuario(usuario);
		// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna
		// cuota
		notaDebito.setObjetoi(objetoi);
		notaDebito.setPeriodoBoleto((long) periodo);
		notaDebito.setVerificadorBoleto(0L);
		notaDebito.setImporte(deudaExigible);
		notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
		bp.save(notaDebito);

		// crear una ctacte y bolcon por cada concepto
		if (deudaCaducidad.getCapital() > 0) {
			guardarConcepto(Concepto.CONCEPTO_CAPITAL, deudaCaducidad.getCapital(), fechaCalculo, notaDebito,
					movimientoCtaCte, itemCtaCte, tipoDebito, Ctacte.DETALLE_DEUDA_EXIGIBLE, cuotaAfectada, "cuota");
			cuotaAfectada.setCapital(deudaCaducidad.getCapital());
			itemCtaCte++;
		}
		if (deudaCaducidad.getCompensatorio() > 0) {
			guardarConcepto(Concepto.CONCEPTO_COMPENSATORIO, deudaCaducidad.getCompensatorio(), fechaCalculo,
					notaDebito, movimientoCtaCte, itemCtaCte, tipoDebito, Ctacte.DETALLE_DEUDA_EXIGIBLE, cuotaAfectada,
					"cuota");
			cuotaAfectada.setCompensatorio(deudaCaducidad.getCompensatorio());
			itemCtaCte++;
		}
		if (deudaCaducidad.getGastosRec() > 0) {
			guardarConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, deudaCaducidad.getGastosRec(), fechaCalculo,
					notaDebito, movimientoCtaCte, itemCtaCte, tipoDebito, Ctacte.DETALLE_DEUDA_EXIGIBLE, cuotaAfectada,
					"cuota");
			itemCtaCte++;
		}
		if (deudaCaducidad.getGastosSimples() > 0) {
			guardarConcepto(Concepto.CONCEPTO_GASTOS, deudaCaducidad.getGastosSimples(), fechaCalculo, notaDebito,
					movimientoCtaCte, itemCtaCte, tipoDebito, Ctacte.DETALLE_DEUDA_EXIGIBLE, cuotaAfectada, "cuota");
			itemCtaCte++;
		}
		if (deudaCaducidad.getMultas() > 0) {
			guardarConcepto(Concepto.CONCEPTO_MULTAS, deudaCaducidad.getMultas(), fechaCalculo, notaDebito,
					movimientoCtaCte, itemCtaCte, tipoDebito, Ctacte.DETALLE_DEUDA_EXIGIBLE, cuotaAfectada, "cuota");
			itemCtaCte++;
		}
	}

	private void crearNotaCredito() {
		Calendar c = Calendar.getInstance();
		int periodo = c.get(Calendar.YEAR);

		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;

		// boleto tipo nota de credito
		Boleto notaCredito = new Boleto();
		notaCredito.setFechaEmision(fechaCalculo);
		notaCredito.setFechaVencimiento(fechaCalculo);
		notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
		notaCredito.setUsuario(usuario);
		// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta asociado a ninguna
		// cuota
		notaCredito.setObjetoi(objetoi);
		notaCredito.setPeriodoBoleto((long) periodo);
		notaCredito.setVerificadorBoleto(0L);
		notaCredito.setImporte(deudaMontoTotal);
		notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
		bp.save(notaCredito);

		for (BeanCtaCte deudaTotal : deudaTotalPorCuota) {
			Cuota cuotaActual = (Cuota) bp.getById(Cuota.class, deudaTotal.getIdCuota());

			// crear una ctacte y bolcon por cada concepto
			if (deudaTotal.getCapital() > 0) {
				guardarConcepto(Concepto.CONCEPTO_CAPITAL, deudaTotal.getCapital(), fechaCalculo, notaCredito,
						movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			if (deudaTotal.getCompensatorio() > 0) {
				guardarConcepto(Concepto.CONCEPTO_COMPENSATORIO, deudaTotal.getCompensatorio(), fechaCalculo,
						notaCredito, movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			if (deudaTotal.getGastosRec() > 0) {
				guardarConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, deudaTotal.getGastosRec(), fechaCalculo,
						notaCredito, movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			if (deudaTotal.getGastosSimples() > 0) {
				guardarConcepto(Concepto.CONCEPTO_GASTOS, deudaTotal.getGastosSimples(), fechaCalculo, notaCredito,
						movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			if (deudaTotal.getMultas() > 0) {
				guardarConcepto(Concepto.CONCEPTO_MULTAS, deudaTotal.getMultas(), fechaCalculo, notaCredito,
						movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			if (deudaTotal.getMoratorio() > 0) {
				guardarConcepto(Concepto.CONCEPTO_MORATORIO, deudaTotal.getMoratorio(), fechaCalculo, notaCredito,
						movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			if (deudaTotal.getPunitorio() > 0) {
				guardarConcepto(Concepto.CONCEPTO_PUNITORIO, deudaTotal.getPunitorio(), fechaCalculo, notaCredito,
						movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_CADUCIDAD, cuotaActual);
				itemCtaCte++;
			}
			cuotaActual.setEstado(Cuota.CANCELADA); // queda saldada
			bp.update(cuotaActual);
		}
	}

	private Ctacte guardarConcepto(String concepto, double valor, Date fechaEmision, Boleto nota, long movimientoCtaCte,
			long itemCtaCte, Tipomov tipo, String detalle) {
		return guardarConcepto(concepto, valor, fechaEmision, nota, movimientoCtaCte, itemCtaCte, tipo, detalle, null);
	}

	private Ctacte guardarConcepto(String concepto, double valor, Date fechaEmision, Boleto nota, long movimientoCtaCte,
			long itemCtaCte, Tipomov tipo, String detalle, Cuota cuota) {
		String tipoMovimiento = null;
		if (tipo.getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
			tipoMovimiento = "movManualDeb";
		} else {
			tipoMovimiento = "movManualCred";
		}
		return guardarConcepto(concepto, valor, fechaEmision, nota, movimientoCtaCte, itemCtaCte, tipo, detalle, cuota,
				tipoMovimiento);
	}

	private Ctacte guardarConcepto(String concepto, double valor, Date fechaEmision, Boleto nota, long movimientoCtaCte,
			long itemCtaCte, Tipomov tipo, String detalle, Cuota cuota, String tipoMovimiento) {
		Ctacte cc = objetoi.crearCtacte(concepto, valor, fechaEmision, nota, objetoi,
				cuota != null ? cuota : cuotaAfectada, Ctacte.TIPO_CONCEPTO_CADUCIDAD_PLAZO);
		cc.setTipomov(tipo);
		cc.setTipoMovimiento(tipoMovimiento);
		cc.setFechaVencimiento(fechaCaducidad); // vencimiento/calculo en fecha caducidad
		cc.setUsuario(usuario);
		cc.getId().setMovimientoCtacte(movimientoCtaCte);
		cc.getId().setItemCtacte(itemCtaCte);
		cc.setBoleto(nota);
		cc.setDetalle(detalle);

		// crear nuevo registro bolcon
		Bolcon bc = objetoi.crearBolcon(concepto, valor, nota, cc.getId(), cuota != null ? cuota : cuotaAfectada);
		bc.setTipomov(tipo);

		bp.save(cc);
		bp.save(bc);

		movsCaducidad.add(cc);

		return cc;
	}

	@Override
	protected String getDefaultForward() {
		return "CaducidadPlazo";
	}

	public String getNroAtencion() {
		return nroAtencion;
	}

	public void setNroAtencion(String nroAtencion) {
		this.nroAtencion = nroAtencion;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public double getMontoGastos() {
		return montoGastos;
	}

	public void setMontoGastos(double montoGastos) {
		this.montoGastos = montoGastos;
	}

	public List<CuentaCorrienteAmpliadaBean> getMovimientos() {
		return movimientos;
	}

	public String getFechaCaducidadStr() {
		return fechaCaducidadStr;
	}

	public void setFechaCaducidadStr(String f) {
		this.fechaCaducidadStr = f;
	}

	public boolean isPermitido() {
		return permitido;
	}

	public String getCriterioFecha() {
		return criterioFecha;
	}

	public boolean isGuardado() {
		return guardado;
	}

	public String getMotivoGasto() {
		return motivoGasto;
	}

	public void setMotivoGasto(String motivoGasto) {
		this.motivoGasto = motivoGasto;
	}

	public double getMontoGasto() {
		return montoGasto;
	}

	public void setMontoGasto(double montoGasto) {
		this.montoGasto = montoGasto;
	}

	public String getIdGastoEliminar() {
		return idGastoEliminar;
	}

	public void setIdGastoEliminar(String idGastoEliminar) {
		this.idGastoEliminar = idGastoEliminar;
	}

	public List<GastoBean> getGastos() {
		return gastos;
	}

	public String getFechaCalculoStr() {
		return DateHelper.getString(fechaCalculo);
	}

	public void setFechaCalculoStr(String s) {
		fechaCalculoStr = s;
	}

	public static class GastoBean {
		private String id;
		private double monto;
		private String motivo;

		public GastoBean(String id, double monto, String motivo) {
			this.id = id;
			this.monto = monto;
			this.motivo = motivo;
		}

		public String getId() {
			return id;
		}

		public double getMonto() {
			return monto;
		}

		public String getMontoStr() {
			return String.format("%,.2f", monto);
		}

		public String getMotivo() {
			return motivo;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			GastoBean other = (GastoBean) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

	}

}
