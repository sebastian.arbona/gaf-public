package com.asf.cred.business;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.asf.cred.TipificadorHandler;
import com.asf.cred.security.SessionHandler;
import com.asf.hibernate.mapping.Director;
import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.ObjetoiArchivo;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class NotificacionReportes extends DispatchAction {
	protected HashMap<String, Object> errores = new HashMap<String, Object>();
	private ZipOutputStream zip;
	private JasperReport reporte;
	private JasperReport reporte2;
	private Integer plazo;
	List<ReportResult> reportes;
	BufferedImage logo;

	public ActionForward generarReporte(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		boolean boleto = (Boolean) request.getSession().getAttribute("boleto");

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String stringNotificacion = "";
		Long idTipoNotificacion = (Long) request.getSession().getAttribute("tipoNoti");
		// Busco segun el codigo que tipo de notificacion es para generar el reporte
		// correcto
		if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("primer aviso")) {
			stringNotificacion = "/reports/NotificacionPrimerAviso.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("segundo aviso")) {
			stringNotificacion = "/reports/NotificacionSegundoAviso.jasper";
			plazo = 7;
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("aviso 48hs")) {
			stringNotificacion = "/reports/NotificacionTercerAviso.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("ultimo aviso")) {
			stringNotificacion = "/reports/NotificacionSegundoAviso.jasper";
			plazo = 5;
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("carta documento")) {
			stringNotificacion = "/reports/NotificacionCorreo.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("aviso prejudicial")) {
			stringNotificacion = "/reports/NotificacionPrejudicial.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("prorrogaVencimiento")) {
			stringNotificacion = "/reports/NotificacionProrroga.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("reclamo por acuerdo")) {
			stringNotificacion = "/reports/NotificacionReclamoAcuerdo.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("primer aviso con interes")) {
			stringNotificacion = "/reports/NotificacionPrimerAvisoInteres.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("segundo aviso con interes")) {
			stringNotificacion = "/reports/NotificacionSegundoAvisoInteres.jasper";
		} else if (buscarCodigo(idTipoNotificacion).equalsIgnoreCase("reclamo por res prorroga")) {
			stringNotificacion = "/reports/NotificacionReclamoAcuerdo1.jasper";
		}

		List<Long> ids = null;
		ids = obtenerIds(request.getParameter("ids"));

		StringBuilder u = new StringBuilder("http://");

		u.append(request.getServerName()).append(":");
		u.append(request.getServerPort());
		u.append(request.getContextPath());

		URL url = new URL(u.toString() + "/reports/boletoTotalDeuda.jasper");
		reporte = (JasperReport) JRLoader.loadObject(url.openStream());

		URL url2 = new URL(u.toString() + stringNotificacion);
		reporte2 = (JasperReport) JRLoader.loadObject(url2.openStream());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		zip = new ZipOutputStream(baos);

		for (Long id : ids) {
			Notificacion noti = buscarNotificacion(id, bp);
			generarReporteNotificacion(noti, bp);
			if (boleto) {
				generarReporte(noti, true, bp);
			}
		}
		zip.close();

		byte[] bytesZip = baos.toByteArray();

		// content type, y length, attachment
		response.setContentType("application/zip");
		response.setHeader("Content-Disposition",
				"attachment;filename=notificaciones" + String.format("%1$tY-%1$tm-%1$td", new Date()) + ".zip");
		response.setContentLength(bytesZip.length);
		response.getOutputStream().write(bytesZip);
		response.getOutputStream().close();

		return null;
	}

	public ActionForward pdf(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String boleto = request.getParameter("boleto");
		request.getSession().setAttribute("boleto", Boolean.valueOf(boleto));

		String tipoNoti = request.getParameter("tipoNoti");
		Tipificadores tipif = TipificadorHandler.getTipificador("notificacion.TipoAviso", tipoNoti);
		request.getSession().setAttribute("tipoNoti", tipif.getId());

		request.getSession().setAttribute("ids", request.getParameter("ids"));

		return generarReporte(mapping, form, request, response);
	}

	// Metodos privados

	private String buscarCodigo(Long idTipo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Tipificadores tipo = (Tipificadores) bp.getById(Tipificadores.class, idTipo);
		return tipo.getCodigo();
	}

	private List<Long> obtenerIds(String ids) {
		ids = (String) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("ids");
		String[] cadena = ids.split(",");
		List<Long> id = new ArrayList<Long>();
		for (String string : cadena) {
			if (!(string.equals("null"))) {
				id.add(Long.parseLong(string.trim()));
			}
		}
		return id;
	}

	private Notificacion buscarNotificacion(Long id, BusinessPersistance bp) {
		return (Notificacion) bp.getById(Notificacion.class, id);
	}

	private void generarReporteNotificacion(Notificacion noti, BusinessPersistance bp) throws JRException, IOException {

		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		List<Director> infoBanco = bp.createQuery("Select d from Director d where d.codigo like :codigo")
				.setParameter("codigo", "NOTIFICACION.%").list();
		String domicilio = "";
		String sucursal = "";
		String cuenta = "";
		String banco = "";
		for (Director director : infoBanco) {
			if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.NUMEROCUENTA")) {
				cuenta = director.getValor();
			} else if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.SUCURSALBANCO")) {
				sucursal = director.getValor();
			} else if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.NOMBREBANCO")) {
				banco = director.getValor();
			} else if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.DOMICILIOBANCO")) {
				domicilio = director.getValor();
			}
		}

		HashMap param = new HashMap();
		param.put("NOTIFICACION_ID", noti.getId());
		param.put("CUENTA", cuenta);
		param.put("SUCURSAL", sucursal);
		param.put("DOMICILIO", domicilio);
		param.put("BANCO", banco);
		param.put("FECHA", noti.getCredito().getFechaUltimoVencimiento());
		param.put("FECHACREADA", noti.getFechaCreadaStr());
		param.put("PLAZO", plazo);
		param.put("REPORTS_PATH", u.toString());
		param.put("SCHEMA2", bp.getSchema());
		param.put("LOGO", SessionHandler.getCurrentSessionHandler().getRequest().getSession().getServletContext()
				.getResourceAsStream("images/logoEmpresa.jpg"));
		param.put("FOOTER", SessionHandler.getCurrentSessionHandler().getRequest().getSession().getServletContext()
				.getResourceAsStream("images/footer_nuevo_ftyc.jpg"));
		param.put("BACKGROUND", SessionHandler.getCurrentSessionHandler().getRequest().getSession().getServletContext()
				.getResourceAsStream("images/ultimoaviso.jpg"));

		Connection conn = bp.getCurrentSession().connection();

		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte2, param, conn);

		byte[] pdfNoti = JasperExportManager.exportReportToPdf(jasperPrint);

		// Adjunto al Zip la notificacion
		adjuntarAZip(noti, pdfNoti, "Notificacion");
		saveObjetoIArchivo(bp, noti, pdfNoti, "Notificacion");
	}

	private void generarReporte(Notificacion noti, boolean add, BusinessPersistance bp)
			throws JRException, IOException {
		Leyenda leyenda = (Leyenda) bp.createQuery("Select l from Leyenda l where l.nombre =:nombre")
				.setParameter("nombre", "Notificacion de Mora").uniqueResult();

		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

		HashMap param = new HashMap();
		param.put("LEYENDA", leyenda.getTexto());
		param.put("VENCIMIENTO", DateHelper.getString(noti.getFechaVenc()));
		param.put("OBJETOI_ID", noti.getCredito().getId().toString());
		param.put("ID_PERSONA", noti.getCredito().getPersona_id().toString());
		param.put("MONEDA", noti.getCredito().getLinea().getMoneda().getDenominacion().toString());
		param.put("USUARIO", SessionHandler.getCurrentSessionHandler().getCurrentUser().toString());
		param.put("REPORTS_PATH", u.toString());
		param.put("SCHEMA", SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema());

		Connection conn = bp.getCurrentSession().connection();
		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, param, conn);
		byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);

		adjuntarAZip(noti, pdf, "Boleto");
		saveObjetoIArchivo(bp, noti, pdf, "Boleto");

		// try {
		// Leyenda leyenda = (Leyenda) bp.createQuery("Select l from Leyenda l where
		// l.nombre =:nombre").setParameter("nombre", "Notificacion de
		// Mora").uniqueResult();
		// reporte = new ReportResult();
		// reporte.setExport("PDF");
		// reporte.setReportName("boletoTotalDeuda.jasper");
		// reporte.setParams("LEYENDA="
		// + leyenda
		// + ";VENCIMIENTO="
		// + noti.getFechaVenc()
		// + ";OBJETOI_ID="
		// + noti.getCredito().getId()
		// + ";ID_PERSONA="
		// + noti.getCredito().getPersona_id()
		// + ";MONEDA="
		// + noti.getCredito().getMoneda().getDenominacion()
		// + ";USUARIO="
		// + SessionHandler.getCurrentSessionHandler().getCurrentUser()
		// + ";SCHEMA="
		// + BusinessPersistance.getSchema()
		// + ";REPORTS_PATH="
		// + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		// } catch (Exception e) {
		// this.errores.put("imprimir.informe.error", new
		// ActionMessage("imprimir.informe.error", "boletoTotalDeuda"));
		// }
		// if(add){
		// reportes.add(reporte);
		// }
	}

	private void adjuntarAZip(Notificacion n, byte[] pdf, String Notificacion) throws IOException {
		Date fecha = new Date();
		InputStream is = new ByteArrayInputStream(pdf);
		ZipEntry zipEntry = new ZipEntry(Notificacion + n.getId().toString() + ".pdf");
		zip.putNextEntry(zipEntry);
		byte[] buffer = new byte[pdf.length];
		int byteCount;
		while (-1 != (byteCount = is.read(buffer))) {
			zip.write(buffer, 0, byteCount);
		}
		zip.closeEntry();
		is.close();
	}

	private void saveObjetoIArchivo(BusinessPersistance bp, Notificacion notificacion, byte[] bytesPdf, String nombre) {
		ObjetoiArchivo objetoiArchivo = new ObjetoiArchivo();
		objetoiArchivo.setArchivo(bytesPdf);
		objetoiArchivo.setFechaAdjunto(new Date());
		objetoiArchivo.setMimetype("application/pdf");
		objetoiArchivo.setNombre(nombre + notificacion.getId().toString() + ".pdf");
		objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		objetoiArchivo.setCredito(notificacion.getCredito());
		objetoiArchivo.setNoBorrar(true);
		objetoiArchivo.setFuncionalidadOrigen("3");
		bp.save(objetoiArchivo);
		notificacion.setObjetoiArchivo(objetoiArchivo);
		bp.save(notificacion);
	}
}
