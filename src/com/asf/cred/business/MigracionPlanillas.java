package com.asf.cred.business;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.NumberHelper;
import com.asf.util.TipificadorHelper;
import com.civitas.hibernate.persona.Domicilio;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.civitas.hibernate.persona.PersonaVinculada;
import com.civitas.hibernate.persona.Provin;
import com.civitas.hibernate.persona.creditos.Contacto;
import com.civitas.importacion.excel.ExcelReader;
import com.civitas.importacion.excel.XLSWorkbookBean;
import com.civitas.migracion.BeanMigraContacto;
import com.civitas.migracion.BeanMigraCuota;
import com.civitas.migracion.BeanMigraDesembolso;
import com.civitas.migracion.BeanMigraDomicilio;
import com.civitas.migracion.BeanMigraGarantia;
import com.civitas.migracion.BeanMigraNotificacion;
import com.civitas.migracion.BeanMigraPago;
import com.civitas.migracion.BeanPlanillaProyecto;
import com.civitas.migracion.MigracionXLSX;
import com.civitas.migracion.cld.BeanGastosSeguro;
import com.civitas.migracion.cld.BeanLibroGastosSeguro;
import com.civitas.migracion.cld.MigracionGastosSeguro;
import com.civitas.migracion.ftyc.BeanLibroFTyC;
import com.civitas.migracion.ftyc.BeanPlanillaPersonas;
import com.civitas.migracion.ftyc.MigracionFTyCPersonas;
import com.civitas.migracion.generica.BeanLibroGenerico;
import com.civitas.migracion.generica.MigracionGenericaXLS;
import com.civitas.migracion.mdzf.BeanLibroMDZF;
import com.civitas.migracion.mdzf.BeanPlanillaDomicilio;
import com.civitas.migracion.mdzf.BeanPlanillaMora;
import com.civitas.migracion.mdzf.MigracionMdzfXLS;
import com.nirven.creditos.hibernate.CampoGarantia;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DomicilioObjetoi;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.GarantiaUso;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.creditos.hibernate.ValorCampoGarantia;
import com.nirven.expedientes.persistencia.Numerador;

public class MigracionPlanillas extends ConversationProcess {

	private String formatoCarga;
	private Long fideicomisoId;
	private Long lineaId;
	private FormFile archivo;

	private Boolean ignorarHojasOcultas = Boolean.TRUE;
	private XLSWorkbookBean workbook;

	private Tipomov tipoMovCredito;
	private Tipomov tipoMovDebito;
	private Usuario usuario;

	public MigracionPlanillas() {
		usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
		tipoMovCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		tipoMovDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
	}

	@ProcessMethod
	public boolean importar() {
		boolean resultado = false;
		try {
			String contentType = archivo.getContentType();
			InputStream inputStream = archivo.getInputStream();
			switch (contentType) {
			case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
				workbook = ExcelReader.readXLSX(inputStream, ignorarHojasOcultas);
				if (workbook == null) {
					errors.put("importacionMovimientos.errorExce", "importacionMovimientos.errorExce");
					resultado = false;
				} else {
					MigracionXLSX migracion;

					switch (formatoCarga.toLowerCase()) {
					case "formato1":
						// generico
						migracion = new MigracionGenericaXLS(workbook);
						BeanLibroGenerico libro = migracion.importar();
						resultado = aplicar1(libro);
						break;

					case "formato2":
						// mendoza fiduciaria
						migracion = new MigracionMdzfXLS(workbook);
						BeanLibroMDZF libroMDZF = migracion.importar();
						resultado = aplicar2(libroMDZF);
						break;
					case "formato3":
						// garantia seguros
						migracion = new MigracionGastosSeguro(workbook);
						BeanLibroGastosSeguro libroGastosSeguro = migracion.importar();
						resultado = aplicar3(libroGastosSeguro);
						break;
					case "formato4":
						migracion = new MigracionFTyCPersonas(workbook);
						System.out.println(
								"------------------------Comienza la lectura de la planilla------------------------");
						BeanLibroFTyC libroFTyC = migracion.importar();
						System.out.println(
								"------------------------Termina la lectura de la planilla------------------------");
						System.out.println(
								"------------------------Comienza el proceso de migracion------------------------");
						resultado = aplicar4(libroFTyC);
						System.out.println(
								"------------------------Termina el proceso de migracion------------------------");
						break;
					default:
						break;
					}
				}
				break;
			case "application/vnd.ms-excel":
			case "text/csv":
			case "text/tab-separated-values":
			default:
				errors.put("importacionMovimientos.formato", "importacionMovimientos.formato");
				resultado = false;
			}
		} catch (Exception e) {
			errors.put("importacionMovimientos.archivo", "importacionMovimientos.archivo");
			resultado = false;
		}
		return resultado;
	}

	public boolean aplicar1(BeanLibroGenerico libro) {
		// si no hay planillas, termina OK
		Boolean status = true;
		for (BeanPlanillaProyecto beanProyecto : libro.getProyectoList()) {
			try {
				bp.begin();
				ArrayList<BeanMigraDomicilio> domicilioList = beanProyecto.getDomicilioList();
				BeanMigraDomicilio domicilio = (domicilioList == null || domicilioList.isEmpty()) ? null
						: domicilioList.get(0);

				Long titularId = beanProyecto.getTitularId() == null
						? buscarPersona(beanProyecto.getNombreTitular(), beanProyecto.getCuitTitular(), domicilio)
						: beanProyecto.getTitularId();

				if (titularId != null) {
					// busca o crea una persona segun el cuit, crea domicilio real
					beanProyecto.setTitularId(titularId);
					// crea el proyecto y lo vincula al titular, el domicilio, tasas y estado
					Long idLinea = this.lineaId;
					beanProyecto.setObjetoiId(crearProyecto(beanProyecto, idLinea));
					// crea contactos
					crearContactos(beanProyecto);
					// crear garantias
					crearGarantias(beanProyecto);
					// crea notificaciones
					crearNotificaciones(beanProyecto);
					// crea los desembolsos y los ejecuta, generando las cuotas
					crearDesembolsos(beanProyecto);
					// facturar intereses
					facturarIntereses(beanProyecto);
					// aplica los pagos si existen
					aplicarPagos(beanProyecto, false);
				}
				bp.commit();
				status = true;
			} catch (Exception e) {
				bp.rollback();
				e.printStackTrace();
//				System.out.println("ERROR: " + beanProyecto.toString());
				status = false;
			} finally {
//				System.out.println("Status: " + status + " - " + beanProyecto.toString());
			}

		}
		return status;
	}

	public boolean aplicar2(BeanLibroMDZF libro) {
		for (BeanPlanillaDomicilio beanPlanillaDomicilio : libro.getDomicilioList()) {
			try {
				bp.begin();
				this.crearPersonas(beanPlanillaDomicilio);
				bp.commit();
			} catch (Exception e) {
				bp.rollback();
				e.printStackTrace();
				System.out.println("ERROR: " + beanPlanillaDomicilio.toString());
			}
		}
		/**
		 * procesa la planilla de mora para extraer el nombre de la linea asociado a
		 * cada titular
		 */
		HashMap<Long, String> titularesLinea = new HashMap<>();
		for (BeanPlanillaMora beanPlanillaMora : libro.getMoraList()) {
			if (titularesLinea.containsKey(beanPlanillaMora.getTitularCuit())) {
				/**
				 * si ya lo encontre no lo vuelvo a cargar ni sobreescribir el dato
				 */
			} else {
				titularesLinea.put(beanPlanillaMora.getTitularCuit(), beanPlanillaMora.getNombreLinea());
			}
		}
		Boolean status = true;
		for (BeanPlanillaProyecto beanPlanillaProyecto : libro.getProyectoList()) {
			try {
				bp.begin();
				ArrayList<BeanMigraDomicilio> domicilioList = beanPlanillaProyecto.getDomicilioList();
				Long titularId = buscarPersona(beanPlanillaProyecto.getNombreTitular(),
						beanPlanillaProyecto.getCuitTitular(), domicilioList.get(0));
				if (titularId != null) {
					// busca o crea una persona segun el cuit, crea domicilio real
					beanPlanillaProyecto.setTitularId(titularId);
					// crea el proyecto y lo vincula al titular, el domicilio, tasas y estado
					String nombreLinea = titularesLinea.get(beanPlanillaProyecto.getCuitTitular());
					Long idLinea;
					/**
					 * intenta recuperar la linea desde la planilla de mora, si no puede coloca el
					 * dato que se cargo en la pantalla
					 */
					if (nombreLinea == null || nombreLinea.isEmpty()) {
						idLinea = this.lineaId;
					} else {
						String consulta = "select id from linea where detalle like '%" + nombreLinea + "%'";
						try {
							idLinea = ((Number) bp.createSQLQuery(consulta).list().get(0)).longValue();
						} catch (Exception e) {
							idLinea = this.lineaId;
						}
					}
					beanPlanillaProyecto.setObjetoiId(crearProyecto(beanPlanillaProyecto, idLinea));
					// crea notificaciones
					crearNotificaciones(beanPlanillaProyecto);
					// crea los desembolsos y los ejecuta, generando las cuotas
					crearDesembolsos(beanPlanillaProyecto);
					// aplica los pagos si existen
					aplicarPagos(beanPlanillaProyecto, true);
				}
				bp.commit();
				status = true;
			} catch (Exception e) {
				bp.rollback();
				e.printStackTrace();
				System.out.println("ERROR: " + beanPlanillaProyecto.toString());
				status = false;
			} finally {
				System.out.println("Status: " + status + " - " + beanPlanillaProyecto.toString());
			}

		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean aplicar3(BeanLibroGastosSeguro libro) {
		ArrayList<BeanGastosSeguro> gastosSeguroList = libro.getGastosSeguroList();
		List<Cuota> cuotas = new ArrayList<Cuota>();
		for (BeanGastosSeguro beanGastosSeguro : gastosSeguroList) {
			if (beanGastosSeguro.getDominio() != null && beanGastosSeguro.getCuotaSeguro() != null) {
				Garantia garantia = (Garantia) bp
						.createQuery(
								"from Garantia g where g.identificadorUnico = '" + beanGastosSeguro.getDominio() + "'")
						.uniqueResult();
				if (garantia == null) {
					errors.put("importacionMovimientos.filasNo",
							new ActionMessage("importacionMovimientos.filasNo", beanGastosSeguro.getDominio()));
				} else {
					ObjetoiGarantia buscacreditoGar = (ObjetoiGarantia) bp
							.createQuery("from ObjetoiGarantia g where g.garantia = " + garantia.getId())
							.uniqueResult();
					/* Recuperar el credito para despues traer las cuotas */
					Objetoi credito = buscacreditoGar.getObjetoi();
					Date objDate = new Date();
					String fechaSql = DateHelper.getStringFull4(objDate);
					cuotas = bp.createQuery("from Cuota g where g.credito.id = " + credito.getId()
							+ " and fechaVencimiento >='" + fechaSql + "'").list();
					if (!cuotas.isEmpty()) {
						Cuota cuota = cuotas.get(0);
						generarCtacte(credito, cuota, credito.getLinea().getGastoId(), credito.getLinea().getGastoId(),
								cuota.getFechaVencimiento(), new BigDecimal(beanGastosSeguro.getCuotaSeguro()),
								Tipomov.TIPOMOV_DEBITO);
					} else {
						errors.put("importacionMovimientos.errorCreditoVigencia",
								"importacionMovimientos.errorCreditoVigencia");
					}
				}
			}
		}
		return true;
	}

	private boolean aplicar4(BeanLibroFTyC libroFTyC) {
		Persona persona;
		for (BeanPlanillaPersonas beanPlanillaPersonas : libroFTyC.getPersonasList()) {
			Long cuit = beanPlanillaPersonas.getCuit();
			List<Persona> personas = bp.getByFilterCast("from Persona p where p.cuil12 = " + cuit);
			if (personas == null || personas.isEmpty()) {
				if (!NumberHelper.validarCuit(cuit.toString())) {
					System.out.println("CUIT INVALIDO: " + beanPlanillaPersonas.toString());
				} else {
					persona = new Persona(null);
					persona.setCodi01(101l);
					persona.setNomb12(beanPlanillaPersonas.getNombreCompleto());
					persona.setCuil12(beanPlanillaPersonas.getCuit());
					persona.setMigracion(Boolean.TRUE);
					persona.setNudo12(beanPlanillaPersonas.getNumeroDocumentoStr());
					persona.setTipo12(beanPlanillaPersonas.getTipoPersonaStr());
					persona.setSexo12(beanPlanillaPersonas.getSexoStr());
					persona.setRazonSocial(beanPlanillaPersonas.getRazonSocialStr());
					String tipoDocumentoStr = beanPlanillaPersonas.getTipoDocumentoStr();
					if (tipoDocumentoStr != null && !tipoDocumentoStr.isEmpty()) {
						String sqlTipodoc = "select codi_47 from Tipodoc t where t.tido_47 = '" + tipoDocumentoStr
								+ "'";
						Number tipodocId = (Number) bp.createSQLQuery(sqlTipodoc).setMaxResults(1).list().get(0);
						persona.setCodi47(tipodocId.longValue());
					}
					String tipoSociedadStr = beanPlanillaPersonas.getTipoSociedadStr();
					if (tipoSociedadStr != null && !tipoSociedadStr.isEmpty()) {
						String sqlTsoci = "select codi_21 from Tsoci t where t.deta_21 = '" + tipoSociedadStr + "'";
						Number tsociId = (Number) bp.createSQLQuery(sqlTsoci).setMaxResults(1).list().get(0);
						persona.setCodi21(tsociId.longValue());
					}
					String nacionalidadStr = beanPlanillaPersonas.getNacionalidadStr();
					if (nacionalidadStr != null && !nacionalidadStr.isEmpty()) {
						String sqlNacionalidad = "select idnacionalidad from Nacionalidad n where n.nacionalidad = '"
								+ nacionalidadStr + "'";
						Number nacionalidadId = (Number) bp.createSQLQuery(sqlNacionalidad).setMaxResults(1).list()
								.get(0);
						persona.setPais12(nacionalidadId.longValue());
					}
					bp.begin();
					try {
						bp.save(persona);
						persona.setEstadoActual("activo", "insercion masiva desde planilla");
						crearContactos(persona.getId(), beanPlanillaPersonas.getContactoList());
						bp.commit();
						System.out.println("REGISTRO GRABADO: " + beanPlanillaPersonas.toString());
					} catch (Exception e) {
						bp.rollback();
						System.out.println("REGISTRO NO GRABADO: " + beanPlanillaPersonas.toString());
					}
				}
			} else {
				System.out.println("REGISTRO EXISTENTE: " + beanPlanillaPersonas.toString());
			}
		}
		return true;
	}

	/**
	 * la firma es necesaria para poder procesar con el mismo metodo la planilla de
	 * domicilios que contiene titulares, cotomadores y fiadores y cada planilla de
	 * proyecto en caso de que el titular no exista en domicilios
	 * 
	 * @param nombre    de la persona a crear si no existe
	 * @param cuit      de la persona, usado como clave de busqueda para determinar
	 *                  preexistencia y para generar datos de dni
	 * @param domicilio real a asignar a la persona
	 * @return el id de la persona que corresponde al cuit
	 */
	private Long buscarPersona(String nombre, Long cuit, BeanMigraDomicilio domicilio) {
		Persona persona = null;
		if (cuit == null || cuit == 0 || nombre == null || nombre.isEmpty()) {
			persona = new Persona(null);
		} else {
			List<Persona> personas = bp.getByFilterCast("from Persona p where p.cuil12 = " + cuit);
			if (personas == null || personas.isEmpty()) {
				persona = new Persona(null);
				persona.setCodi01(101l);
				persona.setNomb12(nombre);
				persona.setCuil12(cuit);
				// para sobreescribir los valores del constructor
				Provin provincia = (Provin) bp.getById(Provin.class, 1l);
				persona.setPais12(1l);
				persona.setProvin(provincia);
				persona.setMigracion(Boolean.TRUE);
				String cuitStr = persona.getCuil12Str(), cuilParte1 = "", cuilParte2 = "", cuilParte3 = "";
				try {
					cuilParte1 = cuitStr.substring(0, 2);
					cuilParte2 = cuitStr.substring(2, 10);
					cuilParte3 = cuitStr.substring(10, 11);
				} catch (Exception e) {
					cuilParte1 = "";
					cuilParte2 = "";
					cuilParte3 = "";
				}
				if (cuilParte1.equals("20") || (cuilParte1.equals("23") && cuilParte3.equals("9"))) {
					persona.setSexo12("M");
					persona.setCodi47(1l);
					persona.setNudo12(new Long(cuilParte2));
					persona.setTipo12("F");
				} else if (cuilParte1.equals("27") || (cuilParte1.equals("23") && cuilParte3.equals("4"))) {
					persona.setSexo12("F");
					persona.setCodi47(1l);
					persona.setNudo12(new Long(cuilParte2));
					persona.setTipo12("F");
				} else {
					persona.setSexo12("");
					persona.setTipo12("J");
					persona.setRazonSocial(nombre);
				}
				persona.setCondicionIva_id(3l);
				bp.saveOrUpdate(persona);
				// crea domicilio real de la persona
				if (domicilio != null) {
					List<Provin> provincias = bp
							.getByFilterCast("from Provin p where p.deta08 = '" + domicilio.getProvincia() + "'");
					if (provincias == null || provincias.isEmpty()) {
						provincia = (Provin) bp.getById(Provin.class, 1l);
					} else {
						provincia = provincias.get(0);
					}
					Domicilio domicilioReal = new Domicilio(persona.getId(), "Real");
					domicilioReal.setProvincia(provincia);
					domicilioReal.setProvinciaNom(domicilio.getProvincia());
					domicilioReal.setCalleNom(domicilio.getCalle());
					domicilioReal.setDepartamentoNom(domicilio.getDepartamento());
					bp.saveOrUpdate(domicilioReal);
				}
			} else {
				persona = personas.get(0);
			}
		}
		return persona.getId();
	}

	@SuppressWarnings("unchecked")
	private Long crearProyecto(BeanPlanillaProyecto beanPlanilla, Long idLinea) {
		Objetoi proyecto = new Objetoi();
		proyecto.setPersona_id(beanPlanilla.getTitularId());
		proyecto.setTipoAmortizacion(beanPlanilla.getMetodoAmortizacion());
		proyecto.setFechaFirmaContrato(beanPlanilla.getFirmaContrato());

		proyecto.setLinea_id(idLinea);
		proyecto.setMoneda(proyecto.getLinea().getMoneda());

		proyecto.setNumeroAtencion(Numerador.getNext(Objetoi.NUMERADOR_CREDITOS));
		proyecto.setExpediente(String.format("L%02d-P%05d", proyecto.getLinea_id(), proyecto.getNumeroAtencion()));

		proyecto.setPlazoCapital(0);
		proyecto.setPrimerVencCapital(null);
		proyecto.setFrecuenciaCapital(null);
		proyecto.setPlazoCompensatorio(0);
		proyecto.setPrimerVencInteres(null);
		proyecto.setFrecuenciaInteres(null);

		double financiamiento = 0d;
		List<BeanMigraCuota> cuotaList = beanPlanilla.getCuotaList();
		// activo acceso a primer cuota
		boolean primerCuotaCapital = true, primerCuotaCompensatorio = true;
		// bloqueo acceso a segunda cuota
		boolean segundaCuotaCapital = false, segundaCuotaCompensatorio = false;
		for (BeanMigraCuota beanCuota : cuotaList) {
			if (beanCuota.getCapital() != null && beanCuota.getCapital().doubleValue() > 0d) {
				if (primerCuotaCapital) {
					proyecto.setPrimerVencCapital(beanCuota.getFechaVencimiento());
					// bloqueo entrada a primer cuota
					primerCuotaCapital = false;
					// activo entrada a segunda cuota
					segundaCuotaCapital = true;
				} else if (segundaCuotaCapital) {
					proyecto.setFrecuenciaCapital(String.valueOf(DateHelper
							.getDiffDates(proyecto.getPrimerVencCapital(), beanCuota.getFechaVencimiento(), 1)));
					// bloqueo acceso a segunda cuota
					segundaCuotaCapital = false;
				}
				proyecto.setPlazoCapital(proyecto.getPlazoCapital() + 1);
				financiamiento += beanCuota.getCapital().doubleValue();
			}
			/**
			 * todas las cuotas tienen compensatorio, si la tasa es cero el valor sera cero,
			 * pero es compensatorio
			 */
			if (beanCuota.getCompensatorio() != null) {
				if (primerCuotaCompensatorio) {
					proyecto.setPrimerVencInteres(beanCuota.getFechaVencimiento());
					// bloqueo entrada a primer cuota
					primerCuotaCompensatorio = false;
					// activo entrada a segunda cuota
					segundaCuotaCompensatorio = true;
				} else if (segundaCuotaCompensatorio) {
					proyecto.setFrecuenciaInteres(String.valueOf(DateHelper
							.getDiffDates(proyecto.getPrimerVencInteres(), beanCuota.getFechaVencimiento(), 1)));
					// bloqueo acceso a segunda cuota
					segundaCuotaCompensatorio = false;
				}
				proyecto.setPlazoCompensatorio(proyecto.getPlazoCompensatorio() + 1);
			}
		}
		proyecto.setFinanciamiento(financiamiento);
		proyecto.setFinanciamientoSol(financiamiento);
		proyecto.setMontoTotal(financiamiento);
		proyecto.setAporteAgente(financiamiento);
		proyecto.setAporteCofinanciador(0d);
		bp.saveOrUpdate(proyecto);
		// recupero los vinculos creados desde la planilla de domicilios y agrego la
		// informacion del proyecto
		List<PersonaVinculada> vinculos = bp
				.getByFilterCast("from PersonaVinculada p where p.personaTitular.id = " + proyecto.getPersona_id());
		for (PersonaVinculada personaVinculada : vinculos) {
			personaVinculada.setIdObjetoi(proyecto.getId());
			personaVinculada.setNumeroAtencion(proyecto.getNumeroAtencion());
			bp.saveOrUpdate(personaVinculada);
		}

		ArrayList<BeanMigraDesembolso> listaDesembolsos = beanPlanilla.getDesembolsoList();
		BeanMigraDesembolso primerDesembolso = null;
		if (listaDesembolsos == null || listaDesembolsos.isEmpty()) {
//			System.out.println("Sin Desembolsos: " + beanPlanilla.toString());
		} else {
			primerDesembolso = listaDesembolsos.get(0);
		}

		// estado segun se indica al crear la planilla, necesario
		ObjetoiEstado estado = new ObjetoiEstado();
		estado.setObjetoi(proyecto);
		estado.setEstado(Estado.find("Objetoi", beanPlanilla.getEstadoInicial()));
		estado.setFecha(primerDesembolso == null
				? (proyecto.getFechaFirmaContrato() == null ? new Date() : proyecto.getFechaFirmaContrato())
				: primerDesembolso.getFecha());
		bp.saveOrUpdate(estado);

		// comportamiento
		ObjetoiComportamiento comportamiento = new ObjetoiComportamiento();
		comportamiento.setObjetoi(proyecto);
		comportamiento.setFecha(primerDesembolso == null
				? (proyecto.getFechaFirmaContrato() == null ? new Date() : proyecto.getFechaFirmaContrato())
				: primerDesembolso.getFecha());
		comportamiento.setObservaciones("Migraci�n");
		comportamiento.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
		bp.saveOrUpdate(comportamiento);

		// crea domicilio del proyecto
		ArrayList<BeanMigraDomicilio> domicilioList = beanPlanilla.getDomicilioList();
		BeanMigraDomicilio domicilio = null;
		if (domicilioList == null || domicilioList.isEmpty()) {
//			System.out.println("Sin domicilios");
		} else {
			domicilio = domicilioList.get(0);
		}
		// llega como parametro, si no trae nada intento buscar el real de la persona
		if (domicilio == null) {
			String consulta = "FROM Domicilio d WHERE d.persona.id = " + proyecto.getPersona_id()
					+ " AND d.tipo = 'Real'";
			ArrayList<Domicilio> listaDomicilios = (ArrayList<Domicilio>) bp.getByFilter(consulta);
			if (listaDomicilios != null && !listaDomicilios.isEmpty()) {
				Domicilio domicilioReal = listaDomicilios.get(0);
				domicilio = new BeanMigraDomicilio(domicilioReal.getCalleNom(), domicilioReal.getLocalidadNom(),
						domicilioReal.getDepartamentoNom(), domicilioReal.getProvinciaNom());
			}
		}
		// si venia algo o lo encontre en el paso anterior
		if (domicilio != null) {
			List<Provin> provincias = bp
					.getByFilterCast("FROM Provin p WHERE p.deta08 = '" + domicilio.getProvincia() + "'");
			Provin provincia;
			if (provincias == null || provincias.isEmpty()) {
				provincia = (Provin) bp.getById(Provin.class, 1l);
			} else {
				provincia = provincias.get(0);
			}
			Domicilio domicilioEspecial = new Domicilio(null, "Especial");
			domicilioEspecial.setProvincia(provincia);
			domicilioEspecial.setProvinciaNom(domicilio.getProvincia());
			domicilioEspecial.setCalleNom(domicilio.getCalle());
			domicilioEspecial.setDepartamentoNom(domicilio.getDepartamento());
			bp.saveOrUpdate(domicilioEspecial);
			DomicilioObjetoi domicilioOi = new DomicilioObjetoi();
			domicilioOi.setDomicilio(domicilioEspecial);
			domicilioOi.setObjetoi(proyecto);
			bp.saveOrUpdate(domicilioOi);
		}

		Indice tasaCompensatorio = null;
		try {
			tasaCompensatorio = buscarTasaCompensatorio(beanPlanilla);
			tasaCompensatorio.getNombre();
		} catch (Exception e) {
			tasaCompensatorio = null;
		}
		if (tasaCompensatorio != null) {
			ObjetoiIndice compensatorio = new ObjetoiIndice();
			compensatorio.setCredito(proyecto);
			compensatorio.setIndice(tasaCompensatorio);
			compensatorio.setTipoTasa(TipificadorHelper.getCodigo("TipoTasa", "Compensatorio"));
			compensatorio.setDiasAntes(0);
			compensatorio.setValorMas(0d);
			compensatorio.setValorPor(1d);
//			compensatorio.setDiasTope(0);
			compensatorio.setTipoCalculo("IC");
			bp.saveOrUpdate(compensatorio);
		}

		Linea linea = proyecto.getLinea();
		// tasa moratorio
		Indice tasaMoratorio = linea.getIndiceMor();
		if (tasaMoratorio != null) {
			ObjetoiIndice moratorio = new ObjetoiIndice();
			moratorio.setCredito(proyecto);
			moratorio.setIndice(tasaMoratorio);
			moratorio.setTipoTasa(TipificadorHelper.getCodigo("TipoTasa", "Moratorio"));
			moratorio.setDiasAntes(linea.getDiasAntesMor());
			moratorio.setValorMas(linea.getValorMasMor());
			moratorio.setValorPor(linea.getValorPorMor());
//			moratorio.setDiasTope(0);
			moratorio.setTipoCalculo("ID");
			bp.saveOrUpdate(moratorio);
		}

		Indice tasaPunitorio = linea.getIndicePun();
		if (tasaPunitorio != null) {
			ObjetoiIndice punitorio = new ObjetoiIndice();
			punitorio.setCredito(proyecto);
			punitorio.setIndice(tasaPunitorio);
			punitorio.setTipoTasa(TipificadorHelper.getCodigo("TipoTasa", "Punitorio"));
			punitorio.setDiasAntes(linea.getDiasAntesPun());
			punitorio.setValorMas(linea.getValorMasPun());
			punitorio.setValorPor(linea.getValorPorPun());
//			punitorio.setDiasTope(0);
			punitorio.setTipoCalculo("ID");
			bp.saveOrUpdate(punitorio);
		}
		return proyecto.getId();
	}

	private Indice buscarTasaCompensatorio(BeanPlanillaProyecto beanPlanilla) {
		Indice tasaCompensatorio = null;
		double valor = beanPlanilla.getTasaCompensatorio().doubleValue();
		String nombre = "Tasa " + DoubleHelper.redondear(valor * 100d, 3) + " % - Migracion";
		List<Indice> tasas = bp.getByFilterCast("FROM Indice i WHERE i.nombre = '" + nombre + "'");
		if (tasas == null || tasas.isEmpty()) {
			tasaCompensatorio = new Indice();
			tasaCompensatorio.setNombre(nombre);
			tasaCompensatorio.setAplicaEn("C");
			tasaCompensatorio.setTipo("1");
			tasaCompensatorio.setDiasPeriodo(30);
//			tasaCompensatorio.setDiasPromedio(0);
			tasaCompensatorio.setValorMaximo(0d);
			tasaCompensatorio.setValorInicial(valor);
			bp.saveOrUpdate(tasaCompensatorio);
		} else {
			tasaCompensatorio = tasas.get(0);
		}
		return tasaCompensatorio;
	}

	private void crearDesembolsos(BeanPlanillaProyecto beanPlanilla) {
		List<BeanMigraDesembolso> desembolsoList = beanPlanilla.getDesembolsoList();
		if (!(desembolsoList == null) && !desembolsoList.isEmpty()) {
			CreditoHandler creditoHandler = new CreditoHandler();
			Objetoi proyecto = (Objetoi) bp.getById(Objetoi.class, beanPlanilla.getObjetoiId());
			Desembolso desembolso;
			for (BeanMigraDesembolso beanDesembolso : desembolsoList) {
				desembolso = new Desembolso();
				desembolso.setCredito(proyecto);
				// como existen algunos numeros duplicados en las planillas genero orden por
				// fecha y lo uso
				desembolso.setNumero(beanDesembolso.getOrden());
				desembolso.setFecha(beanDesembolso.getFecha());
				desembolso.setFechaReal(beanDesembolso.getFecha());
				desembolso.setImporte(beanDesembolso.getImporte().doubleValue());
				desembolso.setImporteReal(beanDesembolso.getImporte().doubleValue());
				desembolso.setImporteSolicitado(beanDesembolso.getImporte().doubleValue());
				desembolso.setEstado(Desembolso.NUEVO);
				desembolso.setEjecutado(Boolean.FALSE);
				desembolso.setImporteGastos(0d);
				desembolso.setImporteIvaGastos(0d);
				desembolso.setImporteMultas(0d);
				bp.saveOrUpdate(desembolso);
				// si no tiene fecha o es futuro no desembolsa
				if (beanDesembolso.getFecha() != null && beanDesembolso.getFecha().before(new Date())) {
					creditoHandler.ejecutarDesembolso(proyecto, desembolso);
					desembolso.setEstado(Desembolso.REALIZADO);
					bp.saveOrUpdate(desembolso);
				}
			}
		}
	}

	private void aplicarPagos(BeanPlanillaProyecto beanPlanilla, Boolean facturarCompensatorio) {
		List<BeanMigraPago> pagoList = beanPlanilla.getPagoList();
		if (pagoList != null && !pagoList.isEmpty()) {
			Objetoi proyecto = (Objetoi) bp.getById(Objetoi.class, beanPlanilla.getObjetoiId());
			Linea linea = proyecto.getLinea();
			BigDecimal importe = BigDecimal.ZERO;
			for (BeanMigraPago beanPago : pagoList) {
				Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setLong("idCredito", proyecto.getId())
						.setInteger("nroCuota", beanPago.getNumero()).uniqueResult();
				importe = beanPago.getCapital();
				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
					this.generarCtacte(proyecto, cuota, linea.getCapitalId(), linea.getCapitalId(),
							beanPago.getFechaPago(), beanPago.getCapital(), Tipomov.TIPOMOV_CREDITO);
				}
				importe = beanPago.getCompensatorio();
				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
					if (facturarCompensatorio) {
						this.generarCtacte(proyecto, cuota, linea.getCompensatorioId(), linea.getCompensatorioId(),
								beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_DEBITO);
					}
					this.generarCtacte(proyecto, cuota, linea.getCompensatorioId(), linea.getCompensatorioId(),
							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
				}
				importe = beanPago.getIvaCompensatorio();
//				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
//					if (facturarCompensatorio) {
//						this.generarCtacte(proyecto, cuota, linea.getIvaId(), linea.getCompensatorioId(),
//								beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_DEBITO);
//					}
//					this.generarCtacte(proyecto, cuota, linea.getIvaId(), linea.getCompensatorioId(),
//							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
//				}
				importe = beanPago.getMoratorio();
				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
					this.generarCtacte(proyecto, cuota, linea.getMoratorioId(), linea.getMoratorioId(),
							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_DEBITO);
					this.generarCtacte(proyecto, cuota, linea.getMoratorioId(), linea.getMoratorioId(),
							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
				}
				importe = beanPago.getIvaMoratorio();
//				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
//					this.generarCtacte(proyecto, cuota, linea.getIvaId(), linea.getMoratorioId(),
//							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_DEBITO);
//					this.generarCtacte(proyecto, cuota, linea.getIvaId(), linea.getMoratorioId(),
//							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
//				}
				importe = beanPago.getPunitorio();
				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
					this.generarCtacte(proyecto, cuota, linea.getPunitorioId(), linea.getPunitorioId(),
							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_DEBITO);
					this.generarCtacte(proyecto, cuota, linea.getPunitorioId(), linea.getPunitorioId(),
							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
				}
				importe = beanPago.getIvaPunitorio();
//				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
//					this.generarCtacte(proyecto, cuota, linea.getIvaId(), linea.getPunitorioId(),
//							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_DEBITO);
//					this.generarCtacte(proyecto, cuota, linea.getIvaId(), linea.getPunitorioId(),
//							beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
//				}
				importe = beanPago.getGastoRecuperar();
				if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
					cuota.setGastos(importe.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
//					cuota.setIvaGastos(0d);
					this.generarCtacte(proyecto, cuota, linea.getGastoId(), linea.getGastoId(),
							cuota.getFechaGeneracion(), importe, Tipomov.TIPOMOV_DEBITO);
					if (!(beanPago.getFechaPago() == null)) {
						this.generarCtacte(proyecto, cuota, linea.getGastoId(), linea.getGastoId(),
								beanPago.getFechaPago(), importe, Tipomov.TIPOMOV_CREDITO);
					}
				}
			}
		}
	}

	private void facturarIntereses(BeanPlanillaProyecto beanPlanilla) {
		List<BeanMigraCuota> cuotaList = beanPlanilla.getCuotaList();
		Objetoi proyecto = (Objetoi) bp.getById(Objetoi.class, beanPlanilla.getObjetoiId());
		Linea linea = proyecto.getLinea();
		BigDecimal importe = BigDecimal.ZERO;
		for (BeanMigraCuota beanCuota : cuotaList) {
			importe = beanCuota.getCompensatorio();
			if (importe != null && importe.compareTo(BigDecimal.ZERO) > 0) {
				Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setLong("idCredito", proyecto.getId())
						.setInteger("nroCuota", beanCuota.getNumero()).uniqueResult();
				generarCtacte(proyecto, cuota, linea.getCompensatorioId(), linea.getCompensatorioId(),
						beanCuota.getFechaVencimiento(), importe, Tipomov.TIPOMOV_DEBITO);
			}
		}
	}

	private void generarCtacte(Objetoi proyecto, Cuota cuota, String facturado, String asociado, Date fechaGeneracion,
			BigDecimal importe, String tipoBoleto) {
		Calendar fechaMovimiento = Calendar.getInstance();
		Integer periodoCtacte = fechaMovimiento.get(Calendar.YEAR);
		String numerador = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", periodoCtacte.toString());
		Concepto facturadoConc = Concepto.buscarConcepto(facturado, periodoCtacte);
		Concepto asociadoConc = Concepto.buscarConcepto(asociado, periodoCtacte);
		CtacteKey cck = new CtacteKey();
		cck.setObjetoi(proyecto);
		cck.setPeriodoCtacte(periodoCtacte.longValue());
		cck.setMovimientoCtacte(Numerador.getNext(numerador));
		cck.setVerificadorCtacte(0l);
		cck.setItemCtacte(1l);
		Ctacte cc = new Ctacte();
		cc.setId(cck);
		cc.setCuota(cuota);
		cc.setFacturado(facturadoConc);
		cc.setAsociado(asociadoConc);
		cc.setFechaGeneracion(fechaGeneracion);
		cc.setFechaVencimiento(fechaGeneracion);
		cc.setImporte(importe.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
		cc.setTipoConcepto("106");
		cc.setUsuario(usuario);
		cc.setCotizaMov(1d);
		if (tipoBoleto.equals(Tipomov.TIPOMOV_DEBITO)) {
			cc.setDetalle(asociadoConc.getConcepto().getDetalle());
			cc.setTipomov(tipoMovDebito);
			cc.setTipoMovimiento("Cuota");
		} else {
			cc.setDetalle("Cobranza");
			cc.setTipomov(tipoMovCredito);
			cc.setTipoMovimiento("Pago");
		}
		bp.saveOrUpdate(cc);

	}

	private void crearNotificaciones(BeanPlanillaProyecto beanPlanilla) {
		List<BeanMigraNotificacion> notificacionList = beanPlanilla.getNotificacionList();
		if (notificacionList != null && !notificacionList.isEmpty()) {
			Objetoi proyecto = (Objetoi) bp.getById(Objetoi.class, beanPlanilla.getObjetoiId());
			Notificacion notificacion;
			for (BeanMigraNotificacion beanNotificacion : notificacionList) {
				notificacion = new Notificacion();
				notificacion.setFechaCreada(beanNotificacion.getFechaNotificacion());
				notificacion.setObservaciones(beanNotificacion.getObservaciones());
				notificacion.setCredito(proyecto);
				notificacion.setTipoAviso("gestionMora");
				bp.saveOrUpdate(notificacion);
			}
		}
	}

	private void crearContactos(BeanPlanillaProyecto beanPlanilla) {
		crearContactos(beanPlanilla.getTitularId(), beanPlanilla.getContactoList());
	}

	private void crearContactos(Long personaId, ArrayList<BeanMigraContacto> contactoList) {
//		ArrayList<BeanMigraContacto> contactoList = beanPlanilla.getContactoList();
		for (BeanMigraContacto beanContacto : contactoList) {
			Contacto contacto = new Contacto();
			contacto.setPersona_id(personaId);
			contacto.setMedio(beanContacto.getMedio());
			contacto.setTipo(beanContacto.getTipo());
			contacto.setDetalle(beanContacto.getDetalle());
			contacto.setObservaciones("Migracion");
			bp.saveOrUpdate(contacto);
		}
	}

	@SuppressWarnings("unchecked")
	private void crearGarantias(BeanPlanillaProyecto beanPlanilla) {
		// garantias
		ArrayList<BeanMigraGarantia> garantiaList = beanPlanilla.getGarantiaList();
		Objetoi proyecto = (Objetoi) bp.getById(Objetoi.class, beanPlanilla.getObjetoiId());
		proyecto.getNumeroAtencion();

		TipoGarantia tipoGarantia = (TipoGarantia) bp.getById(TipoGarantia.class, 7l);
		List<CampoGarantia> campos = bp.getNamedQuery("CampoGarantia.findByTipoGarantia")
				.setEntity("tipoGarantia", tipoGarantia).list();

		ValorCampoGarantia valor = new ValorCampoGarantia();
		for (BeanMigraGarantia beanGarantia : garantiaList) {

			Garantia garantia = new Garantia();
			garantia.setTipo(tipoGarantia);
			garantia.setObservacion("Migracion");
			garantia.setIdentificadorUnico(beanGarantia.getDominio());
			garantia.setSeguro(null);
			garantia.setTasacionInmueble(null);
			garantia.setTasacionMueble(null);
			bp.saveOrUpdate(garantia);

			ObjetoiGarantia objetoiGarantia = new ObjetoiGarantia();
			objetoiGarantia.setObjetoi(proyecto);
			objetoiGarantia.setGarantia(garantia);
			objetoiGarantia.setObservacion("Migracion");
			objetoiGarantia.setEstado(ObjetoiGarantia.ESTADO_ACTIVA);
			bp.saveOrUpdate(objetoiGarantia);

			GarantiaUso garantiaUso = new GarantiaUso(garantia, 100d, "", proyecto.getPersona());
			bp.saveOrUpdate(garantiaUso);

			ArrayList<ValorCampoGarantia> valores = new ArrayList<ValorCampoGarantia>();

			for (CampoGarantia campo : campos) {
				valor = new ValorCampoGarantia();
				valor.setCampo(campo);
				if ("Modelo".equalsIgnoreCase(campo.getNombre())) {
					valor.setValorCadena(beanGarantia.getVersion());
				} else if ("A�o".equalsIgnoreCase(campo.getNombre())) {
					valor.setValorCadena(beanGarantia.getModeloStr());
				} else if ("Marca".equalsIgnoreCase(campo.getNombre())) {
					valor.setValorCadena(beanGarantia.getMarca());
				} else if ("Dominio".equalsIgnoreCase(campo.getNombre())) {
					valor.setValorCadena(beanGarantia.getDominio());
				} else if ("Precio seg�n info:".equalsIgnoreCase(campo.getNombre())) {
					valor.setValorDouble(Double.parseDouble(beanGarantia.getPreSegInfoStr()));
				} else if ("Porcentaje".equalsIgnoreCase(campo.getNombre())) {
					valor.setValorDouble(Double.parseDouble(beanGarantia.getPorcentajeStr()));
				}
				bp.save(valor);
				valores.add(valor);
			}
			garantia.setValores(valores);
			bp.saveOrUpdate(garantia);
		}

	}

	/**
	 * @param beanPlanillaDomicilio
	 */
	private void crearPersonas(BeanPlanillaDomicilio beanPlanillaDomicilio) {
		String cuit = "";
		Long titularId = buscarPersona(beanPlanillaDomicilio.getDeudor(), beanPlanillaDomicilio.getDeudorCuit(),
				beanPlanillaDomicilio.getDomicilio());
		if (titularId != null) {
			if (!(beanPlanillaDomicilio.getCotomador() == null) && !beanPlanillaDomicilio.getCotomador().isEmpty()) {
				cuit = NumberHelper.calcularCuit(beanPlanillaDomicilio.getCotomadorSexo(),
						beanPlanillaDomicilio.getCotomadorDocumento().toString());
				Long cotomadorId = buscarPersona(beanPlanillaDomicilio.getCotomador(), Long.valueOf(cuit), null);
				if (cotomadorId != null) {
					PersonaVinculada cotomador = new PersonaVinculada();
					cotomador.setPersonaTitular_id(titularId);
					cotomador.setPersonaVinculada_id(cotomadorId);
					cotomador.setRelacion_id(9l);
					bp.saveOrUpdate(cotomador);
				}
			}
			if (!(beanPlanillaDomicilio.getFiador() == null) && !beanPlanillaDomicilio.getFiador().isEmpty()) {
				cuit = NumberHelper.calcularCuit(beanPlanillaDomicilio.getFiadorSexo(),
						beanPlanillaDomicilio.getFiadorDocumento().toString());
				Long fiadorId = buscarPersona(beanPlanillaDomicilio.getFiador(), Long.valueOf(cuit), null);
				if (fiadorId != null) {
					PersonaVinculada fiador = new PersonaVinculada();
					fiador.setPersonaTitular_id(titularId);
					fiador.setPersonaVinculada_id(fiadorId);
					fiador.setRelacion_id(21l);
					bp.saveOrUpdate(fiador);

				}
			}
		}

	}

	@Override
	protected String getDefaultForward() {
		return "MigracionPlanillas";
	}

	public FormFile getArchivo() {
		return archivo;
	}

	public void setArchivo(FormFile archivo) {
		this.archivo = archivo;
	}

	public String getFormatoCarga() {
		return formatoCarga;
	}

	public void setFormatoCarga(String formatoCarga) {
		this.formatoCarga = formatoCarga;
	}

	public Long getFideicomisoId() {
		return fideicomisoId;
	}

	public void setFideicomisoId(Long fideicomisoId) {
		this.fideicomisoId = fideicomisoId;
	}

	public Long getLineaId() {
		return lineaId;
	}

	public void setLineaId(Long lineaId) {
		this.lineaId = lineaId;
	}

}