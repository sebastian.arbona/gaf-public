package com.asf.cred.business;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.cred.struts.form.RequisitosForm;
import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaEstado;
import com.nirven.creditos.hibernate.CargaRequisito;
import com.nirven.creditos.hibernate.CargaRequisitoBon;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Requisito;
import com.nirven.creditos.hibernate.Vinedo;

public class BonTasaRequisitos implements IProcess {
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	private String forward;
	private String accion;
	private Object result;
	private List<CargaRequisitoBon> requisitos;
	private Requisito requisito;
	private Long idBonTasa;
	private BonTasa bonificacion;
	private boolean chec = true;
	private Long idPersona;
	private boolean esPersona;
	private Long idRequisito;
	private CargaRequisitoBon carga;

	// estos atributos son para modificar el requisito
	private Date fechaCump;
	private Long id;
	private File archivo;
	private String observaciones;
	private String nombre;

	public BonTasaRequisitos() {
		carga = (CargaRequisitoBon) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("CargaRequisitoBon.carga");
	}

	@Override
	public boolean doProcess() {
		if (accion.equalsIgnoreCase("listar")) {
			buscarBonTasa();
			listarRequisitos();
			forward = "RequisitosList";
		} else if (accion.equalsIgnoreCase("guardar")) {
			forward = "RequisitosList";
			buscarBonTasa();
			listarRequisitos();
			guardarRequisitos();

		} else if (accion.equalsIgnoreCase("eliminar")) {
			eliminarRequisito();
			buscarBonTasa();
			listarRequisitos();
			forward = "RequisitosList";
		} else if (accion.equalsIgnoreCase("cargarRequisito")) {
			cargarRequisito();
			forward = "RequisitoBonTasa";
		} else if (accion.equalsIgnoreCase("guardarReq")) {
			carga = (CargaRequisitoBon) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("CargaRequisitoBon.carga");
			carga.setObservaciones(observaciones);
			carga.setFechaCumplimiento(fechaCump);
			carga.setArchivo(archivo);
			bp.update(carga);
			buscarBonTasa();
			listarRequisitos();
			forward = "RequisitosList";
		}
		return true;
	}

	private void cargarRequisito() {
		carga = (CargaRequisitoBon) bp.getById(CargaRequisitoBon.class, idRequisito);
		id = carga.getRequisito().getId();
		observaciones = carga.getObservaciones();
		fechaCump = carga.getFechaCumplimiento();
		archivo = carga.getArchivo();
		nombre = carga.getRequisito().getNombre();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("CargaRequisitoBon.carga",
				carga);
		idBonTasa = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("idBonTasa");
	}

	private void eliminarRequisito() {
		carga = (CargaRequisitoBon) bp.getById(CargaRequisitoBon.class, idRequisito);
		bp.delete(carga);
	}

	private boolean comprobarRequisitos() {
		List<CargaRequisitoBon> requisitos = listarRequisitos();
		int cumplidos = 0;
		for (CargaRequisitoBon requi : requisitos) {// Compruebo si todos los requisitos estan cumplidos
			if (requi.getFechaCumplimiento() != null) {
				cumplidos += 1;
			}
		}
		if (requisitos.size() == cumplidos) {
			return true;
		} else {
			return false;
		}
	}

	private void comprobarEstado() {
		if (comprobarRequisitos()) {
			BonTasaEstado estado = new BonTasaEstado();
			List<Estado> estados = bp
					.createQuery("select e from Estado e where e.nombreEstado = :estado and e.tipo = 'BonTasa'")
					.setParameter("estado", bonificacion.getTipoEstadoBonTasa()).setMaxResults(1).list();
			if (!estados.isEmpty()) {
				estado.setEstado(estados.get(0));
			}
			Date fecha = new Date();
			estado.setFechaCambio(fecha);
			estado.setAsesor(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			estado.setBonTasa(bonificacion);
			bp.save(estado);
			if (esPersona) {
				forward = "RedirectPersona";
			} else {
				forward = "Redirect";
			}
			idPersona = bonificacion.getPersona().getId();
		} else {
			forward = "RequisitosList";
		}
	}

	private void guardarRequisitos() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		HashSet<String> seleccionados = new HashSet<String>();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("requisito")) {
				String[] p = param.split("-");
				seleccionados.add(new String(p[1]));
			}
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		java.util.Date fecha = new java.util.Date();
		for (CargaRequisitoBon requisitoSel : requisitos) {
			for (String selec : seleccionados) {
				if (selec.equalsIgnoreCase(requisitoSel.getRequisito().getId().toString())) {
					requisitoSel.setFechaCumplimiento(fecha);
					bp.saveOrUpdate(requisitoSel);
					comprobarEstado();// Cambia el estado a "Analisis" si corresponde
				}
			}
		}
	}

	private List<CargaRequisitoBon> listarRequisitos() {
		// Busco los requisitos de la linea "Bonificaciones de Tasa"
		// Director director = (Director) bp.createQuery("Select d from Director d where
		// d.codigo=:cod").setParameter("cod", "BonTasa.IDLinea").uniqueResult();
		// Linea l= (Linea) bp.getById(Linea.class,
		// Long.parseLong(director.getValor()));
		@SuppressWarnings("unchecked")
		List<Requisito> req = (List<Requisito>) bp.createQuery("Select r from Requisito r where r.linea= :linea")
				.setLong("linea", bonificacion.getConvenio().getLinea().getId()).list();

		// Busco los requisitos que ya estan cumplidos en CargaRequisitoBon
		requisitos = bp.createQuery("Select c from CargaRequisitoBon c where c.bonTasa =:bonTasa")
				.setParameter("bonTasa", bonificacion).list();

		// comparo los requisitos para armar la lista final
		for (Requisito requisito : req) {
			int i = 0;
			for (CargaRequisitoBon cargaReq : requisitos) {
				if (requisito.getId().compareTo(cargaReq.getRequisito().getId()) == 0) {
					i = 1;
				}
			}
			if (i == 0) {
				CargaRequisitoBon beanCarga = new CargaRequisitoBon();
				beanCarga.setBonTasa(bonificacion);
				beanCarga.setRequisito(requisito);
				if (beanCarga.getId() != null) {
					beanCarga.setId(null);
				}
				requisitos.add(beanCarga);
			}
		}
		return requisitos;
	}

	private void buscarBonTasa() {
		bonificacion = (BonTasa) bp.createQuery("select b from BonTasa b where b.id = :id").setLong("id", idBonTasa)
				.uniqueResult();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idBonTasa", idBonTasa);
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {

		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public boolean isChec() {
		return chec;
	}

	public void setChec(boolean chec) {
		this.chec = chec;
	}

	public Requisito getRequisito() {
		return requisito;
	}

	public void setRequisito(Requisito requisito) {
		this.requisito = requisito;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public List<CargaRequisitoBon> getRequisitos() {
		return requisitos;
	}

	public void setRequisitos(List<CargaRequisitoBon> requisitos) {
		this.requisitos = requisitos;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	public String getFechaCumplimientoStr() {
		return "";
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public boolean isEsPersona() {
		return esPersona;
	}

	public void setEsPersona(boolean esPersona) {
		this.esPersona = esPersona;
	}

	public Long getIdRequisito() {
		return idRequisito;
	}

	public void setIdRequisito(Long idRequisito) {
		this.idRequisito = idRequisito;
	}

	public CargaRequisitoBon getCarga() {
		return carga;
	}

	public void setCarga(CargaRequisitoBon carga) {
		this.carga = carga;
	}

	public String getFechaCumpStr() {
		return DateHelper.getString(fechaCump);
	}

	public void setFechaCumpStr(String fecha) {
		fechaCump = DateHelper.getDate(fecha);
	}

	public Date getFechaCump() {
		return fechaCump;
	}

	public void setFechaCump(Date fechaCumplimiento) {
		this.fechaCump = fechaCumplimiento;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public File getArchivo() {
		return archivo;
	}

	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
