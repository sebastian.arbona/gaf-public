package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Query;

import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.ldap.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.cred.beans.BeanCtaCte;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.BoletoResumenDetalle;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.Emideta;
import com.nirven.creditos.hibernate.Emision;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class Generacion implements IProcess {
	// =========================ATRIBUTOS===========================
	private static Log log = LogFactory.getLog(Generacion.class);
	private BusinessPersistance bp = null;
	private String forward = "GeneracionResult", input = "Generacion";
	private HashMap<Object, Object> errores;
	private String accion = "";
	private List<Emideta> impactadas;

	private Boolean generacionOK;
	private Long idEmision;
	private String filtroFechaVencimiento;
	private boolean generada = false;
	private boolean facturada = false;
	private boolean deshabilito = false;
	private Usuario usuario;

	// =========================CONSTRUCTOR=========================
	public Generacion() {
		this.errores = new HashMap<Object, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (getIdEmision() == null) {
			idEmision = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("generacion.idEmision");
		}
		if (idEmision != null) {
			deshabilito = true;
		}

		impactadas = (List<Emideta>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("generacionImpactadas");
		usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
	}

	// ======================FUNCIONALIDADES========================
	@SuppressWarnings({ "static-access", "rawtypes" })
	@Override
	public boolean doProcess() {
		if (accion.equalsIgnoreCase("cancelarGeneracion")) {
			deshabilito = false;
			forward = "GeneracionResult";
			return false;
		} else if (accion.equalsIgnoreCase("exportar")) {

			return true;
		} else {
			ProgressStatus.onProcess = true;
			ProgressStatus.iTotal = 1;
			ProgressStatus.iProgress = 0;
			ProgressStatus.abort = false;
			if (this.idEmision == 0) {

			}
			Emision emi = (Emision) bp.getById(Emision.class, this.idEmision);
			if (emi.getFacturada() == true) {
				facturada = true;
			}
			if (emi.getFiltroFechaVencimiento() != null) {
				setFiltroFechaVencimiento(emi.getFiltroFechaVencimiento());
			} else {
				setFiltroFechaVencimiento("");
			}

			if (this.errores.isEmpty()) {
				log.info("Proceso de Generaci�n: Iniciando la generacion");
				ProgressStatus.sStatus = "Iniciando la generacion";
				ProgressStatus.abort = false;
				int interval = SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getMaxInactiveInterval();
				try {
					this.bp.getCurrentSession().setFlushMode(FlushMode.ALWAYS);
					this.bp.getCurrentSession().setCacheMode(CacheMode.IGNORE);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession().setMaxInactiveInterval(-1);
					long l = System.currentTimeMillis();

					log.info("Proceso de Generacion: Buscar Emision");

					try {
						Number count = (Number) bp.getNamedQuery("Emideta.findByEmisionCount")
								.setParameter("idEmision", idEmision).uniqueResult();
						if (count != null && count.intValue() > 0) {
							ProgressStatus.iTotal = count.intValue();
						} else {
							ProgressStatus.iTotal = 1;
							ProgressStatus.iProgress = 1;
						}
					} catch (Exception ex) {
						// no se pudo sacar el total
					}

					Query qEmideta = bp.getNamedQuery("Emideta.findByEmision");
					qEmideta.setParameter("idEmision", idEmision);
					log.info("Proceso de Generacion: Iterando Liquidacion");
					int cantidad = 0;
					int impactadas = 0;
					this.impactadas = new ArrayList<Emideta>();

					Query queryBoletoResumen = bp.createQuery(
							"select b from Boleto b where b.tipo = :tipoResumen and b.objetoi.id = :idCredito order by b.id desc")
							.setMaxResults(1).setParameter("tipoResumen", Boleto.TIPO_BOLETO_RESUMEN);

					Query queryCuota = bp.getNamedQuery("Cuota.findByNumero");

					CreditoHandler handler = new CreditoHandler();

					for (Iterator iterator = qEmideta.iterate(); iterator.hasNext();) {
						this.bp.begin();
						cantidad++;
						Emideta emideta = (Emideta) iterator.next();
						if (!emideta.getActualizada()) {
							Objetoi credito = emideta.getCredito();
							ProgressStatus.sStatus = "Credito actual: " + credito.getNumeroAtencionStr();
							if (!DirectorHandler.getDirector(Linea.LINEA_EMERGENCIA).getValor()
									.equalsIgnoreCase(credito.getLinea().getId().toString())) {
								if (credito.facturar(this.bp, this.log, emideta)) {
									Boleto boletoResumen = (Boleto) queryBoletoResumen
											.setParameter("idCredito", credito.getId()).uniqueResult();
									Cuota cuota = (Cuota) queryCuota.setParameter("idCredito", credito.getId())
											.setParameter("nroCuota", emideta.getNumero()).uniqueResult();
									handler.crearParteBBoletoResumen(cuota, boletoResumen);
									emideta.setActualizada(true);
									impactadas++;
									this.impactadas.add(emideta);
								} else {
									emideta.setActualizada(false);
								}
								this.bp.update(emideta);
							}
						}
						this.bp.commit();
						ProgressStatus.iProgress = ProgressStatus.iProgress + 1;
						bp.getCurrentSession().flush();
						bp.getCurrentSession().clear();
					}

					this.bp.begin();
					Emision emision = (Emision) bp.getById(Emision.class, this.idEmision);
					emision.setFacturada(true);
					if (this.impactadas != null && !this.impactadas.isEmpty()) {
						SessionHandler.getCurrentSessionHandler().getRequest().getSession()
								.setAttribute("generacionImpactadas", this.impactadas);
					}
					this.bp.update(emision);
					this.bp.commit();
					this.errores.put(new Long(this.errores.size()),
							"Tiempo aproximado: " + (System.currentTimeMillis() - l) / 1000);
					this.errores.put(new Long(this.errores.size()),
							"El impacto en cuenta corriente ha sido realizado con �xito");
					this.errores.put(new Long(this.errores.size()),
							"Cuotas procesadas: " + cantidad + ". Cuotas impactadas: " + impactadas);
					log.info("Proceso de Liquidacion: Tiempo: " + (System.currentTimeMillis() - l) / 1000);
					generada = true;
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("generacion.idEmision", idEmision);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("impresionMasivaBoleto.emidetas", null);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("impresionMasivaBoleto.emision", null);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("impactadas",
							impactadas);

				} catch (Exception e) {
					log.warn(e.getMessage(), e);
					this.errores.put(new Long(this.errores.size()), e.getMessage());
					this.errores.put(new Long(this.errores.size()), "NO se realiz� el impacto en cuenta corriente");
					this.bp.rollback();
				}

				finally {
					this.bp.getCurrentSession().setFlushMode(FlushMode.AUTO);
					this.bp.getCurrentSession().setCacheMode(CacheMode.NORMAL);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setMaxInactiveInterval(interval);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("generacion.idEmision", null);
				}
			} else {
				this.errores.put(new Long(this.errores.size()),
						"Para realizar la liquidaci�n, debe poner 'si' en CONFIRMA");
			}
			ProgressStatus.onProcess = false;
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("generacion.idEmision",
					null);
			return true;
		}
	}

	public HashMap<Object, Object> getErrors() {
		return errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.input;
	}

	public Object getResult() {
		return null;
	}

	// =====================UTILIDADES PRIVADAS=====================

	// ======================GETTERS Y SETTERS======================
	public Long getIdEmision() {
		return this.idEmision;
	}

	public void setIdEmision(Long idEmision) {
		System.out.println(idEmision);
		this.idEmision = idEmision;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Boolean getGeneracionOK() {
		return generacionOK;
	}

	public void setGeneracionOK(Boolean generacionOK) {
		this.generacionOK = generacionOK;
	}

	public boolean isGenerada() {
		return generada;
	}

	public void setGenerada(boolean generada) {
		this.generada = generada;
	}

	// ========================VALIDACIONES=========================
	public boolean validate() {
		return this.errores.isEmpty();
	}

	public String getFiltroFechaVencimiento() {
		return filtroFechaVencimiento;
	}

	public void setFiltroFechaVencimiento(String filtroFechaVencimiento) {
		this.filtroFechaVencimiento = filtroFechaVencimiento;
	}

	public boolean isFacturada() {
		return facturada;
	}

	public void setFacturada(boolean facturada) {
		this.facturada = facturada;
	}

	public boolean isDeshabilito() {
		return deshabilito;
	}

	public void setDeshabilito(boolean deshabilito) {
		this.deshabilito = deshabilito;
	}

	public void setImpactadas(List<Emideta> impactadas) {
		this.impactadas = impactadas;
	}

	public List<Emideta> getImpactadas() {
		return impactadas;
	}

}
