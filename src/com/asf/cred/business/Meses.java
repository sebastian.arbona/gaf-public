package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Meses {
	
	ENERO,
	FEBRERO,
	MARZO,
	ABRIL,
	MAYO,
	JUNIO,
	JULIO,
	AGOSTO,
	SETIEMBRE,
	OCTUBRE,
	NOVIEMBRE,
	DICIEMBRE;
	
	public static Meses getMes(int mes) {
		return Meses.values()[mes];
	}
	
	public static List<Meses> getMeses(int desde, int hasta) {
		List<Meses> meses = new ArrayList<Meses>();
		for (int i = desde; i <= hasta; i++) {
			meses.add(Meses.values()[i]);
		}
		return meses;
	}
	
	public static List<Meses> getMeses() {
		return Arrays.asList(Meses.values());
	}
}
