package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.asf.gaf.hibernate.Bancos;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Bonificacion;
import com.nirven.creditos.hibernate.ConvenioBonificacion;

public class ConvenioBonificacionProcess implements IProcess {

	private String forward;
	private String accion;
	private ConvenioBonificacion convenio = new ConvenioBonificacion();
	private List<ConvenioBonificacion> convenios = new ArrayList<ConvenioBonificacion>();
	private Long idConvenio;
	private Long idBanco;
	private boolean error;

	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (accion == null || accion.equalsIgnoreCase("listar")) {
			convenios = (List<ConvenioBonificacion>) bp.createQuery("Select c from ConvenioBonificacion c").list();
			forward = "convenioBonificacionList";
		} else if (accion.equalsIgnoreCase("modificar")) {
			convenio = (ConvenioBonificacion) bp.getById(ConvenioBonificacion.class, idConvenio);
			if (convenio.getBanco() != null) {
				idBanco = convenio.getBanco().getId();
			}
			forward = "convenioBonificacion";
		} else if (accion.equalsIgnoreCase("nuevo")) {
			forward = "convenioBonificacion";
		} else if (accion.equalsIgnoreCase("eliminar")) {
			convenio = (ConvenioBonificacion) bp.getById(ConvenioBonificacion.class, idConvenio);
			List<Bonificacion> bonificaciones = (List<Bonificacion>) bp
					.createQuery("Select b from Bonificacion b where b.convenio=:conv").setParameter("conv", convenio)
					.list();
			if (bonificaciones.size() == 0) {
				bp.delete(convenio);
			} else {
				error = true;
			}
			accion = "listar";
			doProcess();
		} else if (accion.equalsIgnoreCase("guardar")) {
			convenio.setBanco((Bancos) bp.getById(Bancos.class, idBanco));
			bp.saveOrUpdate(convenio);
			accion = "listar";
			doProcess();
		}

		return true;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public ConvenioBonificacion getConvenio() {
		return convenio;
	}

	public void setConvenio(ConvenioBonificacion convenio) {
		this.convenio = convenio;
	}

	public List<ConvenioBonificacion> getConvenios() {
		return convenios;
	}

	public void setConvenios(List<ConvenioBonificacion> convenios) {
		this.convenios = convenios;
	}

	public Long getIdConvenio() {
		return idConvenio;
	}

	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public Long getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Long idBanco) {
		this.idBanco = idBanco;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

}
