package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.struts.action.ActionMessage;
import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ProrrogaMasiva;
import com.nirven.creditos.hibernate.SolicitudProrroga;
import com.nirven.creditos.hibernate.SolicitudProrrogaEstado;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class ProrrogaMasivaProcess implements IProcess {

	private BusinessPersistance bp;

	private String action;
	private String forward;

	private HashMap<String, Object> errores = new HashMap<String, Object>();

	private Date fechaDesde;
	private Date fechaHasta;

	private String tipoProceso;
	private ProrrogaMasiva prorroga;

	private String tipoCorrimiento;
	private String tipoMovimiento;
	private String dias;
	private String meses;
	private Date nuevoVencimiento;
	private ArrayList<Date> feriados;

	private List<Cuota> listaCuotas;

	private Boolean[] seleccion;

	private boolean finalizo = false;

	private String mensajeFin;

	private String linea_id;
	private List<Cuota> cuotasProrrogadas;
	private List<Cuota> cuotasFechaAnterior;
	private List<Cuota> cuotasFechaSuperpuesta;
	private List<Cuota> cuotasCaducidad;
	private List<Date> fechasCuotas;
	private Long nroAtencion;

	private String forwardURL;

	private Objetoi credito;

	private String resolucion;
	private String observaciones;
	private Usuario usuario;
	private Calendar fechaActual;

	@SuppressWarnings("unchecked")
	public ProrrogaMasivaProcess() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		listaCuotas = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("listaCuotas");
		if (listaCuotas != null) {
			seleccion = new Boolean[listaCuotas.size()];
		}
		forward = "ProrrogaMasivaProcess";
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		cuotasProrrogadas = new ArrayList<Cuota>();
		cuotasFechaAnterior = new ArrayList<Cuota>();
		cuotasFechaSuperpuesta = new ArrayList<Cuota>();
		cuotasCaducidad = new ArrayList<Cuota>();
		fechasCuotas = new ArrayList<Date>();
		if (action != null && action.equals("filtrar")) {
			filtrar();
		} else if (action != null && action.equals("modificarFechas")) {
			/**
			 * aca se realiza la modificacion, invocada por el boton "modificar fechas"
			 */
			if (listaCuotas == null || listaCuotas.isEmpty()) {
				errores.put("prorroga.faltaBuscar", "prorroga.faltaBuscar");
			} else {
				usuario = (Usuario) bp.getById(Usuario.class,
						SessionHandler.getCurrentSessionHandler().getCurrentUser());
				fechaActual = Calendar.getInstance();
				fechaActual.clear(Calendar.HOUR_OF_DAY);
				fechaActual.clear(Calendar.MINUTE);
				fechaActual.clear(Calendar.SECOND);
				fechaActual.clear(Calendar.MILLISECOND);
				feriados = (ArrayList<Date>) bp.getNamedQuery("Feriado.findByAnio")
						.setInteger("anio", fechaActual.get(Calendar.YEAR)).list();
				prorroga = crearProrroga();
				if ("Covid19".equalsIgnoreCase(this.tipoProceso)) {
					modificarFechasCovid19();
				} else {
					modificarFechasNormal();
				}
				if (errores.isEmpty()) {
					finalizo = true;
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("cuotasProrrogadas", cuotasProrrogadas);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("cuotasFechaAnterior", cuotasFechaAnterior);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("cuotasFechaSuperpuesta", cuotasFechaSuperpuesta);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("cuotasCaducidad",
							cuotasCaducidad);
					SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("prorroga",
							prorroga);
					forward = "ProcessRedirect";
					forwardURL = "/actions/process.do?do=process&processName=ProrrogaMasivaResult";
				}
			}
		} else if (action != null && action.equals("volver")) {
			forward = "ProrrogaMasivaProcess";
			listaCuotas = new ArrayList<Cuota>();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("listaCuotas",
					new ArrayList<Cuota>());
		} else if (action == null) {
			listaCuotas = null;
			seleccion = null;
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("listaCuotas", null);
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void filtrar() {
		String queryStr = "SELECT c FROM Cuota c WHERE c.fechaVencimiento BETWEEN :fechaDesde AND :fechaHasta ";
		if ("Covid19".equalsIgnoreCase(this.tipoProceso)) {
			queryStr += "AND c.emision IS NOT NULL AND c.fechaVencimientoOriginal IS NULL ";
		} else {
			queryStr += "AND c.emision IS NULL ";
		}
		Linea linea = null;
		if (nroAtencion != null && nroAtencion != 0L) {
			credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion").setParameter("nroAtencion", nroAtencion)
					.uniqueResult();
			queryStr += "AND c.credito.id = :idCredito ";
		} else if (!linea_id.equals("")) {
			linea = (Linea) bp.getById(Linea.class, Long.valueOf(linea_id));
			queryStr += "AND c.credito.linea = :linea ";
		}
		queryStr += "ORDER BY c.credito.numeroAtencion, c.numero";
		Query query = bp.createQuery(queryStr);
		query.setParameter("fechaDesde", fechaDesde);
		query.setParameter("fechaHasta", fechaHasta);
		if (credito != null) {
			query.setParameter("idCredito", credito.getId());
		} else if (linea != null) {
			query.setParameter("linea", linea);
		}
		listaCuotas = new ArrayList<Cuota>(query.list());
		seleccion = new Boolean[listaCuotas.size()];
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("listaCuotas",
				this.listaCuotas);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("seleccion", this.seleccion);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("tipoProceso2",
				this.tipoProceso);
	}

	private void modificarFechasCovid19() {
		prorroga.setCuotas(cuotasProrrogadas);
		Cuota cuotaOriginal, cuotaDestino;
		Date nuevoVencimiento;
		bp.begin();
		try {
			for (int i = listaCuotas.size() - 1; i >= 0; i--) {
				if (seleccion[i] != null && !seleccion[i].booleanValue()) {
					cuotaOriginal = (Cuota) bp.getById(Cuota.class, listaCuotas.get(i).getId());
					if (cuotaOriginal.getCredito().getCaducidadPlazo() != null) {
						// con caducidad de plazo no se puede prorrogar
						cuotasCaducidad.add(cuotaOriginal);
					} else {
						// determinar el nuevo vencimiento, la cantidad de meses deberia tomarse desde
						// la pantalla
						nuevoVencimiento = buscarNuevoVencimiento(cuotaOriginal.getFechaVencimiento(), 1,
								Integer.parseInt(meses), Calendar.MONTH);
						// buscar la cuota de destino
						Object[] nrosCuotas = (Object[]) bp.getNamedQuery("Cuota.findByRangoFechas")
								.setParameter("idCredito", cuotaOriginal.getCredito_id())
								.setParameter("fechaHasta", nuevoVencimiento).uniqueResult();
						cuotaDestino = (Cuota) bp.getNamedQuery("Cuota.findByNumero")
								.setParameter("idCredito", cuotaOriginal.getCredito_id())
								.setParameter("nroCuota", nrosCuotas[1]).uniqueResult();
						// realizar los ajustes en ctacte
						if (cuotaDestino != null) {
							realizarAjustes(cuotaOriginal, cuotaDestino);
							cuotaDestino.setCapitalOriginal(cuotaDestino.getCapital());
							cuotaDestino.setCapital(cuotaDestino.getCapital() + cuotaOriginal.getCapital());
							cuotaDestino.setCompensatorio(
									cuotaDestino.getCompensatorio() + cuotaOriginal.getCompensatorio());
							cuotaDestino.setPunitorio(cuotaDestino.getPunitorio() + cuotaOriginal.getPunitorio());
							cuotaDestino.setMoratorio(cuotaDestino.getMoratorio() + cuotaOriginal.getMoratorio());
							cuotaDestino
									.setBonificacion(cuotaDestino.getBonificacion() + cuotaOriginal.getBonificacion());
							cuotaDestino.setGastos(cuotaDestino.getGastos() + cuotaOriginal.getGastos());
							cuotaDestino.setMultas(cuotaDestino.getMultas() + cuotaOriginal.getMultas());
							bp.update(cuotaDestino);
							cuotaOriginal.setCapitalOriginal(cuotaOriginal.getCapital());
							cuotaOriginal.setFechaVencimientoOriginal(cuotaOriginal.getFechaVencimiento());
							cuotaOriginal.setCapital(0d);
							cuotaOriginal.setFechaVencimiento(cuotaDestino.getFechaVencimiento());
							cuotaOriginal.setCompensatorio(0d);
							cuotaOriginal.setPunitorio(0d);
							cuotaOriginal.setMoratorio(0d);
							cuotaOriginal.setBonificacion(0d);
							cuotaOriginal.setGastos(0d);
							cuotaOriginal.setMultas(0d);
							cuotaOriginal.setEstado(Cuota.CANCELADA);
							bp.update(cuotaOriginal);
							cuotasProrrogadas.add(cuotaOriginal);
							// notificacion, para mostrar datos en la pesta;a "Notificaciones"
							Notificacion notificacion = crearNotificacion(cuotaOriginal.getCredito(),
									prorroga.getFecha());
							bp.save(notificacion);
							// solicitud de prorroga, para mostrar datos en la pesta;a "Prorrogas"
							String idEstadoInicialSolicitud = DirectorHelper.getString("Prorroga.EstadoInicial");
							SolicitudProrrogaEstado solicitudProrrogaEstado = new SolicitudProrrogaEstado();
							Estado estadoInicialSolicitud = (Estado) bp.getById(Estado.class,
									new Long(idEstadoInicialSolicitud.trim()));
							SolicitudProrroga solicitudProrroga = new SolicitudProrroga();
							solicitudProrroga.setFechaIngreso(this.fechaActual.getTime());
							solicitudProrroga.setFechaProceso(this.fechaActual.getTime());
							solicitudProrroga.setMotivo(this.resolucion);
							solicitudProrroga.setObjetoi(cuotaOriginal.getCredito());
							solicitudProrroga.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
							solicitudProrroga.setObservacion(this.observaciones);
							bp.save(solicitudProrroga);
							solicitudProrrogaEstado.setEstado(estadoInicialSolicitud);
							solicitudProrrogaEstado.setFechaDesde(this.fechaActual.getTime());
							solicitudProrrogaEstado.setSolicitudProrroga(solicitudProrroga);
							solicitudProrrogaEstado
									.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
							bp.saveOrUpdate(solicitudProrrogaEstado);
						}
					}
				}
			}
			bp.save(prorroga);
			bp.commit();
		} catch (Exception e) {
			e.printStackTrace();
			bp.rollback();
			errores.put("prorroga.covid19.errorGeneral",
					new ActionMessage("prorroga.covid19.errorGeneral", e.getMessage()));
			return;
		}

	}

	@SuppressWarnings("unchecked")
	private void realizarAjustes(Cuota cuotaOriginal, Cuota cuotaDestino) {
		Objetoi proyecto = cuotaOriginal.getCredito();
		Linea linea = proyecto.getLinea();

		// buscar los movimientos de cuenta corriente para la cuota
		List<Ctacte> movimientos = bp.getNamedQuery("Ctacte.findByCuota").setParameter("idCuota", cuotaOriginal.getId())
				.list();

		// crear notaCredito
		Double importeCreditoOrigen = 0d;
		Boleto notaCreditoOrigen = null;
		Ctacte ctacteCreditoOrigen;
		Bolcon bolconCreditoOrigen;
		// bonificaciones
		Double importeDebitoOrigen = 0d;
		Boleto notaDebitoOrigen = null;
		Ctacte ctacteDebitoOrigen;
		Bolcon bolconDebitoOrigen;

		// crear notaDebito
		Double importeDebitoDestino = 0d;
		Boleto notaDebitoDestino = null;
		Ctacte ctacteDebitoDestino;
		Bolcon bolconDebitoDestino;
		// bonificaciones
		Double importeCreditoDestino = 0d;
		Boleto notaCreditoDestino = null;
		Ctacte ctacteCreditoDestino;
		Bolcon bolconCreditoDestino;

		String conceptoFacturado, conceptoAsociado;
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo",
				String.valueOf(fechaActual.get(Calendar.YEAR)));
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;
		for (Ctacte ctacte : movimientos) {
			conceptoFacturado = ctacte.getFacturado().getConcepto().getConcepto();
			conceptoAsociado = ctacte.getAsociado().getConcepto().getConcepto();
			if (conceptoFacturado.equalsIgnoreCase(linea.getCapital().getConcepto())) {
				/**
				 * si es capital, el movimiento en ctacte cambia de cuota
				 */
				ctacte.setCuota(cuotaDestino);
				/**
				 * #22098 <br>
				 * elimino la relacion con el boleto resumen de la facturacion para que pueda
				 * ser incluido en la facturacion de la cuota destino
				 */
				ctacte.setIdBoletoResumen(null);
				bp.update(ctacte);
			} else if (conceptoFacturado.equalsIgnoreCase("bon")) {
				// El concepto de bonificacion no esta en la linea y desde la ctacte no tengo
				// forma simple de llegar a la bonificacion. Ademas podria haber mas de una
				// bonificacion aplicada. Demasiado complicado solo para comparar el concepto.
				if (notaDebitoOrigen == null) {
					notaDebitoOrigen = crearComprobante(Boleto.TIPO_BOLETO_NOTADEB, cuotaOriginal);
					bp.save(notaDebitoOrigen);
				}
				if (notaCreditoDestino == null) {
					notaCreditoDestino = crearComprobante(Boleto.TIPO_BOLETO_NOTACRED, cuotaDestino);
					bp.save(notaCreditoDestino);
				}
				// crear credito en ctacte
				ctacteDebitoOrigen = proyecto.crearCtacte(conceptoFacturado, conceptoAsociado, ctacte.getImporte(),
						null, fechaActual.getTime(), notaDebitoOrigen, null, cuotaOriginal, false,
						Ctacte.TIPO_CONCEPTO_PRORROGA_COVID19);
				ctacteDebitoOrigen.setEnteBonificador(ctacte.getEnteBonificador());
				ctacteDebitoOrigen.setUsuario(usuario);
				ctacteDebitoOrigen.setDetalle("Traslado de vencimiento "
						+ ctacte.getFacturado().getConcepto().getDetalle() + " por (COVID-19)");
				ctacteDebitoOrigen.setComprobante("SEGUN RESOLUCION DE PRORROGA " + prorroga.getResolucion());
				ctacteDebitoOrigen.setTipomov(Tipomov.buscarDebito());
				ctacteDebitoOrigen.setTipoMovimiento("bonificacion");
				ctacteDebitoOrigen.getId().setMovimientoCtacte(movimientoCtaCte);
				ctacteDebitoOrigen.getId().setItemCtacte(itemCtaCte);
				itemCtaCte++;
				bp.save(ctacteDebitoOrigen);
				// crear bolcon
				bolconDebitoOrigen = proyecto.crearBolcon(conceptoFacturado, ctacte.getImporte(), notaDebitoOrigen,
						ctacteDebitoOrigen.getId(), cuotaOriginal);
				bolconDebitoOrigen.setTipomov(Tipomov.buscarDebito());
				bp.save(bolconDebitoOrigen);
				// incrementar importe
				importeDebitoOrigen += bolconDebitoOrigen.getImporte();
				// crear debito en ctacte
				ctacteCreditoDestino = proyecto.crearCtacte(conceptoFacturado, conceptoAsociado, ctacte.getImporte(),
						null, fechaActual.getTime(), notaCreditoDestino, null, cuotaDestino, false,
						Ctacte.TIPO_CONCEPTO_PRORROGA_COVID19);
				ctacteCreditoDestino.setEnteBonificador(ctacte.getEnteBonificador());
				ctacteCreditoDestino.setUsuario(usuario);
				ctacteCreditoDestino.setDetalle(
						"Aplicación traslado de " + ctacte.getFacturado().getConcepto().getDetalle() + " (COVID-19) "
								+ (ctacte.getBoleto() != null
										? (ctacte.getBoleto().getTipo() + " " + ctacte.getBoleto().getNumeroBoleto())
										: ""));
				ctacteCreditoDestino.setComprobante("SEGUN RESOLUCION DE PRORROGA " + prorroga.getResolucion());
				ctacteCreditoDestino.setTipomov(Tipomov.buscarCredito());
				ctacteCreditoDestino.setTipoMovimiento("bonificacion");
				ctacteCreditoDestino.getId().setMovimientoCtacte(movimientoCtaCte);
				ctacteCreditoDestino.getId().setItemCtacte(itemCtaCte);
				itemCtaCte++;
				bp.save(ctacteCreditoDestino);
				// crear bolcon
				bolconCreditoDestino = proyecto.crearBolcon(conceptoFacturado, ctacte.getImporte(), notaCreditoDestino,
						ctacteCreditoDestino.getId(), cuotaDestino);
				bolconCreditoDestino.setTipomov(Tipomov.buscarCredito());
				bp.save(bolconCreditoDestino);
				// incrementar importe
				importeCreditoDestino += bolconCreditoDestino.getImporte();
			} else {
				if (notaCreditoOrigen == null) {
					notaCreditoOrigen = crearComprobante(Boleto.TIPO_BOLETO_NOTACRED, cuotaOriginal);
					bp.save(notaCreditoOrigen);
				}
				if (notaDebitoDestino == null) {
					notaDebitoDestino = crearComprobante(Boleto.TIPO_BOLETO_NOTADEB, cuotaDestino);
					bp.save(notaDebitoDestino);
				}
				// crear credito en ctacte
				ctacteCreditoOrigen = proyecto.crearCtacte(conceptoFacturado, conceptoAsociado, ctacte.getImporte(),
						null, fechaActual.getTime(), notaCreditoOrigen, null, cuotaOriginal, false,
						Ctacte.TIPO_CONCEPTO_PRORROGA_COVID19);
				ctacteCreditoOrigen.setUsuario(usuario);
				ctacteCreditoOrigen.setDetalle("Traslado de vencimiento "
						+ ctacte.getFacturado().getConcepto().getDetalle() + " por (COVID-19)");
				ctacteCreditoOrigen.setComprobante("SEGUN RESOLUCION DE PRORROGA " + prorroga.getResolucion());
				ctacteCreditoOrigen.setTipomov(Tipomov.buscarCredito());
				ctacteCreditoOrigen.setTipoMovimiento("movManualCred");
				ctacteCreditoOrigen.getId().setMovimientoCtacte(movimientoCtaCte);
				ctacteCreditoOrigen.getId().setItemCtacte(itemCtaCte);
				itemCtaCte++;
				bp.save(ctacteCreditoOrigen);
				// crear bolcon
				bolconCreditoOrigen = proyecto.crearBolcon(conceptoFacturado, ctacte.getImporte(), notaCreditoOrigen,
						ctacteCreditoOrigen.getId(), cuotaOriginal);
				bolconCreditoOrigen.setTipomov(Tipomov.buscarCredito());
				bp.save(bolconCreditoOrigen);
				// incrementar importe
				importeCreditoOrigen += bolconCreditoOrigen.getImporte();
				// crear debito en ctacte
				ctacteDebitoDestino = proyecto.crearCtacte(conceptoFacturado, conceptoAsociado, ctacte.getImporte(),
						null, fechaActual.getTime(), notaDebitoDestino, null, cuotaDestino, false,
						Ctacte.TIPO_CONCEPTO_PRORROGA_COVID19);
				ctacteDebitoDestino.setUsuario(usuario);
				ctacteDebitoDestino.setDetalle(
						"Aplicación traslado de " + ctacte.getFacturado().getConcepto().getDetalle() + " (COVID-19) "
								+ (ctacte.getBoleto() != null
										? (ctacte.getBoleto().getTipo() + " " + ctacte.getBoleto().getNumeroBoleto())
										: ""));
				ctacteDebitoDestino.setComprobante("SEGUN RESOLUCION DE PRORROGA " + prorroga.getResolucion());
				ctacteDebitoDestino.setTipomov(Tipomov.buscarDebito());
				ctacteDebitoDestino.setTipoMovimiento("movManualDeb");
				ctacteDebitoDestino.getId().setMovimientoCtacte(movimientoCtaCte);
				ctacteDebitoDestino.getId().setItemCtacte(itemCtaCte);
				itemCtaCte++;
				bp.save(ctacteDebitoDestino);
				// crear bolcon
				bolconDebitoDestino = proyecto.crearBolcon(conceptoFacturado, ctacte.getImporte(), notaDebitoDestino,
						ctacteDebitoDestino.getId(), cuotaDestino);
				bolconDebitoDestino.setTipomov(Tipomov.buscarDebito());
				bp.save(bolconDebitoDestino);
				// incrementar importe
				importeDebitoDestino += bolconDebitoDestino.getImporte();
			}
		}
		if (notaCreditoOrigen != null) {
			notaCreditoOrigen.setImporte(importeCreditoOrigen.doubleValue());
			bp.save(notaCreditoOrigen);
		}
		if (notaDebitoOrigen != null) {
			notaDebitoOrigen.setImporte(importeDebitoOrigen.doubleValue());
			bp.save(notaDebitoOrigen);
		}
		if (notaCreditoDestino != null) {
			notaCreditoDestino.setImporte(importeCreditoDestino.doubleValue());
			bp.save(notaCreditoDestino);
		}
		if (notaDebitoDestino != null) {
			notaDebitoDestino.setImporte(importeDebitoDestino.doubleValue());
			bp.save(notaDebitoDestino);
		}
	}

	private void modificarFechasNormal() {
		prorroga.setCuotas(cuotasProrrogadas);
		Cuota cuota;
		Date proximoVencimiento;
		String valida = DirectorHelper.getString("validaFecha.prorroga");
		if (tipoCorrimiento.equals("fechaVencimiento")) {
			nuevoVencimiento = DateHelper.getDiaHabil(feriados, nuevoVencimiento);
			if (valida != null && valida.equals("1")) {
				if (nuevoVencimiento.before(fechaActual.getTime())) {
					errores.put("prorroga.errorFechaVencimiento.anterior", "prorroga.errorFechaVencimiento.anterior");
					return;
				}
			}
			bp.begin();
			for (int i = listaCuotas.size() - 1; i >= 0; i--) {
				if (seleccion[i] != null && !seleccion[i].booleanValue()) {
					cuota = (Cuota) bp.getById(Cuota.class, listaCuotas.get(i).getId());
					for (Date fecha : fechasCuotas) {
						if (!cuota.getFechaVencimiento().equals(fecha)) {
							bp.rollback();
							errores.put("prorroga.errorFechaVencimiento.cuotas",
									"prorroga.errorFechaVencimiento.cuotas");
							return;
						}
					}
					if (cuota.getCredito().getCaducidadPlazo() != null) {
						cuotasCaducidad.add(cuota); // con caducidad de plazo no se puede prorrogar
					} else {
						proximoVencimiento = calcularProximoVencimiento(cuota);
						if (proximoVencimiento != null
								&& DateHelper.getDiffDates(nuevoVencimiento, proximoVencimiento, 2) < 0) {
							cuotasFechaSuperpuesta.add(cuota);
						} else {
							fechasCuotas.add(cuota.getFechaVencimiento());
							cuota.setFechaVencimientoOriginal(cuota.getFechaVencimiento());
							cuota.setFechaVencimiento(nuevoVencimiento);
							actualizarIntereses(cuota);
							bp.update(cuota);
							cuotasProrrogadas.add(cuota);
							Notificacion notificacion = crearNotificacion(cuota.getCredito(), prorroga.getFecha());
							bp.save(notificacion);
						}
					}
				}
			}
			bp.save(prorroga);
			bp.commit();
		} else {
			Date nuevoVencimiento;
			bp.begin();
			for (int i = listaCuotas.size() - 1; i >= 0; i--) {
				if (seleccion[i] != null && !seleccion[i].booleanValue()) {
					cuota = (Cuota) bp.getById(Cuota.class, listaCuotas.get(i).getId());
					if (cuota.getCredito().getCaducidadPlazo() != null) {
						// con caducidad de plazo no se puede prorrogar
						cuotasCaducidad.add(cuota);
					} else {
						if (tipoCorrimiento.equals("correrDias")) {
							nuevoVencimiento = calcularNuevoVencimiento(cuota.getFechaVencimiento(), dias,
									tipoMovimiento);
						} else {
							nuevoVencimiento = calcularNuevoVencimientoMeses(cuota.getFechaVencimiento(), meses,
									tipoMovimiento);
						}
						proximoVencimiento = calcularProximoVencimiento(cuota);
						// controlo que el movimiento del vencimiento, no pise el vencimiento de la
						// proxima cuota si el proximoVencimiento es null, es porque esta es la ultima
						// cuota
						if (proximoVencimiento != null
								&& DateHelper.getDiffDates(nuevoVencimiento, proximoVencimiento, 2) < 0) {
							cuotasFechaSuperpuesta.add(cuota);
						} else if (valida != null && valida.equals("1")) {
							if (nuevoVencimiento.before(fechaActual.getTime())) {
								cuotasFechaAnterior.add(cuota);
							} else {
								cuota.setFechaVencimientoOriginal(cuota.getFechaVencimiento());
								cuota.setFechaVencimiento(nuevoVencimiento);
								actualizarIntereses(cuota);
								bp.update(cuota);
								cuotasProrrogadas.add(cuota);
								Notificacion notificacion = crearNotificacion(cuota.getCredito(), prorroga.getFecha());
								bp.save(notificacion);
							}
						} else {
							cuota.setFechaVencimientoOriginal(cuota.getFechaVencimiento());
							cuota.setFechaVencimiento(nuevoVencimiento);
							actualizarIntereses(cuota);
							bp.update(cuota);
							cuotasProrrogadas.add(cuota);
							Notificacion notificacion = crearNotificacion(cuota.getCredito(), prorroga.getFecha());
							bp.save(notificacion);
						}
					}
				}
			}
			bp.save(prorroga);
			bp.commit();
		}
	}

	private Notificacion crearNotificacion(Objetoi credito, Date fechaCreada) {
		Notificacion notificacion = new Notificacion();
		notificacion.setCredito(credito);
		notificacion.setFechaCreada(fechaCreada);
		notificacion.setObservaciones(
				(!observaciones.isEmpty() ? (observaciones + " - ") : "") + "Resolución " + resolucion);
		notificacion.setTipoAviso("prorrogaVencimiento");
		return notificacion;
	}

	private ProrrogaMasiva crearProrroga() {
		ProrrogaMasiva prorroga = new ProrrogaMasiva();
		prorroga.setResolucion(resolucion);
		prorroga.setObservaciones(observaciones);
		prorroga.setUsuario(usuario);
		return prorroga;
	}

	private Boleto crearComprobante(String tipoComprobante, Cuota cuota) {
		Calendar fechaProceso = Calendar.getInstance(new Locale("es-AR"));
		Boleto boleto = new Boleto();
		boleto.setFechaEmision(fechaProceso.getTime());
		boleto.setFechaVencimiento(fechaProceso.getTime());
		boleto.setTipo(tipoComprobante);
		boleto.setUsuario(usuario);
		boleto.setNumeroCuota(cuota.getNumero());
		boleto.setObjetoi(cuota.getCredito());
		boleto.setPeriodoBoleto((long) fechaProceso.get(Calendar.YEAR));
		boleto.setVerificadorBoleto(0L);
		boleto.setImporte(0d);
		boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
		return boleto;
	}

	private Date calcularProximoVencimiento(Cuota cuota) {
		Date proximoVencimiento = null;
		Cuota proximaCuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero")
				.setParameter("idCredito", cuota.getCredito().getId()).setParameter("nroCuota", cuota.getNumero() + 1)
				.uniqueResult();
		if (proximaCuota != null) {
			proximoVencimiento = proximaCuota.getFechaVencimiento();
		}
		return proximoVencimiento;
	}

	/**
	 * @param fechaOriginal fecha de partida
	 * @param operacion     +1 o -1 para saber si suma o resta
	 * @param unidades      la cantidad de unidades a usar en la operacion
	 * @param atributo      alguno de los fields definidos en Calendar
	 * @return
	 */
	private Date buscarNuevoVencimiento(Date fechaOriginal, int operacion, int unidades, int atributo) {
		Date nuevoVencimiento = DateHelper.add(fechaOriginal, unidades * operacion, atributo);
		return DateHelper.getDiaHabil(feriados, nuevoVencimiento);

	}

	private Date calcularNuevoVencimiento(Date fechaVencimiento, String dias, String tipoCorrimiento) {
		int operacion = tipoCorrimiento.equals("atras") ? (-1) : (1);
		return buscarNuevoVencimiento(fechaVencimiento, operacion, Integer.parseInt(dias), Calendar.DAY_OF_MONTH);
	}

	private Date calcularNuevoVencimientoMeses(Date fechaVencimiento, String meses, String tipoCorrimiento) {
		int operacion = tipoCorrimiento.equals("atras") ? (-1) : (1);
		return buscarNuevoVencimiento(fechaVencimiento, operacion, Integer.parseInt(meses), Calendar.MONTH);
	}

	@SuppressWarnings("unchecked")
	private void actualizarIntereses(Cuota cuota) {
		// asumir que el 100% fue desembolsado antes de la primera cuota
		List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
				.setParameter("idCredito", cuota.getCredito_id()).list();
		Objetoi proyectoCalculo = cuota.getCredito();
		Date fechaPrimerDesembolso = null;
		double totalDesembolsado = 0;
		for (Desembolso desembolso : desembolsos) {
			if (fechaPrimerDesembolso == null) {
				fechaPrimerDesembolso = desembolso.getFechaReal() != null ? desembolso.getFechaReal()
						: desembolso.getFecha();
			}
			totalDesembolsado += (desembolso.getImporteReal() != null) ? desembolso.getImporteReal()
					: desembolso.getImporte();
		}
		// si no hay desembolso, usar la primera cuota
		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", cuota.getCredito_id())
				.list();
		if (fechaPrimerDesembolso == null && !cuotas.isEmpty()) {
			fechaPrimerDesembolso = cuotas.get(0).getFechaVencimiento();
		}
		// primer vencimiento anterior es = desembolso
		CreditoHandler ch = new CreditoHandler();
		Date fechaAnterior = fechaPrimerDesembolso;
		double saldoCapital = totalDesembolsado;
		int dias = (int) DateHelper.getDiffDates(fechaAnterior, cuota.getFechaVencimiento(), 2);
		double tasaFinal = proyectoCalculo.findTasaCompensatorio().calcularTasaFinal();
		double icb = CalculoInteresHelper.calcularInteresCompuesto(saldoCapital, tasaFinal, dias, dias);
		double tasaNeta = ch.calcularTasaNeta(cuota.getCredito_id(), cuota.getNumero());
		double icn = CalculoInteresHelper.calcularInteresCompuesto(saldoCapital, tasaNeta, dias, dias);
		cuota.setCompensatorio(icb);
		cuota.setBonificacion(icb - icn);
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		if (action != null && action.equals("filtrar")) {
			if (this.fechaDesde == null) {
				this.errores.put("prorroga.fechaDesdeNula", "prorroga.fechaDesdeNula");
			}
			if (this.fechaHasta == null) {
				this.errores.put("prorroga.fechaHastaNula", "prorroga.fechaHastaNula");
			}
			if (this.tipoProceso == null || this.tipoProceso.isEmpty()) {
				this.errores.put("prorroga.tipoProcesoNulo", "prorroga.tipoProcesoNulo");
			}
		} else if (action != null && action.equals("modificarFechas")) {
			if (this.tipoProceso == null || this.tipoProceso.isEmpty()) {
				this.errores.put("prorroga.tipoProcesoNulo", "prorroga.tipoProcesoNulo");
			}
			if (this.tipoCorrimiento.equals("correrDias")) {
				if ("Covid19".equalsIgnoreCase(this.tipoProceso)) {
					this.errores.put("prorroga.covid19.noPermitido", "prorroga.covid19.noPermitido");
				} else {
					if (this.dias == null || this.dias.isEmpty()) {
						this.errores.put("prorroga.diasNulo", "prorroga.diasNulo");
					} else {
						try {
							int i = Integer.parseInt(dias);
							if (i <= 0) {
								this.errores.put("prorroga.diasNoValido", "prorroga.diasNoValido");
							}
						} catch (NumberFormatException e) {
							this.errores.put("prorroga.diasNoValido", "prorroga.diasNoValido");
						}
					}
					if (this.tipoMovimiento == null || this.tipoMovimiento.isEmpty()) {
						this.errores.put("prorroga.movimientoNulo", "prorroga.movimientoNulo");
					}
				}
			} else if (this.tipoCorrimiento.equals("correrMeses")) {
				if (this.meses == null || this.meses.isEmpty()) {
					this.errores.put("prorroga.mesesNulo", "prorroga.mesesNulo");
				} else {
					try {
						int i = Integer.parseInt(this.meses);
						if (i <= 0) {
							this.errores.put("prorroga.mesesNoValido", "prorroga.mesesNoValido");
						}
					} catch (NumberFormatException e) {
						this.errores.put("prorroga.mesesNoValido", "prorroga.mesesNoValido");
					}
				}
				if (this.tipoMovimiento == null || this.tipoMovimiento.isEmpty()) {
					this.errores.put("prorroga.movimientoNulo", "prorroga.movimientoNulo");
				}
			} else if (this.tipoCorrimiento.equals("fechaVencimiento")) {
				if ("Covid19".equalsIgnoreCase(this.tipoProceso)) {
					this.errores.put("prorroga.covid19.noPermitido", "prorroga.covid19.noPermitido");
				} else {
					if (this.nuevoVencimiento == null) {
						this.errores.put("prorroga.fechaVencimiento", "prorroga.fechaVencimiento");
					}
				}
			}
		}
		return this.errores.isEmpty();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getFechaDesdeStr() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesdeStr(String fechaDesdeStr) {
		this.fechaDesde = DateHelper.getDate(fechaDesdeStr);
	}

	public String getFechaHastaStr() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHastaStr(String fechaHastaStr) {
		this.fechaHasta = DateHelper.getDate(fechaHastaStr);
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getDias() {
		return dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}

	public List<Cuota> getListaCuotas() {
		return listaCuotas;
	}

	public void setListaCuotas(List<Cuota> listaCuotas) {
		this.listaCuotas = listaCuotas;
	}

	public Boolean[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(Boolean[] seleccion) {
		this.seleccion = seleccion;
	}

	public boolean isFinalizo() {
		return finalizo;
	}

	public void setFinalizo(boolean finalizo) {
		this.finalizo = finalizo;
	}

	public String getMensajeFin() {
		return mensajeFin;
	}

	public void setMensajeFin(String mensajeFin) {
		this.mensajeFin = mensajeFin;
	}

	public String getLinea_id() {
		return linea_id;
	}

	public void setLinea_id(String linea_id) {
		this.linea_id = linea_id;
	}

	public List<Cuota> getCuotasProrrogadas() {
		return cuotasProrrogadas;
	}

	public void setCuotasProrrogadas(List<Cuota> cuotasProrrogadas) {
		this.cuotasProrrogadas = cuotasProrrogadas;
	}

	public List<Cuota> getCuotasFechaAnterior() {
		return cuotasFechaAnterior;
	}

	public void setCuotasFechaAnterior(List<Cuota> cuotasFechaAnterior) {
		this.cuotasFechaAnterior = cuotasFechaAnterior;
	}

	public List<Cuota> getCuotasFechaSuperpuesta() {
		return cuotasFechaSuperpuesta;
	}

	public void setCuotasFechaSuperpuesta(List<Cuota> cuotasFechaSuperpuesta) {
		this.cuotasFechaSuperpuesta = cuotasFechaSuperpuesta;
	}

	public String getTipoCorrimiento() {
		return tipoCorrimiento;
	}

	public void setTipoCorrimiento(String tipoCorrimiento) {
		this.tipoCorrimiento = tipoCorrimiento;
	}

	public String getMeses() {
		return meses;
	}

	public void setMeses(String meses) {
		this.meses = meses;
	}

	public void setNuevoVencimientoStr(String nuevoVencimientoStr) {
		this.nuevoVencimiento = DateHelper.getDate(nuevoVencimientoStr);
	}

	public void setForwardURL(String forwardURL) {
		this.forwardURL = forwardURL;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public Long getNroAtencion() {
		return nroAtencion;
	}

	public void setNroAtencion(Long nroAtencion) {
		this.nroAtencion = nroAtencion;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

}