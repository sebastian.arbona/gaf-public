package com.asf.cred.business;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.datatype.XMLGregorianCalendar;

import com.asf.gaf.hibernate.Cotizacion;
import com.asf.gaf.hibernate.Moneda;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.DetalleArchivo;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.DeudoresVarios;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.PagosKey;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class PreaplicacionPagos implements IProcess {

	private String forward;
	private String accion;
	private HashMap<String, String> errores;
	private Long idDetalleArchivo;
	private BusinessPersistance bp;
	private Long tcomp12;
	private String idBoletoList;
	private String fenv12Str;
	private String idDetalleRecaudacionList;
	private String boletosXML;
	private Long idDetalleRecaudacion;
	private Long idBoleto;
	private String idFechaCobranza;

	private Long idmediopago; // Codigo del medio de pago seleccionado..
	private StrutsArray cobroPagos = new StrutsArray(Cobropago.class);
	private Long idcaratula;
	private String[] lista = new String[] {}; // Lista de boletos.
	private Long lote;
	private Double importe; // Importe pagado.
	private boolean guardado = false;

	private String mensaje;

	public PreaplicacionPagos() {
		forward = "PreaplicacionPagos";
		errores = new HashMap<String, String>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		idcaratula = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("ImportacionForm.idCaratula");
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			accion = "generarBoletos";
		} else if (accion.equals("generarBoletos")) {

		} else if (accion.equals("guardar")) {
			guardar();
		} else if (accion.equals("eliminar")) {
			eliminar();
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void guardar() {
		try {
			CobroPagoHandler handler = new CobroPagoHandler();
			List<Object[]> lotes = handler.getLotes(idcaratula);
			for (Object[] row : lotes) {
				Long lote = (Long) row[0];
				handler.delete(idcaratula, lote);
			}

			bp.begin();

			Caratula caratula = (Caratula) bp.getById(Caratula.class, idcaratula);

			/* Genera Pago */
			double importeTotal = 0.0;
			String[] idBoletoArray = idBoletoList.split("-");
			String[] idDetalleRecaudacionArray = idDetalleRecaudacionList.split("-");
			Long idBoleto;
			Long idDetalleRecaudacion;
			Boleto boleto;
			DetalleRecaudacion detalle;
			DetalleArchivo detalleArchivo;
			GenerarPagos generarPagos = new GenerarPagos(bp);
			Pagos pago;
			CreditoHandler ch = new CreditoHandler();

			for (int i = 0; i < idBoletoArray.length; i++) {
				if (idBoletoArray[i] == null || idBoletoArray[i].trim().isEmpty()) {
					continue;
				}
				idBoleto = new Long(idBoletoArray[i]);
				idDetalleRecaudacion = new Long(idDetalleRecaudacionArray[i]);
				boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
				detalle = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class, idDetalleRecaudacion);
				if (detalle.getMedioPago() != null) {
					generarPagos.generar(boleto, caratula, boleto.getImporte(),
							detalle.getMedioPago().getDenominacion());
				} else {
					generarPagos.generar(boleto, caratula, boleto.getImporte());
				}

				bp.update(detalle);

				pago = generarPagos.getPagos();
				pago.setFechaPago(detalle.getFechaAcreditacionValor());
				pago.setMontoOriginal(detalle.getImporte());
				pago.setMoneda(detalle.getMoneda());

				double cotizacion;
				if (detalle.getMoneda() != null) {
					Long idMonedaCredito = boleto.getObjetoi().getMoneda_id() != null
							? boleto.getObjetoi().getMoneda_id()
							: boleto.getObjetoi().getLinea().getMoneda_id();
					if (idMonedaCredito == null) {
						idMonedaCredito = 1L;
					}
					cotizacion = ch.getCotizacion(detalle.getMoneda().getId(), idMonedaCredito,
							detalle.getFechaRecaudacion());
				} else {
					cotizacion = 1L;
				}
				pago.setCotizacion(cotizacion);

				bp.update(pago);
				importeTotal += boleto.getImporte().doubleValue();
			}

			// procesar los 99 ingresos varios
			// aca encuentra movimientos de recaudacion de deudores propios(gic) y
			// extra�os(claudio)
			List<DetalleRecaudacion> detalles = bp.createQuery(
					"select d from DetalleRecaudacion d where d.numeroProyecto > :numeroIngVar and d.caratula.id = :idCaratula and d.recaudacion is null")
					.setParameter("idCaratula", idcaratula).setParameter("numeroIngVar", 9900000000L).list();

			for (DetalleRecaudacion d : detalles) {
				long numeroIngVarios = d.getNumeroProyecto() - 9900000000L;
				// aca encuentra deudores propios ya que combina numero de deudor e importe
				// con mucha mala suerte podria darse un falso positivo, donde coincide numero e
				// importe, pero se originaron en sistemas distintos
				DeudoresVarios dv = (DeudoresVarios) bp
						.createQuery("select d from DeudoresVarios d where d.numero = :numero and d.importe = :importe")
						.setParameter("numero", numeroIngVarios).setParameter("importe", d.getImporte()).uniqueResult();
				if (dv == null) {
					// no encontro, debe ser un deudor de otro sistema
					// permito que continue para que incremente el total cobrado
				} else {
					if (dv.getNumeroAtencion() != null && !dv.getNumeroAtencion().isEmpty() && dv.isImpactoCtacte()) {
						Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
								.setParameter("nroAtencion", new Long(dv.getNumeroAtencion())).uniqueResult();
						Boleto boletoIngVarios = crearBoletoIngresosVarios(d.getFechaAcreditacionValor(),
								d.getImporte(), credito);
						Pagos pagoAsociado = new Pagos();
						pagoAsociado.getId().setBoleto(boletoIngVarios);
						pagoAsociado.getId().setCaratula(caratula);
						pagoAsociado.setActualizado(false);
						pagoAsociado.setFechaPago(d.getFechaAcreditacionValor());
						pagoAsociado.setFechaEmision(boletoIngVarios.getFechaEmision());
						pagoAsociado.setFechaVencimiento(boletoIngVarios.getFechaVencimiento());
						pagoAsociado.setImporte(importe);
						if (d.getMedioPago() != null) {
							pagoAsociado.setMedioPago(d.getMedioPago().getDenominacion());
						}
						pagoAsociado.setObjetoi(credito);
						bp.save(pagoAsociado);

						dv.setPagoAsociado(pagoAsociado);
						bp.update(dv);

						Cobropago cp = new Cobropago();
						cp.setMediopagoId(d.getMedioPago() != null ? d.getMedioPago().getId() : null);
						cp.setIdcaratula(idcaratula);
						cp.setImporte(d.getImporte());
						cobroPagos.add(cp);

						String linea = boletoIngVarios.getId() + " $" + d.getImporte() + "-";
						lista = Arrays.copyOf(lista, lista.length + 1);
						lista[lista.length - 1] = linea;
					} else {
						dv.setCaratula(caratula);
						bp.update(dv);
					}
				}
				importeTotal += d.getImporte();
			}

			/* Registra el pago del boleto en Cobropago, Bolepago y Detallebolepago */
			RegistraCobropago rC = new RegistraCobropago(bp);
			rC.registraCobro(lista, cobroPagos);
			setLote(rC.getLote());

			/* Actualiza total de comprobantes e importe total de Caratula */
			if (caratula.getComprobantes() == null) {
				caratula.setComprobantes(0L);
			}
			if (caratula.getImporte() == null) {
				caratula.setImporte(0D);
			}
			caratula.setComprobantes(caratula.getComprobantes() + this.tcomp12);
			caratula.setImporte(caratula.getImporte() + importeTotal);
			// Busco el ultimo valor de la cotizacion para la moneda
			List<Cotizacion> cotis = bp
					.createQuery("Select c from Cotizacion c where c.moneda.id=:moneda order by c.fechaDesde")
					.setParameter("moneda", 1L)// harcodeo peso argentino, preguntar
					.list();
			Cotizacion coti = new Cotizacion();
			if (cotis.size() != 0) {
				coti = cotis.get(0);
			}
			for (Cotizacion cotizacion : cotis) {
				if (cotizacion.getFechaHasta() == null) {
					coti = cotizacion;
				} else {
					if (coti.getFechaHasta() != null && coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
						coti = cotizacion;
					}
				}
			}
			caratula.setCotiza(coti.getVenta());
			bp.update(caratula);
			bp.commit();
			fenv12Str = caratula.getFechaEnvioStr();
			guardado = true;
			mensaje = "Preaplicacion guardada correctamente.";
			accion = "generarBoletos"; // vuelve a mostrar los boletos
		} catch (PagoException e) {
			errores.put(e.getMessage(), e.getMessage());
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
		}
	}

	public void eliminar() {
		bp.begin();
		DetalleRecaudacion detalle = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class, idDetalleRecaudacion);
		if (detalle.getDetallePadre() != null) {
			DetalleRecaudacion detallePadre = detalle.getDetallePadre();
			detallePadre.setImporte(detallePadre.getImporte() + detalle.getImporte());
			bp.update(detallePadre);
		}
		detalle.setTempRecaudacion(null);
		bp.update(detalle);
		CobroPagoHandler cp = new CobroPagoHandler();
		cp.delete(detalle.getCaratula().getId(), null, idBoleto);

		guardado = true;
		accion = "generarBoletos"; // vuelve a mostrar los boletos
		boletosXML = null; // recarga los boletos
	}

	private Boleto crearBoletoIngresosVarios(Date fechaCobranza, double importe, Objetoi credito) {
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		int periodo = Calendar.getInstance().get(Calendar.YEAR);

		Boleto boleto = new Boleto();
		boleto.setFechaEmision(fechaCobranza);
		boleto.setImporte(importe);
		boleto.setObjetoi(credito);
		boleto.setFechaVencimiento(fechaCobranza);
		boleto.setTipo(Boleto.TIPO_BOLETO_CAPITAL);
		boleto.setUsuario(usuario);
		boleto.setPeriodoBoleto(new Long(periodo));
		boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
		boleto.setVerificadorBoleto(0L);

		bp.save(boleto);

		return boleto;
	}

	@Override
	public HashMap<String, String> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getIdDetalleArchivo() {
		return idDetalleArchivo;
	}

	public void setIdDetalleArchivo(Long idDetalleArchivo) {
		this.idDetalleArchivo = idDetalleArchivo;
	}

	public Long getTcomp12() {
		return tcomp12;
	}

	public void setTcomp12(Long tcomp12) {
		this.tcomp12 = tcomp12;
	}

	public String getIdBoletoList() {
		return idBoletoList;
	}

	public void setIdBoletoList(String idBoletoList) {
		this.idBoletoList = idBoletoList;
	}

	public String getIdDetalleRecaudacionList() {
		return idDetalleRecaudacionList;
	}

	public void setIdDetalleRecaudacionList(String idDetalleRecaudacionList) {
		this.idDetalleRecaudacionList = idDetalleRecaudacionList;
	}

	public String getFenv12Str() {
		return fenv12Str;
	}

	public void setFenv12Str(String fenv12Str) {
		this.fenv12Str = fenv12Str;
	}

	public Long getIdmediopago() {
		return idmediopago;
	}

	public void setIdmediopago(Long idmediopago) {
		this.idmediopago = idmediopago;
	}

	public StrutsArray getCobroPagos() {
		return cobroPagos;
	}

	public void setCobroPagos(StrutsArray cobroPagos) {
		this.cobroPagos = cobroPagos;
	}

	public Long getIdcaratula() {
		return idcaratula;
	}

	public void setIdcaratula(Long idcaratula) {
		this.idcaratula = idcaratula;
	}

	public String[] getLista() {
		return lista;
	}

	public void setLista(String[] lista) {
		this.lista = lista;
	}

	public Long getLote() {
		return lote;
	}

	public void setLote(Long lote) {
		this.lote = lote;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public String getBoletosXML() {
		return boletosXML;
	}

	public void setBoletosXML(String boletosXML) {
		this.boletosXML = boletosXML;
	}

	public Long getIdDetalleRecaudacion() {
		return idDetalleRecaudacion;
	}

	public void setIdDetalleRecaudacion(Long idDetalleRecaudacion) {
		this.idDetalleRecaudacion = idDetalleRecaudacion;
	}

	public Long getIdBoleto() {
		return idBoleto;
	}

	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}

	public boolean isGuardado() {
		return guardado;
	}

	public void setGuardado(boolean guardado) {
		this.guardado = guardado;
	}

	public String getIdFechaCobranza() {
		return idFechaCobranza;
	}

	public void setIdFechaCobranza(String idFechaCobranza) {
		this.idFechaCobranza = idFechaCobranza;
	}

	public DetalleRecaudacion getDetalleRecaudacion() {
		return new DetalleRecaudacion();
	}

	public void setDetalleRecaudacion(DetalleRecaudacion d) {
	}

	public String getMensaje() {
		return mensaje;
	}
}
