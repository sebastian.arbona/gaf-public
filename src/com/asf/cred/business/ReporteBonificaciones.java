package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.ConvenioBonificacion;

public class ReporteBonificaciones implements IProcess {

	private String forward;
	private HashMap<String, Object> errores;
	private String accion;
	private BusinessPersistance bp;

	private ConvenioBonificacion convenio;
	private Date fechaVencimientoDesde;
	private Date fechaVencimientoHasta;
	private ArrayList<BonificacionDTO> bonificaciones;

	public ReporteBonificaciones() {
		convenio = new ConvenioBonificacion();
		forward = "ReporteBonificaciones";
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return false;
		}
		if (accion.equals("listar") && validate()) {
			listar();
		}
		return errores.isEmpty();
	}

	public void listar() {

		// buscar objetos bonificados en el convenio
		// objetoi -> oibonificacion -> bonificacion -> convenio
		String qObjetos = "SELECT o.id 'o.id',b.id 'b.id' FROM ObjetoiBonificacion ob INNER JOIN Bonificacion b ON b.id = ob.idBonificacion INNER JOIN Objetoi o ON o.id = ob.idCredito WHERE b.convenio_id = :idConvenio ORDER BY b.id,o.id";
		ArrayList<Object[]> listaOisBon = (ArrayList<Object[]>) bp.createSQLQuery(qObjetos)
				.setLong("idConvenio", convenio.getId()).list();

		// buscar las cuotas que vencieron en el periodo indicado
		String qCuotas = "SELECT c.id FROM Cuota c WHERE c.fechaVencimiento BETWEEN :fechaVencimientoDesde AND :fechaVencimientoHasta AND c.credito_id = :idCredito";
		String qBonDetalle = "SELECT bd.id FROM BonDetalle bd WHERE bd.cuota_id = :idCuota ORDER BY bd.id DESC";
		ArrayList<Number> listaCtas = null;
		Number idBonDetalle = null;
		BonificacionDTO bonificacionDto = null;
		bonificaciones = new ArrayList<BonificacionDTO>();

		for (Object[] numbers : listaOisBon) {
			listaCtas = (ArrayList<Number>) bp.createSQLQuery(qCuotas)
					.setDate("fechaVencimientoDesde", fechaVencimientoDesde)
					.setDate("fechaVencimientoHasta", fechaVencimientoHasta)
					.setLong("idCredito", ((Number) numbers[0]).longValue()).list();
			// buscar el ultimo registro de bondetalle de cada cuota
			for (Number idCuota : listaCtas) {
				try {
					idBonDetalle = (Number) bp.createSQLQuery(qBonDetalle).setLong("idCuota", idCuota.longValue())
							.list().get(0);
				} catch (Exception e) {
					idBonDetalle = null;
				}
				if (idBonDetalle != null) {
					bonificacionDto = new BonificacionDTO(((Number) numbers[0]).longValue(),
							((Number) numbers[1]).longValue(), idCuota.longValue(), idBonDetalle.longValue());
					if (bonificacionDto != null) {
						bonificaciones.add(bonificacionDto);
					}
				}
			}
		}

	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		this.errores = new HashMap<String, Object>();
		if (fechaVencimientoDesde == null) {
			errores.put("generico.fechaDesde", "generico.fechaDesde");
		}
		if (fechaVencimientoHasta == null) {
			errores.put("generico.fechaHasta", "generico.fechaHasta");
		}
		if (this.fechaVencimientoDesde != null && this.fechaVencimientoHasta != null
				&& this.fechaVencimientoDesde.after(fechaVencimientoHasta)) {
			errores.put("generico.fechaDesdeHasta", "generico.fechaDesdeHasta");
		}
		return errores.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public ConvenioBonificacion getConvenio() {
		return convenio;
	}

	public void setConvenio(ConvenioBonificacion convenio) {
		this.convenio = convenio;
	}

	public Date getFechaVencimientoDesde() {
		return fechaVencimientoDesde;
	}

	public void setFechaVencimientoDesde(Date fechaVencimientoDesde) {
		this.fechaVencimientoDesde = fechaVencimientoDesde;
	}

	public String getFechaVencimientoDesdeStr() {
		return DateHelper.getString(this.getFechaVencimientoDesde());
	}

	public void setFechaVencimientoDesdeStr(String fecha) {
		this.setFechaVencimientoDesde(DateHelper.getDate(fecha));
	}

	public Date getFechaVencimientoHasta() {
		return fechaVencimientoHasta;
	}

	public void setFechaVencimientoHasta(Date fechaVencimientoHasta) {
		this.fechaVencimientoHasta = fechaVencimientoHasta;
	}

	public String getFechaVencimientoHastaStr() {
		return DateHelper.getString(this.getFechaVencimientoHasta());
	}

	public void setFechaVencimientoHastaStr(String fecha) {
		this.setFechaVencimientoHasta(DateHelper.getDate(fecha));
	}

	public ArrayList<BonificacionDTO> getBonificaciones() {
		return bonificaciones;
	}

	public void setBonificaciones(ArrayList<BonificacionDTO> bonificaciones) {
		this.bonificaciones = bonificaciones;
	}

}
