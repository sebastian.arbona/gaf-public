select c.id,
con.concepto_concepto as concepto,
c.capital - SUM(CASE WHEN cc.tipomov_id = 1 THEN cc.importe ELSE 0 END) as saldo
from Cuota c
join Ctacte cc on cc.cuota_id = c.id
join ObjetoiEstado oe on oe.objetoi_id = c.credito_id and oe.fechaHasta is null
join Concepto con on con.id = cc.asociado_id
where oe.estado_idEstado = 12 and con.concepto_concepto = 'cap'
group by c.id, con.concepto_concepto, c.capital
having  c.capital - SUM(CASE WHEN cc.tipomov_id = 1 THEN cc.importe ELSE 0 END) >= 0.01
order by c.id