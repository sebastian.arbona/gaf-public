package com.asf.cred.business;

import static com.civitas.hibernate.persona.AbstractEstado.ACUERDO_INCUMPLIDO;
import static com.civitas.hibernate.persona.AbstractEstado.ANALISIS;
import static com.civitas.hibernate.persona.AbstractEstado.CANCELADO;
import static com.civitas.hibernate.persona.AbstractEstado.FINALIZADO;
import static com.civitas.hibernate.persona.AbstractEstado.ORIGINAL_CON_ACUERDO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.hibernate.Query;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.hibernate.Moneda;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.ReportResult;
import com.asf.util.SQLHelper;
import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.cred.beans.BeanDesembolso;
import com.civitas.cred.metodos.MetodoCalculoAleman;
import com.civitas.hibernate.persona.Estado;
import com.civitas.util.ReportResultHelper;
import com.nirven.creditos.hibernate.ActualizacionIndice;
import com.nirven.creditos.hibernate.BeanCompensatorio;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.BoletoResumenDetalle;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.CategoriaSolicitanteConfig;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.CtaCteAjuste;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DetalleCuota;
import com.nirven.creditos.hibernate.EstadoInmovilizacion;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.Inmovilizaciones;
import com.nirven.creditos.hibernate.Instancia;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.PeriodoCosechaConfig;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;
import com.nirven.mailer.Mail;

public class CreditoHandler {

	private BusinessPersistance bp;
	private Double tasaCompensatorio = null;
	private List<BonDetalle> bonDetalles = new ArrayList<BonDetalle>();
	private Date segundoVencimientoCapital;
	private Date segundoVencimientoCompensatorio;

	public CreditoHandler() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	public Estado findEstado(Objetoi objetoi) {
		try {
			return objetoi.getEstadoActual().getEstado();
		} catch (NullPointerException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Estado findEstado(Objetoi objetoi, Date fecha) {
		try {
			List<Estado> oes = new ArrayList<Estado>();
			oes = bp.createQuery(
					"SELECT oe.estado FROM ObjetoiEstado oe WHERE oe.fechaHasta>= :fecha AND oe.fecha<= :fecha")
					.setParameter("fecha", fecha).list();

			if (oes.size() > 0) {
				return oes.get(0);
			}
			return null;
		} catch (NullPointerException e) {
			return null;
		}
	}

	public ObjetoiEstado findObjetoiEstado(Objetoi objetoi) {
		try {
			return objetoi.getEstadoActual();
		} catch (NullPointerException e) {
			return null;
		}
	}

	@Deprecated
	public ObjetoiIndice findTasaCompensatorio(Objetoi objetoi) {
		return findTasa(objetoi, "1");
	}

	@Deprecated
	public ObjetoiIndice findTasaPunitorio(Objetoi objetoi) {
		return findTasa(objetoi, "3");
	}

	@Deprecated
	public ObjetoiIndice findTasaMoratorio(Objetoi objetoi) {
		return findTasa(objetoi, "2");
	}

	private ObjetoiIndice findTasa(Objetoi objetoi, String codigo) {
		return objetoi.findTasa(codigo);
	}

	public double buscarValorIndice(Long idIndice) {
		Indice indice = (Indice) bp.getById(Indice.class, idIndice);
		Double resultado = indice.buscarValorIndice();
		return resultado;
	}

	public double buscarValorIndicePromedio(Long idIndice) {
		Indice indice = (Indice) bp.getById(Indice.class, idIndice);
		Double resultado = indice.buscarValorIndicePromedio();
		return resultado;
	}

	// Busca el Valor Indice para una fecha especifica
	public double buscarValorIndiceFecha(Long idIndice, Date fecha) {
		Indice indice = (Indice) bp.getById(Indice.class, idIndice);
		Double resultado = indice.buscarValorIndiceFecha(fecha);
		return resultado;
	}

	public double[] calcularTasaNeta(Long idCredito) {
		return calcularTasaNeta(idCredito, new Date());
	}

	public void calcularTasaNetaBon(Long idCredito, List<BeanCuota> cuotas) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		for (BeanCuota beanCuota : cuotas) {
			credito.calcularTasaNetaBonificacion(new Date(), beanCuota.getCompensatorio(), beanCuota.getNumero(),
					bonDetalles, false);
		}
	}

	public double[] calcularTasaNeta(Long idCredito, Date fecha) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		return credito.calcularTasaNeta(fecha, false);
	}

	public double[] calcularTasaNeta(Long idCredito, boolean calcularTasa) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		return credito.calcularTasaNeta(new Date(), calcularTasa);
	}

	public double calcularTasaNeta(Long idCredito, int numeroCuota) {
		return calcularTasaNeta(idCredito, new Date(), numeroCuota);
	}

	public double calcularTasaNeta(Long idCredito, Date fecha, int numeroCuota) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		// no necesito los detalles, por lo tanto tampoco paso los parametros para
		// calcularlos
		List<BonDetalle> bonDetalles = null;
		return credito.calcularTasaNeta(fecha, numeroCuota, new Double(0), bonDetalles, false);
	}

	@SuppressWarnings("unchecked")
	public List<BeanCuota> generarCuotas(Long idCredito, Long idDesembolso) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		ArrayList<BeanDesembolso> desembolsosBeans = new ArrayList<BeanDesembolso>();

		if (idDesembolso == null) {
			List<Desembolso> desembolsos = bp
					.createQuery("select d from Desembolso d where d.credito.id = :idCredito order by d.numero asc")
					.setLong("idCredito", idCredito).list();
			for (Desembolso d : desembolsos) {
				BeanDesembolso desembolso = new BeanDesembolso();
				Date fecha = DateHelper.resetHoraToZero(d.getFechaReal() != null ? d.getFechaReal() : d.getFecha());
				desembolso.setFecha(fecha);
				desembolso.setNumero(d.getNumero());
				desembolso.setImporte(d.getImporte());
				desembolsosBeans.add(desembolso);
			}
			// si esta vacia generar uno ficticio
			if (desembolsosBeans.isEmpty()) {
				BeanDesembolso desembolso = new BeanDesembolso();
				desembolso.setFecha(new Date());
				desembolso.setNumero(1);
				desembolso.setImporte(credito.getTotalAportes());
				desembolsosBeans.add(desembolso);
			}
		} else {
			Desembolso d = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
			if (d != null) {
				BeanDesembolso desembolso = new BeanDesembolso();
				Date fecha = DateHelper.resetHoraToZero(d.getFechaReal() != null ? d.getFechaReal() : d.getFecha());
				desembolso.setFecha(fecha);
				desembolso.setNumero(d.getNumero());
				desembolso.setImporte(d.getImporteReal() != null ? d.getImporteReal()
						: (d.getImporteSolicitado() != null ? d.getImporteSolicitado() : d.getImporte()));
				desembolsosBeans.add(desembolso);
			} else {
				BeanDesembolso desembolso = new BeanDesembolso();
				desembolso.setFecha(new Date());
				desembolso.setNumero(1);
				desembolso.setImporte(credito.getTotalAportes());
				desembolsosBeans.add(desembolso);
			}
		}

		double tasaFinal;
		ObjetoiIndice compensatorio = credito.findTasaCompensatorio();
		if (tasaCompensatorio == null) {
			tasaFinal = compensatorio.calcularTasaFinal();
		} else {
			tasaFinal = tasaCompensatorio;
		}
		double[] tasasNetas = calcularTasaNeta(idCredito);

		double[] tasasBonificacion = new double[tasasNetas.length];
		// guardo las tasas de bonificacion de cada cuota
		for (int i = 0; i < tasasNetas.length; i++) {
			tasasBonificacion[i] = tasaFinal - tasasNetas[i];
		}
		BeanCalculo bean = new BeanCalculo(
				(credito.getPrimerVencCapital() != null) ? credito.getPrimerVencCapital() : new Date(),
				(credito.getPrimerVencInteres() != null) ? credito.getPrimerVencInteres() : new Date(), tasaFinal,
				tasasBonificacion, (credito.getPlazoCapital() != null ? new Integer(credito.getPlazoCapital()) : 1),
				(credito.getPlazoCompensatorio() != null ? new Integer(credito.getPlazoCompensatorio()) : 1),
				(credito.getFrecuenciaCapital() != null) ? new Integer(credito.getFrecuenciaCapital()) : 1,
				(credito.getFrecuenciaInteres() != null) ? new Integer(credito.getFrecuenciaInteres()) : 1,
				desembolsosBeans);
		bean.setTasaCompensatorio(compensatorio);
		// cuando es acuerdo de pago
		bean.setSegundoVencimientoCapital(segundoVencimientoCapital);
		bean.setSegundoVencimientoCompensatorio(segundoVencimientoCompensatorio);
		List<BeanCuota> cuotas;
		// TODO implementar otros metodos de calculo
		MetodoCalculoAleman calculo = new MetodoCalculoAleman();
		cuotas = calculo.amortizar(bean);
		calcularTasaNetaBon(idCredito, cuotas);
		return cuotas;
	}

	public double calcularBonificacionEstimada(Long idCredito) {
		List<BeanCuota> cuotas = generarCuotas(idCredito, null);
		double totalBonificacion = 0;
		for (BeanCuota cuota : cuotas) {
			totalBonificacion += (cuota.getBonificacion().isNaN() ? 0D : cuota.getBonificacion());
		}
		return totalBonificacion;
	}

	@SuppressWarnings("unchecked")
	public List<BeanCompensatorio> calcularMoratorio(Objetoi credito, Date fechaCalculo, int diasLimite) {
		List<BeanCompensatorio> resultado = new ArrayList<BeanCompensatorio>();
		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findSinCancelar").setParameter("idCredito", credito.getId())
				.list();
		// solo usado para guardar el bean, espero que calculos posteriores no lo
		// reutilicen ya que en caso de compuesto variable no sera correcto
		ObjetoiIndice tasa = credito.findTasaMoratorio();
		Date fecha = null;
		for (Cuota cuota : cuotas) {
			if (fecha == null) {
				fecha = fechaCalculo;
				if (diasLimite > 0) {
					Date limite = DateHelper.add(cuota.getFechaVencimiento(), diasLimite, Calendar.DATE);
					if (fechaCalculo.after(limite)) {
						fecha = limite;
					}
				}
			}
			double deuda = calcularDeudaImponible(cuota, fecha);
			Date fechaDesde = buscarUltimoDebitoMoratorio(cuota);
			int dias = (int) DateHelper.getDiffDates(fechaDesde, fecha, 2);
			int diasPeriodo = dias;
			int diasTransaccion = dias;
			double monto = credito.calcularMoratorioNew(deuda, diasPeriodo, diasTransaccion, fechaDesde, fechaDesde,
					fecha);
			if (monto > 0) {
				BeanCompensatorio b = new BeanCompensatorio();
				b.setCuota(cuota);
				b.setDesdePeriodo(fechaDesde);
				b.setHastaPeriodo(fecha);
				b.setDesdeTransaccion(fechaDesde);
				b.setHastaTransaccion(fecha);
				b.setMonto(monto);
				b.setSaldoCapital(deuda);
				b.setTasa(tasa.calcularTasaFinal(fechaDesde));
				resultado.add(b);
			}
		}
		return resultado;
	}

	@SuppressWarnings("unchecked")
	public List<BeanCompensatorio> calcularPunitorio(Objetoi credito, Date fechaCalculo, int diasLimite) {
		List<BeanCompensatorio> resultado = new ArrayList<BeanCompensatorio>();
		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findSinCancelar").setParameter("idCredito", credito.getId())
				.list();
		// solo usado para guardar el bean, espero que calculos posteriores no lo
		// reutilicen ya que en caso de compuesto variable no sera correcto
		ObjetoiIndice tasa = credito.findTasaPunitorio();
		Date fecha = null;
		for (Cuota cuota : cuotas) {
			if (fecha == null) {
				fecha = fechaCalculo;
				if (diasLimite > 0) {
					Date limite = DateHelper.add(cuota.getFechaVencimiento(), diasLimite, Calendar.DATE);
					if (fechaCalculo.after(limite)) {
						fecha = limite;
					}
				}
			}
			double deuda = calcularDeudaImponible(cuota, fecha);
			Date fechaDesde = buscarUltimoDebitoPunitorio(cuota);
			int dias = (int) DateHelper.getDiffDates(fechaDesde, fecha, 2);
			int diasPeriodo = dias;
			int diasTransaccion = dias;
			double monto = credito.calcularPunitorioNew(deuda, diasPeriodo, diasTransaccion, fechaDesde, fechaDesde,
					fecha);
			if (monto > 0) {
				BeanCompensatorio b = new BeanCompensatorio();
				b.setCuota(cuota);
				b.setDesdePeriodo(fechaDesde);
				b.setHastaPeriodo(fecha);
				b.setDesdeTransaccion(fechaDesde);
				b.setHastaTransaccion(fecha);
				b.setMonto(monto);
				b.setSaldoCapital(deuda);
				b.setTasa(tasa.calcularTasaFinal(fechaDesde));
				resultado.add(b);
			}
		}
		return resultado;
	}

	public Date buscarUltimoDebitoMoratorio(Cuota cuota) {
		return buscarUltimoDebito(cuota, Concepto.CONCEPTO_MORATORIO);
	}

	public Date buscarUltimoDebitoPunitorio(Cuota cuota) {
		return buscarUltimoDebito(cuota, Concepto.CONCEPTO_PUNITORIO);
	}

	public Date buscarUltimoDebito(Cuota cuota, String concepto) {
		Date ultimoDebito = (Date) bp.createQuery(
				"SELECT MAX(c.fechaGeneracion) FROM Ctacte c WHERE c.facturado.concepto.concepto LIKE :concepto AND c.cuota.id = :idCuota AND c.tipomov.abreviatura = :deb")
				.setParameter("concepto", concepto).setParameter("idCuota", cuota.getId())
				.setParameter("deb", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		if (ultimoDebito != null) {
			return ultimoDebito;
		}
		return cuota.getFechaVencimiento();
	}

	@SuppressWarnings("unchecked")
	public double calcularMoratorioAcuerdoPago(Objetoi credito, Date fecha) {
		Query query = bp.getNamedQuery("Ctacte.findByConceptosCredito");
		/*
		 * Ticket 10036 List<Ctacte> ccs = query.setParameterList("conceptos", new
		 * String[] { Concepto.CONCEPTO_CAPITAL, Concepto.CONCEPTO_COMPENSATORIO,
		 * Concepto.CONCEPTO_GASTOS, Concepto.CONCEPTO_GASTOS_A_RECUPERAR,
		 * Concepto.CONCEPTO_MULTAS, Concepto.CONCEPTO_BONIFICACION
		 * }).setParameter("idCredito", credito.getId()).list();
		 */
		List<Ctacte> ccs = query
				.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_CAPITAL,
						Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_BONIFICACION })
				.setParameter("idCredito", credito.getId()).list();
		double deudaImponible = calcularSaldo(ccs);
		List<Ctacte> ctacteMorat = query.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_MORATORIO })
				.setParameter("idCredito", credito.getId()).list();
		Date fechaUltimoDebitoMoratorio = null;
		for (Ctacte cc : ctacteMorat) {
			if (cc.isDebito()) {
				fechaUltimoDebitoMoratorio = cc.getFechaGeneracion();
			}
		}
		if (fechaUltimoDebitoMoratorio == null) {
			fechaUltimoDebitoMoratorio = (Date) bp
					.createSQLQuery("select max(p.fechaPago) from Pagos p where p.objetoi_id = :id")
					.setLong("id", credito.getId()).uniqueResult();
			// Pedido por Armando. Tener en cuenta la fecha de la caducidad de
			// plazo
			Date fechaCaducidadPago = (Date) bp.createSQLQuery(
					"select max(c.fechaGeneracion) from CtaCte c where c.objetoi_id = :id and tipoConcepto = :tipoconcepto")
					.setLong("id", credito.getId()).setString("tipoconcepto", "104").uniqueResult();
			if (fechaCaducidadPago != null && fechaUltimoDebitoMoratorio != null) {
				if (fechaCaducidadPago.compareTo(fechaUltimoDebitoMoratorio) > 0) {
					fechaUltimoDebitoMoratorio = fechaCaducidadPago;
				}
			} else {
				if (fechaUltimoDebitoMoratorio == null) {
					fechaUltimoDebitoMoratorio = (Date) bp.createSQLQuery(
							"select min(c.fechaVencimiento) from Cuota c where c.estado = '1' and c.credito_id = :id")
							.setLong("id", credito.getId()).uniqueResult();
				}
			}
		}
		if (fechaUltimoDebitoMoratorio == null) {
			return 0;
		}
		int diasMoratorio = (int) DateHelper.getDiffDates(fechaUltimoDebitoMoratorio, fecha, 2);
		int diasPeriodo = diasMoratorio;
		int diasTransaccion = diasMoratorio;
		double montoMoratorio = credito.calcularMoratorioNew(deudaImponible, diasPeriodo, diasTransaccion,
				fechaUltimoDebitoMoratorio, fechaUltimoDebitoMoratorio, fecha);
		return montoMoratorio;
	}

	@SuppressWarnings("unchecked")
	public double calcularPunitorioAcuerdoPago(Objetoi credito, Date fecha) {
		Query query = bp.getNamedQuery("Ctacte.findByConceptosCredito");
		List<Ctacte> ccs = query
				.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_CAPITAL,
						Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_BONIFICACION })
				.setParameter("idCredito", credito.getId()).list();
		double deudaImponible = calcularSaldo(ccs);
		List<Ctacte> ctactePunit = query.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_PUNITORIO })
				.setParameter("idCredito", credito.getId()).list();
		Date fechaUltimoDebitoPunitorio = null;
		for (Ctacte cc : ctactePunit) {
			if (cc.isDebito()) {
				fechaUltimoDebitoPunitorio = cc.getFechaGeneracion();
			}
		}
		if (fechaUltimoDebitoPunitorio == null) {
			fechaUltimoDebitoPunitorio = (Date) bp
					.createSQLQuery("select max(p.fechaPago) from Pagos p where p.objetoi_id = :id")
					.setLong("id", credito.getId()).uniqueResult();
			Date fechaCaducidadPago = (Date) bp.createSQLQuery(
					"select max(c.fechaGeneracion) from CtaCte c where c.objetoi_id = :id and tipoConcepto = :tipoconcepto")
					.setLong("id", credito.getId()).setString("tipoconcepto", "104").uniqueResult();
			if (fechaCaducidadPago != null && fechaUltimoDebitoPunitorio != null) {
				if (fechaCaducidadPago.compareTo(fechaUltimoDebitoPunitorio) > 0) {
					fechaUltimoDebitoPunitorio = fechaCaducidadPago;
				}
			} else {
				if (fechaUltimoDebitoPunitorio == null) {
					fechaUltimoDebitoPunitorio = (Date) bp.createSQLQuery(
							"select min(c.fechaVencimiento) from Cuota c where c.estado = '1' and c.credito_id = :id")
							.setLong("id", credito.getId()).uniqueResult();
				}
			}
		}
		if (fechaUltimoDebitoPunitorio == null) {
			return 0;
		}
		int diasPunitorio = (int) DateHelper.getDiffDates(fechaUltimoDebitoPunitorio, fecha, 2);
		int diasPeriodo = diasPunitorio;
		int diasTransaccion = diasPunitorio;
		double montoPunitorio = credito.calcularMoratorioNew(deudaImponible, diasPeriodo, diasTransaccion,
				fechaUltimoDebitoPunitorio, fechaUltimoDebitoPunitorio, fecha);
		return montoPunitorio;
	}

	@SuppressWarnings("unchecked")
	public List<Ctacte> listarCtaCtePorTipoMovimiento(Cuota cuota, Tipomov tipoMovimiento) {
		List<Ctacte> ctes = bp
				.createQuery("SELECT cta FROM Ctacte cta WHERE cta.cuota = :cuota AND cta.tipomov = :tipoMov")
				.setParameter("cuota", cuota).setParameter("tipoMov", tipoMovimiento).list();
		return ctes;
	}

	@SuppressWarnings("unchecked")
	public List<Ctacte> listarCtaCtePorTipoMovimiento(Objetoi credito, Tipomov tipoMovimiento) {
		List<Ctacte> ctes = bp
				.createQuery("SELECT cta FROM Ctacte cta WHERE cta.cuota.credito = :credito AND cta.tipomov = :tipoMov")
				.setParameter("credito", credito).setParameter("tipoMov", tipoMovimiento).list();
		return ctes;
	}

	public List<Ctacte> listarCtaCteDebito(Objetoi credito) {
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		return listarCtaCtePorTipoMovimiento(credito, tipoDebito);
	}

	public List<Ctacte> listarCtaCteCredito(Objetoi credito) {
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		return listarCtaCtePorTipoMovimiento(credito, tipoCredito);
	}

	public List<Ctacte> listarCtaCteDebito(Cuota cuota) {
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		return listarCtaCtePorTipoMovimiento(cuota, tipoDebito);
	}

	public List<Ctacte> listarCtaCteCredito(Cuota cuota) {
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		return listarCtaCtePorTipoMovimiento(cuota, tipoCredito);
	}

	/**
	 * Calcula la deuda de capital, compensatorio, gastos y multas, imponible para
	 * calcular moratorio y punitorio.
	 * 
	 * @param cuota
	 * @return
	 */
	public double calcularDeudaImponible(Cuota cuota) {
		return calcularDeudaImponible(cuota, new Date());
	}

	public double calcularDeudaImponible(Cuota cuota, Date hasta) {
		List<Ctacte> ccs = listarCtaCteConceptos(cuota, hasta, Concepto.CONCEPTO_CAPITAL,
				Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_BONIFICACION);
		double saldo = 0;
		for (Ctacte cc : ccs) {
			if (cc.isDebito()) {
				saldo += cc.getImporte();
			} else {
				saldo -= cc.getImporte();
			}
		}
		return saldo;
	}

	public double calcularDeuda(Objetoi objetoi) {
		List<Ctacte> listaDebitos = listarCtaCteDebito(objetoi);
		List<Ctacte> listaCreditos = listarCtaCteCredito(objetoi);
		double debito = 0.0;
		for (Ctacte ctaCteDebito : listaDebitos) {
			debito += ctaCteDebito.getImporte();
		}
		double credito = 0.0;
		for (Ctacte ctaCteCredito : listaCreditos) {
			credito += ctaCteCredito.getImporte();
		}
		return debito - credito;
	}

	public double calcularPagos(Cuota cuota) {
		List<Ctacte> listaCreditos = listarCtaCteCredito(cuota);

		double credito = 0.0;

		for (Ctacte ctaCteCredito : listaCreditos) {
			credito += ctaCteCredito.getImporte();
		}

		return credito;
	}

	public Estado buscarEstado() {
		return (Estado) bp.createQuery("SELECT e FROM Estado e where e.nombreEstado = :nombre")
				.setParameter("nombre", "EJECUCION").uniqueResult();

	}

	public void ejecutarDesembolso(Desembolso desembolso, Double tasaFinal) {
		tasaCompensatorio = tasaFinal;
		ejecutarDesembolso(desembolso.getCredito(), desembolso);
	}

	public double calcularPagos(Objetoi credito) {
		List<Ctacte> listaCreditos = listarCtaCteCredito(credito);

		double total = 0.0;

		for (Ctacte ctaCteCredito : listaCreditos) {
			total += ctaCteCredito.getImporte();
		}

		return total;
	}

	public boolean completarDesembolso(Long idDesembolso, Desembolso desembolso) {
		Desembolso existente = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		if (!verificarImporteDesembolsos(existente.getCredito())) {
			return false;
		}
		existente.setObservacion(desembolso.getObservacion());
		existente.setCuentaInterbanking(null);
		existente.setEstado("2");// Realizado
		existente.setFechaReal(new Date());
		existente.setImporteReal(desembolso.getImporteReal());
		existente.setImporteGastos(desembolso.getImporteGastos());
		existente.setImporteMultas(desembolso.getImporteMultas());
		bp.saveOrUpdate(existente);
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean verificarImporteDesembolsos(Objetoi credito) {
		List<Desembolso> listaDesembolso = bp
				.createQuery("select d from Desembolso d where d.credito = :credito order by d.numero")
				.setParameter("credito", credito).list();
		if (listaDesembolso != null && !listaDesembolso.isEmpty()) {
			if (sumarMonto(listaDesembolso) > credito.getFinanciamiento()) {
				return false;
			}
		}
		return true;
	}

	private int sumarMonto(List<Desembolso> listaDesembolso) {
		int suma = 0;
		for (Desembolso desembolso : listaDesembolso) {
			suma += desembolso.getImporte();
		}
		return suma;
	}

	@SuppressWarnings("unchecked")
	public void ejecutarDesembolso(Objetoi credito, Desembolso desembolso) {
		Long idCredito = credito.getId();
		Long idDesembolso = desembolso.getId();
		boolean acuerdoPago = credito.getLinea().isLineaAcuerdoPago();
		Cuota cuotaGtos = null;
		Number ultimaCuotaNoAfectada = (Number) bp.createQuery(
				"SELECT max(c.numero) FROM Cuota c WHERE c.credito.id = :idCredito AND c.fechaVencimiento <= :fecha")
				.setParameter("idCredito", idCredito).setParameter("fecha", desembolso.getFechaReal()).uniqueResult();
		// cantidad total de cuotas
		int cantidadCuotas = credito.getPlazoCompensatorio();
		if (ultimaCuotaNoAfectada != null) {
			// cantidad de cuotas que quedan luego de la fecha de desembolso
			cantidadCuotas -= ultimaCuotaNoAfectada.intValue();
		}
		// temporal hasta definir si los gtos se guardan en la Cuota
		double gastoPorCuota = DoubleHelper.redondear(desembolso.getImporteGastos() / cantidadCuotas);
		gastoPorCuota = 0.0;

		double multasPorCuota = DoubleHelper.redondear(desembolso.getImporteMultas() / cantidadCuotas);
		double importeDesembolso = 0;
		boolean primerDesembolso = false;

		List<BeanCuota> beanCuotas;
		if (idDesembolso != null) {
			desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
			if (desembolso.getEjecutado() == null || desembolso.getEjecutado().booleanValue() == false) {
				beanCuotas = generarCuotas(idCredito, idDesembolso);
			} else {
				beanCuotas = new ArrayList<BeanCuota>();
			}
			importeDesembolso = desembolso.getImporteReal() != null ? desembolso.getImporteReal()
					: desembolso.getImporte();
			primerDesembolso = desembolso.getNumero() == 1;
		} else {
			beanCuotas = generarCuotas(idCredito, idDesembolso);
		}

		double importeAsignado = 0;
		for (BeanCuota beanCuota : beanCuotas) {
			List<Cuota> cuotasNro = bp.getNamedQuery("Cuota.findByNumero").setLong("idCredito", idCredito)
					.setInteger("nroCuota", beanCuota.getNumero()).list();
			Cuota cuota = null;
			if (!cuotasNro.isEmpty()) {
				// ya existe
				cuota = cuotasNro.get(0);
			} else {
				// no se han generado cuotas
				cuota = new Cuota();
			}
			DetalleCuota detalle = null;
			// si se genera por primera vez o si la cuota ya esta generada y
			// todavia no vence actualizar la cuota
			if (cuota.getFechaVencimiento() == null || desembolso.getFechaReal() == null
					|| cuota.getFechaVencimiento().after(desembolso.getFechaReal())) {
				cuota.setCredito(credito);
				cuota.setFechaVencimiento(beanCuota.getFechaVencimiento());
				cuota.setFechaGeneracion(desembolso.getFechaReal());
				cuota.setNumero(beanCuota.getNumero());
				cuota.setEstado(Cuota.SIN_CANCELAR);

				cuota.setCapital(((cuota.getCapital() != null) ? cuota.getCapital() : 0) + beanCuota.getCapital());
				cuota.setCompensatorio(((cuota.getCompensatorio() != null) ? cuota.getCompensatorio() : 0)
						+ beanCuota.getCompensatorio());
				cuota.setGastos(gastoPorCuota);
				cuota.setMoratorio(
						((cuota.getMoratorio() != null) ? cuota.getMoratorio() : 0) + beanCuota.getMoratorio());
				cuota.setPunitorio(
						((cuota.getPunitorio() != null) ? cuota.getPunitorio() : 0) + beanCuota.getPunitorio());
				cuota.setBonificacion(((cuota.getBonificacion() != null) ? cuota.getBonificacion() : 0)
						+ beanCuota.getBonificacion());
				cuota.setMultas(multasPorCuota);

				if (desembolso.getFechaReal() == null) {
					desembolso.setFechaReal(new Date());
				}

				Tipomov tipoMovimientoDB = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
						.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();

				Usuario usuario = (Usuario) bp.getById(Usuario.class,
						SessionHandler.getCurrentSessionHandler().getCurrentUser());
				int periodo = Calendar.getInstance().get(Calendar.YEAR);

				// guardar lo agregado por este desembolso
				detalle = new DetalleCuota();
				detalle.setCuota(cuota);
				detalle.setDesembolso(desembolso.getNumero());
				detalle.setDesembolsado(desembolso.getImporteReal());
				detalle.setDiasPeriodo(beanCuota.getDiasPeriodo());
				detalle.setDiasTransaccion(beanCuota.getDiasTransaccion());
				detalle.setTasaAmortizacion(beanCuota.getTasaAmortizacion());
				detalle.setTasaBonificacion(beanCuota.getTasaBonificacion());
				detalle.setVencimiento(beanCuota.getFechaVencimiento());
				detalle.setVencimientoAnterior(beanCuota.getFechaVencimientoAnterior());
				detalle.setCapital(beanCuota.getCapital());
				detalle.setSaldoCapital(beanCuota.getSaldoCapital());
				detalle.setCompensatorio(cuota.getCompensatorio() != null ? cuota.getCompensatorio() : 0);
				detalle.setBonificacion(beanCuota.getBonificacion());
				detalle.setMoratorio(beanCuota.getMoratorio());
				detalle.setPunitorio(beanCuota.getPunitorio());
				detalle.setTasaAmortizacion(beanCuota.getTasaAmortizacion());

				bp.saveOrUpdate(cuota);

				if (cuota.getFechaVencimiento().after(desembolso.getFechaReal()) && cuotaGtos == null) {
					cuotaGtos = cuota;
				}

				asignarBolconSinCuota(idCredito, cuota);
				asignarCtaCteSinCuota(idCredito, cuota);
				// Creo el periodo
				// numerador debe incluir el periodo
				String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
				long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);

				// crear nuevo registro de ctacte
				long itemCtaCte = 1;
				Concepto concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL);

				Ctacte cc = new Ctacte();
				cc.setImporte(detalle.getCapital());
				cc.redondear();
				double restaPorAsignar = importeDesembolso - importeAsignado;
				if (restaPorAsignar < cc.getImporte()) {
					cc.setImporte(restaPorAsignar);
					cc.redondear();
				}

				importeAsignado += cc.getImporte();

				cc.setAsociado(concepto);
				cc.setFacturado(concepto);
				cc.setTipomov(tipoMovimientoDB);
				cc.setTipoMovimiento("cuota");
				cc.setFechaGeneracion(new java.sql.Date(desembolso.getFechaReal().getTime()));
				cc.setFechaVencimiento(new java.sql.Date(cuota.getFechaVencimiento().getTime()));
				cc.setCuota(cuota);
				cc.setUsuario(usuario);
				cc.setDesembolso(desembolso);
				cc.setDetalle(!acuerdoPago ? "Desembolso" : "Deuda consolidada");

				CtacteKey ck = new CtacteKey();
				ck.setPeriodoCtacte(new Long(periodo));
				ck.setObjetoi(cuota.getCredito());
				// TODO: reemplazar con Digito Verificador
				ck.setVerificadorCtacte(0L);
				cc.setId(ck);

				cc.getId().setMovimientoCtacte(movimientoCtaCte);
				cc.getId().setItemCtacte(itemCtaCte++);

				CreditoHandler.cargarCotizacion(cc);

				cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_DESEMBOLSO);

				bp.save(cc);
				bp.getCurrentSession().flush();

				if (cuota.getEmision() != null) {
					// crear cuenta corriente compensatorio
					concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_COMPENSATORIO);
					Ctacte ccCompensatorio = new Ctacte();
					double importe = DoubleHelper.redondear(beanCuota.getCompensatorio().doubleValue());
					ccCompensatorio.setImporte(importe);
					ccCompensatorio.redondear();
					ccCompensatorio.setAsociado(concepto);
					ccCompensatorio.setFacturado(concepto);
					ccCompensatorio.setTipomov(tipoMovimientoDB);
					ccCompensatorio.setTipoMovimiento("Cuota");
					ccCompensatorio.setFechaGeneracion(cuota.getFechaVencimiento());
					ccCompensatorio.setFechaVencimiento(cuota.getFechaVencimiento());
					ccCompensatorio.setCuota(cuota);
					ccCompensatorio.setUsuario(usuario);
					ccCompensatorio.setDetalle("Compensatorio por desembolso post facturaci�n");

					// cuenta corriente key
					movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
					CtacteKey ckCompensatorio = new CtacteKey();
					ckCompensatorio.setPeriodoCtacte(new Long(periodo));
					ckCompensatorio.setObjetoi(cuota.getCredito());
					ckCompensatorio.setVerificadorCtacte(0L);
					ckCompensatorio.setMovimientoCtacte(movimientoCtaCte);
					ckCompensatorio.setItemCtacte(1L);
					ccCompensatorio.setId(ckCompensatorio);

					CreditoHandler.cargarCotizacion(ccCompensatorio, desembolso.getFechaReal());
					ccCompensatorio.setTipoConcepto(Ctacte.TIPO_CONCEPTO_FACTURACION_DESEMBOLSO);

					bp.save(ccCompensatorio);

					// crear boleto factura
					Boleto boletoFactura = new Boleto();
					boletoFactura.setObjetoi(credito);
					boletoFactura.setTipo(Boleto.TIPO_BOLETO_FACTURA);
					boletoFactura.setUsuario(usuario);
					boletoFactura.setPeriodoBoleto(new Long(periodo));
					String numerador = boletoFactura.getNumerador();
					boletoFactura.setNumeroBoleto(Numerador.getNext(numerador));
					boletoFactura.setVerificadorBoleto(0L);
					boletoFactura.setImporte(beanCuota.getCompensatorio());
					boletoFactura.setFechaEmision(ccCompensatorio.getFechaGeneracion());
					boletoFactura.setFechaVencimiento(ccCompensatorio.getFechaGeneracion());
					bp.save(boletoFactura);
					ccCompensatorio.setBoleto(boletoFactura);
					bp.update(ccCompensatorio);

					// bolcon key
					BolconKey bck = new BolconKey();
					bck.setBoleto(boletoFactura);
					bck.setPeriodoCtacte(ckCompensatorio.getPeriodoCtacte());
					bck.setMovimientoCtacte(ckCompensatorio.getMovimientoCtacte());
					bck.setVerificadorCtacte(ckCompensatorio.getVerificadorCtacte());
					bck.setItemCtacte(ckCompensatorio.getItemCtacte());

					// crear bolcon
					Bolcon bolconCompensatorio = new Bolcon();
					bolconCompensatorio.setId(bck);
					bolconCompensatorio.setFacturado(concepto);
					bolconCompensatorio.setOriginal(concepto);
					double importeRedondeado = ccCompensatorio.getImporte();
					importeRedondeado = DoubleHelper.redondear(importeRedondeado);
					bolconCompensatorio.setImporte(importeRedondeado);
					bolconCompensatorio.setCuota(cuota);
					bolconCompensatorio.setTipomov(tipoMovimientoDB);
					bp.save(bolconCompensatorio);

					// Creaci�n del boleto resumen
					Boleto boletoResumen = new Boleto();
					boletoResumen.setFechaVencimiento(cuota.getFechaVencimiento());
					boletoResumen.setTipo(Boleto.TIPO_BOLETO_RESUMEN);
					boletoResumen.setUsuario(usuario);
					boletoResumen.setNumeroCuota(cuota.getNumero());
					boletoResumen.setObjetoi(credito);
					boletoResumen.setPeriodoBoleto((long) periodo);
					boletoResumen.setNumeroBoleto(Numerador.getNext(boletoResumen.getNumerador()));
					boletoResumen.setVerificadorBoleto(0L);
					bp.save(boletoResumen);

					// buscar Ctactes para asociar a este boleto resumen capital
					bp.createSQLQuery(
							"update Ctacte set idBoletoResumen = :idBoletoResumen where cuota_id = :idCuota and idBoletoResumen is null and facturado_id in (select id from Concepto where concepto_concepto = :capital) and tipoMovimiento='cuota'")
							.setParameter("idBoletoResumen", boletoResumen.getId())
							.setParameter("idCuota", cuota.getId()).setParameter("capital", Concepto.CONCEPTO_CAPITAL)
							.executeUpdate();

					// solo actualizar el compensatorio generado aca
					ccCompensatorio.setIdBoletoResumen(boletoResumen.getId());
					bp.update(ccCompensatorio);

					// Parte B Recalcular a la fecha
					crearParteBBoletoResumen(cuota, boletoResumen);

					// Emision del correo con el Boleto de Pago
					if ((credito.getPersona().getEmailPrincipal() != null
							&& !credito.getPersona().getEmailPrincipal().trim().isEmpty())) {
						BoletosDePago boletosDePago = new BoletosDePago();
						boletosDePago.setBoleto(boletoResumen);
						boletosDePago.setObjetoi(credito);
						boletosDePago.imprimirBoletos();
						try {
							byte[] adj = ReportResultHelper.getReportePdf((ReportResult) boletosDePago.getResult());
							// #Plantilla 14: Configuraci�n Plantilla Boleto de Pago
							ConfiguracionNotificacion cn = (ConfiguracionNotificacion) SessionHandler
									.getCurrentSessionHandler().getBusinessPersistance()
									.getById(ConfiguracionNotificacion.class, new Long(14));
							List<String> recipients = new ArrayList<>();
							recipients.add("to:" + credito.getPersona().getEmailPrincipal());
							recipients.add("cco:pagos@ftyc.gob.ar");
							recipients.add("replyTo:pagos@ftyc.gob.ar");
							Mail.sendMailPdfTemplate(recipients, cn.getAsunto(), "mails/templateMail.html",
									cn.getTexto(), "recibo", adj);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			if (detalle != null) {
				bp.save(detalle);
			}
		}

		if (desembolso.getImporteGastos() != null && desembolso.getImporteGastos() != 0) {
			// solo se impacta cuando la moneda es Pesos
			if (credito.getLinea().getMoneda_id() != null && credito.getLinea().getMoneda_id().longValue() == 1L) {
				Usuario usuario = (Usuario) bp.getById(Usuario.class,
						SessionHandler.getCurrentSessionHandler().getCurrentUser());
				int periodo = Calendar.getInstance().get(Calendar.YEAR);
				String numeradorCtaCteGtos = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
				Tipomov tipoMovimientoCR = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
						.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
				long movimientoCtaCteGtosCred = Numerador.getNext(numeradorCtaCteGtos);

				List<Cuota> cuotasImpagas = bp.createSQLQuery(
						"SELECT * FROM Cuota C WHERE C.credito_id = :idObjetoi AND C.estado = 1 ORDER BY C.numero")
						.addEntity(Cuota.class).setLong("idObjetoi", credito.getId()).list();

				if (cuotasImpagas.isEmpty()) {
					cuotaGtos = credito.getUltimaCuota();
				} else {
					cuotaGtos = cuotasImpagas.get(0);
				}

				if (cuotaGtos != null) {
					// crea entrada en CtaCte para Gastos de tipo credito
					Ctacte ctacteGastos = credito.crearCtacte(Concepto.CONCEPTO_GASTOS, desembolso.getImporteGastos(),
							desembolso.getFechaReal(), null, credito, cuotaGtos, Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
					ctacteGastos.setTipomov(tipoMovimientoCR);
					ctacteGastos.setTipoMovimiento("Pago");
					ctacteGastos.setUsuario(usuario);
					ctacteGastos.getId().setMovimientoCtacte(movimientoCtaCteGtosCred);
					ctacteGastos.getId().setItemCtacte(1L);
					ctacteGastos.setContabiliza(false);
					bp.save(ctacteGastos);
				}
			}
		}
		if (idDesembolso != null) {
			desembolso.setEjecutado(true);
			bp.update(desembolso);

			if (primerDesembolso) {
				// insertar estado alta contable
				List<ObjetoiGarantia> ogs = bp
						.createQuery("select og from ObjetoiGarantia og where og.objetoi.id = :id and og.baja is null")
						.setParameter("id", idCredito).list();
				if (ogs != null && !ogs.isEmpty()) {
					for (ObjetoiGarantia og : ogs) {
						if (og != null) {
							if (!acuerdoPago && og.getEstadoEntStr() != null
									&& !og.getEstadoEntStr().equals("ALTA_CONTABLE")) {
								Double importe = null;
								GarantiaEstado nuevoEstado = null;
								if ((og.getGarantia().getTipo().getId()) == 6) {
									importe = desembolso.getImporteReal() * og.getGarantia().getTipo().getAforo();
									nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.ALTA_CONTABLE,
											desembolso.getFechaReal(), importe);
									nuevoEstado.setObjetoiGarantia(og);
								} else {
									nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.ALTA_CONTABLE,
											desembolso.getFechaReal());
									nuevoEstado.setObjetoiGarantia(og);
									nuevoEstado.determinarImporte();
								}
								bp.save(nuevoEstado);
							}
						}
					}
				}
			}
		}
	}

	public Ctacte crearCtaCte(Cuota cuota, String tipoConcepto, Tipomov tipoMovimiento, double importe,
			String detalle) {
		Ctacte cc = new Ctacte();
		Concepto concepto = Concepto.buscarConcepto(tipoConcepto);
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		cc.setTipomov(tipoMovimiento);
		cc.setAsociado(concepto);
		cc.setFacturado(concepto);
		cc.setDetalle(detalle);
		cc.setImporte(importe);
		cc.redondear();
		cc.setFechaGeneracion(new java.sql.Date(System.currentTimeMillis()));
		cc.setCuota(cuota);
		cc.setFechaGeneracion(new java.sql.Date(System.currentTimeMillis()));
		cc.setFechaVencimiento(new java.sql.Date(System.currentTimeMillis()));
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		// contador desde 1
		long itemCtaCte = 1;
		CtacteKey ck = new CtacteKey();
		ck.setItemCtacte(itemCtaCte);
		ck.setMovimientoCtacte(movimientoCtaCte);
		ck.setPeriodoCtacte(new Long(periodo));
		ck.setObjetoi(cuota.getCredito());
		// TODO: reemplazar con Digito Verificador
		ck.setVerificadorCtacte(0L);
		cc.setId(ck);
		cc.setCuota(cuota);
		CreditoHandler.cargarCotizacion(cc);
		bp.save(cc);
		return cc;
	}

	public Ctacte crearCtaCte(Cuota cuota, String tipoConcepto, Tipomov tipoMovimiento, double importe) {

		return crearCtaCte(cuota, tipoConcepto, tipoMovimiento, importe, null);
	}

	public Instancia findInstancia(Objetoi objetoi) {
		try {
			return (Instancia) bp
					.createQuery("select c.instancia from InstanciaObjetoi c where c.objetoi.id = :idObjetoi and "
							+ "c.fecha = (select max(e.fecha) from InstanciaObjetoi e where e.objetoi.id = :idObjetoi)")
					.setLong("idObjetoi", objetoi.getId()).uniqueResult();

		} catch (NonUniqueResultException nue) {
			return null;
		} catch (NoResultException nre) {
			return null;
		}
	}

	/**
	 * Calcula el saldo de deuda de la cuota:<br/>
	 * Capital + Compensatorio + Multas + Gastos<br/>
	 * tomando solo las cuotas hasta la fecha indicada.
	 * 
	 * @param cuota
	 * @param hastaFecha
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public double calcularSaldoDeuda(Cuota cuota, Date hastaFecha) {
		List<Ctacte> ccs = bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha").setParameter("idCuota", cuota.getId())
				.setParameter("fecha", hastaFecha)
				.setParameterList("conceptos", new String[] { Concepto.CONCEPTO_CAPITAL,
						Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_GASTOS, Concepto.CONCEPTO_MORATORIO })
				.list();

		return calcularSaldo(ccs);
	}

	public double calcularSaldo(List<Ctacte> ccs) {
		double saldo = 0;
		for (Ctacte cc : ccs) {
			if (cc.isDebito()) {
				saldo += cc.getImporte();
			} else {
				saldo -= cc.getImporte();
			}
		}
		return saldo;
	}

	@SuppressWarnings("unchecked")
	public double calcularSaldoCapital(Cuota cuota) {
		Double capitalDevengado = (Double) bp.getNamedQuery("Cuota.findCapitalDevengadoEstimado")
				.setLong("idCredito", cuota.getCredito().getId()).setInteger("nroCuota", cuota.getNumero())
				.uniqueResult();
		List<Ctacte> ccs = bp.getNamedQuery("Ctacte.findByConceptoCredito")
				.setParameter("concepto", Concepto.CONCEPTO_CAPITAL)
				.setParameter("idCredito", cuota.getCredito().getId()).list();
		// total de debitos
		Double totalDebitos = 0.0;
		Double saldo = 0.0;
		for (Ctacte cc : ccs) {
			if (cc.isDebito()) {
				totalDebitos += cc.getImporte();
				saldo += cc.getImporte();
			} else {
				saldo -= cc.getImporte();
			}
		}
		double saldoEstimado = totalDebitos - ((capitalDevengado != null) ? capitalDevengado : 0);
		return Math.min(saldo, saldoEstimado);
	}

	/**
	 * Calcula la suma de todos los debitos de capital, y resta todos los creditos
	 * que sean menores a
	 * 
	 * @param credito
	 * @param hasta
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public double calcularSaldoCapital(Cuota cuota, Date hasta) {
		Double capitalDevengado = (Double) bp.getNamedQuery("Cuota.findCapitalDevengadoEstimado")
				.setLong("idCredito", cuota.getCredito().getId()).setInteger("nroCuota", cuota.getNumero())
				.uniqueResult();
		List<Ctacte> ccs = bp.getNamedQuery("Ctacte.findByConceptoCreditoFecha")
				.setParameter("concepto", Concepto.CONCEPTO_CAPITAL)
				.setParameter("idCredito", cuota.getCredito().getId()).setParameter("fecha", hasta).list();
		// total de debitos
		Double totalDebitos = 0.0;
		Double saldo = 0.0;
		for (Ctacte cc : ccs) {
			if (cc.isDebito()) {
				totalDebitos += cc.getImporte();
				saldo += cc.getImporte();
			} else {
				saldo -= cc.getImporte();
			}
		}
		double saldoEstimado = totalDebitos - ((capitalDevengado != null) ? capitalDevengado : 0);
		double saldoCC = calcularSaldo(ccs);
		return Math.min(saldoCC, saldoEstimado);
	}

	@SuppressWarnings("unchecked")
	public List<Ctacte> listarCtaCteConceptos(Cuota cuota, Date hasta, String... conceptos) {
		return bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha").setParameterList("conceptos", conceptos)
				.setParameter("idCuota", cuota.getId()).setParameter("fecha", hasta).list();
	}

	@SuppressWarnings("unchecked")
	public List<Ctacte> listarCtaCteConceptos(Objetoi objetoi, String... conceptos) {
		return bp.getNamedQuery("Ctacte.findByConceptosCredito").setParameterList("conceptos", conceptos)
				.setParameter("idCredito", objetoi.getId()).list();
	}

	@SuppressWarnings("unchecked")
	public List<Ctacte> listarCtaCteConceptos(Cuota cuota, String tipoMov, Date hasta, String... conceptos) {
		return bp.getNamedQuery("Ctacte.findByConceptosCuotaFechaTipomov").setParameterList("conceptos", conceptos)
				.setParameter("idCuota", cuota.getId()).setParameter("fecha", hasta).setParameter("tipomov", tipoMov)
				.list();
	}

	/**
	 * en esta version, cuando la ctacte no tiene boleto(por migracion) la consulta
	 * falla public List<Ctacte> listarCtaCteConceptosNoAnulados(Cuota cuota, String
	 * tipoMov, Date hasta, String... conceptos) {
	 * 
	 * return bp.getNamedQuery("Ctacte.findByConceptosCuotaFechaTipomovNoAnulados")
	 * .setParameterList("conceptos", conceptos).setParameter("idCuota",
	 * cuota.getId()) .setParameter("fecha", hasta).setParameter("tipomov",
	 * tipoMov).list(); }
	 */
	public List<Ctacte> listarCtaCteConceptosNoAnulados(Cuota cuota, String tipoMov, Date hasta, String... conceptos) {
		List<Ctacte> resultado = new ArrayList<Ctacte>();
		List<Ctacte> listaCtacte = listarCtaCteConceptos(cuota, tipoMov, hasta, conceptos);
		for (Ctacte ctacte : listaCtacte) {
			if (ctacte.getBoleto() == null) {
				resultado.add(ctacte);
			} else {
				Boleto boleto = ctacte.getBoleto();
				if (boleto.getAnulado() == null || boleto.getAnulado() == Boolean.FALSE) {
					resultado.add(ctacte);
				}
			}
		}
		return resultado;
	}

	public List<BonDetalle> getBonDetalles() {
		return bonDetalles;
	}

	public void setBonDetalles(List<BonDetalle> bonDetalles) {
		this.bonDetalles = bonDetalles;
	}

	public boolean tieneCompensatorio(Cuota cuota) {
		Integer count = (Integer) bp.createSQLQuery("SELECT COUNT(*) FROM Ctacte CC "
				+ "JOIN Cuota C ON C.id = CC.cuota_id JOIN Concepto CO ON CO.id = CC.facturado_id "
				+ "JOIN CConcepto CCO ON CCO.concepto = CO.concepto_concepto WHERE C.id = :cuota_id "
				+ "AND CCO.detalle = 'Compensatorio'").setLong("cuota_id", cuota.getId()).uniqueResult();
		if (count.intValue() == 0)
			return false;
		else
			return true;
	}

	@SuppressWarnings("unchecked")
	public void crearNotaDebitoGastosARecuperar(Long idObjetoi, Boolean anula, String expePago, Date fechaPago,
			Double otrosGastos, Double gastosAdministrativos, Double gastosNovedades, Double gastosARecuperar,
			String desembolsoMovalor) throws Exception {
		// TODO implementar HashMap para recuperar los gastos cuando vienen desde WS

		if (fechaPago == null) {
			fechaPago = new Date();
		}

		bp.begin();
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoi);

		// solo se impacta cuando la moneda es Pesos
		if (objetoi.getLinea().getMoneda_id() != null && objetoi.getLinea().getMoneda_id().longValue() != 1L) {
			bp.rollback();
			return;
		}

		String consultaBasica = "SELECT * FROM desembolso D WHERE D.credito_id = :credito_id AND D.estado = '1' ORDER BY D.numero";
		String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
		Desembolso desembolso = (Desembolso) bp.createSQLQuery(consultaLimitada).addEntity(Desembolso.class)
				.setLong("credito_id", idObjetoi).uniqueResult();
		List<Cuota> cuotasImpagas = bp
				.createSQLQuery(
						"SELECT * FROM Cuota C WHERE C.credito_id = :idObjetoi AND C.estado = 1 ORDER BY C.numero")
				.addEntity(Cuota.class).setLong("idObjetoi", idObjetoi).list();
		Cuota cuota;
		if (cuotasImpagas.isEmpty()) {
			cuota = objetoi.getUltimaCuota();
			if (cuota == null) {
				cuota = new Cuota();
			}
		} else
			cuota = cuotasImpagas.get(0);

		Boleto boleto;
		Ctacte ctacte;
		Bolcon bolcon;
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		Date fecha = new Date();

		List<NovedadCtaCte> detalles = bp
				.createSQLQuery(
						"SELECT N.* FROM NovedadCtaCte N LEFT JOIN DetalleFactura D ON N.detalleFactura_id = D.id "
								+ "LEFT JOIN Factura F ON D.factura_id = F.id "
								+ "WHERE N.credito_id = :idObjetoi AND N.impactado = 0 "
								+ "AND (D.id IS NULL OR F.fechaPago IS NOT NULL) AND N.fecha <= :fecha")
				.addEntity(NovedadCtaCte.class).setLong("idObjetoi", idObjetoi).setDate("fecha", fechaPago).list();

		if (anula.booleanValue()) {
			for (NovedadCtaCte detalle : detalles) {
				if (detalle.getMovimientoDeb() == null) {
					continue;
				}
				boleto = new Boleto();
				boleto.setFechaEmision(fechaPago);
				boleto.setFechaVencimiento(fechaPago);
				boleto.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				boleto.setUsuario(usuario);
				boleto.setObjetoi(objetoi);
				boleto.setPeriodoBoleto((long) periodo);
				boleto.setVerificadorBoleto(0L);
				boleto.setImporte(detalle.getImporte());
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				bp.save(boleto);
				ctacte = objetoi.crearCtacte(detalle.getConcepto(), detalle.getImporte(), fecha, boleto, objetoi, cuota,
						detalle.getTipoConcepto() != null ? detalle.getTipoConcepto()
								: Ctacte.TIPO_CONCEPTO_ANULACION_DESEMBOLSO);
				if (cuota.getId() == null) {
					ctacte.setCuota(null);
				}
				ctacte.setTipomov(tipoCredito);
				ctacte.setFechaGeneracion(fechaPago);
				ctacte.setFechaVencimiento(fechaPago);
				ctacte.setTipoMovimiento("cuota");
				ctacte.setUsuario(usuario);
				ctacte.getId().setMovimientoCtacte(Numerador.getNext(numeradorCtaCte));
				ctacte.getId().setItemCtacte(1L);
				ctacte.setBoleto(boleto);
				ctacte.setDetalle(detalle.getDetalle());
				ctacte.setContabiliza(false);
				CreditoHandler.cargarCotizacion(ctacte);
				bolcon = objetoi.crearBolcon(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(), boleto,
						ctacte.getId(), cuota);
				if (cuota.getId() == null) {
					bolcon.setCuota(null);
				}
				bolcon.setTipomov(tipoCredito);
				bp.save(ctacte);
				bp.save(bolcon);
				detalle.setImpactado(false);
				detalle.setMovimientoDeb(null);
				bp.update(detalle);
				if (detalle.getDetalleFactura() != null) {
					detalle.getDetalleFactura().setMovimientoCtaCte(null);
					detalle.getDetalleFactura().setImpactado(false);
					bp.update(detalle.getDetalleFactura());
				}
			}
		} else {
			for (NovedadCtaCte detalle : detalles) {
				boleto = new Boleto();
				boleto.setFechaEmision(fechaPago);
				boleto.setFechaVencimiento(fechaPago);
				boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				boleto.setUsuario(usuario);
				boleto.setObjetoi(objetoi);
				boleto.setPeriodoBoleto((long) periodo);
				boleto.setVerificadorBoleto(0L);
				boleto.setImporte(detalle.getImporte());
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				bp.save(boleto);
				ctacte = objetoi.crearCtacte(detalle.getConcepto(), detalle.getImporte(), fecha, boleto, objetoi, cuota,
						detalle.getTipoConcepto() != null ? detalle.getTipoConcepto()
								: Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
				if (cuota.getId() == null) {
					ctacte.setCuota(null);
				}
				ctacte.setTipomov(tipoDebito);
				ctacte.setFechaGeneracion(fechaPago);
				ctacte.setFechaVencimiento(fechaPago);
				ctacte.setTipoMovimiento("cuota");
				ctacte.setUsuario(usuario);
				ctacte.getId().setMovimientoCtacte(Numerador.getNext(numeradorCtaCte));
				ctacte.getId().setItemCtacte(1L);
				ctacte.setBoleto(boleto);
				ctacte.setDetalle(detalle.getDetalle());
				ctacte.setContabiliza(false);
				ctacte.setDesembolso(desembolso);
				CreditoHandler.cargarCotizacion(ctacte);
				bolcon = objetoi.crearBolcon(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(), boleto,
						ctacte.getId(), cuota);
				if (cuota.getId() == null) {
					bolcon.setCuota(null);
				}
				bolcon.setTipomov(tipoDebito);
				bp.save(ctacte);
				bp.save(bolcon);
				detalle.setImpactado(true);
				detalle.setMovimientoDeb(ctacte);
				bp.update(detalle);
				if (detalle.getDetalleFactura() != null) {
					detalle.getDetalleFactura().setMovimientoCtaCte(ctacte.getId().getMovimientoCtacte());
					detalle.getDetalleFactura().setImpactado(true);
					bp.update(detalle.getDetalleFactura());
				}
			}
		}

		try {
			// los gastos que hay en el desembolso no se usan, solo deben
			// sumarse los que vienen de GAF
			Double importeGastos = 0.0;
			if (otrosGastos != null) {
				importeGastos += otrosGastos;
			}
			if (gastosAdministrativos != null) {
				importeGastos += gastosAdministrativos;
			}
			if (importeGastos.doubleValue() > 0.0 && (anula.booleanValue() || desembolso.getGastosImpactados() == null
					|| desembolso.getGastosImpactados().booleanValue() == false)) {
				boleto = new Boleto();
				boleto.setFechaEmision(fechaPago);
				boleto.setFechaVencimiento(fechaPago);
				boleto.setTipo(anula.booleanValue() ? Boleto.TIPO_BOLETO_NOTACRED : Boleto.TIPO_BOLETO_NOTADEB);
				boleto.setUsuario(usuario);
				boleto.setObjetoi(objetoi);
				boleto.setPeriodoBoleto((long) periodo);
				boleto.setVerificadorBoleto(0L);
				boleto.setImporte(importeGastos);
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				bp.save(boleto);

				Concepto conceptoGastos = Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS);
				ctacte = new Ctacte();
				ctacte.setImporte(importeGastos);
				ctacte.redondear();
				ctacte.setAsociado(conceptoGastos);
				ctacte.setFacturado(conceptoGastos);
				ctacte.setTipomov(anula.booleanValue() ? tipoCredito : tipoDebito);
				ctacte.setTipoMovimiento("cuota");
				ctacte.setFechaVencimiento(fechaPago);
				ctacte.setFechaGeneracion(fechaPago);
				ctacte.setDetalle(anula.booleanValue() ? "Anulaci�n Gastos Administrativos" : "Gastos Administrativos");
				ctacte.setContabiliza(false);
				if (cuota.getId() == null) {
					ctacte.setCuota(null);
				} else {
					ctacte.setCuota(cuota);
				}
				ctacte.setUsuario(usuario);

				CtacteKey ccKeyGastos = new CtacteKey();
				ccKeyGastos.setPeriodoCtacte(new Long(periodo));
				ccKeyGastos.setObjetoi(objetoi);
				ccKeyGastos.setVerificadorCtacte(0L);
				ctacte.setId(ccKeyGastos);
				ctacte.getId().setMovimientoCtacte(Numerador.getNext(numeradorCtaCte));
				ctacte.getId().setItemCtacte(1L);
				ctacte.setBoleto(boleto);
				CreditoHandler.cargarCotizacion(ctacte);
				ctacte.setTipoConcepto(Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
				bolcon = objetoi.crearBolcon(Concepto.CONCEPTO_GASTOS, importeGastos, boleto, ctacte.getId(), cuota);
				if (cuota.getId() == null) {
					bolcon.setCuota(null);
				}
				bolcon.setTipomov(anula.booleanValue() ? tipoCredito : tipoDebito);
				bp.save(ctacte);
				bp.save(bolcon);
				desembolso.setGastosImpactados(!anula.booleanValue());
				bp.update(desembolso);
			}
		} catch (Exception e) {
			// no hay desembolso pendiente
			e.printStackTrace();
			bp.rollback();
		}
		bp.commit();
	}

	@SuppressWarnings("unchecked")
	public void asignarCtaCteSinCuota(Long idCredito, Cuota cuota) {
		List<Ctacte> ctactes = bp
				.createSQLQuery("SELECT * FROM Ctacte C WHERE C.objetoi_id = :objetoi_id AND C.cuota_id IS NULL")
				.addEntity(Ctacte.class).setLong("objetoi_id", idCredito).list();
		for (Ctacte ctacte : ctactes) {
			ctacte.setCuota(cuota);
			bp.update(ctacte);
		}
	}

	@SuppressWarnings("unchecked")
	public void asignarBolconSinCuota(Long idCredito, Cuota cuota) {
		List<Bolcon> bolcons = bp
				.createSQLQuery("SELECT * FROM Boleto B INNER JOIN Bolcon BO ON B.id = BO.boleto_id "
						+ "WHERE B.objetoi_id = :objetoi_id AND BO.cuota_id IS NULL")
				.addEntity(Bolcon.class).setLong("objetoi_id", idCredito).list();
		for (Bolcon bolcon : bolcons) {
			bolcon.setCuota(cuota);
			bp.update(bolcon);
		}

	}

	public CosechaConfig buscarCosechaConfig(Date fechaSolicitud, boolean varietales) {
		CosechaConfig cosecha = (CosechaConfig) bp.getNamedQuery("CosechaConfig.findByFecha")
				.setParameter("fecha", fechaSolicitud).setParameter("varietales", varietales).uniqueResult();
		return cosecha;
	}

	public CategoriaSolicitanteConfig buscarCategoriaSolicitanteConfig(Long lineaId, String tipoTasa,
			String categoriaSolicitante, Date fechaSolicitud) {
		CategoriaSolicitanteConfig categoriaSolicitanteConfig = (CategoriaSolicitanteConfig) bp
				.getNamedQuery("CategoriaSolicitanteConfig.findByLineaTasaCategoriaFecha").setLong("lineaId", lineaId)
				.setString("tipoTasa", tipoTasa).setString("categoriaSolicitante", categoriaSolicitante)
				.setDate("fechaSolicitud", fechaSolicitud).uniqueResult();
		return categoriaSolicitanteConfig;
	}

	@SuppressWarnings("unchecked")
	public PeriodoCosechaConfig buscarPeriodoCosechaConfig(Date fechaSolicitud, Garantia garantia, boolean varietales) {
		CosechaConfig cosecha = buscarCosechaConfig(fechaSolicitud, varietales);
		List<PeriodoCosechaConfig> periodos = bp
				.createQuery("select p from PeriodoCosechaConfig p where "
						+ "p.cosechaConfig.id = :idCosecha and p.fechaDesde <= :fecha and p.fechaHasta >= :fecha")
				.setParameter("fecha", fechaSolicitud).setParameter("idCosecha", cosecha.getId()).list();
		PeriodoCosechaConfig periodoSinGarantia = null;
		PeriodoCosechaConfig periodo = null;
		for (PeriodoCosechaConfig p : periodos) {
			if (p.getTipoGarantia_id() == null || p.getTipoGarantia_id() == 0) {
				periodoSinGarantia = p;
				continue;
			}
			if (p.getTipoGarantia_id().equals(garantia.getTipoId())
					&& (garantia.getTipoProducto() == null || p.getTipoProducto().equals(garantia.getTipoProducto()))) {
				periodo = p;
				break;
			}
		}
		// si no coincide ninguna garantia, usar el periodo sin garantia
		if (periodo == null) {
			periodo = periodoSinGarantia;
		}
		return periodo;
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, String> generarDatosFinancieros(Objetoi credito) throws Exception {
		HashMap<String, String> errores = new HashMap<String, String>();

		// Crea ObjetoiIndice para el nuevo credito
		ObjetoiIndice tasaCompensatorio = new ObjetoiIndice();
		ObjetoiIndice tasaMoratorio = new ObjetoiIndice();
		ObjetoiIndice tasaPunitorio = new ObjetoiIndice();
		ObjetoiBonificacion tasaBonificacion = new ObjetoiBonificacion();

		if (credito.findTasaCompensatorio() != null) {
			tasaCompensatorio = credito.findTasaCompensatorio();
		}
		if (credito.findTasaMoratorio() != null) {
			tasaMoratorio = credito.findTasaMoratorio();
		}
		if (credito.findTasaPunitorio() != null) {
			tasaPunitorio = credito.findTasaPunitorio();
		}

		Date fechaSolicitud;
		if (credito.getFechaSolicitud() == null) {
			fechaSolicitud = new Date();
		} else {
			fechaSolicitud = credito.getFechaSolicitud();
		}

		if (credito.getLinea().isLineaCosecha()) {
			CosechaConfig cosecha = buscarCosechaConfig(fechaSolicitud, credito.getVarietales());
			if (cosecha == null) {
				errores.put("Solicitud.cosechaConfig", "Solicitud.cosechaConfig");
				return errores;
			}
			credito.setFrecuenciaCapital(cosecha.getPeriodicidad());
			credito.setFrecuenciaInteres(cosecha.getPeriodicidad());
			credito.setPrimerVencCapital(cosecha.getFechaPrimerVto());
			credito.setPrimerVencInteres(cosecha.getFechaPrimerVto());
			credito.setPlazoCapital(cosecha.getCantCuotas());
			credito.setPlazoCompensatorio(cosecha.getCantCuotas());
			// Compruebo a que temporada corresponde el nuevo credito
			List<Garantia> garantias = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
					.setParameter("credito", credito).list();
			Garantia garantia;
			if (garantias.isEmpty()) {
				garantia = new Garantia();
			} else {
				garantia = garantias.get(0);
			}
			PeriodoCosechaConfig periodo = buscarPeriodoCosechaConfig(fechaSolicitud, garantia,
					credito.getVarietales());
			if (periodo == null) {
				errores.put("Solicitud.periodoCosecha", "Solicitud.periodoCosecha");
				return errores;
			}
			tasaCompensatorio.setTipoTasa("1");
			tasaCompensatorio.setIndice(periodo.getIndiceGar());
			tasaCompensatorio.setValorMas(periodo.getValorMasGar());
			tasaCompensatorio.setValorPor(periodo.getValorPorGar());
			tasaCompensatorio.setDiasAntes(periodo.getDiasAntesGar());
			tasaCompensatorio.setTipoCalculo(credito.getLinea().getTipoCalculoCompensatorio());
			tasaCompensatorio.setTasaTope(credito.getLinea().getTopeTasaCompensatorio());
			tasaCompensatorio.setCredito(credito);
			if (periodo.getBonificacionGar() != null && credito.getEsMiPyme()) {
				if (!comprobarBonificaciones(credito)) {
					eliminarBonificaciones(credito);
				}
				tasaBonificacion.setBonificacion(periodo.getBonificacionGar());
				tasaBonificacion.setObjetoi(credito);
			} else if (periodo.getBonificacionGar() == null) {
				eliminarBonificaciones(credito);
			}
			tasaMoratorio.setTipoTasa("2");
			tasaMoratorio.setIndice(credito.getLinea().getIndiceMor());
			tasaMoratorio.setValorMas(credito.getLinea().getValorMasMor());
			tasaMoratorio.setValorPor(credito.getLinea().getValorPorMor());
			tasaMoratorio.setDiasAntes(credito.getLinea().getDiasAntesMor());
			tasaMoratorio.setTipoCalculo(credito.getLinea().getTipoCalculoMoratorio());
			tasaMoratorio.setTasaTope(credito.getLinea().getTopeTasaMoratorio());
			tasaMoratorio.setCredito(credito);
			tasaPunitorio.setTipoTasa("3");
			tasaPunitorio.setIndice(credito.getLinea().getIndicePun());
			tasaPunitorio.setValorMas(credito.getLinea().getValorMasPun());
			tasaPunitorio.setValorPor(credito.getLinea().getValorPorPun());
			tasaPunitorio.setDiasAntes(credito.getLinea().getDiasAntesPun());
			tasaPunitorio.setTipoCalculo(credito.getLinea().getTipoCalculoPunitorio());
			tasaPunitorio.setTasaTope(credito.getLinea().getTopeTasaPunitorio());
			tasaPunitorio.setCredito(credito);
		} else {
			/**
			 * compensatorio
			 */
			CategoriaSolicitanteConfig categoriaSolicitanteConfig = buscarCategoriaSolicitanteConfig(
					credito.getLinea_id(), "1", credito.getCategoriaSolicitante(), fechaSolicitud);
			tasaCompensatorio.setTipoTasa("1");
			tasaCompensatorio.setCredito(credito);
			if (categoriaSolicitanteConfig == null) {
				tasaCompensatorio.setIndice(credito.getLinea().getIndiceComp());
				tasaCompensatorio.setValorMas(credito.getLinea().getValorMasComp());
				tasaCompensatorio.setValorPor(credito.getLinea().getValorPorComp());
				tasaCompensatorio.setDiasAntes(credito.getLinea().getDiasAntesComp());
				tasaCompensatorio.setTipoCalculo(credito.getLinea().getTipoCalculoCompensatorio());
				tasaCompensatorio.setTasaTope(credito.getLinea().getTopeTasaCompensatorio());
			} else {
				tasaCompensatorio.setIndice(categoriaSolicitanteConfig.getIndice());
				tasaCompensatorio.setValorMas(categoriaSolicitanteConfig.getValorMas());
				tasaCompensatorio.setValorPor(categoriaSolicitanteConfig.getValorPor());
				tasaCompensatorio.setDiasAntes(categoriaSolicitanteConfig.getDiasAntes());
				tasaCompensatorio.setTipoCalculo(categoriaSolicitanteConfig.getTipoCalculo());
				tasaCompensatorio.setTasaTope(categoriaSolicitanteConfig.getTopeTasa());
			}
			/**
			 * moratorio
			 */
			categoriaSolicitanteConfig = buscarCategoriaSolicitanteConfig(credito.getLinea_id(), "2",
					credito.getCategoriaSolicitante(), fechaSolicitud);
			tasaMoratorio.setTipoTasa("2");
			tasaMoratorio.setCredito(credito);
			if (categoriaSolicitanteConfig == null) {
				tasaMoratorio.setIndice(credito.getLinea().getIndiceMor());
				tasaMoratorio.setValorMas(credito.getLinea().getValorMasMor());
				tasaMoratorio.setValorPor(credito.getLinea().getValorPorMor());
				tasaMoratorio.setDiasAntes(credito.getLinea().getDiasAntesMor());
				tasaMoratorio.setTipoCalculo(credito.getLinea().getTipoCalculoMoratorio());
				tasaMoratorio.setTasaTope(credito.getLinea().getTopeTasaMoratorio());
			} else {
				tasaMoratorio.setIndice(categoriaSolicitanteConfig.getIndice());
				tasaMoratorio.setValorMas(categoriaSolicitanteConfig.getValorMas());
				tasaMoratorio.setValorPor(categoriaSolicitanteConfig.getValorPor());
				tasaMoratorio.setDiasAntes(categoriaSolicitanteConfig.getDiasAntes());
				tasaMoratorio.setTipoCalculo(categoriaSolicitanteConfig.getTipoCalculo());
				tasaMoratorio.setTasaTope(categoriaSolicitanteConfig.getTopeTasa());
			}
			/**
			 * punitorio
			 */
			categoriaSolicitanteConfig = buscarCategoriaSolicitanteConfig(credito.getLinea_id(), "3",
					credito.getCategoriaSolicitante(), fechaSolicitud);
			tasaPunitorio.setTipoTasa("3");
			tasaPunitorio.setCredito(credito);
			if (categoriaSolicitanteConfig == null) {
				tasaPunitorio.setIndice(credito.getLinea().getIndicePun());
				tasaPunitorio.setValorMas(credito.getLinea().getValorMasPun());
				tasaPunitorio.setValorPor(credito.getLinea().getValorPorPun());
				tasaPunitorio.setDiasAntes(credito.getLinea().getDiasAntesPun());
				tasaPunitorio.setTipoCalculo(credito.getLinea().getTipoCalculoPunitorio());
				tasaPunitorio.setTasaTope(credito.getLinea().getTopeTasaPunitorio());
			} else {
				tasaPunitorio.setIndice(categoriaSolicitanteConfig.getIndice());
				tasaPunitorio.setValorMas(categoriaSolicitanteConfig.getValorMas());
				tasaPunitorio.setValorPor(categoriaSolicitanteConfig.getValorPor());
				tasaPunitorio.setDiasAntes(categoriaSolicitanteConfig.getDiasAntes());
				tasaPunitorio.setTipoCalculo(categoriaSolicitanteConfig.getTipoCalculo());
				tasaPunitorio.setTasaTope(categoriaSolicitanteConfig.getTopeTasa());
			}
		}
		bp.begin();
		if (tasaCompensatorio.getCredito() != null) {
			bp.saveOrUpdate(tasaCompensatorio);
		}
		if (tasaMoratorio.getCredito() != null) {
			bp.saveOrUpdate(tasaMoratorio);
		}
		if (tasaPunitorio.getCredito() != null) {
			bp.saveOrUpdate(tasaPunitorio);
		}
		if (tasaBonificacion.getObjetoi() != null) {
			bp.save(tasaBonificacion);
		}
		bp.commit();

		return errores;
	}

	@SuppressWarnings("unchecked")
	public void calcularFinanciamiento(Objetoi cred) {
		double sumQQAprobados = 0;
		double QQCosto = 0;
		double QQParcial = 0;
		double montoFinal = 0;
		double QQFinal = 0;

		if (!cred.getEstadoActual().getEstado().getNombreEstado().equalsIgnoreCase("ANALISIS")) {
			return;
		}
		List<Garantia> garantiasListado = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
				.setParameter("credito", cred).list();
		Garantia garantia = null;
		if (!garantiasListado.isEmpty()) {
			garantia = garantiasListado.get(0);
		}
		boolean calculaQQSinVinedo = false;
		if (garantia != null && garantia.getTipo() != null && garantia.getTipo().getCalculaQQSinVinedo() != null) {
			calculaQQSinVinedo = garantia.getTipo().getCalculaQQSinVinedo();
		}
		List<ObjetoiVinedo> listaVinedos = bp.createQuery("SELECT v FROM ObjetoiVinedo v WHERE v.credito = :credito")
				.setParameter("credito", cred).list();
		if (calculaQQSinVinedo || listaVinedos.size() > 0) {
			if (cred.getDestino() != null && cred.getDestino().equalsIgnoreCase("CRDA")) { // elaboracion
				for (ObjetoiVinedo vinedoExistente : listaVinedos) {
					if (vinedoExistente.getObjetoiVinedoElab().getQqAprobado() != null) {
						sumQQAprobados += vinedoExistente.getObjetoiVinedoElab().getQqAprobado();
					}
				}
			} else {
				for (ObjetoiVinedo vinedoExistente : listaVinedos) {
					if (vinedoExistente.getQqAprobado() != null) {
						sumQQAprobados += vinedoExistente.getQqAprobado();
					}
				}
			}
			CosechaConfig cosecha = (CosechaConfig) bp
					.createQuery("SELECT c FROM CosechaConfig c WHERE c.fechaInicioPer <= :fecha AND "
							+ "c.fechaFinPer >= :fecha AND c.varietales = :varietales ")
					.setParameter("fecha", cred.getFechaSolicitud()).setParameter("varietales", cred.getVarietales())
					.uniqueResult();
			// sumo el qq aprobado solo si calcula qq sin vi�edo = false
			if (calculaQQSinVinedo) {
				QQParcial = cred.getQqsolicitado() != null ? cred.getQqsolicitado() : 0;
			} else {
				if (cred.getQqsolicitado() != null && cred.getQqsolicitado() != 0) {
					// QQParcial = Math.min(sumQQAprobados,
					// Math.min(sumQQEstimados, cred.getQqsolicitado()));
					QQParcial = Math.min(sumQQAprobados, cred.getQqsolicitado());
				} else {
					// QQParcial = Math.min(sumQQAprobados, sumQQEstimados);
					QQParcial = sumQQAprobados;
				}
			}
			if (cred.getDestino().equals("CRDC")) {
				QQCosto = QQParcial * cosecha.getCostoQQCosecha();
			} else if (cred.getDestino().equals("CRDA")) {
				QQCosto = QQParcial * cosecha.getCostoQQElab();
			}
			if (cred.getFinanciamientoSol() != null && cred.getFinanciamientoSol() != 0) {
				montoFinal = (Math.min(QQCosto, cred.getFinanciamientoSol()));
			} else {
				montoFinal = QQCosto;
			}
			if (cred.getDestino().equals("CRDC")) {
				QQFinal = montoFinal / cosecha.getCostoQQCosecha();
			} else if (cred.getDestino().equals("CRDA")) {
				QQFinal = montoFinal / cosecha.getCostoQQElab();
			}
			if (cred.getDestino().equals("CRDC")) {
				if (garantia.getTipo() != null && QQFinal > garantia.getTipo().getMaxQQProyecto()
						&& garantia.getTipo().getMaxQQProyecto() > 0) {
					QQFinal = garantia.getTipo().getMaxQQProyecto();
				}
				// busco garantia de tipo 'Fianza Fecovita'
				boolean fianzaFecovita = false;
				for (Garantia g : garantiasListado) {
					if (g.getTipo() != null && g.getTipo().getNombre().trim().equalsIgnoreCase("Fianza Fecovita")) {
						fianzaFecovita = true;
						break;
					}
				}
				if (!fianzaFecovita && cosecha.getMaxQQ() != null && QQFinal > cosecha.getMaxQQ()) {
					QQFinal = cosecha.getMaxQQ();
				}
			}
			cred.setQqFinal(QQFinal);
			if (cred.getDestino().equals("CRDC")) {
				cred.setFinanciamiento(QQFinal * cosecha.getCostoQQCosecha());
			} else if (cred.getDestino().equals("CRDA")) {
				cred.setFinanciamiento(QQFinal * cosecha.getCostoQQElab());
			}
			bp.update(cred);
			// Se generan o actualizan los desembolsos del credito
			List<Desembolso> desembolsos = bp
					.createQuery("Select d from Desembolso d where d.credito=:cred order by d.numero")
					.setParameter("cred", cred).list();
			if (garantia == null) {
				garantia = new Garantia();
			}
			if (desembolsos == null || desembolsos.size() == 0) {
				Desembolso desembolso1 = new Desembolso();
				Desembolso desembolso2 = new Desembolso();
				desembolso1.setFecha(new Date());
				desembolso1.setNumero(1);
				desembolso1.setObservacion("Desembolso generado automaticamente");
				desembolso1.setCredito(cred);
				desembolso1.setEstado("4");
				if (garantia.getId() != null) {
					double importe = cred.getFinanciamiento() * garantia.getTipo().getPorcDesembolso1();
					importe = DoubleHelper.redondear(importe);
					desembolso1.setImporte(importe);
				}
				if (garantia.getTipo().getPorcDesembolso1() != 1) {
					desembolso2.setFecha(new Date());
					desembolso2.setNumero(2);
					desembolso2.setObservacion("Desembolso generado automaticamente");
					desembolso2.setCredito(cred);
					desembolso2.setEstado("4");
					if (garantia.getId() != null) {
						double importe = cred.getFinanciamiento() * garantia.getTipo().getPorcDesembolso2();
						importe = DoubleHelper.redondear(importe);
						desembolso2.setImporte(importe);
						importe = DoubleHelper.redondear(importe);
						desembolso2.setImporte(importe);
					}
					bp.save(desembolso2);
				}
				bp.save(desembolso1);
				bp.getCurrentSession().flush();
			} else if (desembolsos.size() == 1) {
				Desembolso desembolso1 = desembolsos.get(0);
				Desembolso desembolso2 = new Desembolso();

				desembolso1.setFecha(new Date());
				desembolso1.setNumero(1);
				desembolso1.setObservacion("Desembolso generado automaticamente");
				desembolso1.setCredito(cred);
				desembolso1.setEstado("4");
				if (garantia.getId() != null) {
					double importe = cred.getFinanciamiento() * garantia.getTipo().getPorcDesembolso1();
					importe = DoubleHelper.redondear(importe);
					desembolso1.setImporte(importe);
				}

				if (garantia.getTipo().getPorcDesembolso1() != 1) {
					desembolso2.setFecha(new Date());
					desembolso2.setNumero(2);
					desembolso2.setObservacion("Desembolso generado automaticamente");
					desembolso2.setCredito(cred);
					desembolso2.setEstado("4");
					if (garantia.getId() != null) {
						double importe = cred.getFinanciamiento() * garantia.getTipo().getPorcDesembolso2();
						importe = DoubleHelper.redondear(importe);
						desembolso2.setImporte(importe);
					}
					bp.save(desembolso2);
				}
				bp.update(desembolso1);

			} else if (desembolsos.size() == 2) {
				Desembolso desembolso1 = desembolsos.get(0);
				Desembolso desembolso2 = desembolsos.get(1);

				desembolso1.setFecha(new Date());
				desembolso1.setNumero(1);
				desembolso1.setObservacion("Desembolso generado automaticamente");
				desembolso1.setCredito(cred);
				desembolso1.setEstado("4");
				if (garantia.getId() != null) {
					double importe = cred.getFinanciamiento() * garantia.getTipo().getPorcDesembolso1();
					importe = DoubleHelper.redondear(importe);
					desembolso1.setImporte(importe);
				}

				if (garantia.getTipo().getPorcDesembolso1() != 1) {
					desembolso2.setFecha(new Date());
					desembolso2.setNumero(2);
					desembolso2.setObservacion("Desembolso generado automaticamente");
					desembolso2.setCredito(cred);
					desembolso2.setEstado("4");
					if (garantia.getId() != null) {
						double importe = DoubleHelper
								.redondear(cred.getFinanciamiento() * garantia.getTipo().getPorcDesembolso2());
						desembolso2.setImporte(importe);
					}
					bp.update(desembolso2);
				} else {
					Desembolso existente = (Desembolso) bp.getById(Desembolso.class, desembolso2.getId());
					bp.delete(existente);
				}
				bp.update(desembolso1);

			}
		}
	}

	@SuppressWarnings("unchecked")
	private boolean comprobarBonificaciones(Objetoi credito) {
		List<ObjetoiBonificacion> bonificaciones = bp
				.createQuery("Select b from ObjetoiBonificacion b where b.objetoi=:cred").setParameter("cred", credito)
				.list();
		if (bonificaciones.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private void eliminarBonificaciones(Objetoi credito) {
		List<ObjetoiBonificacion> bonificaciones = bp
				.createQuery("Select b from ObjetoiBonificacion b where b.objetoi=:cred").setParameter("cred", credito)
				.list();
		for (ObjetoiBonificacion bon : bonificaciones) {
			bp.delete(bon);
		}
	}

	public boolean actualizarComportamiento(Objetoi objetoi, Date desde, Date hasta) {
		return actualizarComportamiento(objetoi, desde, hasta, null);
	}

	@SuppressWarnings("unchecked")
	public boolean actualizarComportamiento(Objetoi objetoi, Date desde, Date hasta, String motivo) {
		boolean seActualizo = false;
		List<Cuota> cuotas = bp.createQuery(
				"SELECT c FROM Cuota c WHERE c.credito = :credito AND c.fechaVencimiento <= :hoy AND c.estado = '1' ORDER BY c.fechaVencimiento")
				.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();

		ObjetoiComportamiento comportamientoNuevo = null;
		Date hoy = new Date();

		if (cuotas.isEmpty()) { // no tiene cuotas vencidas
			comportamientoNuevo = new ObjetoiComportamiento();
			comportamientoNuevo.setObjetoi(objetoi);
			comportamientoNuevo.setFecha(hoy);
			comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
		} else {

			Cuota cuota = cuotas.get(0); // cuota vencida mas antigua

			CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(objetoi.getId(), hasta == null ? new Date() : hasta);
			ccd.calcular();

			List<BeanCtaCte> beansCuota = ccd.getVistaCuota(cuota.getNumero());

			double deudaImponible = CreditoCalculoDeuda.getSaldo(beansCuota, Concepto.CONCEPTO_CAPITAL);
			deudaImponible += CreditoCalculoDeuda.getSaldo(beansCuota, Concepto.CONCEPTO_COMPENSATORIO);
			deudaImponible += CreditoCalculoDeuda.getSaldo(beansCuota, Concepto.CONCEPTO_GASTOS);
			deudaImponible += CreditoCalculoDeuda.getSaldo(beansCuota, Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
			deudaImponible += CreditoCalculoDeuda.getSaldo(beansCuota, Concepto.CONCEPTO_MULTAS);
			deudaImponible -= CreditoCalculoDeuda.getSaldo(beansCuota, Concepto.CONCEPTO_BONIFICACION);

			List<BeanCtaCte> bean = ccd.getBeans();
			double totalCuota = 0;
			for (BeanCtaCte beanCtaCte : bean) {
				if (beanCtaCte.getNumero().equals(cuota.getNumero())) {
					if (beanCtaCte.getDetalle().equalsIgnoreCase("cuota")
							|| beanCtaCte.getDetalle().equalsIgnoreCase("debito manual")) {
						totalCuota += beanCtaCte.getCapital();
						totalCuota += beanCtaCte.getCompensatorio();
						totalCuota += beanCtaCte.getMoratorio();
						totalCuota += beanCtaCte.getPunitorio();
						totalCuota += beanCtaCte.getGastos();
						totalCuota += beanCtaCte.getMultas();
					} else if (beanCtaCte.getDetalle().equalsIgnoreCase("credito manual")) {
						totalCuota -= beanCtaCte.getCapital();
						totalCuota -= beanCtaCte.getCompensatorio();
						totalCuota -= beanCtaCte.getMoratorio();
						totalCuota -= beanCtaCte.getPunitorio();
						totalCuota -= beanCtaCte.getGastos();
						totalCuota -= beanCtaCte.getMultas();
					} else if (beanCtaCte.getDetalle().equalsIgnoreCase("bonificacion")) {
						totalCuota -= beanCtaCte.getCompensatorio();
					}
				}
			}

			if (totalCuota > 0) {
				comportamientoNuevo = new ObjetoiComportamiento();
				comportamientoNuevo.setObjetoi(objetoi);
				comportamientoNuevo.setFecha(hoy);
				// mayor al porcentaje, o mas de una cuota vencida
				if (deudaImponible / totalCuota >= 0.1 || cuotas.size() > 1) {
					Date fechaDesde = (desde == null) ? cuota.getFechaVencimiento() : desde;
					Date fechaHasta = (hasta == null) ? new Date() : hasta;
					int atraso = (int) DateHelper.getDiffDates(fechaDesde, fechaHasta, 2);
					if (atraso <= 60) {
						comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
					} else if (atraso <= 90) {
						comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.INADECUADO_CON_ATRASO);
					} else if (atraso <= 180) {
						comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.CON_PROBLEMAS_DE_ATRASO);
					} else if (atraso <= 365) {
						comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.ALTO_RIESGO_CON_ATRASO);
					} else {
						comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.ALTO_RIESGO_CON_ATRASO);
					}
				} else {
					comportamientoNuevo.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
				}

			}
		}
		if (comportamientoNuevo != null) {
			List<ObjetoiComportamiento> comportamientos = bp
					.createQuery(
							"SELECT c FROM ObjetoiComportamiento c WHERE c.objetoi.id = :id AND c.fechaHasta is null")
					.setParameter("id", objetoi.getId()).list();
			if (comportamientos.size() > 0) {
				ObjetoiComportamiento comportamientoAnterior = comportamientos.get(0);
				boolean igual;
				if (comportamientos.get(0).getComportamientoPago() != null) {
					igual = comportamientos.get(0).getComportamientoPago()
							.equalsIgnoreCase(comportamientoNuevo.getComportamientoPago());
				} else {
					igual = false;
				}
				if (!igual) {
					bp.begin();
					comportamientoAnterior.setFechaHasta(hoy);
					bp.update(comportamientoAnterior);
					comportamientoNuevo.setObservaciones(motivo);
					bp.save(comportamientoNuevo);
					bp.commit();
					seActualizo = true;
				}
			}
		}

		return seActualizo;
	}

	public void setSegundoVencimientoCapital(Date segundoVencimientoCapital) {
		this.segundoVencimientoCapital = segundoVencimientoCapital;
	}

	public void setSegundoVencimientoCompensatorio(Date segundoVencimientoCompensatorio) {
		this.segundoVencimientoCompensatorio = segundoVencimientoCompensatorio;
	}

	public static void cargarCotizacion(Ctacte cc) {
		cargarCotizacion(cc, cc.getFechaGeneracion());
	}

	public static void cargarCotizacion(Ctacte cc, Date fecha) {
		cc.setCotizaMov(cc.getId().getObjetoi().getLinea().getMoneda().getCotizacionCompra(fecha));
	}

	@SuppressWarnings("unchecked")
	public List<Objetoi> realizarAjusteMoneda(Date fecha) {
		List<Long> ids = bp.createQuery(
				"select c.id from ObjetoiEstado oe join oe.objetoi c where oe.fechaHasta is null and oe.estado.nombreEstado not in (:estados) and c.linea.moneda.idMoneda <> :peso")
				.setParameter("peso", 1L)
				.setParameterList("estados",
						new String[] { ANALISIS, CANCELADO, FINALIZADO, ORIGINAL_CON_ACUERDO, ACUERDO_INCUMPLIDO })
				.list();

		List<Objetoi> result = new ArrayList<Objetoi>();

		for (Long id : ids) {
			try {
				Objetoi o = realizarAjusteMoneda(id, fecha);
				if (o != null) {
					result.add(o);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	private void validarFechaAjusta(Long idCredito, Date fecha) throws Exception {
		List<Ctacte> ccsAjustes = bp.createQuery(
				"select cc from CtaCteAjuste cc where cc.id.objetoi.id = :idCredito order by cc.fechaGeneracion DESC")
				.setParameter("idCredito", idCredito).setMaxResults(1).list();
		if (!ccsAjustes.isEmpty()) {
			if (ccsAjustes.get(0).getFechaGeneracion().getTime() >= fecha.getTime()) {
				throw new Exception("Se ha realizado un ajuste con fecha posterior a la solicitada.");
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Objetoi realizarAjusteMoneda(Pagos pago) throws Exception {
		Long idCredito = pago.getObjetoi().getId();
		Date fecha = pago.getFechaPago();
		validarFechaAjusta(idCredito, fecha);

		Moneda moneda = (Moneda) bp.createQuery("select c.linea.moneda from Objetoi c where c.id = :idCredito")
				.setParameter("idCredito", idCredito).uniqueResult();
		double cotizacionCompraFecha = moneda.getCotizacionCompra(fecha);
		double cotizacionVentaFecha = moneda.getCotizacion(fecha);

		double cotizacionDiferencia = cotizacionVentaFecha - cotizacionCompraFecha;

		List<Object[]> result = bp.createSQLQuery(
				"SELECT SUM(importe), con.concepto_concepto from Ctacte cc join Concepto con on cc.asociado_id = con.id where boleto_id = ? group by con.concepto_concepto")
				.setParameter(0, pago.getRecibo().getId()).list();

		Map<String, Double> ajustesPorConcepto = new HashMap<String, Double>();
		double ajusteTotal = 0;
		for (Object[] row : result) {
			Number n = (Number) row[0];
			String concepto = (String) row[1];
			double importeMoneda = n != null ? n.doubleValue() : 0;
			double importeAjuste = importeMoneda * cotizacionDiferencia;
			if (Math.abs(importeAjuste) >= 0.01) {
				ajustesPorConcepto.put(concepto, importeAjuste);
				ajusteTotal += importeAjuste;
			}
		}

		if (ajustesPorConcepto.keySet().isEmpty()) {
			return null;
		}

		String detalle = "Dif. Cambio por Cobro $";
		Boleto comprobante = crearBoletoAjuste(idCredito, fecha, ajusteTotal);
		Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idCredito)
				.setParameter("nroCuota", comprobante.getNumeroCuota()).uniqueResult();
		if (cuota == null) {
			// si no hay cuota con ese numero, la vencida fue la ultima. usar la
			// vencida
			cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idCredito)
					.setParameter("nroCuota", comprobante.getNumeroCuota() - 1).uniqueResult();
		}
		for (String con : ajustesPorConcepto.keySet()) {
			Concepto concepto = Concepto.buscarConcepto(con);
			double importeAjuste = ajustesPorConcepto.get(con);
			guardarAjuste(comprobante, cuota, idCredito, importeAjuste, fecha, concepto, cotizacionCompraFecha, detalle,
					Ctacte.TIPO_CONCEPTO_AJUSTE_DIF_CAMBIO_COBRO);
		}

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		return credito;
	}

	@SuppressWarnings("unchecked")
	public Objetoi realizarAjusteMoneda(Long idCredito, Date fecha) throws Exception {
		validarFechaAjusta(idCredito, fecha);

		Moneda moneda = (Moneda) bp.createQuery("select c.linea.moneda from Objetoi c where c.id = :idCredito")
				.setParameter("idCredito", idCredito).uniqueResult();
		double cotizacionFecha = moneda.getCotizacionCompra(fecha);

		List<Object[]> result = bp.createSQLQuery(
				"SELECT SUM(case tipomov_id when 1 then -importe when 2 then importe end), con.concepto_concepto from Ctacte cc "
						+ "join Concepto con on cc.asociado_id = con.id where objetoi_id = ? and con.concepto_concepto not in ('ADC', 'ADTC') group by con.concepto_concepto")
				.setParameter(0, idCredito).list();

		// busco ultima cotizacion aplicada en cuenta corriente
		List<Number> cotizaciones = bp.createSQLQuery(
				"SELECT CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.cotizacionDiferencia ELSE cc.cotizaMov END AS cotizacion FROM Ctacte cc "
						+ "JOIN Concepto con ON cc.asociado_id = con.id WHERE cc.objetoi_id = ? ORDER BY cc.fechaProceso DESC")
				.setParameter(0, idCredito).list();
		Number ultimaCotizacion = null;
		if (cotizaciones != null && !cotizaciones.isEmpty()) {
			ultimaCotizacion = cotizaciones.get(0);
		}

		Map<String, Double> ajustesPorConcepto = new HashMap<String, Double>();

		double ajusteTotal = 0;
		for (Object[] row : result) {
			Number n = (Number) row[0];
			String concepto = (String) row[1];
			double saldoMoneda = n != null ? n.doubleValue() : 0;

			double saldoPesosEquivalente = saldoMoneda * cotizacionFecha;

			// si el ultimo ajuste fue con el concepto viejo
			double saldoReal = saldoMoneda * ultimaCotizacion.doubleValue();

			double importeAjuste = saldoPesosEquivalente - saldoReal;

			if (Math.abs(importeAjuste) >= 0.01) {
				ajustesPorConcepto.put(concepto, importeAjuste);
				ajusteTotal += importeAjuste;
			}

		}

		if (ajusteTotal == 0 || ajustesPorConcepto.keySet().isEmpty()) {
			return null;
		}

		String detalle = "Dif. Cambio por Ajuste Mensual";
		Boleto comprobante = crearBoletoAjuste(idCredito, fecha, ajusteTotal);
		Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idCredito)
				.setParameter("nroCuota", comprobante.getNumeroCuota()).uniqueResult();
		if (cuota == null) {
			// si no hay cuota con ese numero, la vencida fue la ultima. usar la
			// vencida
			cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idCredito)
					.setParameter("nroCuota", comprobante.getNumeroCuota() - 1).uniqueResult();
		}
		for (String con : ajustesPorConcepto.keySet()) {
			Concepto concepto = Concepto.buscarConcepto(con);
			double importeAjuste = ajustesPorConcepto.get(con);
			guardarAjuste(comprobante, cuota, idCredito, importeAjuste, fecha, concepto, cotizacionFecha, detalle,
					Ctacte.TIPO_CONCEPTO_AJUSTE_DIF_CAMBIO);
		}

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		return credito;
	}

	private Boleto crearBoletoAjuste(Long idCredito, Date fecha, double saldoTotal) {
		// Crear comprobante ND o NC
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		// asociar con cuota segun periodo de devengamiento
		Object[] valores = (Object[]) bp.getNamedQuery("Cuota.findByRangoFechas").setParameter("idCredito", idCredito)
				.setParameter("fechaHasta", fecha).uniqueResult();
		int numeroCuota = 1;
		// valores = {menor vencida, mayor vencida}
		if (valores != null && valores[1] != null) {
			// mayor cuota vencida a la fecha + 1 = siguiente cuota
			numeroCuota = ((Number) valores[1]).intValue() + 1;
		}
		Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idCredito)
				.setParameter("nroCuota", numeroCuota).uniqueResult();
		if (cuota == null) {
			// si no hay cuota con ese numero, la vencida fue la ultima. usar la
			// vencida
			cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", idCredito)
					.setParameter("nroCuota", numeroCuota - 1).uniqueResult();
		}
		if (cuota == null) {
			return null;
		}

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		int periodo = Calendar.getInstance().get(Calendar.YEAR);

		Boleto comprobante = new Boleto();
		comprobante.setFechaEmision(fecha);
		comprobante.setFechaVencimiento(fecha);
		comprobante.setTipo(saldoTotal >= 0 ? Boleto.TIPO_BOLETO_MINUTADEB : Boleto.TIPO_BOLETO_MINUTACRED);
		comprobante.setUsuario(usuario);
		comprobante.setNumeroCuota(cuota.getNumero());
		comprobante.setObjetoi(credito);
		comprobante.setPeriodoBoleto((long) periodo);
		comprobante.setVerificadorBoleto(0L);
		comprobante.setImporte(Math.abs(saldoTotal));
		comprobante.setNumeroBoleto(Numerador.getNext(comprobante.getNumerador()));
		bp.save(comprobante);

		return comprobante;
	}

	private Objetoi guardarAjuste(Boleto comprobante, Cuota cuota, Long idCredito, double importeAjuste, Date fecha,
			Concepto concepto, double cotizacionFecha, String detalle, String tipoConcepto) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);

		Tipomov tipoMov = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", importeAjuste >= 0 ? Tipomov.TIPOMOV_DEBITO : Tipomov.TIPOMOV_CREDITO)
				.uniqueResult();

		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;

		// Crear ajuste en cta cte
		CtaCteAjuste cc = new CtaCteAjuste();

		cc.setTipomov(tipoMov);
		cc.setAsociado(concepto);
		cc.setFacturado(concepto);

		cc.setTipoMovimiento(Objetoi.TIPO_MOVIMIENTO_DIF_CAMBIO);
		cc.setTipoConcepto(tipoConcepto);
		cc.setDetalle(detalle);
		cc.setImporte(0.0);
		cc.redondear();
		cc.setFechaGeneracion(fecha);
		cc.setCuota(cuota);
		cc.setFechaVencimiento(fecha);
		cc.setBoleto(comprobante); // relacionado al comprobante

		cc.setCotizacionDiferencia(cotizacionFecha);
		cc.setDebePesos(importeAjuste >= 0 ? importeAjuste : 0);
		cc.setHaberPesos(importeAjuste >= 0 ? 0 : Math.abs(importeAjuste));
		cc.redondear();

		CtacteKey ck = new CtacteKey();
		ck.setItemCtacte(itemCtaCte);
		ck.setMovimientoCtacte(movimientoCtaCte);
		ck.setPeriodoCtacte(new Long(periodo));
		ck.setObjetoi_id(idCredito);
		// TODO: reemplazar con Digito Verificador
		ck.setVerificadorCtacte(0L);

		cc.setId(ck);

		bp.save(cc);

		// Crear detalle de comprobante
		Bolcon bc = credito.crearBolcon(concepto.getConcepto().getConcepto(), Math.abs(importeAjuste), comprobante,
				cc.getId(), cuota);
		bc.setTipomov(tipoMov);

		bp.save(bc);

		bp.getCurrentSession().flush();

		return credito;
	}

	public double getCotizacion(Long idMoneda, Long idMonedaCredito, Date fecha) {
		double cotizacion = 1;
		Moneda moneda = (Moneda) bp.getById(Moneda.class, idMonedaCredito);
		// decision de cotizacion
		// si moneda del credito NO es peso
		if (idMonedaCredito != null && !idMonedaCredito.equals(1L)) {
			// si el pago es en la misma moneda, tomar compra
			if (idMoneda != null && idMonedaCredito != null && idMoneda.equals(idMonedaCredito)) {
				cotizacion = moneda.getCotizacionCompra(fecha);
			} else if (idMoneda != null && idMoneda.equals(1L)) {
				// si el pago es en pesos, tomar venta
				cotizacion = moneda.getCotizacion(fecha);
			}
		}
		return cotizacion;
	}

	public void crearParteBBoletoResumen(Cuota cuota, Boleto boletoResumen) {
		CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(boletoResumen.getObjetoi_id(),
				cuota.getFechaVencimiento());
		calculoDeuda.calcular();
		List<BeanCtaCte> beans = calculoDeuda.getBeans();
		beans.remove(beans.size() - 1);
		double moratorio = 0.0;
		double punitorio = 0.0;
		String cuotasMoratorio = "";
		String cuotasPunitorio = "";
		for (BeanCtaCte bean : beans) {
			if (bean.getNumero().intValue() < cuota.getNumero().intValue()) {
				if (bean.isDebito()) {
					moratorio += bean.getMoratorio().doubleValue();
					punitorio += bean.getPunitorio().doubleValue();
				} else {
					moratorio -= bean.getMoratorio().doubleValue();
					punitorio -= bean.getPunitorio().doubleValue();
				}
				if (cuotasMoratorio.indexOf(bean.getNumero().toString()) == -1) {
					if (cuotasMoratorio.isEmpty()) {
						cuotasMoratorio += bean.getNumero().toString();
					} else {
						cuotasMoratorio += " - " + bean.getNumero().toString();
					}
				}
				if (cuotasPunitorio.indexOf(bean.getNumero().toString()) == -1) {
					if (cuotasPunitorio.isEmpty()) {
						cuotasPunitorio += bean.getNumero().toString();
					} else {
						cuotasPunitorio += " - " + bean.getNumero().toString();
					}
				}
			}
		}

		Number ordenMax = (Number) bp
				.createQuery("select max(d.orden) from BoletoResumenDetalle d where d.boletoResumen.id = :idBoleto")
				.setParameter("idBoleto", boletoResumen.getId()).uniqueResult();
		int ordenDetalle = 1;
		if (ordenMax != null) {
			ordenDetalle = ordenMax.intValue() + 1;
		}

		if (moratorio >= 0.01) {
			BoletoResumenDetalle detalleMor = new BoletoResumenDetalle();
			detalleMor.setNumero(cuotasMoratorio);
			detalleMor.setDetalle("Moratorio");
			detalleMor.setImporte(moratorio);
			detalleMor.setOrden(ordenDetalle);
			detalleMor.setBoletoResumen(boletoResumen);
			bp.save(detalleMor);

			ordenDetalle++;
		}

		if (punitorio >= 0.01) {
			BoletoResumenDetalle detallePun = new BoletoResumenDetalle();
			detallePun.setNumero(cuotasPunitorio);
			detallePun.setDetalle("Punitorio");
			detallePun.setImporte(punitorio);
			detallePun.setOrden(ordenDetalle);
			detallePun.setBoletoResumen(boletoResumen);
			bp.save(detallePun);
		}
	}

	public String getAreaResponsable(Objetoi credito) {
		ObjetoiEstado oe = credito.getEstadoActual();

		if (oe == null)
			return null;

		String nombreEstado = oe.getEstado().getNombreEstado();
		if (nombreEstado.equals(Estado.GESTION_JUDICIAL) || nombreEstado.equals(Estado.GESTION_EXTRAJUDICIAL)) {
			return credito.getEstadoActual().getAreaResponsable();
		} else {
			ObjetoiComportamiento oc = credito.getObjetoiComportamientoActual();
			if (oc != null) {
				return oc.getAreaResponsable();
			}
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public List<ActualizacionIndiceBean> actualizacionIndice(Long numeroAtencion, Long idIndice, Date actualizacion,
			boolean impactar) {
		Indice i = (Indice) bp.getById(Indice.class, idIndice);
		String concepto = i.getConcepto().getConcepto();

		// buscar ultima fecha de actualizacion
		Date ultimaActualizacion = (Date) bp
				.createSQLQuery("SELECT cc.fechaGeneracion FROM Ctacte cc "
						+ "JOIN Concepto con on cc.asociado_id = con.id and con.concepto_concepto = :concepto "
						+ "JOIN Objetoi o on cc.objetoi_id = o.id "
						+ "WHERE o.numeroAtencion = :numeroAtencion AND cc.tipoMovimiento = 'cuota' "
						+ "ORDER BY cc.fechaGeneracion DESC")
				.setParameter("numeroAtencion", numeroAtencion).setParameter("concepto", concepto).setMaxResults(1)
				.uniqueResult();

		// calcular diferencia de indice entre fechaActualizacionSaldo -
		// fechaUltimaActualizacion
		double movimiento;

		Calendar c = Calendar.getInstance();
		c.setTime(actualizacion);
		c.add(Calendar.DATE, -1);
		Date fechaIndice = c.getTime();

		double valorActual = buscarValorIndiceFecha(i.getId(), fechaIndice);
		if (ultimaActualizacion != null) {
			c.setTime(ultimaActualizacion);
			c.add(Calendar.DATE, -1);
			ultimaActualizacion = c.getTime();
			double valorInicial = buscarValorIndiceFecha(i.getId(), ultimaActualizacion);
			movimiento = valorActual - valorInicial;
		} else {
			movimiento = valorActual;
		}

		List<ActualizacionIndiceBean> result = new ArrayList<ActualizacionIndiceBean>();

		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		Objetoi credito = (Objetoi) bp.createQuery("select o from Objetoi o where o.numeroAtencion = :numeroAtencion")
				.setParameter("numeroAtencion", numeroAtencion).uniqueResult();
		long movimientoCtacte = 0;
		long itemCtacte = 1;

		Boleto boleto = null;
		if (impactar) {
			boleto = new Boleto();
			boleto.setFechaEmision(actualizacion);
			boleto.setFechaVencimiento(actualizacion);
			boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			boleto.setUsuario(usuario);
			boleto.setObjetoi(credito);
			boleto.setPeriodoBoleto((long) periodo);
			boleto.setVerificadorBoleto(0L);
			boleto.setImporte(0.0);
			boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
			bp.save(boleto);

			movimientoCtacte = Numerador.getNext(numeradorCtaCte);
		}

		double importeBoleto = 0;

		List<Object[]> saldos = bp.createSQLQuery(
				"SELECT SUM(CASE WHEN cc.tipomov_id = 1 THEN -cc.importe ELSE cc.importe END) AS saldo, c.id, "
						+ "SUM(CASE WHEN cc.tipomov_id = 1 THEN cc.importe ELSE 0 END) AS sumaPagos FROM Ctacte cc JOIN Cuota c on cc.cuota_id = c.id "
						+ "JOIN Concepto con on cc.asociado_id = con.id and con.concepto_concepto = 'cap' JOIN Objetoi o on cc.objetoi_id = o.id WHERE o.numeroAtencion = :numeroAtencion "
						+ "AND cc.fechaGeneracion <= :fechaGeneracion GROUP BY c.id ORDER BY c.id")
				.setParameter("numeroAtencion", numeroAtencion).setParameter("fechaGeneracion", actualizacion).list();

		ActualizacionIndiceBean primeraTotalmenteImpaga = null;
		Ctacte ccImpaga = null;
		Bolcon bcImpaga = null;
		ActualizacionIndice actualizacionCuotaImpaga = null;

		for (Object[] r : saldos) {
			double saldoCapital = r[0] != null ? ((Number) r[0]).doubleValue() : 0;

			// cuota saldada
			if (saldoCapital < 0.01) {
				continue;
			}

			Long idCuota = ((Number) r[1]).longValue();
			Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);

			ActualizacionIndiceBean b;

			// si tiene pagos parciales, la cuota se actualiza sola
			if (r[2] != null && ((Number) r[2]).doubleValue() > 0) {
				b = new ActualizacionIndiceBean();
				b.setNumeroCuota(cuota.getNumero());
				b.setIndice(movimiento);
			} else {
				// si esta totalmente impaga, se agrupa con la primera
				// totalmente impaga
				if (primeraTotalmenteImpaga == null) {
					primeraTotalmenteImpaga = new ActualizacionIndiceBean();
					primeraTotalmenteImpaga.setNumeroCuota(cuota.getNumero());
					primeraTotalmenteImpaga.setIndice(movimiento);
				}
				b = primeraTotalmenteImpaga;
			}

			double montoActualizacion = (saldoCapital * (movimiento + 1) - saldoCapital);
			b.setSaldoCapital(b.getSaldoCapital() + saldoCapital);
			b.setMontoActualizacion(b.getMontoActualizacion() + montoActualizacion);

			if (impactar) {
				importeBoleto += montoActualizacion;

				if (primeraTotalmenteImpaga != null) {
					if (ccImpaga == null) {
						ccImpaga = credito.crearCtacte(concepto, montoActualizacion, actualizacion, boleto, credito,
								cuota, Ctacte.TIPO_CONCEPTO_ACTUALIZACION_INDICE);
						ccImpaga.setTipomov(tipoDebito);
						ccImpaga.setUsuario(usuario);
						ccImpaga.getId().setMovimientoCtacte(movimientoCtacte);
						ccImpaga.getId().setItemCtacte(itemCtacte++);
						ccImpaga.setBoleto(boleto);
						ccImpaga.setDetalle("Actualizacion por Indice " + i.getNombre() + " " + b.getIndiceStr());
						ccImpaga.setContabiliza(true);
						bp.save(ccImpaga);
					} else {
						ccImpaga.setImporte(ccImpaga.getImporte() + montoActualizacion);
						ccImpaga.redondear();
						bp.update(ccImpaga);
					}

					if (bcImpaga == null) {
						bcImpaga = credito.crearBolcon(concepto, montoActualizacion, boleto, ccImpaga.getId(), cuota);
						bcImpaga.setTipomov(tipoDebito);
						bp.save(bcImpaga);
					} else {
						bcImpaga.setImporte(bcImpaga.getImporte() + montoActualizacion);
						bp.update(bcImpaga);
					}

					if (actualizacionCuotaImpaga == null) {
						actualizacionCuotaImpaga = new ActualizacionIndice();
						actualizacionCuotaImpaga.setCtacte(ccImpaga);
						actualizacionCuotaImpaga.setIndiceValor(movimiento);
						actualizacionCuotaImpaga.setSaldoCapital(saldoCapital);
						actualizacionCuotaImpaga.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
						bp.save(actualizacionCuotaImpaga);
					} else {
						actualizacionCuotaImpaga
								.setSaldoCapital(actualizacionCuotaImpaga.getSaldoCapital() + saldoCapital);
						bp.update(actualizacionCuotaImpaga);
					}
				} else {
					Ctacte cc = credito.crearCtacte(concepto, montoActualizacion, actualizacion, boleto, credito, cuota,
							Ctacte.TIPO_CONCEPTO_ACTUALIZACION_INDICE);
					cc.setTipomov(tipoDebito);
					cc.setUsuario(usuario);
					cc.getId().setMovimientoCtacte(movimientoCtacte);
					cc.getId().setItemCtacte(itemCtacte++);
					cc.setBoleto(boleto);
					cc.setDetalle("Actualizacion por Indice " + i.getNombre() + " " + b.getIndiceStr());
					cc.setContabiliza(true);

					Bolcon bolcon = credito.crearBolcon(concepto, montoActualizacion, boleto, cc.getId(), cuota);
					bolcon.setTipomov(tipoDebito);

					ActualizacionIndice actIndice = new ActualizacionIndice();
					actIndice.setCtacte(cc);
					actIndice.setIndiceValor(movimiento);
					actIndice.setSaldoCapital(saldoCapital);
					actIndice.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());

					bp.save(cc);
					bp.save(bolcon);
					bp.save(actIndice);
				}
			}

			if (!result.contains(b)) {
				result.add(b);
			}
		}

		if (impactar) {
			boleto.setImporte(importeBoleto);
			bp.update(boleto);
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	public void comprobarInmovilizacion(Long idCredito) {
		// misma consulta que la inmovilizacion pendiente + que no haya
		// inmovilizaciones solicitadas

		// Ticket 9949
		CosechaConfig cosecha;

		List<Objetoi> c = bp.createQuery(
				"select distinct c1 from ObjetoiVinedo v, Desembolso d join v.credito c1 join d.credito c2 "
						+ "where c1 = c2 and v.fechaSolicitudInforme2 is not NULL and v.fechaAceptadoInforme2 is NULL and d.numero = 1 and d.estado = '2' and v.credito.id = :idCredito "
						+ "and c1.id not in (select distinct i.objetoi.id from Inmovilizaciones i where i.objetoi.id = :idCredito and i.estado = :solicitada)")
				.setParameter("idCredito", idCredito).setParameter("solicitada", EstadoInmovilizacion.SOLICITADA)
				.list();

		if (c != null && !c.isEmpty()) {
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);

			// buscar producto actual
			String producto = (String) bp.createSQLQuery(
					"select distinct g.tipoProducto from ObjetoiGarantia og join Garantia g on og.garantia_id = g.id "
							+ "where og.objetoi_id = :idCredito and g.tipoProducto is not null")
					.setParameter("idCredito", idCredito).setMaxResults(1).uniqueResult();

			try {
				cosecha = (CosechaConfig) bp.getNamedQuery("CosechaConfig.findByFecha")
						.setParameter("fecha", new Date()).setParameter("varietales", credito.getVarietales());
			} catch (Exception e) {
				cosecha = null;
			}

			Inmovilizaciones inmovilizacion = new Inmovilizaciones();
			inmovilizacion.setObjetoi(credito);
			inmovilizacion.setFecha(new Date());
			inmovilizacion.setVolumen(new Double(credito.getDepositoMinimo().replaceAll(",", ".")));
			inmovilizacion.setEstado(EstadoInmovilizacion.SOLICITADA);
			inmovilizacion.setProducto(producto);
			// Ticket 9949
			if (cosecha != null) {
				inmovilizacion.setAforo(cosecha.getAforo().floatValue());
				switch (credito.getTipoProducto()) {
				case "1":
					inmovilizacion.setPrecioProducto(cosecha.getPrecioVinoTinto().floatValue());
				case "2":
					inmovilizacion.setPrecioProducto(cosecha.getPrecioVinoBlanco().floatValue());
				case "3":
					inmovilizacion.setPrecioProducto(cosecha.getPrecioMosto().floatValue());
				case "4":
					inmovilizacion.setPrecioProducto(cosecha.getPrecioOtrosVarietales().floatValue());
				}
				inmovilizacion.setImporte(inmovilizacion.getVolumen().floatValue() * inmovilizacion.getPrecioProducto()
						/ inmovilizacion.getAforo());
			}

			bp.save(inmovilizacion);
		}
	}

}
