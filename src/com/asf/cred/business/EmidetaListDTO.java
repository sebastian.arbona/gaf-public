package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Emideta;

public class EmidetaListDTO implements Serializable {

	private static final long serialVersionUID = -4923466649020122643L;

	private Emideta emideta;
	private Date fechaVto;
	private String auditoriaFinalPos;

	public EmidetaListDTO() {
	}
	
	public EmidetaListDTO(Emideta e, Date f) {
		emideta = e;
		fechaVto = f;
		AuditoriaFinal auditoriaFinal = (AuditoriaFinal)SessionHandler.getCurrentSessionHandler().getBusinessPersistance().createQuery("Select n from AuditoriaFinal n where n.credito.id=:idObjetoi order by id desc")
				.setParameter("idObjetoi", e.getCredito().getId()).setMaxResults(1).uniqueResult();
		if (auditoriaFinal==null) {
			setAuditoriaFinalPos("");
		} else {
			setAuditoriaFinalPos( auditoriaFinal.getAplicaFondo());
		}
	}

	public EmidetaListDTO(Emideta e) {
		emideta = e;
	}
	
	public Emideta getEmideta() {
		return emideta;
	}

	public Date getFechaVto() {
		return fechaVto;
	}

	public void setEmideta(Emideta emideta) {
		this.emideta = emideta;
	}

	public void setFechaVto(Date fechaVto) {
		this.fechaVto = fechaVto;
	}
	
	public String getFechaVtoStr(){
		return DateHelper.getString(fechaVto);
	}

	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}
}
