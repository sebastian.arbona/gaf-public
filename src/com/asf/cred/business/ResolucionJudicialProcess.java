package com.asf.cred.business;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.AbstractEstado;
import com.civitas.hibernate.persona.Archivo;
import com.civitas.hibernate.persona.EstaPer;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.civitas.hibernate.persona.PersonaVinculada;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.nirven.creditos.hibernate.DocumentoJudicial;
import com.nirven.creditos.hibernate.DocumentoMovimientoInstancia;
import com.nirven.creditos.hibernate.DocumentoResolucion;
import com.nirven.creditos.hibernate.Especialidad;
import com.nirven.creditos.hibernate.FuncionPersonaResolucion;
import com.nirven.creditos.hibernate.MovimientoInstancia;
import com.nirven.creditos.hibernate.NotificacionPersonaInstancia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObservacionRelevante;
import com.nirven.creditos.hibernate.ProcesoResolucion;
import com.nirven.creditos.hibernate.Resolucion;
import com.nirven.creditos.hibernate.TipoMovimientoInstancia;
import com.nirven.creditos.hibernate.TipoProceso;
import com.nirven.creditos.hibernate.TipoResolucion;
import com.nirven.creditos.hibernate.Tribunal;
import com.nirven.mailer.Mail;

public class ResolucionJudicialProcess implements IProcess {

	private String accion = "";
	private Long idSolicitud;
	private String forward = "errorPage";
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;

	// Atributos

	private List<Resolucion> resoluciones;
	private List<FuncionPersonaResolucion> personas;
	private List<ProcesoResolucion> instancias;
	private List<DocumentoJudicial> archivos;
	private List<DocumentoMovimientoInstancia> archivosMovimientoInstancia;
	private List<DocumentoResolucion> archivosResolucion;
	private List<FuncionPersonaResolucion> funcionesPersonasResolucion;
	private List<NotificacionPersonaInstancia> notificacionesPersonasInstancia;
	private List<MovimientoInstancia> movimientosInstancia;
	private List<ObservacionRelevante> observacionesRelevante;

	private String idproyectoPop;
	private String idResolucion;
	private Long nroResolucion;
	private String expediente;
	private String juzgado;
	private Double montoDemandado;
	private Date fecha;
	private Date fechaInicioFuncion;
	private Date fechaFinFuncion;
	private String observaciones;
	private boolean editable = true;
	private Long idSubTipoResolucion;
	private Long idSubTipoResolucion2;
	private Long idTipoResolucion;
	private Long tipoProceso;
	private Long tipoProceso2;
	private Long idFuncion;
	private Long idPersona;
	private Long idPersonaEspecialista;
	private Long idEspecialidad;
	private Long idDocumentoResolucion;
	private String sid;
	private Resolucion resolucion = new Resolucion();
	private FuncionPersonaResolucion funcionPersonResolucion = new FuncionPersonaResolucion();
	private NotificacionPersonaInstancia notificacionPesonaInstancia = new NotificacionPersonaInstancia();
	private int funcionPersonaResolucionQuitar;
	private int notificacionPersonaInstanciaQuitar;
	private ProcesoResolucion procesoResolucion = new ProcesoResolucion();
	private FormFile theFile;
	private String detalleDocumentoResolucion;
	private String persona;
	private Long personaid;
	private Long idArchivo;
	private Boolean definitivo = true;
	private Long idCn = null;
	private Boolean destinatarioMail = true;
	private Boolean notificar = true;
	private Estado estado;
	private Objetoi objetoi;
	private Long numeroAtencion;
	private String numeroResolucion;
	private Long idAbogado;
	private String observacionesArchivo;
	private Boolean enviarAviso = true;
	private String cadenaPersonaSeleccionada;
	private String current_emails;
	private String opcion = "1";
	private Boolean nuevo = true;

	private Long idTribunal;
	private Long idTipoTribunal;

	private MovimientoInstancia movimientoInstancia = new MovimientoInstancia();
	private TipoMovimientoInstancia tipoMovimientoInstancia = new TipoMovimientoInstancia();
	private ObservacionRelevante observacionRelevante = new ObservacionRelevante();
	private String tipoMovimientoIntanciaStr = "";

	public ResolucionJudicialProcess() {

		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@Override
	public boolean doProcess() {
		if (this.accion.equals("list")) {
			this.listarSolicitudes();
		} else if (this.accion.equals("listFiltro")) {
			this.listarSolicitudesFiltro();
		} else if (this.accion.equals("nuevo")) {
			setNuevo(true);
			this.resolucion = new Resolucion();
			setearEspecialidadPorDefecto();
			this.forward = "Resolucion";
		} else if (this.accion.equals("guardar")) {
			this.guardarResolucion();
			this.listarSolicitudes();
			this.forward = "ResolucionList";
		} else if (accion.trim().equalsIgnoreCase("show")) {
			setNuevo(false);
			cargarResolucion();
			this.forward = "Resolucion";
		} else if (this.accion.equals("persona")) {
			this.agregarPersonasResolucion();
			this.forward = "InstanciaJudicial";
		} else if (this.accion.equals("personaBlanco")) {
			this.agregarPersonasResolucionBlanco();
			this.forward = "InstanciaJudicialBlanco";
		} else if (accion.trim().equalsIgnoreCase("quitarPersona")) {
			return eliminarPersona();
		} else if (accion.trim().equalsIgnoreCase("quitarPersonaBlanco")) {
			return eliminarPersonaBlanco();
		} else if (this.accion.equals("personaNotificacion")) {
			this.agregarPersonasNotificacion();
			this.forward = "InstanciaJudicial";
		} else if (this.accion.equals("personaNotificacionBlanco")) {
			this.agregarPersonasNotificacionBlanco();
			this.forward = "InstanciaJudicialBlanco";
		} else if (accion.trim().equalsIgnoreCase("quitarPersonaNotificacion")) {
			return eliminarPersonaNotificacion();
		} else if (accion.trim().equalsIgnoreCase("quitarPersonaNotificacionBlanco")) {
			return eliminarPersonaNotificacionBlanco();
		} else if (this.accion.equals("nuevoSubTipoResolucion")) {
			this.forward = "SubTipoResolucionList";

		} else if (accion.trim().equalsIgnoreCase("nuevoDocumentoResolucion")) {
			this.forward = "DocumentoResolucion";

		} else if (accion.trim().equalsIgnoreCase("listarDocumentosResolucion")) {
			resolucion = (Resolucion) bp.getById(Resolucion.class, this.idSolicitud);
			listarDocumentosResolucion();

		} else if (accion.trim().equalsIgnoreCase("guardarDocumentoResolucion")) {
			guardarDocumentosResolucion();
			listarDocumentosResolucion();

		} else if (accion.trim().equalsIgnoreCase("eliminarDocumentoResolucion")) {
			bp.delete(DocumentoResolucion.class, this.idDocumentoResolucion);
			listarDocumentosResolucion();

		} else if (accion.trim().equalsIgnoreCase("nuevaInstancia")) {

			nuevaInstancia();

		} else if (accion.trim().equalsIgnoreCase("nuevaInstanciaBlanco")) {

			nuevaInstanciaBlanco();

		} else if (accion.trim().equalsIgnoreCase("nuevoMovimiento")) {

			nuevoMovimiento(true);

		} else if (accion.trim().equalsIgnoreCase("nuevoMovimientoBlanco")) {

			nuevoMovimiento(false);

		} else if (accion.trim().equalsIgnoreCase("guardarMovimiento")) {

			guardarMovimiento(true);

		} else if (accion.trim().equalsIgnoreCase("guardarMovimientoBlanco")) {

			guardarMovimiento(false);

		} else if (accion.trim().equalsIgnoreCase("guardarInstancia")
				|| accion.trim().equalsIgnoreCase("guardarInstanciaBlanco")) {

			guardarInstancia();

		} else if (accion.trim().equalsIgnoreCase("showInstancia")) {

			cargarInstancia();

		} else if (accion.trim().equalsIgnoreCase("showInstanciaBlanco")) {

			cargarInstanciaBlanco();

		} else if (accion.trim().equalsIgnoreCase("showMovimiento")) {

			cargarMovimiento(true);

		} else if (accion.trim().equalsIgnoreCase("showMovimientoBlanco")) {

			cargarMovimiento(false);

		} else if (accion.trim().equalsIgnoreCase("eliminarMovimiento")) {

			eliminarMovimiento(true);

		} else if (accion.trim().equalsIgnoreCase("eliminarMovimientoBlanco")) {

			eliminarMovimiento(false);

		} else if (accion.trim().equalsIgnoreCase("eliminarInstancia")) {

			eliminarInstancia();

		} else if (accion.trim().equalsIgnoreCase("eliminarInstanciaBlanco")) {

			eliminarInstanciaBlanco();

		} else if (accion.trim().equalsIgnoreCase("listarInstancias")) {
			Resolucion resolucion = (Resolucion) bp.getById(Resolucion.class, this.idSolicitud);
			resolucion.getId();
			/*
			 * if(resolucion.getProyecto() ==null) { Objetoi o = new Objetoi();
			 * o.setNumeroAtencion(0L); resolucion.setProyecto(o);; }
			 */
			procesoResolucion.setResolucion(resolucion);
			listarInstancias();

		} else if (accion.trim().equalsIgnoreCase("listarMovimientos")) {
			// Resolucion resolucion = (Resolucion) bp.getById(Resolucion.class,
			// this.idSolicitud);
			// resolucion.getId();
			procesoResolucion.setResolucion(resolucion);
			listarMovimientos(true);

		} else if (accion.trim().equalsIgnoreCase("listarMovimientosBlanco")) {
			// Resolucion resolucion = (Resolucion) bp.getById(Resolucion.class,
			// this.idSolicitud);
			// resolucion.getId();
			// procesoResolucion.setResolucion(resolucion);
			listarMovimientos(false);

		} else if (accion.trim().equalsIgnoreCase("listarInstanciasFiltro")
				|| accion.trim().equalsIgnoreCase("listarInstanciasFiltroInicio")) {

			// procesoResolucion.setResolucion((Resolucion)bp.getById(Resolucion.class,
			// this.idSolicitud));
			// listarInstanciasFiltro();
			long id;
			if (this.idSolicitud == 0 || this.idSolicitud == null)
				id = Long.parseLong(idResolucion);
			else
				id = idSolicitud;
			Resolucion resolucion = (Resolucion) bp.getById(Resolucion.class, id);
			resolucion.getId();
			procesoResolucion.setResolucion(resolucion);
			listarInstanciasBlanco();
		} else if (accion.trim().equalsIgnoreCase("listarResolucionFiltroInicio")) {
			listarResolucionBlancoFiltro();
		} else if (accion.trim().equalsIgnoreCase("eliminarArchivoBlanco")) {

			eliminarArchivoBlanco();

		} else if (accion.trim().equalsIgnoreCase("eliminarArchivo")) {

			eliminarArchivo();

		} else if (accion.trim().equalsIgnoreCase("eliminarDocumentosMovimientoInstancia")) {

			eliminarDocumentosMovimientoInstancia(true);

		} else if (accion.trim().equalsIgnoreCase("eliminarDocumentosMovimientoInstanciaBlanco")) {

			eliminarDocumentosMovimientoInstancia(false);

		} else {
			this.listarSolicitudes();
			this.forward = "ResolucionList";
		}
		return this.errores.isEmpty();
	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public void setearEspecialidadPorDefecto() {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Especialidad especialidad = (Especialidad) bp.createQuery("From Especialidad e where e.nombre = :nombre")
				.setParameter("nombre", "Abogado").uniqueResult();

		if (especialidad != null) {

			this.resolucion.getEspecialista().setEspecialidad(especialidad);
		}

	}

	@Override
	public boolean validate() {

		if (this.accion.equals("guardar")
				&& (this.resolucion.getNumero() == null || this.resolucion.getNumero().isEmpty())) {

			errores.put("resolucion.proceso.numeroAtencion.error", "resolucion.proceso.numeroAtencion.error");
		}
		if (this.accion.equals("eliminarInstancia")) {

			List<DocumentoJudicial> documentosAsociados = (List<DocumentoJudicial>) bp
					.createQuery("from DocumentoJudicial d where d.procesoResolucion.id = :idInstancia")
					.setParameter("idInstancia", this.procesoResolucion.getId()).list();

			if (documentosAsociados.size() > 0) {

				errores.put("resolucion.proceso.numeroAtencion.error", "resolucion.proceso.numeroAtencion.error");

			}

			List<FuncionPersonaResolucion> personasAsociadas = (List<FuncionPersonaResolucion>) bp
					.createQuery("from FuncionPersonaResolucion d where d.instancia.id = :idInstancia")
					.setParameter("idInstancia", this.procesoResolucion.getId()).list();

			if (personasAsociadas.size() > 0) {

				errores.put("resolucion.proceso.numeroAtencion.error", "resolucion.proceso.numeroAtencion.error");

			}

		}

		if (this.accion.equals("guardarMovimiento")) {
			if (movimientoInstancia.getTipoMovimientoInstancia().getId() == null) {
				// movimientoinstancia.tipomovimiento

				errores.put("movimientoinstancia.tipomovimiento", "movimientoinstancia.tipomovimiento");
			}

			if (movimientoInstancia.getFechaMovimiento() == null) {
				// movimientoinstancia.fechacargamovimiento
				errores.put("movimientoinstancia.fechamovimiento", "movimientoinstancia.fechamovimiento");
			}
		}

		if (errores.size() > 0) {

			return false;
		}

		return true;
	}

	// Metodos internos
	private void listarSolicitudes() {

		String consulta = "";
		consulta = "SELECT r FROM Resolucion r WHERE ";
		if (opcion.equals("1")) {
			consulta += "r.persona.id is null";
		} else
			consulta += "r.proyecto.id is null";

		resoluciones = bp.createQuery(consulta).list();
		this.forward = "ResolucionList";

	}

	// Metodos internos
	@SuppressWarnings("unchecked")
	private void listarSolicitudesFiltro() {
		Persona per;
		// per.getNomb12()
		String consulta = "";
		consulta = "SELECT r FROM Resolucion r";
		String filtro = " WHERE ";
		if (opcion.equals("1")) {
			filtro += "r.persona.id is null";
			if (this.numeroAtencion != null && this.numeroAtencion.longValue() > 0) {

				Long idObjetoI = (Long) bp
						.createQuery("select id from Objetoi o where o.numeroAtencion = :numeroAtencion")
						.setParameter("numeroAtencion", this.numeroAtencion).uniqueResult();

				filtro += " AND r.proyecto.id = " + idObjetoI;
			}
		} else {
			filtro += "r.proyecto.id is null";
			if (!persona.equals("")) {
				filtro += " AND r.persona.nomb12 like '%" + persona.toUpperCase() + "%'";
			}

		}

		if (this.numeroResolucion != null && !this.numeroResolucion.isEmpty()) {

			if (filtro.length() > 0)
				filtro = filtro + " AND r.numero LIKE '%" + this.numeroResolucion + "%'";
			else
				filtro = " WHERE r.numero LIKE '%" + this.numeroResolucion + "%'";

		}

		if (this.idAbogado != null && this.idAbogado.longValue() > 0) {

			if (filtro.length() > 0)
				filtro = filtro + " AND r.especialista.persona.id=" + this.idAbogado;
			else
				filtro = " WHERE r.especialista.persona.id=" + this.idAbogado;

		}

		resoluciones = bp.createQuery(consulta + filtro).list();
		this.forward = "ResolucionList";

	}

	private void guardarResolucion() {

		Persona per = null;
		Objetoi oi = null;
		String[] parts = persona.split("-");

		/*
		 * HttpSession session =
		 * SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		 */
		@SuppressWarnings("unused")
		Resolucion resolucionActualizada = resolucion;
		// resolucion = (Resolucion) session.getAttribute(sid);
		// actualizarResolucion(resolucionActualizada);
		bp.begin();
		if (this.idproyectoPop == null || this.idproyectoPop == "" || this.idproyectoPop.isEmpty()
				|| idproyectoPop.equals("")) {
			if (parts.length > 0) {
				long perid = (new Long(parts[0])).longValue();
				per = (Persona) bp.createQuery("select p from Persona p where p.id = :id").setParameter("id", perid)
						.uniqueResult();
			}
		} else {
			oi = (Objetoi) bp.createQuery("Select o from Objetoi o where o.numeroAtencion = :numeroAtencion")
					.setParameter("numeroAtencion", new Long(this.idproyectoPop)).uniqueResult();
		}
		TipoResolucion tipoRes = (TipoResolucion) bp.getById(TipoResolucion.class, this.idTipoResolucion);
		Resolucion resolucion = new Resolucion();
		resolucion.setId(this.resolucion.getId());
		resolucion.setFecha(this.resolucion.getFecha());
		resolucion.setNumero(this.resolucion.getNumero());
		resolucion.setObservaciones(this.resolucion.getObservaciones());
		resolucion.setTipoResolucion(tipoRes);
		resolucion.setDefinitivo(this.getDefinitivo());
		resolucion.setEspecialista(idPersonaEspecialista, idEspecialidad);
		if (oi != null) {
			resolucion.setProyecto(oi);
			resolucion.setPersona(per);
		} else {
			if (per != null) {
				resolucion.setPersona(per);
				resolucion.setProyecto(oi);
			}
		}
		bp.saveOrUpdate(resolucion);
		if (resolucion.getDefinitivo()) {
			DocumentoResolucion docJud = new DocumentoResolucion();
			if (theFile.getFileSize() != 0) {
				docJud.setMimetype(theFile.getContentType());
				docJud.setNombre(theFile.getFileName());
				docJud.setResolucion(resolucion);
				docJud.setFecha(resolucion.getFecha());
				docJud.setDetalle(this.getObservacionesArchivo());
				// byte[] fileData = theFile.getFileData();
				// guarda los datos del fichero
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				InputStream stream = null;
				try {
					stream = theFile.getInputStream();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				byte[] buffer = new byte[40000];
				int bytesLeidos = 0;
				try {
					while ((bytesLeidos = stream.read(buffer, 0, 40000)) != -1) {
						baos.write(buffer, 0, bytesLeidos);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				docJud.setArchivo(baos.toByteArray());
				bp.saveOrUpdate(docJud);
			} else if (docJud.getId() != null && docJud.getId() != 0) {
				DocumentoJudicial a = (DocumentoJudicial) bp.getById(DocumentoJudicial.class, docJud.getId());
				docJud.setMimetype(a.getMimetype());
				docJud.setNombre(a.getNombre());
				docJud.setArchivo(a.getArchivo());
				docJud.setResolucion(resolucion);
				docJud.setDetalle(this.getObservacionesArchivo());
				bp.getCurrentSession().evict(a);
				bp.saveOrUpdate(docJud);
			}
		}

		if (oi != null && this.resolucion.getId() == null) {
			// Se cambia el estado al proyecto si se da de alta la instancia "ALTA"
			this.cambiarEstadoPersona(resolucion.getProyecto());
			this.guardarNuevoEstado(resolucion.getProyecto());
		}

		// Se envia el mail a las personas seleccionadas
		if (this.enviarAviso) {
			try {
				if (!idCn.equals(new Long(0))) {
					ConfiguracionNotificacion cn = (ConfiguracionNotificacion) SessionHandler.getCurrentSessionHandler()
							.getBusinessPersistance().getById(ConfiguracionNotificacion.class, idCn);
					String mensaje;
					if (oi != null) {
						mensaje = "Resolucion: " + resolucion.getNumero() + " - Proyecto: "
								+ resolucion.getProyecto().getNumeroAtencionStr() + " - Observaciones: "
								+ resolucion.getObservaciones();
					} else {
						mensaje = "Resolucion: " + resolucion.getNumero() + " - Persona: " + persona + " - "
								+ "Observaciones: " + resolucion.getObservaciones();

					}
					if (this.destinatarioMail) {
						Mail.sendMailTemplate(resolucion.getEspecialista().getPersona().getEmailPrincipal(),
								cn.getAsunto(), "mails/templateMail.html", mensaje);
					} else {
						// Se envia el mail al abogado
						Mail.sendMailTemplate(resolucion.getEspecialista().getPersona().getEmailPrincipal(),
								cn.getAsunto(), "mails/templateMail.html", mensaje);
						// Se envia el mail al titular
						Mail.sendMailTemplate(resolucion.getProyecto().getPersona().getEmailPrincipal(), cn.getAsunto(),
								"mails/templateMail.html", mensaje);
					}
					for (String mail : formatearMails()) {
						Mail.sendMailTemplate(mail.trim(), cn.getAsunto(), "mails/templateMail.html", mensaje);
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		bp.commit();
	}

	private String[] formatearMails() {
		String[] result = {};
		if (this.current_emails != null && !this.current_emails.equals("")) {
			String mailFormateado = this.current_emails.replace("\\", "").replace("[", "").replace("]", "")
					.replace("\"", " ").trim();
			result = mailFormateado.split(",");
		}
		return result;
	}

	/*
	 * Busca las personas que se han agregado para una resoluci�n y crea la
	 * asociaci�n.
	 */
	@SuppressWarnings("unchecked")
	public void agregarPersonasResolucion() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) session
				.getAttribute(sid + "funcionesPersonasResolucion");
		funcionPersonResolucion.setInstancia(procesoResolucion);
		funcionesPersonasResolucion.add(funcionPersonResolucion);
		funcionPersonResolucion = new FuncionPersonaResolucion(); // para agregar el siguiente
	}

	@SuppressWarnings("unchecked")
	public void agregarPersonasNotificacion() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		notificacionesPersonasInstancia = (List<NotificacionPersonaInstancia>) session
				.getAttribute(sid + "notificacionesPersonasInstancia");
		notificacionPesonaInstancia.setInstancia(procesoResolucion);
		String[] listaPersonas = cadenaPersonaSeleccionada.split("-");
		for (String idPersona : listaPersonas) {
			FuncionPersonaResolucion fpr = (FuncionPersonaResolucion) bp.getById(FuncionPersonaResolucion.class,
					new Long(idPersona));
			notificacionPesonaInstancia.setPersona(fpr.getPersona());
			notificacionPesonaInstancia.setIdPersona(fpr.getPersona().getIdpersona().toString());
			notificacionesPersonasInstancia.add(notificacionPesonaInstancia);
			notificacionPesonaInstancia = new NotificacionPersonaInstancia();
		}
		funcionesPersonasResolucion = (ArrayList<FuncionPersonaResolucion>) bp
				.createQuery("from FuncionPersonaResolucion fpr where fpr.instancia.resolucion.id = "
						+ procesoResolucion.getResolucion().getId())
				.list();
	}

	@SuppressWarnings("unchecked")
	public void agregarPersonasResolucionBlanco() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp
				.createQuery("from Resolucion r where r.numero =" + this.resolucion.getNumero()).uniqueResult());
		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) session
				.getAttribute(sid + "funcionesPersonasResolucion");
		funcionPersonResolucion.setInstancia(procesoResolucion);
		funcionesPersonasResolucion.add(funcionPersonResolucion);
		funcionPersonResolucion = new FuncionPersonaResolucion(); // para agregar el siguiente
	}

	@SuppressWarnings("unchecked")
	public void agregarPersonasNotificacionBlanco() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp
				.createQuery("from Resolucion r where r.numero =" + this.resolucion.getNumero()).uniqueResult());
		notificacionesPersonasInstancia = (List<NotificacionPersonaInstancia>) session
				.getAttribute(sid + "notificacionesPersonasInstancia");
		notificacionPesonaInstancia.setInstancia(procesoResolucion);
		notificacionesPersonasInstancia.add(notificacionPesonaInstancia);
		notificacionPesonaInstancia = new NotificacionPersonaInstancia(); // para agregar el siguiente

	}

	@SuppressWarnings("unused")
	private void actualizarResolucion(Resolucion resolucionActualizada) {
		resolucion.setFecha(resolucionActualizada.getFecha());
		resolucion.setFechaStr(resolucionActualizada.getFechaStr());
		resolucion.setObservaciones(resolucionActualizada.getObservaciones());
		resolucion.setTipoResolucion(resolucionActualizada.getTipoResolucion());
		resolucion.setNumero(resolucionActualizada.getNumero());

	}

	@SuppressWarnings("unchecked")
	public boolean eliminarPersona() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) session
				.getAttribute(sid + "funcionesPersonasResolucion");
		this.forward = "InstanciaJudicial";
		FuncionPersonaResolucion funcionesPersonasResolucionEliminar = funcionesPersonasResolucion
				.get(funcionPersonaResolucionQuitar - 1);
		List<NotificacionPersonaInstancia> listaNotificacionesPersona = (List<NotificacionPersonaInstancia>) bp
				.createQuery("From NotificacionPersonaInstancia npi where npi.persona.idpersona = "
						+ funcionesPersonasResolucionEliminar.getPersona().getId())
				.list();
		if (listaNotificacionesPersona.isEmpty()) {
			funcionesPersonasResolucion.remove(funcionPersonaResolucionQuitar - 1);
		} else {
			errores.put("resolucion.persona.borrar.error", "resolucion.persona.borrar.error");
			return false;
		}
		return true;

	}

	@SuppressWarnings("unchecked")
	public boolean eliminarPersonaNotificacion() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		notificacionesPersonasInstancia = (List<NotificacionPersonaInstancia>) session
				.getAttribute(sid + "notificacionesPersonasInstancia");
		this.forward = "InstanciaJudicial";
		@SuppressWarnings("unused")
		NotificacionPersonaInstancia notificacionesPersonasInstanciaEliminar = notificacionesPersonasInstancia
				.get(notificacionPersonaInstanciaQuitar - 1);
		notificacionesPersonasInstancia.remove(notificacionPersonaInstanciaQuitar - 1);
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean eliminarPersonaBlanco() {

		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp
				.createQuery("from Resolucion r where r.numero =" + this.resolucion.getNumero()).uniqueResult());
		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) session
				.getAttribute(sid + "funcionesPersonasResolucion");
		this.forward = "InstanciaJudicialBlanco";
		FuncionPersonaResolucion funcionesPersonasResolucionEliminar = funcionesPersonasResolucion
				.get(funcionPersonaResolucionQuitar - 1);
		List<NotificacionPersonaInstancia> listaNotificacionesPersona = (List<NotificacionPersonaInstancia>) bp
				.createQuery("From NotificacionPersonaInstancia npi where npi.persona.idpersona = "
						+ funcionesPersonasResolucionEliminar.getPersona().getId())
				.list();
		if (listaNotificacionesPersona.isEmpty()) {
			funcionesPersonasResolucion.remove(funcionPersonaResolucionQuitar - 1);
		} else {
			errores.put("resolucion.persona.borrar.error", "resolucion.persona.borrar.error");
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean eliminarPersonaNotificacionBlanco() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		procesoResolucion.setResolucion((Resolucion) bp
				.createQuery("from Resolucion r where r.numero =" + this.resolucion.getNumero()).uniqueResult());
		notificacionesPersonasInstancia = (List<NotificacionPersonaInstancia>) session
				.getAttribute(sid + "notificacionesPersonasInstancia");
		this.forward = "InstanciaJudicialBlanco";
		@SuppressWarnings("unused")
		NotificacionPersonaInstancia notificacionesPersonasInstanciaEliminar = notificacionesPersonasInstancia
				.get(notificacionPersonaInstanciaQuitar - 1);
		notificacionesPersonasInstancia.remove(notificacionPersonaInstanciaQuitar - 1);
		return true;
	}

	public void cargarResolucion() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		sid = UUID.randomUUID().toString();
		resolucion = (Resolucion) bp.getById(Resolucion.class, resolucion.getId());
		if (resolucion.getPersona() != null) {
			personaid = resolucion.getPersona().getId();
			opcion = "2";
		} else {
			opcion = "1";
		}

		session.setAttribute(sid, resolucion);
	}

	public void eliminarInstancia() {
		ProcesoResolucion pr = (ProcesoResolucion) bp.getById(ProcesoResolucion.class, this.procesoResolucion.getId());
		bp.delete(pr);
		procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		this.listarInstancias();
	}

	@SuppressWarnings("unchecked")
	public void eliminarInstanciaBlanco() {
		List<DocumentoJudicial> listaDocumentoJudicial = (List<DocumentoJudicial>) bp
				.createQuery(
						"from DocumentoJudicial dr where dr.procesoResolucion.id =" + this.procesoResolucion.getId())
				.list();
		List<MovimientoInstancia> listaMovimientoInstancia = (List<MovimientoInstancia>) bp
				.createQuery("from MovimientoInstancia mI where mI.instancia.id =" + this.procesoResolucion.getId())
				.list();
		if (listaDocumentoJudicial.isEmpty() && listaMovimientoInstancia.isEmpty()) {

			ProcesoResolucion pr = (ProcesoResolucion) bp.getById(ProcesoResolucion.class,
					this.procesoResolucion.getId());
			bp.delete(pr);
			this.listarInstanciasFiltro();

		}

		if (!listaDocumentoJudicial.isEmpty()) {

			errores.put("Instancia.DocumentoJudicial.Borrar.error", "Instancia.DocumentoJudicial.Borrar.error");

		}

		if (!listaMovimientoInstancia.isEmpty()) {
			errores.put("Instancia.MovimientoInstancia.Borrar.error", "Instancia.MovimientoInstancia.Borrar.error");

		}

		this.listarInstanciasFiltro();
	}

	public void eliminarArchivo() {
		DocumentoJudicial pr = (DocumentoJudicial) bp.getById(DocumentoJudicial.class, this.idArchivo);
		bp.delete(pr);
		cargarInstancia();
	}

	public void eliminarDocumentosMovimientoInstancia(boolean header) {
		DocumentoMovimientoInstancia pr = (DocumentoMovimientoInstancia) bp.getById(DocumentoMovimientoInstancia.class,
				this.idArchivo);
		bp.delete(pr);

		if (header) {
			cargarMovimiento(true);
		} else {
			cargarMovimiento(false);
		}

	}

	public void eliminarArchivoBlanco() {
		DocumentoJudicial pr = (DocumentoJudicial) bp.getById(DocumentoJudicial.class, this.idArchivo);
		bp.delete(pr);
		cargarInstanciaBlanco();
	}

	@SuppressWarnings("unchecked")
	public void cargarInstancia() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		sid = UUID.randomUUID().toString();
		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) bp
				.createQuery("from FuncionPersonaResolucion fpr where fpr.instancia.id = " + procesoResolucion.getId())
				.list();
		session.setAttribute(sid + "funcionesPersonasResolucion", funcionesPersonasResolucion);
		session.setAttribute(sid + "notificacionesPersonasInstancia", notificacionesPersonasInstancia);
		procesoResolucion = (ProcesoResolucion) bp.getById(ProcesoResolucion.class, procesoResolucion.getId());
		archivos = (List<DocumentoJudicial>) bp
				.createQuery(
						"Select d from DocumentoJudicial d where d.procesoResolucion.id = " + procesoResolucion.getId())
				.list();
		this.tipoProceso = procesoResolucion.getTipoProceso().getId();
		this.idproyectoPop = procesoResolucion.getResolucion().getId().toString();
		funcionesPersonasResolucion = (ArrayList<FuncionPersonaResolucion>) bp
				.createQuery("from FuncionPersonaResolucion fpr where fpr.instancia.resolucion.id = "
						+ procesoResolucion.getResolucion().getId())
				.list();

		this.forward = "InstanciaJudicial";
	}

	@SuppressWarnings("unchecked")
	public void cargarInstanciaBlanco() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		sid = UUID.randomUUID().toString();
		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) bp
				.createQuery("from FuncionPersonaResolucion fpr where fpr.instancia.id = " + procesoResolucion.getId())
				.list();
		session.setAttribute(sid + "funcionesPersonasResolucion", funcionesPersonasResolucion);
		session.setAttribute(sid + "notificacionesPersonasInstancia", notificacionesPersonasInstancia);
		procesoResolucion = (ProcesoResolucion) bp.getById(ProcesoResolucion.class, procesoResolucion.getId());
		archivos = (List<DocumentoJudicial>) bp
				.createQuery(
						"Select d from DocumentoJudicial d where d.procesoResolucion.id = " + procesoResolucion.getId())
				.list();
		this.tipoProceso = procesoResolucion.getTipoProceso().getId();
		this.idproyectoPop = procesoResolucion.getResolucion().getId().toString();
		funcionesPersonasResolucion = (ArrayList<FuncionPersonaResolucion>) bp
				.createQuery("from FuncionPersonaResolucion fpr where fpr.instancia.resolucion.id = "
						+ procesoResolucion.getResolucion().getId())
				.list();
		this.forward = "InstanciaJudicialBlanco";
	}

	@SuppressWarnings("unchecked")
	public void listarInstancias() {
		instancias = (List<ProcesoResolucion>) bp
				.createQuery("from ProcesoResolucion where resolucion.id = " + this.idSolicitud).list();
		this.forward = "InstanciaJudicialList";
	}

	@SuppressWarnings("unchecked")
	public void listarInstanciasBlanco() {
		long id;
		if (this.idSolicitud == 0)
			id = Long.parseLong(idResolucion);
		else
			id = idSolicitud;
		instancias = (List<ProcesoResolucion>) bp.createQuery("from ProcesoResolucion where resolucion.id = " + id)
				.list();
		this.forward = "InstanciaJudicialListBlanco";
	}

	@SuppressWarnings("unchecked")
	public void listarMovimientos(boolean header) {
		movimientosInstancia = (List<MovimientoInstancia>) bp
				.createQuery("from MovimientoInstancia where instancia.id = " + this.procesoResolucion.getId()
						+ " AND tipoMovimientoInstancia.nombre LIKE '%" + this.tipoMovimientoIntanciaStr + "%'")
				.list();
		if (!movimientosInstancia.isEmpty()) {
			this.idproyectoPop = movimientosInstancia.get(0).getInstancia().getResolucion().getId().toString();
		}
//		if (!movimientosInstancia.isEmpty()) {
//
//			this.procesoResolucion.setNombre(movimientosInstancia.get(0).getInstancia().getNombre());
//			this.procesoResolucion.getResolucion().setNumero(movimientosInstancia.get(0).getInstancia().getResolucion().getNumero());
//			if (movimientosInstancia.get(0).getInstancia().getResolucion().getProyecto() != null) {
//				this.procesoResolucion.getResolucion().getProyecto()
//						.setId(movimientosInstancia.get(0).getInstancia().getResolucion().getProyecto().getId());
//			}
//		}
		procesoResolucion = (ProcesoResolucion) bp.getById(ProcesoResolucion.class, procesoResolucion.getId());

		if (header) {
			this.forward = "MovimientoInstanciaList";
		} else {
			this.forward = "MovimientoInstanciaListBlanco";
		}

	}

	/*
	 * @SuppressWarnings("unchecked") public void listarInstanciasFiltro() {
	 * 
	 * if (accion.trim().equalsIgnoreCase("listarInstanciasFiltroInicio")) {
	 * //listarSolicitudesFiltro(); //instancias = (List<ProcesoResolucion>)
	 * bp.createQuery("from ProcesoResolucion pr where pr.resolucion.proyecto.id = "
	 * + this.idproyectoPop).list(); resoluciones = (List<Resolucion>)bp.
	 * createQuery("from Resolucion r where r.proyecto.id = :id").setParameter("id",
	 * new Long(this.idproyectoPop)).list();
	 * //this.procesoResolucion.getResolucion().getProyecto().setId(new
	 * Long(this.idproyectoPop));
	 * //this.procesoResolucion.getResolucion().setId(resolucion.getId());
	 * //this.procesoResolucion.getResolucion().getProyecto().setId(new
	 * Long(this.idproyectoPop)); //this.idSolicitud =
	 * this.procesoResolucion.getId(); this.forward = "InstanciaJudicialListBlanco";
	 * return;
	 * 
	 * } else { instancias = (List<ProcesoResolucion>)
	 * bp.createQuery("from ProcesoResolucion pr where pr.resolucion.id = " +
	 * this.procesoResolucion.getResolucion().getId()).list();
	 * procesoResolucion.setResolucion((Resolucion)
	 * bp.createQuery("from Resolucion r where r.id =" +
	 * this.idproyectoPop).uniqueResult()); } if (!instancias.isEmpty()) {
	 * this.procesoResolucion.setResolucion(instancias.get(0).getResolucion());
	 * this.idSolicitud = instancias.get(0).getId(); } else { String hql =
	 * "from Resolucion r where r.proyecto.id =" + this.idproyectoPop; Query query =
	 * bp.createQuery(hql); Resolucion res = (Resolucion) query.uniqueResult();
	 * this.procesoResolucion.setResolucion(res); }
	 * 
	 * this.idproyectoPop = this.procesoResolucion.getResolucion().getNumero();
	 * 
	 * 
	 * this.forward = "InstanciaJudicialListBlanco"; }
	 */
	@SuppressWarnings("unchecked")
	public void listarInstanciasFiltro() {
		if (accion.trim().equalsIgnoreCase("listarInstanciasFiltroInicio")) {
			instancias = (List<ProcesoResolucion>) bp
					.createQuery("from ProcesoResolucion pr where pr.resolucion.proyecto.id = " + this.idproyectoPop)
					.list();
			Resolucion resolucion = (Resolucion) bp.createQuery("from Resolucion r where r.proyecto.id = :id")
					.setParameter("id", new Long(this.idproyectoPop)).uniqueResult();
			this.procesoResolucion.getResolucion().getProyecto().setId(new Long(this.idproyectoPop));
			this.procesoResolucion.getResolucion().setId(resolucion.getId());
			this.procesoResolucion.getResolucion().getProyecto().setId(new Long(this.idproyectoPop));
			this.idSolicitud = this.procesoResolucion.getId();

		} else {
			instancias = (List<ProcesoResolucion>) bp.createQuery("from ProcesoResolucion pr where pr.resolucion.id = "
					+ this.procesoResolucion.getResolucion().getId()).list();
			procesoResolucion.setResolucion(
					(Resolucion) bp.createQuery("from Resolucion r where r.id =" + this.idproyectoPop).uniqueResult());
		}
		if (!instancias.isEmpty()) {
			this.procesoResolucion.setResolucion(instancias.get(0).getResolucion());
			this.idSolicitud = instancias.get(0).getId();
		} else {
			String hql = "from Resolucion r where r.proyecto.id =" + this.idproyectoPop;
			Query query = bp.createQuery(hql);
			Resolucion res = (Resolucion) query.uniqueResult();
			this.procesoResolucion.setResolucion(res);
		}

		this.idproyectoPop = this.procesoResolucion.getResolucion().getNumero();

		this.forward = "InstanciaJudicialListBlanco";
	}

	private void listarResolucionBlancoFiltro() {

		if (accion.trim().equalsIgnoreCase("listarResolucionFiltroInicio")) {
			String consulta = "";
			consulta = "SELECT r FROM Resolucion r";
			String filtro = "";

			if (this.idproyectoPop != null) {
				filtro = " WHERE r.proyecto.id in (SELECT o.id FROM Objetoi o WHERE o.expediente IN ("
						+ "SELECT x.expediente FROM Objetoi x WHERE x.id = " + this.idproyectoPop + ") )";
			}
			resoluciones = bp.createQuery(consulta + filtro).list();
		}

		this.forward = "ResolucionJudicialListBlanco";

	}

	@SuppressWarnings("unchecked")
	public void listarDocumentosResolucion() {

		archivosResolucion = (List<DocumentoResolucion>) bp
				.createQuery("Select d from DocumentoResolucion d where d.resolucion.id = " + this.idSolicitud).list();
		this.forward = "DocumentoResolucionList";
	}

	public void guardarDocumentosResolucion() {
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		DocumentoResolucion docJud = new DocumentoResolucion();
		if (theFile.getFileSize() != 0) {
			docJud.setMimetype(theFile.getContentType());
			docJud.setNombre(theFile.getFileName());
			Resolucion res = (Resolucion) oBp.getById(Resolucion.class, this.idSolicitud);
			docJud.setResolucion(res);
			docJud.setDetalle(this.detalleDocumentoResolucion);
			// guarda los datos del fichero
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream stream = null;
			try {
				stream = theFile.getInputStream();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			byte[] buffer = new byte[8192];
			int bytesLeidos = 0;
			try {
				while ((bytesLeidos = stream.read(buffer, 0, 8192)) != -1) {
					baos.write(buffer, 0, bytesLeidos);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			docJud.setArchivo(baos.toByteArray());
			oBp.saveOrUpdate(docJud);
		} else if (docJud.getId() != null && docJud.getId() != 0) {
			DocumentoResolucion a = (DocumentoResolucion) oBp.getById(DocumentoResolucion.class, docJud.getId());
			docJud.setMimetype(a.getMimetype());
			docJud.setNombre(a.getNombre());
			docJud.setArchivo(a.getArchivo());
			docJud.setResolucion(resolucion);
			docJud.setDetalle(this.detalleDocumentoResolucion);
			oBp.getCurrentSession().evict(a);
			oBp.saveOrUpdate(docJud);
		}

	}

	@SuppressWarnings("unchecked")
	public void nuevaInstancia() {
		sid = UUID.randomUUID().toString();
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		funcionesPersonasResolucion = new ArrayList<FuncionPersonaResolucion>();
		notificacionesPersonasInstancia = new ArrayList<NotificacionPersonaInstancia>();
		accion = "nuevaInstancia";
		this.forward = "InstanciaJudicial";
		procesoResolucion.setId(null);
		session.setAttribute(sid + "funcionesPersonasResolucion", funcionesPersonasResolucion);
		session.setAttribute(sid + "notificacionesPersonasInstancia", notificacionesPersonasInstancia);
		procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		session.setAttribute(sid, procesoResolucion);
		funcionesPersonasResolucion = (ArrayList<FuncionPersonaResolucion>) bp
				.createQuery("from FuncionPersonaResolucion fpr where fpr.instancia.resolucion.id = "
						+ procesoResolucion.getResolucion().getId())
				.list();
	}

	public void nuevaInstanciaBlanco() {
		sid = UUID.randomUUID().toString();
		funcionesPersonasResolucion = new ArrayList<FuncionPersonaResolucion>();
		notificacionesPersonasInstancia = new ArrayList<NotificacionPersonaInstancia>();
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		session.setAttribute(sid + "funcionesPersonasResolucion", funcionesPersonasResolucion);
		session.setAttribute(sid + "notificacionesPersonasInstancia", notificacionesPersonasInstancia);
		Objetoi oi = (Objetoi) bp.createQuery("Select o from Objetoi o where o.id = :numeroAtencion")
				.setParameter("numeroAtencion", new Long(this.idproyectoPop)).uniqueResult();
		procesoResolucion.setResolucion((Resolucion) bp
				.createQuery("from Resolucion r where r.proyecto.id =" + oi.getId() + " and r.id = " + idResolucion)
				.uniqueResult());
		session.setAttribute(sid, procesoResolucion);
		accion = "nuevaInstanciaBlanco";
		this.forward = "InstanciaJudicialBlanco";
	}

	public void nuevoMovimiento(boolean header) {

		try {
			ValoresAnteriores();

			if (header) {
				this.forward = "MovimientoInstancia";
			} else {
				this.forward = "MovimientoInstanciaBlanco";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void ValoresAnteriores() {

		try {
			String sql = " from MovimientoInstancia p ";
			sql += " where id in ( select max(id) from MovimientoInstancia ";
			sql += " where instancia_id = :id )";

			MovimientoInstancia instancias = (MovimientoInstancia) bp.createQuery(sql)
					.setLong("id", procesoResolucion.getId()).uniqueResult();
			movimientoInstancia.setFechaCargaMovimiento(new Date());
			movimientoInstancia.setObservacion(instancias.getObservacion());
			movimientoInstancia.setTipoMovimientoInstancia(instancias.getTipoMovimientoInstancia());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void guardarMovimiento(boolean header) {

		ProcesoResolucion pr = (ProcesoResolucion) bp.getById(ProcesoResolucion.class, this.procesoResolucion.getId());

		this.errores = new HashMap<String, Object>();

		this.movimientoInstancia.setInstancia(pr);

		if (movimientoInstancia.getObservacionRelevante() != null) {
			String[] mails = formatearMails();
			if (mails != null && mails.length > 0) {
				Resolucion r = movimientoInstancia.getInstancia().getResolucion();
				String asunto = ((ObservacionRelevante) bp.getById(ObservacionRelevante.class,
						movimientoInstancia.getObservacionRelevante().getId())).getObservacion();
				String mensaje = "Numero de Proyecto: " + r.getProyecto().getNumeroAtencionStr() + "<br/>"
						+ "Titular del Proyecto: " + r.getProyecto().getPersona().getNomb12() + "<br/>Instancia: "
						+ movimientoInstancia.getInstancia().getTipoProceso().getNombreTipoProceso() + "<br/>"
						+ "Tipo de Tribunal: " + movimientoInstancia.getInstancia().getTipoTribunal().getNombre()
						+ "<br/>N�mero de Tribunal: " + movimientoInstancia.getInstancia().getTribunal().getNombre()
						+ "<br/>N�mero Expediente: " + movimientoInstancia.getInstancia().getExpediente() + "<br/>"
						+ "Caratula: " + movimientoInstancia.getInstancia().getCaratula() + "<br/>Abogado: "
						+ r.getEspecialista().getPersona().getNomb12() + "<br/><br/>"
						+ movimientoInstancia.getObservacionRelevanteDesc();

				for (String mail : mails) {
					if (Mail.sendMailTemplate(mail.trim(), asunto, "mails/templateMail2.html", mensaje)) {
						errores.put("Error", "Error de mail");
						current_emails = "";
					}
				}
			}
		}

		bp.saveOrUpdate(this.movimientoInstancia);

		DocumentoMovimientoInstancia docMovInst = new DocumentoMovimientoInstancia();
		if (theFile.getFileSize() != 0) {
			docMovInst.setMimetype(theFile.getContentType());
			docMovInst.setNombre(theFile.getFileName());
			docMovInst.setMovimientoInstancia(this.movimientoInstancia);
			// guarda los datos del fichero
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream stream = null;
			try {
				stream = theFile.getInputStream();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			byte[] buffer = new byte[8192];
			int bytesLeidos = 0;
			try {
				while ((bytesLeidos = stream.read(buffer, 0, 8192)) != -1) {
					baos.write(buffer, 0, bytesLeidos);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			docMovInst.setArchivo(baos.toByteArray());
			bp.saveOrUpdate(docMovInst);
		} else if (docMovInst.getId() != null && docMovInst.getId() != 0) {
			DocumentoMovimientoInstancia a = (DocumentoMovimientoInstancia) bp
					.getById(DocumentoMovimientoInstancia.class, docMovInst.getId());
			docMovInst.setMimetype(a.getMimetype());
			docMovInst.setNombre(a.getNombre());
			docMovInst.setArchivo(a.getArchivo());
			docMovInst.setMovimientoInstancia(this.movimientoInstancia);
			bp.getCurrentSession().evict(a);
			bp.saveOrUpdate(docMovInst);
		}

		if (header) {
			this.listarMovimientos(true);
		} else {
			this.listarMovimientos(false);
		}

	}

	@SuppressWarnings("unchecked")
	public void cargarMovimiento(boolean header) {

		this.movimientoInstancia = (MovimientoInstancia) bp.getById(MovimientoInstancia.class,
				this.movimientoInstancia.getId());
		this.tipoMovimientoInstancia = movimientoInstancia.getTipoMovimientoInstancia();
		this.procesoResolucion = this.movimientoInstancia.getInstancia();
		archivosMovimientoInstancia = (List<DocumentoMovimientoInstancia>) bp
				.createQuery("Select d from DocumentoMovimientoInstancia d where d.movimientoInstancia.id = "
						+ this.movimientoInstancia.getId())
				.list();

		if (header) {
			this.forward = "MovimientoInstancia";
		} else {
			this.forward = "MovimientoInstanciaBlanco";
		}

	}

	@SuppressWarnings("unchecked")
	public void eliminarMovimiento(boolean header) {

		this.movimientoInstancia = (MovimientoInstancia) bp.getById(MovimientoInstancia.class,
				this.movimientoInstancia.getId());

		if (!header) {
			this.procesoResolucion.getResolucion().getProyecto()
					.setId(this.movimientoInstancia.getInstancia().getResolucion().getProyecto().getId());
		}

		List<DocumentoMovimientoInstancia> listaDocumentoMovimientoInstancia = (List<DocumentoMovimientoInstancia>) bp
				.createQuery(
						"from DocumentoMovimientoInstancia d where d.movimientoInstancia.id = :idMovimientoInstancia")
				.setParameter("idMovimientoInstancia", this.movimientoInstancia.getId()).list();

		if (listaDocumentoMovimientoInstancia.isEmpty()) {
			bp.delete(this.movimientoInstancia);
		} else {
			errores.put("Error.borrar.DocumentoMovimiento", "Error.borrar.DocumentoMovimiento");
		}

		this.procesoResolucion.setId(this.movimientoInstancia.getInstancia().getId());

		if (header) {
			this.listarMovimientos(true);
		} else {
			this.listarMovimientos(false);
		}

	}

	@SuppressWarnings("unchecked")
	public void guardarInstancia() {
		Boolean modificacion = true;
		if (procesoResolucion.getId() == null)
			modificacion = false;
		bp.begin();
		TipoProceso tp = (TipoProceso) bp.getById(TipoProceso.class, procesoResolucion.getIdTipoProceso());
		Tribunal tribunal = (Tribunal) bp.getById(Tribunal.class, this.getIdTribunal());
		procesoResolucion.setTribunal(tribunal);
		procesoResolucion.setTipoTribunal(tribunal.getTipoTribunal());
		procesoResolucion.setTipoProceso(tp);
		if (this.accion.trim().equalsIgnoreCase("guardarInstanciaBlanco")) {
			procesoResolucion.setResolucion((Resolucion) bp
					.createQuery("from Resolucion r where r.numero = '" + this.resolucion.getNumero() + "'")
					.uniqueResult());
		} else {
			procesoResolucion.setResolucion((Resolucion) bp.getById(Resolucion.class, this.idSolicitud));
		}
		bp.saveOrUpdate(procesoResolucion);
		// Buscamos la instancia anterior para setearle como fecha de fin la fecha de
		// inicio de la nueva instancia
		BigDecimal idInstanciaAnterior = (BigDecimal) bp
				.createSQLQuery("select MAX(id) from ProcesoResolucion where resolucion_id = "
						+ procesoResolucion.getResolucion().getId())
				.uniqueResult();
		ProcesoResolucion procesoResolucionAnterior = (ProcesoResolucion) bp
				.createQuery("from ProcesoResolucion pr where pr.resolucion = "
						+ procesoResolucion.getResolucion().getId() + " and pr.id =" + idInstanciaAnterior)
				.uniqueResult();
		if (procesoResolucionAnterior != null) {
			procesoResolucionAnterior.setFechaFin(procesoResolucion.getFechaInicio());
			bp.saveOrUpdate(procesoResolucionAnterior);
		}
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();

		DocumentoJudicial docJud = new DocumentoJudicial();
		if (theFile.getFileSize() != 0) {
			docJud.setMimetype(theFile.getContentType());
			docJud.setNombre(theFile.getFileName());
			docJud.setProcesoResolucion(procesoResolucion);
			// guarda los datos del fichero
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			InputStream stream = null;
			try {
				stream = theFile.getInputStream();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			byte[] buffer = new byte[8192];
			int bytesLeidos = 0;
			try {
				while ((bytesLeidos = stream.read(buffer, 0, 8192)) != -1) {
					baos.write(buffer, 0, bytesLeidos);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			docJud.setArchivo(baos.toByteArray());
			bp.saveOrUpdate(docJud);
		} else if (docJud.getId() != null && docJud.getId() != 0) {
			DocumentoJudicial a = (DocumentoJudicial) bp.getById(DocumentoJudicial.class, docJud.getId());
			docJud.setMimetype(a.getMimetype());
			docJud.setNombre(a.getNombre());
			docJud.setArchivo(a.getArchivo());
			docJud.setProcesoResolucion(procesoResolucion);
			bp.getCurrentSession().evict(a);
			bp.saveOrUpdate(docJud);
		}

		funcionesPersonasResolucion = (List<FuncionPersonaResolucion>) session
				.getAttribute(sid + "funcionesPersonasResolucion");
		if (modificacion) {
			List<FuncionPersonaResolucion> listaFPR = (List<FuncionPersonaResolucion>) bp
					.createQuery(
							"from FuncionPersonaResolucion fpr where fpr.instancia.id = " + procesoResolucion.getId())
					.list();
			for (FuncionPersonaResolucion f : listaFPR) {
				bp.delete(FuncionPersonaResolucion.class, f.getId());
			}
		}

		if (funcionesPersonasResolucion != null && funcionesPersonasResolucion.size() > 0)
			for (FuncionPersonaResolucion f : funcionesPersonasResolucion) {
				f.setInstancia(procesoResolucion);
				bp.save(f);
			}
		bp.commit();
		// Se envia el mail a las personas seleccionadas
		try {
			if (!idCn.equals(new Long(0))) {
				ConfiguracionNotificacion cn = (ConfiguracionNotificacion) SessionHandler.getCurrentSessionHandler()
						.getBusinessPersistance().getById(ConfiguracionNotificacion.class, idCn);
				String mensaje = "Resolucion: " + this.procesoResolucion.getResolucion().getNumero() + " - "
						+ "Instancia: " + this.procesoResolucion.getId() + " - Observaciones: "
						+ this.procesoResolucion.getObservaciones();
				if (this.destinatarioMail) {
					Mail.sendMailTemplate(
							this.procesoResolucion.getResolucion().getEspecialista().getPersona().getEmailPrincipal(),
							cn.getAsunto(), "mails/templateMail.html", mensaje);
				} else {
					// Se envia el mail al abogado
					Mail.sendMailTemplate(
							this.procesoResolucion.getResolucion().getEspecialista().getPersona().getEmailPrincipal(),
							cn.getAsunto(), "mails/templateMail.html", mensaje);
					// Se envia el mail al titular
					Mail.sendMailTemplate(
							this.procesoResolucion.getResolucion().getProyecto().getPersona().getEmailPrincipal(),
							cn.getAsunto(), "mails/templateMail.html", mensaje);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (this.accion.trim().equalsIgnoreCase("guardarInstanciaBlanco")) {
			instancias = (List<ProcesoResolucion>) bp
					.createQuery("from ProcesoResolucion pr where pr.resolucion.proyecto.id = " + this.idproyectoPop)
					.list();
			if (instancias.isEmpty()) {
				instancias = (List<ProcesoResolucion>) bp
						.createQuery("from ProcesoResolucion pr where pr.resolucion.id = " + this.idproyectoPop).list();
			} else {
				this.procesoResolucion.setResolucion(instancias.get(0).getResolucion());
			}
			this.forward = "InstanciaJudicialListBlanco";
		} else {
			this.listarInstancias();
		}
		// Se guardan las notificaciones a los participantes de la Resolucion
		String[] listaPersonas = cadenaPersonaSeleccionada.split("-");
		if (listaPersonas.length > 0 && !listaPersonas[0].equals("")) {
			for (String idPersona : listaPersonas) {
				NotificacionPersonaInstancia npi = new NotificacionPersonaInstancia();
				npi.setInstancia(procesoResolucion);
				npi.setDetalle(notificacionPesonaInstancia.getDetalle());
				npi.setFecha(notificacionPesonaInstancia.getFecha());
				FuncionPersonaResolucion fpr = (FuncionPersonaResolucion) bp.getById(FuncionPersonaResolucion.class,
						new Long(idPersona));
				npi.setPersona(fpr.getPersona());
				npi.setIdPersona(fpr.getPersona().getIdpersona().toString());
				bp.save(npi);
			}
		}
	}

	public ActionForward descargar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String id = (String) request.getParameter("id");
		OutputStream salida = response.getOutputStream();
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Archivo a = (Archivo) oBp.getById(Archivo.class, new Long(id));
		response.setContentType(a.getMimetype());
		response.setHeader("Content-Disposition", "attachment;filename=\"" + a.getNombre() + "\"");
		salida.write(a.getArchivo());
		salida.flush();
		salida.close();
		return null;
	}

	@SuppressWarnings("unchecked")
	private void guardarNuevoEstado(Objetoi objetoi) {
		estado = (Estado) bp.createQuery("from Estado e where e.nombreEstado = 'GESTION JUDICIAL'").uniqueResult();
		// objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoI);
		if (estado.getNombreEstado().equals(AbstractEstado.GESTION_JUDICIAL)
				|| estado.getNombreEstado().equals(AbstractEstado.GESTION_EXTRAJUDICIAL)) {
			ObjetoiComportamiento comportamientoActual = objetoi.getObjetoiComportamientoActual();
			if (comportamientoActual != null && comportamientoActual.getComportamientoPago().equals("1")) {
				errores.put("EstadoCredito.error.gestionJudicial", "EstadoCredito.error.gestionJudicial");
				return;
			}
		}
		try {
			if (estado != null) {
				/**
				 * este metodo resuelve la logica para actualizar el estado de un credito
				 */
				objetoi.setEstadoActual(AbstractEstado.GESTION_JUDICIAL,
						"Cambio de estado Autom�tico desde Gesti�n Judicial - "
								+ SessionHandler.getCurrentSessionHandler().getCurrentUser());
			}
		} catch (Exception e) {
			this.errores.put("EstaPer.error", "EstaPer.error");
		}
	}

	/*
	 * Cristian Anton Cambia estado de la persona a Excluido
	 */

	private void cambiarEstadoPersona(Objetoi credito) {
		estado = (Estado) bp.createQuery("from Estado e where e.nombreEstado = 'EXCLUIDO'").uniqueResult();
		Persona persona = credito.getPersona();
		persona.setEstadoActual(AbstractEstado.EXCLUIDO, "Cambio de estado Autom�tico desde Gesti�n Judicial");

		List<PersonaVinculada> cotomadores = credito.buscarCotomadores();
		for (PersonaVinculada personaVinculada : cotomadores) {
			personaVinculada.getPersonaVinculada().setEstadoActual(AbstractEstado.EXCLUIDO,
					"Cambio de estado Autom�tico desde Gesti�n Judicial");
		}

	}

	// Getters and Setters
	public List<Resolucion> getResoluciones() {
		return resoluciones;
	}

	public void setResoluciones(List<Resolucion> resoluciones) {
		this.resoluciones = resoluciones;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getNroResolucion() {
		return nroResolucion;
	}

	public void setNroResolucion(Long nroResolucion) {
		this.nroResolucion = nroResolucion;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getJuzgado() {
		return juzgado;
	}

	public void setJuzgado(String juzgado) {
		this.juzgado = juzgado;
	}

	public Double getMontoDemandado() {
		return montoDemandado;
	}

	public void setMontoDemandado(Double montoDemandado) {
		this.montoDemandado = montoDemandado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public Long getIdSubTipoResolucion() {
		return idSubTipoResolucion;
	}

	public void setIdSubTipoResolucion(Long idSubTipoResolucion) {
		this.idSubTipoResolucion = idSubTipoResolucion;
	}

	public Long getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(Long tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public String getFechaStr() {
		return DateHelper.getString(fecha);
	}

	public void setFechaStr(String fecha) {
		this.fecha = DateHelper.getDate(fecha);
	}

	public Date getFechaInicioFuncion() {
		return fechaInicioFuncion;
	}

	public void setFechaInicioFuncion(Date fechaInicioFuncion) {
		this.fechaInicioFuncion = fechaInicioFuncion;
	}

	public Date getFechaFinFuncion() {
		return fechaFinFuncion;
	}

	public void setFechaFinFuncion(Date fechaFinFuncion) {
		this.fechaFinFuncion = fechaFinFuncion;
	}

	public String getFechaFinFuncionStr() {
		return DateHelper.getString(fechaFinFuncion);
	}

	public void setFechaFinFuncionStr(String fechaFinFuncion) {
		this.fechaFinFuncion = DateHelper.getDate(fechaFinFuncion);
	}

	public String getFechaInicioFuncionStr() {
		return DateHelper.getString(fechaInicioFuncion);
	}

	public void setFechaInicioFuncionStr(String fechaInicioFuncion) {
		this.fechaInicioFuncion = DateHelper.getDate(fechaInicioFuncion);
	}

	public Long getIdFuncion() {
		return idFuncion;
	}

	public void setIdFuncion(Long idFuncion) {
		this.idFuncion = idFuncion;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public List<FuncionPersonaResolucion> getPersonas() {
		return personas;
	}

	public void setPersonas(List<FuncionPersonaResolucion> personas) {
		this.personas = personas;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public List<FuncionPersonaResolucion> getFuncionesPersonasResolucion() {
		return funcionesPersonasResolucion;
	}

	public void setFuncionesPersonasResolucion(List<FuncionPersonaResolucion> funcionesPersonasResolucion) {
		this.funcionesPersonasResolucion = funcionesPersonasResolucion;
	}

	public Resolucion getResolucion() {
		return resolucion;
	}

	public void setResolucion(Resolucion resolucion) {
		this.resolucion = resolucion;
	}

	public FuncionPersonaResolucion getFuncionPersonResolucion() {
		return funcionPersonResolucion;
	}

	public void setFuncionPersonResolucion(FuncionPersonaResolucion funcionPersonResolucion) {
		this.funcionPersonResolucion = funcionPersonResolucion;
	}

	public int getFuncionPersonaResolucionQuitar() {
		return funcionPersonaResolucionQuitar;
	}

	public void setFuncionPersonaResolucionQuitar(int funcionPersonaResolucionQuitar) {
		this.funcionPersonaResolucionQuitar = funcionPersonaResolucionQuitar;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public Long getIdSubTipoResolucion2() {
		return idSubTipoResolucion2;
	}

	public void setIdSubTipoResolucion2(Long idSubTipoResolucion2) {
		this.idSubTipoResolucion2 = idSubTipoResolucion2;
	}

	public List<ProcesoResolucion> getInstancias() {
		return instancias;
	}

	public void setInstancias(List<ProcesoResolucion> instancias) {
		this.instancias = instancias;
	}

	public ProcesoResolucion getProcesoResolucion() {
		return procesoResolucion;
	}

	public void setProcesoResolucion(ProcesoResolucion procesoResolucion) {
		this.procesoResolucion = procesoResolucion;
	}

	public Long getTipoProceso2() {
		return tipoProceso2;
	}

	public void setTipoProceso2(Long tipoProceso2) {
		this.tipoProceso2 = tipoProceso2;
	}

	public FormFile getTheFile() {
		return theFile;
	}

	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}

	public List<DocumentoJudicial> getArchivos() {
		return archivos;
	}

	public void setArchivos(List<DocumentoJudicial> archivos) {
		this.archivos = archivos;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	public Boolean getDefinitivo() {
		return definitivo;
	}

	public void setDefinitivo(Boolean definitivo) {
		this.definitivo = definitivo;
	}

	public Long getIdPersonaEspecialista() {
		return idPersonaEspecialista;
	}

	public void setIdPersonaEspecialista(Long idPersonaEspecialista) {
		this.idPersonaEspecialista = idPersonaEspecialista;
	}

	public Long getIdEspecialidad() {
		return idEspecialidad;
	}

	public void setIdEspecialidad(Long idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}

	public List<DocumentoResolucion> getArchivosResolucion() {
		return archivosResolucion;
	}

	public void setArchivosResolucion(List<DocumentoResolucion> archivosResolucion) {
		this.archivosResolucion = archivosResolucion;
	}

	public String getDetalleDocumentoResolucion() {
		return detalleDocumentoResolucion;
	}

	public void setDetalleDocumentoResolucion(String detalleDocumentoResolucion) {
		this.detalleDocumentoResolucion = detalleDocumentoResolucion;
	}

	public Long getIdDocumentoResolucion() {
		return idDocumentoResolucion;
	}

	public void setIdDocumentoResolucion(Long idDocumentoResolucion) {
		this.idDocumentoResolucion = idDocumentoResolucion;
	}

	public Long getIdCn() {
		return idCn;
	}

	public void setIdCn(Long idCn) {
		this.idCn = idCn;
	}

	public Boolean getDestinatarioMail() {
		return destinatarioMail;
	}

	public void setDestinatarioMail(Boolean destinatarioMail) {
		this.destinatarioMail = destinatarioMail;
	}

	public Boolean getNotificar() {
		return notificar;
	}

	public void setNotificar(Boolean notificar) {
		this.notificar = notificar;
	}

	public String getIdproyectoPop() {
		return idproyectoPop;
	}

	public void setIdproyectoPop(String idproyectoPop) {
		this.idproyectoPop = idproyectoPop;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public String getObservacionesArchivo() {
		return observacionesArchivo;
	}

	public void setObservacionesArchivo(String observacionesArchivo) {
		this.observacionesArchivo = observacionesArchivo;
	}

	public Boolean getEnviarAviso() {
		return enviarAviso;
	}

	public void setEnviarAviso(Boolean enviarAviso) {
		this.enviarAviso = enviarAviso;
	}

	public List<NotificacionPersonaInstancia> getNotificacionesPersonasInstancia() {
		return notificacionesPersonasInstancia;
	}

	public void setNotificacionesPersonasInstancia(List<NotificacionPersonaInstancia> notificacionesPersonasInstancia) {
		this.notificacionesPersonasInstancia = notificacionesPersonasInstancia;
	}

	public NotificacionPersonaInstancia getNotificacionPesonaInstancia() {
		return notificacionPesonaInstancia;
	}

	public void setNotificacionPesonaInstancia(NotificacionPersonaInstancia notificacionPesonaInstancia) {
		this.notificacionPesonaInstancia = notificacionPesonaInstancia;
	}

	public int getNotificacionPersonaInstanciaQuitar() {
		return notificacionPersonaInstanciaQuitar;
	}

	public void setNotificacionPersonaInstanciaQuitar(int notificacionPersonaInstanciaQuitar) {
		this.notificacionPersonaInstanciaQuitar = notificacionPersonaInstanciaQuitar;
	}

	public String getCadenaPersonaSeleccionada() {
		return cadenaPersonaSeleccionada;
	}

	public void setCadenaPersonaSeleccionada(String cadenaPersonaSeleccionada) {
		this.cadenaPersonaSeleccionada = cadenaPersonaSeleccionada;
	}

	public String getCurrent_emails() {
		return current_emails;
	}

	public void setCurrent_emails(String current_emails) {
		this.current_emails = current_emails;
	}

	public Long getIdTipoResolucion() {
		return idTipoResolucion;
	}

	public void setIdTipoResolucion(Long idTipoResolucion) {
		this.idTipoResolucion = idTipoResolucion;
	}

	public List<MovimientoInstancia> getMovimientosInstancia() {
		return movimientosInstancia;
	}

	public void setMovimientosInstancia(List<MovimientoInstancia> movimientosInstancia) {
		this.movimientosInstancia = movimientosInstancia;
	}

	public MovimientoInstancia getMovimientoInstancia() {

		return movimientoInstancia;
	}

	public void setMovimientoInstancia(MovimientoInstancia movimientoInstancia) {
		this.movimientoInstancia = movimientoInstancia;
	}

	public List<DocumentoMovimientoInstancia> getArchivosMovimientoInstancia() {
		return archivosMovimientoInstancia;
	}

	public void setArchivosMovimientoInstancia(List<DocumentoMovimientoInstancia> archivosMovimientoInstancia) {
		this.archivosMovimientoInstancia = archivosMovimientoInstancia;
	}

	public Long getIdTribunal() {
		return idTribunal;
	}

	public void setIdTribunal(Long idTribunal) {
		this.idTribunal = idTribunal;
	}

	public Long getIdTipoTribunal() {
		return idTipoTribunal;
	}

	public void setIdTipoTribunal(Long idTipoTribunal) {
		this.idTipoTribunal = idTipoTribunal;
	}

	public Long getIdAbogado() {
		return idAbogado;
	}

	public void setIdAbogado(Long idAbogado) {
		this.idAbogado = idAbogado;
	}

	public List<ObservacionRelevante> getObservacionesRelevante() {
		return observacionesRelevante;
	}

	public void setObservacionesRelevante(List<ObservacionRelevante> observacionesRelevante) {
		this.observacionesRelevante = observacionesRelevante;
	}

	public ObservacionRelevante getObservacionRelevante() {
		return observacionRelevante;
	}

	public void setObservacionRelevante(ObservacionRelevante observacionRelevante) {
		this.observacionRelevante = observacionRelevante;
	}

	public String getIdResolucion() {
		return idResolucion;
	}

	public void setIdResolucion(String idResolucion) {
		this.idResolucion = idResolucion;
	}

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	public Long getPersonaid() {
		return personaid;
	}

	public void setPersonaid(Long personaid) {
		this.personaid = personaid;
	}

	public Boolean getNuevo() {
		return nuevo;
	}

	public void setNuevo(Boolean nuevo) {
		this.nuevo = nuevo;
	}

	public String getTipoMovimientoIntanciaStr() {
		return tipoMovimientoIntanciaStr;
	}

	public void setTipoMovimientoIntanciaStr(String tipoMovimientoIntanciaStr) {
		this.tipoMovimientoIntanciaStr = tipoMovimientoIntanciaStr;
	}

}
