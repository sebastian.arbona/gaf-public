package com.asf.cred.business;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.hibernate.Mediopago;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Bolepago;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.Ctacte;

public class CreaCobropago {

	private BusinessPersistance bp;
	
	
	
	public CreaCobropago(BusinessPersistance bp) {
		this.bp = bp!= null ? bp : SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	
	public Cobropago generaCobropago(Cobropago c, Long nrolote){

		Cobropago cobropago = new Cobropago();	
		cobropago.setMediopago(c.getMediopago());
		cobropago.setCaratula(c.getCaratula());
		cobropago.setEntidadEmisora(c.getEntidadEmisora());
		cobropago.setNumeroComprobante(c.getNumeroComprobante());
		cobropago.setFechaEmision(c.getFechaEmision());
		cobropago.setFechaVencimiento(c.getFechaVencimiento());
		cobropago.setCuotas(c.getCuotas());
		cobropago.setPlan(c.getPlan());
		cobropago.setAutorizacion(c.getAutorizacion());
		cobropago.setImporte(c.getImporte());
		cobropago.setEstado(c.getEstado());
		cobropago.setNumeroLote(nrolote);

		bp.save(cobropago);

		return cobropago;
	}
		
	public Bolepago generaBolepago(Cobropago c, Boleto boleto, Double importe){

		Bolepago bolepago = new Bolepago();
		bolepago.getId().setCobropago(c);
		bolepago.getId().setBoleto(boleto);
		bolepago.setImporte(importe);

		bp.save(bolepago);

		return bolepago;
	}
	
	
//	public Detallebolepago generaDetalle(int codi, Bolepago bolepago, Ctacte c,
//			Double importe) {
//
//		try {
//
//			if (c != null) {
//				bp.getCurrentSession().refresh(c);
//			}
//
//			Detallebolepago detallebolepago = new Detallebolepago();
//			DetallebolepagoId detalleKey = new DetallebolepagoId();
//			detalleKey.setCodigodetalle(new Long(codi));
//			detalleKey.setCobropago(bolepago.getId().getCobropago());
//			detalleKey.setNumeroBoleto(bolepago.getId().getNumeroBoleto());
//			detalleKey.setVerificadorBoleto(bolepago.getId()
//					.getVerificadorBoleto());
//			detalleKey.setPeriodoBoleto(bolepago.getId().getPeriodoBoleto());
//			detalleKey.setItemCtacte(c != null ? c.getId().getItemCtacte() : 0);
//			detalleKey.setPeriodoCtacte(c != null ? c.getId()
//					.getPeriodoCtacte() : bolepago.getId().getPeriodoBoleto());
//			detalleKey.setVerificadorCtacte(c != null ? c.getId()
//					.getVerificadorCtacte() : 0);
//			detalleKey.setMovimientoCtaCte(c != null ? c.getId()
//					.getMovimientoCtacte() : 0);
//			detallebolepago.setId(detalleKey);
//			Bolcon bolcon = getBolcon(bolepago);
//			detallebolepago.setConcepto(c != null ? c.getFacturado() : bolcon
//					.getFacturado());
//			detallebolepago.setImporte(importe);
//
//			bp.save(detallebolepago);
//
//			return detallebolepago;
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//
//	}
	
	
	private Bolcon getBolcon( Bolepago bolepago ){
		return (Bolcon)bp.getByFilter("FROM Bolcon b WHERE b.id.numeroBoleto="+ bolepago.getId().getNumeroBoleto() +
				" AND b.id.verificadorBoleto="+ bolepago.getId().getVerificadorBoleto() + " AND b.id.periodoBoleto="+ bolepago.getId().getPeriodoBoleto() ).listIterator().next();
	}
	
	
}
