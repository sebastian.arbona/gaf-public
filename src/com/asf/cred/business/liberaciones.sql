SELECT id, Proyecto, titular, expediente, 
-- volumentContrato, 
Producto, 
-- LiberadoALaFecha, 
-- ultimaLiberacion, 
deudaActual, 
CASE WHEN esPendiente = 1 AND cambioProducto = 1 THEN 0 ELSE
CASE tipoProducto 
WHEN '1' THEN deudaActual * aforo / precioVinoTinto 
WHEN '2' THEN deudaActual * aforo / precioVinoBlanco 
WHEN '3' THEN deudaActual * aforo / precioMosto 
WHEN '4' THEN deudaActual * aforo / precioMalbec 
WHEN '5' THEN deudaActual * aforo / precioOtrosVarietales END
END nuevoVolumen, 
forzarLiberacion, 
-- saldoInmovilizado, 
-- liberacionSolicitada, 
-- liberacionSolicitar, 
esPendiente,
tipoProducto,
aforo, 
CASE tipoProducto 
	WHEN '1' THEN precioVinoTinto 
	WHEN '2' THEN precioVinoBlanco 
	WHEN '3' THEN precioMosto 
	WHEN '4' THEN precioMalbec 
	WHEN '5' THEN precioOtrosVarietales
END precio
FROM ( 
	SELECT O.id, O.numeroAtencion Proyecto, P.NOMB_12 titular, O.expediente, O.forzarLiberacion, 
	-- SUM(inm.volumen) volumentContrato, -- o.getVolumenInmovilizadoProductoEstado(realizada)
	T.TF_DESCRIPCION Producto, 
	-- SUM(lib.volumen) LiberadoALaFecha, -- o.getVolumenLiberadoProductoEstado(realizada)
	-- MAX(lib.fecha) ultimaLiberacion, 
	(SELECT SUM(importe) FROM ( 
		(SELECT SUM(importe) importe FROM Ctacte WHERE objetoi_id = O.id 
		AND asociado_id IN (SELECT id FROM Concepto WHERE concepto_concepto = 'cap') AND tipomov_id = 2) 
		UNION 
		(SELECT SUM(importe)*-1 importe FROM Ctacte WHERE objetoi_id = O.id 
		AND asociado_id IN (SELECT id FROM Concepto WHERE concepto_concepto = 'cap') AND tipomov_id = 1)
	) T ) deudaActual, -- o.getdeudaactual(o.CONCEPTO_CAPITAL)
	G.tipoProducto, C.aforo, C.precioMosto, C.precioVinoBlanco, C.precioVinoTinto, C.precioMalbec, C.precioOtrosVarietales, 
	-- (SUM(inm.volumen) - SUM(CASE WHEN lib.volumen IS NOT NULL THEN lib.volumen ELSE 0 END)) AS saldoInmovilizado, 
	-- SUM(CASE WHEN libSol.volumen IS NOT NULL THEN libSol.volumen ELSE 0 END) AS liberacionSolicitada, 
	-- NULL AS liberacionSolicitar, 
	0 AS esPendiente, 0 AS cambioProducto
	FROM Objetoi O 
	INNER JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id AND OG.baja IS NULL 
	INNER JOIN Garantia G ON OG.garantia_id = G.id AND G.tipo_id in (:tipoGarantia) 
	INNER JOIN PERSONA P ON P.IDPERSONA = O.persona_IDPERSONA 
	INNER JOIN TIPIFICADORES T ON T.TF_CATEGORIA = 'Garantia.tipoProducto' AND TF_CODIGO = G.tipoProducto 
	INNER JOIN CosechaConfig C ON O.varietales = C.varietales AND (O.fechaSolicitud >= C.fechaInicioPer AND O.fechaSolicitud <= C.fechaFinPer) 
	-- LEFT JOIN Inmovilizaciones inm ON inm.objetoi_id = O.id AND inm.estado = 'REALIZADA' AND inm.producto = G.tipoProducto 
	-- LEFT JOIN Liberaciones lib ON lib.objetoi_id = O.id AND lib.estado = 'REALIZADA' AND lib.producto = G.tipoProducto
	-- LEFT JOIN Liberaciones libSol ON libSol.objetoi_id = O.id AND libSol.estado = 'SOLICITADA' AND libSol.producto = G.tipoProducto
	WHERE O.liberar = 1 and o.acuerdoPago_id is null
	GROUP BY O.id, O.numeroAtencion, O.expediente, O.forzarLiberacion, liberar, P.NOMB_12, TF_DESCRIPCION, G.tipoProducto, 
	C.aforo, C.precioVinoTinto, C.precioVinoBlanco, C.precioMosto, C.precioMalbec, C.precioOtrosVarietales, C.temporada 
	HAVING C.temporada = MAX(C.temporada) 

	UNION

	SELECT O.id, O.numeroAtencion Proyecto, P.NOMB_12 titular, O.expediente, O.forzarLiberacion, 
	-- SUM(inm.volumen) volumentContrato, 
	T.TF_DESCRIPCION Producto, 
	-- SUM(lib.volumen) LiberadoALaFecha, 
	-- MAX(lib.fecha) ultimaLiberacion, 
	(SELECT SUM(importe) FROM ( 
		(SELECT SUM(importe) importe FROM Ctacte WHERE objetoi_id = O.id 
		AND asociado_id IN (SELECT id FROM Concepto WHERE concepto_concepto = 'cap') AND tipomov_id = 2) 
		UNION 
		(SELECT SUM(importe)*-1 importe FROM Ctacte WHERE objetoi_id = O.id 
		AND asociado_id IN (SELECT id FROM Concepto WHERE concepto_concepto = 'cap') AND tipomov_id = 1)
	) T ) deudaActual, 
	libPen.producto tipoProducto, C.aforo, C.precioMosto, C.precioVinoBlanco, C.precioVinoTinto, C.precioMalbec, C.precioOtrosVarietales, 
	-- (SUM(inm.volumen) - SUM(CASE WHEN lib.volumen IS NOT NULL THEN lib.volumen ELSE 0 END)) AS saldoInmovilizado, 
	-- SUM(CASE WHEN libSol.volumen IS NOT NULL THEN libSol.volumen ELSE 0 END) AS liberacionSolicitada, 
	-- SUM(CASE WHEN libPen.volumen IS NOT NULL THEN libPen.volumen ELSE 0 END) AS liberacionSolicitar, 
	1 AS esPendiente, libPen.cambioProducto
	FROM Liberaciones libPen 
	INNER JOIN Objetoi O on libPen.objetoi_id = O.id
	INNER JOIN PERSONA P ON P.IDPERSONA = O.persona_IDPERSONA 
	INNER JOIN TIPIFICADORES T ON T.TF_CATEGORIA = 'Garantia.tipoProducto' AND TF_CODIGO = libPen.producto 
	INNER JOIN CosechaConfig C ON O.varietales = C.varietales AND (O.fechaSolicitud >= C.fechaInicioPer AND O.fechaSolicitud <= C.fechaFinPer)
	-- LEFT JOIN Inmovilizaciones inm ON inm.objetoi_id = O.id AND inm.estado = 'REALIZADA' AND inm.producto = libPen.producto 
	-- LEFT JOIN Liberaciones lib ON lib.objetoi_id = O.id AND lib.estado = 'REALIZADA' AND lib.producto = libPen.producto 
	-- LEFT JOIN Liberaciones libSol ON libSol.objetoi_id = O.id AND libSol.estado = 'SOLICITADA' AND libsol.producto = libPen.producto 
	WHERE libPen.estado = 'PENDIENTE' and o.acuerdoPago_id is null
	GROUP BY O.id, O.numeroAtencion, O.expediente, O.forzarLiberacion, liberar, P.NOMB_12, TF_DESCRIPCION, libPen.producto, 
	C.aforo, C.precioVinoTinto, C.precioVinoBlanco, C.precioMosto, C.precioMalbec, C.precioOtrosVarietales, C.temporada, libPen.cambioProducto 
	HAVING C.temporada = MAX(C.temporada) 
) T 
-- WHERE esPendiente = 1 OR saldoInmovilizado - liberacionSolicitada > (CASE tipoProducto 
-- WHEN '1' THEN deudaActual * aforo / precioVinoTinto 
-- WHEN '2' THEN deudaActual * aforo / precioVinoBlanco 
-- WHEN '3' THEN deudaActual * aforo / precioMosto 
-- WHEN '4' THEN deudaActual * aforo / precioMalbec 
-- WHEN '5' THEN deudaActual * aforo / precioOtrosVarietales END) 
ORDER BY expediente
