package com.asf.cred.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;

import com.asf.gaf.hibernate.Ejercicio;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.IReportBean;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.ReportResult;
import com.asf.util.SQLHelper;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;

/**
 * @deprecated Se unifico con Cta Cte Ampliada
 * @author andres
 *
 */
@Deprecated
public class CreditoCuentaCorrienteContable extends ConversationProcess implements IReportBean {

	private static String sql;

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(CreditoCuentaCorrienteContable.class.getResourceAsStream("ctacte-contable.sql"));
			} catch (IOException e) {
				System.err.println("Error al cargar SQL cuenta corriente contable");
			}
		}
		return sql;
	}

	private Date desde;
	private Date hasta;

	@Save
	private List<CuentaCorrienteContableBean> beans;
	@Save
	CuentaCorrienteContableBean bean;

	private boolean esDolares = false;
	private double cotizacion;

	private String contextoGAF;
	private ReportResult reporte;
	private Boleto boleto;
	private Pagos pago;
	private Long idCaratula;
	private Long idBoleto;
	private String esquemaGAF;

	@Save
	private Long idObjetoi;
	@Save
	private Objetoi credito;

	private boolean minuta;

	private Long personaId;

	private String forward = "CreditoCuentaCorrienteContable";

	public CreditoCuentaCorrienteContable() {
		contextoGAF = DirectorHelper.getString("URL.GAF");
		esquemaGAF = DirectorHelper.getString("SGAF");
	}

	@ProcessMethod(defaultAction = true)
	@SuppressWarnings("unchecked")
	public boolean buscar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		beans = new ArrayList<CuentaCorrienteContableBean>();

		credito = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());

		boolean acuerdoPago = revisarAcuerdoPago();

		// String s = "SELECT fechaGeneracion, min(periodoCtacte) AS periodo, tipo,
		// numeroBoleto, " +
		// "idBoleto, tipomov_id, tipoMovimiento, detalle, sum(debe) AS debe, sum(haber)
		// AS haber, " +
		// "min(movimientoCtacte) AS movimiento, " +
		// "min(verificadorCtacte) AS verificador, min(itemCtacte) AS item, caratula_id,
		// boletoReciboId, idObjetoi, MAX(fechaProceso) as fechaProceso,
		// fechaVencimiento, " +
		// "cotizaMov, sum(debePesos) AS debePesos, sum(haberPesos) AS haberPesos " +
		// "FROM " +
		// "(SELECT " +
		// "case when cc.emideta_id is not null then null else cc.fechaGeneracion end as
		// fechaGeneracion," +
		// "cc.periodoCtacte, bol.tipo, bol.numeroBoleto, " +
		// "bol.id as idBoleto, cc.tipomov_id, conc.concepto_concepto,
		// cc.tipoMovimiento, cc.fechaProceso, " +
		// "CASE WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'cap' AND
		// (cc.tipoMovimiento = 'cuota' or cc.tipoMovimiento = 'movManualDeb') THEN
		// cc.fechaGeneracion " +
		// "WHEN cc.tipomov_id = 1 AND conc.concepto_concepto = 'cap' AND
		// cc.tipoMovimiento = 'movManualCred' THEN cc.fechaGeneracion " +
		// "ELSE cc.fechaVencimiento END AS fechaVencimiento, " +
		// "CASE WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'cap' AND
		// cc.tipoMovimiento = 'cuota' AND cc.desembolso_id IS NOT NULL THEN " +
		// (!acuerdoPago ? "'Desembolso' " : "'Deuda exigible' ") +
		// " WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'com' AND " +
		// "cc.tipoMovimiento = 'cuota' THEN 'Int Compensatorio' " +
		// " WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'gas' AND " +
		// "cc.tipoMovimiento = 'cuota' THEN 'Gastos' " +
		// " WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'rec' AND " +
		// "cc.tipoMovimiento = 'cuota' THEN 'Gastos a Recuperar' " +
		// " WHEN cc.tipomov_id = 2 AND (conc.concepto_concepto = 'mor' OR
		// conc.concepto_concepto = 'pun') AND " +
		// "cc.tipoMovimiento = 'cuota' THEN 'Int Moratorio y Punitorio' " +
		// //" WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'mul' THEN 'Multas' "
		// +
		// " WHEN cc.tipomov_id = 1 AND conc.concepto_concepto = 'bon' AND
		// cc.tipoMovimiento = 'bonificacion' THEN " +
		// "cc.detalle" +
		// " WHEN cc.tipomov_id = 1 AND (conc.concepto_concepto = 'gas' AND
		// cc.tipoMovimiento = 'cuota' " +
		// "OR conc.concepto_concepto = 'rec' AND cc.tipoMovimiento = 'cuota') THEN
		// 'Pago Gastos' " +
		// " WHEN cc.tipomov_id = 1 AND (conc.concepto_concepto = 'gas' OR
		// conc.concepto_concepto = 'rec') AND " +
		// "cc.caratula_id is null AND cc.tipoMovimiento like 'pago' THEN 'Pago Gastos'
		// "+
		// " WHEN cc.tipomov_id = 1 AND cc.tipoMovimiento = 'pago' " +
		// "THEN 'Pago' " +
		// //" WHEN cc.tipoMovimiento = 'movManualDeb' THEN 'Nota de D�bito de ' +
		// cconc.detalle " +
		// //" WHEN cc.tipoMovimiento = 'movManualCred' THEN 'Nota de Cr�dito de ' +
		// cconc.detalle " +
		// " ELSE cc.detalle END AS detalle, " +
		// "CASE cc.tipomov_id when 2 THEN cc.importe else 0 end as debe, " +
		// "CASE cc.tipomov_id when 1 THEN cc.importe else 0 end as haber, " +
		// "CASE conc.concepto_concepto when 'cap' then 1 " +
		// " WHEN 'com' then 2 " +
		// " WHEN 'gas' then 2 " +
		// " WHEN 'mul' then 2 " +
		// " WHEN 'bon' then 3 " +
		// " WHEN 'mor' then 4 " +
		// " WHEN 'pun' then 4 " +
		// " ELSE 5 END AS orden, " +
		// "cc.movimientoCtacte, cc.verificadorCtacte, cc.itemCtacte, p.caratula_id,
		// p.boleto_id as boletoReciboId, cc.objetoi_id as idObjetoi, " +
		// "CASE cc.DTYPE WHEN 'CtaCteAjuste' THEN cc.cotizacionDiferencia " +
		// " ELSE cc.cotizaMov END AS cotizaMov, " +
		// "CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.debePesos " +
		// " WHEN cc.tipomov_id = 2 THEN cc.importe * cc.cotizaMov " +
		// " ELSE 0 END AS debePesos, " +
		// "CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.haberPesos " +
		// " WHEN cc.tipomov_id = 1 THEN cc.importe * cc.cotizaMov " +
		// " ELSE 0 END AS haberPesos " +
		// "FROM Ctacte cc LEFT JOIN Boleto bol ON cc.boleto_id = bol.id LEFT JOIN Pagos
		// p ON p.recibo_id = bol.id, Concepto " +
		// "conc, CConcepto cconc " +
		// "WHERE cc.objetoi_id = ? and cc.asociado_id = conc.id and
		// conc.concepto_concepto = cconc.concepto " +
		// ") t " +
		// "GROUP BY fechaGeneracion, tipo, numeroBoleto, " +
		// "idBoleto, tipomov_id, tipoMovimiento, detalle, caratula_id, boletoReciboId,
		// idObjetoi, fechaVencimiento, cotizaMov " +
		// "ORDER BY fechaProceso, movimiento, item";

		String s = getSql();

		s = s.replace("@DESEMBOLSO_EXPR", (!acuerdoPago ? "'Desembolso' " : "'Deuda exigible' "));

		SQLQuery sql = bp.createSQLQuery(s);
		sql.setLong(0, getIdObjetoi());

		if (credito.getLinea().getMoneda().getDenominacion().equalsIgnoreCase("Dolar")) {
			esDolares = true;
		}

		int numeroDesembolso = 0;
		List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
				.setParameter("idCredito", getIdObjetoi()).list();

		List<Object[]> result = sql.list();

		double saldo = 0;
		double saldoPeso = 0;

		for (Object[] fila : result) {

			bean = new CuentaCorrienteContableBean(fila);

			CtacteKey ck = new CtacteKey();
			ck.setObjetoi_id(getIdObjetoi());
			ck.setPeriodoCtacte(bean.getPeriodoCtacte());
			ck.setMovimientoCtacte(bean.getMovimientoCtacte());
			ck.setVerificadorCtacte(bean.getVerificadorCtacte());
			ck.setItemCtacte(bean.getItemCtacte());

			Ctacte cc = (Ctacte) bp.getById(Ctacte.class, ck);
			if (cc != null) {
				Long asiento = cc.getNroAsiento();
				bean.setAsiento(asiento);
				// if(cc.getCotizaMov()!=null){
				// bean.setCotizaMov(cc.getCotizaMov());
				// cotizacion=cc.getCotizaMov();
				// }else{
				// cotizacion=0;
				// }

			}

			// si es un desembolso, buscar la orden de pago GAF asociada
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Desembolso")) {
				if (numeroDesembolso < desembolsos.size()) {
					Desembolso desembolso = desembolsos.get(numeroDesembolso++); // saco el desembolso que corresponde,
																					// porque vienen ordenados
					bean.setIdOrdenPago(desembolso.getIdOrdepago());
					bean.setNumeroOrdenPago(
							desembolso.getNroOrden() != null ? desembolso.getNroOrden().longValue() : 0);
					bean.setEjercicioOrdenPago(
							desembolso.getOrdepago_ejercicio() != null ? desembolso.getOrdepago_ejercicio().longValue()
									: 0);
					bean.setNumeroBoleto(
							desembolso.getNumeroRecibo() != null ? new Long(desembolso.getNumeroRecibo().longValue())
									: null);
				}
			}

			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Pago Gastos")) {
				if (numeroDesembolso >= 1) {
					Desembolso desembolso = desembolsos.get(numeroDesembolso - 1);
					bean.setIdOrdenPago(desembolso.getIdOrdepago());
					bean.setNumeroOrdenPago(
							desembolso.getNroOrden() != null ? desembolso.getNroOrden().longValue() : 0);
					bean.setEjercicioOrdenPago(
							desembolso.getOrdepago_ejercicio() != null ? desembolso.getOrdepago_ejercicio().longValue()
									: 0);
					bean.setNumeroBoleto(
							desembolso.getNumeroRecibo() != null ? new Long(desembolso.getNumeroRecibo().longValue())
									: null);
				}
			}

			if (esDolares) {
				saldoPeso = saldoPeso + bean.getDebePeso() - bean.getHaberPeso();
				bean.setSaldoPeso(saldoPeso);
			}

			saldo = saldo + bean.getDebe() - bean.getHaber();
			bean.setSaldo(saldo);

			if (desde != null && hasta != null) {
				if (bean.getFechaProceso().getTime() >= desde.getTime()
						&& bean.getFechaProceso().getTime() <= hasta.getTime()) {
					beans.add(bean);
					continue;
				}
				continue;
			}
			beans.add(bean);
		}

		// if(esDolares){
		// agregarAjusteCotizacion();
		// }
		return true;

	}

	private boolean revisarAcuerdoPago() {
		String idLineaAcuerdo = DirectorHelper.getString("linea.acuerdoPago");
		if (idLineaAcuerdo == null) {
			return false;
		}

		return (credito.getLinea_id().toString().equals(idLineaAcuerdo));
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean imprimirCtaCteContable() {
		try {
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			buscarObjetoi();
			setObjetoi((Objetoi) bp.getById(Objetoi.class, getObjetoi().getId()));

			String comportamiento = new CreditoHandler().getAreaResponsable(getObjetoi());

			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("cuentaCorrienteContable.jasper");
			reporte.setParams("idObjetoi=" + getObjetoi().getId() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";MONEDA=" + getObjetoi().getLinea().getMoneda().getDenominacion() + ";TITULAR="
					+ getObjetoi().getPersona().getNomb12() + ";CUIT=" + getObjetoi().getPersona().getCuil12Str()
					+ ";LINEA=" + getObjetoi().getLinea().getNombre() + ";AREA=" + comportamiento + ";MONEDA="
					+ getObjetoi().getLinea().getMoneda().getAbreviatura() + ";PROYECTO="
					+ getObjetoi().getNumeroAtencion().toString() + ";COMPORTAMIENTO="
					+ getObjetoi().getComportamientoActual() + ";SEGURO=" + getObjetoi().getEstadoSeguro()
					+ ";EXPEDIENTE=" + getObjetoi().getExpediente() + ";ETAPA="
					+ getObjetoi().getEstadoActual().getEstado().getNombreEstado() + ";ES_DOLARES="
					+ getObjetoi().getLinea().isDolares() + ";USUARIO="
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";FECHA_DESDE="
					+ DateHelper.getString(desde) + ";FECHA_HASTA=" + DateHelper.getString(hasta) + ";FECHA="
					+ BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";BEANS=objetoiId="
					+ getIdObjetoi() + ";desde=" + DateHelper.getString(desde) + ";hasta="
					+ DateHelper.getString(hasta));
			reporte.setClassBeanName(getClass().getName());
		} catch (Exception e) {
			errors.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "cuentaCorrienteContable"));
		}
		if (errors.isEmpty()) {
			forward = "ReporteManual";
		}
		return errors.isEmpty();
	}

	// public void agregarAjusteCotizacion(){
	// List<CuentaCorrienteContableBean> beansOrdenados = new
	// ArrayList<CuentaCorrienteContableBean>();
	// double primeraCotizacion = 0;
	// double importe = 0;
	// long movimiento = 999;
	// long item = 999;
	// double cotizacionCompra= 0;
	// int cotizaEntero;
	// if(!beans.isEmpty()){
	// primeraCotizacion = beans.get(0).getCotizaMov();
	// importe = beans.get(0).getSaldo();
	// }
	// for (CuentaCorrienteContableBean bean : beans) {
	// Cotizacion cotiza = (Cotizacion) bp.createQuery("SELECT c FROM Cotizacion c
	// WHERE c.fechaDesde <= :desde " +
	// "AND (fechaHasta>= :hasta OR fechaHasta is null) AND c.moneda.abreviatura =
	// :moneda")
	// .setParameter("desde", bean.getFechaGeneracion())
	// .setParameter("hasta", bean.getFechaGeneracion())
	// .setParameter("moneda", "Dolar")
	// .uniqueResult();
	//
	// cotizacionCompra = cotiza.getCompra()*100;
	// cotizaEntero = (int)Math.round(cotizacionCompra);
	// cotizacionCompra = cotizaEntero/(new Double(100)).doubleValue();
	// if(bean.getDetalle().equalsIgnoreCase("Pago") && bean.getCotizaMov()!=
	// primeraCotizacion){
	// CuentaCorrienteContableBean nuevo=
	// generarMovimientoAjustePago(primeraCotizacion, cotizacionCompra, bean,
	// item++, movimiento++);
	// beansOrdenados.add(nuevo);
	// beansOrdenados.add(bean);
	// }else{
	// if(cotizacionCompra==bean.getCotizaMov()){
	// beansOrdenados.add(bean);
	// }else{
	// CuentaCorrienteContableBean nuevo =
	// generarMovimientoAjuste(primeraCotizacion,cotizacionCompra,bean, item++,
	// movimiento++);
	// beansOrdenados.add(bean);
	// beansOrdenados.add(nuevo);
	// primeraCotizacion = cotizacionCompra;
	// }
	//
	// }
	// }
	// setBeans(beansOrdenados);
	// }

	// public CuentaCorrienteContableBean generarMovimientoAjuste(double
	// primera,double cotizacionCompra, CuentaCorrienteContableBean bean, long item,
	// long movimiento){
	// CuentaCorrienteContableBean beanNuevo = new CuentaCorrienteContableBean();
	//
	// beanNuevo.setFechaGeneracion(bean.getFechaGeneracion());
	// beanNuevo.setDetalle("Diferencia de Cambio");
	// beanNuevo.setSaldo(bean.getSaldo());
	// beanNuevo.setCotizaMov(cotizacionCompra);
	// beanNuevo.setIdObjetoi(bean.getIdObjetoi());
	// beanNuevo.setMovimientoCtacte(new Long(movimiento));
	// beanNuevo.setItemCtacte(new Long(item));
	// beanNuevo.setPeriodoCtacte(bean.getPeriodoCtacte());
	// beanNuevo.setVerificadorCtacte(bean.getVerificadorCtacte());
	// beanNuevo.setDiferenciaCotizacion(true);
	// if(bean.getSaldo() * (cotizacionCompra - primera) > 0){
	// beanNuevo.setDebePeso(bean.getSaldoPeso() * Math.abs(cotizacionCompra -
	// primera));
	// }else{
	// beanNuevo.setHaberPeso(bean.getSaldoPeso() * Math.abs(cotizacionCompra -
	// primera));
	// }
	// beanNuevo.setSaldoPeso(bean.getSaldoPeso() + beanNuevo.getDebePeso() -
	// beanNuevo.getHaberPeso());
	// return beanNuevo;
	// }

	// public CuentaCorrienteContableBean generarMovimientoAjustePago(double
	// primera,double cotizacionCompra, CuentaCorrienteContableBean bean, long item,
	// long movimiento){
	// CuentaCorrienteContableBean beanNuevo = new CuentaCorrienteContableBean();
	//
	// beanNuevo.setFechaGeneracion(bean.getFechaGeneracion());
	// beanNuevo.setDetalle("Diferencia de Cambio Cobranza");
	// beanNuevo.setSaldo(bean.getSaldo());
	// beanNuevo.setCotizaMov(bean.getCotizaMov());
	// beanNuevo.setIdObjetoi(bean.getIdObjetoi());
	// beanNuevo.setMovimientoCtacte(new Long(movimiento));
	// beanNuevo.setItemCtacte(new Long(item));
	// beanNuevo.setPeriodoCtacte(bean.getPeriodoCtacte());
	// beanNuevo.setVerificadorCtacte(bean.getVerificadorCtacte());
	// beanNuevo.setDiferenciaCotizacion(true);
	//
	// if(bean.getSaldo() * (cotizacionCompra - primera) > 0){
	// beanNuevo.setDebePeso(bean.getSaldoPeso() * Math.abs(bean.getCotizaMov() -
	// cotizacionCompra));
	// }else{
	// beanNuevo.setHaberPeso(bean.getSaldoPeso() * Math.abs(bean.getCotizaMov() -
	// cotizacionCompra));
	// }
	// beanNuevo.setSaldoPeso(bean.getSaldoPeso() + beanNuevo.getDebePeso() -
	// beanNuevo.getHaberPeso());
	// return beanNuevo;
	// }

	@ProcessMethod
	@SuppressWarnings("unchecked")
	public boolean imprimirBoletos() {
		try {
			List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b WHERE b.id.boleto.id=:boleto")
					.setParameter("boleto", idBoleto).list();
			Double capital = 0.0;
			Double compensatorio = 0.0;
			Double punitorio = 0.0;
			Double moratorio = 0.0;
			for (Bolcon bolcon : bolcons) {
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
					capital = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
					compensatorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
					punitorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
					moratorio = bolcon.getImporte();
				}
			}
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			setObjetoi((Objetoi) bp.getById(Objetoi.class, getIdObjetoi()));
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletosFactura.jasper");
			reporte.setParams("idObjetoi=" + getObjetoi().getId() + ";CAPITAL=" + capital + ";idPersona="
					+ getObjetoi().getPersona_id() + ";MORATORIO=" + moratorio + ";PUNITORIO=" + punitorio
					+ ";COMPENSATORIO=" + compensatorio + ";VENCIMIENTO=" + boleto.getFechaVencimientoStr()
					+ ";EMISION=" + boleto.getFechaEmisionStr() + ";MONEDA="
					+ getObjetoi().getLinea().getMoneda().getDenominacion() + ";SIMBOLO="
					+ getObjetoi().getLinea().getMoneda().getSimbolo() + ";NUMERO_BOLETO=" + boleto.getNumeroBoleto()
					+ ";ID_BOLETO=" + boleto.getId() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";SCHEMA="
					+ BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteContable";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			reporte.setHref(href);
		} catch (Exception e) {
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletosFactura"));
		}

		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}

		return errors.isEmpty();
	}

	@ProcessMethod
	@SuppressWarnings("unchecked")
	public boolean imprimirPago() {
		pago = (Pagos) bp
				.createSQLQuery("SELECT * FROM Pagos p WHERE p.boleto_id = :boleto_id AND p.caratula_id = :caratula_id")
				.addEntity(Pagos.class).setLong("boleto_id", idBoleto).setLong("caratula_id", getIdCaratula())
				.setMaxResults(1).uniqueResult();
		try {

			buscarObjetoi();
			Long idBoleto;
			if (pago.getRecibo() != null) {
				idBoleto = pago.getRecibo().getId();
			} else {
				idBoleto = pago.getId().getBoleto().getId();
			}
			Boleto b = (Boleto) bp.getById(Boleto.class, idBoleto);
			String expediente = b.getObjetoi().getExpediente();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			String consultaBasica = "SELECT d.calleNom, d.numero, d.manzana, b.DESC_BRR, l.CP, l.NOMBRE, d.departamentoNom, d.lote, pr.DETA_08 "
					+ "FROM Objetoi ob  LEFT JOIN Persona pe on ob.persona_IDPERSONA = pe.IDPERSONA "
					+ "LEFT JOIN DomicilioObjetoi do on do.objetoi_id = ob.id  "
					+ "LEFT JOIN Domicilio d on do.domicilio_id = d.id "
					+ "LEFT JOIN LOCALIDAD l on l.IDLOCALIDAD = d.localidad_IDLOCALIDAD "
					+ "LEFT JOIN BARRIO b on b.CODI_BRR = d.barrio_CODI_BRR "
					+ "LEFT JOIN PROVIN pr on pr.CODI_08 = d.provincia_CODI_08 WHERE ob.id = :objetoi_id";
			String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> domicilios = bp.createSQLQuery(consultaLimitada).setLong("objetoi_id", getObjetoi().getId())
					.list();
			Object[] domicilio;
			if (!domicilios.isEmpty()) {
				domicilio = domicilios.get(0);
			} else {
				domicilio = null;
			}
			String calleNom = domicilio == null || domicilio[0] == null ? null : domicilio[0].toString();
			String numero = domicilio == null || domicilio[1] == null ? null : domicilio[1].toString();
			String manzana = domicilio == null || domicilio[2] == null ? null : domicilio[2].toString();
			String barrioNom = domicilio == null || domicilio[3] == null ? "" : domicilio[3].toString();
			String cp = domicilio == null || domicilio[4] == null ? null : domicilio[4].toString();
			String nombre = domicilio == null || domicilio[5] == null ? null : domicilio[5].toString();
			String departamentoNom = domicilio == null || domicilio[6] == null ? null : domicilio[6].toString();
			String lote = domicilio == null || domicilio[7] == null ? null : domicilio[7].toString();
			String provin = domicilio == null || domicilio[8] == null ? null : domicilio[8].toString();

			Boleto boleto;
			if (pago.getBoleto() != null)
				boleto = pago.getBoleto();
			else
				boleto = b;
			String medioPago = (String) bp
					.createQuery("SELECT DISTINCT c.mediopago.denominacion "
							+ "FROM Bolepago bp JOIN bp.id.cobropago c WHERE bp.id.boleto = :boleto")
					.setEntity("boleto", boleto).uniqueResult();
			int anioActual = Ejercicio.getEjercicioActual().intValue();

			consultaBasica = "SELECT b.tiponrocuenta, CASE WHEN idmoneda = 1 THEN 'Pesos' "
					+ "WHEN idmoneda = 2 THEN 'D�lares' WHEN idmoneda = 3 THEN 'Euros' "
					+ "ELSE CAST(idmoneda AS varchar) END AS 'moneda', "
					+ "CASE WHEN tipoCta = 'CC' THEN 'Cuenta Corriente' WHEN tipoCta = 'CA' THEN 'Caja de Ahorro' "
					+ "ELSE tipoCta END AS 'tipoCta' FROM " + this.esquemaGAF + ".ParametrizacionBancos p "
					+ "INNER JOIN " + this.esquemaGAF + ".Bancue b ON b.idBancue = p.idBancue "
					+ "WHERE p.codi_Ba = :codiBa AND p.ejercicio =:ejercicio";
			consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> listado = bp.createSQLQuery(consultaLimitada)
					.setParameter("codiBa", pago.getId().getCaratula().getBanco().getCodiBa())
					.setParameter("ejercicio", anioActual).list();

			String tipoNumeroCuenta = "";
			String tipoCta = "";
			if (!listado.isEmpty()) {
				Object[] object = listado.get(0);
				tipoNumeroCuenta = (String) object[0];
				tipoCta = (String) object[2];
			}

			boolean cambioMoneda = false;
			Long idMonedaCredito = b.getObjetoi().getMoneda_id() != null ? b.getObjetoi().getMoneda_id()
					: b.getObjetoi().getLinea().getMoneda_id();
			if (idMonedaCredito == null) {
				idMonedaCredito = 1L;
			}
			if (!idMonedaCredito.equals(1L)) {
				// el credito no esta en pesos
				cambioMoneda = pago.getMoneda() != null && !pago.getMoneda().getId().equals(idMonedaCredito);
			}

			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("pago.jasper");
			reporte.setParams("PROYECTO=" + getObjetoi().getNumeroAtencionStr() + ";TITULAR="
					+ getObjetoi().getPersona().getNomb12Str() + ";BOLETO_ID=" + idBoleto + ";idObjetoi="
					+ getObjetoi().getId() + ";CUIT=" + getObjetoi().getPersona().getCuil12Str()
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";EXPEDIENTE=" + expediente + ";numeroBoleto="
					+ b.getNumeroBoleto() + ";medioPago=" + medioPago + ";FECHA_EMISION="
					+ b.getFechaEmision().getTime() + ";MONEDA="
					+ (pago.getMoneda() != null ? pago.getMoneda().getDenominacion()
							: b.getObjetoi().getLinea().getMoneda().getDenominacion())
					+ ";calleNom=" + calleNom + ";numero=" + numero + ";manzana=" + manzana + ";barrioNom=" + barrioNom
					+ ";cp=" + cp + ";nombre=" + nombre + ";departamentoNom=" + departamentoNom + ";lote=" + lote
					+ ";provincia=" + provin + ";TIPO_NUMERO_CUENTA=" + tipoNumeroCuenta + ";TIPO_CTA=" + tipoCta
					+ ";BANCO=" + pago.getId().getCaratula().getBanco().getProveedor().getFantasia() + ";SCHEMA="
					+ BusinessPersistance.getSchema()
					+ (cambioMoneda && pago.getMontoOriginal() != null
							? ";montoOriginal=" + DoubleHelper.getString(pago.getMontoOriginal())
							: "")
					+ (cambioMoneda && pago.getCotizacion() != null
							? ";cotizacion=" + DoubleHelper.getString(pago.getCotizacion())
							: "")
					+ (cambioMoneda && pago.getMoneda() != null
							? ";monedaOriginal=" + pago.getMoneda().getDenominacion()
							: "")
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteContable";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			reporte.setHref(href);

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("objetoiPagosProcess",
					getObjetoi());
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "pago"));
		}

		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}

		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean imprimirManualDebito() {
		try {
			buscarObjetoi();

			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);

			Object[] datosCtaCte = (Object[]) bp.createQuery(
					"select cc.comprobante, cc.fechaGeneracion, cc.fechaProceso from Ctacte cc where cc.boleto.id = :idBoleto")
					.setParameter("idBoleto", boleto.getId()).setMaxResults(1).uniqueResult();

			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("notaDebitoManual.jasper");
			reporte.setParams("OBJETOI_ID=" + getIdObjetoi() + ";OBSERVACIONES="
					+ (datosCtaCte != null && datosCtaCte[0] != null ? datosCtaCte[0] : "") // comprobante
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";numeroBoleto="
					+ boleto.getPeriodoBoleto().toString().concat(" - ").concat(boleto.getNumeroBoleto().toString())
					+ ";idBoleto=" + boleto.getId() + ";NOMBRE_COMPROBANTE="
					+ (!minuta ? "NOTA DE D�BITO" : "MINUTA D�BITO") + ";fechaEmisionBoleto="
					+ (datosCtaCte != null && datosCtaCte[2] != null ? DateHelper.getString((Date) datosCtaCte[2]) : "") // fechaProceso
					+ ";fechaAplicacion="
					+ (datosCtaCte != null && datosCtaCte[1] != null ? DateHelper.getString((Date) datosCtaCte[1]) : "") // fechaGeneracion
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteContable";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			reporte.setHref(href);
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "notaDebitoManual"));
		}

		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}

		return errors.isEmpty();
	}

	protected void buscarObjetoi() throws HibernateException, Exception {
		if (idObjetoi != null && idObjetoi != 0) {
			credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		}
	}

	@ProcessMethod
	public boolean imprimirManualCredito() {
		try {
			buscarObjetoi();

			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);

			Object[] datosCtaCte = (Object[]) bp.createQuery(
					"select cc.comprobante, cc.fechaGeneracion, cc.fechaProceso from Ctacte cc where cc.boleto.id = :idBoleto")
					.setParameter("idBoleto", boleto.getId()).setMaxResults(1).uniqueResult();

			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("notaCreditoManual.jasper");
			reporte.setParams("OBJETOI_ID=" + getIdObjetoi() + ";OBSERVACIONES="
					+ (datosCtaCte != null && datosCtaCte[0] != null ? datosCtaCte[0] : "") // comprobante
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";numeroBoleto="
					+ boleto.getPeriodoBoleto().toString().concat(" - ").concat(boleto.getNumeroBoleto().toString())
					+ ";idBoleto=" + boleto.getId() + ";NOMBRE_COMPROBANTE="
					+ (!minuta ? "NOTA DE CR�DITO" : "MINUTA CR�DITO") + ";fechaEmisionBoleto="
					+ (datosCtaCte != null && datosCtaCte[2] != null ? DateHelper.getString((Date) datosCtaCte[2]) : "") // fechaProceso
					+ ";fechaAplicacion="
					+ (datosCtaCte != null && datosCtaCte[1] != null ? DateHelper.getString((Date) datosCtaCte[1]) : "") // fechaGeneracion
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteContable";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			reporte.setHref(href);
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "notaCreditoManual"));
		}
		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}

		return errors.isEmpty();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public List<CuentaCorrienteContableBean> getBeans() {
		return beans;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public String getDesdeStr() {
		return DateHelper.getString(desde);
	}

	public String getHastaStr() {
		return DateHelper.getString(hasta);
	}

	public void setDesdeStr(String d) {
		this.desde = DateHelper.getDate(d);
	}

	public void setHastaStr(String d) {
		this.hasta = DateHelper.getDate(d);
	}

	public String getContextoGAF() {
		return contextoGAF;
	}

	public boolean isEsDolares() {
		return esDolares;
	}

	public void setEsDolares(boolean esDolares) {
		this.esDolares = esDolares;
	}

	public double getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(double cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}

	public Long getIdBoleto() {
		return idBoleto;
	}

	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}

	public void setBeans(List<CuentaCorrienteContableBean> beans) {
		this.beans = beans;
	}

	@Override
	protected String getDefaultForward() {
		return forward;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Objetoi getObjetoi() {
		return credito;
	}

	public void setObjetoi(Objetoi credito) {
		this.credito = credito;
	}

	public Long getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}

	@Override
	public Collection getCollection(HashMap arg0) {
		this.idObjetoi = new Long(arg0.get("objetoiId").toString());
		this.desde = DateHelper.getDate(arg0.get("desde").toString());
		this.hasta = DateHelper.getDate(arg0.get("hasta").toString());
		try {
			buscarObjetoi();
			buscar();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return beans;
	}

	public boolean isMinuta() {
		return minuta;
	}

	public void setMinuta(boolean minuta) {
		this.minuta = minuta;
	}
}
