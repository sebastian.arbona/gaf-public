package com.asf.cred.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.asf.hibernate.mapping.Claves;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.StringHelper;
import com.civitas.hibernate.persona.Persona;
import com.civitas.util.PdfHelper;
import com.nirven.creditos.hibernate.DocumentoResolucion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.expedientes.persistencia.Archivo;
import com.nirven.expedientes.persistencia.TipoDeArchivo;

public class ArchivoCreditoProcess implements IProcess {

	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private FormFile formFile;
	private ObjetoiArchivo objetoiArchivo;

	private Long idTipoDeArchivo;

	private List<ObjetoiArchivo> archivos;
	private List<DocumentoResolucion> documentoResolucion;
	private BusinessPersistance bp;
	private String funcionalidadOrigen;

	public ArchivoCreditoProcess() {
		forward = "ArchivoCredito";
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		objetoiArchivo = new ObjetoiArchivo();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
		} else if (accion.equals("adjuntar")) {
			adjuntar();
		} else if (accion.equals("listar")) {
			listar();
		} else if (accion.equals("guardar")) {
			guardar();
			listar();
			accion = "listar";
		} else if (accion.equals("eliminar")) {
			eliminar();
			listar();
			accion = "listar";
		} else if (accion.equals("cancelar")) {
			listar();
			accion = "listar";
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void listar() {
		archivos = bp
				.createQuery(
						"SELECT oa FROM ObjetoiArchivo oa WHERE oa.credito = :credito ORDER BY oa.fechaAdjunto DESC")
				.setEntity("credito", objetoiArchivo.getCredito()).list();
		documentoResolucion = bp.createQuery(
				"SELECT dr FROM DocumentoResolucion dr WHERE dr.resolucion.proyecto= :proyecto ORDER BY dr.fecha DESC")
				.setParameter("proyecto", objetoiArchivo.getCredito()).list();

	}

	private void guardar() {
		if (faltanCampos()) {
			errores.put("", "");
			return;
		}
		try {
			objetoiArchivo.setArchivo(formFile.getFileData());
			objetoiArchivo.setMimetype(formFile.getContentType());
			objetoiArchivo.setNombre(formFile.getFileName());
			objetoiArchivo.setFechaAdjunto(new Date());
			objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			objetoiArchivo.setFuncionalidadOrigen(funcionalidadOrigen);
			Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, objetoiArchivo.getCredito().getId());
			Persona titular = objetoi.getPersona();
			Claves currentUser = new Claves(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			/**
			 * asigno un objeto completo desde la bdd en lugar de uno con el id solamente,
			 * de esta forma cuando regreso al list puedo mostrar el nombre del tipo de
			 * archivo, de lo contrario esa propiedad no se resuelve hasta salir y volver a
			 * entrar en la pesta�a
			 */
			TipoDeArchivo tipoDeArchivo = (TipoDeArchivo) bp.getById(TipoDeArchivo.class, idTipoDeArchivo);
			String origen = SessionHandler.getCurrentSessionHandler().getIDMODULO();
			Integer paginas = PdfHelper.getPdfCantidadPaginas(formFile.getFileData());
			String descripcion = "Descripcion: " + StringHelper.StripHMTL(objetoiArchivo.getDetalle()) + ";"
					+ "Proyecto: " + objetoi.getNumeroAtencionStr() + ";" + "Titular: " + titular.getNomb12() + ";"
					+ "CUIT: " + titular.getCuil12Str();
			Archivo archivoRepo = new Archivo(formFile.getFileName(), formFile.getContentType(), tipoDeArchivo,
					currentUser, descripcion, origen, paginas);
			archivoRepo.setArchivo(formFile.getFileData());
			bp.save(archivoRepo);
			objetoiArchivo.setArchivoRepo(archivoRepo);
			bp.save(objetoiArchivo);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean faltanCampos() {
		if (formFile == null || formFile.getFileSize() == 0) {
			return true;
		} else if (objetoiArchivo.getCredito().getId() == null || objetoiArchivo.getCredito().getId() == 0L) {
			return true;
		}
		return false;
	}

	private void eliminar() {
		bp.delete(objetoiArchivo);
	}

	private void adjuntar() {
		objetoiArchivo.setCredito((Objetoi) bp.getById(Objetoi.class, objetoiArchivo.getCredito().getId()));
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return null;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public ObjetoiArchivo getObjetoiArchivo() {
		return objetoiArchivo;
	}

	public void setObjetoiArchivo(ObjetoiArchivo objetoiArchivo) {
		this.objetoiArchivo = objetoiArchivo;
	}

	public List<ObjetoiArchivo> getArchivos() {
		return archivos;
	}

	public void setArchivos(List<ObjetoiArchivo> archivos) {
		this.archivos = archivos;
	}

	public Long getIdTipoDeArchivo() {
		return idTipoDeArchivo;
	}

	public void setIdTipoDeArchivo(Long idTipoDeArchivo) {
		this.idTipoDeArchivo = idTipoDeArchivo;
	}

	@SuppressWarnings("rawtypes")
	public List getAdjuntosProceso() {
		Objetoi o = (Objetoi) this.bp.getById(Objetoi.class, this.objetoiArchivo.getCredito().getId());
		return o.getAdjuntosProceso();
	}

	public List<DocumentoResolucion> getDocumentoResolucion() {
		return documentoResolucion;
	}

	public void setDocumentoResolucion(List<DocumentoResolucion> documentoResolucion) {
		this.documentoResolucion = documentoResolucion;
	}

	public List<ObjetoiArchivo> getArchivos(String... funcionalidad) {
		if (this.archivos == null || this.archivos.isEmpty())
			return null;
		List<ObjetoiArchivo> ret = new ArrayList<>();
		for (ObjetoiArchivo oa : this.archivos)
			for (String f : funcionalidad) {
				if (f.isEmpty() && (oa.getFuncionalidadOrigen() == null || oa.getFuncionalidadOrigen().isEmpty()))
					ret.add(oa);
				else if (f.equals(oa.getFuncionalidadOrigen()))
					ret.add(oa);
			}
		return ret;

	}

	public List<ObjetoiArchivo> getAntecedentes() {
		return getArchivos("1");
	}

	public List<ObjetoiArchivo> getNormas() {
		return getArchivos("2");
	}

	public List<ObjetoiArchivo> getNotificaciones() {
		return getArchivos("3");
	}

	public List<ObjetoiArchivo> getGeneral() {
		return getArchivos("4", "");
	}

	public String getFuncionalidadOrigen() {
		return funcionalidadOrigen;
	}

	public void setFuncionalidadOrigen(String funcionalidadOrigen) {
		this.funcionalidadOrigen = funcionalidadOrigen;
	}

}
