package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.importacion.ImportarBcoCredicop;
import com.civitas.importacion.ImportarBcoNacion;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.DetalleArchivo;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.DetalleRecaudacionInv;
import com.nirven.creditos.hibernate.EstadoCheque;

public class DetalleRecaudaciones implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	private Long banco;
	private String bancoPantalla;
	private Date fecha;
	private Date fechaRecaudacion;
	private String fechaStr;
	private BusinessPersistance bp;
	private List<DetalleRecaudacion> importadas = new ArrayList<DetalleRecaudacion>();
	private List<DetalleRecaudacion> asociadas = new ArrayList<DetalleRecaudacion>();
	private List<DetalleRecaudacion> rechazadas = new ArrayList<DetalleRecaudacion>();
	private List<DetalleRecaudacion> pendientes = new ArrayList<DetalleRecaudacion>();
	private List<DetalleRecaudacion> noImportadas = new ArrayList<DetalleRecaudacion>();
	private List<DetalleRecaudacionInv> invalidos = new ArrayList<DetalleRecaudacionInv>();
	private double totalRechazadas;
	private double totalNoImportadas;
	private double totalPendientes;
	private int countAsociadas;
	private int countRechazadas;
	private int countPendientes;
	private int countNoImportadas;

	private List<Recaudacion> recaudaciones;

	protected HashMap<String, Object> errores;
	protected HashMap<String, List<DetalleRecaudacion>> mapa;

	private Long idDetalleArchivo;
	private Long idCaratula;
	private String idFechaCobranza;
	private double totalInvalidos;
	private int countInvalidos;

	public DetalleRecaudaciones() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		mapa = new HashMap<String, List<DetalleRecaudacion>>();
		forward = "DetalleRecaudaciones";
		recaudaciones = new ArrayList<Recaudacion>();
		accion = "listarRecaudaciones";
	}

	@Override
	public boolean doProcess() {
		if (idDetalleArchivo != null) {
			DetalleArchivo detalleArchivo = (DetalleArchivo) bp.getById(DetalleArchivo.class, idDetalleArchivo);

			bancoPantalla = detalleArchivo.getBanco().getDetaBa();
			fechaStr = DateHelper.getString(detalleArchivo.getFechaImportacion());
		}
		if (accion != null && accion.equals("listarRecaudaciones")) {
			llenarTablas();
			buscarInvalidos();
			if (errores.isEmpty()) {
				forward = "DetalleRecaudaciones";
			}
			return errores.isEmpty();
		} else if (accion.equalsIgnoreCase("cancelar")) {
			forward = "cobroPagoList";
			return true;
		} else {
			forward = "DetalleRecaudaciones";
			return true;
		}
	}

	public void conciliarGrupo(List<DetalleRecaudacion> grupo, Recaudacion recaudacion) {
		for (DetalleRecaudacion detalleRecaudacion : grupo) {
			conciliarIndividual(detalleRecaudacion, recaudacion);
		}
	}

	public void conciliarIndividual(DetalleRecaudacion detalle, Recaudacion recaudacion) {
		DetalleRecaudacion d = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class, detalle.getId());
		Recaudacion r = (Recaudacion) bp.getById(Recaudacion.class, recaudacion.getId());
		d.setTempRecaudacion(r);
		bp.update(d);
		if (!asociadas.contains(d)) {
			asociadas.add(d);
		}
	}

	@SuppressWarnings("unchecked")
	public void llenarTablas() {

		List<DetalleRecaudacion> detalles = bp
				.createQuery("SELECT d FROM DetalleRecaudacion d "
						+ "WHERE d.detalleArchivo.id = :id ORDER BY d.banco, d.fechaAcreditacionValor")
				.setParameter("id", getIdDetalleArchivo()).list();

		for (DetalleRecaudacion detalle : detalles) {
			if (detalle.getTipoValor().equalsIgnoreCase(DetalleRecaudacion.TIPO_VALOR_CHEQUE)) {

				if (detalle.getEstadoActual().getEstado()
						.equalsIgnoreCase(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_RECHAZADO)
						|| detalle.getEstadoActual().getEstado()
								.equalsIgnoreCase(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_RECHAZADO_FUERA_CANJE)
						|| detalle.getEstadoActual().getEstado()
								.equalsIgnoreCase(EstadoCheque.ESTADO_CHEQUE_NACION_RECHAZADO)) {
					rechazadas.add(detalle);
				} else if (detalle.getEstadoActual().getEstado()
						.equalsIgnoreCase(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO)
						|| detalle.getEstadoActual().getEstado()
								.equalsIgnoreCase(EstadoCheque.ESTADO_CHEQUE_NACION_PRESENTADO)) {
					pendientes.add(detalle);
				} else {
					importadas.add(detalle);
				}
			} else {
				importadas.add(detalle);
			}

		}

		// nuevo
		double importe = 0;
		boolean primero = true;
		Bancos ultimoBanco = null;
		Long nroOperacion = null;
		Date ultimaFecha = null;
		List<DetalleRecaudacion> grupo = new ArrayList<DetalleRecaudacion>();
		List<Recaudacion> listaRecaudaciones = new ArrayList<Recaudacion>();
		// Por cada detalleRecaudacion verifico si hay una recaudacion que
		// coincida con el detalle

		String codiBa = DirectorHelper.getString(ImportarBcoCredicop.DIRECTOR_CODIBA_CREDICOOP);

		if (importadas.size() > 0) {
			ultimoBanco = importadas.get(0).getBanco();
			ultimaFecha = importadas.get(0).getFechaAcreditacionValor();
			nroOperacion = importadas.get(0).getNumeroOperacion();
			grupo.add(importadas.get(0));
			importe = importadas.get(0).getImporte();

			boolean usaNroOperacion = ultimoBanco.getCodiBa().toString().equals(codiBa);

			if (usaNroOperacion) {
				listaRecaudaciones = bp
						.createQuery("SELECT r FROM Recaudacion r WHERE "
								+ "(r.saldo > :saldo OR r.saldo is null) AND r.fechamovimiento =:fecha "
								+ "AND r.codiBa = :banco AND r.nroOperacion = :nroOperacion")
						.setParameter("saldo", 0.0).setParameter("fecha", ultimaFecha)
						.setParameter("banco", ultimoBanco.getCodiBa()).setParameter("nroOperacion", nroOperacion)
						.list();
			} else {
				listaRecaudaciones = bp
						.createQuery("SELECT r FROM Recaudacion r WHERE "
								+ "(r.saldo > :saldo OR r.saldo is null) AND r.fechamovimiento =:fecha "
								+ "AND r.codiBa = :banco")
						.setParameter("saldo", 0.0).setParameter("fecha", ultimaFecha)
						.setParameter("banco", ultimoBanco.getCodiBa()).list();
			}
		}

		for (DetalleRecaudacion detalleRecaudacion : importadas) {

			if (!detalleRecaudacion.getBanco().getCodiBa().equals(ultimoBanco.getCodiBa())
					|| !detalleRecaudacion.getFechaAcreditacionValor().equals(ultimaFecha)
					|| (detalleRecaudacion.getNumeroOperacion() != null
							&& !detalleRecaudacion.getNumeroOperacion().equals(nroOperacion))) {

				for (Recaudacion recaudacion : listaRecaudaciones) {
					if (listaRecaudaciones.size() == 1) {
						double importeRecaudacion = recaudacion.getSaldoTemporal();
						if (importeRecaudacion > importe || Math.abs(importeRecaudacion - importe) < 0.01) {
							conciliarGrupo(grupo, recaudacion);
							grupo = new ArrayList<DetalleRecaudacion>();
						}
					} else if (Math.abs(recaudacion.getSaldoTemporal() - importe) < 0.01) {
						conciliarGrupo(grupo, recaudacion);
						grupo = new ArrayList<DetalleRecaudacion>();
					}
				}

				if (!grupo.isEmpty()) {
					noImportadas.addAll(grupo);
				}

				if (detalleRecaudacion.getBanco().getCodiBa().toString().equals(codiBa)) {
					listaRecaudaciones = bp
							.createQuery("SELECT r FROM Recaudacion r WHERE "
									+ "r.saldo > :saldo AND r.fechamovimiento =:fecha AND r.codiBa = :banco "
									+ "AND r.nroOperacion = :nroOperacion")
							.setParameter("saldo", 0.0)
							.setParameter("fecha", detalleRecaudacion.getFechaAcreditacionValor())
							.setParameter("banco", detalleRecaudacion.getBanco().getCodiBa())
							.setParameter("nroOperacion", detalleRecaudacion.getNumeroOperacion()).list();
				} else {
					listaRecaudaciones = bp
							.createQuery("SELECT r FROM Recaudacion r WHERE "
									+ "r.saldo > :saldo AND r.fechamovimiento =:fecha AND r.codiBa = :banco ")
							.setParameter("saldo", 0.0)
							.setParameter("fecha", detalleRecaudacion.getFechaAcreditacionValor())
							.setParameter("banco", detalleRecaudacion.getBanco().getCodiBa()).list();
				}

				ultimoBanco = detalleRecaudacion.getBanco();
				ultimaFecha = detalleRecaudacion.getFechaAcreditacionValor();
				nroOperacion = detalleRecaudacion.getNumeroOperacion();
				grupo = new ArrayList<DetalleRecaudacion>();
				importe = 0.0;
			}

			for (Iterator iter = listaRecaudaciones.iterator(); iter.hasNext();) {
				boolean conciliar = false;

				// Comparo si el saldo de cada recaudacion == saldo del detalle de la
				// recaudacion
				Recaudacion recaudacion = (Recaudacion) iter.next();
				if (detalleRecaudacion.getNumeroOperacion() != null && recaudacion.getNroOperacion() != null
						&& detalleRecaudacion.getNumeroOperacion().equals(recaudacion.getNroOperacion())) {
					conciliar = true;
				} else {
					if (Math.abs(recaudacion.getSaldoTemporal().doubleValue()
							- detalleRecaudacion.getImporte().doubleValue()) < 0.01) {
						conciliar = true;
					}
				}

				if (conciliar) {
					conciliarIndividual(detalleRecaudacion, recaudacion);
					iter.remove();
					if (primero) {
						grupo.remove(detalleRecaudacion);
						importe -= detalleRecaudacion.getImporte();
					}
				}
			}

			if (!grupo.contains(detalleRecaudacion) && detalleRecaudacion.getTempRecaudacion() == null) {
				grupo.add(detalleRecaudacion);
				importe += detalleRecaudacion.getImporte();
			}
			primero = false;
			// fin nuevo
		}

		if (!grupo.isEmpty()) {
			if (listaRecaudaciones.size() == 1) {
				double importeRecaudacion = listaRecaudaciones.get(0).getSaldoTemporal().doubleValue();
				if (importeRecaudacion > importe || Math.abs(importeRecaudacion - importe) < 0.01) {
					conciliarGrupo(grupo, listaRecaudaciones.get(0));
					grupo = new ArrayList<DetalleRecaudacion>();
				}
			} else if (listaRecaudaciones.size() > 1) {
				for (Recaudacion recaudacion : listaRecaudaciones) {
					if (Math.abs(recaudacion.getSaldoTemporal().doubleValue() - importe) < 0.01) {
						conciliarGrupo(grupo, recaudacion);
						grupo = new ArrayList<DetalleRecaudacion>();
					}
				}
			}
		}

		if (!grupo.isEmpty()) {
			noImportadas.addAll(grupo);
		}

		for (DetalleRecaudacion dr : asociadas) {
			Recaudacion recaudacion = dr.getTempRecaudacion();
			List<DetalleRecaudacion> detallesRecaudaciones = null;
			if (mapa.containsKey(recaudacion.getId().toString())) {
				detallesRecaudaciones = mapa.get(recaudacion.getId().toString());
			} else {
				detallesRecaudaciones = new ArrayList<DetalleRecaudacion>();
				recaudaciones.add(recaudacion);
			}

			detallesRecaudaciones.add(dr);
			mapa.put(recaudacion.getId().toString(), detallesRecaudaciones);
		}

	}

	@SuppressWarnings("unchecked")
	private void buscarInvalidos() {
		invalidos = bp
				.createQuery("select d from DetalleRecaudacionInv d where d.detalleArchivo.id = :idDetalleArchivo")
				.setParameter("idDetalleArchivo", getIdDetalleArchivo()).list();
		countInvalidos = invalidos.size();
		Number total = (Number) bp.createQuery(
				"select sum(d.importe) from DetalleRecaudacionInv d where d.detalleArchivo.id = :idDetalleArchivo")
				.setParameter("idDetalleArchivo", getIdDetalleArchivo()).uniqueResult();
		if (total != null) {
			totalInvalidos = total.doubleValue();
		}
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

	@Override
	public String getForward() {

		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public Long getBanco() {
		return banco;
	}

	public void setBanco(Long banco) {
		this.banco = banco;
	}

	public Date getFecha() {
		return DateHelper.getDate(fechaStr);
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<DetalleRecaudacion> getImportadas() {
		return importadas;
	}

	public void setImportadas(List<DetalleRecaudacion> importadas) {
		this.importadas = importadas;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public List<DetalleRecaudacion> getRechazadas() {
		return rechazadas;
	}

	public void setRechazadas(List<DetalleRecaudacion> rechazadas) {
		this.rechazadas = rechazadas;
	}

	public List<DetalleRecaudacion> getPendientes() {
		return pendientes;
	}

	public void setPendientes(List<DetalleRecaudacion> pendientes) {
		this.pendientes = pendientes;
	}

	public List<DetalleRecaudacion> getNoImportadas() {
		return noImportadas;
	}

	public void setNoImportadas(List<DetalleRecaudacion> noImportadas) {
		this.noImportadas = noImportadas;
	}

	public String getFechaStr() {
		return fechaStr;
	}

	public void setFechaStr(String fechaStr) {
		this.fechaStr = fechaStr;
	}

	public List<Recaudacion> getRecaudaciones() {
		return recaudaciones;
	}

	public void setRecaudaciones(List<Recaudacion> recaudaciones) {
		this.recaudaciones = recaudaciones;
	}

	public HashMap<String, List<DetalleRecaudacion>> getMapa() {
		return mapa;
	}

	public void setMapa(HashMap<String, List<DetalleRecaudacion>> mapa) {
		this.mapa = mapa;
	}

	public List<DetalleRecaudacion> getDetalles(String key) {
		return mapa.get(key);
	}

	public void setDetalles(String key, List<DetalleRecaudacion> detalles) {
		mapa.put(key, detalles);
	}

	public Long getIdDetalleArchivo() {
		return idDetalleArchivo;
	}

	public void setIdDetalleArchivo(Long idDetalleArchivo) {
		this.idDetalleArchivo = idDetalleArchivo;
	}

	public Date getFechaRecaudacion() {
		return fechaRecaudacion;
	}

	public void setFechaRecaudacion(Date fechaRecaudacion) {
		this.fechaRecaudacion = fechaRecaudacion;
	}

	public String getBancoPantalla() {
		return bancoPantalla;
	}

	public void setBancoPantalla(String bancoPantalla) {
		this.bancoPantalla = bancoPantalla;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}

	public String getIdFechaCobranza() {
		return idFechaCobranza;
	}

	public void setIdFechaCobranza(String idFechaCobranza) {
		this.idFechaCobranza = idFechaCobranza;
	}

	public int getCountAsociadas() {
		for (DetalleRecaudacion asociada : asociadas) {
			countAsociadas++;
		}
		return countAsociadas;
	}

	public int getCountPendientes() {
		for (DetalleRecaudacion pendiente : pendientes) {
			countPendientes++;
		}
		return countPendientes;
	}

	public int getCountRechazadas() {
		for (DetalleRecaudacion rechazada : rechazadas) {
			countRechazadas++;
		}
		return countRechazadas;
	}

	public int getCountNoImportadas() {
		for (DetalleRecaudacion noImportada : noImportadas) {
			countNoImportadas++;
		}
		return countNoImportadas;
	}

	public double getTotalNoImportadas() {
		totalNoImportadas = 0;
		for (DetalleRecaudacion detalles : noImportadas) {
			totalNoImportadas += detalles.getImporte();
		}
		return totalNoImportadas;
	}

	public double getTotalPendientes() {
		totalPendientes = 0;
		for (DetalleRecaudacion detalles : pendientes) {
			totalPendientes += detalles.getImporte();
		}
		return totalPendientes;
	}

	public double getTotalRechazadas() {
		totalRechazadas = 0;
		for (DetalleRecaudacion detalles : rechazadas) {
			totalRechazadas += detalles.getImporte();
		}
		return totalRechazadas;
	}

	public List<DetalleRecaudacionInv> getInvalidos() {
		return invalidos;
	}

	public void setInvalidos(List<DetalleRecaudacionInv> invalidos) {
		this.invalidos = invalidos;
	}

	public double getTotalInvalidos() {
		return totalInvalidos;
	}

	public int getCountInvalidos() {
		return countInvalidos;
	}
}
