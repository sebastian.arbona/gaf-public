package com.asf.cred.business;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;

import com.asf.cred.TipificadorHandler;
import com.asf.cred.security.SessionHandler;
import com.asf.gaf.DirectorHandler;
import com.asf.gaf.hibernate.Cotizacion;
import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IReportBean;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.NumberHelper;
import com.asf.util.ReportResult;
import com.asf.util.TipificadorHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.AbstractEstado;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Emideta;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.GarantiaTasacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CreditoCuentaCorriente extends CreditoProcess implements IReportBean {

	private List<BeanCtaCte> beans;
	private Date hastaFecha;
	private Date fechaDesde;
	private String forward;

	private String cuotaId;
	private String concepto;
	private String motivo;
	private String tipoMovimiento;
	private String tipo;
	private Date fechaMovimiento;
	private Double montoActual;
	private Double montoMovimiento;
	private Double montoFinal;
	private String observaciones;
	private String numeroAutorizacion;
	private String detalle;

	private Ctacte ctacte;
	private Double ccId;
	private Cuota cuota;

	private String fechaStr;
	private double interesCompensatorio;
	private double interesMoratoriosPunitorios;
	private double bonificaciones;
	private double gastos;
	private double gastosRec;
	private double capitalDevengado;
	private double totalDevengado;
	private double totalPagado;
	private double totalAdeudado;
	private boolean eligefecha = false;
	private int diasPeriodo;
	private int diasTranscurridos;
	private boolean calcular = false;
	private DecimalFormat ft = new DecimalFormat("#################.##");
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private String fechaVencimiento;
	private String leyenda;
	private boolean emitir;
	private ReportResult reporte;
	private Boleto boleto;
	private boolean montoCero;
	private boolean deudaParcial;
	// atribs para reporte DEByCRED manual
	private Double importe;
	private Date fechaRep;
	private String conceptoRep;
	private String estadoAcuerdo;
	private boolean ocultarCampos;
	private Objetoi obj;
	private String nombreReporte = "";
	private String montoValor;
	private String numeroValor;
	private String bancoEmisor;
	private String sucursal;
	private boolean cer;
	private List<AuditoriaFinal> auditoriaFinal;
	private String auditoriaFinalPos;

	public CreditoCuentaCorriente() {
		Long idObjetoi = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("creditoCC.idObjetoi");
		if (idObjetoi != null) {
			setIdObjetoi(idObjetoi);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.idObjetoi",
					null);
		}
		Long personaId = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("creditoCC.personaId");
		if (personaId != null) {
			setPersonaId(personaId);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.personaId",
					null);
		}
		boleto = new Boleto();
		String hastaFechaStr = DateHelper.getString(new Date());
		hastaFecha = DateHelper.getDate(hastaFechaStr);
		DirectorHelper.getString("SGAF");
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		try {
			estadoAcuerdo = DirectorHandler.getDirector("estado.ObjetoOriginal").getValor();
		} catch (Exception e) {
			e.printStackTrace();
		}

		auditoriaFinal = bp.createQuery("Select n from AuditoriaFinal n where n.credito.id=:credito")
				.setParameter("credito", getIdObjetoi()).list();
		if (auditoriaFinal.isEmpty()) {
			setAuditoriaFinalPos("");
		} else {
			setAuditoriaFinalPos(auditoriaFinal.get(auditoriaFinal.size() - 1).getAplicaFondo());
		}

		obj = buscarObjetoi(getIdObjetoi());

		if (obj != null) {
			if (obj.getEstadoActual() != null
					&& obj.getEstadoActual().getEstado().getId().toString().equalsIgnoreCase(estadoAcuerdo)) {
				ocultarCampos = true;
			} else if (obj.getCaducidadPlazo() != null) {
				ocultarCampos = false; // true;
			} else if (obj.getLinea().isLineaAcuerdoPago() && obj.getEstadoActual().getEstado().getNombreEstado()
					.equalsIgnoreCase(AbstractEstado.ACUERDO_INCUMPLIDO)) {
				ocultarCampos = true;
			} else {
				ocultarCampos = false;
			}
		}
		cer = obj.getLinea().isLineaCer();
		if (action == null || action.equals("load")) {
			setCalcular(false);
			super.doProcess();
			if (hastaFecha == null || getIdObjetoi() == null) {
				beans = (List<BeanCtaCte>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("CreditoCtaCte.beans");
			} else {
				buscarCtaCte();
			}
			crearResumen();
			forward = "CreditoCuentaCorriente";
		} else if (action != null && action.equals("crearCuota")) {
			super.doProcess();
			List<Cuota> cuotas = bp.createQuery("Select c from Cuota c where c.credito = :credito")
					.setParameter("credito", bp.getById(Objetoi.class, getIdObjetoi())).list();
			Cuota primerCuota = new Cuota();
			for (Cuota cuota : cuotas) {
				if (cuota.getNumero().compareTo(1) == 0) {
					primerCuota = cuota;
					break;
				}
			}
			setCuotaId(primerCuota.getId().toString());
			cuota = (Cuota) bp.getById(Cuota.class, new Long(cuotaId));
			concepto = "CAP";
			tipo = "CR";
			cargarCtaCte();
			forward = "ModificarCuota";
		} else if (action != null & action.equals("save")) {
			cuota = (Cuota) bp.getNamedQuery("Cuota.findById").setLong("idCuota", Long.parseLong(getCuotaId()))
					.uniqueResult();
			if (montoMovimiento.compareTo(0.0) > 0) {
				guardarCtaCte();
				verificarCambioEstadoCtaCte();
				action = "load";
				doProcess();
			} else {
				cargarCtaCte();
				montoCero = true;
				forward = "ModificarCuota";
			}

		} else if (action != null & action.equals("calcularIntereses")) {
			setCalcular(true);
			super.doProcess();
			buscarCtaCte();
			crearResumen();
			forward = "CreditoCuentaCorriente";

		} else if (action != null & action.equals("emitir")) {
			emitir = true;
			setCalcular(false);
			super.doProcess();
			buscarCtaCte();
			crearResumen();
			forward = "CreditoCuentaCorriente";
		} else if (action != null & action.equals("generar")) {
			generarReporte();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		} else if (action != null & action.equals("imprimirBoleto")) {
			imprimirBoletos();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		} else if (action != null & action.equals("imprimirManualDebito")) {
			imprimirManualDebito();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		} else if (action != null && action.equals("imprimirManualCredito")) {
			imprimirManualCredito();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		} else if (action != null & action.equals("imprimirPago")) {
			imprimirPagos();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		} else if (action != null & action.equals("imprimirCtaCte")) {
			setObjetoi(obj);
			if (hastaFecha == null || getIdObjetoi() == null) {
				beans = (List<BeanCtaCte>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("CreditoCtaCte.beans");
			} else {
				buscarCtaCte();
			}
			crearResumen();
			imprimirCtaCte();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		} else if (action != null && action.equals("actualizarCuota")) {
			cuota = (Cuota) bp.getById(Cuota.class, new Long(getCuotaId()));
			cargarCtaCte();
			forward = "ModificarCuota";
		} else if (action != null && action.equals("imprimirInforme")) {
			imprimirInforme();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
		}
		return errores.isEmpty();
	}

	protected boolean verificarLineaCER() {
		String directorLineasCER = DirectorHelper.getString("indiceCER.lineas");
		String[] lineasCER;
		if (directorLineasCER != null) {
			lineasCER = directorLineasCER.split(",");
		} else {
			lineasCER = new String[0];
		}
		Arrays.sort(lineasCER);

		return (Arrays.binarySearch(lineasCER, obj.getLinea_id().toString()) >= 0);
	}

	private void imprimirInforme() {
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		String marcaagua = DirectorHelper.getString("marca.agua");
		try {
			Objetoi objetoi = obj;
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("otorgamiento.jasper");
			reporte.setParams("idObjetoi=" + getIdObjetoi() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";idPersona=" + objetoi.getPersona_id() + ";relacionCotomadorId=" + relacionCotomadorId
					+ ";MONEDA=" + objetoi.getLinea().getMoneda().getSimbolo() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";MARCAAGUA="
					+ marcaagua);
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "otorgamiento"));
		}
	}

	@SuppressWarnings("unchecked")
	public void verificarCambioEstadoCtaCte() {
		CreditoCalculoDeuda c = new CreditoCalculoDeuda(getIdObjetoi(), new Date());
		c.calcular();
		double deudaTotal = c.getDeudaTotal();

		if (deudaTotal < 0.01) {
			Estado estadoNuevo = (Estado) bp.getNamedQuery("Estado.findByNombreEstado")
					.setParameter("nombreEstado", Estado.CANCELADO).uniqueResult();
			ObjetoiEstado estadoActual = getObjetoi().getEstadoActual();
			if (!estadoActual.getEstado().getNombreEstado().equals(Estado.CANCELADO)) {
				estadoActual.setFechaHasta(new Date());
				ObjetoiEstado e = new ObjetoiEstado();
				e.setEstado(estadoNuevo);
				e.setFecha(new Date());
				e.setObservacion("Cancelado por movimiento manual.");
				e.setObjetoi(getObjetoi());
				bp.update(estadoActual);
				bp.saveOrUpdate(e);
				actualizarEstadoObjetoiGarantia(getObjetoi(), e.getFecha());
			}
		} else {
			// no deberia estar cancelado si tiene deuda
			ObjetoiEstado estadoActual = getObjetoi().getEstadoActual();
			if (estadoActual.getEstado().getNombreEstado().equalsIgnoreCase(AbstractEstado.CANCELADO)) {
				getObjetoi().setEstadoActual(AbstractEstado.EJECUCION, "Modificado por movimiento manual.");
			}
		}

		// revisar estado de cuotas
		CreditoHandler ch = new CreditoHandler();
		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", getIdObjetoi()).list();
		for (Cuota cuota : cuotas) {
			if (cuota.getEmision() != null) {
				List<Ctacte> movimientosCapital = bp.getNamedQuery("Ctacte.findByCuota")
						.setLong("idCuota", cuota.getId()).list();
				double saldo = ch.calcularSaldo(movimientosCapital);
				if (saldo < 0.01) {
					cuota.setEstado(Cuota.CANCELADA);
					bp.update(cuota);
				} else if (cuota.getEstado() != null && cuota.getEstado().equals(Cuota.CANCELADA)) {
					cuota.setEstado(Cuota.SIN_CANCELAR);
					bp.update(cuota);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void actualizarEstadoObjetoiGarantia(Objetoi credito, Date fecha) {
		// TODO ojo esto esta duplicado en CreditoCuentaCorriente, CierreCaja,
		// CreditoDatosFinancieros, FinalizarCredito
		List<ObjetoiGarantia> og = bp
				.createQuery("SELECT g from ObjetoiGarantia g where g.objetoi = :credito and g.baja is null")
				.setParameter("credito", credito).list();
		if (!og.isEmpty()) {
			for (ObjetoiGarantia objetoiGarantia : og) {
				if (objetoiGarantia != null) {
					if (!credito.getTieneGarantiaFiduciaria()) {
						GarantiaTasacion tasacion = objetoiGarantia.getTasacion();
						tasacion.setFechaCancelacion(fecha);
						GarantiaEstado nuevoEstadoGarantia = new GarantiaEstado(ComportamientoGarantiaEnum.CANCELADA,
								tasacion.getFechaCancelacion());
						nuevoEstadoGarantia.setObjetoiGarantia(objetoiGarantia);
						nuevoEstadoGarantia.determinarImporte();
						bp.save(nuevoEstadoGarantia);
						bp.update(tasacion);
						if (objetoiGarantia.getObjetoi().getLinea().isLineaAcuerdoPago()) {
							// Buscar y Cancelar Garant�a del cr�dito original
							Objetoi creditoOriginal = new Objetoi();
							String sqlCreditoOriginal = "SELECT o FROM Objetoi o WHERE o.acuerdoPago.id = :idAcuerdoPago ";
							creditoOriginal = (Objetoi) bp.createQuery(sqlCreditoOriginal)
									.setLong("idAcuerdoPago", objetoiGarantia.getObjetoi().getId()).uniqueResult();
							if (creditoOriginal != null) {
								ObjetoiGarantia ogOriginal = new ObjetoiGarantia();
								String sqlGarantiaOriginal = "SELECT g FROM ObjetoiGarantia g WHERE g.objetoi.id = :idObjetoi AND g.garantia.id = :idGarantia";
								ogOriginal = (ObjetoiGarantia) bp.createQuery(sqlGarantiaOriginal)
										.setLong("idObjetoi", creditoOriginal.getId())
										.setLong("idGarantia", objetoiGarantia.getGarantia().getId()).uniqueResult();
								if (ogOriginal != null) {
									GarantiaEstado estadoNuevoOriginal = new GarantiaEstado(
											ComportamientoGarantiaEnum.CANCELADA_ACUERDO,
											objetoiGarantia.getObjetoi().getEstadoActual().getFecha());
									estadoNuevoOriginal.setObjetoiGarantia(ogOriginal);
									estadoNuevoOriginal.determinarImporte();
									bp.save(estadoNuevoOriginal);
								}
							}
						}
					}
				}
			}
		}
	}

	@SuppressWarnings({ "static-access" })
	private void imprimirCtaCte() {
		try {

			String comportamiento = new CreditoHandler().getAreaResponsable(getObjetoi());

			reporte = new ReportResult();
			reporte.setExport("PDF");
			if (cer) {
				reporte.setReportName("InformeCtaCteCER.jasper");
			} else {
				reporte.setReportName("InformeCtaCte.jasper");
			}
			String marcaagua = DirectorHelper.getString("marca.agua");
			reporte.setClassBeanName(CreditoCuentaCorriente.class.getName());
			reporte.setParams("LINEA=" + obj.getLinea().getNombre() + ";idObjetoi=" + obj.getId() + ";MONEDA="
					+ obj.getLinea().getMoneda().getDenominacion() + ";LINEA=" + obj.getLinea().getNombre() + ";MONEDA="
					+ obj.getLinea().getMoneda().getAbreviatura() + ";COMPORTAMIENTO=" + obj.getComportamientoActual()
					+ ";AREA=" + comportamiento + ";EXPEDIENTE=" + obj.getExpediente() + ";ETAPA="
					+ obj.getEstadoActual().getEstado().getNombreEstado() + ";HASTAFECHA=" + getHastaFechaStr()
					+ ";COMPENSATORIO=" + getInteresCompensatorioStr() + ";MORPUN="
					+ getInteresMoratoriosPunitoriosStr() + ";BONIFICACION=" + getBonificacionesStr() + ";GASTOS="
					+ getGastosStr() + ";DEVENGADO=" + getCapitalDevengadoStr() + ";TOTALDEVENGADO="
					+ getTotalDevengadoStr() + ";PAGADO=" + getTotalPagadoStr() + ";ADEUDADO=" + getTotalAdeudadoStr()
					+ ";NOMBRE=" + obj.getPersona().getNomb12() + ";CUIL=" + obj.getPersona().getCuil12Str()
					+ ";NROATENCION=" + obj.getNumeroAtencionStr() + ";ESTADO="
					+ obj.getEstadoActual().getEstado().getNombreEstado() + ";SEGURO=" + obj.getEstadoSeguro()
					+ ";SCHEMA=" + SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema()
					+ ";AUDIFINAL=" + getAuditoriaFinalPos() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";OBSERVACIONES="
					+ observaciones + "BEANS=idObjetoi=" + getIdObjetoi() + ";fechaHasta="
					+ DateHelper.getString(this.hastaFecha) + ";MARCAAGUA=" + marcaagua);

		} catch (Exception e) {
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "InformeCtaCte"));
		}
	}

	@SuppressWarnings({ "unused" })
	private Cuota calcularIntereses(Cuota cuotaCalc) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		double moratorio = 0.0;
		double punitorio = 0.0;
		double compensatorio = 0.0;
		buscarPeriodo(cuotaCalc);
		if (diasPeriodo != 0 & diasTranscurridos != 0) {
			compensatorio = credito.calcularCompensatorioNew(cuotaCalc.getCapital(), diasPeriodo, diasTranscurridos,
					new Date(), fechaDesde, hastaFecha);
			moratorio = credito.calcularMoratorioNew(cuotaCalc.getCapital(), diasPeriodo, diasTranscurridos, new Date(),
					fechaDesde, hastaFecha);
			punitorio = credito.calcularPunitorioNew(cuotaCalc.getCapital(), diasPeriodo, diasTranscurridos, new Date(),
					fechaDesde, hastaFecha);
		}
		cuotaCalc.setPunitorio(punitorio);
		cuotaCalc.setCompensatorio(compensatorio);
		cuotaCalc.setMoratorio(moratorio);
		return cuotaCalc;
	}

	@SuppressWarnings("unchecked")
	private void buscarPeriodo(Cuota cuota) {
		if (cuota.getNumero() == 1) {
			// para la primera cuota se calcula con el primer desembolso
			List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
					.setLong("idCredito", getIdObjetoi()).setMaxResults(1).list();
			Desembolso primerDesembolso = null;
			if (!desembolsos.isEmpty()) {
				primerDesembolso = desembolsos.get(0);
			}
			if (primerDesembolso != null) {
				Date fechaDesembolso = DateHelper.resetHoraToZero(primerDesembolso.getFechaReal());
				diasPeriodo = (int) DateHelper.getDiffDates(fechaDesembolso, cuota.getFechaVencimiento(), 2);
				diasTranscurridos = (int) DateHelper.getDiffDates(fechaDesembolso, hastaFecha, 2);
				fechaDesde = fechaDesembolso;
			}
		} else {
			List<Cuota> cuotasAnteriores = bp.createQuery(
					"select c from Cuota c where c.credito.id = :idCredito and c.numero < :nroCuota order by c.numero desc")
					.setParameter("idCredito", getIdObjetoi()).setParameter("nroCuota", cuota.getNumero())
					.setMaxResults(1).list();
			if (!cuotasAnteriores.isEmpty()) {
				// la anterior
				Cuota cuotaAnterior = cuotasAnteriores.get(0);
				diasPeriodo = (int) DateHelper.getDiffDates(cuotaAnterior.getFechaVencimiento(),
						cuota.getFechaVencimiento(), 2);
				diasTranscurridos = (int) DateHelper.getDiffDates(cuotaAnterior.getFechaVencimiento(), hastaFecha, 2);
				fechaDesde = cuotaAnterior.getFechaVencimiento();
			}
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private List<Cuota> buscarCuotas() {
		return bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", getIdObjetoi()).list();
	}

	private void crearResumen() {
		this.fechaStr = (hastaFecha == null ? null : DateHelper.getString(hastaFecha));
		BeanCtaCte bean;
		for (int i = 0; i < beans.size() - 1; i++) {
			bean = beans.get(i);
			Cuota cuota = (Cuota) bp.getById(Cuota.class, bean.getIdCuota());
			Date fechaComparacion = new Date();
			if (!(hastaFecha == null)) {
				fechaComparacion = hastaFecha;
			} else {
				fechaComparacion = DateHelper.getFechaJuliana(cuota.getFechaVencimiento(), 1);
			}
			if (bean.getDetalle().equals("Cuota")) {
				interesCompensatorio += (bean.getCompensatorio() == null ? 0 : bean.getCompensatorio());
				interesMoratoriosPunitorios += ((bean.getMoratorio() == null ? 0 : bean.getMoratorio())
						+ (bean.getPunitorio() == null ? 0 : bean.getPunitorio()));
				gastos += (bean.getGastos() == null ? 0 : bean.getGastos())
						+ (bean.getMultas() == null ? 0 : bean.getMultas());
				gastosRec += (bean.getGastosRec() == null ? 0 : bean.getGastosRec());
				if (cuota.getEmision() != null) {
					// debe haber una sola
					Emideta liquidacion = (Emideta) bp.getNamedQuery("Emideta.findByCreditoCuota")
							.setParameter("idCredito", cuota.getCredito_id()).setParameter("numero", cuota.getNumero())
							.setMaxResults(1).uniqueResult();
					capitalDevengado += liquidacion.getCapital();
				} else if (cuota.getFechaVencimiento().getTime() <= fechaComparacion.getTime()) {
					capitalDevengado += (bean.getCapital() == null ? 0 : bean.getCapital());
				}
			} else if (bean.getDetalle().equals("Pago")) {
				totalPagado += ((bean.getCapital() == null ? 0 : bean.getCapital())
						+ (bean.getCompensatorio() == null ? 0 : bean.getCompensatorio())
						+ (bean.getMoratorio() == null ? 0 : bean.getMoratorio())
						+ (bean.getPunitorio() == null ? 0 : bean.getPunitorio())
						+ (bean.getGastos() == null ? 0 : bean.getGastos()));
			} else if (bean.getDetalle().equals("Bonificacion")) {
				bonificaciones += (bean.getCompensatorio() == null ? 0 : bean.getCompensatorio());
			}
		}

		totalDevengado = interesCompensatorio + interesMoratoriosPunitorios - bonificaciones + gastos + gastosRec
				+ capitalDevengado;

		totalAdeudado = totalDevengado - totalPagado;
	}

	private void guardarCtaCte() {
		crearAjuste();
	}

	@SuppressWarnings("unchecked")
	private void crearAjuste() {
		Tipomov tipoMovimiento = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", tipo)
				.uniqueResult();

		String detalleTip = detalle;
		Tipificadores tf_caidaAcuerdoPago = TipificadorHelper.getTipificador("ctacte.detalle",
				Ctacte.TIPO_CONCEPTO_CAIDA_ACUERDO_PAGO);
		if (tf_caidaAcuerdoPago != null) {
			if (tf_caidaAcuerdoPago.getDescripcion().equals(detalle)) {
				if ("DB".equals(tipoMovimiento.getAbreviatura())) {
					detalleTip = "Nota de D�bito por ca�da de Acuerdo de Pago";
				} else {
					detalleTip = "Reversi�n de movimientos por Ca�da de Acuerdo";
				}
			}
		}

		ctacte = new CreditoHandler().crearCtaCte(cuota, getConcepto(), tipoMovimiento, getMontoFinal(), detalleTip);
		ctacte.setImporte(montoMovimiento);
		ctacte.redondear();
		ctacte.setComprobante(getObservaciones());
		if (fechaMovimiento != null) {
			ctacte.setFechaGeneracion(fechaMovimiento);
			ctacte.setFechaVencimiento(fechaMovimiento);
		}
		try {
			List<Tipificadores> tipifsDetalle = TipificadorHandler.getTipificadores("ctacte.detalle");
			for (Tipificadores tipif : tipifsDetalle) {
				if (tipif.getDescripcion().equalsIgnoreCase(detalle)) {
					ctacte.setTipoConcepto(tipif.getCodigo());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// ctacte.setExpediente(getNumeroAutorizacion());
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		ctacte.setUsuario(usuario);
		if (!(ctacte.getCuota().getCredito().getLinea().getMoneda().getAbreviatura().contains("Peso"))) {
			List<Cotizacion> cotizaciones = bp
					.createQuery("Select c from Cotizacion c where c.moneda=:moneda order by c.fechaDesde")
					.setParameter("moneda", ctacte.getCuota().getCredito().getLinea().getMoneda()).list();
			Cotizacion coti = null;
			for (Cotizacion cotizacion : cotizaciones) {
				if (coti == null) {
					coti = cotizacion;
				} else {
					if (cotizacion.getFechaHasta() == null || coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
						coti = cotizacion;
					}
				}
			}
			if (coti != null) {
				ctacte.setCotizaMov(coti.getCompra());
			}
		}
		// GENERACION DE BOLETO
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		Boleto b = new Boleto();
		b.setFechaEmision(new Date());
		b.setFechaVencimiento(cuota.getFechaVencimiento());
		b.setImporte(ctacte.getImporte());
		b.setUsuario(usuario);
		b.setNumeroCuota(cuota.getNumero());
		credito = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		b.setObjetoi(credito);
		b.setPeriodoBoleto((long) periodo);

		if (tipo.equals("CR")) {
			ctacte.setTipoMovimiento("movManualCred");
			b.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
		} else {
			ctacte.setTipoMovimiento("movManualDeb");
			b.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
		}

		Tipomov tipoMov = (Tipomov) bp.createQuery("Select t from Tipomov t where t.abreviatura=:ab")
				.setParameter("ab", tipo).uniqueResult();
		b.setNumeroBoleto(Numerador.getNext(b.getNumerador()));
		b.setVerificadorBoleto(0L);

		Bolcon bc = new Bolcon();
		BolconKey bk = new BolconKey();
		bk.setBoleto(b);
		bk.setMovimientoCtacte(ctacte.getId().getMovimientoCtacte());
		bk.setPeriodoCtacte(ctacte.getId().getPeriodoCtacte());
		bk.setVerificadorCtacte(ctacte.getId().getVerificadorCtacte());
		bc.setId(bk);
		bk.setItemCtacte(1L);

		bc.setCuota(cuota);
		Concepto concepto = Concepto.buscarConcepto(getConcepto());
		bc.setFacturado(concepto);
		bc.setImporte(ctacte.getImporte());
		bc.setOriginal(concepto);
		bc.setTipomov(tipoMov);

		bp.save(b);
		bp.save(bc);
		ctacte.setBoleto(b);

		bp.update(ctacte);

	}

	@SuppressWarnings("unchecked")
	private void cargarCtaCte() {
		montoActual = 0.0;
		List<Ctacte> listaCtaCte = bp.getNamedQuery("Ctacte.findByConceptoCuota")
				.setLong("idCuota", Long.parseLong(getCuotaId())).setString("concepto", getConcepto()).list();
		if (listaCtaCte != null && !listaCtaCte.isEmpty()) {
			for (Ctacte ctaCte : listaCtaCte) {
				if (ctaCte.isDebito()) {
					montoActual += ctaCte.getImporte();
				} else {
					montoActual -= ctaCte.getImporte();
				}
				this.ctacte = ctaCte;
			}
			Double montoCuota = 0.0;
			// el monto de la cuota ya existe como debitos
			montoActual += montoCuota;
		} else {
			BeanCtaCte ctacte = new BeanCtaCte();
			if (getTipo().equalsIgnoreCase("cr")) {
				// no tiene emision ni vencimiento
				ctacte = new BeanCtaCte(cuota.getNumero(), null, null, "", "Pago", cuota.getId());
			} else {
				// no tiene emision por no existir el debito
				ctacte = new BeanCtaCte(Integer.parseInt(getCuotaId()), null, cuota.getFechaVencimiento(), "", "Cuota",
						Long.parseLong(getCuotaId()));
			}
			ctacte.setCapital(cuota.getCapital());
			ctacte.setCompensatorio(cuota.getCompensatorio());
			ctacte.setMoratorio(cuota.getMoratorio());
			ctacte.setPunitorio(cuota.getPunitorio());
		}
		if (montoActual == 0) {

		}
	}

	private boolean buscarCtaCte() {
		CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(getIdObjetoi(), hastaFecha);
		calculoDeuda.calcular();
		beans = calculoDeuda.getBeans();
		Integer nroUltimaCuota = -1;
		Objetoi creditoPrueba = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());

		// Compruebo si el credito esta en estado "Original con Acuerdo", si es
		// asi debo setearle 0 a los int moratorios y punitorios
		Estado origConAcuerdo = (Estado) bp.createQuery("select e from Estado e where e.nombreEstado = :estado")
				.setParameter("estado", "ORIGINAL CON ACUERDO").uniqueResult();
		if (creditoPrueba.getEstadoActual().getEstado().getIdEstado() == origConAcuerdo.getIdEstado()) {
			for (BeanCtaCte bean : beans) {
				bean.setSaldoCuota(bean.getSaldoCuota() - bean.getMoratorio() - bean.getPunitorio());
				bean.setMoratorio(0.0);
				bean.setPunitorio(0.0);
			}
		}
		BeanCtaCte g;
		for (int i = 0; i < beans.size() - 1; i++) {
			g = beans.get(i);
			cuota = (Cuota) bp.getById(Cuota.class, g.getIdCuota());
			if (nroUltimaCuota != cuota.getNumero()) {
				if (credito.getEstadoActual().getEstado().getNombreEstado().equalsIgnoreCase("ACUERDO CAIDO")) {
					g.setEstado("Cancelada");
				} else {
					if (cuota.getEstado() != null && cuota.getEstado().equals("2")) {
						g.setEstado("Cancelada");
					} else {
						g.setEstado(cuota.getEmision() != null ? "Facturada" : "Pendiente");
					}
				}
				nroUltimaCuota = g.getNumero();
			}
		}

		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("CreditoCtaCte.beans", beans);
		return true;
	}

	@SuppressWarnings("static-access")
	public void generarReporte() {
		if (hastaFecha == null) {
			errores.put("creditoCuentaCorriente.vencimiento", "creditoCuentaCorriente.vencimiento");
			return;
		}
		setObjetoi((Objetoi) bp.getById(Objetoi.class, getIdObjetoi()));
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		String marcaagua = DirectorHelper.getString("marca.agua");

		try {
			reporte = new ReportResult();
			reporte.setExport("PDF");
			if (nombreReporte.equals("boletoDeDeposito.jasper")) {
				String montoStr = montoValor.replace('.', ' ').replace(',', '.').replaceAll("\\s", "");
				Double monto = new Double(montoStr);
				reporte.setReportName("boletoDeDeposito.jasper");
				reporte.setParams("MONTO_VALOR=" + montoValor + ";MONTO_VALOR_LETRAS=" + NumberHelper.getAsDinero(monto)
						+ ";NUMERO_VALOR=" + numeroValor + ";BANCO_EMISOR=" + bancoEmisor + ";SUCURSAL=" + sucursal
						+ ";LEYENDA=" + leyenda + ";VENCIMIENTO=" + DateHelper.getString(hastaFecha) + ";OBJETOI_ID="
						+ getIdObjetoi() + ";NUMERO_BOLETO=" + getIdObjetoi() + ";ID_PERSONA=" + getPersonaId()
						+ ";MONEDA=" + getObjetoi().getLinea().getMoneda().getDenominacion() + ";DEUDA_PARCIAL="
						+ deudaParcial + ";MONEDA_ID=" + getObjetoi().getLinea().getMoneda_id() + ";SIMBOLO="
						+ getObjetoi().getLinea().getMoneda().getSimbolo() + ";USUARIO="
						+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";RELACION_COTOMADOR_ID="
						+ relacionCotomadorId + ";LINEA=" + getObjetoi().getLinea().getNombre() + ";SCHEMA="
						+ SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema() + ";SCHEMA2="
						+ SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema()
						+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
			} else {
				if (cer) {
					reporte.setReportName("boletoTotalDeudaCER.jasper");
				} else {
					reporte.setReportName("boletoTotalDeuda.jasper");
				}
				reporte.setParams("LEYENDA=" + leyenda + ";VENCIMIENTO=" + DateHelper.getString(hastaFecha)
						+ ";OBJETOI_ID=" + getIdObjetoi() + ";NUMERO_BOLETO=" + getIdObjetoi() + ";ID_PERSONA="
						+ getPersonaId() + ";MONEDA=" + getObjetoi().getLinea().getMoneda().getDenominacion()
						+ ";DEUDA_PARCIAL=" + deudaParcial + ";MONEDA_ID=" + getObjetoi().getLinea().getMoneda_id()
						+ ";SIMBOLO=" + getObjetoi().getLinea().getMoneda().getSimbolo() + ";USUARIO="
						+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";RELACION_COTOMADOR_ID="
						+ relacionCotomadorId + ";LINEA=" + getObjetoi().getLinea().getNombre() + ";SCHEMA="
						+ SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema() + ";SCHEMA2="
						+ SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema()
						+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
						+ ";MARCAAGUA=" + marcaagua);
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletoTotalDeuda"));
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.idObjetoi",
				getIdObjetoi());
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.personaId",
				getPersonaId());
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	public void imprimirBoletos() {
		try {
			List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b WHERE b.id.boleto = :boleto")
					.setParameter("boleto", boleto).list();
			Double capital = 0.0;
			Double compensatorio = 0.0;
			Double punitorio = 0.0;
			Double moratorio = 0.0;
			for (Bolcon bolcon : bolcons) {
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
					capital = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
					compensatorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
					punitorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
					moratorio = bolcon.getImporte();
				}
			}
			boleto = (Boleto) bp.getById(Boleto.class, boleto.getId());
			Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletosFactura.jasper");
			reporte.setParams("idObjetoi=" + objetoi.getId() + ";CAPITAL=" + capital + ";idPersona="
					+ objetoi.getPersona_id() + ";MORATORIO=" + moratorio + ";PUNITORIO=" + punitorio
					+ ";COMPENSATORIO=" + compensatorio + ";VENCIMIENTO=" + boleto.getFechaVencimientoStr()
					+ ";NUMERO_BOLETO=" + boleto.getNumeroBoleto().toString() + ";EMISION="
					+ boleto.getFechaEmisionStr() + ";MONEDA=" + objetoi.getLinea().getMoneda().getDenominacion()
					+ ";SIMBOLO=" + objetoi.getLinea().getMoneda().getSimbolo() + ";ID_BOLETO=" + boleto.getId()
					+ ";SCHEMA=" + SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema()
					+ ";SCHEMA2=" + SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletosFactura"));
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.idObjetoi",
				getIdObjetoi());
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.personaId",
				getPersonaId());
	}

	public void imprimirManualDebito() {
		try {
			boleto = (Boleto) bp.getById(Boleto.class, boleto.getId());

			Object[] datosCtaCte = (Object[]) bp.createQuery(
					"select cc.comprobante, cc.fechaGeneracion, cc.fechaProceso from Ctacte cc where cc.boleto.id = :idBoleto")
					.setParameter("idBoleto", boleto.getId()).setMaxResults(1).uniqueResult();

			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("notaDebitoManual.jasper");
			reporte.setParams("OBJETOI_ID=" + getIdObjetoi() + ";OBSERVACIONES="
					+ (datosCtaCte != null && datosCtaCte[0] != null ? datosCtaCte[0] : "") // comprobante
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";numeroBoleto="
					+ boleto.getPeriodoBoleto().toString().concat(" - ").concat(boleto.getNumeroBoleto().toString())
					+ ";idBoleto=" + boleto.getId() + ";fechaEmisionBoleto="
					+ (datosCtaCte != null && datosCtaCte[2] != null ? DateHelper.getString((Date) datosCtaCte[2]) : "") // fechaProceso
					+ ";fechaAplicacion="
					+ (datosCtaCte != null && datosCtaCte[1] != null ? DateHelper.getString((Date) datosCtaCte[1]) : "") // fechaGeneracion
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "notaDebitoManual"));
		}
	}

	public void imprimirManualCredito() {
		try {
			boleto = (Boleto) bp.getById(Boleto.class, boleto.getId());

			Object[] datosCtaCte = (Object[]) bp.createQuery(
					"select cc.comprobante, cc.fechaGeneracion, cc.fechaProceso from Ctacte cc where cc.boleto.id = :idBoleto")
					.setParameter("idBoleto", boleto.getId()).setMaxResults(1).uniqueResult();

			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("notaCreditoManual.jasper");
			reporte.setParams("OBJETOI_ID=" + getIdObjetoi() + ";OBSERVACIONES="
					+ (datosCtaCte != null && datosCtaCte[0] != null ? datosCtaCte[0] : "") // comprobante
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";numeroBoleto="
					+ boleto.getPeriodoBoleto().toString().concat(" - ").concat(boleto.getNumeroBoleto().toString())
					+ ";idBoleto=" + boleto.getId() + ";fechaEmisionBoleto="
					+ (datosCtaCte != null && datosCtaCte[2] != null ? DateHelper.getString((Date) datosCtaCte[2]) : "") // fechaProceso
					+ ";fechaAplicacion="
					+ (datosCtaCte != null && datosCtaCte[1] != null ? DateHelper.getString((Date) datosCtaCte[1]) : "") // fechaGeneracion
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "notaCreditoManual"));
		}
	}

	@SuppressWarnings("static-access")
	public void imprimirPagos() {
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		Boleto b = (Boleto) bp.getById(Boleto.class, boleto.getId());
		String expediente = objetoi.getExpediente();
		String linea = objetoi.getLinea().getNombre();

		String medioPago = (String) bp.createQuery(
				"SELECT c.mediopago.denominacion FROM Bolepago bp JOIN bp.id.cobropago c WHERE bp.id.boleto = :boleto")
				.setEntity("boleto", boleto).uniqueResult();
		try {
			reporte = new ReportResult();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte.setExport("PDF");
			reporte.setReportName("pago.jasper");
			reporte.setParams("PROYECTO=" + objetoi.getNumeroAtencionStr() + " - " + objetoi.getObjeto() + ";TITULAR="
					+ objetoi.getPersona().getNomb12() + ";numeroBoleto=" + b.getNumeroBoleto() + ";BOLETO_ID="
					+ boleto.getId() + ";LINEA=" + linea + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";MONEDA="
					+ objetoi.getLinea().getMoneda().getDenominacion() + ";EXPEDIENTE=" + expediente + ";medioPago="
					+ medioPago + ";idObjetoi=" + getObjetoi().getId() + ";SCHEMA="
					+ SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema() + ";SCHEMA2="
					+ SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.idObjetoi",
					getIdObjetoi());
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.personaId",
					getPersonaId());
		} catch (Exception e) {
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "pago"));
		}
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return "CreditoCuentaCorriente";
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public List<BeanCtaCte> getBeans() {
		return beans;
	}

	public void setBeans(List<BeanCtaCte> beans) {
		this.beans = beans;
	}

	public Date getHastaFecha() {
		return hastaFecha;
	}

	public void setHastaFecha(Date hastaFecha) {
		this.hastaFecha = hastaFecha;
	}

	public String getHastaFechaStr() {
		return DateHelper.getString(hastaFecha);
	}

	public void setHastaFechaStr(String f) {
		java.util.Date dd = DateHelper.getDate(f);
		if (dd != null) {
			this.hastaFecha = new Date(dd.getTime());
		} else {
			this.hastaFecha = null;
		}
	}

	public String getCuotaId() {
		return cuotaId;
	}

	public boolean isEligefecha() {
		return eligefecha;
	}

	public void setEligefecha(boolean eligefecha) {
		this.eligefecha = eligefecha;
	}

	public void setCuotaId(String cuotaId) {
		this.cuotaId = cuotaId;
	}

	public String getConcepto() {
		return concepto;
	}

	@Override
	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setInteresCompensatorio(double interesCompensatorio) {
		this.interesCompensatorio = interesCompensatorio;
	}

	public void setInteresMoratoriosPunitorios(double interesMoratoriosPunitorios) {
		this.interesMoratoriosPunitorios = interesMoratoriosPunitorios;
	}

	public void setBonificaciones(double bonificaciones) {
		this.bonificaciones = bonificaciones;
	}

	public void setGastos(double gastos) {
		this.gastos = gastos;
	}

	public void setCapitalDevengado(double capitalDevengado) {
		this.capitalDevengado = capitalDevengado;
	}

	public void setTotalDevengado(double totalDevengado) {
		this.totalDevengado = totalDevengado;
	}

	public void setTotalPagado(double totalPagado) {
		this.totalPagado = totalPagado;
	}

	public void setTotalAdeudado(double totalAdeudado) {
		this.totalAdeudado = totalAdeudado;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getTipo() {
		if (tipo == null) {
			tipo = new String();
		}
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Double getMontoActual() {
		if (montoActual == null) {
			return 0.0;
		}
		return new Double(ft.format(montoActual).replace(",", "."));
	}

	public void setMontoActual(Double montoActual) {
		this.montoActual = montoActual;
	}

	public Double getMontoFinal() {
		if (getTipo().equalsIgnoreCase("cr")) {
			montoFinal = getMontoActual() - getMontoMovimiento();
		} else {
			montoFinal = getMontoActual() + getMontoMovimiento();
		}
		if (montoFinal == null) {
			return 0.0;
		}
		return montoFinal;
	}

	public void setMontoFinal(Double montoFinal) {
		this.montoFinal = montoFinal;
	}

	public Double getMontoMovimiento() {
		if (montoMovimiento == null) {
			return 0.0;
		}
		return montoMovimiento;
	}

	public void setMontoMovimiento(Double montoMovimiento) {
		this.montoMovimiento = montoMovimiento;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public Ctacte getCtacte() {
		return ctacte;
	}

	public void setCtacte(Ctacte ctacte) {
		this.ctacte = ctacte;
	}

	public Cuota getCuota() {
		return cuota;
	}

	public void setCuota(Cuota cuota) {
		this.cuota = cuota;
	}

	public String getFechaStr() {
		return fechaStr;
	}

	public void setFechaStr(String fechaStr) {
		this.fechaStr = fechaStr;
	}

	public String getInteresCompensatorio() {
		return String.valueOf(ft.format(interesCompensatorio));
	}

	public void setInteresCompensatorio(String interesCompensatorio) {
		this.interesCompensatorio = Double.parseDouble(interesCompensatorio);
	}

	public String getInteresMoratoriosPunitorios() {
		return String.valueOf(ft.format(interesMoratoriosPunitorios));
	}

	public void setInteresMoratoriosPunitorios(String interesMoratoriosPunitorios) {
		this.interesMoratoriosPunitorios = Double.parseDouble(interesMoratoriosPunitorios);
	}

	public String getBonificaciones() {
		return String.valueOf(ft.format(bonificaciones));
	}

	public void setBonificaciones(String bonificaciones) {
		this.bonificaciones = Double.parseDouble(bonificaciones);
	}

	public String getGastosRec() {
		return String.valueOf(ft.format(gastos));
	}

	public String getGastos() {
		return String.valueOf(ft.format(gastos));
	}

	public void setGastos(String gastos) {
		this.gastos = Double.parseDouble(gastos);
	}

	public void setGastosRec(String gastosRec) {
		this.gastosRec = Double.parseDouble(gastosRec);
	}

	public String getCapitalDevengado() {
		return String.valueOf(ft.format(capitalDevengado));
	}

	public void setCapitalDevengado(String capitalDevengado) {
		this.capitalDevengado = Double.parseDouble(capitalDevengado);
	}

	public String getTotalDevengado() {
		return String.valueOf(ft.format(totalDevengado));
	}

	public void setTotalDevengado(String totalDevengado) {
		this.totalDevengado = Double.parseDouble(totalDevengado);
	}

	public String getTotalPagado() {
		return String.valueOf(ft.format(totalPagado));
	}

	public void setTotalPagado(String totalPagado) {
		this.totalPagado = Double.parseDouble(totalPagado);
	}

	public String getTotalAdeudado() {
		return String.valueOf(ft.format(totalAdeudado));
	}

	public void setTotalAdeudado(String totalAdeudado) {
		this.totalAdeudado = Double.parseDouble(totalAdeudado);
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public void setLeyenda(String leyenda) {
		this.leyenda = leyenda;
	}

	public boolean isEmitir() {
		return emitir;
	}

	public void setEmitir(boolean emitir) {
		this.emitir = emitir;
	}

	public Boleto getBoleto() {
		return boleto;
	}

	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	public boolean isMontoCero() {
		return montoCero;
	}

	public void setMontoCero(boolean montoCero) {
		this.montoCero = montoCero;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Date getFechaRep() {
		return fechaRep;
	}

	public void setFechaRep(Date fechaRep) {
		this.fechaRep = fechaRep;
	}

	public String getConceptoRep() {
		return conceptoRep;
	}

	public void setConceptoRep(String conceptoRep) {
		this.conceptoRep = conceptoRep;
	}

	public String getFechaRepStr() {
		return DateHelper.getString(fechaRep);
	}

	public void setFechaRepStr(String fechaRep) {
		this.fechaRep = DateHelper.getDate(fechaRep);
	}

	public String getTotalPagadoStr() {
		return String.format("%.2f", totalPagado).replace(".", ",");
	}

	public String getTotalDevengadoStr() {
		return String.format("%.2f", totalDevengado).replace(".", ",");
	}

	public String getTotalAdeudadoStr() {
		return String.format("%.2f", totalAdeudado).replace(".", ",");
	}

	public String getCapitalDevengadoStr() {
		return String.format("%.2f", capitalDevengado).replace(".", ",");
	}

	public String getGastosStr() {
		return String.format("%.2f", gastos).replace(".", ",");
	}

	public String getGastosRecStr() {
		return String.format("%.2f", gastos).replace(".", ",");
	}

	public String getBonificacionesStr() {
		return String.format("%.2f", bonificaciones).replace(".", ",");
	}

	public String getInteresMoratoriosPunitoriosStr() {
		return String.format("%.2f", interesMoratoriosPunitorios).replace(".", ",");
	}

	public String getInteresCompensatorioStr() {
		return String.format("%.2f", interesCompensatorio).replace(".", ",");
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	private Objetoi buscarObjetoi(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return (Objetoi) bp.getById(Objetoi.class, id);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Collection<BeanCtaCte> getCollection(HashMap arg0) {
		setIdObjetoi(new Long(arg0.get("idObjetoi").toString()));
		credito = buscarObjetoi(getIdObjetoi());
		hastaFecha = (DateHelper.getDate((arg0.get("fechaHasta").toString())));
		buscarCtaCte();
		return beans;
	}

	public void setCcId(Double ccId) {
		this.ccId = ccId;
	}

	public Double getCcId() {
		return ccId;
	}

	public void setCalcular(boolean calcular) {
		this.calcular = calcular;
	}

	public boolean isCalcular() {
		return calcular;
	}

	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}

	public SimpleDateFormat getSdf() {
		return sdf;
	}

	public void setTipoMovimiento(String tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setMotivo(String detalle) {
		this.motivo = detalle;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getDetalle() {
		return detalle;
	}

	public boolean isDeudaParcial() {
		return deudaParcial;
	}

	public void setDeudaParcial(boolean deudaParcial) {
		this.deudaParcial = deudaParcial;
	}

	public void setFechaMovimientoStr(String fechaMovimiento) {
		this.fechaMovimiento = DateHelper.getDate(fechaMovimiento);
	}

	public String getFechaMovimientoStr() {
		return DateHelper.getString(fechaMovimiento);
	}

	public String getNombreReporte() {
		return nombreReporte;
	}

	public void setNombreReporte(String nombreReporte) {
		this.nombreReporte = nombreReporte;
	}

	public String getMontoValor() {
		return montoValor;
	}

	public void setMontoValor(String montoValor) {
		this.montoValor = montoValor;
	}

	public String getNumeroValor() {
		return numeroValor;
	}

	public void setNumeroValor(String numeroValor) {
		this.numeroValor = numeroValor;
	}

	public String getBancoEmisor() {
		return bancoEmisor;
	}

	public void setBancoEmisor(String bancoEmisor) {
		this.bancoEmisor = bancoEmisor;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public boolean isCer() {
		return cer;
	}

	@Override
	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	@Override
	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}

}
