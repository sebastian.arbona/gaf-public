package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.estados.EstaObs;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.SolicitudProrroga;
import com.nirven.creditos.hibernate.SolicitudProrrogaEstado;

public class ProrrogaEstados implements IProcess {
	// ==========================ATRIBUTOS============================
	public static String separador = "@";
	public static Long agregaEstado = 1L;
	public static Long agregaObs = 2L;
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "ProrrogaEstadosList";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Estado estado;
	private Date fecha = new Date(System.currentTimeMillis());
	private List<SolicitudProrrogaEstado> prorrogaEstados;
	private List<SolicitudProrroga> solicitudes;
	private boolean ocultarCampos = false;
	private List<EstaObs> estadosObservaciones;
	private SolicitudProrroga solicitudProrroga;
	private Long idSolicitudProrroga;
	private Long idObjetoi;

	// =======================CONSTRUCTORES=========================
	public ProrrogaEstados() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	}

	@Override
	public boolean doProcess() {

		// solicitudProrroga= (SolicitudProrroga)
		// this.bp.getById(SolicitudProrroga.class, this.idSolicitudProrroga);

		if (this.getAccion().equalsIgnoreCase("list")) {
			this.cargarEstadosProrroga();
		}

		return this.errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void cargarEstadosProrroga() {

		if (idSolicitudProrroga != null) {
			try {
				solicitudProrroga = (SolicitudProrroga) this.bp.getById(SolicitudProrroga.class,
						this.idSolicitudProrroga);
			} catch (Exception e) {
				this.errores.put("SolicitudProrroga.Error.cargar", "SolicitudProrroga.Error.carga");
				e.printStackTrace();
			}
		}

		estadosObservaciones = new ArrayList<EstaObs>();
		if (getSolicitudProrroga() != null) {
			try {
				String consulta;

				setSolicitudProrroga(
						(SolicitudProrroga) bp.getById(SolicitudProrroga.class, getSolicitudProrroga().getId()));
				consulta = "SELECT estado FROM SolicitudProrrogaEstado estado";
				consulta += " WHERE estado.solicitudProrroga.id = " + solicitudProrroga.getId().longValue();

				prorrogaEstados = bp.getByFilter(consulta);

			} catch (Exception e) {
				this.errores.put("EstadosProrroga.Error.cargar", "EstadosProrroga.Error.carga");
				e.printStackTrace();
			}

			EstaObs estaObs;

			for (SolicitudProrrogaEstado solicitudProrrogaEstado : prorrogaEstados) {
				if (solicitudProrrogaEstado != null) {
					this.idObjetoi = solicitudProrrogaEstado.getSolicitudProrroga().getObjetoi().getId();
					estaObs = new EstaObs();
					estaObs.setEstaobs(solicitudProrrogaEstado.getEstado().getNombreEstado());
					estaObs.setColor(solicitudProrrogaEstado.getEstado().getColor());
					estaObs.setFecha(solicitudProrrogaEstado.getFechaDesde());
					estaObs.setFechaHasta(solicitudProrrogaEstado.getFechaHasta());
					estaObs.setObservaciones(solicitudProrrogaEstado.getObservacion());
					estaObs.setAreaResponsable("");
					estadosObservaciones.add(estaObs);
				}
			}
		}
	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

//	public Long getIdObjetoi() {
//		return idObjetoi;
//	}
//
//	public void setIdObjetoi(Long idObjetoi) {
//		this.idObjetoi = idObjetoi;
//	}
//
//	public Objetoi getObjetoi() {
//		return objetoi;
//	}
//
//	public void setObjetoi(Objetoi objetoi) {
//		this.objetoi = objetoi;
//	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<SolicitudProrrogaEstado> getProrrogaEstados() {
		return prorrogaEstados;
	}

	public void setProrrogaEstados(List<SolicitudProrrogaEstado> prorrogaEstados) {
		this.prorrogaEstados = prorrogaEstados;
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	public List<EstaObs> getEstadosObservaciones() {
		return estadosObservaciones;
	}

	public void setEstadosObservaciones(List<EstaObs> estadosObservaciones) {
		this.estadosObservaciones = estadosObservaciones;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public List<SolicitudProrroga> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(List<SolicitudProrroga> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public SolicitudProrroga getSolicitudProrroga() {
		return solicitudProrroga;
	}

	public void setSolicitudProrroga(SolicitudProrroga solicitudProrroga) {
		this.solicitudProrroga = solicitudProrroga;
	}

	public Long getIdSolicitudProrroga() {
		return idSolicitudProrroga;
	}

	public void setIdSolicitudProrroga(Long idSolicitudProrroga) {
		this.idSolicitudProrroga = idSolicitudProrroga;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

}
