declare @idTipoGarantia integer
declare @estado varchar(50)
declare @idLinea integer
declare @estadoSeguro varchar(50)
declare @hoy datetime2
declare @fechaDesde datetime2
declare @fechaHasta datetime2
set @idTipoGarantia = :idTipoGarantia
set @estado = :estado
set @idLinea = :idLinea
set @estadoSeguro = :estadoSeguro
set @hoy = CONVERT(datetime2, ':hoy', 121)
set @fechaDesde = :fechaDesde
set @fechaHasta = :fechaHasta
select gc.ordenFisico, 
g.id as numeroInventarioOriginal,
o.numeroAtencion, 
l.nombre as linea, 
p.NOMB_12 as inversor,
p.CUIL_12, 
o.financiamiento, 
tg.nombre as tipoGarantia,
tg.nombreNumeroIdentificacion + ' ' + g.identificadorUnico as identificador,
g.observacion descripcion, 
og.valor, 
eo.nombreEstado as etapaCredito, 
gt.gravFechaVencGarantia as vencimiento,
gs.fechaVencimiento as vencimientoSeguro, 
u.nombre as sectorCustodia,
resp.NOMB_12 as responsableCustodia,
o.expediente,
eg.nombreEstado as estadoGarantia,
ge.fechaEstado,
gt.gravFechaInscripcion,
gt.gravFechaReinscripcion,
og.aforo,
ge.fechaProceso
from ObjetoiGarantia og
join Garantia g on og.garantia_id = g.id
join Objetoi o on og.objetoi_id = o.id
join GarantiaEstado ge on og.id = ge.objetoiGarantia_id and :consultaProcesoValor <= @fechaHasta)
left join ObjetoiEstado oe ON oe.objetoi_id = o.id and (oe.fechaHasta <= @fechaHasta or oe.fechaHasta is null) and oe.id = (select max(id) from ObjetoiEstado where objetoi_id = o.id)
left join Estado eg on eg.idEstado = ge.estado_idEstado 
left join Estado eo on eo.idEstado = oe.estado_idEstado 
left join PERSONA p on p.IDPERSONA = o.persona_IDPERSONA 
left join Linea l on l.id = o.linea_id
left join TipoGarantia tg on tg.id = g.tipo_id 
left join GarantiaTasacion gt on gt.id = og.tasacion_id 
left join GarantiaSeguro gs on gs.id = g.seguro_id
left join GarantiaCustodia gc on gc.objetoiGarantia_id = og.id and gc.fechaIngreso = (select max(fechaIngreso) from GarantiaCustodia where objetoiGarantia_id = og.id and fechaIngreso <= @fechaDesde)
left join unidad u on u.id = gc.sector_id
left join PERSONA resp on resp.IDPERSONA = gc.responsable_id
where (@idTipoGarantia is null or tg.id in (:tiposGarantias))
and (@estado is null or ge.estado_idEstado in (:estadosGarantias))
and (@idLinea is null or l.id in (:idsLineas))
and (@estadoSeguro is null or @estadoSeguro = 'activo' and gs.fechaVencimiento > @hoy or @estadoSeguro = 'vencido' and gs.fechaVencimiento <= @hoy)
and ge.importe >= 0 and og.baja IS NULL