package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.QQIngresados;

public class RegistrarQQOficialLinea implements IProcess {

	private BusinessPersistance bp;
	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private List<ObjetoiVinedo> vinedos;
	private ObjetoiVinedo vinedo;

	private List<Objetoi> creditos;
	private Objetoi credito;
	private Persona persona;
	private String expediente;

	private Integer rowNum;
	private boolean ingresar;
	private QQIngresados qqIngresados;
	private String qqIngresoStr;
	private boolean ingresarQQ;
	private String civ;
	private String qqIngresoId;

	@SuppressWarnings("unchecked")
	public RegistrarQQOficialLinea() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		forward = "RegistrarQQOficialLinea";
		credito = new Objetoi();
		persona = new Persona();
		creditos = (List<Objetoi>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("creditosRegistrarQQIngresados");
	}

	@Override
	public boolean doProcess() {
		if (accion != null && accion.equals("ingresar")) {
			ingresar();
		}
		if (accion != null && accion.equals("registrarIngreso")) {
			registrarIngreso();
		}
		if (accion != null && accion.equals("listar")) {
			listarCreditos();
		}

		else if (accion != null && accion.equals("listarPorPersona")) {
			if (persona.getId() != null && persona.getId().longValue() != 0L)
				listarPorPersona();
		} else if (accion != null && accion.equals("listarPorExpediente")) {
			if (credito.getExpediente() != null && !credito.getExpediente().equals("0"))
				listarPorExpediente();

		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listarCreditos() {
		try {
			String tipoGarantiaFiduciaria = DirectorHelper.getString("tipoGarantia.inmovilizacion");
			String tipoGarantiaHipotecaria = DirectorHelper.getString("tipoGarantia.hipotecaria");

			List<BigDecimal> creditos_id = (List<BigDecimal>) bp.createSQLQuery("SELECT O.ID FROM Objetoi O "
					+ "INNER JOIN Desembolso D ON D.credito_id = O.id AND D.NUMERO = 1 AND D.estado IN('1', '2') "
					+ "INNER JOIN ObjetoiVinedo OV ON OV.credito_id = O.id "
					+ "INNER JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id "
					+ "INNER JOIN Garantia G ON OG.garantia_id = G.id "
					+ "INNER JOIN TipoGarantia T on T.id = G.TIPO_ID AND G.tipo_id IN (" + tipoGarantiaFiduciaria + ","
					+ tipoGarantiaHipotecaria + ") LEFT JOIN QQIngresados Q ON Q.vinedo_id = OV.id "
					+ "WHERE (T.calculaQQSinVinedo is not null AND T.calculaQQSinVinedo = 1) "
					+ "GROUP BY O.qqFinal, O.id "
					+ "HAVING SUM(CASE WHEN Q.qqIngreso IS NULL THEN 0 ELSE Q.qqIngreso END) < O.qqFinal * 0.7").list();

			if (!creditos_id.isEmpty()) {
				creditos = bp
						.createSQLQuery(
								"SELECT DISTINCT * FROM Objetoi o WHERE o.id IN(:creditos_id) ORDER BY o.expediente")
						.addEntity(Objetoi.class).setParameterList("creditos_id", creditos_id).list();
			} else {
				creditos = null;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosRegistrarQQIngresados", creditos);
	}

	@SuppressWarnings("unchecked")
	public void listarPorPersona() {
		persona = (Persona) bp.getById(Persona.class, persona.getId());
		try {
			String tipoGarantiaFiduciaria = DirectorHelper.getString("tipoGarantia.inmovilizacion");
			String tipoGarantiaHipotecaria = DirectorHelper.getString("tipoGarantia.hipotecaria");

			List<BigDecimal> creditos_id = (List<BigDecimal>) bp.createSQLQuery("SELECT O.ID FROM Objetoi O "
					+ "INNER JOIN Desembolso D ON D.credito_id = O.id AND D.NUMERO = 1 AND D.estado IN('1', '2') "
					+ "INNER JOIN ObjetoiVinedo OV ON OV.credito_id = O.id "
					+ "INNER JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id "
					+ "INNER JOIN Garantia G ON OG.garantia_id = G.id AND G.tipo_id IN (" + tipoGarantiaFiduciaria + ","
					+ tipoGarantiaHipotecaria + ") INNER JOIN TipoGarantia T on T.id = G.TIPO_ID "
					+ "LEFT JOIN QQIngresados Q ON Q.vinedo_id = OV.id WHERE O.persona_IDPERSONA = :persona_id "
					+ "AND (T.calculaQQSinVinedo is not null OR T.calculaQQSinVinedo = 1) "
					+ "GROUP BY O.qqFinal, O.id "
					+ "HAVING SUM(CASE WHEN Q.qqIngreso IS NULL THEN 0 ELSE Q.qqIngreso END) < O.qqFinal * 0.7")
					.setLong("persona_id", persona.getId()).list();
			if (!creditos_id.isEmpty()) {
				creditos = bp
						.createSQLQuery(
								"SELECT DISTINCT * FROM Objetoi o WHERE o.id IN(:creditos_id) ORDER BY o.expediente")
						.addEntity(Objetoi.class).setParameterList("creditos_id", creditos_id).list();
			} else {
				creditos = null;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosRegistrarQQIngresados", creditos);
	}

	@SuppressWarnings("unchecked")
	public void listarPorExpediente() {
		persona = new Persona();
		try {
			String tipoGarantiaFiduciaria = DirectorHelper.getString("tipoGarantia.inmovilizacion");
			String tipoGarantiaHipotecaria = DirectorHelper.getString("tipoGarantia.hipotecaria");

			List<BigDecimal> creditos_id = (List<BigDecimal>) bp.createSQLQuery("SELECT O.ID FROM Objetoi O "
					+ "INNER JOIN Desembolso D ON D.credito_id = O.id AND D.NUMERO = 1 AND D.estado IN('1', '2') "
					+ "INNER JOIN ObjetoiVinedo OV ON OV.credito_id = O.id "
					+ "INNER JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id "
					+ "INNER JOIN Garantia G ON OG.garantia_id = G.id AND G.tipo_id IN (" + tipoGarantiaFiduciaria + ","
					+ tipoGarantiaHipotecaria + ") INNER JOIN TipoGarantia T on T.id = G.TIPO_ID "
					+ "LEFT JOIN QQIngresados Q ON Q.vinedo_id = OV.id WHERE O.expediente = :expediente "
					+ "AND (T.calculaQQSinVinedo is not null OR T.calculaQQSinVinedo = 1) "
					+ "GROUP BY O.qqFinal, O.id "
					+ "HAVING SUM(CASE WHEN Q.qqIngreso IS NULL THEN 0 ELSE Q.qqIngreso END) < O.qqFinal * 0.7")
					.setString("expediente", credito.getExpediente()).list();
			if (!creditos_id.isEmpty()) {
				creditos = bp
						.createSQLQuery("SELECT DISTINCT * " + "FROM Objetoi o " + "WHERE o.id IN(:creditos_id) "
								+ "ORDER BY o.expediente")
						.addEntity(Objetoi.class).setParameterList("creditos_id", creditos_id).list();
			} else {
				creditos = null;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosRegistrarQQIngresados", creditos);
	}

	@SuppressWarnings("unchecked")
	public void ingresar() {
		credito = (Objetoi) bp.getById(Objetoi.class, credito.getId());
		vinedo = (ObjetoiVinedo) bp
				.createSQLQuery("SELECT * " + "FROM ObjetoiVinedo ov " + "WHERE ov.credito_id = :credito_id "
						+ "ORDER BY ov.id")
				.addEntity(ObjetoiVinedo.class).setParameter("credito_id", credito.getId()).setMaxResults(1)
				.uniqueResult();
		Double total = new Double(0D);
		total = (Double) bp.getNamedQuery("QQIngresados.SumQQIngresoByVinedo").setEntity("vinedo", vinedo)
				.uniqueResult();
		if (total == null) {
			vinedo.setTotalQQIngresados(total);
		}
		// garantia
		List<Garantia> garantias = bp
				.createQuery("SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja IS NULL")
				.setEntity("credito", vinedo.getCredito()).list();
		if (garantias.isEmpty()) {
			vinedo.setGarantia(new Garantia());
		} else {
			vinedo.setGarantia(garantias.get(0));
		}
		ingresar = true;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("RegistrarQQIngresados.vinedo",
				vinedo);
	}

	public void registrarIngreso() {
//		credito = (Objetoi) bp.getById(Objetoi.class, credito.getId());
		persona.setId(0L);
		vinedo = (ObjetoiVinedo) bp
				.createSQLQuery("SELECT * " + "FROM ObjetoiVinedo ov " + "WHERE ov.credito_id = :credito_id "
						+ "ORDER BY ov.id")
				.addEntity(ObjetoiVinedo.class).setParameter("credito_id", credito.getId()).setMaxResults(1)
				.uniqueResult();
		Double ingreso;
		try {
			Double ingresadoALaFecha = new Double(
					qqIngresoStr.replace(',', '.').replace('Q', ' ').replace('q', ' ').trim());
			ingreso = ingresadoALaFecha - vinedo.getTotalQQIngresados();
			if (ingreso <= 0) {
				errores.put("registrarQQIngresados.ingresadoALaFecha", "registrarQQIngresados.ingresadoALaFecha");
				qqIngresoStr = null;
				return;
			}
		} catch (NumberFormatException e) {
			errores.put("registrarQQIngresados.qqIngreso", "registrarQQIngresados.qqIngreso");
			qqIngresoStr = null;
			return;
		}
		qqIngresados = new QQIngresados();
		qqIngresados.setQqIngreso(ingreso);
		qqIngresados.setVinedo(vinedo);
		qqIngresados.setFechaIngreso(new Date());
		bp.begin();
		bp.save(qqIngresados);
		vinedo.setTotalQQIngresados(vinedo.getTotalQQIngresados() + ingreso);
		bp.update(vinedo);
		calcularPorcentajeQQ(vinedo.getCredito());
		bp.commit();
		ingresar = false;
		ingresarQQ = true;
		qqIngresoStr = null;
	}

	@SuppressWarnings("unchecked")
	private void calcularPorcentajeQQ(Objetoi credito) {
		List<ObjetoiVinedo> vin = bp.createQuery("Select v from ObjetoiVinedo v where v.credito=:credito")
				.setParameter("credito", credito).list();
		Double total = 0.0;
		for (ObjetoiVinedo ov : vin) {
			if (ov.getTotalQQIngresados() != null) {
				total += ov.getTotalQQIngresados();
			}
		}
		Desembolso desembolso = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", credito.getId()).setInteger("numero", 1).setMaxResults(1).uniqueResult();
		Desembolso desembolso2 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", credito.getId()).setInteger("numero", 2).setMaxResults(1).uniqueResult();
		if (total >= (credito.getQqFinal() * 0.7) && desembolso != null && desembolso.getEstado().trim().equals("2")
				&& desembolso2 != null && !desembolso2.getEstado().trim().equals("3")
				&& !credito.getEstadoActual().getEstado().getNombreEstado().equalsIgnoreCase("EJECUCION")) {
			bp.begin();
			credito.setEstadoActual("PENDIENTE SEGUNDO DESEMBOLSO", null);
			bp.commit();
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(List<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public ObjetoiVinedo getVinedo() {
		return vinedo;
	}

	public void setVinedo(ObjetoiVinedo vinedo) {
		this.vinedo = vinedo;
	}

	public boolean isIngresar() {
		return ingresar;
	}

	public void setIngresar(boolean ingresar) {
		this.ingresar = ingresar;
	}

	public QQIngresados getQqIngresados() {
		return qqIngresados;
	}

	public void setQqIngresados(QQIngresados qqIngresados) {
		this.qqIngresados = qqIngresados;
	}

	public String getQqIngresoStr() {
		return qqIngresoStr;
	}

	public void setQqIngresoStr(String qqIngresoStr) {
		this.qqIngresoStr = qqIngresoStr;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public boolean isIngresarQQ() {
		return ingresarQQ;
	}

	public void setIngresarQQ(boolean ingresarQQ) {
		this.ingresarQQ = ingresarQQ;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public String getCiv() {
		return civ;
	}

	public void setCiv(String civ) {
		this.civ = civ;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getQqIngresoId() {
		return qqIngresoId;
	}

	public void setQqIngresoId(String qqIngresoId) {
		this.qqIngresoId = qqIngresoId;
	}
}
