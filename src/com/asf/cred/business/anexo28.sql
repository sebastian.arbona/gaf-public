select l.codigoCtaContablePatrimonioCapital, l.nombre, 
s.saldo, d.desembolsado, 
s.saldoCorriente, CASE s.saldoCorriente WHEN 0 THEN 0 ELSE (p.pagosCorriente / s.saldoCorriente) * 100 END porcCorriente, 
p.pagosCorriente, a.ajustesCorriente, s.saldoCorriente - p.pagosCorriente + a.ajustesCorriente saldoFinalCorriente, 
s.saldoMorosos, CASE s.saldoMorosos WHEN 0 THEN 0 ELSE (p.pagosMorosos / s.saldoMorosos) * 100 END porcMorosos, 
p.pagosMorosos, a.ajustesMorosos, s.saldoMorosos - p.pagosMorosos + a.ajustesMorosos saldoFinalMorosos,
s.saldoJudicial, CASE s.saldoJudicial WHEN 0 THEN 0 ELSE (p.pagosJudicial / s.saldoJudicial) * 100 END porcJudicial, 
p.pagosJudicial, a.ajustesJudicial, s.saldoJudicial - p.pagosJudicial + a.ajustesJudicial saldoFinalJudicial, 
s.saldo + d.desembolsado - p.pagosCorriente + a.ajustesCorriente 
- p.pagosMorosos + a.ajustesMorosos 
- p.pagosJudicial + a.ajustesJudicial saldoFinal 
from Linea l
left join 
(select o.linea_id, SUM(CASE WHEN c.tipomov_id = 2 THEN c.importe ELSE -c.importe END) saldo,
SUM(CASE WHEN e.nombreEstado = 'GESTION JUDICIAL' THEN 
CASE WHEN c.tipomov_id = 2 THEN c.importe ELSE -c.importe END
ELSE 0 END) saldoJudicial,
SUM(CASE WHEN e.nombreEstado in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO')
AND oc.comportamientoPago <> '1' THEN 
CASE WHEN c.tipomov_id = 2 THEN c.importe ELSE -c.importe END
ELSE 0 END) saldoMorosos, 
SUM(CASE WHEN e.nombreEstado <> 'GESTION JUDICIAL' and 
(e.nombreEstado not in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO')
OR oc.comportamientoPago = '1') THEN 
CASE WHEN c.tipomov_id = 2 THEN c.importe ELSE -c.importe END
ELSE 0 END) saldoCorriente
from Objetoi o
left join Ctacte c on c.objetoi_id = o.id
left join Concepto con on c.asociado_id = con.id
join ObjetoiEstado oe on o.id = oe.objetoi_id and oe.fechaHasta is null
join Estado e on oe.estado_idEstado = e.idEstado
left join ObjetoiComportamiento oc on oc.objetoi_id = o.id and oc.fechaHasta is null
where con.concepto_concepto = 'cap' and c.fechaProceso < :fechaDesde
group by o.linea_id) s on s.linea_id = l.id 
left join
(select o.linea_id,
SUM(CASE WHEN e.nombreEstado = 'GESTION JUDICIAL' THEN 
CASE WHEN c.tipoMovimiento like 'pago' THEN c.importe ELSE 0 END
ELSE 0 END) pagosJudicial,
SUM(CASE WHEN e.nombreEstado in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO')
AND oc.comportamientoPago <> '1' THEN 
CASE WHEN c.tipoMovimiento like 'pago' THEN c.importe ELSE 0 END
ELSE 0 END) pagosMorosos, 
SUM(CASE WHEN e.nombreEstado <> 'GESTION JUDICIAL' and 
(e.nombreEstado not in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO')
OR oc.comportamientoPago = '1') THEN 
CASE WHEN c.tipoMovimiento like 'pago' THEN c.importe ELSE 0 END
ELSE 0 END) pagosCorriente
from Objetoi o
left join Ctacte c on c.objetoi_id = o.id
left join Concepto con on c.asociado_id = con.id
join ObjetoiEstado oe on o.id = oe.objetoi_id and oe.fechaHasta is null
join Estado e on oe.estado_idEstado = e.idEstado
left join ObjetoiComportamiento oc on oc.objetoi_id = o.id and oc.fechaHasta is null
where con.concepto_concepto = 'cap' 
and c.fechaProceso >= :fechaDesde and c.fechaProceso <= :fechaHasta
group by o.linea_id) p on p.linea_id = l.id
left join
(select o.linea_id,
SUM(CASE WHEN e.nombreEstado = 'GESTION JUDICIAL' THEN 
CASE WHEN c.tipoMovimiento in ('movManualDeb','movManualCred') AND c.tipomov_id = 2 THEN c.importe 
WHEN c.tipoMovimiento in ('movManualDeb','movManualCred') AND c.tipomov_id = 1 THEN -c.importe
ELSE 0 END
ELSE 0 END) ajustesJudicial,
SUM(CASE WHEN e.nombreEstado in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO')
AND oc.comportamientoPago <> '1' THEN 
CASE WHEN c.tipoMovimiento in ('movManualDeb','movManualCred') AND c.tipomov_id = 2 THEN c.importe 
WHEN c.tipoMovimiento in ('movManualDeb','movManualCred') AND c.tipomov_id = 1 THEN -c.importe
ELSE 0 END
ELSE 0 END) ajustesMorosos, 
SUM(CASE WHEN e.nombreEstado <> 'GESTION JUDICIAL' and 
(e.nombreEstado not in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO')
OR oc.comportamientoPago = '1') THEN 
CASE WHEN c.tipoMovimiento in ('movManualDeb','movManualCred') AND c.tipomov_id = 2 THEN c.importe 
WHEN c.tipoMovimiento in ('movManualDeb','movManualCred') AND c.tipomov_id = 1 THEN -c.importe
ELSE 0 END
ELSE 0 END) ajustesCorriente
from Objetoi o
left join Ctacte c on c.objetoi_id = o.id
left join Concepto con on c.asociado_id = con.id
join ObjetoiEstado oe on o.id = oe.objetoi_id and oe.fechaHasta is null
join Estado e on oe.estado_idEstado = e.idEstado
left join ObjetoiComportamiento oc on oc.objetoi_id = o.id and oc.fechaHasta is null
where con.concepto_concepto = 'cap' 
and c.fechaProceso >= :fechaDesde and c.fechaProceso <= :fechaHasta
group by o.linea_id) a on a.linea_id = l.id
left join
(select o.linea_id, SUM(d.importeReal) desembolsado
from Objetoi o 
left join Desembolso d on d.credito_id = o.id and d.importeReal is not null
where d.fechaReal >= :fechaDesde and d.fechaReal <= :fechaHasta
group by o.linea_id) d on d.linea_id = l.id
where l.codigoCtaContablePatrimonioCapital is not null and l.codigoCtaContablePatrimonioCapital <> ''