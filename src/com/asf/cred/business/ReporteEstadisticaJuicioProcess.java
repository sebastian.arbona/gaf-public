package com.asf.cred.business;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nirven.creditos.hibernate.TipoProceso;

public class ReporteEstadisticaJuicioProcess implements IProcess {
	private String forward = "ReporteEstadisticaJuicioProcess";
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;
	private String action;
	private String estadistica;
	private String codAbogado;
	private Date fechaDesde;
	private Date fechaHasta;

	public ReporteEstadisticaJuicioProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@SuppressWarnings("unchecked")
	private void getEstadisticaJuicio() {
		this.estadistica = "";
		java.sql.Date fechaActual = new java.sql.Date(new Date().getTime());

		JsonArray list = new JsonArray();
		try {

			List<TipoProceso> estados = bp.getAll(TipoProceso.class);

			String query = "SELECT r.fecha, r.numero, p.NOMB_12,tp.nombreTipoProceso FROM ProcesoResolucion pr"
					+ " INNER JOIN Resolucion r ON pr.resolucion_id = r.id"
					+ " INNER JOIN EspecialidadPersona ep ON ep.id = r.especialista_id"
					+ " INNER JOIN Persona p ON p.IDPERSONA = ep.persona_IDPERSONA"
					+ " INNER JOIN TipoProceso tp ON tp.id = pr.tipoProceso_id" + " WHERE 1=1"
					// + " AND r.fecha BETWEEN '"+ DateHelper.getStringFull4(fechaDesde) +"
					// 00:00:00' AND '"+ DateHelper.getStringFull4(fechaHasta) +" 59:59:59'"
					+ " AND r.fecha BETWEEN CONVERT(datetime, ':fechaDesde', 102)  and CONVERT(datetime, ':fechaHasta', 102)"
					+ (this.getCodAbogado() != null && !this.getCodAbogado().isEmpty()
							? (" AND p.IDPERSONA = " + this.getCodAbogado())
							: "");

			query = query.replace(":fechaDesde", new Timestamp(fechaDesde.getTime()).toString());
			query = query.replace(":fechaHasta", new Timestamp(fechaHasta.getTime()).toString());

			List<Object[]> resultados = bp.createSQLQuery(query).list();

			if (estados != null) {

				for (TipoProceso estado : estados) {
					JsonObject obj = new JsonObject();

					ArrayList<Integer> valores = new ArrayList<>();
					valores.add(0);
					valores.add(0);
					valores.add(0);

					Integer mayor7 = 0;
					Integer mayor15 = 0;
					Integer mayor20 = 0;

					for (Object[] l : resultados) {
						java.sql.Timestamp fecha = (java.sql.Timestamp) l[0];
						String numero = (String) l[1];
						String nombre = (String) l[2];
						String tipoProceso = (String) l[3];

						if (estado.getNombreTipoProceso().equals(tipoProceso)) {
							java.sql.Date fechaInicial = (java.sql.Date) DateHelper.timestampToDate(fecha);

							long diferencia = (fechaActual.getTime() - fechaInicial.getTime()) / (1000 * 60 * 60 * 24);

							if (diferencia >= 20) {
								mayor20++;
							} else if (diferencia >= 15) {
								mayor15++;
							} else if (diferencia >= 7) {
								mayor7++;
							}
						}

					}

					valores.set(0, (mayor7 != null ? mayor7 : 0));
					valores.set(1, (mayor15 != null ? mayor15 : 0));
					valores.set(2, (mayor20 != null ? mayor20 : 0));

					obj.addProperty("estado", estado.getNombreTipoProceso());
					obj.addProperty("cantidad", valores.toString());

					list.add(obj);
				}

			}

			this.estadistica = list.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean doProcess() {

		if (this.action == null) {
			this.fechaDesde = DateHelper.restarFechasDias(new Date(), 30);
			this.fechaHasta = new Date();
		}

		getEstadisticaJuicio();

		return this.errores.isEmpty();
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public boolean validate() {
		return this.errores.isEmpty();
	}

	@Override
	public Object getResult() {
		return null;
	}

	public HashMap getErrors() {
		return this.errores;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	public String getCodAbogado() {
		return codAbogado;
	}

	public void setCodAbogado(String codAbogado) {
		this.codAbogado = codAbogado;
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
