package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;

import com.asf.cred.struts.form.InformeEnteRecaudadorForm;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Linea;

public class GenerarInformeDeuda implements IProcess{
	
	private String forward;
	private String ultimaActualizacion;
	private String ultimaRecaudacion;
	private String ultimaCobranza;
	private String ultimaActualizacionEstado;
	
	private List<Linea> lineas;
	
	public GenerarInformeDeuda() {
		forward = "GenerarInformeDeuda";
	}
	
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lineas = bp.createQuery("select l from Linea l order by l.nombre").list();
		
		return true;
	}

	public List<Linea> getLineas() {
		return lineas;
	}
	
	public String getUltimaActualizacionEstado() {
		return ultimaActualizacionEstado;
	}
	
	public void setUltimaActualizacionEstado(String ultimaActualizacionEstado) {
		this.ultimaActualizacionEstado = ultimaActualizacionEstado;
	}
	
	public String getUltimaActualizacion() {
		return ultimaActualizacion;
	}

	public void setUltimaActualizacion(String ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}

	public String getUltimaRecaudacion() {
		return ultimaRecaudacion;
	}

	public void setUltimaRecaudacion(String ultimaRecaudacion) {
		this.ultimaRecaudacion = ultimaRecaudacion;
	}

	public String getUltimaCobranza() {
		return ultimaCobranza;
	}

	public void setUltimaCobranza(String ultimaCobranza) {
		this.ultimaCobranza = ultimaCobranza;
	}

	@Override
	public HashMap<String, String> getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
