package com.asf.cred.business;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;

import com.asf.gaf.hibernate.Bancos;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Persona;
import com.civitas.importacion.ImportarBcoCredicop;
import com.civitas.importacion.ImportarBcoNacion;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.EstadoCheque;

public class AcreditacionValores implements IProcess{
    
    private String accion;
    private String forward;
    private BusinessPersistance bp;
    private HashMap<String, Object> errores;
    
    private Date fechaDesde;
    private Bancos banco;
    private Persona titular;
    private String numeroValor;
    private List<DetalleRecaudacion> detalles;
    private Date fechaAcreditacion;
    private String mensaje;

    public AcreditacionValores() {
	bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	forward = "AcreditacionValores";
	errores = new HashMap<String, Object>();
	banco = new Bancos();
	fechaDesde = new Date();
	fechaAcreditacion = new Date();
	titular = new Persona();
    }
    
    @Override
    public boolean doProcess() {
	if(accion == null){
	    return false;
	}else if(accion.equals("listar")){		    
	    return listar();
	}else if(accion.equals("acreditar")){
	    return acreditar();
	}
	return errores.isEmpty();
    }
    
    @SuppressWarnings("unchecked")
    public boolean listar(){
	if(fechaDesde == null){
	    errores.put("acreditacionValores.fechaDesde", "acreditacionValores.fechaDesde");
	    return false;
	}
	String queryStr = "SELECT DISTINCT d.* " +
                        "FROM DetalleRecaudacion d " + 
                        "JOIN EstadoCheque e ON e.detalleRecaudacion_id = d.id " +
                        "JOIN Objetoi o ON o.numeroAtencion = d.numeroProyecto " +
                        "JOIN PERSONA p ON p.IDPERSONA = o.persona_IDPERSONA " +  
                        "WHERE d.fechaRecaudacion >= :fechaDesde " +  
                        "AND d.banco_CODI_BA = :banco_id " +
                        "AND e.estado = :estado " + 
                        "AND e.id = (SELECT MAX(es.id) FROM EstadoCheque es WHERE es.detalleRecaudacion_id = d.id) ";
	if(titular.getId() != null && titular.getId().longValue() != 0){
	    queryStr += "AND p.IDPERSONA = :persona_id ";
	}
	if(numeroValor != null && !numeroValor.trim().isEmpty() && !numeroValor.trim().equals("0")){
	    queryStr += "AND d.numeroValor = :numeroValor ";
	}
	String estado = banco.getCodiBa() == 8 ? EstadoCheque.ESTADO_CHEQUE_NACION_PRESENTADO : EstadoCheque.ESTADO_CHEQUE_CREDICOOP_PRESENTADO;
	Query query = bp.createSQLQuery(queryStr).addEntity(DetalleRecaudacion.class)
    	    			.setLong("banco_id", banco.getCodiBa())
    	    			.setDate("fechaDesde", fechaDesde)
    	    			.setString("estado", estado);
	if(titular.getId() != null && titular.getId().longValue() != 0){
	    query.setLong("persona_id", titular.getId());
	}
	if(numeroValor != null && !numeroValor.trim().isEmpty() && !numeroValor.trim().equals("0")){
	    query.setString("numeroValor", numeroValor);
	}
	detalles = query.list();
	return true;
    }
    
    @SuppressWarnings("unchecked")
    public boolean acreditar(){
	if(fechaAcreditacion == null){
	    errores.put("acreditacionValores.fechaAcreditacion", "acreditacionValores.fechaAcreditacion");
    	    return false;
	}
	HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
	Enumeration<String> paramNames = request.getParameterNames();
	HashSet<Long> seleccionados = new HashSet<Long>();
	String param;
	String[]p;
	while (paramNames.hasMoreElements()) {
	    param = paramNames.nextElement();
	    if (param.startsWith("requisito")) {
		p = param.split("-");
		seleccionados.add(new Long(p[1]));
	    }
	}
	if(seleccionados.isEmpty()){
	    errores.put("acreditacionValores.seleccion", "acreditacionValores.seleccion");
	    return false;
	}
	EstadoCheque estadoCheque;
	String estado = banco.getCodiBa() == 8 ? EstadoCheque.ESTADO_CHEQUE_NACION_LIBERADO : EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO;
	DetalleRecaudacion detalle;
	for(Long id : seleccionados){
	    	detalle = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class, id);
		bp.begin();
		estadoCheque = new EstadoCheque();
		estadoCheque.setEstado(estado);
		estadoCheque.setDetalleRecaudacion(detalle);
		estadoCheque.setFecha(fechaAcreditacion);
		bp.save(estadoCheque);
		detalle.setFechaAcreditacionValor(fechaAcreditacion);
		bp.update(detalle);
		try {
		    if(banco.getCodiBa() == 8){
			ImportarBcoNacion.invocarCambioEstadoValor(detalle, "Acreditado");
		    }else{
			ImportarBcoCredicop.invocarCambioEstadoValor(detalle, "Acreditado");
		    }
		    bp.commit();
		} catch (MalformedURLException e) {
		    e.printStackTrace();
		    bp.rollback();
		    errores.put("acreditacionValores.error", "acreditacionValores.error");
		    return false;
		} catch (Exception e) {
		    e.printStackTrace();
		    bp.commit();
//		    bp.rollback();
//		    errores.put("acreditacionValores.error", "acreditacionValores.error");
//		    return false;
		}		
	}
	mensaje = "Se acreditaron los valores con �xito.";
	return true;
    }

    @Override
    public HashMap<String, Object> getErrors() {
	return errores;
    }

    @Override
    public String getForward() {
	return forward;
    }

    @Override
    public String getInput() {
	return forward;
    }

    @Override
    public Object getResult() {
	return null;
    }

    @Override
    public boolean validate() {
	return true;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Bancos getBanco() {
        return banco;
    }

    public void setBanco(Bancos banco) {
        this.banco = banco;
    }

    public List<DetalleRecaudacion> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleRecaudacion> detalles) {
        this.detalles = detalles;
    }        
    
    public String getFechaDesdeStr() {
	return DateHelper.getString(fechaDesde);
    }
    
    public void setFechaDesdeStr(String fechaDesdeStr) {
	this.fechaDesde = DateHelper.getDate(fechaDesdeStr);
    }
    public String getNumeroValor() {
	return numeroValor;
    }
    public void setNumeroValor(String numeroValor) {
	this.numeroValor = numeroValor;
    }
    public String getFechaAcreditacionStr() {
	return DateHelper.getString(fechaAcreditacion);
    }
    public void setFechaAcreditacionStr(String fechaAcreditacionStr) {
	this.fechaAcreditacion = DateHelper.getDate(fechaAcreditacionStr);
    }
    public Persona getTitular() {
	return titular;
    }
    public void setTitular(Persona titular) {
	this.titular = titular;
    }
    public String getMensaje() {
	return mensaje;
    }
}
