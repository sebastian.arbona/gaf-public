package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.civitas.hibernate.persona.*;
import com.civitas.hibernate.persona.creditos.*;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;

/**
 * 
 * @author eselman
 *
 */
public class PersonaVinculada implements IProcess 
{
    //=========================ATRIBUTOS===========================
    //atributos utilitarios del proceso.-
    private String forward = "personaVinculadaList";
    private HashMap<String, Object> errores;
    private String accion = "";
    private BusinessPersistance bp;
    //atributos del proceso.-
    private com.civitas.hibernate.persona.PersonaVinculada personaVinculada;
    private List<com.civitas.hibernate.persona.PersonaVinculada> personasVinculadas;
    private Persona personaTitular;
    //=======================CONSTRUCTORES=========================
    public PersonaVinculada() 
    {
        this.errores = new HashMap<String, Object>();
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        this.personaVinculada = new com.civitas.hibernate.persona.PersonaVinculada();
        this.personaTitular = new Persona();
    }
    //======================FUNCIONALIDADES========================
    public boolean doProcess() 
    {
        if (this.getAccion().equalsIgnoreCase("asignar")) 
        {
            this.forward = "personaVinculada";
        }
        else if(this.getAccion().equalsIgnoreCase("guardar"))
        {
        	this.validarPersonaVinculada();
            if (this.errores.isEmpty()) 
            {
                this.asignarPersonaVinculada();
                if(this.errores.isEmpty())
                {
                	this.forward = "personaVinculadaList";
                }
                else
                {
                	this.forward = "personaVinculada";
                }
            }
            else
            {
            	this.forward = "personaVinculada";
            }
        }
        else if(this.getAccion().equalsIgnoreCase("cancelar"))
        {
        	this.forward = "personaVinculadaList";
        }
        else if (this.getAccion().equalsIgnoreCase("eliminar")) 
        {
            this.eliminarPersonaVinculada();
        }
        else if (this.getAccion().equalsIgnoreCase("load")) 
        {
            this.cargarPersonaVinculada();
            this.forward = "personaVinculada";
            return true;
        }
        this.listarPersonasVinculadas();
        return this.errores.isEmpty();
    }
    //=====================UTILIDADES PRIVADAS=====================
    /**
     * Validaciones a realizar:
     * 1) La persona titular es obligatoria.-
     * 2) La persona vinculada es obligatoria.-
     * 3) El tipo de relaci�n es obligatorio.-
     */
    private void validarPersonaVinculada() 
    {
        //1)
        if (this.getPersonaTitular() == null || this.getPersonaTitular().getId().longValue() < 1) 
        {
            this.errores.put("PersonaVinculada.PersonaTitular.vacia", "PersonaVinculada.PersonaTitular.vacia");
        }

        //2)
        if (this.getPersonaVinculada().getPersonaVinculada() == null || this.getPersonaVinculada().getPersonaVinculada().getId().longValue() < 1) 
        {
            this.errores.put("PersonaVinculada.Persona.vacia", "PersonaVinculada.Persona.vacia");
        }

        //3)
        if (this.getPersonaVinculada().getRelacion() == null || this.getPersonaVinculada().getRelacion().getId().longValue() < 1) 
        {
            this.errores.put("PersonaVinculada.Relacion.vacia", "PersonaVinculada.Relacion.vacia");
        }
    }

    private void asignarPersonaVinculada() 
    {
        try 
        {
            this.setPersonaTitular((Persona) this.bp.getById(Persona.class, this.getPersonaTitular().getId()));
            this.getPersonaVinculada().setPersonaTitular(this.getPersonaTitular());
            this.bp.saveOrUpdate(this.getPersonaVinculada());
            this.setPersonaVinculada(new com.civitas.hibernate.persona.PersonaVinculada());
        }
        catch (Exception e) 
        {
            this.errores.put("PersonaVinculada.Error.guardar", "PersonaVinculada.Error.guardar");
            e.printStackTrace();
        }
    }
    private void eliminarPersonaVinculada() 
    {
        if (this.personaVinculada.getId() != null) 
        {
            try 
            {
                this.bp.delete(this.personaVinculada);
            } 
            catch (Exception e) 
            {
                this.errores.put("PersonaVinculada.Error.eliminar", "PersonaVinculada.Error.eliminar");
                e.printStackTrace();
            }
        }
    }

    private void cargarPersonaVinculada() 
    {
        if (this.getPersonaVinculada() != null) 
        {
            try 
            {
                this.setPersonaVinculada((com.civitas.hibernate.persona.PersonaVinculada) this.bp.getById(com.civitas.hibernate.persona.PersonaVinculada.class, this.getPersonaVinculada().getId()));
            }
            catch (Exception e) 
            {
                this.setPersonaVinculada(new com.civitas.hibernate.persona.PersonaVinculada());
                this.errores.put("PersonaVinculada.Error.cargar", "PersonaVinculada.Error.carga");
                e.printStackTrace();
            }
        }
    }

    private void listarPersonasVinculadas() 
    {
        if (this.personaTitular != null && this.personaTitular.getId().longValue() > 0) 
        {
            try 
            {
            	
                this.personaTitular = (Persona) this.bp.getById(Persona.class, this.personaTitular.getId());
                
                
                String consulta = "select id as id, personaTitular_idpersona as personaTitular_idpersona, personaVinculada_idpersona as personaVinculada_idpersona, relacion_idRelacion as relacion_idRelacion, idObjetoi, numeroAtencion from "+this.bp.getSchema()+".PersonaVinculada "+
                	"WHERE personaTitular_idpersona = '" + this.personaTitular.getId().longValue() + "' "+
                	"union "+
                	"select id as id, personaVinculada_idpersona as personaTitular_idpersona, personaTitular_idpersona as personaVinculada_idpersona, inversa_idRelacion as relacion_idRelacion, idObjetoi, numeroAtencion from "+ 
                	this.bp.getSchema()+".PersonaVinculada inner join "+this.bp.getSchema()+".Relacion on relacion_idRelacion=idrelacion and inversa_idRelacion is not null "+
                	"WHERE personaVinculada_idpersona = '" + this.personaTitular.getId().longValue() + "'";

  //              consulta += " WHERE pv.personaTitular.id = '" + this.personaTitular.getId().longValue() + "'";
                
                this.personasVinculadas=this.bp.createSQLQuery(consulta).addEntity(com.civitas.hibernate.persona.PersonaVinculada.class).list();
                
            }
            catch (Exception e) 
            {
                this.personasVinculadas = new ArrayList<com.civitas.hibernate.persona.PersonaVinculada>();
                e.printStackTrace();
            }
        }
    }
    //======================GETTERS Y SETTERS======================
    public HashMap<String, Object> getErrors() 
    {
        return this.errores;
    }
    public String getForward() 
    {
        return this.forward;
    }
    public String getInput() 
    {
        return this.forward;
    }
    public Object getResult() 
    {
        return null;
    }
    public com.civitas.hibernate.persona.PersonaVinculada getPersonaVinculada() 
    {
        return this.personaVinculada;
    }
    public void setPersonaVinculada(com.civitas.hibernate.persona.PersonaVinculada personaVinculada) 
    {
        this.personaVinculada = personaVinculada;
    }
    public String getAccion() 
    {
        return this.accion;
    }
    public void setAccion(String accion) 
    {
        this.accion = accion;
    }
    public List<com.civitas.hibernate.persona.PersonaVinculada> getPersonasVinculadas() 
    {
        return this.personasVinculadas;
    }
    public void setPersonasVinculadas(List<com.civitas.hibernate.persona.PersonaVinculada> personasVinculadas) 
    {
        this.personasVinculadas = personasVinculadas;
    }
    public Persona getPersonaTitular() 
    {
        return personaTitular;
    }
    public void setPersonaTitular(Persona personaTitular) 
    {
        this.personaTitular = personaTitular;
    }
    //========================VALIDACIONES=========================
    public boolean validate() 
    {
        return this.errores.isEmpty();
    }
}