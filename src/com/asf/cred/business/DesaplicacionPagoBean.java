package com.asf.cred.business;

import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_BONIFICACION;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_CAPITAL;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_COMPENSATORIO;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_GASTOS;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_GASTOS_A_RECUPERAR;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_MORATORIO;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_MULTAS;
import static com.nirven.creditos.hibernate.Concepto.CONCEPTO_PUNITORIO;
import static com.nirven.creditos.hibernate.Tipomov.TIPOMOV_CREDITO;

import java.io.Serializable;
import java.util.Date;

import com.asf.cred.business.Reimputacion.RecaudacionSeleccionada;
import com.nirven.creditos.hibernate.Ctacte;

public class DesaplicacionPagoBean implements Serializable {

	private static final long serialVersionUID = 6000439855837284120L;

	private Long idBoleto;
	private String tipoComprobante;
	private Long numeroComprobante;
	private double importeTotal;
	private Date fechaProceso;
	private Date fechaPago;
	private int numeroCuota;
	private double importeDesaplicar;
	private double capital;
	private double compensatorio;
	private double moratorio;
	private double punitorio;
	private double gastos;
	private double gastosRec;
	private double multas;
	private boolean comprobante;
	private Long idBoletoAnulado;
	private String comportamientoReimputacion;
	private Date fechaVencimiento;
	private String detalle;
	private RecaudacionSeleccionada recaudacion;
	
	public DesaplicacionPagoBean() {
	}
	
	public DesaplicacionPagoBean(Ctacte cc) {
		this(cc, true);
	}
	
	public DesaplicacionPagoBean(Ctacte cc, boolean comprobante) {
		this.comprobante = comprobante;
		this.idBoleto = cc.getBoleto().getId();
		this.tipoComprobante = cc.getBoleto().getTipo();
		this.numeroComprobante = cc.getBoleto().getNumeroBoleto();
		this.importeTotal = cc.getBoleto().getImporte() != null ? cc.getBoleto().getImporte() : 0;
		this.fechaProceso = cc.getFechaProceso();
		this.fechaPago = cc.getFechaGeneracion();
		this.numeroCuota = cc.getCuota().getNumero();
		
		addMovimiento(cc);
	}
	
	public void addMovimiento(Ctacte cc) {
		double importe = cc.getTipomov().getAbreviatura().equals(TIPOMOV_CREDITO)? -cc.getImporte() : cc.getImporte();
		
		this.importeDesaplicar += importe;
		
		String con = cc.getAsociado().getConcepto().getAbreviatura();
		if (con.equals(CONCEPTO_CAPITAL)) {
			this.capital += importe;
		} else if (con.equals(CONCEPTO_COMPENSATORIO) || con.equals(CONCEPTO_BONIFICACION)) {
			this.compensatorio += importe;
		} else if (con.equals(CONCEPTO_MORATORIO)) {
			this.moratorio += importe;
		} else if (con.equals(CONCEPTO_PUNITORIO)) {
			this.punitorio += importe;
		} else if (con.equals(CONCEPTO_GASTOS)) {
			this.gastos += importe;
		} else if (con.equals(CONCEPTO_GASTOS_A_RECUPERAR)) {
			this.gastosRec += importe;
		} else if (con.equals(CONCEPTO_MULTAS)) {
			this.multas += importe;
		}
	}
	
	public void addMovimiento(DesaplicacionPagoBean d) {
		this.capital += d.capital;
		this.compensatorio += d.compensatorio;
		this.moratorio += d.moratorio;
		this.punitorio += d.punitorio;
		this.gastos += d.gastos;
		this.gastosRec += d.gastosRec;
		this.multas += d.multas;
	}
	
	public boolean isSaldoCero() {
		return isCero(capital) && isCero(compensatorio) && isCero(moratorio)
				&& isCero(punitorio) && isCero(gastos) && isCero(gastosRec) && isCero(multas);
	}
	
	private boolean isCero(double m) {
		return m < 0.005;
	}
	
	public double getDeudaImponible() {
		return capital + compensatorio + gastos + gastosRec + multas;
	}
	
	public String getDescripcionComprobante() {
		if (!comprobante)
			return "";
		
		return tipoComprobante + " " + numeroComprobante;
	}
	
	public double pagar(DesaplicacionPagoBean saldo, double pago) {
		if (saldo.gastosRec >= 0.005) {
			if (pago > saldo.gastosRec) {
				gastosRec = saldo.gastosRec;
				pago -= saldo.gastosRec;
				saldo.gastosRec = 0;
			} else {
				gastosRec = pago;
				saldo.gastosRec -= pago;
				pago = 0;
			}
		}
		
		if (saldo.gastos >= 0.005) {
			if (pago > saldo.gastos) {
				gastos = saldo.gastos;
				pago -= saldo.gastos;
				saldo.gastos = 0;
			} else {
				gastos = pago;
				saldo.gastos -= pago;
				pago = 0;
			}
		}
		
		if (saldo.multas >= 0.005) {
			if (pago > saldo.multas) {
				multas = saldo.multas;
				pago -= saldo.multas;
				saldo.multas = 0;
			} else {
				multas = pago;
				saldo.multas -= pago;
				pago = 0;
			}
		}
		
		if (saldo.punitorio >= 0.005) {
			if (pago > saldo.punitorio) {
				punitorio = saldo.punitorio;
				pago -= saldo.punitorio;
				saldo.punitorio = 0;
			} else {
				punitorio = pago;
				saldo.punitorio -= pago;
				pago = 0;
			}
		}
		
		if (saldo.moratorio >= 0.005) {
			if (pago > saldo.moratorio) {
				moratorio = saldo.moratorio;
				pago -= saldo.moratorio;
				saldo.moratorio = 0;
			} else {
				moratorio = pago;
				saldo.moratorio -= pago;
				pago = 0;
			}
		}
		
		if (saldo.compensatorio >= 0.005) {
			if (pago > saldo.compensatorio) {
				compensatorio = saldo.compensatorio;
				pago -= saldo.compensatorio;
				saldo.compensatorio = 0;
			} else {
				compensatorio = pago;
				saldo.compensatorio -= pago;
				pago = 0;
			}
		}
		
		if (saldo.capital >= 0.005) {
			if (pago > saldo.capital) {
				capital = saldo.capital;
				pago -= saldo.capital;
				saldo.capital = 0;
			} else {
				capital = pago;
				saldo.capital -= pago;
				pago = 0;
			}
		}
		
		return pago;
	}
	
	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public Long getNumeroComprobante() {
		return numeroComprobante;
	}

	public void setNumeroComprobante(Long numeroComprobante) {
		this.numeroComprobante = numeroComprobante;
	}

	public double getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(double importeTotal) {
		this.importeTotal = importeTotal;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public int getNumeroCuota() {
		return numeroCuota;
	}

	public void setNumeroCuota(int numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	public double getImporteDesaplicar() {
		return importeDesaplicar;
	}

	public void setImporteDesaplicar(double importeDesaplicar) {
		this.importeDesaplicar = importeDesaplicar;
	}

	public double getCapital() {
		return capital;
	}

	public void setCapital(double capital) {
		this.capital = capital;
	}

	public double getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(double compensatorio) {
		this.compensatorio = compensatorio;
	}

	public double getMoratorio() {
		return moratorio;
	}

	public void setMoratorio(double moratorio) {
		this.moratorio = moratorio;
	}

	public double getPunitorio() {
		return punitorio;
	}

	public void setPunitorio(double punitorio) {
		this.punitorio = punitorio;
	}

	public double getGastos() {
		return gastos;
	}

	public void setGastos(double gastos) {
		this.gastos = gastos;
	}

	public double getGastosRec() {
		return gastosRec;
	}

	public void setGastosRec(double gastosRec) {
		this.gastosRec = gastosRec;
	}

	public double getMultas() {
		return multas;
	}

	public void setMultas(double multas) {
		this.multas = multas;
	}
	
	public boolean isComprobante() {
		return comprobante;
	}
	
	public void setComprobante(boolean comprobante) {
		this.comprobante = comprobante;
	}

	public Long getIdBoleto() {
		return idBoleto;
	}

	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}

	public Long getIdBoletoAnulado() {
		return idBoletoAnulado;
	}

	public void setIdBoletoAnulado(Long idBoletoAnulado) {
		this.idBoletoAnulado = idBoletoAnulado;
	}

	public String getComportamientoReimputacion() {
		return comportamientoReimputacion;
	}

	public void setComportamientoReimputacion(String comportamientoReimputacion) {
		this.comportamientoReimputacion = comportamientoReimputacion;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public RecaudacionSeleccionada getRecaudacion() {
		return recaudacion;
	}

	public void setRecaudacion(RecaudacionSeleccionada recaudacion) {
		this.recaudacion = recaudacion;
	}
	
}
