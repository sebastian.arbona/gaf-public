package com.asf.cred.business;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;

import com.asf.cred.security.SessionHandler;
import com.asf.hibernate.mapping.Director;
import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.cred.beans.BeanNotificacion;
import com.civitas.dto.EstadoNotificacionDTO;
import com.civitas.hibernate.persona.Persona;
import com.civitas.logger.Log4jConstants;
import com.civitas.logger.LoggerService;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.mailer.Mail;
import com.sun.istack.ByteArrayDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class NotificacionesListProcess extends HttpServlet implements IProcess {

	private static final long serialVersionUID = 1L;
	private String forward;
	private String action;
	private Object result;
	private List<Objetoi> creditos;
	private List<BeanNotificacion> creditosBean;
	private List<Linea> lineas;

	private String[] idsLineas;
	private String codigo;
	private Long numeroAtencion;
	private Date fechaMora;
	private List<Notificacion> notificaciones;
	private List<Objetoi> creditosSeleccionados;
	private List<EstadoNotificacionDTO> listaEstadoNotificacionDTO;
	private Notificacion notificacion;

	private Date fechaCreada;
	private Date fechaVenc;
	private boolean terceros;
	private boolean mail;
	private boolean emitir;
	private boolean fiador;
	private String observaciones;
	private Long idTipoAviso;
	private Long idTipoNotificacion;
	private Tipificadores tipoAviso;
	// private Tipificadores tipoNotificacion;
	private Long idConf;
	private JasperReport reporte;
	private JasperReport reporte2;
	private List<JasperReport> reportes;
	protected HashMap<String, Object> errores = new HashMap<String, Object>();
	private String ids;
	// private String estado;
	SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
	private Long idPersona;
	private LoggerService logger = LoggerService.getInstance();

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lineas = bp.getByFilter("select l from Linea l order by l.nombre");

		if (action == null) {
			forward = "NotificacionesMoraList";
		} else if (action.equalsIgnoreCase("listar")) {
			// Filtros para la consulta
			String estadosWhere = "('EJECUCION', 'PRIMER DESEMBOLSO', 'PENDIENTE SEGUNDO DESEMBOLSO')";
			String numeroAtencionWhere = "";
			String codigoWhere = "";
			String personaWhere = "";
			// Caso: N�mero de Atenci�n espec�fico
			if (numeroAtencion != null && numeroAtencion != 0L) {
				numeroAtencionWhere = "AND o.numeroAtencion = " + this.numeroAtencion;
			}
			// Caso: Estado espec�fico
			if (this.codigo != null && !this.codigo.isEmpty()) {
				codigoWhere = "AND oc.comportamientoPago = " + this.codigo;
			}
			// Caso: Persona espec�fica
			if (idPersona != null && idPersona != 0L) {
				personaWhere = "AND o.persona.id = " + this.idPersona;
			}
			List<Long> lineasSeleccionadas = new ArrayList<Long>();
			if (idsLineas != null) {
				for (String l : idsLineas) {
					lineasSeleccionadas.add(new Long(l));
				}
			}

			// Caso con fecha de mora
			if (fechaMora != null) {
				List<Long> creditosId = bp.createQuery(
						"select distinct o.id from Cuota c join c.credito o where c.fechaVencimiento = :fecha and c.fechaVencimiento < :hoy and c.estado = '1' ")
						.setParameter("fecha", fechaMora).setParameter("hoy", new Date()).list();

				// Los conjuntos no pueden ser vacios en la consulta
				if (creditosId.isEmpty())
					creditosId.add(new Long(0));

				// Caso: L�nea espec�fica
				if (!lineasSeleccionadas.isEmpty()) {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere
								+ " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).setParameterList("linea", lineasSeleccionadas)
								.setParameterList("objetoiId", creditosId).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere
								+ " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).setParameterList("linea", lineasSeleccionadas)
								.setParameterList("objetoiId", creditosId).list();
					}

				}
				// Caso: Todas las l�neas
				else {
					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).setParameterList("objetoiId", creditosId).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).setParameterList("objetoiId", creditosId).list();
					}
				}
			}
			// Caso sin fecha de mora
			else {
				// Caso: L�nea espec�fica
				if (!lineasSeleccionadas.isEmpty()) {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).setParameterList("linea", lineasSeleccionadas).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).setParameterList("linea", lineasSeleccionadas).list();
					}

				}
				// Caso: Todas las L�neas
				else {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).list();
					} else {
						this.creditos = bp
								.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc JOIN oc.objetoi o "
										+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
										+ estadosWhere + " AND obje.fechaHasta IS NULL AND obje.fecha = "
										+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
										+ codigoWhere + " AND oc.fechaHasta IS NULL "
										+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
										+ numeroAtencionWhere)
								.list();
					}

				}
			}
			Query bonifQuery = bp.createQuery(
					"select count(b) from ObjetoiBonificacion b where b.objetoi.id = :id and b.objetoi.fechaBajaBonificacion is null");
			creditosBean = new ArrayList<BeanNotificacion>();
			for (Objetoi cred : creditos) {
				BeanNotificacion bn = new BeanNotificacion();
				bn.setCredito(cred);
				Number countBonif = (Number) bonifQuery.setParameter("id", cred.getId()).uniqueResult();
				if (countBonif != null && countBonif.intValue() != 0) {
					bn.setBonificado(true);
				}
				creditosBean.add(bn);
			}
			// guardarDeuda();
			// guardarDeudaActual();
			// guardarEstadoListado();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("NotificacionList.credito",
					creditos);
			forward = "NotificacionesMoraList";
		} else if (action.equalsIgnoreCase("generar")) {
			actualizarSeleccion();
			forward = "GenerarNotificaciones"; // va a la vita de genera notificaciones
		} else if (action.equalsIgnoreCase("guardar")) {
			guardarNotificaciones();
			if (emitir) {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("boleto", true);
			} else {
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("boleto", false);
			}
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("ids", ids);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("tipoNoti", idTipoAviso);
			forward = "NotificacionesRedirect";
		} else if (action.equalsIgnoreCase("listarVolver")) {
			// Lista cuando vuelve del historial de alguna notificacion
			creditos = (List<Objetoi>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("NotificacionList.credito");
			creditosBean = new ArrayList<BeanNotificacion>();
			for (Objetoi cred : creditos) {
				BeanNotificacion bn = new BeanNotificacion();
				bn.setCredito(cred);
				creditosBean.add(bn);
			}
			forward = "NotificacionesMoraList";
		}
		return true;
	}

	// Metodos privados
	private void guardarDeuda() {
		for (Objetoi credito : creditos) {
			CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(credito.getId(), new Date());
			calculo.calcular();

			BeanCtaCte beanTotal = calculo.getTotal();
			credito.setDeudaTotal(beanTotal.getCapital());
			double deudaTotal = beanTotal.getSaldoCuota();
			credito.setTotalDeudaActual(deudaTotal);
		}
	}

	// El siguiente metodo es provisorio hasta definir donde se calcula
	// definitivamente la deuda
	private void guardarDeudaActual() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		for (Objetoi cred : creditos) {
			CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(cred.getId(), new Date());
			calculoDeuda.calcular();
			BeanCtaCte beanTotal = calculoDeuda.getTotal();
			double deudaTotal = beanTotal.getSaldoCuota();
			cred.setTotalDeudaActual(deudaTotal);
			bp.update(cred);
		}
	}

	private void guardarNotificaciones() {
		creditosSeleccionados = (List<Objetoi>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("NotificacionList.creditosSel");
		listaEstadoNotificacionDTO = new ArrayList<EstadoNotificacionDTO>();
		for (Objetoi cred : creditosSeleccionados) {
			Notificacion notificacion = crearNotificacion(cred);

			ids += "," + notificacion.getId();
			if (mail) {
				String email = esEmail(cred.getPersona().getEmailNotificacion())
						? cred.getPersona().getEmailNotificacion()
						: cred.getPersona().getEmailPrincipal();
				envioMail(cred, notificacion, email);
				continue;
			}

			if (terceros) {
				BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
				List<Persona> terceros = bp.createQuery(
						"Select distinct gu.persona from GarantiaUso gu where gu.porcentaje>:porc and gu.persona!=:pers and gu.garantia=(Select g.garantia from ObjetoiGarantia g where g.objetoi=:cred and g.baja is null)")
						.setParameter("cred", cred).setParameter("porc", 0.0).setParameter("pers", cred.getPersona())
						.list();
				for (Persona persona : terceros) {
					if (esEmail(persona.getEmailPrincipal())) {
						EstadoNotificacionDTO estadoNotDTO = new EstadoNotificacionDTO();
						String estado, relacion;
						try {
							enviarMail(cred, notificacion, persona.getEmailPrincipal());
							estado = "Mensaje Enviado";
							relacion = "Titular";
						} catch (JRException e) {
							estado = "Error en la generacion del reporte";
							relacion = "Fiador";
						} catch (IOException e) {
							estado = "Error de Comunicacion";
							relacion = "Fiador";
						} catch (MessagingException e) {
							estado = "Error en la creacion del Mensaje";
							relacion = "Fiador";
						} catch (Exception e) {
							estado = "Error: Consulte con el Administrador";
							relacion = "Fiador";
						}
						estadoNotDTO.setEstado(cred, estado, relacion, persona.getEmailPrincipal());
						listaEstadoNotificacionDTO.add(estadoNotDTO);
					}
				}
			}
		}

	}

	private boolean envioMail(Objetoi cred, Notificacion notificacion, String email) {
		EstadoNotificacionDTO estadoNotDTO = new EstadoNotificacionDTO();
		String Mensaje = null;
		// Refactorizo envio mail titular
		try {
			enviarMail(cred, notificacion, email);
			estadoNotDTO.setEstado(cred, "Mensaje Enviado", "Titular", email);
		} catch (JRException e) {
			Mensaje = "Error en la Generaci�n del Reporte";
			logger.log(Log4jConstants.ERROR, e.toString());
		} catch (IOException e) {
			Mensaje = "Error de Comunicaci�n";
			logger.log(Log4jConstants.ERROR, "Error de Comunicaci�n");
		} catch (MessagingException e) {
			Mensaje = "Error en la creaci�n del Mensaje";
			logger.log(Log4jConstants.ERROR, "Error en la creaci�n del Mensaje");
		} catch (Exception e) {
			Mensaje = "Error:Consulte con el administrador";
			logger.log(Log4jConstants.ERROR, "Error:Consulte con el administrador");
		}
		estadoNotDTO.setEstado(cred, Mensaje, "Titular", email);
		listaEstadoNotificacionDTO.add(estadoNotDTO);
		return true;
	}

	// metodo para validar correo electronico
	public boolean esEmail(String correo) {
		boolean esEmail;
		if (!(correo == null || correo.isEmpty())) {
			Pattern pat = Pattern.compile(
					"^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
			Matcher mat = pat.matcher(correo);
			esEmail = mat.find();
		} else {
			esEmail = false;
		}
		return esEmail;
	}

	private boolean enviarMail(Objetoi credito, Notificacion noti, String dirMail) throws Exception {
		String asunto = noti.getConfiguracion().getAsunto();
		String mensaje = noti.getConfiguracion().getTexto() + "\n \n \nFecha emision: "
				+ formato.format(noti.getFechaCreada()) + "\nFecha Vencimiento: " + formato.format(noti.getFechaVenc())
				+ "\nObservaciones: " + noti.getObservaciones();
		HashMap<String, byte[]> adjuntos = new HashMap<String, byte[]>();
		byte[] pdf = generarReporte(noti);
		if (pdf != null) {
			String fileNameBoleto = "BoletoNotificacionDelCreditoNro:" + credito.getNumeroAtencionStr() + ".pdf";
			adjuntos.put(fileNameBoleto, pdf);
		}
		byte[] pdfNoti = generarReporteNotificacion(noti);
		if (pdfNoti != null) {
			String fileNameNotif = "Notificacion Credito Nro:" + credito.getNumeroAtencionStr() + ".pdf";
			adjuntos.put(fileNameNotif, pdfNoti);
		}
		return Mail.sendMailPdf(dirMail, asunto, mensaje, adjuntos);
	}

	private Notificacion crearNotificacion(Objetoi cred) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Notificacion notificacion = new Notificacion();
		notificacion.setCredito(cred);
		notificacion.setFechaCreada(fechaCreada);
		notificacion.setFechaVenc(fechaVenc);
		notificacion.setObservaciones(observaciones);
		notificacion.setConfiguracion((ConfiguracionNotificacion) bp.getById(ConfiguracionNotificacion.class, idConf));
		notificacion.setTipoAviso(buscarCodigo(idTipoAviso));
		notificacion.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		bp.save(notificacion);
		return notificacion;
	}

	private String buscarCodigo(Long idTipo) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Tipificadores tipo = (Tipificadores) bp.getById(Tipificadores.class, idTipo);
		return tipo.getCodigo();
	}

	private byte[] generarReporteNotificacion(Notificacion noti) throws JRException, IOException {
		if (reporte2 == null) {
			return null;
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		StringBuilder u = new StringBuilder("http://");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		u.append("/");
		List<Director> infoBanco = bp.createQuery("Select d from Director d where d.codigo like :codigo")
				.setParameter("codigo", "NOTIFICACION.%").list();
		String domicilio = "";
		String sucursal = "";
		String cuenta = "";
		String banco = "";
		for (Director director : infoBanco) {
			if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.NUMEROCUENTA")) {
				cuenta = director.getValor();
			} else if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.SUCURSALBANCO")) {
				sucursal = director.getValor();
			} else if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.NOMBREBANCO")) {
				banco = director.getValor();
			} else if (director.getCodigo().equalsIgnoreCase("NOTIFICACION.DOMICILIOBANCO")) {
				domicilio = director.getValor();
			}
		}

		HashMap param = new HashMap();
		param.put("NOTIFICACION_ID", noti.getId());
		param.put("CUENTA", cuenta);
		param.put("SUCURSAL", sucursal);
		param.put("DOMICILIO", domicilio);
		param.put("BANCO", banco);
		param.put("FECHA", noti.getCredito().getFechaUltimoVencimiento());
		param.put("REPORTS_PATH", u.toString());
		param.put("SCHEMA2", bp.getSchema());

		Connection conn = bp.getCurrentSession().connection();

		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte2, param, conn);
		byte[] pdfNoti = JasperExportManager.exportReportToPdf(jasperPrint);

		return pdfNoti;
	}

	private byte[] generarReporte(Notificacion noti) throws JRException, IOException {
		byte[] pdf = null;
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Leyenda leyenda = (Leyenda) bp.createQuery("Select l from Leyenda l where l.nombre =:nombre")
				.setParameter("nombre", "Notificacion de Mora").uniqueResult();

		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		u.append("/");

		if (emitir) {
			URL url = new URL(u.toString() + "reports/boletoTotalDeuda.jasper");
			reporte = (JasperReport) JRLoader.loadObject(url.openStream());
			HashMap param = new HashMap();
			param.put("LEYENDA", leyenda != null ? leyenda.getTexto() : "");
			param.put("VENCIMIENTO", DateHelper.getString(noti.getFechaVenc()));
			param.put("OBJETOI_ID", noti.getCredito().getId().toString());
			param.put("ID_PERSONA", noti.getCredito().getPersona_id().toString());
			// param.put("MONEDA", "Peso Argentino");
			param.put("USUARIO", SessionHandler.getCurrentSessionHandler().getCurrentUser());
			param.put("REPORTS_PATH", u.toString());
			param.put("SCHEMA", bp.getSchema());
			Connection conn = bp.getCurrentSession().connection();
			JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, param, conn);
			pdf = JasperExportManager.exportReportToPdf(jasperPrint);
		}

		String stringNotificacion = "";
		if (buscarCodigo(idTipoAviso).equalsIgnoreCase("primer aviso")) {
			stringNotificacion = "reports/NotificacionPrimerAviso.jasper";
		} else if (buscarCodigo(idTipoAviso).equalsIgnoreCase("segundo aviso")) {
			stringNotificacion = "reports/NotificacionSegundoAviso.jasper";
		} else if (buscarCodigo(idTipoAviso).equalsIgnoreCase("tercer aviso")) {
			stringNotificacion = "reports/NotificacionTercerAviso.jasper";
		} else if (buscarCodigo(idTipoAviso).equalsIgnoreCase("ultimo aviso")) {
			stringNotificacion = "reports/NotificacionCorreo.jasper";
		}

		if (stringNotificacion != null && !stringNotificacion.isEmpty()) {
			URL url2 = new URL(u.toString() + stringNotificacion);
			reporte2 = (JasperReport) JRLoader.loadObject(url2.openStream());
		}
		return pdf;
	}

	private void actualizarSeleccion() {
		creditosSeleccionados = new ArrayList<Objetoi>();
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();

		// String codigo = request.getParameter("process.codigo");

		Enumeration<String> paramNames = request.getParameterNames();

		HashSet<String> seleccionados = new HashSet<String>();

		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();

			if (param.startsWith("notificacion")) {
				String[] p = param.split("-");
				seleccionados.add(new String(p[1]));
			}
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		creditos = (List<Objetoi>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("NotificacionList.credito");
		for (Objetoi creditoSel : creditos) {
			for (String id : seleccionados)
				if (id.equalsIgnoreCase(creditoSel.getNumeroAtencion().toString())) {
					creditosSeleccionados.add(creditoSel);
				}
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("NotificacionList.creditosSel",
				creditosSeleccionados);
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		if (creditos == null) {
			this.creditos = null;
		} else {
			this.creditos = creditos;
		}
	}

	public List<Notificacion> getNotificaciones() {
		return notificaciones;
	}

	public void setNotificaciones(List<Notificacion> notificaciones) {
		this.notificaciones = notificaciones;
	}

	public List<Objetoi> getCreditosSeleccionados() {
		return creditosSeleccionados;
	}

	public void setCreditosSeleccionados(List<Objetoi> creditosSeleccionados) {
		this.creditosSeleccionados = creditosSeleccionados;
	}

	public Notificacion getNotificacion() {
		return notificacion;
	}

	public void setNotificacion(Notificacion notificacion) {
		this.notificacion = notificacion;
	}

	public Date getFechaCreada() {
		return fechaCreada;
	}

	public void setFechaCreada(Date fechaCreada) {
		this.fechaCreada = fechaCreada;
	}

	public String getFechaCreadaStr() {
		return DateHelper.getString(fechaCreada);
	}

	public void setFechaCreadaStr(String fechaCreada) {
		this.fechaCreada = DateHelper.getDate(fechaCreada);
	}

	public Date getFechaVenc() {
		return fechaVenc;
	}

	public void setFechaVenc(Date fechaVenc) {
		this.fechaVenc = fechaVenc;
	}

	public String getFechaVencStr() {
		return DateHelper.getString(fechaVenc);
	}

	public void setFechaVencStr(String fechaVenc) {
		this.fechaVenc = DateHelper.getDate(fechaVenc);
	}

	public boolean isTerceros() {
		return terceros;
	}

	public void setTerceros(boolean terceros) {
		this.terceros = terceros;
	}

	public boolean isMail() {
		return mail;
	}

	public void setMail(boolean mail) {
		this.mail = mail;
	}

	public boolean isEmitir() {
		return emitir;
	}

	public void setEmitir(boolean emitir) {
		this.emitir = emitir;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getIdTipoAviso() {
		return idTipoAviso;
	}

	public void setIdTipoAviso(Long idTipoAviso) {
		this.idTipoAviso = idTipoAviso;
	}

	public Long getIdTipoNotificacion() {
		return idTipoNotificacion;
	}

	public void setIdTipoNotificacion(Long idTipoNotificacion) {
		this.idTipoNotificacion = idTipoNotificacion;
	}

	public Tipificadores getTipoAviso() {
		return tipoAviso;
	}

	public void setTipoAviso(Tipificadores tipoAviso) {
		this.tipoAviso = tipoAviso;
	}

	public boolean isFiador() {
		return fiador;
	}

	public void setFiador(boolean fiador) {
		this.fiador = fiador;
	}

	public Long getIdConf() {
		return idConf;
	}

	public void setIdConf(Long idConf) {
		this.idConf = idConf;
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public JasperReport getReporte() {
		return reporte;
	}

	public void setReporte(JasperReport reporte) {
		this.reporte = reporte;
	}

	public List<JasperReport> getReportes() {
		return reportes;
	}

	public void setReportes(List<JasperReport> reportes) {
		this.reportes = reportes;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	/*
	 * public String getEstado() { return estado; }
	 * 
	 * public void setEstado(String estado) { this.estado = estado; }
	 */
	public List<BeanNotificacion> getCreditosBean() {
		return creditosBean;
	}

	public void setCreditosBean(List<BeanNotificacion> creditosBean) {
		this.creditosBean = creditosBean;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long idObjetoi) {
		this.numeroAtencion = idObjetoi;
	}

	public void setFechaMora(String fechaMora) {
		this.fechaMora = DateHelper.getDate(fechaMora);
	}

	public String getFechaMora() {
		return DateHelper.getString(fechaMora);
	}

	public String[] getIdsLineas() {
		return idsLineas;
	}

	public void setIdsLineas(String[] idsLineas) {
		this.idsLineas = idsLineas;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String getUrlGaf() {
		return DirectorHelper.getString("URL.GAF");
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public List<EstadoNotificacionDTO> getListaEstadoNotificacionDTO() {
		return listaEstadoNotificacionDTO;
	}

	public void setListaEstadoNotificacionDTO(List<EstadoNotificacionDTO> listaEstadoNotificacionDTO) {
		this.listaEstadoNotificacionDTO = listaEstadoNotificacionDTO;
	}

}
