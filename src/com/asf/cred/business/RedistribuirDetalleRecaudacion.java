package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;

import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.CuentaBancaria;
import com.asf.gaf.hibernate.Mediopago;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.Objetoi;

public class RedistribuirDetalleRecaudacion implements IProcess{
	
	private String forward;
	private HashMap<String, String> errores;
	private BusinessPersistance bp;
	private String accion;
	private Long idDetalleArchivo;
	private String boletosXML;
	private DetalleRecaudacion detalleRecaudacion;
	private Objetoi objetoi;
	private Long idcaratula;
	private Double importeFinal;
	private Objetoi proyectoDestino;
	private Double importeAsignado;
	private double deudaActual;
	private String forwardURL;
	private double deudaVencida;
	private double deudaTotal;
	private double saldoVencida;
	private double saldoTotal;

	public RedistribuirDetalleRecaudacion() {
		forward = "RedistribuirDetalleRecaudacion";
		errores = new HashMap<String, String>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		objetoi = new Objetoi();
		detalleRecaudacion = new DetalleRecaudacion();
		proyectoDestino = new Objetoi();
	}
	
	@Override
	public boolean doProcess() {
		if (accion == null) {
			
		} else if(accion.equals("cargar")) {
			cargar();
		} else if(accion.equals("cargarProyectoDestino")) {
			cargar();
			cargarProyectoDestino();
		} else if (accion.equals("guardar")) {
			cargar();
			guardar();
		}
		return errores.isEmpty();
	}
	
	public void cargar(){
		detalleRecaudacion = (DetalleRecaudacion)bp.getById(DetalleRecaudacion.class, detalleRecaudacion.getId());
		objetoi = detalleRecaudacion.getObjetoi();
		importeFinal = detalleRecaudacion.getImporte();
		
		Date fechaCalculo = detalleRecaudacion.getFechaAcreditacionValor();
		if (fechaCalculo == null) {
			fechaCalculo = detalleRecaudacion.getFechaRecaudacion();
		}
		if (fechaCalculo != null) {
			CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(objetoi.getId(), fechaCalculo);
			
			calculo.calcular();
			deudaVencida = calculo.getDeudaVencidaFechaEstricta();
			deudaTotal = calculo.getDeudaTotal();
			saldoVencida = deudaVencida - detalleRecaudacion.getImporte();
			saldoTotal = deudaTotal - detalleRecaudacion.getImporte();
			
			if (saldoVencida > 0) {
				saldoVencida = 0;
			} else {
				saldoVencida = Math.abs(saldoVencida);
			}
			
			if (saldoTotal > 0) {
				saldoTotal = 0;
			} else {
				saldoTotal = Math.abs(saldoTotal);
			}
		}
	}

	public void cargarProyectoDestino(){
		proyectoDestino = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion").setParameter("nroAtencion", proyectoDestino.getNumeroAtencion()).uniqueResult();

		// calcular deuda proyecto destino
		CreditoCalculoDeuda c = new CreditoCalculoDeuda(proyectoDestino.getId(), detalleRecaudacion.getFechaAcreditacionValor());
		c.calcular();

		deudaActual = c.getDeudaCapitalParcial();
	}
	
	public void guardar() {
		if (importeAsignado > detalleRecaudacion.getImporte()) {
			errores.put("cobranza.redistribucion.importeOriginal", "cobranza.redistribucion.importeOriginal");
			return;
		}

		proyectoDestino = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion").setParameter("nroAtencion", proyectoDestino.getNumeroAtencion()).uniqueResult();
		
		CreditoCalculoDeuda c = new CreditoCalculoDeuda(proyectoDestino.getId(), detalleRecaudacion.getFechaAcreditacionValor());
		c.calcular();

		deudaActual = c.getDeudaTotal();
		
		if (importeAsignado > deudaActual) {
			errores.put("cobranza.redistribucion.importeDeuda", "cobranza.redistribucion.importeDeuda");
			return;
		}
		
		DetalleRecaudacion detalle = new DetalleRecaudacion();
		detalle.setFechaAcreditacionValor(detalleRecaudacion.getFechaAcreditacionValor());
		detalle.setDetalleArchivo(detalleRecaudacion.getDetalleArchivo());
		detalle.setImporte(importeAsignado);
		detalle.setOrigen(DetalleRecaudacion.ORIGEN_MANUAL);
		detalle.setTempRecaudacion(detalleRecaudacion.getTempRecaudacion());
		detalle.setNumeroProyecto(proyectoDestino.getNumeroAtencion());
		detalle.setCaratula(detalleRecaudacion.getCaratula());
		detalle.setFechaRecaudacion(detalleRecaudacion.getFechaRecaudacion());
		detalle.setMoneda(detalleRecaudacion.getMoneda());
		detalle.setTipoValor(detalleRecaudacion.getTipoValor());
		detalle.setCodigoMovimiento(detalleRecaudacion.getCodigoMovimiento());
		detalle.setDetallePadre(detalleRecaudacion);
		detalle.setNumeroValor(detalleRecaudacion.getNumeroValor());
		detalle.setNumeroOperacion(detalleRecaudacion.getNumeroOperacion());
		
		if (detalleRecaudacion.getMedioPago() != null && detalleRecaudacion.getMedioPago().getId() != 0) {
			Mediopago mp = (Mediopago) bp.getById(Mediopago.class, detalleRecaudacion.getMedioPago().getId());
			detalle.setMedioPago(mp);
		}
		
		if (detalleRecaudacion.getBanco() != null && detalleRecaudacion.getBanco().getId() != 0) {
			Bancos banco = (Bancos) bp.getById(Bancos.class, detalleRecaudacion.getBanco().getId());
			detalle.setBanco(banco);
		}
		if (detalleRecaudacion.getCuentaBancaria() != null && detalleRecaudacion.getCuentaBancaria().getId() != 0) {
			CuentaBancaria cb = (CuentaBancaria) bp.getById(CuentaBancaria.class, detalleRecaudacion.getCuentaBancaria().getId());
			detalle.setCuentaBancaria(cb);
		}
		
		Recaudacion rec = (Recaudacion) bp.getById(Recaudacion.class, detalleRecaudacion.getTempRecaudacion().getId());
		detalle.setTempRecaudacion(rec);
		
		detalleRecaudacion.setImporte(detalleRecaudacion.getImporte() - importeAsignado);
		bp.save(detalle);
		bp.save(detalleRecaudacion);
		
		forwardURL = "/actions/process.do?do=process&processName=PreaplicacionPagos" +
				"&process.idcaratula="+idcaratula;
		forward = "ProcessRedirect";
	}
	
	@Override
	public HashMap<String, String> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}
	
	public String getAccion() {
		return accion;
	}
	
	public void setAccion(String accion) {
		this.accion = accion;
	}
	
	public Long getIdDetalleArchivo() {
		return idDetalleArchivo;
	}
	
	public void setIdDetalleArchivo(Long idDetalleArchivo) {
		this.idDetalleArchivo = idDetalleArchivo;
	}
	
	public String getBoletosXML() {
		return boletosXML;
	}
	
	public void setBoletosXML(String boletosXML) {
		this.boletosXML = boletosXML;
	}

	public DetalleRecaudacion getDetalleRecaudacion() {
		return detalleRecaudacion;
	}

	public void setDetalleRecaudacion(DetalleRecaudacion detalleRecaudacion) {
		this.detalleRecaudacion = detalleRecaudacion;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Long getIdcaratula() {
		return idcaratula;
	}

	public void setIdcaratula(Long idCaratula) {
		this.idcaratula = idCaratula;
	}

	public Double getImporteFinal() {
		return importeFinal;
	}

	public void setImporteFinal(Double importeFinal) {
		this.importeFinal = importeFinal;
	}

	public Objetoi getProyectoDestino() {
		return proyectoDestino;
	}

	public void setProyectoDestino(Objetoi proyectoDestino) {
		this.proyectoDestino = proyectoDestino;
	}

	public Double getImporteAsignado() {
		return importeAsignado;
	}

	public void setImporteAsignado(Double importeAsignado) {
		this.importeAsignado = importeAsignado;
	}

	public double getDeudaActual() {
		return deudaActual;
	}

	public void setDeudaActual(double deudaActual) {
		this.deudaActual = deudaActual;
	}

	public String getDeudaActualStr() {
		return String.format("%,.2f", deudaActual);
	}
	
	public void setDeudaActualStr(String deuda) {}
	
	public String getForwardURL() {
		return forwardURL;
	}

	public String getDeudaVencida() {
		return String.format("%,.2f", deudaVencida);
	}

	public String getDeudaTotal() {
		return String.format("%,.2f", deudaTotal);
	}

	public String getSaldoVencida() {
		return String.format("%,.2f", saldoVencida);
	}

	public String getSaldoTotal() {
		return String.format("%,.2f", saldoTotal);
	}

}
