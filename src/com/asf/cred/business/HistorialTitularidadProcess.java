package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.bpm.DocumentoHelper;
import com.nirven.creditos.hibernate.HistorialTitularidad;
import com.nirven.creditos.hibernate.Objetoi;

public class HistorialTitularidadProcess implements IProcess{

	private HashMap<String, Object> errores;
	private Long idSolicitud;
	private List<HistorialTitularidad> historialList;
	private HistorialTitularidad historialTitularidad;
	private String fechaHastaStr;
	private String numeroResolucionCambio;
	private Long idTitularNuevo;
	private String observaciones;
	private String action;
	private String forward;
	
	public HistorialTitularidadProcess() {
		this.historialTitularidad = new HistorialTitularidad();
	}
	
	public boolean doProcess() {
		boolean retorno = true;
		if (action.equalsIgnoreCase("list")) {
			retorno = this.listarHistorialTitularidad();
		} else if (action.equalsIgnoreCase("new")) {
			this.forward = "HistorialTitularidad";
		} else if (action.equalsIgnoreCase("save")) {
			retorno = this.guardarNuevaTitularidad();
		} else if (action.equalsIgnoreCase("load")) {
			// retorno = this.cargarRegistro();
		} else {
			retorno = this.listarHistorialTitularidad();
		}
		return retorno;
	}

	private boolean guardarNuevaTitularidad() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		bp.begin();
		Objetoi solicitud;
		Persona viejoTitular;
		Persona nuevoTitular;
		if(errores==null){
			errores = new HashMap<String, Object>();
		}
		try {
			solicitud = (Objetoi) bp.getById(Objetoi.class, this.idSolicitud);
		} catch (Exception e) {
			this.errores.put("historialTitularidad.error", 
					new ActionMessage("historialTitularidad.error","Error al buscar la solicitud"));
			e.printStackTrace();
			bp.rollback();
			return false;
		}
		try {
			viejoTitular = solicitud.getPersona();
			this.historialTitularidad.setTitularAnterior(viejoTitular);
			nuevoTitular = (Persona) bp.getById(Persona.class, this.idTitularNuevo);
		} catch (Exception e) {
			this.errores.put("historialTitularidad.error", 
					new ActionMessage("historialTitularidad.error","Error al buscar al nuevo titular"));
			e.printStackTrace();
			bp.rollback();
			return false;
		}
		solicitud.setPersona(nuevoTitular);
		try {
			if(historialTitularidad.getFechaHasta()==null){
				historialTitularidad.setFechaHasta(new Date());
			}
			historialTitularidad.setSolicitud(solicitud);
			historialTitularidad.setResponsable(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			bp.saveOrUpdate(historialTitularidad);
			bp.saveOrUpdate(solicitud);
			DocumentoHelper docHelper=new DocumentoHelper();
			String respuesta=docHelper.modificarIniciador(solicitud.getExpediente(), nuevoTitular.getIdpersona().toString(), nuevoTitular.getNomb12(), nuevoTitular.getDomicilioCompleto());
			if(respuesta!=null&&respuesta.equals(solicitud.getExpediente())){
				bp.commit();
			}else{	
				this.errores.put("historialTitularidad.errorADE", 
						new ActionMessage("historialTitularidad.error","Error al actualizar Titular en ADE. Revise los logs."));
				this.errores.put("historialTitularidad.errores", 
						new ActionMessage("historialTitularidad.error",respuesta));
				throw new Exception();
			}
			
		} catch (Exception e) {
			this.errores.put("historialTitularidad.error", 
					new ActionMessage("historialTitularidad.error","Error al guardar los cambios. Revise los logs."));
			e.printStackTrace();
			bp.rollback();
			
			return false;
		}
		
		
		
		this.listarHistorialTitularidad();
		return true;
						
	}

	@SuppressWarnings("unchecked")
	private boolean listarHistorialTitularidad() {
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Query query = bp.createQuery("SELECT h FROM HistorialTitularidad h " +
					"WHERE h.solicitud.id = :idsolicitud ORDER BY h.fechaHasta DESC h.id ASC");
			query.setParameter("idsolicitud", idSolicitud);
			this.historialList = (ArrayList<HistorialTitularidad>) query.list();
			this.forward = "HistorialTitularidadList";
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return "HistorialTitularidadList";
	}

	public Object getResult() {
		return this.historialList;
	}

	public boolean validate() {
		return true;
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public List<HistorialTitularidad> getHistorialList() {
		return historialList;
	}

	public void setHistorialList(List<HistorialTitularidad> historialList) {
		this.historialList = historialList;
	}

	public HistorialTitularidad getHistorialTitularidad() {
		return historialTitularidad;
	}

	public void setHistorialTitularidad(HistorialTitularidad historialTitularidad) {
		this.historialTitularidad = historialTitularidad;
	}

	public Long getIdTitularNuevo() {
		return idTitularNuevo;
	}

	public void setIdTitularNuevo(Long idTitularNuevo) {
		this.idTitularNuevo = idTitularNuevo;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getFechaHastaStr() {
		return fechaHastaStr;
	}

	public void setFechaHastaStr(String fechaHastaStr) {
		this.fechaHastaStr = fechaHastaStr;
	}

	public String getNumeroResolucionCambio() {
		return numeroResolucionCambio;
	}

	public void setNumeroResolucionCambio(String numeroResolucionCambio) {
		this.numeroResolucionCambio = numeroResolucionCambio;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
}
