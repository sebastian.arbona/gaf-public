package com.asf.cred.business;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.asf.struts.action.IReportBean;
import com.nirven.creditos.hibernate.HectareasCultivadas;

public class HectareasCultivadasBean implements IReportBean {

	private List<HectareasCultivadas> hectareasCultivadas;
	
	
	HectareasCultivadasBean(List<HectareasCultivadas> hectareasCultivadas){
		this.hectareasCultivadas = hectareasCultivadas;
	}
	
	@Override
	public Collection getCollection(HashMap hsParametros) {
		return this.hectareasCultivadas;
	}

}
