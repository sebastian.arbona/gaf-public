/**
 * 
 */
package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.cred.beans.BeanDesembolso;
import com.civitas.cred.metodos.MetodoCalculoAleman;

/**
 * @author cnoguerol
 */
public class Simulador implements IProcess {
    // =========================ATRIBUTOS===========================
    private String forward = "Simulador";
    private HashMap<String, Object> errores;
    private String accion = "";
    private Double tasaAmortizacion;
    private String metodoAmortizacion;
    private Integer plazoCapital;
    private Integer plazoCompensatorio;
    private Integer periodicidadCapital;
    private Integer periodicidadCompensatorio;
    private ArrayList<BeanCuota> cuotas = new ArrayList<BeanCuota>();
    private Date primerVencimientoCapital;
    private Date primerVencimientoCompensatorio;
    private Date segundoVencimientoCapital;
    private Date segundoVencimientoCompensatorio;
    private Date fecha1;
    private Date fecha2;
    private Date fecha3;
    private Date fecha4;
    private Date fecha5;
    private Date fecha6;
    private Double importe1;
    private Double importe2;
    private Double importe3;
    private Double importe4;
    private Double importe5;
    private Double importe6;

    // =========================CONSTRUCTOR=========================
    public Simulador() {
        this.errores = new HashMap<String, Object>();
        this.limpiarVariables();
    }

    // ======================FUNCIONALIDADES========================
    @Override
    public boolean doProcess() {
        if (this.accion.equalsIgnoreCase("cancelar")) {
            this.limpiarVariables();
        } else if (this.accion.equalsIgnoreCase("calcular")) {
            this.calcular();
        }
        return this.errores.isEmpty();
    }

    @Override
    public HashMap<String, Object> getErrors() {
        return errores;
    }

    @Override
    public String getForward() {
        return this.forward;
    }

    @Override
    public String getInput() {
        return this.forward;
    }

    @Override
    public Object getResult() {
        return null;
    }

    // =====================UTILIDADES PRIVADAS=====================
    private void limpiarVariables() {
        this.tasaAmortizacion = 0D;
        this.metodoAmortizacion = "A";
        this.plazoCapital = 0;
        this.plazoCompensatorio = 0;
        this.periodicidadCompensatorio = 0;
        this.periodicidadCapital = 0;
        this.primerVencimientoCapital = null;
        this.primerVencimientoCompensatorio = null;
        this.fecha1 = null;
        this.fecha2 = null;
        this.fecha3 = null;
        this.fecha4 = null;
        this.fecha5 = null;
        this.fecha6 = null;
        this.importe1 = 0D;
        this.importe2 = 0D;
        this.importe3 = 0D;
        this.importe4 = 0D;
        this.importe5 = 0D;
        this.importe6 = 0D;
        this.cuotas = new ArrayList<BeanCuota>();
    }

    private void calcular() {
    	double[] bonificaciones = new double[this.plazoCompensatorio];
        BeanCalculo beanCalculo = new BeanCalculo(this.primerVencimientoCapital, this.primerVencimientoCompensatorio,
                                                  this.tasaAmortizacion, bonificaciones, this.plazoCapital, this.plazoCompensatorio,
                                                  this.periodicidadCapital, this.periodicidadCompensatorio, this.armarDesembolsos());
        
        beanCalculo.setSegundoVencimientoCapital(segundoVencimientoCapital);
        beanCalculo.setSegundoVencimientoCompensatorio(segundoVencimientoCompensatorio);
        
        if (this.validate()) {
            if (metodoAmortizacion.equalsIgnoreCase("A")) {
                MetodoCalculoAleman metodo = new MetodoCalculoAleman();
                this.cuotas = metodo.amortizar(beanCalculo);
            } else if (metodoAmortizacion.equalsIgnoreCase("F")) {
                // MetodoCalculoFrances metodo = new MetodoCalculoFrances();
                // this.cuotas = metodo.amortizar(this.getDesembolsos(), this.tasaAmortizacion, this.plazoAmortizacion, this.prestamo, this.periodicidadCuotas, this.plazoGracia);
            } else if (metodoAmortizacion.equalsIgnoreCase("D")) {
                // MetodoCalculoDirecto metodo = new MetodoCalculoDirecto();
                // this.cuotas = metodo.amortizar(this.getDesembolsos(), this.tasaAmortizacion, this.plazoAmortizacion, this.prestamo, this.periodicidadCuotas, this.plazoGracia);
            }
        }
    }

    public ArrayList<BeanDesembolso> armarDesembolsos() {
        ArrayList<BeanDesembolso> desembolsos = new ArrayList<BeanDesembolso>();
        BeanDesembolso desembolso = new BeanDesembolso();
        if (this.fecha1 != null && this.importe1 != 0D) {
            desembolso = new BeanDesembolso();
            desembolso.setNumero(1);
            desembolso.setFecha(this.fecha1);
            desembolso.setImporte(this.importe1);
            desembolsos.add(desembolso);
            if (this.fecha2 != null && this.importe2 != 0D) {
                desembolso = new BeanDesembolso();
                desembolso.setNumero(2);
                desembolso.setFecha(this.fecha2);
                desembolso.setImporte(this.importe2);
                desembolsos.add(desembolso);
                if (this.fecha3 != null && this.importe3 != 0D) {
                    desembolso = new BeanDesembolso();
                    desembolso.setNumero(3);
                    desembolso.setFecha(this.fecha3);
                    desembolso.setImporte(this.importe3);
                    desembolsos.add(desembolso);
                    if (this.fecha4 != null && this.importe4 != 0D) {
                        desembolso = new BeanDesembolso();
                        desembolso.setNumero(4);
                        desembolso.setFecha(this.fecha4);
                        desembolso.setImporte(this.importe4);
                        desembolsos.add(desembolso);
                        if (this.fecha5 != null && this.importe5 != 0D) {
                            desembolso = new BeanDesembolso();
                            desembolso.setNumero(5);
                            desembolso.setFecha(this.fecha5);
                            desembolso.setImporte(this.importe5);
                            desembolsos.add(desembolso);
                            if (this.fecha6 != null && this.importe6 != 0D) {
                                desembolso = new BeanDesembolso();
                                desembolso.setNumero(6);
                                desembolso.setFecha(this.fecha6);
                                desembolso.setImporte(this.importe6);
                                desembolsos.add(desembolso);
                            }
                        }
                    }
                }
            }
        }
        return desembolsos;
    }

    // ======================GETTERS Y SETTERS======================

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Double getTasaAmortizacion() {
        return tasaAmortizacion;
    }

    public void setTasaAmortizacion(Double tasaAmortizacion) {
        this.tasaAmortizacion = tasaAmortizacion;
    }

    public String getMetodoAmortizacion() {
        return metodoAmortizacion;
    }

    public void setMetodoAmortizacion(String metodoAmortizacion) {
        this.metodoAmortizacion = metodoAmortizacion;
    }

    public Integer getPlazoCapital() {
        return plazoCapital;
    }

    public void setPlazoCapital(Integer plazoCapital) {
        this.plazoCapital = plazoCapital;
    }

    public Integer getPlazoCompensatorio() {
        return plazoCompensatorio;
    }

    public void setPlazoCompensatorio(Integer plazoCompensatorio) {
        this.plazoCompensatorio = plazoCompensatorio;
    }

    public Integer getPeriodicidadCapital() {
        return periodicidadCapital;
    }

    public void setPeriodicidadCapital(Integer periodicidadCapital) {
        this.periodicidadCapital = periodicidadCapital;
    }

    public Integer getPeriodicidadCompensatorio() {
        return periodicidadCompensatorio;
    }

    public void setPeriodicidadCompensatorio(Integer periodicidadCompensatorio) {
        this.periodicidadCompensatorio = periodicidadCompensatorio;
    }

    public Date getPrimerVencimientoCapital() {
        return primerVencimientoCapital;
    }

    public void setPrimerVencimientoCapital(Date primerVencimientoCapital) {
        this.primerVencimientoCapital = primerVencimientoCapital;
    }

    public Date getPrimerVencimientoCompensatorio() {
        return primerVencimientoCompensatorio;
    }

    public void setPrimerVencimientoCompensatorio(Date primerVencimientoCompensatorio) {
        this.primerVencimientoCompensatorio = primerVencimientoCompensatorio;
    }

    public ArrayList<BeanCuota> getCuotas() {
        return cuotas;
    }

    public void setCuotas(ArrayList<BeanCuota> cuotas) {
        this.cuotas = cuotas;
    }

    public Date getFecha1() {
        return fecha1;
    }

    public void setFecha1(Date fecha1) {
        this.fecha1 = fecha1;
    }

    public Date getFecha2() {
        return fecha2;
    }

    public void setFecha2(Date fecha2) {
        this.fecha2 = fecha2;
    }

    public Date getFecha3() {
        return fecha3;
    }

    public void setFecha3(Date fecha3) {
        this.fecha3 = fecha3;
    }

    public Date getFecha4() {
        return fecha4;
    }

    public void setFecha4(Date fecha4) {
        this.fecha4 = fecha4;
    }

    public Date getFecha5() {
        return fecha5;
    }

    public void setFecha5(Date fecha5) {
        this.fecha5 = fecha5;
    }

    public Date getFecha6() {
        return fecha6;
    }

    public void setFecha6(Date fecha6) {
        this.fecha6 = fecha6;
    }

    public Double getImporte1() {
        return importe1;
    }

    public void setImporte1(Double importe1) {
        this.importe1 = importe1;
    }

    public Double getImporte2() {
        return importe2;
    }

    public void setImporte2(Double importe2) {
        this.importe2 = importe2;
    }

    public Double getImporte3() {
        return importe3;
    }

    public void setImporte3(Double importe3) {
        this.importe3 = importe3;
    }

    public Double getImporte4() {
        return importe4;
    }

    public void setImporte4(Double importe4) {
        this.importe4 = importe4;
    }

    public Double getImporte5() {
        return importe5;
    }

    public void setImporte5(Double importe5) {
        this.importe5 = importe5;
    }

    public Double getImporte6() {
        return importe6;
    }

    public void setImporte6(Double importe6) {
        this.importe6 = importe6;
    }
    
    public Date getSegundoVencimientoCapital() {
		return segundoVencimientoCapital;
	}

	public void setSegundoVencimientoCapital(Date segundoVencimientoCapital) {
		this.segundoVencimientoCapital = segundoVencimientoCapital;
	}

	public Date getSegundoVencimientoCompensatorio() {
		return segundoVencimientoCompensatorio;
	}

	public void setSegundoVencimientoCompensatorio(Date segundoVencimientoCompensatorio) {
		this.segundoVencimientoCompensatorio = segundoVencimientoCompensatorio;
	}

	public String getSegundoVencimientoCapitalStr() {
		return DateHelper.getString(segundoVencimientoCapital);
	}
	
	public String getSegundoVencimientoCompensatorioStr() {
		return DateHelper.getString(segundoVencimientoCompensatorio);
	}
	
	public void setSegundoVencimientoCapitalStr(String fecha) {
		if (fecha == null || fecha.length() == 0) {
            this.setSegundoVencimientoCapital(null);
            return;
        }
		this.segundoVencimientoCapital = DateHelper.getDate(fecha);
	}
	
	public void setSegundoVencimientoCompensatorioStr(String fecha) {
		if (fecha == null || fecha.length() == 0) {
            this.setSegundoVencimientoCompensatorio(null);
            return;
        }
		this.segundoVencimientoCompensatorio = DateHelper.getDate(fecha);
	}
	
	public String getPrimerVencimientoCapitalStr() {
        return DateHelper.getString(this.getPrimerVencimientoCapital());
    }

    public void setPrimerVencimientoCapitalStr(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setPrimerVencimientoCapital(null);
            return;
        }
        this.setPrimerVencimientoCapital(DateHelper.getDate(fecha));
    }

    public String getPrimerVencimientoCompensatorioStr() {
        return DateHelper.getString(this.getPrimerVencimientoCompensatorio());
    }

    public void setPrimerVencimientoCompensatorioStr(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setPrimerVencimientoCompensatorio(null);
            return;
        }
        this.setPrimerVencimientoCompensatorio(DateHelper.getDate(fecha));
    }

    public String getFecha1Str() {
        return DateHelper.getString(this.getFecha1());
    }

    public void setFecha1Str(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setFecha1(null);
            return;
        }
        this.setFecha1(DateHelper.getDate(fecha));
    }

    public String getFecha2Str() {
        return DateHelper.getString(this.getFecha2());
    }

    public void setFecha2Str(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setFecha2(null);
            return;
        }
        this.setFecha2(DateHelper.getDate(fecha));
    }

    public String getFecha3Str() {
        return DateHelper.getString(this.getFecha3());
    }

    public void setFecha3Str(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setFecha3(null);
            return;
        }
        this.setFecha3(DateHelper.getDate(fecha));
    }

    public String getFecha4Str() {
        return DateHelper.getString(this.getFecha4());
    }

    public void setFecha4Str(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setFecha4(null);
            return;
        }
        this.setFecha4(DateHelper.getDate(fecha));
    }

    public String getFecha5Str() {
        return DateHelper.getString(this.getFecha5());
    }

    public void setFecha5Str(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setFecha5(null);
            return;
        }
        this.setFecha5(DateHelper.getDate(fecha));
    }

    public String getFecha6Str() {
        return DateHelper.getString(this.getFecha6());
    }

    public void setFecha6Str(String fecha) {
        if (fecha == null || fecha.length() == 0) {
            this.setFecha6(null);
            return;
        }
        this.setFecha6(DateHelper.getDate(fecha));
    }

    // ========================VALIDACIONES=========================
    @Override
    public boolean validate() {
        return this.errores.isEmpty();
    }
}
