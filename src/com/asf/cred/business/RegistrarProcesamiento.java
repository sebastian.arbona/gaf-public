package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.struts.form.AbmFormCaratula;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Cobropago;

public class RegistrarProcesamiento implements IProcess {

	@SuppressWarnings("rawtypes")
	private List lResult = new ArrayList();
	private BusinessPersistance bp = null;
	private Caratula caratula;
	private Long idcaratula; // Codigo de caratula seleccionada..
	private Date fenv12; // Fecha de envio.
	private Long codi12; // Codigo de grupo.
	private Long tcomp12 = new Long(0); // Total de comprobantes.
	private String detaba; // Detalle del banco.
	private String fcob12; // Fecha de cobro.
	private Double timp12 = new Double(0); // Importe total.
	private String[] lista = new String[] {}; // Lista de boletos.

	/* Formulario CobroPago */
	private Long idmediopago; // Codigo del medio de pago seleccionado..
	private String entidademisora; // Descripción de la entidad emisora.
	private String nrocomprobante; // Detalle del nro. de comprobante.
	private String plan; // Detalle del plan.
	private String autorizacion; // Datos de autorización.
	private Date femision; // Fecha de emision.
	private Date fvencimiento; // Fecha de vencimiento.
	private String tipomedio; // Tipo de medio de pago.
	private Long cuotas, nrolote;
	private Double importe; // Importe pagado.
	private StrutsArray cobroPagos = new StrutsArray(Cobropago.class);
	private boolean save;
	@SuppressWarnings("rawtypes")
	private List boletos = new ArrayList();
	private String lisb[];
	private Double impoTotal = 0D;
	private Double aCobrar = 0D; // Total a Cobrar..
	private Double difTotal = 0D; // Diferencia entre el total cobrado y el total de los boletos..

	// private Query qBoleto;
	private Long lote;

	public RegistrarProcesamiento() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean doProcess() {
		// Estado del mediocobro = 1 -> Informado.
		// qBoleto = bp.createQuery("FROM Boletomediocobro b where b.id.codi09 = ? and
		// b.id.digi09 = ? and b.id.peri09 = ? and b.Mediocobro.fechapago is null and
		// b.Mediocobro.idcaratula is null and b.Mediocobro.estado = 1");

		if (idcaratula != null) {
			caratula = (Caratula) bp.getById(Caratula.class, idcaratula);
			codi12 = caratula.getGrupo();
			detaba = (caratula.getBanco() != null) ? caratula.getBanco().getDetaBa() : "";
			fcob12 = DateHelper.getString(caratula.getFechaCobranza());

		}

		if (isSave()) {
			bp.begin();

			try {
				List lstTemp = new ArrayList();
				for (int i = 0; i < lista.length; i++) {
					lstTemp.add(lista[i]);
				}
				List<String> lstAgregados = new ArrayList<String>();

				String[] oBoletos = new AbmFormCaratula().getBoletos();
				boolean bExiste = false;
				for (int i = 0; i < oBoletos.length; i++) {
					String[] sBoleto = oBoletos[i].split("\\$");
					bExiste = false;

					for (String sKey : (List<String>) lstTemp) {
						if (sKey.split("\\$")[0].trim().equals(sBoleto[0].trim())) {
							bExiste = true;
							break;
						}
					}

					if (!bExiste) {
						lstAgregados.add(oBoletos[i]);
					}
				}

				lstTemp.addAll(lstAgregados);
				lista = (String[]) lstTemp.toArray(lista);

				/* Actualiza total de comprobantes e importe total de Caratula */

				// modificaciones necesarias para que funcione correctamente con los cambios del
				// ticket 273
				if (caratula.getComprobantes() == null) {
					caratula.setComprobantes(0L);
				}
				if (caratula.getImporte() == null) {
					caratula.setImporte(0D);
				}
				// Fin modificaciones ticket 273
				caratula.setComprobantes(caratula.getComprobantes() + this.tcomp12);
				caratula.setImporte(caratula.getImporte() + this.timp12);
				bp.saveOrUpdate(caratula);

				/* Genera Pago */
				for (int i = 0; i < lista.length; i++) {
					lisb = lista[i].split("-");
					// Busco el boleto por los 3 campos que anteriormente componian el boletoKey
					Boleto boleto = (Boleto) bp.createQuery(
							"Select b from Boleto b where b.numeroBoleto=:num and b.verificadorBoleto = :ver and b.periodoBoleto=:per")
							.setParameter("num", new Long(lisb[0])).setParameter("ver", new Long(lisb[1]))
							.setParameter("per", new Long(lisb[2].substring(0, 4))).uniqueResult();
					boletos.add(boleto);
					if (!lisb[2].contains("$")) {
						// Esto es para los boletos que son cargados de manera automatica
						lisb[2] += " $" + boleto.getImporte();
					}
					// el importe viene ingresado manualmente de la pantalla
					Double importeManual = new Double(lisb[2].substring(6, lisb[2].length()));
					impoTotal += importeManual; // importe Total de la lista de boletos..

					GenerarPagos generarPagos = new GenerarPagos(bp);
					generarPagos.generar(boleto, caratula, importeManual.doubleValue());
				}

				/* Registra el pago del boleto en Cobropago, Bolepago y Detallebolepago */
				RegistraCobropago rC = new RegistraCobropago(bp);
				rC.registraCobro(lista, cobroPagos);
				setLote(rC.getLote());

				bp.commit();

			} catch (Exception e) {
				bp.rollback();
				e.printStackTrace();

			}
		}

		this.tcomp12 = 0L;
		this.timp12 = 0D;
		this.lista = new String[] {};
		this.difTotal = 0D;
		this.aCobrar = 0D;

		return true;
	}

	@SuppressWarnings("rawtypes")
	public HashMap getErrors() {
		return null;
	}

	public String getForward() {
		return "RegistrarProcesamiento";
	}

	public String getInput() {
		return "RegistrarProcesamiento";
	}

	public Object getResult() {
		return this.lResult;
	}

	public boolean validate() {
		return true;
	}

	public Long getIdcaratula() {
		return idcaratula;
	}

	public void setIdcaratula(Long idcaratula) {
		this.idcaratula = idcaratula;
	}

	public Long getCodi12() {
		return codi12;
	}

	public void setCodi12(Long codi12) {
		this.codi12 = codi12;
	}

	public String getDetaba() {
		return detaba;
	}

	public void setDetaba(String detaba) {
		this.detaba = detaba;
	}

	public String getFenv12Str() {
		return DateHelper.getString(fenv12);
	}

	public void setFenv12Str(String fecha) {
		if (fecha != null && fecha.length() > 0) {
			this.fenv12 = DateHelper.getDate(fecha);
		} else {
			this.fenv12 = DateHelper.getDate("");
		}
	}

	public Date getFenv12() {
		return fenv12;
	}

	public void setFenv12(Date fenv12) {
		this.fenv12 = fenv12;
	}

	public String getFcob12() {
		return fcob12;
	}

	public void setFcob12(String fcob12) {
		this.fcob12 = fcob12;
	}

	public Long getTcomp12() {
		return tcomp12;
	}

	public void setTcomp12(Long tcomp12) {
		this.tcomp12 = tcomp12;
	}

	public Double getTimp12() {
		return timp12;
	}

	public void setTimp12(Double timp12) {
		this.timp12 = timp12;
	}

	public Long getIdmediopago() {
		return idmediopago;
	}

	public void setIdmediopago(Long idmediopago) {
		this.idmediopago = idmediopago;
	}

	public String getEntidademisora() {
		return entidademisora;
	}

	public void setEntidademisora(String entidademisora) {
		this.entidademisora = entidademisora;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public Long getCuotas() {
		return cuotas;
	}

	public void setCuotas(Long cuotas) {
		this.cuotas = cuotas;
	}

	public String getFemisionStr() {
		return DateHelper.getString(femision);
	}

	public void setFemisionStr(String fecha) {
		if (fecha != null && fecha.length() > 0) {
			this.femision = DateHelper.getDate(fecha);
		} else {
			this.femision = DateHelper.getDate("");
		}
	}

	public Date getFvencimiento() {
		return fvencimiento;
	}

	public void setFvencimiento(Date fvencimiento) {
		this.fvencimiento = fvencimiento;
	}

	public String getFvencimientoStr() {
		return DateHelper.getString(fvencimiento);
	}

	public void setFvencimientoStr(String fecha) {
		if (fecha != null && fecha.length() > 0) {
			this.fvencimiento = DateHelper.getDate(fecha);
		} else {
			this.fvencimiento = DateHelper.getDate("");
		}
	}

	public String getNrocomprobante() {
		return nrocomprobante;
	}

	public void setNrocomprobante(String nrocomprobante) {
		this.nrocomprobante = nrocomprobante;
	}

	public Long getNrolote() {
		return nrolote;
	}

	public void setNrolote(Long nrolote) {
		this.nrolote = nrolote;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public StrutsArray getCobroPagos() {
		return cobroPagos;
	}

	public void setCobroPagos(StrutsArray cobroPagos) {
		this.cobroPagos = cobroPagos;
	}

	public boolean isSave() {
		return save;
	}

	public void setSave(boolean save) {
		this.save = save;
	}

	public Caratula getCaratula() {
		return caratula;
	}

	public void setCaratula(Caratula caratula) {
		this.caratula = caratula;
	}

	public String[] getLista() {
		return this.lista;
	}

	public void setLista(String[] lista) {
		this.lista = lista;
	}

	public Double getDifTotal() {
		return difTotal;
	}

	public void setDifTotal(Double difTotal) {
		this.difTotal = difTotal;
	}

	public Double getACobrar() {
		return aCobrar;
	}

	public void setACobrar(Double cobrar) {
		aCobrar = cobrar;
	}

	public String getTipomedio() {
		return tipomedio;
	}

	public void setTipomedio(String tipomedio) {
		this.tipomedio = tipomedio;
	}

	public Long getLote() {
		return lote;
	}

	public void setLote(Long lote) {
		this.lote = lote;
	}

}
