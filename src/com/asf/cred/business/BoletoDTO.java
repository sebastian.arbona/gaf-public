package com.asf.cred.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Objetoi;

public class BoletoDTO implements Serializable, Comparable<BoletoDTO> {

	private static final long serialVersionUID = 4428294876046218911L;

	private Date fechaProceso;
	private String tipoComprobante;
	private Long numeroComprobante;
	private Long numeroAtencion;
	private String tomador;
	private String cuil;
	private String linea;
	private String moneda;
	private Double capital;
	private Double compensatorio;
	private Double moratorio;
	private Double punitorio;
	private Double bonificacion;
	private Double gastosAdministrativos;
	private Double multas;
	private Double gastosRecuperar;
	private Double total;

	public BoletoDTO() {

	}

	public BoletoDTO(Long idBoleto, Date fechaProceso) {
		this((Boleto) SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getById(Boleto.class,
				idBoleto), fechaProceso);
	}

	@SuppressWarnings("unchecked")
	public BoletoDTO(Boleto boleto, Date fechaProceso) {
		this.fechaProceso = fechaProceso;
		this.numeroComprobante = boleto.getNumeroBoleto();
		this.tipoComprobante = boleto.getTipo();
		Objetoi objetoi = boleto.getObjetoi();
		this.numeroAtencion = objetoi.getNumeroAtencion();
		this.tomador = objetoi.getPersona().getNomb12();
		this.cuil = objetoi.getPersona().getCuil12Str();
		this.linea = objetoi.getLinea().getNombre();
		this.moneda = objetoi.getLinea().getMoneda().getAbreviatura();

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ArrayList<Bolcon> detalles = (ArrayList<Bolcon>) bp
				.createQuery("SELECT b FROM Bolcon b WHERE b.id.boleto.id = :boleto")
				.setParameter("boleto", boleto.getId()).list();

		this.capital = 0.0;
		this.compensatorio = 0.0;
		this.moratorio = 0.0;
		this.punitorio = 0.0;
		this.bonificacion = 0.0;
		this.gastosAdministrativos = 0.0;
		this.multas = 0.0;
		this.gastosRecuperar = 0.0;
		this.total = 0.0;

		for (Bolcon detalle : detalles) {
			switch (detalle.getFacturado().getConcepto().getAbreviatura()) {
			case Concepto.CONCEPTO_CAPITAL:
				this.capital += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_COMPENSATORIO:
				this.compensatorio += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_MORATORIO:
				this.moratorio += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_PUNITORIO:
				this.punitorio += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_BONIFICACION:
				this.bonificacion += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_GASTOS:
				this.gastosAdministrativos += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_MULTAS:
				this.multas += detalle.getImporte();
				break;
			case Concepto.CONCEPTO_GASTOS_A_RECUPERAR:
				this.gastosRecuperar += detalle.getImporte();
				break;
			}
			this.total += detalle.getImporte();
		}
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public Long getNumeroComprobante() {
		return numeroComprobante;
	}

	public void setNumeroComprobante(Long numeroComprobante) {
		this.numeroComprobante = numeroComprobante;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long proyecto) {
		this.numeroAtencion = proyecto;
	}

	public String getTomador() {
		return tomador;
	}

	public void setTomador(String tomador) {
		this.tomador = tomador;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Double getCapital() {
		return capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

	public Double getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(Double compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Double getMoratorio() {
		return moratorio;
	}

	public void setMoratorio(Double moratorio) {
		this.moratorio = moratorio;
	}

	public Double getPunitorio() {
		return punitorio;
	}

	public void setPunitorio(Double punitorio) {
		this.punitorio = punitorio;
	}

	public Double getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(Double bonificacion) {
		this.bonificacion = bonificacion;
	}

	public Double getGastosAdministrativos() {
		return gastosAdministrativos;
	}

	public void setGastosAdministrativos(Double gastosAdministrativos) {
		this.gastosAdministrativos = gastosAdministrativos;
	}

	public Double getMultas() {
		return multas;
	}

	public void setMultas(Double multas) {
		this.multas = multas;
	}

	public Double getGastosRecuperar() {
		return gastosRecuperar;
	}

	public void setGastosRecuperar(Double gastosRecuperar) {
		this.gastosRecuperar = gastosRecuperar;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Override
	public int compareTo(BoletoDTO o) {
		int retorno = this.fechaProceso.compareTo(o.fechaProceso);
		if (retorno == 0) {
			retorno = this.numeroComprobante.compareTo(o.numeroComprobante);
		}
		return retorno;
	}

}
