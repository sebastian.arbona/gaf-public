package com.asf.cred.business;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Notificacion;

public class PanelNotificacionesProcess implements IProcess {

	private String forward = "PanelNotificacionesList";
	private String accion;
	private Object result;
	private List<Notificacion> notificaciones;
	protected HashMap<String, Object> errores = new HashMap<String, Object>();
	private Long idCredito;
	private Long idPersona;
	private Long numeroAtencion;
	private Date fechaDesde;
	private Date fechaHasta;
	private String emisor;

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return false;
		} else if (accion.equals("list")) {
			buscarNotificaciones();
			return this.errores.isEmpty();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void buscarNotificaciones() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String consulta;

		if (this.fechaDesde != null && this.fechaHasta == null) {
			consulta = "SELECT n FROM Notificacion n WHERE n.fechaCreada >= '"
					+ new Timestamp(this.fechaDesde.getTime()).toString() + "' ORDER BY n.fechaCreada";
		} else if (this.fechaHasta != null && this.fechaDesde == null) {
			consulta = "SELECT n FROM Notificacion n WHERE n.fechaCreada <= '"
					+ new Timestamp(this.fechaHasta.getTime()).toString() + "' ORDER BY n.fechaCreada";
		} else if (this.fechaDesde != null && this.fechaHasta != null) {
			consulta = "SELECT n FROM Notificacion n WHERE n.fechaCreada >= '"
					+ new Timestamp(this.fechaDesde.getTime()).toString() + "' AND n.fechaCreada <= '"
					+ new Timestamp(this.fechaHasta.getTime()).toString() + "' ORDER BY n.fechaCreada";
		} else if (this.numeroAtencion != null && this.numeroAtencion != 0L) {
			consulta = "SELECT n FROM Notificacion n WHERE n.credito.numeroAtencion = " + this.numeroAtencion
					+ " ORDER BY n.fechaCreada";
		} else if (this.idPersona != null && idPersona != 0L) {
			consulta = "SELECT n FROM Notificacion n WHERE n.persona.id = " + this.idPersona
					+ " ORDER BY n.fechaCreada";
		} else if (this.emisor != null && !this.emisor.trim().isEmpty()) {
			consulta = "SELECT n FROM Notificacion n WHERE n.usuario = '" + this.emisor + "' ORDER BY n.fechaCreada";
		} else {
			consulta = "SELECT n FROM Notificacion n ORDER BY n.fechaCreada";
		}

		this.notificaciones = bp.getByFilter(consulta);
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<Notificacion> getNotificaciones() {
		return notificaciones;
	}

	public void setNotificaciones(List<Notificacion> notificaciones) {
		this.notificaciones = notificaciones;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

}
