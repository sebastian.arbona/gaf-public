package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MultiHashMap;
import org.hibernate.Query;

import com.asf.cred.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.AbstractEstado;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class AcuerdoPagoCaida extends ConversationProcess {

	private Long idCredito;
	@Save(refresh = true)
	private Objetoi credito;
	@Save
	private List<Objetoi> originales;
	private Usuario usuario;
	private Tipomov tipoDebito;
	@Save
	private List<Boleto> boletos;
	private Tipomov tipoCredito;
	private MultiHashMap pagos;
	@Save
	private String[] deudas;
	private boolean guardado;
	private Date fechaCaida = new Date();

	// Ticket 9319
	private Estado estadoAcuerdo;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscar() {
		if (idCredito == null || idCredito == 0L) {
			return false;
		}

		credito = (Objetoi) bp.getById(Objetoi.class, idCredito);

		originales = bp.createQuery("select c from Objetoi c where c.acuerdoPago.id = :idAcuerdo order by c.id")
				.setParameter("idAcuerdo", idCredito).list();

		List<BigDecimal> idsBoletos = bp.createSQLQuery(
				"select distinct boleto_id from Ctacte where tipomovimiento like 'pago' and tipomov_id = 1 and objetoi_id = :idCredito")
				.setParameter("idCredito", idCredito).list();

		if (!idsBoletos.isEmpty()) {
			List<Long> ids = new ArrayList<Long>();
			for (BigDecimal id : idsBoletos) {
				if (id != null) {
					ids.add(id.longValue());
				}
			}
			if (!ids.isEmpty()) {
				boletos = bp.createQuery("select b from Boleto b where b.id in (:ids)").setParameterList("ids", ids)
						.list();
			}
		}

		deudas = new String[originales.size()];
		int i = 0;
		for (Objetoi original : originales) {
			CreditoCalculoDeuda calc = new CreditoCalculoDeuda(original.getId(), new Date());
			calc.calcular();
			deudas[i++] = String.format("%,.2f", calc.getDeudaTotal());
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardar() {

		bp.begin();

		tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		idCredito = credito.getId();

		parsearAplicacionPagos();
		if (!validarAplicacionPagos()) {
			// buscar(); // vuelve a mostrar los originales y los boletos en
			// pantalla
			return false;
		}

		// revertir acuerdo
		/*
		 * Ticket 9314 saldarAcuerdo();
		 */
		this.estadoAcuerdo = this.credito.getEstadoActual().getEstado();
		cambiarEstadoAcuerdo();

		// reabrir originales
		generarDebitosOriginales();
		reimputarPagos();
		generarIntereses();
		actualizarComportamientoOriginales();
		actualizarEstadoOriginales();

		// bp.commit();

		/* Ticket 9319. Reimputo gastos y multas */
		generarDebitosGastos();
		/* Agrego imputaciones inversas del acuerdo de pago */
		revertirBoletos();
		/* Ticket 9319. Traslado los gastos al credito original */
		List<DetalleFactura> listaDetfact = bp
				.createSQLQuery("select {d.*} from DetalleFactura d where d.objetoi_id = :idCredito")
				.addEntity("d", DetalleFactura.class).setParameter("idCredito", this.idCredito).list();
		for (DetalleFactura det : listaDetfact) {
			det.setCredito(this.originales.get(0));
			bp.update(det);
		}

		bp.commit();

		errors.put("acuerdoPago.revertir.ok", "acuerdoPago.revertir.ok");
		guardado = true;

		forward = "AcuerdoPagoCaidaOk";

		return true;
	}

	private void parsearAplicacionPagos() {
		pagos = new MultiHashMap(); // credito -> list<pago>

		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("pago-")) {
				String[] partes = param.split("-"); // formato
				// pago-boleto=credito
				Long idBoleto = new Long(partes[1]);

				Long idObjetoi = new Long(request.getParameter(param));
				pagos.put(idObjetoi, idBoleto);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private boolean validarAplicacionPagos() {
		List<BigDecimal> idsBoletos = bp.createSQLQuery(
				"select distinct boleto_id from Ctacte where tipomovimiento like 'pago' and tipomov_id = 1 and objetoi_id = :idCredito")
				.setParameter("idCredito", idCredito).list();

		List<Long> ids = new ArrayList<Long>();
		if (!idsBoletos.isEmpty()) {
			for (BigDecimal id : idsBoletos) {
				if (id != null) {
					ids.add(id.longValue());
				}
			}
		}

		for (Object idBoletoObj : pagos.values()) {
			Long idBoleto = (Long) idBoletoObj;
			ids.remove(idBoleto); // sacar de ids cuando coincida
		}

		// si no quedo vacia, faltan aplicar pagos
		if (!ids.isEmpty()) {
			errors.put("acuerdoPago.aplicacionPagos", "acuerdoPago.aplicacionPagos");
			return false;
		}

		return true;
	}

	/**
	 * Actualiza comportamiento de todos los creditos originales en base a los dias
	 * de atraso
	 */
	private void actualizarComportamientoOriginales() {
		CreditoHandler handler = new CreditoHandler();

		for (Objetoi original : originales) {
			if (original.getCaducidadPlazo() != null) {
				handler.actualizarComportamiento(original, original.getCaducidadPlazo().getFecha(), null);
			} else {
				handler.actualizarComportamiento(original, null, null);
			}
		}
	}

	/**
	 * Actualiza estado de creditos originales
	 */
	private void actualizarEstadoOriginales() {
		for (Objetoi original : originales) {
			if (this.estadoAcuerdo.toString().equals(AbstractEstado.GESTION_JUDICIAL)) {
				// Ticket 9319
				original.setEstadoActual(AbstractEstado.GESTION_JUDICIAL, "Caida de Acuerdo de Pago");
			} else {
				original.setEstadoActual(AbstractEstado.GESTION_EXTRAJUDICIAL, "Cai�da de Acuerdo de Pago");
			}
		}
	}

	/**
	 * Genera moratorio y punitorio para el acuerdo de pago, y hace una NC para
	 * saldar la deuda.
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private void saldarAcuerdo() {
		Date fechaCalculo = fechaCaida;

		CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(credito.getId(), fechaCalculo);
		calculo.calcular();

		BeanCtaCte deudaTotal = calculo.getTotal();

		double montoMoratorio = deudaTotal.getMoratorio();
		double montoPunitorio = deudaTotal.getPunitorio();

		// no generar
		// generarMoratorioPunitorio(credito.getId(), montoMoratorio,
		// montoPunitorio, fechaCalculo);

		Calendar c = Calendar.getInstance();
		int periodo = c.get(Calendar.YEAR);

		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;
		// no incluir MyP en el monto
		double deudaMontoTotal = calculo.getDeudaTotal() - montoMoratorio - montoPunitorio;

		// boleto tipo nota de credito
		Boleto notaCredito = new Boleto();
		notaCredito.setFechaEmision(fechaCalculo);
		notaCredito.setFechaVencimiento(fechaCalculo);
		notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
		notaCredito.setUsuario(usuario);
		notaCredito.setObjetoi(credito);
		notaCredito.setPeriodoBoleto((long) periodo);
		notaCredito.setVerificadorBoleto(0L);
		notaCredito.setImporte(deudaMontoTotal);
		notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
		bp.save(notaCredito);

		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", credito.getId()).list();
		Cuota ultimaCuota = cuotas.get(cuotas.size() - 1);

		// crear una ctacte y bolcon por cada concepto
		if (deudaTotal.getCapital() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_CAPITAL, deudaTotal.getCapital(), fechaCalculo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_REVERSION_ACUERDO, ultimaCuota);
			itemCtaCte++;
		}
		if (deudaTotal.getCompensatorio() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_COMPENSATORIO, deudaTotal.getCompensatorio(), fechaCalculo,
					notaCredito, movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_REVERSION_ACUERDO,
					ultimaCuota);
			itemCtaCte++;
		}
		if (deudaTotal.getGastosRec() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_GASTOS_A_RECUPERAR, deudaTotal.getGastosRec(), fechaCalculo,
					notaCredito, movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_REVERSION_ACUERDO,
					ultimaCuota);
			itemCtaCte++;
		}
		if (deudaTotal.getGastosSimples() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_GASTOS, deudaTotal.getGastosSimples(), fechaCalculo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_REVERSION_ACUERDO, ultimaCuota);
			itemCtaCte++;
		}
		if (deudaTotal.getMultas() > 0) {
			guardarConcepto(credito, Concepto.CONCEPTO_MULTAS, deudaTotal.getMultas(), fechaCalculo, notaCredito,
					movimientoCtaCte, itemCtaCte, tipoCredito, Ctacte.DETALLE_REVERSION_ACUERDO, ultimaCuota);
			itemCtaCte++;
		}
		// if (deudaTotal.getMoratorio() > 0) {
		// guardarConcepto(credito, Objetoi.CONCEPTO_MORATORIO,
		// deudaTotal.getMoratorio(), fechaCalculo, notaCredito,
		// movimientoCtaCte, itemCtaCte, tipoCredito,
		// Ctacte.DETALLE_REVERSION_ACUERDO, ultimaCuota);
		// itemCtaCte++;
		// }
		// if (deudaTotal.getPunitorio() > 0) {
		// guardarConcepto(credito, Objetoi.CONCEPTO_PUNITORIO,
		// deudaTotal.getPunitorio(), fechaCalculo, notaCredito,
		// movimientoCtaCte, itemCtaCte, tipoCredito,
		// Ctacte.DETALLE_REVERSION_ACUERDO, ultimaCuota);
		// }

	}

	@SuppressWarnings("unchecked")
	private void cambiarEstadoAcuerdo() {
		ObjetoiEstado estadoCreditoAcuerdo = new ObjetoiEstado();
		estadoCreditoAcuerdo.setFecha(fechaCaida);
		if (credito.getEstadoActual() != null) {
			credito.getEstadoActual().setFechaHasta(fechaCaida);
		}

		List<Estado> estadosAcuerdoIncumplido = bp.createQuery("select e from Estado e where e.nombreEstado = :estado")
				.setParameter("estado", Estado.ACUERDO_CAIDO).list();

		if (!estadosAcuerdoIncumplido.isEmpty()) {
			Estado estadoAcuerdo = estadosAcuerdoIncumplido.get(0);
			estadoCreditoAcuerdo.setEstado(estadoAcuerdo);
		}
		estadoCreditoAcuerdo.setObjetoi(credito);
		bp.save(estadoCreditoAcuerdo);
	}

	private void generarIntereses() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		Date hoy = c.getTime();

		for (Objetoi original : originales) {
			double moratorio = calcularMoratorio(original, hoy);
			double punitorio = calcularPunitorio(original, hoy);

			generarMoratorioPunitorio(original.getId(), moratorio, punitorio, hoy);
		}
	}

	/**
	 * Genera nota de debito en cada credito original, revirtiendo las notas de
	 * credito generadas cuando se hizo el acuerdo de pago.
	 */
	@SuppressWarnings("unchecked")
	private void generarDebitosOriginales() {
		com.nirven.creditos.hibernate.AcuerdoPago acuerdo = (com.nirven.creditos.hibernate.AcuerdoPago) bp
				.createQuery("select a from AcuerdoPago a where a.creditoAcuerdo.id = :idAcuerdo")
				.setParameter("idAcuerdo", credito.getId()).uniqueResult();

		originales = bp.createQuery("select c from Objetoi c where c.acuerdoPago.id = :idAcuerdo order by c.id")
				.setParameter("idAcuerdo", credito.getId()).list();

		usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
		tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();

		Calendar c = Calendar.getInstance();
		int periodo = c.get(Calendar.YEAR);

		for (int i = 0; i < originales.size(); i++) {
			Objetoi original = originales.get(i);

			// nota de debito para reflotar la deuda
			Boleto notaDebito = new Boleto();
			notaDebito.setFechaEmision(acuerdo.getFecha());
			notaDebito.setFechaVencimiento(acuerdo.getFecha());
			notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			notaDebito.setUsuario(usuario);
			notaDebito.setObjetoi(original);
			notaDebito.setPeriodoBoleto((long) periodo);
			notaDebito.setVerificadorBoleto(0L);
			notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
			bp.save(notaDebito);
			double importeTotalDebito = 0;
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;
			List<Ctacte> movimientos = bp
					.createQuery("select m from AcuerdoPago a join a.movimientos m "
							+ "where a.id = :idAcuerdo and m.id.objetoi.id = :idOriginal")
					.setParameter("idAcuerdo", acuerdo.getId()).setParameter("idOriginal", original.getId()).list();

			List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", original.getId())
					.list();
			Cuota ultimaCuota = cuotas.get(cuotas.size() - 1);

			for (Ctacte mov : movimientos) {
				// solo revertir los creditos, los debitos de moratorio y
				// punitorio quedan
				if (!mov.isDebito()) {

					guardarConcepto(original, mov.getFacturado().getConcepto().getAbreviatura(), mov.getImporte(),
							acuerdo.getFecha(), notaDebito, movimientoCtaCte, itemCtaCte, tipoDebito,
							"Nota de Debito por Cai�da de Acuerdo de Pago", ultimaCuota);
					itemCtaCte++;
					importeTotalDebito += mov.getImporte();
				}
			}

			notaDebito.setImporte(importeTotalDebito);
			bp.update(notaDebito);
		}
	}

	/**
	 * Reimputa los pagos del acuerdo en los originales, ordenados por vencimiento
	 * de primera cuota impaga
	 */
	@SuppressWarnings("unchecked")
	private void reimputarPagos() {
		for (Object idObjetoi : pagos.keySet()) {
			List<Long> idsPagos = (List<Long>) pagos.get(idObjetoi);
			aplicarPagos((Long) idObjetoi, idsPagos);
		}
	}

	@SuppressWarnings("unchecked")
	private void aplicarPagos(Long idCredito, List<Long> idsPagos) {
		CreditoHandler handler = new CreditoHandler();
		Calendar c = Calendar.getInstance();
		int periodo = c.get(Calendar.YEAR);
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idCredito);
		List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", objetoi.getId()).list();
		Cuota ultimaCuota = cuotas.get(cuotas.size() - 1);
		// ordenar boletos por fecha de pago
		idsPagos = bp
				.createQuery("select p.recibo.id from Pagos p where p.recibo.id in (:idsPagos) order by p.fechaPago")
				.setParameterList("idsPagos", idsPagos).list();
		// Buscar pago asociado al boleto, hay uno solo
		Query queryPago = bp.createQuery("select p from Pagos p where p.recibo.id = :idBoleto");
		// iterar los pagos seleccionados
		for (Long idBoleto : idsPagos) {
			Boleto boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			List<Pagos> pagos = queryPago.setParameter("idBoleto", idBoleto).list();
			if (pagos.isEmpty()) {
				continue;
			}
			// debe haber uno solo
			Pagos pago = pagos.get(0);
			// generar MyP
			double montoMoratorio = calcularMoratorio(objetoi, pago.getFechaPago());
			double montoPunitorio = calcularPunitorio(objetoi, pago.getFechaPago());
			generarMoratorioPunitorio(idCredito, montoMoratorio, montoPunitorio, pago.getFechaPago());
			List<Ctacte> ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_MULTAS);
			double saldoMultas = handler.calcularSaldo(ccs);
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
			double saldoGastosRec = handler.calcularSaldo(ccs);
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_GASTOS);
			double saldoGastos = handler.calcularSaldo(ccs);
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_PUNITORIO);
			double saldoPunitorio = handler.calcularSaldo(ccs);
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_MORATORIO);
			double saldoMoratorio = handler.calcularSaldo(ccs);
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_COMPENSATORIO);
			double saldoCompensatorio = handler.calcularSaldo(ccs);
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_BONIFICACION);
			double saldoBonificaciones = handler.calcularSaldo(ccs);
			// ajustar bonificaciones
			saldoCompensatorio += saldoBonificaciones;
			ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_CAPITAL);
			double saldoCapital = handler.calcularSaldo(ccs);
			// nota de credito
			Boleto notaCredito = new Boleto();
			notaCredito.setFechaEmision(boleto.getFechaEmision());
			notaCredito.setFechaVencimiento(boleto.getFechaVencimiento());
			notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
			notaCredito.setUsuario(usuario);
			notaCredito.setObjetoi(objetoi);
			notaCredito.setPeriodoBoleto((long) periodo);
			notaCredito.setVerificadorBoleto(0L);
			notaCredito.setImporte(boleto.getImporte());
			notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
			bp.save(notaCredito);
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;
			double saldoNotaCredito = boleto.getImporte();
			String detalleReimp = "Reimputacion de Pago"
					+ (pago.getRecibo() != null ? " - Recibo Nro. " + pago.getRecibo().getNumeroBoleto() : "");
			// creditos individuales por conceptos > 0
			if (saldoMultas > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_MULTAS, Math.min(saldoMultas, saldoNotaCredito),
						pago.getFechaPago(), notaCredito, movimientoCtaCte, itemCtaCte++, tipoCredito, detalleReimp,
						ultimaCuota, Objetoi.TIPO_MOVIMIENTO_REIMPUTACION, Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoMultas < saldoNotaCredito ? saldoNotaCredito - saldoMultas : 0);
			}
			if (saldoNotaCredito > 0 && saldoGastosRec > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_GASTOS_A_RECUPERAR,
						Math.min(saldoGastosRec, saldoNotaCredito), pago.getFechaPago(), notaCredito, movimientoCtaCte,
						itemCtaCte++, tipoCredito, detalleReimp, ultimaCuota, Objetoi.TIPO_MOVIMIENTO_REIMPUTACION,
						Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoGastosRec < saldoNotaCredito ? saldoNotaCredito - saldoGastosRec : 0);
			}
			if (saldoNotaCredito > 0 && saldoGastos > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_GASTOS, Math.min(saldoGastos, saldoNotaCredito),
						pago.getFechaPago(), notaCredito, movimientoCtaCte, itemCtaCte++, tipoCredito, detalleReimp,
						ultimaCuota, Objetoi.TIPO_MOVIMIENTO_REIMPUTACION, Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoGastos < saldoNotaCredito ? saldoNotaCredito - saldoGastos : 0);
			}
			if (saldoNotaCredito > 0 && saldoPunitorio > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_PUNITORIO, Math.min(saldoPunitorio, saldoNotaCredito),
						pago.getFechaPago(), notaCredito, movimientoCtaCte, itemCtaCte++, tipoCredito, detalleReimp,
						ultimaCuota, Objetoi.TIPO_MOVIMIENTO_REIMPUTACION, Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoPunitorio < saldoNotaCredito ? saldoNotaCredito - saldoPunitorio : 0);
			}
			if (saldoNotaCredito > 0 && saldoMoratorio > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_MORATORIO, Math.min(saldoMoratorio, saldoNotaCredito),
						pago.getFechaPago(), notaCredito, movimientoCtaCte, itemCtaCte++, tipoCredito, detalleReimp,
						ultimaCuota, Objetoi.TIPO_MOVIMIENTO_REIMPUTACION, Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoMoratorio < saldoNotaCredito ? saldoNotaCredito - saldoMoratorio : 0);
			}
			if (saldoNotaCredito > 0 && saldoCompensatorio > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_COMPENSATORIO,
						Math.min(saldoCompensatorio, saldoNotaCredito), pago.getFechaPago(), notaCredito,
						movimientoCtaCte, itemCtaCte++, tipoCredito, detalleReimp, ultimaCuota,
						Objetoi.TIPO_MOVIMIENTO_REIMPUTACION, Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoCompensatorio < saldoNotaCredito ? saldoNotaCredito - saldoCompensatorio : 0);
			}
			if (saldoNotaCredito > 0 && saldoCapital > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_CAPITAL, Math.min(saldoCapital, saldoNotaCredito),
						pago.getFechaPago(), notaCredito, movimientoCtaCte, itemCtaCte++, tipoCredito, detalleReimp,
						ultimaCuota, Objetoi.TIPO_MOVIMIENTO_REIMPUTACION, Ctacte.TIPO_CONCEPTO_REIMPUTACION4);
				saldoNotaCredito = (saldoCapital < saldoNotaCredito ? saldoNotaCredito - saldoCapital : 0);
			}
		}
	}

	private double calcularMoratorio(Objetoi objetoi, Date fechaCalculo) {
		CreditoHandler ch = new CreditoHandler();
		double deudaExigible = ch.calcularSaldo(ch.listarCtaCteConceptos(objetoi,
				new String[] { Concepto.CONCEPTO_CAPITAL, Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_GASTOS,
						Concepto.CONCEPTO_GASTOS_A_RECUPERAR, Concepto.CONCEPTO_MULTAS,
						Concepto.CONCEPTO_BONIFICACION }));
		double moratorio = 0;
		Date ultimoMoratorio = (Date) bp.createQuery(
				"select max(c.fechaGeneracion) from Ctacte c where c.facturado.concepto.abreviatura = :concepto and c.id.objetoi.id = :idCredito and c.tipomov.abreviatura = :debito")
				.setParameter("concepto", Concepto.CONCEPTO_MORATORIO).setParameter("idCredito", objetoi.getId())
				.setParameter("debito", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		if (ultimoMoratorio != null) {
			int dias = (int) DateHelper.getDiffDates(ultimoMoratorio, fechaCalculo, 2);
			moratorio = objetoi.calcularMoratorioNew(deudaExigible, dias, dias, ultimoMoratorio, ultimoMoratorio,
					fechaCalculo);
		}
		return moratorio;
	}

	private double calcularPunitorio(Objetoi objetoi, Date fechaCalculo) {
		CreditoHandler ch = new CreditoHandler();
		double deudaExigible = ch.calcularSaldo(ch.listarCtaCteConceptos(objetoi,
				new String[] { Concepto.CONCEPTO_CAPITAL, Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_GASTOS,
						Concepto.CONCEPTO_GASTOS_A_RECUPERAR, Concepto.CONCEPTO_MULTAS,
						Concepto.CONCEPTO_BONIFICACION }));
		double punitorio = 0;
		Date ultimoPunitorio = (Date) bp.createQuery(
				"select max(c.fechaGeneracion) from Ctacte c where c.facturado.concepto.abreviatura = :concepto and c.id.objetoi.id = :idCredito and c.tipomov.abreviatura = :debito")
				.setParameter("concepto", Concepto.CONCEPTO_PUNITORIO).setParameter("idCredito", objetoi.getId())
				.setParameter("debito", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		if (ultimoPunitorio != null) {
			int dias = (int) DateHelper.getDiffDates(ultimoPunitorio, fechaCalculo, 2);
			punitorio = objetoi.calcularPunitorioNew(deudaExigible, dias, dias, ultimoPunitorio, ultimoPunitorio,
					fechaCalculo);
		}
		return punitorio;
	}

	@SuppressWarnings("unchecked")
	private void generarMoratorioPunitorio(Long idCredito, double montoMoratorio, double montoPunitorio,
			Date fechaCalculo) {
		if (montoMoratorio > 0 || montoPunitorio > 0) {
			Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idCredito);
			Calendar c = Calendar.getInstance();
			int periodo = c.get(Calendar.YEAR);
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			long itemCtaCte = 1;

			// boleto tipo nota de debito
			Boleto notaDebito = new Boleto();
			notaDebito.setFechaEmision(fechaCalculo);
			notaDebito.setFechaVencimiento(fechaCalculo);
			notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			notaDebito.setUsuario(usuario);
			// notaCredito.setNumeroCuota(cuota.getNumero()); // no esta
			// asociado a ninguna cuota
			notaDebito.setObjetoi(objetoi);
			notaDebito.setPeriodoBoleto((long) periodo);
			notaDebito.setVerificadorBoleto(0L);
			notaDebito.setImporte(montoMoratorio + montoPunitorio);
			notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
			bp.save(notaDebito);
			List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", idCredito).list();
			Cuota ultimaCuota = cuotas.get(cuotas.size() - 1);
			if (montoMoratorio > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_MORATORIO, montoMoratorio, fechaCalculo, notaDebito,
						movimientoCtaCte, itemCtaCte, tipoDebito, "Int. MyP", ultimaCuota, "pago",
						Ctacte.TIPO_CONCEPTO_REIMPUTACION3);
				itemCtaCte++;
			}
			if (montoPunitorio > 0) {
				guardarConcepto(objetoi, Concepto.CONCEPTO_PUNITORIO, montoPunitorio, fechaCalculo, notaDebito,
						movimientoCtaCte, itemCtaCte, tipoDebito, "Int. MyP", ultimaCuota, "pago",
						Ctacte.TIPO_CONCEPTO_REIMPUTACION3);
			}
		}
	}

	private void guardarConcepto(Objetoi objetoi, String concepto, double valor, Date fechaEmision, Boleto nota,
			long movimientoCtaCte, long itemCtaCte, Tipomov tipo, String detalle, Cuota cuota) {
		String tipoMovimiento = "";
		if (tipo.getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
			tipoMovimiento = "movManualDeb";
		} else {
			tipoMovimiento = "movManualCred";
		}
		guardarConcepto(objetoi, concepto, valor, fechaEmision, nota, movimientoCtaCte, itemCtaCte, tipo, detalle,
				cuota, tipoMovimiento, Ctacte.TIPO_CONCEPTO_CAIDA_ACUERDO_PAGO);

	}

	private void guardarConcepto(Objetoi objetoi, String concepto, double valor, Date fechaEmision, Boleto nota,
			long movimientoCtaCte, long itemCtaCte, Tipomov tipo, String detalle, Cuota cuota, String tipoMovimiento,
			String TipoCtaCte) {

		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());

		Ctacte cc = objetoi.crearCtacte(concepto, valor, fechaEmision, nota, objetoi, cuota, TipoCtaCte);

		cc.setTipomov(tipo);
		cc.setTipoMovimiento(tipoMovimiento);
		cc.setUsuario(usuario);
		cc.getId().setMovimientoCtacte(movimientoCtaCte);
		cc.getId().setItemCtacte(itemCtaCte);
		cc.setBoleto(nota);
		cc.setDetalle(detalle);

		// crear nuevo registro bolcon
		Bolcon bc = objetoi.crearBolcon(concepto, valor, nota, cc.getId(), cuota);
		bc.setTipomov(tipo);

		bp.save(cc);
		bp.save(bc);

	}

	/*
	 * fin modificacion
	 */

	@Override
	protected String getDefaultForward() {
		return "AcuerdoPagoCaida";
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public List<Objetoi> getOriginales() {
		return originales;
	}

	public List<Boleto> getBoletos() {
		return boletos;
	}

	public String[] getDeudas() {
		return deudas;
	}

	public boolean isGuardado() {
		return guardado;
	}

	public String getFechaCaida() {
		return DateHelper.getString(fechaCaida);
	}

	public void setFechaCaida(String fechaCaida) {
		this.fechaCaida = DateHelper.getDate(fechaCaida);
	}

	@SuppressWarnings("unchecked")
	private List<Bolcon> listarBolcons(Long boleto_id, Long movimiento) {
		return bp
				.createSQLQuery("select {bc.*} from Bolcon bc where bc.boleto_id = :bol_id "
						+ "       and bc.movimientoCtacte = :mov order by bc.boleto_id, bc.movimientoCtacte")
				.addEntity("bc", Bolcon.class).setParameter("bol_id", boleto_id).setParameter("mov", movimiento).list();
	}

	@SuppressWarnings("unchecked")
	private List<Boleto> listarBoletos() {
		return bp.createSQLQuery("select {bl.*} from Boleto bl "
				+ " 	where bl.id in (select cc.boleto_id from Ctacte cc where cc.objetoi_id = :cred_id and cc.tipoConcepto != '116') "
				+ "union select top(1) {bl.*} from Boleto bl where bl.objetoi_id = :cred_id and tipo = 'Capital' "
				+ "order by bl.id").addEntity("bl", Boleto.class).setParameter("cred_id", credito.getId()).list();
	}

	@SuppressWarnings("unchecked")
	private List<Ctacte> listarCtaCte(Long boleto_id) {
		return bp
				.createSQLQuery("select {cc.*} from CtaCte cc where cc.objetoi_id = :id "
						+ "       and cc.boleto_id = :bol_id order by cc.movimientoCtacte")
				.addEntity("cc", Ctacte.class).setParameter("id", credito.getId()).setParameter("bol_id", boleto_id)
				.list();
	}

	@SuppressWarnings("unchecked")
	private List<Ctacte> listarCtaCteCapital() {
		return bp
				.createSQLQuery("select {cc.*} "
						+ "		from CtaCte cc left join Boleto bol on cc.boleto_id = bol.id, Concepto conc "
						+ " 	where cc.asociado_id = conc.id and cc.objetoi_id = :id "
						+ "       and conc.concepto_concepto = 'cap' and cc.tipomov_id = 2 "
						+ "       and cc.tipoMovimiento = 'cuota' and cc.desembolso_id IS NOT NULL ")
				.addEntity("cc", Ctacte.class).setParameter("id", credito.getId()).list();
	}

	@SuppressWarnings("unchecked")
	private List<Object[]> importeConcepto(String concepto) {
		// return bp.createSQLQuery("select convert(varchar,
		// min(cc.fechaGeneracion), 121) as fechaGeneracion, "
		return bp
				.createSQLQuery("select (cc.fechaGeneracion) fechaGeneracion, (b.periodoBoleto) periodo, "
						+ "      (case when cc.tipomov_id = 2 then cc.importe else -cc.importe end), "
						+ "      (cc.cuota_id) cuota, cc.detalle from CtaCte cc, Concepto conc, Boleto b "
						+ " 	where cc.boleto_id = b.id and cc.asociado_id = conc.id and cc.objetoi_id = :id "
						+ "       and conc.concepto_concepto = :concept and cc.tipomov_id = 2 ")
				.setParameter("id", credito.getId()).setParameter("concept", concepto).list();
	}

	// Ticket 9319. Trasladar los Gastos por Facturaci�n acuerdo al Credito
	// Original
	public void generarDebitosGastos() {
		List<Object[]> registros = importeConcepto(Concepto.CONCEPTO_GASTOS);

		String sql = "SELECT {cu.*} FROM Cuota cu where cu.id in(SELECT MAX(id) FROM Cuota A WHERE A.credito_id IN(";
		sql += " SELECT B.ID FROM Objetoi A, Objetoi B ";
		sql += "    WHERE B.acuerdoPago_id = A.id ";
		sql += "    AND A.id = :id))";

		Cuota cuota = (Cuota) bp.createSQLQuery(sql).addEntity("cu", Cuota.class).setParameter("id", idCredito)
				.uniqueResult();

		for (int i = 0; i < registros.size(); i++) {
			Object[] registro = registros.get(i);

			if (registro[2] != null) {
				// BigDecimal bdcuota_id = (BigDecimal) registro[3];
				// Long cuota_id = bdcuota_id.longValue();
				// BigDecimal bdboleto_id = (BigDecimal) registro[1];
				// Long boleto_id = bdboleto_id.longValue();
				Boleto boleto = new Boleto();
				boleto.setFechaEmision((Date) registro[0]);
				boleto.setImporte((Double) registro[2]);
				boleto.setObjetoi(this.credito);
				boleto.setPeriodoBoleto((long) Calendar.getInstance().get(Calendar.YEAR));
				boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				// ejemplo
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				boleto.setUsuario(this.usuario);
				boleto.setVerificadorBoleto(0L);
				bp.save(boleto);
				guardarConcepto(this.originales.get(0), Concepto.CONCEPTO_GASTOS, (Double) registro[2],
						(Date) registro[0], boleto, Numerador.getNext("ctacte." + registro[1]), 1L,
						Tipomov.buscarDebito(), "Gasto " + (registro[4] == null ? "" : registro[4].toString()), cuota);
			}
		}

		registros = importeConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
		for (int i = 0; i < registros.size(); i++) {
			Object[] registro = registros.get(i);
			if (registro[2] != null) {
				// BigDecimal bdcuota_id = (BigDecimal) registro[3];
				// Long cuota_id = bdcuota_id.longValue();
				// BigDecimal bdboleto_id = (BigDecimal) registro[1];
				// Long boleto_id = bdboleto_id.longValue();
				/*
				 * 
				 * Cuota cuota = (Cuota)
				 * bp.getNamedQuery("Cuota.findById").setParameter("idCuota", cuota_id)
				 * .uniqueResult();
				 */
				Boleto boleto = new Boleto();

				boleto.setFechaEmision((Date) registro[0]);
				boleto.setImporte((Double) registro[2]);
				boleto.setObjetoi(this.credito);
				boleto.setPeriodoBoleto((long) Calendar.getInstance().get(Calendar.YEAR));
				boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				boleto.setUsuario(this.usuario);
				boleto.setVerificadorBoleto(0L);
				bp.save(boleto);

				guardarConcepto(this.originales.get(0), Concepto.CONCEPTO_GASTOS_A_RECUPERAR, (Double) registro[2],
						(Date) registro[0], boleto, Numerador.getNext("ctacte." + registro[1]), 1L,
						Tipomov.buscarDebito(),
						"Gasto a Recuperar " + (registro[4] == null ? "" : registro[4].toString()), cuota);
			}
		}

		registros = importeConcepto(Concepto.CONCEPTO_MULTAS);
		for (int i = 0; i < registros.size(); i++) {
			Object[] registro = registros.get(i);
			if (registro[2] != null) {
				Boleto boleto = new Boleto();
				boleto.setFechaEmision((Date) registro[0]);
				boleto.setImporte((Double) registro[2]);
				boleto.setObjetoi(this.credito);
				boleto.setPeriodoBoleto((long) Calendar.getInstance().get(Calendar.YEAR));
				boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				boleto.setUsuario(this.usuario);
				boleto.setVerificadorBoleto(0L);
				bp.save(boleto);

				guardarConcepto(this.originales.get(0), Concepto.CONCEPTO_MULTAS, (Double) registro[2],
						(Date) registro[0], boleto, Numerador.getNext("ctacte." + registro[1]), 1L,
						Tipomov.buscarDebito(), "Multas " + (registro[4] == null ? "" : registro[4].toString()), cuota);
			}
		}
	}

	// Si no existen boletos, no se revierte ningón movimiento
	public void revertirBoletos() {
		// Ticket 9314
		String tipoconcepto;
		bp.begin();
		List<Boleto> boletos = listarBoletos();
		for (Boleto boleto : boletos) {
			Boleto newboleto = new Boleto();
			newboleto.setAnulado(boleto.getAnulado());
			newboleto.setBoletoAnulacion(boleto.getBoletoAnulacion());
			newboleto.setFechaEmision(boleto.getFechaEmision());
			newboleto.setFechaEmisionCuota(boleto.getFechaEmisionCuota());
			newboleto.setFechaVencimiento(boleto.getFechaVencimiento());
			newboleto.setImporte(boleto.getImporte());
			newboleto.setImporteVencido(boleto.getImporteVencido());
			newboleto.setNumeroCuota(boleto.getNumeroCuota());
			newboleto.setObjetoi(boleto.getObjetoi());
			newboleto.setPeriodoBoleto((long) Calendar.getInstance().get(Calendar.YEAR));
			switch (boleto.getTipo()) {
			case Boleto.TIPO_BOLETO_FACTURA:
				newboleto.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				newboleto.setNumeroBoleto(Numerador.getNext(newboleto.getNumerador()));
				tipoconcepto = Ctacte.TIPO_CONCEPTO_REIMPUTACION1;
				break;
			case Boleto.TIPO_BOLETO_NOTADEB:
				newboleto.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				newboleto.setNumeroBoleto(Numerador.getNext(newboleto.getNumerador()));
				tipoconcepto = Ctacte.TIPO_CONCEPTO_REIMPUTACION1;
				break;
			case Boleto.TIPO_BOLETO_RECIBO:
				newboleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				newboleto.setNumeroBoleto(Numerador.getNext(newboleto.getNumerador()));
				tipoconcepto = Ctacte.TIPO_CONCEPTO_REIMPUTACION2;
				break;
			case Boleto.TIPO_BOLETO_CAPITAL:
				newboleto.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				newboleto.setNumeroBoleto(Numerador.getNext(newboleto.getNumerador()));
				tipoconcepto = Ctacte.TIPO_CONCEPTO_CAIDA_ACUERDO_PAGO;
				break;
			case Boleto.TIPO_BOLETO_NOTACRED:
				newboleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				newboleto.setNumeroBoleto(Numerador.getNext(newboleto.getNumerador()));
				tipoconcepto = Ctacte.TIPO_CONCEPTO_REIMPUTACION1;
				break;
			default:
				newboleto.setTipo("");
				tipoconcepto = Ctacte.TIPO_CONCEPTO_REIMPUTACION1;
			}
			newboleto.setUsuario(boleto.getUsuario());
			newboleto.setVerificadorBoleto(boleto.getVerificadorBoleto());

			bp.save(newboleto);

			// Guardo los nuevos registros de ctacte
			List<Ctacte> ctactes = listarCtaCte(boleto.getId());
			if (ctactes.isEmpty()) { // El boleto es el de capital
				for (Ctacte cc : this.listarCtaCteCapital()) {
					Ctacte newctacte = new Ctacte();
					CtacteKey newCtactekey = new CtacteKey();

					newCtactekey.setItemCtacte(cc.getId().getItemCtacte());
					newCtactekey.setMovimientoCtacte(Numerador.getNext("ctacte." + boleto.getPeriodoBoleto()));
					newCtactekey.setObjetoi(this.credito);
					newCtactekey.setPeriodoCtacte(boleto.getPeriodoBoleto());
					newCtactekey.setVerificadorCtacte(0L);

					newctacte.setId(newCtactekey);
					newctacte.setAsociado(Concepto.buscarConcepto("cap", boleto.getPeriodoBoleto().intValue()));
					newctacte.setBoleto(newboleto);
					newctacte.setCuota(cc.getCuota());
					newctacte.setDetalle(Ctacte.DETALLE_REVERSION_ACUERDO);
					newctacte.setFechaGeneracion(boleto.getFechaEmision());
					newctacte.setFechaProceso(new Date());
					newctacte.setFechaVencimiento(boleto.getFechaVencimiento());
					newctacte.setImporte(cc.getImporte());
					newctacte.setTipoConcepto(Ctacte.TIPO_CONCEPTO_CAIDA_ACUERDO_PAGO);

					newctacte.setTipomov_id(1L);
					newctacte.setTipoMovimiento("reimputacion");
					newctacte.setUsuario(boleto.getUsuario());

					bp.save(newctacte);
				}
			} else {
				for (Ctacte ctacte : ctactes) {
					Ctacte newctacte = new Ctacte();
					CtacteKey oldCtactekey = ctacte.getId();
					CtacteKey newCtactekey = new CtacteKey();

					newCtactekey.setItemCtacte(oldCtactekey.getItemCtacte());
					newCtactekey.setMovimientoCtacte(Numerador.getNext("ctacte." + oldCtactekey.getPeriodoCtacte()));
					newCtactekey.setObjetoi(oldCtactekey.getObjetoi());
					newCtactekey.setPeriodoCtacte(oldCtactekey.getPeriodoCtacte());
					newCtactekey.setVerificadorCtacte(oldCtactekey.getVerificadorCtacte());

					newctacte.setId(newCtactekey);
					newctacte.setAsociado(ctacte.getAsociado());
					newctacte.setBoleto(newboleto);
					newctacte.setCaratula(ctacte.getCaratula());
					newctacte.setComprobante(ctacte.getComprobante());
					newctacte.setContabiliza(ctacte.getContabiliza());
					newctacte.setCotizaMov(ctacte.getCotizaMov());
					newctacte.setCuota(ctacte.getCuota());
					newctacte.setCuotaOriginal(ctacte.getCuotaOriginal());
					newctacte.setDesembolso(ctacte.getDesembolso());
					newctacte.setDetalle("Desaplicaci�n " + boleto.getTipo() + " " + boleto.getNumeroBoleto());
					newctacte.setEmideta(ctacte.getEmideta());
					newctacte.setEnteBonificador(ctacte.getEnteBonificador());
					newctacte.setExpediente(ctacte.getExpediente());
					newctacte.setFacturado(ctacte.getFacturado());
					newctacte.setFechaGeneracion(ctacte.getFechaGeneracion());
					newctacte.setFechaProceso(new Date());
					newctacte.setFechaVencimiento(ctacte.getFechaVencimiento());
					newctacte.setIdBoletoResumen(ctacte.getIdBoletoResumen());
					newctacte.setImporte(ctacte.getImporte());
					newctacte.setTipoConcepto(tipoconcepto);
					if (ctacte.getTipomov_id() == 1) {
						newctacte.setTipomov_id(2L);
					} else {
						newctacte.setTipomov_id(1L);
					}
					newctacte.setTipoMovimiento(ctacte.getTipoMovimiento());
					newctacte.setUsuario(ctacte.getUsuario());

					bp.save(newctacte);

					// Guardo los nuevos detalles del boleto
					List<Bolcon> bolcones = listarBolcons(boleto.getId(), oldCtactekey.getMovimientoCtacte());
					for (Bolcon bolcon : bolcones) {
						Bolcon newbolcon = new Bolcon();
						BolconKey oldbolconkey = bolcon.getId();
						BolconKey newbolconkey = new BolconKey();

						newbolconkey.setBoleto(newboleto);
						newbolconkey.setItemCtacte(oldbolconkey.getItemCtacte());
						newbolconkey.setMovimientoCtacte(newCtactekey.getMovimientoCtacte());
						newbolconkey.setPeriodoCtacte(oldbolconkey.getPeriodoCtacte());
						newbolconkey.setVerificadorCtacte(oldbolconkey.getVerificadorCtacte());

						newbolcon.setCuota(bolcon.getCuota());
						newbolcon.setFacturado(bolcon.getFacturado());
						newbolcon.setId(newbolconkey);
						newbolcon.setImporte(bolcon.getImporte());
						newbolcon.setMes(bolcon.getMes());
						newbolcon.setOriginal(bolcon.getOriginal());
						if (bolcon.getTipomov_id() == 1) {
							newbolcon.setTipomov_id(2L);
						} else {
							newbolcon.setTipomov_id(1L);
						}
						bp.save(newbolcon);
					}
				}
				bp.commit();
			}
		}

	}

}
