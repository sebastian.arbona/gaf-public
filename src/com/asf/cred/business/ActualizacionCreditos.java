package com.asf.cred.business;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;

import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Instancia;
import com.nirven.creditos.hibernate.InstanciaObjetoi;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.Tipomov;

public class ActualizacionCreditos implements IProcess {

	private HashMap<String, Object> errores;
	private List<ObjetoiDTO> creditos;
	private String accion;
	private BusinessPersistance bp;
	private Integer desde;
	private Integer hasta;
	private Long idEstado;

	@SuppressWarnings("unchecked")
	public ActualizacionCreditos() {
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		creditos = (List<ObjetoiDTO>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("creditosVencidos");
	}

	public ActualizacionCreditos(BusinessPersistance bp) {
		errores = new HashMap<String, Object>();
		this.bp = bp;

	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public boolean doProcess() {
		if (accion == null) {
			return errores.isEmpty();
		}
		if (accion.equals("listar")) {
			listar();
		}
		if (accion.equals("actualizar")) {
			actualizar();
		}
		if (accion.equals("actualizarSeleccion")) {
			actualizarSeleccion();
			listar();
		}
		return errores.isEmpty();
	}

	public void listar() {
		List<Objetoi> objetos = buscarCreditosPorDesembolso("2");
		filtrarCreditos(objetos);
	}

	@SuppressWarnings("unchecked") // Estado = Realizado
	public List<Objetoi> buscarCreditosPorDesembolso(String estado) {
		return (List<Objetoi>) bp
				.createQuery("SELECT DISTINCT c FROM Desembolso d JOIN d.credito c WHERE d.estado = :estado")
				.setString("estado", estado).list();
	}

	@SuppressWarnings("unchecked")
	public void filtrarCreditos(List<Objetoi> objetos) {
		CreditoHandler ch = new CreditoHandler();
		Estado estado;

		creditos = new ArrayList<ObjetoiDTO>();

		ProgressStatus.iTotal = objetos.size();
		ProgressStatus.iProgress = 0;
		ProgressStatus.onProcess = true;

		for (Objetoi objetoi : objetos) {

			ProgressStatus.iProgress++;
			ProgressStatus.sStatus = "Analizando credito " + objetoi.getNumeroAtencionStr();

			estado = ch.findEstado(objetoi);
			if (estado != null && (estado.getNombreEstado().equals(Estado.ORIGINAL_CON_ACUERDO)
					|| estado.getNombreEstado().equals(Estado.CANCELADO)
					|| estado.getNombreEstado().equals(Estado.FINALIZADO))) {
				continue;
			}

			List<Cuota> cuotas = (List<Cuota>) bp
					.createQuery("SELECT c FROM Cuota c WHERE c.credito = :credito AND c.fechaVencimiento <= :hoy "
							+ "AND c.estado = '1' ORDER BY c.fechaVencimiento")
					.setMaxResults(1).setParameter("credito", objetoi).setParameter("hoy", new Date()).list();

			if (!cuotas.isEmpty()) { // tiene cuotas vencidas sin cancelar
				Cuota cuota = cuotas.get(0); // mas antigua sin cancelar

				CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(objetoi.getId(), new Date());
				ccd.calcular();
				double deuda = ccd.getDeudaVencidaFechaEstricta();

				int atraso = DateHelper.getDiasEntreFechas(cuota.getFechaVencimiento(), new Date());

				if (tieneAtraso(atraso)) {
					// deuda al menos del 10% de la ultima cuota
					if (deuda / cuota.getImporteCuota() >= 0.1) {

						ObjetoiDTO credito;
						if (estado != null) {
							credito = new ObjetoiDTO(objetoi, estado.getNombreEstado(), deuda);
							creditos.add(credito);
						} else {
							credito = new ObjetoiDTO(objetoi, "SIN ESTADO", deuda);
							creditos.add(credito);
						}
						credito.setDiasAtraso(atraso);

						List<ObjetoiComportamiento> comportamientos = bp.createQuery(
								"SELECT c FROM ObjetoiComportamiento c WHERE c.objetoi.id = :id AND c.fechaHasta is null")
								.setParameter("id", objetoi.getId()).list();
						if (comportamientos.size() > 0) {
							ObjetoiComportamiento comportamientoAnterior = comportamientos.get(0);
							credito.setComportamiento(comportamientoAnterior.getNombreComportamiento());
						}
					}
				}
			}
		}
		ProgressStatus.onProcess = false;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditosVencidos", creditos);
	}

	private boolean tieneAtraso(int atraso) {
		if (desde != null && desde.intValue() != 0 && hasta != null && hasta.intValue() != 0) {
			if (atraso >= desde.intValue() && atraso <= hasta.intValue()) {
				return true;
			} else {
				return false;
			}
		}
		if (desde != null && desde.intValue() != 0 && (hasta == null || hasta.intValue() == 0)) {
			if (atraso >= desde.intValue()) {
				return true;
			} else {
				return false;
			}
		}
		if (hasta != null && hasta.intValue() != 0 && (desde == null || desde.intValue() == 0)) {
			if (atraso <= hasta.intValue()) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public String getUltimoInicio() {
		try {
			Director d = (Director) bp.createQuery("SELECT d FROM Director d WHERE d.codigo = :codigo")
					.setParameter("codigo", "comportamientoPago.ultimaActualizacionInicio").uniqueResult();
			return d.getValor();
		} catch (Exception e) {

		}
		return "";
	}

	public String getUltimoFin() {
		try {
			Director d = (Director) bp.createQuery("SELECT d FROM Director d WHERE d.codigo = :codigo")
					.setParameter("codigo", "comportamientoPago.ultimaActualizacion").uniqueResult();
			String inicio = this.getUltimoInicio();
			if (inicio != null && !inicio.isEmpty() && d.getValor() != null && !d.getValor().isEmpty()) {
				Date i, f;
				DateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
				i = s.parse(inicio);
				f = s.parse(d.getValor());
				if (f.before(i))
					return "EN PROCESO";
			}
			return d.getValor();
		} catch (Exception e) {

		}
		return "";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void actualizar() {
		boolean seActualizo = false;
		try {
			// Seteo en la tabla Director la fecha y hora en que se inicia la
			// �ltima actualizaci�n de estados
			Director d = (Director) bp.createQuery("SELECT d FROM Director d WHERE d.codigo = :codigo")
					.setParameter("codigo", "comportamientoPago.ultimaActualizacionInicio").uniqueResult();
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			d.setValor(format.format(new Date()));
			bp.update(d);

		} catch (Exception e) {

			Director d = new Director();
			d.setCodigo("comportamientoPago.ultimaActualizacionInicio");
			d.setNombre("comportamientoPago.ultimaActualizacionInicio");
			d.setDescripcion("comportamientoPago.ultimaActualizacionInicio");
			d.setCodi01(SessionHandler.getCurrentSessionHandler().getCodi01());
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			d.setValor(format.format(new Date()));
			bp.save(d);
		}

		try {
			ProgressStatus.iTotal = 1;
			ProgressStatus.iProgress = 0;
			ProgressStatus.onProcess = true;

			Date hoy = new Date();

			// Actualizo el estado a "Cancelada" de las cuotas que se encuentran
			// en estado "Sin Cancelar", que tengan una emisi�n un importe mayor
			// a 0.005
			String sqlGetCuotasId = "SELECT c.id FROM " + BusinessPersistance.getSchema() + ".Ctacte cc JOIN "
					+ BusinessPersistance.getSchema() + ".Cuota c ON cc.cuota_id = c.id "
					+ "WHERE c.emision_id IS NOT NULL and c.estado = '1' GROUP BY c.id, c.credito_id, c.numero "
					+ "HAVING SUM(case WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) < 0.05";
			List cuotasId = bp.createSQLQuery(sqlGetCuotasId).list();
			if (!cuotasId.isEmpty()) {
				String sqlUpdateCuotas = "UPDATE " + BusinessPersistance.getSchema()
						+ ".Cuota SET estado = 2 WHERE id IN (-1";
				for (Iterator it = cuotasId.iterator(); it.hasNext();) {
					Number cuotaIdIt = (Number) it.next();
					sqlUpdateCuotas += ", " + cuotaIdIt.toString();
				}
				sqlUpdateCuotas += ");";
				bp.createSQLQuery(sqlUpdateCuotas).executeUpdate();
			}

			if (BusinessPersistance.getDBMS() == "SQLSERVER") {
				// Registro el atraso de los cr�ditos que cambiaron de
				// comportamiento (SITUACI�N NORMAL, INADECUADO CON ATRASO, CON
				// PROBLEMAS ATRASO, ALTO RIESGO CON ATRASO)
				// que no est�n en estado 'GESTION EXTRAJUDICIAL', 'GESTION
				// JUDICIAL', 'ORIGINAL CON ACUERDO', 'INCOBRABLE', 'CANCELADO',
				// 'ACUERDO CA�DO'
				String sqlInsert = IOUtils
						.toString(getClass().getResourceAsStream("comportamiento-actualizar-insert.sql"));
				bp.createSQLQuery(sqlInsert).setTimestamp("fecha", hoy).executeUpdate();

				// Actualizo la fecha hasta del comportamiento de los cr�ditos si
				// cambiaron de estado (SITUACI�N NORMAL, INADECUADO CON ATRASO, CON
				// PROBLEMAS ATRASO, ALTO RIESGO CON ATRASO)
				// que no est�n en estado 'GESTION EXTRAJUDICIAL', 'GESTION
				// JUDICIAL', 'ORIGINAL CON ACUERDO', 'INCOBRABLE', 'CANCELADO',
				// 'ACUERDO CA�DO'
				String sqlUpdate = IOUtils
						.toString(getClass().getResourceAsStream("comportamiento-actualizar-update.sql"));
				bp.createSQLQuery(sqlUpdate).setTimestamp("fecha", hoy).executeUpdate();

			} else if (BusinessPersistance.getDBMS() == "MYSQL") {
				String sqlInsert = IOUtils
						.toString(getClass().getResourceAsStream("comportamiento-actualizar-insert-mariadb.sql"));
				bp.createSQLQuery(sqlInsert).setParameter("fecha", new Date()).executeUpdate();

				String sqlUpdate = IOUtils
						.toString(getClass().getResourceAsStream("comportamiento-actualizar-update-mariadb.sql"));
				bp.createSQLQuery(sqlUpdate).setTimestamp("fecha", new Date()).executeUpdate();

			}
			ProgressStatus.iProgress = 1;
			ProgressStatus.onProcess = false;

			seActualizo = true;
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO
		}

		if (seActualizo) {
			errores.put("actualizacionCreditos.seActualizo", "actualizacionCreditos.seActualizo");
		} else {
			errores.put("actualizacionCreditos.noSeActualizo", "actualizacionCreditos.noSeActualizo");
		}

		try {
			// Seteo en la tabla Director la fecha y hora en que finaliza la
			// �ltima actualizaci�n de estados
			Director d = (Director) bp.createQuery("SELECT d FROM Director d WHERE d.codigo = :codigo")
					.setParameter("codigo", "comportamientoPago.ultimaActualizacion").uniqueResult();
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			d.setValor(format.format(new Date()));
			bp.update(d);

		} catch (Exception e) {
			Director d = new Director();
			d.setCodigo("comportamientoPago.ultimaActualizacion");
			d.setNombre("comportamientoPago.ultimaActualizacion");
			d.setDescripcion("comportamientoPago.ultimaActualizacion");
			d.setCodi01(SessionHandler.getCurrentSessionHandler().getCodi01());
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			d.setValor(format.format(new Date()));
			bp.save(d);
		}
	}

	public void actualizarSeleccion() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		String idEstado = request.getParameter("process.idEstado");
		Enumeration<String> paramNames = request.getParameterNames();
		HashSet<Long> seleccionados = new HashSet<Long>();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("requisito")) {
				String[] p = param.split("-");
				seleccionados.add(new Long(p[1]));
			}
		}

		ProgressStatus.iTotal = seleccionados.size();
		ProgressStatus.iProgress = 0;
		ProgressStatus.onProcess = true;

		for (ObjetoiDTO dto : creditos) {
			if (seleccionados.contains(dto.getIdObjetoi())) {
				Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
				Estado estado = (Estado) bp.getById(Estado.class, new Long(idEstado));
				bp.begin();
				if (!pasarInstancia(dto.getIdObjetoi(), estado)) {
					return;
				}
				// estado actual
				CreditoHandler creditoHandler = new CreditoHandler();
				ObjetoiEstado estadoActual = creditoHandler.findObjetoiEstado(objetoi);
				if (estadoActual != null) {
					estadoActual.setFechaHasta(new Date());
					bp.update(estadoActual);
				}
				// nuevo estado
				ObjetoiEstado nuevoEstado = new ObjetoiEstado();
				nuevoEstado.setObjetoi(objetoi);
				nuevoEstado.setFecha(new Date());
				nuevoEstado.setEstado(estado);
				bp.save(nuevoEstado);
				bp.commit();

				ProgressStatus.iProgress++;
				ProgressStatus.sStatus = "Actualizando credito " + objetoi.getNumeroAtencionStr();
			} else {
				System.out.println("Objeto NO encontrado: " + dto.getIdObjetoi());
			}
		}

		ProgressStatus.onProcess = false;

		listar();
	}

	@SuppressWarnings("unchecked")
	public boolean pasarInstancia(Long idOjetoi, Estado estado) {
		if (estado.getNombreEstado().equals("GESTION JUDICIAL")) {
			try {
				Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idOjetoi);
				List<Instancia> instancias = (List<Instancia>) bp
						.createQuery("SELECT i FROM Instancia i WHERE i.orden = 0").list();
				if (instancias.size() > 1) {
					throw new NonUniqueResultException();
				}
				if (instancias.isEmpty()) {
					throw new NoResultException();
				}
				Instancia instancia = instancias.get(0);
				if (!asociarGastosInstancia(instancia, objetoi)) {
					return false;
				}
				InstanciaObjetoi instanciaObjetoi = new InstanciaObjetoi();
				instanciaObjetoi.setFecha(new Date());
				instanciaObjetoi.setObjetoi(objetoi);
				instanciaObjetoi.setInstancia(instancia);
				bp.save(instanciaObjetoi);
				return true;
			} catch (NonUniqueResultException e) {
				e.printStackTrace();
				errores.put("pasarDeInstancia.error.ordenRepetido", "pasarDeInstancia.error.ordenRepetido");
				return false;
			} catch (NoResultException e) {
				e.printStackTrace();
				errores.put("pasarDeInstancia.error.instanciaCero", "pasarDeInstancia.error.instanciaCero");
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean asociarGastosInstancia(Instancia instancia, Objetoi objetoi) {
		if (instancia.getGastoAsociado().doubleValue() != 0.0) {
			List<Cuota> cuotas = (List<Cuota>) bp.createQuery(
					"SELECT c FROM Cuota c WHERE c.credito = :credito AND c.fechaVencimiento >= :hoy ORDER BY c.fechaVencimiento")
					.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();
			Cuota cuota = null;
			if (!cuotas.isEmpty()) {
				cuota = cuotas.get(0);
			} else {
				List<Cuota> cuotasVencidas = (List<Cuota>) bp.createQuery(
						"SELECT c FROM Cuota c WHERE c.credito = :credito AND c.fechaVencimiento < :hoy ORDER BY c.fechaVencimiento DESC")
						.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();
				if (!cuotasVencidas.isEmpty()) {
					cuota = cuotasVencidas.get(0);
				} else {
					errores.put("pasarDeInstancia.error.cuotas", "pasarDeInstancia.error.cuotas");
					return false;
				}
			}
			Tipomov tipoMovimiento = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
			CreditoHandler creditoHandler = new CreditoHandler();
			Ctacte cc = creditoHandler.crearCtaCte(cuota, Concepto.CONCEPTO_GASTOS, tipoMovimiento,
					instancia.getGastoAsociado());
			cc.setTipoMovimiento("cuota");
			cc.setFechaVencimiento(cuota.getFechaVencimiento());
			bp.saveOrUpdate(cc);
			return true;
		}
		return true;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return "ActualizacionCreditos";
	}

	@Override
	public String getInput() {
		return "ActualizacionCreditos";
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Integer getDesde() {
		return desde;
	}

	public void setDesde(Integer desde) {
		this.desde = desde;
	}

	public Integer getHasta() {
		return hasta;
	}

	public void setHasta(Integer hasta) {
		this.hasta = hasta;
	}

}
