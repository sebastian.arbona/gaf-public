package com.asf.cred.business;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;

import org.hibernate.HibernateException;
import org.kohsuke.rngom.digested.DAttributePattern;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.Moneda;
import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCalculo;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.cred.beans.BeanDesembolso;
import com.civitas.cred.metodos.MetodoCalculoAleman;
import com.civitas.cred.metodos.MetodoCalculoFrances;
import com.civitas.importacion.Fecha;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaIndice;
import com.nirven.creditos.hibernate.Bonificacion;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.CuotaBonTasa;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DesembolsoBonTasa;
import com.nirven.creditos.hibernate.DetalleCuota;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.IndiceValor;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class BonTasaDatosFinancieros implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	private BonTasa bonificacion;
	private Long idBonTasa;

	private Date fechaMutuo;
	private Double tasaCompensatorio;
	private Double tasaBonificacion;
	private Long idCompensatorio;
	private Long idIndice;
	private BonTasaIndice bonTasaIndice;
	private Indice indiceBT;

	private String tipoAmortizacion;
	private Double montoFinanciamiento;
	private String frecuenciaCapital;
	private String frecuenciaInteres;
	private Integer plazoCapital;
	private Integer plazoCompensatorio;
	private Date primerVencCapital;
	private Date primerVencInteres;
	private Long idEnte;
	private String abreviatura;
	private String frecuenciaCapitalStr;
	private String frecuenciaInteresStr;
	private boolean generada = false;
	private boolean gene = true;
	private boolean error = false;
	private DecimalFormat ft = new DecimalFormat("#################.##");
	private Long idMoneda;
	private boolean indice;
	private Integer contar = 0;
	private Integer correr = 0;
	private Double tasaBonificada;
	private Date fechaDesembolso;
	private Double importeDesembolso;
	private List<DesembolsoBonTasa> desembolsos;
	private Integer indiceDesembolso;
	private DesembolsoBonTasa desembolso;
	private boolean crearDesembolso;
	private boolean errorEjecutar;
	private Double cotizacion;
	private Boolean calculoBancoBice;
	private String operatoria;
	private String numeroPrestamo;
	private String bancosucursal_id;
	private Double valorFinalComp;

	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
			.getBusinessPersistance();
	private Moneda moneda;
	private Double importeReal;
	
	 // compensatorio
    private Indice indiceComp;
    private Double valorMasComp;
    private Double valorPorComp;
    private Integer diasAntesComp;
	
	public BonTasaDatosFinancieros() {
		desembolso = new DesembolsoBonTasa();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			buscarBonTasa();
			cargarPropiedades();
			comprobarCuotas();
			forward = "BonTasaDatosFinancieros";
		} else if (accion == "") {
			buscarBonTasa();
			indice = true;
			cargarPropiedades();
			comprobarCuotas();
			forward = "BonTasaDatosFinancieros";
		} else if (accion.equalsIgnoreCase("save")) {
			buscarBonTasa();
			cargarBonificacion();
			if (comprobarCampos()) {
				error = true;
				cargarPropiedades();
			} else {
				guardarDatos();
				buscarBonTasa();
				indice = false;
				cargarPropiedades();
				comprobarCuotas();
				borrarBonTasaDeSession();
			}
			forward = "BonTasaDatosFinancieros";
		} else if (accion.equalsIgnoreCase("loadIndice")) {
			buscarBonTasa();
			guardarCampos();// Guardo los campos que han sido completados y los
							// guardo en session.
			cargarBonTasaIndice();
			forward = "BonTasaIndice";
		} else if (accion.equalsIgnoreCase("saveIndice")) {
			buscarBonTasa2();
			indice = true;
			cargarPropiedades();
			forward = "BonTasaDatosFinancieros";
		} else if(accion.equals("mostrarDesembolsos")) {
			buscarBonTasa();
//			cargarBonificacion();
			listarDesembolsos();
			forward = "BonTasaDesembolso";
		} else if(accion.equals("crearDesembolso")) {
			if(comprobarCamposDesembolso()) {
				error = true;
				crearDesembolso = true;
			} else {
				buscarBonTasa();
				crearDesembolso();
				listarDesembolsos();
			}
			forward = "BonTasaDesembolso";
		} else if (accion.equals("prepararEjecucion")) {
			buscarBonTasa();
			cargarPropiedades();
			prepararEjecucion();
			forward = "BonTasaDesembolso";
		} else if(accion.equals("ejecutarDesembolso")) {
			buscarBonTasa();
			cargarPropiedades();
			if (comprobarCampos()) {
				errorEjecutar = true;
			} else {
				ejecutarDesembolso();
			}
			listarDesembolsos();
			forward = "BonTasaDesembolso";
		} else if(accion.equals("eliminarDesembolso")){
			eliminarDesembolso();
			buscarBonTasa();
			listarDesembolsos();
			forward = "BonTasaDesembolso";
		} else if(accion.equals("actualizarCotizacion")){
			buscarBonTasa();
			cargarBonificacion();
			cargarPropiedades();
			forward = "BonTasaDatosFinancieros";
		}
		return true;
	}
	
	private void prepararEjecucion() {
		desembolso = (DesembolsoBonTasa) bp.getById(DesembolsoBonTasa.class, desembolso.getId());
		importeDesembolso = desembolso.getImporte();
		importeReal = desembolso.getImporte();
	}
	
	private void borrarBonTasaDeSession() {
		SessionHandler.getCurrentSessionHandler()
						.getRequest()
						.getSession()
						.setAttribute("BonTasaDatosFinancieros.BonTasa", null);
	}

	private void eliminarDesembolso() {
		bp.delete(desembolso);
	}

	private void ejecutarDesembolso() {
		desembolso = (DesembolsoBonTasa) bp.getById(DesembolsoBonTasa.class, desembolso.getId());
		bp.begin();
		desembolso.setFecha(fechaDesembolso);
		desembolso.setImporteReal(importeReal);
		desembolso.setNumeroPrestamo(numeroPrestamo);
		generarCuotas();
		desembolso.setEstado(DesembolsoBonTasa.ESTADO_EJECUTADO);
		bp.update(desembolso);
		bp.commit();
		
		indice = false;
		
		//CambioEstado a Pendiente Resoluci�n
		
		//imputaci�n preventiva
//		comprobarCuotas();
	}
	
	@SuppressWarnings("unchecked")
	private void listarDesembolsos(){
		desembolsos = bp.getNamedQuery("DesembolsoBonTasa.findByBonTasa")
						.setEntity("bonTasa", bonificacion)
						.list();
	}

	private void crearDesembolso() {
		try {
			Integer ultimoNumero = (Integer) bp.getNamedQuery("DesembolsoBonTasa.findUltimoNumeroByBonTasa")
												.setEntity("bonTasa", bonificacion)
												.uniqueResult();
			if(ultimoNumero == null) {
				desembolso.setNumero(1);
			} else {
				desembolso.setNumero(++ultimoNumero);
			}
		}catch(NoResultException e) {
			desembolso.setNumero(1);
		}
		desembolso.setFecha(fechaDesembolso);
		desembolso.setEstado(DesembolsoBonTasa.ESTADO_CREADO);
		desembolso.setImporte(importeDesembolso);
		desembolso.setBonTasa(bonificacion);		
		bp.save(desembolso);
		listarDesembolsos();
	}	

	private void guardarCampos() {
		bonificacion.setTipoAmortizacion(tipoAmortizacion);
		bonificacion.setPrimerVencCapital(primerVencCapital);
		bonificacion.setPlazoCapital(plazoCapital);
		bonificacion.setPrimerVencInteres(primerVencInteres);
		bonificacion.setPlazoCompensatorio(plazoCompensatorio);
		bonificacion.setFrecuenciaCapital(frecuenciaCapital);
		bonificacion.setFrecuenciaInteres(frecuenciaInteres);
		Bancos banco = (Bancos) bp.createQuery(
				"Select b from Bancos b where b.codiBa=:id").setParameter("id",
				idEnte).uniqueResult();
		bonificacion.setBanco(banco);
		bonificacion.setMontoFinanciamiento(montoFinanciamiento);
		Moneda moneda = (Moneda) bp.createQuery(
				"Select m from Moneda m where m=:moneda").setParameter(
				"moneda", bp.getById(Moneda.class, idMoneda)).uniqueResult();
		bonificacion.setMoneda(moneda);
		bonificacion.setContar(contar);
		bonificacion.setCorrer(correr);
		bonificacion.setCalculoBancoBice(calculoBancoBice);
		bonificacion.setTasaCompensatorio(tasaCompensatorio);
		bonificacion.setTasaBonificacion(tasaBonificacion);
		bonificacion.setBancosucursal_id(bancosucursal_id);
		indice = true;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("BonTasaDatosFinancieros.BonTasa", bonificacion);
	}

	private boolean comprobarCampos() {
		boolean incompleto = false;
		if (primerVencCapital == null) {
			incompleto = true;
		} else if (plazoCapital == 0) {
			incompleto = true;
		} else if (primerVencInteres == null) {
			incompleto = true;
		} else if (plazoCompensatorio == 0) {
			incompleto = true;
		} else if (montoFinanciamiento == 0.0) {
			incompleto = true;
		}	else {
			incompleto = false;
		}
		return incompleto;
	}
	
	private boolean comprobarCamposDesembolso() {
		boolean incompleto = false;
		if (fechaDesembolso == null) {
			incompleto = true;
		} else if (importeDesembolso == null || importeDesembolso <= 0.0) {
			incompleto = true;
		} 
		return incompleto;
	}

	private void comprobarCuotas() {
		List<CuotaBonTasa> cuotas = (List<CuotaBonTasa>) bp.createQuery(
				"Select c from CuotaBonTasa c where c.bonTasa=:bonTasa")
				.setParameter("bonTasa", bonificacion).list();
		if (cuotas.isEmpty()) {
			generada = false;
			gene = true;
		} else {
			gene = false;
			generada = true;
		}
	}

	private void cargarPropiedades() {
		if (indice) {// Si viene del indice, carga los datos de las cariables
						// guardadas anteriormente
			bonificacion = (BonTasa) SessionHandler.getCurrentSessionHandler()
					.getRequest().getSession().getAttribute(
							"BonTasaDatosFinancieros.BonTasa");
		}
		
		tipoAmortizacion = bonificacion.getTipoAmortizacion();
		primerVencCapital = bonificacion.getprimerVencCapital();
		plazoCapital = bonificacion.getPlazoCapital();
		primerVencInteres = bonificacion.getPrimerVencInteres();
		plazoCompensatorio = bonificacion.getPlazoCompensatorio();
		frecuenciaCapital = bonificacion.getFrecuenciaCapital();
		frecuenciaInteres = bonificacion.getFrecuenciaInteres();
		abreviatura = bonificacion.getMoneda().getAbreviatura();
		idMoneda = bonificacion.getMoneda().getId();
		moneda = bonificacion.getMoneda();

		indiceComp = bonificacion.getIndiceComp();
		valorMasComp = bonificacion.getValorMasComp();
		valorPorComp = bonificacion.getValorPorComp();
		diasAntesComp = bonificacion.getDiasAntesComp();
		bancosucursal_id = bonificacion.getBancosucursal_id();
		
		
		try{
			cotizacion = (Double) bp.getNamedQuery("Cotizacion.findByMoneda")
									.setEntity("moneda", bonificacion.getMoneda())
									.uniqueResult();
		}catch(NoResultException e){
			cotizacion = null;
		}catch(NonUniqueResultException e){
			cotizacion = null;
		}
		montoFinanciamiento = bonificacion.getMontoFinanciamiento();
		
		//tasaCompensatorio = bonificacion.getTasaCompensatorio();
		tasaBonificacion = bonificacion.getTasaBonificacion();
		
		tasaBonificada = bonificacion.getTasaBonificada();
		contar = bonificacion.getContar() != null ? bonificacion.getContar() : 0;
		correr = bonificacion.getCorrer() != null ? bonificacion.getCorrer() : 0;
		frecuenciaCapitalStr = bonificacion.getFrecuenciaCapital();
		frecuenciaInteresStr = bonificacion.getFrecuenciaInteres();
		calculoBancoBice = bonificacion.getCalculoBancoBice();
		operatoria = bonificacion.getOperatoria();
		fechaMutuo = bonificacion.getFechaMutuo();
	}

	private void cargarBonificacion() {
		bonificacion.setTipoAmortizacion(tipoAmortizacion);
		bonificacion.setPrimerVencCapital(primerVencCapital);
		bonificacion.setPlazoCapital(plazoCapital);
		bonificacion.setPrimerVencInteres(primerVencInteres);
		bonificacion.setPlazoCompensatorio(plazoCompensatorio);
		bonificacion
				.setFrecuenciaCapital(frecuenciaCapitalStr);
		bonificacion
				.setFrecuenciaInteres(frecuenciaInteresStr);
		bonificacion.setMontoFinanciamiento(montoFinanciamiento);
		//bonificacion.setTasaCompensatorio(tasaCompensatorio);
		bonificacion.setTasaBonificacion(tasaBonificacion);
		bonificacion.setOperatoria(operatoria);
		bonificacion.setFechaMutuo(fechaMutuo);
		
		bonificacion.setIndiceComp(indiceComp);
		bonificacion.setValorMasComp(valorMasComp);
		bonificacion.setValorPorComp(valorPorComp);
		bonificacion.setDiasAntesComp(diasAntesComp);
		bonificacion.setBancosucursal_id(bancosucursal_id);
		
		
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
		.setAttribute("BonTasaDatosFinancieros.BonTasa", bonificacion);
	}

	private BonTasaIndice buscarBonTasaIndice() {
		BonTasaIndice i = (BonTasaIndice) bp.createQuery(
				"Select b from BonTasaIndice b where b.bonTasa=:bonTasa")
				.setParameter("bonTasa", bonificacion).uniqueResult();
		return i;
	}

	private void cargarIndice() {
		indiceBT = (Indice) bp.getById(Indice.class, idIndice);
	}

	private void buscarBonTasa() {
		bonificacion = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
		if (bonificacion.getMoneda() == null) {
			this.bonificacion.setMoneda_id(1L);
		}
		//si la bonificacion no tiene indice trae el del convenio asociado
		if(bonificacion.getIndiceComp()==null) {
			bonificacion.setIndiceComp(bonificacion.getConvenio().getIndiceComp()); 
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
		.setAttribute("BonTasaDatosFinancieros.BonTasa", bonificacion);
	}
	
	private void buscarBonTasa2() {
		bonificacion = (BonTasa) SessionHandler.getCurrentSessionHandler().getRequest()
						.getSession().getAttribute("BonTasaDatosFinancieros.BonTasa");
//		BonTasaIndice compensatorio = bonificacion.findTasa("1");
//		if (compensatorio != null) {
//			idCompensatorio = compensatorio.getIndice().getId();
//			tasaCompensatorio = compensatorio.getIndice().getValorInicial();
//		}
	}

	private void cargarBonTasaIndice() {
		if (idCompensatorio != null && idCompensatorio != 0) {
			bonTasaIndice = (BonTasaIndice) bp.getById(BonTasaIndice.class,
					idCompensatorio);
		} else {
			bonTasaIndice = new BonTasaIndice();
		}
	}

	private void guardarDatos() {

		BonTasa bonTasa = (BonTasa) bp.getById(BonTasa.class, bonificacion
				.getId());

		bonTasa.setTipoAmortizacion(bonificacion.getTipoAmortizacion());
		bonTasa.setPrimerVencCapital(bonificacion.getprimerVencCapital());
		bonTasa.setPrimerVencInteres(bonificacion.getPrimerVencInteres());
		bonTasa.setPlazoCapital(bonificacion.getPlazoCapital());
		bonTasa.setPlazoCompensatorio(bonificacion.getPlazoCompensatorio());
		bonTasa.setFrecuenciaCapital(bonificacion.getFrecuenciaCapital());
		bonTasa.setFrecuenciaInteres(bonificacion.getFrecuenciaInteres());
		bonTasa.setMoneda(bonificacion.getMoneda());
		bonTasa.setMontoFinanciamiento(bonificacion.getMontoFinanciamiento());
		bonTasa.setBanco(bonificacion.getBanco());
		bonTasa.setContar(contar);
		bonTasa.setCorrer(correr);
		//bonTasa.setTasaCompensatorio(tasaCompensatorio);
		bonTasa.setTasaBonificacion(tasaBonificacion);
		bonTasa.setCalculoBancoBice(calculoBancoBice);
		bonTasa.setOperatoria(operatoria);
		
		bonTasa.setIndiceComp(indiceComp);
		bonTasa.setValorMasComp(valorMasComp);
		bonTasa.setValorPorComp(valorPorComp);
		bonTasa.setDiasAntesComp(diasAntesComp);
		bonTasa.setFechaMutuo(fechaMutuo);
		bonTasa.setBancosucursal_id(bancosucursal_id);
		idBonTasa = bonTasa.getId();
		bp.saveOrUpdate(bonTasa);
	}

	public String getIndiceValorStr() {
		if (bonTasaIndice != null && bonTasaIndice.getIndice() != null) {
			return String.format("%3.2f%%", bonTasaIndice.getIndice()
					.getValorInicial() * 100);
		}
		return String.format("%3.2f%%", 0.00);
	}

	private String buscarFrecuencia(String frecuencia) {
		Tipificadores tip = (Tipificadores) bp
				.createQuery(
						"Select t from Tipificadores t where t.categoria=:categoria and t.descripcion=:desc")
				.setParameter("categoria", "amortizacion.periodicidad")
				.setParameter("desc", frecuencia).uniqueResult();
		return tip.getCodigo();
	}

	private Double buscarFrecuenciaDouble(String frecuencia) {
		Tipificadores tip = (Tipificadores) bp
				.createQuery(
						"Select t from Tipificadores t where t.categoria=:categoria and t.descripcion=:desc")
				.setParameter("categoria", "amortizacion.periodicidad")
				.setParameter("desc", frecuencia).uniqueResult();
		if (tip != null && tip.getCodigo() != null
				&& !tip.getCodigo().isEmpty()) {
			return Double.parseDouble(tip.getCodigo());
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	private void generarCuotas() {
		
		this.tasaCompensatorio = new CreditoHandler().buscarValorIndicePromedio(bonificacion.getIndiceComp().getId()) * 100;
		this.tasaCompensatorio = (this.tasaCompensatorio * bonificacion.getValorPorComp()) + bonificacion.getValorMasComp();
		if(this.tasaCompensatorio>bonificacion.getIndiceComp().getValorMaximo()*100) this.tasaCompensatorio = bonificacion.getIndiceComp().getValorMaximo()*100; 
		
		BeanCalculo beanCalculo = new BeanCalculo(this.primerVencCapital,
				this.primerVencInteres, this.tasaCompensatorio,
				this.plazoCapital, this.plazoCompensatorio, new Integer(frecuenciaCapitalStr), 
				new Integer(frecuenciaInteresStr), desembolso.getFecha(),
				desembolso.getImporteReal());
		double[] tb = new  double[1];
		tb[0] = tasaBonificacion;
		beanCalculo.setTasasBonificacion(tb);
		ArrayList<DesembolsoBonTasa> desembolsos = new ArrayList<DesembolsoBonTasa>();
		desembolsos.add(desembolso);
		BeanDesembolso beanDesembolso = new BeanDesembolso();
		beanDesembolso.setFecha(desembolso.getFecha());
		beanDesembolso.setImporte(desembolso.getImporte());
		beanDesembolso.setNumero(desembolso.getNumero());
		ArrayList<BeanDesembolso> beanDesembolsos = new ArrayList<BeanDesembolso>();
		beanDesembolsos.add(beanDesembolso);
		beanCalculo.setDesembolsos(beanDesembolsos);
		ArrayList<BeanCuota> cuotas = new ArrayList<BeanCuota>();
		if (this.validate()) {
			if (tipoAmortizacion.equalsIgnoreCase("A")) {
				MetodoCalculoAleman metodo = new MetodoCalculoAleman();
				cuotas = metodo.amortizarBonTasa(beanCalculo);
//			} else if (tipoAmortizacion.equalsIgnoreCase("F") && calculoBancoBice) {
//				MetodoCalculoFrances metodo = new MetodoCalculoFrances();
//				cuotas = metodo.amortizarBonTasaBice(desembolsos,
//						tasaCompensatorio, tasaBonificada, plazoCompensatorio.doubleValue(),
//						contar, correr, primerVencInteres,
//						new Double(frecuenciaInteresStr));
			} else if ("F".equals(tipoAmortizacion)){
				MetodoCalculoFrances metodo = new MetodoCalculoFrances();
				cuotas = metodo.amortizarBonTasaCredicoop(desembolsos,
						tasaCompensatorio/100, tasaBonificada/100, plazoCompensatorio.doubleValue(),
						contar, correr, primerVencInteres,
						new Double(frecuenciaInteresStr));
			} else if ("FB".equals(tipoAmortizacion)){
				MetodoCalculoFrances metodo = new MetodoCalculoFrances();
				cuotas = metodo.amortizarBonTasaFB(desembolsos,
						tasaCompensatorio/100, tasaBonificada/100, plazoCompensatorio.doubleValue(),
						contar, correr, primerVencInteres,
						new Double(frecuenciaInteresStr));
			} 
		}

		List<CuotaBonTasa> cuotasAnteriores = bp.getNamedQuery("CuotaBonTasa.findByBonTasa")
												.setEntity("bonTasa", bonificacion)
												.list();
		CuotaBonTasa cuota;
		// Crear o actualizar las cuotas
		for (BeanCuota beanCuota : cuotas) {
//        	if (!cuotasAnteriores.isEmpty()) {
//        		cuota = cuotasAnteriores.get(beanCuota.getNumero() - 1); // ya existe
//        	} else {
        		cuota = new CuotaBonTasa(); // no se han generado cuotas
//        	}
        	// si se genera por primera vez o si la cuota ya esta generada y todavia no vence
        	// actualizar la cuota
        	if (cuota.getFechaVencimiento() == null || cuota.getFechaVencimiento().after(desembolso.getFecha())) {
        		if(cuota.getFechaVencimiento() == null) {
        			cuota.setBonTasa(bonificacion);
        			cuota.setFechaVencimiento(beanCuota.getFechaVencimiento());
        			cuota.setCapital(beanCuota.getCapital());
        			cuota.setCompensatorio(beanCuota.getInteres());
        			cuota.setMoratorio(beanCuota.getMoratorio());
        			cuota.setNumero(beanCuota.getNumero());
        			cuota.setPunitorio(beanCuota.getPunitorio());
        			cuota.setTasaCompensatorio(beanCuota.getTasaAmortizacion());
        			cuota.setTipoEstadoBonCuota("Pendiente");
        			cuota.setDesembolsoBonTasa(desembolso);
        			cuota.setSubsidio(beanCuota.getSubsidio());
        		} else {
        			cuota.setFechaVencimiento(beanCuota.getFechaVencimiento());
        			cuota.setCapital(beanCuota.getCapital() + cuota.getCapital());
        			cuota.setCompensatorio(beanCuota.getInteres() + cuota.getCompensatorio());
        			cuota.setSubsidio(beanCuota.getSubsidio() + cuota.getSubsidio());
        			cuota.setDesembolsoBonTasa(desembolso);
        		}
    			bp.save(cuota);
        	}
		}

	}

	/*
	 * BeanCalculo bean = new BeanCalculo((bonificacion.getPrimerVencCapital()
	 * != null) ? bonificacion.getPrimerVencCapital() : new Date(),
	 * (bonificacion.getPrimerVencInteres() != null) ?
	 * bonificacion.getPrimerVencInteres() : new Date(), tasaFinal,
	 * (bonificacion.getPlazoCapital() != null ? new
	 * Integer(bonificacion.getPlazoCapital()) : 1),
	 * (bonificacion.getPlazoCompensatorio() != null ? new
	 * Integer(bonificacion.getPlazoCompensatorio()) : 1),
	 * (bonificacion.getFrecuenciaCapital() != null) ? new
	 * Integer(bonificacion.getFrecuenciaCapital()) : 1,
	 * (bonificacion.getFrecuenciaInteres() != null) ? new
	 * Integer(bonificacion.getFrecuenciaInteres()) : 1,
	 * bonificacion.getFechaDesembolso(),
	 * bonificacion.getMontoFinanciamiento()); List<BeanCuota> cuotas;
	 * MetodoCalculoAleman calculo = new MetodoCalculoAleman(); cuotas=
	 * calculo.amortizarBonTasa(bean); return cuotas;
	 */

	private double calcularNuevoValor(double resto, double valor) {
		double nuevo = resto / (plazoCapital - 1) + valor;
		return Double.valueOf(ft.format(nuevo).replaceAll(",", "."));
	}

	private CuotaBonTasa asignarFechaVenc(CuotaBonTasa cuota, int i) {
		if (i == 1) {
			cuota.setFechaVencimiento(primerVencCapital);
		} else {
			Date fecha = new Date();
			cuota.setFechaVencimiento(Fecha.sumarDias(fecha,
					(calcularFrecuencia(frecuenciaCapitalStr) * (i - 1))));
		}
		return cuota;
	}

	private int calcularFrecuencia(String frecuencia) {
		int frec = 0;
		if (frecuencia.equalsIgnoreCase("mensual")) {
			frec = 30;
		} else if (frecuencia.equalsIgnoreCase("Bimestral")) {
			frec = 60;
		} else if (frecuencia.equalsIgnoreCase("Trimestral")) {
			frec = 90;
		} else if (frecuencia.equalsIgnoreCase("Cuatrimestral")) {
			frec = 120;
		} else if (frecuencia.equalsIgnoreCase("Semestral")) {
			frec = 6 * 30;
		} else if (frecuencia.equalsIgnoreCase("Anual")) {
			frec = 365;
		}
		return frec;
	}

	public HashMap<String, Object> getErrors() {
		return null;
	}

	public String getForward() {
		return forward;
	}

	public String getTasaCompensatorioStr() {
		if (tasaCompensatorio != null) {
			return String.format("%.2f%%", tasaCompensatorio * 100);
		} else {
			return String.format("%.2f%%", 0.0);
		}
	}

	public String getInput() {
		return forward;
	}

	public Object getResult() {
		return result;
	}

	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getFrecuenciaCapitalStr() {
		return frecuenciaCapitalStr;
	}

	public void setFrecuenciaCapitalStr(String frecuenciaCapitalStr) {
		this.frecuenciaCapitalStr = frecuenciaCapitalStr;
	}

	public String getFrecuenciaInteresStr() {
		return frecuenciaInteresStr;
	}

	public void setFrecuenciaInteresStr(String frecuenciaInteresStr) {
		this.frecuenciaInteresStr = frecuenciaInteresStr;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	public BonTasaIndice getBonTasaIndice() {
		return bonTasaIndice;
	}

	public void setBonTasaIndice(BonTasaIndice bonTasaIndice) {
		this.bonTasaIndice = bonTasaIndice;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Double getTasaCompensatorio() {
		return tasaCompensatorio;
	}

	public void setTasaCompensatorio(Double tasaCompensatorio) {
		this.tasaCompensatorio = tasaCompensatorio;
	}

	public Double getTasaBonificacion() {
		return tasaBonificacion;
	}

	public void setTasaBonificacion(Double tasaBonificacion) {
		this.tasaBonificacion = tasaBonificacion;
	}

	public Long getIdCompensatorio() {
		return idCompensatorio;
	}

	public void setIdCompensatorio(Long idCompensatorio) {
		this.idCompensatorio = idCompensatorio;
	}

	public Indice getIndiceBT() {
		return indiceBT;
	}

	public void setIndiceBT(Indice indiceBT) {
		this.indiceBT = indiceBT;
	}

	public Long getIdIndice() {
		return idIndice;
	}

	public void setIdIndice(Long idIndice) {
		this.idIndice = idIndice;
	}

	public String getTipoAmortizacion() {
		return tipoAmortizacion;
	}

	public void setTipoAmortizacion(String tipoAmortizacion) {
		this.tipoAmortizacion = tipoAmortizacion;
	}

	public Double getMontoFinanciamiento() {
		return montoFinanciamiento;
	}

	public void setMontoFinanciamiento(Double montoFinanciamiento) {
		this.montoFinanciamiento = montoFinanciamiento;
	}

	public String getFrecuenciaCapital() {
		return frecuenciaCapital;
	}

	public void setFrecuenciaCapital(String frecuenciaCapital) {
		this.frecuenciaCapital = frecuenciaCapital;
	}

	public String getFrecuenciaInteres() {
		return frecuenciaInteres;
	}

	public void setFrecuenciaInteres(String frecuenciaInteres) {
		this.frecuenciaInteres = frecuenciaInteres;
	}

	public Integer getPlazoCapital() {
		return plazoCapital;
	}

	public void setPlazoCapital(Integer plazoCapital) {
		this.plazoCapital = plazoCapital;
	}

	public Integer getPlazoCompensatorio() {
		return plazoCompensatorio;
	}

	public void setPlazoCompensatorio(Integer plazoCompensatorio) {
		this.plazoCompensatorio = plazoCompensatorio;
	}

	public Date getPrimerVencCapital() {
		return primerVencCapital;
	}

	public void setPrimerVencCapital(Date primerVencCapital) {
		this.primerVencCapital = primerVencCapital;
	}

	public Date getPrimerVencInteres() {
		return primerVencInteres;
	}

	public void setPrimerVencInteres(Date primerVencInteres) {
		this.primerVencInteres = primerVencInteres;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public String getPrimerVencCapitalStr() {
		return DateHelper.getString(this.getPrimerVencCapital());
	}

	public void setPrimerVencCapitalStr(String s) {
		this.setPrimerVencCapital((Date) DateHelper.getDate(s));
	}

	public String getPrimerVencInteresStr() {
		return DateHelper.getString(this.getPrimerVencInteres());
	}

	public void setPrimerVencInteresStr(String s) {
		this.setPrimerVencInteres((Date) DateHelper.getDate(s));
	}

	public Long getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(Long idEnte) {
		this.idEnte = idEnte;
	}

	public boolean isGenerada() {
		return generada;
	}

	public void setGenerada(boolean generada) {
		this.generada = generada;
	}

	public DecimalFormat getFt() {
		return ft;
	}

	public void setFt(DecimalFormat ft) {
		this.ft = ft;
	}

	public boolean isGene() {
		return gene;
	}

	public void setGene(boolean gene) {
		this.gene = gene;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isIndice() {
		return indice;
	}

	public void setIndice(boolean indice) {
		this.indice = indice;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public Integer getContar() {
		return contar;
	}

	public void setContar(Integer contar) {
		this.contar = contar;
	}

	public Integer getCorrer() {
		return correr;
	}

	public void setCorrer(Integer correr) {
		this.correr = correr;
	}

	public Double getTasaBonificada() {
		return tasaBonificada;
	}

	public void setTasaBonificada(Double tasaBonificada) {
		this.tasaBonificada = tasaBonificada;
	}

	public String getTasaBonificadaStr() {
		if (tasaBonificada == null) {
			tasaBonificada = 0.0;
		}
		return String.format("%.2f%%", tasaBonificada * 100);
	}

	public void setTasaBonificadaStr(String tasa) {
		tasaBonificada = new Double(tasa.replace(",", ".").replace("%", "")) / 100;
	}
	
	public String getFechaDesembolsoStr() {
		if(fechaDesembolso != null)
			return DateHelper.getString(fechaDesembolso);
		else 
			return null;
	}

	public void setFechaDesembolsoStr(String fechaDesembolsoStr) {
		fechaDesembolso = DateHelper.getDate(fechaDesembolsoStr);
	}

	public Date getFechaDesembolso() {
		return fechaDesembolso;
	}

	public void setFechaDesembolso(Date fechaDesembolso) {
		this.fechaDesembolso = fechaDesembolso;
	}

	public Double getImporteDesembolso() {
		return importeDesembolso;
	}

	public void setImporteDesembolso(Double importeDesembolso) {
		this.importeDesembolso = importeDesembolso;
	}

	public List<DesembolsoBonTasa> getDesembolsos() {
		return desembolsos;
	}

	public void setDesembolsos(List<DesembolsoBonTasa> desembolsos) {
		this.desembolsos = desembolsos;
	}

	public Integer getIndiceDesembolso() {
		return indiceDesembolso;
	}

	public void setIndiceDesembolso(Integer indiceDesembolso) {
		this.indiceDesembolso = indiceDesembolso;
	}
	
	public boolean isCrearDesembolso() {
		return crearDesembolso;
	}
	
	public void setCrearDesembolso(boolean crearDesembolso) {
		this.crearDesembolso = crearDesembolso;
	}
	
	public DesembolsoBonTasa getDesembolso() {
		return desembolso;
	}
	
	public void setDesembolso(DesembolsoBonTasa desembolso) {
		this.desembolso = desembolso;
	}
	
	public boolean isErrorEjecutar() {
		return errorEjecutar;
	}
	
	public void setErrorEjecutar(boolean errorEjecutar) {
		this.errorEjecutar = errorEjecutar;
	}
	
	public void setCotizacion(Double cotizacion) {
		this.cotizacion = cotizacion;
	}
	public Double getCotizacion() {
		return cotizacion;
	}
	public Boolean getCalculoBancoBice() {
		return calculoBancoBice;
	}
	public void setCalculoBancoBice(Boolean calculoBancoBice) {
		this.calculoBancoBice = calculoBancoBice;
	}
	public String getOperatoria() {
		return operatoria;
	}
	public void setOperatoria(String operatoria) {
		this.operatoria = operatoria;
	}

	public String getFechaMutuo() {
		return DateHelper.getString(fechaMutuo);
	}

	public void setFechaMutuo(String fechaMutuo) {
		this.fechaMutuo = DateHelper.getDate(fechaMutuo);
	}

	public Moneda getMoneda() {
		return moneda;
	}

	public Double getImporteReal() {
		return importeReal;
	}

	public void setImporteReal(Double importeReal) {
		this.importeReal = importeReal;
	}

	public String getNumeroPrestamo() {
		return numeroPrestamo;
	}

	public void setNumeroPrestamo(String numeroPrestamo) {
		this.numeroPrestamo = numeroPrestamo;
	}

	public Indice getIndiceComp() {
		return indiceComp;
	}

	public void setIndiceComp(Indice indiceComp) {
		this.indiceComp = indiceComp;
	}

	public Double getValorPorComp() {
		return valorPorComp;
	}

	public void setValorPorComp(Double valorPorComp) {
		this.valorPorComp = valorPorComp;
	}

	public Double getValorMasComp() {
		return valorMasComp;
	}

	public void setValorMasComp(Double valorMasComp) {
		this.valorMasComp = valorMasComp;
	}

	public Integer getDiasAntesComp() {
		return diasAntesComp;
	}

	public void setDiasAntesComp(Integer diasAntesComp) {
		this.diasAntesComp = diasAntesComp;
	}
	
	
	// compensatorio
    public Long getIdIndiceComp() {
    	if (indiceComp != null) {
            return indiceComp.getId();
        } else {
            return null;
        }
    }

    public void setIdIndiceComp(Long id) {
        BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        indiceComp = (Indice) bp.getById(Indice.class, id);
    }

    public String getIndiceValorComp() {
        if (indiceComp != null) {
            return indiceComp.getValorInicialStr();
        } else {
            return new String("0,0");
        }
    }

    
    public String getValorFinalComp() {
        return String.format("%.2f%%", calcularTasaFinalComp(new Date()));
    }

    public String getValorMasCompStr() {
        if (valorMasComp == null) {
            return String.format("%.2f%%", 0.0);
        } else {
            return String.format("%.2f%%", valorMasComp);
        }

    }

    public void setValorMasCompStr(String valor) {
        valorMasComp = new Double(valor.replace(",", ".").replace("%", ""));
    }

    public String getValorPorCompStr() {
        if (valorPorComp == null) {
            return new Double(1.0).toString().replace(".", ",");
        } else {
            return valorPorComp.toString().replace(".", ",");
        }
    }

    public void setValorPorCompStr(String valor) {
        valorPorComp = new Double(valor.replace(",", "."));
    }


    public double calcularTasaFinalComp(Date fecha) {
        if (getIndiceComp() != null) {
            BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
            setIndiceComp((Indice) bp.getById(Indice.class, getIndiceComp().getId()));
            Double valor = getIndiceComp().getValorInicial();

            // buscar valores del indice, vigentes a la fecha
            List<IndiceValor> valores = bp.createQuery("select v from IndiceValor v where v.indice.id = :idIndice and v.fecha <= :fecha order by v.fecha desc")
                    .setLong("idIndice", getIndiceComp().getId()).setDate("fecha", fecha).setMaxResults(1).list();
            if (!valores.isEmpty()) {
                IndiceValor valorIndice = valores.get(0); // es el ultimo por
                                                          // fecha
                valor = valorIndice.getValorPromedio(); // utilizo el IndiceValor en vez
                                                // del inicial
            }
            double tasaFinal = valor * 100;
            if (getValorMasComp() != null && getValorPorComp() != null) {
                tasaFinal = (tasaFinal + getValorMasComp()) * getValorPorComp();
            }
            return tasaFinal;
        }
        return 0;
    }

	public String getBancosucursal_id() {
		return bancosucursal_id;
	}

	public void setBancosucursal_id(String bancosucursal_id) {
		this.bancosucursal_id = bancosucursal_id;
	}
/*
	public Double getValorFinalComp() {
		return valorFinalComp;
	}

	public void setValorFinalComp(Double valorFinalComp) {
		this.valorFinalComp = valorFinalComp;
	}

*/
	
}
