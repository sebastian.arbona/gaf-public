/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asf.cred.business;

import com.civitas.hibernate.persona.*;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author cnoguerol
 */
public class Domicilio implements IProcess {
    // =========================ATRIBUTOS==================================
    private String forward = "domicilioList";
    private HashMap<String, Object> errors;
    private String accion = "";
    private BusinessPersistance bp;
    private Persona personaTitular;
    private com.civitas.hibernate.persona.Domicilio domicilio;
    private List<com.civitas.hibernate.persona.Domicilio> listaDomicilios;
    private Long idpais;

    // =========================CONSTRUCTORES================================
    public Domicilio() {
        this.errors = new HashMap<String, Object>();
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        this.domicilio = new com.civitas.hibernate.persona.Domicilio();
        this.domicilio.setProvincia_id(1L);
        this.personaTitular = new Persona();
    }

    // =========================FUNCIONALIDAD==================================
    public boolean doProcess() {
        if (this.getAccion().equalsIgnoreCase("asignar")) {
            this.forward = "domicilio";
        } else if (this.getAccion().equalsIgnoreCase("guardar")) {
            this.validarDomicilio();
            if (this.errors.isEmpty()) {
                this.asignarDomicilio();
                if (this.errors.isEmpty()) {
                    this.forward = "domicilioList";
                } else {
                    this.forward = "domicilio";
                }
            } else {
                this.forward = "domicilio";
            }
        } else if (this.getAccion().equalsIgnoreCase("cancelar")) {
            this.forward = "domicilioList";
        } else if (this.getAccion().equalsIgnoreCase("eliminar")) {
            this.eliminarDomicilio();
        } else if (this.getAccion().equalsIgnoreCase("load")) {
            this.cargarDomicilio();
            this.forward = "domicilio";
        }
        this.listarDomicilios();
        return this.errors.isEmpty();
    }

    // =========================UTILIDADES PRIVADAS==============================
    private void validarDomicilio() {
        if (("").equalsIgnoreCase(this.getDomicilio().getProvinciaStr())) {
            this.errors.put("Domicilio.Provincia.vacia", "Domicilio.Provincia.vacia");
        }
        if (("").equalsIgnoreCase(this.getDomicilio().getLocalidadStr())) {
            this.errors.put("Domicilio.Localidad.vacia", "Domicilio.Localidad.vacia");
        }
        if (("").equalsIgnoreCase(this.getDomicilio().getCalleStr())) {
            this.errors.put("Domicilio.Calle.vacia", "Domicilio.Calle.vacia");
        }
        if (this.getDomicilio().getTipo() == null) {
            this.errors.put("Domicilio.Tipo.vacio", "Domicilio.Tipo.vacio");
        }
    }

    private void asignarDomicilio() {
        try {
            this.setPersonaTitular((Persona) this.bp.getById(Persona.class, this.getPersonaTitular().getId()));
            this.getDomicilio().setPersona(this.getPersonaTitular());
            this.bp.saveOrUpdate(this.getDomicilio());
            if (this.domicilio.getTipo().equalsIgnoreCase(DirectorHelper.getString("DomPer"))) {
                Persona persona = (Persona) this.bp.getById(Persona.class, this.getDomicilio().getPersona().getId());
                persona.setDomicilioCompleto(this.getDomicilio());
                this.bp.saveOrUpdate(persona);
            }
            this.setDomicilio(new com.civitas.hibernate.persona.Domicilio());
        }
        catch (Exception e) {
            this.errors.put("Domicilio.Error.guardar", "Domicilio.Error.guardar");
            e.printStackTrace();
        }
    }

    private void eliminarDomicilio() {
        if (this.domicilio.getId() != null) {
            try {
                this.bp.delete(this.domicilio);
            }
            catch (Exception e) {
                this.errors.put("Domicilio.Error.eliminar", "Domicilio.Error.eliminar");
                e.printStackTrace();
            }
        }
    }

    private void cargarDomicilio() {
        if (this.getDomicilio() != null) {
            try {
                this.setDomicilio((com.civitas.hibernate.persona.Domicilio) this.bp.getById(com.civitas.hibernate.persona.Domicilio.class, this.getDomicilio().getId()));
            }
            catch (Exception e) {
                this.setDomicilio(new com.civitas.hibernate.persona.Domicilio());
                this.errors.put("Domicilio.Error.cargar", "Domicilio.Error.carga");
                e.printStackTrace();
            }
        }
    }

    private void listarDomicilios() {
        if (this.personaTitular != null && this.personaTitular.getId().longValue() > 0) {
            try {
                this.personaTitular = (Persona) this.bp.getById(Persona.class, this.personaTitular.getId());
                String consulta = "SELECT d FROM Domicilio d";
                consulta += " WHERE d.persona.id = '" + this.personaTitular.getId().longValue() + "'";
                this.listaDomicilios = this.bp.getByFilter(consulta);
            }
            catch (Exception e) {
                this.listaDomicilios = new ArrayList<com.civitas.hibernate.persona.Domicilio>();
            }
        }
    }

    // =========================GETTERS y SETTERS==================================
    public HashMap<String, Object> getErrors() {
        return this.errors;
    }

    public Object getResult() {
        return null;
    }

    public String getInput() {
        return this.forward;
    }

    public String getForward() {
        return this.forward;
    }

    public String getAccion() {
        return this.accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public com.civitas.hibernate.persona.Domicilio getDomicilio() {
        return this.domicilio;
    }

    public void setDomicilio(com.civitas.hibernate.persona.Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public Persona getPersonaTitular() {
        return this.personaTitular;
    }

    public void setPersonaTitular(Persona personaTitular) {
        this.personaTitular = personaTitular;
    }

    public List<com.civitas.hibernate.persona.Domicilio> getListaDomicilios() {
        return this.listaDomicilios;
    }

    public void setListaDomicilios(List<com.civitas.hibernate.persona.Domicilio> listaDomicilios) {
        this.listaDomicilios = listaDomicilios;
    }

    public Long getIdpais() {
        return this.idpais;
    }

    public void setIdpais(Long idpais) {
        this.idpais = idpais;
    }

    // =========================VALIDACIONES==================================
    public boolean validate() {
        return this.errors.isEmpty();
    }
}
