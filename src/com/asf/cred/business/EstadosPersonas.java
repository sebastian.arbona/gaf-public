package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import com.asf.cred.estados.ComparadorEstaObs;
import com.asf.cred.estados.EstaObs;
import com.civitas.hibernate.persona.*;
import com.civitas.hibernate.persona.creditos.*;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;

/**
 * @author eselman
 */

public class EstadosPersonas implements IProcess {
	// ==========================ATRIBUTOS============================
	public static String separador = "@";
	public static Long agregaEstado = 1L;
	public static Long agregaObs = 2L;
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "EstadosPersonasList";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Persona persona;
	private Estado estado;
	private Observacion observacion;
	private Date fecha = new Date(System.currentTimeMillis());
	private String obsEstado;
	private String detalleObservacion;
	private List<EstaObs> estadosPersonas;
	private Long agrega;

	// =======================CONSTRUCTORES=========================
	public EstadosPersonas() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		this.persona = new Persona();
		this.estado = new Estado();
		this.observacion = new Observacion();
	}

	// ======================FUNCIONALIDADES========================
	public boolean doProcess() {
		if (this.accion.equals("nuevo")) {
			this.forward = "EstadosPersonas";
		} else if (this.accion.equals("guardar")) {
			if (this.agrega.longValue() == EstadosPersonas.agregaEstado
					.longValue()) {
				this.guardarNuevoEstado();
			} else if (this.agrega.longValue() == EstadosPersonas.agregaObs
					.longValue()) {
				this.guardarObservacion();
			}

			if (this.errores.isEmpty()) {
				this.forward = "EstadosPersonasList";
			} else {
				this.forward = "EstadosPersonas";
			}
		} else if (this.accion.equals("cancelar")) {
			this.forward = "EstadosPersonasList";
		}

		this.listarEstadosPersonas();

		return this.errores.isEmpty();
	}

	// =====================UTILIDADES PRIVADAS=====================
	private void listarEstadosPersonas() {
		ComparadorEstaObs comparador = new ComparadorEstaObs();
		SortedMap<String, EstaObs> estadosobs = new TreeMap<String, EstaObs>(
				comparador);

		String key = "";
		EstaObs registro = null;

		if (this.persona != null) {
			try {
				this.persona = (Persona) this.bp.getById(Persona.class,
						this.persona.getId());

				String consulta = "";
				consulta += "SELECT ep FROM EstaPer ep WHERE";
				consulta += " ep.persona.id=" + this.persona.getId()
						+ " order By ep.fechaDesde DESC";

				List<EstaPer> listadoEstaPer = this.bp.getByFilter(consulta);

				for (EstaPer estaPer : listadoEstaPer) {
					key = estaPer.getFechaDesdeStr()
							+ EstadosPersonas.separador;
					key += estaPer.getFechaHasta() != null ? estaPer
							.getFechaHastaStr() : "0";
					key += EstadosPersonas.separador;
					key += EstadosPersonas.agregaEstado
							+ EstadosPersonas.separador;
					key += estaPer.getId();

					if (!estadosobs.containsKey(key)) {
						registro = this.crearRegistroEstaPer(estaPer);
						estadosobs.put(key, registro);
					}
				}

				consulta = "SELECT o FROM Observacion o WHERE o.persona.idpersona="
						+ this.persona.getIdpersona()
						+ " order By o.fecha DESC";
				List<Observacion> listadoObservaciones = this.bp
						.getByFilter(consulta);

				for (Observacion observacion : listadoObservaciones) {
					key = observacion.getFechaStr() + EstadosPersonas.separador;
					key += "0" + EstadosPersonas.separador;
					key += EstadosPersonas.agregaObs
							+ EstadosPersonas.separador;
					key += observacion.getId();

					if (!estadosobs.containsKey(key)) {
						registro = this.crearRegistroObs(observacion);
						estadosobs.put(key, registro);
					}
				}

				this.estadosPersonas = new ArrayList<EstaObs>();
				for (String llave : estadosobs.keySet()) {
					this.estadosPersonas.add(estadosobs.get(llave));
				}
				sortEstadoPersonas();
			} catch (Exception e) {
				this.estadosPersonas = new ArrayList<EstaObs>();
				e.printStackTrace();
			}
		}
	}

	private void sortEstadoPersonas() {
		Collections.sort(estadosPersonas, new Comparator<EstaObs>() {

			@Override
			public int compare(EstaObs o1, EstaObs o2) {
				return o2.getFecha().compareTo(o1.getFecha());
			}
		});
	}

	/**
	 * Actualiza el estado de la persona.-
	 */
	private void guardarNuevoEstado() {
		try {
			EstaPer estadoPersona = new EstaPer();
			estadoPersona.setPersona(this.persona);
			estadoPersona.setEstado(this.estado);
			estadoPersona.setFechaDesde(new Date(System.currentTimeMillis()));
			estadoPersona.setObservaciones(this.obsEstado);
			this.validarNuevoEstado(estadoPersona);
			if (this.errores.isEmpty()) {
				this.persona = (Persona) this.bp.getById(Persona.class,
						this.persona.getId());
				this.estado = (Estado) this.bp.getById(Estado.class,
						this.estado.getId());
				estadoPersona.setPersona(this.persona);
				estadoPersona.setEstado(this.estado);
				String consulta = "SELECT ep FROM EstaPer ep WHERE ep.fechaHasta IS NULL";
				consulta += " AND ep.persona.idpersona="
						+ this.persona.getIdpersona();
				List<EstaPer> historicoEstados = this.bp.getByFilter(consulta);
				if (historicoEstados.size() > 0) {
					EstaPer ultimoEstado = historicoEstados.get(0);
					ultimoEstado.setFechaHasta(estadoPersona.getFechaDesde());
					this.bp.saveOrUpdate(ultimoEstado);
				}
				this.bp.saveOrUpdate(estadoPersona);
				this.estado = new Estado();
				this.obsEstado = null;
				this.fecha = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("EstaPer.error", "EstaPer.error");
		}
	}

	/**
	 * Guarda una observación.-
	 */
	private void guardarObservacion() {
		try {
			this.persona = (Persona) this.bp.getById(Persona.class,
					this.persona.getId());
			this.observacion.setObservacion(this.detalleObservacion);
			this.observacion.setFecha(new Date(System.currentTimeMillis()));
			this.observacion.setPersona(this.persona);
			this.observacion.validate();
			this.errores.putAll(this.observacion.getChErrores());
			if (this.errores.isEmpty()) {
				this.bp.saveOrUpdate(observacion);
				this.observacion = new Observacion();
				this.detalleObservacion = null;
				this.fecha = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("Oservacion.error", "Oservacion.error");
		}
	}

	/**
	 * Crea un registro EstaObs a partir de un EstaPer.-
	 */
	private EstaObs crearRegistroEstaPer(EstaPer estaPer) {
		EstaObs estaobs = new EstaObs();

		estaobs.setEstaobs(estaPer.getEstado().getNombreEstado());
		estaobs.setColor(estaPer.getEstado().getColor());
		estaobs.setFecha(estaPer.getFechaDesde());
		estaobs.setFechaHasta(estaPer.getFechaHasta());
		estaobs.setObservaciones(estaPer.getObservaciones());

		return estaobs;
	}

	/**
	 * Crea un registro EstaObs a partir de una Observacion.-
	 */
	private EstaObs crearRegistroObs(Observacion observacion) {
		EstaObs estaobs = new EstaObs();
		estaobs.setEstaobs(null);
		estaobs.setColor(null);
		estaobs.setFecha(observacion.getFecha());
		estaobs.setFechaHasta(null);
		estaobs.setObservaciones(observacion.getObservacion());
		return estaobs;
	}

	/**
	 * Valida un cambio de estado.-
	 * 
	 * 1) Validaciones generales de EstaPer.- 2) La Fecha Desde debe ser
	 * Posterior a la Fecha del Estado Actual.-
	 */
	private void validarNuevoEstado(EstaPer estadoPersona) {
		// 1)
		estadoPersona.validate();
		this.errores.putAll(estadoPersona.getChErrores());
		// 2)
		EstaPer estadoActual = this.persona.getEstadoActual();
		if (estadoActual != null) {
			if (estadoPersona.getFechaDesde().before(
					estadoActual.getFechaDesde())) {
				this.errores.put("EstaPer.fechaDesde.anterior",
						"EstaPer.fechaDesde.anterior");
			}
		}
	}

	// ======================GETTERS Y SETTERS======================
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Object getResult() {
		return null;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<EstaObs> getEstadosPersonas() {
		return this.estadosPersonas;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getFechaStr() {
		if (this.getFecha() == null) {
			this.setFecha(new Date(System.currentTimeMillis()));
		}
		return DateHelper.getString(this.getFecha());
	}

	public void setFechaStr(String fecha) {
		if (fecha == null || fecha.length() == 0) {
			this.setFecha(null);
			return;
		}
		this.setFecha(DateHelper.getDate(fecha));
	}

	public void setEstadosPersonas(List<EstaObs> estadosPersonas) {
		this.estadosPersonas = estadosPersonas;
	}

	public String getObsEstado() {
		return this.obsEstado;
	}

	public void setObsEstado(String obsEstado) {
		this.obsEstado = obsEstado;
	}

	public String getDetalleObservacion() {
		return this.detalleObservacion;
	}

	public void setDetalleObservacion(String detalleObservacion) {
		this.detalleObservacion = detalleObservacion;
	}

	public Observacion getObservacion() {
		return this.observacion;
	}

	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}

	public Long getAgrega() {
		return this.agrega;
	}

	public void setAgrega(Long agrega) {
		this.agrega = agrega;
	}

	// ========================VALIDACIONES=========================
	public boolean validate() {
		return this.errores.isEmpty();
	}
}