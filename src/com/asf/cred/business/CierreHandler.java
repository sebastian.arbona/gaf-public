package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Bolepago;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Movpagos;
import com.nirven.creditos.hibernate.MovpagosKey;
import com.nirven.creditos.hibernate.Pagos;

/**
 *
 * @author cnoguerol
 */
public class CierreHandler {

    private BusinessPersistance bp;
    private CtacteKey ctacteKey = null;

    /**
     *
     */
    public CierreHandler() {
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
    }

    /**
     *
     * @param bp
     * @param movpagos
     * @throws Exception
     */
    public CierreHandler(BusinessPersistance bp, Movpagos movpagos) throws Exception {
        super();
        this.bp = bp != null ? bp : SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        setCtacteKey(movpagos);
    }

    /**
     * 
     * @param p
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<Movpagos> getMovpagos(Pagos p) {
    	Long idBoleto;
		if(p.getRecibo() != null){
			idBoleto = p.getRecibo().getId();
		}else{
			idBoleto = p.getId().getBoleto().getId();
		}
        return bp.getByFilter("FROM Movpagos m WHERE m.id.caratula.id=" + p.getId().getCaratula().getId()
                              + " AND m.id.boleto.id=" + idBoleto + " AND m.actualizado = false ORDER BY m.id.movimientoCtacte DESC");
    }

    /**
     * 
     * @param idCaratula
     * @return
     */
	@SuppressWarnings("unchecked")
	public List<Bolepago> getBolepagos(Long idCaratula) {
        List<Bolepago> lstBolepagos = new ArrayList<Bolepago>();
        List<Cobropago> lstCobropagos = bp.getByFilter("FROM Cobropago c WHERE c.caratula.id=" + idCaratula);
        for (int i = 0; i < lstCobropagos.size(); i++) {
            Cobropago c = lstCobropagos.get(i);
            List<Bolepago> lst = bp.getByFilter("FROM Bolepago b WHERE b.id.cobropago.id=" + c.getId() + " ORDER BY b.id.cobropago.id");
            for (int ii = 0; ii < lst.size(); ii++) {
                lstBolepagos.add(lst.get(ii));
            }
        }

        Collections.sort(lstBolepagos, new CuotaComparator());
        return lstBolepagos;
    }

    /**
     * 
     * @param m
     * @param ctacteKey
     * @param eCta
     */
    @SuppressWarnings("unchecked")
	public void actualiza(Movpagos m, CtacteKey ctacteKey, boolean eCta) {
        //Actualiza Movpagos con el nro de movimiento de la ctacte generada..
        bp.getCurrentSession().evict(m);
        borrarMovpagos(m);
        crearMovpagos(m, ctacteKey);
        //Actualiza Bolcon con el nro de movimiento de la ctacte generada..
        List<Bolcon> lstB = bp.getByFilter("FROM Bolcon b WHERE b.id.boleto.id=" + m.getId().getBoleto().getId() + " AND b.cuota.credito.id=" + m.getCuota().getCredito_id()
                                           + " AND b.id.movimientoCtacte=" + m.getId().getMovimientoCtacte() + " AND b.facturado.id=" + m.getFacturado().getId());
        if (lstB.size() > 0) {
            Bolcon b = (Bolcon) lstB.get(0);
            bp.getCurrentSession().evict(b);
            borrarBolcon(b);
            crearBolcon(b, m);
        }
    }

    /**
     *
     * @return
     */
    public CtacteKey getCtacteKey() {
        return ctacteKey;
    }

    /**
     * 
     * @param m
     * @throws Exception
     */
    public void setCtacteKey(Movpagos m) throws Exception {
        try {
            this.ctacteKey = new CtacteKey();
            this.ctacteKey.setObjetoi_id(m.getCuota().getCredito_id());
            this.ctacteKey.setPeriodoCtacte(m.getId().getPeriodoCtacte());
            this.ctacteKey.setItemCtacte(m.getId().getItemCtacte());
            this.ctacteKey.setVerificadorCtacte(m.getId().getVerificadorCtacte());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void crearMovpagos(Movpagos m, CtacteKey c) {
        Movpagos movpagos = m;
        MovpagosKey mKey = new MovpagosKey();

        mKey.setCaratula(m.getId().getCaratula());        
        mKey.setItemCtacte(c.getItemCtacte());
        mKey.setVerificadorCtacte(c.getVerificadorCtacte());
        mKey.setMovimientoCtacte(m.getId().getMovimientoCtacte());
        mKey.setBoleto(m.getId().getBoleto());
        mKey.setPeriodoCtacte(m.getId().getPeriodoCtacte());

        movpagos.setId(mKey);

        bp.save(m);

    }

    private void borrarMovpagos(Movpagos m) {
        bp.delete(m);
    }

    private void crearBolcon(Bolcon bolcon, Movpagos m) {
        Bolcon b = bolcon;
        BolconKey bKey = new BolconKey();

        bKey.setBoleto(bolcon.getId().getBoleto());
        bKey.setItemCtacte(m.getId().getItemCtacte());        
        bKey.setVerificadorCtacte(m.getId().getVerificadorCtacte());
        bKey.setMovimientoCtacte(bolcon.getId().getMovimientoCtacte());
        bKey.setPeriodoCtacte(bolcon.getId().getPeriodoCtacte());

        b.setId(bKey);

        bp.save(bolcon);
    }

    private void borrarBolcon(Bolcon b) {
        bp.delete(b);
    }

    /**
     *
     */
    public static class CuotaComparator implements Comparator<Bolepago> {
        public int compare(Bolepago bo1, Bolepago bo2) {
            return bo1.getBoleto().getNumeroBoleto().compareTo(bo2.getBoleto().getNumeroBoleto());
        }
    }
}
