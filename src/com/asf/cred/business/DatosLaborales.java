package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.civitas.hibernate.persona.*;
import com.civitas.hibernate.persona.creditos.*;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;

/**
 * @author eselman
 */

public class DatosLaborales implements IProcess
{
//=========================ATRIBUTOS===========================
	//atributos utilitarios del proceso.-
	private String forward = "DatosLaboralesList";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	//atributos del proceso.-
	private Persona persona;
	private TipoRelacionLaboral tipoRelacionLaboral;
	private RefLabProf referencia;
	private List<RefLabProf> referencias;
//=======================CONSTRUCTORES=========================
	public DatosLaborales()
	{
		this.errores             = new HashMap<String, Object>();
		this.bp		             = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.persona             = new Persona();
		this.tipoRelacionLaboral = new TipoRelacionLaboral();
		this.referencia          = new RefLabProf();
	}
//======================FUNCIONALIDADES========================
	public boolean doProcess()
	{
		if(this.accion.equals("nuevo"))
		{
			this.forward = "DatosLaborales";
		}
		else if(this.accion.equals("guardar"))
		{
			this.validarReferencia();
			if(this.errores.isEmpty())
			{
				this.guardarReferencia();
				if(this.errores.isEmpty())
				{
					this.forward = "DatosLaboralesList";
				}	
				else
				{
					this.forward = "DatosLaborales";
				}
			}
			else
			{
				this.forward = "DatosLaborales";
			}
		}
		else if(this.accion.equals("cancelar"))
		{
			this.forward = "DatosLaboralesList";
		}
		else if(this.accion.equals("load"))
		{
			this.referencia = (RefLabProf) this.bp.getById(RefLabProf.class, this.referencia.getId());
			this.forward = "DatosLaborales";
		}
		else if(this.accion.equals("eliminar"))
		{
			this.eliminar();
		}
		
		this.listarReferencias();
		
		return this.errores.isEmpty();
	}
//=====================UTILIDADES PRIVADAS=====================
	private void listarReferencias()
	{
		if( this.persona != null)
		{
			try
			{
				this.persona = (Persona)this.bp.getById(Persona.class, this.persona.getId());
				
				String consulta = "";
				consulta += "SELECT rlp FROM RefLabProf rlp WHERE";
				consulta += " rlp.persona.id=" + this.persona.getId();
								
				this.referencias = this.bp.getByFilter(consulta);
			}
			catch( Exception e )
			{
				this.referencias = new ArrayList<RefLabProf>();
				e.printStackTrace();
			}
		}
	}
	
	private void validarReferencia()
	{
		this.referencia.validate();
		this.errores.putAll(this.referencia.getChErrores());
	}
	
	private void guardarReferencia()
	{
		this.tipoRelacionLaboral = (TipoRelacionLaboral) this.bp.getById(TipoRelacionLaboral.class,this.tipoRelacionLaboral.getId());
		this.persona = (Persona)this.bp.getById(Persona.class, this.persona.getId());
		this.bp.getCurrentSession().refresh(this.persona);
		this.referencia.setTipoRelacionLaboral(this.tipoRelacionLaboral);
		this.referencia.setPersona(this.persona);
		try
		{
			this.bp.saveOrUpdate(this.referencia);
			this.referencia = new RefLabProf();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * La eliminación consiste en dar de baja la inscripción al impuesto.-
	 */
	private void eliminar()
	{
		this.referencia = (RefLabProf) this.bp.getById(RefLabProf.class, this.referencia.getId());
		this.bp.delete(this.referencia);
		this.referencia = new RefLabProf();
	}
//======================GETTERS Y SETTERS======================
	public HashMap<String, Object> getErrors()
	{
		return this.errores;
	}
	public String getForward()
	{
		return this.forward;
	}
	public String getInput()
	{
		return this.forward;
	}
	public Object getResult()
	{
		return null;
	}
	public Persona getPersona() 
	{
		return this.persona;
	}
	public void setPersona(Persona persona) 
	{
		this.persona = persona;
	}
	public String getAccion()
	{
		return this.accion;
	}
	public void setAccion(String accion)
	{
		this.accion = accion;
	}
	public TipoRelacionLaboral getTipoRelacionLaboral() 
	{
		return this.tipoRelacionLaboral;
	}
	public void setTipoRelacionLaboral(TipoRelacionLaboral tipoRelacionLaboral) 
	{
		this.tipoRelacionLaboral = tipoRelacionLaboral;
	}
	public RefLabProf getReferencia() 
	{
		return this.referencia;
	}
	public void setReferencia(RefLabProf referencia) 
	{
		this.referencia = referencia;
	}
	public List<RefLabProf> getReferencias() 
	{
		return this.referencias;
	}
	public void setReferencias(List<RefLabProf> referencias) 
	{
		this.referencias = referencias;
	}
	//========================VALIDACIONES=========================
	public boolean validate()
	{
		return this.errores.isEmpty();
	}
}