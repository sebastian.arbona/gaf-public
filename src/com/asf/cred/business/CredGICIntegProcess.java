package com.asf.cred.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;
import org.hibernate.SQLQuery;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.nirven.creditos.hibernate.Objetoi;

public class CredGICIntegProcess extends ConversationProcess {

	private static final String ERROR_NO_EXISTE = "El n�mero de solicitud no existe en el sistema.";
	private static final String ERROR_FORMATO = "Formato incorrecto.";
	private static final String ERROR_DB = "Error al guardar en base de datos.";
	private static final String ESTADO_ACTUALIZADO = "Actualizado";
	private static final String ESTADO_NUEVO = "Nuevo";

	private FormFile archivo;
	private boolean ejecutado;
	private List<CredGICResult> beansOk;
	private List<CredGICResult> beansNoExiste;
	private List<CredGICResult> beansError;

	@ProcessMethod(defaultAction = true)
	public boolean importar() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(archivo.getInputStream()));

			beansOk = new ArrayList<CredGICResult>();
			beansNoExiste = new ArrayList<CredGICResult>();
			beansError = new ArrayList<CredGICResult>();

			SQLQuery selectQuery = bp
					.createSQLQuery("SELECT COUNT(*) FROM CORRELACION_CREDITO_GIC WHERE idProyecto = ?");
			SQLQuery insertQuery = bp
					.createSQLQuery("INSERT INTO CORRELACION_CREDITO_GIC (idProyecto, NumeroSolicitud) VALUES (?, ?)");
			SQLQuery updateQuery = bp
					.createSQLQuery("UPDATE CORRELACION_CREDITO_GIC SET NumeroSolicitud = ? WHERE idProyecto = ?");

			int nroLinea = 1;

			String l = reader.readLine();
			while (l != null) {
				l = l.trim();

				if (l.isEmpty()) {
					l = reader.readLine();
					nroLinea++;
					continue;
				}

				CredGICResult bean = new CredGICResult();

				String[] partes = l.split("\t");
				try {
					String numeroCred = new Long(partes[0]).toString();
					Long oid = new Long(partes[1]);

					Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
							.setParameter("nroAtencion", oid).uniqueResult();
					if (credito == null) {
						bean.setCodigo1(numeroCred);
						bean.setIdCredito(oid);
						bean.setLineaOriginal("" + nroLinea);
						bean.setMensaje(ERROR_NO_EXISTE);
						beansNoExiste.add(bean);
						l = reader.readLine();
						nroLinea++;
						continue;
					}

					int result = 0;

					String estado = "";

					Number count = (Number) selectQuery.setParameter(0, numeroCred).uniqueResult();
					if (count == null || count.intValue() == 0) {
						result = insertQuery.setParameter(0, numeroCred).setParameter(1, oid).executeUpdate();
						estado = ESTADO_NUEVO;
					} else if (count != null && count.intValue() == 1) {
						result = updateQuery.setParameter(0, oid).setParameter(1, numeroCred).executeUpdate();
						estado = ESTADO_ACTUALIZADO;
					}

					if (result == 0) {
						bean.setCodigo1(numeroCred);
						bean.setIdCredito(oid);
						bean.setLineaOriginal("" + nroLinea);
						bean.setMensaje(ERROR_DB);
						beansNoExiste.add(bean);
					} else {
						bean.setCodigo1(numeroCred);
						bean.setIdCredito(oid);
						bean.setTitular(credito.getPersona().getNomb12());
						bean.setLineaCredito(credito.getLinea().getNombre());
						bean.setMensaje(estado);
						beansOk.add(bean);
					}
				} catch (Exception e) {
					// linea de titulo o error de formato
					bean.setCodigo1("" + nroLinea);
					bean.setLineaOriginal(l);
					bean.setMensaje(ERROR_FORMATO);
					beansError.add(bean);
				}

				l = reader.readLine();
				nroLinea++;
			}
		} catch (IOException e) {
			errors.put("credGICInteg.archivo", "credGICInteg.archivo");
			return false;
		}

		ejecutado = true;

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "CredGICIntegProcess";
	}

	public FormFile getArchivo() {
		return archivo;
	}

	public void setArchivo(FormFile archivo) {
		this.archivo = archivo;
	}

	public int getCantidadOk() {
		return beansOk.size();
	}

	public int getCantidadNoExiste() {
		return beansNoExiste.size();
	}

	public int getCantidadError() {
		return beansError.size();
	}

	public List<CredGICResult> getBeansOk() {
		return beansOk;
	}

	public List<CredGICResult> getBeansError() {
		return beansError;
	}

	public List<CredGICResult> getBeansNoExiste() {
		return beansNoExiste;
	}

	public boolean isEjecutado() {
		return ejecutado;
	}
}
