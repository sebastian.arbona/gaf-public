declare @idTipoGarantia integer
set @idTipoGarantia = :idTipoGarantia

select o.numeroAtencion, 
p.NOMB_12 as inversor,
p.CUIL_12, 
l.nombre as linea, 
o.expediente,
g.identificadorUnico,
tg.nombre as tipoGarantia,
o.financiamiento, 
u.nombre as sectorCustodia,
--og.valor,
gt.gravFechaVencGarantia as vencimiento,
gc.ordenFisico,
ge.importe,
c.compra as cotizacionCompra,
g.id,
e.nombreEstado,
ge.fechaEstado,
ge.fechaProceso,
og.aforo
from ObjetoiGarantia og
join Garantia g on g.id = og.garantia_id
join Objetoi o on o.id = og.objetoi_id
left join objetoiestado oe ON oe.objetoi_id = o.id AND oe.fecha <= CONVERT(datetime2, ':fecha', 121) and (oe.fechaHasta >= CONVERT(datetime2, ':fecha', 121) or oe.fechaHasta is null) 
left join PERSONA p on p.IDPERSONA = o.persona_IDPERSONA 
left join Linea l on l.id = o.linea_id 
left join TipoGarantia tg on tg.id = g.tipo_id
left join GarantiaTasacion gt on gt.id = og.tasacion_id 
left join GarantiaSeguro gs on gs.id = g.seguro_id
left join GarantiaCustodia gc on gc.objetoiGarantia_id = og.id and gc.orden = (select max(orden) from GarantiaCustodia where objetoiGarantia_id = og.id)
left join unidad u on u.id = gc.sector_id
left join PERSONA resp on resp.IDPERSONA = gc.responsable_id
left join cotizacion c on c.moneda_IDMONEDA = l.moneda_IDMONEDA and c.fechaDesde <= CONVERT(date, ':fecha', 121) and (c.fechaHasta >= CONVERT(date, ':fecha', 121) or c.fechaHasta is null) 
left join GarantiaEstado ge on ge.objetoiGarantia_id = og.id and :consultaProcesoValor <= CONVERT(datetime2, ':fecha', 121))
left join Estado e ON e.idEstado = ge.estado_idEstado 
where (@idTipoGarantia is null or tg.id in (:tiposGarantias)) and og.baja IS NULL AND oe.estado_idestado IN (6,7,8,9,10) 
and ge.importe >= 0
:estadosExcluidosHTC
and l.moneda_IDMONEDA = :idMoneda and (
(
case
when oe.estado_idestado IN (7,8,9,10) then 1
when oe.estado_idestado = 6 and o.linea_id in (1535,1536,1545,1546,1554,1555,1725,1721,1737,1738) and exists (select * from desembolso where credito_id = o.id and estado = '2') then 1
else 0
end) = 1)