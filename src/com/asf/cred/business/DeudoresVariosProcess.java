package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;

import com.asf.gaf.hibernate.Moneda;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.DeudoresVarios;
import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.expedientes.persistencia.Numerador;

public class DeudoresVariosProcess implements IProcess {

	private String forward;
	private HashMap<String, Object> errores;
	private String accion;
	private BusinessPersistance bp;
	private ReportResult reporte;
	private DeudoresVarios deudoresVarios;
	private Long idDeudoresVarios;
	private Leyenda leyenda;
	private boolean gastos;

	public DeudoresVariosProcess() {
		forward = "DeudoresVarios";
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		deudoresVarios = new DeudoresVarios();
		leyenda = new Leyenda();
		gastos = false;
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return false;
		}
		if (accion.equals("cargarPersona")) {
			cargarPersona();
		}
		if (accion.equals("guardar")) {
			guardar();
		}
		if (accion.equals("reimprimir")) {
			reimprimir();
		}
		return errores.isEmpty();
	}

	public void cargarPersona() {
		forward = "Persona";
	}

	@SuppressWarnings("unchecked")
	public void guardar() {

		if (deudoresVarios.getPersona().getId() == null || deudoresVarios.getPersona().getId().longValue() == 0) {
			errores.put("deudoresVarios.persona", "deudoresVarios.persona");
			return;
		}
		if (deudoresVarios.getConcepto() == null || deudoresVarios.getConcepto().isEmpty()
				|| deudoresVarios.getTipoConcepto() == null || deudoresVarios.getTipoConcepto().isEmpty()) {
			errores.put("deudoresVarios.concepto", "deudoresVarios.concepto");
			return;
		}
		if (deudoresVarios.getMoneda().getIdMoneda() == null
				|| deudoresVarios.getMoneda().getIdMoneda().longValue() == 0) {
			errores.put("deudoresVarios.moneda", "deudoresVarios.moneda");
			return;
		}
		if (deudoresVarios.getFechaVto() == null) {
			errores.put("deudoresVarios.fecha", "deudoresVarios.fecha");
			return;
		}

		if (deudoresVarios.getImporte() == null || deudoresVarios.getImporte() == 0.0) {
			errores.put("deudoresVarios.importe", "deudoresVarios.importe");
			return;
		}

		if (deudoresVarios.getNumeroAtencion() != null && deudoresVarios.getNumeroAtencion().trim().isEmpty()) {
			deudoresVarios.setNumeroAtencion(null);
		}

		if (deudoresVarios.getConcepto()
				.equalsIgnoreCase(DirectorHelper.getString("ftyc.deudoresVarios.conceptoGastosVarios"))
				&& deudoresVarios.getTipoConcepto()
						.equalsIgnoreCase(DirectorHelper.getString("ftyc.deudoresVarios.tipoConceptoGastosVarios"))) {
			if (deudoresVarios.getNumeroAtencion() != null) {
				Objetoi credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion")
						.setParameter("nroAtencion", new Long(deudoresVarios.getNumeroAtencion())).uniqueResult();
				if (credito != null) {
					Calendar c = Calendar.getInstance();
					c.set(Calendar.HOUR_OF_DAY, 0);
					c.set(Calendar.MINUTE, 0);
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);
					Date hoy = c.getTime();
					List<Cuota> cuotas = (List<Cuota>) bp.getNamedQuery("Cuota.findByObjetoi")
							.setLong("idCredito", credito.getId()).list();
					if (cuotas != null && cuotas.size() > 0) {
						for (Cuota cuota : cuotas) {
							if (cuota.getEmision() == null && cuota.getFechaVencimiento().after(hoy)) {
								errores.put("deudoresVarios.boleta", "deudoresVarios.boleta");
								return;
							}
						}
					}

					Long numero = Numerador.getNext(DeudoresVarios.NUMERADOR);
					deudoresVarios.setNumero(numero);
					bp.save(deudoresVarios);

					List<DetalleFactura> detalles = (List<DetalleFactura>) bp
							.createQuery(
									"select d from DetalleFactura d where d.credito.id = :credito and d.impactado = 0")
							.setParameter("credito", credito.getId()).list();

					if (detalles != null && detalles.size() > 0) {
						for (DetalleFactura detalleFactura : detalles) {
							detalleFactura.setDeudoresVarios(deudoresVarios);
							bp.update(detalleFactura);
						}
					}
					gastos = true;
				}
			}
		} else {
			Long numero = Numerador.getNext(DeudoresVarios.NUMERADOR);
			deudoresVarios.setNumero(numero);
			bp.save(deudoresVarios);
		}

		imprimirReporte();
		if (errores.isEmpty()) {
			forward = "ReportesProcess";
		}
	}

	@ProcessMethod
	public void reimprimir() {
		if (idDeudoresVarios != null) {
			this.deudoresVarios = (DeudoresVarios) bp.getById(DeudoresVarios.class, this.idDeudoresVarios);
			imprimirReporte();
		}
		if (errores.isEmpty()) {
			forward = "ReportesProcess";
		}
	}

	public void imprimirReporte() {
		try {
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			Moneda moneda = (Moneda) bp.getById(Moneda.class, deudoresVarios.getMoneda().getId());
			String nombreBoleto = null;
			if (gastos) {
				nombreBoleto = "BOLETO DE GASTOS VARIOS N�";
			} else {
				nombreBoleto = "BOLETO DE INGRESOS VARIOS N�";
			}
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletosDePagoSinCC.jasper");
			reporte.setParams("LEYENDA=" + leyenda.getTexto() + ";ID_DEUDOR=" + deudoresVarios.getId() + ";USUARIO="
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";MONEDA=" + moneda.getDenominacion()
					+ ";SIMBOLO=" + moneda.getSimbolo() + ";NUMERO_BOLETO=" + deudoresVarios.getNumero()
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ ";NOMBRE_BOLETO=" + nombreBoleto);
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "boletosDePagoSinCC"));
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public DeudoresVarios getDeudoresVarios() {
		return deudoresVarios;
	}

	public void setDeudoresVarios(DeudoresVarios deudoresVarios) {
		this.deudoresVarios = deudoresVarios;
	}

	public Leyenda getLeyenda() {
		return leyenda;
	}

	public void setLeyenda(Leyenda leyenda) {
		this.leyenda = leyenda;
	}

	public boolean isGastos() {
		return gastos;
	}

	public void setGastos(boolean gastos) {
		this.gastos = gastos;
	}

	public Long getIdDeudoresVarios() {
		return idDeudoresVarios;
	}

	public void setIdDeudoresVarios(Long idDeudoresVarios) {
		this.idDeudoresVarios = idDeudoresVarios;
	}

}
