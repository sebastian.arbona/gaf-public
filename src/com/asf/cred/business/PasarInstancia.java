package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Instancia;
import com.nirven.creditos.hibernate.InstanciaObjetoi;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;

public class PasarInstancia implements IProcess {
	private BusinessPersistance bp;
	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private List<InstanciaObjetoi> instancias;
	private Objetoi objetoi;
	private InstanciaObjetoi instanciaObjetoi;
	private Integer indice;

	@SuppressWarnings("unchecked")
	public PasarInstancia() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		accion = "listar";
		forward = "PasarInstancia";
		objetoi = new Objetoi();
		instanciaObjetoi = new InstanciaObjetoi();
		instancias = (List<InstanciaObjetoi>) SessionHandler.getCurrentSessionHandler().getRequest()
				.getAttribute("pasarInstancia.instancias");
		indice = -1;
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return true;
		}
		if (accion.equals("listar")) {
			listar();
		}
		if (accion.equals("seleccionar")) {
			seleccionar();
		}
		if (accion.equals("guardar")) {
			guardar();
		}
		if (accion.equals("pasarInstancia")) {
			pasarInstancia();
			listar();
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listar() {
		instancias = bp.createQuery("SELECT io FROM InstanciaObjetoi io WHERE io.objetoi = :objetoi ORDER BY io.fecha")
				.setEntity("objetoi", objetoi).list();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("pasarInstancia.instancias",
				instancias);
	}

	public void seleccionar() {
		listar();
		instanciaObjetoi = instancias.get(indice);
	}

	public void guardar() {
		String observaciones = instanciaObjetoi.getObservaciones();
		instanciaObjetoi = (InstanciaObjetoi) bp.getById(InstanciaObjetoi.class, instanciaObjetoi.getId());
		instanciaObjetoi.setObservaciones(observaciones);
		bp.update(instanciaObjetoi);
		indice = -1;
		listar();
	}

	@SuppressWarnings("unchecked")
	public void pasarInstancia() {
		listar();
		if (instancias == null || instancias.isEmpty()) {
			return;
		}
		try {
			Instancia instanciaAnterior = instancias.get(instancias.size() - 1).getInstancia();
			if (instanciaAnterior != null) {
				List<Instancia> instancias1 = (List<Instancia>) bp
						.createQuery("SELECT i FROM Instancia i WHERE i.orden = :orden")
						.setParameter("orden", instanciaAnterior.getOrden() + 1).list();
				if (instancias1.size() > 1) {
					throw new NonUniqueResultException();
				}
				if (instancias1.isEmpty()) {
					throw new NoResultException();
				}
				Instancia instanciaNueva = instancias1.get(0);
				if (!asociarGastosInstancia(instanciaAnterior, instanciaNueva, objetoi)) {
					errores.put("pasarDeInstancia.error.gastoCero", "pasarDeInstancia.error.gastoCero");
					return;
				}
				Date hoy = new Date();
				InstanciaObjetoi anterior = (InstanciaObjetoi) bp
						.createQuery("SELECT i FROM InstanciaObjetoi i "
								+ "WHERE i.instancia.id = :id AND i.objetoi.id = :objetoiId")
						.setParameter("id", instanciaAnterior.getId()).setParameter("objetoiId", objetoi.getId())
						.uniqueResult();
				// instancias.get(instancias1.size()-1);
				anterior.setFechaHasta(hoy);
				InstanciaObjetoi instanciaObjetoi = new InstanciaObjetoi();
				instanciaObjetoi.setFecha(new Date());
				instanciaObjetoi.setObjetoi(objetoi);
				instanciaObjetoi.setInstancia(instanciaNueva);
				bp.begin();
				bp.update(anterior);
				bp.save(instanciaObjetoi);
				bp.commit();
				return;
			} else {
				errores.put("pasarDeInstancia.error.anterior", "pasarDeInstancia.error.anterior");
				return;
			}
		} catch (NonUniqueResultException e) {
			errores.put("pasarDeInstancia.error.ordenRepetido", "pasarDeInstancia.error.ordenRepetido");
			return;
		} catch (NoResultException e) {
			errores.put("pasarDeInstancia.error.siguiente", "pasarDeInstancia.error.siguiente");
			return;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean asociarGastosInstancia(Instancia anterior, Instancia nueva, Objetoi objetoi) {
		if (nueva.getGastoAsociado().doubleValue() != 0.0) {
			List<Cuota> cuotas = (List<Cuota>) bp
					.createQuery("SELECT c FROM Cuota c WHERE c.credito = :credito "
							+ "AND c.fechaVencimiento >= :hoy ORDER BY c.fechaVencimiento")
					.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();
			Cuota cuota = null;
			if (!cuotas.isEmpty()) {
				cuota = cuotas.get(0);
			} else {
				List<Cuota> cuotasVencidas = (List<Cuota>) bp
						.createQuery("SELECT c FROM Cuota c WHERE c.credito = :credito "
								+ "AND c.fechaVencimiento < :hoy ORDER BY c.fechaVencimiento DESC")
						.setParameter("credito", objetoi).setParameter("hoy", new Date()).list();
				if (!cuotasVencidas.isEmpty()) {
					cuota = cuotasVencidas.get(0);
				} else {
					errores.put("pasarDeInstancia.error.cuotas", "pasarDeInstancia.error.cuotas");
					return false;
				}
			}
			Tipomov tipoMovimiento = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
			CreditoHandler creditoHandler = new CreditoHandler();
			Ctacte cc = creditoHandler.crearCtaCte(cuota, Concepto.CONCEPTO_GASTOS, tipoMovimiento,
					nueva.getGastoAsociado());
			cc.setTipoMovimiento("cuota");
			cc.setFechaVencimiento(cuota.getFechaVencimiento());
			bp.saveOrUpdate(cc);
			return true;
		}
		return true;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<InstanciaObjetoi> getInstancias() {
		return instancias;
	}

	public void setInstancias(List<InstanciaObjetoi> instancias) {
		this.instancias = instancias;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Integer getInstanciaActual() {
		if (instancias != null && !instancias.isEmpty()) {
			return instancias.get(instancias.size() - 1).getInstancia().getOrden();
		} else {
			return null;
		}
	}

	public InstanciaObjetoi getInstanciaObjetoi() {
		return instanciaObjetoi;
	}

	public void setInstanciaObjetoi(InstanciaObjetoi instanciaObjetoi) {
		this.instanciaObjetoi = instanciaObjetoi;
	}

	public Integer getIndice() {
		return indice;
	}

	public void setIndice(Integer indice) {
		this.indice = indice;
	}
}
