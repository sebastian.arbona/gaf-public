package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.JReportAction;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.struts.form.JReportForm;
import com.asf.util.ArrayHelper;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.PersonalizacionHelper;
import com.asf.util.ReportResult;
import com.civitas.cred.beans.BeanNotificacion;
import com.civitas.cred.beans.BeanNotificacionMora;
import com.civitas.cred.beans.BeanNotificacionMoraDetalle;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

@Save
public class NotificacionMora extends ConversationProcess {

	private boolean paso1;

	private String comportamientoPago;
	private List<Linea> lineasList;
	private String[] idsLinea;
	private Long idPersona;
	private Long numeroAtencionDesde;
	private Long numeroAtencionHasta;
	private Date fechaMora;
	private Long idTipoGarantia;
	private String identificadorUnico;
	// private ArrayList<BeanNotificacion> beanNotificacionList;
	private ArrayList<BeanNotificacionMoraDetalle> beanNotificacionMoraDetalleList;
	private ReportResult result;

	@Override
	protected String getDefaultForward() {
		return "NotificacionMora";
	}

	public boolean validate() {
		boolean status = true;
		if ("iniciarPantalla".equalsIgnoreCase(this.action)) {
			// no valido nada, estoy iniciando
		}
		if ("listar".equalsIgnoreCase(this.action)) {
			// todos los parametros pueden estar vacios
			// la unica combinacion explicita es que para indicar identificacion de
			// garantia, primero deberia estar seleccionado un tipo de garantia, pero es
			// algo que se puede ignorar ya que no hay diferencias
		}
		if ("exportar".equalsIgnoreCase(this.action)) {
			// el boton aparece siempre, si la lista esta vacia intento calcular nuevamente
			// con los parametros en pantalla
			// si la lista no esta vacia exporta el contenido de esa lista
			// no necesito validar si hay algo seleccionado porque no interesa para esta
			// funcion
		}
		if ("generar".equalsIgnoreCase(this.action)) {
			// habria que validar si hay algo seleccionado
		}
		return status;
	}

	@ProcessMethod(defaultAction = true)
	public boolean iniciarPantalla() {
		// ingreso a la pantalla
		boolean status = true;
		if (paso1 == false) {
			// paso es falso al ingresar la primera vez, si vuelvo desde el siguiente ya
			// sera true entonces no debo reiniciar la pantalla
			status = limpiarPantalla();
			paso1 = true;
		}
		return status;

	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean limpiarPantalla() {
		// limpio al ingresar la primera vez o cuando el user presiona el boton
		boolean status = true;
		comportamientoPago = null;
		lineasList = bp.getNamedQuery("Linea.todasPorNombre").list();
		idsLinea = null;
		idPersona = null;
		numeroAtencionDesde = null;
		numeroAtencionHasta = null;
		fechaMora = null;
		idTipoGarantia = null;
		identificadorUnico = null;
		beanNotificacionMoraDetalleList = null;
		return status;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean listar() {
		boolean status = true;
		Query queryGarantias = bp
				.createQuery("SELECT g FROM ObjetoiGarantia g WHERE g.objetoi = :credito AND g.baja IS NULL");
		Query queryBonificaciones = bp.createQuery(
				"SELECT COUNT(b) FROM ObjetoiBonificacion b WHERE b.objetoi.id = :id AND b.objetoi.fechaBajaBonificacion IS NULL");
		beanNotificacionMoraDetalleList = new ArrayList<BeanNotificacionMoraDetalle>();
		String consulta = "";
		String selectBase = "SELECT o.id FROM objetoi o WHERE o.numeroAtencion IS NOT NULL AND o.numeroAtencion > 0";
		String filtroNumeroAtencion = "";
		String filtroIdPersona = "";
		String filtroIdLinea = "";
		String filtroFechaMora = "";
		List<String> filtroEstado = Arrays.asList("EJECUCION", "PRIMER DESEMBOLSO", "PENDIENTE SEGUNDO DESEMBOLSO");
		if (numeroAtencionDesde != null && numeroAtencionDesde > 0) {
			filtroNumeroAtencion += " AND o.numeroAtencion >= " + numeroAtencionDesde;
		}
		if (numeroAtencionHasta != null && numeroAtencionHasta > 0) {
			filtroNumeroAtencion += " AND o.numeroAtencion <= " + numeroAtencionHasta;
		}
		if (idPersona != null && idPersona > 0) {
			filtroIdPersona += " AND o.persona_idpersona = " + idPersona;
		}
		if (idsLinea != null && idsLinea.length > 0) {
			filtroIdLinea += " AND o.linea_id IN ("
					+ ArrayHelper.arrayToString2(new ArrayList<String>(Arrays.asList(idsLinea))) + ")";
		}
		if (fechaMora != null) {
			filtroFechaMora += " AND o.id IN (SELECT DISTINCT c.credito_id FROM cuota c WHERE c.fechaVencimiento = '"
					+ DateHelper.getStringFull4(fechaMora) + "' AND c.fechaVencimiento < '"
					+ DateHelper.getStringFull4(new Date()) + "' AND c.estado = '1') ";
		}
		consulta = selectBase + filtroNumeroAtencion + filtroIdPersona + filtroIdLinea + filtroFechaMora
				+ " ORDER BY o.numeroAtencion";
		ArrayList<Number> idObjetoiList = (ArrayList<Number>) bp.createSQLQuery(consulta).list();
		boolean controlaTipo = false, controlaIdentificacion = false, agregar;
		if ((idTipoGarantia != null && idTipoGarantia > 0)) {
			controlaTipo = true;
		}
		if (identificadorUnico != null && !identificadorUnico.isEmpty()) {
			controlaIdentificacion = true;
		}
		int total = idObjetoiList.size();
		long startTime, endTime, partialTime, globalStartTime, globalEndTime, globalTime;
		globalStartTime = System.nanoTime();
		for (int i = 0; i < total; i++) {
			startTime = System.nanoTime();
			agregar = false;
			Number idObjetoi = idObjetoiList.get(i);
			Objetoi proyecto = (Objetoi) bp.getById(Objetoi.class, idObjetoi.longValue());
			String estadoActual;
			try {
				estadoActual = proyecto.getEstadoActual().getEstado().getNombreEstado();
			} catch (Exception e) {
				estadoActual = "";
				System.out.println("proyecto " + idObjetoi + " sin estado");
			}
			if (estadoActual != null && !estadoActual.isEmpty() && filtroEstado.contains(estadoActual.toUpperCase())) {
				if ((comportamientoPago == null || comportamientoPago.isEmpty())
						|| comportamientoPago.equalsIgnoreCase(proyecto.getComportamientoActual())) {
					if (controlaTipo || controlaIdentificacion) {
						ArrayList<ObjetoiGarantia> garantiaList = (ArrayList<ObjetoiGarantia>) queryGarantias
								.setParameter("credito", proyecto).list();
						if (!garantiaList.isEmpty()) {
							for (ObjetoiGarantia objetoiGarantia : garantiaList) {
								if (objetoiGarantia != null) {
									if (controlaTipo
											&& objetoiGarantia.getGarantia().getTipo().getId() == idTipoGarantia) {
										agregar = true;
									}
									if (controlaIdentificacion && objetoiGarantia.getGarantia().getIdentificadorUnico()
											.equalsIgnoreCase(identificadorUnico)) {
										agregar = true;
									}
								}
								if (agregar) {
									break;
								}
							}
						}
					} else {
						agregar = true;
					}
					if (agregar) {
						try {
							BeanNotificacion beanNotificacion = new BeanNotificacion();
							beanNotificacion.setCredito(proyecto);
							Number bonificaciones = (Number) queryBonificaciones.setParameter("id", proyecto.getId())
									.uniqueResult();
							if (bonificaciones != null && bonificaciones.intValue() != 0) {
								beanNotificacion.setBonificado(true);
							}

							BeanNotificacionMoraDetalle beanDetalle = new BeanNotificacionMoraDetalle(beanNotificacion);
							beanNotificacionMoraDetalleList.add(beanDetalle);
						} catch (Exception e) {
							System.out.println("error al calcular la deuda, id " + idObjetoi);
						}
					}
				}
			} // fin filtro de estados
			endTime = System.nanoTime();
			partialTime = (endTime - startTime) / 1000000000;
			System.out.println(
					"procesado id " + idObjetoi + ", orden " + (i + 1) + " de " + total + ", tiempo " + (partialTime));
		} // fin lista de ids
		globalEndTime = System.nanoTime();
		globalTime = (globalEndTime - globalStartTime) / 1000000000;
		System.out.println("terminado, duracion: " + globalTime);
		return status;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ProcessMethod
	public boolean exportar() {
		boolean status = true;
		try {
			if (beanNotificacionMoraDetalleList == null || beanNotificacionMoraDetalleList.isEmpty()) {
				// si no hay datos, intento volver a calcular
				// si primero hizo listar y despues exportar, el resultado sera el mismo
				status = listar();
			}
			if (beanNotificacionMoraDetalleList != null && !beanNotificacionMoraDetalleList.isEmpty()) {
				BeanNotificacionMora beanNotificacionMora = new BeanNotificacionMora(comportamientoPago, idsLinea,
						idPersona, numeroAtencionDesde, numeroAtencionHasta, fechaMora, idTipoGarantia,
						identificadorUnico, beanNotificacionMoraDetalleList);
				ArrayList resultados = new ArrayList();
				resultados.add(beanNotificacionMora);
				HashMap parametros = (HashMap) PersonalizacionHelper.getPersonalizacionReporte(new HashMap());
				JReportAction.imprimirReporte(parametros, resultados, "notificacionMora.jrxml", this.getResponse(),
						"notificacionMora", JReportForm.EXPORT_XLS);
			}
		} catch (Exception e) {
			status = false;
		}
		return status;
	}

	@ProcessMethod
	public boolean generar() {
		boolean status = true;

		return status;
	}

	public String getComportamientoPago() {
		return comportamientoPago;
	}

	public void setComportamientoPago(String comportamientoPago) {
		this.comportamientoPago = comportamientoPago;
	}

	public List<Linea> getLineasList() {
		return lineasList;
	}

	public void setLineasList(List<Linea> lineasList) {
		this.lineasList = lineasList;
	}

	public String[] getIdsLinea() {
		return idsLinea;
	}

	public void setIdsLinea(String[] idsLinea) {
		this.idsLinea = idsLinea;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getNumeroAtencionDesde() {
		return numeroAtencionDesde;
	}

	public void setNumeroAtencionDesde(Long numeroAtencionDesde) {
		this.numeroAtencionDesde = numeroAtencionDesde;
	}

	public Long getNumeroAtencionHasta() {
		return numeroAtencionHasta;
	}

	public void setNumeroAtencionHasta(Long numeroAtencionHasta) {
		this.numeroAtencionHasta = numeroAtencionHasta;
	}

	public String getFechaMora() {
		return DateHelper.getString(this.fechaMora);
	}

	public void setFechaMora(Date fechaMora) {
		this.fechaMora = fechaMora;
	}

	public void setFechaMora(String fechaMora) {
		this.fechaMora = DateHelper.getDate(fechaMora);
	}

	public Long getIdTipoGarantia() {
		return idTipoGarantia;
	}

	public void setIdTipoGarantia(Long idTipoGarantia) {
		this.idTipoGarantia = idTipoGarantia;
	}

	public String getIdentificadorUnico() {
		return identificadorUnico;
	}

	public void setIdentificadorUnico(String identificadorUnico) {
		this.identificadorUnico = identificadorUnico;
	}

	public String getUrlGaf() {
		return DirectorHelper.getString("URL.GAF");
	}

	public ReportResult getResult() {
		return result;
	}

	public void setResult(ReportResult result) {
		this.result = result;
	}

	public ArrayList<BeanNotificacionMoraDetalle> getBeanNotificacionMoraDetalleList() {
		return beanNotificacionMoraDetalleList;
	}

	public void setBeanNotificacionMoraDetalleList(
			ArrayList<BeanNotificacionMoraDetalle> beanNotificacionMoraDetalleList) {
		this.beanNotificacionMoraDetalleList = beanNotificacionMoraDetalleList;
	}

}
