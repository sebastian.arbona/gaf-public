package com.asf.cred.business;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.GarantiaSeguro;
import com.nirven.creditos.hibernate.GarantiaSeguroEstado;

/**
 * Proceso que analiza la fecha de vencimiento de un seguro y carga el estado
 * VENCIDO si la fecha de vencimiento es anterior a hoy y todavia no se ha
 * asignado uno de los estados finales [VENCIDO,ANULADO,SIN_COBERTURA]
 * 
 * @author Cesar
 *
 */
public class SegurosVencidos implements Serializable {

	private static final long serialVersionUID = 7946329452841252833L;

	private BusinessPersistance bp;

	/**
	 * @param bp acceso a la persistencia
	 */
	public SegurosVencidos(BusinessPersistance bp) {
		this.bp = bp;
	}

	/**
	 * Ejecuta el analisis de las garantias y si corresponde crea el nuevo estado
	 * VENCIDA.
	 */
	@SuppressWarnings("unchecked")
	public void ejecutar() {
		/**
		 * Consulta que determina el id de todas las garantiaSeguro que estan en
		 * condiciones de cambiar de estado
		 * 
		 */
		try {
			String consulta = "SELECT gs.id FROM GarantiaSeguro gs LEFT JOIN GarantiaSeguroEstado gse ON gse.garantiaSeguro_id = gs.id "
					+ "WHERE gse.id = (SELECT MAX(ugse.id) FROM GarantiaSeguroEstado ugse WHERE ugse.garantiaSeguro_id = gs.id)  AND gse.estado NOT IN('anulado','vencido','sin_cobertura')"
					+ "  AND gs.fechaVencimiento IS NOT NULL AND gs.fechaVencimiento <= :hoy";
			Query query = bp.createSQLQuery(consulta).setParameter("hoy", new Date());
			List<Number> ids = query.list();
			for (Number id : ids) {
				GarantiaSeguro garantiaSeguro = (GarantiaSeguro) bp.getById(GarantiaSeguro.class, id.longValue());
				GarantiaSeguroEstado estadoNuevo = new GarantiaSeguroEstado();
				estadoNuevo.setEstado("VENCIDO");
				estadoNuevo.setFecha(new Date());
				estadoNuevo.setGarantiaSeguro(garantiaSeguro);
				estadoNuevo.setObservaciones("Estado Automatico " + Calendar.getInstance().getTime());
				bp.save(estadoNuevo);
			}
		} catch (Exception e) {
			// TODO
		}
	}

}
