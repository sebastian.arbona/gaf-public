/*
 * ReciboScriptlet.java
 *
 * Created on 31 de marzo de 2006, 9:57
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.asf.cred.business;

import com.asf.util.NumberHelper;
import net.sf.jasperreports.engine.JRAbstractScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 *
 * @author aromeo
 */
public class ReciboScriptlet extends JRAbstractScriptlet{
    private static Double codi27_993 = new Double( 0 );
    private static Double codi27_994 = new Double( 0 );
    private static Double codi27_995 = new Double( 0 );
    private static Double codi27_996 = new Double( 0 );
    private static Double codi27_998 = new Double( 0 );
    private static Double codi27_999 = new Double( 0 );
    
    /** Creates a new instance of ReciboScriptlet */
    public ReciboScriptlet() {
    }
         
    public String totales(Double codi27, Double impo31){
        
        switch(codi27.intValue()){
            
            case 993:
                codi27_993 = new Double( impo31.doubleValue() );
                break;
                
            case 994:
                codi27_994 = new Double( impo31.doubleValue() );
                break;
                
            case 995:
                codi27_995 = new Double( impo31.doubleValue() );
                break;
                
            case 996:
                codi27_996 = new Double( impo31.doubleValue() );
                break;
                
            case 998:
                codi27_998 = new Double( impo31.doubleValue() );
                break;
                
            case 999:
                codi27_999 = new Double( impo31.doubleValue() );
                break;
        }
        
        return null;
    }
    
    
    public Double mostrar(int codi27) throws JRScriptletException {
        
        switch(codi27){
            
            case 993:
                return codi27_993;
                
            case 994:
                return codi27_994;
                
            case 995:
                return codi27_995;
                
            case 996:
                return codi27_996;
                
            case 998:
                return codi27_998;
                
            case 999:
                return codi27_999;
                
            default:
                return new Double(0);
        }
        
        
    }
    
    public String getDinero(Double total)throws JRScriptletException{
        
        return NumberHelper.getAsDinero(total.doubleValue());
        
    }
    public void beforeReportInit() throws JRScriptletException {
    }
    
    public void afterReportInit() throws JRScriptletException {
    }
    
    public void beforePageInit() throws JRScriptletException {
    }
    
    public void afterPageInit() throws JRScriptletException {
    }
    
    public void beforeColumnInit() throws JRScriptletException {
    }
    
    public void afterColumnInit() throws JRScriptletException {
    }
    
    public void beforeGroupInit(String string) throws JRScriptletException {
    }
    
    public void afterGroupInit(String string) throws JRScriptletException {
    }
    
    public void beforeDetailEval() throws JRScriptletException {
    }
    
    public void afterDetailEval() throws JRScriptletException {
    }
    
}
