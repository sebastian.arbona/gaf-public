package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

public class GarantiasInventarioContableBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String id;
    private Long numeroAtencion;
    private String linea;
    private String inversor;
    private Long cuit;
    private Double financiamiento;
    private String tipoGarantia;
    private String expediente;
    private String sectorCustodia;
    private Date vencimiento;
    private String ordenFisico;
    private Double importe;
    private Double cotizacion;
    private Double valorPesos;
    private Long idGarantia;
    private String estadoGarantia;
    private Date fechaEstado;
    private Double aforo;

    public GarantiasInventarioContableBean() {
    }

    public GarantiasInventarioContableBean(Object[] r) {

        this.numeroAtencion = r[0] != null ? ((Number) r[0]).longValue() : null;
        this.inversor = (String) r[1];
        this.cuit = r[2] != null ? ((Number) r[2]).longValue() : null;
        this.linea = (String) r[3];
        this.expediente = (String) r[4];
        this.id = (String) r[5];
        this.tipoGarantia = (String) r[6];
        this.financiamiento = r[7] != null ? ((Number) r[7]).doubleValue() : null;
        this.sectorCustodia = (String) r[8];
        // this.valor = r[9] != null ? ((Number) r[9]).doubleValue() : null;
        this.vencimiento = (Date) r[9];
        this.ordenFisico = (String) r[10];
        this.importe = r[11] != null ? ((Number) r[11]).doubleValue() : null;
        this.cotizacion = r[12] != null ? ((Number) r[12]).doubleValue() : null;
        if (importe != null && cotizacion != null) {
            this.valorPesos = importe * cotizacion;
        } else {
            this.valorPesos = (double) 0;
        }
        this.idGarantia = r[13] != null ? ((Number) r[13]).longValue() : null;
        this.estadoGarantia = (String) r[14];
        this.fechaEstado = (Date) r[16];
        this.aforo = (Double) r[17];
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getNumeroAtencion() {
        return numeroAtencion;
    }

    public void setNumeroAtencion(Long numeroAtencion) {
        this.numeroAtencion = numeroAtencion;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getInversor() {
        return inversor;
    }

    public void setInversor(String inversor) {
        this.inversor = inversor;
    }

    public Long getCuit() {
        return cuit;
    }

    public void setCuit(Long cuit) {
        this.cuit = cuit;
    }

    public Double getFinanciamiento() {
        return financiamiento;
    }

    public void setFinanciamiento(Double financiamiento) {
        this.financiamiento = financiamiento;
    }

    public String getTipoGarantia() {
        return tipoGarantia;
    }

    public void setTipoGarantia(String tipoGarantia) {
        this.tipoGarantia = tipoGarantia;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public String getSectorCustodia() {
        return sectorCustodia;
    }

    public void setSectorCustodia(String sectorCustodia) {
        this.sectorCustodia = sectorCustodia;
    }

    public Date getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(Date vencimiento) {
        this.vencimiento = vencimiento;
    }

    public String getOrdenFisico() {
        return ordenFisico;
    }

    public void setOrdenFisico(String ordenFisico) {
        this.ordenFisico = ordenFisico;
    }

    public Double getValorPesos() {
        return valorPesos;
    }

    public void setValorPesos(Double valorPesos) {
        this.valorPesos = valorPesos;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public Double getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(Double cotizacion) {
        this.cotizacion = cotizacion;
    }

    public Long getIdGarantia() {
        return idGarantia;
    }

    public void setIdGarantia(Long idGarantia) {
        this.idGarantia = idGarantia;
    }

    public String getEstadoGarantia() {
        return estadoGarantia;
    }

    public void setEstadoGarantia(String estadoGarantia) {
        this.estadoGarantia = estadoGarantia;
    }

    public Date getFechaEstado() {
        return fechaEstado;
    }

    public void setFechaEstado(Date fechaEstado) {
        this.fechaEstado = fechaEstado;
    }

	public Double getAforo() {
		return aforo;
	}

	public void setAforo(Double aforo) {
		this.aforo = aforo;
	}

}
