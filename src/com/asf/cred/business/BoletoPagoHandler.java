package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.logging.Log;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class BoletoPagoHandler {

    @SuppressWarnings({ "unused", "unchecked" })
	public Boleto generarBoleto(Objetoi credito, Date fechaCobranza, Log log, BusinessPersistance bp) throws Exception {
    	Long idObjetoi = credito.getId();
    	
    	List<Ctacte> ccs = (List<Ctacte>) bp.getNamedQuery("Ctacte.findByCredito").setLong("idCredito", idObjetoi).list();
    	
    	if (ccs.isEmpty()) {
    		// no tiene nada generado
    		// deberia haberse generado con el desembolso
    		// generar debito de capital
    		try {
    			// generar la primera cuota
    			Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setLong("idCredito", idObjetoi).setInteger("nroCuota", 1).uniqueResult();
    		
    			if (cuota == null) {
    				// no tiene cuotas
    				throw new Exception("El credito no tiene cuotas generadas.");
    			}
    			
    			// usuario loggeado
                Usuario usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
                
                // uso como periodo el a�o de generacion = actual
                int periodo = Calendar.getInstance().get(Calendar.YEAR);
                
                // numerador debe incluir el periodo
				//String numerador = Boleto.NUMERADOR_BOLETO_RECIBO;
                
               
                // numerador debe incluir el periodo
                String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo); 
                long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
                
                //tipo movimiento debito
            	Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
                
            	
            	// creo solo concepto capital
				Ctacte cc = credito.crearCtacte(Concepto.CONCEPTO_CAPITAL, cuota.getCapital(), fechaCobranza, null, credito, cuota, Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
				cc.setTipomov(tipoDebito);
				cc.getId().setMovimientoCtacte(movimientoCtaCte);
				cc.getId().setItemCtacte(1L);
				
				bp.save(cc);
				
		    	// traer ahora los debitos creados
		    	ccs = (List<Ctacte>) bp.getNamedQuery("Ctacte.findByCredito").setLong("idCredito", idObjetoi).list();
    		} catch (NoResultException e) {
    			// no tiene cuotas, lanzar error
    			throw e;
    		}
    	}
    	
    	double totalDeuda = 0;
    	Ctacte primeraCapitalNoCancelada = null;
    	Ctacte ultimoCapital = null;
    	for (Ctacte cc : ccs) {
    		if (cc.isDebito()) {
    			totalDeuda += cc.getImporte();
    			// busco capital de la primera NO cancelada para asignarle el boleto
    			if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
    				ultimoCapital = cc;
    				if (primeraCapitalNoCancelada == null && cc.getCuota().getEstado().equals(Cuota.SIN_CANCELAR)) {
    					primeraCapitalNoCancelada = cc;
    				}
    			}
    		} else {
    			totalDeuda -= cc.getImporte();
    		}
    	}
    	if (primeraCapitalNoCancelada == null) { // estan todas canceladas
    		primeraCapitalNoCancelada = ultimoCapital; // tomar el ultimo debito de capital
    	}
    	
    	totalDeuda = Math.max(0, totalDeuda);
    	
    	// usuario loggeado
        Usuario usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
        
        // uso como periodo el a�o de generacion = actual
        int periodo = Calendar.getInstance().get(Calendar.YEAR);
        
        //crear nuevo boleto
        Boleto boleto = new Boleto();
        boleto.setFechaEmision(fechaCobranza); 
        boleto.setImporte(totalDeuda);
        boleto.setObjetoi(credito);
        boleto.setFechaVencimiento(fechaCobranza); 
        boleto.setTipo(Boleto.TIPO_BOLETO_CAPITAL);
        boleto.setUsuario(usuario);
        boleto.setPeriodoBoleto(new Long(periodo));
        
        // numerador debe incluir el periodo
        boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
        boleto.setVerificadorBoleto(0L);       
         
        bp.save(boleto);
        
        Concepto concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL);
    	
    	Bolcon bc = new Bolcon();
    	bc.setFacturado(concepto);
    	bc.setOriginal(concepto);
    	bc.setImporte(totalDeuda);
    	
    	CtacteKey ck = primeraCapitalNoCancelada.getId();
    	
    	BolconKey bck = new BolconKey();
    	bck.setBoleto(boleto);
    	bck.setPeriodoCtacte(ck.getPeriodoCtacte());
    	bck.setMovimientoCtacte(ck.getMovimientoCtacte());
    	bck.setVerificadorCtacte(ck.getVerificadorCtacte());
    	bck.setItemCtacte(ck.getItemCtacte());
    	bc.setId(bck);
    	
    	//tipo movimiento debito
    	Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
    	bc.setTipomov(tipoDebito);
    	
    	bp.save(bc);
    	
    	return boleto;
	}
    
    
    
    
    public Boleto generarBoletoParcial(double deuda, Objetoi credito, Date fechaCobranza, Log log, BusinessPersistance bp) throws Exception {
    	Long idObjetoi = credito.getId();
    	
    	List<Ctacte> ccs = (List<Ctacte>) bp.getNamedQuery("Ctacte.findByCredito").setLong("idCredito", idObjetoi).list();
    	
    	if (ccs.isEmpty()) {
    		// no tiene nada generado
    		// deberia haberse generado con el desembolso
    		// generar debito de capital
    		try {
    			// generar la primera cuota
    			Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setLong("idCredito", idObjetoi).setInteger("nroCuota", 1).uniqueResult();
    		
    			if (cuota == null) {
    				// no tiene cuotas
    				throw new Exception("El credito no tiene cuotas generadas.");
    			}
    			
    			// usuario loggeado
                Usuario usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
                
                // uso como periodo el a�o de generacion = actual
                int periodo = Calendar.getInstance().get(Calendar.YEAR);
                
                // numerador debe incluir el periodo
				//String numerador = Boleto.NUMERADOR_BOLETO_RECIBO;
                
               
                // numerador debe incluir el periodo
                String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo); 
                long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
                
                //tipo movimiento debito
            	Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
                
            	
            	// creo solo concepto capital
				Ctacte cc = credito.crearCtacte(Concepto.CONCEPTO_CAPITAL, cuota.getCapital(), fechaCobranza, null, credito, cuota, Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
				cc.setTipomov(tipoDebito);
				cc.getId().setMovimientoCtacte(movimientoCtaCte);
				cc.getId().setItemCtacte(1L);
				
				bp.save(cc);
				
		    	// traer ahora los debitos creados
		    	ccs = (List<Ctacte>) bp.getNamedQuery("Ctacte.findByCredito").setLong("idCredito", idObjetoi).list();
    		} catch (NoResultException e) {
    			// no tiene cuotas, lanzar error
    			throw e;
    		}
    	}
    	
    	Ctacte primeraCapitalNoCancelada = null;
    	Ctacte ultimoCapital = null;
    	for (Ctacte cc : ccs) {
    		if (cc.isDebito()) {
    			
    			// busco capital de la primera NO cancelada para asignarle el boleto
    			if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
    				ultimoCapital = cc;
    				if (primeraCapitalNoCancelada == null && cc.getCuota().getEstado().equals(Cuota.SIN_CANCELAR)) {
    					primeraCapitalNoCancelada = cc;
    				}
    			}
    		}
    	}
    	if (primeraCapitalNoCancelada == null) { // estan todas canceladas
    		primeraCapitalNoCancelada = ultimoCapital; // tomar el ultimo debito de capital
    	}
    	
    	// usuario loggeado
        Usuario usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
        
        // uso como periodo el a�o de generacion = actual
        int periodo = Calendar.getInstance().get(Calendar.YEAR);
        
        //crear nuevo boleto
        Boleto boleto = new Boleto();
        boleto.setFechaEmision(fechaCobranza); 
        boleto.setImporte(deuda);
        boleto.setObjetoi(credito);
        boleto.setFechaVencimiento(fechaCobranza); 
        boleto.setTipo(Boleto.TIPO_BOLETO_CAPITAL);
        boleto.setUsuario(usuario);
        boleto.setPeriodoBoleto(new Long(periodo));
        
        // numerador debe incluir el periodo
        boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
        boleto.setVerificadorBoleto(0L);       
         
        bp.save(boleto);
        
        Concepto concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL);
    	
    	Bolcon bc = new Bolcon();
    	bc.setFacturado(concepto);
    	bc.setOriginal(concepto);
    	bc.setImporte(deuda);
    	
    	CtacteKey ck = primeraCapitalNoCancelada.getId();
    	
    	BolconKey bck = new BolconKey();
    	bck.setBoleto(boleto);
    	bck.setPeriodoCtacte(ck.getPeriodoCtacte());
    	bck.setMovimientoCtacte(ck.getMovimientoCtacte());
    	bck.setVerificadorCtacte(ck.getVerificadorCtacte());
    	bck.setItemCtacte(ck.getItemCtacte());
    	bc.setId(bck);
    	
    	//tipo movimiento debito
    	Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
    	bc.setTipomov(tipoDebito);
    	
    	bp.save(bc);
    	
    	return boleto;
	}
	
}
