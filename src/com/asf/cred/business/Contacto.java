/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.StringHelper;
import com.civitas.hibernate.persona.Persona;

/**
 * @author cnoguerol
 */
public class Contacto implements IProcess {
    // =========================ATRIBUTOS==================================
    private String forward = "contactoList";
    private HashMap<String, Object> errors;
    private String accion = "";
    private BusinessPersistance bp;
    private Persona personaTitular;
    private com.civitas.hibernate.persona.creditos.Contacto contacto;
    private List<com.civitas.hibernate.persona.creditos.Contacto> listaContactos;

    // =========================CONSTRUCTORES===============================
    public Contacto() {
        this.errors = new HashMap<String, Object>();
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        this.contacto = new com.civitas.hibernate.persona.creditos.Contacto();
        this.personaTitular = new Persona();
    }

    // =========================FUNCIONALIDAD===============================
    public boolean doProcess() {
        if (this.getAccion().equalsIgnoreCase("asignar")) {
            this.forward = "contacto";
        } else if (this.getAccion().equalsIgnoreCase("guardar")) {
            this.validarContacto();
            if (this.errors.isEmpty()) {
                this.asignarContacto();
                if (this.errors.isEmpty()) {
                    this.forward = "contactoList";
                } else {
                    this.forward = "contacto";
                }
            } else {
                this.forward = "contacto";
            }
        } else if (this.accion.equalsIgnoreCase("cancelar")) {
            this.forward = "contactoList";
        } else if (this.getAccion().equalsIgnoreCase("eliminar")) {
            this.eliminarContacto();
        } else if (this.getAccion().equalsIgnoreCase("load")) {
            this.cargarContacto();
            this.forward = "contacto";
        }

        this.listarContactos();

        return this.errors.isEmpty();
    }

    // =========================UTILIDADES PRIVADAS=============================
    /**
     * Validaciones a Realizar:
     * 1) El Medio de Contacto es Obligatorio.-
     * 2) El Tipo de Contacto es Obligatorio.-
     * 3) El Detalle es Obligatorio.-
     * 4) Solo debe existir un contacto principal para un medio
     */
    private void validarContacto() {
    	
    	
        // 1)
        if (this.getContacto().getMedio() == null || this.getContacto().getMedio().equals("")) {
            this.errors.put("Contacto.Medio.Obligatorio", "Contacto.Medio.Obligatorio");
        }
        // 2)
        if (this.getContacto().getTipo() == null || this.getContacto().getTipo().equals("")) {
            this.errors.put("Contacto.TipoContacto.Obligatorio", "Contacto.TipoContacto.Obligatorio");
        }
        // 3)
        if (this.getContacto().getDetalle() == null || this.getContacto().getDetalle().trim().equals("")) {
            this.errors.put("Contacto.Detalle.Obligatorio", "Contacto.Detalle.Obligatorio");
        }else{
        	 if(this.getContacto().getMedio().equals("E-Mail")){
             	if(!StringHelper.validarEmail(this.getContacto().getDetalle().trim())){
             		this.errors.put("Contacto.Detalle.Mail.MalFormado", "Contacto.Detalle.Mail.MalFormado");
             	}
             }
        }
        // 4) Solo debe existir un contacto principal para un medio
        
        if(this.getContacto().getId() != null && this.getContacto().getId() == 0L){
            List<Contacto> listaContactoPrincipal = (ArrayList<Contacto>)this.bp.createQuery("from Contacto c where c.tipo = 'Principal' and c.medio = '"+
            this.getContacto().getMedio()+"' and c.persona.idpersona = :idPersona")
            .setParameter("idPersona", this.getPersonaTitular().getId()).list();
            
            if(!listaContactoPrincipal.isEmpty() && this.getContacto().getTipo().equals("Principal")){
                this.errors.put("Contacto.Medio.Pricipal.Unico", "Contacto.Medio.Pricipal.Unico"); 
            }
        
        }
        
       
        
        
        
    }

    private void asignarContacto() {
        try {
            this.setPersonaTitular((Persona) this.bp.getById(Persona.class, this.getPersonaTitular().getId()));
            this.getContacto().setPersona(this.getPersonaTitular());
            this.bp.saveOrUpdate(this.getContacto());
            this.setContacto(new com.civitas.hibernate.persona.creditos.Contacto());
        }
        catch (Exception e) {
            this.errors.put("Contacto.Error.guardar", "Contacto.Error.guardar");
            e.printStackTrace();
        }
    }

    private void eliminarContacto() {
        if (this.contacto.getId() != null) {
            try {
                this.bp.delete(this.contacto);
            }
            catch (Exception e) {
                this.errors.put("Contacto.Error.eliminar", "Contacto.Error.eliminar");
                e.printStackTrace();
            }
        }
    }

    private void cargarContacto() {
        if (this.getContacto() != null) {
            try {
                this.setContacto((com.civitas.hibernate.persona.creditos.Contacto) this.bp.getById(com.civitas.hibernate.persona.creditos.Contacto.class, this.getContacto().getId()));
            }
            catch (Exception e) {
                this.setContacto(new com.civitas.hibernate.persona.creditos.Contacto());
                this.errors.put("Contacto.Error.cargar", "Contacto.Error.carga");
                e.printStackTrace();
            }
        }
    }

    private void listarContactos() {
        if (this.personaTitular != null) {
            try {
                this.personaTitular = (Persona) this.bp.getById(Persona.class, this.personaTitular.getId());
                String consulta = "SELECT c FROM Contacto c";
                consulta += " WHERE c.persona.id = '" + this.personaTitular.getId().longValue() + "'";
                this.listaContactos = this.bp.getByFilter(consulta);
            }
            catch (Exception e) {
                this.listaContactos = new ArrayList<com.civitas.hibernate.persona.creditos.Contacto>();
            }
        }
    }

    // =========================GETTERS y SETTERS=============================
    public HashMap<String, Object> getErrors() {
        return this.errors;
    }

    public Object getResult() {
        return null;
    }

    public String getInput() {
        return this.forward;
    }

    public String getForward() {
        return this.forward;
    }

    public String getAccion() {
        return this.accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public com.civitas.hibernate.persona.creditos.Contacto getContacto() {
        return this.contacto;
    }

    public void setContacto(com.civitas.hibernate.persona.creditos.Contacto contacto) {
        this.contacto = contacto;
    }

    public Persona getPersonaTitular() {
        return this.personaTitular;
    }

    public void setPersonaTitular(Persona personaTitular) {
        this.personaTitular = personaTitular;
    }

    public List<com.civitas.hibernate.persona.creditos.Contacto> getListaContactos() {
        return this.listaContactos;
    }

    public void setListaContactos(List<com.civitas.hibernate.persona.creditos.Contacto> listaContactos) {
        this.listaContactos = listaContactos;
    }

    // =========================VALIDACIONES=============================
    public boolean validate() {
        return this.errors.isEmpty();
    }

}
