package com.asf.cred.business;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.nirven.creditos.hibernate.CuotaBonTasa;

public class BonTasaCuotas extends BonTasaProcess {
	private List<CuotaBonTasa> cuotas;
	private double desembolsado;
	private Long idCuota;

	private Long[] ids;
	private String[] fechas;
	private String[] montos;
	private String[] porcentajes;
	protected HashMap<String, Object> errores;
	private ReportResult reportResult;
	private boolean error;
	private String detalleError;
	private String cuotasSeleccionadas;

	private Integer numeroOrdenPago;
	private String observaciones;
	private Date fechaPago;

	public BonTasaCuotas() {
		ids = new Long[50];
		fechas = new String[50];
		montos = new String[50];
		porcentajes = new String[50];
	}

	@Override
	public boolean doProcess() {

		if (getAccion() == null) {
			buscarBonTasa();
			buscarCuotas();
			inicializarArreglos();
			super.setForward("BonTasaCuotas");
		} else if (getAccion().equalsIgnoreCase("modificar")) {
			buscarCuota();
			super.setForward("BonTasaCuotas");
		} else if (getAccion().equals("imprimirReporte")) {
			System.out.println("**** ENTRO A CREAR EL REPORTE");
			getReportResult();
			if (this.getErrores().isEmpty()) {
				setForward("Reportes");
			}
		} else if (getAccion().equalsIgnoreCase("pagar")) {
			fechaPago = new Date();
			cargarCuotasSeleccionadas();
			super.setForward("PagoCuotaBonTasa");

		}

		return true;
	}

	private void cargarCuotasSeleccionadas() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();

		setCuotasSeleccionadas("");

		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			System.out.println(param);
			if (param.startsWith("cuota")) {
				String[] p = param.split("-");
				setCuotasSeleccionadas(getCuotasSeleccionadas() + new String(p[1]) + ",");
			}
		}
	}

	private void buscarCuota() {
		bp.getById(CuotaBonTasa.class, idCuota);
	}

	private void getReportResult() {
		try {
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName("InformeCuota.jasper");
			reportResult.setParams("idObjetoi=" + getIdBonTasa() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

		} catch (Exception e) {
			this.getErrores().put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "InformeCuota"));
		}
	}

	@SuppressWarnings("unchecked")
	private boolean buscarCuotas() {
		cuotas = bp.createQuery("Select c from CuotaBonTasa c where c.bonTasa = :bonTasa")
				.setParameter("bonTasa", getBonificacion()).list();
		return true;
	}

	private void inicializarArreglos() {
		ids = new Long[cuotas.size()];
		fechas = new String[cuotas.size()];
		montos = new String[cuotas.size()];
		porcentajes = new String[cuotas.size()];

		for (int i = 0; i < cuotas.size(); i++) {
			ids[i] = cuotas.get(i).getId();
			fechas[i] = cuotas.get(i).getFechaVencimientoStr();
			double capital = cuotas.get(i).getCapital();
			montos[i] = String.format("%.2f", capital);
			porcentajes[i] = String.format("%.4f", capital / desembolsado * 100);
		}
	}

	public Double getTotalBonificaciones() {
		Double total = 0.0;
		for (CuotaBonTasa cuota : cuotas) {
			total += (new Double(Math.round(cuota.getSubsidio() * 100))) / 100;
		}
		return (new Double(Math.round(total * 100))) / 100;
	}

	@Override
	public String getForward() {
		return super.getForward();
	}

	@Override
	public String getInput() {
		return super.getForward();
	}

	public List<CuotaBonTasa> getCuotas() {
		return cuotas;
	}

	public void setCuotas(List<CuotaBonTasa> cuotas) {
		this.cuotas = cuotas;
	}

	public double getDesembolsado() {
		return desembolsado;
	}

	public void setDesembolsado(double desembolsado) {
		this.desembolsado = desembolsado;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String[] getFechas() {
		return fechas;
	}

	public void setFechas(String[] fechas) {
		this.fechas = fechas;
	}

	public String[] getMontos() {
		return montos;
	}

	public void setMontos(String[] montos) {
		this.montos = montos;
	}

	public String[] getPorcentajes() {
		return porcentajes;
	}

	public void setPorcentajes(String[] porcentajes) {
		this.porcentajes = porcentajes;
	}

	public Object getResult() {
		return reportResult;
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getDetalleError() {
		return detalleError;
	}

	public void setDetalleError(String detalleError) {
		this.detalleError = detalleError;
	}

	public String getFechaPago() {
		return DateHelper.getString(fechaPago);
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = DateHelper.getDate(fechaPago);
	}

	public Integer getNumeroOrdenPago() {
		return numeroOrdenPago;
	}

	public void setNumeroOrdenPago(Integer numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getCuotasSeleccionadas() {
		return cuotasSeleccionadas;
	}

	public void setCuotasSeleccionadas(String cuotasSeleccionadas) {
		this.cuotasSeleccionadas = cuotasSeleccionadas;
	}

}
