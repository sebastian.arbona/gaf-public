package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;

import org.apache.struts.action.ActionMessage;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.nirven.creditos.bpm.ImputacionesHelper;
import com.nirven.creditos.hibernate.Desembolso;

public class DesembolsoHelper {

	public static HashMap<String, Object> solicitar(Desembolso desembolso) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		HashMap<String, Object> errores = new HashMap<String, Object>();
		bp.getCurrentSession().evict(desembolso);
		if (desembolso.getImporteSolicitado() == null || desembolso.getImporteSolicitado().doubleValue() == 0.0) {
			errores.put("desembolsoMasivo.imputacion.importe", "desembolsoMasivo.imputacion.importe");
		} else {
			ImputacionesHelper ih = new ImputacionesHelper();
			ih.setEtapa(3l);
			ih.setTipoExpediente(15l);
			try {
				ih.imputar(desembolso);
			} catch (Exception e) {
				e.printStackTrace();
				errores.put("desembolsosEjecucion.SolicitarGafPermiso",
						new ActionMessage("desembolsosEjecucion.SolicitarGafPermiso", e.getMessage()));

			}
			bp.getCurrentSession().refresh(desembolso);
			desembolso.setEstado("1"); // PENDIENTE
			desembolso.setFechaSolicitud(new Date());
			if (desembolso.getCredito().getLinea().getMoneda().getDenominacion().equalsIgnoreCase("Dolar")) {
				desembolso.setCotizacion(desembolso.getCredito().getLinea().getMoneda().getCotizacion());
			}
			bp.update(desembolso);
		}
		return errores;
	}

}
