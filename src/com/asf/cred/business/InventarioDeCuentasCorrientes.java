package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.gaf.hibernate.Area;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;

public class InventarioDeCuentasCorrientes implements IProcess {

	private static final int NO_MOSTRAR_TABLA = 0;
	private static final int MOSTRAR_TABLA = 1;
	private static final int PESOS = 1;
	private static final int FECHA_PROCESO = 1;
	private static final int PROCESO_POR_SIN_FILTRO = 1;

	private Date desdeFecha;
	private Date hastaFecha;
	private Date informacionAFecha;
	private int filtroMoneda;
	private int fechas;
	private int mostrar;
	private int proceso;
	private String usuario;

	private List<InventarioDeCuentasCorrientesBean> inventarioDeCuentasCorrientesBeanList;

	private String numeroAtencion;
	private ReportResult reporte;
	private BusinessPersistance bp;
	private String accion;
	private HashMap<String, String> errores;
	private String forward = "InventarioDeCuentasCorrientes";

	private List<Linea> lineas;
	private String[] lineasSeleccionadas;
	private List<Estado> estados;
	private String[] estadosSeleccionados;
	private List<Area> areas;
	private String[] areasSeleccionadas;
	private Long cuit;
	private String directorIdEstados = "";

	@SuppressWarnings("unchecked")
	public InventarioDeCuentasCorrientes() {
		try {
			Linea linea;

			this.accion = "";
			this.filtroMoneda = PESOS;
			this.fechas = FECHA_PROCESO;
			this.proceso = PROCESO_POR_SIN_FILTRO;
			this.mostrar = NO_MOSTRAR_TABLA;
			this.errores = new HashMap<String, String>();
			this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			this.lineas = bp.createQuery("SELECT l FROM Linea l WHERE l.nombre IS NOT NULL ORDER BY l.id").list();
			this.areas = bp
					.createQuery("SELECT u FROM Unidad u WHERE codigo in ('DEPTESA', 'LEES', 'DSECRE', 'SUDDRL') "
							+ "ORDER BY u.nombre")
					.list();

			for (int i = 0; i < this.lineas.size(); i++) {
				linea = lineas.get(i);
				linea.setNombre(linea.getId() + " - " + linea.getNombre());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void filtraEstadosVisiblesPorUsuario() {
		// Filtrar los estados por tipo de usuario.
		if (this.usuario != null && this.usuario.equals("ftyc")) {
			this.directorIdEstados = (String) bp
					.createQuery("SELECT d.valor FROM Director d WHERE d.id = 'idInventarioDeCuentasCorrientes'").list()
					.get(0);
		} else if (this.usuario != null && this.usuario.equals("tribunalDeCuentas")) {
			this.directorIdEstados = (String) bp
					.createQuery("SELECT d.valor FROM Director d WHERE d.id = 'idTribunalDeCuentasEstado'").list()
					.get(0);
		}

		if (this.directorIdEstados.trim().length() > 0) {
			this.estados = bp.createQuery(
					"SELECT e FROM Estado e WHERE e.nombreEstado IS NOT NULL AND e.tipo='Objetoi' AND e.id IN ("
							+ this.directorIdEstados + ") ORDER BY e.nombreEstado")
					.list();
		} else {
			this.estados = bp.createQuery(
					"SELECT e FROM Estado e WHERE e.nombreEstado IS NOT NULL AND e.tipo='Objetoi' ORDER BY e.nombreEstado")
					.list();
		}
	}

	private String procesarPor(String columna, String[] id) {
		String estractoConsulta = "";
		if (id != null && id.length > 0) {
			estractoConsulta = "AND " + columna + " IN (";
			for (int i = 0; i < id.length; i++) {
				estractoConsulta += id[i] + ", ";
			}
			estractoConsulta = estractoConsulta.substring(0, estractoConsulta.length() - 2) + ") ";
		}
		return estractoConsulta;
	}

	private String procesarPorEstado(String columna, String[] id) {
		String estractoConsulta = "";
		if (id != null && id.length > 0) {
			estractoConsulta = "AND " + columna + " IN (";
			for (int i = 0; i < id.length; i++) {
				estractoConsulta += id[i] + ", ";
			}
			estractoConsulta = estractoConsulta.substring(0, estractoConsulta.length() - 2) + ") ";
		} else
			estractoConsulta = "AND " + columna + " IN (" + this.directorIdEstados + ") ";
		return estractoConsulta;
	}

	@SuppressWarnings("static-access")
	private String AreaComportamientoPago(String area) {
		String estractoConsulta = "";
		ObjetoiComportamiento comportamiento = new ObjetoiComportamiento();
		if (area.equals("DEPTESA")) {
			estractoConsulta = "(area.codigo = 'DEPTESA' AND objc.comportamientoPago = "
					+ comportamiento.SITUACION_NORMAL + ")";
		} else if (area.equals("DSECRE")) {
			estractoConsulta = "(area.codigo = 'DSECRE' AND (objc.comportamientoPago = "
					+ comportamiento.INADECUADO_CON_ATRASO + " OR objc.comportamientoPago = "
					+ comportamiento.CON_PROBLEMAS_DE_ATRASO + " OR objc.comportamientoPago = "
					+ comportamiento.ALTO_RIESGO_CON_ATRASO + "))";
		} else if (area.equals("LEES")) {
			estractoConsulta = "(area.codigo = 'LEES' AND (objc.comportamientoPago = " + comportamiento.INCOBRABLE + " "
					+ "OR objc.comportamientoPago = " + comportamiento.CONCURSADOS + " "
					+ "OR objc.comportamientoPago = " + comportamiento.CREDITOS_EN_GESTION_JUDICIAL + "))";
		} else if (area.equals("SUDDRL")) {
			estractoConsulta = "(area.codigo = 'SUDDRL' AND objc.comportamientoPago = "
					+ comportamiento.RECLAMO_PREJUDICIAL + ")";
		}
		return estractoConsulta;
	}

	private String left(String[] lista) {
		if (lista != null && lista.length > 0)
			return "";
		return "LEFT ";
	}

	@SuppressWarnings("unchecked")
	public boolean doProcess() {
		this.filtraEstadosVisiblesPorUsuario();

		if (this.accion.equals("cargarTabla")) {
			String consulta;
			List<Object> objectInventarioDeCuentasCorrientesBean;
			Object[] filaObjectInventarioDeCuentasCorrientesBean;
			InventarioDeCuentasCorrientesBean inventarioDeCuentasCorrientesBean;
			InventarioDeCuentasCorrientesBean saldoInventarioDeCuentasCorrientesBean;
			String monedaAbreviatura;
			String fechaDesde;
			String fechaHasta;
			String fechaInformacionAFecha;
			String fechaNoCorriente;
			String fechaCtacte;
			String cuitStr;
			String cuentaPatrimonialAnterior;
			String leftJoinArea;
			String cotizacion, cotizacionDesembolso;

			this.mostrar = MOSTRAR_TABLA;

			if (this.filtroMoneda == PESOS)
				monedaAbreviatura = "Peso";
			else
				monedaAbreviatura = "Dolar";

			fechaDesde = this.getDesdeFechaStr();
			fechaDesde = fechaDesde.substring(fechaDesde.lastIndexOf("/") + 1) + "-"
					+ fechaDesde.substring(fechaDesde.indexOf("/") + 1, fechaDesde.lastIndexOf("/")) + "-"
					+ fechaDesde.substring(0, fechaDesde.indexOf("/")) + " 00:00:00";

			fechaHasta = this.getHastaFechaStr();
			fechaHasta = fechaHasta.substring(fechaHasta.lastIndexOf("/") + 1) + "-"
					+ fechaHasta.substring(fechaHasta.indexOf("/") + 1, fechaHasta.lastIndexOf("/")) + "-"
					+ fechaHasta.substring(0, fechaHasta.indexOf("/")) + " 23:59:59";

			fechaInformacionAFecha = this.getInformacionAFechaStr();
			fechaInformacionAFecha = fechaInformacionAFecha.substring(fechaInformacionAFecha.lastIndexOf("/") + 1) + "-"
					+ fechaInformacionAFecha.substring(fechaInformacionAFecha.indexOf("/") + 1,
							fechaInformacionAFecha.lastIndexOf("/"))
					+ "-" + fechaInformacionAFecha.substring(0, fechaInformacionAFecha.indexOf("/")) + " 23:59:59";

			fechaNoCorriente = this.getInformacionAFechaStr();
			fechaNoCorriente = Integer.parseInt(fechaNoCorriente.substring(fechaNoCorriente.lastIndexOf("/") + 1)) + 1
					+ "-01-01 00:00:00";

			if (this.fechas == FECHA_PROCESO)
				fechaCtacte = "fechaProceso";
			else
				fechaCtacte = "fechaVencimiento";

			boolean ultimoDia = false;
			String periodo = "";
			if (this.fechas == FECHA_PROCESO) {
				Calendar c = Calendar.getInstance();
				c.setTime(DateHelper.getDate(getInformacionAFechaStr()));
				if (c.get(Calendar.DATE) == 31 && c.get(Calendar.MONTH) == Calendar.DECEMBER) {
					ultimoDia = true;
				}
				periodo = Integer.toString(c.get(Calendar.YEAR));
			}

			if (this.cuit > 0)
				cuitStr = "AND per.idpersona = " + this.cuit + " ";
			else
				cuitStr = "";

			leftJoinArea = left(this.areasSeleccionadas);

			cotizacion = "(SELECT TOP 1 CASE WHEN cotiza.DTYPE = 'CtaCteAjuste' "
					+ "THEN (CASE WHEN cotiza.cotizacionDiferencia IS NULL THEN 1 ELSE cotiza.cotizacionDiferencia END) "
					+ "ELSE (CASE WHEN cotiza.cotizaMov IS NULL THEN 1 ELSE cotiza.cotizaMov END) END "
					+ "FROM ctacte cotiza WHERE cotiza.objetoi_id = cc.objetoi_id AND cotiza." + fechaCtacte + " = "
					+ "(SELECT MAX(cta." + fechaCtacte + ") FROM ctacte cta WHERE cta." + fechaCtacte + " <= '"
					+ fechaInformacionAFecha + "' AND cta.objetoi_id = cotiza.objetoi_id)) ";

			cotizacionDesembolso = "(SELECT TOP 1 CASE WHEN cotiza.DTYPE = 'CtaCteAjuste' "
					+ "THEN (CASE WHEN cotiza.cotizacionDiferencia IS NULL THEN 1 ELSE cotiza.cotizacionDiferencia END) "
					+ "ELSE (CASE WHEN cotiza.cotizaMov IS NULL THEN 1 ELSE cotiza.cotizaMov END) END "
					+ "FROM ctacte cotiza WHERE cotiza.objetoi_id = des.credito_id AND cotiza." + fechaCtacte + " = "
					+ "(SELECT MAX(cta." + fechaCtacte + ") FROM ctacte cta WHERE cta." + fechaCtacte + " <= '"
					+ fechaInformacionAFecha + "' AND cta.objetoi_id = cotiza.objetoi_id)) ";

			consulta =
					/* 0,1,2 */
					"SELECT obj.numeroAtencion, per.nomb_12, per.CUIL_12, "
							/* 3 */
							+ "(CASE WHEN lin.codigoCtaContablePatrimonioCapital IS NULL THEN 'Sin c�digo' ELSE lin.codigoCtaContablePatrimonioCapital END) 'codigoCtaContablePatrimonioCapital', "
							/* 4,5 */
							+ "lin.nombre, obj.resolucion, "
							/* 6 */
							+ "(CASE WHEN saldoCapital.saldoCapitalOriginal IS NULL THEN 0 ELSE saldoCapital.saldoCapitalOriginal END) 'saldoCapitalOriginal', "
							/* 7 */
							+ "(CASE WHEN saldoCapital.saldoCapitalPesos IS NULL THEN 0 ELSE saldoCapital.saldoCapitalPesos END) 'saldoCapitalPesos', "
							/* 8 */
							+ "(CASE WHEN corriente.saldoCorrienteOriginal IS NULL THEN 0 ELSE corriente.saldoCorrienteOriginal END) 'saldoCorrienteOriginal', "
							/* 9 */
							+ "(CASE WHEN corriente.saldoNoCorrienteOriginal IS NULL THEN 0 ELSE corriente.saldoNoCorrienteOriginal END) 'saldoNoCorrienteOriginal', "
							/* 10 */
							+ "(CASE WHEN corriente.saldoCorrientePesos IS NULL THEN 0 ELSE corriente.saldoCorrientePesos END) 'saldoCorrientePesos', "
							/* 11 */
							+ "(CASE WHEN lin.CERid IS NULL THEN '' ELSE lin.CERid END) 'CER', "
							/* 12 */
							+ "(CASE WHEN otros.otrosOriginal IS NULL THEN 0 ELSE otros.otrosOriginal END) 'otrosOriginal', "
							/* 13 */
							+ "((CASE WHEN saldoCapital.saldoCapitalOriginal IS NULL THEN 0 ELSE saldoCapital.saldoCapitalOriginal END) + (CASE WHEN intereses.interesesOriginal IS NULL THEN 0 ELSE intereses.interesesOriginal END) + (CASE WHEN otros.otrosOriginal IS NULL THEN 0 ELSE otros.otrosOriginal END)) 'saldoTotalOriginal', "
							/* 14 */
							+ "est.nombreEstado, "
							/* 15 */
							+ "(CASE est.nombreEstado WHEN 'GESTION JUDICIAL' THEN 'DEPTO. ASESORIA LETRADA (LEGALES)' WHEN 'GESTION EXTRAJUDICIAL' THEN 'SUBDIRECCION DE DESARROLLO' ELSE comportamiento.nombre END) 'comportamientoNombre', "
							/* 16,17 */
							+ "obj.numeroCredito, obj.expediente, "
							/* 18,19 */
							+ "(CASE WHEN obj.fechaMutuo IS NULL THEN obj.fechaFirmaContrato ELSE obj.fechaMutuo END) 'fechaMutuo', mon.abreviatura, "
							/* 20 */
							+ "(CASE when est.nombreEstado in ('EJECUCION','PRIMER DESEMBOLSO','PENDIENTE SEGUNDO DESEMBOLSO') THEN comportamiento.TF_DESCRIPCION ELSE 'N/A' END) 'comportamientoPago', "
							/* 21 */
							+ "(CASE WHEN intcomp.intcompOriginal IS NULL THEN 0 ELSE intcomp.intcompOriginal END) 'intcompOriginal', "
							/* 22 */
							+ "(CASE WHEN intmor.intmorOriginal IS NULL THEN 0 ELSE intmor.intmorOriginal END) 'intmorOriginal', "
							/* 23 */
							+ "(CASE WHEN intpun.intpunOriginal IS NULL THEN 0 ELSE intpun.intpunOriginal END) 'intpunOriginal', "
							/* 24 */
							+ "(CASE WHEN desemb.desembOriginal IS NULL THEN 0 ELSE desemb.desembOriginal END) 'desembolsadoOriginal', "
							/* 25 */
							+ "(CASE WHEN corriente.saldoNoCorrientePesos IS NULL THEN 0 ELSE corriente.saldoNoCorrientePesos END) 'saldoNoCorrientePesos', "
							/* 26 */
							+ "(CASE WHEN intcomp.intcompPesos IS NULL THEN 0 ELSE intcomp.intcompPesos END) 'intcompPesos', "
							/* 27 */
							+ "(CASE WHEN intmor.intmorPesos IS NULL THEN 0 ELSE intmor.intmorPesos END) 'intmorPesos', "
							/* 28 */
							+ "(CASE WHEN intpun.intpunPesos IS NULL THEN 0 ELSE intpun.intpunPesos END) 'intpunPesos', "
							/* 29 */
							+ "(CASE WHEN desemb.desembPesos IS NULL THEN 0 ELSE desemb.desembPesos END) 'desembolsadoPesos', "
							/* 30 */
							+ "(CASE WHEN otros.otrosPesos IS NULL THEN 0 ELSE otros.otrosPesos END) 'otrosPesos', "
							/* 31 */
							+ "((CASE WHEN saldoCapital.saldoCapitalPesos IS NULL THEN 0 ELSE saldoCapital.saldoCapitalPesos END) + (CASE WHEN intereses.interesesPesos IS NULL THEN 0 ELSE intereses.interesesPesos END) + (CASE WHEN otros.otrosPesos IS NULL THEN 0 ELSE otros.otrosPesos END)) 'saldoTotalPesos', "
							/* 32 */
							+ "(CASE WHEN cer.cer IS NULL THEN 0 ELSE cer.cer END) 'valorCer', "
							/* 33 */
							+ "(CASE WHEN ap.id IS NULL OR ap.judicial = 0 THEN 'NO' ELSE 'SI' END) 'judicial' "
							+ "FROM objetoi obj LEFT JOIN AcuerdoPago ap ON ap.creditoAcuerdo_id = obj.id "
							+ "JOIN persona per ON per.idpersona = obj.persona_idpersona " + cuitStr
							+ "JOIN linea lin ON lin.id = obj.linea_id "
							+ procesarPor("lin.id", this.lineasSeleccionadas)
							+ "JOIN moneda mon ON lin.moneda_idmoneda = mon.idMoneda AND mon.abreviatura = '"
							+ monedaAbreviatura + "' JOIN objetoiestado obje ON obje.objetoi_id = obj.id "
							+ "AND obje.fecha = (SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE obje.objetoi_id = obje2.objetoi_id "
							+ "AND obje2.fecha <= '" + fechaInformacionAFecha
							+ "' AND obje2.estado_idestado IS NOT NULL) "
							+ "JOIN estado est ON est.idEstado = obje.estado_idEstado "
							+ procesarPorEstado("est.idEstado", this.estadosSeleccionados) + leftJoinArea + "JOIN "
							+ "(SELECT objc.objetoi_id, area.nombre, TC.TF_DESCRIPCION FROM objetoiComportamiento objc "
							+ "JOIN unidad area ON (" + AreaComportamientoPago("DEPTESA") + " OR "
							+ AreaComportamientoPago("DSECRE") + " OR " + AreaComportamientoPago("LEES") + " OR "
							+ AreaComportamientoPago("SUDDRL") + ") " + procesarPor("area.id", this.areasSeleccionadas)
							+ "LEFT JOIN TIPIFICADORES TC ON objc.comportamientoPago = TC.TF_CODIGO AND TC.TF_CATEGORIA = 'comportamientoPago' "
							+ "WHERE objc.fecha = (SELECT MAX(objc2.fecha) FROM ObjetoiComportamiento objc2 WHERE objc.objetoi_id = objc2.objetoi_id "
							+ "AND objc2.fecha <= '" + fechaInformacionAFecha
							+ "' AND objc2.comportamientoPago IS NOT NULL)) comportamiento "
							+ "ON comportamiento.objetoi_id = obj.id LEFT JOIN (SELECT cc.objetoi_id, "
							// saldoCapitalOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'saldoCapitalOriginal', "
							// saldoCapitalPesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
							+ " 'saldoCapitalPesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN('cap') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) saldoCapital ON saldoCapital.objetoi_id = obj.id "
							+ "LEFT JOIN (SELECT cc.objetoi_id, "
							// interesesOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'interesesOriginal', "
							// interesesPesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
							+ " 'interesesPesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('com','mor','pun','bon') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) intereses ON intereses.objetoi_id = obj.id LEFT JOIN "
							+ "(SELECT cc.objetoi_id, "
							// intcompOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'intcompOriginal', "
							// intcompPesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
							+ " 'intcompPesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('com','bon') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) intcomp ON intcomp.objetoi_id = obj.id LEFT JOIN "
							+ "(SELECT cc.objetoi_id, "
							// intmorOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'intmorOriginal', "
							// intmorPesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
							+ "'intmorPesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('mor') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) intmor ON intmor.objetoi_id = obj.id LEFT JOIN "
							+ "(SELECT cc.objetoi_id, "
							// intpunOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'intpunOriginal', "
							// intpunPesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
							+ " 'intpunPesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('pun') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) intpun ON intpun.objetoi_id = obj.id "
							// CER
							+ "LEFT JOIN (SELECT cc.objetoi_id, "
							// cer
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'cer' "
							+ "FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('CER') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) cer ON cer.objetoi_id = obj.id LEFT JOIN "
							+ "(SELECT des.credito_id, "
							// desembOriginal
							+ "SUM(des.importeReal) 'desembOriginal', "
							// desembPesos
							+ "SUM(des.importeReal) * " + cotizacionDesembolso + "'desembPesos' "
							+ "FROM Desembolso des WHERE des.importeReal IS NOT NULL "
							+ "GROUP BY des.credito_id) desemb ON desemb.credito_id = obj.id LEFT JOIN "
							+ "(SELECT cc.objetoi_id, "
							// otrosOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'otrosOriginal', "
							// otrosPesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
							+ "'otrosPesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('rec','gas','mul') "
							+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
									: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
							+ "GROUP BY cc.objetoi_id) otros ON otros.objetoi_id = obj.id LEFT JOIN "
							+ "(SELECT cc.objetoi_id, "
							// saldoCorrienteOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento < '" + fechaNoCorriente
							+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento < '"
							+ fechaNoCorriente + "' THEN -cc.importe ELSE 0 END) END) 'saldoCorrienteOriginal', "
							// saldoCorrientePesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento < '" + fechaNoCorriente
							+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento < '"
							+ fechaNoCorriente + "' THEN -cc.importe ELSE 0 END) END) * " + cotizacion
							+ "'saldoCorrientePesos', "
							// saldoNoCorrienteOriginal
							+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento >= '" + fechaNoCorriente
							+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento >= '"
							+ fechaNoCorriente + "' THEN -cc.importe ELSE 0 END) END) 'saldoNoCorrienteOriginal', "
							// saldoNoCorrientePesos
							+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento >= '" + fechaNoCorriente
							+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento >= '"
							+ fechaNoCorriente + "' THEN -cc.importe ELSE 0 END) END) * " + cotizacion
							+ "'saldoNoCorrientePesos' FROM ctacte cc "
							+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN('cap','ADC') "
							+ "JOIN cuota cuo ON cc.cuota_id = cuo.id WHERE cc." + fechaCtacte + " <= '"
							+ fechaInformacionAFecha + "' GROUP BY cc.objetoi_id) corriente "
							+ "ON corriente.objetoi_id = obj.id "
							+ "WHERE 0 < (SELECT COUNT(des.id) FROM desembolso des WHERE des.fechaReal BETWEEN '"
							+ fechaDesde + "' AND '" + fechaHasta + "' "
							+ "AND des.fechaReal IS NOT NULL AND des.numero = 1 AND des.credito_id = obj.id) "
							+ "ORDER BY lin.codigoCtaContablePatrimonioCapital, obj.numeroAtencion;";

			System.out.println("<---------------- comienza consulta inventario proyectos ---------------->");
			System.out.println(consulta);
			System.out.println("<---------------- termina consulta inventario proyectos ---------------->");

			objectInventarioDeCuentasCorrientesBean = this.bp.createSQLQuery(consulta).list();
			this.inventarioDeCuentasCorrientesBeanList = new ArrayList<InventarioDeCuentasCorrientesBean>();

			cuentaPatrimonialAnterior = "";
			saldoInventarioDeCuentasCorrientesBean = new InventarioDeCuentasCorrientesBean();
			for (int i = 0; i < objectInventarioDeCuentasCorrientesBean.size(); i++) {
				filaObjectInventarioDeCuentasCorrientesBean = (Object[]) objectInventarioDeCuentasCorrientesBean.get(i);
				// Acumulando saldos agrupados por n�mero de cuenta pratimonial.
				if (cuentaPatrimonialAnterior.equals((String) filaObjectInventarioDeCuentasCorrientesBean[3])) {
					saldoInventarioDeCuentasCorrientesBean.setSaldoCapitalMonedaOriginal(
							saldoInventarioDeCuentasCorrientesBean.getSaldoCapitalMonedaOriginal()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[6]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalPesos(saldoInventarioDeCuentasCorrientesBean.getSaldoCapitalPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[7]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalCorriente(saldoInventarioDeCuentasCorrientesBean.getSaldoCapitalCorriente()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[8]);
					saldoInventarioDeCuentasCorrientesBean.setSaldoCapitalNoCorriente(
							saldoInventarioDeCuentasCorrientesBean.getSaldoCapitalNoCorriente()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[9]);
					saldoInventarioDeCuentasCorrientesBean.setSaldoCapitalNoCorrientePesos(
							saldoInventarioDeCuentasCorrientesBean.getSaldoCapitalNoCorrientePesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[25]);
					saldoInventarioDeCuentasCorrientesBean.setSaldoCapitalCorrientePesos(
							saldoInventarioDeCuentasCorrientesBean.getSaldoCapitalCorrientePesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[10]);
					saldoInventarioDeCuentasCorrientesBean
							.setCompensatorios(saldoInventarioDeCuentasCorrientesBean.getCompensatorios()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[21]);
					saldoInventarioDeCuentasCorrientesBean
							.setCompensatoriosPesos(saldoInventarioDeCuentasCorrientesBean.getCompensatoriosPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[26]);
					saldoInventarioDeCuentasCorrientesBean
							.setMoratorios(saldoInventarioDeCuentasCorrientesBean.getMoratorios()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[22]);
					saldoInventarioDeCuentasCorrientesBean
							.setMoratoriosPesos(saldoInventarioDeCuentasCorrientesBean.getMoratoriosPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[27]);
					saldoInventarioDeCuentasCorrientesBean
							.setPunitorios(saldoInventarioDeCuentasCorrientesBean.getPunitorios()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[23]);
					saldoInventarioDeCuentasCorrientesBean
							.setPunitoriosPesos(saldoInventarioDeCuentasCorrientesBean.getPunitoriosPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[28]);
					saldoInventarioDeCuentasCorrientesBean
							.setDesembolsos(saldoInventarioDeCuentasCorrientesBean.getDesembolsos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[24]);
					saldoInventarioDeCuentasCorrientesBean
							.setDesembolsosPesos(saldoInventarioDeCuentasCorrientesBean.getDesembolsosPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[29]);
					saldoInventarioDeCuentasCorrientesBean.setOtros(saldoInventarioDeCuentasCorrientesBean.getOtros()
							+ (Double) filaObjectInventarioDeCuentasCorrientesBean[12]);
					saldoInventarioDeCuentasCorrientesBean
							.setOtrosPesos(saldoInventarioDeCuentasCorrientesBean.getOtrosPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[30]);
					saldoInventarioDeCuentasCorrientesBean
							.setCerValor(saldoInventarioDeCuentasCorrientesBean.getCerValor()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[32]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoTotal(saldoInventarioDeCuentasCorrientesBean.getSaldoTotal()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[13]
									+ saldoInventarioDeCuentasCorrientesBean.getCerValor());
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoTotalPesos(saldoInventarioDeCuentasCorrientesBean.getSaldoTotalPesos()
									+ (Double) filaObjectInventarioDeCuentasCorrientesBean[31]);
				} else {
					if (i > 0)
						this.inventarioDeCuentasCorrientesBeanList.add(saldoInventarioDeCuentasCorrientesBean);
					cuentaPatrimonialAnterior = (String) filaObjectInventarioDeCuentasCorrientesBean[3];
					saldoInventarioDeCuentasCorrientesBean = new InventarioDeCuentasCorrientesBean();
					saldoInventarioDeCuentasCorrientesBean.setNumeroSolicitud("Subtotal por Cuenta Patrimonial");
					saldoInventarioDeCuentasCorrientesBean
							.setNumeroCuentaPatrimonial((String) filaObjectInventarioDeCuentasCorrientesBean[3]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalMonedaOriginal((Double) filaObjectInventarioDeCuentasCorrientesBean[6]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[7]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalCorriente((Double) filaObjectInventarioDeCuentasCorrientesBean[8]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalNoCorriente((Double) filaObjectInventarioDeCuentasCorrientesBean[9]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalNoCorrientePesos((Double) filaObjectInventarioDeCuentasCorrientesBean[25]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoCapitalCorrientePesos((Double) filaObjectInventarioDeCuentasCorrientesBean[10]);
					saldoInventarioDeCuentasCorrientesBean
							.setCompensatorios((Double) filaObjectInventarioDeCuentasCorrientesBean[21]);
					saldoInventarioDeCuentasCorrientesBean
							.setCompensatoriosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[26]);
					saldoInventarioDeCuentasCorrientesBean
							.setMoratorios((Double) filaObjectInventarioDeCuentasCorrientesBean[22]);
					saldoInventarioDeCuentasCorrientesBean
							.setMoratoriosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[27]);
					saldoInventarioDeCuentasCorrientesBean
							.setPunitorios((Double) filaObjectInventarioDeCuentasCorrientesBean[23]);
					saldoInventarioDeCuentasCorrientesBean
							.setPunitoriosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[28]);
					saldoInventarioDeCuentasCorrientesBean
							.setDesembolsos((Double) filaObjectInventarioDeCuentasCorrientesBean[24]);
					saldoInventarioDeCuentasCorrientesBean
							.setDesembolsosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[29]);
					saldoInventarioDeCuentasCorrientesBean
							.setOtros((Double) filaObjectInventarioDeCuentasCorrientesBean[12]);
					saldoInventarioDeCuentasCorrientesBean
							.setOtrosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[30]);
					saldoInventarioDeCuentasCorrientesBean
							.setCerValor((Double) filaObjectInventarioDeCuentasCorrientesBean[32]);
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoTotal((Double) filaObjectInventarioDeCuentasCorrientesBean[13]
									+ saldoInventarioDeCuentasCorrientesBean.getCerValor());
					saldoInventarioDeCuentasCorrientesBean
							.setSaldoTotalPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[31]);
				}
				inventarioDeCuentasCorrientesBean = new InventarioDeCuentasCorrientesBean();
				inventarioDeCuentasCorrientesBean
						.setNumeroSolicitud(((BigDecimal) filaObjectInventarioDeCuentasCorrientesBean[0]).toString());
				inventarioDeCuentasCorrientesBean
						.setIdentificacionTomador((String) filaObjectInventarioDeCuentasCorrientesBean[1]);
				inventarioDeCuentasCorrientesBean
						.setCuit(((BigDecimal) filaObjectInventarioDeCuentasCorrientesBean[2]).toString());
				inventarioDeCuentasCorrientesBean
						.setNumeroCuentaPatrimonial((String) filaObjectInventarioDeCuentasCorrientesBean[3]);
				inventarioDeCuentasCorrientesBean
						.setLineaCredito((String) filaObjectInventarioDeCuentasCorrientesBean[4]);
				inventarioDeCuentasCorrientesBean
						.setResolucionAprobatoria((String) filaObjectInventarioDeCuentasCorrientesBean[5]);
				inventarioDeCuentasCorrientesBean
						.setSaldoCapitalMonedaOriginal((Double) filaObjectInventarioDeCuentasCorrientesBean[6]);
				inventarioDeCuentasCorrientesBean
						.setSaldoCapitalPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[7]);
				inventarioDeCuentasCorrientesBean
						.setSaldoCapitalCorriente((Double) filaObjectInventarioDeCuentasCorrientesBean[8]);
				inventarioDeCuentasCorrientesBean
						.setSaldoCapitalNoCorriente((Double) filaObjectInventarioDeCuentasCorrientesBean[9]);
				inventarioDeCuentasCorrientesBean
						.setSaldoCapitalNoCorrientePesos((Double) filaObjectInventarioDeCuentasCorrientesBean[25]);
				inventarioDeCuentasCorrientesBean
						.setSaldoCapitalCorrientePesos((Double) filaObjectInventarioDeCuentasCorrientesBean[10]);
				inventarioDeCuentasCorrientesBean
						.setCompensatorios((Double) filaObjectInventarioDeCuentasCorrientesBean[21]);
				inventarioDeCuentasCorrientesBean
						.setCompensatoriosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[26]);
				inventarioDeCuentasCorrientesBean
						.setMoratorios((Double) filaObjectInventarioDeCuentasCorrientesBean[22]);
				inventarioDeCuentasCorrientesBean
						.setMoratoriosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[27]);
				inventarioDeCuentasCorrientesBean
						.setPunitorios((Double) filaObjectInventarioDeCuentasCorrientesBean[23]);
				inventarioDeCuentasCorrientesBean
						.setPunitoriosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[28]);
				inventarioDeCuentasCorrientesBean
						.setDesembolsos((Double) filaObjectInventarioDeCuentasCorrientesBean[24]);
				inventarioDeCuentasCorrientesBean
						.setDesembolsosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[29]);
				inventarioDeCuentasCorrientesBean.setCer((String) filaObjectInventarioDeCuentasCorrientesBean[11]);
				inventarioDeCuentasCorrientesBean.setCerValor((Double) filaObjectInventarioDeCuentasCorrientesBean[32]);
				inventarioDeCuentasCorrientesBean.setOtros((Double) filaObjectInventarioDeCuentasCorrientesBean[12]);
				inventarioDeCuentasCorrientesBean
						.setOtrosPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[30]);
				inventarioDeCuentasCorrientesBean.setSaldoTotal((Double) filaObjectInventarioDeCuentasCorrientesBean[13]
						+ inventarioDeCuentasCorrientesBean.getCerValor());
				inventarioDeCuentasCorrientesBean
						.setSaldoTotalPesos((Double) filaObjectInventarioDeCuentasCorrientesBean[31]);
				inventarioDeCuentasCorrientesBean
						.setEstadoCredito((String) filaObjectInventarioDeCuentasCorrientesBean[14]);
				inventarioDeCuentasCorrientesBean
						.setAreaResponsable((String) filaObjectInventarioDeCuentasCorrientesBean[15]);
				inventarioDeCuentasCorrientesBean
						.setNumeroCredito((String) filaObjectInventarioDeCuentasCorrientesBean[16]);
				inventarioDeCuentasCorrientesBean
						.setExpediente((String) filaObjectInventarioDeCuentasCorrientesBean[17]);
				inventarioDeCuentasCorrientesBean.setFechaMutuo((Date) filaObjectInventarioDeCuentasCorrientesBean[18]);
				inventarioDeCuentasCorrientesBean.setMoneda((String) filaObjectInventarioDeCuentasCorrientesBean[19]);
				inventarioDeCuentasCorrientesBean
						.setComportamientoPago((String) filaObjectInventarioDeCuentasCorrientesBean[20]);
				inventarioDeCuentasCorrientesBean.setJudicial((String) filaObjectInventarioDeCuentasCorrientesBean[33]);
				this.inventarioDeCuentasCorrientesBeanList.add(inventarioDeCuentasCorrientesBean);
			}
			// Insertando �ltimo saldo.
			if (objectInventarioDeCuentasCorrientesBean.size() > 0) {
				this.inventarioDeCuentasCorrientesBeanList.add(saldoInventarioDeCuentasCorrientesBean);
			}
		} else
			this.mostrar = NO_MOSTRAR_TABLA;
		return true;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public int getMostrar() {
		return mostrar;
	}

	public void setMostrar(int mostrar) {
		this.mostrar = mostrar;
	}

	public void setInventarioDeCuentasCorrientesBean(
			List<InventarioDeCuentasCorrientesBean> inventarioDeCuentasCorrientesBeanList) {
		this.inventarioDeCuentasCorrientesBeanList = inventarioDeCuentasCorrientesBeanList;
	}

	public List<InventarioDeCuentasCorrientesBean> getInventarioDeCuentasCorrientesBean() {
		return this.inventarioDeCuentasCorrientesBeanList;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	protected String getDefaultForward() {
		return forward;
	}

	public Date getDesdeFecha() {
		return desdeFecha;
	}

	public void setDesdeFecha(Date desdeFecha) {
		this.desdeFecha = desdeFecha;
	}

	public String getDesdeFechaStr() {
		return DateHelper.getString(desdeFecha);
	}

	public void setDesdeFechaStr(String f) {
		this.desdeFecha = DateHelper.getDate(f);
	}

	public Date getHastaFecha() {
		return hastaFecha;
	}

	public void setHastaFecha(Date hastaFecha) {
		this.hastaFecha = hastaFecha;
	}

	public String getHastaFechaStr() {
		return DateHelper.getString(hastaFecha);
	}

	public void setHastaFechaStr(String f) {
		setHastaFecha(DateHelper.getDate(f));
	}

	public Date getInformacionAFecha() {
		return this.informacionAFecha;
	}

	public void setInformacionAFecha(Date informacionAFecha) {
		this.informacionAFecha = informacionAFecha;
	}

	public String getInformacionAFechaStr() {
		return DateHelper.getString(this.informacionAFecha);
	}

	public void setInformacionAFechaStr(String informacionAFecha) {
		this.informacionAFecha = DateHelper.getDate(informacionAFecha);
	}

	public int getFiltroMoneda() {
		return this.filtroMoneda;
	}

	public void setFiltroMoneda(int filtroMoneda) {
		this.filtroMoneda = filtroMoneda;
	}

	public int getFechas() {
		return fechas;
	}

	public void setFechas(int fechas) {
		this.fechas = fechas;
	}

	public int getProceso() {
		return proceso;
	}

	public void setProceso(int proceso) {
		this.proceso = proceso;
	}

	public List<Linea> getLineas() {
		return this.lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String[] getLineasSeleccionadas() {
		return lineasSeleccionadas;
	}

	public void setLineasSeleccionadas(String[] lineasSeleccionadas) {
		this.lineasSeleccionadas = lineasSeleccionadas;
	}

	public List<Estado> getEstados() {
		return this.estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public String[] getEstadosSeleccionados() {
		return this.estadosSeleccionados;
	}

	public void setEstadosSeleccionados(String[] estadosSeleccionados) {
		this.estadosSeleccionados = estadosSeleccionados;
	}

	public List<Area> getAreas() {
		return this.areas;
	}

	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}

	public String[] getAreasSeleccionadas() {
		return areasSeleccionadas;
	}

	public void setAreasSeleccionadas(String[] areasSeleccionadas) {
		this.areasSeleccionadas = areasSeleccionadas;
	}

	public Collection getCollection(HashMap arg0) {
		return null;
	}

	public HashMap getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public boolean validate() {
		return true;
	}

	public Long getCuit() {
		return this.cuit;
	}

	public void setCuit(Long cuit) {
		this.cuit = cuit;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}