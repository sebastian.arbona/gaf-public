package com.asf.cred.business;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.EstadoInmovilizacion;
import com.nirven.creditos.hibernate.EstadoLiberacion;
//import com.asf.gaf.hibernate.GarantiaEstado;
//import com.asf.gaf.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Liberaciones;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class ConfirmarLiberacionesPendientes implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	private List<Objetoi> creditos;
	private Long id;
	private List<BeanCtaCte> beans;
	private ArrayList<ObjetoiDTO> creditosDTO;
	private ObjetoiDTO credito;
	private BusinessPersistance bp;
	private static String sql;
	private String msg;

	@SuppressWarnings("unchecked")
	public ConfirmarLiberacionesPendientes() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		credito = new ObjetoiDTO();
		creditosDTO = (ArrayList<ObjetoiDTO>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("ConfirmarLiberacionesPendientes.creditosDTO");
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(ConfirmarLiberacionesPendientes.class.getResourceAsStream("liberaciones.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion == null || accion.equals("listar")) {
			try {
				String sql = getSql();
				String tipoGarantia = DirectorHelper.getString("tipoGarantia.inmovilizacion");
				List<Object[]> filas = bp.createSQLQuery(sql).setParameterList("tipoGarantia", tipoGarantia.split(","))
						.list();
				creditosDTO = new ArrayList<ObjetoiDTO>();
				ObjetoiDTO objetoiDTO;
				for (Object[] fila : filas) {
					// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
					// id,numeroAtencion,nomb12,expediente,volumenContrato,producto,liberadoAlaFecha,ultimaLiberacion,deudaActual,nuevoVolumen,forzarLiberacion,saldoInmovilizado,liberacionSolicitada,liberacionSolicitar,esPendiente,tipoProducto
					// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
					// --------------- ---------------- ---------------- -----------------
					// -------------------- -------------------

					// objetoiDTO = new ObjetoiDTO(((BigDecimal) fila[0]).longValue(), ((BigDecimal)
					// fila[1]).longValue(), (fila[2] == null ? null : (String) fila[2]), (fila[3]
					// == null ? null
					// : (String) fila[3]), (fila[4] == null ? null : (Double) fila[4]), (fila[5] ==
					// null ? null : (String) fila[5]), (fila[6] == null ? null : (Double) fila[6]),
					// (fila[7] == null ? null : DateHelper.getString(new Date(((Timestamp)
					// fila[7]).getTime()))), (fila[8] == null ? null : (Double) fila[8]),
					// (((fila[9] == null) || ((fila[10] != null) && (Byte) fila[10] == 1)) ? 0 :
					// (Double) fila[9]));

					/*
					 * cnoguerol, reemplazo el constructor con parametros con el uso de setters para
					 * clarificar
					 */
					objetoiDTO = new ObjetoiDTO();
					objetoiDTO.setIdObjetoi(((BigDecimal) fila[0]).longValue());
					objetoiDTO.setNumeroAtencion(((BigDecimal) fila[1]).longValue());
					objetoiDTO.getTitular().setNomb12((fila[2] == null ? null : (String) fila[2]));
					objetoiDTO.setExpediente((fila[3] == null ? null : (String) fila[3]));
					objetoiDTO.setTotalInmovilizado(0D); // (fila[4] == null ? null : (Double) fila[4])
					// objetoiDTO.setProducto((fila[5] == null ? null : (String) fila[5]));
					objetoiDTO.setProducto((fila[4] == null ? null : (String) fila[4]));
					objetoiDTO.setLiberadoALaFecha(0D); // (fila[6] == null ? null : (Double) fila[6])
					objetoiDTO.setUltimaLiberacion(""); // (fila[7] == null ? null : DateHelper.getString(new
														// Date(((Timestamp) fila[7]).getTime())))
					// objetoiDTO.setDeudaActual ((fila[8] == null ? null : (Double) fila[8]));
					objetoiDTO.setDeudaActual((fila[5] == null ? null : (Double) fila[5]));
					// objetoiDTO.setNuevoVolumen ((((fila[9] == null) || ((fila[10] != null) &&
					// (Byte) fila[10] == 1)) ? 0 : (Double) fila[9])) ;
					objetoiDTO.setNuevoVolumen(
							(((fila[6] == null) || ((fila[7] != null) && (Byte) fila[7] == 1)) ? 0 : (Double) fila[6]));
					objetoiDTO.setSaldoInmovilizado(0D); // (fila[11] == null ? null : ((Number)
															// fila[11]).doubleValue());
					objetoiDTO.setLiberacionSolicitada(0D); // (fila[12] == null ? null : ((Number)
															// fila[12]).doubleValue());
					objetoiDTO.setLiberacionSolicitar(0D); // (fila[13] == null ? null : ((Number)
															// fila[13]).doubleValue());

					int esPendiente = ((Number) fila[8]).intValue();
					int tipoProducto = new Integer((String) fila[9]);

					/*
					 * cnoguerol, estos datos son los que no vienen ahora en la consulta y debo usar
					 * el getter desde objetoi
					 */
					Objetoi proyecto = objetoiDTO.getObjetoi();
					double inmovilizadoTotal = proyecto.getVolumenInmovilizadoProductoEstado(tipoProducto,
							EstadoInmovilizacion.REALIZADA);
					double liberadoTotal = proyecto.getVolumenLiberadoProductoEstado(tipoProducto,
							EstadoLiberacion.REALIZADA);
					double liberadoSolicitado = proyecto.getVolumenLiberadoProductoEstado(tipoProducto,
							EstadoLiberacion.SOLICITADA);
					double liberadoPendiente = proyecto.getVolumenLiberadoProductoEstado(tipoProducto,
							EstadoLiberacion.PENDIENTE);
					double saldoInmovilizado = inmovilizadoTotal - liberadoTotal;
					Date ultimaLiberacion = proyecto.getUltimaLiberacionProductoEstado(tipoProducto,
							EstadoLiberacion.REALIZADA);

					objetoiDTO.setTotalInmovilizado(inmovilizadoTotal);
					objetoiDTO.setLiberadoALaFecha(liberadoTotal); // (fila[6] == null ? null : (Double) fila[6])
					objetoiDTO.setUltimaLiberacion(DateHelper.getString(ultimaLiberacion)); // (fila[7] == null ? null :
																							// DateHelper.getString(new
																							// Date(((Timestamp)
																							// fila[7]).getTime())))
					objetoiDTO.setSaldoInmovilizado(saldoInmovilizado); // (fila[11] == null ? null : ((Number)
																		// fila[11]).doubleValue());
					objetoiDTO.setLiberacionSolicitada(liberadoSolicitado); // (fila[12] == null ? null : ((Number)
																			// fila[12]).doubleValue());
					objetoiDTO.setLiberacionSolicitar(liberadoPendiente); // (fila[13] == null ? null : ((Number)
																			// fila[13]).doubleValue());
					// Ticket 9949
					Double valAux = (Double) fila[10];
					objetoiDTO.setLibAforo(valAux.floatValue());
					valAux = (Double) fila[11];
					objetoiDTO.setLibPrecioProducto(valAux.floatValue());
					objetoiDTO.setLibImporte(
							(float) liberadoSolicitado * objetoiDTO.getLibPrecioProducto() / objetoiDTO.getLibAforo());

					if (esPendiente == 1
							|| (objetoiDTO.getSaldoInmovilizado() - objetoiDTO.getLiberacionSolicitada()) > objetoiDTO
									.getNuevoVolumen()) {
						objetoiDTO.analizarfiltros();
						creditosDTO.add(objetoiDTO);
					}

				}
				SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.setAttribute("ConfirmarLiberacionesPendientes.creditosDTO", creditosDTO);
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			forward = "ConfirmarLiberacionesPendientes";
		} else if (accion.equalsIgnoreCase("confirmar")) {
			HashSet<Long> seleccionados = getSeleccionados();
			if (seleccionados.isEmpty()) {
				return true;
			}
			bp.begin();
			Iterator<ObjetoiDTO> iteratorCreditos = creditosDTO.iterator();
			while (iteratorCreditos.hasNext()) {
				ObjetoiDTO dto = iteratorCreditos.next();
				if (seleccionados.contains(dto.getIdObjetoi())) {
					Objetoi credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());

					if (tieneLiberacionesSolicitada(credito)) {
						if (msg == null)
							msg = "Existen proyectos que ya tenían liberaciones solicitadas, por lo que no fueron procesados.";
						continue;
					}

					// buscar liberacion pendiente
					List<Liberaciones> libPendientes = bp.createQuery(
							"select l from Liberaciones l where l.objetoi.id = :idCredito and l.estado = 'PENDIENTE'")
							.setParameter("idCredito", dto.getIdObjetoi()).list();
					if (!libPendientes.isEmpty()) {
						for (Liberaciones liberacion : libPendientes) {
							liberacion.setEstado(EstadoLiberacion.SOLICITADA);
							bp.update(liberacion);
						}
					} else {
						// buscar producto actual
						String producto = (String) bp.createSQLQuery(
								"select distinct g.tipoProducto from ObjetoiGarantia og join Garantia g on og.garantia_id = g.id where og.objetoi_id = :idCredito "
										+ "and g.tipoProducto is not null")
								.setParameter("idCredito", credito.getId()).setMaxResults(1).uniqueResult();
						Liberaciones lib = new Liberaciones();
						lib.setEstado(EstadoLiberacion.SOLICITADA);
						lib.setFecha(new Date());
						lib.setObjetoi(credito);
						lib.setProducto(producto);
						// Ticket 9949
						lib.setAforo(dto.getLibAforo());
						lib.setPrecioProducto(dto.getLibPrecioProducto());
						Object sumVolumen = bp.getNamedQuery("Liberaciones.sumVolumenProducto")
								.setEntity("objetoi", credito).setParameter("producto", producto).uniqueResult();
						if (credito.getForzarLiberacion() != null && credito.getForzarLiberacion()) {
							lib.setVolumen((dto.getTotalInmovilizado() != null ? dto.getTotalInmovilizado() : 0D)
									- (sumVolumen != null ? (Double) sumVolumen : 0D));
						} else {
							lib.setVolumen((dto.getTotalInmovilizado() != null ? dto.getTotalInmovilizado() : 0D)
									- (dto.getNuevoVolumen() != null ? dto.getNuevoVolumen() : 0D)
									- (sumVolumen != null ? (Double) sumVolumen : 0D));
						}
						// Ticket 9949
						lib.setImporte(lib.getVolumen().floatValue() * dto.getLibPrecioProducto() / dto.getLibAforo());
						if (lib.getVolumen() != null && lib.getVolumen().doubleValue() > 0) {
							bp.save(lib);
						}
					}
					credito.setLiberar(false);
					credito.setAutorizado(true);
					bp.update(credito);
					// Ticket 9998. Esta funcionalidad estaba en InformeLiberacionProcess
					List<ObjetoiGarantia> objetoigarantias = bp
							.createQuery(
									"SELECT g from ObjetoiGarantia g where g.objetoi = :credito and g.baja is null")
							.setParameter("credito", credito).list();
					if (!objetoigarantias.isEmpty()) {
						// Double volumenLiberaciones = (Double)
						// bp.getNamedQuery("Liberaciones.sumVolumenProducto").setEntity("objetoi",
						// credito).setParameter("producto",
						// objetoigarantias.get(0).getGarantia().getTipoProducto()).uniqueResult();
						Double volumenLiberacionesRealizadas = (Double) bp
								.getNamedQuery("Liberaciones.sumVolumenProductoEstado").setEntity("objetoi", credito)
								.setParameter("producto", objetoigarantias.get(0).getGarantia().getTipoProducto())
								.setParameter("estado", EstadoLiberacion.REALIZADA).uniqueResult();
						Double volumenLiberacionesSolicitadas = (Double) bp
								.getNamedQuery("Liberaciones.sumVolumenProductoEstado").setEntity("objetoi", credito)
								.setParameter("producto", objetoigarantias.get(0).getGarantia().getTipoProducto())
								.setParameter("estado", EstadoLiberacion.SOLICITADA).uniqueResult();
						Double volumenLiberaciones = (volumenLiberacionesRealizadas != null
								? volumenLiberacionesRealizadas
								: 0D) + (volumenLiberacionesSolicitadas != null ? volumenLiberacionesSolicitadas : 0D);
						Double volumenInmovilizaciones = (Double) bp.getNamedQuery("Inmovilizaciones.sumVolumen")
								.setEntity("objetoi", credito).uniqueResult();
						Double saldo = (volumenInmovilizaciones != null ? volumenInmovilizaciones : 0D)
								- volumenLiberaciones;
						List<CosechaConfig> cosechas = bp.getNamedQuery("CosechaConfig.findByFecha")
								.setParameter("fecha", credito.getFechaSolicitud())
								.setParameter("varietales", credito.getVarietales()).list();
						CosechaConfig cosecha = cosechas.get(0);
						for (ObjetoiGarantia objetoiGarantia : objetoigarantias) {
							GarantiaEstado nuevoEstado = null;
							ObjetoiEstado estadoActual = credito.getEstadoActual();
							Double precio = 0d;
							if (saldo != 0 && objetoiGarantia.getGarantia() != null
									&& objetoiGarantia.getGarantia().getTipoProducto() != null) {
								if (objetoiGarantia.getGarantia().getTipoProducto().equalsIgnoreCase("3")) {
									precio = cosecha.getPrecioMosto();
									// nuevoEstado.setImporte(saldo * cosecha.getPrecioMosto());
								}
								if (objetoiGarantia.getGarantia().getTipoProducto().equalsIgnoreCase("2")) {
									precio = cosecha.getPrecioVinoBlanco();
									// nuevoEstado.setImporte(saldo * cosecha.getPrecioVinoBlanco());
								}
								if (objetoiGarantia.getGarantia().getTipoProducto().equalsIgnoreCase("1")) {
									precio = cosecha.getPrecioVinoTinto();
									// nuevoEstado.setImporte(saldo * cosecha.getPrecioVinoTinto());
								}
								if (objetoiGarantia.getGarantia().getTipoProducto().equalsIgnoreCase("4")) {
									precio = cosecha.getPrecioMalbec();
									// nuevoEstado.setImporte(saldo * cosecha.getPrecioMalbec());
								}
								if (objetoiGarantia.getGarantia().getTipoProducto().equalsIgnoreCase("5")) {
									precio = cosecha.getPrecioOtrosVarietales();
									// nuevoEstado.setImporte(saldo * cosecha.getPrecioOtrosVarietales());
								}
							}
							Double importe = saldo * precio;
							if (estadoActual != null
									&& !estadoActual.getEstado().getNombreEstado().equals(Estado.CANCELADO)) {
								nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.PARCIALMENTE_LIBERADA,
										new Date(), importe);
							} else {
								nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.TOTALMENTE_LIBERADA,
										new Date(), importe);
							}
							nuevoEstado.setObjetoiGarantia(objetoiGarantia);
							bp.save(nuevoEstado);
						}

					}

				}
			}
			bp.commit();
			accion = "listar";
			doProcess();
		}
		return true;
	}

	private boolean tieneLiberacionesSolicitada(Objetoi credito) {
		Number n = (Number) bp.createQuery(
				"select count(l.id) from Liberaciones l where l.objetoi.id = :idCredito and l.estado = 'SOLICITADA'")
				.setParameter("idCredito", credito.getId()).uniqueResult();
		return n.longValue() > 0;
	}

	private HashSet<Long> getSeleccionados() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		HashSet<Long> seleccionados = new HashSet<Long>();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("credito")) {
				String[] p = param.split("-");
				seleccionados.add(new Long(p[1]));
			}
		}
		return seleccionados;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<BeanCtaCte> getBeans() {
		return beans;
	}

	public void setBeans(List<BeanCtaCte> beans) {
		this.beans = beans;
	}

	public ArrayList<ObjetoiDTO> getCreditosDTO() {
		return creditosDTO;
	}

	public void setCreditosDTO(ArrayList<ObjetoiDTO> creditosDTO) {
		this.creditosDTO = creditosDTO;
	}

	public ObjetoiDTO getCredito() {
		return credito;
	}

	public void setCredito(ObjetoiDTO credito) {
		this.credito = credito;
	}

	public String getMsg() {
		return msg;
	}

}
