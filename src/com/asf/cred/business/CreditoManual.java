package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import com.nirven.expedientes.persistencia.Numerador;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Domicilio;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.CargaRequisito;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.DomicilioObjetoi;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.PeriodoCosechaConfig;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.creditos.hibernate.Turno;
import com.nirven.creditos.hibernate.Usuario;
import com.sun.org.apache.commons.beanutils.BeanUtils;

public class CreditoManual implements IProcess {

	private String forward = "errorPage";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	private Turno turno;
	private Objetoi credito;
	private Long idSolicitud;
	private Usuario currentUser;
	private List<Objetoi> solicitudes;
	private String hora = "";
	private Long idPersona;
	private Long numeroAtencion;
	private Long numeroSolicitud;
	private String linea;
	private boolean editable = true;
	private String estadoSolicitud;
	private String isCosechaYAcarreo;
	// agregados
	private Persona persona;

	public CreditoManual() {
		this.turno = new Turno();
		this.credito = new Objetoi();
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String idUsuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		this.currentUser = (Usuario) bp.getById(com.nirven.creditos.hibernate.Usuario.class, idUsuario);
		if (persona == null) {
			persona = new Persona();
		}
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public boolean doProcess() {
		this.isCosechaYAcarreo = null;

		// @SuppressWarnings("unused")
		// Linea lineaCosecha = (Linea)bp.getById(Linea.class, idlinea);
		this.editable = true;
		if (this.accion.equals("startProcess")) {
			this.forward = "Solicitud";
			this.cargarSolicitud();
			this.credito.startSolicitud();
			this.accion = "load";
			this.editable = false;
		}
		if (this.accion.equals("list")) {
			this.forward = "SolicitudList";
			this.listarSolicitudes();
		} else if (this.accion.equals("nuevo")) {
			this.forward = "CreditoManual";
			this.cargarPersona();
		} else if (this.accion.equals("load")) {
			this.forward = "CreditoManual";
			this.cargarSolicitud();
		} else if (this.accion.equals("show")) {
			this.editable = false;
			this.forward = "Solicitud";
			this.cargarSolicitud();
			if (credito.getLinea().isLineaCosecha()) {
				this.isCosechaYAcarreo = "mostrar";
			}
		} else if (this.accion.equals("eliminar")) {
			cargarSolicitud();
			if (this.validateEliminar()) {
				this.forward = "SolicitudList";
				this.eliminarSolicitud();
			} else {
				this.forward = "errorPage";
			}
		} else if (this.accion.equals("guardar")) {
			if (credito.getLinea().isLineaCosecha()) {
				this.isCosechaYAcarreo = "mostrar";
			}
			if (this.validate()) {
				this.guardarSolicitud();
				this.forward = "CreditoManual";
				this.cargarSolicitud();
			} else {
				this.forward = "errorPage";
			}
		} else if (this.accion.equals("cancelar")) {
			this.forward = "SolicitudList";
		} else {
			this.forward = "SolicitudList";
		}

		return this.errores.isEmpty();
	}

	private boolean validateEliminar() {

		if (!this.credito.getDesembolsos().isEmpty())
			this.errores.put("Solicitud.error.cronograma", "Solicitud.error.cronograma");

		if (!this.credito.getRequisitos().isEmpty())
			this.errores.put("Solicitud.error.estadopersona", "Solicitud.error.estadopersona");

		return this.errores.isEmpty();
	}

	private void eliminarSolicitud() {
		try {
			this.bp.delete(this.credito);
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("Solicitud.error.general", "Solicitud.error.general");
		}

	}

	private void cargarSolicitud() {
		accion = "show";
		editable = false;
		if (this.idSolicitud != null && this.idSolicitud.longValue() > 0)
			this.credito = (Objetoi) bp.getById(Objetoi.class, this.idSolicitud);
		CreditoHandler creditoHandler = new CreditoHandler();
		Estado estado = creditoHandler.findEstado(credito);
		if (estado != null)
			estadoSolicitud = estado.getNombreEstado();
	}

	private void cargarPersona() {
		persona = (Persona) bp.getById(Persona.class, idPersona);
		credito.setPersona(persona);
		// numero de solicitud
		if (credito.getNumeroAtencion() == null)
			credito.setNumeroAtencion(Numerador.getNext("objetoi.numeroAtencion"));
	}

	@SuppressWarnings("unchecked")
	private void listarSolicitudes() {
		String consulta = "";
		consulta = "SELECT o FROM Objetoi o";
		String filtro = "";

		if (this.idPersona != null && this.idPersona.longValue() > 0)
			filtro = " WHERE o.persona.id = " + this.idPersona.toString();
		if (this.numeroAtencion != null && this.numeroAtencion.longValue() > 0)
			if (filtro.length() > 0)
				filtro += " AND o.numeroAtencion = " + this.numeroAtencion.toString();
			else
				filtro = " WHERE o.numeroAtencion=" + this.numeroAtencion.toString();

		if (this.idSolicitud != null && this.idSolicitud.longValue() > 0)
			filtro = " WHERE  o.id = " + this.idSolicitud.toString();

		if (filtro.length() > 0) {
			this.solicitudes = this.bp.getByFilter(consulta + filtro);
			if (this.solicitudes != null)
				if (this.solicitudes.size() > 0 && this.solicitudes.get(0) != null)
					this.idSolicitud = this.solicitudes.get(0).getId();
		}
	}

	public boolean validate() {
		if (this.accion.equals("guardar")) {
			if (this.credito.getFechaSolicitud() == null)
				this.errores.put("Solicitud.error.fecha", "Solicitud.error.fecha");
			cargarPersona();
			if (this.credito.getPersona() == null)
				this.errores.put("Solicitud.error.persona", "Solicitud.error.persona");

			try {
				String lineasStr;
				String lineasPreaprobadoStr;
				lineasStr = DirectorHandler.getDirector(Linea.LINEA_COSECHA).getValor();
				lineasPreaprobadoStr = DirectorHandler.getDirector(Linea.LINEA_COSECHA_PREAPROBADA).getValor();
				boolean esCyA = false;
				boolean esDestCosecha = false;

				if (credito.getLinea().isLineaCosechaPreAprobada()) {
					if (credito.getDestino().toString().equals("CRDA")) {
						this.errores.put("Solicitud.error.preElab", "Solicitud.error.preElab");
					}
				}
				if (credito.getLinea().isLineaCosecha()) {
					esCyA = true;
					if (credito.getDestino().equals("CRDC")) {
						esDestCosecha = true;
					} else {
						esDestCosecha = false;
					}

				}

				if (esCyA && esDestCosecha) {
					List<CosechaConfig> cosechas = (List<CosechaConfig>) bp.getNamedQuery("CosechaConfig.findByFecha")
							.setParameter("fecha", credito.getFechaSolicitud())
							.setParameter("varietales", credito.getVarietales()).list();
					if (cosechas.size() > 0) {
						CosechaConfig cosechaConfig = cosechas.get(0);
						List<Objetoi> creditos = bp
								.createQuery("select c from Objetoi c where c.persona = :persona "
										+ "and c.fechaSolicitud >= :fecha1 and c.fechaSolicitud <= :fecha2 "
										+ "and c.destino ='CRDC'")
								.setParameter("persona", credito.getPersona())
								.setParameter("fecha1", cosechaConfig.getFechaInicioPer())
								.setParameter("fecha2", cosechaConfig.getFechaFinPer()).list();
						for (Objetoi cred : creditos) {
							bp.getCurrentSession().evict(cred);
							if (cred.getLinea().isLineaCosecha()
									&& (credito.getId() == null || credito.getId().longValue() == 0)
									&& cred.getLinea().getSolExcluyente() == true) {
								this.errores.put("Solicitud.error.fechaSolicitud", "Solicitud.error.fechaSolicitud");
							}

							if (cred.getLinea().getSolExcluyente()) {
								this.errores.put("Solicitud.error.fechaSolicitud", "Solicitud.error.fechaSolicitud");
							}

						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (credito.getLinea() != null && credito.getLinea().getMonto() != 0.0) {
			Double sumFinanciamiento = (Double) bp.createSQLQuery("SELECT SUM(FINANCIAMIENTO) FROM Objetoi O "
					+ "INNER JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id "
					+ "INNER JOIN Estado E ON OE.estado_idEstado = E.idEstado AND E.nombreEstado NOT IN ('ESPERANDO DOCUMENTACION') "
					+ "WHERE linea_id = :linea_id AND OE.fechaHasta IS NULL")
					.setLong("linea_id", credito.getLinea().getId()).uniqueResult();
			if (sumFinanciamiento == null)
				sumFinanciamiento = 0.0;
			if ((sumFinanciamiento + credito.getFinanciamiento()) > credito.getLinea().getMonto()) {
				errores.put("Solicitud.error.financiamiento", "Solicitud.error.financiamiento");
			}
		}
		if (credito.getLinea() != null && credito.getLinea().getMontoObjetoi() != null
				&& credito.getLinea().getMontoObjetoi() != 0.0) {
			if (credito.getFinanciamiento() > credito.getLinea().getMontoObjetoi()) {
				errores.put("Solicitud.error.montoObjetoi", "Solicitud.error.montoObjetoi");
			}
		}

		return this.errores.isEmpty();
	}

	private void guardarSolicitud() {
		boolean esNuevo = false;
		try {
			// persona
			if (credito.getPersona() != null && credito.getPersona().getEstadoActual() != null
					&& !credito.getPersona().getEstadoActual().getEstado().getNombreEstado().equals("NORMAL")) {
				errores.put("creditoManual.estadoPersona", "creditoManual.estadoPersona");
				return;
			}
			// estado del credito
			ObjetoiEstado objetoiEstado = null;
			boolean cumpleRequisitos = false; // Cumple los requisitos
			List<CargaRequisito> req = new ArrayList<CargaRequisito>();
			req = credito.getRequisitos();
			if (req != null) {
				cumpleRequisitos = true;
				for (CargaRequisito cargaRequisito : req) {
					if (cargaRequisito.getFechaCumplimiento() == null) {
						cumpleRequisitos = false;
					}
				}
			}
			if (credito.getId() == null || credito.getId().longValue() == 0) {
				esNuevo = true;
				objetoiEstado = new ObjetoiEstado();
				objetoiEstado.setFecha(new Date());
				if (cumpleRequisitos) {
					try {
						Estado estado = (Estado) bp
								.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = 'ANALISIS'").uniqueResult();
						objetoiEstado.setEstado(estado);
					} catch (NonUniqueResultException nue) {
						objetoiEstado = null;
					} catch (NoResultException nre) {
						objetoiEstado = null;
					}
				} else {
					try {
						Estado estado = (Estado) bp
								.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = 'ESPERANDO DOCUMENTACION'")
								.uniqueResult();
						objetoiEstado.setEstado(estado);
					} catch (NonUniqueResultException nue) {
						objetoiEstado = null;
					} catch (NoResultException nre) {
						objetoiEstado = null;
					}
				}
			}
			ObjetoiComportamiento comportamiento = null;
			// comportamiento del pago
			if (credito.getId() == null || credito.getId().longValue() == 0) {
				comportamiento = new ObjetoiComportamiento();
				comportamiento.setFecha(new Date());
				comportamiento.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
			}
			if (this.credito.getNumeroAtencion() == null || this.credito.getNumeroAtencion() == 0) {
				this.credito.setNumeroAtencion(Numerador.getNext("objetoi.numeroAtencion"));
			}
			this.credito.setDelegacion(this.currentUser.getDelegacion());
			// compruebo si la persona tiene un domicilio real
			List<com.civitas.hibernate.persona.Domicilio> doms = (List<com.civitas.hibernate.persona.Domicilio>) bp
					.createQuery("Select d from Domicilio d where d.persona=:persona and d.tipo=:tipo")
					.setParameter("persona", credito.getPersona()).setParameter("tipo", "Real").list();
			DomicilioObjetoi domO = new DomicilioObjetoi();
			if (doms.size() >= 1) {
				com.civitas.hibernate.persona.Domicilio dom = doms.get(0);
				com.civitas.hibernate.persona.Domicilio nuevoDom = new com.civitas.hibernate.persona.Domicilio();
				nuevoDom = (Domicilio) BeanUtils.cloneBean(dom);
				nuevoDom.setTipo("Especial");
				nuevoDom.setPersona(null);
				nuevoDom.setId(null);
				bp.save(nuevoDom);
				domO.setDomicilio(nuevoDom);
				domO.setObjetoi(credito);
			}
			// Fin creacion Domicilio
			String lineasStr = DirectorHandler.getDirector("linea.cosecha").getValor();
			String[] lineas = lineasStr.split(",");
			boolean esCyA = false;
			for (String linea : lineas) {
				if (credito.getLinea().getId().toString().equals(linea)) {
					esCyA = true;
					break;
				}
			}

			if (credito.getLinea() != null) {
				credito.setMontoGastosCuota(credito.getLinea().getImporte());
				credito.setPorcentajeGastosCuota(credito.getLinea().getPorcentaje());
				credito.setCalcularGastosLocal(false);
			}
			// si el credito es de Cosecha y Acarreo
			if (esCyA) {
				CosechaConfig cosechaConfig = (CosechaConfig) bp.getNamedQuery("CosechaConfig.findByTemporadaMayor")
						.setParameter("varietales", this.credito.getVarietales()).list().get(0);
				this.credito.setFrecuenciaCapital(cosechaConfig.getPeriodicidad());
				this.credito.setFrecuenciaInteres(cosechaConfig.getPeriodicidad());
				this.credito.setPrimerVencCapital(cosechaConfig.getFechaPrimerVto());
				this.credito.setPrimerVencInteres(cosechaConfig.getFechaPrimerVto());
				this.credito.setPlazoCapital(cosechaConfig.getCantCuotas());
				this.credito.setPlazoCompensatorio(cosechaConfig.getCantCuotas());
			} else {
				this.credito.setFrecuenciaCapital(credito.getLinea().getPeriodicidadCapital());
				this.credito.setFrecuenciaInteres(credito.getLinea().getPeriodicidadInteres());
				this.credito.setPlazoCapital(credito.getLinea().getCantCuotasCapital());
				this.credito.setPlazoCompensatorio(credito.getLinea().getCantCuotasInteres());
			}
			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			credito.setAsesor(usuario);
			credito.setTipoAmortizacion(Objetoi.METODO_CALCULO_ALEMAN);
			bp.begin();
			this.bp.save(this.credito);
			this.idSolicitud = this.credito.getId();
			if (domO.getObjetoi() != null) {
				bp.save(domO);
			}
			if (objetoiEstado != null) {
				objetoiEstado.setObjetoi(credito);
				bp.save(objetoiEstado);
			}
			if (comportamiento != null) {
				comportamiento.setObjetoi(credito);
				bp.save(comportamiento);
			}
			bp.commit();
			// Crea ObjetoiIndice para el nuevo credito
			if (esNuevo)
				generarTasa();
			// FIN ObjetoiIndice
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("Solicitud.error.general", "Solicitud.error.general");
		}
	}

	public void generarTasa() throws Exception {
		CreditoHandler ch = new CreditoHandler();
		errores.putAll(ch.generarDatosFinancieros(credito));
	}

	private boolean comprobarGarantia(Garantia garantia, String tipoGar) {
		if (tipoGar.equals("1") || tipoGar.equals("2") || tipoGar.equals("3")) {
			if (garantia.getTipoProducto().equals(tipoGar)) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	private boolean comprobarBonificaciones(Objetoi credito) {
		List<ObjetoiBonificacion> bonificaciones = (List<ObjetoiBonificacion>) bp
				.createQuery("Select b from ObjetoiBonificacion b where b.objetoi=:cred").setParameter("cred", credito)
				.list();
		if (bonificaciones.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private void eliminarBonificaciones(Objetoi credito) {
		List<ObjetoiBonificacion> bonificaciones = (List<ObjetoiBonificacion>) bp
				.createQuery("Select b from ObjetoiBonificacion b where b.objetoi=:cred").setParameter("cred", credito)
				.list();
		for (ObjetoiBonificacion bon : bonificaciones) {
			bp.delete(bon);
		}
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

	public Turno getTurno() {
		if (this.turno == null)
			this.turno = new Turno();
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Object getResult() {
		return null;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Usuario getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(Usuario currentUser) {
		this.currentUser = currentUser;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public List<Objetoi> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(List<Objetoi> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(Long numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}

	public String getIsCosechaYAcarreo() {
		return isCosechaYAcarreo;
	}

	public void setIsCosechaYAcarreo(String isCosechaYAcarreo) {
		this.isCosechaYAcarreo = isCosechaYAcarreo;
	}

}