package com.asf.cred.business;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.CConcepto;
import com.nirven.creditos.hibernate.Indice;

public class IndiceConcepto extends ConversationProcess {

	private Long indiceId;
	private String conceptoId;
	@Save(refresh = true)
	private Indice indice;

	@ProcessMethod(defaultAction = true)
	public boolean buscar() {
		if (indiceId == null) {
			return false;
		}

		indice = (Indice) bp.getById(Indice.class, indiceId);
		conceptoId = indice.getConcepto() != null ? indice.getConcepto().getConcepto() : null;
		return true;
	}

	@ProcessMethod
	public boolean guardar() {
		if (conceptoId == null) {
			return false;
		}

		CConcepto con = (CConcepto) bp.getById(CConcepto.class, conceptoId);
		indice.setConcepto(con);
		bp.update(indice);

		forward = "ProcessRedirect";
		return true;
	}

	public String getForwardURL() {
		return "/actions/abmAction.do?do=list&entityName=Indice";
	}

	@Override
	protected String getDefaultForward() {
		return "IndiceConcepto";
	}

	public Long getIndiceId() {
		return indiceId;
	}

	public void setIndiceId(Long indiceId) {
		this.indiceId = indiceId;
	}

	public String getConceptoId() {
		return conceptoId;
	}

	public void setConceptoId(String conceptoId) {
		this.conceptoId = conceptoId;
	}

	public Indice getIndice() {
		return indice;
	}

	public void setIndice(Indice indice) {
		this.indice = indice;
	}

}
