package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.hibernate.HibernateException;

import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Bolepago;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.Movpagos;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.PagosKey;

public class CobroPagoHandler {
	BusinessPersistance bp;

	
	@SuppressWarnings("unchecked")
	public boolean delete(Long idCaratula, Long nroLote){
		return this.delete(idCaratula, nroLote, null);
	} 
	
	
	@SuppressWarnings("unchecked")
	public boolean delete(Long idCaratula, Long nroLote, Long idBoleto) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		
		List<Boleto> boletos;
		
		if(idBoleto == null){
			boletos =this.getBoletos(idCaratula, nroLote);
		}else{
			Boleto b =(Boleto) bp.getById(Boleto.class, idBoleto);
			boletos = new ArrayList<Boleto>();
			boletos.add(b);
		}
			
		List bolepagos=this.getBolepagos(idCaratula, nroLote, idBoleto);
		
		List<Cobropago> cobropagos=this.getCobropagos(idCaratula, nroLote,idBoleto);
		bp.begin();
		
		try{
		//Actualizo Car�tula
			Caratula caratula=(Caratula)bp.getById(Caratula.class, idCaratula);
			caratula.setComprobantes(caratula.getComprobantes().longValue()-boletos.size());
			caratula.setImporte(caratula.getImporte().doubleValue()-this.getTotal(bolepagos));
			
			bp.update(caratula);
			
		//Eliminar Bolepago
			for (Iterator iter = bolepagos.iterator(); iter.hasNext();) {
				bp.delete(iter.next());				
			}
		//Eliminar Lote (Cobropago)
			Recaudacion recaudacion;
			Cobropago cobropago;
			for (Iterator<Cobropago> iter = cobropagos.iterator(); iter.hasNext();) {
				cobropago = iter.next();
				recaudacion = cobropago.getRecaudacion();
				if(recaudacion != null){
					recaudacion.setSaldo(recaudacion.getSaldo() + cobropago.getImporte()*recaudacion.getCotizacion());
					if(recaudacion.getSaldo().doubleValue() == recaudacion.getImporte().doubleValue()){
						recaudacion.setEstadoComprobante("1");
					}
					bp.update(recaudacion);
				}
				bp.delete(cobropago);				
			}
		//Eliminar Pago, movpago y recabole)
			for (Iterator iter = boletos.iterator(); iter.hasNext();) {
				Boleto boleto = (Boleto) iter.next();
				PagosKey pk=new PagosKey();
				pk.setBoleto(boleto);
				pk.setCaratula(caratula);
				List<Movpagos> movpagos = bp.createQuery("SELECT m FROM Movpagos m WHERE m.pagos.id = :idPagos").setParameter("idPagos", pk).list();
				bp.delete(movpagos);
				bp.delete(Pagos.class, pk);				
			}
			
			bp.commit();
		}catch (HibernateException e) {
			e.printStackTrace();
			bp.rollback();
			return false;
		}			
		return true;
	}
	
	private double getTotal(List bolepagos) {
		double ret=0;
		for (Iterator iter = bolepagos.iterator(); iter.hasNext();) {
			Bolepago bolepago = (Bolepago) iter.next();
			ret+=bolepago.getImporte().doubleValue();
		}
		
		return ret;
	}

	public List getBoletos(Long idCaratula, Long nroLote) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return bp.getByFilter("SELECT DISTINCT b.id.boleto FROM Bolepago b " +
				"WHERE b.id.cobropago.numeroLote="+nroLote+" AND " +
					"b.id.cobropago.caratula.id="+idCaratula);
	}

	public List getBolepagos(Long idCaratula, Long nroLote) {
		return getBolepagos(idCaratula, nroLote,null);
	}
	
	public List getBolepagos(Long idCaratula, Long nroLote, Long idBoleto) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		
		if(nroLote == null){
			return bp.createQuery("SELECT b FROM Bolepago b WHERE b.id.boleto.id = :idBoleto")
		        	.setParameter("idBoleto",idBoleto)
		        	.list();
		}else{
			return bp.getByFilter("FROM Bolepago b WHERE b.id.cobropago.numeroLote="+nroLote+" AND " +
					"b.id.cobropago.caratula.id="+idCaratula);		
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Cobropago> getCobropagos(Long idCaratula, Long nroLote){
		return  getCobropagos(idCaratula,nroLote,null);
	}
	
	@SuppressWarnings("unchecked")
	public List<Cobropago> getCobropagos(Long idCaratula, Long nroLote,Long idBoleto) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		
		if(idBoleto != null){
			return  bp.createQuery("Select b.id.cobropago FROM Bolepago b " +
					"WHERE b.id.boleto.id =:idBoleto")
					.setParameter("idBoleto", idBoleto)
					.list();
		}else{
			return bp.getByFilter("FROM Cobropago c WHERE c.numeroLote="+nroLote+" AND " +
					"c.caratula.id="+idCaratula);
		}
	}
	
	public List getLotes(Long idCaratula){
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return bp.getByFilter("SELECT b.id.cobropago.numeroLote, SUM(b.importe), " +
				"COUNT(DISTINCT b.id.cobropago) as nroMediosPagos, " +
				"COUNT(DISTINCT b.id.boleto.numeroBoleto) as nroBoletos, " +
				"MIN(b.id.cobropago) as cobropago " +
			"FROM Bolepago b " +
			"WHERE b.id.cobropago.caratula.id="+idCaratula+" " +
			"GROUP BY b.id.cobropago.numeroLote");
	}
	
	/*public List getDetallebolepagos(Long idCaratula, String[] lstB){
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return bp.getByFilter("FROM Detallebolepago d WHERE d.id.bolcon.id.numeroBoleto="+ lstB[0] +
					" AND d.id.bolcon.id.verificadorBoleto="+ lstB[1] +
					" AND d.id.bolcon.id.periodoBoleto="+ lstB[2] +
					" AND d.bolepago.id.cobropago.caratula.id="+ idCaratula +
					" ORDER BY d.id.cobropago.id, d.id.bolcon.id.periodoCtacte");
		
	}*/
	
	
	public List getTBoletos(Long idCaratula, Long nroLote) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return bp.getByFilter("SELECT DISTINCT b.id.boleto.numeroBoleto, b.id.boleto.verificadorBoleto, b.id.boleto.periodoBoleto, SUM(b.importe) FROM Bolepago b " +
								"WHERE b.id.cobropago.numeroLote="+nroLote+" AND b.id.cobropago.caratula.id="+idCaratula+
								" GROUP BY b.id.boleto.numeroBoleto, b.id.boleto.verificadorBoleto, b.id.boleto.periodoBoleto" +
								" ORDER BY 1");
	}
	
	public List getBoletosLote(Long idCaratula, Long nroLote) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return bp.getByFilter("SELECT DISTINCT b.id.boleto.id, b.id.boleto.numeroBoleto, b.id.boleto.verificadorBoleto, b.id.boleto.periodoBoleto, SUM(b.importe) FROM Bolepago b " +
								"WHERE b.id.cobropago.numeroLote="+nroLote+" AND b.id.cobropago.caratula.id="+idCaratula+
								" GROUP BY b.id.boleto.id, b.id.boleto.numeroBoleto, b.id.boleto.verificadorBoleto, b.id.boleto.periodoBoleto" +
								" ORDER BY 1");
	}
	
	public List getBoletosRecibo(Long numeroLote) {
		if(this.bp==null)
			bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		
		double totalImporte = 0;
		double totalCapital= 0;
		double totalCompensatorio= 0;
		double totalMoratorio= 0;
		double totalPunitorio= 0;
		double totalBonificacion= 0;
		double totalGastos= 0;
		double totalMultas= 0;
		double totalGasRec= 0;
		
		List<Object[]> arreglo;
		 arreglo = bp.createSQLQuery("SELECT b.id, b.numeroBoleto, b.verificadorBoleto, b.periodoBoleto, b.importe,pe.NOMB_12, o.numeroAtencion, b.fechaEmision, "+
				"SUM(case when co.concepto_concepto = 'cap' then bn.importe else 0 END) capital, "+
				"SUM(case when co.concepto_concepto = 'com' then bn.importe else 0 END) compensatorio, "+
				"SUM(case when co.concepto_concepto = 'mor' then bn.importe else 0 END) moratorio, "+
				"SUM(case when co.concepto_concepto = 'pun' then bn.importe else 0 END) punitorio, "+
				"SUM(case when co.concepto_concepto = 'bon' then bn.importe else 0 END) bonificacion, "+
				"SUM(case when co.concepto_concepto = 'gas' then bn.importe else 0 END) gastos, "+
				"SUM(case when co.concepto_concepto = 'mul' then bn.importe else 0 END) multas, "+
				"SUM(case when co.concepto_concepto = 'rec' then bn.importe else 0 END) gasrec ,pe.cuil_12,o.expediente,l.nombre," +
				"m.simbolo, p.cotizacion "+
				"FROM Pagos p  "+
				"JOIN Boleto b ON p.recibo_id = b.id  "+
				"INNER JOIN Objetoi o on o.id = b.objetoi_id "+
				"INNER JOIN Linea l ON o.linea_id = l.id "+
				"INNER JOIN PERSONA pe on pe.IDPERSONA = o.persona_IDPERSONA "+
				"INNER JOIN Bolcon bn on bn.boleto_id= b.id "+
				"INNER JOIN Concepto co on co.id = bn.facturado_id "+
				"LEFT JOIN Moneda m on p.moneda_IDMONEDA = m.IDMONEDA "+
				"WHERE p.boleto_id IN(SELECT bole.boleto_id FROM bolepago bole JOIN cobropago c ON c.id = bole.cobropago_id WHERE c.numeroLote = :numeroLote) "+
				"GROUP BY bn.boleto_id, b.id, b.numeroBoleto, b.verificadorBoleto, b.periodoBoleto, b.importe,pe.NOMB_12, o.numeroAtencion,b.fechaEmision,pe.cuil_12, o.expediente,l.nombre," +
				"m.simbolo, p.cotizacion")
								.setLong("numeroLote", numeroLote)
								.list();
		
		 for (Object[] fila : arreglo) {
			totalImporte += (Double)fila[4];
			totalCapital+= (Double)fila[8];
			totalCompensatorio+= (Double)fila[9];
			totalMoratorio+= (Double)fila[10];
			totalPunitorio+= (Double)fila[11];
			totalBonificacion+= (Double)fila[12];
			totalGastos+= (Double)fila[13];
			totalMultas+= (Double)fila[14];
			totalGasRec+= (Double)fila[15];
		}
		 
		 Object[] total = new Object[]{null, "TOTAL", null,null,totalImporte,null,null,null,totalCapital,totalCompensatorio,
				 totalMoratorio, totalPunitorio, totalBonificacion, totalGastos, totalMultas, totalGasRec};
		 arreglo.add(total);
		
		return arreglo;
	}

}
