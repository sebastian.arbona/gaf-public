package com.asf.cred.business;

import java.util.List;

import com.asf.cred.Constants;
import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.civitas.hibernate.persona.Empresa;
import com.tonbeller.wcf.pagestack.Page;

public class ParametrosGralReport {

    private BusinessPersistance persistance;

    public ParametrosGralReport() {
        persistance = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
    }

    public String generarParametros(String llamado, String[] param) {

        for (int i = 0; i < param.length; i++) {


            if (param[i].equalsIgnoreCase("Empresa")) {

                if (!llamado.equals("")) {
                    llamado += ";";
                }

                llamado += this.getNombreCliente();

            } else if (param[i].equalsIgnoreCase("REPORTS_PATH")) {

                if (!llamado.equals("")) {
                    llamado += ";";
                }

                llamado += this.getReportePath();

            } else if (param[i].equalsIgnoreCase("EmpresaCuit")) {

                if (!llamado.equals("")) {
                    llamado += ";";
                }

                llamado += this.getCuitCliente();

            } else if (param[i].equalsIgnoreCase("EmpresaDir")) {

                if (!llamado.equals("")) {
                    llamado += ";";
                }

                llamado += this.getDirCliente();
            } else if (param[i].equalsIgnoreCase("EmpresaLocalidad")) {

                if (!llamado.equals("")) {
                    llamado += ";";
                }

                llamado += this.getLocalidadEmpresa();

            } else {

                List parameter = persistance.getByFilter("FROM Director d WHERE upper(d.nombre)='" + param[i].toUpperCase() + "'");

                if (parameter.size() > 0) {
                    Director para = (Director) parameter.get(0);
                    if (!llamado.equals("")) {
                        llamado += ";";
                    }
                    llamado += param[i].toUpperCase() + "=" + para.getValor();
                }

            }
        }

        return llamado;
    }

    public String getNombreCliente() {

        List param = persistance.getByFilter("FROM Empresa e WHERE e.id=" + Constants.CODI_01);

        if (param.size() > 0) {

            Empresa para = (Empresa) param.get(0);

            return "EMPRESA=" + para.getNomb01();
        }

        return "";
    }

    public String getCuitCliente() {

        List param = persistance.getByFilter("FROM Empresa e WHERE e.id=" + Constants.CODI_01);

        if (param.size() > 0) {

            Empresa para = (Empresa) param.get(0);

            return "EMPRESACUIT=" + para.getCuit01();
        }

        return "";
    }

    public String getDirCliente() {

        List param = persistance.getByFilter("FROM Empresa e WHERE e.id=" + Constants.CODI_01);

        if (param.size() > 0) {

            Empresa para = (Empresa) param.get(0);

            return "EMPRESADIR=" + para.getDomi01();
        }

        return "";
    }

    public String getReportePath() {

        return "REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
    }

    public String getLocalidadEmpresa() {

        List param = persistance.getByFilter("FROM Empresa e WHERE e.id=" + Constants.CODI_01);

        if (param.size() > 0) {

            Empresa para = (Empresa) param.get(0);

            return "EMPLOCAL=" + para.getLoca01();
        }

        return "";

    }
}
