package com.asf.cred.business;

import java.util.HashMap;

import com.asf.struts.action.IProcess;

public class LegajoDigital implements IProcess {

	private HashMap<String, String> errores;
	private String forward;
	private String accion;

	public LegajoDigital() {
		this.errores = new HashMap<String, String>();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			forward = "LegajoDigitalModule";
		}
		return true;
	}

	@Override
	public String getForward() {
		return forward;

	}

	@Override
	public String getInput() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

}
