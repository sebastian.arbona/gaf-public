package com.asf.cred.business;

import java.sql.Timestamp;
import java.util.Date;

import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Objetoi;

public class CreditoDatosGenerales extends CreditoProcess {
	@Override
	public boolean doProcess() {
		if (action == null || action.equals("load")) {
			return super.doProcess();
		} else if (action.equals("save")) {
			return guardarObjetoi();
		}
		return true;
	}

	private boolean guardarObjetoi() {
		if (!validarObjetoi()) {
			return false;
		}
		Objetoi existente = (Objetoi) bp.getById(Objetoi.class, credito.getId());
		existente.setObjeto(credito.getObjeto());
		existente.setFechaSolicitud(credito.getFechaSolicitud());
		existente.setFechaExpediente(credito.getFechaExpediente());
		existente.setFechaFirmaContrato(credito.getFechaFirmaContrato());
		existente.setTipoCult(credito.getTipoCult());
		existente.setPrecioMateria(credito.getPrecioMateria());
		existente.setCantMateria(credito.getCantMateria());
		existente.setGastosElaboracion(credito.getGastosElaboracion());
		existente.setTaxiModelo(credito.getTaxiModelo());
		existente.setTaxiMarca(credito.getTaxiMarca());
		existente.setTaxiDominio(credito.getTaxiDominio());
		existente.setEsMiPymeStr(credito.getEsMiPymeStr());
		existente.setTipoEmpresa(credito.getTipoEmpresa());
		existente.setFechaResolucion(DateHelper.getDate(credito.getFechaResolucionStr()));
		existente.setVolumenVtaAnual(credito.getVolumenVtaAnual());
		existente.getPropiedad().setQuintalesEstimados(credito.getPropiedad().getQuintalesEstimados());
		existente.setCantPersonal(credito.getCantPersonal());
		existente.getPropiedad().setTipoCultivo(credito.getPropiedad().getTipoCultivo());
		existente.getPropiedad().setCantHas(credito.getPropiedad().getCantHas());
		existente.setPeriodoVolVtaAnual(credito.getPeriodoVolVtaAnual());
		existente.getPropiedad().setQuintalesIngresados(credito.getPropiedad().getQuintalesIngresados());
		existente.getPropiedad().setNroINV(credito.getPropiedad().getNroINV());
		existente.getPropiedad().setNroIrrigacion(credito.getPropiedad().getNroIrrigacion());
		existente.getPropiedad().setNroEstabRural(credito.getPropiedad().getNroEstabRural());
		existente.getPropiedad().setNroCatastral(credito.getPropiedad().getNroCatastral());
		existente.getPersona().setActividadAfip(credito.getPersona().getActividadAfip());
		existente.getPersona().setSituacionIva(credito.getPersona().getSituacionIva());
		existente.getPersona().setTipoSector(credito.getPersona().getTipoSector());
		existente.getPersona().setIibb(credito.getPersona().getIibb());
		existente.getPersona().setFechaCierreEjercicio(credito.getPersona().getFechaCierreEjercicio());
		existente.getPersona().setCantPersonal(credito.getPersona().getCantPersonal());
		existente.getPersona().setVolumenVta(credito.getPersona().getVolumenVta());
		existente.getPersona().setVolumenVtaPeriodo(credito.getPersona().getVolumenVtaPeriodo());
		existente.getPersona().setFechaInicioActividad(credito.getPersona().getFechaInicioActividad());
		existente.setObservaciones(credito.getObservaciones());
		existente.setFecovitaStr(credito.getFecovitaStr());
		existente.setVarietales(credito.getVarietales());
		existente.setFecovitaTipo(credito.getFecovitaTipo());
		existente.setTipoSector(credito.getTipoSector());
		if (credito.getFechaCosechaStr() != null) {
			existente.setFechaCosecha(DateHelper.getDate(credito.getFechaCosechaStr()));
		}
		bp.saveOrUpdate(existente);
		// queda para mostrar luego de guardar
		credito = existente;
		return true;
	}

	private boolean validarObjetoi() {
		if (credito == null) {
			return true;
		}
		return errores.isEmpty();
	}

	@Override
	public String getForward() {
		return "CreditoDatosGenerales";
	}

	@Override
	public String getInput() {
		return "CreditoDatosGenerales";
	}

	public String getFechaInmovilizacion() {
		Timestamp fecha = (Timestamp) bp
				.createSQLQuery("SELECT MAX(i.fecha) FROM Inmovilizaciones i WHERE i.objetoi_id = :objetoi_id")
				.setLong("objetoi_id", getObjetoi().getId()).uniqueResult();
		if (fecha == null) {
			return null;
		} else {
			return DateHelper.getString(new Date(fecha.getTime()));
		}
	}

	public String getTipoProductoGarantia() {
		String query = "select t.tf_descripcion from ObjetoiGarantia og join Garantia g on og.garantia_id = g.id "
				+ "join Tipificadores t on g.tipoProducto = t.tf_codigo and tf_categoria = 'Garantia.tipoProducto' "
				+ "where og.objetoi_id = :id";
		String tipoProducto = (String) bp.createSQLQuery(query).setParameter("id", getObjetoi().getId())
				.setMaxResults(1).uniqueResult();
		return tipoProducto;
	}

	public String getVolumenInmovilizado() {
		Object volumen = bp
				.createSQLQuery("SELECT i.volumen FROM Inmovilizaciones i WHERE i.objetoi_id = :objetoi_id "
						+ "AND i.fecha = (SELECT MAX(inmo.fecha) FROM Inmovilizaciones inmo "
						+ "WHERE inmo.objetoi_id = :objetoi_id)")
				.setLong("objetoi_id", getObjetoi().getId()).setMaxResults(1).uniqueResult();
		if (volumen == null) {
			return null;
		} else {
			return String.format("%.2f", (Double) volumen);
		}
	}

	public String getFechaUltimaLiberacion() {
		Timestamp fecha = (Timestamp) bp
				.createSQLQuery("SELECT MAX(l.fecha) FROM Liberaciones l WHERE l.objetoi_id = :objetoi_id")
				.setLong("objetoi_id", getObjetoi().getId()).uniqueResult();
		if (fecha == null) {
			return null;
		} else {
			return DateHelper.getString(new Date(fecha.getTime()));
		}
	}

	public String getVolumenLiberado() {
		Object sumVolumen = bp.getNamedQuery("Liberaciones.sumVolumen").setEntity("objetoi", getObjetoi())
				.uniqueResult();
		if (sumVolumen == null) {
			return null;
		} else {
			return String.format("%.2f", (Double) sumVolumen);
		}
	}

}
