package com.asf.cred.business;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Documento;
import com.nirven.creditos.hibernate.Escrito;

public class documentosProcess implements IProcess {
	private Documento documento = new Documento();
	private String action;
	private HashMap errores = new HashMap();

	@Override
	public boolean doProcess() {
		if (action == null || action.equals("load")) {
			return loadDoc();
		} else if (action.equals("print")) {
			return print();
		} else {

		}
		return false;
	}

	private boolean loadDoc() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		documento = (Documento) bp.getById(Documento.class, documento.getId());
		documento.getArchivo();
		return true;
	}

	private boolean print() {

		return true;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return "verDocumento";
	}

	@Override
	public String getInput() {
		return "verDocumento";
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
