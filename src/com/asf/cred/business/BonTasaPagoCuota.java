package com.asf.cred.business;

import java.util.Date;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.CuotaBonTasa;

public class BonTasaPagoCuota extends ConversationProcess {

	private String cuotasSeleccionadas;
	private Integer numeroOrdenPago;
	private String observaciones;
	private Date fechaPago;
	private String forwardURL;

	@Override
	protected String getDefaultForward() {
		return "PagoCuotaBonTasa";
	}

	@ProcessMethod(defaultAction = true)
	public boolean pagar() {
		String[] cs = cuotasSeleccionadas.split(",");
		BonTasa bonTasa = null;
		for (String id : cs) {
			CuotaBonTasa cuota = (CuotaBonTasa) bp.getById(CuotaBonTasa.class, new Long(id));
			if (cuota != null) {
				bonTasa = cuota.getBonTasa();
			}
			if (cuota.getFechaPago() == null) {
				cuota.setTipoEstadoBonCuota("Pagada");
				cuota.setFechaPago(fechaPago);
				cuota.setObservaciones(observaciones);
				cuota.setNroOrdenPago(numeroOrdenPago);
				bp.update(cuota);
			} else {
				error("No se puede realizar la entrega del Pago");
//				error= true;
//				detalleError="No se puede realizar la entrega del Pago";
			}
		}
		if (bonTasa != null) {
			forwardURL = "/actions/process.do?do=process&processName=BonTasaCuotas&process.idBonTasa="
					+ bonTasa.getId();
			forward = "ProcessRedirect";
		}

		return true;
	}

	public String getFechaPago() {
		return DateHelper.getString(fechaPago);
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = DateHelper.getDate(fechaPago);
	}

	public Integer getNumeroOrdenPago() {
		return numeroOrdenPago;
	}

	public void setNumeroOrdenPago(Integer numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getCuotasSeleccionadas() {
		return cuotasSeleccionadas;
	}

	public void setCuotasSeleccionadas(String cuotasSeleccionadas) {
		this.cuotasSeleccionadas = cuotasSeleccionadas;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public void setForwardURL(String forwardURL) {
		this.forwardURL = forwardURL;
	}

}
