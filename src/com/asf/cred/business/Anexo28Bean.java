package com.asf.cred.business;

import java.io.Serializable;

public class Anexo28Bean implements Serializable {

	private static final long serialVersionUID = 8812454647423262437L;
	
	private String cuentaContable;
	private String linea;
	private double saldoInicial;
	private double desembolsado;
	private Anexo28DetalleBean corriente;
	private Anexo28DetalleBean judicial;
	private Anexo28DetalleBean morosos;
	
	public Anexo28Bean() {
	}
	
	public Anexo28Bean(Object[] r) {
		cuentaContable = (String) r[0];
		linea = (String) r[1];
		saldoInicial = (r[2] != null) ? ((Number)r[2]).doubleValue() : 0;
		desembolsado = (r[3] != null) ? ((Number)r[3]).doubleValue() : 0;
		
		corriente = new Anexo28DetalleBean(((r[4] != null) ? ((Number)r[4]).doubleValue() : 0),
				((r[6] != null) ? ((Number)r[6]).doubleValue() : 0),
				((r[7] != null) ? ((Number)r[7]).doubleValue() : 0));
		
		morosos = new Anexo28DetalleBean(((r[9] != null) ? ((Number)r[9]).doubleValue() : 0),
				((r[11] != null) ? ((Number)r[11]).doubleValue() : 0),
				((r[12] != null) ? ((Number)r[12]).doubleValue() : 0));
		
		judicial = new Anexo28DetalleBean(((r[14] != null) ? ((Number)r[14]).doubleValue() : 0),
				((r[16] != null) ? ((Number)r[16]).doubleValue() : 0),
				((r[17] != null) ? ((Number)r[17]).doubleValue() : 0));
		
		
	}
	
	public boolean isNulo() {
		return (saldoInicial + desembolsado +
				corriente.getSaldoInicial() + corriente.getRecupero() + corriente.getAjustes() +
				morosos.getSaldoInicial() + morosos.getRecupero() + morosos.getAjustes() +
				judicial.getSaldoInicial() + judicial.getRecupero() + judicial.getAjustes()) < 0.01;
	}
	
	public static class Anexo28DetalleBean {
		private double saldoInicial;
		private double recupero;
		private double ajustes;
		
		public Anexo28DetalleBean() {
		}
		
		public Anexo28DetalleBean(double saldoInicial, double recupero, double ajustes) {
			this.saldoInicial = saldoInicial;
			this.recupero = recupero;
			this.ajustes = ajustes;
		}

		public String getPorcentajeRecuperoStr() {
			return String.format("%.2f", getPorcentajeRecupero());
		}
		
		public double getPorcentajeRecupero() {
			if (saldoInicial == 0) {
				return 0;
			}
			return recupero / saldoInicial * 100;
		}
		
		public String getSaldoFinalStr() {
			return String.format("%.2f", getSaldoFinal());
		}
		
		public double getSaldoFinal() {
			return saldoInicial - recupero + ajustes;
		}
		
		public String getSaldoInicialStr() {
			return String.format("%.2f", getSaldoInicial());
		}
		
		public double getSaldoInicial() {
			return saldoInicial;
		}

		public void setSaldoInicial(double saldoInicial) {
			this.saldoInicial = saldoInicial;
		}

		public String getRecuperoStr() {
			return String.format("%.2f", getRecupero());
		}
		
		public double getRecupero() {
			return recupero;
		}

		public void setRecupero(double recupero) {
			this.recupero = recupero;
		}

		public String getAjustesStr() {
			return String.format("%.2f", getAjustes());
		}
		
		public double getAjustes() {
			return ajustes;
		}

		public void setAjustes(double ajustes) {
			this.ajustes = ajustes;
		}
	}

	public String getCuentaContable() {
		return cuentaContable;
	}

	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getSaldoInicialStr() {
		return String.format("%.2f", getSaldoInicial());
	}
	
	public double getSaldoInicial() {
		return saldoInicial;
	}

	public void setSaldoInicial(double saldoInicial) {
		this.saldoInicial = saldoInicial;
	}

	public String getDesembolsadoStr() {
		return String.format("%.2f", getDesembolsado());
	}
	
	public double getDesembolsado() {
		return desembolsado;
	}

	public void setDesembolsado(double desembolsado) {
		this.desembolsado = desembolsado;
	}

	public Anexo28DetalleBean getCorriente() {
		return corriente;
	}

	public void setCorriente(Anexo28DetalleBean corriente) {
		this.corriente = corriente;
	}

	public Anexo28DetalleBean getJudicial() {
		return judicial;
	}

	public void setJudicial(Anexo28DetalleBean judicial) {
		this.judicial = judicial;
	}

	public Anexo28DetalleBean getMorosos() {
		return morosos;
	}

	public void setMorosos(Anexo28DetalleBean morosos) {
		this.morosos = morosos;
	}
	
	public String getSaldoFinalStr() {
		return String.format("%.2f", getSaldoFinal());
	}
	
	public double getSaldoFinal() {
		return saldoInicial + desembolsado 
				+ (corriente.getSaldoFinal() - corriente.getSaldoInicial())
				+ (morosos.getSaldoFinal() - morosos.getSaldoInicial())
				+ (judicial.getSaldoFinal() - judicial.getSaldoInicial());
	}
}
