package com.asf.cred.business;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.asf.hibernate.mapping.Tipificadores;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.nirven.creditos.hibernate.CConcepto;
import com.nirven.creditos.hibernate.DeudoresVarios;

public class DeudoresVariosList extends ConversationProcess {

	private Long idPersona;
	private boolean pagados;
	private String numeroAtencion;
	private Long numero;
	private List<DeudoresVarios> resultado;
	private CConcepto concepto = new CConcepto();
	private Tipificadores tipificadores = new Tipificadores();

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean listar() {

		Criteria c = bp.getCurrentSession().createCriteria(DeudoresVarios.class);
		c.add(pagados ? Restrictions.or(Restrictions.isNotNull("pagoAsociado"), Restrictions.isNotNull("caratula"))
				: Restrictions.and(Restrictions.isNull("pagoAsociado"), Restrictions.isNull("caratula")));
		if (idPersona != null && idPersona != 0L) {
			c.add(Restrictions.eq("persona.idpersona", idPersona));
		}
		if (numeroAtencion != null && !numeroAtencion.isEmpty()) {
			c.add(Restrictions.eq("numeroAtencion", numeroAtencion));
		}
		if (numero != null && numero != 0L) {
			c.add(Restrictions.eq("numero", numero));
		}

		if (concepto.getId() != "") {
			c.add(Restrictions.eq("concepto", concepto.getId()));
		}

		if (tipificadores.getCodigo() != "") {
			c.add(Restrictions.eq("tipoConcepto", tipificadores.getCodigo()));
		}

		resultado = c.list();

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "DeudoresVariosList";
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public boolean isPagados() {
		return pagados;
	}

	public void setPagados(boolean pagados) {
		this.pagados = pagados;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(String numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public List<DeudoresVarios> getResultado() {
		return resultado;
	}

	public void setResultado(List<DeudoresVarios> resultado) {
		this.resultado = resultado;
	}

	public CConcepto getConcepto() {
		return concepto;
	}

	public void setConcepto(CConcepto concepto) {
		this.concepto = concepto;
	}

	public Tipificadores getTipificadores() {
		return tipificadores;
	}

	public void setTipificadores(Tipificadores tipificadores) {
		this.tipificadores = tipificadores;
	}
}
