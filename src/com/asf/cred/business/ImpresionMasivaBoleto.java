package com.asf.cred.business;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.struts.action.ActionMessage;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Domicilio;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.nirven.creditos.hibernate.Emideta;
import com.nirven.creditos.hibernate.Emision;
import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.mailer.Mail;

public class ImpresionMasivaBoleto implements IProcess {

	private String accion;
	private HashMap<String, Object> errores;
	private String forward;
	private ReportResult reporte;
	private BusinessPersistance bp;
	private List<Emideta> emidetas;
	private List<Emision> emisiones;
	private Emision emision;
	private Integer ordenarPor;
	private Integer tipoOrden;
	private String seleccionarTodos;
	String seleccionados = "";
	private Long idCn = null;
	Long emidetaId = new Long(0);

	@SuppressWarnings("unchecked")
	public ImpresionMasivaBoleto() {

		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		forward = "ImpresionMasivaBoleto";
		errores = new HashMap<String, Object>();
		emidetas = (List<Emideta>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("impresionMasivaBoleto.emidetas");
		emision = (Emision) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("impresionMasivaBoleto.emision");
		if (emision == null) {
			emision = new Emision();
			emision.setLeyenda(new Leyenda());
		}
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return errores.isEmpty();
		}
		if (accion.equals("listar")) {
			listar();
		}
		if (accion.equals("seleccionar")) {
			seleccionar();
		}
		if (accion.equals("ordenar")) {
			ordenar();
		}
		if (accion.equals("imprimir")) {
			imprimir();
			if (errores.isEmpty()) {
				forward = "ReportesProcess";
			}
			return errores.isEmpty();
		}
		if (accion.equals("enviarMails")) {
			enviarMails();
			if (errores.isEmpty()) {
				errores.put("desembolsoMasivo.mails.exito", "desembolsoMasivo.mails.exito");
			}

		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listar() {
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("impresionMasivaBoleto.emidetas", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("impresionMasivaBoleto.emision", null);
		emidetas = null;
		emisiones = bp
				.createQuery("SELECT DISTINCT em FROM Emideta e JOIN e.emision em "
						+ "WHERE em.fechaEmision IS NOT NULL AND e.actualizada = true ORDER BY em.fechaEmision DESC")
				.list();
	}

	@SuppressWarnings("unchecked")
	public void seleccionar() {
		emision = (Emision) bp.getById(Emision.class, emision.getId());
		emidetas = bp.createQuery("SELECT e FROM Emideta e WHERE e.emision = :emision ORDER BY e.numero")
				.setEntity("emision", emision).list();
		Domicilio domicilio;
		for (Emideta emideta : emidetas) {
			// Provisorio hasta definir por Damian y Victoria
			List<Domicilio> domicilios = (List<Domicilio>) bp.createQuery("SELECT d FROM Domicilio d JOIN d.persona p "
					+ "WHERE p = (SELECT DISTINCT pe FROM Emideta e JOIN e.credito c JOIN c.persona pe "
					+ "WHERE e = :emideta) AND d.tipo = 'Real'").setEntity("emideta", emideta).list();
			if (domicilios.size() > 0) {
				domicilio = domicilios.get(0);
				emideta.setDomicilio(domicilio);
			}
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("impresionMasivaBoleto.emidetas", emidetas);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("impresionMasivaBoleto.emision", emision);
	}

	public void ordenar() {
		switch (ordenarPor) {
		case 0:
			break;
		case 1:
			ordenarPorLocalidad();
			break;
		case 2:
			// domicilio
			break;
		case 3:
			ordenarPorLinea();
			break;
		}
	}

	@SuppressWarnings("unchecked")
	public void ordenarPorLocalidad() {
		if (tipoOrden == 1) {
			Comparator ascendente = new Comparator<Emideta>() {
				@Override
				public int compare(Emideta o1, Emideta o2) {
					if (o1.getDomicilio() == null || o1.getDomicilio().getLocalidad() == null) {
						return 1;
					}
					if (o2.getDomicilio() == null || o2.getDomicilio().getLocalidad() == null) {
						return -1;
					}
					return o1.getDomicilio().getLocalidad().getNombre()
							.compareTo(o2.getDomicilio().getLocalidad().getNombre());
				}
			};
			Collections.sort(emidetas, ascendente);
		} else {
			Comparator descendente = new Comparator<Emideta>() {
				@Override
				public int compare(Emideta o1, Emideta o2) {
					if (o1.getDomicilio() == null || o1.getDomicilio().getLocalidad() == null) {
						return -1;
					}
					if (o2.getDomicilio() == null || o2.getDomicilio().getLocalidad() == null) {
						return 1;
					}
					return -o1.getDomicilio().getLocalidad().getNombre()
							.compareTo(o2.getDomicilio().getLocalidad().getNombre());
				}
			};
			Collections.sort(emidetas, descendente);
		}
	}

	@SuppressWarnings("unchecked")
	public void ordenarPorLinea() {
		if (tipoOrden == 1) {
			Comparator ascendente = new Comparator<Emideta>() {
				@Override
				public int compare(Emideta o1, Emideta o2) {
					if (o1.getCredito() == null || o1.getCredito().getLinea() == null) {
						return 1;
					}
					if (o2.getCredito() == null || o2.getCredito().getLinea() == null) {
						return -1;
					}
					return o1.getCredito().getLinea().getNombre().compareTo(o2.getCredito().getLinea().getNombre());
				}
			};
			Collections.sort(emidetas, ascendente);
		} else {
			Comparator descendente = new Comparator<Emideta>() {
				@Override
				public int compare(Emideta o1, Emideta o2) {
					if (o1.getCredito() == null || o1.getCredito().getLinea() == null) {
						return -1;
					}
					if (o2.getCredito() == null || o2.getCredito().getLinea() == null) {
						return 1;
					}
					return -o1.getCredito().getLinea().getNombre().compareTo(o2.getCredito().getLinea().getNombre());
				}
			};
			Collections.sort(emidetas, descendente);
		}
	}

	@SuppressWarnings("unchecked")
	public void imprimir() {

		if (emidetas.size() > 0) {

			Emideta e = (Emideta) bp.getById(Emideta.class, emidetas.get(0).getId());

			List<Object[]> obs = bp.createSQLQuery("SELECT o.id,c.fechaVencimiento,c.numero as idCuota "
					+ "FROM Emideta e inner join Objetoi o on o.id = e.credito_id "
					+ "inner join Cuota c on (c.numero = e.numero AND c.credito_id = o.id) WHERE e.emision_id = "
					+ emision.getId() + " ORDER BY o.id, c.numero").list();

			String idReporte = UUID.randomUUID().toString();
			BoletoResumenHelper h = BoletoResumenHelper.getInstance(idReporte);
			for (Object[] ob : obs) {
				Long idObjetoi = ((BigDecimal) ob[0]).longValue();
				Date fecha = (Date) ob[1];
				Integer numeroCuota = (Integer) ob[2];
				h.calcularIntereses(idObjetoi, fecha, numeroCuota);
			}
			e.getCredito().getLinea().getMoneda().getAbreviatura().equalsIgnoreCase("Dolar");
			if (e.getCredito().getLinea().getMoneda().getDenominacion().equalsIgnoreCase("Dolar")) {
				imprimirBoletoResumenDolar(idReporte);
			} else {
				imprimirBoletoResumen(idReporte);
			}
		}
	}

	public void imprimirBoletoResumen(String idReporte) {
		try {
			String leyenda = null;
			if (emision.getLeyenda() != null && emision.getLeyenda().getId() != null
					&& emision.getLeyenda().getId() != 0) {
				Leyenda l = (Leyenda) bp.getById(Leyenda.class, emision.getLeyenda().getId());
				leyenda = l.getTexto();
			}
			if (leyenda == null) {
				leyenda = "";
			}
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletoResumenMasivo.jasper");
			reporte.setParams("EMISION_ID=" + emision.getId() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId
					+ ";LEYENDA=" + leyenda + ";IDREPORTE=" + idReporte + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletoResumenMasivo"));
		}

	}

	public void imprimirBoletoResumenDolar(String idReporte) {
		try {
			String leyenda = null;
			if (emision.getLeyenda() != null && emision.getLeyenda().getId() != null
					&& emision.getLeyenda().getId() != 0) {
				Leyenda l = (Leyenda) bp.getById(Leyenda.class, emision.getLeyenda().getId());
				leyenda = l.getTexto();
			}
			if (leyenda == null) {
				leyenda = "";
			}
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletoResumenMasivoDolares.jasper");
			reporte.setParams("EMISION_ID=" + emision.getId() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId
					+ ";LEYENDA=" + leyenda + ";IDREPORTE=" + idReporte + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "boletoResumenMasivoDolares"));
		}

	}

	@SuppressWarnings("unchecked")
	public void enviarMails() {
		ProgressStatus.onProcess = true;
		ProgressStatus.iTotal = 1;
		ProgressStatus.iProgress = 0;
		ProgressStatus.abort = false;

		String[] seleccionadosList = seleccionados.split(",");
		Emideta e = (Emideta) bp.getById(Emideta.class, emidetas.get(0).getId());

		for (int i = 0; seleccionadosList.length > i; i++) {
			if (!seleccionadosList[i].equals("")) {
				emidetaId = new Long(seleccionadosList[i].trim());
			}
			List<Object[]> obs = bp.createSQLQuery("SELECT o.id,c.fechaVencimiento,c.numero as idCuota "
					+ "FROM Emideta e inner join Objetoi o on o.id = e.credito_id "
					+ "inner join Cuota c on (c.numero = e.numero AND c.credito_id = o.id) WHERE e.id = " + emidetaId
					+ " ORDER BY o.id, c.numero").list();
			String idReporte = UUID.randomUUID().toString();
			BoletoResumenHelper h = BoletoResumenHelper.getInstance(idReporte);
			// Verifica que todos los desembolsos seleccionados esten asociados con una
			// persona que tenga una direccion de mail
			for (Object[] ob : obs) {
				Long idObjetoi = ((BigDecimal) ob[0]).longValue();
				Objetoi o = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
			}
			for (Object[] ob : obs) {
				Long idObjetoi = ((BigDecimal) ob[0]).longValue();
				Date fecha = (Date) ob[1];
				Integer numeroCuota = (Integer) ob[2];
				h.calcularIntereses(idObjetoi, fecha, numeroCuota);
				Objetoi o = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
				try {
					byte[] adj = getReporte(e);
					ConfiguracionNotificacion cn = (ConfiguracionNotificacion) SessionHandler.getCurrentSessionHandler()
							.getBusinessPersistance().getById(ConfiguracionNotificacion.class, idCn);

					// Mail.sendMailPdf(o.getPersona().getEmailPrincipal(),
					// cn.getAsunto(),cn.getTexto(), "boleto", adj);
					List<String> recipients = new ArrayList<>();
					recipients.add(o.getPersona().getEmailPrincipal());
					recipients.add(o.getPersona().getEmailNotificacion());
					Mail.sendMailPdfTemplate(recipients, cn.getAsunto(), "mails/templateMail.html", cn.getTexto(),
							"boleto", adj);

				} catch (Exception ex) {

					ex.printStackTrace();
				}

			}

		}

	}

	@SuppressWarnings("deprecation")
	private byte[] getReporte(Emideta e) throws Exception {
		String nombreReporte = "";
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		String idReporte = UUID.randomUUID().toString();
		String leyenda = null;
		if (emision.getLeyenda() != null && emision.getLeyenda().getId() != null && emision.getLeyenda().getId() != 0) {
			Leyenda l = (Leyenda) bp.getById(Leyenda.class, emision.getLeyenda().getId());
			leyenda = l.getTexto();
		}
		if (leyenda == null) {
			leyenda = "";
		}
		if (e.getCredito().getLinea().getMoneda().getDenominacion().equalsIgnoreCase("Dolar")) {
			nombreReporte = "boletoResumenMasivoDolaresMail.jasper";
		} else {
			nombreReporte = "boletoResumenMasivoMail.jasper";
		}
		JasperReport reporte = (JasperReport) JRLoader.loadObjectFromFile(
				SessionHandler.getCurrentSessionHandler().getRealPath() + "reports/" + nombreReporte);

		Map<String, Object> ret = new HashMap<String, Object>();
		ret.put("SCHEMA", BusinessPersistance.getSchema());
		ret.put("REPORTS_PATH", SessionHandler.getCurrentSessionHandler().getRealPath());
		ret.put("USUARIO", SessionHandler.getCurrentSessionHandler().getCurrentUser());
		ret.put("EMISION_ID", emidetaId.toString());
		ret.put("RELACION_COTOMADOR_ID", relacionCotomadorId);
		ret.put("LEYENDA", leyenda);
		ret.put("IDREPORTE", idReporte);

		Connection conn = SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getCurrentSession()
				.connection();

		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, ret, conn);
		byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
		return pdf;
	}

	public void setLeyenda(Long leyenda_id) {
		if (leyenda_id == 0)
			return;
		else {
			emision.setLeyenda((Leyenda) bp.getById(Leyenda.class, leyenda_id));
			bp.update(emision);
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		if (accion == null) {
			return errores.isEmpty();
		}
		if (accion.equals("enviarMails")) {
			// Se valida que se haya seleccionado una configuracion de notificación
			if (idCn.equals(new Long(0))) {
				errores.put("desembolsoMasivo.mails.error.notificacion", "desembolsoMasivo.mails.error.notificacion");
			}
		}
		return errores.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<Emideta> getEmidetas() {
		return emidetas;
	}

	public void setEmidetas(List<Emideta> emidetas) {
		this.emidetas = emidetas;
	}

	public List<Emision> getEmisiones() {
		return emisiones;
	}

	public void setEmisiones(List<Emision> emisiones) {
		this.emisiones = emisiones;
	}

	public Emision getEmision() {
		return emision;
	}

	public void setEmision(Emision emision) {
		this.emision = emision;
	}

	public Integer getOrdenarPor() {
		return ordenarPor;
	}

	public void setOrdenarPor(Integer ordenarPor) {
		this.ordenarPor = ordenarPor;
	}

	public Integer getTipoOrden() {
		return tipoOrden;
	}

	public void setTipoOrden(Integer tipoOrden) {
		this.tipoOrden = tipoOrden;
	}

	public String getSeleccionarTodos() {
		return seleccionarTodos;
	}

	public void setSeleccionarTodos(String seleccionarTodos) {
		this.seleccionarTodos = seleccionarTodos;
	}

	public String getSeleccionados() {
		return seleccionados;
	}

	public void setSeleccionados(String seleccionados) {
		this.seleccionados = seleccionados;
	}

	public Long getIdCn() {
		return idCn;
	}

	public void setIdCn(Long idCn) {
		this.idCn = idCn;
	}

}
