package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.EstadoInmovilizacion;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;

public class InmovilizacionPendiente implements IProcess {

	protected ObjetoiVinedo vinedo = new ObjetoiVinedo();
	protected ArrayList<ObjetoiVinedo> vinedos;
	protected Objetoi credito;
	protected List<Objetoi> creditos;
	private String accion; 
	private String forward; 
	private Long id;
	private Garantia garantia = new Garantia();
	private boolean detalle= false;
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		
		creditos = bp.createQuery("select distinct i.objetoi from Inmovilizaciones i where i.estado = :solicitada")
				.setParameter("solicitada", EstadoInmovilizacion.SOLICITADA)
				.list();
		/*creditos = bp.createQuery("select distinct c1 " +
									"from ObjetoiVinedo v, Desembolso d " +
									"join v.credito c1 " +
									"join d.credito c2 " +
									"where c1 = c2 " +
									"and v.fechaSolicitudInforme2 is not NULL " +
									"and v.fechaAceptadoInforme2 is NULL " +
									"and d.numero = 1 " +
									"and d.estado = '2'")
									.list();*/
		
		Iterator<Objetoi> creditosIterator = creditos.iterator();
		Objetoi objetoi;
		while(creditosIterator.hasNext()){
			objetoi = creditosIterator.next();
			if(!objetoi.getTieneGarantiaFiduciaria()){
				creditosIterator.remove();
			}
		}
		if (accion == null) {
			detalle=false;
			forward = "InmovilizacionPendienteList";
		} else if (accion.equalsIgnoreCase("ver")) {
			credito = (Objetoi) bp.getById(Objetoi.class, id); 
			setGarantia(buscarGarantia());
			detalle= true;
			forward = "InmovilizacionPendienteList";
		}
		return true;
	}
	
	@Override
	public HashMap<String, Object> getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}
	
	@SuppressWarnings("unchecked")
	public Garantia buscarGarantia() {
		List<Garantia> garantias = new ArrayList<Garantia>();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		garantias =bp
				.createQuery(
						"SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja is NULL")
				.setEntity("credito", credito).list();
		if (garantias.isEmpty()) {
			garantia = new Garantia();
		} else {
			garantia= garantias.get(0);
		}
		return garantia;
	}
	public Garantia setGarantia(Garantia garantia) {
		this.garantia = garantia;
		return garantia;
	}

	
	
	public ObjetoiVinedo getVinedo() {
		return vinedo;
	}
	public void setVinedo(ObjetoiVinedo vinedo) {
		this.vinedo = vinedo;
	}
	public ArrayList<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}
	public void setVinedos(ArrayList<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}
	public boolean isDetalle() {
		return detalle;
	}
	public void setDetalle(boolean detalle) {
		this.detalle = detalle;
	}
	@Override
	public String getInput() {
		return forward;
	}

	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}
	public Garantia getGarantia() {
		return garantia;
	}
	public Objetoi getCredito() {
		return credito;
	}
	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}
	public List<Objetoi> getCreditos() {
		return creditos;
	}
	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

}
