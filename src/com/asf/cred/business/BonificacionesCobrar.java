package com.asf.cred.business;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.hibernate.transform.Transformers;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.nirven.creditos.hibernate.ConvenioBonificacion;

public class BonificacionesCobrar extends ConversationProcess {
	private static String sql;
	private List<BonificacionesCobrarBean> beans;
	private ConvenioBonificacion convenioBonificacion = new ConvenioBonificacion();

	private Date fechaVencimientoDesde;
	private Date fechaVencimientoHasta;

	public String getFechaVencimientoDesde() {
		return DateHelper.getString(fechaVencimientoDesde);
	}

	public void setFechaVencimientoDesde(String fechaVencimientoDesde) {
		this.fechaVencimientoDesde = DateHelper.getDate(fechaVencimientoDesde);
	}

	public String getFechaVencimientoHasta() {
		return DateHelper.getString(fechaVencimientoHasta);
	}

	public void setFechaVencimientoHasta(String fechaVencimientoHasta) {
		this.fechaVencimientoHasta = DateHelper.getDate(fechaVencimientoHasta);
	}

	@Override
	protected String getDefaultForward() {
		return "BonificacionesCobrar";

	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		Boolean parametrosOK = Boolean.TRUE;
		if (fechaVencimientoDesde == null) {
			errors.put("bonificacionescobrar.fechaVencimientoDesde", "bonificacionescobrar.fechaVencimientoDesde");
			parametrosOK = Boolean.FALSE;
		}
		if (fechaVencimientoHasta == null) {
			errors.put("bonificacionescobrar.fechaVencimientoHasta", "bonificacionescobrar.fechaVencimientoHasta");
			parametrosOK = Boolean.FALSE;
		}
		String estados = DirectorHelper.getString("Anexo14Estados");
		if (estados == null || estados.isEmpty()) {
			errors.put("anexo14.estados", "anexo14.estados");
			parametrosOK = Boolean.FALSE;
		}
		if (!parametrosOK) {
			return parametrosOK;
		}

		// reescribir la consulta con innerjoin en lugar de select from todas las tablas
		// para la determinacion de cuotas vencidas ver la consulta en
		// objetoi.getClasificacionMora
		// extraer la consulta a otro metodo y reutilizar
		String sql = getSql();
		sql = sql.replace(":fechaVencimientoDesde", new Timestamp(fechaVencimientoDesde.getTime()).toString());
		sql = sql.replace(":fechaVencimientoHasta", new Timestamp(fechaVencimientoHasta.getTime()).toString());
		sql = sql.replace(":estados", estados);
		String filtroConvenio = "";
		if (convenioBonificacion.getId() != null) {
			filtroConvenio = "and cb.id = " + convenioBonificacion.getId();
		}
		sql = sql.replace(":filtroConvenio", filtroConvenio);
		// si es necesario agregar addScalar para definir los nombres de campos
		List<BonificacionesCobrarBean> beans2;
		beans2 = bp.createSQLQuery(sql).setResultTransformer(Transformers.aliasToBean(BonificacionesCobrarBean.class))
				.list();
		for (BonificacionesCobrarBean bonificacionesCobrarBean : beans2) {
			if (bonificacionesCobrarBean.getFechaVencimiento() == fechaVencimientoDesde) {
				String valorEstado = bonificacionesCobrarBean.getEstado().toString();
				if (Integer.parseInt(valorEstado) == 6 || Integer.parseInt(valorEstado) == 7
						|| Integer.parseInt(valorEstado) == 8 || Integer.parseInt(valorEstado) == 9
						|| Integer.parseInt(valorEstado) == 10 || Integer.parseInt(valorEstado) == 11) {
					beans2.add(bonificacionesCobrarBean);
				}
			}
		}

		beans = beans2;

		return true;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(BonificacionesCobrar.class.getResourceAsStream("bonificaciones-cobrar.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	public List<BonificacionesCobrarBean> getBeans() {
		return beans;
	}

	public ConvenioBonificacion getConvenioBonificacion() {
		return convenioBonificacion;
	}

	public void setConvenioBonificacion(ConvenioBonificacion convenioBonificacion) {
		this.convenioBonificacion = convenioBonificacion;
	}

}
