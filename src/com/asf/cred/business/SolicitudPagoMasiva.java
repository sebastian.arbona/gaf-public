package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.asf.security.BusinessPersistance;
import com.asf.security.light.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.CuotaBonTasa;

public class SolicitudPagoMasiva implements IProcess {

	private String forward;
	private String accion;
	private BusinessPersistance bp;
	private Date fecha;
	private List<BonTasa> bonificaciones;
	private List<Object[]> bonificaciones_importeCuota;
	private Long idEnte;

	public SolicitudPagoMasiva() {
		forward = "SolicitudPagoMasiva";
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {

		} else if (accion.equals("listar")) {
			listar();
		} else if (accion.equals("solicitarImputacion")) {
			bp.begin();
			solicitarImputacion();
			bp.commit();
			listar();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private void listar() {
		if (idEnte == null || idEnte.longValue() == 0) {
			bonificaciones_importeCuota = bp.getNamedQuery("BonTasa.solicitudPagoMasiva").setDate("fecha", fecha)
					.list();
		} else {
			bonificaciones_importeCuota = bp
					.createSQLQuery(
							"SELECT b.id, SUM(c.capital + c.compensatorio), SUM(c.subsidio) " + "FROM CuotaBonTasa c "
									+ "JOIN BonTasa b ON b.id = c.bonTasa_id " + "WHERE c.fechaVencimiento <= :fecha "
									+ "AND c.fechaSolPago IS NULL " + "AND c.fechaPago IS NULL "
									+ "AND c.tipoEstadoBonCuota NOT IN('Imputación Solicitada','Pagada') "
									+ "AND b.banco_CODI_BA = :banco_CODI_BA " + "GROUP BY b.id")
					.setDate("fecha", fecha).setLong("banco_CODI_BA", idEnte).list();
		}
		bonificaciones = new ArrayList<BonTasa>();
		BonTasa bonTasa;
		BigDecimal idBonTasa;
		for (Object[] bonificacion_importe : bonificaciones_importeCuota) {
			if (bonificacion_importe[0] instanceof Long)
				bonTasa = (BonTasa) bp.getById(BonTasa.class, (Long) bonificacion_importe[0]);
			else
				bonTasa = (BonTasa) bp.getById(BonTasa.class, ((BigDecimal) bonificacion_importe[0]).longValue());
			bonTasa.setImporteSolicitudPagoMasiva((Double) bonificacion_importe[1]);
			bonTasa.setSubsidioSolicitudPagoMasiva((Double) bonificacion_importe[2]);
			bonificaciones.add(bonTasa);
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("SolicitudPagoMasiva.fecha",
				fecha);
	}

	@SuppressWarnings("unchecked")
	private void solicitarImputacion() {
		fecha = (Date) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("SolicitudPagoMasiva.fecha");
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		List<Long> seleccionados = new ArrayList<Long>();
		String param;
		String[] p;
		while (paramNames.hasMoreElements()) {
			param = paramNames.nextElement();
			if (param.startsWith("bonTasa")) {
				p = param.split("-");
				seleccionados.add(new Long(p[1]));
			}
		}
		List<CuotaBonTasa> cuotas;
		for (Long id : seleccionados) {
			cuotas = bp.getNamedQuery("CuotaBonTasa.solicitudImputacionMasiva").setLong("bonTasaId", id)
					.setDate("fecha", fecha).list();
			for (CuotaBonTasa cuota : cuotas) {
				cuota.setTipoEstadoBonCuota("Imputación Solicitada");
				cuota.setFechaSolPago(new Date());
				bp.update(cuota);
			}
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return null;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<BonTasa> getBonificaciones() {
		return bonificaciones;
	}

	public void setBonificaciones(List<BonTasa> bonificaciones) {
		this.bonificaciones = bonificaciones;
	}

	public String getFechaStr() {
		return DateHelper.getString(fecha);
	}

	public void setFechaStr(String fechaStr) {
		fecha = DateHelper.getDate(fechaStr);
	}

	public Long getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(Long idEnte) {
		this.idEnte = idEnte;
	}

}
