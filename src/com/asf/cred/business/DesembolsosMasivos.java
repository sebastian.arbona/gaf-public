package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.expedientes.el.impl.StringLiteral;

/**
 * @author eselman
 */

public class DesembolsosMasivos implements IProcess {
    // =========================ATRIBUTOS===========================
    // atributos utilitarios del proceso.-
    private String forward = "DesembolsosMasivos";
    private HashMap<String, Object> errores;
    private String accion = "";
    private BusinessPersistance bp;
    // atributos del proceso.-
    private Persona persona;
    private List<ObjetoiDTO> creditos;
    private ReportResult reportResult;
    private Long idObjetoi;
    private Long idPersona;
    private List<StringLiteral> mensajes;
    private List<StringLiteral> mensajesGarantia;

    // =======================CONSTRUCTORES=========================
    public DesembolsosMasivos() {
        errores = new HashMap<String, Object>();
        bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        mensajes = new ArrayList<StringLiteral>();
        mensajesGarantia = new ArrayList<StringLiteral>();
    }

    // ======================FUNCIONALIDADES========================
    public boolean doProcess() {
        if (accion != null && accion.equals("generar")) {
            generar();
        }
        listarCreditos();
        return errores.isEmpty();
    }

    // =====================UTILIDADES PRIVADAS=====================
    @SuppressWarnings("unchecked")
    private void listarCreditos() {
        try {
            List<Objetoi> objetos = bp
                    .createQuery("SELECT credito FROM ObjetoiEstado oe JOIN oe.objetoi credito JOIN oe.estado estado WHERE estado.nombreEstado = 'ANALISIS' AND oe.fechaHasta IS NULL").list();
            creditos = new ArrayList<ObjetoiDTO>();
            CreditoHandler handler = new CreditoHandler();
            Estado estado;
            ObjetoiDTO credito;
            for (Objetoi o : objetos) {
                estado = handler.findEstado(o);
                if (estado != null) {
                    // TODO: falta calcular deuda
                    credito = new ObjetoiDTO(o, estado.getNombreEstado(), 0);
                    creditos.add(credito);
                } else if (estado == null) {
                    // TODO: falta calcular deuda
                    credito = new ObjetoiDTO(o, "Sin estado", 0);
                    creditos.add(credito);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditos", creditos);

    }

    @SuppressWarnings("unchecked")
    public void generar() {
        HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
        Enumeration<String> paramNames = request.getParameterNames();
        HashSet<Long> seleccionados = new HashSet<Long>();
        while (paramNames.hasMoreElements()) {
            String param = paramNames.nextElement();
            if (param.startsWith("requisito")) {
                String[] p = param.split("-");
                seleccionados.add(new Long(p[1]));
            }
        }
        creditos = (List<ObjetoiDTO>) (SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("creditos"));
        for (ObjetoiDTO dto : creditos) {
            if (seleccionados.contains(dto.getIdObjetoi())) {
                Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
                List<Garantia> garantias = bp
                        .createQuery("SELECT g.garantia FROM ObjetoiGarantia g WHERE g.garantia.credito = :credito "
                                + "AND g.garantia.valor = (SELECT MAX(ga.garantia.valor) FROM ObjetoiGarantia ga WHERE ga.garantia.credito = :credito) AND g.baja IS NULL")
                        .setParameter("credito", objetoi).list();
                if (garantias.isEmpty()) {
                    mensajesGarantia.add(StringLiteral.fromLiteralValue("No se puede generar desembolso para el cr�dito " + objetoi.getNumeroAtencionStr() + ". Falta configurar la garant�a."));
                } else {
                    Garantia garantia = garantias.get(0);
                    TipoGarantia tipoGarantia = garantia.getTipo();
                    if (!objetoi.validarRegistroGarantiaDesembolso()) {
                        mensajes.add(StringLiteral.fromLiteralValue(
                                "No se puede generar desembolso para el cr�dito " + objetoi.getNumeroAtencionStr() + ". La garantia requiere fecha de inscripci�n en registro para desembolsar."));
                        continue;
                    }
                    if (tipoGarantia.getPorcDesembolso1() == 0.0 || tipoGarantia.getPorcDesembolso2() == 0.0) {
                        mensajes.add(StringLiteral.fromLiteralValue("No se puede generar desembolso para el cr�dito " + objetoi.getNumeroAtencionStr()
                                + ". Configure los porcentajes de desembolso del Tipo Garant�a: " + tipoGarantia.getNombre() + "."));
                    } else {
                        Date fecha = new Date();
                        Desembolso desembolso1 = new Desembolso();
                        desembolso1.setImporte(objetoi.getFinanciamiento() * tipoGarantia.getPorcDesembolso1());
                        desembolso1.setCredito(objetoi);
                        desembolso1.setEstado("1");// No Realizado
                        desembolso1.setNumero(1);
                        desembolso1.setFecha(fecha);
                        Desembolso desembolso2 = new Desembolso();
                        desembolso2.setImporte(objetoi.getFinanciamiento() * tipoGarantia.getPorcDesembolso2());
                        desembolso2.setCredito(objetoi);
                        desembolso2.setEstado("1");// No Realizado
                        desembolso2.setNumero(2);
                        desembolso2.setFecha(fecha);
                        ObjetoiEstado anterior = objetoi.getEstadoActual();
                        anterior.setFechaHasta(new Date());
                        Estado estado = (Estado) bp.getNamedQuery("Estado.findByNombreEstado").setString("nombreEstado", "ESPERANDO EJECUCION").uniqueResult();
                        ObjetoiEstado nuevo = new ObjetoiEstado();
                        nuevo.setObjetoi(objetoi);
                        nuevo.setEstado(estado);
                        nuevo.setFecha(new Date());
                        bp.begin();
                        bp.update(anterior);
                        bp.save(nuevo);
                        bp.save(desembolso1);
                        bp.save(desembolso2);
                        bp.commit();
                    }
                }
            }
        }
    }

    // ======================GETTERS Y SETTERS======================
    public HashMap<String, Object> getErrors() {
        return this.errores;
    }

    public String getForward() {
        return this.forward;
    }

    public String getInput() {
        return this.forward;
    }

    public Object getResult() {
        return reportResult;
    }

    public Persona getPersona() {
        return this.persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getAccion() {
        return this.accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public List<ObjetoiDTO> getCreditos() {
        return creditos;
    }

    public void setCreditos(List<ObjetoiDTO> creditos) {
        this.creditos = creditos;
    }

    public Long getIdObjetoi() {
        return idObjetoi;
    }

    public void setIdObjetoi(Long idObjetoi) {
        this.idObjetoi = idObjetoi;
    }

    public Long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    // ========================VALIDACIONES=========================
    public boolean validate() {
        return this.errores.isEmpty();
    }

    public List<StringLiteral> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<StringLiteral> mensajes) {
        this.mensajes = mensajes;
    }

    public List<StringLiteral> getMensajesGarantia() {
        return mensajesGarantia;
    }

    public void setMensajesGarantia(List<StringLiteral> mensajesGarantia) {
        this.mensajesGarantia = mensajesGarantia;
    }
}