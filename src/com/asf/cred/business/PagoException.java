package com.asf.cred.business;

public class PagoException extends Exception {

	private static final long serialVersionUID = 1896282080460909090L;

	public PagoException(String message) {
		super(message);
	}
	
}
