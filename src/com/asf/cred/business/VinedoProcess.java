package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Transient;

import org.apache.struts.action.ActionMessage;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Localidad;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.DetalleVinedoINV;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.PeriodoCosechaConfig;
import com.nirven.creditos.hibernate.Vinedo;

public class VinedoProcess implements IProcess {

	private String codigo;
	private Double qqestimados;
	private Double qqaprobados;
	private String observaciones;
	private String departamento;
	private Long idLocalidad;
	private String forward;
	private ReportResult result;
	private String accion;
	private boolean civ;
	private boolean existe;
	private Long idSolicitud;
	private Double hectareas;
	private boolean noExiste;
	private boolean noValido;
	private List<ObjetoiVinedo> vinedos;
	private Long idVinedo;
	private Date fechaInformeINV;
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	@SuppressWarnings("unchecked")
	private HashMap<String, Object> chErrores;
	Objetoi actual = null;
	Vinedo vinedoActual = null;
	ObjetoiVinedo objetoiVinedoActual = null;
	private boolean elaboracion = false;
	private String titular;
	private boolean elaboracionTabla = false;
	private Objetoi cred;
	private boolean requisitosCumplidos = false;
	private ObjetoiVinedo objetoiVinedo = new ObjetoiVinedo();

	@Override
	public boolean doProcess() {
		chErrores = new HashMap<String, Object>();

		if (idSolicitud != null) {
			cred = (Objetoi) bp.getById(Objetoi.class, idSolicitud);
			if (cred.getEstadoActual() != null && cred.getEstadoActual().getEstado() != null
					&& cred.getEstadoActual().getEstado().getNombreEstado().equalsIgnoreCase("ANALISIS")) {
				requisitosCumplidos = true;
			}
			if (cred.getDestino() != null && cred.getDestino().equalsIgnoreCase("CRDA")) {
				elaboracionTabla = true;
			}
		}
		if (accion == null || accion.equals("")) {
			buscarVinedos();
			civ = true;
			forward = "CrearVinedo";
		} else if (accion.equalsIgnoreCase("ver")) {
			if (esElaboracion()) {
				elaboracion = true;
				if (valido()) {
					Vinedo v = comprobarExistenciaElaboracion();
					if (v == null) {
						chErrores.put("vinedo.elaboracion.no.existe",
								new ActionMessage("vinedo.elaboracion.no.existe"));
						buscarVinedos();
						civ = true;
						forward = "CrearVinedo";
					} else {
						setCodigo(v.getCodigo());
						setHectareas(v.getHectareas());
						setQqestimados(objetoiVinedoActual.getQqEstimados());
						setQqaprobados(objetoiVinedoActual.getQqAprobado());
						setFechaInformeINV(objetoiVinedoActual.getFechaInformeINV());
						forward = "CrearVinedo";
					}
				} else {
					buscarVinedos();
					civ = true;
					noValido = true;
					forward = "CrearVinedo";
				}
			} else {
				if (valido()) {
					existe = comprobarExistencia();
					if (existe == false) {
						noExiste = true;
					}
				} else {
					buscarVinedos();
					civ = true;
					noValido = true;
					forward = "CrearVinedo";
				}
				forward = "CrearVinedo";
			}
		} else if (accion.equalsIgnoreCase("guardar")) {
			if (validarCosecha()) {
				chErrores.put("vinedo.cosecha.existe.periodo", new ActionMessage("vinedo.cosecha.existe.periodo"));
				buscarVinedos();
				civ = true;
				forward = "CrearVinedo";
			} else {
				guardar();
				buscarVinedos();
				civ = true;
				forward = "CrearVinedo";
			}
		} else if (accion.equalsIgnoreCase("eliminar")) {
			eliminarObjetoiVinedo();
			accion = "";
			doProcess();
			forward = "CrearVinedo";
		} else if (accion.equalsIgnoreCase("calcularDatosFinancieros")) {
			calcularDatosFinancieros();
			buscarVinedos();
			civ = true;
			forward = "CrearVinedo";
		} else if (accion.equals("modificar")) {
			objetoiVinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, objetoiVinedo.getId());
			forward = "ModificarObjetoiVinedo";
		} else if (accion.equals("guardarVinedo")) {
			if (validarVinedo()) {
				guardarVinedo();
				forward = "CrearVinedo";
				accion = null;
				doProcess();
			} else {
				forward = "ModificarObjetoiVinedo";
			}
		} else if (accion.equals("cancelar")) {
			forward = "CrearVinedo";
			accion = null;
			doProcess();
		} else if (accion.equals("imprimirINV")) {
			result = new ReportResult();
			result.setExport("PDF");
			result.setReportName("vinedosINV.jasper");
			result.setParams("SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";idCredito="
					+ idSolicitud);
			forward = "ReportesProcess2";
		}
		return chErrores.isEmpty();
	}

	private boolean esElaboracion() {
		if (idSolicitud != null && idSolicitud != 0) {
			Objetoi oi = (Objetoi) bp.getById(Objetoi.class, idSolicitud);
			if (oi.getDestino().equalsIgnoreCase("CRDA")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private void eliminarObjetoiVinedo() {
		if (idVinedo != null && idVinedo != 0) {
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			/**
			 * busco todos los detalles referidos al vi�edo que se va a eliminar
			 */
			ArrayList<DetalleVinedoINV> detalles = (ArrayList<DetalleVinedoINV>) bp
					.getNamedQuery("DetalleVinedoINV.findByVinedo").setParameter("vinedo", vinedo).list();
			for (DetalleVinedoINV detalleVinedoINV : detalles) {
				bp.delete(detalleVinedoINV);
			}
			/**
			 * busco todos los vi�edos vinculados al vi�edo a eliminar para romper la
			 * relacion
			 */
			ArrayList<ObjetoiVinedo> vinedosElaboracion = (ArrayList<ObjetoiVinedo>) bp
					.getNamedQuery("ObjetoiVinedo.findByElaboracion").setParameter("vinedo", vinedo).list();
			for (ObjetoiVinedo objetoiVinedo : vinedosElaboracion) {
				objetoiVinedo.setObjetoiVinedoElab(null);
				bp.saveOrUpdate(objetoiVinedo);
			}
			bp.delete(vinedo);
		}
	}

	@SuppressWarnings("unchecked")
	private void buscarVinedos() {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idSolicitud);
		vinedos = bp.getNamedQuery("ObjetoiVinedo.findByCredito").setParameter("credito", credito).list();
	}

	@SuppressWarnings("unchecked")
	private boolean validarCosecha() {
		if (cred.getDestino().equalsIgnoreCase("CRDC") || cred.getDestino().equalsIgnoreCase("CRDA")) {
			CosechaConfig cosecha = (CosechaConfig) bp
					.createQuery("SELECT c FROM CosechaConfig c WHERE c.fechaInicioPer <= :fecha AND "
							+ "c.fechaFinPer >= :fecha AND c.varietales = :varietales")
					.setParameter("fecha", cred.getFechaSolicitud()).setParameter("varietales", cred.getVarietales())
					.uniqueResult();
			List<Garantia> garantias = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
					.setParameter("credito", cred).list();
			Garantia garantia;
			if (garantias.isEmpty()) {
				garantia = new Garantia();
			} else {
				garantia = garantias.get(0);
			}
			PeriodoCosechaConfig periodoSinGarantia = null;
			PeriodoCosechaConfig periodo = null;
			List<PeriodoCosechaConfig> periodos = bp
					.createQuery("select p from PeriodoCosechaConfig p where "
							+ "p.cosechaConfig.id = :idCosecha and p.fechaDesde <= :fecha and p.fechaHasta >= :fecha")
					.setParameter("fecha", cred.getFechaSolicitud()).setParameter("idCosecha", cosecha.getId()).list();
			for (PeriodoCosechaConfig p : periodos) {
				if (p.getTipoGarantia_id() == null || p.getTipoGarantia_id() == 0) {
					periodoSinGarantia = p;
					continue;
				}
				if (p.getTipoGarantia_id().equals(garantia.getTipoId()) && (garantia.getTipoProducto() == null
						|| p.getTipoProducto().equals(garantia.getTipoProducto()))) {
					periodo = p;
					break;
				}
			}

			if (periodo == null && periodoSinGarantia == null) {
				chErrores.put("Solicitud.periodoCosecha", "Solicitud.periodoCosecha");
				return true;
			}

			if (periodo == null) { // si no coincide ninguna garantia, usar el periodo sin garantia
				periodo = periodoSinGarantia;
			}

			if (periodo != null) {
				// busco otros creditos con el mismo destino
				List<ObjetoiVinedo> ov = bp.createQuery("SELECT o FROM ObjetoiVinedo o "
						+ "WHERE o.vinedo.codigo =:vinedo AND o.credito.fechaSolicitud between :desde AND :hasta "
						+ "and o.credito.destino = :destino").setParameter("vinedo", codigo)
						.setParameter("desde", cosecha.getFechaInicioPer())
						.setParameter("hasta", cosecha.getFechaFinPer()).setParameter("destino", cred.getDestino())
						.list();

				if (ov.size() > 0) {
					// no puede repetirse el vinedo con el mismo destino
					return true;
				} else {
					if (cred.getDestino().equalsIgnoreCase("CRDA")) {
						// si es elaboracion, debe haber uno de cosecha en la misma temporada
						ov = bp.createQuery("SELECT o FROM ObjetoiVinedo o "
								+ "WHERE o.vinedo.codigo =:vinedo AND o.credito.fechaSolicitud between :desde AND :hasta "
								+ "and o.credito.destino = :destino").setParameter("vinedo", codigo)
								.setParameter("desde", cosecha.getFechaInicioPer())
								.setParameter("hasta", cosecha.getFechaFinPer()).setParameter("destino", "CRDC").list();
						if (ov.isEmpty()) {
							return true;
						} else {
							return false;
						}
					} else {
						return false;
					}
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void calcularDatosFinancieros() {
		idVinedo = 0L;
		codigo = "";
		CreditoHandler ch = new CreditoHandler();
		ch.calcularFinanciamiento(cred);
	}

	// valida el codigo, debe ser una letra mayuscula y 8 numeros
	private boolean valido() {
		if (codigo.length() == 6) {
			if (codigo.charAt(0) >= 65 && codigo.charAt(0) <= 90) {
				int count = 0;
				for (int i = 1; i <= 5; i++) {
					if (codigo.charAt(i) >= 48 && codigo.charAt(i) <= 57) {
						count += 1;
					}
				}
				if (count == 5) {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private boolean comprobarExistencia() {
		List<Vinedo> vinedos = bp.getNamedQuery("Vinedo.findByCodigo").setParameter("codigo", codigo).list();
		if (vinedos != null && vinedos.size() > 0) {
			cargarDatos(vinedos.get(0));
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private Vinedo comprobarExistenciaElaboracion() {
		List<ObjetoiVinedo> lista = bp.createQuery(
				"Select o from ObjetoiVinedo o where o.vinedo.codigo = :codigo AND o.credito.destino = :destino")
				.setParameter("codigo", codigo).setParameter("destino", "CRDC").list();

		if (lista != null && lista.size() > 0) {
			Date fecha = lista.get(0).getCredito().getFechaSolicitud();
			for (ObjetoiVinedo objetoiVinedo : lista) {
				if (objetoiVinedo.getCredito().getFechaSolicitud() != null) {
					if (!objetoiVinedo.getCredito().getFechaSolicitud().before(fecha)) {
						fecha = objetoiVinedo.getCredito().getFechaSolicitud();
						actual = objetoiVinedo.getCredito();
						vinedoActual = objetoiVinedo.getVinedo();
						objetoiVinedoActual = objetoiVinedo;
						titular = objetoiVinedo.getCredito().getPersona().getNomb12();
					}
				}
			}
			;
			return vinedoActual;
		} else {
			return null;
		}
	}

	private void guardar() {
		comprobarExistenciaElaboracion();
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idSolicitud);
		ObjetoiVinedo objVinedo = new ObjetoiVinedo();

		if (credito.getDestino().equalsIgnoreCase("CRDA")) {
			ObjetoiVinedo ova = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, objetoiVinedoActual.getId());
			Vinedo va = (Vinedo) bp.getById(Vinedo.class, vinedoActual.getId());
			objVinedo.setObjetoiVinedoElab(ova);
			// va.setHectareas(ova.getHectareas());
			objVinedo.setQqEstimados(ova.getQqEstimados());
			objVinedo.setCredito(credito);
			objVinedo.setEstadoSolicitudINV("Sin Solicitud");
			objVinedo.setVinedo(va);

			bp.save(objVinedo);

		} else {
			objVinedo.setQqEstimados(qqestimados);
			objVinedo.setCredito(credito);
			objVinedo.setEstadoSolicitudINV("Sin Solicitud");
			if (existe) {
				Vinedo vinedo = (Vinedo) bp.getNamedQuery("Vinedo.findByCodigo").setParameter("codigo", codigo)
						.uniqueResult();
				Localidad localidad = (Localidad) bp.getById(Localidad.class, idLocalidad);
				vinedo.setLocalidad(localidad);
				vinedo.setDepartamento(departamento);
				vinedo.setObservaciones(observaciones);
				vinedo.setHectareas(hectareas);
				bp.saveOrUpdate(vinedo);
				bp.commit();
				objVinedo.setVinedo(vinedo);
				bp.save(objVinedo);
			} else {
				Vinedo vinedo = new Vinedo();
				Localidad localidad = (Localidad) bp.getById(Localidad.class, idLocalidad);
				vinedo.setLocalidad(localidad);
				vinedo.setObservaciones(observaciones);
				vinedo.setCodigo(codigo);
				vinedo.setPersona(credito.getPersona());
				vinedo.setHectareas(hectareas);
				vinedo.setDepartamento(departamento);
				bp.save(vinedo);
				objVinedo.setVinedo(vinedo);
				bp.save(objVinedo);
			}
		}
		bp.getCurrentSession().flush();
		bp.commit();
	}

	private void cargarDatos(Vinedo vinedo) {
		hectareas = vinedo.getHectareas();
		departamento = vinedo.getDepartamento();
		idLocalidad = vinedo.getLocalidad().getId();
		observaciones = vinedo.getObservaciones();
	}

	private void guardarVinedo() {
		ObjetoiVinedo existente = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, objetoiVinedo.getId());
		existente.getVinedo().setHectareas(objetoiVinedo.getVinedo().getHectareas());
		existente.getVinedo().setObservaciones(objetoiVinedo.getVinedo().getObservaciones());
		existente.getVinedo().setIdLocalidad(objetoiVinedo.getVinedo().getIdLocalidad());
		existente.setQqEstimados(objetoiVinedo.getQqEstimados());
		bp.update(existente);
	}

	private boolean validarVinedo() {
		if (objetoiVinedo.getQqEstimados() == null || objetoiVinedo.getQqEstimados().doubleValue() <= 0.0) {
			chErrores.put("objetoiVinedo.faltanDatos", "objetoiVinedo.faltanDatos");
		}
		return chErrores.isEmpty();
	}

	public boolean isEstadoParaModificarYEliminar() {
		if (cred != null) {
			if (cred.getEstadoActual().getEstado().getNombreEstado().trim().equals("ANALISIS")
					|| cred.getEstadoActual().getEstado().getNombreEstado().trim().equals("ESPERANDO DOCUMENTACION")) {
				return true;
			} else
				return false;
		} else
			return false;
	}

	@Override
	public HashMap<String, Object> getErrors() {

		return chErrores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public Long getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Long idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public boolean isCiv() {
		return civ;
	}

	public void setCiv(boolean civ) {
		this.civ = civ;
	}

	public boolean isExiste() {
		return existe;
	}

	public void setExiste(boolean existe) {
		this.existe = existe;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public Double getHectareas() {
		return hectareas;
	}

	public void setHectareas(Double hectareas) {
		this.hectareas = hectareas;
	}

	public boolean isNoExiste() {
		return noExiste;
	}

	public void setNoExiste(boolean noExiste) {
		this.noExiste = noExiste;
	}

	public Double getQqestimados() {
		return qqestimados;
	}

	public void setQqestimados(Double qqestimados) {
		this.qqestimados = qqestimados;
	}

	public boolean isNoValido() {
		return noValido;
	}

	public void setNoValido(boolean noValido) {
		this.noValido = noValido;
	}

	public List<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(List<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public Long getIdVinedo() {
		return idVinedo;
	}

	public void setIdVinedo(Long idVinedo) {
		this.idVinedo = idVinedo;
	}

	@Transient
	public HashMap<String, Object> getChErrores() {
		if (chErrores == null) {
			chErrores = new HashMap<String, Object>();
		}
		return chErrores;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setChErrores(HashMap chErrores) {
		this.chErrores = chErrores;
	}

	public Objetoi getActual() {
		return actual;
	}

	public void setActual(Objetoi actual) {
		this.actual = actual;
	}

	public Vinedo getVinedoActual() {
		return vinedoActual;
	}

	public void setVinedoActual(Vinedo vinedoActual) {
		this.vinedoActual = vinedoActual;
	}

	public ObjetoiVinedo getObjetoiVinedoActual() {
		return objetoiVinedoActual;
	}

	public void setObjetoiVinedoActual(ObjetoiVinedo objetoiVinedoActual) {
		this.objetoiVinedoActual = objetoiVinedoActual;
	}

	public boolean isElaboracion() {
		return elaboracion;
	}

	public void setElaboracion(boolean elaboracion) {
		this.elaboracion = elaboracion;
	}

	public boolean isElaboracionTabla() {
		return elaboracionTabla;
	}

	public void setElaboracionTabla(boolean elaboracionTabla) {
		this.elaboracionTabla = elaboracionTabla;
	}

	public Double getQqaprobados() {
		return qqaprobados;
	}

	public void setQqaprobados(Double qqaprobados) {
		this.qqaprobados = qqaprobados;
	}

	public Objetoi getCred() {
		return cred;
	}

	public void setCred(Objetoi cred) {
		this.cred = cred;
	}

	public Date getFechaInformeINV() {
		return fechaInformeINV;
	}

	public void setFechaInformeINV(Date fechaInformeINV) {
		this.fechaInformeINV = fechaInformeINV;
	}

	public boolean isRequisitosCumplidos() {
		return requisitosCumplidos;
	}

	public void setRequisitosCumplidos(boolean requisitosCumplidos) {
		this.requisitosCumplidos = requisitosCumplidos;
	}

	public ObjetoiVinedo getObjetoiVinedo() {
		return objetoiVinedo;
	}

	public void setObjetoiVinedo(ObjetoiVinedo objetoiVinedo) {
		this.objetoiVinedo = objetoiVinedo;
	}

}
