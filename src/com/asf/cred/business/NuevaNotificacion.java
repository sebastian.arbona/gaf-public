package com.asf.cred.business;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.struts.upload.FormFile;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.ConfiguracionNotificacion;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.mailer.Mail;

public class NuevaNotificacion extends ConversationProcess {

	private Date fecha;
	private Date fechaVencimiento;
	private String observaciones;
	private String idTipoAviso;
	@Save
	private Long idObjetoi;
	private String forwardURL;
	private FormFile formFile;
	ObjetoiArchivo objetoiArchivo;
	@Save
	private Long idPersona;
	@Save
	private Long idNotificacion;
	private Notificacion notificacion;

	@ProcessMethod(defaultAction = true)
	public boolean mostrar() {
		if (idPersona != null) {
			forward = "NuevaNotificacionPer";
			return true;
		} else if (idObjetoi != null) {
			forward = "NuevaNotificacion";
			return true;
		} else if (idNotificacion != null) {
			load();
			forward = "NuevaNotificacion";
			return true;
		} else {
			return false;
		}
	}

	@ProcessMethod
	public boolean guardar() {

		if (fecha == null) {
			errors.put("nuevaNotificacion.fecha", "nuevaNotificacion.fecha");
			return false;
		}

		if (idPersona != null && idPersona != 0L) {
			try {

				Persona persona = (Persona) bp.getById(Persona.class, idPersona);

				if (persona != null) {
					Notificacion notif = new Notificacion();
					notif.setPersona(persona);

					if (idObjetoi != null && idObjetoi != 0L) {
						Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
						notif.setCredito(credito);
					}

					notif.setFechaCreada(fecha);
					notif.setFechaVenc(fechaVencimiento);
					notif.setObservaciones(observaciones);
					notif.setTipoAviso(idTipoAviso);
					notif.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());

					if (formFile != null && formFile.getFileSize() > 0) {
						objetoiArchivo = new ObjetoiArchivo();
						objetoiArchivo.setCredito(null);
						objetoiArchivo.setArchivo(formFile.getFileData());
						objetoiArchivo.setMimetype(formFile.getContentType());
						objetoiArchivo.setNombre(formFile.getFileName());
						objetoiArchivo.setFechaAdjunto(new Date());
						objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
						objetoiArchivo.setFuncionalidadOrigen("3");
						bp.saveOrUpdate(objetoiArchivo);
					}

					notif.setObjetoiArchivo(objetoiArchivo);
					bp.save(notif);

					if (persona.getEmailPrincipal() != null && esEmail(persona.getEmailPrincipal())) {
						enviarMails(persona.getEmailPrincipal());
					}

					forward = "ProcessRedirect";
					forwardURL = "/actions/process.do?do=process&processName=NotificacionProcess&process.accion=verNotifPersona&process.pestania=true&process.idPersona="
							+ idPersona;

					return true;
				} else {
					return false;
				}

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} else if (idObjetoi != null && idObjetoi != 0L) {
			try {

				Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);

				if (credito != null) {
					Notificacion notif = new Notificacion();
					notif.setCredito(credito);
					notif.setFechaCreada(fecha);
					notif.setFechaVenc(fechaVencimiento);
					notif.setObservaciones(observaciones);
					notif.setTipoAviso(idTipoAviso);
					notif.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
					notif.setPersona(credito.getPersona());

					if (formFile != null && formFile.getFileSize() > 0) {
						objetoiArchivo = new ObjetoiArchivo();
						objetoiArchivo.setCredito(null);
						objetoiArchivo.setArchivo(formFile.getFileData());
						objetoiArchivo.setMimetype(formFile.getContentType());
						objetoiArchivo.setNombre(formFile.getFileName());
						objetoiArchivo.setFechaAdjunto(new Date());
						objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
						bp.saveOrUpdate(objetoiArchivo);
					}

					notif.setObjetoiArchivo(objetoiArchivo);
					bp.save(notif);

					forward = "ProcessRedirect";
					forwardURL = "/actions/process.do?do=process&processName=NotificacionProcess&process.accion=verNotif&process.pestania=true&process.idCredito="
							+ idObjetoi;

					return true;
				} else {
					return false;
				}

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} else if (idNotificacion != null && idNotificacion != 0L) {
			try {

				Notificacion notificacion = (Notificacion) bp.getById(Notificacion.class, idNotificacion);

				if (notificacion != null) {
					notificacion.setFechaCreada(fecha);
					notificacion.setFechaVenc(fechaVencimiento);
					notificacion.setObservaciones(observaciones);
					notificacion.setTipoAviso(idTipoAviso);
					notificacion.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());

					if (formFile != null && formFile.getFileSize() > 0) {
						objetoiArchivo = new ObjetoiArchivo();
						objetoiArchivo.setCredito(null);
						objetoiArchivo.setArchivo(formFile.getFileData());
						objetoiArchivo.setMimetype(formFile.getContentType());
						objetoiArchivo.setNombre(formFile.getFileName());
						objetoiArchivo.setFechaAdjunto(new Date());
						objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
						bp.saveOrUpdate(objetoiArchivo);
					}

					// Ticket 9894. Evito que se borre la relaci�n con el adjunto si objetoiArchivo
					// es nulo --%>
					if (objetoiArchivo != null) {
						notificacion.setObjetoiArchivo(objetoiArchivo);
					}
					bp.update(notificacion);

					forward = "ProcessRedirect";
					forwardURL = "/actions/process.do?do=process&processName=NotificacionProcess&process.accion=verNotif&process.pestania=true&process.idCredito="
							+ notificacion.getCredito().getId();

					return true;
				} else {
					return false;
				}

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}

	}

	@ProcessMethod
	public boolean load() {
		if (idNotificacion != null && idNotificacion != 0L) {
			try {
				setNotificacion((Notificacion) bp.getById(Notificacion.class, idNotificacion));
				// forward = "ProcessRedirect";
				// forwardURL =
				// "/actions/process.do?do=process&processName=NotificacionProcess&process.accion=verNotif&process.pestania=true&process.idCredito="+notificacion.getCredito().getId();
				fecha = notificacion.getFechaCreada();
				fechaVencimiento = notificacion.getFechaVenc();
				observaciones = notificacion.getObservaciones();
				idTipoAviso = notificacion.getTipoAviso();
				objetoiArchivo = notificacion.getObjetoiArchivo();
				return true;
			} catch (Exception e) {
				this.setNotificacion(new Notificacion());
				this.errors.put("Notificacion.Error.cargar", "Notificacion.Error.carga");
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	// metodo para validar correo electronio
	public boolean esEmail(String correo) {
		Pattern pat = null;
		Matcher mat = null;
		pat = Pattern.compile(
				"^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
		mat = pat.matcher(correo);
		return mat.find();
	}

	public void enviarMails(String email) {

		try {

			String cnId = DirectorHelper.getString("configuracion.mailNotificaciones");
			if (cnId != null && !cnId.isEmpty()) {
				ConfiguracionNotificacion cn = (ConfiguracionNotificacion) SessionHandler.getCurrentSessionHandler()
						.getBusinessPersistance().getById(ConfiguracionNotificacion.class, new Long(cnId));
				if (cn != null) {
					Mail.sendMailTemplate(email.trim(), cn.getAsunto(), "mails/templateMail.html", cn.getTexto());
				}
			}

		} catch (Exception ex) {

			ex.printStackTrace();
		}

	}

	@Override
	protected String getDefaultForward() {
		return "NuevaNotificacion";
	}

	public String getFecha() {
		return DateHelper.getString(fecha);
	}

	public void setFecha(String fecha) {
		this.fecha = DateHelper.getDate(fecha);
	}

	public String getFechaVencimiento() {
		return DateHelper.getString(fechaVencimiento);
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = DateHelper.getDate(fechaVencimiento);
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getIdTipoAviso() {
		return idTipoAviso;
	}

	public void setIdTipoAviso(String idTipoAviso) {
		this.idTipoAviso = idTipoAviso;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getIdNotificacion() {
		return idNotificacion;
	}

	public void setIdNotificacion(Long idNotificacion) {
		this.idNotificacion = idNotificacion;
	}

	public Notificacion getNotificacion() {
		return notificacion;
	}

	public void setNotificacion(Notificacion notificacion) {
		this.notificacion = notificacion;
	}

}
