package com.asf.cred.business;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.MovimientoInstancia;
import com.nirven.creditos.hibernate.Objetoi;

public class ReporteMovimientoJudicial extends ConversationProcess {

	private String codAbogado;
	private Date fechaDesde;
	private Date fechaHasta;
	private int diasPeriodoAnterior;
	private String accion;
	private List<BeanReporteMovimientoJudicial> beanreportemovimientojudicial;
	private HashMap<Long, BeanReporteMovimientoJudicial> hashBeanReporteMovimientoJudicial;
	private BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ProcessMethod
	public boolean buscar() {
		String where = "";
		String wherepa = "";

		if (!(fechaDesde == null && fechaHasta == null)) {
			if (fechaDesde == null) {
				errors.put("bonificacionescobrar.fechaVencimientoDesde", "bonificacionescobrar.fechaVencimientoDesde");
				return false;
			}
			if (fechaHasta == null) {
				errors.put("bonificacionescobrar.fechaVencimientoHasta", "bonificacionescobrar.fechaVencimientoHasta");
				return false;
			}
			where = " Where mi.fechaMovimiento between CONVERT(datetime, ':fechaDesde', 102)  and CONVERT(datetime, ':fechaHasta', 102)";
			where = where.replace(":fechaDesde", new Timestamp(fechaDesde.getTime()).toString());
			where = where.replace(":fechaHasta", new Timestamp(fechaHasta.getTime()).toString());

			Date fechaDesdePeriodoAnterior = DateHelper.add(this.fechaDesde, this.getDiasPeriodoAnterior() * -1, 5);
			Date fechaHastaPeriodoAnterior = DateHelper.add(this.fechaDesde, -1, 5);

			wherepa = " Where mi.fechaMovimiento between CONVERT(datetime, ':fechaDesdePeriodoAnterior', 102)  and CONVERT(datetime, ':fechaHastaPeriodoAnterior', 102)";
			wherepa = wherepa.replace(":fechaDesdePeriodoAnterior",
					new Timestamp(fechaDesdePeriodoAnterior.getTime()).toString());
			wherepa = wherepa.replace(":fechaHastaPeriodoAnterior",
					new Timestamp(fechaHastaPeriodoAnterior.getTime()).toString());

		}

		String pHQL = "SELECT mi FROM MovimientoInstancia mi " + where;
		String pHQLpa = "SELECT mi FROM MovimientoInstancia mi " + wherepa;

		List<MovimientoInstancia> listMovimientoInstancia = bp.createQuery(pHQL).list();
		List<MovimientoInstancia> listMovimientoInstanciapa = bp.createQuery(pHQLpa).list();
		beanreportemovimientojudicial = new ArrayList<BeanReporteMovimientoJudicial>();

		hashBeanReporteMovimientoJudicial = new HashMap<>();

		for (MovimientoInstancia movimientoInstancia : listMovimientoInstancia) {

			BeanReporteMovimientoJudicial bean = new BeanReporteMovimientoJudicial();
			if (bean.fetch(movimientoInstancia, codAbogado, false)) {
				hashBeanReporteMovimientoJudicial = bean.merge(hashBeanReporteMovimientoJudicial, bean, false);
			}
			;
		}

		for (MovimientoInstancia movimientoInstancia : listMovimientoInstanciapa) {

			BeanReporteMovimientoJudicial bean = new BeanReporteMovimientoJudicial();
			if (bean.fetch(movimientoInstancia, codAbogado, true)) {
				hashBeanReporteMovimientoJudicial = bean.merge(hashBeanReporteMovimientoJudicial, bean, true);
			}
			;
		}

		Iterator it = hashBeanReporteMovimientoJudicial.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) it.next();
			BeanReporteMovimientoJudicial mapEntryElement = (BeanReporteMovimientoJudicial) mapEntry.getValue();
			beanreportemovimientojudicial.add(mapEntryElement);
		}

		return true;

	}

	@Override
	protected String getDefaultForward() {
		return "ReporteMovimientoJudicial";
	}

	public String getCodAbogado() {
		return codAbogado;
	}

	public void setCodAbogado(String codAbogado) {
		this.codAbogado = codAbogado;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<BeanReporteMovimientoJudicial> getBeanreportemovimientojudicial() {
		return beanreportemovimientojudicial;
	}

	public void setBeanreportemovimientojudicial(List<BeanReporteMovimientoJudicial> beanreportemovimientojudicial) {
		this.beanreportemovimientojudicial = beanreportemovimientojudicial;
	}

	public int getDiasPeriodoAnterior() {
		if (this.diasPeriodoAnterior <= 0) {
			return 90;
		} else {
			return diasPeriodoAnterior;
		}
	}

	public void setDiasPeriodoAnterior(int diasPeriodoAnterior) {
		this.diasPeriodoAnterior = diasPeriodoAnterior;
	}

	/*
	 * @Transient public HashMap<String,Object> getChErrores() { return
	 * this.chErrores; } public void setChErrores(HashMap<String, Object> chErrores)
	 * { this.chErrores = chErrores; }
	 */
	public class BeanReporteMovimientoJudicial {
		private Objetoi credito;
		private double deudaTotal;
		private String tomador;
		private long nroatencion;
		private String expediente;
		private String resolucion;
		private Date fechamovim;
		private Date fechamovimanterior;
		private String caratula;
		private String tipotribunal;
		private String nrotribunal;
		private Date fechainicio;
		private String ultimomov;
		private String movanterior;
		private String abogado;
		private String autos;
		private String obs1;
		private String obs2;
		private long id;
		private double saldo;
		private MovimientoInstancia movimientoInstancia;
		private boolean conmovimiento = false;

		public boolean fetch(MovimientoInstancia movimientoInstancia, String codigoAbogado, boolean scopeAnterior) {

			boolean fetchThis = false;
			if (codigoAbogado == null || "".equals(codigoAbogado)) {
				fetchThis = true;
			} else {
				Long idAbogado = Long.parseLong(codigoAbogado);
				Long idEspecialista = movimientoInstancia.getInstancia().getResolucion().getEspecialista().getPersona()
						.getId();
				if (idAbogado.longValue() == idEspecialista.longValue()) {
					fetchThis = true;
				} else {
					fetchThis = false;
				}
			}

			if (fetchThis) {

				if (scopeAnterior) {
					this.fechamovimanterior = movimientoInstancia.getFechaMovimiento();
					this.movanterior = movimientoInstancia.getTipoMovimientoInstancia().getNombre();
				} else {
					this.fechamovim = movimientoInstancia.getFechaMovimiento();
					this.ultimomov = movimientoInstancia.getTipoMovimientoInstancia().getNombre();
					this.conmovimiento = true;
				}

				this.caratula = movimientoInstancia.getInstancia().getCaratula();
				this.tipotribunal = movimientoInstancia.getInstancia().getTipoTribunal().getNombre();
				this.nrotribunal = movimientoInstancia.getInstancia().getTribunal().getNombre();
				this.autos = movimientoInstancia.getInstancia().getExpediente();

				this.resolucion = movimientoInstancia.getInstancia().getResolucion().getNumero();
				this.fechainicio = movimientoInstancia.getInstancia().getFechaInicio();
				this.abogado = movimientoInstancia.getInstancia().getResolucion().getEspecialista().getPersona()
						.getNomb12();

				if (movimientoInstancia.getInstancia().getResolucion().getProyecto() != null) {

					this.expediente = movimientoInstancia.getInstancia().getResolucion().getProyecto().getExpediente();
					this.nroatencion = movimientoInstancia.getInstancia().getResolucion().getProyecto()
							.getNumeroAtencion();
					this.tomador = movimientoInstancia.getInstancia().getResolucion().getProyecto().getPersona()
							.getNomb12();
					this.credito = movimientoInstancia.getInstancia().getResolucion().getProyecto();
					this.id = movimientoInstancia.getInstancia().getResolucion().getId();

				}

				this.movimientoInstancia = movimientoInstancia;

				return true;

			} else {
				return false;
			}
		}

		private void calcularDeudas() {

			if (this.movimientoInstancia.getInstancia().getResolucion().getProyecto() != null) {

				CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(
						this.movimientoInstancia.getInstancia().getResolucion().getProyecto().getId(), new Date());
				ccd.calcular();
				this.deudaTotal = ccd.getDeudaTotal();
				this.saldo = ccd.getSaldoCuota();
			}
		}

		public HashMap<Long, BeanReporteMovimientoJudicial> merge(HashMap<Long, BeanReporteMovimientoJudicial> hash,
				BeanReporteMovimientoJudicial bean, boolean scopeAnterior) {

			BeanReporteMovimientoJudicial beanCompare = hash.get(bean.getId());
			if (beanCompare == null) {
				bean.calcularDeudas();
				hash.put(bean.getId(), bean);
			} else {

				boolean upd = false;
				if (scopeAnterior) {

					if (beanCompare.getFechamovimanterior() == null && bean.getFechamovimanterior() == null) {
						upd = false;
					} else if (beanCompare.getFechamovimanterior() == null) {
						upd = true;
					} else if (bean.getFechamovimanterior() == null) {
						upd = false;
					} else {
						if (bean.getFechamovimanterior().compareTo(beanCompare.getFechamovimanterior()) > 0) {
							upd = true;
						} else {
							upd = false;
						}
					}

					if (upd) {
						beanCompare.setFechamovimanterior(bean.getFechamovimanterior());
						beanCompare.setMovanterior(bean.getMovanterior());
					}

				} else {

					if (beanCompare.getFechamovim() == null && bean.getFechamovim() == null) {
						upd = false;
					} else if (beanCompare.getFechamovim() == null) {
						upd = true;
					} else if (bean.getFechamovim() == null) {
						upd = false;
					} else {
						if (bean.getFechamovim().compareTo(beanCompare.getFechamovim()) > 0) {
							upd = true;
						} else {
							upd = false;
						}
					}

					if (upd) {
						beanCompare.setFechamovim(bean.getFechamovim());
						beanCompare.setUltimomov(bean.getUltimomov());
						beanCompare.setConmovimiento(true);
					}

				}
				hash.put(beanCompare.getId(), beanCompare);
			}
			return hash;
		}

		BeanReporteMovimientoJudicial() {
		}

		BeanReporteMovimientoJudicial(Object[] dato) {

//		sql = "SELECT o.id , pe.NOMB_12, o.numeroAtencion,  o.expediente, ";
//		sql += "r.numero, isnull(CONVERT(VARCHAR,pr.fechaInicio,103), '') as fechaInicio, isnull(pr.expediente,'') as nroauto,";
//		sql += "isnull(pr.caratula,'') as caratula , isnull(tt.nombre,'') as nombre, isnull(CONVERT(VARCHAR, mi.fechaMovimiento,103),'') as fechaMovimiento,";
//		sql += "isnull(tm.nombre,'') as nombre1, isnull(t.nombre,'') as nombre2, isnull(mi.observacion,'') as observacion1, isnull(p.NOMB_12,'') as abogado, isnull(ob.observacion,'') as observacion2 ";
			long id;
			if (dato != null) {
				id = ((BigDecimal) dato[0]).longValue();
				this.credito = (Objetoi) bp.createQuery("select o from Objetoi o where o.id = :id").setLong("id", id)
						.uniqueResult(); // o.id
				// this.tomador = dato[1].toString(); //pe.NOMB_12,
				// this.nroatencion = ((BigDecimal) dato[2]).longValue(); //o.numeroAtencion
				// this.expediente = dato[3].toString(); //o.expediente
				// this.resolucion = dato[4].toString(); //r.numero,
				// this.fechainicio = this.formatDate(dato[5].toString()); //
				// isnull(CONVERT(VARCHAR,pr.fechaInicio,103), '') as fechaInicio
				// this.autos = dato[6].toString(); //isnull(pr.expediente,'') as nroauto
				// this.caratula = dato[7].toString(); //isnull(pr.caratula,'') as caratula
				// this.tipotribunal = dato[8].toString(); //isnull(tt.nombre,'') as nombre
				// this.fechamovim = this.formatDate(dato[9].toString()); // 9
				// isnull(CONVERT(VARCHAR, mi.fechaMovimiento,103),'') as fechaMovimiento
				// this.ultimomov = dato[10].toString(); //isnull(tm.nombre,'') as nombre1
				// this.nrotribunal = dato[11].toString(); //isnull(t.nombre,'') as nombre2
				// this.obs1 = dato[12].toString(); //snull(mi.observacion,'') as observacion1
				this.abogado = dato[13].toString(); // isnull(p.NOMB_12,'') as abogado
				this.obs2 = dato[14].toString();// isnull(ob.observacion,'') as observacion2

				CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(credito.getId(), new Date());
				ccd.calcular();
				deudaTotal = ccd.getDeudaTotal();
			}
		}

		BeanReporteMovimientoJudicial(MovimientoInstancia movimientoInstancia, String codAbogado) {

		}

		public Objetoi getCredito() {
			return credito;
		}

		public void setCredito(Objetoi credito) {
			this.credito = credito;
		}

		public double getDeudaTotal() {
			return deudaTotal;
		}

		public void setDeudaTotal(double deudaTotal) {
			this.deudaTotal = deudaTotal;
		}

		public String getCaratula() {
			return caratula;
		}

		public void setCaratula(String caratula) {
			this.caratula = caratula;
		}

		public Date getFechamovim() {
			return fechamovim;
		}

		public void setFechamovim(Date fechamovim) {
			this.fechamovim = fechamovim;
		}

		public String getResolucion() {
			return resolucion;
		}

		public void setResolucion(String resolucion) {
			this.resolucion = resolucion;
		}

		public String getExpediente() {
			return expediente;
		}

		public void setExpediente(String expediente) {
			this.expediente = expediente;
		}

		public long getNroatencion() {
			return nroatencion;
		}

		public void setNroatencion(long nroatencion) {
			this.nroatencion = nroatencion;
		}

		public String getTomador() {
			return tomador;
		}

		public void setTomador(String tomador) {
			this.tomador = tomador;
		}

		public Date getFechainicio() {
			return fechainicio;
		}

		public void setFechainicio(Date fechainicio) {
			this.fechainicio = fechainicio;
		}

		public String getUltimomov() {
			return ultimomov;
		}

		public void setUltimomov(String ultimomov) {
			this.ultimomov = ultimomov;
		}

		public String getAbogado() {
			return abogado;
		}

		public void setAbogado(String abogado) {
			this.abogado = abogado;
		}

		public String getAutos() {
			return autos;
		}

		public void setAutos(String autos) {
			this.autos = autos;
		}

		public String getTipotribunal() {
			return tipotribunal;
		}

		public void setTipotribunal(String tipotribunal) {
			this.tipotribunal = tipotribunal;
		}

		public String getNrotribunal() {
			return nrotribunal;
		}

		public void setNrotribunal(String nrotribunal) {
			this.nrotribunal = nrotribunal;
		}

		public String getObs1() {
			return obs1;
		}

		public void setObs1(String obs1) {
			this.obs1 = obs1;
		}

		public String getObs2() {
			return obs2;
		}

		public void setObs2(String obs2) {
			this.obs2 = obs2;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public double getSaldo() {
			return saldo;
		}

		public void setSaldo(double saldo) {
			this.saldo = saldo;
		}

		public Date getFechamovimanterior() {
			return fechamovimanterior;
		}

		public void setFechamovimanterior(Date fechamovimanterior) {
			this.fechamovimanterior = fechamovimanterior;
		}

		public String getMovanterior() {
			return movanterior;
		}

		public void setMovanterior(String movanterior) {
			this.movanterior = movanterior;
		}

		public boolean getConmovimiento() {
			return conmovimiento;
		}

		public void setConmovimiento(boolean conmovimiento) {
			this.conmovimiento = conmovimiento;
		}

	}

}
