package com.asf.cred.business;

import java.io.Serializable;

public class SaldoCapitalBean implements Serializable {

	private static final long serialVersionUID = 2566864241844159524L;
	
	private String detalle;
	private double capital;
	private double interes;
	private double otros;
	private SaldoCapitalBean pesos;
	
	public SaldoCapitalBean(String detalle, double capital, double interes,
			double otros) {
		this.detalle = detalle;
		this.capital = capital;
		this.interes = interes;
		this.otros = otros;
	}
	
	public void addCapital(double capital) {
		this.capital += capital;
	}
	public void addInteres(double interes) {
		this.interes += interes;
	}
	public void addOtros(double otros) {
		this.otros += otros;
	}
	public void addCapitalPesos(double capital) {
		this.getPesos().addCapital(capital);
	}
	public void addInteresPesos(double interes) {
		this.getPesos().addInteres(interes);
	}
	public void addOtrosPesos(double otros) {
		this.getPesos().addOtros(otros);
	}
	
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public double getCapital() {
		return capital;
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	public double getInteres() {
		return interes;
	}
	public void setInteres(double interes) {
		this.interes = interes;
	}
	public double getOtros() {
		return otros;
	}
	public void setOtros(double otros) {
		this.otros = otros;
	}
	public double getTotal() {
		return capital + interes + otros;
	}
	public SaldoCapitalBean getPesos() {
		if (pesos == null) {
			pesos = new SaldoCapitalBean(detalle, 0, 0, 0);
		}
		return pesos;
	}
}
