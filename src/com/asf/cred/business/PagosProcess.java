package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;

import com.asf.gaf.hibernate.Ejercicio;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.ReportResult;
import com.asf.util.SQLHelper;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;

public class PagosProcess implements IProcess {

	private BusinessPersistance bp;
	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private Objetoi objetoi;
	private List<Pagos> pagos;
	private ReportResult reporte;
	private Pagos pago;
	// private String idBoleto;
	private Long idCaratula;
	private String esquemaGAF;

	public PagosProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		forward = "PagosProcess";
		objetoi = (Objetoi) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("objetoiPagosProcess");
		if (objetoi == null) {
			objetoi = new Objetoi();
		}
		pagos = new ArrayList<Pagos>();
		pago = new Pagos();
		esquemaGAF = DirectorHelper.getString("SGAF");
	}

	@Override
	public boolean doProcess() {
		if (accion != null && accion.equals("imprimir")) {
			objetoi = (Objetoi) bp.getById(Objetoi.class, objetoi.getId());
			imprimir();
			if (errores.isEmpty()) {
				forward = "ReportesProcess2";
			}
			return errores.isEmpty();
		}
		listarPagos();
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listarPagos() {
		objetoi = (Objetoi) bp.getById(Objetoi.class, objetoi.getId());
		pagos = bp
				.createQuery(
						"SELECT p FROM Pagos p JOIN p.id.boleto b WHERE b.objetoi = :objetoi AND p.recibo IS NOT NULL")
				.setEntity("objetoi", objetoi).list();
		for (Pagos pago : pagos) {
			Boleto boleto = pago.getBoleto();
			String medioPago = (String) bp
					.createQuery("SELECT c.mediopago.denominacion FROM Bolepago bp JOIN bp.id.cobropago c "
							+ "WHERE bp.id.boleto = :boleto")
					.setEntity("boleto", boleto).setMaxResults(1).uniqueResult();
			pago.setMedioPago(medioPago);
		}
	}

	@SuppressWarnings("unchecked")
	public void imprimir() {
		pago = (Pagos) bp
				.createSQLQuery("SELECT * FROM Pagos p WHERE p.boleto_id = :boleto_id AND p.caratula_id = :caratula_id")
				.addEntity(Pagos.class).setLong("boleto_id", pago.getId().getBoleto().getId())
				.setLong("caratula_id", getIdCaratula()).setMaxResults(1).uniqueResult();
		try {
			Long idBoleto;
			if (pago.getRecibo() != null) {
				idBoleto = pago.getRecibo().getId();
			} else {
				idBoleto = pago.getId().getBoleto().getId();
			}
			Boleto b = (Boleto) bp.getById(Boleto.class, idBoleto);
			Boleto boleto;
			if (pago.getBoleto() != null)
				boleto = pago.getBoleto();
			else
				boleto = b;
			String medioPago = (String) bp
					.createQuery("SELECT c.mediopago.denominacion "
							+ "FROM Bolepago bp JOIN bp.id.cobropago c WHERE bp.id.boleto = :boleto")
					.setEntity("boleto", boleto).uniqueResult();
			String expediente = b.getObjetoi().getExpediente();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			String queryDomicilio = "SELECT (CASE WHEN d.calle_IDCALLE IS NULL THEN d.calleNom ELSE ca.DETA_08 end) AS 'calleNom', d.numero,"
					+ "d.manzana, (CASE WHEN d.barrio_CODI_BRR IS NULL THEN (CASE WHEN d.barrioNom IS NULL THEN '' ELSE d.barrioNom end) ELSE b.DESC_BRR end) AS 'barrioNom',"
					+ "(CASE WHEN d.localidad_IDLOCALIDAD IS NULL THEN '' ELSE l.CP end) AS 'localidadCP',"
					+ "(CASE WHEN d.localidad_IDLOCALIDAD IS NULL THEN d.localidadNom ELSE l.NOMBRE end) AS 'localidadNom',"
					+ "(CASE WHEN d.departamentoNom IS NULL THEN '' ELSE d.departamentoNom END) AS 'departamentoNom',"
					+ "d.lote,(CASE WHEN d.provincia_CODI_08 IS NULL THEN d.provinciaNom ELSE pr.DETA_08 end) AS 'provinciaNom' FROM Objetoi ob "
					+ "LEFT JOIN Persona pe on ob.persona_IDPERSONA = pe.IDPERSONA "
					+ "LEFT JOIN DomicilioObjetoi do on do.objetoi_id = ob.id "
					+ "LEFT JOIN Domicilio d on do.domicilio_id = d.id "
					+ "LEFT JOIN Calle ca ON ca.IDCALLE = d.calle_IDCALLE "
					+ "LEFT JOIN LOCALIDAD l on l.IDLOCALIDAD = d.localidad_IDLOCALIDAD "
					+ "LEFT JOIN BARRIO b on b.CODI_BRR = d.barrio_CODI_BRR "
					+ "LEFT JOIN PROVIN pr on pr.CODI_08 = d.provincia_CODI_08 WHERE ob.id = :objetoi_id";
			queryDomicilio = SQLHelper.getLimitedQuery(queryDomicilio, 1);
			List<Object[]> domicilios = bp.createSQLQuery(queryDomicilio).setLong("objetoi_id", objetoi.getId()).list();
			Object[] domicilio;
			if (!domicilios.isEmpty()) {
				domicilio = domicilios.get(0);
			} else {
				domicilio = null;
			}

			int anioActual = Ejercicio.getEjercicioActual().intValue();

			String calleNom = domicilio == null || domicilio[0] == null ? null : domicilio[0].toString();
			String numero = domicilio == null || domicilio[1] == null ? null : domicilio[1].toString();
			String manzana = domicilio == null || domicilio[2] == null ? null : domicilio[2].toString();
			String barrioNom = domicilio == null || domicilio[3] == null ? "" : domicilio[3].toString();
			String cp = domicilio == null || domicilio[4] == null ? null : domicilio[4].toString();
			String nombre = domicilio == null || domicilio[5] == null ? null : domicilio[5].toString();
			String departamentoNom = domicilio == null || domicilio[6] == null ? null : domicilio[6].toString();
			String lote = domicilio == null || domicilio[7] == null ? null : domicilio[7].toString();
			String provin = domicilio == null || domicilio[8] == null ? null : domicilio[8].toString();
			String consultaBasica = "SELECT b.tiponrocuenta, CASE WHEN idmoneda = 1 THEN 'Pesos' "
					+ "WHEN idmoneda = 2 THEN 'D�lares' WHEN idmoneda = 3 THEN 'Euros' "
					+ "ELSE CAST(idmoneda AS varchar) END AS 'moneda', "
					+ "CASE WHEN tipoCta = 'CC' THEN 'Cuenta Corriente' WHEN tipoCta = 'CA' THEN 'Caja de Ahorro' "
					+ "ELSE tipoCta END AS 'tipoCta' FROM " + this.esquemaGAF + ".ParametrizacionBancos p "
					+ "INNER JOIN " + this.esquemaGAF + ".Bancue b ON b.idBancue = p.idBancue "
					+ "WHERE p.codi_Ba = :codiBa AND p.ejercicio = :ejercicio";
			String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> listado = bp.createSQLQuery(consultaLimitada)
					.setParameter("codiBa", pago.getId().getCaratula().getBanco().getCodiBa())
					.setParameter("ejercicio", anioActual).list();
			String tipoNumeroCuenta = "";
			String tipoCta = "";
			String banco = "";
			String moneda = "";
			if (!listado.isEmpty()) {
				Object[] object = listado.get(0);
				tipoNumeroCuenta = (String) object[0];
				moneda = (String) object[1];
				tipoCta = (String) object[2];
			}

			boolean cambioMoneda = false;
			Long idMonedaCredito = b.getObjetoi().getMoneda_id() != null ? b.getObjetoi().getMoneda_id()
					: b.getObjetoi().getLinea().getMoneda_id();
			if (idMonedaCredito == null) {
				idMonedaCredito = 1L;
			}
			if (!idMonedaCredito.equals(1L)) {
				// el credito no esta en pesos
				cambioMoneda = pago.getMoneda() != null && !pago.getMoneda().getId().equals(idMonedaCredito);
			}

			String marcaagua = DirectorHelper.getString("marca.agua");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("pago.jasper");
			reporte.setParams("PROYECTO=" + objetoi.getNumeroAtencionStr() + ";TITULAR="
					+ objetoi.getPersona().getNomb12Str() + ";BOLETO_ID=" + idBoleto + ";CUIT="
					+ objetoi.getPersona().getCuil12Str() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId
					+ ";EXPEDIENTE=" + expediente + ";numeroBoleto=" + b.getNumeroBoleto() + ";medioPago=" + medioPago
					+ ";FECHA_EMISION=" + b.getFechaEmision().getTime() + ";calleNom=" + calleNom + ";idObjetoi="
					+ b.getObjetoi().getId() + ";MONEDA="
					// Se comenta la siguiente linea para obtener la denominacion de la moneda de la
					// linea y no del pago.
					// + (pago.getMoneda() != null ? pago.getMoneda().getDenominacion() :
					// b.getObjetoi().getLinea().getMoneda().getDenominacion())
					+ b.getObjetoi().getLinea().getMoneda().getDenominacion() + ";numero=" + numero + ";manzana="
					+ manzana + ";barrioNom=" + barrioNom + ";cp=" + cp + ";nombre=" + nombre + ";departamentoNom="
					+ departamentoNom + ";lote=" + lote + ";provincia=" + provin + ";TIPO_NUMERO_CUENTA="
					+ tipoNumeroCuenta + ";TIPO_CTA=" + tipoCta + ";BANCO="
					+ pago.getId().getCaratula().getBanco().getProveedor().getFantasia() + ";SCHEMA="
					+ BusinessPersistance.getSchema()
					+ (cambioMoneda && pago.getMontoOriginal() != null
							? ";montoOriginal=" + DoubleHelper.getString(pago.getMontoOriginal())
							: "")
					+ (cambioMoneda && pago.getCotizacion() != null
							? ";cotizacion=" + DoubleHelper.getString(pago.getCotizacion())
							: "")
					+ (cambioMoneda && pago.getMoneda() != null
							? ";monedaOriginal=" + pago.getMoneda().getDenominacion()
							: "")
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ ";MARCAAGUA=" + marcaagua);

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("objetoiPagosProcess",
					objetoi);
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "pago"));
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public List<Pagos> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pagos> pagos) {
		this.pagos = pagos;
	}

	public Pagos getPago() {
		return pago;
	}

	public void setPago(Pagos pago) {
		this.pago = pago;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCartula) {
		this.idCaratula = idCartula;
	}

}
