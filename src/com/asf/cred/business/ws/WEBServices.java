package com.asf.cred.business.ws;

import java.util.Date;

import javax.jws.WebMethod;

import org.codehaus.xfire.util.Base64;

import com.asf.cred.business.BoletosDePago;
import com.asf.cred.business.CreditoCuentaCorriente;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.civitas.cred.jasper.BoletosDePagoScriptlet;
import com.civitas.util.ReportResultHelper;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Objetoi;

public class WEBServices {
	
	@WebMethod(operationName="boletoActual")
	public String boletoActual(Long objetoiId, Long boletoId) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			BoletosDePago bp=new BoletosDePago();
			bp.setObjetoi(new Objetoi());
			bp.getObjetoi().setId(objetoiId);
			bp.setBoleto(new Boleto());
			bp.getBoleto().setId(boletoId);
			bp.imprimirBoletos();
			ReportResult rr=(ReportResult)bp.getResult();
			return Base64.encode(ReportResultHelper.getReportePdf(rr));
			
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return null;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}
	
	@WebMethod(operationName="boletoDeuda")
	public String boletoDeuda(Long objetoiId, Long personaId, boolean deudaParcial) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			CreditoCuentaCorriente ccc=new CreditoCuentaCorriente();
			ccc.setDeudaParcial(deudaParcial);
			ccc.setIdObjetoi(objetoiId);
			ccc.setAction("generar");
			ccc.setPersonaId(personaId);
			ccc.setHastaFecha(new Date());
			ccc.generarReporte();
			
			ReportResult rr=(ReportResult)ccc.getResult();
			return Base64.encode(ReportResultHelper.getReportePdf(rr));
			
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return null;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}
	
	@WebMethod(operationName="calcularDeuda")
	public Double calcularDeuda(Long objetoiId, Long personaId, Boolean deudaParcial) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			BoletosDePagoScriptlet bps=new BoletosDePagoScriptlet();
			bps.calcularDeuda(objetoiId.toString(), DateHelper.getString(new Date()), deudaParcial.toString());
			
			return bps.getTotal();
			
		} catch (Exception e) {
			e.printStackTrace();
			//sh.getBusinessPersistance().rollback();
			return null;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}


}
