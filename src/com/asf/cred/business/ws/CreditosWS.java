package com.asf.cred.business.ws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import javax.jws.WebMethod;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.http.HTTPException;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.cred.business.CreditoDesembolsos;
import com.asf.cred.business.CreditoHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.bpm.ConsultaDeudaAHFt;
import com.nirven.creditos.bpm.ResultadoConsulta;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.HectareasCultivadas;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;

public class CreditosWS {

	@WebMethod
	public Boolean ejecutarDesembolso(String desembolsoMovalor, Double total, XMLGregorianCalendar fechaEjecucion1,
			Integer numeroRecibo, HashMap<String, Object> gastos) {
		/* otros gastos gastos de la l�nea, novedades, facturas */
		Double otrosGastos = (Double) gastos.get("otrosGastos");
		Double gastosAdministrativos = (Double) gastos.get("gastosAdministrativos");
		Double ivaGastosAdministrativos = (Double) gastos.get("ivaGastosAdministrativos");
		String novedadesIds = (String) gastos.get("novedadesIds");
		String gastosARecuperarIds = (String) gastos.get("gastosARecuperarIds");

		Date fechaEjecucion = fechaEjecucion1.toGregorianCalendar().getTime();
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			BusinessPersistance bp = sh.getBusinessPersistance();
			bp.begin();

			// Novedades Cta Cte
			novedadesIds = (String) gastos.get("novedadesIds");
			List<NovedadCtaCte> novedades = this.getNovedades(novedadesIds);
			Double totalNovedades = getTotalNovedades(novedades);

			// Gastos a recuperar (Detalles Factura)
			gastosARecuperarIds = (String) gastos.get("gastosARecuperarIds");
			List<DetalleFactura> detalles = this.getDetalles(gastosARecuperarIds);
			Double totalGastosARecuperar = getTotalGastosARecuperar(detalles);

			// Gastos Administrativos
			gastosAdministrativos = (Double) gastos.get("gastosAdministrativos");
			ivaGastosAdministrativos = (Double) gastos.get("ivaGastosAdministrativos");

			// otros gastos
			otrosGastos = (Double) gastos.get("otrosGastos");

			gastos.clear();
			gastos.put("totalNovedades", totalNovedades);
			gastos.put("totalGastosARecuperar", totalGastosARecuperar);
			gastos.put("gastosAdministrativos", gastosAdministrativos);
			gastos.put("ivaGastosAdministrativos", ivaGastosAdministrativos);
			gastos.put("otrosGastos", otrosGastos);

			Desembolso desembolso = (Desembolso) bp.getById(Desembolso.class,
					new Long(desembolsoMovalor.split("-")[0]));
			CreditoHandler ch = new CreditoHandler();
			// TODO modificar firma del metodo para recibir el HashMap gastos, la rama "iva"
			// ya lo tiene implementado
			ch.crearNotaDebitoGastosARecuperar(desembolso.getCredito_id(), false, "", fechaEjecucion, otrosGastos,
					gastosAdministrativos, totalNovedades, totalGastosARecuperar, desembolsoMovalor);
			CreditoDesembolsos cd = new CreditoDesembolsos();
			// TODO modificar firma del metodo para recibir el HashMap gastos, la rama "iva"
			// ya lo tiene implementado
			boolean r = cd.ejecutarDesembolso(new Long(desembolsoMovalor.split("-")[0]), total, fechaEjecucion,
					numeroRecibo, otrosGastos, gastosAdministrativos, totalNovedades, totalGastosARecuperar);
			bp.commit();
			return r;
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return false;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}

	private Double getTotalGastosARecuperar(List<DetalleFactura> detalles) {
		if (detalles == null || detalles.isEmpty()) {
			return new Double(0.0);
		}
		double ret = 0;
		for (DetalleFactura d : detalles) {
			ret += d.getImporte();
		}
		return new Double(ret);
	}

	private Double getTotalNovedades(List<NovedadCtaCte> detalles) {
		if (detalles == null || detalles.isEmpty()) {
			return new Double(0.0);
		}
		double ret = 0;
		for (NovedadCtaCte d : detalles) {
			ret += d.getImporte();
		}
		return new Double(ret);
	}

	@SuppressWarnings("unchecked")
	private List<NovedadCtaCte> getNovedades(String novedadesIds) {
		if (novedadesIds == null || novedadesIds.isEmpty()) {
			return null;
		}
		return SessionHandler.getCurrentSessionHandler().getBusinessPersistance()
				.createQuery("FROM " + NovedadCtaCte.class.getCanonicalName() + " n WHERE n.id in (:ids)")
				.setParameterList("ids", getIds(novedadesIds)).list();
	}

	@SuppressWarnings("unchecked")
	private List<DetalleFactura> getDetalles(String novedadesIds) {
		if (novedadesIds == null || novedadesIds.isEmpty()) {
			return null;
		}
		return SessionHandler.getCurrentSessionHandler().getBusinessPersistance()
				.createQuery("FROM " + DetalleFactura.class.getCanonicalName() + " n WHERE n.id in (:ids)")
				.setParameterList("ids", getIds(novedadesIds)).list();
	}

	@SuppressWarnings("rawtypes")
	private Collection getIds(String novedadesIds) {
		String[] a = novedadesIds.split(",");
		List<Long> ret = new ArrayList<Long>();
		for (String s : a) {
			ret.add(new Long(s));
		}
		return ret;
	}

	@WebMethod
	public String anularEntregaPago(Long idDesembolso, String fecha) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			BusinessPersistance bp = sh.getBusinessPersistance();
			bp.begin();

			Date date = DateHelper.getDate(fecha);

			Desembolso desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);

			CreditoDesembolsos cd = new CreditoDesembolsos();
			String r = cd.anularEntregaPago(idDesembolso, date);

			CreditoHandler ch = new CreditoHandler();
			ch.crearNotaDebitoGastosARecuperar(desembolso.getCredito_id(), true, "", date, null, null, null, null,
					null);

			bp.commit();
			return r;
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return "Error en la anulaci�n del pago del desembolso " + idDesembolso;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}

	@WebMethod
	public String anularPagosDesembolsos(Long idOrdepago, Long nroOrden) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			CreditoDesembolsos cd = new CreditoDesembolsos();
			sh.getBusinessPersistance().begin();
			String resultado = cd.anularPagosDesembolsos(idOrdepago, nroOrden);
			if (resultado.equals("ok"))
				sh.getBusinessPersistance().commit();
			else
				sh.getBusinessPersistance().rollback();
			return resultado;
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return "Error en la anulaci�n del desembolso asociado al idOrdepago: " + idOrdepago + ".";
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}

	@WebMethod
	public String crearNotaDebitoGastosARecuperar(Long idObjetoi, Boolean anula, String expePago, String fecha) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		CreditoHandler ch = new CreditoHandler();
		try {
			sh.getBusinessPersistance().begin();
			Date fechaPago = null;
			if (fecha != null) {
				fechaPago = DateHelper.getDate(fecha);
			}
			ch.crearNotaDebitoGastosARecuperar(idObjetoi, anula, expePago, fechaPago, null, null, null, null, null);
			sh.getBusinessPersistance().commit();
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return "Error en la ejecuci�n del WS crearNotaDebitoGastosARecuperar";
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}
	
	@WebMethod
	public List<ConsultaDeudaResult>  consultaDeuda(Long cuit) throws Exception{
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			BusinessPersistance bp=sh.getBusinessPersistance();
			Persona p=(Persona)bp.createQuery("FROM Persona p where p.cuil12=:cuit").setLong("cuit", cuit).setMaxResults(1).uniqueResult();
			if(p==null)
				return new ArrayList<ConsultaDeudaResult>() ;
			return getDeuda(p.getId());
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			throw e;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}
	
	@WebMethod
	public HectareasCultivadasResponse consultaHectareas(Long cuit) {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		try {
			BusinessPersistance bp=sh.getBusinessPersistance();
			Persona p=(Persona)bp.createQuery("FROM Persona p where p.cuil12=:cuit").setLong("cuit", cuit).setMaxResults(1).uniqueResult();
			List<HectareasCultivadas>hectareasCultivadas = bp.createQuery("FROM HectareasCultivadas WHERE cuit LIKE '%" + cuit + "%'").list();
			int vinedos=0;
			double superficie=0.0;
			for(HectareasCultivadas hc:hectareasCultivadas) {
				vinedos+=hc.getVinedos();
				superficie+=hc.getSuperficie();
			}
			
			return new HectareasCultivadasResponse(p==null?"":p.getNomb12Str(), vinedos, superficie);
			
		} catch (Exception e) {
			e.printStackTrace();
			sh.getBusinessPersistance().rollback();
			return null;
		} finally {
			sh.unRegister();
			sh.logout();
		}
	}
	
	
	
	public List<ConsultaDeudaResult> getDeuda(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		long idRelacionCotomador = DirectorHelper.getLong("relacion.cotomador", -1l);
		List<Number> personaVinculada = new ArrayList<Number>();
		HashSet<Objetoi> objetos = new LinkedHashSet<Objetoi>();
		personaVinculada.addAll(bp.createSQLQuery("select p.personaTitular_IDPERSONA from " + bp.getSchema()
				+ ".tipoRelacion tr inner join " + bp.getSchema()
				+ ".relacion r on r.tipoRelacion_idTipoRelacion = tr.idTipoRelacion inner join " + bp.getSchema()
				+ ".PersonaVinculada p on p.relacion_idRelacion = r.idRelacion "
				+ "WHERE p.personaVinculada_IDPERSONA = :id "
				+ "AND ((tr.descripcion = 'Cotomador' or tr.idTipoRelacion = 5) "
				+ "OR ((tr.descripcion = 'Comercial' OR tr.idTipoRelacion = 4) AND (r.nombreRelacion = 'Fiador' OR r.idRelacion = 21)))")
				.setLong("id", id).list());

		// buscar los creditos de todas las personas titulares;
		objetos.addAll(bp.createQuery("SELECT o FROM Objetoi o WHERE o.persona.idpersona = :idPersona ")
				.setLong("idPersona", id).list());

		// buscar los creditos de todas las personas vinculadas;
		for (Number personaV : personaVinculada)
			objetos.addAll(bp.createQuery("SELECT o FROM Objetoi o WHERE o.id IN "
					+ "(SELECT p.idObjetoi FROM PersonaVinculada p WHERE personaTitular_IDPERSONA = :idPersonaVinculada AND personaVinculada_IDPERSONA = :idPersona AND relacion_idRelacion = :idRelacionCotomador) ORDER BY o.expediente ASC")
					.setLong("idPersonaVinculada", personaV.longValue()).setLong("idPersona", id)
					.setLong("idRelacionCotomador", idRelacionCotomador).list());

		List<ConsultaDeudaResult> ret= new ArrayList<>();
		for (Objetoi o : objetos) {
			// excluir los que est�n en original con acuerdo
			if (!o.getEstadoActual().getEstado().getNombreEstado().equals("ORIGINAL CON ACUERDO")) {
				if (("EJECUCION,PRIMER DESEMBOLSO,PENDIENTE SEGUNDO DESEMBOLSO,GESTION EXTRAJUDICIAL,CONTRATO EMITIDO,ESPERANDO DOCUMENTACION,GESTION JUDICIAL,ACUERDO INCUMPLIDO,INCOBRABLE,")
						.contains(o.getEstadoActual().getEstado().getNombreEstado() + ",")) {
					CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
					calculoDeuda.calcular();
					if(calculoDeuda.getTotal().getSaldoCuota() > 0.05)						
						ret.add(new ConsultaDeudaResult(o.getPersona().getNomb12Str(), o.getPersona().getCuil12Str(), o.getNumeroAtencion(), o.getLinea().getNombre(), o.getExpediente(), o.getEtapa(), o.getComportamientoActual(), calculoDeuda.getTotal().getSaldoCuota(), calculoDeuda.getPrimerVtoImpago()));
					
				}
			}
		}
		return ret;
	}
	
	
	

}
