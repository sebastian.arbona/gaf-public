package com.asf.cred.business.ws;



public class HectareasCultivadasResponse  {

	int vinedos;
	double superficie;
	String nombre;
	
	
	
	public HectareasCultivadasResponse( String nombre, int vinedos,
			double superficie) {
		
		this.nombre = nombre;
		this.vinedos = vinedos;
		this.superficie = superficie;
	}
	public int getVinedos() {
		return vinedos;
	}
	public void setVinedos(int vinedos) {
		this.vinedos = vinedos;
	}
	public double getSuperficie() {
		return superficie;
	}
	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
