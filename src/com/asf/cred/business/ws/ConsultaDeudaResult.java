package com.asf.cred.business.ws;

import java.util.Date;

public class ConsultaDeudaResult{
	
	
	String persona;
	String cuit;
	Long numero;
	String linea;
	String expediente;
	String etapa;
	String comportamiento;
	Double deuda;
	Date fechaVencimiento;
	
	public ConsultaDeudaResult() {
		super();
	}
	
	
	public ConsultaDeudaResult(String persona, String cuit, Long numero, String linea, String expediente,
			String etapa, String comportamiento, Double deuda, Date fechaVencimiento) {
		super();
		this.persona = persona;
		this.cuit = cuit;
		this.numero = numero;
		this.linea = linea;
		this.expediente = expediente;
		this.etapa = etapa;
		this.comportamiento = comportamiento;
		this.deuda = deuda;
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getPersona() {
		return persona;
	}
	public void setPersona(String persona) {
		this.persona = persona;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getExpediente() {
		return expediente;
	}
	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}
	public String getEtapa() {
		return etapa;
	}
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	public String getComportamiento() {
		return comportamiento;
	}
	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}
	public Double getDeuda() {
		return deuda;
	}
	public void setDeuda(Double deuda) {
		this.deuda = deuda;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	
	
	
	
}
