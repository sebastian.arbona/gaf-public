package com.asf.cred.business;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.nirven.creditos.hibernate.AuditoriaFinal;

public class EliminarAuditoriaFinal extends ConversationProcess {

	private Long idObjetoi;
	private Long idAuditoriaFinal;

	@ProcessMethod(defaultAction = true)
	public boolean eliminar() {

		AuditoriaFinal auditoria = (AuditoriaFinal) bp.getById(AuditoriaFinal.class, idAuditoriaFinal);
		idObjetoi = auditoria.getCredito() != null ? auditoria.getCredito().getId() : null;

		if (idObjetoi != null && idObjetoi != 0L) {
			bp.delete(auditoria);

			forward = "Location: " + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ "/actions/process.do?do=process&processName=AuditoriaFinalProcess" + "&process.idCredito="
					+ idObjetoi + "&process.accion=verNotif";
			return true;
		} else {
			return false;
		}

	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	protected String getDefaultForward() {
		return forward;
	}

	public Long getIdAuditoriaFinal() {
		return idAuditoriaFinal;
	}

	public void setIdAuditoriaFinal(Long idAuditoriaFinal) {
		this.idAuditoriaFinal = idAuditoriaFinal;
	}

}
