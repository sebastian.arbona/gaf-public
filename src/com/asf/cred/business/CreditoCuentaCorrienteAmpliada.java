package com.asf.cred.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import com.asf.gaf.hibernate.Ejercicio;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.IReportBean;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.ReportResult;
import com.asf.util.SQLHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;

public class CreditoCuentaCorrienteAmpliada extends ConversationProcess implements IReportBean {

	private static final int MOSTRAR_POR_FECHA = 1;
	private static final int MOSTRAR_POR_CUOTA = 2;
	private static final int FECHA_VENCIMIENTO = 1;
	private static final int FECHA_PROCESO = 2;
	private static final int FECHA_EXTRACTO = 3;

	private static String sqlFecha;
	private static String sqlCuota;

	private Date hastaFecha;
	private Date desdeFecha;
	private int fechas = 0;
	private int mostrar = MOSTRAR_POR_FECHA;

	@Save
	private List<CuentaCorrienteAmpliadaBean> beans;
	@Save
	private List<SaldoCapitalBean> resumenBeans;

	private boolean esDolares;
	private boolean reducida;
	private String contextoGAF;
	private String numeroAtencion;
	private ReportResult reporte;
	private Boleto boleto;
	private Pagos pago;
	private Long idCaratula;
	@Save
	private Long idBoleto;
	private boolean ocultar;
	private String areaCobro;
	private String esquemaGAF;

	private Long idObjetoi;
	@Save
	private SaldoCapitalBean corriente;
	@Save
	private SaldoCapitalBean noCorriente;
	@Save
	private Objetoi credito;
	@Save
	private CuentaCorrienteAmpliadaBean bean;

	private boolean minuta;

	private boolean cer;

	private String forward = "CreditoCuentaCorrienteAmpliada";
	private SaldoCapitalBean resumenExigible;

	private static String getSqlFecha() {
		if (sqlFecha == null) {
			try {
				sqlFecha = IOUtils.toString(
						CreditoCuentaCorrienteAmpliada.class.getResourceAsStream("ctacte-ampliada-fecha.sql"));
			} catch (IOException e) {
				System.err.println("Error al cargar SQL cuenta corriente ampliada por fecha");
			}
		}
		return sqlFecha;
	}

	private static String getSqlCuota() {
		if (sqlCuota == null) {
			try {
				sqlCuota = IOUtils.toString(
						CreditoCuentaCorrienteAmpliada.class.getResourceAsStream("ctacte-ampliada-cuota.sql"));
			} catch (IOException e) {
				System.err.println("Error al cargar SQL cuenta corriente ampliada por cuota");
			}
		}
		return sqlCuota;
	}

	public CreditoCuentaCorrienteAmpliada() {
		contextoGAF = DirectorHelper.getString("URL.GAF");
		boleto = new Boleto();
		pago = new Pagos();
		esquemaGAF = DirectorHelper.getString("SGAF");
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		beans = new ArrayList<CuentaCorrienteAmpliadaBean>();
		credito = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		numeroAtencion = credito.getNumeroAtencionStr();
		resumenBeans = new ArrayList<SaldoCapitalBean>();

		if (credito.getLinea().getMoneda().getDenominacion().equalsIgnoreCase("Dolar")) {
			esDolares = true;
		}

		boolean acuerdoPago = revisarAcuerdoPago();
		cer = credito.getLinea().isLineaCer();

		List<Object[]> result = null;
		Number n = null, ndolar = null;
		if (mostrar == MOSTRAR_POR_FECHA) {
			String s = getSqlFecha();

			s = s.replace("@IDOBJETOI", "" + getIdObjetoi());
			s = s.replace("@DESEMBOLSO_EXPR1", (!acuerdoPago ? "'Desembolso' " : "'Deuda consolidada' "));
			s = s.replace("@DESEMBOLSO_EXPR2", (!acuerdoPago ? "cc.detalle " : "'Deuda consolidada' "));

			String[] columnas = { SQLHelper.getTO_CHARFunction("movimientoCtacte", null), "'-'",
					SQLHelper.getTO_CHARFunction("periodoCtacte", null), "'-'",
					SQLHelper.getTO_CHARFunction("itemCtacte", null), "'-'",
					SQLHelper.getTO_CHARFunction("verificadorCtacte", null) };
			s = s.replace("@EXPRESION_IDCTACTE", SQLHelper.getConcatFunction(columnas));

			if (fechas == FECHA_VENCIMIENTO || fechas == FECHA_EXTRACTO) {
				if (getDesdeFecha() != null && getHastaFecha() != null) {
					s = s.replace("@FECHAS_WHERE_EXPR",
							"and  cc.fechaGeneracion >= '" + new java.sql.Date(getDesdeFecha().getTime())
									+ "' and cc.fechaGeneracion <= '" + new java.sql.Date(getHastaFecha().getTime())
									+ "' ");
				}
			} else if (fechas == FECHA_PROCESO) {
				if (getDesdeFecha() != null && getHastaFecha() != null) {
					s = s.replace("@FECHAS_WHERE_EXPR",
							"and  cc.fechaProceso >= '" + new java.sql.Date(getDesdeFecha().getTime())
									+ "' and cc.fechaProceso <= '" + new java.sql.Date(getHastaFecha().getTime())
									+ "' ");
				}
			} else {
				s = s.replace("@FECHAS_WHERE_EXPR", "");
			}
			SQLQuery sql = bp.createSQLQuery(s);
			// sql.setLong(0, getIdObjetoi());
			if (fechas == FECHA_VENCIMIENTO || fechas == FECHA_EXTRACTO) {
				n = (Number) bp
						.createSQLQuery(
								"SELECT SUM(case tipomov_id when 1 then -importe when 2 then importe end) from Ctacte "
										+ "where objetoi_id=? AND fechaGeneracion< ?")
						.setParameter(0, getObjetoi().getId()).setParameter(1, getDesdeFecha()).uniqueResult();
				if (esDolares) {
					ndolar = (Number) bp.createSQLQuery("SELECT SUM(CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN "
							+ "	CASE WHEN cc.tipomov_id = 2 THEN cc.debePesos ELSE cc.haberPesos END ELSE "
							+ "	CASE WHEN cc.tipomov_id = 2 THEN cc.importe * cc.cotizaMov ELSE -cc.importe * cc.cotizaMov END "
							+ "END) FROM Ctacte cc where objetoi_id=? AND fechaGeneracion < ?")
							.setParameter(0, getObjetoi().getId()).setParameter(1, getDesdeFecha()).uniqueResult();
				}
			} else if (fechas == FECHA_PROCESO) {
				// sql.setParameter(1, getDesdeFecha());
				// sql.setParameter(2, getHastaFecha());

				n = (Number) bp
						.createSQLQuery(
								"SELECT SUM(case tipomov_id when 1 then -importe when 2 then importe end) from Ctacte "
										+ "where objetoi_id=? AND fechaProceso<?")
						.setParameter(0, getObjetoi().getId()).setParameter(1, getDesdeFecha()).uniqueResult();

				if (esDolares) {
					ndolar = (Number) bp.createSQLQuery("SELECT SUM(CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN "
							+ "	CASE WHEN cc.tipomov_id = 2 THEN cc.debePesos ELSE cc.haberPesos END ELSE "
							+ "	CASE WHEN cc.tipomov_id = 2 THEN cc.importe * cc.cotizaMov ELSE -cc.importe * cc.cotizaMov END "
							+ "END) FROM Ctacte cc where objetoi_id=? AND fechaProceso < ?")
							.setParameter(0, getObjetoi().getId()).setParameter(1, getDesdeFecha()).uniqueResult();
				}
			}
			result = sql.list();

		} else if (mostrar == MOSTRAR_POR_CUOTA) {
			ocultar = true;
			desdeFecha = null;
			hastaFecha = null;

			String s = getSqlCuota();

			s = s.replace("@IDOBJETOI", "" + getIdObjetoi());

			String[] columnas = { SQLHelper.getTO_CHARFunction("movimientoCtacte", null), "'-'",
					SQLHelper.getTO_CHARFunction("periodoCtacte", null), "'-'",
					SQLHelper.getTO_CHARFunction("itemCtacte", null), "'-'",
					SQLHelper.getTO_CHARFunction("verificadorCtacte", null) };
			s = s.replace("@EXPRESION_IDCTACTE", SQLHelper.getConcatFunction(columnas));

			SQLQuery sql = bp.createSQLQuery(s);

			result = sql.list();
		}

		int anioActual = Ejercicio.getEjercicioActual().intValue();

		double totalDebe = 0, totalHaber = 0, totalCapital = 0, totalCompensatorio = 0, totalMoratorio = 0,
				totalPunitorio = 0, totalGastos = 0, saldo = 0, saldoPesos = 0;

		if (n != null) {
			saldo = n.doubleValue();
		}
		if (ndolar != null) {
			saldoPesos = ndolar.doubleValue();
		}

		boolean cuota = mostrar == MOSTRAR_POR_CUOTA;
		int numeroCuota = 0;

		List<Desembolso> desembolsos = null;
		int numeroDesembolso = 0;

		if (!cuota) { // cuando es por fecha, buscar los desembolsos
			desembolsos = bp.getNamedQuery("Desembolso.findByCredito").setParameter("idCredito", getIdObjetoi()).list();
		}

		for (Object[] fila : result) {
			bean = new CuentaCorrienteAmpliadaBean(fila, cuota);

			CtacteKey ck = bean.getCtacteKey();

			Ctacte cc = (Ctacte) bp.getById(Ctacte.class, ck);
			if (cc != null) {
				Long asiento = cc.getNroAsiento();
				bean.setAsiento(asiento);
			}

			if (!cuota) {
				saldo = saldo + bean.getDebe() - bean.getHaber();

				if (esDolares) {
					saldoPesos = saldoPesos + bean.getDebePesos() - bean.getHaberPesos();
				}

				// si es un desembolso, buscar la orden de pago GAF asociada
				if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Desembolso")) {
					if (numeroDesembolso < desembolsos.size()) {
						// saco el desembolso que corresponde, porque vienen ordenados
						Desembolso desembolso = desembolsos.get(numeroDesembolso++);
						bean.setIdOrdenPago(desembolso.getIdOrdepago());
						bean.setNumeroOrdenPago(
								desembolso.getNroOrden() != null ? desembolso.getNroOrden().longValue() : 0);
						bean.setEjercicioOrdenPago(desembolso.getOrdepago_ejercicio() != null
								? desembolso.getOrdepago_ejercicio().longValue()
								: 0);
						bean.setNumeroBoleto(desembolso.getNumeroRecibo() != null
								? new Long(desembolso.getNumeroRecibo().longValue())
								: null);
					}
				}

				if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Pago Gastos")) {
					if (numeroDesembolso >= 1) {
						Desembolso desembolso = desembolsos.get(numeroDesembolso - 1);
						bean.setIdOrdenPago(desembolso.getIdOrdepago());
						bean.setNumeroOrdenPago(
								desembolso.getNroOrden() != null ? desembolso.getNroOrden().longValue() : 0);
						bean.setEjercicioOrdenPago(desembolso.getOrdepago_ejercicio() != null
								? desembolso.getOrdepago_ejercicio().longValue()
								: 0);
						bean.setNumeroBoleto(desembolso.getNumeroRecibo() != null
								? new Long(desembolso.getNumeroRecibo().longValue())
								: null);
					}
				}
			} else {
				// reseteo cuando cambia de cuota
				if (numeroCuota < bean.getCuota()) {
					numeroCuota = bean.getCuota();
					saldo = 0;
				}
				saldo = saldo + bean.getCapital() + bean.getCompensatorio() + bean.getMoratorio() + bean.getPunitorio()
						+ bean.getGastos();
				if (saldo <= 0.00 && saldo >= -0.009) {
					saldo = 0;
				}
			}
			// acumular totales
			totalDebe += bean.getDebe();
			totalHaber += bean.getHaber();
			totalCapital += bean.getCapital();
			totalCompensatorio += bean.getCompensatorio();
			totalMoratorio += bean.getMoratorio();
			totalPunitorio += bean.getPunitorio();
			totalGastos += bean.getGastos();
			bean.getDebePesos();
			bean.getHaberPesos();

			bean.setSaldo(saldo);
			bean.setSaldoPesos(saldoPesos);
			beans.add(bean);
		}

		CuentaCorrienteAmpliadaBean totalBean = new CuentaCorrienteAmpliadaBean();
		totalBean.setDetalle("Total");
		totalBean.setCapital(totalCapital);
		totalBean.setCompensatorio(totalCompensatorio);
		totalBean.setMoratorio(totalMoratorio);
		totalBean.setPunitorio(totalPunitorio);
		totalBean.setGastos(totalGastos);
		totalBean.setDebe(totalDebe);
		totalBean.setHaber(totalHaber);
		totalBean.setSaldo(totalCapital + totalCompensatorio + totalMoratorio + totalPunitorio + totalGastos);

		if (!beans.isEmpty() && mostrar == MOSTRAR_POR_FECHA) {
			CuentaCorrienteAmpliadaBean ultimo = beans.get(beans.size() - 1);
			totalBean.setSaldo(ultimo.getSaldo());
			totalBean.setSaldoPesos(ultimo.getSaldoPesos());
		}

		beans.add(totalBean);

		if (!cuota) {
			// resumen de saldo
			String sqlResumen = "select sum(importe) as monto, sum(importeCotiz) as montoCotiz, concepto, corriente from (select case when cc.tipomov_id = 2 then cc.importe "
					+ "else -cc.importe end as importe, case when cc.DTYPE = 'CtaCteAjuste' then cc.debePesos - cc.haberPesos "
					+ "else case when cc.tipomov_id = 2 then cc.importe * cc.cotizaMov else -cc.importe * cc.cotizaMov end end as importeCotiz, case "
					+ "when con.concepto_concepto = 'cap' then 'Capital' "
					+ "when con.concepto_concepto = 'com' or con.concepto_concepto = 'bon' or con.concepto_concepto = 'pun' or con.concepto_concepto = 'mor' then 'Interes' "
					+ "else 'Otros' end as concepto, case when c.fechaVencimiento <= ? then 'Corriente'	else 'No Corriente' end as corriente from Ctacte cc "
					+ "inner join Cuota c on c.id = cc.cuota_id inner join Concepto con on con.id = cc.asociado_id where cc.objetoi_id = ? "
					+ (fechas == FECHA_PROCESO ? "and cc.fechaProceso <= ?"
							: (fechas == FECHA_VENCIMIENTO || fechas == FECHA_EXTRACTO ? "and cc.fechaGeneracion <= ?"
									: ""))
					+ ") t group by concepto, corriente order by corriente, concepto";

			Calendar c = Calendar.getInstance();
			c.set(Calendar.YEAR, anioActual);
			c.set(Calendar.MONTH, Calendar.DECEMBER);
			c.set(Calendar.DATE, 31);
			c.set(Calendar.HOUR_OF_DAY, 23);
			c.set(Calendar.MINUTE, 59);
			c.set(Calendar.SECOND, 59);

			Calendar hasta = Calendar.getInstance();
			hasta.setTime(getHastaFecha() != null ? getHastaFecha() : new Date());
			hasta.set(Calendar.HOUR_OF_DAY, 23);
			hasta.set(Calendar.MINUTE, 59);
			hasta.set(Calendar.SECOND, 59);

			Query q = bp.createSQLQuery(sqlResumen).setDate(0, c.getTime()).setLong(1, getIdObjetoi());
			if (fechas != 0) {
				q.setTimestamp(2, hasta.getTime());
			}
			List<Object[]> valoresResumen = q.list();

			corriente = new SaldoCapitalBean("Corriente", 0, 0, 0);
			noCorriente = new SaldoCapitalBean("No Corriente", 0, 0, 0);

			Double importe;

			for (Object[] row : valoresResumen) {

				if (row[3].equals("Corriente")) {
					if (row[2].equals("Capital")) {
						importe = row[0] != null ? ((Double) row[0]).doubleValue() : 0D;
						importe = Math.round(importe * 100D) / 100D;
						corriente.addCapital(importe);
						corriente.addCapitalPesos(row[1] != null ? ((Number) row[1]).doubleValue() : 0.0);

					} else if (row[2].equals("Interes")) {
						importe = row[0] != null ? ((Number) row[0]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;

						corriente.addInteres(importe);
						importe = row[1] != null ? ((Number) row[1]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						corriente.addInteresPesos(importe);

					} else {
						importe = row[0] != null ? ((Number) row[0]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						corriente.addOtros(importe);
						importe = row[1] != null ? ((Number) row[1]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						corriente.addOtrosPesos(importe);
					}
				} else {
					if (row[2].equals("Capital")) {
						importe = row[0] != null ? ((Number) row[0]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						noCorriente.addCapital(importe);
						importe = row[1] != null ? ((Number) row[1]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						noCorriente.addCapitalPesos(importe);
					} else if (row[2].equals("Interes")) {
						importe = row[0] != null ? ((Number) row[0]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						noCorriente.addInteres(importe);
						importe = row[1] != null ? ((Number) row[1]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						noCorriente.addInteresPesos(importe);
					} else {
						importe = row[0] != null ? ((Number) row[0]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						noCorriente.addOtros(importe);
						importe = row[1] != null ? ((Number) row[1]).doubleValue() : 0.0;
						importe = Math.round(importe * 100D) / 100D;
						noCorriente.addOtrosPesos(importe);
					}
				}
			}

			SaldoCapitalBean totalResumen = new SaldoCapitalBean("Total",
					corriente.getCapital() + noCorriente.getCapital(),
					corriente.getInteres() + noCorriente.getInteres(), corriente.getOtros() + noCorriente.getOtros());
			totalResumen.addCapitalPesos(corriente.getPesos().getCapital() + noCorriente.getPesos().getCapital());
			totalResumen.addInteresPesos(corriente.getPesos().getInteres() + noCorriente.getPesos().getInteres());
			totalResumen.addOtrosPesos(corriente.getPesos().getOtros() + noCorriente.getPesos().getOtros());
			resumenBeans.add(corriente);
			resumenBeans.add(noCorriente);
			resumenBeans.add(totalResumen);

			// resumen de exigibilidad
			CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(getIdObjetoi(),
					hastaFecha != null ? hastaFecha : new Date());
			calculo.calcular();
			List<BeanCtaCte> vencidas = calculo.getVistaVencida();
			resumenExigible = new SaldoCapitalBean("Deuda Exigible",
					CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_CAPITAL),
					CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_COMPENSATORIO)
							- CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_BONIFICACION),
					CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_MORATORIO)
							+ CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_PUNITORIO)
							+ CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_MULTAS)
							+ CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_GASTOS)
							+ CreditoCalculoDeuda.getSaldo(vencidas, Concepto.CONCEPTO_GASTOS_A_RECUPERAR));

		}
		return true;

	}

	private boolean revisarAcuerdoPago() {
		String idLineaAcuerdo = DirectorHelper.getString("linea.acuerdoPago");
		if (idLineaAcuerdo == null) {
			return false;
		}

		String[] ids = idLineaAcuerdo.split(",");
		for (String id : ids) {
			if (credito.getLinea_id().toString().equals(id)) {
				return true;
			}
		}

		return false;
	}

	@ProcessMethod
	public boolean buscarPorCuotaFecha() {
		if (mostrar == MOSTRAR_POR_CUOTA) {
			ocultar = true;
			desdeFecha = null;
			hastaFecha = null;
			buscar();
		} else {
			cer = credito.getLinea().isLineaCer();
			ocultar = false;
		}
		return true;
	}

	@ProcessMethod
	@SuppressWarnings("unchecked")
	public boolean imprimirBoletos() {
		try {
			List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b WHERE b.id.boleto.id=:boleto")
					.setParameter("boleto", idBoleto).list();
			Double capital = 0.0;
			Double compensatorio = 0.0;
			Double punitorio = 0.0;
			Double moratorio = 0.0;
			for (Bolcon bolcon : bolcons) {
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
					capital = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
					compensatorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
					punitorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
					moratorio = bolcon.getImporte();
				}
			}
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			setObjetoi((Objetoi) bp.getById(Objetoi.class, getIdObjetoi()));
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletosFactura.jasper");
			reporte.setParams("idObjetoi=" + getObjetoi().getId() + ";CAPITAL=" + capital + ";idPersona="
					+ getObjetoi().getPersona_id() + ";MORATORIO=" + moratorio + ";PUNITORIO=" + punitorio
					+ ";COMPENSATORIO=" + compensatorio + ";VENCIMIENTO=" + boleto.getFechaVencimientoStr()
					+ ";EMISION=" + boleto.getFechaEmisionStr() + ";MONEDA="
					+ getObjetoi().getLinea().getMoneda().getDenominacion() + ";SIMBOLO="
					+ getObjetoi().getLinea().getMoneda().getSimbolo() + ";NUMERO_BOLETO=" + boleto.getNumeroBoleto()
					+ ";ID_BOLETO=" + boleto.getId() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";SCHEMA="
					+ BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			href += "&process.mostrar=" + mostrar;
			reporte.setHref(href);
		} catch (Exception e) {
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletosFactura"));
		}

		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}
		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean imprimirCtaCteAmpliada() {
		try {
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			setObjetoi((Objetoi) bp.getById(Objetoi.class, getObjetoi().getId()));

			String comportamiento = new CreditoHandler().getAreaResponsable(getObjetoi());
			cer = credito.getLinea().isLineaCer();
			String marcaagua = DirectorHelper.getString("marca.agua");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			if (cer) {
				reporte.setReportName("cuentaCorrienteAmpliadaCER.jasper");
			} else {
				reporte.setReportName("cuentaCorrienteAmpliada.jasper");
			}

			String parametro = "idObjetoi=" + getObjetoi().getId() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";MONEDA=" + getObjetoi().getLinea().getMoneda().getDenominacion() + ";AREA=" + comportamiento
					+ ";TITULAR=" + getObjetoi().getPersona().getNomb12() + ";CUIT="
					+ getObjetoi().getPersona().getCuil12Str() + ";LINEA=" + getObjetoi().getLinea().getNombre()
					+ ";MONEDA=" + getObjetoi().getLinea().getMoneda().getAbreviatura() + ";PROYECTO="
					+ getObjetoi().getNumeroAtencion().toString() + ";COMPORTAMIENTO="
					+ getObjetoi().getComportamientoActual() + ";ESTADO="
					+ getObjetoi().getEstadoActual().getEstado().getNombreEstado() + ";SEGURO="
					+ getObjetoi().getEstadoSeguro() + ";EXPEDIENTE=" + getObjetoi().getExpediente() + ";ETAPA="
					+ getObjetoi().getEstadoActual().getEstado().getNombreEstado() + ";CapCorriente="
					+ corriente.getCapital() + ";IntCorriente=" + corriente.getInteres() + ";OtrosCorriente="
					+ corriente.getOtros() + ";NoCorrienteCap=" + noCorriente.getCapital() + ";NoCorrienteInt="
					+ noCorriente.getInteres() + ";NoCorrienteOtros=" + noCorriente.getOtros() + ";USUARIO="
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";FECHA="
					+ BusinessPersistance.getSchema() + ";FECHA_DESDE=" + DateHelper.getString(desdeFecha)
					+ ";FECHA_HASTA=" + DateHelper.getString(hastaFecha) + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";BEANS=objetoiId="
					+ getIdObjetoi() + ";FECHA_DESDE=" + DateHelper.getString(desdeFecha) + ";FECHA_HASTA="
					+ DateHelper.getString(hastaFecha) + ";fechas=" + fechas + ";MARCAAGUA=" + marcaagua;

			reporte.setParams(parametro);
			reporte.setClassBeanName(getClass().getName());
			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada";
			href += "&process.idObjetoi=" + getIdObjetoi();
			href += "&process.mostrar=" + mostrar;
			reporte.setHref(href);

		} catch (Exception e) {
			errors.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "cuentaCorrienteAmpliada"));
		}
		if (errors.isEmpty()) {
			forward = "ReportesProcess2";
		}
		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean imprimirCtaCteContable() {
		try {
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			buscarObjetoi();
			setObjetoi((Objetoi) bp.getById(Objetoi.class, getObjetoi().getId()));
			String comportamiento = new CreditoHandler().getAreaResponsable(getObjetoi());
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("cuentaCorrienteContable.jasper");
			reporte.setParams("idObjetoi=" + getObjetoi().getId() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";MONEDA=" + getObjetoi().getLinea().getMoneda().getDenominacion() + ";TITULAR="
					+ getObjetoi().getPersona().getNomb12() + ";CUIT=" + getObjetoi().getPersona().getCuil12Str()
					+ ";LINEA=" + getObjetoi().getLinea().getNombre() + ";AREA=" + comportamiento + ";MONEDA="
					+ getObjetoi().getLinea().getMoneda().getAbreviatura() + ";PROYECTO="
					+ getObjetoi().getNumeroAtencion().toString() + ";COMPORTAMIENTO="
					+ getObjetoi().getComportamientoActual() + ";SEGURO=" + getObjetoi().getEstadoSeguro()
					+ ";EXPEDIENTE=" + getObjetoi().getExpediente() + ";ETAPA="
					+ getObjetoi().getEstadoActual().getEstado().getNombreEstado() + ";ES_DOLARES="
					+ getObjetoi().getLinea().isDolares() + ";USUARIO="
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";FECHA_DESDE="
					+ DateHelper.getString(desdeFecha) + ";FECHA_HASTA=" + DateHelper.getString(hastaFecha) + ";FECHA="
					+ BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";BEANS=objetoiId="
					+ getIdObjetoi() + ";FECHA_DESDE=" + DateHelper.getString(desdeFecha) + ";FECHA_HASTA="
					+ DateHelper.getString(hastaFecha) + ";fechas=" + fechas);
			reporte.setClassBeanName(getClass().getName());
		} catch (Exception e) {
			errors.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "cuentaCorrienteContable"));
		}
		if (errors.isEmpty()) {
			forward = "ReporteManual";
		}
		return errors.isEmpty();
	}

	@ProcessMethod
	@SuppressWarnings("unchecked")
	public boolean imprimirPago() {
		pago = (Pagos) bp
				.createSQLQuery("SELECT * FROM Pagos p WHERE p.boleto_id = :boleto_id AND p.caratula_id = :caratula_id")
				.addEntity(Pagos.class).setLong("boleto_id", idBoleto).setLong("caratula_id", getIdCaratula())
				.setMaxResults(1).uniqueResult();
		try {
			buscarObjetoi();
			Long idBoleto;
			if (pago.getRecibo() != null) {
				idBoleto = pago.getRecibo().getId();
			} else {
				idBoleto = pago.getId().getBoleto().getId();
			}
			Boleto b = (Boleto) bp.getById(Boleto.class, idBoleto);
			String expediente = b.getObjetoi().getExpediente();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			String consultaBasica = "SELECT d.calleNom, d.numero, d.manzana,  b.DESC_BRR, l.CP, l.NOMBRE, d.departamentoNom, d.lote, pr.DETA_08 "
					+ "FROM Objetoi ob LEFT JOIN Persona pe on ob.persona_IDPERSONA = pe.IDPERSONA "
					+ "LEFT JOIN DomicilioObjetoi do on do.objetoi_id = ob.id  "
					+ "LEFT JOIN Domicilio d on do.domicilio_id = d.id "
					+ "LEFT JOIN LOCALIDAD l on l.IDLOCALIDAD = d.localidad_IDLOCALIDAD "
					+ "LEFT JOIN BARRIO b on b.CODI_BRR = d.barrio_CODI_BRR "
					+ "LEFT JOIN PROVIN pr on pr.CODI_08 = d.provincia_CODI_08 WHERE ob.id = :objetoi_id";
			String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> domicilios = bp.createSQLQuery(consultaLimitada).setLong("objetoi_id", getObjetoi().getId())
					.list();
			Object[] domicilio;
			if (!domicilios.isEmpty()) {
				domicilio = domicilios.get(0);
			} else {
				domicilio = null;
			}
			String calleNom = domicilio == null || domicilio[0] == null ? null : domicilio[0].toString();
			String numero = domicilio == null || domicilio[1] == null ? null : domicilio[1].toString();
			String manzana = domicilio == null || domicilio[2] == null ? null : domicilio[2].toString();
			String barrioNom = domicilio == null || domicilio[3] == null ? "" : domicilio[3].toString();
			String cp = domicilio == null || domicilio[4] == null ? null : domicilio[4].toString();
			String nombre = domicilio == null || domicilio[5] == null ? null : domicilio[5].toString();
			String departamentoNom = domicilio == null || domicilio[6] == null ? null : domicilio[6].toString();
			String lote = domicilio == null || domicilio[7] == null ? null : domicilio[7].toString();
			String provin = domicilio == null || domicilio[8] == null ? null : domicilio[8].toString();

			Boleto boleto;
			if (pago.getBoleto() != null)
				boleto = pago.getBoleto();
			else
				boleto = b;

			int anioActual = Ejercicio.getEjercicioActual().intValue();

			String medioPago = (String) bp.createQuery(
					"SELECT c.mediopago.denominacion FROM Bolepago bp JOIN bp.id.cobropago c WHERE bp.id.boleto = :boleto")
					.setEntity("boleto", boleto).uniqueResult();
			consultaBasica = "SELECT b.tiponrocuenta, CASE WHEN idmoneda = 1 THEN 'Pesos' WHEN idmoneda = 2 THEN 'D�lares' "
					+ "WHEN idmoneda = 3 THEN 'Euros' ELSE CAST(idmoneda AS varchar) END AS 'moneda', "
					+ "CASE WHEN tipoCta = 'CC' THEN 'Cuenta Corriente' WHEN tipoCta = 'CA' THEN 'Caja de Ahorro' "
					+ "ELSE tipoCta END AS 'tipoCta' FROM " + this.esquemaGAF + ".ParametrizacionBancos p INNER JOIN "
					+ this.esquemaGAF
					+ ".Bancue b ON b.idBancue = p.idBancue WHERE p.codi_Ba = :codiBa AND p.ejercicio =:ejercicio";
			consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			List<Object[]> listado = bp.createSQLQuery(consultaLimitada)
					.setParameter("codiBa", pago.getId().getCaratula().getBanco().getCodiBa())
					.setParameter("ejercicio", anioActual).list();
			String tipoNumeroCuenta = "";
			String tipoCta = "";
			if (!listado.isEmpty()) {
				Object[] object = listado.get(0);
				tipoNumeroCuenta = (String) object[0];
				tipoCta = (String) object[2];
			}

			boolean cambioMoneda = false;
			Long idMonedaCredito = b.getObjetoi().getMoneda_id() != null ? b.getObjetoi().getMoneda_id()
					: b.getObjetoi().getLinea().getMoneda_id();
			if (idMonedaCredito == null) {
				idMonedaCredito = 1L;
			}
			if (!idMonedaCredito.equals(1L)) {
				// el credito no esta en pesos
				cambioMoneda = pago.getMoneda() != null && !pago.getMoneda().getId().equals(idMonedaCredito);
			}

			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("pago.jasper");
			reporte.setParams("PROYECTO=" + getObjetoi().getNumeroAtencionStr() + ";TITULAR="
					+ getObjetoi().getPersona().getNomb12() + ";BOLETO_ID=" + idBoleto + ";CUIT="
					+ getObjetoi().getPersona().getCuil12Str() + ";idObjetoi=" + b.getObjetoi().getId()
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";EXPEDIENTE=" + expediente + ";MONEDA="
					+ (pago.getMoneda() != null ? pago.getMoneda().getDenominacion()
							: b.getObjetoi().getLinea().getMoneda().getDenominacion())
					+ ";numeroBoleto=" + b.getNumeroBoleto() + ";medioPago=" + medioPago + ";FECHA_EMISION="
					+ b.getFechaEmision().getTime() + ";calleNom=" + calleNom + ";numero=" + numero + ";manzana="
					+ manzana + ";barrioNom=" + barrioNom + ";cp=" + cp + ";nombre=" + nombre + ";departamentoNom="
					+ departamentoNom + ";lote=" + lote + ";provincia=" + provin + ";TIPO_NUMERO_CUENTA="
					+ tipoNumeroCuenta + ";TIPO_CTA=" + tipoCta + ";BANCO="
					+ pago.getId().getCaratula().getBanco().getProveedor().getFantasia()
					+ (cambioMoneda && pago.getMontoOriginal() != null
							? ";montoOriginal=" + DoubleHelper.getString(pago.getMontoOriginal())
							: "")
					+ (cambioMoneda && pago.getCotizacion() != null
							? ";cotizacion=" + DoubleHelper.getString(pago.getCotizacion())
							: "")
					+ (cambioMoneda && pago.getMoneda() != null
							? ";monedaOriginal=" + pago.getMoneda().getDenominacion()
							: "")
					+ ";SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			href += "&process.mostrar=" + mostrar;
			reporte.setHref(href);

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("objetoiPagosProcess",
					getObjetoi());
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "pago"));
		}

		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}
		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean imprimirManualDebito() {
		try {
			buscarObjetoi();
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			Object[] datosCtaCte = (Object[]) bp.createQuery(
					"select cc.comprobante, cc.fechaGeneracion, cc.fechaProceso from Ctacte cc where cc.boleto.id = :idBoleto")
					.setParameter("idBoleto", boleto.getId()).setMaxResults(1).uniqueResult();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("notaDebitoManual.jasper");
			reporte.setParams("OBJETOI_ID=" + getIdObjetoi() + ";OBSERVACIONES="
					+ (datosCtaCte != null && datosCtaCte[0] != null ? datosCtaCte[0] : "") // comprobante
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";numeroBoleto="
					+ boleto.getPeriodoBoleto().toString().concat(" - ").concat(boleto.getNumeroBoleto().toString())
					+ ";idBoleto=" + boleto.getId() + ";NOMBRE_COMPROBANTE="
					+ (!minuta ? "NOTA DE D�BITO" : "MINUTA D�BITO") + ";fechaEmisionBoleto="
					+ (datosCtaCte != null && datosCtaCte[2] != null ? DateHelper.getString((Date) datosCtaCte[2]) : "") // fechaProceso
					+ ";fechaAplicacion="
					+ (datosCtaCte != null && datosCtaCte[1] != null ? DateHelper.getString((Date) datosCtaCte[1]) : "") // fechaGeneracion
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			href += "&process.mostrar=" + mostrar;
			reporte.setHref(href);
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "notaDebitoManual"));
		}

		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}
		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean imprimirManualCredito() {
		try {
			buscarObjetoi();
			boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
			Object[] datosCtaCte = (Object[]) bp.createQuery(
					"select cc.comprobante, cc.fechaGeneracion, cc.fechaProceso from Ctacte cc where cc.boleto.id = :idBoleto")
					.setParameter("idBoleto", boleto.getId()).setMaxResults(1).uniqueResult();
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("notaCreditoManual.jasper");
			reporte.setParams("OBJETOI_ID=" + getIdObjetoi() + ";OBSERVACIONES="
					+ (datosCtaCte != null && datosCtaCte[0] != null ? datosCtaCte[0] : "") // comprobante
					+ ";RELACION_COTOMADOR_ID=" + relacionCotomadorId + ";numeroBoleto="
					+ boleto.getPeriodoBoleto().toString().concat(" - ").concat(boleto.getNumeroBoleto().toString())
					+ ";idBoleto=" + boleto.getId() + ";NOMBRE_COMPROBANTE="
					+ (!minuta ? "NOTA DE CR�DITO" : "MINUTA CR�DITO") + ";fechaEmisionBoleto="
					+ (datosCtaCte != null && datosCtaCte[2] != null ? DateHelper.getString((Date) datosCtaCte[2]) : "") // fechaProceso
					+ ";fechaAplicacion="
					+ (datosCtaCte != null && datosCtaCte[1] != null ? DateHelper.getString((Date) datosCtaCte[1]) : "") // fechaGeneracion
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoCuentaCorrienteAmpliada";
			href += "&process.idObjetoi=" + getObjetoi().getId();
			href += "&process.personaId=" + getObjetoi().getPersona_id();
			href += "&process.mostrar=" + mostrar;
			reporte.setHref(href);
		} catch (Exception e) {
			e.printStackTrace();
			errors.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "notaCreditoManual"));
		}
		if (errors.isEmpty()) {
			forward = "ReportesProcessFrame";
		}
		return errors.isEmpty();
	}

	protected void buscarObjetoi() throws HibernateException, Exception {
		if (idObjetoi != null && idObjetoi != 0) {
			credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		}
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public List<CuentaCorrienteAmpliadaBean> getBeans() {
		return beans;
	}

	public int getMostrar() {
		return mostrar;
	}

	public void setMostrar(int mostrar) {
		this.mostrar = mostrar;
	}

	public List<SaldoCapitalBean> getResumenBeans() {
		return resumenBeans;
	}

	public String getContextoGAF() {
		return contextoGAF;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}

	public Long getIdBoleto() {
		return idBoleto;
	}

	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Objetoi getObjetoi() {
		return credito;
	}

	public void setObjetoi(Objetoi credito) {
		this.credito = credito;
	}

	@Override
	protected String getDefaultForward() {
		return forward;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Collection getCollection(HashMap arg0) {
		this.idObjetoi = new Long(arg0.get("objetoiId").toString());
		setDesdeFechaStr(arg0.get("FECHA_DESDE").toString());
		setHastaFechaStr(arg0.get("FECHA_HASTA").toString());
		this.fechas = Integer.parseInt(arg0.get("fechas").toString());
		try {
			buscarObjetoi();
			buscar();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return beans;
	}

	public Date getHastaFecha() {
		return hastaFecha;
	}

	public void setHastaFecha(Date hastaFecha) {
		this.hastaFecha = hastaFecha;
		if (this.hastaFecha != null) {
			Calendar c = Calendar.getInstance();
			c.setTime(this.hastaFecha);
			c.set(Calendar.HOUR_OF_DAY, 23);
			c.set(Calendar.MINUTE, 59);
			c.set(Calendar.SECOND, 59);
			this.hastaFecha = c.getTime();
		}
	}

	public String getHastaFechaStr() {
		return DateHelper.getString(hastaFecha);
	}

	public void setHastaFechaStr(String f) {
		setHastaFecha(DateHelper.getDate(f));
	}

	public Date getDesdeFecha() {
		return desdeFecha;
	}

	public void setDesdeFecha(Date desdeFecha) {
		this.desdeFecha = desdeFecha;
	}

	public String getDesdeFechaStr() {
		return DateHelper.getString(desdeFecha);
	}

	public void setDesdeFechaStr(String f) {
		this.desdeFecha = DateHelper.getDate(f);
	}

	public int getFechas() {
		return fechas;
	}

	public void setFechas(int fechas) {
		this.fechas = fechas;
	}

	public String getCriterioStr() {
		switch (fechas) {
		case 1:
			return "Vencimiento";
		case 2:
			return "Proceso";
		case 3:
			return "Extracto";
		default:
			return "";
		}
	}

	public void setOcultar(boolean ocultar) {
		this.ocultar = ocultar;
	}

	public boolean isOcultar() {
		return ocultar;
	}

	public String getAreaCobro() {
		CreditoHandler c = new CreditoHandler();
		Estado e = c.findEstado(getObjetoi(), hastaFecha);
		if (e != null) {
			if (e.getNombreEstado().equalsIgnoreCase("")) {
				return "";
			}
		}
		return areaCobro;
	}

	public void setAreaCobro(String areaCobro) {
		this.areaCobro = areaCobro;
	}

	public boolean isMinuta() {
		return minuta;
	}

	public void setMinuta(boolean minuta) {
		this.minuta = minuta;
	}

	public boolean isEsDolares() {
		return esDolares;
	}

	public boolean isReducida() {
		return reducida;
	}

	public void setReducida(boolean reducida) {
		this.reducida = reducida;
	}

	public boolean isCer() {
		return cer;
	}

	public SaldoCapitalBean getResumenExigible() {
		return resumenExigible;
	}

	public void setResumenExigible(SaldoCapitalBean resumenExigible) {
		this.resumenExigible = resumenExigible;
	}
}
