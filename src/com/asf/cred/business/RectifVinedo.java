package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.business.inv.AccionINV;
import com.asf.gaf.TipificadorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.importacion.Fecha;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.Vinedo;

public class RectifVinedo implements IProcess {

	private List<ObjetoiVinedo> vinedos = new ArrayList<ObjetoiVinedo>();
	private Long idVinedo;
	private Double qqAprobado;
	private AccionINV accion = AccionINV.LISTAR_VINEDOS_PENDIENTE;
	private BusinessPersistance bp;
	private String forward;
	private String estado;
	private HashMap errores;
	private String rcf;
	private String rcfINV;
	private String codigo;
	private long qq;
	private Date desde;
	private Date hasta;
	private String observacion;
	private String observacionINV;
	private double estimados;
	private double hectareas;
	
	private String expediente;
	private String titular;
	
	private String titulo = "Rectificación de Verificación Técnica";
	private int INV;

	public RectifVinedo() {
		this.bp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		this.desde = new Date();
		this.hasta = new Date();
		this.errores = new HashMap();
		try {
			this.rcf = TipificadorHandler.getDescripcion("estadoSolicitudINV",
					"RCF");
			this.rcfINV = TipificadorHandler.getDescripcion("estadoSolicitudINV",
			"RCINV");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			accion = AccionINV.LISTAR_VINEDOS_PENDIENTE;
		}
		switch (accion) {
		case LISTAR_RECTIFICACIONF_PENDIENTE:
			listarVinedos("RCF");
			forward = "rectifVinedo";
			break;
		case SAVE_RECTIFICACIONF_PENDIENTE:
			rectificarVinedoINV();
			listarVinedos("RCF");
			forward = "rectifVinedo";
			break;
		case LISTAR_RECTIFICACION_INV_PENDIENTE:
			listarVinedos("RCINV");
			forward = "rectifVinedo";
			break;
		case SAVE_RECTIFICACION_INV_PENDIENTE:
			rectificarVinedoFondo();
			listarVinedos("RCINV");
			forward = "rectifVinedo";
			break;
		case DELETE_RECTIFICACIONF_PENDIENTE:
			eliminarVinedoFondo();
			listarVinedos("RCF");
			forward = "rectifVinedo";
			break;
		}
		forward = "rectifVinedo";
		return (errores.size() <= 0);
	}

	private void rectificarVinedoINV() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			//vinedo.setHectareas((double) hectareas);		
			vinedo.getVinedo().setHectareas(hectareas);
			vinedo.setQqEstimados((double) estimados);
			vinedo.setFechaInformeINV(null);
			vinedo.setObservaciones(observacion);
			//No hace falta setear un nuevo vinedo, comento las prox 4 lineas
			//Vinedo vin = new Vinedo();			
			//vin.setCodigo((String) codigo);
			//vinedo.setVinedo(vin);
			vinedo.setObservacionINV(observacionINV);
			try {
				String estado = TipificadorHandler.getDescripcion(
						"estadoSolicitudINV", "SO");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
			observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE;
		listarVinedos("RCF");
	}

	private void rectificarINV() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			vinedo.setFechaInformeINV(null);
			vinedo.setQqAprobado(null);
			vinedo.setObservaciones(observacion);
			vinedo.setObservacionINV(observacionINV);

			try {
				String estado = TipificadorHandler.getDescripcion(
						"estadoSolicitudINV", "RCF");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
			observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE;
		listarVinedos("SO");
	}

	private void rectificarVinedoFondo() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			vinedo.setFechaInformeINV(new Date());
			if(qq!=0){
				vinedo.setQqAprobado((double) qq);
			}	
			vinedo.setObservacionINV(observacionINV);
			try {
				String estado = TipificadorHandler.getDescripcion(
						"estadoSolicitudINV", "RE");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
			observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_RECTIFICACION_INV_PENDIENTE;
		listarVinedos("RCINV");
	}

	private void rectificarFondo() {
		if (idVinedo != null && idVinedo > 0) {
			bp.begin();
			ObjetoiVinedo vinedo = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
			try {
				String estado = TipificadorHandler.getDescripcion(
						"estadoSolicitudINV", "RCINV");
				vinedo.setEstadoSolicitudINV(estado);
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.saveOrUpdate(vinedo);
			bp.commit();
			idVinedo = 0L;
			estimados = 0;
			hectareas = 0;
			codigo = "";
		       		observacion = "";
			observacionINV = "";
		}
		accion = AccionINV.LISTAR_RECTIFICACION_INV_PENDIENTE;
		listarVinedos("RCINV");
	}

	@SuppressWarnings("unchecked")
	private void listarVinedos(String estado) {
		StringBuilder sb = new StringBuilder(
				"SELECT o FROM ObjetoiVinedo o WHERE o.estadoSolicitudINV = :estado");
		if (estado.equals("SN") || estado.equals("SO")) {
			GregorianCalendar gc = new GregorianCalendar();
			int anio = gc.get(GregorianCalendar.YEAR);
			desde = new GregorianCalendar(anio - 1, 0, 1).getTime();
			hasta = new GregorianCalendar(anio, 11, 31).getTime();
			sb.append(" AND o.credito.fechaSolicitud >= :desde "
					+ "AND o.credito.fechaSolicitud <= :hasta "
					+ "ORDER BY o.credito.fechaSolicitud");
		} else {
			GregorianCalendar gc = new GregorianCalendar();
			int anio = gc.get(GregorianCalendar.YEAR);
			desde = Fecha.sumarDias(new Date(), -45);
			hasta = new Date();
		}
		try {
			estado = TipificadorHandler.getDescripcion("estadoSolicitudINV",
					estado);
		} catch (Exception e) {
			e.printStackTrace();
		}
		vinedos = (List<ObjetoiVinedo>) bp.createQuery(sb.toString()).setParameter(
				"estado", estado).list();
	}
	
	private void eliminarVinedoFondo(){
		
		bp.delete(ObjetoiVinedo.class,idVinedo);
		accion = AccionINV.LISTAR_RECTIFICACIONF_PENDIENTE;
		listarVinedos("RCF");
	}

	public String getObservacionINV() {
		return observacionINV;
	}

	public void setObservacionINV(String observacionINV) {
		this.observacionINV = observacionINV;
	}

	public int getINV() {
		return INV;
	}

	public void setINV(int iNV) {
		INV = iNV;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getEstimados() {
		return estimados;
	}

	public void setEstimados(double estimados) {
		this.estimados = estimados;
	}

	public double getHectareas() {
		return hectareas;
	}

	public void setHectareas(double hectareas) {
		this.hectareas = hectareas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public long getQq() {
		return qq;
	}

	public void setQq(long qq) {
		this.qq = qq;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getAccionStr() {
		return accion.toString();
	}

	public void setAccionStr(String accion) {
		if (accion != null) {
			setAccion(AccionINV.valueOf(accion));
		} else {
			setAccion(AccionINV.LISTAR_VINEDOS_PENDIENTE);
		}
	}

	public void setAccion(AccionINV accion) {
		this.accion = accion;
	}

	public AccionINV getAccion() {
		return accion;
	}

	public Long getIdVinedo() {
		return idVinedo;
	}

	public void setIdVinedo(Long idVinedo) {
		this.idVinedo = idVinedo;
	}

	public Double getQqAprobado() {
		return qqAprobado;
	}

	public void setQqAprobado(Double qqAprobado) {
		this.qqAprobado = qqAprobado;
	}

	public List<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(List<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getRcf() {
		return rcf;
	}

	public void setRcf(String rcf) {
		this.rcf = rcf;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public String getRcfINV() {
		return rcfINV;
	}

	public void setRcfINV(String rcfINV) {
		this.rcfINV = rcfINV;
	}
	
	public String getExpediente() {
		return expediente;
	}
	
	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}
	
	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}
}
