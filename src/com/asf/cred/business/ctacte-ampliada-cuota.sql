select fechaGeneracion, min(periodoCtacte) as periodo, min(movimientoCtacte) as movimiento, 
min(verificadorCtacte) as verificador, min(itemCtacte) as item, tipo, numeroBoleto, idBoleto, detalle, 
sum(capital) as capital, sum(compensatorio) as compensatorio, sum(moratorio) as moratorio, 
sum(punitorio) as punitorio, sum(gastos) as gastos, numero, fechaVencimiento, estado, orden, 
caratula_id, boletoReciboId, idObjetoi, min(fechaProceso) as fechaProceso,
-- min(CONVERT(VARCHAR, movimientoCtacte) + '-' + CONVERT(VARCHAR, periodoCtacte) + '-' + CONVERT(VARCHAR, itemCtacte) + '-' + CONVERT(VARCHAR, verificadorCtacte)) AS idctacte,
min(@EXPRESION_IDCTACTE) AS idctacte, 
sum(cer) as cer 
from (select cc.objetoi_id, 
case when cc.emideta_id IS NOT NULL OR (cc.tipomov_id = 2 and conc.concepto_concepto = 'cap' and cc.tipoMovimiento = 'cuota') 
then null else cc.fechaGeneracion end as fechaGeneracion, cc.periodoCtacte, 
case when cc.tipomov_id = 2 and conc.concepto_concepto = 'cap' and cc.tipoMovimiento = 'cuota' 
then null else cc.fechaProceso end as fechaProceso, 
case when bol.tipo = 'Nota Debito Desaplicacion' then 'Nota Debito' 
when bol.tipo = 'Nota Credito Anulacion' then 'Nota Credito'
else bol.tipo end as tipo,  
bol.numeroBoleto, bol.id as idBoleto, cc.tipomov_id, conc.concepto_concepto, cc.tipoMovimiento, 
case when cc.tipomov_id = 2 and conc.concepto_concepto = 'cap' and cc.tipoMovimiento = 'cuota' then 'Capital' 
when bol.tipo = 'Nota Debito Desaplicacion' then cc.detalle 
when bol.tipo = 'Nota Credito Anulacion' then cc.detalle 
when cc.tipomov_id = 2 and (conc.concepto_concepto = 'com' or conc.concepto_concepto = 'gas') and cc.tipoMovimiento = 'cuota' then 'Comp y Gastos' 
when cc.tipomov_id = 2 and (conc.concepto_concepto = 'mor' or conc.concepto_concepto = 'pun') and cc.tipoMovimiento = 'pago' then 'Mor y Pun' 
when cc.tipomov_id = 2 and conc.concepto_concepto = 'mul' then 'Multas' 
when cc.tipomov_id = 1 and conc.concepto_concepto = 'bon' and cc.tipoMovimiento = 'bonificacion' then cc.detalle 
when cc.tipomov_id = 1 and cc.detalle = 'Pago excedente' then 'Cobranza' 
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'rec') and cc.tipoMovimiento = 'cuota' then 'Pago Gastos'
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'rec') and cc.caratula_id is null and cc.tipoMovimiento like 'pago' and cc.detalle is not null then cc.detalle 
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'rec') and cc.caratula_id is null and cc.tipoMovimiento like 'pago' then 'Pago Gastos'
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'pago' and cc.detalle is not null then cc.detalle 
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'pago' then 'Cobranza' 
when cc.tipoMovimiento = 'movManualDeb' then 'Nota de D�bito de ' + cconc.detalle 
when cc.tipoMovimiento = 'movManualCreb' then 'Nota de Cr�dito de ' + cconc.detalle 
when cc.tipoMovimiento = 'reimputacion' then cc.detalle 
else cc.detalle end as detalle, 
case when conc.concepto_concepto = 'cap' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as capital, 
case when (conc.concepto_concepto = 'com' or conc.concepto_concepto = 'bon') then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as compensatorio, 
case when conc.concepto_concepto = 'mor' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as moratorio, 
case when conc.concepto_concepto = 'pun' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as punitorio, 
case when (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'mul' or conc.concepto_concepto = 'rec') 
	then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end 
	else 0 end as gastos,  
case when conc.concepto_concepto = 'cer' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as cer, 
c.numero, 
case when cdp.CaducidadPlazo_id is null then c.fechaVencimiento else cp.fecha end as fechaVencimiento, 
case c.estado when '1' then 'Sin Can' 
	when '2' then 'Canc' end as estado, 
case when (cc.tipoMovimiento = 'pago' or cc.tipoMovimiento = 'reimputacion') then 5 
else case conc.concepto_concepto when 'cap' then 1 
	when 'com' then 2 
	when 'gas' then 2 
	when 'mul' then 2 
	when 'bon' then 3 
	when 'mor' then 4 
	when 'pun' then 4 
	else 5 end 
end as orden, 
cc.movimientoCtacte, cc.verificadorCtacte, cc.itemCtacte, p.caratula_id, p.boleto_id as boletoReciboId, cc.objetoi_id as idObjetoi 
from Ctacte cc left join Boleto bol on cc.boleto_id = bol.id left join Pagos p on p.recibo_id = bol.id 
left join CaducidadPlazo_Ctacte cdp on cdp.movimientos_itemCtacte = cc.itemCtacte and cdp.movimientos_movimientoCtacte = cc.movimientoCtacte and 
    cdp.movimientos_objetoi_id = cc.objetoi_id and cdp.movimientos_periodoCtacte = cc.periodoCtacte and cdp.movimientos_verificadorCtacte = cc.verificadorCtacte 
left join CaducidadPlazo cp on cdp.CaducidadPlazo_id = cp.id, 
Cuota c, Concepto conc, CConcepto cconc 
where cc.objetoi_id = @IDOBJETOI and cc.asociado_id = conc.id and conc.concepto_concepto = cconc.concepto 
and cc.cuota_id = c.id) t 
group by fechaGeneracion, tipo, numeroBoleto, idBoleto, 
detalle, numero, fechaVencimiento, estado, orden, caratula_id, boletoReciboId, idObjetoi , cast(fechaProceso as date)
order by numero, fechaProceso;