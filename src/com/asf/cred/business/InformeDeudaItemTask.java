package com.asf.cred.business;

import java.util.Date;
import java.util.concurrent.Callable;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.DetalleInformeDeuda;
import com.nirven.creditos.hibernate.Objetoi;

public class InformeDeudaItemTask implements Callable<DetalleInformeDeuda> {

	private Long idCredito;
	private Date fechaCalculo;
	private String user;
	private String pass;
	private InformeDeudaItemListener listener;

	public static interface InformeDeudaItemListener {
		public void detalleNuevo(DetalleInformeDeuda detalle);
	}

	public InformeDeudaItemTask(Long idCredito, Date fechaCalculo, String user, String pass) {
		this.idCredito = idCredito;
		this.fechaCalculo = fechaCalculo;
		this.user = user;
		this.pass = pass;
	}

	@Override
	public DetalleInformeDeuda call() throws Exception {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		if (sh == null) {
			sh = new com.asf.cred.security.SessionHandler();
			sh.register(null);
			if (!sh.login(null, user, pass)) {
				System.err.println(getClass() + ": Usuario o clave inv�lido");
				return null;
			}
		}

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda();
		calculoDeuda.setHastaFecha(fechaCalculo);
		calculoDeuda.setIdObjetoi(idCredito);
		calculoDeuda.calcular();

		DetalleInformeDeuda detalle = new DetalleInformeDeuda();

		detalle.setCompensatorio(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_COMPENSATORIO)
				- calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_BONIFICACION));

		detalle.setGastos(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_GASTOS)
				+ calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR)
				+ calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_MULTAS));

		detalle.setMoratorio(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_MORATORIO));
		detalle.setPunitorio(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_PUNITORIO));
		detalle.setCapital(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_CAPITAL));
		detalle.setCer(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_CER));
		detalle.setDeudaVencida(calculoDeuda.getDeudaVencidaFechaEstricta());
		detalle.setMultas(calculoDeuda.getSaldoVencidoConcepto(Concepto.CONCEPTO_MULTAS));

		if (detalle.getTotalFondo() >= 0.01) {
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
			detalle.setCredito(credito);
			detalle.setFechaCalculo(fechaCalculo);

			Date vencimiento = (Date) bp
					.createSQLQuery("SELECT MAX(C.fechaVencimiento) FROM Cuota C "
							+ "WHERE C.credito_id = :id AND C.fechaVencimiento <= :fecha ")
					.setLong("id", idCredito).setDate("fecha", fechaCalculo).uniqueResult();
			detalle.setFechaVencimiento(vencimiento);
			// try {
			bp.save(detalle);
//			}catch(Exception e) {
//				e.printStackTrace();
//			}

			listener.detalleNuevo(detalle);
		} else {
			listener.detalleNuevo(null);
		}

		bp.getCurrentSession().clear();

		return detalle;
	}

	public void setListener(InformeDeudaItemListener listener) {
		this.listener = listener;
	}
}
