package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.MovimientoInstancia;
import com.nirven.creditos.hibernate.ProcesoTipoMovimiento;
import com.nirven.creditos.hibernate.ProcesoTipoResolucion;
import com.nirven.creditos.hibernate.Resolucion;
import com.nirven.creditos.hibernate.SubTipoResolucion;
import com.nirven.creditos.hibernate.TipoMovimientoInstancia;
import com.nirven.creditos.hibernate.TipoProceso;

public class TipoMovimientoProcess implements IProcess {
	private String accion = "";
	private String forward = "errorPage";
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;
	// Atributos
	private List<TipoMovimientoInstancia> tipoMovimientos;
	private String nombre;
	private String observaciones;
	private boolean editable = true;
	private long idTipoResolucion;
	private List<TipoProceso> tipoProcesos;
	private List<TipoProceso> tipoProcesosSeleccionados = new ArrayList<TipoProceso>();
	private String[] seleccion;
	private String[] disponibles;
	private Long idTipoMovimiento = null;
	TipoMovimientoInstancia tipoMov = null;

	public TipoMovimientoProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		tipoProcesos = bp.createQuery("select tp from TipoProceso tp order by tp.nombreTipoProceso").list();
	}

	@Override
	public boolean doProcess() {
		if (this.accion.equals("list")) {
			this.forward = "TipoMovimientoList";
			this.listarTipoMovimiento();
		} else if (this.accion.equals("nuevo")) {
			this.cargarTipoMovimiento();
			this.forward = "TipoMovimiento";
		} else if (this.accion.equals("guardar")) {
			this.guardarTipoMovimiento();
			this.listarTipoMovimiento();
			this.forward = "TipoMovimientoList";
		} else if (this.accion.equals("nuevoTipoMovimiento")) {
			// this.guardarResolucion();
			this.forward = "TipoMovimientoList";
		} else if (this.accion.equals("eliminar")) {
			eliminar();
			this.listarTipoMovimiento();
			this.forward = "TipoMovimientoList";
		} else {
			this.listarTipoMovimiento();
			this.forward = "TipoMovimientoList";
		}
		return this.errores.isEmpty();
	}

	@Override
	public HashMap getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		// Se valida que se haya seleccionado al menos un Proceso
		if (this.accion.equals("guardar") && this.seleccion == null) {
			errores.put("subTipoResolucion.proceso.error", "subTipoResolucion.proceso.error");
		}
		if (this.accion.equals("guardar")) {
			if (this.nombre == null || this.nombre.equals("")) {
				this.errores.put("subtiporesolucion.nombre.error", "subtiporesolucion.nombre.error");
			}
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			String consulta = "";
			consulta += "SELECT e FROM SubTipoResolucion e WHERE UPPER(e.nombre)='" + this.nombre.toUpperCase() + "'";
			consulta += " AND e.id!=" + this.idTipoMovimiento;
			List<TipoProceso> estados = bp.getByFilter(consulta);
			if (estados.size() > 0) {
				this.errores.put("subtiporesolucion.nombre.repetido", "subtiporesolucion.nombre.repetido");
			}
		}
		return errores.isEmpty();
	}

	// Metodos internos
	private void listarTipoMovimiento() {
		String consulta = "";
		consulta = "SELECT t FROM TipoMovimientoInstancia t";
		String filtro = "";
		tipoMovimientos = bp.createQuery(consulta + filtro).list();
	}

	private void guardarTipoMovimiento() {
		ArrayList<ProcesoTipoMovimiento> listaProcesoTipoMovimiento = null;
		// Si se esta modificando un Tipo
		if (!idTipoMovimiento.equals(new Long(0))) {
			tipoMov = (TipoMovimientoInstancia) bp.getById(TipoMovimientoInstancia.class, idTipoMovimiento);
			tipoMov.setNombre(this.nombre);
			bp.update(tipoMov);
			// Busco Los Tipos de Procesos Relacionados
			listaProcesoTipoMovimiento = (ArrayList<ProcesoTipoMovimiento>) bp.createQuery(
					"Select ptr from ProcesoTipoMovimiento ptr where ptr.tipoMovimientoInstancia.id = :idTipoMovimientoInstancia")
					.setParameter("idTipoMovimientoInstancia", idTipoMovimiento).list();
			// Borro las clases asociativas existentes
			for (int i = 0; listaProcesoTipoMovimiento.size() > i; i++) {
				bp.delete(listaProcesoTipoMovimiento.get(i));
			}
			// Creo las nuevas clases asociativas para los procesos que el usuario
			// selecciono
			for (int i = 0; seleccion.length > i; i++) {
				TipoProceso proceso = (TipoProceso) bp.getById(TipoProceso.class, new Long(seleccion[i]));
				ProcesoTipoMovimiento procesoTipoRes = new ProcesoTipoMovimiento();
				procesoTipoRes.setProceso(proceso);
				procesoTipoRes.setTipoMovimientoInstancia(tipoMov);
				bp.save(procesoTipoRes);
			}
		} else {
			tipoMov = new TipoMovimientoInstancia();
			tipoMov.setNombre(this.nombre);
			bp.save(tipoMov);
			for (int i = 0; seleccion.length > i; i++) {
				TipoProceso proceso = (TipoProceso) bp.getById(TipoProceso.class, new Long(seleccion[i]));
				ProcesoTipoMovimiento procesoTipoRes = new ProcesoTipoMovimiento();
				procesoTipoRes.setProceso(proceso);
				procesoTipoRes.setTipoMovimientoInstancia(tipoMov);
				bp.save(procesoTipoRes);
			}
		}
	}

	private void cargarTipoMovimiento() {
		ArrayList<ProcesoTipoMovimiento> listaProcesoTipoMovimiento = null;
		if (idTipoMovimiento != null) {
			TipoMovimientoInstancia tipoMovIns = (TipoMovimientoInstancia) bp.getById(TipoMovimientoInstancia.class,
					idTipoMovimiento);
			this.nombre = tipoMovIns.getNombre();
			listaProcesoTipoMovimiento = (ArrayList<ProcesoTipoMovimiento>) bp.createQuery(
					"Select ptr from ProcesoTipoMovimiento ptr where ptr.tipoMovimientoInstancia.id = :idSubTipoResolucion")
					.setParameter("idSubTipoResolucion", idTipoMovimiento).list();
			// Busca los procesos seleccionados
			for (int i = 0; listaProcesoTipoMovimiento.size() > i; i++) {
				tipoProcesosSeleccionados.add((TipoProceso) bp.getById(TipoProceso.class,
						listaProcesoTipoMovimiento.get(i).getProceso().getId()));
			}
			// Elimina los procesos seleccionados de la lista de disponibles
			for (int i = 0; tipoProcesosSeleccionados.size() > i; i++) {
				for (int j = 0; tipoProcesos.size() > j; j++) {
					if (listaProcesoTipoMovimiento.get(i).getProceso().equals(tipoProcesos.get(j))) {
						tipoProcesos.remove(j);
					}
				}
			}
		}
	}

	public void eliminar() {
		List<MovimientoInstancia> resolucionList = (List<MovimientoInstancia>) bp
				.createQuery("from MovimientoInstancia r where r.tipoMovimientoInstancia.id = " + this.idTipoMovimiento)
				.list();
		if (!resolucionList.isEmpty()) {
			errores.put("Error.borrar.TipoMovimiento.MovimientoInstancia",
					"Error.borrar.TipoMovimiento.MovimientoInstancia");
		} else {
			List<ProcesoTipoMovimiento> procesoTipoMovimientoList = (List<ProcesoTipoMovimiento>) bp.createQuery(
					"from ProcesoTipoMovimiento r where r.tipoMovimientoInstancia.id = " + this.idTipoMovimiento)
					.list();// List<ProcesoTipoResolucion> procesoTipoResolucionList =
							// (List<ProcesoTipoResolucion>)bp.createQuery("from
							// ProcesoTipoResolucion r where r.subTipoResolucion.id =
							// "+this.idTipoMovimiento).list();
			for (ProcesoTipoMovimiento pTM : procesoTipoMovimientoList) {
				bp.delete(pTM);
			}
			TipoMovimientoInstancia tMI = (TipoMovimientoInstancia) bp.getById(TipoMovimientoInstancia.class,
					this.idTipoMovimiento);
			bp.delete(tMI);
		}
	}

	// Getters and Setters
	public String getAccion() {
		return accion;
	}

	public List<TipoMovimientoInstancia> getTipoMovimientos() {
		return tipoMovimientos;
	}

	public void setTipoMovimientos(List<TipoMovimientoInstancia> tipoMovimientos) {
		this.tipoMovimientos = tipoMovimientos;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public long getIdTipoResolucion() {
		return idTipoResolucion;
	}

	public void setIdTipoResolucion(long idTipoResolucion) {
		this.idTipoResolucion = idTipoResolucion;
	}

	public String[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String[] seleccion) {
		this.seleccion = seleccion;
	}

	public String[] getDisponibles() {
		return disponibles;
	}

	public void setDisponibles(String[] disponibles) {
		this.disponibles = disponibles;
	}

	public List<TipoProceso> getTipoProcesos() {
		return tipoProcesos;
	}

	public void setTipoProcesos(List<TipoProceso> tipoProcesos) {
		this.tipoProcesos = tipoProcesos;
	}

	public List<TipoProceso> getTipoProcesosSeleccionados() {
		return tipoProcesosSeleccionados;
	}

	public void setTipoProcesosSeleccionados(List<TipoProceso> tipoProcesosSeleccionados) {
		this.tipoProcesosSeleccionados = tipoProcesosSeleccionados;
	}

	public Long getIdTipoMovimiento() {
		return idTipoMovimiento;
	}

	public void setIdTipoMovimiento(Long idTipoMovimiento) {
		this.idTipoMovimiento = idTipoMovimiento;
	}
}
