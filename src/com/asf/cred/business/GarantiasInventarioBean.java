package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

public class GarantiasInventarioBean implements Serializable {

	private String ordenFisico;
	private Long numeroInventarioOriginal;
	private Long numeroAtencion;
	private String linea;
	private String inversor;
	private Long cuit;
	private Double financiamiento;
	private String tipoGarantia;
	private String identificador;
	private String descripcion;
	private Double valor;
	private String estado;
	private Date vencimiento;
	private Date vencimientoSeguro;
	private String sectorCustodia;
	private String responsableCustodia;
	private String expediente;
	private String etapaCredito;
	private Date fechaEstado;
	private Date fechaInscripcion;
	private Date fechaReinscripcion;
	private Date inscripcion;
	private Double aforo;
	private Date fechaProceso;
	
	public GarantiasInventarioBean() {
	}
	
	public GarantiasInventarioBean(Object[] r) {
		this.ordenFisico = (String) r[0];
		this.numeroInventarioOriginal = r[1] != null ? ((Number) r[1]).longValue() : null;
		this.numeroAtencion = r[2] != null ? ((Number) r[2]).longValue() : null;
		this.linea = (String) r[3];
		this.inversor = (String) r[4];
		this.cuit = r[5] != null ? ((Number) r[5]).longValue() : null;
		this.financiamiento = r[6] != null ? ((Number) r[6]).doubleValue() : null;
		this.tipoGarantia = (String) r[7];
		this.identificador = (String) r[8];
		this.descripcion = (String) r[9];
		this.valor = r[10] != null ? ((Number) r[10]).doubleValue() : null;
		this.etapaCredito = (String) r[11];
		this.vencimiento = (Date) r[12];
		this.vencimientoSeguro = (Date) r[13];
		this.sectorCustodia = (String) r[14];
		this.responsableCustodia = (String) r[15];
		this.expediente = (String) r[16];
		this.estado = (String) r[17];
		this.fechaEstado = (Date) r[18];
		this.fechaInscripcion = (Date) r[19];
		this.fechaReinscripcion = (Date) r[20];
		this.aforo = (Double) r[21];
		this.fechaProceso = (Date) r[22];
		if(fechaInscripcion!=null){
			inscripcion = fechaInscripcion;
		}else{
			inscripcion = fechaReinscripcion;
		}
	}
	
	
	public Long getNumeroAtencion() {
		return numeroAtencion;
	}
	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public String getInversor() {
		return inversor;
	}
	public void setInversor(String inversor) {
		this.inversor = inversor;
	}
	public Long getCuit() {
		return cuit;
	}
	public void setCuit(Long cuit) {
		this.cuit = cuit;
	}
	public Double getFinanciamiento() {
		return financiamiento;
	}
	public void setFinanciamiento(Double financiamiento) {
		this.financiamiento = financiamiento;
	}
	public String getTipoGarantia() {
		return tipoGarantia;
	}
	public void setTipoGarantia(String tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getVencimiento() {
		return vencimiento;
	}
	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}
	public Date getVencimientoSeguro() {
		return vencimientoSeguro;
	}
	public void setVencimientoSeguro(Date vencimientoSeguro) {
		this.vencimientoSeguro = vencimientoSeguro;
	}
	public String getSectorCustodia() {
		return sectorCustodia;
	}
	public void setSectorCustodia(String sectorCustodia) {
		this.sectorCustodia = sectorCustodia;
	}
	public String getResponsableCustodia() {
		return responsableCustodia;
	}
	public void setResponsableCustodia(String responsableCustodia) {
		this.responsableCustodia = responsableCustodia;
	}
	public Long getNumeroInventarioOriginal() {
		return numeroInventarioOriginal;
	}
	public void setNumeroInventarioOriginal(Long numeroInventarioOriginal) {
		this.numeroInventarioOriginal = numeroInventarioOriginal;
	}

	public String getOrdenFisico() {
		return ordenFisico;
	}

	public void setOrdenFisico(String ordenFisico) {
		this.ordenFisico = ordenFisico;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getEtapaCredito() {
		return etapaCredito;
	}

	public void setEtapaCredito(String etapaCredito) {
		this.etapaCredito = etapaCredito;
	}

	public Date getFechaEstado() {
		return fechaEstado;
	}

	public void setFechaEstado(Date fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public Date getFechaReinscripcion() {
		return fechaReinscripcion;
	}

	public void setFechaReinscripcion(Date fechaReinscripcion) {
		this.fechaReinscripcion = fechaReinscripcion;
	}

	public Date getInscripcion() {
		return inscripcion;
	}

	public void setInscripcion(Date inscripcion) {
		this.inscripcion = inscripcion;
	}

	public Double getAforo() {
		return aforo;
	}

	public void setAforo(Double aforo) {
		this.aforo = aforo;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
}
