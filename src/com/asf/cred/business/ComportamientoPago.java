package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;

public class ComportamientoPago implements IProcess {
	private String forward;
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;
	private List<ObjetoiComportamiento> comportamientos;
	private Objetoi objetoi;
	
	public ComportamientoPago() {
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		forward = "ComportamientoPagoList";
		objetoi = new Objetoi();
	}

	// ======================FUNCIONALIDADES========================
	@SuppressWarnings("unchecked")
	public boolean doProcess() {
		comportamientos = bp.createQuery("SELECT c FROM ObjetoiComportamiento c " +
										"WHERE c.objetoi = :objetoi " +
										"ORDER BY c.fecha")
										.setEntity("objetoi", objetoi)
										.list();
		objetoi = (Objetoi) bp.getById(Objetoi.class, objetoi.getId());
		return this.errores.isEmpty();
	}

	// ======================GETTERS Y SETTERS======================
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Object getResult() {
		return null;
	}

	public boolean validate() {
		return this.errores.isEmpty();
	}

	public List<ObjetoiComportamiento> getComportamientos() {
		return comportamientos;
	}

	public void setComportamientos(List<ObjetoiComportamiento> comportamientos) {
		this.comportamientos = comportamientos;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}
	
	

	
}