package com.asf.cred.business;

import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.AbmAction;
import com.nirven.creditos.hibernate.DocumentoMovimientoInstancia;
import com.nirven.creditos.hibernate.DocumentoResolucion;
import com.nirven.creditos.hibernate.ObjetoiArchivo;

public class ArchivoCreditoAction extends AbmAction {

	public ActionForward descargar(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String id = (String) request.getParameter("id");
		OutputStream salida = response.getOutputStream();
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		// TODO implementar descarga desde archivoRepo
		ObjetoiArchivo archivo = (ObjetoiArchivo) oBp.getById(ObjetoiArchivo.class, new Long(id));
		response.setContentType(archivo.getMimetype());
		response.setHeader("Content-Disposition", "attachment;filename=\"" + archivo.getNombre() + "\"");
		salida.write(archivo.getArchivo());
		salida.flush();
		salida.close();
		return null;
	}

	public ActionForward descargarArchivoJudicial(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String id = (String) request.getParameter("id");
		OutputStream salida = response.getOutputStream();
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		DocumentoResolucion archivo = (DocumentoResolucion) oBp.getById(DocumentoResolucion.class, new Long(id));
		response.setContentType(archivo.getMimetype());
		response.setHeader("Content-Disposition", "attachment;filename=\"" + archivo.getNombre() + "\"");
		salida.write(archivo.getArchivo());
		salida.flush();
		salida.close();
		return null;
	}

	public ActionForward descargarArchivoMovimientoInstancia(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		String id = (String) request.getParameter("id");
		OutputStream salida = response.getOutputStream();
		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		DocumentoMovimientoInstancia archivo = (DocumentoMovimientoInstancia) oBp
				.getById(DocumentoMovimientoInstancia.class, new Long(id));
		response.setContentType(archivo.getMimetype());
		response.setHeader("Content-Disposition", "attachment;filename=\"" + archivo.getNombre() + "\"");
		salida.write(archivo.getArchivo());
		salida.flush();
		salida.close();
		return null;
	}

}
