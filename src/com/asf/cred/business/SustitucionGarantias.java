package com.asf.cred.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.upload.FormFile;
import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.SQLHelper;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.CampoGarantia;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.GarantiaUso;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.TipoGarantia;
import com.nirven.creditos.hibernate.ValorCampoGarantia;

public class SustitucionGarantias implements IProcess {

	private String accion;
	private String forward;
	private BusinessPersistance bp;
	private HashMap<String, String> errores;
	private List<ObjetoiDTO> creditos;
	private Long idLinea;
	private Long idTipoGarantia;
	private Long idEstado;
	private Integer fecovita;
	private Garantia garantia;
	private TipoGarantia tipoGarantia;
	private int aforo;
	private String fechaLibramiento;
	private FormFile formFile;
	private String idTipoFianza;
	private String idTipoFiduciaria;
	private List<ObjetoiDTO> seleccionados;
	private Integer[] creditosSeleccionados;
	private String exito;
	private List<Long> ids;
	private Long numeroAtencion;

	@SuppressWarnings("unchecked")
	public SustitucionGarantias() {
		forward = "SustitucionGarantias";
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, String>();
		fecovita = 1;
		garantia = new Garantia();
		tipoGarantia = new TipoGarantia();
		idTipoFianza = DirectorHelper.getString("tipoGarantia.fianzaPersonal");
		idTipoFiduciaria = DirectorHelper.getString("tipoGarantia.fiduciaria");
		creditos = (List<ObjetoiDTO>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("SustitucionGarantias.creditos");
		if (creditos != null)
			creditosSeleccionados = new Integer[creditos.size()];
	}

	@Override
	public boolean doProcess() {
		if (tipoGarantia.getId() != null)
			tipoGarantia = (TipoGarantia) bp.getById(TipoGarantia.class, tipoGarantia.getId());
		if (accion == null) {
			return true;
		} else if (accion.equals("listar")) {
			listar();
		} else if (accion.equals("mostrarCampos")) {
			mostrarCampos();
		} else if (accion.equals("sustituir")) {
			sustituir();
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listar() {
		String consultaStr = "SELECT DISTINCT O.id, O.numeroAtencion, O.expediente, P.NOMB_12, P.CUIL_12, T.TF_DESCRIPCION, TG.nombre, TG.id 'idTipoGarantia', G.identificadorUnico "
				+ "FROM Objetoi O JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id "
				+ "JOIN Estado E ON OE.estado_idEstado = E.idEstado JOIN Linea L ON L.id = O.linea_id "
				+ "JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id JOIN Garantia G ON OG.garantia_id = G.id "
				+ "JOIN TipoGarantia TG ON TG.id = G.tipo_id JOIN PERSONA P ON O.persona_IDPERSONA = P.IDPERSONA "
				+ "LEFT JOIN TIPIFICADORES T ON T.TF_CATEGORIA = 'Garantia.tipoProducto' AND T.TF_CODIGO = G.tipoProducto "
				+ "LEFT JOIN GarantiaEstado ge ON ge.objetoiGarantia_id = OG.id WHERE OE.fechaHasta IS NULL "
				+ "AND O.fecovita = :fecovita "
				+ "AND (ge.estado IS NULL OR ge.fechaProceso = (SELECT MAX(fechaProceso) FROM GarantiaEstado WHERE objetoiGarantia_id = OG.id) "
				+ "AND ge.estado NOT IN ('SUSTITUIDA','CANCELADA')) ";
		if (idLinea != null && idLinea.longValue() != 0L) {
			consultaStr += "AND L.id = :linea_id ";
		}
		if (idTipoGarantia != null && idTipoGarantia.longValue() != 0L) {
			consultaStr += "AND TG.id = :tipo_id ";
		}
		if (idEstado != null && idEstado.longValue() != 0L) {
			consultaStr += "AND E.idEstado = :idEstado ";
		} else {
			consultaStr += "AND E.nombreEstado NOT IN('DESISTIDO ', 'CANCELADO', 'GESTION JUDICIAL', 'GESTION EXTRAJUDICIAL') ";
		}
		if (numeroAtencion != null && numeroAtencion.longValue() != 0L) {
			consultaStr += "AND O.numeroAtencion = :numeroAtencion ";
		}
		consultaStr += "ORDER BY O.id";
		Query consulta = bp.createSQLQuery(consultaStr);
		consulta.setInteger("fecovita", fecovita);
		if (idLinea != null && idLinea.longValue() != 0L) {
			consulta.setLong("linea_id", idLinea);
		}
		if (idTipoGarantia != null && idTipoGarantia.longValue() != 0L) {
			consulta.setLong("tipo_id", idTipoGarantia);
		}
		if (idEstado != null && idEstado.longValue() != 0L) {
			consulta.setLong("idEstado", idEstado);
		}
		if (numeroAtencion != null && numeroAtencion.longValue() != 0L) {
			consulta.setLong("numeroAtencion", numeroAtencion);
		}
		List<Object[]> objetois = consulta.list();
		ObjetoiDTO dto;
		creditos = new ArrayList<ObjetoiDTO>();
		for (Object[] objeto : objetois) {
			dto = new ObjetoiDTO();
			dto.setIdObjetoi(((BigDecimal) objeto[0]).longValue());
			dto.setNumeroCredito(((BigDecimal) objeto[1]).toString());
			dto.setExpediente((String) objeto[2]);
			dto.getTitular().setNomb12((String) objeto[3]);
			dto.setCuil(((BigDecimal) objeto[4]).toString());
			dto.setProducto(objeto[5] == null ? "" : (String) objeto[5]);
			dto.setTipoGarantia((String) objeto[6]);
			dto.setIdTipoGarantia(((BigDecimal) objeto[7]).longValue());
			dto.setIdUnicoGarantia((String) objeto[8]);
			creditos.add(dto);
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("SustitucionGarantias.creditos", creditos);
	}

	public void mostrarCampos() {
		// implementacion vacia
	}

	@SuppressWarnings("unchecked")
	public void sustituir() {
		bp.begin();
		Garantia existente = null;
		Garantia nueva = null;
		existente = (Garantia) bp.getNamedQuery("Garantia.findByIdentificadorYTipo")
				.setString("identificador", garantia.getIdentificadorUnico()).setEntity("tipo", tipoGarantia)
				.setMaxResults(1).uniqueResult();
		if (tipoGarantia.getId().toString().equals(idTipoFianza)) {
			double coeficiente = aforo / 100D + 1D;
			Object sumDebitos = bp
					.createSQLQuery("SELECT SUM(CC.importe) FROM Objetoi O "
							+ "JOIN Ctacte CC ON CC.objetoi_id = O.id JOIN Concepto CO ON CC.facturado_id = CO.id "
							+ "JOIN CConcepto CCO ON CO.concepto_concepto = CCO.concepto WHERE CC.tipomov_id = 2 "
							+ "AND CCO.abreviatura = 'cap' AND CC.objetoi_id IN(:creditos)")
					.setParameterList("creditos", ids).uniqueResult();
			double debitos = sumDebitos == null ? 0.0 : ((Double) sumDebitos).doubleValue();
			Object sumCreditos = bp
					.createSQLQuery("SELECT SUM(CC.importe) FROM Objetoi O "
							+ "JOIN Ctacte CC ON CC.objetoi_id = O.id JOIN Concepto CO ON CC.facturado_id = CO.id "
							+ "JOIN CConcepto CCO ON CO.concepto_concepto = CCO.concepto WHERE CC.tipomov_id = 1 "
							+ "AND CCO.abreviatura = 'cap' AND CC.objetoi_id IN(:creditos)")
					.setParameterList("creditos", ids).uniqueResult();
			double creditos = sumCreditos == null ? 0.0 : ((Double) sumCreditos).doubleValue();
			if (existente != null) {
				existente.setObservacion(garantia.getObservacion());
				// existente.setValor((debitos - creditos) * coeficiente);
				// Actualizar fecha libramiento
				List<ValorCampoGarantia> valores;
				if (existente.getValores() != null && !existente.getValores().isEmpty())
					valores = existente.getValores();
				else
					valores = new ArrayList<ValorCampoGarantia>();
				boolean agrego = false;
				for (ValorCampoGarantia valor : valores) {
					if (valor.getCampo().getNombre().equals("Fecha Libramiento (Pagar� y/o Cheque)")) {
						valor.setValorCadena(fechaLibramiento);
						bp.update(valor);
						agrego = true;
						break;
					}
				}
				if (agrego == false) {
					ValorCampoGarantia valor = new ValorCampoGarantia();
					List<CampoGarantia> campos = bp.getNamedQuery("CampoGarantia.findByTipoGarantia")
							.setEntity("tipoGarantia", tipoGarantia).list();
					for (CampoGarantia campo : campos) {
						if (campo.getNombre().equals("Fecha Libramiento (Pagar� y/o Cheque)")) {
							valor = new ValorCampoGarantia();
							valor.setCampo(campo);
							valor.setValorCadena(fechaLibramiento);
							bp.save(valor);
							valores.add(valor);
							existente.setValores(valores);
							break;
						}
					}
				}
				bp.update(existente);
			} else {
				nueva = new Garantia();
				nueva.setIdentificadorUnico(garantia.getIdentificadorUnico());
				nueva.setObservacion(garantia.getObservacion());
				// nueva.setValor((debitos - creditos) * coeficiente);
				nueva.setTipo(tipoGarantia);
				nueva.setSeguro(null);
				nueva.setTasacionInmueble(null);
				nueva.setTasacionMueble(null);
				// Agregar fecha libramiento
				List<CampoGarantia> campos = bp.getNamedQuery("CampoGarantia.findByTipoGarantia")
						.setEntity("tipoGarantia", tipoGarantia).list();
				ValorCampoGarantia valor;
				List<ValorCampoGarantia> valores = new ArrayList<ValorCampoGarantia>();
				for (CampoGarantia campo : campos) {
					if (campo.getNombre().equals("Fecha Libramiento (Pagaré y/o Cheque)")) {
						valor = new ValorCampoGarantia();
						valor.setCampo(campo);
						valor.setValorCadena(fechaLibramiento);
						bp.save(valor);
						valores.add(valor);
						nueva.setValores(valores);
						break;
					}
				}
				bp.save(nueva);
			}
		} else {
			if (existente != null) {
				existente.setObservacion(garantia.getObservacion());
				bp.update(existente);
			} else {
				nueva = new Garantia();
				nueva.setIdentificadorUnico(garantia.getIdentificadorUnico());
				nueva.setObservacion(garantia.getObservacion());
				nueva.setTipo(tipoGarantia);
				nueva.setSeguro(null);
				nueva.setTasacionInmueble(null);
				nueva.setTasacionMueble(null);
				bp.save(nueva);
			}
		}
		List<ObjetoiDTO> sustituidos = new ArrayList<ObjetoiDTO>();
		Objetoi credito;
		ObjetoiArchivo objetoiArchivo;
		ObjetoiGarantia objetoiGarantia;
		ObjetoiGarantia nuevoObjetoiGarantia;
		final String usuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		final Date fecha = new Date();
		List<Persona> personas;
		GarantiaUso garantiaUso;
		Integer count;
		for (ObjetoiDTO dto : seleccionados) {
			credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
			try {
				objetoiArchivo = new ObjetoiArchivo();
				objetoiArchivo.setCredito(credito);
				objetoiArchivo.setNoBorrar(true);
				objetoiArchivo.setArchivo(formFile.getFileData());
				objetoiArchivo.setMimetype(formFile.getContentType());
				objetoiArchivo.setNombre(formFile.getFileName());
				objetoiArchivo.setFechaAdjunto(fecha);
				objetoiArchivo.setUsuario(usuario);
				bp.save(objetoiArchivo);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				bp.rollback();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				bp.rollback();
				return;
			}

			String consultaBasica = "SELECT * FROM ObjetoiGarantia OG LEFT JOIN GarantiaEstado ge ON ge.objetoiGarantia_id = OG.id "
					+ "WHERE OG.objetoi_id = :objetoi_id AND OG.baja IS NULL AND ge.fechaProceso = (SELECT MAX(fechaProceso) FROM GarantiaEstado WHERE objetoiGarantia_id = OG.id) "
					+ "AND ge.estado NOT IN ('SUSTITUIDA','CANCELADA') ORDER BY OG.id";
			String consultaLimitada = SQLHelper.getLimitedQuery(consultaBasica, 1);
			objetoiGarantia = (ObjetoiGarantia) bp.createSQLQuery(consultaLimitada).addEntity(ObjetoiGarantia.class)
					.setLong("objetoi_id", dto.getIdObjetoi()).uniqueResult();
			if (objetoiGarantia != null) {
				// objetoiGarantia.setEstado("2");
				objetoiGarantia.setFechaSustitucion(fecha);
				String obsActual = objetoiGarantia.getObservacion();
				String obs = "Sustituida por Garantia "
						+ (nueva == null ? existente.getIdentificadorUnico() : nueva.getIdentificadorUnico());
				if (obsActual != null) {
					obsActual = obs + "\n" + obsActual;
				} else {
					obsActual = obs;
				}
				objetoiGarantia.setObservacion(obs);
				if (objetoiGarantia.getTasacion() != null) {
					objetoiGarantia.getTasacion().setFechaSustitucion(fecha);
					bp.update(objetoiGarantia.getTasacion());
				}
				bp.update(objetoiGarantia);
				GarantiaEstado ge = new GarantiaEstado(ComportamientoGarantiaEnum.SUSTITUIDA, fecha);
				ge.setObjetoiGarantia(objetoiGarantia);
				ge.determinarImporte();
				bp.save(ge);
			}

			nuevoObjetoiGarantia = new ObjetoiGarantia();
			nuevoObjetoiGarantia.setTasacion(null);

			if (existente != null) {
				nuevoObjetoiGarantia.setGarantia(existente);
			} else {
				nuevoObjetoiGarantia.setGarantia(nueva);
			}
			nuevoObjetoiGarantia.setObjetoi(credito);
			// nuevoObjetoiGarantia.setEstado("1");

			if (objetoiGarantia != null) {
				Double aforo = nuevoObjetoiGarantia.getGarantia().getTipo().getAforo();
				if (aforo != null) {
					nuevoObjetoiGarantia.setAforo(aforo);
					nuevoObjetoiGarantia.setValor(credito.getFinanciamiento() * aforo);
				} else {
					nuevoObjetoiGarantia.setAforo(0.0);
					nuevoObjetoiGarantia.setValor(credito.getFinanciamiento());
				}
			}

			bp.save(nuevoObjetoiGarantia);

			// GarantiaEstado estadoNuevo = new GarantiaEstado(GarantiaEstadoEnum.ACTIVA);
			// estadoNuevo.setFechaEstado(fecha);
			// estadoNuevo.setObjetoiGarantia(nuevoObjetoiGarantia);
			// estadoNuevo.setUsuario(usuario);
			// estadoNuevo.setImporte(nuevoObjetoiGarantia.getValor() != null ?
			// nuevoObjetoiGarantia.getValor() : 0);

			GarantiaEstado estadoNuevo = new GarantiaEstado(ComportamientoGarantiaEnum.ACTIVA, fecha);
			estadoNuevo.setObjetoiGarantia(nuevoObjetoiGarantia);
			estadoNuevo.determinarImporte();
			bp.save(estadoNuevo);

			if (dto.getIdTipoGarantia().toString().equals(idTipoFiduciaria) ? true : false) {
				credito.setLiberar(true);
				credito.setForzarLiberacion(true);
				bp.update(credito);
			}
			if (tipoGarantia.getEsPersona() != null && tipoGarantia.getEsPersona().booleanValue()) {
				personas = bp.createSQLQuery("SELECT * FROM PERSONA WHERE CUIL_12 = :cuil").addEntity(Persona.class)
						.setString("cuil", garantia.getIdentificadorUnico()).list();
				if (personas.isEmpty()) {
					errores.put("sustitucionGarantias.persona", "sustitucionGarantias.persona");
					bp.rollback();
					return;
				} else if (personas.size() > 1) {
					errores.put("sustitucionGarantias.personas", "sustitucionGarantias.personas");
					bp.rollback();
					return;
				} else {
					garantiaUso = new GarantiaUso();
					garantiaUso.setPersona(personas.get(0));
					garantiaUso.setPorcentaje(1D);
					Long idGarantia;
					if (nueva != null) {
						garantiaUso.setGarantia(nueva);
						idGarantia = nueva.getId();
					} else {
						garantiaUso.setGarantia(existente);
						idGarantia = existente.getId();
					}
					count = (Integer) bp
							.createSQLQuery("SELECT COUNT(id) FROM GarantiaUso WHERE garantia_id = :garantia_id")
							.setLong("garantia_id", idGarantia).uniqueResult();
					if (count.intValue() == 0)
						bp.save(garantiaUso);
				}
			}
			dto.setTipoGarantia(tipoGarantia.getNombre());
			if (nueva != null)
				dto.setProducto(null);
			sustituidos.add(dto);
		}
		bp.commit();
		creditos = sustituidos;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("SustitucionGarantias.creditos", creditos);
		tipoGarantia.setId(null);
		tipoGarantia.setNombre(null);
		exito = "Garant�as sustitu�das correctamente";
	}

	@Override
	public HashMap<String, String> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		if (accion != null && accion.equals("sustituir")) {
			boolean haySeleccion = false;
			seleccionados = new ArrayList<ObjetoiDTO>();
			ids = new ArrayList<Long>();
			for (Integer indice : creditosSeleccionados) {
				if (indice != null) {
					seleccionados.add(creditos.get(indice));
					ids.add(creditos.get(indice).getIdObjetoi());
					haySeleccion = true;
				}
			}
			if (haySeleccion == false) {
				errores.put("sustitucionGarantias.seleccion", "sustitucionGarantias.seleccion");
			}
			if (garantia.getIdentificadorUnico() == null || garantia.getIdentificadorUnico().isEmpty()
					|| garantia.getIdentificadorUnico().equals("0")) {
				errores.put("sustitucionGarantias.identificador", "sustitucionGarantias.identificador");
			}
			if (garantia.getObservacion() == null || garantia.getObservacion().isEmpty()) {
				errores.put("sustitucionGarantias.observacion", "sustitucionGarantias.observacion");
			}
			if (formFile == null || formFile.getFileName() == null || formFile.getFileName().isEmpty()) {
				errores.put("sustitucionGarantias.resolucion", "sustitucionGarantias.resolucion");
			}
			if (tipoGarantia.getId() != null && tipoGarantia.getId().toString().equals(idTipoFianza)) {
				if (aforo < 0 || aforo > 100) {
					errores.put("sustitucionGarantias.aforo", "sustitucionGarantias.aforo");
				}
				if (fechaLibramiento == null || fechaLibramiento.isEmpty()) {
					errores.put("sustitucionGarantias.fechaLibramiento", "sustitucionGarantias.fechaLibramiento");
				} else if (DateHelper.getDate(fechaLibramiento) == null) {
					errores.put("sustitucionGarantias.fechaLibramiento", "sustitucionGarantias.fechaLibramiento");
				}
			}
		}
		return errores.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public Long getIdTipoGarantia() {
		return idTipoGarantia;
	}

	public void setIdTipoGarantia(Long idTipoGarantia) {
		this.idTipoGarantia = idTipoGarantia;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public Integer getFecovita() {
		return fecovita;
	}

	public void setFecovita(Integer fecovita) {
		this.fecovita = fecovita;
	}

	public Garantia getGarantia() {
		return garantia;
	}

	public void setGarantia(Garantia garantia) {
		this.garantia = garantia;
	}

	public TipoGarantia getTipoGarantia() {
		return tipoGarantia;
	}

	public void setTipoGarantia(TipoGarantia tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}

	public int getAforo() {
		return aforo;
	}

	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	public String getFechaLibramiento() {
		return fechaLibramiento;
	}

	public void setFechaLibramiento(String fechaLibramiento) {
		this.fechaLibramiento = fechaLibramiento;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public Integer[] getCreditosSeleccionados() {
		return creditosSeleccionados;
	}

	public void setCreditosSeleccionados(Integer[] creditosSeleccionados) {
		this.creditosSeleccionados = creditosSeleccionados;
	}

	public String getExito() {
		return exito;
	}

	public void setExito(String exito) {
		this.exito = exito;
	}

	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

}
