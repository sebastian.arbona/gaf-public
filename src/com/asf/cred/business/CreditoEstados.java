package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.asf.cred.estados.EstaObs;
import com.asf.gaf.DirectorHandler;
import com.asf.hibernate.mapping.Claves;
import com.asf.hibernate.mapping.Director;
import com.asf.hibernate.mapping.Permiso;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.bpm.ImputacionesHelper;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObservacionObjetoi;

public class CreditoEstados implements IProcess {
	// ==========================ATRIBUTOS============================
	public static String separador = "@";
	public static Long agregaEstado = 1L;
	public static Long agregaObs = 2L;
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "CreditoEstadosList";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Long idObjetoi;
	private Objetoi objetoi;
	private Estado estado;
	private Date fecha = new Date(System.currentTimeMillis());
	private String obsEstado;
	private List<ObjetoiEstado> objetoiEstados;
	private String estadoAcuerdo;
	private boolean ocultarCampos = false;
	private String detalleObservacion;
	private Long agrega;
	private ObservacionObjetoi observacion;
	private List<EstaObs> estadosObservaciones;
	private ReportResult reporte;
	private String numeroResolucion;
	private FormFile formFile;

	// =======================CONSTRUCTORES=========================
	public CreditoEstados() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.objetoi = new Objetoi();
		this.estado = new Estado();
		observacion = new ObservacionObjetoi();
	}

	// ======================FUNCIONALIDADES========================
	@Override
	@SuppressWarnings("unchecked")
	public boolean doProcess() {

		try {
			estadoAcuerdo = DirectorHandler.getDirector("estado.ObjetoOriginal").getValor();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Objetoi obj = buscarObjetoi(getObjetoi().getId());

		if (obj != null) {
			if (obj.getEstadoActual() != null) {
				if (obj.getEstadoActual().getEstado().getId().toString().equalsIgnoreCase(estadoAcuerdo)) {
					ocultarCampos = true;
				} else {
					ocultarCampos = false;
				}
			}
		}

		if (this.accion.equals("nuevo")) {
			this.forward = "CreditoEstados";
		} else if (this.accion.equals("guardar")) {

			if (estado.getIdEstado() == 0 && agrega.equals(new Long(2))) {
				objetoiEstados = bp
						.createQuery("from ObjetoiEstado where objetoi_id = :idObjetoi and fechaHasta IS NULL")
						.setParameter("idObjetoi", objetoi.getId()).list();
				if (objetoiEstados.size() > 0)
					estado = objetoiEstados.get(0).getEstado();
			} else {
				estado = (Estado) bp.getById(Estado.class, estado.getId());
			}
			if ((estado.getNombreEstado()).trim().equalsIgnoreCase("DESISTIDO")) {
				if (verificarPermiso()) {
					Number count = (Number) bp
							.createSQLQuery("select count(*) from Desembolso d where d.estado in (:estados) "
									+ "and d.credito_id = :id")
							.setParameterList("estados", new String[] { Desembolso.REALIZADO, Desembolso.PARCIAL })
							.setParameter("id", objetoi.getId()).uniqueResult();
					if (count != null && count.intValue() > 0) {
						errores.put("EstadoCredito.error.desistirDesembolsado",
								"EstadoCredito.error.desistirDesembolsado");
					} else {
						if (this.agrega.longValue() == EstadosPersonas.agregaEstado.longValue()) {
							this.guardarNuevoEstado();
						} else if (this.agrega.longValue() == EstadosPersonas.agregaObs.longValue()) {
							this.guardarObservacion();
						}
						if (this.errores.isEmpty()) {
							this.forward = "CreditoEstadosList";
						} else {
							this.forward = "CreditoEstados";
						}
					}
				} else {
					errores.put("permiso.desestimiento.error", "permiso.desestimiento.error");
				}
			} else {
				if (this.agrega.longValue() == EstadosPersonas.agregaEstado.longValue()) {
					// el metodo guardarNuevoEstado resetea la variable de clase estado a un estado
					// vacio, por lo tanto el atributo nombre no se puede chequear
					if (estado.getNombreEstado().trim().equalsIgnoreCase("FINALIZADO")) {
						Date hoy = new Date();
						// actualizar todas las garantias a finalizadas tambien
						List<ObjetoiGarantia> ogs = bp.createQuery(
								"select og from ObjetoiGarantia og where og.objetoi.id = :id and og.baja is null")
								.setParameter("id", obj.getId()).list();
						for (ObjetoiGarantia og : ogs) {
							if (og.getTasacion() != null) {
								og.getTasacion().setFechaCancelacion(hoy);
								og.getTasacion().setNroCancelacion(numeroResolucion);
								bp.update(og.getTasacion());
							}
							GarantiaEstado nuevoEstado = new GarantiaEstado(ComportamientoGarantiaEnum.FINALIZADA, hoy);
							nuevoEstado.setObjetoiGarantia(og);
							nuevoEstado.determinarImporte();
							bp.save(nuevoEstado);
						}
					}

					this.guardarNuevoEstado();

				} else if (this.agrega.longValue() == EstadosPersonas.agregaObs.longValue()) {
					this.guardarObservacion();
				}
				if (this.errores.isEmpty()) {
					this.forward = "CreditoEstadosList";
				} else {
					this.forward = "CreditoEstados";
				}

			}

		} else if (this.accion.equals("cancelar")) {
			this.forward = "CreditoEstadosList";
		} else if (this.accion.equals("imprimirInforme")) {
			imprimirInforme();
			if (errores.isEmpty()) {
				forward = "ReportesProcess2";
			}
			return errores.isEmpty();
		}

		this.listarEstadosObjetoi();

		return this.errores.isEmpty();
	}

	public void imprimirInforme() {
		try {
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("informeDesestimiento.jasper");
			String href = SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
			href += "/actions/process.do?do=process&processName=CreditoEstados";
			href += "&process.objetoi.id=" + getObjetoi().getId();
			reporte.setHref(href);
			reporte.setParams("OBJETOI_ID=" + objetoi.getId() + ";RELACION_COTOMADOR_ID=" + relacionCotomadorId
					+ ";SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "informeDesestimiento"));
		}
	}

	public boolean verificarPermiso() {
		String user = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		boolean tienePermiso = false;

		Director d = null;
		try {
			d = DirectorHandler.getDirector("permiso.desistimiento");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Claves clave = (Claves) bp.getById(Claves.class, user);

		if (d != null) {
			@SuppressWarnings("unchecked")
			List<Permiso> permisos = bp.createQuery(
					"SELECT p FROM Permiso p WHERE p.Menu.id = :id AND (p.id.causerk = :user OR p.id.causerk = :perfil)")
					.setParameter("id", "permiso.desistimiento").setParameter("user", user)
					.setParameter("perfil", clave.getPerfilK()).list();

			if (permisos.size() > 0) {
				for (Permiso permiso : permisos) {

					if (permiso.getTipo() > 1) {
						tienePermiso = true;
					}
				}
			}
		}
		return tienePermiso;
	}

	/**
	 * Guarda una observación.-
	 */
	private void guardarObservacion() {
		try {
			objetoi = (Objetoi) this.bp.getById(Objetoi.class, objetoi.getId());
			observacion.setObservacion(detalleObservacion);
			observacion.setFecha(new Date(System.currentTimeMillis()));
			observacion.setObjetoi(objetoi);
			observacion.validate();
			errores.putAll(observacion.getChErrores());
			if (errores.isEmpty()) {
				bp.save(observacion);
				observacion = new ObservacionObjetoi();
				detalleObservacion = null;
				fecha = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("permiso.desestimiento.error", "permiso.desestimiento.error");
		}
	}

	// =====================UTILIDADES PRIVADAS=====================
	/*
	 * busca los estados del objetoi en particular
	 */
	@SuppressWarnings("unchecked")
	private void listarEstadosObjetoi() {
		estadosObservaciones = new ArrayList<EstaObs>();
		if (this.objetoi != null) {
			try {
				this.objetoi = (Objetoi) this.bp.getById(Objetoi.class, this.objetoi.getId());
				String consulta = "";
				consulta += "SELECT oie FROM ObjetoiEstado oie WHERE";
				consulta += " oie.objetoi.id=" + this.objetoi.getId() + " order By oie.fecha DESC";
				objetoiEstados = this.bp.getByFilter(consulta);
			} catch (Exception e) {
				this.objetoiEstados = new ArrayList<ObjetoiEstado>();
				e.printStackTrace();
			}
			EstaObs estaObs;
			for (ObjetoiEstado objetoiEstado : objetoiEstados) {
				estaObs = new EstaObs();
				estaObs.setEstaobs(objetoiEstado.getEstado().getNombreEstado());
				estaObs.setColor(objetoiEstado.getEstado().getColor());
				estaObs.setFecha(objetoiEstado.getFecha());
				estaObs.setFechaHasta(objetoiEstado.getFechaHasta());
				estaObs.setObservaciones(objetoiEstado.getObservacion());
				estaObs.setAreaResponsable(objetoiEstado.getAreaResponsable());
				estadosObservaciones.add(estaObs);
			}
			List<ObservacionObjetoi> observacionObjetois = bp
					.createQuery("SELECT o FROM ObservacionObjetoi o WHERE o.objetoi = :objetoi ORDER BY o.fecha DESC")
					.setEntity("objetoi", objetoi).list();
			for (ObservacionObjetoi observacionObjetoi : observacionObjetois) {
				estaObs = new EstaObs();
				estaObs.setObservaciones(observacionObjetoi.getObservacion());
				estaObs.setFecha(observacionObjetoi.getFecha());
				estadosObservaciones.add(estaObs);
			}
			Collections.sort(estadosObservaciones, new Comparator<EstaObs>() {
				@Override
				public int compare(EstaObs o1, EstaObs o2) {
					return o2.getFecha().compareTo(o1.getFecha());
				}
			});
		}
	}

	/**
	 * Actualiza el estado de la objetoi.-
	 */
	@SuppressWarnings("unchecked")
	private void guardarNuevoEstado() {
		estado = (Estado) bp.getById(Estado.class, estado.getId());
		objetoi = (Objetoi) bp.getById(Objetoi.class, objetoi.getId());

		// comentado por requerimiento #907
		// if(estado.getNombreEstado().equals("GESTION JUDICIAL") ||
		// estado.getNombreEstado().equals("GESTION EXTRAJUDICIAL")){
		// ObjetoiComportamiento comportamientoActual =
		// objetoi.getObjetoiComportamientoActual();
		// if(comportamientoActual != null &&
		// comportamientoActual.getComportamientoPago().equals("1")){
		// errores.put("EstadoCredito.error.gestionJudicial",
		// "EstadoCredito.error.gestionJudicial");
		// return;
		// }
		// }

		try {
			ObjetoiEstado objetoiEstado = new ObjetoiEstado();
			objetoiEstado.setObjetoi(this.objetoi);
			objetoiEstado.setEstado(this.estado);
			objetoiEstado.setFecha(new Date(System.currentTimeMillis()));
			objetoiEstado.setObservacion(this.obsEstado);
			this.validarNuevoEstado(objetoiEstado);
			if (this.errores.isEmpty()) {
				this.objetoi = (Objetoi) this.bp.getById(Objetoi.class, this.objetoi.getId());
				this.estado = (Estado) this.bp.getById(Estado.class, this.estado.getId());
				objetoiEstado.setObjetoi(this.objetoi);
				objetoiEstado.setEstado(this.estado);
				String consulta = "SELECT oie FROM ObjetoiEstado oie WHERE oie.fechaHasta IS NULL";
				consulta += " AND oie.objetoi.id=" + this.objetoi.getId();
				List<ObjetoiEstado> historicoEstados = this.bp.getByFilter(consulta);
				int i = historicoEstados.size();
				if ((estado.getNombreEstado()).trim().equalsIgnoreCase("DESISTIDO")) {
					boolean desimputacionOk = desimputar(this.objetoi.getId(),
							historicoEstados.get(historicoEstados.size() - i));
					if (!desimputacionOk) {
						errores.put("EstadoCredito.error.desimputar", "EstadoCredito.error.desimputar");
						return;
					}
					// this.bp.saveOrUpdate(objetoiEstado);
					// this.estado = new Estado();
					// this.obsEstado = null;
					// this.fecha = null;
					// return;
				}
				while (i > 0) {
					try {
						ObjetoiEstado ultimoEstado = historicoEstados.get(historicoEstados.size() - i);
						ultimoEstado.setFechaHasta(objetoiEstado.getFecha());
						this.bp.saveOrUpdate(ultimoEstado);
						i--;
					} catch (IndexOutOfBoundsException e) {
						System.out.println("El objeto " + this.objetoi.getId() + " no posee estados.....");
						e.printStackTrace();
					}
				}

				if (formFile != null && formFile.getFileSize() > 0) {
					ObjetoiArchivo objetoiArchivo = new ObjetoiArchivo();
					objetoiArchivo.setCredito(objetoi);
					objetoiArchivo.setArchivo(formFile.getFileData());
					objetoiArchivo.setMimetype(formFile.getContentType());
					objetoiArchivo.setNombre(formFile.getFileName());
					objetoiArchivo.setFechaAdjunto(new Date());
					objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
					bp.saveOrUpdate(objetoiArchivo);
				}

				this.bp.saveOrUpdate(objetoiEstado);
				this.estado = new Estado();
				this.obsEstado = null;
				this.fecha = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("EstaPer.error", "EstaPer.error");
		}
	}

	private boolean desimputar(Long idCredito, ObjetoiEstado oie) {
		ImputacionesHelper ih = new ImputacionesHelper();
		ih.setIdEstado(oie.getEstado().getId());
		ih.setIdCredito(idCredito);
		try {
			ih.desimputar();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Valida un cambio de estado.- 1) Validaciones generales de ObjetoiEstado.- 2)
	 * La Fecha debe ser posterior a la fecha del fstado Actual.-
	 */
	private void validarNuevoEstado(ObjetoiEstado objetoiEstado) {
		objetoiEstado.validate();
		this.errores.putAll(objetoiEstado.getChErrores());
		ObjetoiEstado estadoActual = this.objetoi.getEstadoActual();
		if (estadoActual != null) {
			if (objetoiEstado.getFecha().before(estadoActual.getFecha())) {
				this.errores.put("ObjetoiEstado.fecha.anterior", "ObjetoiEstado.fecha.anterior");
			}
		}
	}

	// ======================GETTERS Y SETTERS======================
	@Override
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Objetoi getObjetoi() {
		return this.objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getFechaStr() {
		if (this.getFecha() == null) {
			this.setFecha(new Date(System.currentTimeMillis()));
		}
		return DateHelper.getString(this.getFecha());
	}

	public void setFechaStr(String fecha) {
		if (fecha == null || fecha.length() == 0) {
			this.setFecha(null);
			return;
		}
		this.setFecha(DateHelper.getDate(fecha));
	}

	public String getObsEstado() {
		return this.obsEstado;
	}

	public void setObsEstado(String obsEstado) {
		this.obsEstado = obsEstado;
	}

	// ========================VALIDACIONES=========================
	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public List<ObjetoiEstado> getObjetoiEstados() {
		return objetoiEstados;
	}

	public void setObjetoiEstados(List<ObjetoiEstado> objetoiEstados) {
		this.objetoiEstados = objetoiEstados;
	}

	private Objetoi buscarObjetoi(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return (Objetoi) bp.getById(Objetoi.class, id);
	}

	public String getEstadoAcuerdo() {
		return estadoAcuerdo;
	}

	public void setEstadoAcuerdo(String estadoAcuerdo) {
		this.estadoAcuerdo = estadoAcuerdo;
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	public String getDetalleObservacion() {
		return detalleObservacion;
	}

	public void setDetalleObservacion(String detalleObservacion) {
		this.detalleObservacion = detalleObservacion;
	}

	public Long getAgrega() {
		return agrega;
	}

	public void setAgrega(Long agrega) {
		this.agrega = agrega;
	}

	public List<EstaObs> getEstadosObservaciones() {
		return estadosObservaciones;
	}

	public void setEstadosObservaciones(List<EstaObs> estadosObservaciones) {
		this.estadosObservaciones = estadosObservaciones;
	}

	public String getEstadoActual() {
		if (objetoi.getId() != null && objetoi.getId().longValue() != 0L) {
			objetoi = (Objetoi) bp.getById(Objetoi.class, objetoi.getId());
			return objetoi.getEstadoActual().getEstado().getNombreEstado().trim();
		}
		return null;
	}

	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

}