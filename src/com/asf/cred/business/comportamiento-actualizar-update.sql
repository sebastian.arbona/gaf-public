UPDATE ObjetoiComportamiento
SET fechaHasta = :fecha WHERE id in (
select t.idComportamiento
from (
select o.id, oc.id idComportamiento, oc.comportamientoPago, ccts.cantidadCuotas, DATEDIFF(dd, acts.fechaVencimientoAtraso, :fecha) atraso,
SUM(CASE WHEN cc.tipomov_id = 2 then cc.importe else -cc.importe end) saldo, 
SUM(CASE WHEN cc.tipomov_id = 2 AND cc.cuota_id = ct.id then cc.importe else 0 end) total
from Objetoi o
join Ctacte cc on cc.objetoi_id = o.id
join ObjetoiEstado oe on oe.objetoi_id = o.id and oe.fechaHasta is null
join Estado e on e.idEstado = oe.estado_idEstado
join Cuota ct on ct.credito_id = o.id and ct.id = (select min(id) from Cuota where credito_id = o.id and estado = '1' and capital > 0)
join ObjetoiComportamiento oc on oc.objetoi_id = o.id and oc.fechaHasta is null and oc.id = (
select MIN(id) FROM ObjetoiComportamiento oc WHERE oc.fechaHasta IS NULL AND oc.objetoi_id = o.id
)
left join (
select COUNT(*) cantidadCuotas, o.id idObjetoi, MIN(c.fechaVencimiento) as fechaVencimiento 
from Objetoi o join Cuota c on c.credito_id = o.id
where c.fechaVencimiento < :fecha and c.estado = '1'
group by o.id) ccts on o.id = ccts.idObjetoi
left join (
select dxc.idObjetoiAtraso, MIN(dxc.fechaVencimientoAtraso) as fechaVencimientoAtraso
from
(select o.id idObjetoiAtraso, c.fechaVencimiento as fechaVencimientoAtraso  
from Objetoi o join Cuota c on c.credito_id = o.id 
join Ctacte cc on c.id = cc.cuota_id 
where c.fechaVencimiento < :fecha and c.estado = '1'
group by o.id, c.fechaVencimiento
having SUM(CASE WHEN cc.tipomov_id = 2 then cc.importe else 0 end) = 0
or (SUM(CASE WHEN cc.tipomov_id = 2 then cc.importe else -cc.importe end) / SUM(CASE WHEN cc.tipomov_id = 2 then cc.importe else 0 end)) >= 0.1) dxc
group by dxc.idObjetoiAtraso) acts on o.id = acts.idObjetoiAtraso
where e.idestado not in (9,10,11,12,19,30)
group by o.id, oc.id, oc.comportamientoPago, ccts.cantidadCuotas, ccts.fechaVencimiento, acts.fechaVencimientoAtraso
) t 
where (t.atraso is null or t.saldo / t.total >= 0.1)
and (CASE WHEN atraso IS NULL OR atraso <= 60 THEN '1'
WHEN atraso <= 90 THEN '2'
WHEN atraso <= 180 THEN '3'
ELSE '4' END ) <> comportamientoPago
)