package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;

public class CobranzaProcess implements IProcess {

	private BusinessPersistance bp;
	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private Objetoi objetoi;
	private List<Pagos> pagos;
	private ReportResult reporte;
	private Pagos pago;
	// private String idBoleto;
	private Long idCaratula;
	private Long proyecto;
	private Long numeroAtencion;

	public CobranzaProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		forward = "cobranzasList";
		objetoi = (Objetoi) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("objetoiPagosProcess");
		if (objetoi == null) {
			objetoi = new Objetoi();
		}
		pagos = new ArrayList<Pagos>();
		pago = new Pagos();
		DirectorHelper.getString("SGAF");
	}

	@Override
	public boolean doProcess() {
		if (accion != null && accion.equals("imprimir")) {
			Objetoi objetoI = (Objetoi) bp.createQuery("SELECT o FROM Objetoi o WHERE o.numeroAtencion = :proyecto")
					.setLong("proyecto", this.numeroAtencion).uniqueResult();
			if (objetoI != null) {
				imprimir(objetoI);
				if (errores.isEmpty()) {
					forward = "ReportesProcess2";
				}
				return errores.isEmpty();
			} else {
				errores.put("imprimir.informe.cobranza.error", "imprimir.informe.cobranza.error");
			}
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void imprimir(Objetoi objetoI) {
		try {

			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("ReporteDeCobranzas.jasper");
			reporte.setParams("PROYECTO=" + objetoI.getNumeroAtencionStr() + ";TITULAR="
					+ objetoI.getPersona().getNomb12Str() + ";CUIT=" + objetoI.getPersona().getCuil12Str()
					+ ";NOMBRELINEA=" + objetoI.getLinea().getNombre() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("objetoiPagosProcess",
					objetoi);
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "ReporteDeCobranzas"));
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public List<Pagos> getPagos() {
		return pagos;
	}

	public void setPagos(List<Pagos> pagos) {
		this.pagos = pagos;
	}

	public Pagos getPago() {
		return pago;
	}

	public void setPago(Pagos pago) {
		this.pago = pago;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCartula) {
		this.idCaratula = idCartula;
	}

	public Long getProyecto() {
		return proyecto;
	}

	public void setProyecto(Long proyecto) {
		this.proyecto = proyecto;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

}
