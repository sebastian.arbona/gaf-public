package com.asf.cred.business;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.TipoGarantia;

public class GarantiasInventarioContable extends ConversationProcess {

	private static String sql;
	private String[] tiposGarantias;
	private int fechaProsValor = 1;
	private String consultaProcesoValor;
	private Date fecha;
	private List<GarantiasInventarioContableBean> resultado;
	private Long idMoneda;
	private String usuario;

	@SuppressWarnings({ "unchecked" })
	@ProcessMethod(defaultAction = true)
	public boolean ejecutar() {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		// a la fecha indicada le sumo 1 dia
		calendario.add(Calendar.DAY_OF_MONTH, 1);
		// retrocedo la fecha al primer milisegundo del dia
		calendario.setTime(DateHelper.resetHoraToZero(calendario.getTime()));
		// retrocedo el tiempo al 1 milisegundo, lo que me lleva al dia anterior
		calendario.add(Calendar.MILLISECOND, -1);

		String query = getSql().replaceAll(":fecha", new Timestamp(calendario.getTimeInMillis()).toString());
		if (tiposGarantias != null && tiposGarantias.length > 0) {
			query = query.replace(":idTipoGarantia", "1").replace(":tiposGarantias", getArrayString(tiposGarantias));
		} else {
			query = query.replace(":idTipoGarantia", "null").replace(":tiposGarantias", "0");
		}
		if (idMoneda != null) {
			query = query.replace(":idMoneda", idMoneda.toString());
		} else {
			query = query.replace(":idMoneda", "1");
		}
		switch (fechaProsValor) {
		case 1:
			consultaProcesoValor = "ge.fechaProceso = (select max(fechaProceso) from GarantiaEstado where objetoiGarantia_id = og.id and fechaProceso";
			break;
		case 2:
			consultaProcesoValor = "ge.fechaEstado = (select max(fechaEstado) from GarantiaEstado where objetoiGarantia_id = og.id and fechaEstado";
			break;
		}
		query = query.replace(":consultaProcesoValor", consultaProcesoValor);

		String excl = "SUSTITUIDA";
		if ("tribunalDeCuentas".equals(this.usuario)) {
			excl = excl + "," + DirectorHelper.getString("GarantiaInventarioContableHTC.Excluidas",
					"CANCELADA,EJECUTADA,VENCIDA");
		}
		String exclList[] = excl.split(",");
		query = query.replace(":estadosExcluidosHTC", this.getEstadosGarantiasHtcList(exclList));
		List<Object[]> result = bp.createSQLQuery(query).list();
		resultado = new ArrayList<GarantiasInventarioContableBean>();
		for (Object[] r : result) {
			GarantiasInventarioContableBean b = new GarantiasInventarioContableBean(r);
			resultado.add(b);
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public String getEstadosGarantiasHtcList(String exclList[]) {
		String sql = "select e from Estado e where e.tipo = 'Garantia' AND e.comportamiento NOT IN ( ''";
		for (String excl : exclList) {
			sql += ", '" + excl + "'";
		}
		sql += ") order by e.nombreEstado";
		List<Estado> estadoList = bp.getByFilter(sql);
		String rtnSql = " and ge.estado_idEstado IN (0";
		for (Estado estado : estadoList) {
			rtnSql += ", " + estado.getId();
		}
		rtnSql += ')';
		return rtnSql;
	}

	@Override
	protected String getDefaultForward() {
		return "GarantiasInventarioContable";
	}

	private static String getArrayString(String[] array) {
		return getArrayString(array, false);
	}

	private static String getArrayString(String[] array, boolean quote) {
		if (array == null || array.length == 0) {
			return "";
		}
		String r = "";
		for (String s : array) {
			r += (quote ? "'" : "") + s + (quote ? "'" : "") + ",";
		}
		r = r.substring(0, r.length() - 1);
		return r;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getFechaStr() {
		return DateHelper.getString(fecha);
	}

	public void setFechaStr(String f) {
		this.fecha = DateHelper.getDate(f);
	}

	public String[] getTiposGarantias() {
		return tiposGarantias;
	}

	public void setTiposGarantias(String[] tiposGarantias) {
		this.tiposGarantias = tiposGarantias;
	}

	@SuppressWarnings("unchecked")
	public List<TipoGarantia> getTiposGarantiasList() {
		return bp.getByFilter("select t from TipoGarantia t order by t.id");
	}

	public List<GarantiasInventarioContableBean> getResultado() {
		return resultado;
	}

	public void setResultado(List<GarantiasInventarioContableBean> resultado) {
		this.resultado = resultado;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(
						GarantiasInventarioContable.class.getResourceAsStream("garantias-inventario-contable.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public int getFechaProsValor() {
		return fechaProsValor;
	}

	public void setFechaProsValor(int fechaProsValor) {
		this.fechaProsValor = fechaProsValor;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}
