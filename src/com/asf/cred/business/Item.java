package com.asf.cred.business;

public class Item {
	private Object value;
	private String name;
	
	public Item(){		
	}
	
	public Item(String name, Object value){
		this.name=name;
		this.value=value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	

}
