package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.BonTasa;

public class TitularesBonTasa implements IProcess {
	private String forward;
	private String accion;
	private Object result;
	private Long idPersona;
	private com.nirven.creditos.hibernate.TitularesBonTasa titular;
	private List<TitularesBonTasa> titulares;
	private Long idBonTasa;
	private BonTasa bonificacion;
	private Long idTitular;
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	private HashMap<String, String> errors = new HashMap<String, String>();

	@Override
	public boolean doProcess() {
		if (accion == null) {
			buscarTitulares();
			forward = "TitularesBonTasa";
		} else if (accion.equalsIgnoreCase("eliminar")) {
			eliminar();
			buscarTitulares();
			forward = "TitularesBonTasa";
		} else if (accion.equalsIgnoreCase("Agregar")) {
			// validarAgregar();
			agregarTitular();
			buscarTitulares();
			forward = "TitularesBonTasa";
		}
		return errors.isEmpty();
	}

	private void validarAgregar() {
		if (idPersona != null) {
			if (idPersona != 0)
				errors.put("", "");
		}
	}

	private void agregarTitular() {
		try {
			if (idPersona != 0) {
				Persona persona = (Persona) bp.getById(Persona.class, idPersona);
				if (persona.getId() != null) {
					buscarBonTasa();
					if (comprobarTitular(persona)) {
						titular = new com.nirven.creditos.hibernate.TitularesBonTasa();
						titular.setBonTasa(bonificacion);
						titular.setPersona(persona);
						bp.save(titular);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean comprobarTitular(Persona persona) {
		List<TitularesBonTasa> titu = bp
				.createQuery("Select t from TitularesBonTasa t where t.persona=:persona and t.bonTasa =:bonTasa")
				.setParameter("persona", persona).setParameter("bonTasa", bonificacion).list();
		if (titu == null || titu.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	public void eliminar() {
		buscarBonTasa();
		List<com.nirven.creditos.hibernate.TitularesBonTasa> titu = bp
				.createQuery("Select t from TitularesBonTasa t where t.bonTasa=:bonTasa")
				.setParameter("bonTasa", bonificacion).list();
		if (titu.size() != 1) {
			com.nirven.creditos.hibernate.TitularesBonTasa titular = (com.nirven.creditos.hibernate.TitularesBonTasa) bp
					.getById(com.nirven.creditos.hibernate.TitularesBonTasa.class, idTitular);
			bp.delete(titular);
		}
	}

	private void buscarBonTasa() {
		bonificacion = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
	}

	private void buscarTitulares() {
		buscarBonTasa();
		titulares = bp.createQuery("Select t from TitularesBonTasa t where t.bonTasa=:bonTasa")
				.setParameter("bonTasa", bonificacion).list();
	}

	@Override
	public HashMap getErrors() {
		return errors;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return errors.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public com.nirven.creditos.hibernate.TitularesBonTasa getTitular() {
		return titular;
	}

	public void setTitular(com.nirven.creditos.hibernate.TitularesBonTasa titular) {
		this.titular = titular;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	public List<TitularesBonTasa> getTitulares() {
		return titulares;
	}

	public void setTitulares(List<TitularesBonTasa> titulares) {
		this.titulares = titulares;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public Long getIdTitular() {
		return idTitular;
	}

	public void setIdTitular(Long idTitular) {
		this.idTitular = idTitular;
	}

}
