package com.asf.cred.business;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.ActividadAgenda;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.Usuario;

public class AgendaProcess implements IProcess{
	
	private String action="list"; //new, load, list, save.
	private ActividadAgenda actividadAgenda=new ActividadAgenda();
	private HashMap<String, Object> errores;
	private List<ActividadAgenda> actividadesAgenda;
	private String forward;
	private Object result;
	private String listBy="objetoi"; //Rol, usuario, credito
	private Long idObjetoi;
	private FormFile formFile;
	private boolean pestana=true;
	private boolean mostrarTodos;
	

	@Override
	public boolean doProcess() {
		BusinessPersistance bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		if(action.equals("new")){
			this.actividadAgenda=new ActividadAgenda();
			this.actividadAgenda.setFechaCreacion(new Date());
			this.actividadAgenda.setObjetoi_id(this.idObjetoi);
			this.forward="ActividadAgenda";
			
		}else if(action.equals("load")){
			this.actividadAgenda=(ActividadAgenda)bp.getById(ActividadAgenda.class, actividadAgenda.getId());
			this.result=this.actividadAgenda;
			this.idObjetoi=this.actividadAgenda.getObjetoi().getId();
			this.forward="ActividadAgenda";
		}else if(action.equals("save")){
			this.action="list";
			this.actividadAgenda=this.save(this.actividadAgenda, this.idObjetoi);
			this.idObjetoi=this.actividadAgenda.getObjetoi().getId();
		}
		if(!pestana)
			idObjetoi=null;
		if(action.equals("list")){
			actividadesAgenda=getList(listBy, idObjetoi);
			this.result=actividadesAgenda;
			this.forward="ActividadAgendaList";
		}
		
		return true;
	}

	private ActividadAgenda save(ActividadAgenda aa, long idObjetoi) {
		ActividadAgenda aas;
		BusinessPersistance bp=com.asf.security.SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.errores=new HashMap<String, Object>();
		
		bp.begin();
		try{
			if(aa.getId()!=null&&aa.getId().longValue()>0){
				//update
				aas=(ActividadAgenda)bp.getById(ActividadAgenda.class, aa.getId());
			}else{
				//save new
				aas=new ActividadAgenda();
				aas.setObjetoi((Objetoi)bp.getById(Objetoi.class, idObjetoi));
				aas.setFechaCreacion(new Date());
				aas.setUsuarioCreador(Usuario.load(SessionHandler.getCurrentSessionHandler().getCurrentUser()));			
			}
			aas.setAnalisis(aa.getAnalisis());
			
			aas.setDetalle(aa.getDetalle());
			aas.setFechaCumplimiento(aa.getFechaCumplimiento());
			aas.setFechaVencimiento(aa.getFechaVencimiento());
			aas.setResponsable(aa.getResponsable());
			aas.setTipoActividadAgenda(aa.getTipoActividadAgenda());
			if(formFile!=null&&!formFile.getFileName().isEmpty()){
				ObjetoiArchivo objetoiArchivo=aas.getObjetoiArchivo();
				if(objetoiArchivo==null){
					objetoiArchivo=new ObjetoiArchivo();
					objetoiArchivo.setCredito(aas.getObjetoi());
					aas.setObjetoiArchivo(objetoiArchivo);
				}
				
			
				objetoiArchivo.setArchivo(formFile.getFileData());
				objetoiArchivo.setMimetype(formFile.getContentType());
				objetoiArchivo.setNombre(formFile.getFileName());
				objetoiArchivo.setFechaAdjunto(new Date());
				objetoiArchivo.setNoBorrar(true);
				objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
				bp.saveOrUpdate(objetoiArchivo);
			
			}
			
			
			if(!aa.validate())
				this.errores.putAll(aa.getChErrores());
			
			bp.saveOrUpdate(aas);
			bp.commit();
			return aas;
		}catch(Exception e){
			e.printStackTrace();
			bp.rollback();
		}
		return null;
	}

	private List<ActividadAgenda> getList(String listBy, Long id) {
		BusinessPersistance bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String mtq=mostrarTodos?"":" and a.fechaCumplimiento is null";
		if(listBy==null||listBy.equals("objetoi")&&id!=null){
			this.listBy="objetoi";
			return bp.createQuery("FROM "+ActividadAgenda.class.getCanonicalName()+" a where a.objetoi.id=:id"+mtq).setParameter("id", id).list();
		}
		//Listo para el recurso responsale
		if(id==null){
			this.pestana=false;
			this.listBy="responsable";
			return bp.createQuery("FROM "+ActividadAgenda.class.getCanonicalName()+" a where a.responsable.id=:id"+mtq).setParameter("id", SessionHandler.getCurrentSessionHandler().getCurrentUser()).list();
		}
		
		return null;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		if(pestana)
			return forward;
		else return forward+"2";
	}

	@Override
	public String getInput() {
		if(pestana)
			return forward;
		else return forward+"2";
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public ActividadAgenda getActividadAgenda() {
		return actividadAgenda;
	}

	public void setActividadAgenda(ActividadAgenda actividadAgenda) {
		this.actividadAgenda = actividadAgenda;
	}

	public List<ActividadAgenda> getActividadesAgenda() {
		return actividadesAgenda;
	}

	public void setActividadesAgenda(List<ActividadAgenda> actividadesAgenda) {
		this.actividadesAgenda = actividadesAgenda;
	}

	public String getListBy() {
		return listBy;
	}

	public void setListBy(String listBy) {
		this.listBy = listBy;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}
	
	public String getCurrentUser(){
		String str=SessionHandler.getCurrentSessionHandler().getCurrentUser();
		Usuario u=Usuario.load(str);
		if(u!=null&&u.getPersona()!=null)
			str+=" - "+u.getPersona().getNomb12Str();
		return str;
	}

	public boolean isPestana() {
		return pestana;
	}

	public void setPestana(boolean pestana) {
		this.pestana = pestana;
	}

	public boolean getMostrarTodos() {
		return mostrarTodos;
	}

	public void setMostrarTodos(boolean mostrarTodos) {
		this.mostrarTodos = mostrarTodos;
	}

}
