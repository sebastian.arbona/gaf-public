package com.asf.cred.business;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BonificacionesCobrarBean implements Serializable {
	private static final long serialVersionUID = -4423950639463616123L;

	// datos del proyecto
	private BigDecimal numeroAtencion;
	private String titularNombre;
	private BigDecimal titularCuit;

	private int cuotasTotales;
	private Double capitalAcreditado;
	private int cuotasPagadas;
	private Double saldoCapital;

	// datos de la cuota
	private int numeroCuota;
	private Date fechaVencimiento;
	private int diasVencida;
	private int diasAtraso;
	private Double bonificacion;
	private int cuotasImpagas;
	private BigDecimal estado;
	
	public BigDecimal getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(BigDecimal numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getTitularNombre() {
		return titularNombre;
	}

	public void setTitularNombre(String titularNombre) {
		this.titularNombre = titularNombre;
	}

	public BigDecimal getTitularCuit() {
		return titularCuit;
	}

	public void setTitularCuit(BigDecimal titularCuit) {
		this.titularCuit = titularCuit;
	}

	public int getCuotasTotales() {
		return cuotasTotales;
	}

	public void setCuotasTotales(int cuotasTotales) {
		this.cuotasTotales = cuotasTotales;
	}

	public Double getCapitalAcreditado() {
		return capitalAcreditado;
	}

	public void setCapitalAcreditado(Double capitalAcreditado) {
		this.capitalAcreditado = capitalAcreditado;
	}

	public int getCuotasPagadas() {
		return cuotasPagadas;
	}

	public void setCuotasPagadas(int cuotasPagadas) {
		this.cuotasPagadas = cuotasPagadas;
	}

	public Double getSaldoCapital() {
		return saldoCapital;
	}

	public void setSaldoCapital(Double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}

	public int getNumeroCuota() {
		return numeroCuota;
	}

	public void setNumeroCuota(int numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public int getDiasVencida() {
		return diasVencida;
	}

	public void setDiasVencida(int diasVencida) {
		this.diasVencida = diasVencida;
	}

	public int getDiasAtraso() {
		return diasAtraso;
	}

	public void setDiasAtraso(int diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	public Double getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(Double bonificacion) {
		this.bonificacion = bonificacion;
	}

	public int getCuotasImpagas() {
		return cuotasImpagas;
	}

	public void setCuotasImpagas(int cuotasImpagas) {
		this.cuotasImpagas = cuotasImpagas;
	}

	public BigDecimal getEstado() {
		return estado;
	}

	public void setEstado(BigDecimal estado) {
		this.estado = estado;
	}

}
