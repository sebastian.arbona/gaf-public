package com.asf.cred.business;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.civitas.hibernate.persona.PersonaVinculada;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.GarantiaUso;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class VerTitulares implements IProcess {

	private String accion;
	private String forward;
	private List<GarantiaUso> gusos = new ArrayList<GarantiaUso>();
	private boolean nuevo = true;
	private Double resto = 0.0;
	private Double porcentaje;
	private Persona persona;
	private Long idPersona;
	private Long id;
	private Long idGarantiaUso;
	private Garantia garantia;
	private boolean modifico = false;
	private GarantiaUso gu;
	private Long idObjetoi;
	private HashMap errores = new HashMap();
	private PersonaVinculada personaVinculada;
	private static final String DIRECTOR_RELACION_FIADOR = "relacion.fiador";

	public VerTitulares() {

		personaVinculada = new PersonaVinculada();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean doProcess() {
		forward = "verTitulares";

		if (accion == null) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			obtenerGarantia();
			listar();
			comprobarGarantiaUsos();
			nuevo = true;
			forward = "verTitulares";
		} else if (accion.equalsIgnoreCase("Nuevo")) {
			buscarTitulares();
			nuevo = false;
			forward = "verTitulares";
		} else if (accion.equalsIgnoreCase("Volver")) {

			forward = "ListarGarantia";
		} else if (accion.equalsIgnoreCase("guardar")) {
			nuevoTitular();
			nuevo = true;
			accion = null;
			forward = "verTitulares";
			obtenerGarantia();
			listar();
		} else if (accion.equalsIgnoreCase("eliminar")) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			gu = (GarantiaUso) bp.getById(GarantiaUso.class, idGarantiaUso);
			if (gu.getPorcentaje() != null && gu.getPorcentaje() != 0.0) {
				errores.put("verTitulares.eliminarPorcentaje", "verTitulares.eliminarPorcentaje");
			} else {
				bp.delete(gu);
			}
			obtenerGarantia();
			listar();
			accion = null;
			forward = "verTitulares";
		} else if (accion.equalsIgnoreCase("habilitarMod")) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			obtenerGarantia();
			listar();
			comprobarGarantiaUsos();
			gu = (GarantiaUso) bp.getById(GarantiaUso.class, idGarantiaUso);
			buscarTitularesMod();
			modifico = true;
			nuevo = true;
			accion = null;
			forward = "verTitulares";
		} else if (accion.equalsIgnoreCase("modificar")) {
			try {
				modificarTitularidad();
				nuevo = true;
				modifico = false;
				forward = "verTitulares";
				obtenerGarantia();
				listar();
			} catch (Exception e) {
				errores.put(e.getMessage(), e.getMessage());
			}
		} else if (accion.equalsIgnoreCase("cancelar")) {
			obtenerGarantia();
			listar();
			forward = "verTitulares";
		}
		return false;
	}

	public void buscarTitulares() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ObjetoiGarantia oGar = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id);
		garantia = oGar.getGarantia();
		List<GarantiaUso> garusos = bp.createQuery("select gu from GarantiaUso gu where garantia= :garantia")
				.setParameter("garantia", garantia).list();
		Double total = 0.0;
		for (GarantiaUso garantiaUso : garusos) {
			total += garantiaUso.getPorcentaje();
		}
		resto = 1 - total;
	}

	public void buscarTitularesMod() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		garantia = (Garantia) bp.getById(Garantia.class, id);
		List<GarantiaUso> garusos = bp
				.createQuery("select gu from GarantiaUso gu where garantia= :garantia and gu!= :garantiaUso")
				.setParameter("garantia", garantia).setParameter("garantiaUso", gu).list();
		Double total = 0.0;
		for (GarantiaUso garantiaUso : garusos) {
			total += garantiaUso.getPorcentaje();
		}
		resto = 1 - total;
	}

	public void listar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		gusos = bp.createQuery("select gu from GarantiaUso gu where gu.garantia = :garantia")
				.setParameter("garantia", garantia).list();
	}

	public void modificarTitularidad() throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		garantia = (Garantia) bp.getById(Garantia.class, id);
		List<GarantiaUso> garusos = bp
				.createQuery("select gu from GarantiaUso gu where garantia= :garantia and gu!= :garantiaUso")
				.setParameter("garantia", garantia).setParameter("garantiaUso", gu).list();
		Double total = 0.0;
		for (GarantiaUso garantiaUso : garusos) {
			if (!garantiaUso.getId().equals(idGarantiaUso)) {
				total += garantiaUso.getPorcentaje();
			}
		}

		if (total + porcentaje > 1) {
			throw new Exception("verTitulares.porcentaje");
		}

		gu = (GarantiaUso) bp.getById(GarantiaUso.class, idGarantiaUso);
		gu.setPorcentaje(porcentaje);
		bp.update(gu);
	}

	public void obtenerGarantia() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ObjetoiGarantia oGar = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id);
		garantia = oGar.getGarantia();
	}

	public void nuevoTitular() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ObjetoiGarantia oGar = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id);
		garantia = oGar.getGarantia();
		persona = (Persona) bp.getById(Persona.class, idPersona);
		GarantiaUso gu = new GarantiaUso(garantia, porcentaje, "", persona);
		bp.save(gu);

		personaVinculada.setPersonaTitular(oGar.getObjetoi().getPersona());
		personaVinculada.setIdObjetoi(oGar.getObjetoi().getId());
		personaVinculada.setNumeroAtencion(oGar.getObjetoi().getNumeroAtencion());
		personaVinculada.setPersonaVinculada_id(persona.getId());

		Long idRelacionCotomador;
		try {
			idRelacionCotomador = new Long(DirectorHandler.getDirector(DIRECTOR_RELACION_FIADOR).getValor());
			personaVinculada.setRelacion_id(idRelacionCotomador);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		bp.save(personaVinculada);
	}

	public void comprobarGarantiaUsos() {
		for (GarantiaUso gar : gusos) {
			if (gar.getPersona() == null) {
				gar.setPersona(new Persona());
			}
		}
	}

	private String parseToPercent(Double percent) {
		DecimalFormat df = new DecimalFormat("#.##");
		return (percent != null ? new String(df.format(percent) + "%") : new String("0%"));
	}

	private Double parsePercent(String percent) {
		return new Double(percent.replaceAll(",", ".").replaceAll("%", ""));
	}

	public String getPorcentajeStr() {
		return parseToPercent((porcentaje != null ? porcentaje * 100 : 0));
	}

	public void setPorcentajeStr(String porcentaje) {
		this.porcentaje = parsePercent(porcentaje) / 100;
	}

	public String getRestoStr() {

		return parseToPercent((resto != null ? resto * 100 : 0));
	}

	public void setRestoStr(String resto) {
		this.resto = parsePercent(resto) / 100;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	public Long getIdGarantiaUso() {
		return idGarantiaUso;
	}

	public void setIdGarantiaUso(Long idGarantiaUso) {
		this.idGarantiaUso = idGarantiaUso;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public List<GarantiaUso> getGusos() {
		return gusos;
	}

	public void setGusos(List<GarantiaUso> gusos) {
		this.gusos = gusos;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public boolean isNuevo() {
		return nuevo;
	}

	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}

	public Double getResto() {
		return resto;
	}

	public void setResto(Double resto) {
		this.resto = resto;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Garantia getGarantia() {
		return garantia;
	}

	public void setGarantia(Garantia garantia) {
		this.garantia = garantia;
	}

	public boolean isModifico() {
		return modifico;
	}

	public void setModifico(boolean modifico) {
		this.modifico = modifico;
	}

	public GarantiaUso getGu() {
		return gu;
	}

	public void setGu(GarantiaUso gu) {
		this.gu = gu;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}
}
