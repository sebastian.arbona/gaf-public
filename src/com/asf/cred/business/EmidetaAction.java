package com.asf.cred.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.AbmAction;
import com.asf.struts.form.AbmForm;
import com.asf.struts.form.ListForm;
import com.asf.util.DoubleHelper;
import com.nirven.creditos.hibernate.Emision;

public class EmidetaAction extends AbmAction {

	@Override
	@SuppressWarnings("unchecked")
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		AbmForm oAForm = null;
		ListForm oLForm = null;
		String fecha = new String();
		boolean facturada = false;
		if (form == null) {
			oLForm = new ListForm();
		} else {
			if (form instanceof AbmForm) {
				oAForm = (AbmForm) form;
				oLForm = new ListForm();
				oLForm.setEntityName(oAForm.getEntityName());
			} else {
				oLForm = (ListForm) form;
			}
		}

		oLForm.setEntityName("Emideta");

		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		java.util.List oList = new java.util.ArrayList();

		Long idEmision = 0L;
		try {
			idEmision = new Long(request.getParameter("idEmision"));
			oList = oBp.createQuery("SELECT new com.asf.cred.business.EmidetaListDTO(e, c.fechaVencimiento)"
					+ " FROM com.nirven.creditos.hibernate.Emideta AS e, com.nirven.creditos.hibernate.Cuota AS c"
					+ " WHERE e.credito.id = c.credito.id AND e.numero = c.numero AND e.emision.id = " + idEmision
					+ " ORDER BY e.credito.numeroAtencion").list();
			Emision emi = (Emision) oBp.getById(Emision.class, idEmision);
			fecha = emi.getFiltroFechaVencimiento();
			facturada = emi.getFacturada();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("generacion.idEmision",
					idEmision);
		} catch (Exception e) {

		}

		oLForm.setResult(oList);

		Double totalImporteCuota = new Double(0);
		Double totalCapital = new Double(0);
		Double totalIntCompensatorio = new Double(0);
		Double totalIvaCompensatorio = new Double(0);
		Double totalTasaCompensatorio = new Double(0);
		Double totalBonificacion = new Double(0);
		Double totalIvaBonificacion = new Double(0);
		Double totalGastos = new Double(0);
		Double totalIvaGastos = new Double(0);
		Double totalGastosRecuperar = new Double(0);
		Double totalIvaGastosRecuperar = new Double(0);

		for (EmidetaListDTO e : (List<EmidetaListDTO>) oList) {
			totalImporteCuota += e.getEmideta().getImporteCuota() != null ? e.getEmideta().getImporteCuota()
					: new Double(0);
			totalCapital += e.getEmideta().getCapital() != null ? e.getEmideta().getCapital() : new Double(0);
			totalIntCompensatorio += e.getEmideta().getCompensatorio() != null ? e.getEmideta().getCompensatorio()
					: new Double(0);
			totalIvaCompensatorio += e.getEmideta().getIvaCompensatorio() != null ? e.getEmideta().getIvaCompensatorio()
					: new Double(0);

			totalTasaCompensatorio += e.getEmideta().getTasaCompensatorio() != null
					? e.getEmideta().getTasaCompensatorio()
					: new Double(0);
			totalBonificacion += e.getEmideta().getBonificacion() != null && !e.getEmideta().isBajaEstadoBonificacion()
					? e.getEmideta().getBonificacion()
					: new Double(0);
			totalIvaBonificacion += e.getEmideta().getIvaBonificacion() != null
					&& !e.getEmideta().isBajaEstadoBonificacion() ? e.getEmideta().getIvaBonificacion() : new Double(0);

			totalGastos += e.getEmideta().getGastos() != null ? e.getEmideta().getGastos() : new Double(0);
			totalIvaGastos += e.getEmideta().getIvaGastos() != null ? e.getEmideta().getIvaGastos() : new Double(0);

			totalGastosRecuperar += e.getEmideta().getGastosRecuperar() != null ? e.getEmideta().getGastosRecuperar()
					: new Double(0);

			// totalIvaGastosRecuperar += e.getEmideta().getIvaGastosRecuperar() != null
			// ? e.getEmideta().getIvaGastosRecuperar()
			// : new Double(0);

		}

		request.setAttribute("ListResult", oLForm);
		request.setAttribute("fecha", fecha);
		request.setAttribute("facturada", facturada);
		request.setAttribute("idEmision", idEmision);

		request.setAttribute("totalImporteCuota", DoubleHelper.getStringAsCurrency(totalImporteCuota));
		request.setAttribute("totalCapital", DoubleHelper.getStringAsCurrency(totalCapital));

		request.setAttribute("totalIntCompensatorio", DoubleHelper.getStringAsCurrency(totalIntCompensatorio));
		request.setAttribute("totalIvaCompensatorio", DoubleHelper.getStringAsCurrency(totalIvaCompensatorio));
		request.setAttribute("totalTasaCompensatorio", DoubleHelper.getStringAsCurrency(totalTasaCompensatorio));

		request.setAttribute("totalBonificacion", DoubleHelper.getStringAsCurrency(totalBonificacion));
		request.setAttribute("totalIvaBonificacion", DoubleHelper.getStringAsCurrency(totalIvaBonificacion));

		request.setAttribute("totalGastos", DoubleHelper.getStringAsCurrency(totalGastos));
		request.setAttribute("totalIvaGastos", DoubleHelper.getStringAsCurrency(totalIvaGastos));

		request.setAttribute("totalGastosRecuperar", DoubleHelper.getStringAsCurrency(totalGastosRecuperar));
		request.setAttribute("totalIvaGastosRecuperar", DoubleHelper.getStringAsCurrency(totalIvaGastosRecuperar));
		return mapping.findForward("EmidetaList");
	}

}
