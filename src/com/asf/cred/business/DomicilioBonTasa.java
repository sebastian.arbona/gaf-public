package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Domicilio;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.BonTasa;

public class DomicilioBonTasa implements IProcess {
	// =========================ATRIBUTOS==================================
    private String forward = "domicilioBonTasaList";
    private HashMap<String, Object> errors;
    private String accion = "";
    private BusinessPersistance bp;
    private Persona personaTitular;
    private Domicilio domicilio;
    private List<Domicilio> listaDomicilios;
    private List<com.nirven.creditos.hibernate.DomicilioBonTasa> listaDomiciliosBonTasa;
    private Long idpais;
    private BonTasa bonTasa;
    private Long idBonTasa;
    private com.nirven.creditos.hibernate.DomicilioBonTasa domicilioBonTasa;
	// =========================CONSTRUCTORES================================
    public DomicilioBonTasa() {
        this.errors = new HashMap<String, Object>();
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        this.domicilio = new Domicilio();
        this.personaTitular = new Persona();
        listaDomicilios = new ArrayList<Domicilio>();
        listaDomiciliosBonTasa = new ArrayList<com.nirven.creditos.hibernate.DomicilioBonTasa>();
        bonTasa = new BonTasa();
    }

    // =========================FUNCIONALIDAD==================================
    public boolean doProcess() {
        if (this.getAccion().equalsIgnoreCase("asignar")) {
            this.forward = "DomicilioBonTasa";
        } else if (this.getAccion().equalsIgnoreCase("guardar")) {
            this.validarDomicilio();
            if (this.errors.isEmpty()) {
                this.asignarDomicilio();
                if (this.errors.isEmpty()) {
                    this.forward = "DomicilioBonTasaList";
                } else {
                    this.forward = "DomicilioBonTasa";
                }
            	buscarBonTasa();
                this.listarDomicilios();
            } else {
                this.forward = "DomicilioBonTasa";
            }
        } else if (this.getAccion().equalsIgnoreCase("cancelar")) {
        	domicilioBonTasa = null;
            this.forward = "DomicilioBonTasaList";
            buscarBonTasa();
            this.listarDomicilios();
        } else if (this.getAccion().equalsIgnoreCase("eliminar")) {
            this.eliminarDomicilio();
            this.listarDomicilios();
        } else if (this.getAccion().equalsIgnoreCase("load")) {
            this.cargarDomicilio();
            this.forward = "DomicilioBonTasa";
        } else if (this.getAccion().equalsIgnoreCase("listar")) {
        	buscarBonTasa();
        	listarDomicilios();
        	forward="DomicilioBonTasaList";
        } else if(accion.equalsIgnoreCase("nuevo")){
        	forward="NuevoDomBonTasa";
        }
        
        return this.errors.isEmpty();
    }

    // =========================UTILIDADES PRIVADAS==============================
    private void validarDomicilio() {
        if (("").equalsIgnoreCase(this.getDomicilio().getProvinciaStr())) {
            this.errors.put("Domicilio.Provincia.vacia", "Domicilio.Provincia.vacia");
        }
        if (("").equalsIgnoreCase(this.getDomicilio().getLocalidadStr())) {
            this.errors.put("Domicilio.Localidad.vacia", "Domicilio.Localidad.vacia");
        }
        if (("").equalsIgnoreCase(this.getDomicilio().getCalleStr())) {
            this.errors.put("Domicilio.Calle.vacia", "Domicilio.Calle.vacia");
        }
        if (this.getDomicilio().getTipo() == null) {
            this.errors.put("Domicilio.Tipo.vacio", "Domicilio.Tipo.vacio");
        }
    }
    
    private void buscarBonTasa(){
    	bonTasa= (BonTasa) bp.getById(BonTasa.class, idBonTasa);
    }
    
    private void asignarDomicilio() {
        try {
            if(domicilio.getId() == 0) {
            	domicilioBonTasa = new com.nirven.creditos.hibernate.DomicilioBonTasa();
            	/*String consulta = "SELECT o FROM BonTasa o " +
									"WHERE o.id = " + bonTasa.getId().longValue();
            	bonTasa = (BonTasa )bp.getByFilter(consulta).get(0);*/
            	buscarBonTasa();
            	domicilioBonTasa.setBonTasa(bonTasa);
            	domicilioBonTasa.setDomicilio(domicilio);
            	bp.save(domicilioBonTasa);
            } else {
            	bp.update(domicilio);
            }
            this.setDomicilio(new Domicilio());
            domicilioBonTasa = null;
        }
        catch (Exception e) {
            this.errors.put("Domicilio.Error.guardar", "Domicilio.Error.guardar");
            e.printStackTrace();
        }
    }

    private void eliminarDomicilio() {
        if (domicilio.getId() != null) {
            try {
            	String consulta = "SELECT d FROM DomicilioBonTasa d";
                consulta += " WHERE d.domicilio = " + domicilio.getId().longValue();
                domicilioBonTasa = (com.nirven.creditos.hibernate.DomicilioBonTasa) bp.getByFilter(consulta).get(0);
                bp.delete(domicilioBonTasa);
            }
            catch (Exception e) {
                this.errors.put("Domicilio.Error.eliminar", "Domicilio.Error.eliminar");
                e.printStackTrace();
            }
        }
    }

    private void cargarDomicilio() {
        if (getDomicilio() != null) {
            try {
                setDomicilio((Domicilio) bp.getById(Domicilio.class, getDomicilio().getId()));
            }
            catch (Exception e) {
                this.setDomicilio(new Domicilio());
                this.errors.put("Domicilio.Error.cargar", "Domicilio.Error.carga");
                e.printStackTrace();
            }
        }
    }

    private void listarDomicilios() {
    	if (bonTasa != null && bonTasa.getId() != null && bonTasa.getId().longValue() > 0) {
            try {
            	String consulta2 = "SELECT o FROM BonTasa o " +
            						"WHERE o.id = " + bonTasa.getId().longValue();
            	bonTasa = (BonTasa )bp.getByFilter(consulta2).get(0);
                /*String consulta = "SELECT d FROM DomicilioBonTasa d";
                consulta += " WHERE d.BonTasa.id = " + bonTasa.getId().longValue();
                listaDomiciliosBonTasa = bp.getByFilter(consulta);*/
            	listaDomiciliosBonTasa= bp.createQuery("Select d from DomicilioBonTasa d where d.bonTasa= :bonTasa").setParameter("bonTasa",bonTasa).list();
                listaDomicilios.clear();
                for(com.nirven.creditos.hibernate.DomicilioBonTasa domicilioBonTasa : listaDomiciliosBonTasa) {
                	if(domicilioBonTasa.getDomicilio() != null) {
                		listaDomicilios.add(domicilioBonTasa.getDomicilio());
                	}
                }
            }
            catch (Exception e) {
            	e.printStackTrace();
            }
        }	
    }

    // =========================GETTERS y SETTERS==================================
    public HashMap<String, Object> getErrors() {
        return this.errors;
    }

    public Object getResult() {
        return null;
    }

    public String getInput() {
        return this.forward;
    }

    public String getForward() {
        return this.forward;
    }

    public String getAccion() {
        return this.accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Domicilio getDomicilio() {
        return this.domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public Persona getPersonaTitular() {
        return this.personaTitular;
    }

    public void setPersonaTitular(Persona personaTitular) {
        this.personaTitular = personaTitular;
    }

    public List<Domicilio> getListaDomicilios() {
        return this.listaDomicilios;
    }

    public void setListaDomicilios(List<Domicilio> listaDomicilios) {
        this.listaDomicilios = listaDomicilios;
    }

    public Long getIdpais() {
        return this.idpais;
    }

    public void setIdpais(Long idpais) {
        this.idpais = idpais;
    }

	public void setBonTasa(BonTasa bonTasa) {
		this.bonTasa = bonTasa;
	}

	public BonTasa getBonTasa() {
		return bonTasa;
	}

    public com.nirven.creditos.hibernate.DomicilioBonTasa getDomicilioBonTasa() {
		return domicilioBonTasa;
	}

	public void setDomicilioBonTasa(
			com.nirven.creditos.hibernate.DomicilioBonTasa domicilioBonTasa) {
		this.domicilioBonTasa = domicilioBonTasa;
	}
	

	public List<com.nirven.creditos.hibernate.DomicilioBonTasa> getListaDomiciliosBonTasa() {
		return listaDomiciliosBonTasa;
	}

	public void setListaDomiciliosBonTasa(
			List<com.nirven.creditos.hibernate.DomicilioBonTasa> listaDomiciliosBonTasa) {
		this.listaDomiciliosBonTasa = listaDomiciliosBonTasa;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	// =========================VALIDACIONES==================================
    public boolean validate() {
        return this.errors.isEmpty();
    }
}
