select CASE WHEN ccon.concepto = 'bon' THEN 'Compensatorio' ELSE ccon.detalle END,
SUM(CASE WHEN c.fechaProceso < :fechaDesde THEN CASE WHEN c.tipomov_id = 2 THEN c.importe ELSE -c.importe END ELSE 0 END) as saldoAnterior,
SUM(CASE WHEN c.fechaProceso >= :fechaDesde AND c.tipomov_id = 2 THEN c.importe ELSE 0 END) as debitos,
SUM(CASE WHEN b.tipo is not null AND b.tipo = 'Recibo' THEN 
      CASE WHEN c.fechaProceso >= :fechaDesde AND c.tipomov_id = 1 THEN c.importe ELSE 0 END
    ELSE 0 END) creditospagos,
SUM(CASE WHEN b.tipo is null OR b.tipo <> 'Recibo' THEN 
      CASE WHEN c.fechaProceso >= :fechaDesde AND c.tipomov_id = 1 THEN c.importe ELSE 0 END
    ELSE 0 END) creditos
from Ctacte c
join Concepto con on c.asociado_id = con.id
join Cconcepto ccon on con.concepto_concepto = ccon.concepto
join Objetoi o on c.objetoi_id = o.id
join Linea l on o.linea_id = l.id
join ObjetoiEstado oe on oe.objetoi_id = o.id and oe.fechaHasta is null 
left join Boleto b on c.boleto_id = b.id
where c.fechaProceso <= :fechaHasta
and l.moneda_idmoneda = :idMoneda
and oe.estado_idEstado in (:idsEstados)
group by CASE WHEN ccon.concepto = 'bon' THEN 'com' ELSE ccon.concepto END,
CASE WHEN ccon.concepto = 'bon' THEN 'Compensatorio' ELSE ccon.detalle END