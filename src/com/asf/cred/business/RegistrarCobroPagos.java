package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.struts.form.AbmFormCaratula;
import com.asf.gaf.hibernate.Cotizacion;
import com.asf.gaf.hibernate.Moneda;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.MovIngresosVarios;
import com.nirven.creditos.hibernate.Pagos;

public class RegistrarCobroPagos implements IProcess {

	private List<Object> lResult = new ArrayList<Object>();
	private BusinessPersistance bp = null;
	private Caratula caratula;
	private Long idcaratula;
	/**
	 * Fecha de envio
	 */
	private Date fenv12;
	/**
	 * Codigo de grupo
	 */
	private Long codi12;
	/**
	 * Total de comprobantes
	 */
	private Long tcomp12 = new Long(0);
	/**
	 * Detalle del banco
	 */
	private String detaba;
	/**
	 * Fecha de cobro
	 */
	private String fcob12;
	/**
	 * Importe total
	 */
	private Double timp12 = new Double(0);
	/**
	 * Lista de boletos
	 */
	private String[] lista = new String[] {};
	/**
	 * Lista de impuesto
	 */
	private String[] impuesto = new String[] {};
	private Long idMoneda;
	private Double cotizacion;
	/* Formulario CobroPago */
	/**
	 * medio de pago seleccionado
	 */
	private Long idmediopago;
	/**
	 * Descripción de la entidad emisora
	 */
	private String entidademisora;
	/**
	 * Detalle del nro. de comprobante
	 */
	private String nrocomprobante;
	/**
	 * Detalle del plan
	 */
	private String plan;
	/**
	 * Datos de autorización
	 */
	private String autorizacion;
	/**
	 * Fecha de emision
	 */
	private Date femision;
	/**
	 * Fecha de vencimiento
	 */
	private Date fvencimiento;
	/**
	 * Tipo de medio de pago
	 */
	private String tipomedio;
	private Long cuotas, nrolote;
	/**
	 * Importe pagado
	 */
	private Double importe;
	private StrutsArray cobroPagos = new StrutsArray(Cobropago.class);
	private boolean save;
	private List<Boleto> boletos = new ArrayList<Boleto>();

	private Double impoTotal = 0D;
	/**
	 * Total a Cobrar
	 */
	private Double aCobrar = 0D;
	/**
	 * Diferencia entre el total cobrado y el total de los boletos
	 */
	private Double difTotal = 0D;
	private Long lote;
	private Long recaudId = 0L;
	private Long movIngresosVariosId = 0L;
	private String recaudFecha = "";
	private Double recaudImporte = 0D;
	private Double recaudSaldo = 0D;
	private Integer metodoCarga;
	private String forward;
	private Date fechaCalculoFecovita;

	public RegistrarCobroPagos() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		forward = "RegistrarCobroPagos";
		caratula = new Caratula();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		// Estado del mediocobro = 1 -> Informado.
		// qBoleto =
		// bp.createQuery("FROM Boletomediocobro b where b.id.codi09 = ? and b.id.digi09
		// = ? and b.id.peri09 = ? and b.Mediocobro.fechapago is null and
		// b.Mediocobro.idcaratula is null and b.Mediocobro.estado = 1");

		if (idcaratula != null) {
			caratula = (Caratula) bp.getById(Caratula.class, idcaratula);
			codi12 = caratula.getGrupo();
			detaba = (caratula.getBanco() != null) ? caratula.getBanco().getDetaBa() : "";
			fcob12 = DateHelper.getString(caratula.getFechaCobranza());

		}

		if (isSave()) {
			bp.begin();

			try {
				List<String> lstTemp = new ArrayList<String>();
				for (int i = 0; i < lista.length; i++) {
					lstTemp.add(lista[i]);
				}
				List<String> lstAgregados = new ArrayList<String>();

				String[] oBoletos = new AbmFormCaratula().getBoletos();
				boolean bExiste = false;
				for (int i = 0; i < oBoletos.length; i++) {
					String[] sBoleto = oBoletos[i].split("\\$");
					bExiste = false;

					for (String sKey : lstTemp) {
						if (sKey.split("\\$")[0].trim().equals(sBoleto[0].trim())) {
							bExiste = true;
							break;
						}
					}

					if (!bExiste) {
						lstAgregados.add(oBoletos[i]);
					}
				}

				lstTemp.addAll(lstAgregados);
				lista = lstTemp.toArray(lista);

				/* Actualiza total de comprobantes e importe total de Caratula */

				// modificaciones necesarias para que funcione correctamente con
				// los cambios del ticket 273
				if (caratula.getComprobantes() == null) {
					caratula.setComprobantes(0L);
				}
				if (caratula.getImporte() == null) {
					caratula.setImporte(0D);
				}
				// Fin modificaciones ticket 273
				caratula.setComprobantes(caratula.getComprobantes() + this.tcomp12);
				caratula.setImporte(caratula.getImporte() + this.timp12);

				// por procesamiento, la moneda se saca de la recaudacion
				if (metodoCarga == 2) {
					Recaudacion rec = (Recaudacion) bp.getById(Recaudacion.class, recaudId);
					idMoneda = rec.getIdMoneda();
				}

				// por Pago a cuenta, la moneda se saca desde ???
				if (metodoCarga == 3) {
					MovIngresosVarios mov = (MovIngresosVarios) bp.getById(MovIngresosVarios.class,
							movIngresosVariosId);
					idMoneda = mov.getDeudoresVarios().getMoneda().getId();
				}

				// Busco el ultimo valor de la cotizacion para la moneda
				List<Cotizacion> cotis = bp
						.createQuery("Select c from Cotizacion c where c.moneda.id=:moneda order by c.fechaDesde")
						.setParameter("moneda", idMoneda).list();
				Cotizacion coti = new Cotizacion();
				if (cotis.size() != 0) {
					coti = cotis.get(0);
				}
				for (Cotizacion cotizacion : cotis) {
					if (cotizacion.getFechaHasta() == null) {
						coti = cotizacion;
					} else {
						if (coti.getFechaHasta() != null && coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
							coti = cotizacion;
						}
					}
				}

				caratula.setCotiza(coti.getVenta());
				bp.saveOrUpdate(caratula);

				Moneda moneda = (Moneda) bp.getById(Moneda.class, idMoneda);

				CreditoHandler ch = new CreditoHandler();

				/* Genera Pago */
				for (int i = 0; i < lista.length; i++) {
					String boletoItem = lista[i].split("-")[0];
					int index = boletoItem.indexOf(' ');
					if (index != -1) {
						Long idBoleto = new Long(boletoItem.substring(0, index));
						// Busco el Boleto por id
						Boleto boleto = (Boleto) bp.getById(Boleto.class, idBoleto);
						boletos.add(boleto);
						// desde el primer espacio
						String datosBoleto = boletoItem.substring(index).trim();
						if (!datosBoleto.contains("$")) {
							// Esto es para los boletos que son cargados de
							// manera automatica
							datosBoleto += " $" + boleto.getImporte();
						}
						index = datosBoleto.indexOf('$');
						// el importe viene ingresado manualmente de la pantalla
						Double importeManual = new Double(datosBoleto.substring(index + 1));
						// importe Total de la lista de boletos..
						impoTotal += importeManual;
						Long idMonedaCredito = boleto.getObjetoi().getMoneda_id() != null
								? boleto.getObjetoi().getMoneda_id()
								: boleto.getObjetoi().getLinea().getMoneda_id();
						if (idMonedaCredito == null) {
							idMonedaCredito = 1L;
						}
						double cotizacion = ch.getCotizacion(idMoneda, idMonedaCredito, caratula.getFechaCobranza());
						double importeConvertido = importeManual;
						if (idMoneda != null && !idMoneda.equals(idMonedaCredito)) {
							importeConvertido = importeConvertido / cotizacion;
						}
						GenerarPagos generarPagos = new GenerarPagos(bp);
						generarPagos.generar(boleto, caratula, importeConvertido);
						Pagos pago = generarPagos.getPagos();
						if (caratula.getMetodoCarga().intValue() == 0) {
							Date fechaPago = new AbmFormCaratula().getFechaPago(boleto);
							pago.setFechaPago(fechaPago);
						}
						pago.setMoneda(moneda);
						pago.setMontoOriginal(importeManual);
						pago.setCotizacion(cotizacion);
						bp.update(pago);
					}
				}

				/*
				 * Registra el pago del boleto en Cobropago, Bolepago y Detallebolepago
				 */
				RegistraCobropago rC = new RegistraCobropago(bp);
				rC.registraCobro(lista, cobroPagos);
				setLote(rC.getLote());
				if (metodoCarga == 2) {
					Recaudacion recaudacion = new Recaudacion();
					recaudacion.setId(recaudId);
					for (Cobropago cp : rC.getCobroPagos()) {
						cp.setRecaudacion(recaudacion);
					}
				}
				if (metodoCarga == 3) {
					MovIngresosVarios mov = (MovIngresosVarios) bp.getById(MovIngresosVarios.class,
							movIngresosVariosId);
					mov.setSaldo(mov.getSaldo() - this.aCobrar);
					bp.save(mov);
				}
				bp.commit();
				forward = "RecaudacionRedirect";
			} catch (Exception e) {
				bp.rollback();
				e.printStackTrace();
			}
		}

		this.tcomp12 = 0L;
		this.timp12 = 0D;
		this.lista = new String[] {};
		this.difTotal = 0D;
		this.aCobrar = 0D;

		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public HashMap getErrors() {
		return null;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return this.lResult;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public Long getIdcaratula() {
		return idcaratula;
	}

	public void setIdcaratula(Long idcaratula) {
		this.idcaratula = idcaratula;
	}

	public Long getCodi12() {
		return codi12;
	}

	public void setCodi12(Long codi12) {
		this.codi12 = codi12;
	}

	public String getDetaba() {
		return detaba;
	}

	public void setDetaba(String detaba) {
		this.detaba = detaba;
	}

	public String getFenv12Str() {
		return DateHelper.getString(fenv12);
	}

	public void setFenv12Str(String fecha) {
		if (fecha != null && fecha.length() > 0) {
			this.fenv12 = DateHelper.getDate(fecha);
		} else {
			this.fenv12 = DateHelper.getDate("");
		}
	}

	public Date getFenv12() {
		return fenv12;
	}

	public void setFenv12(Date fenv12) {
		this.fenv12 = fenv12;
	}

	public String getFcob12() {
		return fcob12;
	}

	public void setFcob12(String fcob12) {
		this.fcob12 = fcob12;
	}

	public Long getTcomp12() {
		return tcomp12;
	}

	public void setTcomp12(Long tcomp12) {
		this.tcomp12 = tcomp12;
	}

	public Double getTimp12() {
		return timp12;
	}

	public void setTimp12(Double timp12) {
		this.timp12 = timp12;
	}

	public Long getIdmediopago() {
		return idmediopago;
	}

	public void setIdmediopago(Long idmediopago) {
		this.idmediopago = idmediopago;
	}

	public String getEntidademisora() {
		return entidademisora;
	}

	public void setEntidademisora(String entidademisora) {
		this.entidademisora = entidademisora;
	}

	public String getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}

	public Long getCuotas() {
		return cuotas;
	}

	public void setCuotas(Long cuotas) {
		this.cuotas = cuotas;
	}

	public String getFemisionStr() {
		return DateHelper.getString(femision);
	}

	public void setFemisionStr(String fecha) {
		if (fecha != null && fecha.length() > 0) {
			this.femision = DateHelper.getDate(fecha);
		} else {
			this.femision = DateHelper.getDate("");
		}
	}

	public Date getFvencimiento() {
		return fvencimiento;
	}

	public void setFvencimiento(Date fvencimiento) {
		this.fvencimiento = fvencimiento;
	}

	public String getFvencimientoStr() {
		return DateHelper.getString(fvencimiento);
	}

	public void setFvencimientoStr(String fecha) {
		if (fecha != null && fecha.length() > 0) {
			this.fvencimiento = DateHelper.getDate(fecha);
		} else {
			this.fvencimiento = DateHelper.getDate("");
		}
	}

	public String getNrocomprobante() {
		return nrocomprobante;
	}

	public void setNrocomprobante(String nrocomprobante) {
		this.nrocomprobante = nrocomprobante;
	}

	public Long getNrolote() {
		return nrolote;
	}

	public void setNrolote(Long nrolote) {
		this.nrolote = nrolote;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public StrutsArray getCobroPagos() {
		return cobroPagos;
	}

	public void setCobroPagos(StrutsArray cobroPagos) {
		this.cobroPagos = cobroPagos;
	}

	public boolean isSave() {
		return save;
	}

	public void setSave(boolean save) {
		this.save = save;
	}

	public Caratula getCaratula() {
		return caratula;
	}

	public void setCaratula(Caratula caratula) {
		this.caratula = caratula;
	}

	public String[] getLista() {
		return this.lista;
	}

	public void setLista(String[] lista) {
		this.lista = lista;
	}

	public Double getDifTotal() {
		return difTotal;
	}

	public void setDifTotal(Double difTotal) {
		this.difTotal = difTotal;
	}

	public Double getACobrar() {
		return aCobrar;
	}

	public void setACobrar(Double cobrar) {
		aCobrar = cobrar;
	}

	public String getTipomedio() {
		return tipomedio;
	}

	public void setTipomedio(String tipomedio) {
		this.tipomedio = tipomedio;
	}

	public Long getLote() {
		return lote;
	}

	public void setLote(Long lote) {
		this.lote = lote;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setCotizacion(Double cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Double getCotizacion() {
		return cotizacion;
	}

	public Long getRecaudId() {
		return recaudId;
	}

	public String getRecaudFecha() {
		return recaudFecha;
	}

	public Double getRecaudImporte() {
		return recaudImporte;
	}

	public Double getRecaudSaldo() {
		return recaudSaldo;
	}

	public void setRecaudId(Long recaudId) {
		this.recaudId = recaudId;
	}

	public void setRecaudFecha(String recaudFecha) {
		this.recaudFecha = recaudFecha;
	}

	public void setRecaudImporte(Double recaudImporte) {
		this.recaudImporte = recaudImporte;
	}

	public void setRecaudSaldo(Double recaudSaldo) {
		this.recaudSaldo = recaudSaldo;
	}

	public Integer getMetodoCarga() {
		return metodoCarga;
	}

	public void setMetodoCarga(Integer metodoCarga) {
		this.metodoCarga = metodoCarga;
	}

	public String getUsuarioSesion() {
		return SessionHandler.getCurrentSessionHandler().getCurrentUser();
	}

	public Double getImpoTotal() {
		return impoTotal;
	}

	public void setImpoTotal(Double impoTotal) {
		this.impoTotal = impoTotal;
	}

	public Date getFechaCalculoFecovita() {
		return fechaCalculoFecovita;
	}

	public void setFechaCalculoFecovita(Date fechaCalculoFecovita) {
		this.fechaCalculoFecovita = fechaCalculoFecovita;
	}

	public String getFechaCalculoFecovitaStr() {
		return DateHelper.getString(fechaCalculoFecovita);
	}

	public void setFechaCalculoFecovitaStr(String f) {
		java.util.Date dd = DateHelper.getDate(f);
		if (dd != null) {
			this.fechaCalculoFecovita = new Date(dd.getTime());
		} else {
			this.fechaCalculoFecovita = null;
		}
	}

	public String[] getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String[] impuesto) {
		this.impuesto = impuesto;
	}

	public Long getMovIngresosVariosId() {
		return movIngresosVariosId;
	}

	public void setMovIngresosVariosId(Long movIngresosVariosId) {
		this.movIngresosVariosId = movIngresosVariosId;
	}

}
