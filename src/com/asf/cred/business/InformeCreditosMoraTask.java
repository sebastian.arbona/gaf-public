package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.hibernate.Query;

import com.asf.cred.business.InformeDeudaItemTask.InformeDeudaItemListener;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanNotificacion;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.DetalleInformeDeuda;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.informeCreditosMora;
import com.nirven.creditos.hibernate.informeDetalleCreditosMora;

public class InformeCreditosMoraTask implements Runnable, InformeDeudaItemListener {

	private String fecha;
	private String fechaDesde;
	private String fechaHasta;
	private Date fechaMora;
	private informeCreditosMora informe;
	private String user;
	private String pass;
	private ExecutorService service;
	private BusinessPersistance bp;
	private List<Objetoi> creditos;
	private List<BeanNotificacion> creditosBean;
	private Long idPersona;
	private List<AuditoriaFinal> auditoriaFinal;
	private String auditoriaFinalPos;
	private String[] idsLineas;
	private String codigo;
	private Long numeroAtencion;
	private List<Long> lineasSeleccionadas = new ArrayList<Long>();
	private String estadosWhere = "";
	private String numeroAtencionWhere = "";
	private String codigoWhere = "";
	private String personaWhere = "";

	public InformeCreditosMoraTask(Date fechaMora, String estadosWhere, Long numeroAtencion, String codigo,
			Long idPersona, String[] idsLineas, String fecha, String fechaDesde, String fechaHasta, String user,
			String pass, ExecutorService service) {
		this.fechaMora = fechaMora;
		this.estadosWhere = estadosWhere;
		this.numeroAtencion = numeroAtencion;
		this.codigo = codigo;
		this.idPersona = idPersona;
		this.idsLineas = idsLineas;
		this.fecha = fecha;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
		this.user = user;
		this.pass = pass;
		this.service = service;
	}

	@Override
	public void run() {
		try {
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			if (sh == null) {
				sh = new com.asf.cred.security.SessionHandler();
				sh.register(null);
				if (pass != null) {
					if (!sh.login(null, user, pass)) {
						// Verifico si tiene permiso para loguearse en el sistema
						System.err.println(getClass() + ": Usuario o clave inv�lido");
						return;
					} else {

						GeneroListar();
					}
				}
			}
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			bp.getCurrentSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void GeneroListar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		// Filtros para la consulta

		// Caso: N�mero de Atenci�n espec�fico
		if (numeroAtencion != null && numeroAtencion != 0L)
			numeroAtencionWhere = "AND o.numeroAtencion = " + this.numeroAtencion;

		// Caso: Estado espec�fico
		if (this.codigo != null && !this.codigo.isEmpty())
			codigoWhere = "AND oc.comportamientoPago = " + this.codigo;

		// Caso: Persona espec�fica
		if (idPersona != null && idPersona != 0L)
			personaWhere = "AND o.persona.id = " + this.idPersona;

		if (idsLineas != null) {
			for (String l : idsLineas) {
				lineasSeleccionadas.add(new Long(l));
			}
		}

		// Caso con fecha de mora
		if (fechaMora != null) {
			List<Long> creditosId = bp
					.createQuery("select distinct o.id from Cuota c join c.credito o "
							+ "where c.fechaVencimiento = :fecha and c.fechaVencimiento < :hoy and c.estado = '1' ")
					.setParameter("fecha", fechaMora).setParameter("hoy", new Date()).list();

			// Los conjuntos no pueden ser vac�os en la consulta
			if (creditosId.isEmpty())
				creditosId.add(new Long(0));

			// Caso: L�nea espec�fica
			if (!lineasSeleccionadas.isEmpty()) {

				if (idPersona != null && idPersona != 0L) {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ "AND o.linea.id IN (:linea) " + codigoWhere
							+ " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ personaWhere).setParameterList("linea", lineasSeleccionadas)
							.setParameterList("objetoiId", creditosId).list();
				} else {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ "AND o.linea.id IN (:linea) " + codigoWhere
							+ " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ numeroAtencionWhere).setParameterList("linea", lineasSeleccionadas)
							.setParameterList("objetoiId", creditosId).list();
				}

			}

			// Caso: Todas las l�neas
			else {

				if (idPersona != null && idPersona != 0L) {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ codigoWhere + " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ personaWhere).setParameterList("objetoiId", creditosId).list();
				} else {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ codigoWhere + " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ numeroAtencionWhere).setParameterList("objetoiId", creditosId).list();
				}

			}
		}

		// Caso sin fecha de mora
		else {
			// Caso: L�nea espec�fica
			if (!lineasSeleccionadas.isEmpty()) {

				if (idPersona != null && idPersona != 0L) {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ personaWhere).setParameterList("linea", lineasSeleccionadas).list();
				} else {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ numeroAtencionWhere).setParameterList("linea", lineasSeleccionadas).list();
				}

			}

			// Caso: Todas las l�neas
			else {

				if (idPersona != null && idPersona != 0L) {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ codigoWhere + " AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ personaWhere).list();
				} else {
					this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
							+ "JOIN oc.objetoi o "
							+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
							+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
							+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
							+ codigoWhere + " AND oc.fechaHasta IS NULL "
							+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
							+ numeroAtencionWhere).list();
				}

			}
		}

		Query bonifQuery = bp.createQuery(
				"select count(b) from ObjetoiBonificacion b where b.objetoi.id = :id and b.objetoi.fechaBajaBonificacion is null");
		Query garantiaSGRQuery = bp.createSQLQuery(
				"select count(*) from ObjetoiGarantia og left join Objetoi o on og.objetoi_id = o.id left join Garantia g on og.garantia_id = g.id where g.tipo_id = 6 and o.id = :id ");
		Query seguroVencidoQuery = bp.createSQLQuery(
				"select count(*) from ObjetoiGarantia og join Objetoi o on og.objetoi_id = o.id join Garantia g on og.garantia_id = g.id left join GarantiaSeguro gs on g.seguro_id = gs.id where gs.fechaVencimiento <= :hoy and o.id = :id");
		creditosBean = new ArrayList<BeanNotificacion>();

		// INSERTO CABECERA

		int CantidadRegistros = creditos.size();
		String itemsRegistros = "";

		if (!lineasSeleccionadas.isEmpty()) {
			for (String l : idsLineas) {
				itemsRegistros = itemsRegistros + (l + ",");
			}
		}
		if (CantidadRegistros != 0) {

			informeCreditosMora infCredMor = new informeCreditosMora();
			infCredMor.setEstado("En Proceso");
			infCredMor.setFechaProcesado(DateHelper.getDate(fecha));
			infCredMor.setFechaDesde(DateHelper.getDate(fechaDesde));
			infCredMor.setFechaHasta(DateHelper.getDate(fechaHasta));
			infCredMor.setItems(CantidadRegistros);
			infCredMor.setLineasId(itemsRegistros);
			infCredMor.setProcesados(0);
			bp.saveOrUpdate(infCredMor);
			int contador = 0;

//			String temaId = DirectorHelper.getString("Garantia.TipoSGR");
			int idTipoGarantia = 0;

			for (Objetoi cred : creditos) {
				BeanNotificacion bn = new BeanNotificacion();
				bn.setCredito(cred);
				Number countBonif = (Number) bonifQuery.setParameter("id", cred.getId()).uniqueResult();
				if (countBonif != null && countBonif.intValue() != 0) {
					bn.setBonificado(true);
				}
//				Number countGarantiaSGR = null;
//				if (!temaId.isEmpty()) {
//					String[] idsOp = (temaId != null && !temaId.isEmpty() ? temaId.toString().split(",") : null);
//
//					for (int i = 0; i < idsOp.length; i++) {
//						idTipoGarantia = Integer.parseInt(idsOp[i]);
//						garantiaSGRQuery = bp.createSQLQuery(
//								"select count(*) from ObjetoiGarantia og left join Objetoi o on og.objetoi_id = o.id left join Garantia g on og.garantia_id = g.id where g.tipo_id = "
//										+ idTipoGarantia + " and o.id = :id ");
//						countGarantiaSGR = (Number) garantiaSGRQuery.setParameter("id", cred.getId()).uniqueResult();
//						if (countGarantiaSGR.intValue() != 0) {
//							break;
//						}
//					}
//
//				}

//				if (countGarantiaSGR != null && countGarantiaSGR.intValue() != 0) {
//					bn.setGarantiaSGR(true);
//				}
//				Number countSeguroVencido = (Number) seguroVencidoQuery.setParameter("hoy", new Date())
//						.setParameter("id", cred.getId()).uniqueResult();
//				if (countSeguroVencido != null && countSeguroVencido.intValue() != 0) {
//					bn.setSeguroVencido(true);
//				}

//				auditoriaFinal = bp.createQuery("Select n from AuditoriaFinal n where n.credito.id=:credito")
//						.setParameter("credito", cred.getId()).list();
//				if (auditoriaFinal.isEmpty()) {
//					setAuditoriaFinalPos("");
//				} else {
//					setAuditoriaFinalPos(auditoriaFinal.get(auditoriaFinal.size() - 1).getAplicaFondo());
//				}
//				bn.setAuditoriaFinalPosee(getAuditoriaFinalPos());

				creditosBean.add(bn);

				informeDetalleCreditosMora infDetMora = new informeDetalleCreditosMora();
				infDetMora.setProyecto(Integer.parseInt(numeroAtencion.toString()));
				if (bn.getCredito().getNumeroAtencion() != null) {
					infDetMora.setProyecto(Integer.parseInt(bn.getCredito().getNumeroAtencion().toString()));
				}
				if (cred.getNumeroCredito() != null) {
					infDetMora.setNroCreditoAnt(Integer.parseInt(cred.getNumeroCredito()));
				}
				// ReUtilizo el bean para setear mis datos en la tabla q mostrar�
				infDetMora.setDepartamento(bn.getDepartamento());
				infDetMora.setIdTitutar(Integer.parseInt(cred.getPersona().getIdpersona().toString()));
				infDetMora.setTitular(cred.getPersona().getNomb12());
				infDetMora.setCuit(cred.getPersona().getCuil12Str());
				infDetMora.setLinea(cred.getLinea().getNombre());
				infDetMora.setEstado(cred.getObjetoiComportamientoActual().getNombreComportamiento());
				infDetMora.setEtapa(cred.getEstadoActual().getEstado().getNombreEstado());
				infDetMora.setFechaMora(cred.getFechaUltimoVencimientoDate());
				infDetMora.setDiasAtraso(cred.getDiasDeAtraso());
				infDetMora.setImporteTotalAdeudado(bn.getDeudaTotal());
				infDetMora.setDeudaExigible(bn.getDeudaExigible());
				infDetMora.setFechaUltimaCobranza(cred.getFechaUltimoPago());
				infDetMora.setMoneda(cred.getLinea().getMoneda().getDenominacion());
				infDetMora.setCotizacion(cred.getLinea().getMoneda().getCotizacion().toString());
				if (bn.isBonificado() == true) {
					infDetMora.setBonificado("SI");
				} else
					infDetMora.setBonificado("NO");
				infDetMora.setValoresCartera(cred.getMontoValoresCartera());
				if (cred.getUltimaNotificacion() != null) {
					infDetMora.setTipoUltimaNotificacion(cred.getUltimaNotificacion().getTipoAviso());
					infDetMora.setObservacionUltimaNotificacion(cred.getUltimaNotificacion().getObservaciones());
					infDetMora.setFechaUltimaNotificacion(
							DateHelper.getDate(cred.getUltimaNotificacion().getFechaCreadaStr()));
				}
				if (bn.isGarantiaSGR() == true) {
					infDetMora.setGarantia("SI");
				} else {
					infDetMora.setGarantia("NO");
				}
				if (cred.getFechaBajaBonificacion() != null) {
					infDetMora
							.setFechaBonificacionCaida(DateHelper.getDate(cred.getFechaBajaBonificacion().toString()));
				}
				if (bn.isSeguroVencido() == true) {
					infDetMora.setSeguroCaido("SI");
				} else {
					infDetMora.setSeguroCaido("NO");
				}
				if (cred.getUltimaProrroga() != null) {
					infDetMora.setUltimaProrroga(cred.getUltimaProrroga().getId().toString());
					infDetMora.setEstadoUltimaProrroga(cred.getUltimaProrroga().getNombreEstado());
				}

				if (bn.getAuditoriaFinalPosee() != null) {
					infDetMora.setAplicoFondos(bn.getAuditoriaFinalPosee());
				}
				infDetMora.setInformeMora(infCredMor);
				contador++;
				infCredMor.setProcesados(contador);
				bp.saveOrUpdate(infCredMor);
				bp.saveOrUpdate(infDetMora);

			}
			infCredMor.setEstado("Procesado");
			bp.saveOrUpdate(infCredMor);
		}
	}

	@Override
	public synchronized void detalleNuevo(DetalleInformeDeuda detalle) {

		informe.setProcesados(informe.getProcesados() + 1);
		bp.update(informe);
		bp.getCurrentSession().flush();
	}

	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}

}
