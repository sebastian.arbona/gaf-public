package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.struts.action.IReportBean;
import com.asf.struts.form.JReportForm;
import com.asf.util.ReportResult;
import com.nirven.creditos.hibernate.CosechaConfig;

public class HectareasCultivadas implements IProcess, IReportBean {

	private String forward = "HectareasCultivadas";
	private String accion = "";
	private BusinessPersistance bp;

	private Long cuit;
	private Long cuitSeleccionado;
	private List<Long> cuitSesion;
	private List<Long> cuitSesionManual;
	private List<com.nirven.creditos.hibernate.HectareasCultivadas> hectareasCultivadas;
	private List<com.nirven.creditos.hibernate.HectareasCultivadas> hectareasCultivadasSeleccionadas;
	private Double totalHectareas;
	private ReportResult result;

	public HectareasCultivadas() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.totalHectareas = new Double(0);
	}

	public Long getCuit() {
		return cuit;
	}

	public void setCuit(Long cuit) {
		this.cuit = cuit;
	}

	public List<com.nirven.creditos.hibernate.HectareasCultivadas> getHectareasCultivadas() {
		return hectareasCultivadas;
	}

	public void setHectareasCultivadas(List<com.nirven.creditos.hibernate.HectareasCultivadas> hectareasCultivadas) {
		this.hectareasCultivadas = hectareasCultivadas;
	}

	public Long getCuitSeleccionado() {
		return cuitSeleccionado;
	}

	public void setCuitSeleccionado(Long cuitSeleccionado) {
		this.cuitSeleccionado = cuitSeleccionado;
	}

	public List<com.nirven.creditos.hibernate.HectareasCultivadas> getHectareasCultivadasSeleccionadas() {
		return hectareasCultivadasSeleccionadas;
	}

	public void setHectareasCultivadasSeleccionadas(
			List<com.nirven.creditos.hibernate.HectareasCultivadas> hectareasCultivadasSeleccionadas) {
		this.hectareasCultivadasSeleccionadas = hectareasCultivadasSeleccionadas;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Double getTotalHectareas() {
		return totalHectareas;
	}

	public void setTotalHectareas(Double totalHectareas) {
		this.totalHectareas = totalHectareas;
	}

	@Override
	public boolean doProcess() {
		loadSession();
		if (this.accion.isEmpty()) {
			dropSession();
		}
		if ("buscar".contains(this.accion)) {
			buscar();
		} else if ("agregar".contains(this.accion)) {
			buscar();
			agregar();
		} else if ("nuevo".contains(this.accion)) {
			buscar();
			nuevo();
		} else if ("quitar".contains(this.accion)) {
			buscar();
			quitar();
		} else if ("imprimir".contains(this.accion)) {
			listarSeleccionados();
			result = new ReportResult();
			result.setClassBeanName(this.getClass().getName());
			result.setParams(this.getParams());
			result.setReportName("HectareasCultivadas.jasper");
			result.setPrint(false);
			result.setExport(JReportForm.EXPORT_PDF);
			this.forward = "Reportes";
		} else {
			dropSession();
		}
		listarSeleccionados();
		return true;
	}

	public boolean doShow() {
		return this.doProcess();
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return this.result;
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

	private void loadSession() {
		this.cuitSesion = (List<Long>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("HectareasCultivadas.cuit");
		if (this.cuitSesion == null) {
			this.cuitSesion = new ArrayList<Long>();
		}
		this.cuitSesionManual = (List<Long>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("HectareasCultivadas.cuitManual");
		if (this.cuitSesionManual == null) {
			this.cuitSesionManual = new ArrayList<Long>();
		}
	}

	private void dropSession() {
		this.cuitSesion = new ArrayList<Long>();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("HectareasCultivadas.cuit",
				null);
		this.cuitSesionManual = new ArrayList<Long>();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("HectareasCultivadas.cuitManual", null);
	}

	private void buscar() {
		this.hectareasCultivadas = this.bp.createQuery("FROM HectareasCultivadas WHERE cuit LIKE '%" + this.cuit + "%'")
				.list();
	}

	private void agregar() {
		this.cuitSesion.add(this.cuitSeleccionado);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("HectareasCultivadas.cuit",
				this.cuitSesion);
	}

	private void nuevo() {
		this.cuitSesionManual.add(this.cuit);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("HectareasCultivadas.cuitManual", this.cuitSesionManual);
	}

	private void quitar() {
		this.cuitSesion.remove(this.cuitSeleccionado);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("HectareasCultivadas.cuit",
				this.cuitSesion);
	}

	private void listarSeleccionados() {

		String inClause = "-1";
		for (Long elmCuit : this.cuitSesion) {
			inClause = inClause + ", " + elmCuit.toString();
		}
		this.hectareasCultivadasSeleccionadas = this.bp
				.createQuery("FROM HectareasCultivadas WHERE cuit IN (" + inClause + ")").list();
		this.totalHectareas = (double) 0;
		for (com.nirven.creditos.hibernate.HectareasCultivadas hectareaCultivada : this.hectareasCultivadasSeleccionadas) {
			this.totalHectareas += hectareaCultivada.getSuperficie();
		}
		for (Long elmCuitManual : this.cuitSesionManual) {
			com.nirven.creditos.hibernate.HectareasCultivadas hectareasCultivadasManual = new com.nirven.creditos.hibernate.HectareasCultivadas();
			hectareasCultivadasManual.setCuit(elmCuitManual);
			hectareasCultivadasManual.setSuperficie((double) 0);
			hectareasCultivadasManual.setVinedos(0);
			this.hectareasCultivadasSeleccionadas.add(hectareasCultivadasManual);
		}
	}

	@Override
	public Collection getCollection(HashMap hsParametros) {
		loadSession();
		listarSeleccionados();
		return this.getHectareasCultivadasSeleccionadas();
	}

	public int getLimiteSuperficie() {
		List<CosechaConfig> cosechaConfigList = bp.createQuery("FROM CosechaConfig c ORDER BY c.temporada DESC").list();
		if (cosechaConfigList.size() > 0) {
			CosechaConfig cosechaConfig = cosechaConfigList.get(0);
			return cosechaConfig.getSuperficiePermitida() != null ? cosechaConfig.getSuperficiePermitida().intValue()
					: 0;
		} else {
			return 0;
		}
	}

	public int getExcedeLimite() {
		return (this.totalHectareas > getLimiteSuperficie()) ? 1 : 0;
	}

	private String getParams() {
		loadSession();
		listarSeleccionados();
		String params = "";
		params += "excedeLimite=" + this.getExcedeLimite();
		params += ";usuario=" + SessionHandler.getCurrentSessionHandler().getCurrentUser();
		params += ";totalHectareas=" + this.totalHectareas;
		return params;
	}

}
