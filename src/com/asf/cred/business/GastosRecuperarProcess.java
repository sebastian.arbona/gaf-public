package com.asf.cred.business;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.aegis.type.TypeMapping;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.service.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.asf.gaf.hibernate.Cotizacion;
import com.asf.gaf.hibernate.Moneda;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.importacion.Fecha;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.Factura;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;

public class GastosRecuperarProcess implements IProcess {

	private static final String FORWARD = "GastosRecuperar";
	private static final String FORWARD_LIST = "GastosRecuperarList";
	private static final String GASTOS_RECUPERAR_CUENTA_CONTABLE = "GasRecCC";
	private static final String MOTIVO_ANULACION = "Eliminación de factura";

	private HashMap<String, String> errors = new HashMap<String, String>();

	private String accion;
	private BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	private Factura factura = new Factura();
	private DetalleFactura detalle = new DetalleFactura();
	private List<DetalleFactura> detalles;
	private String sid;
	private Date desde;
	private Date hasta;
	private String condicion;
	private double saldo;
	private List<DetalleFactura> listado;
	private Long idFactura;

	private String forward = FORWARD_LIST;
	private int detalleQuitar;
	private Long idCredito;
	private String contextoGAF;
	private Long proyecto;
	private Long lineaId;
	private String pagada;
	private String impactada;
	private boolean tipo;
	private Objetoi objetoi;
	private String expediente;
	private String persona;
	private Long idPersona;

	public GastosRecuperarProcess() {
		hasta = new Date();
		desde = Fecha.sumarDias(hasta, -30);
		contextoGAF = DirectorHelper.getString("URL.GAF");
	}

	@Override
	public boolean doProcess() {
		if (accion == null || accion.trim().equalsIgnoreCase("listar")) {
			listarFacturas();
			forward = FORWARD_LIST;
		} else if (accion.trim().equalsIgnoreCase("delete")) {
			deleteFactura(this.idFactura);
			listarFacturas();
			forward = FORWARD_LIST;
		} else if (accion.trim().equalsIgnoreCase("crear")) {
			crearFactura();
			forward = FORWARD;
		} else if (accion.trim().equalsIgnoreCase("modificar")) {
			modificarFactura();
			forward = FORWARD;
		} else if (accion.trim().equalsIgnoreCase("detalle")) {
			return agregarDetalle();
		} else if (accion.trim().equalsIgnoreCase("quitar")) {
			return eliminarDetalle();
		} else if (accion.trim().equalsIgnoreCase("guardar")) {
			return guardarFactura();
		} else if (accion.trim().equalsIgnoreCase("anular")) {
			forward = FORWARD_LIST;
		} else if (accion.trim().equalsIgnoreCase("listarGastosRecuperarPestania")) {
			listarGastosRecuperarPestania();
			forward = "GastosRecuperarPestania";
		}
		return this.errors.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void deleteFactura(Long idFactura2) {
		factura = (Factura) bp.getById(Factura.class, idFactura);
		bp.getCurrentSession().refresh(factura);
		// Invocar ws para eliminar débito
		// llamar WS de GAF para debito
		String error = eliminarDebitoGAF(factura);
		if (error != null) {
			String[] listaErrores = error.split("-");
			for (int i = 0; i < listaErrores.length; i++) {
				errors.put(listaErrores[i], listaErrores[i]);
			}

			forward = FORWARD;
		} else {
			// Eliminar factura
			List<NovedadCtaCte> novedades = bp
					.createQuery("select n from NovedadCtaCte n where n.detalleFactura.factura.id = :id")
					.setParameter("id", factura.getId()).list();
			for (NovedadCtaCte n : novedades) {
				bp.delete(n);
			}
			bp.getCurrentSession().flush();

			SessionHandler.getCurrentSessionHandler().getBusinessPersistance().delete(factura);
		}

	}

	private void crearFactura() {
		sid = UUID.randomUUID().toString();

		detalles = new ArrayList<DetalleFactura>();

		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		session.setAttribute(sid, factura);
		session.setAttribute(sid + "detalle", detalles);
	}

	@SuppressWarnings("unchecked")
	private void modificarFactura() {
		factura = (Factura) bp.getById(Factura.class, idFactura);
		bp.getCurrentSession().refresh(factura);
		detalles = bp.getNamedQuery("DetalleFactura.findByFactura").setParameter("idFactura", idFactura).list();

		sid = UUID.randomUUID().toString();

		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		session.setAttribute(sid, factura);
		session.setAttribute(sid + "detalle", detalles);
	}

	@SuppressWarnings("unchecked")
	private boolean agregarDetalle() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();

		Factura facturaActualizada = factura;
		factura = (Factura) session.getAttribute(sid);
		actualizarFactura(facturaActualizada);

		detalles = (List<DetalleFactura>) session.getAttribute(sid + "detalle");

		double suma = 0;
		for (DetalleFactura d : detalles) {
			suma += d.getImporte();
		}

		// redondeo para que permita agregar detalles cuando la suma no cuadra por
		// milesimas
		suma = (int) (suma * 100 + 0.5) / 100.0;

		forward = FORWARD;

		if (suma + detalle.getImporte() - factura.getImporte() >= 0.01) {
			errors.put("factura.detalle.importe", "factura.detalle.importe");
			return false;
		}
		if (detalle.getExpediente() == null && detalle.getExpediente().isEmpty()) {
			errors.put("Solicitud.expediente.gic", "Solicitud.expediente.gic");
			return false;
		}
		if (detalle.getCredito() == null) {
			errors.put("factura.detalle.proyecto", "factura.detalle.proyecto");
			return false;
		} else {

			if (detalle.getCredito().getLinea().isLineaCosecha()) {
				errors.put("factura.detalle.linea", "factura.detalle.linea");
				return false;
			}
		}
		suma += detalle.getImporte();
		detalle.setFactura(factura);
		detalles.add(detalle);
		detalle = new DetalleFactura(); // para agregar el siguiente
		saldo = factura.getImporte() - suma;
		saldo = (int) (saldo * 100 + 0.5) / 100.0;
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean eliminarDetalle() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();

		Factura facturaActualizada = factura;
		factura = (Factura) session.getAttribute(sid);
		actualizarFactura(facturaActualizada);

		detalles = (List<DetalleFactura>) session.getAttribute(sid + "detalle");

		forward = FORWARD;

		DetalleFactura detalleEliminar = detalles.get(detalleQuitar - 1);
		if (detalleEliminar.getId() != null) {
			List<NovedadCtaCte> novedades = bp.createQuery("select n from NovedadCtaCte n "
					+ "where n.detalleFactura.id = :id and (n.movimientoDeb is not null or n.movimientoCred is not null)")
					.setParameter("id", detalleEliminar.getId()).list();
			if (novedades != null && !novedades.isEmpty()) {
				errors.put("factura.detalle.impactado", "factura.detalle.impactado");
				return false;
			}
		}

		detalles.remove(detalleQuitar - 1);

		double suma = 0;
		for (DetalleFactura d : detalles) {
			suma += d.getImporte();
		}
		saldo = factura.getImporte() - suma;

		saldo = (int) (saldo * 100 + 0.5) / 100.0;

		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean guardarFactura() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();

		Factura facturaActualizada = factura;
		factura = (Factura) session.getAttribute(sid);
		actualizarFactura(facturaActualizada);

		detalles = (List<DetalleFactura>) session.getAttribute(sid + "detalle");
		factura.setDetalles(detalles);

		if (!factura.validate()) {
			this.errors.putAll(factura.getChErrores());
			forward = FORWARD;
			return false;
		}

		// llamar WS de GAF para debito
		String error = debitoGAF(factura);
		if (error != null) {
			errors.put(error, error);
			return false;
		}

		if (factura.getId() != null && factura.getId() != 0) {
			bp.createQuery("delete from NovedadCtaCte n where n.detalleFactura.factura.id = :idFactura")
					.setParameter("idFactura", factura.getId()).executeUpdate();
			bp.createQuery("delete from DetalleFactura d where d.factura.id = :idFactura")
					.setParameter("idFactura", factura.getId()).executeUpdate();
		}

		bp.saveOrUpdate(factura);
		int linea = 1;
		for (DetalleFactura d : detalles) {
			d.setLinea(linea++);
			bp.save(d);

			NovedadCtaCte n = new NovedadCtaCte();
			n.setConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
			n.setCredito(d.getCredito());
			n.setFecha(factura.getFecha());
			n.setImporte(d.getImporte());
			n.setDetalle("Gastos Proveedor: " + factura.getProveedor().getPersona().getNomb12() + " - Factura: "
					+ factura.getNumero());
			n.setDetalleFactura(d);
			bp.save(n);
		}

		forward = FORWARD_LIST;
		return true;
	}

	private String debitoGAF(Factura factura) {
		Document doc = null;

		Calendar cal = Calendar.getInstance();
		Long ejercicio = new Long(cal.get(Calendar.YEAR));
		Long tipoExpediente = 16L;
		String sucursal = factura.getSucursal() != null ? factura.getSucursal().toString() : "";
		Boolean retiene = true;
		Double cotizacion = findCotizacion(factura.getMoneda());
		String cuentaContable = DirectorHelper.getString(GASTOS_RECUPERAR_CUENTA_CONTABLE);

		GregorianCalendar c = new GregorianCalendar();
		c.setTime(factura.getFechaContable());
		XMLGregorianCalendar fecha = null;
		try {
			fecha = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e1) {
			e1.printStackTrace();
		}

		if (factura.getId() == null || factura.getId() == 0) {
			// alta debito
			try {
				Object[] response = invoke("altaDebito",
						new Object[] { ejercicio, cuentaContable, factura.getProveedor().getIdpersona(),
								factura.getExpepago(), tipoExpediente, factura.getTipoComprobante(), sucursal,
								new Long(factura.getNumero()), fecha, retiene, factura.getImporte(),
								factura.getIdMoneda(), cotizacion });

				doc = (Document) response[0];
			} catch (Exception e) {
				e.printStackTrace();
				return "factura.debito.alta";
			}
		} else {
			if (factura.getNroDebito() == null) {
				return null;
			}

			// modifica debito
			try {
				Object[] response = invoke("modificaDebito",
						new Object[] { factura.getNroDebito(), ejercicio, cuentaContable,
								factura.getProveedor().getIdpersona(), factura.getExpepago(), tipoExpediente,
								factura.getTipoComprobante(), sucursal, new Long(factura.getNumero()), fecha, retiene,
								factura.getImporte(), factura.getIdMoneda(), cotizacion });

				doc = (Document) response[0];
			} catch (Exception e) {
				e.printStackTrace();
				return "factura.debito.modificar";
			}
		}

		Node out = doc.getFirstChild();
		NodeList nodes = out.getChildNodes();

		String error = null;
		Long nroDebito = null;
		Boolean ok = null;

		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (n.getNodeName().equalsIgnoreCase("error")) {
				error = n.getFirstChild() != null ? n.getFirstChild().getNodeValue() : null;
			} else if (n.getNodeName().equalsIgnoreCase("nroDebito")) {
				try {
					if (n.getFirstChild() != null) {
						nroDebito = new Long(n.getFirstChild().getNodeValue());
					}
				} catch (NumberFormatException nfe) {
				}
			} else if (n.getNodeName().equalsIgnoreCase("ok")) {
				ok = Boolean.valueOf(n.getFirstChild() != null ? n.getFirstChild().getNodeValue() : "false");
			}
		}

		if (ok) {
			factura.setNroDebito(nroDebito);
			return null;
		} else {
			return error;
		}
	}

	private String eliminarDebitoGAF(Factura factura) {
		Document doc = null;

		Calendar cal = Calendar.getInstance();
		Long ejercicio = new Long(cal.get(Calendar.YEAR));

		GregorianCalendar c = new GregorianCalendar();
		c.setTime(factura.getFechaContable());
		XMLGregorianCalendar fecha = null;
		try {
			fecha = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e1) {
			e1.printStackTrace();
		}

		if (factura.getNroDebito() == null) {
			return null;
		}

		// modifica debito
		try {

			Object[] response = invoke("anulaDebito",
					new Object[] { factura.getNroDebito(), ejercicio, fecha, MOTIVO_ANULACION });

			doc = (Document) response[0];
		} catch (Exception e) {
			e.printStackTrace();
			return "factura.debito.modificar";
		}

		Node out = doc.getFirstChild();
		NodeList nodes = out.getChildNodes();

		String error = null;

		Boolean ok = null;

		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (n.getNodeName().equalsIgnoreCase("error")) {
				error = n.getFirstChild() != null ? n.getFirstChild().getNodeValue() : null;
			} else if (n.getNodeName().equalsIgnoreCase("ok")) {
				ok = Boolean.valueOf(n.getFirstChild() != null ? n.getFirstChild().getNodeValue() : "false");
			}
		}

		if (ok) {
			return null;
		} else {
			return error;
		}
	}

	@SuppressWarnings("unchecked")
	private double findCotizacion(Moneda moneda) {
		List<Cotizacion> cotizaciones = bp
				.createQuery("Select c from Cotizacion c where c.moneda=:moneda order by c.fechaDesde")
				.setParameter("moneda", moneda).list();
		Cotizacion coti = new Cotizacion();
		if (cotizaciones.size() != 0) {
			coti = cotizaciones.get(0);
		}
		for (Cotizacion cotizacion : cotizaciones) {
			if (cotizacion.getFechaHasta() == null) {
				coti = cotizacion;
			} else {
				if (coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
					coti = cotizacion;
				}
			}
		}
		return coti.getVenta();
	}

	@SuppressWarnings("unused")
	private Object[] invoke(String metodo, Object[] params) throws Exception {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		String url = DirectorHelper.getString("URL.GAF") + "/services/Debitos?wsdl";
		Client c = WebServiceHelper.getCliente(url);
		Service model = c.getService();
		AegisBindingProvider bp = (AegisBindingProvider) model.getBindingProvider();
		TypeMapping typeMapping = bp.getTypeMapping(model);
		c.addOutHandler(sh.getAutenticacionClienteHandler());
		return c.invoke(metodo, params);
	}

	private void actualizarFactura(Factura facturaActualizada) {
		factura.setMotivo(facturaActualizada.getMotivo());
		factura.setFecha(facturaActualizada.getFecha());
		factura.setFechaContable(facturaActualizada.getFechaContable());
		factura.setImporte(facturaActualizada.getImporte());
		factura.setMoneda(facturaActualizada.getMoneda());
		factura.setNumero(facturaActualizada.getNumero());
		factura.setProveedor(facturaActualizada.getProveedor());
		factura.setExpepago(facturaActualizada.getExpepago());
		factura.setSucursal(facturaActualizada.getSucursal());
		factura.setTipoComprobante(facturaActualizada.getTipoComprobante());

	}

	@SuppressWarnings("unchecked")
	private void listarFacturas() {
		List<Object[]> filas;
		String query = "select d, f from DetalleFactura d right "
				+ "join d.factura f where f.fecha >= :desde and f.fecha <= :hasta ";

		if (condicion != null && !condicion.trim().isEmpty()) {
			if (condicion.equalsIgnoreCase("P")) {
				query += " AND f.fechaPago is null";
			} else {
				query += " AND f.fechaPago is not null";
			}
		}

		if (proyecto != null && proyecto != 0) {
			query += " AND d.credito.numeroAtencion =:credito";
		}

		if (lineaId != null && lineaId != 0) {
			query += " AND d.credito.linea.id =:lineaId";
		}

		if (impactada != null && !impactada.isEmpty()) {
			if (impactada.equalsIgnoreCase("Si")) {
				query += " AND d.movimientoCtaCte is not null";
			} else if (impactada.equalsIgnoreCase("No")) {
				query += " AND d.movimientoCtaCte is null";
			}
		}

		if (pagada != null && !pagada.isEmpty()) {
			if (pagada.equalsIgnoreCase("Si")) {
				query += " AND f.fechaPago is not null";
			} else if (pagada.equalsIgnoreCase("No")) {
				query += " AND f.fechaPago is null";
			}
		}

		if (expediente != null && !expediente.equals("")) {
			query += " AND d.expediente =:expediente";
		}

		if (idPersona != null && !idPersona.equals(new Long(0))) {
			query += " AND d.credito.persona.idpersona =:idPersona";
		}

		query += " order by f.fecha";
		org.hibernate.Query consulta = (org.hibernate.Query) bp.createQuery(query);

		if (proyecto != null && proyecto != 0) {
			consulta.setParameter("credito", proyecto);
		}

		if (lineaId != null && lineaId != 0) {
			consulta.setParameter("lineaId", lineaId);
		}
		consulta.setParameter("desde", desde);
		consulta.setParameter("hasta", hasta);

		if (expediente != null && !expediente.equals("")) {
			consulta.setParameter("expediente", expediente);
		}

		if (idPersona != null && !idPersona.equals(new Long(0))) {

			consulta.setParameter("idPersona", idPersona);
		}

		filas = consulta.list();

		listado = new ArrayList<DetalleFactura>();

		for (Object[] f : filas) {
			DetalleFactura d = (DetalleFactura) f[0];

			if (d != null) {
				listado.add(d);
			} else {
				Factura fc = (Factura) f[1];

				DetalleFactura nodet = new DetalleFactura();
				nodet.setFactura(fc);
				listado.add(nodet);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void listarGastosRecuperarPestania() {
		List<Object[]> filas = bp.getNamedQuery("DetalleFactura.findByCredito").setLong("idCredito", idCredito).list();

		listado = new ArrayList<DetalleFactura>();

		for (Object[] f : filas) {
			DetalleFactura d = (DetalleFactura) f[0];

			if (objetoi == null) {
				objetoi = d.getCredito();
			}

			if (d != null) {
				listado.add(d);
			} else {
				Factura fc = (Factura) f[1];

				DetalleFactura nodet = new DetalleFactura();
				nodet.setFactura(fc);
				listado.add(nodet);
			}
		}
	}

	@Override
	public HashMap<String, String> getErrors() {
		return errors;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Factura getFactura() {
		return factura;
	}

	public void setFactura(Factura factura) {
		this.factura = factura;
	}

	public String getDesde() {
		return DateHelper.getString(desde);
	}

	public void setDesde(String desde) {
		this.desde = DateHelper.getDate(desde);
	}

	public String getHasta() {
		return DateHelper.getString(hasta);
	}

	public void setHasta(String hasta) {
		this.hasta = DateHelper.getDate(hasta);
	}

	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}

	public List<DetalleFactura> getListado() {
		return listado;
	}

	public void setListado(List<DetalleFactura> listado) {
		this.listado = listado;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public DetalleFactura getDetalle() {
		return detalle;
	}

	public void setDetalle(DetalleFactura detalle) {
		this.detalle = detalle;
	}

	public List<DetalleFactura> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetalleFactura> detalles) {
		this.detalles = detalles;
	}

	public double getSaldo() {
		return saldo;
	}

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public int getDetalleQuitar() {
		return detalleQuitar;
	}

	public void setDetalleQuitar(int detalleQuitar) {
		this.detalleQuitar = detalleQuitar;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public String getContextoGAF() {
		return contextoGAF;
	}

	public void setContextoGAF(String contextoGAF) {
		this.contextoGAF = contextoGAF;
	}

	public Long getProyecto() {
		return proyecto;
	}

	public void setProyecto(Long proyecto) {
		this.proyecto = proyecto;
	}

	public Long getLineaId() {
		return lineaId;
	}

	public void setLineaId(Long lineaId) {
		this.lineaId = lineaId;
	}

	public String getPagada() {
		return pagada;
	}

	public void setPagada(String pagada) {
		this.pagada = pagada;
	}

	public String getImpactada() {
		return impactada;
	}

	public void setImpactada(String impactada) {
		this.impactada = impactada;
	}

	public boolean isTipo() {
		return tipo;
	}

	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}

	public String getTotalStr() {
		double t = 0d;
		if (this.listado == null)
			return null;
		for (DetalleFactura df : this.listado)
			t += df.getImporte();
		return NumberFormat.getCurrencyInstance().format(t);
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

}
