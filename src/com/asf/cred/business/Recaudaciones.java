package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;

import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.aegis.type.TypeMapping;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.service.Service;

import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.util.WebServiceHelper;

public class Recaudaciones implements IProcess {
	private String forward = "recaudacionList";
	private HashMap<String, String> errores;
	private ArrayList<Recaudacion> recaudaciones;
	private String fecha;
	private Long idEnteRecaudador;
	private Long idRecaudacion;
	private Integer metodoCarga;
	private String action;

	public Recaudaciones() {
		this.errores = new HashMap<String, String>();
	}

	@Override
	public boolean doProcess() {
		if (this.action != null) {

			if (action.equals("pp")) {
				pp(idRecaudacion);
			} else if (action.equals("saldo")) {
				saldo(idRecaudacion);
			}
		}
		buscar();
		return this.errores.isEmpty();
	}

	private void pp(Long id) {
		try {
			this.invoke("pp", new Object[] { id });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saldo(Long id) {
		try {
			this.invoke("saldo", new Object[] { id });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Object[] invoke(String metodo, Object[] params) throws Exception {
		SessionHandler sh = SessionHandler.getCurrentSessionHandler();
		String url = DirectorHelper.getString("URL.GAF") + "/services/Recaudaciones?wsdl";
		Client c = WebServiceHelper.getCliente(url);
		Service model = c.getService();
		AegisBindingProvider bp = (AegisBindingProvider) model.getBindingProvider();
		TypeMapping typeMapping = bp.getTypeMapping(model);
		c.addOutHandler(sh.getAutenticacionClienteHandler());
		return c.invoke(metodo, params);
	}

	@SuppressWarnings("unchecked")
	private void buscar() {
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			if (metodoCarga.intValue() == 2) {
				recaudaciones = (ArrayList<Recaudacion>) bp
						.createSQLQuery("SELECT r.* FROM Recaudacion r WHERE r.fechamovimiento = :fecha "
								+ "AND r.codi_ba = :codi_ba AND (r.saldo > 0 or r.saldoPP>0)")
						.addEntity(Recaudacion.class).setDate("fecha", DateHelper.getDate(fecha))
						.setLong("codi_ba", idEnteRecaudador).list();
				String consulta = "";
				consulta += "SELECT m.idmediopago FROM Mediopago m WHERE m.denominacion = 'Recaudaciones'";
				consulta += " AND m.tipomedio = 'RC' AND m.abreviatura = 'REC'";
				this.idRecaudacion = new Long(bp.getByFilter(consulta).get(0).toString());
			} else if (metodoCarga.intValue() == 0) {
				recaudaciones = (ArrayList<Recaudacion>) bp.createSQLQuery("SELECT R.* FROM recaudacion R "
						+ "JOIN TIPIFICADORES T ON T.TF_CODIGO = R.TIPOCOMPROBANTE AND T.TF_CATEGORIA = 'recaudacion.tipocomprobante' "
						+ "WHERE R.fechamovimiento = :fecha AND R.codi_ba = :codi_ba "
						+ "AND T.TF_DESCRIPCION = 'Rendición por servicio de Recaudación'").addEntity(Recaudacion.class)
						.setDate("fecha", DateHelper.getDate(fecha)).setLong("codi_ba", idEnteRecaudador).list();
			} else if (metodoCarga.intValue() == 3) {
				recaudaciones = (ArrayList<Recaudacion>) bp.createSQLQuery("SELECT R.* FROM recaudacion R "
						+ "JOIN TIPIFICADORES T ON T.TF_CODIGO = R.TIPOCOMPROBANTE AND T.TF_CATEGORIA = 'recaudacion.tipocomprobante' "
						+ "WHERE R.fechamovimiento = :fecha AND R.codi_ba = :codi_ba "
//						+ "AND T.TF_DESCRIPCION = 'Rendición por servicio de Recaudación'")
				).addEntity(Recaudacion.class).setDate("fecha", DateHelper.getDate(fecha))
						.setLong("codi_ba", idEnteRecaudador).list();
			}
		} catch (Exception e) {
			this.recaudaciones = new ArrayList<Recaudacion>();
			e.printStackTrace();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		buscar();
		return this.forward;
	}

	@Override
	public String getInput() {
		buscar();
		return this.forward;
	}

	@Override
	public Object getResult() {
		return recaudaciones;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public void setIdEnteRecaudador(Long idBanco) {
		this.idEnteRecaudador = idBanco;
	}

	public Long getIdEnteRecaudador() {
		return idEnteRecaudador;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getFecha() {
		return fecha;
	}

	public ArrayList<Recaudacion> getRecaudaciones() {
		return recaudaciones;
	}

	public void setRecaudaciones(ArrayList<Recaudacion> recaudaciones) {
		this.recaudaciones = recaudaciones;
	}

	public void setIdRecaudacion(Long idRecaudacion) {
		this.idRecaudacion = idRecaudacion;
	}

	public Long getIdRecaudacion() {
		return idRecaudacion;
	}

	public Integer getMetodoCarga() {
		return metodoCarga;
	}

	public void setMetodoCarga(Integer metodoCarga) {
		this.metodoCarga = metodoCarga;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
