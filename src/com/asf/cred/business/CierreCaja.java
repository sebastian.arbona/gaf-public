package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.GarantiaTasacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.Pagos;

public class CierreCaja implements IProcess {

	private BusinessPersistance bp;
	@SuppressWarnings("rawtypes")
	private HashMap chErrores;
	private Object lResult;
	private Long idcaratula;
	private Date fenv12;
	private boolean cobro;
	private static Log log = LogFactory.getLog(CierreCaja.class);

	@SuppressWarnings("rawtypes")
	public CierreCaja() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.chErrores = new HashMap();
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean doProcess() {

		// Elo
		ProgressStatus.onProcess = true;
		ProgressStatus.iTotal = 1;
		ProgressStatus.iProgress = 0;
		ProgressStatus.abort = false;
		/////
		if (this.getFenv12() != null) {
			log.info("Iniciando Cierre de Caja");
			ProgressStatus.sStatus = "Iniciando Cierre de Caja";
			ProgressStatus.abort = false;

			// Obtiene las caratulas no actualizadas a partir de una fecha de
			// envio determinada.
			List<Caratula> lis = bp
					.createQuery("FROM Caratula c WHERE c.id=" + this.getIdcaratula()
							+ " AND c.fechaEnvio= :FENV12 AND c.actualizada=0")
					.setDate("FENV12", new java.sql.Date(this.getFenv12().getTime())).list();
			if (lis.size() > 0) {
				for (Caratula caratula : lis) {
					CierreCaratulaHandler cierreCaratulaHandler = CierreCaratulaHandler.getInstance();
					if (!cierreCaratulaHandler.cerrar(caratula)) {
						this.chErrores.putAll(cierreCaratulaHandler.getErrores());
						caratula.setActualizada(0L);
						bp.saveOrUpdate(caratula);
						actualizarObjetoiComportamiento(caratula);
						actualizarEstado(caratula);
						break;
					} else {
						bp.saveOrUpdate(caratula);
						actualizarObjetoiComportamiento(caratula);
						actualizarEstado(caratula);
					}

				}
			}
		}

		if (this.isCobro()) {
			CobroPagos p = new CobroPagos();
			p.setFenv12(this.getFenv12());
			p.doProcess();
			this.lResult = p.getResult();
		}
		ProgressStatus.onProcess = false;
		return this.chErrores.isEmpty();

	}

	@SuppressWarnings("unchecked")
	public void actualizarObjetoiComportamiento(Caratula caratula) {
		// Ticket 512
		CreditoHandler ch = new CreditoHandler();
		List<Pagos> pagos = bp.getByFilter("FROM Pagos p WHERE p.id.caratula.id=" + caratula.getId());
		for (Pagos pago : pagos) {
			Boleto boleto = pago.getId().getBoleto();
			Objetoi objetoi = boleto.getObjetoi();
			ch.actualizarComportamiento(objetoi, null,
					caratula.getFechaCobranza() != null ? caratula.getFechaCobranza() : pago.getFechaPago(),
					"Actualizado por Registraci�n de Pago.");

		}
	}

	@SuppressWarnings("unchecked")
	public void actualizarEstado(Caratula caratula) {
		List<Pagos> pagos = bp.getByFilter("FROM Pagos p WHERE p.id.caratula.id=" + caratula.getId());
		Boleto boleto;
		Objetoi objetoi;
		ObjetoiEstado estadoActual;
		ObjetoiEstado nuevo;
		Date hoy;
		Estado estado;
		try {
			estado = (Estado) bp.getNamedQuery("Estado.findByNombreEstado").setString("nombreEstado", "CANCELADO")
					.setMaxResults(1).uniqueResult();
			if (estado == null)
				return;
		} catch (NoResultException e) {
			return;
		}
		bp.begin();
		for (Pagos pago : pagos) {
			boleto = pago.getId().getBoleto();
			objetoi = boleto.getObjetoi();

			CreditoCalculoDeuda calculo = new CreditoCalculoDeuda();
			calculo.setHastaFecha(pago.getFechaPago());
			calculo.setIdObjetoi(boleto.getObjetoi().getId());
			calculo.calcular();
			double deuda = calculo.getDeudaTotal();

			if (deuda < 0.01) {
				hoy = new Date();
				estadoActual = objetoi.getEstadoActual();
				estadoActual.setFechaHasta(hoy);
				nuevo = new ObjetoiEstado();
				nuevo.setFecha(hoy);
				nuevo.setObjetoi(objetoi);
				nuevo.setEstado(estado);
				bp.update(estadoActual);
				bp.save(nuevo);
				actualizarEstadoObjetoiGarantia(objetoi, nuevo.getFecha());
			}
		}
		bp.commit();
	}

	@SuppressWarnings("unchecked")
	private void actualizarEstadoObjetoiGarantia(Objetoi objetoi, Date fecha) {
		// TODO ojo esto esta duplicado en CreditoCuentaCorriente, CierreCaja,
		// CreditoDatosFinancieros, FinalizarCredito
		List<ObjetoiGarantia> og = bp
				.createQuery("SELECT g from ObjetoiGarantia g where g.objetoi = :credito and g.baja is null")
				.setParameter("credito", objetoi).list();
		if (!og.isEmpty()) {
			for (ObjetoiGarantia objetoiGarantia : og) {
				if (objetoiGarantia != null) {
					if (!objetoi.getTieneGarantiaFiduciaria()) {
						GarantiaTasacion tasacion = objetoiGarantia.getTasacion();
						tasacion.setFechaCancelacion(fecha);
						GarantiaEstado nuevoEstadoGarantia = new GarantiaEstado(ComportamientoGarantiaEnum.CANCELADA,
								tasacion.getFechaCancelacion());
						nuevoEstadoGarantia.setObjetoiGarantia(objetoiGarantia);
						nuevoEstadoGarantia.determinarImporte();
						bp.save(nuevoEstadoGarantia);
						bp.update(tasacion);
						if (objetoiGarantia.getObjetoi().getLinea().isLineaAcuerdoPago()) {
							// Buscar y Cancelar Garant�a del cr�dito original
							Objetoi creditoOriginal = new Objetoi();
							String sqlCreditoOriginal = "SELECT o FROM Objetoi o WHERE o.acuerdoPago.id = :idAcuerdoPago ";
							creditoOriginal = (Objetoi) bp.createQuery(sqlCreditoOriginal)
									.setLong("idAcuerdoPago", objetoiGarantia.getObjetoi().getId()).uniqueResult();
							if (creditoOriginal != null) {
								ObjetoiGarantia ogOriginal = new ObjetoiGarantia();
								String sqlGarantiaOriginal = "SELECT g FROM ObjetoiGarantia g WHERE g.objetoi.id = :idObjetoi AND g.garantia.id = :idGarantia";
								ogOriginal = (ObjetoiGarantia) bp.createQuery(sqlGarantiaOriginal)
										.setLong("idObjetoi", creditoOriginal.getId())
										.setLong("idGarantia", objetoiGarantia.getGarantia().getId()).uniqueResult();
								if (ogOriginal != null) {
									GarantiaEstado estadoNuevoOriginal = new GarantiaEstado(
											ComportamientoGarantiaEnum.CANCELADA_ACUERDO,
											objetoiGarantia.getObjetoi().getEstadoActual().getFecha());
									estadoNuevoOriginal.setObjetoiGarantia(ogOriginal);
									estadoNuevoOriginal.determinarImporte();
									bp.save(estadoNuevoOriginal);
								}
							}
						}
					}
				}
			}
		}

	}

	@Override
	public String getForward() {
		return !this.isCobro() ? "CierreCaja" : "CobroPagos";
	}

	@Override
	public String getInput() {
		return !this.isCobro() ? "CierreCaja" : "CobroPagos";
	}

	@Override
	public boolean validate() {
		return this.chErrores.isEmpty();
	}

	@Override
	public Object getResult() {
		return lResult;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public HashMap getErrors() {
		return chErrores;
	}

	public Date getFenv12() {
		return fenv12;
	}

	public void setFenv12(Date fenv12) {
		this.fenv12 = fenv12;
	}

	public Long getIdcaratula() {
		return idcaratula;
	}

	public void setIdcaratula(Long idcaratula) {
		this.idcaratula = idcaratula;
	}

	public String getFenv12Str() {
		return DateHelper.getString(fenv12);
	}

	public void setFenv12Str(String fenv12) {
		if (fenv12 != null && fenv12.length() > 0) {
			this.fenv12 = DateHelper.getDate(fenv12);
		} else {
			this.fenv12 = DateHelper.getDate("");
		}
	}

	public boolean isCobro() {
		return cobro;
	}

	public void setCobro(boolean cobro) {
		this.cobro = cobro;
	}

	public String getFechaStr() {
		return this.getFenv12Str();
	}

	@SuppressWarnings("unchecked")
	public List<Ctacte> listarCtaCteConceptos(Cuota cuota, Date hasta, String... conceptos) {
		return bp.getNamedQuery("Ctacte.findByConceptosCuotaFecha").setParameterList("conceptos", conceptos)
				.setParameter("idCuota", cuota.getId()).setParameter("fecha", hasta).list();
	}

	public String getUsuarioSesion() {
		return SessionHandler.getCurrentSessionHandler().getCurrentUser();
	}
}