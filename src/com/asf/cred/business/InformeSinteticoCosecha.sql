select q1.*, sum(d2.cantidad) as d2cantidad, sum(d2.monto) as d2monto, q1.d1monto + sum(d2.monto) as montoTotal from 
(select des.fechaReal, des.cantidad, des.monto, sum(d1.cantidad) as d1cantidad, sum(d1.monto) as d1monto from 
(select d.fechaReal, count(d.fechaReal) cantidad, sum(case when d.fechaReal is null then 0 else d.importeReal end) monto
from Desembolso d, Objetoi o
where d.credito_id = o.id 
and d.estado <> '3'
and o.fechaResolucion >= '$P{FECHA_DESDE}' and o.fechaResolucion <= '$P{FECHA_HASTA}'
-- and d.fechaReal is not null
and d.numero in (1,2)
and o.linea_id in ($P{LINEAS})
group by d.fechaReal) as des
join
(select d.fechaReal, sum(case when d.importeReal is not null then d.importeReal else d.importe end) monto, count(*) cantidad
from Desembolso d, Objetoi o
where d.credito_id = o.id 
and d.estado <> '3'
and o.fechaResolucion >= '$P{FECHA_DESDE}' and o.fechaResolucion <= '$P{FECHA_HASTA}'
and d.numero = 1
and o.linea_id in ($P{LINEAS})
group by d.fechaReal) as d1
on d1.fechaReal is null or d1.fechaReal > des.fechaReal
group by des.fechaReal, des.monto, des.cantidad) as q1
join
(select d.fechaReal, count(*) cantidad, sum(case when d.importeReal is not null then d.importeReal else d.importe end) monto
from Desembolso d, Objetoi o
where d.credito_id = o.id 
and d.estado <> '3'
and o.fechaResolucion >= '$P{FECHA_DESDE}' and o.fechaResolucion <= '$P{FECHA_HASTA}'
and d.numero = 2
and o.linea_id in ($P{LINEAS})
group by d.fechaReal) as d2
on d2.fechaReal is null or d2.fechaReal > q1.fechaReal
group by q1.fechaReal, q1.monto, q1.cantidad, q1.d1monto, q1.d1cantidad
order by q1.fechaReal