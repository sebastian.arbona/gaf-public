package com.asf.cred.business;

import java.util.List;

import com.asf.gaf.DirectorHandler;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Persona;
import com.civitas.hibernate.persona.PersonaVinculada;
import com.nirven.creditos.hibernate.Linea;

public class CreditoCotomadores extends CreditoProcess {

	private static final String DIRECTOR_RELACION_COTOMADOR = "relacion.cotomador";
	private Persona personaTitular;
	private List<PersonaVinculada> personasVinculadas;
	private PersonaVinculada personaVinculada;
	private String forward;
	private String msg;

	public CreditoCotomadores() {
		personaTitular = new Persona();
		personaVinculada = new PersonaVinculada();
	}

	@Override
	public boolean doProcess() {
		if (action == null) {
			return false;
		}

		if (action.equals("cancelar")) {
			listarVinculadas();
			forward = "CreditoCotomadores";
		}

		super.doProcess(); // busca objetoi

		personaTitular = getObjetoi().getPersona();

		if (action.equals("new")) {
			if (personaVinculada.getPersonaVinculada() != null) {
				personaVinculada.getPersonaVinculada().setId(null);
			}
			forward = "CreditoCotomador";
		} else if (action.equals("save")) {
			try {
				boolean esCosecha = this.credito.getLinea().isLineaCosecha();
				if (esCosecha && credito.getPersona() != null) {
					Long idRelacionCAPE = new Long(DirectorHelper.getString("id.relacion.CAPE", "28"));
					Number n = (Number) bp
							.createQuery("SELECT count(pv.id) from "
									+ com.civitas.hibernate.persona.PersonaVinculada.class.getCanonicalName()
									+ " pv where pv.relacion.id = :relacion and pv.personaVinculada = :persona ")
							.setEntity("persona", personaVinculada.getPersonaVinculada())
							.setLong("relacion", idRelacionCAPE).uniqueResult();
					if (n.longValue() > 0) {
						msg = "La persona seleccionada esta asociada a la cooperativa CAPE. Por favor, intente nuevamente.";
						forward = "CreditoCotomador";
						return true;
					}
				}

				personaVinculada.setPersonaTitular(personaTitular);
				personaVinculada.setIdObjetoi(getIdObjetoi());
				personaVinculada.setNumeroAtencion(getObjetoi().getNumeroAtencion());

				Long idRelacionCotomador = new Long(
						DirectorHandler.getDirector(DIRECTOR_RELACION_COTOMADOR).getValor());
				personaVinculada.setRelacion_id(idRelacionCotomador);

				bp.save(personaVinculada);

				listarVinculadas();

				forward = "CreditoCotomadores";
			} catch (Exception e) {
				log.error("Error buscando codigo relacion cotomador: " + e.getMessage());
				errores.put("credito.relacion.cotomador", "credito.relacion.cotomador");
				return false;
			}
		} else if (action.equals("eliminar")) {
			bp.delete(bp.getById(PersonaVinculada.class, personaVinculada.getId()));
			listarVinculadas();
			forward = "CreditoCotomadores";
		} else if (action.equals("listar")) {
			listarVinculadas();
			forward = "CreditoCotomadores";
		}

		return true;
	}

	private void listarVinculadas() {
		personasVinculadas = credito.buscarCotomadores();
	}

	@Override
	public String getForward() {
		return forward;
	}

	public Persona getPersonaTitular() {
		return personaTitular;
	}

	public void setPersonaTitular(Persona personaTitular) {
		this.personaTitular = personaTitular;
	}

	public List<PersonaVinculada> getPersonasVinculadas() {
		return personasVinculadas;
	}

	public void setPersonasVinculadas(List<PersonaVinculada> personasVinculadas) {
		this.personasVinculadas = personasVinculadas;
	}

	public PersonaVinculada getPersonaVinculada() {
		return personaVinculada;
	}

	public void setPersonaVinculada(PersonaVinculada personaVinculada) {
		this.personaVinculada = personaVinculada;
	}

	public boolean isOcultar() {
		// if(
		// !credito.getEstadoActual().getEstado().getNombreEstado().equals("ANALISIS")
		// &&
		// !credito.getEstadoActual().getEstado().getNombreEstado().equals("ESPERANDO
		// DOCUMENTACION")){
		// return true;
		// }else{
		return false;
		// }
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
