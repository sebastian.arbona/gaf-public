package com.asf.cred.business;

import java.util.Date;

public class InventarioDeCuentasCorrientesBean {
    private String numeroSolicitud;
    private String identificacionTomador;
    private String cuit;
    private String numeroCuentaPatrimonial;
    private String lineaCredito;
    private String resolucionAprobatoria;
    private double saldoCapitalMonedaOriginal;
    private double saldoCapitalPesos;
    private double saldoCapitalCorriente;
    private double saldoCapitalNoCorriente;
    private double saldoCapitalCorrientePesos;
    private double saldoCapitalNoCorrientePesos;
    private String cer;
    private double cerValor;
    private double otros;
    private double saldoTotal;
    private String estadoCredito;
    private String areaResponsable;
    private String numeroCredito;
    private String expediente;
    private Date fechaMutuo;
    private String moneda;
    private String comportamientoPago;
    private double compensatorios;
    private double moratorios;
    private double punitorios;
    private double desembolsos;
    private double desembolsosPesos;
    private double compensatoriosPesos;
    private double moratoriosPesos;
    private double punitoriosPesos;
    private double otrosPesos;
    private double saldoTotalPesos;
    private String judicial;
    private String departamento;

    public InventarioDeCuentasCorrientesBean() {
    }

    public String getNumeroSolicitud() {
        return this.numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getIdentificacionTomador() {
        return this.identificacionTomador;
    }

    public void setIdentificacionTomador(String identificacionTomador) {
        this.identificacionTomador = identificacionTomador;
    }

    public String getCuit() {
        return this.cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getNumeroCuentaPatrimonial() {
        return this.numeroCuentaPatrimonial;
    }

    public void setNumeroCuentaPatrimonial(String numeroCuentaPatrimonial) {
        this.numeroCuentaPatrimonial = numeroCuentaPatrimonial;
    }

    public String getLineaCredito() {
        return this.lineaCredito;
    }

    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public String getResolucionAprobatoria() {
        return this.resolucionAprobatoria;
    }

    public void setResolucionAprobatoria(String resolucionAprobatoria) {
        this.resolucionAprobatoria = resolucionAprobatoria;
    }

    public double getSaldoCapitalMonedaOriginal() {
        return this.saldoCapitalMonedaOriginal;
    }

    public void setSaldoCapitalMonedaOriginal(double saldoCapitalMonedaOriginal) {
        this.saldoCapitalMonedaOriginal = saldoCapitalMonedaOriginal;
    }

    public double getSaldoCapitalPesos() {
        return this.saldoCapitalPesos;
    }

    public void setSaldoCapitalPesos(double saldoCapitalPesos) {
        this.saldoCapitalPesos = saldoCapitalPesos;
    }

    public double getSaldoCapitalCorriente() {
        return this.saldoCapitalCorriente;
    }

    public void setSaldoCapitalCorriente(double saldoCapitalCorriente) {
        this.saldoCapitalCorriente = saldoCapitalCorriente;
    }

    public double getSaldoCapitalNoCorriente() {
        return this.saldoCapitalNoCorriente;
    }

    public void setSaldoCapitalNoCorriente(double saldoCapitalNoCorriente) {
        this.saldoCapitalNoCorriente = saldoCapitalNoCorriente;
    }

    public double getSaldoCapitalCorrientePesos() {
        return this.saldoCapitalCorrientePesos;
    }

    public void setSaldoCapitalCorrientePesos(double saldoCapitalCorrientePesos) {
        this.saldoCapitalCorrientePesos = saldoCapitalCorrientePesos;
    }

    public String getCer() {
        return this.cer;
    }

    public void setCer(String cer) {
        this.cer = cer;
    }

    public double getOtros() {
        return this.otros;
    }

    public void setOtros(double otros) {
        this.otros = otros;
    }

    public double getSaldoTotal() {
        return this.saldoTotal;
    }

    public void setSaldoTotal(double saldoTotal) {
        this.saldoTotal = saldoTotal;
    }

    public String getEstadoCredito() {
        return this.estadoCredito;
    }

    public void setEstadoCredito(String estadoCredito) {
        this.estadoCredito = estadoCredito;
    }

    public String getAreaResponsable() {
        return this.areaResponsable;
    }

    public void setAreaResponsable(String areaResponsable) {
        this.areaResponsable = areaResponsable;
    }

    public String getNumeroCredito() {
        return numeroCredito;
    }

    public void setNumeroCredito(String numeroCredito) {
        this.numeroCredito = numeroCredito;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public Date getFechaMutuo() {
        return fechaMutuo;
    }

    public void setFechaMutuo(Date fechaMutuo) {
        this.fechaMutuo = fechaMutuo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getComportamientoPago() {
        return comportamientoPago;
    }

    public void setComportamientoPago(String comportamientoPago) {
        this.comportamientoPago = comportamientoPago;
    }

    public double getCompensatorios() {
        return compensatorios;
    }

    public void setCompensatorios(double compensatorios) {
        this.compensatorios = compensatorios;
    }

    public double getMoratorios() {
        return moratorios;
    }

    public void setMoratorios(double moratorios) {
        this.moratorios = moratorios;
    }

    public double getPunitorios() {
        return this.punitorios;
    }

    public void setPunitorios(double punitorios) {
        this.punitorios = punitorios;
    }

    public double getDesembolsos() {
        return this.desembolsos;
    }

    public void setDesembolsos(double desembolsos) {
        this.desembolsos = desembolsos;
    }

    public double getDesembolsosPesos() {
        return this.desembolsosPesos;
    }

    public void setDesembolsosPesos(double desembolsosPesos) {
        this.desembolsosPesos = desembolsosPesos;
    }

    public double getSaldoCapitalNoCorrientePesos() {
        return this.saldoCapitalNoCorrientePesos;
    }

    public void setSaldoCapitalNoCorrientePesos(double saldoCapitalNoCorrientePesos) {
        this.saldoCapitalNoCorrientePesos = saldoCapitalNoCorrientePesos;
    }

    public double getCompensatoriosPesos() {
        return this.compensatoriosPesos;
    }

    public void setCompensatoriosPesos(double compensatoriosPesos) {
        this.compensatoriosPesos = compensatoriosPesos;
    }

    public double getMoratoriosPesos() {
        return this.moratoriosPesos;
    }

    public void setMoratoriosPesos(double moratoriosPesos) {
        this.moratoriosPesos = moratoriosPesos;
    }

    public double getPunitoriosPesos() {
        return this.punitoriosPesos;
    }

    public void setPunitoriosPesos(double punitoriosPesos) {
        this.punitoriosPesos = punitoriosPesos;
    }

    public double getOtrosPesos() {
        return this.otrosPesos;
    }

    public void setOtrosPesos(double otrosPesos) {
        this.otrosPesos = otrosPesos;
    }

    public double getSaldoTotalPesos() {
        return this.saldoTotalPesos;
    }

    public void setSaldoTotalPesos(double saldoTotalPesos) {
        this.saldoTotalPesos = saldoTotalPesos;
    }

    public double getCerValor() {
        return cerValor;
    }

    public void setCerValor(double cerValor) {
        this.cerValor = cerValor;
    }

    public String getJudicial() {
        return this.judicial;
    }

    public void setJudicial(String judicial) {
        this.judicial = judicial;
    }

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
    
    

}