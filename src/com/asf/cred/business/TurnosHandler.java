package com.asf.cred.business;

import java.util.Calendar;
import java.util.HashMap;

import javax.persistence.Transient;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.TipificadorHelper;
import com.nirven.creditos.hibernate.Turno;
import com.nirven.creditos.hibernate.Usuario;

public class TurnosHandler implements IProcess {
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "Turno";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Turno turno;
	private String persona;
	private Long personaid;
	private Usuario currentUser;

	private String hora;

	@Transient
	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	// =======================CONSTRUCTORES=========================
	public TurnosHandler() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		String idUsuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		this.currentUser = (Usuario) bp.getById(com.nirven.creditos.hibernate.Usuario.class, idUsuario);

		this.turno = new Turno();
		// turno.setUnidadId(this.currentUser.getUnidad().getId());
	}

	// ======================FUNCIONALIDADES========================
	@Override
	public boolean doProcess() {
		if (this.accion.equals("save")) {

			if (this.validate()) {
				this.guardarTurno();
				errores.put("turno.save.correcto", "turno.save.correcto");
			}

		} else {
			// this.turno = new Turno();
			turno.setUnidadId(this.currentUser.getUnidad().getId());
		}
		this.forward = "Turno";
		return this.errores.isEmpty();
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	String StringRemove(String string) {
		String stringremove = "0123456789-,";
		for (int i = 0; i <= stringremove.length() - 1; i++) {
			string = string.replaceAll(stringremove.substring(i, i + 1), "");
		}
		return string;
	}

	private void guardarTurno() {

		persona = turno.getNombre();
		int count = persona.length() - persona.replace("-", "").length();
		String[] parts = persona.split("-");
		if (count > 1) {
			persona = parts[1];
		}
		persona = StringRemove(persona).toUpperCase();
		turno.setNombre(persona);

		if (this.hora != null) {
			String[] temp;
			temp = hora.split(":");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(turno.getLlegada());
			calendar.set(Calendar.HOUR, new Integer(temp[0]).intValue());
			calendar.set(Calendar.MINUTE, new Integer(temp[1]).intValue());
			this.turno.setLlegada(calendar.getTime());
			String idUsuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
			this.turno.setAsesor((Usuario) bp.getById(Usuario.class, idUsuario));
			// TODO conozco el codigo y aun asi uso tipificador para traer el codigo Deberia
			// usarse una enumeracion de estados
			this.turno.setEstado(TipificadorHelper.getTipificador("turno.estado", "pendiente").getCodigo());

		}
		try {
			this.bp.saveOrUpdate(this.turno);
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("turno.estado.error", "turno.estado.error");
			this.forward = "Turno";
		}
		this.turno = new Turno();
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@Override
	public boolean validate() {
		if (this.turno.getLlegada() != null) {
			HashMap<String, String> error = new HashMap<String, String>();
			if (this.turno.getNombre() == null || this.turno.getNombre().trim().length() == 0) {
				error.put("turno.error.nombre", "turno.error.nombre");
			}
			this.errores.putAll(error);
		}
		return this.errores.isEmpty();
	}

	public String getPersona() {
		return persona;
	}

	public void setPersona(String persona) {
		this.persona = persona;
	}

	public Long getPersonaid() {
		return personaid;
	}

	public void setPersonaid(Long personaid) {
		this.personaid = personaid;
	}

}