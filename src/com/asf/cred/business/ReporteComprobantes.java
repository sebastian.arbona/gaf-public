package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Boleto;

public class ReporteComprobantes implements IProcess {

	private String forward;
	private HashMap<String, Object> errores;
	private String accion;
	private BusinessPersistance bp;

	private String tipoBoleto;
	private Date fechaProcesoDesde;
	private Date fechaProcesoHasta;

	private ArrayList<BoletoDTO> comprobantes;

	public ReporteComprobantes() {
		forward = "ReporteComprobantes";
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return false;
		}
		if (accion.equals("listar") && validate()) {
			listar();
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listar() {
		HashMap<Long, BoletoDTO> boletos = new HashMap<Long, BoletoDTO>();

		this.tipoBoleto = (this.tipoBoleto == null || this.tipoBoleto.isEmpty()) ? Boleto.TIPO_BOLETO_FACTURA
				: this.tipoBoleto;
		this.comprobantes = new ArrayList<BoletoDTO>();

		String consulta = "select c.boleto_id,c.fechaProceso from ctacte c where c.fechaProceso >= :fechaProcesoDesde and c.fechaProceso < :fechaProcesoHasta and c.boleto_id in (select b.id from boleto b where b.tipo = :tipoBoleto)";
		List<Object[]> lista = bp.createSQLQuery(consulta)
				.setDate("fechaProcesoDesde", DateHelper.resetHoraToZero(fechaProcesoDesde))
				.setDate("fechaProcesoHasta",
						DateHelper.resetHoraToZero(DateHelper.add(fechaProcesoHasta, 1, Calendar.DATE)))
				.setString("tipoBoleto", this.tipoBoleto).list();

		for (Object[] fila : lista) {
			Long idBoleto = ((BigDecimal) fila[0]).longValue();
			Date fechaProceso = (Date) fila[1];
			if (!boletos.containsKey(idBoleto)) {
				BoletoDTO boletoDto = new BoletoDTO(idBoleto, fechaProceso);
				boletos.put(idBoleto, boletoDto);
				comprobantes.add(boletoDto);
			}
		}
		Collections.sort(comprobantes);
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		this.errores = new HashMap<String, Object>();
		if (fechaProcesoDesde == null) {
			errores.put("generico.fechaDesde", "generico.fechaDesde");
		}
		if (fechaProcesoHasta == null) {
			errores.put("generico.fechaHasta", "generico.fechaHasta");
		}
		if (this.fechaProcesoDesde != null && this.fechaProcesoHasta != null
				&& this.fechaProcesoDesde.after(fechaProcesoHasta)) {
			errores.put("generico.fechaDesdeHasta", "generico.fechaDesdeHasta");
		}
		return errores.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getTipoBoleto() {
		return tipoBoleto;
	}

	public void setTipoBoleto(String tipoBoleto) {
		this.tipoBoleto = tipoBoleto;
	}

	public Date getFechaProcesoDesde() {
		return fechaProcesoDesde;
	}

	public void setFechaProcesoDesde(Date fechaProcesoDesde) {
		this.fechaProcesoDesde = fechaProcesoDesde;
	}

	public String getFechaProcesoDesdeStr() {
		return DateHelper.getString(this.getFechaProcesoDesde());
	}

	public void setFechaProcesoDesdeStr(String fecha) {
		this.setFechaProcesoDesde(DateHelper.getDate(fecha));
	}

	public Date getFechaProcesoHasta() {
		return fechaProcesoHasta;
	}

	public void setFechaProcesoHasta(Date fechaProcesoHasta) {
		this.fechaProcesoHasta = fechaProcesoHasta;
	}

	public String getFechaProcesoHastaStr() {
		return DateHelper.getString(this.getFechaProcesoHasta());
	}

	public void setFechaProcesoHastaStr(String fecha) {
		this.setFechaProcesoHasta(DateHelper.getDate(fecha));
	}

	public ArrayList<BoletoDTO> getComprobantes() {
		return comprobantes;
	}

	public void setComprobantes(ArrayList<BoletoDTO> comprobantes) {
		this.comprobantes = comprobantes;
	}

}
