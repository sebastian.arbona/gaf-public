SELECT id, numeroAtencion, fechaimputacion, fechaReal, NOMB_12, DATEDIFF( DAY, fechaimputacion, fechaReal) Dias
FROM (
SELECT O.id, O.numeroAtencion, I.importe, D.importeReal, I.fechaimputacion, D.fechaReal, P.NOMB_12
FROM @GAF_SCHEMA.imputacion I
INNER JOIN Objetoi O ON I.expepago = O.expediente
INNER JOIN Desembolso D ON D.credito_id = O.id AND D.numero = 1
INNER JOIN persona P ON P.IDPERSONA = O.persona_IDPERSONA
WHERE I.idimputacion IN(

    SELECT idimputacion FROM @GAF_SCHEMA.imputacion I2
    WHERE expepago = O.expediente
    AND idetapa in(SELECT idetapa FROM @GAF_SCHEMA.etapa WHERE orden = 5)
    AND fechaimputacion = (SELECT MIN(fechaImputacion) FROM @GAF_SCHEMA.imputacion WHERE expepago = I2.expepago and idetapa = I2.idetapa)
    )

GROUP BY I.idimputacion, I.importe, I.fechaimputacion, O.expediente, D.fechaReal, D.importeReal, O.numeroAtencion, P.NOMB_12, O.id
HAVING I.importe = (

    SELECT MAX(importe) FROM @GAF_SCHEMA.imputacion I3
    WHERE expepago = O.expediente
    AND idetapa in(SELECT idetapa FROM @GAF_SCHEMA.etapa WHERE orden = 5)
    AND fechaimputacion = (SELECT MIN(fechaImputacion) FROM @GAF_SCHEMA.imputacion WHERE expepago = I3.expepago and idetapa = I3.idetapa)
    )

)T
WHERE fechaimputacion <> fechaReal
AND id NOT IN (SELECT objetoi_id FROM ctaCte WHERE tipoMovimiento = 'Pago')