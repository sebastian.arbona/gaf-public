package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.mailer.Mail;

public class NuevaAuditoriaFinal extends ConversationProcess {

	private Date fechaPro = new Date();
	private String observacion;
	private String aplicaFon;
	private Date fechanoapli;
	private String caducidadpla;
	private Double multaporcen;

	private Double importeaudi;
	private String inttasa;
	private Double basecal;
	private String forwardURL;
	private String guarda;

	@Save
	private Long idObjetoi;
	@Save
	private Long idIndice;
	private Long idauditoriafinal;
	private FormFile formFile;
	ObjetoiArchivo objetoiArchivo;
	private AuditoriaFinal auditoria;
	private Indice indiceporcentaje;

	@ProcessMethod(defaultAction = true)
	public boolean mostrar() {
		if (idObjetoi != null) {
			forward = "NuevaAuditoriaFinal";
			guarda = "Si";
		} else if (idauditoriafinal != null) {
			load();
			forward = "NuevaAuditoriaFinal";
			guarda = "No";
			return true;
		}
		return true;
	}

	@ProcessMethod
	public boolean guardar() {

//		if (fechaPro == null || fechanoapli == null) {
//			errors.put("nuevaAuditoriaFinal.fechaPro", "nuevaAuditoriaFinal.fechaPro");
//			//errors.put("nuevaAuditoriaFinal.fechanoapli", "nuevaAuditoriaFinal.fechanoapli");
//			return false;
//		}

		try {

			if (idObjetoi != null && idObjetoi != 0L && idauditoriafinal == 0L) {
				AuditoriaFinal audi = new AuditoriaFinal();

				Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);

				audi.setCredito(credito);
				audi.setFechaProceso(fechaPro);
				audi.setObservaciones(observacion);
				audi.setAplicaFondo(aplicaFon);

				if (aplicaFon.equals("SI")) {
					audi.setFechanoaplica(null);
					audi.setCaducidadplazo(null);
					audi.setMultaporcentaje(null);
					audi.setImporteaudi(null);
					// este es el indice;
					audi.setInterestasa(null);
					audi.setBasecalculo(null);
				} else {
					audi.setFechanoaplica(fechanoapli);
					audi.setCaducidadplazo(caducidadpla);
					audi.setMultaporcentaje(multaporcen);
					audi.setImporteaudi(importeaudi);
					// este es el indice;
					audi.setInterestasa(inttasa);
					audi.setBasecalculo(basecal);
					if (aplicaFon.equals("NO")) {
						enviarMail(audi);
					}
				}

				if (formFile != null && formFile.getFileSize() > 0) {
					objetoiArchivo = new ObjetoiArchivo();
					objetoiArchivo.setCredito(credito);
					objetoiArchivo.setArchivo(formFile.getFileData());
					objetoiArchivo.setMimetype(formFile.getContentType());
					objetoiArchivo.setNombre(formFile.getFileName());
					objetoiArchivo.setFechaAdjunto(new Date());
					objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
					bp.saveOrUpdate(objetoiArchivo);
				}

				audi.setObjetoiArchivo(objetoiArchivo);
				bp.save(audi);

				forward = "ProcessRedirect";
				forwardURL = "/actions/process.do?do=process&processName=AuditoriaFinalProcess&process.accion=verNotif&process.idCredito="
						+ idObjetoi;

				return true;

			} else if (idauditoriafinal != null && idauditoriafinal != 0L) {

				AuditoriaFinal audifinal = (AuditoriaFinal) bp.getById(AuditoriaFinal.class, idauditoriafinal);

				if (audifinal != null) {
					audifinal.setFechaProceso(fechaPro);
					audifinal.setObservaciones(observacion);
					audifinal.setAplicaFondo(aplicaFon);
					audifinal.setFechanoaplica(fechanoapli);
					audifinal.setCaducidadplazo(caducidadpla);
					audifinal.setMultaporcentaje(multaporcen);
					audifinal.setImporteaudi(importeaudi);
					audifinal.setInterestasa(inttasa);
					audifinal.setBasecalculo(basecal);

					if (formFile != null && formFile.getFileSize() > 0) {
						objetoiArchivo = new ObjetoiArchivo();
						objetoiArchivo.setCredito(null);
						objetoiArchivo.setArchivo(formFile.getFileData());
						objetoiArchivo.setMimetype(formFile.getContentType());
						objetoiArchivo.setNombre(formFile.getFileName());
						objetoiArchivo.setFechaAdjunto(new Date());
						objetoiArchivo.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
						bp.saveOrUpdate(objetoiArchivo);
					}

					if (objetoiArchivo != null) {
						audifinal.setObjetoiArchivo(objetoiArchivo);
					}
					bp.update(audifinal);

					forward = "ProcessRedirect";
					forwardURL = "/actions/process.do?do=process&processName=AuditoriaFinalProcess&process.accion=verNotif&process.idCredito="
							+ idObjetoi;

					return true;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@ProcessMethod
	public boolean load() {

		if (idauditoriafinal != null) {
			try {
				setAuditoria((AuditoriaFinal) bp.getById(AuditoriaFinal.class, idauditoriafinal));
				idObjetoi = auditoria.getCredito().getId();
				fechaPro = auditoria.getFechaProceso();
				observacion = auditoria.getObservaciones();
				aplicaFon = auditoria.getAplicaFondo();
				fechanoapli = auditoria.getFechanoaplica();
				// indiceporcentaje = auditoria.getIndice();
				caducidadpla = auditoria.getCaducidadplazo();
				multaporcen = auditoria.getMultaporcentaje();
				importeaudi = auditoria.getImporteaudi();
				inttasa = auditoria.getInterestasa();
				basecal = auditoria.getBasecalculo();
				objetoiArchivo = auditoria.getObjetoiArchivo();
				return true;
			} catch (Exception e) {
				this.setAuditoria(new AuditoriaFinal());
				this.errors.put("AuditoriaFinal.Error.cargar", "AuditoriaFinal.Error.carga");
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	private void enviarMail(AuditoriaFinal audi) {
		String asunto = "Aplic� fondos (estado NO)";
		// En el cuerpo del mail se debe notificar el n�mero de proyecto, titular, l�nea
		// de cr�dito y la fecha de proceso de dicha auditor�a.
		String linea_credito = audi.getCredito().getLinea().getDetalle();
		String numero_proyecto = audi.getCredito().getNumeroAtencionStr();
		String titular = audi.getCredito().getPersona().getNomb12();

		Date d = Calendar.getInstance().getTime(); // Current time
		SimpleDateFormat dma = new SimpleDateFormat("dd-MM-yyyy"); // Set your date format
		String fecha_proc = dma.format(audi.getFechaProceso()); // Get Date String according to date format

		try {
			for (String mail : formatearMails()) {
				String[] a = mail.split("\\|");
				if (a.length == 2) {
					try {
						String sql = "select p from Persona p where p.id = :id";
						Persona p = (Persona) bp.createQuery(sql).setString("id", a[0]).uniqueResult();

						for (com.civitas.hibernate.persona.creditos.Contacto cont : p.getContactos()) {
							if (cont.getMedio().equals("E-Mail") && cont.getTipo().equals(a[1])) {

								String elmail = cont.getDetalle();

								String mensaje = "Proyecto: " + numero_proyecto + System.lineSeparator();
								mensaje += "Titular: " + titular + System.lineSeparator();
								mensaje += "Fecha de proceso: " + fecha_proc + System.lineSeparator();
								mensaje += "Linea: " + linea_credito + System.lineSeparator();
								Mail.sendMailTemplate(elmail, asunto, "mails/templateMail.html", mensaje);

							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<String> formatearMails() {
		ArrayList<String> result = new ArrayList<String>();
		String tipopro = DirectorHelper.getString("TIPO.PRO");
		for (String mail : tipopro.split(",")) {
			result.add(mail);
		}
		return result;
	}

	@Override
	protected String getDefaultForward() {
		return "NuevaAuditoriaFinal";
	}

	public String getFechaPro() {
		return DateHelper.getString(fechaPro);
	}

	public void setFechaPro(String fechaPro) {
		this.fechaPro = DateHelper.getDate(fechaPro);
	}

	// public void setFechaPro(Date fechaPro) {
	// this.fechaPro = fechaPro;
	// }

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getAplicaFon() {
		return aplicaFon;
	}

	public void setAplicaFon(String aplicaFon) {
		this.aplicaFon = aplicaFon;
	}

	public String getFechanoapli() {
		return DateHelper.getString(fechanoapli);
	}

	public void setFechanoapli(String fechanoapli) {
		this.fechanoapli = DateHelper.getDate(fechanoapli);
	}

	public String getCaducidadpla() {
		return caducidadpla;
	}

	public void setCaducidadpla(String caducidadpla) {
		this.caducidadpla = caducidadpla;
	}

	public Double getMultaporcen() {
		return multaporcen;
	}

	public void setMultaporcen(Double multaporcen) {
		this.multaporcen = multaporcen;
	}

	public Double getImporteaudi() {
		return importeaudi;
	}

	public void setImporteaudi(Double importeaudi) {
		this.importeaudi = importeaudi;
	}

	public String getInttasa() {
		return inttasa;
	}

	public void setInttasa(String inttasa) {
		this.inttasa = inttasa;
	}

	public Double getBasecal() {
		return basecal;
	}

	public void setBasecal(Double basecal) {
		this.basecal = basecal;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Long getIdindice() {
		return idIndice;
	}

	public void setIdindice(Long idIndice) {
		this.idIndice = idIndice;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public ObjetoiArchivo getObjetoiArchivo() {
		return objetoiArchivo;
	}

	public void setObjetoiArchivo(ObjetoiArchivo objetoiArchivo) {
		this.objetoiArchivo = objetoiArchivo;
	}

	public AuditoriaFinal getAuditoria() {
		return auditoria;
	}

	public void setAuditoria(AuditoriaFinal auditoria) {
		this.auditoria = auditoria;
	}

	public Indice getIndiceporcentaje() {
		return indiceporcentaje;
	}

	public void setIndiceporcentaje(Indice indiceporcentaje) {
		this.indiceporcentaje = indiceporcentaje;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public Long getIdauditoriafinal() {
		return idauditoriafinal;
	}

	public void setIdauditoriafinal(Long idauditoriafinal) {
		this.idauditoriafinal = idauditoriafinal;
	}

	public String getGuarda() {
		return guarda;
	}

	public void setGuarda(String guarda) {
		guarda = guarda;
	}

}
