package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpSession;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.ConsultaDeudaPersona;

public class ConsultaDeuda implements IProcess {
	private BusinessPersistance bp;

	private String action;
	private Long idConsultaDeuda;
	private String mostrar;
	private String fechaDesdeStr;
	private String fechaHastaStr;
	private Integer estado = 0;
	private String idpersonaStr = "";

	private String forward = "ConsultaDeuda";
	private HashMap<String, Object> chErrores;
	private int detalleQuitar;

	private boolean mendozaFiduciaria;
	private boolean fondoVitivinicola;
	private boolean emergenciaAgropecuaria;
	private boolean deudaProvincialDelFondo;
	private boolean cfiTurismo;

	private Integer caracterSelected;
	private Long idpersona;
	private List<ConsultaDeudaPersona> consultaDeudaPersonaArray = new ArrayList<ConsultaDeudaPersona>();

	private List<ConsultaDeuda> consultaDeudaList;
	private List<ConsultaDeudaPersona> consultaDeudaPersonaList;

	private ReportResult rr;

	// =====================CONSTRUCTORES===========================
	public ConsultaDeuda() {
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.mostrar = "lista";
		this.chErrores = new HashMap<String, Object>();
	}

	// =====================FUNCIONALIDAD===========================
	@Override
	public boolean doProcess() {
		if (this.action == null || this.action.equals("cargar")) {
			cargar();
		}

		else if (this.action.equals("iniciar")) {
			iniciar(this.idConsultaDeuda);
			this.action = "consultar";
			consultaFiltro();
		} else if (this.action.equals("eliminar")) {
			eliminar(this.idConsultaDeuda);
			this.idConsultaDeuda = null;
			this.action = "consultar";
			consultaFiltro();
		}

		else if (this.action.equals("nuevaConsultaDeuda")) {
			nuevaConsultaDeuda();
		}

		else if (this.action.equals("nuevaConsultaDeudaMasiva")) {
			nuevaConsultaDeudaMasiva();
		}

		else if (this.action.equals("agregarPersona")) {
			agregarPersona();
		}

		else if (this.action.equals("eliminarPersona")) {
			eliminarPersona();
		}

		else if (this.action.equals("guardarConsultaDeudaPersona")) {
			this.idConsultaDeuda = guardarConsultaDeudaPersona();
			if (this.chErrores.isEmpty()) {
				consultaFiltro();
			}
		}

		else if (this.action.equals("consultar")) {
			consultaFiltro();
		}

		else if (this.action.equals("imprimirReporte")) {
			imprimirReporte();
		}

		return this.chErrores.isEmpty();
	}

	private void iniciar(Long idConsultaDeuda2) {
		try {
			((com.nirven.creditos.hibernate.ConsultaDeuda) SessionHandler.getSessionHandler().getBusinessPersistance()
					.getById(com.nirven.creditos.hibernate.ConsultaDeuda.class, idConsultaDeuda2)).initProcess();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void eliminar(Long idConsultaDeuda2) {
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			com.nirven.creditos.hibernate.ConsultaDeuda c = (com.nirven.creditos.hibernate.ConsultaDeuda) bp
					.getById(com.nirven.creditos.hibernate.ConsultaDeuda.class, idConsultaDeuda2);
			bp.delete(c.getPersonas());
			bp.delete(c);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	private void cargar() {
		String consulta;

		consulta = "FROM ConsultaDeuda cons ORDER BY cons.id DESC";

		this.consultaDeudaList = this.bp.createQuery(consulta).setMaxResults(20).list();
	}

	private void nuevaConsultaDeuda() {
		this.consultaDeudaPersonaArray = new ArrayList<ConsultaDeudaPersona>();

		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		session.setAttribute("consultaDeudaPersonaArray", this.consultaDeudaPersonaArray);

		this.mostrar = "nuevaConsultaDeudaPersona";
	}

	private void nuevaConsultaDeudaMasiva() {

		this.consultaDeudaPersonaArray = new ArrayList<ConsultaDeudaPersona>();

		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		session.setAttribute("consultaDeudaPersonaArray", this.consultaDeudaPersonaArray);

		this.mostrar = "nuevaConsultaDeudaMasiva";
	}

	@SuppressWarnings("unchecked")
	private void consultaFiltro() {
		String filtro = "";
		String join = "";
		String joinConsultaDeudaPersona = "";
		String consulta;
		SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");

		if (this.fechaDesdeStr != null && this.fechaDesdeStr.trim().length() != 0)
			filtro += " AND cons.fecha >= '" + formatoFecha.format(DateHelper.getDate(this.fechaDesdeStr)) + "'";

		if (this.fechaHastaStr != null && this.fechaHastaStr.trim().length() != 0)
			filtro += " AND cons.fecha <= '" + formatoFecha.format(DateHelper.getDate(this.fechaHastaStr)) + "'";

		if (this.idConsultaDeuda != null && this.idConsultaDeuda > 0)
			filtro += " AND cons.id = " + this.idConsultaDeuda;
		else
			this.idConsultaDeuda = null;

		if (this.idpersonaStr != null && this.idpersonaStr.trim().length() != 0) {
			joinConsultaDeudaPersona = " LEFT JOIN cons.personas consper";
			filtro += " AND consper.persona.idpersona = " + this.idpersonaStr;
		}

		if (this.estado != null && this.estado > 0) {
			// 1 no ini 2 inic 3 final
			if (estado.intValue() == 1) {
				join = " left join cons.proceso pr ";
				filtro += " and pr.id is null";
			}

			if (estado.intValue() == 2) {
				filtro += " and cons.proceso.id is not null and cons.proceso.end is null";
			}
			if (estado.intValue() == 3) {
				filtro += " and cons.proceso.id is not null and cons.proceso.end is not null";
			}
		}

		consulta = "SELECT cons FROM ConsultaDeuda cons " + join + joinConsultaDeudaPersona + " WHERE 1=1" + filtro
				+ " ORDER BY cons.id DESC";

		this.consultaDeudaList = this.bp.createQuery(consulta).list();
	}

	@SuppressWarnings("unchecked")
	public void agregarPersona() {
		Persona persona;
		ConsultaDeudaPersona consultaDeudaPersona = new ConsultaDeudaPersona();

		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		this.consultaDeudaPersonaArray = (List<ConsultaDeudaPersona>) session.getAttribute("consultaDeudaPersonaArray");

		if (this.idpersona > 0) {
			persona = (Persona) this.bp.createQuery("FROM Persona per WHERE per.idpersona = " + this.idpersona).list()
					.get(0);

			if (persona.getFechaBaja() != null) {
				chErrores.put("Solicitud.error.estodoPersonaBaja", "Solicitud.error.estodoPersonaBaja");
			} else {
				consultaDeudaPersona.setCaracterSelected(this.caracterSelected);
				consultaDeudaPersona.setPersona(persona);
				this.consultaDeudaPersonaArray.add(consultaDeudaPersona);
			}
		}

		this.mostrar = "nuevaConsultaDeudaPersona";
	}

	@SuppressWarnings("unchecked")
	public void agregarPersonasMasivo(String predicate) {
		List<Object> personas = this.bp.createQuery("FROM Persona per WHERE per.cuil12 IN " + predicate).list();
		Iterator<Object> it = personas.iterator();

		while (it.hasNext()) {
			Persona persona = (Persona) it.next();
			if (persona.getFechaBaja() == null) {
				ConsultaDeudaPersona elementoDeudaPersona = new ConsultaDeudaPersona();
				elementoDeudaPersona.setCaracterSelected(1);
				elementoDeudaPersona.setPersona(persona);
				this.consultaDeudaPersonaArray.add(elementoDeudaPersona);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void eliminarPersona() {
		HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
		this.consultaDeudaPersonaArray = (List<ConsultaDeudaPersona>) session.getAttribute("consultaDeudaPersonaArray");

		this.consultaDeudaPersonaArray.remove(this.detalleQuitar - 1);
		this.mostrar = "nuevaConsultaDeudaPersona";
	}

	@SuppressWarnings("unchecked")
	private Long guardarConsultaDeudaPersona() {
		this.bp.begin();

		try {
			HttpSession session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
			this.consultaDeudaPersonaArray = (List<ConsultaDeudaPersona>) session
					.getAttribute("consultaDeudaPersonaArray");

			com.nirven.creditos.hibernate.ConsultaDeuda consultaDeuda = new com.nirven.creditos.hibernate.ConsultaDeuda();
			consultaDeuda.setMf(this.mendozaFiduciaria);
			consultaDeuda.setFv(this.fondoVitivinicola);
			consultaDeuda.setEa(this.emergenciaAgropecuaria);
			consultaDeuda.setFt(this.deudaProvincialDelFondo);
			consultaDeuda.setCfi(this.cfiTurismo);
			consultaDeuda.setFecha(new Date());

			this.bp.saveOrUpdate(consultaDeuda);

			for (ConsultaDeudaPersona consultaDeudaPersona : this.consultaDeudaPersonaArray) {
				consultaDeudaPersona.setConsulta(consultaDeuda);
			}

			this.bp.saveOrUpdate(this.consultaDeudaPersonaArray);
			this.bp.commit();
			this.bp.getCurrentSession().evict(consultaDeuda);
			return consultaDeuda.getId();
		} catch (Exception e) {
			e.printStackTrace();
			bp.rollback();
		}
		return null;

	}

	private void imprimirReporte() {
		com.nirven.creditos.hibernate.ConsultaDeuda consultaDeudaAImprimir;

		consultaDeudaAImprimir = (com.nirven.creditos.hibernate.ConsultaDeuda) bp
				.createQuery("FROM ConsultaDeuda con WHERE con.id = " + this.getIdConsultaDeuda()).list().get(0);

		this.rr = new ReportResult();

		this.rr.setParams(
				"ID=" + this.getIdConsultaDeuda() + ";FECHA=" + consultaDeudaAImprimir.getFechaStr() + ";ESTADO="
						+ (consultaDeudaAImprimir.getProceso() == null ? "No Iniciado"
								: (consultaDeudaAImprimir.getEndStr() == null ? "Iniciado" : "Finalizado"))
						+ ";MF=" + consultaDeudaAImprimir.isMf() + ";FV=" + consultaDeudaAImprimir.isFv() + ";EA="
						+ consultaDeudaAImprimir.isEa() + ";FT=" + consultaDeudaAImprimir.isFt() + ";SCHEMA="
						+ BusinessPersistance.getSchema() + ";");

		this.rr.setReportName("ConsultaDeDeuda.jasper");
		this.rr.setExport("PDF");
		this.forward = "ReportesProcess";
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return this.chErrores;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public boolean validate() {
		return this.chErrores.isEmpty();
	}

	@Override
	public Object getResult() {
		return this.rr;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Long getIdConsultaDeuda() {
		return this.idConsultaDeuda;
	}

	public void setIdConsultaDeuda(Long idConsultaDeuda) {
		this.idConsultaDeuda = idConsultaDeuda;
	}

	public String getMostrar() {
		return this.mostrar;
	}

	public void setMostrar(String mostrar) {
		this.mostrar = mostrar;
	}

	public List<ConsultaDeuda> getConsultaDeudaList() {
		return this.consultaDeudaList;
	}

	public void setConsultaDeudaList(List<ConsultaDeuda> consultaDeudaList) {
		this.consultaDeudaList = consultaDeudaList;
	}

	public List<ConsultaDeudaPersona> getConsultaDeudaPersonaList() {
		return this.consultaDeudaPersonaList;
	}

	public void setConsultaDeudaPersonaList(List<ConsultaDeudaPersona> consultaDeudaPersonaList) {
		this.consultaDeudaPersonaList = consultaDeudaPersonaList;
	}

	public String getFechaDesdeStr() {
		return this.fechaDesdeStr;
	}

	public void setFechaDesdeStr(String fechaDesdeStr) {
		this.fechaDesdeStr = fechaDesdeStr;
	}

	public String getFechaHastaStr() {
		return this.fechaHastaStr;
	}

	public void setFechaHastaStr(String fechaHastaStr) {
		this.fechaHastaStr = fechaHastaStr;
	}

	public boolean getMendozaFiduciaria() {
		return this.mendozaFiduciaria;
	}

	public void setMendozaFiduciaria(boolean mendozaFiduciaria) {
		this.mendozaFiduciaria = mendozaFiduciaria;
	}

	public boolean getFondoVitivinicola() {
		return this.fondoVitivinicola;
	}

	public void setFondoVitivinicola(boolean fondoVitivinicola) {
		this.fondoVitivinicola = fondoVitivinicola;
	}

	public boolean getEmergenciaAgropecuaria() {
		return this.emergenciaAgropecuaria;
	}

	public void setEmergenciaAgropecuaria(boolean emergenciaAgropecuaria) {
		this.emergenciaAgropecuaria = emergenciaAgropecuaria;
	}

	public boolean getDeudaProvincialDelFondo() {
		return this.deudaProvincialDelFondo;
	}

	public void setDeudaProvincialDelFondo(boolean deudaProvincialDelFondo) {
		this.deudaProvincialDelFondo = deudaProvincialDelFondo;
	}
	
	public boolean getCfiTurismo() {
		return this.cfiTurismo;
	}

	public void setCfiTurismo(boolean deudaProvincialDelFondo) {
		this.cfiTurismo = deudaProvincialDelFondo;
	}

	public int getDetalleQuitar() {
		return detalleQuitar;
	}

	public void setDetalleQuitar(int detalleQuitar) {
		this.detalleQuitar = detalleQuitar;
	}

	public Long getIdpersona() {
		return this.idpersona;
	}

	public void setIdpersona(Long idpersona) {
		this.idpersona = idpersona;
	}

	public Integer getCaracterSelected() {
		return this.caracterSelected;
	}

	public void setCaracterSelected(Integer caracterSelected) {
		this.caracterSelected = caracterSelected;
	}

	public List<ConsultaDeudaPersona> getConsultaDeudaPersonaArray() {
		return this.consultaDeudaPersonaArray;
	}

	public void setConsultaDeudaPersonaArray(List<ConsultaDeudaPersona> consultaDeudaPersonaArray) {
		this.consultaDeudaPersonaArray = consultaDeudaPersonaArray;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getEstadoStr() {

		switch (estado) {
		case 0:
			return "Todos";
		case 1:
			return "No Iniciados";
		case 2:
			return "Iniciados/Pendientes";
		case 3:
			return "Finalizados";

		}
		return "";
	}

	public String getIdpersonaStr() {
		return this.idpersonaStr;
	}

	public void setIdpersonaStr(String idpersonaStr) {
		this.idpersonaStr = idpersonaStr;
	}
}