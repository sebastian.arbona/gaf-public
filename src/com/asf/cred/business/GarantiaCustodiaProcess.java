package com.asf.cred.business;

import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.GarantiaCustodia;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.expedientes.persistencia.Numerador;

public class GarantiaCustodiaProcess extends ConversationProcess {

	@Save
	private Long id;
	@Save
	private ObjetoiGarantia objetoiGarantia;
	private GarantiaCustodia custodia = new GarantiaCustodia();
	@Save(refresh = true)
	private GarantiaCustodia custodiaActual;
	@Save
	private boolean alta;
	private List<GarantiaCustodia> custodias;
	private String forwardURL;

	@ProcessMethod
	public boolean nuevo() {
		objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id);

		custodiaActual = (GarantiaCustodia) bp.createQuery(
				"select c from GarantiaCustodia c where c.objetoiGarantia.id = :ogid " + "order by c.orden desc")
				.setParameter("ogid", id).setMaxResults(1).uniqueResult();

		alta = custodiaActual == null;

		custodia = new GarantiaCustodia();
		custodia.setNroInventario(custodiaActual != null ? custodiaActual.getNroInventario() : null);
		custodia.setOrden(custodiaActual != null ? custodiaActual.getOrden() + 1 : 1);
		custodia.setFechaIngreso(new Date());

		return true;
	}

	@ProcessMethod
	public boolean guardar() {
		if (custodia.getSector() == null) {
			errors.put("garantias.custodia.sector", "garantias.custodia.sector");
			return false;
		}
		if (custodia.getResponsable() == null) {
			errors.put("garantias.custodia.responsable", "garantias.custodia.responsable");
			return false;
		}
		if (custodia.getFechaIngreso() == null) {
			errors.put("garantias.custodia.fechaIngreso", "garantias.custodia.fechaIngreso");
			return false;
		}
		if (custodia.getFechaIngreso().after(new Date())) {
			errors.put("garantias.custodia.fechaIngresoMayor", "garantias.custodia.fechaIngresoMayor");
			return false;
		}

		if (custodiaActual == null) {
			Long nroInventario = Numerador.getNext("GarantiaCustodia");
			custodia.setNroInventario(nroInventario);
			custodia.setOrden(1);
		} else {
			custodia.setNroInventario(custodiaActual.getNroInventario());
			custodia.setOrden(custodiaActual.getOrden() + 1);
		}
		custodia.setObjetoiGarantia((ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id));
		bp.save(custodia);

		forward = "ProcessRedirect";
		forwardURL = "/actions/process.do?processName=GarantiaProcess&do=process&process.accionStr=MODIFICAR_GARANTIA&process.idSolicitud="
				+ custodia.getObjetoiGarantia().getObjetoi().getId() + "&process.oGarantiaId="
				+ custodia.getObjetoiGarantia().getId();

		return true;
	}

	// TODO: pagina garantiaCustodiaList - historico custodia
	@ProcessMethod
	@SuppressWarnings("unchecked")
	public boolean listar() {
		objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id);
		custodias = bp
				.createQuery(
						"select c from GarantiaCustodia c where c.objetoiGarantia.id = :ogid " + "order by c.orden")
				.setParameter("ogid", id).list();
		forward = "GarantiaCustodiaList";
		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "GarantiaCustodia";
	}

	public String getResponsableId() {
		if (custodia.getResponsable() != null) {
			return custodia.getResponsable().getId() + "-" + custodia.getResponsable().getNomb12Str() + "-"
					+ custodia.getResponsable().getNudo12Str();
		}
		return "";
	}

	public void setResponsableId(String id) {
		if (id != null && !id.trim().isEmpty()) {
			String[] partes = id.split("-");
			Long idResp = new Long(partes[0]);
			custodia.setResponsable((Persona) bp.getById(Persona.class, idResp));
		} else {
			custodia.setResponsable(null);
		}
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GarantiaCustodia getCustodia() {
		return custodia;
	}

	public boolean isAlta() {
		return alta;
	}

	public List<GarantiaCustodia> getCustodias() {
		return custodias;
	}

	public ObjetoiGarantia getObjetoiGarantia() {
		return objetoiGarantia;
	}

	public void setObjetoiGarantia(ObjetoiGarantia objetoiGarantia) {
		this.objetoiGarantia = objetoiGarantia;
	}

}
