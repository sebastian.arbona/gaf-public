package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Ctacte;

/**
 * @deprecated Se unifico con Cta Cte Ampliada
 * @author andres
 *
 */
@Deprecated
public class CuentaCorrienteContableBean implements Serializable {

	private static final long serialVersionUID = -7372259933449986249L;
	
	private Date fechaGeneracion;
	private Date fechaProceso;
	private Date fechaVencimiento;
	private Long asiento;
	private Long idBoleto;
	private String tipoBoleto;
	private Long numeroBoleto;
	private String detalle;
	private double debe; 
	private double haber;
	private double saldo;
	private double debePeso; 
	private double haberPeso;
	private double saldoPeso;
	private Long periodoCtacte;
    private Long movimientoCtacte;
    private Long verificadorCtacte;
    private Long itemCtacte;
    private double cotizaMov;
	
    private Long idCaratula;
    private Long idBoletoRecibo;
    
    private Long idOrdenPago;
    private Long numeroOrdenPago;
    private Long ejercicioOrdenPago;
    private String motivo;
    private Long idObjetoi;
    private boolean diferenciaCotizacion;
    
    public CuentaCorrienteContableBean(){
    	
    }
    
	public CuentaCorrienteContableBean(Object[] r) {
		this.fechaGeneracion = (Date) r[0];
		Number periodo = (Number) r[1];
		this.periodoCtacte = periodo != null ? periodo.longValue() : null;
		this.tipoBoleto = (String) r[2];
		Number nroBol = (Number) r[3];
		this.numeroBoleto = nroBol != null ? nroBol.longValue() : null;
		Number idBol = (Number) r[4];
		this.idBoleto = idBol != null ? idBol.longValue() : null;
		this.detalle = (String) r[7];
		Double d = (Double) r[8];
		this.debe = d != null ? d : 0;
		Double h = (Double) r[9];
		this.haber = h != null ? h : 0;
		Number mc = (Number) r[10];
		this.movimientoCtacte = mc != null ? mc.longValue() : null;
		Number vc = (Number) r[11];
		this.verificadorCtacte = vc != null ? vc.longValue() : null;
		Number ic = (Number) r[12];
		this.itemCtacte = ic != null ? ic.longValue() : null;
		
		Number car = (Number) r[13];
		this.idCaratula = car != null ? car.longValue() : 0;
		Number recibo = (Number) r[14];
		this.idBoletoRecibo = recibo != null ? recibo.longValue() : 0;		
		Number idObjetoi = (Number) r[15];
		this.idObjetoi = idObjetoi != null ? idObjetoi.longValue() : null;
		this.fechaProceso = (Date) r[16];
		this.fechaVencimiento = (Date) r[17];

		if (r.length > 18) {
			Double cotizaMov = (Double) r[18];
			this.cotizaMov = cotizaMov != null ? cotizaMov : 0; 
			
			Double debePesos = (Double) r[19];
			this.debePeso = debePesos != null ? debePesos : 0;
			
			Double haberPesos = (Double) r[20];
			this.haberPeso = haberPesos != null ? haberPesos : 0;
		}
		
		
	}
	
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public String getFechaGeneracionStr() {
		return DateHelper.getString(fechaGeneracion);
	}
	public Long getAsiento() {
		return asiento;
	}
	public void setAsiento(Long asiento) {
		this.asiento = asiento;
	}
	public Long getIdBoleto() {
		return idBoleto;
	}
	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}
	public String getTipoBoleto() {
		return tipoBoleto;
	}
	public void setTipoBoleto(String tipoBoleto) {
		this.tipoBoleto = tipoBoleto;
	}
	public Long getNumeroBoleto() {
		return numeroBoleto;
	}
	public void setNumeroBoleto(Long numeroBoleto) {
		this.numeroBoleto = numeroBoleto;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public double getDebe() {
		return debe;
	}
	public void setDebe(double debe) {
		this.debe = debe;
	}
	public double getHaber() {
		return haber;
	}
	public void setHaber(double haber) {
		this.haber = haber;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public Long getPeriodoCtacte() {
		return periodoCtacte;
	}
	public void setPeriodoCtacte(Long periodoCtacte) {
		this.periodoCtacte = periodoCtacte;
	}
	public Long getMovimientoCtacte() {
		return movimientoCtacte;
	}
	public void setMovimientoCtacte(Long movimientoCtacte) {
		this.movimientoCtacte = movimientoCtacte;
	}
	public Long getVerificadorCtacte() {
		return verificadorCtacte;
	}
	public void setVerificadorCtacte(Long verificadorCtacte) {
		this.verificadorCtacte = verificadorCtacte;
	}
	public Long getItemCtacte() {
		return itemCtacte;
	}
	public void setItemCtacte(Long itemCtacte) {
		this.itemCtacte = itemCtacte;
	}
	public Long getIdCaratula() {
		return idCaratula;
	}
	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}
	public Long getIdBoletoRecibo() {
		return idBoletoRecibo;
	}
	public void setIdBoletoRecibo(Long idBoletoRecibo) {
		this.idBoletoRecibo = idBoletoRecibo;
	}
	public Long getIdOrdenPago() {
		return idOrdenPago;
	}
	public void setIdOrdenPago(Long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}
	public Long getNumeroOrdenPago() {
		return numeroOrdenPago;
	}
	public void setNumeroOrdenPago(Long numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}
	public Long getEjercicioOrdenPago() {
		return ejercicioOrdenPago;
	}
	public void setEjercicioOrdenPago(Long ejercicioOrdenPago) {
		this.ejercicioOrdenPago = ejercicioOrdenPago;
	}
	public double getDebePeso() {
		return debePeso;
	}
	public double getHaberPeso() {
		return haberPeso;
	}
	public double getSaldoPeso() {
		return saldoPeso;
	}
	public double getCotizaMov() {
		return cotizaMov;
	}

	public void setDebePeso(double debePeso) {
		this.debePeso = debePeso;
	}

	public void setHaberPeso(double haberPeso) {
		this.haberPeso = haberPeso;
	}

	public void setSaldoPeso(double saldoPeso) {
		this.saldoPeso = saldoPeso;
	}

	public void setCotizaMov(double cotizaMov) {
		this.cotizaMov = cotizaMov;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getMotivo() {
		return motivo;
	}
	
	public Long getIdObjetoi() {
		return idObjetoi;
	}
	
	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}
	
	public Long getNroAsiento(){
		if(isDiferenciaCotizacion()){
			return null;
		}else{
		Ctacte cc = new Ctacte();
		cc.getId().setItemCtacte(itemCtacte);
		cc.getId().setMovimientoCtacte(movimientoCtacte);
		cc.getId().setObjetoi_id(idObjetoi);
		cc.getId().setPeriodoCtacte(periodoCtacte);
		cc.getId().setVerificadorCtacte(verificadorCtacte);
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		cc = (Ctacte) bp.getById(Ctacte.class, cc.getId());
		return cc.getNroAsiento(); 
		}
	}
	
	public Long getIdAsiento(){
		if(isDiferenciaCotizacion()){
			return null;
		}else{
		Ctacte cc = new Ctacte();
		cc.getId().setItemCtacte(itemCtacte);
		cc.getId().setMovimientoCtacte(movimientoCtacte);
		cc.getId().setObjetoi_id(idObjetoi);
		cc.getId().setPeriodoCtacte(periodoCtacte);
		cc.getId().setVerificadorCtacte(verificadorCtacte);
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		cc = (Ctacte) bp.getById(Ctacte.class, cc.getId());
		return cc.getIdAsiento(); 
		}
	}
	
	public boolean isDiferenciaCotizacion() {
		return diferenciaCotizacion;
	}
	
	public void setDiferenciaCotizacion(boolean diferenciaCotizacion) {
		this.diferenciaCotizacion = diferenciaCotizacion;
	}
	
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getFechaProcesoStr() {
		return DateHelper.getString(fechaProceso);
	}
	
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public String getFechaVencimientoStr() {
		return DateHelper.getString(fechaVencimiento);
	}
}
