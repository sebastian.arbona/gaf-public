package com.asf.cred.business;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class InformeDeudaThreadPoolListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent e) {
		ExecutorService threadPool = (ExecutorService) e.getServletContext().getAttribute("InformeDeudaThreadPool");
	    threadPool.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent e) {
		ExecutorService threadPool = Executors.newFixedThreadPool(4);
	    e.getServletContext().setAttribute("InformeDeudaThreadPool", threadPool);
	}

}
