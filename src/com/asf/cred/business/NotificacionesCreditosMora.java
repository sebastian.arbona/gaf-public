package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.cred.beans.BeanNotificacion;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;

public class NotificacionesCreditosMora implements IProcess {

	private String forward;
	private String action;
	private List<Objetoi> creditos;
	private List<BeanNotificacion> creditosBean;
	private List<Linea> lineas;

	private String[] idsLineas;
	private String codigo;
	private Long numeroAtencion;
	private Date fechaMora;

	protected HashMap<String, Object> errores = new HashMap<String, Object>();
	private Long idPersona;

	private List<AuditoriaFinal> auditoriaFinal;
	private String auditoriaFinalPos;

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lineas = bp.getByFilter("select l from Linea l order by l.nombre");

		if (action == null) {
			forward = "NotificacionesCreditosMora";
		} else if (action.equalsIgnoreCase("listar")) {

			// Filtros para la consulta
			String estadosWhere = "('EJECUCION', 'PRIMER DESEMBOLSO', 'PENDIENTE SEGUNDO DESEMBOLSO')";
			String numeroAtencionWhere = "";
			String codigoWhere = "";
			String personaWhere = "";

			// Caso: Numero de Atenci�n especefico
			if (numeroAtencion != null && numeroAtencion != 0L)
				numeroAtencionWhere = "AND o.numeroAtencion = " + this.numeroAtencion;

			// Caso: Estado especefico
			if (this.codigo != null && !this.codigo.isEmpty())
				codigoWhere = "AND oc.comportamientoPago = " + this.codigo;

			// Caso: Persona especefica
			if (idPersona != null && idPersona != 0L)
				personaWhere = "AND o.persona.id = " + this.idPersona;

			List<Long> lineasSeleccionadas = new ArrayList<Long>();
			if (idsLineas != null) {
				for (String l : idsLineas) {
					lineasSeleccionadas.add(new Long(l));
				}
			}

			// Caso con fecha de mora
			if (fechaMora != null) {
				List<Long> creditosId = bp
						.createQuery("select distinct o.id from Cuota c join c.credito o "
								+ "where c.fechaVencimiento = :fecha and c.fechaVencimiento < :hoy and c.estado = '1' ")
						.setParameter("fecha", fechaMora).setParameter("hoy", new Date()).list();

				// Los conjuntos no pueden ser vacios en la consulta
				if (creditosId.isEmpty())
					creditosId.add(new Long(0));

				// Caso: Linea especefica
				if (!lineasSeleccionadas.isEmpty()) {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere
								+ " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).setParameterList("linea", lineasSeleccionadas)
								.setParameterList("objetoiId", creditosId).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere
								+ " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).setParameterList("linea", lineasSeleccionadas)
								.setParameterList("objetoiId", creditosId).list();
					}

				}

				// Caso: Todas las lineas
				else {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).setParameterList("objetoiId", creditosId).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND o.id IN (:objetoiId) AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).setParameterList("objetoiId", creditosId).list();
					}

				}
			}

			// Caso sin fecha de mora
			else {
				// Caso: Linea especefica
				if (!lineasSeleccionadas.isEmpty()) {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).setParameterList("linea", lineasSeleccionadas).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).setParameterList("linea", lineasSeleccionadas).list();
					}

				}

				// Caso: Todas las lineas
				else {

					if (idPersona != null && idPersona != 0L) {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ personaWhere).list();
					} else {
						this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
								+ "JOIN oc.objetoi o "
								+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
								+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
								+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
								+ codigoWhere + " AND oc.fechaHasta IS NULL "
								+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) "
								+ numeroAtencionWhere).list();
					}

				}
			}
			creditosBean = new ArrayList<BeanNotificacion>();
			for (Objetoi cred : creditos) {
				BeanNotificacion bn = new BeanNotificacion();
				bn.setCredito(cred);
				creditosBean.add(bn);
			}

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("NotificacionList.credito",
					creditos);
			forward = "NotificacionesCreditosMora";
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		if (creditos == null) {
			this.creditos = null;
		} else {
			this.creditos = creditos;
		}
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public List<BeanNotificacion> getCreditosBean() {
		return creditosBean;
	}

	public void setCreditosBean(List<BeanNotificacion> creditosBean) {
		this.creditosBean = creditosBean;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long idObjetoi) {
		this.numeroAtencion = idObjetoi;
	}

	public void setFechaMora(String fechaMora) {
		this.fechaMora = DateHelper.getDate(fechaMora);
	}

	public String getFechaMora() {
		return DateHelper.getString(fechaMora);
	}

	public String[] getIdsLineas() {
		return idsLineas;
	}

	public void setIdsLineas(String[] idsLineas) {
		this.idsLineas = idsLineas;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String getUrlGaf() {
		return DirectorHelper.getString("URL.GAF");
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}

}
