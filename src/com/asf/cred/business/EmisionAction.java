package com.asf.cred.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.AbmAction;
import com.asf.struts.form.AbmForm;
import com.asf.struts.form.ListForm;
import com.asf.util.DoubleHelper;
import com.nirven.creditos.hibernate.Emision;

public class EmisionAction extends AbmAction {

	@SuppressWarnings("unchecked")
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)	throws Exception {
		AbmForm oAForm = null;
		ListForm oLForm = null;

		if (form == null) {
			oLForm = new ListForm();
		} else {
			if (form instanceof AbmForm) {
				oAForm = (AbmForm) form;
				oLForm = new ListForm();
				oLForm.setEntityName(oAForm.getEntityName());
			} else {
				oLForm = (ListForm) form;
			}
		}
		oLForm.setEntityName("Emision");

		BusinessPersistance oBp = SessionHandler.getCurrentSessionHandler()
				.getBusinessPersistance();
		java.util.List oList = new java.util.ArrayList();

		try {
			oList = oBp.getByFilter("FROM Emision e where e.cerrada = false order by e.fechaEmision desc, e.ordenEmision asc");
		} catch (Exception e) {
		}

		oLForm.setResult(oList);

		Double totalImporte = new Double(0);

		for (Emision e : (List<Emision>) oList) {
			totalImporte += e.getImporte() != null ? e.getImporte()	: new Double(0);
		}
		request.setAttribute("ListResult", oLForm);
		request.setAttribute("totalImporte", DoubleHelper.getStringAsCurrency(totalImporte));
		return mapping.findForward("EmisionList");

	}

}
