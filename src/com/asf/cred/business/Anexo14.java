package com.asf.cred.business;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;

public class Anexo14 extends ConversationProcess {

	private Date fechaDesde;
	private Date fechaHasta;
	private List<Anexo14Bean> beans;
	private static String sql;
	private Long idMoneda;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscar() {
		if (fechaDesde == null || fechaHasta == null || idMoneda == null) {
			errors.put("anexo14.obligatorios", "anexo14.obligatorios");
			return false;
		}

		String estados = DirectorHelper.getString("Anexo14Estados");

		if (estados == null) {
			errors.put("anexo14.estados", "anexo14.estados");
			return false;
		}

		Calendar c = Calendar.getInstance();
		c.setTime(fechaHasta);
		c.add(Calendar.DATE, 1);
		fechaHasta = c.getTime(); // condicion es fechaProceso < fechaHasta (hora 0)

		String[] ids = estados.split(",");
		Long[] idsEstados = new Long[ids.length];
		int i = 0;
		for (String id : ids) {
			idsEstados[i++] = new Long(id);
		}

		List<Object[]> result = bp.createSQLQuery(getSql()).setParameter("fechaDesde", fechaDesde)
				.setParameter("fechaHasta", fechaHasta).setParameter("idMoneda", idMoneda)
				.setParameterList("idsEstados", idsEstados).list();

		SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");
		String periodo = format.format(fechaDesde);

		beans = new ArrayList<Anexo14Bean>();
		for (Object[] r : result) {
			Anexo14Bean b = new Anexo14Bean(r, periodo);
			beans.add(b);
		}
		return true;
	}

	public List<Anexo14Bean> getBeans() {
		return beans;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(Anexo14.class.getResourceAsStream("anexo14.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	@Override
	protected String getDefaultForward() {
		return "Anexo14";
	}

	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public String getFechaDesdeStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaDesde);
	}

	public String getFechaHastaStr() {
		return String.format("%1$td/%1$tm/%1$tY", fechaHasta);
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
}
