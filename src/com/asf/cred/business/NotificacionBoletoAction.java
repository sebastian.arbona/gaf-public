package com.asf.cred.business;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.Notificacion;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class NotificacionBoletoAction extends Action {
	private Long idNotificacion;
	private JasperReport reporte;
	private byte[] pdf;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

		URL url = new URL(u.toString() + "/reports/boletoTotalDeuda.jasper");
		reporte = (JasperReport) JRLoader.loadObject(url.openStream());
		idNotificacion = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("idNoti");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Notificacion noti = buscarNotificacion(idNotificacion);
		generarReporte(noti, true);

		response.setHeader("Content-Disposition", "filename=notificacion " + idNotificacion + ".pdf");
		// content type, y length, attachment
		response.setContentType("application/pdf");
		response.setContentLength(pdf.length);
		response.getOutputStream().write(pdf);
		// response.getOutputStream().close();

		return null;
	}

	private Notificacion buscarNotificacion(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return (Notificacion) bp.getById(Notificacion.class, id);
	}

	private void generarReporte(Notificacion noti, boolean add) throws JRException, IOException {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Leyenda leyenda = (Leyenda) bp.createQuery("Select l from Leyenda l where l.nombre =:nombre")
				.setParameter("nombre", "Notificacion de Mora").uniqueResult();

		StringBuilder u = new StringBuilder("http://");

		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerName()).append(":");
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getServerPort());
		u.append(SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());

		HashMap param = new HashMap();
		param.put("LEYENDA", leyenda.getTexto());
		param.put("VENCIMIENTO", DateHelper.getString(noti.getFechaVenc()));
		param.put("OBJETOI_ID", noti.getCredito().getId().toString());
		param.put("ID_PERSONA", noti.getCredito().getPersona_id().toString());
		param.put("MONEDA", noti.getCredito().getLinea().getMoneda().getDenominacion().toString());
		param.put("USUARIO", SessionHandler.getCurrentSessionHandler().getCurrentUser().toString());
		param.put("SCHEMA", SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema());
		param.put("REPORTS_PATH", u.toString());
		param.put("NUMERO_BOLETO", idNotificacion);

		Connection conn = bp.getCurrentSession().connection();

		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, param, conn);
		pdf = JasperExportManager.exportReportToPdf(jasperPrint);
	}

	public Long getIdNotificacion() {
		return idNotificacion;
	}

	public void setIdNotificacion(Long idNotificacion) {
		this.idNotificacion = idNotificacion;
	}

}
