package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;
import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.ReportResult;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;

public class BoletosDePago implements IProcess {

	private BusinessPersistance bp;
	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private Objetoi objetoi;
	private List<Boleto> boletos;
	private List<Cuota> cuotas;
	private ReportResult reporte;
	private Boleto boleto;
	private Long idObjetoi;

	public BoletosDePago() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		forward = "BoletosDePago";
		objetoi = (Objetoi) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("objetoiBoletosDePago");
		if (objetoi == null) {
			objetoi = new Objetoi();
		}
		boletos = new ArrayList<Boleto>();
		boleto = new Boleto();
	}

	@Override
	public boolean doProcess() {
		if (accion != null && accion.equals("imprimirBoleto")) {
			imprimirBoletos();
			if (errores.isEmpty()) {
				forward = "ReporteManual";
			}
			return errores.isEmpty();
		}
		listarBoletos();
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listarBoletos() {
		cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", objetoi.getId()).list();
		List<Boleto> boletosResumen = bp.createQuery(
				"select b from Boleto b where b.tipo = :tipoResumen and b.objetoi.id = :idCredito order by b.id")
				.setParameter("tipoResumen", Boleto.TIPO_BOLETO_RESUMEN).setParameter("idCredito", objetoi.getId())
				.list();
		Query query = bp.createQuery("select c from Ctacte cc join cc.cuota c where cc.idBoletoResumen = :idBoleto");
		for (Boleto boletoResumen : boletosResumen) {
			Cuota cuota = (Cuota) query.setParameter("idBoleto", boletoResumen.getId()).setMaxResults(1).uniqueResult();
			if (cuota != null && cuota.getEmision() != null) {
				boletoResumen.setNumeroCuota(cuota.getNumero());
				boletoResumen.setFechaEmisionCuota(cuota.getEmision().getFechaEmision());
			}
			boletos.add(boletoResumen);
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("objetoiBoletosDePago",
				objetoi);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("BoletosDePago.cuotas",
				cuotas);
	}

	@SuppressWarnings("unchecked")
	public void imprimirBoletos() {
		try {
			String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
			boleto = (Boleto) bp.getById(Boleto.class, boleto.getId());
			objetoi = (Objetoi) bp.getById(Objetoi.class, objetoi.getId());
			idObjetoi = objetoi.getId();
			cuotas = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("BoletosDePago.cuotas");
			Cuota cuota = null;
			if (boleto.getTipo() != null && boleto.getTipo().equals(Boleto.TIPO_BOLETO_RESUMEN)) {
				cuota = (Cuota) bp
						.createQuery(
								"select c from Ctacte cc join cc.cuota c where cc.idBoletoResumen = :idBoletoResumen")
						.setParameter("idBoletoResumen", boleto.getId()).setMaxResults(1).uniqueResult();
			} else {
				for (Cuota c : cuotas) {
					if (boleto.getId().equals(c.getIdBoletoResumen())) {
						cuota = c;
						break;
					}
				}
			}
			if (cuota != null) {
				if (objetoi.getLinea().getMoneda().getDenominacion().equals("Peso Argentino")) {
					try {
						String marcaagua = DirectorHelper.getString("marca.agua");
						reporte = new ReportResult();
						reporte.setExport("PDF");
						reporte.setReportName("boletoResumen.jasper");
						reporte.setParams("OBJETOI_ID=" + objetoi.getId() + ";idPersona=" + getObjetoi().getPersona_id()
								+ ";CUOTA_ID=" + cuota.getId() + ";VENCIMIENTO_CUOTA=" + cuota.getFechaVencimientoStr()
								+ ";NUMERO_CUOTA=" + cuota.getNumero() + ";FECHA_EMISION="
								+ cuota.getEmision().getFechaEmisionStr() + ";RELACION_COTOMADOR_ID="
								+ relacionCotomadorId + ";numeroBoleto="
								+ (boleto.getPeriodoBoleto() + "-" + boleto.getNumeroBoleto()) + ";moneda="
								+ getObjetoi().getLinea().getMoneda().getAbreviatura() + ";idBoletoResumen="
								+ boleto.getId() + ";SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
								+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
								+ ";MARCAAGUA=" + marcaagua);
					} catch (Exception e) {
						e.printStackTrace();
						errores.put("imprimir.informe.error",
								new ActionMessage("imprimir.informe.error", "boletoResumen"));
					}
				} else if (objetoi.getLinea().getMoneda().getDenominacion().equals("Dolar")) {
					try {
						reporte = new ReportResult();
						reporte.setExport("PDF");
						reporte.setReportName("boletoResumenDolares.jasper");
						reporte.setParams("OBJETOI_ID=" + objetoi.getId() + ";idPersona=" + getObjetoi().getPersona_id()
								+ ";CUOTA_ID=" + cuota.getId() + ";VENCIMIENTO_CUOTA=" + cuota.getFechaVencimientoStr()
								+ ";NUMERO_CUOTA=" + cuota.getNumero() + ";FECHA_EMISION="
								+ cuota.getEmision().getFechaEmisionStr() + ";RELACION_COTOMADOR_ID="
								+ relacionCotomadorId + ";numeroBoleto="
								+ (boleto.getPeriodoBoleto() + "-" + boleto.getNumeroBoleto()) + ";moneda="
								+ getObjetoi().getLinea().getMoneda().getAbreviatura() + ";idBoletoResumen="
								+ boleto.getId() + ";SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
								+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
					} catch (Exception e) {
						e.printStackTrace();
						errores.put("imprimir.informe.error",
								new ActionMessage("imprimir.informe.error", "boletoResumenDolares"));
					}
				}
			}
		} catch (Exception e) {
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletoResumen"));
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public List<Boleto> getBoletos() {
		return boletos;
	}

	public void setBoletos(List<Boleto> boletos) {
		this.boletos = boletos;
	}

	public Boleto getBoleto() {
		return boleto;
	}

	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

}
