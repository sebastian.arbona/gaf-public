package com.asf.cred.business;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.AcuerdoPago;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;

public class CreditoProcess implements IProcess {

	protected String action;
	protected Objetoi credito;
	private Estado estadoActual;
	private Long idObjetoi;
	protected HashMap<String, Object> errores;
	protected BusinessPersistance bp;
	private boolean valorCartera;
	protected boolean acuerdoPago;
	protected boolean lineaAcuerdo;

	private String forward = "Credito";
	private Long personaId;
	private boolean lineaCyA;// se pone en true si la linea del credito es
								// "Cosecha y acarreo"
	private boolean lineaE;// se pone en true si la linea del credito es
							// "Emergencia agropuecuaria"
	// Cristian Anton
	private boolean pasoGestionJudicial; // se pone true si paso por GESTION
											// JUDICIAL
	protected Log log = LogFactory.getLog(getClass());
	private boolean hayGastosARecuperar;
	private boolean hayNroCredito;
	private boolean hayProrrogas;
	private String judicial;
	// private boolean auditoriaFinalPos;
	private String auditoriaFinalPos;
	private List<AuditoriaFinal> auditoriaFinal;

	public CreditoProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		credito = new Objetoi();
		credito.setLinea(new Linea());
		estadoActual = new Estado();

	}

	@Override
	public boolean doProcess() {
		try {
			buscarObjetoi();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	protected void buscarObjetoi() throws HibernateException, Exception {
		if (idObjetoi != null && idObjetoi != 0) {

			credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
			personaId = credito.getPersona_id();

			CreditoHandler handler = new CreditoHandler();
			estadoActual = handler.findEstado(credito);
			if (estadoActual != null && estadoActual.getNombreEstado() != null) {
				acuerdoPago = estadoActual.getNombreEstado().equals("ORIGINAL CON ACUERDO");
				String idLineaAcuerdo = DirectorHelper.getString(Linea.LINEA_ACUERDO_PAGO);
				String[] lineas = idLineaAcuerdo.split(",");
				List<String> lineasList = Arrays.asList(lineas);
				lineaAcuerdo = lineasList.contains(credito.getLinea_id().toString());
			}

			if (credito.getLinea().isLineaCosecha()) {
				lineaCyA = true;
			}
			if (credito.getValorCartera() != null && credito.getValorCartera() == true) {
				valorCartera = true;
			}
			if (estadoActual == null) {
				estadoActual = new Estado();
			}

			String idLineaEmergencia = DirectorHandler.getDirector(Linea.LINEA_EMERGENCIA).getValor();
			lineaE = false;
			if (!(idLineaEmergencia == null || idLineaEmergencia.trim().isEmpty())) {
				Linea lEmer = (Linea) bp.getById(Linea.class, new Long(idLineaEmergencia));
				if (lEmer != null) {
					lineaE = (credito.getLinea() == lEmer);
				}
			}

			Number cantidadGastosRecuperar = (Number) bp
					.createSQLQuery("SELECT COUNT(*) FROM DetalleFactura D WHERE D.objetoi_id = :objetoi_id ")
					.setLong("objetoi_id", idObjetoi).uniqueResult();
			hayGastosARecuperar = !(cantidadGastosRecuperar == null || cantidadGastosRecuperar.intValue() == 0);
			hayNroCredito = !(credito.getNumeroCredito() == null || credito.getNumeroCredito().equals(""));
			Number cantidadSolicitudProrrogas = (Number) bp
					.createSQLQuery("SELECT COUNT(*) FROM SolicitudProrroga S WHERE S.objetoi_id = :objetoi_id ")
					.setLong("objetoi_id", idObjetoi).uniqueResult();
			hayProrrogas = !(cantidadSolicitudProrrogas == null || cantidadSolicitudProrrogas.intValue() == 0);
			com.nirven.creditos.hibernate.AcuerdoPago acuerdo = (AcuerdoPago) bp
					.createQuery("select a from AcuerdoPago a where a.creditoAcuerdo.id = :idObjetoi")
					.setParameter("idObjetoi", idObjetoi).uniqueResult();
			if (acuerdo != null) {
				if (acuerdo.getJudicial() != 0) {
					judicial = "SI";
				} else {
					judicial = "NO";
				}
			} else {
				judicial = "";
			}

			auditoriaFinal = bp.createQuery("Select n from AuditoriaFinal n where n.credito.id=:idObjetoi")
					.setParameter("idObjetoi", idObjetoi).list();
			if (auditoriaFinal.isEmpty()) {
				setAuditoriaFinalPos("");
			} else {
				setAuditoriaFinalPos(
						"Aplica fondos: " + auditoriaFinal.get(auditoriaFinal.size() - 1).getAplicaFondo());
			}

			String queryGestionJudicial = "SELECT COUNT(*) FROM Estado e WHERE e.nombreEstado = 'GESTION JUDICIAL' AND e.idEstado in ("
					+ "SELECT estado_idEstado FROM ObjetoiEstado a, Resolucion b WHERE a.objetoi_id = b.proyecto_id AND a.objetoi_id IN ("
					+ "SELECT o.id FROM Objetoi o WHERE o.expediente IN ("
					+ "SELECT x.expediente FROM Objetoi x WHERE x.id = " + idObjetoi + ") ) )";

			Number pasosGestionJudicial = (Number) bp.createSQLQuery(queryGestionJudicial).uniqueResult();
			pasoGestionJudicial = !(pasosGestionJudicial == null || pasosGestionJudicial.intValue() == 0);

		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Objetoi getObjetoi() {
		return credito;
	}

	public void setObjetoi(Objetoi objetoi) {
		this.credito = objetoi;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Estado getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(Estado estadoActual) {
		this.estadoActual = estadoActual;
	}

	public boolean isAcuerdoPago() {
		return acuerdoPago;
	}

	public void setAcuerdoPago(boolean acuerdoPago) {
		this.acuerdoPago = acuerdoPago;
	}

	public boolean isLineaAcuerdo() {
		return lineaAcuerdo;
	}

	public void setLineaAcuerdo(boolean lineaAcuerdo) {
		this.lineaAcuerdo = lineaAcuerdo;
	}

	public Long getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Long personaId) {
		this.personaId = personaId;
	}

	public boolean isLineaCyA() {
		return lineaCyA;
	}

	public void setLineaCyA(boolean lineaCyA) {
		this.lineaCyA = lineaCyA;
	}

	public boolean isValorCartera() {
		return valorCartera;
	}

	public void setValorCartera(boolean valorCartera) {
		this.valorCartera = valorCartera;
	}

	public boolean isLineaE() {
		return lineaE;
	}

	public void setLineaE(boolean lineaE) {
		this.lineaE = lineaE;
	}

	public String getLineaAcuerdoPago() {
		return DirectorHelper.getString("linea.acuerdoPago");
	}

	public boolean isHayGastosARecuperar() {
		return hayGastosARecuperar;
	}

	public void setHayGastosARecuperar(boolean hayGastosARecuperar) {
		this.hayGastosARecuperar = hayGastosARecuperar;
	}

	public boolean isHayNroCredito() {
		return hayNroCredito;
	}

	public void setHayNroCredito(boolean hayNroCredito) {
		this.hayNroCredito = hayNroCredito;
	}

	public boolean isHayProrrogas() {
		return hayProrrogas;
	}

	public void setHayProrrogas(boolean hayProrrogas) {
		this.hayProrrogas = hayProrrogas;
	}

	public String getJudicial() {
		return judicial;
	}

	public void setJudicial(String judicial) {
		this.judicial = judicial;
	}

	public String getAuditoriaFinalPos() {
		return auditoriaFinalPos;
	}

	public void setAuditoriaFinalPos(String auditoriaFinalPos) {
		this.auditoriaFinalPos = auditoriaFinalPos;
	}

	public boolean isPasoGestionJudicial() {
		return this.pasoGestionJudicial;
	}

}
