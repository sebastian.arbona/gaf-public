package com.asf.cred.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.AbstractEstado;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Bonificacion;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.ConvenioBonificacion;
import com.nirven.creditos.hibernate.CtaCteBonif;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.EstadoBonificacion;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CaidaBonificaciones implements IProcess {

	private String forward;
	private String accion;
	private List<InformeBonificacionesBean> bonifs;
	private String texto;
	private boolean baja = false;
	private Date fechaCaida = new Date();

	private Long idConvenio;
	private int diasMoraFiltro = 1;
	private Long idLinea;

	@Override
	public boolean doProcess() {
		if (accion == null || accion.equalsIgnoreCase("listar")) {
			buscarCreditos();
			this.forward = "caidaBonificaciones";
		} else if (accion.equalsIgnoreCase("baja")) {
			darBaja();
			baja = true;
			this.forward = "caidaBonificaciones";
		} else if (accion.equalsIgnoreCase("volver")) {
			accion = "listar";
			doProcess();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private void buscarCreditos() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		bonifs = new ArrayList<InformeBonificacionesBean>();

		String query = "Select distinct ob.id from ObjetoiEstado oe join Objetoi o on oe.objetoi_id = o.id "
				+ "join ObjetoiBonificacion ob on ob.idCredito = o.id join Estado e on oe.estado_idEstado = e.idEstado "
				+ "join Bonificacion b on ob.idBonificacion = b.id left join EstadoBonificacion eb on eb.objetoiBonificacion_id = ob.id "
				+ "where oe.fechaHasta is null and e.nombreEstado not in (:estados) and (eb.id is null or eb.baja = 0) ";

		if (idLinea != null && idLinea != 0L) {
			query += "and o.linea_id = :idLinea ";
		}
		if (idConvenio != null && idConvenio != 0L) {
			query += "and b.convenio_id = :idConvenio ";
		}

		Query sqlQuery = bp.createSQLQuery(query).setParameterList("estados", new String[] {
				AbstractEstado.ESPERANDO_DOCUMENTACION, AbstractEstado.ANALISIS, AbstractEstado.CANCELADO });

		if (idLinea != null && idLinea != 0L) {
			sqlQuery.setParameter("idLinea", idLinea);
		}
		if (idConvenio != null && idConvenio != 0L) {
			sqlQuery.setParameter("idConvenio", idConvenio);
		}

		List<BigDecimal> listaCred = sqlQuery.list();

		for (BigDecimal idBonif : listaCred) {
			ObjetoiBonificacion bonif = (ObjetoiBonificacion) bp.getById(ObjetoiBonificacion.class,
					new Long(idBonif.longValue()));

			Bonificacion bonificacion = bonif.getBonificacion();

			ConvenioBonificacion convenio = bonificacion.getConvenio();

			Objetoi credito = bonif.getObjetoi();

			int diasAtraso = credito.getDiasDeAtraso();
			if ((diasAtraso > convenio.getDiasMora() && diasAtraso > diasMoraFiltro) || diasMoraFiltro == 0) {
				InformeBonificacionesBean bean = new InformeBonificacionesBean();

				bean.setNumeroAtencion(credito.getNumeroAtencion());
				bean.setExpediente(credito.getExpediente());
				bean.setTitular(credito.getPersona().getNomb12());
				bean.setCuit(credito.getPersona().getCuil12Str());
				bean.setLinea(credito.getLinea().getNombre());
				bean.setComportamiento(credito.getComportamientoActual());
				bean.setConvenio(convenio.getNombre());
				bean.setTasaBonificacion(bonificacion.getTasaBonificadaStr());
				bean.setTipoBonificacion(bonificacion.getTipoBonificacionStr());
				bean.setDiasMora(diasAtraso);
				bean.setDiasExcedido(diasAtraso - convenio.getDiasMora());
				bean.setIdObjetoi(credito.getId());
				bean.setIdBonificacion(bonif.getId());

				bonifs.add(bean);
			}
		}

		java.util.Comparator<InformeBonificacionesBean> comp = new java.util.Comparator<InformeBonificacionesBean>() {

			@Override
			public int compare(InformeBonificacionesBean o1, InformeBonificacionesBean o2) {
				if (o1 == null) {
					return 1;
				}
				if (o2 == null) {
					return -1;
				}
				int diferencia = o1.getDiasExcedido() - o2.getDiasExcedido();
				if (diferencia == 0) {
					return 1;
				}
				return diferencia;
			}
		};

		// Collections.sort(bonifs, comp);
	}

	@SuppressWarnings("unchecked")
	private void darBaja() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		HashSet<String> seleccionados = new HashSet<String>();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("credito")) {
				String[] p = param.split("-");
				seleccionados.add(p[1]);
			}
		}
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Date fecha = fechaCaida;
		// Datos para la creaci�n de la nota de debito y la cuenta corriente
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);

		for (String idSelec : seleccionados) {
			Long idObjetoiBonif = new Long(idSelec);
			ObjetoiBonificacion bonif = (ObjetoiBonificacion) bp.getById(ObjetoiBonificacion.class, idObjetoiBonif);

			Objetoi creditoSeleccionado = bonif.getObjetoi();
			creditoSeleccionado.setFechaBajaBonificacion(fecha);

			Notificacion not = new Notificacion();
			not.setObservaciones(texto);
			not.setCredito(creditoSeleccionado);
			not.setFechaCreada(fecha);
			bp.saveOrUpdate(creditoSeleccionado);
			bp.save(not);

			// Datos para la creaci�n de la nota de debito y la cuenta corriente
			List<Cuota> cuotas = bp
					.createSQLQuery("SELECT * FROM Cuota c WHERE c.estado = '1' AND c.credito_id = :credito_id "
							+ "ORDER BY c.numero")
					.addEntity(Cuota.class).setLong("credito_id", creditoSeleccionado.getId()).list();

			// cambiar estado a baja
			EstadoBonificacion estadoBaja = new EstadoBonificacion();
			estadoBaja.setBaja(true);
			estadoBaja.setObjetoiBonificacion(bonif);
			estadoBaja.setFecha(fecha);
			if (!cuotas.isEmpty())
				estadoBaja.setBajaEfectiva(true);
			bp.save(estadoBaja);

			for (Cuota cuota : cuotas) {
				Object sumatoria = bp
						.createSQLQuery("SELECT SUM(B.monto) FROM BonDetalle B WHERE B.cuota_id = :cuota_id "
								+ "AND B.objetoiBonificacion_id = :objetoiBonificacion_id")
						.setLong("cuota_id", cuota.getId()).setLong("objetoiBonificacion_id", bonif.getId())
						.uniqueResult();
				Double importe = sumatoria == null ? 0D : (Double) sumatoria;
				importe = Math.rint(importe * 100D) / 100D;

				if (importe.doubleValue() < 0.01) {
					continue;
				}

				Number numeroNC = (Number) bp
						.createSQLQuery("SELECT b.numeroBoleto FROM Ctacte cc JOIN Boleto b ON cc.boleto_id = b.id "
								+ "WHERE cc.DTYPE = 'CtaCteBonif' AND cc.objetoiBonificacion_id = ? "
								+ "AND cc.cuota_id = ? AND cc.tipomov_id = 1")
						.setLong(0, bonif.getId()).setLong(1, cuota.getId()).setMaxResults(1).uniqueResult();
				if (numeroNC == null) {
					numeroNC = 0L;
				}

				Boleto notaDebito = new Boleto();
				notaDebito.setFechaEmision(cuota.getFechaVencimiento());
				notaDebito.setFechaVencimiento(cuota.getFechaVencimiento());
				notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				notaDebito.setUsuario(usuario);
				notaDebito.setNumeroCuota(cuota.getNumero());
				notaDebito.setObjetoi(creditoSeleccionado);
				notaDebito.setVerificadorBoleto(0L);
				notaDebito.setImporte(importe);
				notaDebito.setPeriodoBoleto((long) periodo);
				notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
				bp.save(notaDebito);
				CtaCteBonif deb = (CtaCteBonif) creditoSeleccionado.crearCtacte(Concepto.CONCEPTO_BONIFICACION, importe,
						fecha, notaDebito, creditoSeleccionado, cuota, Ctacte.TIPO_CONCEPTO_CAIDA_BONIFICACION);
				deb.setBonificacion(bonif);
				deb.setTipomov(tipoDebito);
				deb.setTipoMovimiento("bonificacion");
				deb.setUsuario(usuario);
				deb.getId().setMovimientoCtacte(Numerador.getNext(numeradorCtaCte));
				deb.getId().setItemCtacte(1L);
				deb.setEnteBonificador(bonif.getBonificacion().getEnteBonificador());
				deb.setBonificacion(bonif);
				deb.setBoleto(notaDebito);
				deb.setDetalle(Ctacte.DETALLE_BAJA_BONIFICACION + " - NC " + numeroNC + " - "
						+ bonif.getBonificacion().getTipoBonificacionStr() + " "
						+ bonif.getBonificacion().getTasaBonificadaStr() + " ("
						+ bonif.getBonificacion().getEnteBonificador().getDetaBa() + ")");
				Bolcon bcDeb = creditoSeleccionado.crearBolcon(Concepto.CONCEPTO_BONIFICACION, importe, notaDebito,
						deb.getId(), cuota);
				bcDeb.setTipomov(tipoDebito);
				bp.save(deb);
				bp.save(bcDeb);
			}
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return null;
	}

	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getFechaCaida() {
		return DateHelper.getString(fechaCaida);
	}

	public void setFechaCaida(String fechaCaida) {
		this.fechaCaida = DateHelper.getDate(fechaCaida);
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<InformeBonificacionesBean> getBonifs() {
		return bonifs;
	}

	public void setBonifs(List<InformeBonificacionesBean> bonifs) {
		this.bonifs = bonifs;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public boolean isBaja() {
		return baja;
	}

	public void setBaja(boolean baja) {
		this.baja = baja;
	}

	public Long getIdConvenio() {
		return idConvenio;
	}

	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}

	public int getDiasMoraFiltro() {
		return diasMoraFiltro;
	}

	public void setDiasMoraFiltro(int diasMora) {
		this.diasMoraFiltro = diasMora;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}
}
