package com.asf.cred.business;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.cred.beans.BeanCtaCte;
import com.nirven.creditos.hibernate.BeanLiberacion;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.EstadoLiberacion;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Liberaciones;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.ValorCampoGarantia;

public class InformeLiberacionProcess implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	private Date fechaDesde;
	private List<Objetoi> creditos;
	private List<BeanLiberacion> liberaciones = new ArrayList<BeanLiberacion>();
	private List<ValorCampoGarantia> valores = new ArrayList<ValorCampoGarantia>();
	private boolean lista;
	private Long id;
	private boolean detalle;
	private List<BeanCtaCte> beans;
	private int diasTranscurridos;
	private int diasPeriodo;
	private Double interesCompensatorio = 0.0;
	private Double interesMoratoriosPunitorios = 0.0;
	private Double bonificaciones = 0.0;
	private Double gastos = 0.0;
	private Double capitalDevengado = 0.0;
	private Double totalPagado = 0.0;
	private Double totalDevengado = 0.0;
	private Double totalAdeudado = 0.0;
	private Date fecha = new Date();
	private SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	private ArrayList<ObjetoiDTO> creditosDTO;
	private ObjetoiDTO credito;

	public InformeLiberacionProcess() {
		credito = new ObjetoiDTO();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion == null) {
			forward = "InformeLiberacion";
		} else if (accion.equalsIgnoreCase("listar")) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			try {
				String tipoGarantia = DirectorHelper.getString("tipoGarantia.inmovilizacion");
				List<Object[]> filas = bp.createSQLQuery(
						"SELECT id, Proyecto, titular, expediente, volumentContrato, Producto, LiberadoALaFecha, ultimaLiberacion, deudaActual, CASE cambioProducto WHEN 1 THEN 0 ELSE "
								+ "CASE tipoProducto WHEN '1' THEN deudaActual * aforo / precioVinoTinto WHEN '2' THEN deudaActual * aforo / precioVinoBlanco "
								+ "WHEN '3' THEN deudaActual * aforo / precioMosto WHEN '4' THEN deudaActual * aforo / precioMalbec "
								+ "WHEN '5' THEN deudaActual * aforo / precioOtrosVarietales END END nuevoVolumen, forzarLiberacion, liberacionSolicitada, saldoInmovilizado FROM ( "
								+ "SELECT O.id, O.numeroAtencion Proyecto, P.NOMB_12 titular, O.expediente, O.forzarLiberacion,SUM(inm.volumen) volumentContrato, "
								+ "T.TF_DESCRIPCION Producto, SUM(lib.volumen) LiberadoALaFecha, MAX(lib.fecha) ultimaLiberacion, (SELECT SUM(importe) FROM ( "
								+ "(SELECT SUM(importe) importe FROM Ctacte WHERE objetoi_id = O.id "
								+ "AND asociado_id IN (SELECT id FROM Concepto WHERE concepto_concepto = 'cap') AND tipomov_id = 2) UNION "
								+ "(SELECT SUM(importe)*-1 importe FROM Ctacte WHERE objetoi_id = O.id "
								+ "AND asociado_id IN (SELECT id FROM Concepto WHERE concepto_concepto = 'cap') AND tipomov_id = 1)) T ) deudaActual, "
								+ "libSol.producto tipoProducto, C.aforo, C.precioMosto, C.precioVinoBlanco, C.precioVinoTinto, C.precioMalbec, C.precioOtrosVarietales, "
								+ "SUM(libSol.volumen) AS liberacionSolicitada, (SUM(inm.volumen) - SUM(CASE WHEN lib.volumen IS NOT NULL THEN lib.volumen ELSE 0 END)) AS saldoInmovilizado, "
								+ "libSol.cambioProducto FROM Objetoi O "
								+ "INNER JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id AND OG.baja IS NULL "
								+ "INNER JOIN Garantia G ON OG.garantia_id = G.id AND G.tipo_id in (" + tipoGarantia
								+ ") INNER JOIN PERSONA P ON P.IDPERSONA = O.persona_IDPERSONA "
								+ "INNER JOIN CosechaConfig C ON O.varietales = C.varietales AND (O.fechaSolicitud >= C.fechaInicioPer AND O.fechaSolicitud <= C.fechaFinPer) "
								+ "INNER JOIN Liberaciones libSol ON libSol.objetoi_id = O.id AND libSol.estado = 'SOLICITADA' "
								+ "INNER JOIN TIPIFICADORES T ON T.TF_CATEGORIA = 'Garantia.tipoProducto' AND TF_CODIGO = libSol.producto "
								+ "INNER JOIN Inmovilizaciones inm ON inm.objetoi_id = O.id AND inm.estado = 'REALIZADA' AND inm.producto = libSol.producto "
								+ "LEFT JOIN Liberaciones lib ON lib.objetoi_id = O.id AND lib.estado = 'REALIZADA' "
								// "WHERE O.liberar = 1 " +
								// "AND O.autorizado = 1 " +
								+ "GROUP BY O.id, O.numeroAtencion, O.expediente, O.forzarLiberacion, liberar, P.NOMB_12, TF_DESCRIPCION, libSol.producto, "
								+ "C.aforo, C.precioVinoTinto, C.precioVinoBlanco, C.precioMosto, C.precioMalbec, C.precioOtrosVarietales, C.temporada, libSol.cambioProducto "
								+ "HAVING C.temporada = MAX(C.temporada) AND SUM(libSol.volumen) > 0 "
								+ ")T ORDER BY expediente")
						.list();
				creditosDTO = new ArrayList<ObjetoiDTO>();
				ObjetoiDTO objetoiDTO;
				for (Object[] fila : filas) {
					objetoiDTO = new ObjetoiDTO(((BigDecimal) fila[0]).longValue(), ((BigDecimal) fila[1]).longValue(),
							(fila[2] == null ? null : (String) fila[2]), (fila[3] == null ? null : (String) fila[3]),
							(fila[4] == null ? null : (Double) fila[4]), (fila[5] == null ? null : (String) fila[5]),
							(fila[6] == null ? null : (Double) fila[6]),
							(fila[7] == null ? null : DateHelper.getString(new Date(((Timestamp) fila[7]).getTime()))),
							(fila[8] == null ? null : (Double) fila[8]),
							(((fila[9] == null) || ((fila[10] != null) && (Byte) fila[10] == 1)) ? 0
									: (Double) fila[9]));
					objetoiDTO.setLiberacionSolicitada(fila[11] == null ? 0.0 : ((Number) fila[11]).doubleValue());
					objetoiDTO.setSaldoInmovilizado(fila[12] == null ? 0.0 : ((Number) fila[12]).doubleValue());
					creditosDTO.add(objetoiDTO);
				}
				lista = true;
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			forward = "InformeLiberacion";
		} else if (accion.equalsIgnoreCase("Recibido")) {
			lista = true;
			// Busca los creditos de la linea "Cosecha y acarreo" y que sean pagos
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Objetoi cred = (Objetoi) bp.getById(Objetoi.class, id);
			List<ObjetoiVinedo> vins = bp.createQuery("Select v from ObjetoiVinedo v where v.credito=:cred")
					.setParameter("cred", cred).list();
			boolean libero = false;
			Date fechaLiberacion = new Date();
			bp.begin();
			for (ObjetoiVinedo vin : vins) {
				vin.setFechaUltimaLiberacion(fechaLiberacion);
				bp.update(vin);
				libero = true;
			}

			if (libero) {
				List<Liberaciones> liberaciones = bp
						.createQuery("select l from Liberaciones l where l.estado = :solicitada and l.objetoi.id = :id")
						.setParameter("solicitada", EstadoLiberacion.SOLICITADA).setParameter("id", id).list();
				for (Liberaciones lib : liberaciones) {
					lib.setFechaAceptacion(new Date());
					lib.setEstado(EstadoLiberacion.REALIZADA);
					bp.update(lib);
				}

			}
			cred.setLiberar(false);
			cred.setAutorizado(false);
			cred.setForzarLiberacion(false);
			bp.update(cred);
			bp.commit();
			accion = "listar";
			doProcess();
		} else if (accion.equalsIgnoreCase("detalle")) {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Garantia gar = (Garantia) bp.getById(Garantia.class, id);
			valores = gar.getValores();
			detalle = true;
			lista = true;
			Long lineaId = null;
			fechaDesde = (Date) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("fechaListar");
			try {
				creditos = new ArrayList<Objetoi>(bp.createQuery(
						"select distinct v.credito from ObjetoiVinedo v where v.fechaUltimaLiberacion>=:fecha or v.fechaUltimaLiberacion is NULL")
						.setParameter("fecha", fechaDesde).list());

				Iterator<Objetoi> it = creditos.iterator();
				while (it.hasNext()) {
					Objetoi o = it.next();
					if (revisarLiberacion(o)) {
						CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
						calculoDeuda.calcular();
						BeanCtaCte beanTotal = calculoDeuda.getTotal();
						double deudaTotal = beanTotal.getSaldoCuota();
						o.setTotalDeudaActual(deudaTotal);
					} else {
						it.remove();
					}
				}
				it = creditos.iterator();
				Objetoi credito;
				while (it.hasNext()) {
					credito = it.next();
					if (!credito.getTieneGarantiaFiduciaria()) {
						it.remove();
					}
				}
				// generarLiberaciones(bp);
				forward = "InformeLiberacion";
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean revisarLiberacion(Objetoi o) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (o.getLiberar() != null && o.getLiberar() == true) {
			return true;
		}
		List<Ctacte> ccs = null;

		// buscar ccs del credito posteriores a la fecha de ultima liberacion
		// de concepto CAPITAL
		if (o.getFechaUltimaLib() == null || o.getFechaUltimaLib().isEmpty()) {
			ccs = bp.getNamedQuery("Ctacte.findByConceptoCredito").setParameter("concepto", Concepto.CONCEPTO_CAPITAL)
					.setParameter("idCredito", o.getId()).list();
		} else {
			ccs = bp.getNamedQuery("Ctacte.findByConceptoCreditoDesde")
					.setParameter("concepto", Concepto.CONCEPTO_CAPITAL).setParameter("idCredito", o.getId())
					.setParameter("fecha", DateHelper.getDate(o.getFechaUltimaLib())).list();
		}

		boolean creditos = false;
		for (Ctacte cc : ccs) {
			if (!cc.isDebito()) {
				creditos = true;
				break;
			}
		}
		// si se encontraron creditos de capital
		if (creditos) {
			// buscar la ultima cuota vencida
			Object[] valores = (Object[]) bp.getNamedQuery("Cuota.findByRangoFechas")
					.setParameter("idCredito", o.getId()).setParameter("fechaHasta", new Date()).uniqueResult();
			if (valores != null && valores[1] != null) {
				int ultimoNumeroVencida = ((Number) valores[1]).intValue(); // valores[1]
																			// trae
																			// max(nroCuota)
				Cuota ultimaCuotaVencida = (Cuota) bp.getNamedQuery("Cuota.findByNumero")
						.setParameter("idCredito", o.getId()).setParameter("nroCuota", ultimoNumeroVencida)
						.uniqueResult();
				CreditoHandler ch = new CreditoHandler();
				List<Ctacte> ccsCapital = ch.listarCtaCteConceptos(ultimaCuotaVencida, new Date(),
						Concepto.CONCEPTO_CAPITAL);
				double saldo = ch.calcularSaldo(ccsCapital);
				return (ultimaCuotaVencida.getCapital() == 0 || saldo / ultimaCuotaVencida.getCapital() <= 0.1);
			} else {
				return true;
			}
		}
		return false;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public List<BeanLiberacion> getLiberaciones() {
		return liberaciones;
	}

	public void setLiberaciones(List<BeanLiberacion> liberaciones) {
		this.liberaciones = liberaciones;
	}

	public String getFechaDesdeStr() {
		return DateHelper.getString(fechaDesde);
	}

	public void setFechaDesdeStr(String fecha) {
		fechaDesde = DateHelper.getDate(fecha);

	}

	public List<ValorCampoGarantia> getValores() {
		return valores;
	}

	public void setValores(List<ValorCampoGarantia> valores) {
		this.valores = valores;
	}

	public boolean isLista() {
		return lista;
	}

	public void setLista(boolean lista) {
		this.lista = lista;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDetalle() {
		return detalle;
	}

	public void setDetalle(boolean detalle) {
		this.detalle = detalle;
	}

	public List<BeanCtaCte> getBeans() {
		return beans;
	}

	public void setBeans(List<BeanCtaCte> beans) {
		this.beans = beans;
	}

	public int getDiasTranscurridos() {
		return diasTranscurridos;
	}

	public void setDiasTranscurridos(int diasTranscurridos) {
		this.diasTranscurridos = diasTranscurridos;
	}

	public int getDiasPeriodo() {
		return diasPeriodo;
	}

	public void setDiasPeriodo(int diasPeriodo) {
		this.diasPeriodo = diasPeriodo;
	}

	public Double getInteresCompensatorio() {
		return interesCompensatorio;
	}

	public void setInteresCompensatorio(Double interesCompensatorio) {
		this.interesCompensatorio = interesCompensatorio;
	}

	public Double getInteresMoratoriosPunitorios() {
		return interesMoratoriosPunitorios;
	}

	public void setInteresMoratoriosPunitorios(Double interesMoratoriosPunitorios) {
		this.interesMoratoriosPunitorios = interesMoratoriosPunitorios;
	}

	public Double getBonificaciones() {
		return bonificaciones;
	}

	public void setBonificaciones(Double bonificaciones) {
		this.bonificaciones = bonificaciones;
	}

	public Double getGastos() {
		return gastos;
	}

	public void setGastos(Double gastos) {
		this.gastos = gastos;
	}

	public Double getCapitalDevengado() {
		return capitalDevengado;
	}

	public void setCapitalDevengado(Double capitalDevengado) {
		this.capitalDevengado = capitalDevengado;
	}

	public Double getTotalPagado() {
		return totalPagado;
	}

	public void setTotalPagado(Double totalPagado) {
		this.totalPagado = totalPagado;
	}

	public Double getTotalDevengado() {
		return totalDevengado;
	}

	public void setTotalDevengado(Double totalDevengado) {
		this.totalDevengado = totalDevengado;
	}

	public Double getTotalAdeudado() {
		return totalAdeudado;
	}

	public void setTotalAdeudado(Double totalAdeudado) {
		this.totalAdeudado = totalAdeudado;
	}

	public String getFecha() {
		return formato.format(fecha);
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public ArrayList<ObjetoiDTO> getCreditosDTO() {
		return creditosDTO;
	}

	public void setCreditosDTO(ArrayList<ObjetoiDTO> creditosDTO) {
		this.creditosDTO = creditosDTO;
	}

	public ObjetoiDTO getCredito() {
		return credito;
	}

	public void setCredito(ObjetoiDTO credito) {
		this.credito = credito;
	}
}
