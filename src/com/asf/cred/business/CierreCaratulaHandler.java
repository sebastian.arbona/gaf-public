package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.nirven.creditos.hibernate.BeanCompensatorio;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.CtaCteAjuste;
import com.nirven.creditos.hibernate.CtaCteBonif;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.DeudoresVarios;
import com.nirven.creditos.hibernate.Emision;
import com.nirven.creditos.hibernate.EstadoBonificacion;
import com.nirven.creditos.hibernate.MovIngresosVarios;
import com.nirven.creditos.hibernate.Movpagos;
import com.nirven.creditos.hibernate.MovpagosKey;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CierreCaratulaHandler {
	private Log log = LogFactory.getLog(CierreCaratulaHandler.class);
	private HashMap<String, String> hsErrores;

	private Usuario usuario;
	private long itemCtaCte;
	private int periodo;

	// caratula que se esta cerrando
	private Caratula caratula;
	private Pagos pago;
	private List<Ctacte> ctasCreditos;
	private HashMap<Cuota, List<Ctacte>> cuota_ccs;
	private static CierreCaratulaHandler instance;

	public static CierreCaratulaHandler getInstance() {
		if (instance == null) {
			instance = new CierreCaratulaHandler();
		}
		return instance;
	}

	@SuppressWarnings("unchecked")
	private Ctacte pagarConcepto(Cuota cuota, String tipo, double pagoImporte, Pagos pago) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Ctacte> movimientos = bp.getNamedQuery("Ctacte.findByConceptoCuota").setString("concepto", tipo)
				.setLong("idCuota", cuota.getId()).list();

		// para compensatorio
		if (tipo.equals(Concepto.CONCEPTO_COMPENSATORIO)) {
			// buscar las bonificaciones tambien
			List<Ctacte> bonifs = bp.getNamedQuery("Ctacte.findByConceptoCuota")
					.setString("concepto", Concepto.CONCEPTO_BONIFICACION).setLong("idCuota", cuota.getId()).list();
			movimientos.addAll(bonifs);
		}

		double totalDebitos = 0;
		double totalCreditos = 0;
		for (Ctacte mov : movimientos) {
			if (mov.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
				totalDebitos += mov.getImporte();
			} else if (mov.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_CREDITO)) {
				totalCreditos += mov.getImporte();
			}
		}
		double saldo = totalDebitos - totalCreditos;

		if (saldo > 0) {
			// supongo que se puede pagar todo el saldo
			double saldoAPagar = saldo;
			double dif = saldo - pagoImporte;
			if (dif >= 0.005) {
				// si no se puede pagar todo, ocupar el pago completo
				saldoAPagar = pagoImporte;
			}

			Ctacte cc = new Ctacte();
			Concepto concepto = Concepto.buscarConcepto(tipo);

			int periodo = Calendar.getInstance().get(Calendar.YEAR);

			cc.setAsociado(concepto);
			cc.setFacturado(concepto);
			cc.setImporte(saldoAPagar);
			cc.redondear();
			if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
				cc.setFechaGeneracion(pago.getFechaPago());
			} else {
				cc.setFechaGeneracion(new java.sql.Date(caratula.getFechaCobranza().getTime()));
			}
			cc.setCuota(cuota);
			cc.setFechaVencimiento(cuota.getFechaVencimiento());

			CtacteKey ck = new CtacteKey();
			ck.setPeriodoCtacte(new Long(periodo));
			ck.setObjetoi(cuota.getCredito());
			// TODO: reemplazar con Digito Verificador
			ck.setVerificadorCtacte(0L);
			cc.setId(ck);

			// CreditoHandler.cargarCotizacion(cc);
			cc.setCotizaMov(pago.getCotizacion());
			// cc.setCotizaMov(cc.getId().getObjetoi().getLinea().getMoneda().getCotizacionCompra(cc.getFechaGeneracion()));

			if (caratula.getMetodoCarga() == 3) {
				cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_PAGO_A_CUENTA);
			} else {
				cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_COBRANZA);
			}

			return cc;
		}

		return null;
	}

	private Ctacte crearCtacte(Cuota cuota, String concepto, double pagoImporte, Boleto boleto, Pagos pago) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();

		Ctacte ctacte = pagarConcepto(cuota, concepto, pagoImporte, pago);
		if (ctacte != null) {
			// numerador debe incluir el periodo
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
			// Actualizo las bonificaciones correspondientes a la Cuota
			actualizarBonificaciones(cuota, concepto, pagoImporte);
			ctacte.setTipomov(tipoCredito);
			ctacte.setUsuario(usuario);
			ctacte.getId().setMovimientoCtacte(movimientoCtaCte);
			ctacte.getId().setItemCtacte(itemCtaCte++);
			ctacte.setTipoMovimiento("pago");
			ctacte.setCaratula(caratula);
			ctacte.setBoleto(boleto);
			bp.save(ctacte);
		}
		return ctacte;
	}

	private Ctacte crearCtacteExcedente(Cuota cuota, double pagoImporte) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		Concepto concepto = pagoImporte >= 100 ? Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL)
				: Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS);
		Tipomov tipoMov = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
		Tipomov tipoMovDeb = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);

		Boleto boletoDebExcedente = new Boleto();
		if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
			boletoDebExcedente.setFechaEmision(pago.getFechaPago());
			boletoDebExcedente.setFechaVencimiento(pago.getFechaPago());
		} else {
			boletoDebExcedente.setFechaEmision(new java.sql.Date(caratula.getFechaCobranza().getTime()));
			boletoDebExcedente.setFechaVencimiento(new java.sql.Date(caratula.getFechaCobranza().getTime()));
		}

		boletoDebExcedente.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
		boletoDebExcedente.setUsuario(usuario);
		boletoDebExcedente.setObjetoi(pago.getObjetoi());
		boletoDebExcedente.setVerificadorBoleto(0L);
		boletoDebExcedente.setImporte(pagoImporte);
		boletoDebExcedente.setPeriodoBoleto((long) periodo);
		boletoDebExcedente.setNumeroBoleto(Numerador.getNext(boletoDebExcedente.getNumerador()));
		bp.save(boletoDebExcedente);

		Ctacte ccdb = new Ctacte();
		ccdb.setImporte(pagoImporte);
		ccdb.redondear();
		ccdb.setAsociado(concepto);
		ccdb.setFacturado(concepto);
		ccdb.setTipomov(tipoMovDeb);
		ccdb.setBoleto(boletoDebExcedente);
		ccdb.setTipoMovimiento(pagoImporte >= 100 ? "movManualDeb" : "cuota");
		if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
			ccdb.setFechaGeneracion(pago.getFechaPago());
		} else {
			ccdb.setFechaGeneracion(new java.sql.Date(caratula.getFechaCobranza().getTime()));
		}
		ccdb.setCuota(cuota);
		ccdb.setFechaVencimiento(cuota.getFechaVencimiento());
		ccdb.setUsuario(usuario);
		ccdb.setDetalle(pagoImporte >= 100 ? "Reimputacion" : "Gastos administrativos");

		CtacteKey ckdb = new CtacteKey();
		ckdb.setObjetoi(cuota.getCredito());
		ckdb.setPeriodoCtacte(new Long(periodo));
		ckdb.setMovimientoCtacte(movimientoCtaCte);
		ckdb.setVerificadorCtacte(0L); // TODO: reemplazar con Digito
										// Verificador
		ckdb.setItemCtacte(itemCtaCte++);
		ccdb.setId(ckdb);

		ccdb.setCotizaMov(pago.getCotizacion());

		ccdb.setTipoConcepto(
				pagoImporte >= 100 ? Ctacte.TIPO_CONCEPTO_REIMPUTACION : Ctacte.TIPO_CONCEPTO_GASTOS_ADMINISTRATIVOS);

		bp.save(ccdb);

		Bolcon bolcon = new Bolcon();
		bolcon.setCuota(cuota);
		bolcon.setFacturado(concepto);
		bolcon.setImporte(pagoImporte);
		bolcon.setTipomov(tipoMovDeb);
		BolconKey bck = new BolconKey();
		bck.setBoleto(boletoDebExcedente);
		bck.setPeriodoCtacte(ckdb.getPeriodoCtacte());
		bck.setMovimientoCtacte(ckdb.getMovimientoCtacte());
		bck.setVerificadorCtacte(ckdb.getVerificadorCtacte());
		bck.setItemCtacte(ckdb.getItemCtacte());
		bolcon.setId(bck);

		bp.save(bolcon);

		Ctacte cc = new Ctacte();
		cc.setImporte(pagoImporte);
		cc.redondear();
		cc.setAsociado(concepto);
		cc.setFacturado(concepto);
		cc.setTipomov(tipoMov);
		cc.setTipoMovimiento("pago");
		if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
			cc.setFechaGeneracion(pago.getFechaPago());
		} else {
			cc.setFechaGeneracion(new java.sql.Date(caratula.getFechaCobranza().getTime()));
		}
		cc.setCuota(cuota);
		cc.setFechaVencimiento(cuota.getFechaVencimiento());
		cc.setUsuario(usuario);
		cc.setDetalle("Pago excedente");
		cc.setCaratula(caratula);

		CtacteKey ck = new CtacteKey();
		ck.setObjetoi(cuota.getCredito());
		ck.setPeriodoCtacte(new Long(periodo));
		ck.setMovimientoCtacte(movimientoCtaCte);
		// TODO: reemplazar con Digito Verificador
		ck.setVerificadorCtacte(0L);
		ck.setItemCtacte(itemCtaCte++);
		cc.setId(ck);

		cc.setCotizaMov(pago.getCotizacion());

		cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_COBRANZA);

		bp.save(cc);

		return cc;
	}

	@SuppressWarnings("unchecked")
	private boolean actualizarBonificaciones(Cuota cuota, String tipo, double pagoImporte) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Ctacte> movimientos = bp.getNamedQuery("Ctacte.findByConceptoCuota").setString("concepto", tipo)
				.setLong("idCuota", cuota.getId()).list();
		double totalDebitos = 0;
		double totalCreditos = 0;
		Date fechaBonificada = null;
		for (Ctacte mov : movimientos) {
			if (mov.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_DEBITO)) {
				totalDebitos += mov.getImporte();
			} else if (mov.getTipomov().getAbreviatura().equals(Tipomov.TIPOMOV_CREDITO)) {
				totalCreditos += mov.getImporte();
				if (mov.getFacturado().getConcepto().getAbreviatura().equals("com")) {
					fechaBonificada = mov.getFechaGeneracion();
				}
			}
		}
		double saldo = totalDebitos - totalCreditos;

		if (saldo > 0) {
			if (pagoImporte < saldo) {
				return false;
			} else {
				if (fechaBonificada == null)
					fechaBonificada = new Date();
				List<BonDetalle> bon = bp.createQuery("Select b from BonDetalle b where b.cuota= :cuota")
						.setParameter("cuota", cuota).list();
				for (BonDetalle bonDetalle : bon) {
					bonDetalle.setFechaBonificada(fechaBonificada);
					bonDetalle.setEsEfectiva(true);
					bp.update(bonDetalle);
				}
				bp.commit();
				return true;
			}
		}
		return false;
	}

	private void crearMovPago(Ctacte ctacte, Pagos pago, Boleto boleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		Movpagos movPago = new Movpagos();
		MovpagosKey mpKey = new MovpagosKey();
		mpKey.setCaratula(pago.getId().getCaratula());
		mpKey.setItemCtacte(ctacte.getId().getItemCtacte());
		mpKey.setMovimientoCtacte(ctacte.getId().getMovimientoCtacte());
		mpKey.setPeriodoCtacte(ctacte.getId().getPeriodoCtacte());
		mpKey.setVerificadorCtacte(ctacte.getId().getVerificadorCtacte());
		mpKey.setBoleto(boleto);
		movPago.setId(mpKey);
		movPago.setActualizado(true);
		movPago.setCuota(ctacte.getCuota());
		movPago.setFacturado(ctacte.getFacturado());
		movPago.setOriginal(ctacte.getFacturado());
		movPago.setImporte(ctacte.getImporte());
		movPago.setPagos(pago);
		movPago.setTipomov(ctacte.getTipomov());
		bp.save(movPago);
	}

	/**
	 * Realiza el Cierre de caja de los boletos pagados para una determinada
	 * caratula.
	 * 
	 * @param caratula
	 * @return true si el cierre se completo con exito.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
	public synchronized boolean cerrar(Caratula caratula) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		try {
			this.caratula = caratula;

			hsErrores = new HashMap<String, String>();
			ctasCreditos = new ArrayList<Ctacte>();
			cuota_ccs = new HashMap<Cuota, List<Ctacte>>();
			new ArrayList<Ctacte>();

			bp.begin();

			// tipo movimiento credito
			Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
			// tipo movimiento debito
			Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();

			// usuario loggeado
			usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());

			// numerador debe incluir el periodo
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);

			// contador desde 1
			itemCtaCte = 1;

			// uso como periodo el a�o de generacion = actual
			periodo = Calendar.getInstance().get(Calendar.YEAR);

			Emision emision = null;

			Date fechaCobranza = caratula.getFechaCobranza();
			CreditoHandler ch = new CreditoHandler();

			Query queryFecha = bp.getNamedQuery("Movpagos.findFechasByCredito");

			// procesar ingresos varios sin objetoi
			List<DetalleRecaudacion> detalles = bp.createQuery(
					"select d from DetalleRecaudacion d where d.numeroProyecto > :numeroIngVar and d.caratula.id = :idCaratula and d.recaudacion is null")
					.setParameter("idCaratula", caratula.getId()).setParameter("numeroIngVar", 9900000000L).list();
			for (DetalleRecaudacion d : detalles) {
				long numeroIngVarios = d.getNumeroProyecto() - 9900000000L;
				// se genera una diferencia por redondeo no visible en la tabla de
				// deudoresvarios. round(impporte,2) - importe da 0 en las consultas
				// pero si se usa como filtro el resultado es > 0
				Number idDeudor = (Number) bp.createSQLQuery(
						"select d.id from DeudoresVarios d where d.numero = :numero and abs(round(d.importe,2) - :importe) <= 0.01")
						.setParameter("numero", numeroIngVarios).setParameter("importe", d.getImporte()).uniqueResult();
				if (idDeudor == null) {
					/**
					 * #19306 es posible que se realicen pagos por un importe mayor al boleto
					 * emitido
					 */
					idDeudor = (Number) bp.createSQLQuery("select d.id from DeudoresVarios d where d.numero = :numero ")
							.setParameter("numero", numeroIngVarios).uniqueResult();
					if (idDeudor != null) {
						System.out.println("Para el Ds.Vs " + numeroIngVarios
								+ " se ha detectado un pago que difiere de la emision");
					}
				}
				DeudoresVarios dv = (DeudoresVarios) bp.getById(DeudoresVarios.class, idDeudor.longValue());
				if (dv == null) {
					// no encontro, debe ser un deudor de otro sistema
					// permito que continue para que incremente el total cobrado
				} else {
					MovIngresosVarios cc = new MovIngresosVarios();
					Concepto concepto = Concepto.buscarConcepto(dv.getConcepto());
					cc.setConcepto(concepto);
					cc.setImporte(d.getImporte());
					cc.setSaldo(d.getImporte());
					cc.setFechaGeneracion(d.getFechaAcreditacionValor());
					cc.setCotizacion(d.getMoneda().getCotizacionCompra(cc.getFechaGeneracion()));
					cc.setTipoConcepto(dv.getTipoConcepto());
					cc.setDeudoresVarios(dv);
					bp.save(cc);
					/* Actualiza a impactado los detalleFactura asociados a dv */
					List<DetalleFactura> detFacturas = bp.createSQLQuery(
							"SELECT * FROM DetalleFactura D JOIN Factura F ON D.factura_id = F.id WHERE D.boleto_id = :idBoleto AND D.impactado = 0 AND F.fechaPago IS NOT NULL")
							.addEntity(DetalleFactura.class).setLong("idBoleto", dv.getId()).list();
					for (DetalleFactura df : detFacturas) {
						df.setImpactado(true);
						bp.update(df);
					}
				}

				d.setRecaudacion(d.getTempRecaudacion());
				Recaudacion rec = d.getRecaudacion();
				rec.setSaldo(rec.getSaldo() - d.getImporte());
				bp.update(d);
				bp.update(rec);
			}

			// Consulta los Pagos no actualizados de determinada caratula.
			List<Pagos> pagos = bp.getByFilter("SELECT DISTINCT p FROM Pagos p WHERE p.id.caratula.id = "
					+ caratula.getId() + " AND p.actualizado = false");

			ProgressStatus.iTotal = pagos.size();

			for (Pagos pago : pagos) {
				double pagoImporte = pago.getImporte();
				this.pago = pago;
				Boleto boleto = pago.getId().getBoleto();
				Objetoi credito = boleto.getObjetoi();
				DeudoresVarios dv = (DeudoresVarios) bp
						.createQuery("select dv from DeudoresVarios dv where dv.pagoAsociado = :pago")
						.setParameter("pago", pago).uniqueResult();
				if (dv != null) {
					Cuota cuota;
					List<Cuota> cs = bp.createQuery(
							"select c from Cuota c where c.credito.id = :idCredito and c.fechaVencimiento >= :fecha order by c.numero asc")
							.setParameter("idCredito", credito.getId()).setParameter("fecha", pago.getFechaPago())
							.setMaxResults(1).list();
					if (!cs.isEmpty()) {
						cuota = cs.get(0);
					} else {
						cs = bp.createQuery(
								"select c from Cuota c where c.credito.id = :idCredito order by c.numero desc")
								.setParameter("idCredito", credito.getId()).setMaxResults(1).list();
						if (!cs.isEmpty()) {
							cuota = cs.get(0);
						} else {
							continue;
						}
					}

					Ctacte cc = new Ctacte();
					Concepto concepto = Concepto.buscarConcepto(dv.getConcepto());

					int periodo = Calendar.getInstance().get(Calendar.YEAR);

					cc.setAsociado(concepto);
					cc.setFacturado(concepto);
					cc.setImporte(dv.getImporte());
					cc.redondear();
					if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
						cc.setFechaGeneracion(pago.getFechaPago());
					} else {
						cc.setFechaGeneracion(new java.sql.Date(caratula.getFechaCobranza().getTime()));
					}
					cc.setCuota(cuota);
					cc.setFechaVencimiento(cuota.getFechaVencimiento());

					CtacteKey ck = new CtacteKey();
					ck.setPeriodoCtacte(new Long(periodo));
					ck.setObjetoi(credito);
					// TODO: reemplazar con Digito Verificador
					ck.setVerificadorCtacte(0L);
					cc.setId(ck);

					cc.setCotizaMov(pago.getCotizacion());

					cc.setTipoConcepto(dv.getTipoConcepto());

					long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
					cc.setTipomov(tipoCredito);
					cc.setUsuario(usuario);
					cc.getId().setMovimientoCtacte(movimientoCtaCte);
					cc.getId().setItemCtacte(itemCtaCte++);
					cc.setTipoMovimiento("pago");
					cc.setCaratula(caratula);
					cc.setBoleto(boleto);

					bp.save(cc);

					List<Ctacte> creditosCC = cuota_ccs.get(cuota);
					if (creditosCC == null) {
						creditosCC = new ArrayList<Ctacte>();
						cuota_ccs.put(cuota, creditosCC);
					}
					creditosCC.add(cc);

					Boleto boletoRecibo = crearBoletoRecibo(credito);
					pago.setRecibo(boletoRecibo);

					pago.setActualizado(true);

					bp.update(pago);

					List<DetalleRecaudacion> dets = bp.createQuery(
							"select d from DetalleRecaudacion d where d.caratula = :caratula and d.numeroProyecto = :numeroProyecto and d.recaudacion is null and d.tempRecaudacion is not null")
							.setParameter("caratula", caratula)
							.setParameter("numeroProyecto", new Long("99" + String.format("%08d", dv.getNumero())))
							.list();

					for (DetalleRecaudacion det : dets) {
						det.setRecaudacion(det.getTempRecaudacion());
						Recaudacion rec = det.getRecaudacion();
						rec.setSaldo(rec.getSaldo() - det.getImporte());
						bp.update(det);
						bp.update(rec);
					}

					ProgressStatus.iProgress++;

					continue;
				}

				Date fechaDesembolso = null;
				Date fechaVtoAnterior = null;
				Date fechaVtoAnteriorUltimaPaga = null;
				Date fechaUltimoPago = null;

				if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
					fechaCobranza = pago.getFechaPago();
				}

				if (pago.getObjetoi().getLinea().isLineaCer()) {
					actualizarCER(pago);
				}

				// buscar fecha ultimo pago
				List fechas = queryFecha.setParameter("idCredito", credito.getId()).setMaxResults(1).list();
				if (!fechas.isEmpty()) {
					fechaUltimoPago = (Date) fechas.get(0);
				}

				List<Cuota> cuotasSinCancelar = bp.getNamedQuery("Cuota.findSinCancelar")
						.setLong("idCredito", credito.getId()).list();
				int indice = 0;
				Boleto boletoFactura;
				Bolcon bolconCompensatorio;
				Bolcon bolconGastosCuota;
				Bolcon bolconPunitorio;
				Bolcon bolconMoratorio;
				Bolcon bolconBonificacion;

				double importeBoleto;
				double importeBoletoDebito;
				Ctacte ccCompensatorio;
				Ctacte ccGastosCuota;
				Ctacte ccPunitorio;
				Ctacte ccMoratorio;

				List<CtaCteBonif> ccBonif;
				Concepto conceptoBoleto;

				Ctacte movGastos = null;
				Ctacte movGastosRec = null;
				Ctacte movMultas = null;
				Ctacte movCer = null;
				Ctacte movCompensatorio = null;
				Ctacte movPunitorio = null;
				Ctacte movMoratorio = null;
				Ctacte movCapital = null;

				Cuota ultimaCuotaPaga = null;
				ProgressStatus.iProgress++;
				for (Cuota cuota : cuotasSinCancelar) {
					ultimaCuotaPaga = cuota;
					if (cuota.getNumero() == 1) {
						Desembolso primerDesembolso = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
								.setParameter("idCredito", credito.getId()).setParameter("numero", 1).uniqueResult();
						fechaDesembolso = (primerDesembolso.getFechaReal() != null) ? primerDesembolso.getFechaReal()
								: primerDesembolso.getFecha();
						fechaVtoAnterior = fechaDesembolso;
					} else if (cuota.getNumero() > 1) {
						List<Cuota> cuotasAnteriores = bp.createQuery(
								"select c from Cuota c where c.credito.id = :idCredito and c.numero < :nroCuota order by c.numero desc")
								.setParameter("idCredito", credito.getId()).setParameter("nroCuota", cuota.getNumero())
								.setMaxResults(1).list();
						if (!cuotasAnteriores.isEmpty()) {
							// la anterior
							Cuota cuotaAnterior = cuotasAnteriores.get(0);
							fechaVtoAnterior = cuotaAnterior.getFechaVencimiento();
						}
					}

					Date generarDesde = (fechaUltimoPago == null) ? fechaVtoAnterior : fechaUltimoPago;
					boletoFactura = null;
					bolconCompensatorio = null;
					bolconGastosCuota = null;
					bolconPunitorio = null;
					bolconMoratorio = null;
					bolconBonificacion = null;

					importeBoleto = 0.0;
					importeBoletoDebito = 0.0;
					ccCompensatorio = null;
					ccGastosCuota = null;
					ccPunitorio = null;
					ccMoratorio = null;
					ccBonif = new ArrayList<CtaCteBonif>();

					movGastos = null;
					movGastosRec = null;
					movMultas = null;
					movCer = null;
					movCompensatorio = null;
					movPunitorio = null;
					movMoratorio = null;
					movCapital = null;

					// no esta liquidada, generar compensatorio parcial
					if (cuota.getEmision() == null || (cuota.getEmision().getFacturada() != null
							&& cuota.getEmision().getFacturada() == false)) {

						// el ultimo pago fue anterior al vencimiento o el pago
						// actual esta dentro de este periodo de devengamiento
						if (generarDesde.getTime() <= cuota.getFechaVencimiento().getTime()
								|| fechaCobranza.getTime() > fechaVtoAnterior.getTime()
										&& fechaCobranza.getTime() <= cuota.getFechaVencimiento().getTime()) {

							CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(cuota.getCredito_id(), fechaCobranza);
							// incluye bonificaciones
							calculo.setLiquidacion(true);
							List<BeanCompensatorio> comps = calculo.calcularCompensatorio(cuota, fechaCobranza,
									fechaVtoAnterior);
							double compensatorioParcial = 0, bonificacionAcumulada = 0;
							for (BeanCompensatorio comp : comps) {
								if (!comp.isGenerado()) {
									if (comp.getMonto() >= 0) {
										compensatorioParcial += comp.getMonto();
									} else {
										bonificacionAcumulada += Math.abs(comp.getMonto());
									}
								}
							}

							// debito compensatorio
							compensatorioParcial = DoubleHelper.redondear(compensatorioParcial);
							// tipo movimiento credito
							bonificacionAcumulada = DoubleHelper.redondear(bonificacionAcumulada);

							if (compensatorioParcial > 0) {
								ccCompensatorio = credito.crearCtacte(Concepto.CONCEPTO_COMPENSATORIO,
										compensatorioParcial, null,
										(fechaCobranza.after(cuota.getFechaVencimiento()) ? cuota.getFechaVencimiento()
												: fechaCobranza),
										null, credito, cuota, false, Ctacte.TIPO_CONCEPTO_COBRANZA);
								ccCompensatorio.setTipomov(tipoDebito);
								ccCompensatorio.setUsuario(usuario);
								long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
								ccCompensatorio.getId().setMovimientoCtacte(movimientoCtaCte);
								ccCompensatorio.getId().setItemCtacte(itemCtaCte++);
								bp.save(ccCompensatorio);

								if (bonificacionAcumulada > 0) {
									List<BonDetalle> bonDetalles = calculo.getBonDetalles();
									for (BonDetalle bonDetalle : bonDetalles) {
										ObjetoiBonificacion ob = bonDetalle.getObjetoiBonificacion();
										ObjetoiBonificacion obExistente = (ObjetoiBonificacion) bp
												.getById(ObjetoiBonificacion.class, ob.getId());
										bonDetalle.setObjetoiBonificacion(obExistente);
										bp.save(bonDetalle);
									}

									// agrupar bondetalles por cuota y ente
									double montoBonDetalles = 0;
									List<BonDetalle> bonDetallesAgrupados = new ArrayList<BonDetalle>();
									for (BonDetalle bonDetalle : bonDetalles) {
										// busco bondetalle para el mismo ente y
										// cuota
										BonDetalle agrupado = null;
										for (BonDetalle bd : bonDetallesAgrupados) {
											if (bd.getCuota().getId().equals(bonDetalle.getCuota().getId())
													&& bd.getObjetoiBonificacion().getIdEnte()
															.equals(bonDetalle.getObjetoiBonificacion().getIdEnte())) {
												agrupado = bd;
											}
										}
										if (agrupado == null) { // no encontrado
											agrupado = new BonDetalle();
											agrupado.setCuota(bonDetalle.getCuota());
											agrupado.setObjetoiBonificacion(bonDetalle.getObjetoiBonificacion());
											agrupado.setMonto(0.0);
											bonDetallesAgrupados.add(agrupado);
										}
										agrupado.setMonto(agrupado.getMonto() + bonDetalle.getMonto()); // sumarizo
																										// montos
										montoBonDetalles += bonDetalle.getMonto();
									}

									double diferenciaSumar = 0;

									// diferencia menor o igual a 1 centavo
									if (Math.abs(montoBonDetalles - compensatorioParcial) <= 0.01) {
										diferenciaSumar = compensatorioParcial - montoBonDetalles;
									}

									boolean diferenciaSumada = false;
									// crear nuevos registros de ctacte por
									// bondetalle agrupado
									for (BonDetalle bonDetalle : bonDetallesAgrupados) {
										double monto = bonDetalle.getMonto().doubleValue();
										if (!diferenciaSumada) {
											monto += diferenciaSumar;
											// sumar la diferencia a solo una de
											// las cc generadas
											diferenciaSumada = true;
										}

										CtaCteBonif cc = (CtaCteBonif) credito.crearCtacte(
												Concepto.CONCEPTO_BONIFICACION, monto, null,
												(fechaCobranza.after(cuota.getFechaVencimiento())
														? cuota.getFechaVencimiento()
														: fechaCobranza),
												null, credito, cuota, false, Ctacte.TIPO_CONCEPTO_FACTURACION);
										cc.setBonificacion(bonDetalle.getObjetoiBonificacion());
										cc.setFechaGeneracion(fechaCobranza);
										cc.setFechaVencimiento(fechaCobranza);
										cc.setTipomov(tipoCredito);
										cc.setTipoMovimiento("bonificacion");
										cc.setUsuario(usuario);
										cc.getId().setMovimientoCtacte(movimientoCtaCte);
										cc.getId().setItemCtacte(itemCtaCte++);
										cc.setDetalle(Ctacte.DETALLE_BONIFICACION + " - "
												+ cc.getBonificacion().getBonificacion().getTipoBonificacionStr() + " "
												+ cc.getBonificacion().getBonificacion().getTasaBonificadaStr() + " ("
												+ cc.getBonificacion().getBonificacion().getEnteBonificador()
														.getDetaBa()
												+ ")");
										bp.save(cc);

										ccBonif.add(cc);
										EstadoBonificacion estado = bonDetalle.getObjetoiBonificacion().buscarEstado();
										if (estado != null && estado.isBaja()) {
											// baja: crear debito para anular el
											// credito
											CtaCteBonif deb = (CtaCteBonif) credito.crearCtacte(
													Concepto.CONCEPTO_BONIFICACION, monto, null,
													fechaCobranza.after(cuota.getFechaVencimiento())
															? cuota.getFechaVencimiento()
															: fechaCobranza,
													null, credito, cuota, false,
													Ctacte.TIPO_CONCEPTO_CAIDA_BONIFICACION);
											deb.setBonificacion(bonDetalle.getObjetoiBonificacion());
											deb.setTipomov(tipoDebito);
											deb.setTipoMovimiento("bonificacion");
											deb.setUsuario(usuario);
											deb.getId().setMovimientoCtacte(movimientoCtaCte);
											deb.setFechaGeneracion(fechaCobranza);
											deb.setFechaVencimiento(fechaCobranza);
											deb.getId().setItemCtacte(itemCtaCte++);
											bp.save(deb);
											ccBonif.add(deb);
										}
									}
								}
							}

						}
					}

					// revisar si hay que generar moratorio y punitorio
					// buscar lo ya generado y la ultima fecha de generacion
					List<Ctacte> ctactePunit = ch.listarCtaCteConceptosNoAnulados(cuota, Tipomov.TIPOMOV_DEBITO,
							fechaCobranza, Concepto.CONCEPTO_PUNITORIO);
					Date fechaUltimoDebitoPunitorio = null;
					Date fechaTasaPunitorio = null;
					for (Ctacte cc : ctactePunit) {
						if (cc.isDebito() && !"movManualDeb".equalsIgnoreCase(cc.getTipoMovimiento())
								&& !(cc instanceof CtaCteAjuste)) {
							fechaUltimoDebitoPunitorio = cc.getFechaGeneracion();
							/**
							 * Omite los movimientos que se generan a fin de cada ejercicio para fijar los
							 * punitorios devengados y no pagados o por caducidad de plazo
							 */
							if ("117".equalsIgnoreCase(cc.getTipoConcepto())
									|| "104".equalsIgnoreCase(cc.getTipoConcepto())) {
							} else {
								fechaTasaPunitorio = cc.getFechaGeneracion();
							}
						}
					}
					fechaUltimoDebitoPunitorio = fechaUltimoDebitoPunitorio == null ? cuota.getFechaVencimiento()
							: fechaUltimoDebitoPunitorio;
					fechaTasaPunitorio = fechaTasaPunitorio == null ? cuota.getFechaVencimiento() : fechaTasaPunitorio;

					List<Ctacte> ctacteMorat = ch.listarCtaCteConceptosNoAnulados(cuota, Tipomov.TIPOMOV_DEBITO,
							fechaCobranza, Concepto.CONCEPTO_MORATORIO);
					Date fechaUltimoDebitoMoratorio = null;
					Date fechaTasaMoratorio = null;
					for (Ctacte cc : ctacteMorat) {
						if (cc.isDebito() && !"movManualDeb".equalsIgnoreCase(cc.getTipoMovimiento())
								&& !(cc instanceof CtaCteAjuste)) {
							fechaUltimoDebitoMoratorio = cc.getFechaGeneracion();
							/**
							 * Omite los movimientos que se generan a fin de cada ejercicio para fijar los
							 * moratorios devengados y no pagados o por caducidad de plazo
							 */
							if ("117".equalsIgnoreCase(cc.getTipoConcepto())
									|| "104".equalsIgnoreCase(cc.getTipoConcepto())) {
							} else {
								fechaTasaMoratorio = cc.getFechaGeneracion();
							}
						}
					}
					fechaUltimoDebitoMoratorio = fechaUltimoDebitoMoratorio == null ? cuota.getFechaVencimiento()
							: fechaUltimoDebitoMoratorio;
					fechaTasaMoratorio = fechaTasaMoratorio == null ? cuota.getFechaVencimiento() : fechaTasaMoratorio;

					// si hay atraso, calcular la cantidad de dias
					int diasPunitorio = (int) DateHelper.getDiffDates(fechaUltimoDebitoPunitorio, fechaCobranza, 2);
					int diasMoratorio = (int) DateHelper.getDiffDates(fechaUltimoDebitoMoratorio, fechaCobranza, 2);
					double deuda = 0;

					// solo si hay atraso, para que no de division por cero
					// generar punitorio
					if (diasPunitorio > 0) {
						deuda = ch.calcularDeudaImponible(cuota, fechaCobranza);
						double dPunitorio = 0.0;
						if (deuda > 0.01) {
							double ip = calcularInteresesDeudaVencida(ch, credito, cuota, fechaCobranza,
									Concepto.CONCEPTO_PUNITORIO);
							dPunitorio = ip;
							cuota.setPunitorio(ip);
						}
						if (dPunitorio >= 0.005) {
							dPunitorio = DoubleHelper.redondear(dPunitorio);
							ccPunitorio = crearCte(cuota, Concepto.CONCEPTO_PUNITORIO, dPunitorio, tipoDebito, boleto);
							ccPunitorio.setDetalle("Int. Punitorio");
						}
					}

					// solo si hay atraso, para que no de division por cero
					// generar moratorio
					if (diasMoratorio > 0) {
						if (deuda == 0) {
							deuda = ch.calcularDeudaImponible(cuota, fechaCobranza);
						}
						double dMoratorio = 0.0;
						if (deuda > 0.01) {
							double im = calcularInteresesDeudaVencida(ch, credito, cuota, fechaCobranza,
									Concepto.CONCEPTO_MORATORIO);
							dMoratorio = im;
							cuota.setMoratorio(im);
						}
						if (dMoratorio >= 0.005) {
							dMoratorio = DoubleHelper.redondear(dMoratorio);
							ccMoratorio = crearCte(cuota, Concepto.CONCEPTO_MORATORIO, dMoratorio, tipoDebito, boleto);
							ccMoratorio.setDetalle("Int. Moratorio");
						}
					}

					double importeRedondeado;
					if (ccCompensatorio != null) {
						conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_COMPENSATORIO);
						bolconCompensatorio = new Bolcon();
						bolconCompensatorio.setFacturado(conceptoBoleto);
						bolconCompensatorio.setOriginal(conceptoBoleto);
						importeRedondeado = ccCompensatorio.getImporte();
						importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
						bolconCompensatorio.setImporte(importeRedondeado);
						bolconCompensatorio.setCuota(cuota);
						importeBoleto += bolconCompensatorio.getImporte();
						if (ccBonif != null && !ccBonif.isEmpty()) {
							Long numeroBoletoCredito = null;
							for (CtaCteBonif ccb : ccBonif) {
								if (!ccb.isDebito()) {
									Boleto notaCredito = new Boleto();
									notaCredito.setFechaEmision(ccb.getFechaGeneracion());
									notaCredito.setFechaVencimiento(ccb.getFechaGeneracion());
									notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
									notaCredito.setUsuario(usuario);
									notaCredito.setNumeroCuota(cuota.getNumero());
									notaCredito.setObjetoi(credito);
									notaCredito.setPeriodoBoleto((long) periodo);
									notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
									notaCredito.setVerificadorBoleto(0L);
									conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_BONIFICACION);
									bolconBonificacion = new Bolcon();
									bolconBonificacion.setFacturado(conceptoBoleto);
									bolconBonificacion.setOriginal(conceptoBoleto);
									importeRedondeado = ccb.getImporte();
									importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
									bolconBonificacion.setImporte(importeRedondeado);
									bolconBonificacion.setCuota(cuota);
									notaCredito.setImporte(importeRedondeado);
									bp.save(notaCredito);

									CtacteKey ckb = ccb.getId();
									BolconKey bckb = new BolconKey();
									bckb.setBoleto(notaCredito);
									bckb.setPeriodoCtacte(ckb.getPeriodoCtacte());
									bckb.setMovimientoCtacte(ckb.getMovimientoCtacte());
									bckb.setVerificadorCtacte(ckb.getVerificadorCtacte());
									bckb.setItemCtacte(ckb.getItemCtacte());
									bolconBonificacion.setId(bckb);
									bolconBonificacion.setTipomov(tipoCredito);
									bp.save(bolconBonificacion);
									ccb.setBoleto(notaCredito);
									bp.update(ccb);
								} else {
									Boleto notaDebito = new Boleto();
									notaDebito.setFechaEmision(ccb.getFechaGeneracion());
									notaDebito.setFechaVencimiento(ccb.getFechaGeneracion());
									notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
									notaDebito.setUsuario(usuario);
									notaDebito.setNumeroCuota(cuota.getNumero());
									notaDebito.setObjetoi(credito);
									notaDebito.setPeriodoBoleto((long) periodo);
									notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
									notaDebito.setVerificadorBoleto(0L);
									conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_BONIFICACION);
									bolconBonificacion = new Bolcon();
									bolconBonificacion.setFacturado(conceptoBoleto);
									bolconBonificacion.setOriginal(conceptoBoleto);
									importeRedondeado = ccb.getImporte();
									importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
									bolconBonificacion.setImporte(importeRedondeado);
									bolconBonificacion.setCuota(cuota);
									notaDebito.setImporte(importeRedondeado);
									bp.save(notaDebito);

									CtacteKey ckb = ccb.getId();
									BolconKey bckb = new BolconKey();
									bckb.setBoleto(notaDebito);
									bckb.setPeriodoCtacte(ckb.getPeriodoCtacte());
									bckb.setMovimientoCtacte(ckb.getMovimientoCtacte());
									bckb.setVerificadorCtacte(ckb.getVerificadorCtacte());
									bckb.setItemCtacte(ckb.getItemCtacte());
									bolconBonificacion.setId(bckb);
									bolconBonificacion.setTipomov(tipoDebito);
									bp.save(bolconBonificacion);

									ccb.setBoleto(notaDebito);
									// debito viene justo despues del credito
									// que anula
									// usar el ultimo numeroBoletoCredito para
									// referencia
									ccb.setDetalle(Ctacte.DETALLE_BAJA_BONIFICACION + " - NC " + numeroBoletoCredito
											+ " - " + ccb.getBonificacion().getBonificacion().getTipoBonificacionStr()
											+ " " + ccb.getBonificacion().getBonificacion().getTasaBonificadaStr()
											+ " ("
											+ ccb.getBonificacion().getBonificacion().getEnteBonificador().getDetaBa()
											+ ")");
									bp.update(ccb);
								}
							}
						}
					}
					if (ccGastosCuota != null) {
						conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS);
						bolconGastosCuota = new Bolcon();
						bolconGastosCuota.setFacturado(conceptoBoleto);
						bolconGastosCuota.setOriginal(conceptoBoleto);
						importeRedondeado = ccGastosCuota.getImporte();
						importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
						bolconGastosCuota.setImporte(importeRedondeado);
						bolconGastosCuota.setCuota(cuota);
						importeBoleto += bolconGastosCuota.getImporte();
					}

					if (importeBoleto > 0.0) {
						boletoFactura = new Boleto();
						boletoFactura.setObjetoi(credito);
						boletoFactura.setTipo(Boleto.TIPO_BOLETO_FACTURA);
						boletoFactura.setUsuario(usuario);
						boletoFactura.setPeriodoBoleto(new Long(periodo));
						boletoFactura.setNumeroBoleto(Numerador.getNext(boletoFactura.getNumerador()));
						boletoFactura.setVerificadorBoleto(0L);
						boletoFactura.setImporte(importeBoleto);
						boletoFactura.setFechaEmision(pago.getFechaPago());
						boletoFactura.setFechaVencimiento(cuota.getFechaVencimiento());
						bp.save(boletoFactura);
						if (bolconCompensatorio != null) {
							CtacteKey ck = ccCompensatorio.getId();
							BolconKey bck = new BolconKey();
							bck.setBoleto(boletoFactura);
							bck.setPeriodoCtacte(ck.getPeriodoCtacte());
							bck.setMovimientoCtacte(ck.getMovimientoCtacte());
							bck.setVerificadorCtacte(ck.getVerificadorCtacte());
							bck.setItemCtacte(ck.getItemCtacte());
							bolconCompensatorio.setId(bck);
							bolconCompensatorio.setTipomov(tipoDebito);
							bp.save(bolconCompensatorio);
							ccCompensatorio.setBoleto(boletoFactura);
							bp.update(ccCompensatorio);
						}
						if (bolconGastosCuota != null) {
							CtacteKey ck = ccGastosCuota.getId();
							BolconKey bck = new BolconKey();
							bck.setBoleto(boletoFactura);
							bck.setPeriodoCtacte(ck.getPeriodoCtacte());
							bck.setMovimientoCtacte(ck.getMovimientoCtacte());
							bck.setVerificadorCtacte(ck.getVerificadorCtacte());
							bck.setItemCtacte(ck.getItemCtacte());
							bolconGastosCuota.setId(bck);
							bolconGastosCuota.setTipomov(tipoDebito);
							bp.save(bolconGastosCuota);
							ccGastosCuota.setBoleto(boletoFactura);
							bp.update(ccGastosCuota);
						}
					}

					if (ccPunitorio != null) {
						conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_PUNITORIO);
						bolconPunitorio = new Bolcon();
						bolconPunitorio.setFacturado(conceptoBoleto);
						bolconPunitorio.setOriginal(conceptoBoleto);
						importeRedondeado = ccPunitorio.getImporte();
						importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
						bolconPunitorio.setImporte(importeRedondeado);
						bolconPunitorio.setCuota(cuota);
						importeBoletoDebito += bolconPunitorio.getImporte();
					}
					if (ccMoratorio != null) {
						conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_MORATORIO);
						bolconMoratorio = new Bolcon();
						bolconMoratorio.setFacturado(conceptoBoleto);
						bolconMoratorio.setOriginal(conceptoBoleto);
						importeRedondeado = ccMoratorio.getImporte();
						importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
						bolconMoratorio.setImporte(importeRedondeado);
						bolconMoratorio.setCuota(cuota);
						importeBoletoDebito += bolconMoratorio.getImporte();
					}
					if (importeBoletoDebito > 0.0) {
						Boleto notaDebito = new Boleto();
						if (caratula.getMetodoCarga().intValue() == 0) {
							notaDebito.setFechaEmision(pago.getFechaPago());
							notaDebito.setFechaVencimiento(pago.getFechaPago());
						} else {
							notaDebito.setFechaEmision(caratula.getFechaCobranza());
							notaDebito.setFechaVencimiento(caratula.getFechaCobranza());
						}
						notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
						notaDebito.setUsuario(usuario);
						notaDebito.setNumeroCuota(cuota.getNumero());
						notaDebito.setObjetoi(credito);
						notaDebito.setPeriodoBoleto((long) periodo);
						notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
						notaDebito.setVerificadorBoleto(0L);
						notaDebito.setImporte(importeBoletoDebito);
						bp.save(notaDebito);
						if (bolconPunitorio != null) {
							CtacteKey ck = ccPunitorio.getId();
							BolconKey bck = new BolconKey();
							bck.setBoleto(notaDebito);
							bck.setPeriodoCtacte(ck.getPeriodoCtacte());
							bck.setMovimientoCtacte(ck.getMovimientoCtacte());
							bck.setVerificadorCtacte(ck.getVerificadorCtacte());
							bck.setItemCtacte(ck.getItemCtacte());
							bolconPunitorio.setId(bck);
							bolconPunitorio.setTipomov(tipoDebito);
							bp.save(bolconPunitorio);
							ccPunitorio.setBoleto(notaDebito);
							ccPunitorio.setFechaGeneracion(fechaCobranza);
							ccPunitorio.setUsuario(usuario);
							ccPunitorio.setFechaVencimiento(fechaCobranza);
							bp.update(ccPunitorio);
						}
						if (bolconMoratorio != null) {
							CtacteKey ck = ccMoratorio.getId();
							BolconKey bck = new BolconKey();
							bck.setBoleto(notaDebito);
							bck.setPeriodoCtacte(ck.getPeriodoCtacte());
							bck.setMovimientoCtacte(ck.getMovimientoCtacte());
							bck.setVerificadorCtacte(ck.getVerificadorCtacte());
							bck.setItemCtacte(ck.getItemCtacte());
							bolconMoratorio.setId(bck);
							bolconMoratorio.setTipomov(tipoDebito);
							bp.save(bolconMoratorio);
							ccMoratorio.setBoleto(notaDebito);
							ccMoratorio.setFechaGeneracion(fechaCobranza);
							ccMoratorio.setFechaVencimiento(fechaCobranza);
							ccMoratorio.setUsuario(usuario);
							bp.update(ccMoratorio);
						}
					}

					ctasCreditos = cuota_ccs.get(cuota);
					if (ctasCreditos == null) {
						ctasCreditos = new ArrayList<Ctacte>();
						cuota_ccs.put(cuota, ctasCreditos);
					}
					// esto se hace solo si queda saldo en el pago
					if (pagoImporte >= 0.005) {
						// Gastos a recuperar
						movGastosRec = crearCtacte(cuota, Concepto.CONCEPTO_GASTOS_A_RECUPERAR, pagoImporte, boleto,
								pago);
						if (movGastosRec != null) {

							movGastosRec.setFechaGeneracion(fechaCobranza);
							movGastosRec.setFechaVencimiento(fechaCobranza);
							bp.update(movGastosRec);

							pagoImporte -= movGastosRec.getImporte();
							ctasCreditos.add(movGastosRec);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movGastos = crearCtacte(cuota, Concepto.CONCEPTO_GASTOS, pagoImporte, boleto, pago);
						if (movGastos != null) {

							movGastos.setFechaGeneracion(fechaCobranza);
							movGastos.setFechaVencimiento(fechaCobranza);
							bp.update(movGastos);

							pagoImporte -= movGastos.getImporte();
							ctasCreditos.add(movGastos);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movMultas = crearCtacte(cuota, Concepto.CONCEPTO_MULTAS, pagoImporte, boleto, pago);
						if (movMultas != null) {

							movMultas.setFechaGeneracion(fechaCobranza);
							movMultas.setFechaVencimiento(fechaCobranza);
							bp.update(movMultas);

							pagoImporte -= movMultas.getImporte();
							ctasCreditos.add(movMultas);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movPunitorio = crearCtacte(cuota, Concepto.CONCEPTO_PUNITORIO, pagoImporte, boleto, pago);
						if (movPunitorio != null) {

							movPunitorio.setFechaGeneracion(fechaCobranza);
							movPunitorio.setFechaVencimiento(fechaCobranza);
							bp.update(movPunitorio);

							pagoImporte -= movPunitorio.getImporte();
							ctasCreditos.add(movPunitorio);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movMoratorio = crearCtacte(cuota, Concepto.CONCEPTO_MORATORIO, pagoImporte, boleto, pago);
						if (movMoratorio != null) {

							movMoratorio.setFechaGeneracion(fechaCobranza);
							movMoratorio.setFechaVencimiento(fechaCobranza);
							bp.update(movMoratorio);

							pagoImporte -= movMoratorio.getImporte();
							ctasCreditos.add(movMoratorio);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movCompensatorio = crearCtacte(cuota, Concepto.CONCEPTO_COMPENSATORIO, pagoImporte, boleto,
								pago);
						if (movCompensatorio != null) {

							movCompensatorio.setFechaGeneracion(fechaCobranza);
							movCompensatorio.setFechaVencimiento(fechaCobranza);
							bp.update(movCompensatorio);

							pagoImporte -= movCompensatorio.getImporte();
							ctasCreditos.add(movCompensatorio);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movCer = crearCtacte(cuota, Concepto.CONCEPTO_CER, pagoImporte, boleto, pago);
						if (movCer != null) {

							movCer.setFechaGeneracion(fechaCobranza);
							movCer.setFechaVencimiento(fechaCobranza);
							bp.update(movCer);

							pagoImporte -= movCer.getImporte();
							ctasCreditos.add(movCer);
							if (pagoImporte < 0.005) {
								fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
								fechaVtoAnterior = cuota.getFechaVencimiento();
								indice++;
								break;
							}
						}

						movCapital = crearCtacte(cuota, Concepto.CONCEPTO_CAPITAL, pagoImporte, boleto, pago);
						if (movCapital != null) {

							movCapital.setFechaGeneracion(fechaCobranza);
							movCapital.setFechaVencimiento(fechaCobranza);
							bp.update(movCapital);

							pagoImporte -= movCapital.getImporte();
							ctasCreditos.add(movCapital);
							if (liberar(cuota.getId(), Concepto.CONCEPTO_CAPITAL)) {
								credito.setLiberar(true);
							}
							bp.update(credito);

							bp.getCurrentSession().flush();
						}

						// calcular compensatorio a generar hasta el vencimiento
						CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(cuota.getCredito_id(),
								cuota.getFechaVencimiento());
						calculo.setLiquidacion(true); // incluye bonificaciones
						List<BeanCompensatorio> comps = calculo.calcularCompensatorio(cuota,
								cuota.getFechaVencimiento(), fechaVtoAnterior);
						double compensatorioAGenerar = 0;
						for (BeanCompensatorio comp : comps) {
							if (!comp.isGenerado()) {
								compensatorioAGenerar += comp.getMonto();
							}
						}
						// si no hay mas compensatorio por generar, revisar si
						// ya se pago todo el capital
						if (compensatorioAGenerar < 0.01) {
							List<Ctacte> movimientosCapital = bp.getNamedQuery("Ctacte.findByConceptoCuota")
									.setString("concepto", Concepto.CONCEPTO_CAPITAL).setLong("idCuota", cuota.getId())
									.list();
							double saldo = ch.calcularSaldo(movimientosCapital);
							if (saldo < 0.01) { // alcanzo a pagar todo
								cuota.setEstado(Cuota.CANCELADA); // CUOTA CANCELADA
								bp.update(cuota);
							}
						}

						if (pagoImporte < 0.005) {
							fechaVtoAnteriorUltimaPaga = fechaVtoAnterior;
							fechaVtoAnterior = cuota.getFechaVencimiento();
							indice++;
							break;
						}
					}

					fechaVtoAnterior = cuota.getFechaVencimiento();
					indice++;
				}

				if (ultimaCuotaPaga != null) {
					if (ultimaCuotaPaga.getEstado() == null || ultimaCuotaPaga.getEstado().equals("1")) {
						// calcular compensatorio a generar hasta el vencimiento
						CreditoCalculoDeuda calculo = new CreditoCalculoDeuda(ultimaCuotaPaga.getCredito_id(),
								ultimaCuotaPaga.getFechaVencimiento());
						calculo.setLiquidacion(true); // incluye bonificaciones
						List<BeanCompensatorio> comps = calculo.calcularCompensatorio(ultimaCuotaPaga,
								ultimaCuotaPaga.getFechaVencimiento(), fechaVtoAnteriorUltimaPaga);
						double compensatorioAGenerar = 0;
						for (BeanCompensatorio comp : comps) {
							if (!comp.isGenerado()) {
								compensatorioAGenerar += comp.getMonto();
							}
						}
						// si no hay mas compensatorio por generar, revisar si
						// se pago todo lo ya generado
						if (compensatorioAGenerar < 0.01) {
							List<Ctacte> movimientos = bp.getNamedQuery("Ctacte.findByCuota")
									.setLong("idCuota", ultimaCuotaPaga.getId()).list();
							double saldo = ch.calcularSaldo(movimientos);
							if (saldo < 0.01) { // alcanzo a pagar todo
								// CUOTA CANCELADA
								ultimaCuotaPaga.setEstado(Cuota.CANCELADA);
								bp.update(ultimaCuotaPaga);
							}
						}
					}

					if (pagoImporte >= 0.009) {
						Ctacte ccExcedente = crearCtacteExcedente(ultimaCuotaPaga, pagoImporte);
						ctasCreditos.add(ccExcedente);
					}
				}
				Boleto boletoRecibo = crearBoletoRecibo(credito);
				pago.setRecibo(boletoRecibo);

				pago.setActualizado(true);

				bp.update(pago);

				// si el credito no es en Pesos y el pago es en Pesos,
				// ajustar posible diferencia de cambio
				if (credito.getLinea().getMoneda_id() != null && credito.getLinea().getMoneda_id().longValue() != 1L) {
					if (pago.getMoneda() != null && pago.getMoneda().getId() != null
							&& pago.getMoneda().getId().longValue() == 1L) {
						try {
							// ajuste por diferencia de cambio en la moneda
							ch.realizarAjusteMoneda(pago);
						} catch (Exception ex) {
							// ya tiene ajuste realizado
							ex.printStackTrace();
						}
					}
				}

				List<DetalleRecaudacion> dets = bp
						.createQuery("select d from DetalleRecaudacion d where d.caratula = :caratula "
								+ "and d.numeroProyecto = :numeroProyecto and d.recaudacion is null and d.tempRecaudacion is not null")
						.setParameter("caratula", caratula).setParameter("numeroProyecto", credito.getNumeroAtencion())
						.list();
				for (DetalleRecaudacion det : dets) {
					det.setRecaudacion(det.getTempRecaudacion());
					Recaudacion rec = det.getRecaudacion();
					rec.setSaldo(rec.getSaldo() - det.getImporte());
					bp.update(det);
					bp.update(rec);
				}
			}

			if (emision != null) {
				emision.setCerrada(true);
				emision.setFacturada(true);
				bp.update(emision);
			}

			caratula.setActualizada(1L);
			caratula.setFechaActualizacion(new Date());

			bp.commit();
		} catch (Exception e) {
			if (log.isDebugEnabled()) {
				log.debug("Fallo Cierre de Caja.", e);
			}
			bp.rollback();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private double calcularInteresesDeudaVencida(CreditoHandler ch, Objetoi credito, Cuota cuota, Date fechaCobranza,
			String concepto) {
		double interesDeudaVencida = 0d;
		// revisar si hay que generar moratorio y punitorio
		// buscar lo ya generado y la ultima fecha de generacion
		List<Ctacte> ctacteList = ch.listarCtaCteConceptosNoAnulados(cuota, Tipomov.TIPOMOV_DEBITO, fechaCobranza,
				concepto);
		Date fechaUltimoDebito = null;
		Date fechaTasa = null;
		for (Ctacte ctacte : ctacteList) {
			if (ctacte.isDebito() && !"movManualDeb".equalsIgnoreCase(ctacte.getTipoMovimiento())
					&& !(ctacte instanceof CtaCteAjuste)) {
				fechaUltimoDebito = ctacte.getFechaGeneracion();
				/**
				 * Omite los movimientos que se generan a fin de cada ejercicio para fijar los
				 * punitorios devengados y no pagados o por caducidad de plazo
				 */
				if ("117".equalsIgnoreCase(ctacte.getTipoConcepto())
						|| "104".equalsIgnoreCase(ctacte.getTipoConcepto())) {
				} else {
					fechaTasa = ctacte.getFechaGeneracion();
				}
			}
		}
		fechaUltimoDebito = (fechaUltimoDebito == null) ? cuota.getFechaVencimiento() : fechaUltimoDebito;
		fechaTasa = (fechaTasa == null) ? cuota.getFechaVencimiento() : fechaTasa;
		int diasInteres = (int) DateHelper.getDiffDates(fechaUltimoDebito, fechaCobranza, 2);
		if (diasInteres > 0) {
			double deuda = ch.calcularDeudaImponible(cuota, fechaCobranza);
			if (deuda > 0.01) {
				int diasPeriodo = diasInteres;
				if (concepto.equalsIgnoreCase(Concepto.CONCEPTO_PUNITORIO)) {
					interesDeudaVencida = credito.calcularPunitorioNew(deuda, diasPeriodo, diasInteres, fechaTasa,
							fechaUltimoDebito, fechaCobranza);
				} else if (concepto.equalsIgnoreCase(Concepto.CONCEPTO_MORATORIO)) {
					interesDeudaVencida = credito.calcularMoratorioNew(deuda, diasPeriodo, diasInteres, fechaTasa,
							fechaUltimoDebito, fechaCobranza);
				}
			}
		}
		return DoubleHelper.redondear(interesDeudaVencida);
	}

	private void actualizarCER(Pagos pago) {
		CreditoHandler ch = new CreditoHandler();
		Long idCer = new Long(DirectorHelper.getString("indiceCER.idIndice"));
		ch.actualizacionIndice(pago.getObjetoi().getNumeroAtencion(), idCer, pago.getFechaPago(), true);
	}

	@SuppressWarnings("unchecked")
	private boolean liberar(Long idCuota, String concepto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Ctacte> ctas = bp.getNamedQuery("Ctacte.findByConceptoCuota").setString("concepto", concepto)
				.setLong("idCuota", idCuota).list();
		double debito = 0.0;
		double credito = 0.0;
		for (Ctacte cc : ctas) {
			if (cc.isDebito()) {
				debito += cc.getImporte();
			} else {
				credito += cc.getImporte();
			}
		}
		double porc = debito / credito;
		if (porc >= 0.3) {// porcentaje minimo 30% harcodeado
			return true;
		} else {
			return false;
		}
	}

	private Ctacte crearCte(Cuota cuota, String con, double importe, Tipomov tipo, Boleto boleto) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Ctacte cc = new Ctacte();
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		Concepto concepto = Concepto.buscarConcepto(con);
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		cc.setTipomov(tipo);
		cc.setAsociado(concepto);
		cc.setFacturado(concepto);
		cc.setImporte(importe);
		cc.redondear();
		if (tipo.getAbreviatura().equalsIgnoreCase("DB")) {
			if (con.equalsIgnoreCase(Concepto.CONCEPTO_MORATORIO)) {
				cc.setDetalle("Int. Moratorio");
			}
			if (con.equalsIgnoreCase(Concepto.CONCEPTO_PUNITORIO)) {
				cc.setDetalle("Int. Punitorio");
			}
		}
		if (caratula.getMetodoCarga().intValue() == 0 && pago.getFechaPago() != null) {
			cc.setFechaGeneracion(pago.getFechaPago());
		} else {
			cc.setFechaGeneracion(new java.sql.Date(caratula.getFechaCobranza().getTime()));
		}
		cc.setCuota(cuota);
		cc.setFechaVencimiento(cuota.getFechaVencimiento());
		cc.setTipoMovimiento("pago");
		cc.setCaratula(caratula);

		CtacteKey ck = new CtacteKey();
		ck.setPeriodoCtacte(new Long(periodo));
		ck.setObjetoi(cuota.getCredito());
		ck.setVerificadorCtacte(0L); // TODO: reemplazar con Digito Verificador
		ck.setMovimientoCtacte(movimientoCtaCte);
		ck.setItemCtacte(itemCtaCte++);
		cc.setId(ck);

		CreditoHandler.cargarCotizacion(cc);
		cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_COBRANZA);
		bp.save(cc);

		return cc;
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<String, String> getErrores() {
		return hsErrores;
	}

	private Boleto crearBoletoRecibo(Objetoi credito) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();

		double importeRedondeado;
		Concepto conceptoBoleto;
		double importeBoleto;
		Bolcon bolconCompensatorioRecibo;
		Bolcon bolconPunitorioRecibo;
		Bolcon bolconMoratorioRecibo;
		Bolcon bolconMultas;
		Bolcon bolconGastos;
		Bolcon bolconGastosRec;
		Bolcon bolconCapital;

		Boleto boletoRecibo = new Boleto();
		if (caratula.getMetodoCarga().intValue() == 0) {
			boletoRecibo.setFechaEmision(pago.getFechaPago());
			boletoRecibo.setFechaVencimiento(pago.getFechaPago());
		} else {
			boletoRecibo.setFechaEmision(caratula.getFechaCobranza());
			boletoRecibo.setFechaVencimiento(caratula.getFechaCobranza());
		}
		boletoRecibo.setObjetoi(credito);
		boletoRecibo.setTipo(Boleto.TIPO_BOLETO_RECIBO);
		boletoRecibo.setUsuario(usuario);
		boletoRecibo.setPeriodoBoleto(new Long(periodo));
		boletoRecibo.setVerificadorBoleto(0L);
		bp.save(boletoRecibo);

		importeBoleto = 0.0;
		for (Cuota cuota : cuota_ccs.keySet()) {
			for (Ctacte cc : cuota_ccs.get(cuota)) {
				crearMovPago(cc, pago, boletoRecibo);
				bolconCompensatorioRecibo = null;
				bolconPunitorioRecibo = null;
				bolconMoratorioRecibo = null;
				bolconMultas = null;
				bolconGastos = null;
				bolconGastosRec = null;
				bolconCapital = null;
				Bolcon bolcon = null;
				if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_COMPENSATORIO);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconCompensatorioRecibo = new Bolcon();
					bolconCompensatorioRecibo.setFacturado(conceptoBoleto);
					bolconCompensatorioRecibo.setOriginal(conceptoBoleto);
					bolconCompensatorioRecibo.setImporte(importeRedondeado);
					bolconCompensatorioRecibo.setCuota(cc.getCuota());
					importeBoleto += bolconCompensatorioRecibo.getImporte();
				} else if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_PUNITORIO);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconPunitorioRecibo = new Bolcon();
					bolconPunitorioRecibo.setFacturado(conceptoBoleto);
					bolconPunitorioRecibo.setOriginal(conceptoBoleto);
					bolconPunitorioRecibo.setImporte(importeRedondeado);
					bolconPunitorioRecibo.setCuota(cc.getCuota());
					importeBoleto += bolconPunitorioRecibo.getImporte();
				} else if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_MORATORIO);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconMoratorioRecibo = new Bolcon();
					bolconMoratorioRecibo.setFacturado(conceptoBoleto);
					bolconMoratorioRecibo.setOriginal(conceptoBoleto);
					bolconMoratorioRecibo.setImporte(importeRedondeado);
					bolconMoratorioRecibo.setCuota(cc.getCuota());
					importeBoleto += bolconMoratorioRecibo.getImporte();
				} else if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_GASTOS)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconGastos = new Bolcon();
					bolconGastos.setFacturado(conceptoBoleto);
					bolconGastos.setOriginal(conceptoBoleto);
					bolconGastos.setImporte(importeRedondeado);
					bolconGastos.setCuota(cc.getCuota());
					importeBoleto += bolconGastos.getImporte();
				} else if (cc.getFacturado().getConcepto().getAbreviatura()
						.equals(Concepto.CONCEPTO_GASTOS_A_RECUPERAR)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconGastosRec = new Bolcon();
					bolconGastosRec.setFacturado(conceptoBoleto);
					bolconGastosRec.setOriginal(conceptoBoleto);
					bolconGastosRec.setImporte(importeRedondeado);
					bolconGastosRec.setCuota(cc.getCuota());
					importeBoleto += bolconGastosRec.getImporte();
				} else if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MULTAS)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_MULTAS);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconMultas = new Bolcon();
					bolconMultas.setFacturado(conceptoBoleto);
					bolconMultas.setOriginal(conceptoBoleto);
					bolconMultas.setImporte(importeRedondeado);
					bolconMultas.setCuota(cc.getCuota());
					importeBoleto += bolconMultas.getImporte();
				} else if (cc.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
					conceptoBoleto = Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL);
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolconCapital = new Bolcon();
					bolconCapital.setFacturado(conceptoBoleto);
					bolconCapital.setOriginal(conceptoBoleto);
					bolconCapital.setImporte(importeRedondeado);
					bolconCapital.setCuota(cc.getCuota());
					importeBoleto += bolconCapital.getImporte();
				} else {
					conceptoBoleto = Concepto.buscarConcepto(cc.getFacturado().getConcepto().getAbreviatura());
					importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolcon = new Bolcon();
					bolcon.setFacturado(conceptoBoleto);
					bolcon.setOriginal(conceptoBoleto);
					bolcon.setImporte(importeRedondeado);
					bolcon.setCuota(cc.getCuota());
					importeBoleto += bolcon.getImporte();

					CtacteKey ck = cc.getId();
					BolconKey bk = new BolconKey();
					bk.setBoleto(boletoRecibo);
					bk.setPeriodoCtacte(ck.getPeriodoCtacte());
					bk.setMovimientoCtacte(ck.getMovimientoCtacte());
					bk.setVerificadorCtacte(ck.getVerificadorCtacte());
					bk.setItemCtacte(ck.getItemCtacte());
					bolcon.setId(bk);
					bolcon.setTipomov(tipoCredito);
					bp.save(bolcon);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}

				// Actualizar ctas ctes con los bolcon creados arriba
				if (bolconCompensatorioRecibo != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconCompensatorioRecibo.setId(bckRecibo);
					bolconCompensatorioRecibo.setTipomov(tipoCredito);
					bp.save(bolconCompensatorioRecibo);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
				if (bolconPunitorioRecibo != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconPunitorioRecibo.setId(bckRecibo);
					bolconPunitorioRecibo.setTipomov(tipoCredito);
					bp.save(bolconPunitorioRecibo);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
				if (bolconMoratorioRecibo != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconMoratorioRecibo.setId(bckRecibo);
					bolconMoratorioRecibo.setTipomov(tipoCredito);
					bp.save(bolconMoratorioRecibo);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
				if (bolconGastos != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconGastos.setId(bckRecibo);
					bolconGastos.setTipomov(tipoCredito);
					bp.save(bolconGastos);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
				if (bolconGastosRec != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconGastosRec.setId(bckRecibo);
					bolconGastosRec.setTipomov(tipoCredito);
					bp.save(bolconGastosRec);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
				if (bolconMultas != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconMultas.setId(bckRecibo);
					bolconMultas.setTipomov(tipoCredito);
					bp.save(bolconMultas);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
				if (bolconCapital != null) {
					CtacteKey ck = cc.getId();
					BolconKey bckRecibo = new BolconKey();
					bckRecibo.setBoleto(boletoRecibo);
					bckRecibo.setPeriodoCtacte(ck.getPeriodoCtacte());
					bckRecibo.setMovimientoCtacte(ck.getMovimientoCtacte());
					bckRecibo.setVerificadorCtacte(ck.getVerificadorCtacte());
					bckRecibo.setItemCtacte(ck.getItemCtacte());
					bolconCapital.setId(bckRecibo);
					bolconCapital.setTipomov(tipoCredito);
					bp.save(bolconCapital);
					cc.setBoleto(boletoRecibo);
					bp.update(cc);
				}
			}
		}
		cuota_ccs = new HashMap<Cuota, List<Ctacte>>();
		if (importeBoleto > 0.0) {
			boletoRecibo.setImporte(importeBoleto);
			boletoRecibo.setNumeroBoleto(Numerador.getNext(boletoRecibo.getNumerador()));
			bp.update(boletoRecibo);
			return boletoRecibo;
		} else {
			bp.delete(boletoRecibo);
			return null;
		}
	}

}
