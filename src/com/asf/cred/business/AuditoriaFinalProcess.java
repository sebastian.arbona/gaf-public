package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
//import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
//import com.asf.util.ReportResult;
//import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.AuditoriaFinal;
//import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;

public class AuditoriaFinalProcess implements IProcess {

	private Object result;
	protected HashMap<String, Object> errores = new HashMap<String, Object>();

	private String forward;
	private String accion;
	private Long idCredito;

	private List<AuditoriaFinal> auditoriafin;
	private Objetoi credito;
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion.equalsIgnoreCase("verNotif")) {
			credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
			auditoriafin = bp.createQuery("Select n from AuditoriaFinal n where n.credito=:credito")
					.setParameter("credito", credito).list();
			forward = "AuditoriaFinal";
		}
		return true;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public List<AuditoriaFinal> getAuditoriafin() {
		return auditoriafin;
	}

	public void setAuditoriafin(List<AuditoriaFinal> auditoriafin) {
		this.auditoriafin = auditoriafin;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

}
