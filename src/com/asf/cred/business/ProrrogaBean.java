package com.asf.cred.business;

import java.io.Serializable;

import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.ProrrogaMasiva;

public class ProrrogaBean extends com.nirven.creditos.hibernate.ProrrogaMasiva implements Serializable {

	private static final long serialVersionUID = 5882825052689102857L;
	
	private Cuota cuota;
	
	public ProrrogaBean(ProrrogaMasiva p, Cuota c) {
		setFecha(p.getFecha());
		setObservaciones(p.getObservaciones());
		setResolucion(p.getResolucion());
		setUsuario(p.getUsuario());
		this.cuota = c;
	}
	
	public Cuota getCuota() {
		return cuota;
	}
	
}
