package com.asf.cred.business;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Cobropago;

public class RegistraCobropago {

	private BusinessPersistance bp;
	private Cobropago cobropago;
	private String lisb[];
	private List lstBolepagos = new ArrayList();        //Lista de Bolepagos generados..
	private List<Cobropago> cobroPagos = new ArrayList<Cobropago>();

	public RegistraCobropago() {

	}

	public RegistraCobropago(BusinessPersistance bp) {
		this.bp = bp!=null ? bp : SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	public void registraCobro(String[] lista, StrutsArray cobroPagos){
		registraCobro(lista, cobroPagos, null);
	}
	
	public void registraCobro(String[] lista, StrutsArray cobroPagos, Recaudacion recaudacion){

		Long lote;
		Double importeB = 0D, importeC = 0D, impo = 0D;
		NumberFormat n = NumberFormat.getInstance();
		n.setMaximumFractionDigits(2);
		n.setMinimumFractionDigits(2);
		n.setGroupingUsed(false);

		try{

			/*Genera Cobropago*/
			lote = (Long)bp.getByFilter("SELECT max(c.numeroLote) FROM Cobropago c").iterator().next();
			lote = lote==null ? new Long(1) : (lote + 1);

			Boolean cancela[]= new Boolean[lista.length];

			for (int j = 0; j < cobroPagos.size(); j++) {
				Cobropago c = (Cobropago)cobroPagos.get(j);
				CreaCobropago cPago = new CreaCobropago(bp);
				cobropago = cPago.generaCobropago(c,lote);
				
				cobropago.setRecaudacion(recaudacion);
				
				this.cobroPagos.add(cobropago);
				importeC = cobropago.getImporte(); //importe del cobro..

				for (int i = 0; i < lista.length; i++){
				  if(lista[i]!=null){
					String boletoItem = lista[i].split("-")[0];
					
					int indice = boletoItem.indexOf(' ');
					String idBoleto;
					if(indice != -1)
						idBoleto = boletoItem.substring(0, indice);
					else
						idBoleto = boletoItem;
					impo = new Double(boletoItem.substring(boletoItem.indexOf('$') + 1).replace(",", ".")); //importe del boleto..
					if(	importeB.doubleValue() > impo.doubleValue() || importeC.doubleValue() == 0 || (cancela[i]!=null && cancela[i] == true )){
						continue;
					}

					/*Genera Bolepago*/
					impo = importeB>0 && importeC.doubleValue()!= importeB.doubleValue()? importeB : impo;

					Boleto boleto = (Boleto) bp.getById(Boleto.class, new Long(idBoleto));
					if(importeC >= impo){
						
						this.lstBolepagos.add( cPago.generaBolepago(cobropago,boleto,impo) );
						importeC -= impo;
						importeB = 0D;
						cancela[i] = true;
					}
					else{
						if(importeC.doubleValue()>0) {
							this.lstBolepagos.add( cPago.generaBolepago(cobropago, boleto, importeC) );
						}
						importeB = impo - importeC;
						importeC = 0D;
					}

				}
			  }
			}



		}catch (Exception e) {
			e.printStackTrace();
			bp.rollback();
		}

	}
	
	
	public Long getLote(){
		return cobropago != null ? cobropago.getNumeroLote() : null;
	}
	
	public List<Cobropago> getCobroPagos() {
		return cobroPagos;
	}
}
