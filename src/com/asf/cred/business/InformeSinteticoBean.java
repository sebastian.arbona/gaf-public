package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;

public class InformeSinteticoBean implements Serializable {

	private Date fecha;
	private int cantDesembolsados;
	private double montoDesembolsados;
	private int cantPendiente1;
	private double montoPendiente1;
	private int cantPendiente2;
	private double montoPendiente2;
	private double montoPendienteTotal;
	
	public InformeSinteticoBean() {
	}
	
	public InformeSinteticoBean(Object[] row) {
		fecha = (Date) row[0];
		cantDesembolsados = row[1] != null ? ((Number) row[1]).intValue() : 0;
		montoDesembolsados = row[2] != null ? ((Number) row[2]).doubleValue() : 0;
		cantPendiente1 = row[3] != null ? ((Number) row[3]).intValue() : 0;
		montoPendiente1 = row[4] != null ? ((Number) row[4]).doubleValue() : 0;
		cantPendiente2 = row[5] != null ? ((Number) row[5]).intValue() : 0;
		montoPendiente2 = row[6] != null ? ((Number) row[6]).doubleValue() : 0;
		montoPendienteTotal = row[7] != null ? ((Number) row[7]).doubleValue() : 0;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getCantDesembolsados() {
		return cantDesembolsados;
	}
	public void setCantDesembolsados(int cantDesembolsados) {
		this.cantDesembolsados = cantDesembolsados;
	}
	public double getMontoDesembolsados() {
		return montoDesembolsados;
	}
	public void setMontoDesembolsados(double montoDesembolsados) {
		this.montoDesembolsados = montoDesembolsados;
	}
	public int getCantPendiente1() {
		return cantPendiente1;
	}
	public void setCantPendiente1(int cantPendiente1) {
		this.cantPendiente1 = cantPendiente1;
	}
	public double getMontoPendiente1() {
		return montoPendiente1;
	}
	public void setMontoPendiente1(double montoPendiente1) {
		this.montoPendiente1 = montoPendiente1;
	}
	public int getCantPendiente2() {
		return cantPendiente2;
	}
	public void setCantPendiente2(int cantPendiente2) {
		this.cantPendiente2 = cantPendiente2;
	}
	public double getMontoPendiente2() {
		return montoPendiente2;
	}
	public void setMontoPendiente2(double montoPendiente2) {
		this.montoPendiente2 = montoPendiente2;
	}
	public double getMontoPendienteTotal() {
		return montoPendienteTotal;
	}
	public void setMontoPendienteTotal(double montoPendienteTotal) {
		this.montoPendienteTotal = montoPendienteTotal;
	}

	public void add(InformeSinteticoBean bean) {
		cantDesembolsados += bean.getCantDesembolsados();
		montoDesembolsados += bean.getMontoDesembolsados();
	}
	
}
