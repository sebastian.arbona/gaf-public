package com.asf.cred.business;

import java.util.Date;

import com.civitas.cred.beans.BeanCtaCte;

import it.businesslogic.ireport.IReportScriptlet;

public class ConsultaDeudaScriptlet extends IReportScriptlet {

	public ConsultaDeudaScriptlet() {
	}

	public Object[] getDeuda(Long idObjetoi, Date fecha) {
		Double deuda = new Double(0.0);
		Date fechaVto = null;
		Object[] deudaYFecha = new Object[2];
		try {
			CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(idObjetoi, fecha);
			calculoDeuda.calcular();
			BeanCtaCte deudaTotal = calculoDeuda.getTotal();
			if (deudaTotal != null && deudaTotal.getSaldoCuota() > 0.05) {
				deuda = deudaTotal.getSaldoCuota();
				Date primerVtoImpago = calculoDeuda.getPrimerVtoImpago();
				if (primerVtoImpago != null && (fechaVto == null || fechaVto.after(primerVtoImpago))) {
					fechaVto = primerVtoImpago;
				}
			}
			deudaYFecha[0] = deuda;
			deudaYFecha[1] = fechaVto == null ? "" : fechaVto;
		} catch (Exception e) {
			deudaYFecha[0] = new Double(0.0);
			deudaYFecha[1] = "";
		}
		return deudaYFecha;
	}
}