package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.beansReportes.BeanAuditoriaFinal;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;

public class ReporteAuditoriaFinal implements IProcess {
	protected HashMap<String, Object> errores = new HashMap<String, Object>();
	private String forward = "ReporteAuditoriaFinal";
	private String action;
	BusinessPersistance bp;

	private List<BeanAuditoriaFinal> beanAuditorias;
	private String[] idsLineas;
	private String[] idsEstados;
	private List<Linea> lineas;
	private Date fecha;
	private String aplicaFon;
	private List<Estado> estados;

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {

		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		estados = bp.createQuery("Select e from Estado e where e.tipo=:tipo").setParameter("tipo", "Objetoi").list();
		;
		lineas = bp.getByFilter("select l from Linea l order by l.nombre");
		if (action != null && action.equalsIgnoreCase("listar")) {
			String lineasIds = obtenerIdsLineas();
			String estadosIds = obtenerIdsEstados();
			String fechaSql = DateHelper.getStringFull4(fecha);
			String query = armarQuery(lineasIds, estadosIds, fechaSql);
			cargarListBeanAuditoriaFinal(query);

			/*
			 * List<AuditoriaFinal> auditoriaFinal = bp.getAll(AuditoriaFinal.class);
			 * 
			 * for (AuditoriaFinal auditoria : auditoriaFinal) { for (BeanAuditoriaFinal
			 * bean : beanAuditorias) { if (bean.getNroProyecto()==13203L){
			 * if(auditoria.getCredito().getId().equals(bean.getCredito_id())) {
			 * bean.setAplicaFondos(auditoria.getAplicaFondo()); } } } }
			 */
		}
		return true;
	}

	private void cargarListBeanAuditoriaFinal(String query) {
		beanAuditorias = new ArrayList<BeanAuditoriaFinal>();

		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Object[]> result = bp.createSQLQuery(query).list();
		Object[] filaObject;

		Long credito_id;
		Long nroProyecto;
		String titular;
		String linea;
		String fechaReal;
		String expediente;
		String observaciones = "";
		String nombreestado;
		String fechaproceso = "";
		String aplicafondo = "";

		Long nroDesembolso;
		Long cantDesembolso;
		Long diasTranscurridos;
		beanAuditorias.clear();

		for (int i = 0; i < result.size(); i++) {
			filaObject = (Object[]) result.get(i);

			credito_id = new Long(filaObject[0].toString());
			nroProyecto = new Long(filaObject[1].toString());
			titular = (String) filaObject[2];
			linea = (String) filaObject[3];
			fechaReal = DateHelper.getString((Date) filaObject[4]);
			nroDesembolso = ((Integer) filaObject[5]).longValue();
			cantDesembolso = ((Integer) filaObject[6]).longValue();
			diasTranscurridos = ((Integer) filaObject[7]).longValue();
			expediente = (String) filaObject[8];
			nombreestado = (String) filaObject[9];
			if (filaObject.length == 13) {
				observaciones = (String) filaObject[10];
				fechaproceso = DateHelper.getString((Date) filaObject[11]);
				aplicafondo = (String) filaObject[12];
			}

			BeanAuditoriaFinal raf = new BeanAuditoriaFinal();
			raf.setCredito_id(credito_id);
			raf.setNroProyecto(nroProyecto);
			raf.setTitular(titular);
			raf.setLinea(linea);
			raf.setFechaReal(fechaReal);
			raf.setNroDesembolso(nroDesembolso);
			raf.setCantDesembolso(cantDesembolso);
			raf.setDiasTranscurridos(diasTranscurridos);
			raf.setExpediente(expediente);
			raf.setNombreEstado(nombreestado);
			raf.setObservaciones(observaciones);
			raf.setFechaProceso(fechaproceso);
			raf.setAplicaFondos(aplicafondo);
			/*
			 * List<AuditoriaFinal> auditoriaFinal; auditoriaFinal =
			 * bp.createQuery("Select n from AuditoriaFinal n where n.credito.id=:credito").
			 * setParameter("credito", credito_id).list(); if(auditoriaFinal.isEmpty()){
			 * raf.setAplicaFondos(""); }else{
			 * raf.setAplicaFondos(auditoriaFinal.get(auditoriaFinal.size()-1).
			 * getAplicaFondo()); }
			 */
			beanAuditorias.add(raf);
		}
	}

	private String obtenerIdsLineas() {
		String res = "";
		if (idsLineas != null) {
			for (int i = 0; i < idsLineas.length; i++) {
				res += idsLineas[i] + ",";
			}
			if (res.length() > 0) {
				res = res.substring(0, res.length() - 1);
			}
		}
		return res;
	}

	private String obtenerIdsEstados() {
		String res = "";
		if (idsEstados != null) {
			for (int i = 0; i < idsEstados.length; i++) {
				res += idsEstados[i] + ",";
			}
			if (res.length() > 0) {
				res = res.substring(0, res.length() - 1);
			}
		}
		return res;
	}

	private String armarQuery(String lineasIds, String estadosIds, String fechaSql) {
		String query = "";

		query = "select * from (SELECT oi.id, oi.numeroatencion, p.nomb_12 , l.detalle, t.fechareal, t.numero as numero1,  t.numero as numero2,";
		if (this.bp.getDBMS().equals("MYSQL")) {
			query += " DATEDIFF('" + fechaSql + "' , t.fechareal) as diferencia";
		} else if (bp.getDBMS().equals("SQLSERVER")) {
			query += " DATEDIFF(DAY, t.fechareal, '" + fechaSql + "') as diferencia";
		}
		query += ", oi.expediente,  es.nombreEstado,";
		query += "(select observaciones from AuditoriaFinal where id in (select MAX(id) from AuditoriaFinal where credito_id = oi.id)) as observaciones, ";
		query += "(select fechaProceso  from AuditoriaFinal where id in (select MAX(id) from AuditoriaFinal where credito_id = oi.id)) as fechaProceso, ";
		query += "(select UPPER(aplicaFondo) from AuditoriaFinal where id in (select MAX(id) from AuditoriaFinal where credito_id = oi.id)) as aplicaFondo ";

		query += " FROM objetoi oi " + " INNER JOIN ("
				+ "SELECT credito_id,max(fechareal) AS 'fechareal' ,max(numero) AS 'numero' "
				+ "FROM desembolso WHERE fechareal IS NOT NULL GROUP BY credito_id" + ") t ON t.credito_id = oi.id"
				+ " INNER JOIN persona p ON p.idpersona = oi.persona_idpersona"
				+ " INNER JOIN linea l ON l.id = oi.linea_id" + " INNER JOIN ObjetoiEstado oe ON oe.objetoi_id = oi.id"
				+ "	INNER JOIN Estado es ON es.idEstado = oe.estado_idEstado";

		String where = " WHERE oe.fechaHasta is null ";

		if (lineasIds.length() > 0) {
			where += " AND l.id in (" + lineasIds + ")";
		}

		if (estadosIds.length() > 0) {
			where += " AND es.idEstado in (" + estadosIds + ")";
		}

		query += where + ") a";

		if (aplicaFon.equals("NO") || aplicaFon.equals("SI") || aplicaFon.equals("PROCESO")) {
			query += " where UPPER(a.aplicaFondo) = '" + aplicaFon.toUpperCase() + "'";
		}
		query += " order by a.numeroatencion asc";

		return query;

	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public List<BeanAuditoriaFinal> getBeanAuditorias() {
		return beanAuditorias;
	}

	public void setBeanAuditorias(List<BeanAuditoriaFinal> beanAuditorias) {
		this.beanAuditorias = beanAuditorias;
	}

	public String[] getIdsLineas() {
		return idsLineas;
	}

	public void setIdsLineas(String[] idsLineas) {
		this.idsLineas = idsLineas;
	}

	public String getFecha() {
		return DateHelper.getString(fecha);
	}

	public void setFecha(String fecha) {
		this.fecha = DateHelper.getDate(fecha);
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String getAplicaFon() {
		return aplicaFon;
	}

	public void setAplicaFon(String aplicaFon) {
		this.aplicaFon = aplicaFon;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public String[] getIdsEstados() {
		return idsEstados;
	}

	public void setIdsEstados(String[] idsEstados) {
		this.idsEstados = idsEstados;
	}
}
