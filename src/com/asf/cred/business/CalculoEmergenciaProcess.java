package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.DirectorHandler;
import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.CalculoEmergencia;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.IndiceValor;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.Tipomov;

public class CalculoEmergenciaProcess implements IProcess {

	private BusinessPersistance bp;
	private HashMap<String, String> errores;
	private String forward = "calculoEmergencia";
	private Long idPersona;
	private Date pFecha;
	private String accion;
	private String nombre;
	private CalculoEmergencia rd;
	private Long idRd;
	private List<Persona> personas;
	private ReportResult reportResult;

	public CalculoEmergenciaProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, String>();
		idPersona = 0L;
		pFecha = new Date();
		nombre = "";
		rd = new CalculoEmergencia();
		personas = new ArrayList<Persona>();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			forward = "calculoEmergencia";
		} else if (accion.equals("calcular")) {
			try {
				calcularDeuda(idPersona, pFecha);
				setIdRd(rd.getId());
				if (errores.isEmpty()) {
					forward = "emergencia";
				} else {
					listar();
					forward = "calculoEmergencia";
				}
			} catch (Exception e) {
				e.printStackTrace();
				errores.put(e.getMessage(), e.getMessage());
				forward = "calculoEmergencia";
			}
		} else if (accion.equals("listar")) {
			listar();
			forward = "calculoEmergencia";
		} else if (accion.equals("imprimir")) {
			imprimir();
		}

		return errores.isEmpty();
	}

	private void imprimir() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.rd = (CalculoEmergencia) bp.getById(CalculoEmergencia.class, idRd);
		reportResult = new ReportResult();
		reportResult.setParams("persona=" + rd.getPersona().getNomb12() + ";codigo=" + rd.getPersona().getId()
				+ ";capitalOrig=" + rd.getCapitalOriginal() + ";capitalNeto=" + rd.getCapitalNeto()
				+ ";intCompensatorio=" + rd.getCompensatorio() + ";tasa=" + rd.getPorce1() + ";intPunitorio="
				+ rd.getMoratorio() + ";cer=" + rd.getCer() + ";fecha=" + rd.getFechaStr() + ";REPORTS_PATH="
				+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		reportResult.setReportName("EmergenciaAgro.jasper");
		reportResult.setExport("PDF");
		this.forward = "Reportes";
	}

	@SuppressWarnings("unchecked")
	private void listar() {
		if (nombre == null) {
			personas = new ArrayList<Persona>();
		} else {
			Director director = null;
			try {
				director = DirectorHandler.getDirector(Linea.LINEA_EMERGENCIA);
			} catch (Exception e) {
			}
			Long id = new Long(director.getValor());
			personas = new ArrayList<Persona>();
			personas = bp.createQuery(
					"SELECT DISTINCT o.persona FROM Objetoi o WHERE o.persona.nomb12 LIKE :nombre AND o.linea.id = :linea")
					.setParameter("nombre", nombre + "%").setParameter("linea", id).list();
		}
	}

	/*
	 * Elimina datos de calculos que no posean acuerdo de pago.
	 */
	@SuppressWarnings("unchecked")
	public void eliminarDatosRefinanciacion(Long idPersona) throws Exception {
		bp.begin();

		try {
			List<CalculoEmergencia> calculos = bp
					.createQuery("SELECT c FROM CalculoEmergencia c WHERE c.persona.id = :id AND c.acuerdo IS NULL")
					.setParameter("id", idPersona).list();

			for (CalculoEmergencia calculo : calculos) {
				calculo.setFechaEliminacion(new Date());
			}

			bp.commit();
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * Se calcula el total de la deuda de la persona en creditos de emergencia.
	 */
	@SuppressWarnings("unchecked")
	public double calcularImporteCuentaCorriente(List<Objetoi> creditos) throws Exception {
		try {
			Double deuda = 0.0;
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
			Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
			String consulta = "SELECT cta FROM Ctacte cta WHERE cta.cuota.credito.id = :credito AND cta.tipomov = :tipoMov";
			for (Objetoi credito : creditos) {
				List<Ctacte> ctesDebito = new ArrayList<Ctacte>();
				List<Ctacte> ctes = bp.createQuery(consulta).setParameter("credito", credito.getId())
						.setParameter("tipoMov", tipoDebito).list();
				ctesDebito.addAll(ctes);
				List<Ctacte> ctesCreditos = new ArrayList<Ctacte>();
				ctes = bp.createQuery(consulta).setParameter("credito", credito.getId())
						.setParameter("tipoMov", tipoCredito).list();
				ctesCreditos.addAll(ctes);
				Double deudaCredito = 0D;
				for (Ctacte ctacte : ctesDebito) {
					if (ctacte.getAsociado().getConcepto().getAbreviatura().equals("cap")) {
						deudaCredito += Math.abs(ctacte.getImporte());
					}
				}
				for (Ctacte ctacte : ctesCreditos) {
					if (ctacte.getAsociado().getConcepto().getAbreviatura().equals("cap")) {
						deudaCredito -= Math.abs(ctacte.getImporte());
					}
				}
				deuda += deudaCredito;
			}
			return deuda;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * Genera un nuevo calculo para una persona determinada.
	 */
	public CalculoEmergencia crearCalculo(Long idPersona, Double capitalOriginal) throws Exception {
		try {
			Persona persona = (Persona) bp.getById(Persona.class, idPersona);

			CalculoEmergencia refinancia = new CalculoEmergencia();
			refinancia.setPersona(persona);
			refinancia.setCapitalOriginal(capitalOriginal);

			return refinancia;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public long diferenciaDias(Date mayor, Date menor) {
		long cal = (mayor.getTime() - menor.getTime()) / 1000 / 60 / 60 / 24;
		return cal;
	}

	/*
	 * Calcula la deuda de una perona en creditos de emergencia a una fecha
	 * determinada.
	 */
	public void calcularDeuda(Long idPersona, Date pFecha) throws Exception {
		int vDias = 0;
		double nuevoCapital = 0.0;
		double v1 = 0;
		@SuppressWarnings("unused")
		double v2 = 0;
		double v3 = 0;
		double v6 = 0;

		// Paso 23: Banco == 1
		Double v5 = 0D;
		// Paso 25: Calculo final
		double pTotal = 0;

		try {
			// Paso 1: Eliminar datos de refinanciacion
			eliminarDatosRefinanciacion(idPersona);

			// Paso 4: Busco creditos de la persona
			List<Objetoi> creditos = buscarCreditos(idPersona);

			// Paso 2: Eliminar calcular el capital original
			Double capitalOriginal = calcularImporteCuentaCorriente(creditos);

			// Paso 3: Generar objeto de refinanciacion
			rd = crearCalculo(idPersona, capitalOriginal);
			if (creditos != null && !creditos.isEmpty()) {
				rd.setPersona(creditos.get(0).getPersona());
				rd.setFecha(new Date());
			}
			double nuevoCapital1 = 0;

			String codigo = null;

			for (Objetoi credito : creditos) {

				codigo = credito.getNumeroCredito().substring(3, 4);

				// Paso 5: Calcular Coeficiente Libor
				// Double coeficiente = calcularCoeficienteLibor();

				// Paso 6: Calcular Coeficiente Renta
				// Double renta = calcularRenta();

				// Paso 7: Inicializar Variables de Calculo
				// Double totalCuenta = 0.0;
				Double interesCompensatorio = 0.0;
				// Double interesDecreto = 0.0;
				Double interesPunitorio = 0.0;
				// Double totalCuentaCapital = 0.0;
				Double pri_cuenta = 0.0;

				// Paso 8: Calcular Cantidad de Creditos de Emergencia
				Integer cantidadCreditos = calcularCantidadCreditos(idPersona);

				// Paso 9: Calcular Descuentos x Emergencia el maximo son 10.000
				Double dtoEmergencia = 3400.00 * cantidadCreditos;
				if (dtoEmergencia > 10000) {
					dtoEmergencia = 10000.00;
				}

				// Paso 10: Genero el capital neto de emergencia original -
				// descuento
				rd.setCapitalNeto(rd.getCapitalOriginal() - dtoEmergencia);

				// Paso 11: Calculo el total de desembolsos
				Double desembolsosTotal = calcularTotalDesembolsos(creditos);

				// Paso 12: Calculo los desembolsos para ese credito
				Double desembolsosCredito = calcularDesembolsosCredito(credito);

				// Paso 13: Para cada desembolsos del credito
				List<Desembolso> desembolsos = buscarDesembolsos(credito.getId());

				Date fecha1 = ((Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", credito.getId())
						.setParameter("nroCuota", 1).uniqueResult()).getFechaVencimiento();

				ObjetoiIndice oi = (ObjetoiIndice) bp
						.createQuery(
								"SELECT o FROM ObjetoiIndice o WHERE o.credito.id = :idcredito AND tipoTasa = :tipo")
						.setParameter("idcredito", credito.getId()).setParameter("tipo", "1").list().get(0);

				Double ctasa = oi.getIndice().getValorInicial();

				for (Desembolso desembolso : desembolsos) {

					// Paso 14: Porcentaje de Desembolsos
					Double porcentajeDesembolsos = (desembolsosCredito * 1.0000) / (desembolsosTotal * 1.0000);
					if (porcentajeDesembolsos == 1) {
						porcentajeDesembolsos = 1.000;
					}

					// Paso 15: Calculo de variables
					v1 = diferenciaDias(fecha1, desembolso.getFecha()) * ctasa * 0.5 / 365.0;
					vDias += v1;
					v2 = diferenciaDias(fecha1, desembolso.getFecha());

					double importeDesembolso = desembolso.getImporte() - (dtoEmergencia * porcentajeDesembolsos);

					interesCompensatorio += (v1 * importeDesembolso);

					nuevoCapital = importeDesembolso + (v1 * importeDesembolso);
					nuevoCapital1 += nuevoCapital;

					if (codigo.equals("2")) {

						// Paso 16: Calculo banco == (2)

						v1 = diferenciaDias(pFecha, fecha1) * (ctasa * 0.5 * 3) / 365.0;
						interesPunitorio += (nuevoCapital * v1);
						nuevoCapital1 += (nuevoCapital * v1);
					} else {

						// Paso 17: Calculo banco <> (2)
						double tasa = calcularTasaBancoMendoza(fecha1);
						double tasa10 = 479371.446 / tasa;

						// Paso 18: Calculo de intereses
						v1 = tasa10;
						v6 = (nuevoCapital * v1) - nuevoCapital;

						interesPunitorio += v6;
						nuevoCapital1 += v6;

						// Paso 19: Busqueda de coeficientes de banco prevision
						tasa = calcularTasaBancoPrevision(fecha1);
						tasa10 = 164213.2907 / tasa;

						// Paso 20: Calculo de interes punitorio y actualizacion de
						// capital
						v1 = tasa10;
						interesPunitorio += (((nuevoCapital + v6) * v1) - (nuevoCapital + v6));
						nuevoCapital1 += (((nuevoCapital + v6) * v1) - (nuevoCapital + v6));

					}
				}

				// Paso 21: Actualizacion de refinanciacion
				rd.setCapitalCuenta(rd.getCapitalCuenta() == null ? pri_cuenta : rd.getCapitalCuenta() + pri_cuenta);
				rd.setCompensatorio(rd.getCompensatorio() == null ? interesCompensatorio
						: rd.getCompensatorio() + interesCompensatorio);
				rd.setMoratorio(rd.getMoratorio() == null ? interesPunitorio : rd.getMoratorio() + interesPunitorio);
				rd.setPorce1(nuevoCapital);
				rd.setPorce2(nuevoCapital1);

				// rd.setNuevoCapital(nuevoCapital);
			}

			// Paso 22: Actualizacion de nuevo capital
			nuevoCapital = rd.getCapitalNeto() + rd.getCompensatorio();

			v5 = null;
			Double tasa = 0.0;
			Double tasaLibro = 0.0;
			if (codigo.equals("1")) {
				// Paso 23: Calculo Indice Renta
				tasa = calcularIndiceRenta(pFecha);
				if (tasa != null) {
					tasa /= 3.0821;
					v5 = (nuevoCapital * tasa) - nuevoCapital;
					v5 *= 1.5;
				}
				if (v5 == null) {
					v5 = 0.0;
				}
				nuevoCapital1 += v5;
			}

			// Paso 24: Calcula CER
			tasaLibro = calcularCER(pFecha);
			if (tasaLibro == null) {
				tasaLibro = 0.0;
			}

			v3 = nuevoCapital1 * tasaLibro;
			rd.setCer(v3);

			rd.setMoratorio(rd.getMoratorio() + v5);
			rd.setPorce2(nuevoCapital1);

			pTotal = rd.getCapitalCuenta() + rd.getCompensatorio() + rd.getMoratorio();
			if (pTotal < 0) {
				pTotal = 0;
				// rd.setCapitalCuenta(0.0);
				rd.setCompensatorio(0.0);
				rd.setMoratorio(0.0);
				rd.setCapitalCuenta(0.0);
				rd.setCer(0.0);
			}
			bp.begin();
			bp.saveOrUpdate(rd);
			bp.commit();
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
		}

	}

	public CalculoEmergencia getRd() {

		return rd;
	}

	public void setRd(CalculoEmergencia rd) {
		this.rd = rd;
	}

	/*
	 * Busca la tasa maxima anterior a una fecha. La tasa maxima la determina a
	 * partir de la tabla director.
	 */
	public Double buscarTasaMaximaAnterior(String director, Date fecha) throws Exception {
		Indice indice = buscarTasa(director);
		if (indice != null) {
			String consulta = "SELECT MAX(i.fecha) FROM IndiceValor i WHERE i.indice = :indice AND i.fecha <= :fecha";
			if (director.equals("emergencia.tasa.DRG")) {
				consulta = "SELECT MAX(i.fecha) FROM IndiceValor i WHERE i.indice = :indice AND i.fecha = :fecha";
			}
			Date fechaMaxima = (Date) bp.createQuery(consulta).setParameter("indice", indice)
					.setParameter("fecha", fecha).uniqueResult();

			IndiceValor iv = (IndiceValor) bp
					.createQuery("SELECT i FROM IndiceValor i WHERE i.indice = :indice AND i.fecha = :fecha")
					.setParameter("indice", indice).setParameter("fecha", fechaMaxima).uniqueResult();

			if (iv != null) {
				return iv.getValor();
			} else {
				return null;
			}
		} else {
			throw new Exception(director);
		}
	}

	/*
	 * Busca el indice asociado a traves de la tabla director.
	 */
	public Indice buscarTasa(String d) {
		Director director = null;
		try {
			director = DirectorHandler.getDirector(d);

			Long id = new Long(director.getValor());
			Indice indice = (Indice) bp.getById(Indice.class, id);
			return indice;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/*
	 * Determina el CER a una fecha determinada.
	 */
	public Double calcularCER(Date fecha) throws Exception {

		Date ayer = new Date(fecha.getTime() - (60 * 60 * 24 * 1000));
		double tasa = buscarTasaMaximaAnterior("emergencia.tasa.CER", ayer);
		return tasa - 1;
	}

	/*
	 * Determina la tasa de DGR a una fecha determinada.
	 */
	public Double calcularIndiceRenta(Date pFecha) throws Exception {
		return buscarTasaMaximaAnterior("emergencia.tasa.DRG", pFecha);
	}

	/*
	 * Determina la tasa del Banco de Prevision Social a una fecha determinada.
	 */
	public double calcularTasaBancoPrevision(Date fecha) throws Exception {
		double tasa = buscarTasaMaximaAnterior("emergencia.tasa.banco_prevision", fecha);
		return tasa; // 142433.7822;
	}

	/*
	 * Determina la tasa del Banco de Mendoza a una fecha determinada.
	 */
	public double calcularTasaBancoMendoza(Date fecha) throws Exception {
		double tasa = buscarTasaMaximaAnterior("emergencia.tasa.banco_mendoza", fecha);
		return tasa;
	}

	/*
	 * Busca todos los desembolsos de un credito.
	 */
	@SuppressWarnings("unchecked")
	public List<Desembolso> buscarDesembolsos(Long idCredito) {

		return bp.getNamedQuery("Desembolso.findByCredito").setParameter("idCredito", idCredito).list();
	}

	/*
	 * Busca el monto total desembolsado de todos los creditos de una persona.
	 */
	public double calcularTotalDesembolsos(List<Objetoi> creditos) {
		Double desembolso = 0.0;

		for (Objetoi credito : creditos) {
			desembolso += calcularDesembolsosCredito(credito);
		}
		return desembolso;
	}

	/*
	 * Busca el monto total desembolsado para un credito particular.
	 */
	public double calcularDesembolsosCredito(Objetoi credito) {
		List<Desembolso> desembolsos = buscarDesembolsos(credito.getId());

		Double total = 0.0;
		for (Desembolso desembolso : desembolsos) {
			total += desembolso.getImporte();
		}

		return total;

	}

	/*
	 * Busca la cantidad de emergencias de una persona.
	 */
	public int calcularCantidadCreditos(Long idPersona) {
		return ((Long) bp.createQuery("SELECT COUNT(e) FROM Emergencia e WHERE e.persona.id = :idPersona")
				.setParameter("idPersona", idPersona).uniqueResult()).intValue();
	}

	// Busca la tasa de renta maxima al 31/12/2000 es siempre 0.342210
	public Double calcularRenta() {
		return 0.342210;
	}

	/*
	 * Busca la tasa libor maxima anterior al 30/12/2000.
	 */
	public Double calcularCoeficienteLibor() throws Exception {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd");
		Date fecha = sd.parse("2000/12/29");
		return buscarTasaMaximaAnterior("emergencia.tasa.libor", fecha) + 0.03;
	}

	/*
	 * Retorna todos los creditos de emergencia asociados a una persona.
	 */
	@SuppressWarnings("unchecked")
	public List<Objetoi> buscarCreditos(long idPersona) throws Exception {
		Director director = null;
		try {
			director = DirectorHandler.getDirector(Linea.LINEA_EMERGENCIA);
		} catch (Exception e) {
			throw new Exception(Linea.LINEA_EMERGENCIA);
		}

		Long id = new Long(director.getValor());
		List<Objetoi> creditos = bp.createQuery(
				"SELECT o FROM Objetoi o WHERE o.numeroCredito IS NOT NULL AND o.persona.id = :idPersona AND o.linea.id = :idlinea")
				.setParameter("idPersona", idPersona).setParameter("idlinea", id).list();
		if (creditos != null && !creditos.isEmpty()) {
			nombre = creditos.get(0).getPersona().getNomb12();
		}
		return creditos;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reportResult;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Date getpFecha() {
		return pFecha;
	}

	public void setpFecha(Date pFecha) {
		this.pFecha = pFecha;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setIdRd(Long idRd) {
		this.idRd = idRd;
	}

	public Long getIdRd() {
		return idRd;
	}

}
