package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.civitas.hibernate.persona.Estado;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Emergencia;
import com.nirven.creditos.hibernate.EmergenciaPeriodo;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;

public class EmergenciaAgropecuariaProcess implements IProcess{
	
	private Long idPersona;
	@SuppressWarnings("unchecked")
	private HashMap errors;
	private String action;
	private BusinessPersistance bp;
	private List<Objetoi> creditosEmergencia;
	private Boolean[] seleccion;
	private String forward = "EmergenciaAgropecuariaList";
	private Persona persona;
	private List<Emergencia> emergencias;
	
	//Atributo para crear y modificar Emergencias
	private Long idPeriodo;
	private String expediente;
	private String observaciones;
	private boolean listar;
	private Long idEmergencia;
	
	public EmergenciaAgropecuariaProcess() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	@Override
	public boolean doProcess() {
		if(action == null || action.equals("search")){
			buscarCreditos();
			forward = "EmergenciaAgropecuariaList";			
		}else if(action.equals("generar")){
			genenarAcuerdoPagos();
			forward = "EmergenciaAgropecuariaList";
		}else if(action.equalsIgnoreCase("listarEmergencias")){
			Long idSession = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("Emergencia.idPersona");
			if(idSession==null||idSession==0){
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("Emergencia.idPersona", idPersona);
			}else if(idSession.compareTo(idPersona)!=0){
				SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("Emergencia.idPersona", idPersona);
			}			
			buscarEmergencias();
			listar=true;
			forward="EmergenciaList";
		}else if(action.equalsIgnoreCase("nueva")){
			idEmergencia=null;
			forward="EmergenciaList";
		}else if(action.equalsIgnoreCase("modificar")){
			cargarDatos();
			forward="EmergenciaList";
		}else if(action.equalsIgnoreCase("guardar")){
			idPersona=(Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("Emergencia.idPersona");
			guardarEmergencia();
			action="listarEmergencias";
			this.doProcess();
		}else if(action.equalsIgnoreCase("cancelar")){
			idPersona=(Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession().getAttribute("Emergencia.idPersona");			
			action="listarEmergencias";
			this.doProcess();
		}
		return true;
	}
	
	//Metodos privados
	
	private void guardarEmergencia(){
		Emergencia emergencia;
		if(idEmergencia==null||idEmergencia==0){
			emergencia = new Emergencia();
		}else{
			emergencia = (Emergencia) bp.getById(Emergencia.class, idEmergencia);
		}
		emergencia.setEmergenciaPeriodo((EmergenciaPeriodo) bp.getById(EmergenciaPeriodo.class, idPeriodo));
		emergencia.setPersona((Persona) bp.getById(Persona.class, idPersona));
		emergencia.setExpediente(expediente) ;
		emergencia.setObservaciones(observaciones);
		bp.saveOrUpdate(emergencia);
	}
	
	private void cargarDatos(){
		Emergencia emergencia = 
			(Emergencia) bp.getById(Emergencia.class, idEmergencia);
		idPeriodo= emergencia.getEmergenciaPeriodo().getId();
		observaciones=emergencia.getObservaciones();
		expediente = emergencia.getExpediente();
	}
	
	@SuppressWarnings("unchecked")
	private void buscarEmergencias(){
		emergencias= (List<Emergencia>) bp.getNamedQuery("Emergencia.findByIdPersona").setParameter("persona", bp.getById(Persona.class, idPersona)).list();
	}
	
	private void genenarAcuerdoPagos() {
		
		Estado estadoNuevo = (Estado)bp.getNamedQuery("Estado.findByNombreEstado").
								setParameter("nombreEstado", "ORIGINAL CON ACUERDO").
								uniqueResult();
		
		ObjetoiEstado creditoEstado;
		
		bp.begin();
		
		for(Objetoi o : creditosEmergencia){
			
			ObjetoiEstado estadoAnterior = (ObjetoiEstado)bp.
											createQuery("SELECT oe FROM ObjetoiEstado oe WHERE oe.fechaHasta is null")
											.uniqueResult();
			
			estadoAnterior.setFechaHasta(new Date());
			
			creditoEstado = new ObjetoiEstado();
			creditoEstado.setEstado(estadoNuevo);
			creditoEstado.setFecha(new Date());
			creditoEstado.setObjetoi(o);
			
			bp.saveOrUpdate(estadoAnterior);
			bp.save(creditoEstado);
			
		}
		
		bp.commit();
		
	}

	@SuppressWarnings("unchecked")
	private void buscarCreditos(){
		if(idPersona ==null){
			creditosEmergencia = new ArrayList<Objetoi>();
			return;
		}
		persona = (Persona)bp.getById(Persona.class, idPersona);
		
		creditosEmergencia = (ArrayList<Objetoi>)bp.createQuery("SELECT o FROM Objetoi o JOIN o.linea l WHERE l.nombre = :nombreLinea " +
								"AND o.persona = :persona")
								.setParameter("persona",persona)
								.setParameter("nombreLinea", "EMERGENCIA AGROPECUARIA")
								.list();
	}

	@SuppressWarnings("unchecked")
	public HashMap getErrors() {
		return errors;
	}

	public String getForward() {
		return forward;
	}

	public String getInput() {
		return forward;
	}

	public Object getResult() {
		return null;
	}

	public boolean validate() {
		return true;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<Objetoi> getCreditosEmergencia() {
		return creditosEmergencia;
	}

	public void setCreditosEmergencia(List<Objetoi> creditosEmergencia) {
		this.creditosEmergencia = creditosEmergencia;
	}

	public Boolean[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(Boolean[] seleccion) {
		this.seleccion = seleccion;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Emergencia> getEmergencias() {
		return emergencias;
	}

	public void setEmergencias(List<Emergencia> emergencias) {
		this.emergencias = emergencias;
	}

	public Long getIdPeriodo() {
		return idPeriodo;
	}

	public void setIdPeriodo(Long idPeriodo) {
		this.idPeriodo = idPeriodo;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public boolean isListar() {
		return listar;
	}

	public void setListar(boolean listar) {
		this.listar = listar;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public Long getIdEmergencia() {
		return idEmergencia;
	}

	public void setIdEmergencia(Long idEmergencia) {
		this.idEmergencia = idEmergencia;
	}

}
