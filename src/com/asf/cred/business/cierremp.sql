SELECT o.id, o.numeroAtencion, p.nomb_12, l.nombre FROM Objetoi o 
JOIN ObjetoiEstado oe ON oe.objetoi_id = o.id AND oe.fechaHasta IS NULL 
JOIN PERSONA p ON p.IDPERSONA = o.persona_IDPERSONA 
JOIN Linea l ON o.linea_id = l.id 
WHERE o.id NOT IN (SELECT DISTINCT credito_id FROM Cuota WHERE emision_id IS NULL) 
AND o.id NOT IN (SELECT DISTINCT objetoi_id FROM Ctacte WHERE fechaProceso >= :fechaCalculo AND fechaProceso < :fechaProceso) 
AND oe.estado_idEstado IN (@estados) 
ORDER BY o.numeroAtencion  