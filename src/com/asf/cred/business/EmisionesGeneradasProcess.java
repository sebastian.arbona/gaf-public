package com.asf.cred.business;

import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.Emision;

public class EmisionesGeneradasProcess implements IProcess {

	private String forward;
	private String action;
	private Object result;
	private List<Emision> emisiones;

	@Override
	public boolean doProcess() {
		if (action == null) {
			setEmisiones(listarEmisionesGeneradas());
			forward = "EmisionesGeneradasList";

		}
		return true;
	}

	public String getForward() {
		return forward;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	@SuppressWarnings("unchecked")
	public List<Emision> listarEmisionesGeneradas() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		List<Emision> emisiones = bp
				.createQuery("SELECT e from Emision e where e.facturada is :true ORDER BY e.fechaEmision desc")
				.setParameter("true", true).list();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("EmisionesGeneradas.emision",
				emisiones);
		return emisiones;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public List<Emision> getEmisiones() {
		return emisiones;
	}

	public void setEmisiones(List<Emision> emisiones) {
		this.emisiones = emisiones;
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
