package com.asf.cred.business;
import java.io.Serializable;
import java.util.Date;

import com.nirven.creditos.hibernate.Objetoi;

public class ActualizacionIndiceBean implements Serializable {

	private static final long serialVersionUID = -8302087563415382787L;
	
	private Objetoi credito;
	
	private int numeroCuota;
	private double saldoCapital;
	private double indice;
	private double montoActualizacion;
	private Date fecha;
	private String usuario;
	
	public ActualizacionIndiceBean() {
	}
	
	public String getSaldoCapitalStr() {
		return String.format("%.2f", saldoCapital);
	}
	public String getIndiceStr() {
		return String.format("%.2f", indice);
	}
	public String getMontoActualizacionStr() {
		return String.format("%.2f", montoActualizacion);
	}
	
	public int getNumeroCuota() {
		return numeroCuota;
	}
	public void setNumeroCuota(int numeroCuota) {
		this.numeroCuota = numeroCuota;
	}
	public double getSaldoCapital() {
		return saldoCapital;
	}
	public void setSaldoCapital(double saldoCapital) {
		this.saldoCapital = saldoCapital;
	}
	public double getIndice() {
		return indice;
	}
	public void setIndice(double indice) {
		this.indice = indice;
	}
	public double getMontoActualizacion() {
		return montoActualizacion;
	}
	public void setMontoActualizacion(double montoActualizacion) {
		this.montoActualizacion = montoActualizacion;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}
