package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.hibernate.SQLQuery;
import org.jbpm.graph.exe.ProcessInstance;

import com.asf.gaf.hibernate.Cotizacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Domicilio;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.CargaRequisito;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.DomicilioObjetoi;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiBonificacion;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.Turno;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;
import com.sun.org.apache.commons.beanutils.BeanUtils;

public class SolicitudesHandler implements IProcess {
	private String forward = "errorPage";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	private Turno turno;
	private Objetoi credito;
	private String moneda;
	private String cotizacion;
	private Long idSolicitud;
	private Usuario currentUser;
	private String texto;
	private List<Objetoi> solicitudes;
	private String hora = "";
	private Long idPersona;
	private Long numeroAtencion;
	private Long numeroSolicitud;
	private String linea;
	private boolean editable = true;
	private String estadoSolicitud;
	private Long nroAte;
	private Long idFideicomiso;

	public SolicitudesHandler() {
		this.turno = new Turno();
		this.credito = new Objetoi();
		credito.setProcesoAprobacion(new ProcessInstance());
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String idUsuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		this.currentUser = (Usuario) bp.getById(com.nirven.creditos.hibernate.Usuario.class, idUsuario);
		this.idFideicomiso = null;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public boolean doProcess() {
		this.editable = true;
		if (this.accion.equals("list")) {
			this.forward = "SolicitudList";
			this.listarSolicitudes();
		} else if (this.accion.equals("nuevo")) {
			this.cargarTurno();
			this.forward = "Solicitud";
		} else if (this.accion.equals("load")) {
			this.forward = "Solicitud";
			this.cargarSolicitud();
		} else if (this.accion.equals("show")) {
			this.editable = false;
			this.forward = "Solicitud";
			this.cargarSolicitud();
		} else if (this.accion.equals("eliminar")) {
			cargarSolicitud();
			if (this.validateEliminar()) {
				this.forward = "SolicitudList";
				this.eliminarSolicitud();
			} else {
				this.forward = "errorPage";
			}
		} else if (this.accion.equals("guardar")) {
			if (this.validate()) {
				this.guardarSolicitud();
				this.listarSolicitudes();
				this.forward = "SolicitudList";
			} else {
				this.forward = "errorPage";
			}
		} else if (this.accion.equals("cancelar")) {
			this.forward = "SolicitudList";
		} else if (this.accion.equals("startProcess")) {
			this.forward = "Solicitud";
			this.cargarSolicitud();
			this.credito.startSolicitud();
			this.accion = "show";
			this.editable = false;
		} else {
			this.forward = "SolicitudList";
		}
		return this.errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private boolean validateEliminar() {
		if (!this.credito.getDesembolsos().isEmpty()) {
			this.errores.put("Solicitud.error.cronograma", "Solicitud.error.cronograma");
		}
		List<Garantia> garantias = bp
				.createQuery("SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :cred and g.baja is null")
				.setParameter("cred", credito).list();
		if (!garantias.isEmpty()) {
			this.errores.put("solicitud.error.garantias", "solicitud.error.garantias");
		}
		List<ObjetoiVinedo> vinedos = bp.createQuery("Select v from ObjetoiVinedo v where v.credito=:cred")
				.setParameter("cred", credito).list();
		if ((!vinedos.isEmpty())) {
			this.errores.put("solicitud.error.vinedos", "solicitud.error.vinedos");
		}
		if (credito.getExpediente() != null && !credito.getExpediente().isEmpty()
				&& !credito.getExpediente().equals("0")) {
			errores.put("Solicitud.error.eliminarExpediente", "Solicitud.error.eliminarExpediente");
		}
		return this.errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void eliminarSolicitud() {
		try {
			List<CargaRequisito> requisitos = bp.createQuery("Select b from CargaRequisito b where b.credito=:cred")
					.setParameter("cred", credito).list();
			for (CargaRequisito requisito : requisitos) {
				bp.delete(requisito);
			}
			List<ObjetoiBonificacion> bonificaciones = bp
					.createQuery("Select b from ObjetoiBonificacion b where b.objetoi=:cred")
					.setParameter("cred", credito).list();
			for (ObjetoiBonificacion bonificacion : bonificaciones) {
				bp.delete(bonificacion);
			}
			List<ObjetoiComportamiento> comportamientos = bp
					.createQuery("Select b from ObjetoiComportamiento b where b.objetoi=:cred")
					.setParameter("cred", credito).list();
			for (ObjetoiComportamiento comportamiento : comportamientos) {
				bp.delete(comportamiento);
			}
			List<ObjetoiIndice> indices = bp.createQuery("Select b from ObjetoiIndice b where b.credito=:cred")
					.setParameter("cred", credito).list();
			for (ObjetoiIndice indice : indices) {
				bp.delete(indice);
			}
			List<DomicilioObjetoi> domicilios = bp.createQuery("Select b from DomicilioObjetoi b where b.objetoi=:cred")
					.setParameter("cred", credito).list();
			for (DomicilioObjetoi domicilio : domicilios) {
				bp.delete(domicilio);
			}
			List<ObjetoiEstado> estados = bp.createQuery("Select b from ObjetoiEstado b where b.objetoi=:cred")
					.setParameter("cred", credito).list();
			for (ObjetoiEstado estado : estados) {
				bp.delete(estado);
			}
			this.bp.delete(this.credito);
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("Solicitud.error.general", "Solicitud.error.general");
		}
	}

	private void cargarSolicitud() {
		if (this.idSolicitud != null && this.idSolicitud.longValue() > 0) {
			this.credito = (Objetoi) bp.getById(Objetoi.class, this.idSolicitud);
		}
		if (credito.getTurno() != null) {
			nroAte = credito.getTurno().getNumeroAtencion();
		} else {
			nroAte = null;
		}
		CreditoHandler creditoHandler = new CreditoHandler();
		Estado estado = creditoHandler.findEstado(credito);
		if (estado != null) {
			estadoSolicitud = estado.getNombreEstado();
		}
	}

	private void cargarTurno() {
		if (this.turno != null && this.turno.getId() != null && this.turno.getId().longValue() > 0) {
			Long turnos = (Long) bp.createQuery("SELECT COUNT(ob) FROM Objetoi ob WHERE ob.turno = :turno")
					.setParameter("turno", turno).uniqueResult();
			if (turnos > 0) {
				// si el turno ya esta relacionado con algun objetoi con se
				// puede cargar
				errores.put("Solicitud.error.turno", "Solicitud.error.turno");
				return;
			}
			this.turno = (Turno) this.bp.getById(Turno.class, this.turno.getId());
			nroAte = turno.getNumeroAtencion();
			this.credito = new Objetoi();
			this.credito.setMontoTotal(this.turno.getMonto());
			this.credito.setFinanciamiento(this.turno.getMonto());
			this.credito.setFinanciamientoSol(this.turno.getMonto());
			this.credito.setLinea(this.turno.getLinea());
			this.credito.setObjeto(this.turno.getObjeto());
			this.credito.setFechaSolicitud(this.turno.getAtencion());
			this.credito.setObservaciones(this.turno.getObservaciones());
			this.credito.setTurno(turno);
		} else {
			this.turno = new Turno();
			this.credito = new Objetoi();
			this.credito.setMontoTotal(this.turno.getMonto());
			this.credito.setFinanciamiento(this.turno.getMonto());
			this.credito.setFinanciamientoSol(this.turno.getMonto());
			this.credito.setLinea(this.turno.getLinea());
			this.credito.setObjeto(this.turno.getObjeto());
			this.credito.setFechaSolicitud(this.turno.getAtencion());
			this.credito.setObservaciones(this.turno.getObservaciones());
			this.credito.setTurno(turno);
		}
	}

	@SuppressWarnings("unchecked")
	private void listarSolicitudes() {
		String consulta = "";
		consulta = "SELECT o FROM Objetoi o";
		String filtro = "";
		if (this.idPersona != null && this.idPersona.longValue() > 0) {
			filtro = " WHERE o.persona.id = " + this.idPersona.toString();
		}
		if (this.numeroAtencion != null && this.numeroAtencion.longValue() > 0)
			if (filtro.length() > 0)
				filtro += " AND o.numeroAtencion = " + this.numeroAtencion.toString();
			else
				filtro = " WHERE o.numeroAtencion=" + this.numeroAtencion.toString();
		if (this.idSolicitud != null && this.idSolicitud.longValue() > 0)
			filtro = " WHERE  o.id = " + this.idSolicitud.toString();
		if (filtro.length() > 0) {
			this.solicitudes = this.bp.getByFilter(consulta + filtro);
			if (this.solicitudes != null)
				if (this.solicitudes.size() > 0 && this.solicitudes.get(0) != null)
					this.idSolicitud = this.solicitudes.get(0).getId();
		}
	}

	@SuppressWarnings("unchecked")
	public boolean validate() {
		if (this.accion.equals("guardar")) {
			if (credito.getPersona() != null && credito.getPersona().getEstadoActual() != null) {
				if (credito.getPersona().getEstadoActual().getEstado().getNombreEstado().toUpperCase()
						.equals("EXCLUIDO")) {
					errores.put("Solicitud.error.estodoPersonaExcluida", "Solicitud.error.estodoPersonaExcluida");
				} else if (credito.getPersona().getEstadoActual().getEstado().getNombreEstado().toUpperCase()
						.equals("BAJA")) {
					errores.put("Solicitud.error.estodoPersonaBaja", "Solicitud.error.estodoPersonaBaja");
				}
			}
			if (this.credito.getFechaSolicitud() == null) {
				this.errores.put("Solicitud.error.fecha", "Solicitud.error.fecha");
			}
			if (this.credito.getTipoSector() == null || this.credito.getTipoSector().isEmpty()) {
				this.errores.put("Solicitud.error.tipoSector", "Solicitud.error.tipoSector");
			}
			if (this.credito.getPersona().getId() == null) {
				/**
				 * el objeto persona asociado al credito nunca vuelve null
				 */
				this.errores.put("Solicitud.error.persona", "Solicitud.error.persona");
			}

			// validar que no haya una persona en cooperativa con un credito
			// en la misma linea, en la misma temporada (si es cosecha)
			boolean esCosecha = this.credito.getLinea().isLineaCosecha();
			boolean esCosechaPreAprobada = this.credito.getLinea().isLineaCosechaPreAprobada();

			// Valido si el titular est� relacionado a otra persona con
			// relaci�nn tipo CAPE
			if ((esCosecha ) && credito.getPersona() != null) {
				Long idRelacionCAPE = new Long(DirectorHelper.getString("id.relacion.CAPE", "28"));
				Number n = (Number) this.getBp()
						.createQuery("SELECT count(pv.id) from "
								+ com.civitas.hibernate.persona.PersonaVinculada.class.getCanonicalName()
								+ " pv where pv.relacion.id = :relacion and pv.personaVinculada = :persona ")
						.setEntity("persona", this.credito.getPersona()).setLong("relacion", idRelacionCAPE)
						.uniqueResult();
				if (n.longValue() > 0) {
					this.errores.put("Solicitud.error.personaCAPE", "Solicitud.error.personaCAPE");
				}
			}
			CosechaConfig cosechaConfig = null;
			String queryCoop = "select count(*) from Objetoi o where persona_IDPERSONA in (select personaTitular_IDPERSONA from PersonaVinculada where relacion_idRelacion = "
					+ "	(select idRelacion from Relacion where nombreRelacion = 'Asociados Cooperativa') and personaVinculada_IDPERSONA = ?) and o.linea_id = ? ";
			if (esCosecha || esCosechaPreAprobada) {
				cosechaConfig = (CosechaConfig) bp
						.createQuery("SELECT c FROM CosechaConfig c WHERE c.fechaInicioPer <= :fecha AND "
								+ "c.fechaFinPer >= :fecha AND c.varietales = :varietales")
						.setParameter("fecha", this.credito.getFechaSolicitud())
						.setParameter("varietales", this.credito.getVarietales()).uniqueResult();
				if (cosechaConfig == null) {
					this.errores.put("Solicitud.cosechaConfig", "Solicitud.cosechaConfig");
				} else
					queryCoop += " and o.fechaSolicitud >= ? and o.fechaSolicitud <= ? ";
			}
			SQLQuery query = bp.createSQLQuery(queryCoop);
			query.setLong(0, this.credito.getPersona().getId());
			query.setLong(1, this.credito.getLinea().getId());
			if ((esCosecha || esCosechaPreAprobada) && cosechaConfig != null) {
				query.setDate(2, cosechaConfig.getFechaInicioPer());
				query.setDate(3, cosechaConfig.getFechaFinPer());
			}
			Number countCooperativa = (Number) query.uniqueResult();
			if (countCooperativa != null && countCooperativa.intValue() > 0 && esCosecha) {
				this.errores.put("Solicitud.error.cooperativa", "Solicitud.error.cooperativa");
			}
			try {
				boolean esCyA = false;
				boolean esDestCosecha = false;
				if (esCosechaPreAprobada) {
					if (credito.getDestino().toString().equals("CRDA")) {
						this.errores.put("Solicitud.error.preElab", "Solicitud.error.preElab");
					}
				}
				if (esCosecha || esCosechaPreAprobada) {
					esCyA = true;
					esDestCosecha = credito.getDestino().equals("CRDC");
				}
				if (esCyA && esDestCosecha) {
					List<CosechaConfig> cosechas = (List<CosechaConfig>) bp.getNamedQuery("CosechaConfig.findByFecha")
							.setParameter("fecha", credito.getFechaSolicitud())
							.setParameter("varietales", credito.getVarietales()).list();
					if (cosechas.size() > 0) {
						cosechaConfig = cosechas.get(0);
						List<Objetoi> creditos = bp
								.createQuery("select c from Objetoi c where c.persona = :persona "
										+ "and c.fechaSolicitud >= :fecha1 and c.fechaSolicitud <= :fecha2 "
										+ "and c.destino ='CRDC'")
								.setParameter("persona", credito.getPersona())
								.setParameter("fecha1", cosechaConfig.getFechaInicioPer())
								.setParameter("fecha2", cosechaConfig.getFechaFinPer()).list();
						for (Objetoi cred : creditos) {
							bp.getCurrentSession().evict(cred);
							if (cred.getLinea().isLineaCosecha()
									&& (credito.getId() == null || credito.getId().longValue() == 0)
									&& cred.getLinea().getSolExcluyente() == true) {
								this.errores.put("Solicitud.error.fechaSolicitud", "Solicitud.error.fechaSolicitud");
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (credito.getLinea() != null && credito.getLinea().getMonto() != 0.0) {
			Double sumFinanciamiento = (Double) bp.createSQLQuery("SELECT SUM(FINANCIAMIENTO) FROM Objetoi O "
					+ "INNER JOIN ObjetoiEstado OE ON OE.objetoi_id = O.id "
					+ "INNER JOIN Estado E ON OE.estado_idEstado = E.idEstado AND E.nombreEstado NOT IN ('ESPERANDO DOCUMENTACION', 'DESISTIDO') "
					+ "WHERE linea_id = :linea_id AND OE.fechaHasta IS NULL")
					.setLong("linea_id", credito.getLinea().getId()).uniqueResult();
			if (sumFinanciamiento == null) {
				sumFinanciamiento = 0.0;
			}
			if ((sumFinanciamiento + credito.getFinanciamiento()) > credito.getLinea().getMonto()) {
				errores.put("Solicitud.error.financiamiento", "Solicitud.error.financiamiento");
			}
		}
		if (credito.getLinea() != null && credito.getLinea().getMontoObjetoi() != null
				&& credito.getLinea().getMontoObjetoi() != 0.0) {
			if (credito.getFinanciamiento() > credito.getLinea().getMontoObjetoi()) {
				errores.put("Solicitud.error.montoObjetoi", "Solicitud.error.montoObjetoi");
			}
		}
		return this.errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void guardarSolicitud() {
		boolean esNuevo = false;
		try {
			// estado del credito
			ObjetoiEstado objetoiEstado = null;
			if (credito.getId() == null || credito.getId().longValue() == 0) {
				esNuevo = true;
				objetoiEstado = new ObjetoiEstado();
				objetoiEstado.setFecha(new Date());
				try {
					Estado estado = (Estado) bp
							.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = 'ESPERANDO DOCUMENTACION'")
							.uniqueResult();
					objetoiEstado.setEstado(estado);
				} catch (NonUniqueResultException nue) {
					objetoiEstado = null;
				} catch (NoResultException nre) {
					objetoiEstado = null;
				}
			}
			ObjetoiComportamiento comportamiento = null;
			// comportamiento del pago
			if (credito.getId() == null || credito.getId().longValue() == 0) {
				comportamiento = new ObjetoiComportamiento();
				comportamiento.setFecha(new Date());
				comportamiento.setComportamientoPago(ObjetoiComportamiento.SITUACION_NORMAL);
			}
			if (this.credito.getNumeroAtencion() == null || this.credito.getNumeroAtencion() == 0) {
				this.credito.setNumeroAtencion(Numerador.getNext("objetoi.numeroAtencion"));
			}
			if (nroAte != null && nroAte != 0) {
				this.credito
						.setTurno((Turno) (bp.getByFilter("FROM Turno t WHERE t.numeroAtencion = " + nroAte)).get(0));
			}
			this.credito.setDelegacion(this.currentUser.getUnidad());
			// compruebo si la persona tiene un domicilio real
			List<com.civitas.hibernate.persona.Domicilio> doms = (List<com.civitas.hibernate.persona.Domicilio>) bp
					.createQuery("Select d from Domicilio d where d.persona=:persona and d.tipo=:tipo")
					.setParameter("persona", credito.getPersona()).setParameter("tipo", "Real").list();
			DomicilioObjetoi domO = new DomicilioObjetoi();
			if (doms.size() >= 1 && esNuevo) {
				com.civitas.hibernate.persona.Domicilio dom = doms.get(0);
				com.civitas.hibernate.persona.Domicilio nuevoDom = new com.civitas.hibernate.persona.Domicilio();
				nuevoDom = (Domicilio) BeanUtils.cloneBean(dom);
				nuevoDom.setTipo("Especial");
				nuevoDom.setId(null);
				nuevoDom.setPersona(null);
				bp.save(nuevoDom);
				domO.setDomicilio(nuevoDom);
				domO.setObjetoi(credito);
			}
			// Fin creacion Domicilio
			boolean esCyA = credito.getLinea().isLineaCosecha();
			// Seteo los gastos generales de la linea en el credito
			if (credito.getLinea() != null) {
				credito.setPorcentajeGastosCuota(credito.getLinea().getPorcentaje());
				credito.setMontoGastosCuota(credito.getLinea().getImporte());
				credito.setCalcularGastosLocal(false);
			}
			// si el credito es de Cosecha y Acarreo
			if (esCyA && esNuevo) {
				CosechaConfig cosechaConfig = (CosechaConfig) bp.getNamedQuery("CosechaConfig.findByTemporadaMayor")
						.setParameter("varietales", this.credito.getVarietales()).list().get(0);
				this.credito.setFrecuenciaCapital(cosechaConfig.getPeriodicidad());
				this.credito.setFrecuenciaInteres(cosechaConfig.getPeriodicidad());
				this.credito.setPrimerVencCapital(cosechaConfig.getFechaPrimerVto());
				this.credito.setPrimerVencInteres(cosechaConfig.getFechaPrimerVto());
				this.credito.setPlazoCapital(cosechaConfig.getCantCuotas());
				this.credito.setPlazoCompensatorio(cosechaConfig.getCantCuotas());
			} else if (esNuevo) {
				this.credito.setFrecuenciaCapital(credito.getLinea().getPeriodicidadCapital());
				this.credito.setFrecuenciaInteres(credito.getLinea().getPeriodicidadInteres());
				this.credito.setPlazoCapital(credito.getLinea().getCantCuotasCapital());
				this.credito.setPlazoCompensatorio(credito.getLinea().getCantCuotasInteres());
			}
			if (!esNuevo) {
				Objetoi existente = (Objetoi) bp.getById(Objetoi.class, credito.getId());
				credito.setProcesoAprobacion(existente.getProcesoAprobacion());
				bp.getCurrentSession().evict(existente);
				ProcessInstance p = credito.getProcesoAprobacion();
				if (p != null) {
					p = (ProcessInstance) bp.getById(ProcessInstance.class, p.getId());
					credito.setProcesoAprobacion(p);
				}
				credito.setPrimerVencCapital(existente.getPrimerVencCapital());
				credito.setPrimerVencInteres(existente.getPrimerVencInteres());
				credito.setBonificacion(existente.getBonificacion());
				credito.setCalcularGastosLocal(existente.getCalcularGastosLocal());
				credito.setPorcentajeGastosCuota(existente.getPorcentajeGastosCuota());
				credito.setMontoGastosCuota(existente.getMontoGastosCuota());
				credito.setAportePropio(existente.getAportePropio());
				credito.setOtrosAportes(existente.getOtrosAportes());
				credito.setCofinanciador(existente.getCofinanciador());
				credito.setAporteCofinanciador(existente.getAporteCofinanciador());
			} else {
				Usuario usuario = (Usuario) bp.getById(Usuario.class,
						SessionHandler.getCurrentSessionHandler().getCurrentUser());
				credito.setAsesor(usuario);
				credito.setProcesoAprobacion(null);
				credito.setTipoAmortizacion(Objetoi.METODO_CALCULO_ALEMAN);
			}
			// Guardo la cotizacion Inicial del credito
			List<Cotizacion> cotis = bp
					.createQuery("Select c from Cotizacion c where c.moneda=:moneda order by c.fechaDesde")
					.setParameter("moneda", credito.getLinea().getMoneda()).list();
			Cotizacion coti = new Cotizacion();
			if (cotis.size() != 0) {
				coti = cotis.get(0);
			}
			for (Cotizacion cotizacion : cotis) {
				if (cotizacion.getFechaHasta() == null) {
					coti = cotizacion;
				} else {
					if (coti.getFechaHasta() != null && coti.getFechaHasta().before(cotizacion.getFechaHasta())) {
						coti = cotizacion;
					}
				}
			}
			credito.setCotizaInicial(coti.getVenta());
			bp.begin();
			this.bp.saveOrUpdate(this.credito);
			this.idSolicitud = this.credito.getId();
			if (domO.getObjetoi() != null) {
				bp.save(domO);
			}
			if (objetoiEstado != null) {
				objetoiEstado.setObjetoi(credito);
				bp.save(objetoiEstado);
			}
			if (comportamiento != null) {
				comportamiento.setObjetoi(credito);
				bp.save(comportamiento);
			}
			bp.commit();
			// Crea ObjetoiIndice para el nuevo credito
			if (esNuevo) {
				generarTasa();
			}
			// FIN ObjetoiIndice
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("Solicitud.error.general", "Solicitud.error.general");
		}
	}

	public void generarTasa() throws Exception {
		CreditoHandler ch = new CreditoHandler();
		errores.putAll(ch.generarDatosFinancieros(credito));
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

	public Turno getTurno() {
		if (this.turno == null)
			this.turno = new Turno();
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Object getResult() {
		return null;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Usuario getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(Usuario currentUser) {
		this.currentUser = currentUser;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public List<Objetoi> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(List<Objetoi> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Long getIdSolicitud() {
		return idSolicitud;
	}

	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public Long getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(Long numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}

	public Long getNroAte() {
		return nroAte;
	}

	public void setNroAte(Long nroAte) {
		this.nroAte = nroAte;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(String cotizacion) {
		this.cotizacion = cotizacion;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

	public Long getIdFideicomiso() {
		return idFideicomiso;
	}

	public void setIdFideicomiso(Long idFideicomiso) {
		this.idFideicomiso = idFideicomiso;
	}

}