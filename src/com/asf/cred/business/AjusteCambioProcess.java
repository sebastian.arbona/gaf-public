package com.asf.cred.business;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Objetoi;

public class AjusteCambioProcess extends ConversationProcess implements Serializable {

	private static final long serialVersionUID = 6862443136344417926L;

	private Date fecha = new Date();
	private String mensaje;

	private List<Objetoi> creditos;

	@ProcessMethod
	public boolean realizarAjuste() {
		fecha = new Date(); // todos los ajustes se hacen a la fecha del dia
		try {
			CreditoHandler ch = new CreditoHandler();
			creditos = ch.realizarAjusteMoneda(new Date());
			mensaje = "Se han realizado los ajustes en las cuentas corrientes de " + creditos.size() + " creditos.";
			return true;
		} catch (Exception e) {
			mensaje = e.getMessage();
			e.printStackTrace();
			errors.put("ajusteCambio.error", "ajusteCambio.error");
			return false;
		}
	}

	@Override
	protected String getDefaultForward() {
		return "AjusteCambio";
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public String getFecha() {
		return DateHelper.getString(fecha);
	}

	public void setFecha(String fecha) {
		this.fecha = DateHelper.getDate(fecha);
	}

	public String getMensaje() {
		return mensaje;
	}
}
