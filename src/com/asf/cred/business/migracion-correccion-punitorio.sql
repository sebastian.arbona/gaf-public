select ccd.* 
--ccd.itemCtacte, ccd.movimientoCtacte, ccd.periodoCtacte, ccd.verificadorCtacte
from Ctacte ccc
join Cuota c on ccc.cuota_id = c.id
join Ctacte ccd on ccc.cuota_id = ccd.cuota_id
and ccc.importe = ccd.importe and ccc.asociado_id = ccd.asociado_id 
join Objetoi o on o.id = ccd.objetoi_id
join ObjetoiEstado oe on oe.objetoi_id = ccc.objetoi_id and oe.fechaHasta is null
and oe.estado_idEstado in (6,7,8,9,10)
join Movpagos mp on ccc.movimientoCtacte = mp.movimientoCtacte
and ccc.itemCtacte = mp.itemCtacte and ccc.periodoCtacte = mp.periodoCtacte
and ccc.verificadorCtacte = mp.verificadorCtacte
left join (
select mpCap.movimientoCtacte, 
mpCap.itemCtacte, mpCap.periodoCtacte, mpCap.verificadorCtacte,
mpCap.pagos_boleto_id, mpCap.pagos_caratula_id
from Movpagos mpCap 
join Ctacte cccap on cccap.movimientoCtacte = mpCap.movimientoCtacte
and cccap.itemCtacte = mpCap.itemCtacte and cccap.periodoCtacte = mpCap.periodoCtacte
and cccap.verificadorCtacte = mpCap.verificadorCtacte and cccap.asociado_id = 2
) t on mp.pagos_boleto_id = t.pagos_boleto_id and mp.pagos_caratula_id = t.pagos_caratula_id
left join (
select id, credito_id, numero
from Cuota where estado = '2'
) cp on cp.credito_id = ccc.objetoi_id and cp.numero > c.numero
left join (
select objetoi_id, numero
from Ctacte join Cuota on cuota_id = id where tipomov_id = 1 and tipoMovimiento like 'pago'
) pagosFuturos on pagosFuturos.objetoi_id = o.id and pagosFuturos.numero > c.numero
where ccc.asociado_id in (3,4) and ccc.tipomov_id = 1 and ccd.tipomov_id = 2
and t.pagos_boleto_id is null 
and pagosFuturos.objetoi_id is null
and c.estado = '1'
and cp.id is null
order by o.numeroCredito, ccd.fechaGeneracion