package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.hibernate.CuentaBancaria;
import com.asf.security.BusinessPersistance;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.bpm.ImputacionesHelper;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.CategoriaSolicitanteConfig;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.ConvenioBonificacion;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;
import com.nirven.creditos.hibernate.ObjetoiIndice;

public class CreditoDatosFinancieros extends CreditoProcess {

	private double tasaCompensatorio;
	private double topeTasaCompensatorio;
	private double tasaBonificada;
	private double tasaPunitorio;
	private double topeTasaPunitorio;
	private double tasaMoratorio;
	private double topeTasaMoratorio;
	private Long idCompensatorio;
	private Long idPunitorio;
	private Long idMoratorio;

	private Double valorMas2;
	private Double valorPor2;
	private Double tasaTope2;

	private Double valorMas3;
	private Double valorPor3;
	private Double tasaTope3;

	double QQFinal;

	private ObjetoiIndice objetoiIndice;

	private String tipoTasa;
	private double bonificacion;
	private Long idObjetoiIndice;
	private boolean compensatorio;

	private String forward = "CreditoDatosFinancieros";
	private double tasaNeta;

	private Long idIndice;

	private boolean deshabilitarCampos;
	private boolean ocultarCampos;
	private String estadoAcuerdo;
	private boolean ocultarCamposPago;
	private String estadoAcuerdoPago;
	private Objetoi existente;
	private Long cantidadCuentasBancarias;
	private boolean calcularGastosLocal;
	private boolean permisoInteresCompensatorio;
	private double importeGastos;
	private double porcentajeGastos;
	private String categoriaSolicitante = "";

	public CreditoDatosFinancieros() {
		// this.credito.setSolicitud(new Solicitud());
		this.credito.setMoneda_id(1L);
		objetoiIndice = new ObjetoiIndice();
		objetoiIndice.setIndice(new Indice());
		objetoiIndice.setCredito(credito);
		cantidadCuentasBancarias = -1L;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (idIndice != null) {
			objetoiIndice.setIndice((Indice) bp.getById(Indice.class, idIndice));
		}
		// las siguientes lineas se utiliza para obtener el idObjetoi cuando se
		// carga las pagina de indices de inter�s.
		if (getIdObjetoi() != null) {
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("creditoDatosFinancieros.idObjetoi", getIdObjetoi());
		} else {
			setIdObjetoi((Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("creditoDatosFinancieros.idObjetoi"));
		}
		estadoAcuerdo = DirectorHelper.getString("estado.ObjetoOriginal", "");

		if (action == null || action.equals("load")) {
			super.doProcess();
			try {
				buscarObjetoi();
			} catch (HibernateException e1) {
				e1.printStackTrace();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			Objetoi obj = buscarObjetoi(getIdObjetoi());
			if (obj != null) {
				if (obj.getEstadoActual() != null
						&& obj.getEstadoActual().getEstado().getId().toString().equalsIgnoreCase(estadoAcuerdo)) {
					ocultarCampos = true;
				} else {
					ocultarCampos = false;
				}
				if (obj.getCalcularGastosLocal() != null && obj.getCalcularGastosLocal()) {
					importeGastos = obj.getMontoGastosCuota();
					porcentajeGastos = obj.getPorcentajeGastosCuota();
				} else {
					importeGastos = obj.getLinea().getImporte();
					porcentajeGastos = obj.getLinea().getPorcentaje();
				}
			}
			estadoAcuerdo = DirectorHelper.getString("estado.ObjetoOriginal", "");
			if (obj != null) {
				if (obj.getEstadoActual() != null
						&& obj.getEstadoActual().getEstado().getId().toString().equalsIgnoreCase(estadoAcuerdoPago)) {
					ocultarCamposPago = true;
				} else {
					ocultarCamposPago = false;
				}
			}
			Double sumaDesembolsos = (Double) bp
					.createQuery("select sum(d.importe) from Desembolso d where d.credito.id = :idCredito")
					.setLong("idCredito", getIdObjetoi()).uniqueResult();
			getObjetoi().setAporteAgente(sumaDesembolsos);
			verificarPrimerDesembolso();
			cargarObjetoiIndice2();
			buscarObjetoiIndice();
			if (getEstadoActual() != null && getEstadoActual().getNombreEstado() != null
					&& getEstadoActual().getNombreEstado().equalsIgnoreCase("ANALISIS")) {
				deshabilitarCampos = false;
			} else {
				deshabilitarCampos = true;
			}
			verificarCBU();
			calcularGastosLocal = getObjetoi().getCalcularGastosLocal() != null ? getObjetoi().getCalcularGastosLocal()
					: false;
			return true;
		} else if (action.equals("save")) {
			try {
				return guardarObjetoi();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("loadIndice")) {
			cargarObjetoiIndice();
			buscarObjetoiIndice();
			forward = "CreditoIndice";
			return true;
		} else if (action.equals("saveIndice")) {
			try {
				boolean ok = guardarObjetoiIndice();
				cargarObjetoiIndice();
				buscarObjetoiIndice();
				forward = "CreditoIndice"; // no navegar de vuelta
				return ok;
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("saveConfSolicitante")) {
			try {
				boolean ok = guardarObjetoiIndiceConfigSolicitante();
				forward = "CreditoIndice"; // no navegar de vuelta
				return ok;
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("modificarBonificacion")) {
			try {
				guardarObjetoiIndice();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			cargarObjetoiIndice();
			buscarObjetoiIndice();
			compensatorio = true;
			forward = "CreditoIndice";
			return true;
		} else if (action.equalsIgnoreCase("verificar")) {
			log.debug("Verificar");
			if (!verificarDatos()) {
				try {
					buscarObjetoi();
					log.debug(this.getObjetoi());
				} catch (HibernateException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return errores.isEmpty();
			}
			/**
			 * fuerzo el guardado de los datos y entonces despues puedo usar la instancia
			 * recuperada de la bdd
			 */
			try {
				if (guardarObjetoi()) {
					existente = (Objetoi) bp.getById(Objetoi.class, credito.getId());
					log.debug(existente);
					if (cantidadCuentasBancarias != null && cantidadCuentasBancarias.intValue() == 1) {
						try {
							List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
									.setLong("idCredito", existente.getId()).list();
							CuentaBancaria cb = (CuentaBancaria) bp
									.createQuery("SELECT cb FROM CuentaBancaria cb WHERE cb.persona = :persona")
									.setEntity("persona", existente.getPersona()).setMaxResults(1).uniqueResult();
							bp.begin();
							if (cb != null) {
								for (Desembolso desembolso : desembolsos) {
									desembolso.setCuentaInterbanking(cb);
									bp.update(desembolso);
									log.debug(desembolso);
								}
							}
							bp.commit();
						} catch (Exception e) {
							// no se puede vincular
							bp.rollback();
							e.printStackTrace();
						}
					}

					ObjetoiEstado estadoExistente = existente.getEstadoActual();

					boolean esCosecha = existente.getLinea().isLineaCosecha();
					boolean esElaboracion = verificarElaboracion(existente);
					boolean deshabilita = false;
					if (!esCosecha) {
						if (getSumaDesembolsos() != 0) {
							deshabilita = true;
						}
					}
					log.debug("if((existente.getQqFinal()!= null &&  existente.getQqFinal() != 0) || deshabilita)");
					if ((existente.getQqFinal() != null && existente.getQqFinal() != 0) || deshabilita) {
						log.debug("esCosecha=" + esCosecha);
						if (esCosecha) {
							log.debug("Llamo a calcular QQ");
							CreditoHandler ch = new CreditoHandler();
							ch.calcularFinanciamiento(existente);
							actualizarEstadoObjetoiGarantia(existente, new Date());
						}
						ImputacionesHelper ih = new ImputacionesHelper();
						ih.setEtapa(1l);
						ih.setTipoExpediente(15l);
						try {
							log.debug("Llamo a imputacion WS.");
							ih.imputar(existente);
							log.debug("Imputacion WS OK.");
							CreditoHandler ch = new CreditoHandler();
							double importeBonificaciones = ch.calcularBonificacionEstimada(existente.getId());
							existente.setBonificacion(importeBonificaciones);
							int periodoActual = Calendar.getInstance().get(Calendar.YEAR);
							if (importeBonificaciones >= 0.01) {
								String expediente = DirectorHelper.getString("imputacion.bonif.expediente");
								if (expediente != null) {
									List<BonDetalle> bonDetalles = ch.getBonDetalles();
									// agrupar por convenio
									Map<Long, Double> montosPorConvenio = new HashMap<Long, Double>();
									for (BonDetalle d : bonDetalles) {
										if (d.getBonificacion() != null && d.getBonificacion().getConvenio() != null) {
											// solo sumar vencimientos del periodo actual
											if (d.getCuota() != null) {
												Calendar c = Calendar.getInstance();
												c.setTime(d.getCuota().getFechaVencimiento());
												if (c.get(Calendar.YEAR) == periodoActual) {
													Double monto = montosPorConvenio
															.get(d.getBonificacion().getConvenio().getId());
													if (monto == null) {
														montosPorConvenio.put(d.getBonificacion().getConvenio().getId(),
																d.getMonto());
													} else {
														montosPorConvenio.put(d.getBonificacion().getConvenio().getId(),
																monto + d.getMonto());
													}
												}
											}
										}
									}
									Calendar c = Calendar.getInstance();
									c.setTime(existente.getFechaSolicitud());
									int periodo = c.get(Calendar.YEAR);
									for (Long idConvenio : montosPorConvenio.keySet()) {
										ConvenioBonificacion convenio = (ConvenioBonificacion) bp
												.getById(ConvenioBonificacion.class, idConvenio);
										Double montoBonificaciones = montosPorConvenio.get(idConvenio);
										Number idIEgreso = (Number) bp.createSQLQuery(
												"select idiegreso from iegreso where codigo = :codigo and ejercicio = :ejercicio")
												.setParameter("codigo", convenio.getCodigoIegreso())
												.setParameter("ejercicio", periodo).setMaxResults(1).uniqueResult();
										Number idInstitucional = (Number) bp.createSQLQuery(
												"select idinstitucional from institucional where codigo = :codigo and ejercicio = :ejercicio")
												.setParameter("codigo", convenio.getCodigoInstitucional())
												.setParameter("ejercicio", periodo).setMaxResults(1).uniqueResult();
										Number idNomenclador = (Number) bp.createSQLQuery(
												"select idnomenclador from nomenclador where codigo = :codigo and ejercicio = :ejercicio")
												.setParameter("codigo", convenio.getCodigoNomenclador())
												.setParameter("ejercicio", periodo).setMaxResults(1).uniqueResult();
										if (idIEgreso != null && idInstitucional != null && idNomenclador != null) {
											log.debug("Llamo a imputacion bonificaciones WS");
											ih.imputar(1l, 1l, 15l, existente.getId(), expediente, montoBonificaciones,
													idIEgreso.longValue(), idInstitucional.longValue(),
													idNomenclador.longValue());
											log.debug("Imputacion bonificaciones WS");
										}
									}
								}
							}
							Estado estNuevo = buscarEstadoPorNombre("PENDIENTE RESOLUCION");
							log.debug("Estado Existente " + estadoExistente);
							if (estadoExistente != null) {
								bp.begin();
								if (estNuevo != null) {
									existente.setEstadoActual(estNuevo, "Modificado por "
											+ SessionHandler.getCurrentSessionHandler().getCurrentUser());
								}
								bp.update(existente);
								bp.commit();
								log.debug("commit cambio de estado");
							}
							// dejar el valor de volumen de contrato
							// fijo una vez verificados los datos
							if (esCosecha || esElaboracion) {
								double volumen = existente.getDepositoMinimoDouble();
								bp.begin();
								existente.setVolumenContrato(volumen);
								bp.update(existente);
								bp.commit();
							}
							deshabilitarCampos = true;
						} catch (Exception e) {
							errores.put("datosVerificados.ws1", "datosVerificados.ws1");
							errores.put("datosVerificados.ws2", "datosVerificados.ws2");
							e.printStackTrace();
							return errores.isEmpty();
						}
					} else {
						deshabilitarCampos = false;
					}
					log.debug("Fin Verificar");
					action = "load";
					doProcess();
					// forward = "CreditoDatosFinancieros";
					return true;
				}
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		} else if (action.equals("generarTasa")) {
			try {
				generarTasa();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			action = "load";
			doProcess();
		}
		return true;
	}

	public boolean verificarElaboracion(Objetoi credito) {
		return credito.getDestino().equalsIgnoreCase("CRDA");
	}

	public double getQQFinal() {
		return QQFinal;
	}

	public void setQQFinal(double qQFinal) {
		QQFinal = qQFinal;
	}

	public Objetoi getExistente() {
		return existente;
	}

	public void setExistente(Objetoi existente) {
		this.existente = existente;
	}

	public void generarTasa() throws NumberFormatException, HibernateException, Exception {
		buscarObjetoi();
		CreditoHandler ch = new CreditoHandler();
		errores.putAll(ch.generarDatosFinancieros(getObjetoi()));

	}

	@SuppressWarnings("unchecked")
	private void verificarPrimerDesembolso() {
		List<Desembolso> listaDesembolso = bp
				.createQuery("select d from Desembolso d where d.credito = :credito order by d.numero")
				.setParameter("credito", getObjetoi()).list();
		for (Desembolso desembolso : listaDesembolso) {
			if (desembolso.getEstado() != null && desembolso.getEstado().equals("2")) {
				deshabilitarCampos = true;
				break;
			}
		}
	}

	@Override
	protected void buscarObjetoi() throws HibernateException, Exception {
		super.buscarObjetoi();
		if (credito != null) {
			CreditoHandler handler = new CreditoHandler();

			// buscar tasas.
			// si no existen, crearlas la primera vez.

			ObjetoiIndice compensatorio = credito.findTasaCompensatorio();
			if (compensatorio == null) {
				compensatorio = new ObjetoiIndice();
				compensatorio.setCredito(credito);
				compensatorio.setTipoTasa("1"); // compensatorio
				bp.save(compensatorio);
			}
			tasaCompensatorio = compensatorio.getValorFinal();
			idCompensatorio = compensatorio.getId();

			if (compensatorio.getTasaTope() == null) {
				topeTasaCompensatorio = 0.0;
			} else {
				topeTasaCompensatorio = compensatorio.getTasaTope();
			}

			// tomo la inicial del credito = primera cuota
			double[] bonificaciones = handler.calcularTasaNeta(getIdObjetoi());
			if (bonificaciones.length > 0) {
				tasaNeta = bonificaciones[0];
			} else {
				tasaNeta = 0;
			}

			ObjetoiIndice moratorio = credito.findTasaMoratorio();
			if (moratorio == null) {
				moratorio = new ObjetoiIndice();
				moratorio.setCredito(credito);
				moratorio.setTipoTasa("2"); // moratorio

				bp.save(moratorio);
			}
			valorMas2 = moratorio.getValorMas();
			valorPor2 = moratorio.getValorPor();
			tasaTope2 = moratorio.getTasaTope();

			tasaMoratorio = moratorio.getValorFinal();
			idMoratorio = moratorio.getId();

			if (moratorio.getTasaTope() == null) {
				topeTasaMoratorio = 0.0;
			} else {
				topeTasaMoratorio = moratorio.getTasaTope();
			}

			ObjetoiIndice punitorio = credito.findTasaPunitorio();
			if (punitorio == null) {
				punitorio = new ObjetoiIndice();
				punitorio.setCredito(credito);
				punitorio.setTipoTasa("3"); // punitorio
				bp.save(punitorio);
			}

			valorMas3 = punitorio.getValorMas();
			valorPor3 = punitorio.getValorPor();
			tasaTope3 = punitorio.getTasaTope();
			tasaPunitorio = punitorio.getValorFinal();
			idPunitorio = punitorio.getId();

			if (punitorio.getTasaTope() == null) {
				topeTasaPunitorio = 0.0;
			} else {
				topeTasaPunitorio = punitorio.getTasaTope();
			}

		}
	}

	private boolean guardarObjetoi() throws HibernateException, Exception {
		if (!validarObjetoi()) {
			buscarObjetoi();
			if (credito.getEstadoActual().getEstado().getNombreEstado().equalsIgnoreCase("ANALISIS")) {
				deshabilitarCampos = false;
			} else {
				deshabilitarCampos = true;
			}
			return false;
		}
		Objetoi existente = (Objetoi) bp.getById(Objetoi.class, credito.getId());
		existente.setTipoAmortizacion(credito.getTipoAmortizacion());
		existente.setPrimerVencCapital(credito.getPrimerVencCapital());
		existente.setPrimerVencInteres(credito.getPrimerVencInteres());
		existente.setPlazoCapital(credito.getPlazoCapital());
		existente.setPlazoCompensatorio(credito.getPlazoCompensatorio());
		existente.setFrecuenciaCapital(credito.getFrecuenciaCapital());
		existente.setFrecuenciaInteres(credito.getFrecuenciaInteres());
		existente.setAgente(credito.getAgente());
		existente.setAporteAgente(credito.getAporteAgente());
		existente.setCofinanciador(credito.getCofinanciador());
		existente.setAporteCofinanciador(credito.getAporteCofinanciador());
		existente.setAportePropio(credito.getAportePropio());
		existente.setOtrosAportes(credito.getOtrosAportes());
		existente.setCalcularGastosLocal(calcularGastosLocal);
		existente.setCategoriaSolicitante(categoriaSolicitante);
		bp.saveOrUpdate(existente);
		buscarObjetoi();
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean validarObjetoi() {
		if (credito == null) {
			return true;
		}
		if (credito.getTipoAmortizacion() == null || credito.getTipoAmortizacion().isEmpty()) {
			errores.put("objetoi.tipoAmortizacion", "objetoi.tipoAmortizacion");
		}
		if (credito.getPrimerVencCapital() == null) {
			errores.put("objetoi.primerVencCapital", "objetoi.primerVencCapital");
		}
		if (credito.getPrimerVencInteres() == null) {
			errores.put("objetoi.primerVencInteres", "objetoi.primerVencInteres");
		}
		if (credito.getPlazoCapital() == null || credito.getPlazoCapital() == 0) {
			errores.put("objetoi.cuotasCapital", "objetoi.cuotasCapital");
		}
		if (credito.getPlazoCompensatorio() == null || credito.getPlazoCompensatorio() == 0) {
			errores.put("objetoi.cuotasInteres", "objetoi.cuotasInteres");
		}
		if (credito.getPorcentajeGastosCuota() != null
				&& (credito.getPorcentajeGastosCuota() < 0 || credito.getPorcentajeGastosCuota() > 100)) {
			errores.put("objetoi.porcentajeGastosCuota", "objetoi.porcentajeGastosCuota");
		}
		if (credito.getMontoGastosCuota() != null && (credito.getMontoGastosCuota() < 0)) {
			errores.put("objetoi.montoGastosCuota", "objetoi.montoGastosCuota");
		}
		if (credito.getFrecuenciaCapital() == null || credito.getFrecuenciaCapital().isEmpty()) {
			errores.put("objetoi.frecuenciaCapital", "objetoi.frecuenciaCapital");
		}
		if (credito.getFrecuenciaInteres() == null || credito.getFrecuenciaInteres().isEmpty()) {
			errores.put("objetoi.frecuenciaInteres", "objetoi.frecuenciaInteres");
		}
		// validar fechas y cantidad de cuotas
		ArrayList<Date> feriados = new ArrayList<Date>(bp.getByFilter("SELECT f.fecha FROM Feriado f"));
		Calendar vencCapital = Calendar.getInstance();
		vencCapital.setTime(credito.getPrimerVencCapital());
		Calendar vencInteres = Calendar.getInstance();
		vencInteres.setTime(credito.getPrimerVencInteres());
		int frec = Integer.parseInt(credito.getFrecuenciaInteres());
		int cant = credito.getPlazoCompensatorio();
		int mesesInteres = (cant - 1) * frec;
		frec = Integer.parseInt(credito.getFrecuenciaCapital());
		cant = credito.getPlazoCapital();
		int mesesCapital = (cant - 1) * frec;
		vencInteres.add(Calendar.MONTH, mesesInteres);
		vencCapital.add(Calendar.MONTH, mesesCapital);
		Date ultimaInteres = DateHelper.getDiaHabil(feriados, vencInteres.getTime());
		Date ultimaCapital = DateHelper.getDiaHabil(feriados, vencCapital.getTime());
		if (!ultimaInteres.equals(ultimaCapital)) {
			errores.put("objetoi.cuotas.ultima", "objetoi.cuotas.ultima");
		}
		return errores.isEmpty();
	}

	private void cargarObjetoiIndice() {
		objetoiIndice = (ObjetoiIndice) bp.getById(ObjetoiIndice.class, idObjetoiIndice);
	}

	private void cargarObjetoiIndice2() {
		objetoiIndice = (ObjetoiIndice) bp.getById(ObjetoiIndice.class, idCompensatorio);
	}

	private void buscarObjetoiIndice() {
		String tipoTasaTipif = objetoiIndice.getTipoTasa();
		if (tipoTasaTipif.equals("1")) {
			tipoTasa = "Compensatorio";
			compensatorio = true;
			CreditoHandler handler = new CreditoHandler();
			// tomo la inicial del credito = primera cuota
			double[] bonificaciones = handler.calcularTasaNeta(objetoiIndice.getCredito().getId(), true);
			if (bonificaciones.length > 0) {
				tasaNeta = bonificaciones[0];
			} else {
				tasaNeta = 0;
			}

			if (objetoiIndice.getTasaTope() == null) {
				topeTasaCompensatorio = 0.0;
			} else {
				topeTasaCompensatorio = objetoiIndice.getTasaTope();
			}
			bonificacion = objetoiIndice.getValorFinal() - tasaNeta;
		} else if (tipoTasaTipif.equals("2")) {
			tipoTasa = "Moratorio";

			if (objetoiIndice.getTasaTope() == null) {
				topeTasaMoratorio = 0.0;
			} else {
				topeTasaMoratorio = objetoiIndice.getTasaTope();
			}
			compensatorio = false;
		} else if (tipoTasaTipif.equals("3")) {
			tipoTasa = "Punitorio";
			if (objetoiIndice.getTasaTope() == null) {
				topeTasaPunitorio = 0.0;
			} else {
				topeTasaPunitorio = objetoiIndice.getTasaTope();
			}
			compensatorio = false;
		}
	}

	private boolean guardarObjetoiIndice() throws HibernateException, Exception {
		if (!validarObjetoiIndice()) {
			forward = "CreditoIndice";
			return false;
		}
		ObjetoiIndice existente = (ObjetoiIndice) bp.getById(ObjetoiIndice.class, objetoiIndice.getId());
		existente.setDiasAntes(objetoiIndice.getDiasAntes());
		existente.setIndice(objetoiIndice.getIndice());
		existente.setValorMas(objetoiIndice.getValorMas());
		existente.setValorPor(objetoiIndice.getValorPor());
		existente.setTipoCalculo(objetoiIndice.getTipoCalculo());
		existente.setTasaTope(objetoiIndice.getTasaTope());

		bp.begin();
		bp.saveOrUpdate(existente);
		bp.commit();
		buscarObjetoi();
		forward = "CreditoDatosFinancieros";
		return true;
	}
	
	
	private boolean guardarObjetoiIndiceConfigSolicitante() throws HibernateException, Exception {
		
		List <CategoriaSolicitanteConfig> estado = (List<CategoriaSolicitanteConfig>) bp.getNamedQuery("CategoriaSolicitanteConfig.findByCategoriaSolicitante").setParameter("categoriaSolicitante",categoriaSolicitante).list();
		
		if(estado.size() > 0) {
			for (CategoriaSolicitanteConfig categoriaSolicitanteConfig : estado) {

				if(categoriaSolicitanteConfig.getTipoTasa().equals("1")) {
					objetoiIndice = (ObjetoiIndice) bp.getById(ObjetoiIndice.class, idCompensatorio);
				}
				if(categoriaSolicitanteConfig.getTipoTasa().equals("2")) {
					objetoiIndice = (ObjetoiIndice) bp.getById(ObjetoiIndice.class, idMoratorio);
				}
				if(categoriaSolicitanteConfig.getTipoTasa().equals("3")) {
				BusinessPersistance bp1 = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
					objetoiIndice = (ObjetoiIndice) bp1.getById(ObjetoiIndice.class, idPunitorio);
					System.out.println("Valor de retorno mas: " + objetoiIndice.getValorMasStr());					
				}
				System.out.println("valor categoria: " + categoriaSolicitanteConfig.getDiasAntes());
				objetoiIndice.setDiasAntes(categoriaSolicitanteConfig.getDiasAntes());
				objetoiIndice.setValorMas(categoriaSolicitanteConfig.getValorMas());
				objetoiIndice.setValorPor(categoriaSolicitanteConfig.getValorPor());
				objetoiIndice.setTipoCalculo(categoriaSolicitanteConfig.getTipoCalculo());
				bp.begin();
				bp.saveOrUpdate(objetoiIndice);
				bp.commit();
				bp.getCurrentSession().evict(objetoiIndice);
				System.out.println("valor categoria: " + objetoiIndice.getDiasAntes());
				
				
			}
			Objetoi existente = (Objetoi) bp.getById(Objetoi.class, idObjetoiIndice);
			existente.setCategoriaSolicitante(categoriaSolicitante);
			bp.saveOrUpdate(existente);
			bp.commit();
			bp.getCurrentSession().evict(existente);
			System.out.println("valor categoria: " + existente.getCategoriaSolicitante());
			
		}

		forward = "CreditoDatosFinancieros";
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean validarObjetoiIndice() {
		if (credito == null) {
			return true;
		}
		List<ObjetoiIndice> listaObjetoiIndice = bp.createQuery("SELECT o FROM Indice o WHERE id = :id")
				.setParameter("id", objetoiIndice.getIndice().getId()).list();
		if (listaObjetoiIndice == null) {
			errores.put("objetoiIndice.indice", "objetoiIndice.indice");
		}
		if (listaObjetoiIndice.size() < 1) {
			errores.put("objetoiIndice.indice", "objetoiIndice.indice");
		}
		if (objetoiIndice.getIndice() == null || objetoiIndice.getIndice().getId() == 0) {
			errores.put("objetoiIndice.indice", "objetoiIndice.indice");
		}
		if (objetoiIndice.getValorMas() == null || objetoiIndice.getValorMas() < 0) {
			errores.put("objetoiIndice.valorMas", "objetoiIndice.valorMas");
		}
		if (objetoiIndice.getValorPor() == null || objetoiIndice.getValorPor() == 0) {
			errores.put("objetoiIndice.valorPor", "objetoiIndice.valorPor");
		}
		ObjetoiIndice indiceExistente = (ObjetoiIndice) bp.getById(ObjetoiIndice.class, idObjetoiIndice);
		String tipoTasaTipif = indiceExistente.getTipoTasa();
		if (tipoTasaTipif.equals("1")) { // COMPENSATORIO
			if (objetoiIndice.getDiasAntes() == null || objetoiIndice.getDiasAntes() < 0) {
				errores.put("objetoiIndice.diasAntes", "objetoiIndice.diasAntes");
			}
		}
		return errores.isEmpty();
	}

	public Double getFinanciamientoSolicitado() {
		Double desembolsado = (Double) bp
				.createQuery("select sum(d.importe) from Desembolso d where d.credito = :credito")
				.setParameter("credito", credito).uniqueResult();
		if (desembolsado != null && desembolsado.doubleValue() > 0) {
			return desembolsado;
		} else {
			return credito.getFinanciamiento();
		}
	}

	public String getFinanciamientoSolicitadoStr() {
		Double desembolsado = (Double) bp
				.createQuery("select sum(d.importe) from Desembolso d where d.credito = :credito")
				.setParameter("credito", credito).uniqueResult();
		if (desembolsado != null && desembolsado.doubleValue() > 0) {
			return String.format("%.2f", desembolsado);
		} else {
			return String.format("%.2f", credito.getFinanciamiento());
		}
	}

	public Double getSumaDesembolsos() {
		Double desembolsado = (Double) bp
				.createQuery("select sum(d.importe) from Desembolso d where d.credito = :credito")
				.setParameter("credito", credito).uniqueResult();
		if (desembolsado != null && desembolsado.doubleValue() > 0) {
			return desembolsado;
		} else {
			return new Double(0.0);
		}
	}

	@SuppressWarnings("unchecked")
	private void actualizarEstadoObjetoiGarantia(Objetoi credito, Date date) {
		// TODO ojo esto esta duplicado en CreditoCuentaCorriente, CierreCaja,
		// CreditoDatosFinancieros, FinalizarCredito
		List<ObjetoiGarantia> og = bp
				.createQuery("SELECT g from ObjetoiGarantia g where g.objetoi = :credito and g.baja is null")
				.setParameter("credito", credito).list();
		if (!og.isEmpty()) {
			for (ObjetoiGarantia objetoiGarantia : og) {
				if (objetoiGarantia != null) {
					GarantiaEstado nuevoEstadoGarantia = new GarantiaEstado(ComportamientoGarantiaEnum.ACTIVA, date);
					nuevoEstadoGarantia.setObjetoiGarantia(objetoiGarantia);
					nuevoEstadoGarantia.determinarImporte();
					bp.save(nuevoEstadoGarantia);
				}
			}
		}
	}

	@Override
	public String getForward() {
		return forward;
	}

	@SuppressWarnings("unchecked")
	public Estado buscarEstadoPorNombre(String nombre) {
		List<Estado> lista = new ArrayList<Estado>();
		lista = bp.createQuery("SELECT e FROM Estado e WHERE e.nombreEstado = :nombreEstado")
				.setParameter("nombreEstado", nombre).list();
		return lista.get(0);
	}

	@Override
	public String getInput() {
		return "CreditoDatosFinancieros";
	}

	public double getTasaCompensatorio() {
		return tasaCompensatorio;
	}

	public String getIndiceValorStr() {
		if (objetoiIndice.getIndice() != null) {
			if (objetoiIndice.getDiasAntes() == null) {
				objetoiIndice.setDiasAntes(0);
			}
			
			try {
				if(objetoiIndice.getIndice().getId() != null) {
					return String.format("%3.2f%%",
							new CreditoHandler().buscarValorIndice(objetoiIndice.getIndice().getId()) * 100);
				}
			} catch (Exception e) {
				e.getMessage();
			}
			
		}
		return String.format("%3.2f%%", 0.00);
	}

	public String getTasaCompensatorioStr() {
		return String.format("%.2f%%", tasaCompensatorio);
	}

	public String getTasaMoratorioStr() {
		return String.format("%.2f%%", tasaMoratorio);
	}

	public String getTasaPunitorioStr() {
		return String.format("%.2f%%", tasaPunitorio);
	}

	public void setTasaCompensatorio(double tasaCompensatorio) {
		this.tasaCompensatorio = tasaCompensatorio;
	}

	public double getTasaBonificada() {
		return tasaBonificada;
	}

	public void setTasaBonificada(double tasaBonificada) {
		this.tasaBonificada = tasaBonificada;
	}

	public double getTasaPunitorio() {
		return tasaPunitorio;
	}

	public void setTasaPunitorio(double tasaPunitorio) {
		this.tasaPunitorio = tasaPunitorio;
	}

	public double getTasaMoratorio() {
		return tasaMoratorio;
	}

	public void setTasaMoratorio(double tasaMoratorio) {
		this.tasaMoratorio = tasaMoratorio;
	}

	public ObjetoiIndice getObjetoiIndice() {
		return objetoiIndice;
	}

	public void setObjetoiIndice(ObjetoiIndice objetoiIndice) {
		this.objetoiIndice = objetoiIndice;
	}

	public String getTipoTasa() {
		return tipoTasa;
	}

	public void setTipoTasa(String tipoTasa) {
		this.tipoTasa = tipoTasa;
	}

	public double getBonificacion() {
		return bonificacion;
	}

	public String getBonificacionStr() {
		return String.format("%.2f%%", bonificacion);
	}

	public void setBonificacion(double bonificacion) {
		this.bonificacion = bonificacion;
	}

	public void setBonificacionStr(String bonificacionStr) {
		bonificacion = Double.parseDouble(bonificacionStr.replaceAll(",", ".").replaceAll("%", ""));
	}

	public String getValorFinal() {
		return String.format("%.2f%%", objetoiIndice.getValorFinal());
	}

	public Long getIdObjetoiIndice() {
		return idObjetoiIndice;
	}

	public void setIdObjetoiIndice(Long idObjetoiIndice) {
		this.idObjetoiIndice = idObjetoiIndice;
	}

	public boolean isCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(boolean compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Long getIdCompensatorio() {
		return idCompensatorio;
	}

	public void setIdCompensatorio(Long idCompensatorio) {
		this.idCompensatorio = idCompensatorio;
	}

	public Long getIdPunitorio() {
		return idPunitorio;
	}

	public void setIdPunitorio(Long idPunitorio) {
		this.idPunitorio = idPunitorio;
	}

	public Long getIdMoratorio() {
		return idMoratorio;
	}

	public void setIdMoratorio(Long idMoratorio) {
		this.idMoratorio = idMoratorio;
	}

	public Double getValorMas2() {
		return valorMas2;
	}

	public void setValorMas2(Double valorMas2) {
		this.valorMas2 = valorMas2;
	}

	public Double getValorPor2() {
		return valorPor2;
	}

	public void setValorPor2(Double valorPor2) {
		this.valorPor2 = valorPor2;
	}

	public Double getTasaTope2() {
		return tasaTope2;
	}

	public void setTasaTope2(Double tasaTope2) {
		this.tasaTope2 = tasaTope2;
	}

	public Double getValorMas3() {
		return valorMas3;
	}

	public void setValorMas3(Double valorMas3) {
		this.valorMas3 = valorMas3;
	}

	public Double getValorPor3() {
		return valorPor3;
	}

	public void setValorPor3(Double valorPor3) {
		this.valorPor3 = valorPor3;
	}

	public Double getTasaTope3() {
		return tasaTope3;
	}

	public void setTasaTope3(Double tasaTope3) {
		this.tasaTope3 = tasaTope3;
	}

	public double getTasaNeta() {
		return tasaNeta;
	}

	public String getTasaNetaStr() {
		return String.format("%.2f%%", tasaNeta);
	}

	public void setTasaNeta(double tasaNeta) {
		this.tasaNeta = tasaNeta;
	}

	public boolean isDeshabilitarCampos() {
		return deshabilitarCampos;
	}

	public void setDeshabilitarCampos(boolean deshabilitarCampos) {
		this.deshabilitarCampos = deshabilitarCampos;
	}

	public Long getIdIndice() {
		return idIndice;
	}

	public void setIdIndice(Long idIndice) {
		this.idIndice = idIndice;
	}

	@Override
	public void setForward(String forward) {
		this.forward = forward;
	}

	@SuppressWarnings("unchecked")
	private boolean verificarDatos() {
		Objetoi credito = getObjetoi();
		if (credito.getTipoAmortizacion() == null) {
			errores.put("desembolsosEjecucion.tipoAmortizacionNulo", "desembolsosEjecucion.tipoAmortizacionNulo");
		}
		List<ObjetoiIndice> listaIndice = bp
				.createQuery("SELECT o FROM ObjetoiIndice o WHERE o.credito = :credito and o.tipoTasa =:tipo")
				.setParameter("credito", credito).setParameter("tipo", "1").list();
		if (listaIndice == null || listaIndice.isEmpty() || listaIndice.get(0).getIndice() == null) {
			errores.put("desembolsosEjecucion.tazaInteresCompensatorioNulo",
					"desembolsosEjecucion.tazaInteresCompensatorioNulo");
		}
		if (credito.getPrimerVencCapital() == null) {
			errores.put("desembolsosEjecucion.primerVencimientoCapitalNulo",
					"desembolsosEjecucion.primerVencimientoCapitalNulo");
		}
		if (credito.getPlazoCapital() == null || credito.getPlazoCapital().intValue() <= 0) {
			errores.put("desembolsosEjecucion.cantidadCuotasCampitalNulo",
					"desembolsosEjecucion.cantidadCuotasCampitalNulo");
		}
		if (credito.getPrimerVencInteres() == null) {
			errores.put("desembolsosEjecucion.primerVencimientoInteresNulo",
					"desembolsosEjecucion.primerVencimientoInteresNulo");
		}
		if (credito.getPlazoCompensatorio() == null || credito.getPlazoCompensatorio().intValue() <= 0) {
			errores.put("desembolsosEjecucion.cantidadCuotasInteresNulo",
					"desembolsosEjecucion.cantidadCuotasInteresNulo");
		}
		if (credito.getFrecuenciaCapital() == null) {
			errores.put("desembolsosEjecucion.periocidadCuotasCapitalNulo",
					"desembolsosEjecucion.periocidadCuotasCapitalNulo");
		}
		if (credito.getFrecuenciaInteres() == null) {
			errores.put("desembolsosEjecucion.periocidadCuotasInteresNulo",
					"desembolsosEjecucion.periocidadCuotasInteresNulo");
		}
		if (credito.getFinanciamiento() == null || credito.getFinanciamiento().doubleValue() <= 0.0) {
			errores.put("desembolsosEjecucion.financiamiento", "desembolsosEjecucion.financiamiento");
		}
		if (!credito.getEstadoActual().getEstado().getNombreEstado().equals("ANALISIS")) {
			errores.put("desembolsosEjecucion.estado", "desembolsosEjecucion.estado");
		}
		if (credito.getEstadoActual().getEstado().getNombreEstado().trim().equals("DESISTIDO")) {
			errores.put("desembolsosEjecucion.estado.desistido", "desembolsosEjecucion.estado.desistido");
		}
		if (credito.getDomicilioProyecto() == null) {
			errores.put("objetoi.domicilio.proyecto", "objetoi.domicilio.proyecto");
		}
		return errores.isEmpty();
	}

	public void verificarCBU() {
		if (credito.getLinea().getControlaCBU() != null && credito.getLinea().getControlaCBU()) {
			try {
				Desembolso desembolso = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
						.setLong("idCredito", credito.getId()).setInteger("numero", 1).setMaxResults(1).uniqueResult();
				if (desembolso != null) {
					if (desembolso.getCuentaInterbanking() == null) {
						cantidadCuentasBancarias = (Long) bp
								.createQuery("SELECT COUNT(cb) FROM CuentaBancaria cb WHERE cb.persona = :persona")
								.setEntity("persona", credito.getPersona()).uniqueResult();
					} else {
						cantidadCuentasBancarias = -1L;
					}
				} else {
					cantidadCuentasBancarias = -1L;
				}
			} catch (Exception e) {
				// no se puede verificar
				cantidadCuentasBancarias = -1L;
			}
		} else {
			cantidadCuentasBancarias = -1L;
		}
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	private Objetoi buscarObjetoi(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		return (Objetoi) bp.getById(Objetoi.class, id);
	}

	public String getEstadoAcuerdo() {
		return estadoAcuerdo;
	}

	public void setEstadoAcuerdo(String estadoAcuerdo) {
		this.estadoAcuerdo = estadoAcuerdo;
	}

	public boolean isOcultarCamposPago() {
		return ocultarCamposPago;
	}

	public void setOcultarCamposPago(boolean ocultarCamposPago) {
		this.ocultarCamposPago = ocultarCamposPago;
	}

	public String getEstadoAcuerdoPago() {
		return estadoAcuerdoPago;
	}

	public void setEstadoAcuerdoPago(String estadoAcuerdoPago) {
		this.estadoAcuerdoPago = estadoAcuerdoPago;
	}

	public Long getCantidadCuentasBancarias() {
		return cantidadCuentasBancarias;
	}

	public void setCantidadCuentasBancarias(Long cantidadCuentasBancarias) {
		this.cantidadCuentasBancarias = cantidadCuentasBancarias;
	}

	public boolean isCalcularGastosLocal() {
		return calcularGastosLocal;
	}

	public void setCalcularGastosLocal(boolean calcularGastosLocal) {
		this.calcularGastosLocal = calcularGastosLocal;
	}

	public double getImporteGastos() {
		return importeGastos;
	}

	public void setImporteGastos(double importeGastos) {
		this.importeGastos = importeGastos;
	}

	public double getPorcentajeGastos() {
		return porcentajeGastos;
	}

	public void setPorcentajeGastos(double porcentajeGastos) {
		this.porcentajeGastos = porcentajeGastos;
	}

	public boolean isPermisoInteresCompensatorio() {
		SessionHandler sh = (SessionHandler) SessionHandler.getCurrentSessionHandler();
		this.permisoInteresCompensatorio = sh.isAllowed("/actions/PERMISO PARA MODIFICAR INTERES COMPENSATORIO");
		return permisoInteresCompensatorio;
	}

	public void setPermisoInteresCompensatorio(boolean permisoInteresCompensatorio) {
		this.permisoInteresCompensatorio = permisoInteresCompensatorio;
	}

	public double getTopeTasaCompensatorio() {
		return topeTasaCompensatorio;
	}

	public void setTopeTasaCompensatorio(double topeTasaCompensatorio) {
		this.topeTasaCompensatorio = topeTasaCompensatorio;
	}

	public String getValorPor2Str() {
		if (getValorPor2() != null) {
			return String.format("%5.3f", getValorPor2());
		}
		return "1,000";
	}
	public String getValorPor3Str() {
		if (getValorPor3() != null) {
			return String.format("%5.3f", getValorPor3());
		}
		return "1,000";
	}
	public String getValorMas2Str() {
		if (getValorMas2() != null) {
			return String.format("%5.2f%%", getValorMas2());
		}
		return String.format("%5.2f%%", 0.00);
	}
	
	public String getValorMas3Str() {
		if (getValorMas3() != null) {
			return String.format("%5.2f%%", getValorMas3());
		}
		return String.format("%5.2f%%", 0.00);
	}
	
	public String getTasaTope3Str() {
		if (getTasaTope3() != null) {
			return String.format("%5.2f%%", getTasaTope3());
		}
		return String.format("%5.2f%%", 0.00);
	}
	
	public String getTasaTope2Str() {
		if (getTasaTope2() != null) {
			return String.format("%5.2f%%", getTasaTope2());
		}
		return String.format("%5.2f%%", 0.00);
	}
	
	
	public String getTopeTasaCompensatorioStr() {
		return String.format("%.2f%%", this.topeTasaCompensatorio);
	}

	public void setTopeTasaCompensatorioStr(String topeTasaCompensatorioStr) {
		topeTasaCompensatorio = Double.parseDouble(topeTasaCompensatorioStr.replaceAll(",", ".").replaceAll("%", ""));
	}

	public double getTopeTasaPunitorio() {
		return topeTasaPunitorio;
	}

	public void setTopeTasaPunitorio(double topeTasaPunitorio) {
		this.topeTasaPunitorio = topeTasaPunitorio;
	}

	public String getTopeTasaPunitorioStr() {
		return String.format("%.2f%%", this.topeTasaPunitorio);
	}

	public void setTopeTasaPunitorioStr(String topeTasaPunitorioStr) {
		topeTasaPunitorio = Double.parseDouble(topeTasaPunitorioStr.replaceAll(",", ".").replaceAll("%", ""));
	}

	public double getTopeTasaMoratorio() {
		return topeTasaMoratorio;
	}

	public void setTopeTasaMoratorio(double topeTasaMoratorio) {
		this.topeTasaMoratorio = topeTasaMoratorio;
	}

	public String getTopeTasaMoratorioStr() {
		return String.format("%.2f%%", this.topeTasaMoratorio);
	}

	public void setTopeTasaMoratorioStr(String topeTasaMoratorioStr) {
		topeTasaMoratorio = Double.parseDouble(topeTasaMoratorioStr.replaceAll(",", ".").replaceAll("%", ""));
	}

	public String getCategoriaSolicitante() {
		return categoriaSolicitante;
	}

	public void setCategoriaSolicitante(String categoriaSolicitante) {
		this.categoriaSolicitante = categoriaSolicitante;
	}
}
