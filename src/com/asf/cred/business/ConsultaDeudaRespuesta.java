package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.ConsultaDeuda;
import com.nirven.creditos.hibernate.ConsultaDeudaPersona;
import com.nirven.creditos.hibernate.ConsultaDeuda.EntidadEnum;

public class ConsultaDeudaRespuesta implements IProcess {

	private Long consulta_id;
	private Long consultaPersona_id;
	private HashMap<String, Object> errores;
	private ConsultaDeuda consulta;
	private com.nirven.creditos.hibernate.ConsultaDeudaRespuesta consultaRespuesta;
	private EntidadEnum entidad;
	private String action;
	private String tomador;

	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		if (this.consulta_id != null && this.consulta_id.longValue() > 0)
			this.consulta = (ConsultaDeuda) bp.getById(ConsultaDeuda.class, consulta_id);

		if ("guardar".equalsIgnoreCase(this.action)) {
			this.guardar();
		}

		if ("cancelar".equalsIgnoreCase(this.action)) {
			this.consultaPersona_id = null;
		}

		if ("init".equalsIgnoreCase(this.action)) {
			try {
				this.consulta.initProcess();
				return false;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (this.consultaPersona_id != null && this.consultaPersona_id.longValue() > 0) {
			ConsultaDeudaPersona consultaPersona = (ConsultaDeudaPersona) bp.getById(ConsultaDeudaPersona.class,
					consultaPersona_id);
			this.consultaRespuesta = consultaPersona.getRespuesta(entidad);
			if (this.consultaRespuesta.getFechaConsulta() == null)
				this.consultaRespuesta.setFechaConsulta(new Date());
		}

		return true;
	}

	private void guardar() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		com.nirven.creditos.hibernate.ConsultaDeudaRespuesta cr2 = null;
		if (this.consultaRespuesta.getId() == null || this.getConsultaRespuesta().getId().longValue() <= 0) {

			ConsultaDeudaPersona consultaPersona = (ConsultaDeudaPersona) bp.getById(ConsultaDeudaPersona.class,
					consultaPersona_id);
			cr2 = consultaPersona.getRespuesta(entidad);
			cr2.setFechaConsulta(new Date());

		} else {
			cr2 = (com.nirven.creditos.hibernate.ConsultaDeudaRespuesta) bp
					.getById(com.nirven.creditos.hibernate.ConsultaDeudaRespuesta.class, consultaRespuesta.getId());
		}
		cr2.setMonto(consultaRespuesta.getMonto());
		cr2.setObservaciones(consultaRespuesta.getObservaciones());
		cr2.setFechaVencimiento(consultaRespuesta.getFechaVencimiento());
		this.setConsultaPersona_id(null);
		bp.saveOrUpdate(cr2);
		this.readOnly = false;

	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return this.consultaPersona_id == null ? "ConsultaDeudaRespuestaList" : "ConsultaDeudaRespuesta";
	}

	@Override
	public String getInput() {
		return "ConsultaDeudaRespuestaList";
	}

	@Override
	public Object getResult() {
		return this.consultaPersona_id == null ? consulta.getRespuestas(entidad) : consultaRespuesta;
	}

	private boolean readOnly = false;

	public boolean isReadOnly() {
		return readOnly;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public Long getConsulta_id() {
		return consulta_id;
	}

	public void setConsulta_id(Long consultaId) {
		consulta_id = consultaId;
	}

	public Long getConsultaPersona_id() {
		return consultaPersona_id;
	}

	public void setConsultaPersona_id(Long consultaPersonaId) {
		consultaPersona_id = consultaPersonaId;
	}

	public ConsultaDeuda getConsulta() {
		return consulta;
	}

	public void setConsulta(ConsultaDeuda consulta) {
		this.consulta = consulta;
	}

	public com.nirven.creditos.hibernate.ConsultaDeudaRespuesta getConsultaRespuesta() {
		if (this.consultaRespuesta == null)
			this.consultaRespuesta = new com.nirven.creditos.hibernate.ConsultaDeudaRespuesta();
		return consultaRespuesta;
	}

	public void consultaDeudaRespuesta(com.nirven.creditos.hibernate.ConsultaDeudaRespuesta consultaRespuesta) {
		this.consultaRespuesta = consultaRespuesta;
	}

	public String getEntidadStr() {
		return entidad.name();
	}

	public void setEntidadStr(String entidad) {
		this.entidad = EntidadEnum.valueOf(entidad);
	}

	public String getEntidadToStr() {
		return entidad.toString();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public EntidadEnum getEntidad() {
		return entidad;
	}

	public void setEntidad(EntidadEnum entidad) {
		this.entidad = entidad;
	}

	public String getTomador() {
		return tomador;
	}

	public void setTomador(String tomador) {
		this.tomador = tomador;
		this.readOnly = this.tomador == null || this.tomador.isEmpty();
	}

}
