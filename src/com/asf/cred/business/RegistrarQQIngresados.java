package com.asf.cred.business;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DirectorHelper;
import com.asf.util.HTTPHelper;
import com.civitas.hibernate.persona.Persona;
import com.civitas.logger.LoggerService;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.QQIngresados;

public class RegistrarQQIngresados implements IProcess {

	private BusinessPersistance bp;
	private String forward;
	private String accion;
	private HashMap<String, Object> errores;
	private List<ObjetoiVinedo> vinedos;
	private ObjetoiVinedo vinedo;

	private List<Objetoi> creditos;
	private Objetoi credito;
	private Persona persona;
	private String expediente;

	private Integer rowNum;
	private boolean ingresar;
	private QQIngresados qqIngresados;
	private String qqIngresoStr;
	private boolean ingresarQQ;
	private String civ;
	private String qqIngresoId;
	private List<Linea> lineas;
	private Long[] seleccion;
	private String autorizacionId;
	LoggerService logger = LoggerService.getInstance();

	@SuppressWarnings("unchecked")
	public RegistrarQQIngresados() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		forward = "RegistrarQQIngresados";
		credito = new Objetoi();
		persona = new Persona();
		creditos = (List<Objetoi>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("creditosRegistrarQQIngresados");
		String lineasCosecha = DirectorHelper.getString("linea.cosecha");
		String lineasPreaprob = DirectorHelper.getString("linea.cosechaPreAprobada");
		if (lineasCosecha != null && !lineasCosecha.isEmpty()) {
			lineasCosecha += "," + lineasPreaprob;
		} else {
			lineasCosecha = lineasPreaprob;
		}
		lineas = bp.createQuery("select l from Linea l where l.id in (" + lineasCosecha + ") order by l.id desc")
				.list();
	}

	@Override
	public boolean doProcess() {
		if (accion != null && accion.equals("ingresar")) {
			ingresar();
		}
		if (accion != null && accion.equals("registrarIngreso")) {
			registrarIngreso();
		}
		if (accion != null && accion.equals("listar")) {
			listarCreditos();
		}
		// else if(accion !=null&& accion.equals("listarVinedo")){
		// listarCreditosVinedo();
		// }
		else if (accion != null && accion.equals("listarPorPersona")) {
			if (persona.getId() != null && persona.getId().longValue() != 0L)
				listarPorPersona();
		} else if (accion != null && accion.equals("listarPorExpediente")) {
			if (credito.getExpediente() != null && !credito.getExpediente().equals("0"))
				listarPorExpediente();

		} else if (accion != null && accion.equals("consultaINV")) {
			consultaINV1();
		} else if (accion != null && accion.equals("autorizar")) {
			autorizar();
		} else if (accion != null && accion.equals("guardarQQManual")) {
			guardarQQManual();
		}
		return errores.isEmpty();
	}

	private void autorizar() {
		if (autorizacionId == null || autorizacionId.trim().isEmpty()) {
			return;
		}
		Long idCredito = new Long(autorizacionId);
		Desembolso desembolso = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", idCredito).setInteger("numero", 1).setMaxResults(1).uniqueResult();
		Desembolso desembolso2 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", idCredito).setInteger("numero", 2).setMaxResults(1).uniqueResult();
		if (desembolso != null && desembolso.getEstado().trim().equals("2") && desembolso2 != null
				&& !desembolso2.getEstado().trim().equals("3")) {
			bp.begin();
			Objetoi c = (Objetoi) bp.getById(Objetoi.class, idCredito);
			c.setAutorizadoQQManual(true);
			bp.update(c);
			bp.commit();
			actualizarListado();
		}
	}

	private void guardarQQManual() {
		try {
			bp.begin();
			Enumeration<String> params = SessionHandler.getCurrentSessionHandler().getRequest().getParameterNames();
			while (params.hasMoreElements()) {
				String param = params.nextElement();
				if (param.startsWith("qqmanual-")) {
					Long id = new Long(param.split("-")[1]);
					String qq = SessionHandler.getCurrentSessionHandler().getRequest().getParameter(param);
					if (qq.trim().isEmpty()) {
						continue;
					}
					double valorQQ = Double.parseDouble(qq.trim().replace(",", "."));
					ObjetoiVinedo vinedo = (ObjetoiVinedo) bp
							.createSQLQuery(
									"SELECT * FROM ObjetoiVinedo ov WHERE ov.credito_id = :credito_id ORDER BY ov.id")
							.addEntity(ObjetoiVinedo.class).setParameter("credito_id", id).setMaxResults(1)
							.uniqueResult();
					QQIngresados qqIngresados = new QQIngresados();
					qqIngresados.setVinedo(vinedo);
					qqIngresados.setFechaIngreso(new Date());
					qqIngresados.setBodegaGarantia(true);
					qqIngresados.setQqIngreso(valorQQ);
					bp.save(qqIngresados);
					vinedo.setTotalQQIngresados(vinedo.getTotalQQIngresados() + valorQQ);
					bp.update(vinedo);
					calcularPorcentajeQQ(vinedo.getCredito());
				}
			}
			bp.commit();
			actualizarListado();
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("registrarQQIngresados.guardarQQManual.error", "registrarQQIngresados.guardarQQManual.error");
		}
	}

	private void actualizarListado() {
		Long idPersona = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("RegistrarQQIngresados.persona.id");
		String expediente = (String) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("RegistrarQQIngresados.credito.expediente");
		seleccion = (Long[]) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("RegistrarQQIngresados.seleccion");
		if (idPersona != null) {
			persona = new Persona();
			persona.setId(idPersona);
			listarPorPersona();
		} else if (expediente != null) {
			credito = new Objetoi();
			credito.setExpediente(expediente);
			listarPorExpediente();
		} else {
			listarCreditos();
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private void consultaINV() {
		LoggerService logger = LoggerService.getInstance();
		String[] ids = SessionHandler.getCurrentSessionHandler().getRequest().getParameterValues("qqconsulta");
		if (ids == null) {
			return;
		}
		logger.log("debug", "Inicia consulta a INV para creditos " + Arrays.toString(ids));

		String urlString = DirectorHelper.getString("inv.consulta.ingreso.url");
		if (urlString == null || urlString.trim().isEmpty()) {
			errores.put("registrarQQIngresados.url", "registrarQQIngresados.url");
			return;
		}
		Query qVinedos = bp.createQuery("SELECT v FROM ObjetoiVinedo v WHERE v.credito.id = :id");
		Query qQuintales = bp.createQuery(
				"SELECT SUM(qq.qqIngreso) FROM QQIngresados qq WHERE qq.vinedo.id = :id AND qq.bodegaGarantia = :bodega");
		Query qGarantias = bp.createQuery(
				"SELECT g.cuit FROM ObjetoiGarantia og JOIN og.garantia g WHERE og.objetoi.id = :objetoi AND og.baja IS NULL");
		for (String id : ids) {
			try {
				Long idCredito = new Long(id);
				List<ObjetoiVinedo> vinedos = qVinedos.setParameter("id", idCredito).list();
				/**
				 * asumo que los creditos de cosecha tienen solo 1 garantia, si hay error lo
				 * captura el try-catch
				 */
				String cuitBodega = (String) qGarantias.setParameter("objetoi", idCredito).uniqueResult();
				logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega
						+ ", Cant.Vi�edos: " + vinedos.size());
				for (ObjetoiVinedo vinedo : vinedos) {
					String urlConsulta = urlString.replace("{cuitBodega}", cuitBodega).replace("{vinedo}",
							vinedo.getVinedo().getCodigo());
					HttpsURLConnection conn = HTTPHelper.getConexion(urlConsulta, false);
					int responseCode = conn.getResponseCode();
					logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega
							+ ", Vi�edo: " + vinedo.getVinedo().getCodigo() + ", url:" + urlConsulta);
					logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega
							+ ", Vi�edo: " + vinedo.getVinedo().getCodigo() + ", Resultado: " + responseCode);
					if (responseCode == HttpsURLConnection.HTTP_OK) {
						String responseText = IOUtils.toString(conn.getInputStream(), "utf-8");
						logger.log("debug",
								"Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega + ", Vi�edo: "
										+ vinedo.getVinedo().getCodigo() + ", Resultado: " + responseCode + ", "
										+ (responseText.isEmpty() ? "Sin resultado" : responseText));
						double kgInvBodega = 0;
						double kgInvOtros = 0;
						String[] registros = responseText.split("\\|");
						for (String registro : registros) {
							if (registro.trim().isEmpty()) {
								continue;
							}
							String[] datos = registro.split(",");
							if (datos.length > 1) {
								double kgBodega, kgTotal = 0;
								kgBodega = Double.parseDouble(datos[1]);
								kgTotal = Double.parseDouble(datos[2]);
								kgInvBodega += kgBodega;
								kgInvOtros += kgTotal - kgBodega;
							} else {
								continue;
							}

						}
						Number qqv = (Number) qQuintales.setParameter("id", vinedo.getId()).setParameter("bodega", true)
								.uniqueResult();
						double qqVinedoBodega = qqv != null ? qqv.doubleValue() : 0;
						qqv = (Number) qQuintales.setParameter("id", vinedo.getId()).setParameter("bodega", false)
								.uniqueResult();
						double qqVinedoOtros = qqv != null ? qqv.doubleValue() : 0;
						bp.begin();
						double ingresoBodega = kgInvBodega / 100 - qqVinedoBodega;
						if (ingresoBodega > 0) {
							QQIngresados qqIngresados = new QQIngresados();
							qqIngresados.setQqIngreso(ingresoBodega);
							qqIngresados.setVinedo(vinedo);
							qqIngresados.setFechaIngreso(new Date());
							qqIngresados.setBodegaGarantia(true);
							bp.save(qqIngresados);
							vinedo.setTotalQQIngresados(vinedo.getTotalQQIngresados() + ingresoBodega);
							bp.update(vinedo);
						}
						double ingresoOtros = kgInvOtros / 100 - qqVinedoOtros;
						if (ingresoOtros > 0) {
							QQIngresados qqIngresados = new QQIngresados();
							qqIngresados.setQqIngreso(ingresoOtros);
							qqIngresados.setVinedo(vinedo);
							qqIngresados.setFechaIngreso(new Date());
							qqIngresados.setBodegaGarantia(false);
							bp.save(qqIngresados);
						}
						bp.commit();
					}
				}
				calcularPorcentajeQQ((Objetoi) bp.getById(Objetoi.class, idCredito));
			} catch (NonUniqueResultException e) {
				errores.put("registrarQQIngresados.errorconsultainterna", "registrarQQIngresados.errorconsultainterna");
				e.printStackTrace();
			} catch (HibernateException e) {
				errores.put("registrarQQIngresados.errorconsultainterna", "registrarQQIngresados.errorconsultainterna");
				e.printStackTrace();
			} catch (IOException e) {
				errores.put("registrarQQIngresados.errorcomunicacion", "registrarQQIngresados.errorcomunicacion");
				e.printStackTrace();
			} catch (Exception e) {
				errores.put("registrarQQIngresados.errorformato", "registrarQQIngresados.errorformato");
				e.printStackTrace();
			}
		}
	}

	private void consultaINV1() {
		LoggerService logger = LoggerService.getInstance();

		String[] ids = SessionHandler.getCurrentSessionHandler().getRequest().getParameterValues("qqconsulta");
		if (ids == null) {
			return;
		}
		logger.log("debug", "Inicia consulta a INV para creditos " + Arrays.toString(ids));
		for (String id : ids) {
			try {
				CreditoInvHelper.consultaINV(id);
			} catch (NonUniqueResultException e) {
				errores.put("registrarQQIngresados.errorconsultainterna", "registrarQQIngresados.errorconsultainterna");
				e.printStackTrace();
			} catch (HibernateException e) {
				errores.put("registrarQQIngresados.errorconsultainterna", "registrarQQIngresados.errorconsultainterna");
				e.printStackTrace();
			} catch (IOException e) {
				errores.put("registrarQQIngresados.errorcomunicacion", "registrarQQIngresados.errorcomunicacion");
				e.printStackTrace();
			} catch (NullPointerException e) {
				errores.put("registrarQQIngresados.url", "registrarQQIngresados.url");
				e.printStackTrace();
			} catch (Exception e) {
				errores.put("registrarQQIngresados.errorformato", "registrarQQIngresados.errorformato");
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void listar(Query query) {
		try {
			List<BigDecimal> creditos_id = (List<BigDecimal>) query.list();
			if (!creditos_id.isEmpty()) {
				creditos = bp
						.createSQLQuery(
								"SELECT DISTINCT * FROM Objetoi o WHERE o.id IN(:creditos_id) ORDER BY o.expediente")
						.addEntity(Objetoi.class).setParameterList("creditos_id", creditos_id).list();
			} else {
				creditos = null;
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosRegistrarQQIngresados", creditos);
	}

	private String armarConsulta(Long idPersona, String expediente) {
		String lineasIds = Arrays.toString(seleccion).replace("[", "").replace("]", "");
//		String consulta = "SELECT O.ID FROM Objetoi O "
//				+ "INNER JOIN Desembolso D ON D.credito_id = O.id AND D.estado IN('1', '2') "
//				+ "INNER JOIN ObjetoiVinedo OV ON OV.credito_id = O.id "
//				+ "INNER JOIN ObjetoiGarantia OG ON OG.objetoi_id = O.id AND OG.baja IS NULL "
//				+ "INNER JOIN Garantia G ON OG.garantia_id = G.id " + "INNER JOIN TipoGarantia T on T.id = G.TIPO_ID "
//				+ "LEFT JOIN QQIngresados Q ON Q.vinedo_id = OV.id AND Q.bodegaGarantia = 1 "
//				+ "WHERE (T.calculaQQSinVinedo is null OR T.calculaQQSinVinedo = 0) AND (O.autorizadoQQManual is null OR O.autorizadoQQManual = 0) "
//				+ "AND O.linea_id in (" + lineasIds + ") "
//				+ (idPersona == null ? "" : ("AND O.persona_IDPERSONA = " + idPersona + " "))
//				+ (expediente == null ? "" : ("AND O.expediente = '" + expediente + "' "))
//				+ "GROUP BY O.qqFinal, O.id HAVING SUM(CASE WHEN Q.qqIngreso IS NULL THEN 0 ELSE Q.qqIngreso END) < O.qqFinal * 0.7";
		String consultaSimple = "SELECT o.id FROM Objetoi o INNER JOIN Desembolso d ON d.credito_id = o.id "
				+ "INNER JOIN ObjetoiEstado oe ON oe.objetoi_id = o.id "
				+ "INNER JOIN Estado e ON e.idEstado = oe.estado_idEstado WHERE o.linea_id IN (" + lineasIds + ") "
				+ "AND o.destino = 'crdc' AND d.estado = 4 AND oe.fechaHasta IS NULL AND e.nombreEstado = 'EJECUCION' "
				+ (idPersona == null ? "" : ("AND O.persona_IDPERSONA = " + idPersona + " "))
				+ (expediente == null ? "" : ("AND O.expediente = '" + expediente + "' "));
		return consultaSimple;
	}

	public void listarCreditos() {
		if (seleccion == null) {
			errores.put("registrarQQIngresados.seleccion", "registrarQQIngresados.seleccion");
			return;
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.persona.id", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.credito.expediente", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.seleccion", seleccion);
		String consulta = armarConsulta(null, null);
		listar(bp.createSQLQuery(consulta));
	}

	public void listarPorPersona() {
		if (seleccion == null) {
			errores.put("registrarQQIngresados.seleccion", "registrarQQIngresados.seleccion");
			return;
		}
		persona = (Persona) bp.getById(Persona.class, persona.getId());
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.persona.id", persona.getId());
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.credito.expediente", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.seleccion", seleccion);
		String consulta = armarConsulta(persona.getId(), null);
		listar(bp.createSQLQuery(consulta));
	}

	public void listarPorExpediente() {
		if (seleccion == null) {
			errores.put("registrarQQIngresados.seleccion", "registrarQQIngresados.seleccion");
			return;
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.persona.id", null);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.credito.expediente", credito.getExpediente());
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("RegistrarQQIngresados.seleccion", seleccion);
		String consulta = armarConsulta(null, credito.getExpediente());
		listar(bp.createSQLQuery(consulta));
	}

	@SuppressWarnings("unchecked")
	@Deprecated
	public void ingresar() {
		credito = (Objetoi) bp.getById(Objetoi.class, credito.getId());
		vinedo = (ObjetoiVinedo) bp
				.createSQLQuery("SELECT * FROM ObjetoiVinedo ov WHERE ov.credito_id = :credito_id ORDER BY ov.id")
				.addEntity(ObjetoiVinedo.class).setParameter("credito_id", credito.getId()).setMaxResults(1)
				.uniqueResult();
		Double total = new Double(0D);
		total = (Double) bp.getNamedQuery("QQIngresados.SumQQIngresoByVinedo").setEntity("vinedo", vinedo)
				.uniqueResult();
		if (total == null) {
			vinedo.setTotalQQIngresados(total);
		}
		// garantia
		List<Garantia> garantias = bp
				.createQuery("SELECT g.garantia from ObjetoiGarantia g where g.objetoi = :credito AND g.baja IS NULL")
				.setEntity("credito", vinedo.getCredito()).list();
		if (garantias.isEmpty()) {
			vinedo.setGarantia(new Garantia());
		} else {
			vinedo.setGarantia(garantias.get(0));
		}
		ingresar = true;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("RegistrarQQIngresados.vinedo",
				vinedo);
	}

	public void registrarIngreso() {
		persona.setId(0L);
		vinedo = (ObjetoiVinedo) bp
				.createSQLQuery("SELECT * FROM ObjetoiVinedo ov WHERE ov.credito_id = :credito_id ORDER BY ov.id")
				.addEntity(ObjetoiVinedo.class).setParameter("credito_id", credito.getId()).setMaxResults(1)
				.uniqueResult();
		Double ingreso;
		try {
			Double ingresadoALaFecha = new Double(
					qqIngresoStr.replace(',', '.').replace('Q', ' ').replace('q', ' ').trim());
			ingreso = ingresadoALaFecha - vinedo.getTotalQQIngresados();
			if (ingreso <= 0) {
				errores.put("registrarQQIngresados.ingresadoALaFecha", "registrarQQIngresados.ingresadoALaFecha");
				// ingresar = true;
				qqIngresoStr = null;
				return;
			}
		} catch (NumberFormatException e) {
			errores.put("registrarQQIngresados.qqIngreso", "registrarQQIngresados.qqIngreso");
			// ingresar = true;
			qqIngresoStr = null;
			return;
		}
		qqIngresados = new QQIngresados();
		qqIngresados.setQqIngreso(ingreso);
		qqIngresados.setVinedo(vinedo);
		qqIngresados.setFechaIngreso(new Date());
		bp.begin();
		bp.save(qqIngresados);
		vinedo.setTotalQQIngresados(vinedo.getTotalQQIngresados() + ingreso);
		bp.update(vinedo);
		calcularPorcentajeQQ(vinedo.getCredito());
		bp.commit();
		ingresar = false;
		ingresarQQ = true;
		qqIngresoStr = null;
	}

	private void calcularPorcentajeQQ(Objetoi credito) {
		Float porcCosecha = credito.getPorcentajeCosecha();
		double financiado = credito.getQqFinal();
		double ingresadoTotal = credito.getQqIngresadosTotales();
		double ingresadoBodega = credito.getQqIngresadosGarantia();
		boolean condicion1 = ingresadoTotal >= (financiado * 70 / 100);
		boolean condicion2 = (credito.getTipoGarantia() != 2)
				|| (credito.getTipoGarantia() == 2 && ingresadoBodega >= (financiado * porcCosecha / 100));
		if (condicion1 && condicion2) {
			bp.begin();
			credito.setAutorizadoQQManual(true);
			bp.commit();
		}
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<ObjetoiVinedo> getVinedos() {
		return vinedos;
	}

	public void setVinedos(List<ObjetoiVinedo> vinedos) {
		this.vinedos = vinedos;
	}

	public ObjetoiVinedo getVinedo() {
		return vinedo;
	}

	public void setVinedo(ObjetoiVinedo vinedo) {
		this.vinedo = vinedo;
	}

	public boolean isIngresar() {
		return ingresar;
	}

	public void setIngresar(boolean ingresar) {
		this.ingresar = ingresar;
	}

	public QQIngresados getQqIngresados() {
		return qqIngresados;
	}

	public void setQqIngresados(QQIngresados qqIngresados) {
		this.qqIngresados = qqIngresados;
	}

	public String getQqIngresoStr() {
		return qqIngresoStr;
	}

	public void setQqIngresoStr(String qqIngresoStr) {
		this.qqIngresoStr = qqIngresoStr;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public boolean isIngresarQQ() {
		return ingresarQQ;
	}

	public void setIngresarQQ(boolean ingresarQQ) {
		this.ingresarQQ = ingresarQQ;
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public String getCiv() {
		return civ;
	}

	public void setCiv(String civ) {
		this.civ = civ;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getQqIngresoId() {
		return qqIngresoId;
	}

	public void setQqIngresoId(String qqIngresoId) {
		this.qqIngresoId = qqIngresoId;
	}

	public Long[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(Long[] seleccion) {
		this.seleccion = seleccion;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public String getAutorizacionId() {
		return autorizacionId;
	}

	public void setAutorizacionId(String autorizacionId) {
		this.autorizacionId = autorizacionId;
	}
}
