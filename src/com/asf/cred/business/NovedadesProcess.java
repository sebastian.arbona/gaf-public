package com.asf.cred.business;

import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;

public class NovedadesProcess extends ConversationProcess {

	@Save
	private Long idObjetoi;
	private List<NovedadCtaCte> novedades;
	private NovedadCtaCte novedad = new NovedadCtaCte();

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean listar() {
		if (idObjetoi == null) {
			return true;
		}
		novedades = bp.createQuery(
				"select n from NovedadCtaCte n where n.credito.id = :id and n.detalleFactura is null order by n.fecha")
				.setParameter("id", idObjetoi).list();
		forward = "NovedadList";
		return true;
	}

	@ProcessMethod
	public boolean nuevo() {
		novedad.setFecha(new Date());
		forward = "Novedad";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardar() {
		forward = "Novedad";

		if (novedad.getConcepto() == null) {
			errors.put("novedades.concepto", "novedades.concepto");
			return false;
		}

		if (novedad.getFecha() == null) {
			errors.put("novedades.fecha", "novedades.fecha");
			return false;
		}

		if (novedad.getImporte() <= 0) {
			errors.put("novedades.importe", "novedades.importe");
			return false;
		}

		List<Cuota> cuotas = bp
				.createQuery("select c from Cuota c where c.credito.id = :id order by c.fechaVencimiento")
				.setParameter("id", idObjetoi).list();
		boolean vencimientoPosterior = false;
		if (!cuotas.isEmpty()) {
			for (Cuota c : cuotas) {
				if (c.getEmision() == null && c.getFechaVencimiento().getTime() >= novedad.getFecha().getTime()) {
					vencimientoPosterior = true;
				}
			}
		} else {
			vencimientoPosterior = true;
		}

		if (!vencimientoPosterior) {
			errors.put("novedades.vencimiento", "novedades.vencimiento");
			return false;
		}

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		novedad.setCredito(credito);
		novedad.setImpactado(false);

		bp.save(novedad);

		novedad = new NovedadCtaCte();

		return listar();
	}

	@ProcessMethod
	public boolean eliminar() {
		if (novedad.getId() == null || novedad.getId() == 0L) {
			return false;
		}

		NovedadCtaCte n = (NovedadCtaCte) bp.getById(NovedadCtaCte.class, novedad.getId());
		if (n.getMovimientoDeb() != null || n.getMovimientoCred() != null) {
			errors.put("novedad.impactado", "novedad.impactado");
			return false;
		}
		if (n.getDetalleFactura() != null) {
			errors.put("novedades.detalleFactura", "novedades.detalleFactura");
			return false;
		}

		bp.delete(n);
		return listar();
	}

	@Override
	protected String getDefaultForward() {
		return "NovedadList";
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public List<NovedadCtaCte> getNovedades() {
		return novedades;
	}

	public NovedadCtaCte getNovedad() {
		return novedad;
	}

	public void setNovedad(NovedadCtaCte novedad) {
		this.novedad = novedad;
	}

}
