package com.asf.cred.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.IOUtils;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.BeanCompensatorio;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CierreMP extends ConversationProcess {

	@Save
	private List<CierreMPBean> listado;
	private Date fechaValor;
	private int diasLimite;
	private String[] estados;
	private Long periodo;
	private List<Estado> estadosList;
	private boolean success;

	private static String sql;

	@SuppressWarnings("unchecked")
	public CierreMP() {
		estadosList = bp.createQuery("select e from Estado e where tipo = 'Objetoi' order by e.nombreEstado").list();

		periodo = (long) Calendar.getInstance().get(Calendar.YEAR);
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean listar() {
		if (estados == null || estados.length == 0) {
			errors.put("cierremp.estados", "cierremp.estados");
			return false;
		}
		if (fechaValor == null) {
			errors.put("cierremp.fechaValor", "cierremp.fechaValor");
			return false;
		}

		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date fechaProceso = c.getTime();

		String sql = getSql().replace("@estados", getArrayString(estados));
		List<Object[]> rows = bp.createSQLQuery(sql).setDate("fechaCalculo", fechaValor)
				.setDate("fechaProceso", fechaProceso).list();

		CreditoHandler ch = new CreditoHandler();
		listado = new ArrayList<CierreMPBean>();

		for (Object[] r : rows) {
			CierreMPBean b = new CierreMPBean();
			b.setFechaCalculo(fechaValor);
			b.setId(((Number) r[0]).longValue());
			b.setNumeroAtencion(((Number) r[1]).longValue());
			b.setTitular((String) r[2]);
			b.setLinea((String) r[3]);

			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, b.getId());

			b.setPrimerVencimientoImpago(credito.getFechaUltimoVencimientoDate());

			if (b.getPrimerVencimientoImpago() == null) {
				continue;
			}

			b.setDiasPrimerVencimientoImpago(
					(int) DateHelper.getDiffDates(b.getPrimerVencimientoImpago(), new Date(), 2));

			Map<Long, CierreMPBean.Interes> interesesPorCuota = new TreeMap<Long, CierreMPBean.Interes>();
			List<BeanCompensatorio> moratorio = ch.calcularMoratorio(credito, fechaValor, diasLimite);
			List<BeanCompensatorio> punitorio = ch.calcularPunitorio(credito, fechaValor, diasLimite);
			b.setDetalleMoratorio(moratorio);
			b.setDetallePunitorio(punitorio);
			if (b.getMoratorio() < 0.005 && b.getPunitorio() < 0.005) {
				continue;
			}
			for (BeanCompensatorio m : moratorio) {
				CierreMPBean.Interes interes = interesesPorCuota.get(m.getCuota().getId());
				if (interes == null) {
					interes = new CierreMPBean.Interes();
					interesesPorCuota.put(m.getCuota().getId(), interes);
				}
				interes.setMoratorio(m);
			}

			for (BeanCompensatorio p : punitorio) {
				CierreMPBean.Interes interes = interesesPorCuota.get(p.getCuota().getId());
				if (interes == null) {
					interes = new CierreMPBean.Interes();
					interesesPorCuota.put(p.getCuota().getId(), interes);
				}

				interes.setPunitorio(p);
			}

			b.setInteresesPorCuota(interesesPorCuota);

			listado.add(b);
		}

		return true;
	}

	@ProcessMethod
	public boolean guardar() {
		if (periodo == null || periodo == 0L) {
			errors.put("cierremp.periodo", "cierremp.periodo");
			return false;
		}

		bp.begin();

		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		Concepto concMoratorio = Concepto.buscarConcepto(Concepto.CONCEPTO_MORATORIO, periodo.intValue());
		Concepto concPunitorio = Concepto.buscarConcepto(Concepto.CONCEPTO_PUNITORIO, periodo.intValue());
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
				.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		long itemCtaCte = 1;

		for (CierreMPBean b : listado) {
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, b.getId());

			Map<Long, CierreMPBean.Interes> intereses = b.getInteresesPorCuota();
			for (Long idCuota : intereses.keySet()) {
				Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);

				CierreMPBean.Interes interes = intereses.get(idCuota);

				if (interes.getMoratorio() == null && interes.getPunitorio() == null) {
					continue;
				}

				Boleto nd = new Boleto();
				nd.setFechaEmision(b.getFechaCalculo());
				nd.setFechaVencimiento(b.getFechaCalculo());

				nd.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				nd.setUsuario(usuario);
				nd.setNumeroCuota(cuota.getNumero());
				nd.setObjetoi(credito);
				nd.setImporte(0.0);

				nd.setPeriodoBoleto(periodo);
				nd.setNumeroBoleto(Numerador.getNext(nd.getNumerador()));
				nd.setVerificadorBoleto(0L);

				bp.save(nd);

				if (interes.getMoratorio() != null) {
					Ctacte cc = credito.crearCtacte(Concepto.CONCEPTO_MORATORIO, interes.getMoratorio().getMonto(),
							interes.getMoratorio().getHastaPeriodo(), nd, credito, cuota,
							Ctacte.TIPO_CONCEPTO_DEVENGAMIENTO_CIERRE_MP);
					cc.setDetalle("Int. Moratorio");
					cc.setTipomov(tipoDebito);
					cc.setFacturado(concMoratorio);
					cc.setAsociado(concMoratorio);

					CtacteKey ck = cc.getId();
					ck.setPeriodoCtacte(periodo);
					ck.setMovimientoCtacte(movimientoCtaCte);
					ck.setItemCtacte(itemCtaCte++);

					bp.save(cc);

					Bolcon bolcon = new Bolcon();
					bolcon.setFacturado(concMoratorio);
					bolcon.setOriginal(concMoratorio);
					double importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolcon.setImporte(importeRedondeado);
					bolcon.setCuota(cuota);

					nd.setImporte(nd.getImporte() + importeRedondeado);
					bp.update(nd);

					BolconKey bck = new BolconKey();
					bck.setBoleto(nd);
					bck.setPeriodoCtacte(ck.getPeriodoCtacte());
					bck.setMovimientoCtacte(ck.getMovimientoCtacte());
					bck.setVerificadorCtacte(ck.getVerificadorCtacte());
					bck.setItemCtacte(ck.getItemCtacte());
					bolcon.setId(bck);
					bolcon.setTipomov(tipoDebito);
					bp.save(bolcon);

					cc.setBoleto(nd);
					cc.setFechaGeneracion(interes.getMoratorio().getHastaPeriodo());
					cc.setUsuario(usuario);
					cc.setFechaVencimiento(interes.getMoratorio().getHastaPeriodo());
					bp.update(cc);
				}

				if (interes.getPunitorio() != null) {
					Ctacte cc = credito.crearCtacte(Concepto.CONCEPTO_PUNITORIO, interes.getPunitorio().getMonto(),
							interes.getPunitorio().getHastaPeriodo(), nd, credito, cuota,
							Ctacte.TIPO_CONCEPTO_DEVENGAMIENTO_CIERRE_MP);
					cc.setDetalle("Int. Punitorio");
					cc.setTipomov(tipoDebito);
					cc.setFacturado(concPunitorio);
					cc.setAsociado(concPunitorio);

					CtacteKey ck = cc.getId();
					ck.setPeriodoCtacte(periodo);
					ck.setMovimientoCtacte(movimientoCtaCte);
					ck.setItemCtacte(itemCtaCte++);

					bp.save(cc);

					Bolcon bolcon = new Bolcon();
					bolcon.setFacturado(concPunitorio);
					bolcon.setOriginal(concPunitorio);
					double importeRedondeado = cc.getImporte();
					importeRedondeado = Math.rint(importeRedondeado * 100) / 100;
					bolcon.setImporte(importeRedondeado);
					bolcon.setCuota(cuota);

					nd.setImporte(nd.getImporte() + importeRedondeado);
					bp.update(nd);

					BolconKey bck = new BolconKey();
					bck.setBoleto(nd);
					bck.setPeriodoCtacte(ck.getPeriodoCtacte());
					bck.setMovimientoCtacte(ck.getMovimientoCtacte());
					bck.setVerificadorCtacte(ck.getVerificadorCtacte());
					bck.setItemCtacte(ck.getItemCtacte());
					bolcon.setId(bck);
					bolcon.setTipomov(tipoDebito);
					bp.save(bolcon);

					cc.setBoleto(nd);
					cc.setFechaGeneracion(interes.getPunitorio().getHastaPeriodo());
					cc.setUsuario(usuario);
					cc.setFechaVencimiento(interes.getPunitorio().getHastaPeriodo());
					bp.update(cc);
				}
			}

			bp.getCurrentSession().flush();
		}

		bp.commit();

		listado = null;

		success = true;

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "CierreMP";
	}

	private static String getArrayString(String[] array) {
		if (array == null || array.length == 0) {
			return "";
		}

		String r = "";
		for (String s : array) {

			r += s + ",";
		}
		r = r.substring(0, r.length() - 1);
		return r;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(CierreMP.class.getResourceAsStream("cierremp.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	public void setFechaValor(String f) {
		fechaValor = DateHelper.getDate(f);
	}

	public String getFechaValor() {
		return DateHelper.getString(fechaValor);
	}

	public List<CierreMPBean> getListado() {
		return listado;
	}

	public String[] getEstados() {
		return estados;
	}

	public void setEstados(String[] estados) {
		this.estados = estados;
	}

	public List<Estado> getEstadosList() {
		return estadosList;
	}

	public Long getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Long periodo) {
		this.periodo = periodo;
	}

	public boolean isSuccess() {
		return success;
	}

	public int getDiasLimite() {
		return diasLimite;
	}

	public void setDiasLimite(int diasLimite) {
		this.diasLimite = diasLimite;
	}

}
