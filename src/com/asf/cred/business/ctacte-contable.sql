SELECT fechaGeneracion, min(periodoCtacte) AS periodo, tipo, numeroBoleto,  
idBoleto, tipomov_id, tipoMovimiento, detalle, sum(debe) AS debe, sum(haber) AS haber, 
min(movimientoCtacte) AS movimiento, min(verificadorCtacte) AS verificador, min(itemCtacte) AS item, 
caratula_id, boletoReciboId, idObjetoi, MAX(fechaProceso) as fechaProceso, fechaVencimiento, 
cotizaMov, sum(debePesos) AS debePesos, sum(haberPesos) AS haberPesos 
FROM  
(SELECT 
case when cc.emideta_id is not null then null else cc.fechaGeneracion end as fechaGeneracion,
cc.periodoCtacte, bol.tipo, bol.numeroBoleto,  
bol.id as idBoleto, cc.tipomov_id, conc.concepto_concepto, cc.tipoMovimiento, cc.fechaProceso, 
CASE WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'cap' AND (cc.tipoMovimiento = 'cuota' or cc.tipoMovimiento = 'movManualDeb') THEN cc.fechaGeneracion  
WHEN cc.tipomov_id = 1 AND conc.concepto_concepto = 'cap' AND cc.tipoMovimiento = 'movManualCred' THEN cc.fechaGeneracion  
	ELSE cc.fechaVencimiento END AS fechaVencimiento,  
CASE WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'cap' AND cc.tipoMovimiento = 'cuota' AND cc.desembolso_id IS NOT NULL THEN @DESEMBOLSO_EXPR 
    WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'com' AND cc.tipoMovimiento = 'cuota' THEN 'Int Compensatorio' 
    WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'gas' AND cc.tipoMovimiento = 'cuota' THEN 'Gastos'  
    WHEN cc.tipomov_id = 2 AND conc.concepto_concepto = 'rec' AND cc.tipoMovimiento = 'cuota' THEN 'Gastos a Recuperar'  
    WHEN cc.tipomov_id = 2 AND (conc.concepto_concepto = 'mor' OR conc.concepto_concepto = 'pun') AND cc.tipoMovimiento = 'cuota' THEN 'Int Moratorio y Punitorio'    
    WHEN cc.tipomov_id = 1 AND conc.concepto_concepto = 'bon' AND cc.tipoMovimiento = 'bonificacion' THEN cc.detalle 
    WHEN cc.tipomov_id = 1 AND (conc.concepto_concepto = 'gas' AND cc.tipoMovimiento = 'cuota' 
		OR conc.concepto_concepto = 'rec' AND cc.tipoMovimiento = 'cuota') THEN 'Pago Gastos'  
    WHEN cc.tipomov_id = 1 AND (conc.concepto_concepto = 'gas' OR conc.concepto_concepto = 'rec') AND 
		cc.caratula_id is null AND cc.tipoMovimiento like 'pago' THEN 'Pago Gastos' 
    WHEN cc.tipomov_id = 1 AND cc.tipoMovimiento = 'pago' THEN 'Pago' 
    ELSE cc.detalle END AS detalle,  
CASE cc.tipomov_id when 2 THEN cc.importe else 0 end as debe,  
CASE cc.tipomov_id when 1 THEN cc.importe else 0 end as haber,  
CASE conc.concepto_concepto when 'cap' then 1  
	WHEN 'com' then 2  
	WHEN 'gas' then 2  
	WHEN 'mul' then 2 
	WHEN 'bon' then 3  
	WHEN 'mor' then 4  
	WHEN 'pun' then 4  
	ELSE 5 END AS orden,  
cc.movimientoCtacte, cc.verificadorCtacte, cc.itemCtacte, p.caratula_id, p.boleto_id as boletoReciboId, cc.objetoi_id as idObjetoi, 
CASE cc.DTYPE WHEN 'CtaCteAjuste' THEN cc.cotizacionDiferencia 
	ELSE cc.cotizaMov END AS cotizaMov, 
CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.debePesos 
	WHEN cc.tipomov_id = 2 THEN cc.importe * cc.cotizaMov 
	ELSE 0 END AS debePesos, 
CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.haberPesos 
	WHEN cc.tipomov_id = 1 THEN cc.importe * cc.cotizaMov 
	ELSE 0 END AS haberPesos  
FROM Ctacte cc LEFT JOIN Boleto bol ON cc.boleto_id = bol.id LEFT JOIN Pagos p ON p.recibo_id = bol.id, Concepto  
conc, CConcepto cconc  
WHERE cc.objetoi_id = ? and  cc.asociado_id = conc.id and conc.concepto_concepto = cconc.concepto 
) t  
GROUP BY fechaGeneracion, tipo, numeroBoleto,  
idBoleto, tipomov_id,  tipoMovimiento, detalle, caratula_id, boletoReciboId, idObjetoi, fechaVencimiento, cotizaMov  
ORDER BY fechaProceso, movimiento, item