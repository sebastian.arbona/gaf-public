package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.action.ActionMessage;

import com.asf.cred.security.SessionHandler;
import com.asf.hibernate.mapping.Tipificadores;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Leyenda;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;

public class NotificacionProcess implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	private Long idCredito;
	private Date fechaCreada;
	private Date fechaVenc;
	private boolean terceros;
	private boolean mail;
	private boolean emitir;
	private String observaciones;
	private Long idTipoAviso;
	private Long idTipoNotificacion;
	private Tipificadores tipoAviso;
	private Tipificadores tipoNotificacion;
	private List<Notificacion> notificaciones;
	private Long idReporte;
	protected HashMap<String, Object> errores = new HashMap<String, Object>();
	private ReportResult reporte;
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	private boolean pestania;
	private Long idNoti;
	private Objetoi credito;
	private Long idPersona;

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion == null) {
			credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
			notificaciones = bp.createQuery("Select n from Notificacion n where n.credito=:credito")
					.setParameter("credito", credito).list();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("NotificacionProcess.pestania", false);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("NotificacionProcess.idCredito", idCredito);
			forward = "HistorialNotificaciones";
		} else if (accion.equalsIgnoreCase("ver")) {
			pestania = (Boolean) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("NotificacionProcess.pestania");
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idNoti", idReporte);
			if (pestania) {
				forward = "ReporteBoletoCred";
			} else {
				forward = "ReporteBoleto";
			}
		} else if (accion.equalsIgnoreCase("verNotif")) {
			if (idCredito == null || idCredito == 0) {
				idCredito = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("NotificacionProcess.idCredito");
			}
			credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
			notificaciones = bp.createQuery("Select n from Notificacion n where n.credito=:credito")
					.setParameter("credito", credito).list();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("NotificacionProcess.idCredito", idCredito);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("NotificacionProcess.pestania", pestania);
			forward = "HistorialNotificacionesCred";
		} else if (accion.equalsIgnoreCase("verNotifPersona")) {
			if (idPersona == null || idPersona == 0) {
				idPersona = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("NotificacionProcess.idPersona");
			}
			Persona persona = (Persona) bp.getById(Persona.class, idPersona);
			notificaciones = bp.createQuery("Select n from Notificacion n where n.persona=:persona")
					.setParameter("persona", persona).list();

			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("NotificacionProcess.idPersona", idPersona);
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("NotificacionProcess.pestania", pestania);

			forward = "HistorialNotificacionesPer";

		} else if (accion.equalsIgnoreCase("listarVolver")) {
			if (idCredito == null || idCredito == 0) {
				idCredito = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
						.getAttribute("NotificacionProcess.idCredito");
			}
			// las siguientes lineas son para ver si el llamado viene desde la pestania o
			// no, y dependiendo de eso es el jsp que se muestra
			pestania = (Boolean) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("NotificacionProcess.pestania");
			if (pestania) {
				credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
				notificaciones = bp.createQuery("Select n from Notificacion n where n.credito=:credito")
						.setParameter("credito", credito).list();
				forward = "HistorialNotificacionesCred";
			} else {
				accion = null;
				this.doProcess();
			}
		}
		return true;
	}

	// Metodos privados

	public void generarReporte(Notificacion noti) {

		if (noti.getFechaVenc() == null) {
			errores.put("creditoCuentaCorriente.vencimiento", "creditoCuentaCorriente.vencimiento");
			return;
		}
		try {
			Leyenda leyenda = (Leyenda) bp.createQuery("Select l from Leyenda l where l.nombre =:nombre")
					.setParameter("nombre", "Notificacion de Mora").uniqueResult();
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("boletoTotalDeuda.jasper");
			reporte.setParams("LEYENDA=" + leyenda + ";VENCIMIENTO=" + noti.getFechaVenc() + ";OBJETOI_ID="
					+ noti.getCredito().getId() + ";ID_PERSONA=" + noti.getCredito().getPersona_id() + ";MONEDA="
					+ noti.getCredito().getLinea().getMoneda().getDenominacion() + ";USUARIO="
					+ SessionHandler.getCurrentSessionHandler().getCurrentUser() + ";SCHEMA="
					+ BusinessPersistance.getSchema() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletoTotalDeuda"));
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.idObjetoi",
				noti.getCredito().getId());
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("creditoCC.personaId",
				noti.getCredito().getPersona_id());
	}

	private String buscarCodigo(Long idTipo) {
		Tipificadores tipo = (Tipificadores) bp.getById(Tipificadores.class, idTipo);
		return tipo.getCodigo();
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public Date getFechaCreada() {
		return fechaCreada;
	}

	public void setFechaCreada(Date fechaCreada) {
		this.fechaCreada = fechaCreada;
	}

	public Date getFechaVenc() {
		return fechaVenc;
	}

	public void setFechaVenc(Date fechaVenc) {
		this.fechaVenc = fechaVenc;
	}

	public boolean isTerceros() {
		return terceros;
	}

	public void setTerceros(boolean terceros) {
		this.terceros = terceros;
	}

	public boolean isMail() {
		return mail;
	}

	public void setMail(boolean mail) {
		this.mail = mail;
	}

	public boolean isEmitir() {
		return emitir;
	}

	public void setEmitir(boolean emitir) {
		this.emitir = emitir;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getIdTipoAviso() {
		return idTipoAviso;
	}

	public void setIdTipoAviso(Long idTipoAviso) {
		this.idTipoAviso = idTipoAviso;
	}

	public Long getIdTipoNotificacion() {
		return idTipoNotificacion;
	}

	public void setIdTipoNotificacion(Long idTipoNotificacion) {
		this.idTipoNotificacion = idTipoNotificacion;
	}

	public List<Notificacion> getNotificaciones() {
		return notificaciones;
	}

	public void setNotificaciones(List<Notificacion> notificaciones) {
		this.notificaciones = notificaciones;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Tipificadores getTipoAviso() {
		return tipoAviso;
	}

	public void setTipoAviso(Tipificadores tipoAviso) {
		this.tipoAviso = tipoAviso;
	}

	public Tipificadores getTipoNotificacion() {
		return tipoNotificacion;
	}

	public void setTipoNotificacion(Tipificadores tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}

	public Long getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}

	public ReportResult getReporte() {
		return reporte;
	}

	public void setReporte(ReportResult reporte) {
		this.reporte = reporte;
	}

	public boolean ispestania() {
		return pestania;
	}

	public void setpestania(boolean pestania) {
		this.pestania = pestania;
	}

	public Long getIdNoti() {
		return idNoti;
	}

	public void setIdNoti(Long idNoti) {
		this.idNoti = idNoti;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
}
