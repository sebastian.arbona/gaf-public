package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;

import com.asf.cred.MessageBean;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Localidad;
import com.civitas.hibernate.persona.Nacionalidad;
import com.nirven.creditos.hibernate.Turno;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class AtencionTurnos implements IProcess {
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "AtencionTurno";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Turno turno = new Turno();

	private Usuario currentUser;
	private List<Turno> turnos;
	private Long idTurno;
	private String hora;
	private String horaAtencion;
	private Boolean imprimir;
	private MessageBean message;
	private String redirect = null;
	private Long idNacionalidad;
	private Long idLocalidad = null;
	private Long numeroatencion;
	private String unidad;

	private Long cantidadTurnos = null;

	// =======================CONSTRUCTORES=========================
	public AtencionTurnos() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		String idUsuario = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		this.currentUser = (Usuario) bp.getById(com.nirven.creditos.hibernate.Usuario.class, idUsuario);
	}

	// ======================FUNCIONALIDADES========================

	@Override
	public boolean doProcess() {
		if (accion == null || accion.isEmpty()) {
			try {
				this.cargarCantidadTurnos();
			} catch (NoResultException nre) {
			} catch (NonUniqueResultException nue) {
			}
		} else if (this.accion.equals("nuevo")) {
			this.forward = "AtencionSolicitudTurno";
		} else if (this.accion.equals("load")) {
			this.forward = "AtencionSolicitudTurno";
			this.listarTurnos();
		} else if (this.accion.equals("guardar")) {
			this.forward = "AtencionTurno";
			this.guardarTurno();
			this.cargarCantidadTurnos();
			this.turno = new Turno();
		} else if (this.accion.equals("solicitud")) {
			this.forward = "AtencionTurno";
			this.guardarTurno();
			this.redirect = this.turno.getId().toString();
		} else if (this.accion.equals("cancelarload")) {
			this.cancelar();
			this.cargarCantidadTurnos();
			this.forward = "AtencionTurno";
		} else if (this.accion.equals("cancelarbuscar")) {
			this.cargarCantidadTurnos();
			this.forward = "AtencionTurno";
		} else if (this.accion.equals("buscar")) {
			this.cargarCantidadTurnos();
			this.buscarTurno();
		}
		return this.errores.isEmpty();
	}

	private void buscarTurno() {
		turno = (Turno) bp.createQuery("select t from Turno t where t.numeroAtencion = :numeroAtencion")
				.setLong("numeroAtencion", numeroatencion).uniqueResult();
		if (turno == null) {
			this.forward = "AtencionTurno";
			numeroatencion = null;
			this.errores.put("AtencionTurnos.error.nofound", "AtencionTurnos.error.nofound");
			return;
		}
		SimpleDateFormat formatoHora = new SimpleDateFormat("HH");
		SimpleDateFormat formatoMinuto = new SimpleDateFormat("mm");
		hora = formatoHora.format(turno.getLlegada()) + ":" + formatoMinuto.format(turno.getLlegada());
		if (turno.getNacionalidad() != null) {
			idNacionalidad = turno.getNacionalidad().getId();
		}
		if (turno.getLocalidad() != null) {
			idLocalidad = turno.getLocalidad().getId();
		}
		this.forward = "AtencionSolicitudTurno";
	}

	private void cargarCantidadTurnos() {
		this.cantidadTurnos = (Long) bp
				.createQuery("select count(t) from Turno t "
						+ "where t.unidad.id = :idUnidad and t.atencion is null and t.estado = :estado")
				.setLong("idUnidad",
						((Usuario) bp.getById(com.nirven.creditos.hibernate.Usuario.class,
								SessionHandler.getCurrentSessionHandler().getCurrentUser())).getUnidad().getId())
				.setString("estado", "Pendiente").uniqueResult();
	}

	@SuppressWarnings("unchecked")
	private synchronized void listarTurnos() {
		String consulta = "SELECT e FROM Turno e WHERE e.atencion IS NULL AND e.unidad.id = "
				+ currentUser.getUnidad().getId() + " ORDER BY e.llegada ASC";
		this.turnos = this.bp.getByFilter(consulta);
		if (!this.turnos.isEmpty()) {
			this.turno = this.turnos.get(0);
			this.turno.setAsesor(this.currentUser);
			this.turno.setAtencion(new Date());
			this.turno.setNumeroAtencion(Numerador.getNext("turno.nroatencion"));
			this.unidad = turno.getUnidad().getId().toString();

			this.bp.saveOrUpdate(this.turno);
			SimpleDateFormat formatoHora = new SimpleDateFormat("HH");
			SimpleDateFormat formatoMinuto = new SimpleDateFormat("mm");
			hora = formatoHora.format(turno.getLlegada()) + ":" + formatoMinuto.format(turno.getLlegada());
		} else {
			this.errores.put("AtencionTurnos.error.cantidadTurnos", "AtencionTurnos.error.cantidadTurnos");
			this.forward = "AtencionTurno";
		}
	}

	private synchronized void cancelar() {
		Turno t2 = this.loadTurno();
		if (this.turno != null) {
			t2.setAsesor(null);
			t2.setAtencion(null);
			t2.setNumeroAtencion(null);
			Numerador numerador = (Numerador) (bp.getByFilter("FROM Numerador WHERE nombre = 'turno.nroatencion'"))
					.get(0);
			numerador.setNumero(numerador.getNumero() - 1);
			this.bp.saveOrUpdate(numerador);
			this.bp.saveOrUpdate(t2);
		} else {
			this.errores.put("AtencionTurnos.error.cantidadTurnos", "AtencionTurnos.error.cantidadTurnos");
		}
	}

	private Turno loadTurno() {
		if (this.turno != null && this.turno.getId() != null) {
			return (Turno) this.bp.getById(Turno.class, this.turno.getId());
		}
		return null;

	}

	@Override
	public boolean validate() {
		return this.errores.isEmpty();
	}

	private void guardarTurno() {
		this.bp.begin();
		try {
			if (this.turno.getNumeroAtencion() == null || this.turno.getNumeroAtencion().longValue() == 0) {
				this.turno.setNumeroAtencion(Numerador.getNext("turno.nroatencion"));
			}

			if (this.hora != null) {
				String[] temp;
				temp = hora.split(":");
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(turno.getLlegada());
				calendar.set(Calendar.HOUR, new Integer(temp[0]).intValue());
				calendar.set(Calendar.MINUTE, new Integer(temp[1]).intValue());
				this.turno.setLlegada(calendar.getTime());
			}

			if (this.horaAtencion != null) {
				String[] temp;
				temp = horaAtencion.split(":");
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(turno.getAtencion());
				calendar.set(Calendar.HOUR, new Integer(temp[0]).intValue());
				calendar.set(Calendar.MINUTE, new Integer(temp[1]).intValue());
				this.turno.setAtencion(calendar.getTime());
			}

			this.turno.setAsesor(this.currentUser);
			if (idNacionalidad != null) {
				Nacionalidad nacionalidad = (Nacionalidad) bp
						.createQuery("select n from Nacionalidad n where n.id = :id").setLong("id", idNacionalidad)
						.uniqueResult();
				if (nacionalidad != null)
					turno.setNacionalidad(nacionalidad);
			}
			if (idLocalidad != null) {
				Localidad localidad = (Localidad) bp.createQuery("select l from Localidad l where l.id = :id")
						.setLong("id", idLocalidad).uniqueResult();
				if (localidad != null)
					turno.setLocalidad(localidad);
			}
			if (unidad != null && !unidad.equals("")) {
				long u = Long.parseLong(unidad);
				turno.setUnidadId(u);
			}

			this.bp.update(this.turno);
			this.bp.commit();
			if (this.imprimir.booleanValue()) {
				MessageBean m = new MessageBean("turno.save.correcto");
				m.setReport(this.getReportResult());
				this.message = m;
				this.forward = "AtencionTurno";
			}
		} catch (Exception e) {
			e.printStackTrace();
			this.errores.put("Turno.estado.error", "Turno.estado.error");
			this.forward = "AtencionTurno";
		}
	}

	public HashMap<String, Object> getErrores() {
		return errores;
	}

	public void setErrores(HashMap<String, Object> errores) {
		this.errores = errores;
	}

	public BusinessPersistance getBp() {
		return bp;
	}

	public void setBp(BusinessPersistance bp) {
		this.bp = bp;
	}

	public Turno getTurno() {
		if (this.turno == null) {
			this.turno = new Turno();
		}
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	public List<Turno> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<Turno> turnos) {
		this.turnos = turnos;
	}

	public Long getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Long idTurno) {
		this.idTurno = idTurno;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	@Override
	public String getForward() {
		return this.forward;
	}

	@Override
	public String getInput() {
		return this.forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Usuario getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(Usuario currentUser) {
		this.currentUser = currentUser;
	}

	private ReportResult getReportResult() {
		ReportResult rr = new ReportResult();
		rr.setParams("idturno=" + this.turno.getId() + ";SCHEMA=" + BusinessPersistance.getSchema() + ";REPORTS_PATH="
				+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		rr.setPrint(false);
		rr.setReportName("ReporteTurnoNew.jasper");
		rr.setExport("PDF");
		return rr;
	}

	public MessageBean getMessage() {
		return message;
	}

	public void setMessage(MessageBean message) {
		this.message = message;
	}

	public Boolean getImprimir() {
		return imprimir;
	}

	public void setImprimir(Boolean imprimir) {
		this.imprimir = imprimir;
	}

	@Transient
	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	@Transient
	public String getHoraAtencion() {
		return horaAtencion;
	}

	public void setHoraAtencion(String horaAtencion) {
		this.horaAtencion = horaAtencion;
	}

	public Long getCantidadTurnos() {
		return cantidadTurnos;
	}

	public void setCantidadTurnos(Long cantidadTurnos) {
		this.cantidadTurnos = cantidadTurnos;
	}

	public Long getIdNacionalidad() {
		return idNacionalidad;
	}

	public void setIdNacionalidad(Long idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}

	public Long getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Long idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public Long getNumeroatencion() {
		return numeroatencion;
	}

	public void setNumeroatencion(Long numeroatencion) {
		this.numeroatencion = numeroatencion;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

}