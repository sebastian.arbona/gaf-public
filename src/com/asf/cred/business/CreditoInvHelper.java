package com.asf.cred.business;

import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.IOUtils;
import org.hibernate.Query;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.util.DirectorHelper;
import com.asf.util.HTTPHelper;
import com.civitas.logger.LoggerService;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.QQIngresados;

public class CreditoInvHelper {

	@SuppressWarnings("unchecked")
	public static void consultaINV(String id) throws Exception {

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		LoggerService logger = LoggerService.getInstance();
		String urlString = DirectorHelper.getString("inv.consulta.ingreso.url");
		if (urlString == null || urlString.trim().isEmpty()) {
			throw null;
		}
		Query qVinedos = bp.createQuery("SELECT v FROM ObjetoiVinedo v WHERE v.credito.id = :id");
		Query qQuintales = bp.createQuery(
				"SELECT SUM(qq.qqIngreso) FROM QQIngresados qq WHERE qq.vinedo.id = :id AND qq.bodegaGarantia = :bodega");
		Query qGarantias = bp.createQuery(
				"SELECT g.cuit FROM ObjetoiGarantia og JOIN og.garantia g WHERE og.objetoi.id = :objetoi AND og.baja IS NULL");
		Long idCredito = new Long(id);
		List<ObjetoiVinedo> vinedos = qVinedos.setParameter("id", idCredito).list();
		/**
		 * asumo que los creditos de cosecha tienen solo 1 garantia, si hay error lo
		 * captura el try-catch
		 */
		String cuitBodega = (String) qGarantias.setParameter("objetoi", idCredito).uniqueResult();
		logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega + ", Cant.Vi�edos: "
				+ vinedos.size());
		for (ObjetoiVinedo vinedo : vinedos) {
			if (cuitBodega != null && vinedo.getVinedo().getCodigo() != null) {
				String urlConsulta = urlString.replace("{cuitBodega}", cuitBodega).replace("{vinedo}",
						vinedo.getVinedo().getCodigo());
				HttpsURLConnection conn = HTTPHelper.getConexion(urlConsulta, false);
				int responseCode = conn.getResponseCode();
				logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega + ", Vi�edo: "
						+ vinedo.getVinedo().getCodigo() + ", url:" + urlConsulta);
				logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega + ", Vi�edo: "
						+ vinedo.getVinedo().getCodigo() + ", Resultado: " + responseCode);
				if (responseCode == HttpsURLConnection.HTTP_OK) {
					String responseText = IOUtils.toString(conn.getInputStream(), "utf-8");
					logger.log("debug",
							"Consulta INV Credito: " + idCredito + ", CUIT Bodeda: " + cuitBodega + ", Vi�edo: "
									+ vinedo.getVinedo().getCodigo() + ", Resultado: " + responseCode + ", "
									+ (responseText.isEmpty() ? "Sin resultado" : responseText));
					double kgInvBodega = 0;
					double kgInvOtros = 0;
					String[] registros = responseText.split("\\|");
					for (String registro : registros) {
						if (registro.trim().isEmpty()) {
							continue;
						}
						/**
						 * el tercer dato puede venir vacio, pero se mantiene la estructura de la
						 * respuesta cuit, valor1, valor2 entonces al parsear una cadena vacia o con
						 * espacios se genera una excepcion
						 */
						String[] datos = registro.split(",");
						for (int i = 0; i < datos.length; i++) {
							String string = datos[i];
							datos[i] = string.trim();
						}
						if (datos.length > 1) {
							double kgBodega, kgTotal = 0;
							kgBodega = datos[1].isEmpty() ? 0l : Double.parseDouble(datos[1]);
							kgTotal = datos[2].isEmpty() ? 0l : Double.parseDouble(datos[2]);
							kgInvBodega += kgBodega;
							kgInvOtros += kgTotal - kgBodega;
						} else {
							continue;
						}

					}
					Number qqv = (Number) qQuintales.setParameter("id", vinedo.getId()).setParameter("bodega", true)
							.uniqueResult();
					double qqVinedoBodega = qqv != null ? qqv.doubleValue() : 0;
					qqv = (Number) qQuintales.setParameter("id", vinedo.getId()).setParameter("bodega", false)
							.uniqueResult();
					double qqVinedoOtros = qqv != null ? qqv.doubleValue() : 0;
					bp.begin();
					double ingresoBodega = kgInvBodega / 100 - qqVinedoBodega;
					if (ingresoBodega > 0) {
						QQIngresados qqIngresados = new QQIngresados();
						qqIngresados.setQqIngreso(ingresoBodega);
						qqIngresados.setVinedo(vinedo);
						qqIngresados.setFechaIngreso(new Date());
						qqIngresados.setBodegaGarantia(true);
						bp.save(qqIngresados);
						vinedo.setTotalQQIngresados(vinedo.getTotalQQIngresados() + ingresoBodega);
						bp.update(vinedo);
					}
					double ingresoOtros = kgInvOtros / 100 - qqVinedoOtros;
					if (ingresoOtros > 0) {
						QQIngresados qqIngresados = new QQIngresados();
						qqIngresados.setQqIngreso(ingresoOtros);
						qqIngresados.setVinedo(vinedo);
						qqIngresados.setFechaIngreso(new Date());
						qqIngresados.setBodegaGarantia(false);
						bp.save(qqIngresados);
					}
					bp.commit();
				}
			} else {
				logger.log("debug", "Consulta INV Credito: " + idCredito + ", CUIT Bodeda o Vi�edo codigo en null");
			}
		}

		// calcularPorcentajeQQ((Objetoi) bp.getById(Objetoi.class, idCredito));

		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		Float porcCosecha = credito.getPorcentajeCosecha();
		double financiado = credito.getQqFinal();
		double ingresadoTotal = credito.getQqIngresadosTotales();
		double ingresadoBodega = credito.getQqIngresadosGarantia();
		boolean condicion1 = ingresadoTotal >= (financiado * 70 / 100);
		boolean condicion2 = (credito.getTipoGarantia() != 2)
				|| (credito.getTipoGarantia() == 2 && ingresadoBodega >= (financiado * porcCosecha / 100));
		if (condicion1 && condicion2) {
			bp.begin();
			credito.setAutorizadoQQManual(true);
			bp.commit();
		}
	}
}
