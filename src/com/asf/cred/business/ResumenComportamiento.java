package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanResumenComportamiento;
import com.civitas.cred.beans.BeanResumenComportamientoGeneral;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Objetoi;

public class ResumenComportamiento implements IProcess {

	private String forward;
	private String action;
	private List<Linea> lineas;
	private List<Objetoi> creditos;
	private List<BeanResumenComportamiento> creditosBean;
	private List<BeanResumenComportamientoGeneral> beans;

	private String[] idsLineas;
	private String codigo;
	private Date fechaHasta;

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		lineas = bp.getByFilter("select l from Linea l order by l.nombre");

		if (action == null) {
			forward = "ResumenComportamiento";
		} else if (action.equalsIgnoreCase("listar")) {

			// Filtros para la consulta
			String estadosWhere = "('EJECUCION', 'PRIMER DESEMBOLSO', 'PENDIENTE SEGUNDO DESEMBOLSO')";
			String codigoWhere = "";

			// Caso: Estado espec�fico
			if (this.codigo != null && !this.codigo.isEmpty())
				codigoWhere = "AND oc.comportamientoPago = " + this.codigo;

			List<Long> lineasSeleccionadas = new ArrayList<Long>();
			if (idsLineas != null) {
				for (String l : idsLineas) {
					lineasSeleccionadas.add(new Long(l));
				}
			}

			// Caso: L�nea espec�fica
			if (!lineasSeleccionadas.isEmpty()) {

				this.setCreditos(bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc "
						+ "JOIN oc.objetoi o "
						+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
						+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
						+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
						+ "AND o.linea.id IN (:linea) " + codigoWhere + " AND oc.fechaHasta IS NULL "
						+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) ")
						.setParameterList("linea", lineasSeleccionadas).list());

			}

			// Caso: Todas las l�neas
			else {
				this.creditos = bp.createQuery("SELECT DISTINCT o FROM ObjetoiComportamiento oc " + "JOIN oc.objetoi o "
						+ "WHERE (SELECT COUNT(obje) FROM ObjetoiEstado obje WHERE obje.objetoi.id = o.id AND obje.estado.nombreEstado IN "
						+ estadosWhere + "AND obje.fechaHasta IS NULL AND obje.fecha = "
						+ "(SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE o.numeroAtencion = obje2.objetoi.numeroAtencion)) > 0 "
						+ codigoWhere + " AND oc.fechaHasta IS NULL "
						+ "AND oc.fecha = (SELECT MAX(oc2.fecha) FROM ObjetoiComportamiento oc2 WHERE o.numeroAtencion = oc2.objetoi.numeroAtencion) ")
						.list();

			}

			setCreditosBean(new ArrayList<BeanResumenComportamiento>());

			for (Objetoi cred : creditos) {
				BeanResumenComportamiento bn = new BeanResumenComportamiento();
				bn.setCredito(cred);
				creditosBean.add(bn);
			}

			setBeans(new ArrayList<BeanResumenComportamientoGeneral>());

			BeanResumenComportamientoGeneral beanGral = new BeanResumenComportamientoGeneral();
			beans = beanGral.armarReporteResumenComportamiento(creditosBean, lineasSeleccionadas);

			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("NotificacionList.credito",
					creditos);
			forward = "ResumenComportamiento";
		}
		return true;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public HashMap getErrors() {
		return new HashMap();
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<Linea> getLineas() {
		return lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public String[] getIdsLineas() {
		return idsLineas;
	}

	public void setIdsLineas(String[] idsLineas) {
		this.idsLineas = idsLineas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	public List<Objetoi> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Objetoi> creditos) {
		this.creditos = creditos;
	}

	public List<BeanResumenComportamiento> getCreditosBean() {
		return creditosBean;
	}

	public void setCreditosBean(List<BeanResumenComportamiento> creditosBean) {
		this.creditosBean = creditosBean;
	}

	public List<BeanResumenComportamientoGeneral> getBeans() {
		return beans;
	}

	public void setBeans(List<BeanResumenComportamientoGeneral> beans) {
		this.beans = beans;
	}

}
