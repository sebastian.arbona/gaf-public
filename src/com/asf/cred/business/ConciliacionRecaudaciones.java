package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;

import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.civitas.importacion.ImportarBcoCredicop;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.DetalleRecaudacion;
import com.nirven.creditos.hibernate.EstadoCheque;

public class ConciliacionRecaudaciones implements IProcess {

	private String cid;

	private Long idCaratula;

	private String fechaMov;
	private String nroComprobante;
	private String codOp;
	private String estadoComprobante; // ?
	private Double importeDesde;
	private Double importeHasta;

	private List<Recaudacion> recaudaciones;
	private Map<String, List<DetalleRecaudacion>> detallesVinculados;
	private List<Recaudacion> recaudacionesFiltro;

	private List<Recaudacion> recaudacionesCero;
	private List<Recaudacion> recaudacionesPendientes;

	private Long idRecaudacionSeleccionada;
	private Recaudacion recaudacion;
	private List<DetalleRecaudacion> detallesSeleccionar;

	private Long idDetalleRec;

	private boolean guardado;

	private String accion;
	private String forward;
	private String idFechaCobranza;

	private BusinessPersistance bp;

	private HttpSession session;

	private List<DetalleRecaudacion> detallesSinVincular;

	private boolean colapsarPendientes;

	public ConciliacionRecaudaciones() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		session = SessionHandler.getCurrentSessionHandler().getRequest().getSession();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return init();
		}
		if (accion.equalsIgnoreCase("buscar")) {
			return buscar();
		} else if (accion.equalsIgnoreCase("seleccionar")) {
			return seleccionar();
		} else if (accion.equalsIgnoreCase("desvincular")) {
			return desvincular();
		} else if (accion.equalsIgnoreCase("vincular")) {
			return vincular();
		} else if (accion.equalsIgnoreCase("guardar")) {
			return guardar();
		} else if (accion.equalsIgnoreCase("regSinVincular")) {
			return regSinVincular();
		}
		return false;
	}

	private boolean guardar() {
		restaurarVinculados();

		bp.begin();

		idCaratula = (Long) session.getAttribute(cid);
		Caratula caratula = (Caratula) bp.getById(Caratula.class, idCaratula);

		for (List<DetalleRecaudacion> dets : detallesVinculados.values()) {
			for (DetalleRecaudacion d : dets) {
				d.setCaratula(caratula);
				bp.update(d);
			}
		}

		bp.commit();

		guardado = true;

		forward = "ConciliacionRecaudaciones";
		colapsarPendientes = true;
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean vincular() {
		List<Long> idsDetalles = new ArrayList<Long>();
		Enumeration<String> params = SessionHandler.getCurrentSessionHandler().getRequest().getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = params.nextElement();
			if (paramName.startsWith("det-")) {
				idsDetalles.add(new Long(paramName.substring(4)));
			}
		}

		if (!idsDetalles.isEmpty()) {
			bp.begin();

			idRecaudacionSeleccionada = (Long) session.getAttribute(cid + "idRecaudacionSeleccionada");
			recaudacion = (Recaudacion) bp.getById(Recaudacion.class, idRecaudacionSeleccionada);

			idCaratula = (Long) session.getAttribute(cid);

			List<DetalleRecaudacion> detallesVincular = bp
					.createQuery("select d from DetalleRecaudacion d where d.id in (:ids)")
					.setParameterList("ids", idsDetalles).list();

			for (DetalleRecaudacion d : detallesVincular) {
				d.setTempRecaudacion(recaudacion);
			}

			actualizarVinculados();

			bp.commit();
		} else {
			restaurarVinculados();
		}

		restaurarFiltro();

		forward = "ConciliacionRecaudaciones";
		colapsarPendientes = true;
		return true;
	}

	private boolean desvincular() {
		bp.begin();

		DetalleRecaudacion detalle = (DetalleRecaudacion) bp.getById(DetalleRecaudacion.class, idDetalleRec);
		detalle.setTempRecaudacion(null);

		idCaratula = (Long) session.getAttribute(cid);

		actualizarVinculados();

		bp.commit();

		restaurarFiltro();

		forward = "ConciliacionRecaudaciones";
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean seleccionar() {
		recaudacion = (Recaudacion) bp.getById(Recaudacion.class, idRecaudacionSeleccionada);

		String codiBaCredicoopStr = DirectorHelper.getString(ImportarBcoCredicop.DIRECTOR_CODIBA_CREDICOOP);
		Long codiBaCredicoop = new Long(codiBaCredicoopStr);

//		if (recaudacion.getBancos() != null && recaudacion.getBancos().getCodiBa().equals(codiBaCredicoop) &&
//				recaudacion.getNroOperacion() != null && recaudacion.getNroOperacion() != 0L) {
//			detallesSeleccionar = new ArrayList<DetalleRecaudacion>(
//					bp.createQuery("select d from DetalleRecaudacion d where d.tempRecaudacion is null and d.recaudacion is null and d.banco = :banco and d.fechaAcreditacionValor = :fecha " +
//							"and d.numeroOperacion = :nroOp")
//					.setParameter("banco", recaudacion.getBancos())
//					.setParameter("fecha", recaudacion.getFechamovimiento())
//					.setParameter("nroOp", recaudacion.getNroOperacion()).list());
//		} else {
		detallesSeleccionar = new ArrayList<DetalleRecaudacion>(bp.createQuery(
				"select d from DetalleRecaudacion d where d.tempRecaudacion is null and d.recaudacion is null and d.banco = :banco and d.fechaAcreditacionValor = :fecha ")
				.setParameter("banco", recaudacion.getBancos()).setParameter("fecha", recaudacion.getFechamovimiento())
				.list());
//		}

		Iterator<DetalleRecaudacion> it = detallesSeleccionar.iterator();
		while (it.hasNext()) {
			DetalleRecaudacion d = it.next();
			EstadoCheque estado = d.getEstadoActual();
			if (estado != null && (!estado.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_NACION_LIBERADO)
					&& !estado.getEstado().equals(EstadoCheque.ESTADO_CHEQUE_CREDICOOP_ACREDITADO))) {
				it.remove();
			}
		}

		recaudacionesFiltro = (List<Recaudacion>) session.getAttribute(cid + "recaudacionesFiltro");
		guardarFiltro();

		session.setAttribute(cid + "idRecaudacionSeleccionada", idRecaudacionSeleccionada);

		forward = "ConciliacionDetalles";
		return true;
	}

	@SuppressWarnings("unchecked")
	private boolean regSinVincular() {
		detallesSinVincular = bp.createQuery(
				"select d from DetalleRecaudacion d where d.tempRecaudacion is null and d.recaudacion is null order by d.banco.codiBa, d.fechaAcreditacionValor")
				.list();

		recaudacionesFiltro = (List<Recaudacion>) session.getAttribute(cid + "recaudacionesFiltro");
		guardarFiltro();

		forward = "ConciliacionSinVincular";
		return true;
	}

	/**
	 * Hace lo mismo que buscar, salvo que no hace la consulta con los filtros
	 * 
	 * @return true
	 */
	private boolean init() {
		if (cid != null && session.getAttribute(cid) != null) { // recuperar asignaciones
			restaurarVinculados();

			idCaratula = (Long) session.getAttribute(cid);
		} else { // primera vez
			cid = UUID.randomUUID().toString();
			session.setAttribute(cid, idCaratula); // guardo cid para proximos pedidos

			actualizarVinculados();
		}

		forward = "ConciliacionRecaudaciones";

		return true;
	}

	private boolean buscar() {
		if (cid != null && session.getAttribute(cid) != null) { // recuperar asignaciones
			restaurarVinculados();

			idCaratula = (Long) session.getAttribute(cid);
		} else { // primera vez
			cid = UUID.randomUUID().toString();
			session.setAttribute(cid, idCaratula); // guardo cid para proximos pedidos

			actualizarVinculados();
		}

		buscarRecaudacionesFiltro();

		forward = "ConciliacionRecaudaciones";

		colapsarPendientes = true;

		return true;
	}

	@SuppressWarnings("unchecked")
	private void buscarRecaudacionesFiltro() {
		String q = "select r from Recaudacion r where ";

		if (fechaMov != null && !fechaMov.isEmpty()) {
			q += "r.fechamovimiento = :fechaMov and ";
		}
		if (codOp != null && !codOp.isEmpty()) {
			q += "r.codigoOperacion = :codOp and ";
		}
		if (nroComprobante != null && !nroComprobante.isEmpty()) {
			q += "r.numeroComprobanteExterno = :nroComprobante and ";
		}
		if (estadoComprobante != null && !estadoComprobante.isEmpty() && !estadoComprobante.equalsIgnoreCase("T")) {
			if (estadoComprobante.equalsIgnoreCase("P")) {
				q += "(r.saldo is null or r.saldo = r.importe) and ";
			} else if (estadoComprobante.equalsIgnoreCase("A")) {
				q += "r.saldo is not null and r.saldo < r.importe and ";
			}
		}
		if (importeDesde != null && importeDesde != 0) {
			q += "r.importe >= :importeDesde and ";
		}
		if (importeHasta != null && importeHasta != 0) {
			q += "r.importe <= :importeHasta and ";
		}

		if (q.endsWith("where ")) {
			q = q.substring(0, q.length() - 6);
		} else if (q.endsWith("and ")) {
			q = q.substring(0, q.length() - 4);
		}

		Query query = bp.createQuery(q);

		if (fechaMov != null && !fechaMov.isEmpty()) {
			query.setParameter("fechaMov", DateHelper.getDate(fechaMov));
		}
		if (codOp != null && !codOp.isEmpty()) {
			query.setParameter("codOp", codOp);
		}
		if (nroComprobante != null && !nroComprobante.isEmpty()) {
			query.setParameter("nroComprobante", nroComprobante);
		}
		if (importeDesde != null && importeDesde != 0) {
			query.setParameter("importeDesde", importeDesde);
		}
		if (importeHasta != null && importeHasta != 0) {
			query.setParameter("importeHasta", importeHasta);
		}

		recaudacionesFiltro = new ArrayList<Recaudacion>(query.list());

		eliminarDuplicadas();

		session.setAttribute(cid + "recaudacionesFiltro", recaudacionesFiltro);
	}

	private void eliminarDuplicadas() {
		if (recaudacionesFiltro != null) {
			Iterator<Recaudacion> it = recaudacionesFiltro.iterator();
			while (it.hasNext()) {
				Recaudacion r = it.next();
				if (recaudaciones.contains(r)) { // no repetir las que ya estan conciliadas
					it.remove();
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void actualizarVinculados() {
		List<DetalleRecaudacion> detRecs = bp.createQuery(
				"select d from DetalleRecaudacion d where d.tempRecaudacion is not null and d.recaudacion is null")
				.list();

		detallesVinculados = new HashMap<String, List<DetalleRecaudacion>>();
		recaudaciones = new ArrayList<Recaudacion>();

		for (DetalleRecaudacion detRec : detRecs) {
			if (detRec.getTempRecaudacion() != null) {
				List<DetalleRecaudacion> detallesRec = detallesVinculados
						.get(detRec.getTempRecaudacion().getId().toString());

				if (detallesRec == null) {
					detallesRec = new ArrayList<DetalleRecaudacion>();
					detallesVinculados.put(detRec.getTempRecaudacion().getId().toString(), detallesRec);
					recaudaciones.add(detRec.getTempRecaudacion());
				}

				detallesRec.add(detRec);
			}
		}

		// dividir entre recaudaciones con saldo cero y saldo mayor a cero
		recaudacionesCero = new ArrayList<Recaudacion>();
		recaudacionesPendientes = new ArrayList<Recaudacion>();
		for (Recaudacion rec : recaudaciones) {
			if (rec.getSaldoTemporal() != null && rec.getSaldoTemporal() > 0) {
				recaudacionesPendientes.add(rec);
			} else {
				recaudacionesCero.add(rec);
			}
		}

		session.setAttribute(cid + "detallesVinculados", detallesVinculados);
		session.setAttribute(cid + "recaudaciones", recaudaciones);
		session.setAttribute(cid + "recaudacionesCero", recaudacionesCero);
		session.setAttribute(cid + "recaudacionesPendientes", recaudacionesPendientes);
	}

	@SuppressWarnings("unchecked")
	private void restaurarVinculados() {
		detallesVinculados = (Map<String, List<DetalleRecaudacion>>) session.getAttribute(cid + "detallesVinculados");
		recaudaciones = (List<Recaudacion>) session.getAttribute(cid + "recaudaciones");
		recaudacionesCero = (List<Recaudacion>) session.getAttribute(cid + "recaudacionesCero");
		recaudacionesPendientes = (List<Recaudacion>) session.getAttribute(cid + "recaudacionesPendientes");
	}

	private void guardarFiltro() {
		session.setAttribute(cid + "fechaMov", fechaMov);
		session.setAttribute(cid + "codOp", codOp);
		session.setAttribute(cid + "nroComprobante", nroComprobante);
		session.setAttribute(cid + "estadoComprobante", estadoComprobante);
		session.setAttribute(cid + "importeDesde", importeDesde);
		session.setAttribute(cid + "importeHasta", importeHasta);
		session.setAttribute(cid + "recaudacionesFiltro", recaudacionesFiltro);
	}

	@SuppressWarnings("unchecked")
	private void restaurarFiltro() {
		fechaMov = (String) session.getAttribute(cid + "fechaMov");
		codOp = (String) session.getAttribute(cid + "codOp");
		nroComprobante = (String) session.getAttribute(cid + "nroComprobante");
		estadoComprobante = (String) session.getAttribute(cid + "estadoComprobante");
		importeDesde = (Double) session.getAttribute(cid + "importeDesde");
		importeHasta = (Double) session.getAttribute(cid + "importeHasta");
		/*
		 * recaudacionesFiltro = (List<Recaudacion>)
		 * session.getAttribute(cid+"recaudacionesFiltro"); eliminarDuplicadas();
		 */
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public boolean validate() {
		return true;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public HashMap getErrors() {
		return null;
	}

	public Long getIdCaratula() {
		return idCaratula;
	}

	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getFechaMov() {
		return fechaMov;
	}

	public void setFechaMov(String fechaMov) {
		this.fechaMov = fechaMov;
	}

	public String getNroComprobante() {
		return nroComprobante;
	}

	public void setNroComprobante(String nroComprobante) {
		this.nroComprobante = nroComprobante;
	}

	public String getCodOp() {
		return codOp;
	}

	public void setCodOp(String codOp) {
		this.codOp = codOp;
	}

	public String getEstadoComprobante() {
		return estadoComprobante;
	}

	public void setEstadoComprobante(String estadoComprobante) {
		this.estadoComprobante = estadoComprobante;
	}

	public Double getImporteDesde() {
		return importeDesde;
	}

	public void setImporteDesde(Double importeDesde) {
		this.importeDesde = importeDesde;
	}

	public Double getImporteHasta() {
		return importeHasta;
	}

	public void setImporteHasta(Double importeHasta) {
		this.importeHasta = importeHasta;
	}

	public List<Recaudacion> getRecaudaciones() {
		return recaudaciones;
	}

	public void setRecaudaciones(List<Recaudacion> recaudaciones) {
		this.recaudaciones = recaudaciones;
	}

	public Map<String, List<DetalleRecaudacion>> getDetallesVinculados() {
		return detallesVinculados;
	}

	public void setDetallesVinculados(Map<String, List<DetalleRecaudacion>> detallesVinculados) {
		this.detallesVinculados = detallesVinculados;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<DetalleRecaudacion> getDetallesVinculados(String idRec) {
		return detallesVinculados.get(idRec);
	}

	public void setDetallesVinculados(String idRec, List<DetalleRecaudacion> detalles) {
		detallesVinculados.put(idRec, detalles);
	}

	public Recaudacion getRecaudacion() {
		return recaudacion;
	}

	public void setRecaudacion(Recaudacion recaudacion) {
		this.recaudacion = recaudacion;
	}

	public List<DetalleRecaudacion> getDetallesSeleccionar() {
		return detallesSeleccionar;
	}

	public void setDetallesSeleccionar(List<DetalleRecaudacion> detallesSeleccionar) {
		this.detallesSeleccionar = detallesSeleccionar;
	}

	public Long getIdDetalleRec() {
		return idDetalleRec;
	}

	public void setIdDetalleRec(Long idDetalleRec) {
		this.idDetalleRec = idDetalleRec;
	}

	public List<Recaudacion> getRecaudacionesFiltro() {
		return recaudacionesFiltro;
	}

	public void setRecaudacionesFiltro(List<Recaudacion> recaudacionesFiltro) {
		this.recaudacionesFiltro = recaudacionesFiltro;
	}

	public Long getIdRecaudacionSeleccionada() {
		return idRecaudacionSeleccionada;
	}

	public void setIdRecaudacionSeleccionada(Long idRecaudacionSeleccionada) {
		this.idRecaudacionSeleccionada = idRecaudacionSeleccionada;
	}

	public boolean isGuardado() {
		return guardado;
	}

	public void setGuardado(boolean guardado) {
		this.guardado = guardado;
	}

	public String getIdFechaCobranza() {
		return idFechaCobranza;
	}

	public void setIdFechaCobranza(String idFechaCobranza) {
		this.idFechaCobranza = idFechaCobranza;
	}

	public List<Recaudacion> getRecaudacionesCero() {
		return recaudacionesCero;
	}

	public void setRecaudacionesCero(List<Recaudacion> recaudacionesCero) {
		this.recaudacionesCero = recaudacionesCero;
	}

	public List<Recaudacion> getRecaudacionesPendientes() {
		return recaudacionesPendientes;
	}

	public void setRecaudacionesPendientes(List<Recaudacion> recaudacionesPendientes) {
		this.recaudacionesPendientes = recaudacionesPendientes;
	}

	public List<DetalleRecaudacion> getDetallesSinVincular() {
		return detallesSinVincular;
	}

	public boolean isColapsarPendientes() {
		return colapsarPendientes;
	}
}
