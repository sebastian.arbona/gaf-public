package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DetalleFactura;
import com.nirven.creditos.hibernate.Factura;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CtaCteManual extends ConversationProcess {

	private Long numeroAtencion;
	@Save
	private List<DetalleFactura> detalles;
	@Save
	private List<Desembolso> desembolsos;
	@Save
	private List<Cuota> cuotas;
	private Long idDetalle;
	private Long idDesembolso;
	private Long idCuota;
	private boolean debito;
	private boolean credito;
	private Date fechaGeneracion;
	private Date fechaProceso;
	private int periodo;
	@Save(refresh = true)
	private Objetoi objetoi;
	private String tipoConcepto;
	private boolean success;

	public CtaCteManual() {
		periodo = Calendar.getInstance().get(Calendar.YEAR);
		tipoConcepto = Ctacte.TIPO_CONCEPTO_DESEMBOLSO;
		fechaProceso = new Date();
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscarGastos() {
		if (numeroAtencion == null) {
			error("ctactemanual.numeroatencion");
			return false;
		}

		objetoi = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion").setParameter("nroAtencion", numeroAtencion)
				.uniqueResult();
		detalles = bp
				.createQuery(
						"select d from DetalleFactura d where d.credito.numeroAtencion = :nro order by d.factura.fecha")
				.setParameter("nro", numeroAtencion).list();
		desembolsos = bp.getNamedQuery("Desembolso.findByCredito").setParameter("idCredito", objetoi.getId()).list();
		cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", objetoi.getId()).list();
		return true;
	}

	@ProcessMethod
	public boolean impactar() {
		if (idDetalle == null) {
			error("ctactemanual.iddetalle");
			return false;
		}
		if (idDesembolso == null) {
			error("ctactemanual.iddesembolso");
			return false;
		}
		if (idCuota == null) {
			error("ctactemanual.idcuota");
			return false;
		}

		bp.begin();

		try {
			DetalleFactura detalle = (DetalleFactura) bp.getById(DetalleFactura.class, idDetalle);
			Desembolso desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
			Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);
			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_DEBITO).uniqueResult();
			Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
					.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
			String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
			Concepto concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, periodo);

			Factura factura = detalle.getFactura();
			NovedadCtaCte novedad = (NovedadCtaCte) bp
					.createQuery("select n from NovedadCtaCte n where n.detalleFactura.id = :id")
					.setParameter("id", detalle.getId()).uniqueResult();

			if (debito) {
				Boleto boleto = new Boleto();
				boleto.setFechaEmision(fechaGeneracion);
				boleto.setFechaVencimiento(fechaGeneracion);
				boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
				boleto.setUsuario(usuario);
				boleto.setObjetoi(objetoi);
				boleto.setPeriodoBoleto((long) periodo);
				boleto.setVerificadorBoleto(0L);
				boleto.setImporte(detalle.getImporte());
				boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
				bp.save(boleto);

				Ctacte cc = new Ctacte();
				cc.setAsociado(concepto);
				cc.setFacturado(concepto);
				cc.setBoleto(boleto);
				cc.setImporte(detalle.getImporte());
				cc.redondear();
				cc.setFechaGeneracion(fechaGeneracion);
				cc.setFechaProceso(fechaProceso);
				if (cuota != null) {
					cc.setFechaVencimiento(cuota.getFechaVencimiento());
				}
				cc.setCuota(cuota);
				cc.setTipoMovimiento("cuota");
				cc.setTipomov(tipoDebito);
				cc.setUsuario(usuario);

				cc.setDetalle("Gastos Proveedor: " + factura.getProveedor().getPersona().getNomb12() + " - Factura: "
						+ factura.getNumero());
				cc.setContabiliza(false);
				cc.setDesembolso(desembolso);

				CtacteKey ck = new CtacteKey();
				ck.setPeriodoCtacte(new Long(periodo));
				ck.setObjetoi(objetoi);
				ck.setVerificadorCtacte(0L);
				ck.setMovimientoCtacte(Numerador.getNext(numeradorCtaCte));
				ck.setItemCtacte(1L);
				cc.setId(ck);

				Objetoi.cargarCotizacion(cc, fechaGeneracion);

				cc.setTipoConcepto(tipoConcepto);

				Bolcon bc = new Bolcon();
				bc.setFacturado(concepto);
				bc.setOriginal(concepto);
				bc.setImporte(cc.getImporte());
				bc.setCuota(cuota);
				bc.setTipomov(tipoDebito);

				BolconKey bck = new BolconKey();
				bck.setBoleto(boleto);
				bck.setPeriodoCtacte(ck.getPeriodoCtacte());
				bck.setMovimientoCtacte(ck.getMovimientoCtacte());
				bck.setVerificadorCtacte(ck.getVerificadorCtacte());
				bck.setItemCtacte(ck.getItemCtacte());
				bc.setId(bck);

				CreditoHandler.cargarCotizacion(cc);

				bp.save(cc);
				bp.save(bc);

				detalle.setImpactado(true);
				detalle.setMovimientoCtaCte(ck.getMovimientoCtacte());
				bp.update(detalle);

				if (novedad != null) {
					novedad.setImpactado(true);
					novedad.setMovimientoDeb(cc);
					bp.update(novedad);
				}
			}

			if (credito) {
				long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
				Boleto notaCredito = new Boleto();
				notaCredito.setFechaEmision(fechaGeneracion);
				notaCredito.setFechaVencimiento(fechaGeneracion);
				notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				notaCredito.setUsuario(usuario);
				notaCredito.setNumeroCuota(cuota.getNumero());
				notaCredito.setObjetoi(getObjetoi());
				notaCredito.setPeriodoBoleto((long) periodo);
				notaCredito.setVerificadorBoleto(0L);
				notaCredito.setImporte(detalle.getImporte());
				notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
				bp.save(notaCredito);

				// Creo credito el la cta cte
				Ctacte cc = getObjetoi().crearCtacte(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(),
						fechaGeneracion, notaCredito, objetoi, cuota, tipoConcepto);
				cc.setAsociado(concepto);
				cc.setFacturado(concepto);
				cc.setTipomov(tipoCredito);
				cc.setTipoMovimiento("Pago");
				cc.setUsuario(usuario);
				cc.setFechaProceso(fechaProceso);
				cc.getId().setMovimientoCtacte(movimientoCtaCte);
				cc.getId().setItemCtacte(new Long(1));
				cc.setDetalle("Gastos Proveedor: " + factura.getProveedor().getPersona().getNomb12() + " - Factura: "
						+ detalle.getFactura().getNumero());
				cc.setDesembolso(desembolso);
				bp.save(cc);

				Bolcon bc = getObjetoi().crearBolcon(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(),
						notaCredito, cc.getId(), cuota);
				bc.setTipomov(tipoCredito);
				bp.save(bc);

				detalle.setImpactado(true);
				bp.update(detalle);

				if (novedad != null) {
					novedad.setImpactado(true);
					novedad.setMovimientoCred(cc);
					bp.update(novedad);
				}
			}

			bp.commit();

			detalles = null;

			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			bp.rollback();
			error("ctactemanual.errorimpactar");
		}

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "CtaCteManual";
	}

	public List<Cuota> getCuotas() {
		return cuotas;
	}

	public List<Desembolso> getDesembolsos() {
		return desembolsos;
	}

	public String getTipoConcepto() {
		return tipoConcepto;
	}

	public void setTipoConcepto(String tipoConcepto) {
		this.tipoConcepto = tipoConcepto;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public List<DetalleFactura> getDetalles() {
		return detalles;
	}

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}

	public boolean isDebito() {
		return debito;
	}

	public void setDebito(boolean debito) {
		this.debito = debito;
	}

	public boolean isCredito() {
		return credito;
	}

	public void setCredito(boolean credito) {
		this.credito = credito;
	}

	public Long getIdDesembolso() {
		return idDesembolso;
	}

	public void setIdDesembolso(Long idDesembolso) {
		this.idDesembolso = idDesembolso;
	}

	public Long getIdCuota() {
		return idCuota;
	}

	public void setIdCuota(Long idCuota) {
		this.idCuota = idCuota;
	}

	public String getFechaGeneracion() {
		return DateHelper.getString(fechaGeneracion);
	}

	public void setFechaGeneracion(String fechaGeneracion) {
		this.fechaGeneracion = DateHelper.getDate(fechaGeneracion);
	}

	public String getFechaProceso() {
		return DateHelper.getString(fechaProceso);
	}

	public void setFechaProceso(String fechaProceso) {
		this.fechaProceso = DateHelper.getDate(fechaProceso);
	}

	public int getPeriodo() {
		return periodo;
	}

	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}

	public boolean isSuccess() {
		return success;
	}
}
