/*
 * MyArrayList.java
 *
 * Created on 15 de marzo de 2006, 0:21
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.asf.cred.business;

import java.util.ArrayList;

/**
 *
 * @author cgarcia
 */
public class StrutsArray extends ArrayList {
	private Class csClass = null;

	/** Creates a new instance of MyArrayList */

	public StrutsArray(Class pClass) {
		this.csClass = pClass;
	}

	@SuppressWarnings("unchecked")
	public Object get(int i) {
		Object oObj = null;
		try {
			try {
				oObj = super.get(i);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			if (oObj == null) {
				while (i >= size()) {
					try {
						if (super.get(i) == null)
							add(null);
					} catch (Exception ex) {
						add(null);
					}
				}
				set(i, this.csClass.newInstance());
				oObj = super.get(i);
			}
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return oObj;
	}
}
