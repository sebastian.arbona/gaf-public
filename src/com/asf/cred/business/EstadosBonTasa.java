package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaEstado;
import com.nirven.creditos.hibernate.CuotaBonTasa;
import com.nirven.creditos.hibernate.EstadoBonifExterna;
import com.nirven.creditos.hibernate.TipoResolucionBonTasa;
import com.nirven.creditos.hibernate.TipoResolucionBonTasaDto;

public class EstadosBonTasa extends ConversationProcess {

	@Save
	private Long idBonTasa;
	private List<BonTasaEstado> estados;
	private Estado estadoActual;
	@Save
	private BonTasa bonificacion;
	private BonTasaEstado estado = new BonTasaEstado();

	@Save
	private List<TipoResolucionBonTasaDto> tipoResolucionList;

	@ProcessMethod
	public boolean nuevo() {
		forward = "NuevoEstadoBonTasa";
		estado.setFechaCambio(new Date());
//		bonificacion = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
		cargarTipoResolucionList();
		return true;
	}

	private void cargarTipoResolucionList() {
		if (tipoResolucionList == null) {
			tipoResolucionList = new ArrayList<TipoResolucionBonTasaDto>();
			TipoResolucionBonTasaDto dtoSR = new TipoResolucionBonTasaDto();
			dtoSR.setId("SIN_RESOLUCION");
			dtoSR.setNombre("Sin Resolución");
			tipoResolucionList.add(dtoSR);
			for (TipoResolucionBonTasa tipo : TipoResolucionBonTasa.values()) {
				TipoResolucionBonTasaDto dto = new TipoResolucionBonTasaDto();
				dto.setId(tipo.toString());
				dto.setNombre(tipo.getTexto());
				tipoResolucionList.add(dto);
			}
		}
	}

	@ProcessMethod
	public boolean save() {
		forward = "NuevoEstadoBonTasa";

		if (estado.getEstado() == null) {
			error("estadobontasa.error.estadovacio");
			return false;
		}
		bonificacion = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
		BonTasaEstado estadoActual = bonificacion.getEstadoActual();
		if (estadoActual != null && estadoActual.getIdEstado() != null
				&& estado.getEstado().getIdEstado().equals(estadoActual.getIdEstado())) {
			error("estadobontasa.error.igualestadoactual");
			return false;
		}
		if (estado.getTipoResolucion() != null && (estado.getNumeroResolucion() == null
				|| estado.getNumeroResolucion().isEmpty() || estado.getFechaResolucion() == null)) {
			error("estadobontasa.error.datosresolucion");
			return false;
		}
		estado.setAsesor(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		estado.setBonTasa(bonificacion);
		bp.save(estado);

		// Cuando se elige estado CADUCIDAD, cambia el estado a todas las cuotas con
		// fecha de vencimiento >= a la fecha del estado. Tambien poner cero en
		// subsidio.
		if (estado.getEstado().getNombreEstado().equals(EstadoBonifExterna.CADUCA.toString())) {
			@SuppressWarnings("unchecked")
			List<CuotaBonTasa> cuotas = (List<CuotaBonTasa>) bp
					.createQuery("Select c from CuotaBonTasa c "
							+ " where c.bonTasa=:bonTasa and c.fechaVencimiento >= :fechaEstado"
							+ " AND c.tipoEstadoBonCuota != :pagada")
					.setParameter("bonTasa", estado.getBonTasa()).setParameter("fechaEstado", estado.getFechaCambio())
					.setParameter("pagada", "pagada").list();

			for (CuotaBonTasa cuotaBonTasa : cuotas) {
				cuotaBonTasa.setTipoEstadoBonCuota("Caduca");
				cuotaBonTasa.setSubsidio(0.0);
				bp.update(cuotaBonTasa);
			}
		}

		forward = getDefaultForward();
		buscarEstados();

		return true;
	}

//	private void actual(){	
//		BonTasa bonTasa = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
//		List<BonTasaEstado> estados = (List<BonTasaEstado>) bp.createQuery("Select b from BonTasaEstado b where b.bonTasa=:bonTasa").setParameter("bonTasa", bonTasa).list();
//		BonTasaEstado estado = estados.get(0);
//		for (BonTasaEstado bonTasaEstado : estados) {
//			if(bonTasaEstado.getFechaCambio().after(estado.getFechaCambio())){
//				estado=bonTasaEstado;
//			}			
//		}
//		if(estado.getEstado().getNombreEstado().equalsIgnoreCase("activo")){
//			activa=true;
//		}else if (estado.getEstado().getNombreEstado().equalsIgnoreCase("baja")){
//			baja=true;
//		}
//	}
//	
//	private void modificarEstado(String numero){
//		BonTasaEstado estado= new BonTasaEstado();
//		List<Estado> estados = bp.createQuery("select e from Estado e where e.nombreEstado = :estado and e.tipo = 'BonTasa'").setParameter("estado", bonificacion.getTipoEstadoBonTasa()).setMaxResults(1).list();
//		if (!estados.isEmpty()) {
//			estado.setEstado(estados.get(0));
//		}
//		Date fecha = new Date();
//		estado.setFechaCambio(fecha);
//		estado.setAsesor(SessionHandler.getCurrentSessionHandler().getCurrentUser());
//		estado.setBonTasa(bonificacion);
//		bp.save(estado);
//	}

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscarEstados() {
		BonTasa bonTasa = (BonTasa) bp.getById(BonTasa.class, idBonTasa);
		estados = bp.createQuery("Select eb from BonTasaEstado eb where eb.bonTasa=:bon").setParameter("bon", bonTasa)
				.list();
		return true;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public List<BonTasaEstado> getEstados() {
		return estados;
	}

	public void setEstados(List<BonTasaEstado> estados) {
		this.estados = estados;
	}

	public Estado getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(Estado estadoActual) {
		this.estadoActual = estadoActual;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	@Override
	protected String getDefaultForward() {
		return "estadosBonTasa";
	}

	public BonTasaEstado getEstado() {
		return estado;
	}

	public void setEstado(BonTasaEstado estado) {
		this.estado = estado;
	}

	public List<TipoResolucionBonTasaDto> getTipoResolucionList() {
		return tipoResolucionList;
	}

	public void setTipoResolucionList(List<TipoResolucionBonTasaDto> tipoResolucionList) {
		this.tipoResolucionList = tipoResolucionList;
	}

}
