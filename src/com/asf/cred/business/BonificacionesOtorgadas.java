package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.gaf.hibernate.Bancos;
import com.asf.gaf.hibernate.Ejercicio;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;

public class BonificacionesOtorgadas implements IProcess {

	private String forward;
	private HashMap<String, Object> errores;
	private String accion;
	private BusinessPersistance bp;
	private Bancos ente;
	private Ejercicio ejercicio;
	private Integer mesDesde;
	private Integer mesHasta;
	private List<ObjetoiDTO> creditos;
	private List<BonDetalleDTO> bonDetalleDTOs;

	public BonificacionesOtorgadas() {
		forward = "BonificacionesOtorgadas";
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ente = new Bancos();
		ejercicio = new Ejercicio();
		creditos = new ArrayList<ObjetoiDTO>();
		bonDetalleDTOs = new ArrayList<BonDetalleDTO>();
	}

	@Override
	public boolean doProcess() {
		if (accion == null) {
			return false;
		}
		if (accion.equals("listar")) {
			listar();
		}
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void listar() {
		if (ente.getId() == null || ente.getId().longValue() == 0 || ejercicio.getEjercicio() == null
				|| ejercicio.getEjercicio().longValue() == 0) {
			errores.put("bonificacionesOtorgadas.ente_ejercicio", "bonificacionesOtorgadas.ente_ejercicio");
			return;
		}
		Calendar calendar = Calendar.getInstance();
		int meses;
		if (mesDesde.equals(-1) && mesHasta.equals(-1)) {
			mesDesde = 0;
			mesHasta = 11;
			meses = 11;
		} else if (mesDesde >= 0 && mesHasta >= 0) {
			if ((meses = mesHasta - mesDesde) < 0) {
				errores.put("bonificacionesOtorgadas.mesPosterior", "bonificacionesOtorgadas.mesPosterior");
				return;
			}
		} else {
			errores.put("bonificacionesOtorgadas.mesNulo", "bonificacionesOtorgadas.mesNulo");
			return;
		}

		calendar.set(ejercicio.getEjercicio().intValue(), mesDesde, 1, 0, 0, 0);
		Date desde = calendar.getTime();
		Date hasta = desplazarMeses(calendar, meses + 1);

		List<BonDetalle> bonDetalles = bp.getNamedQuery("BonDetalle.findByEnteYFechaBonificada").setEntity("ente", ente)
				.setDate("desde", desde).setDate("hasta", hasta).list();

		List<Object[]> creditos_cuotas = listarCreditosConSusCuotas(bonDetalles);
		Objetoi credito;
		Cuota cuota;
		ObjetoiDTO dto;
		for (Object[] credito_cuota : creditos_cuotas) {
			credito = (Objetoi) credito_cuota[0];
			cuota = (Cuota) credito_cuota[1];
			dto = new ObjetoiDTO(credito, "", 0, cuota);
			agregarMontosMensuales(dto, calcularMontosMensuales(calendar, cuota));
		}
		calcularTotales();
	}

	public void calcularTotales() {
		ObjetoiDTO total = new ObjetoiDTO();
		total.getTitular().setCuil12(null);
		total.setNumeroCuota("TOTALES");
		total.llenarMontosVacios();
		int i;
		double subTotal;
		double montoMensual;
		for (ObjetoiDTO dto : creditos) {
			for (i = mesDesde; i <= mesHasta; i++) {
				subTotal = dto.getMontosBonDetalleMensual().get(i);
				montoMensual = total.getMontosBonDetalleMensual().get(i);
				total.getMontosBonDetalleMensual().put(i, subTotal + montoMensual);
			}
		}
		total.armarBonDetalles(Meses.getMeses(mesDesde, mesHasta));
		bonDetalleDTOs.addAll(total.getBonDetalleDTOs());
		creditos.add(total);
	}

	public Date desplazarMeses(Calendar calendar, int meses) {
		calendar.add(Calendar.MONTH, meses);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> listarCreditosConSusCuotas(List<BonDetalle> bonDetalles) {
		if (!bonDetalles.isEmpty()) {
			return bp
					.createQuery("SELECT DISTINCT cre, cu FROM BonDetalle b JOIN b.cuota cu "
							+ "JOIN cu.credito cre WHERE b IN (:bonDetalles) ORDER BY cre.expediente, cu.numero")
					.setParameterList("bonDetalles", bonDetalles).list();
		} else {
			return new ArrayList<Object[]>();
		}
	}

	public List<Object[]> calcularMontosMensuales(Calendar calendar, Cuota cuota) {
		List<Object[]> montos = new ArrayList<Object[]>();
		Object[] mes_monto;
		Date desde;
		Date hasta;
		Double monto;
		for (int mes = mesDesde; mes <= mesHasta; mes++) {
			mes_monto = new Object[2];
			calendar.set(ejercicio.getEjercicio().intValue(), mes, 1, 0, 0, 0);
			desde = calendar.getTime();
			hasta = desplazarMeses(calendar, 1);
			monto = (Double) bp
					.createQuery("SELECT SUM(b.monto) FROM BonDetalle b JOIN b.cuota c WHERE c = :cuota "
							+ "AND b.fechaBonificada BETWEEN :desde AND :hasta")
					.setEntity("cuota", cuota).setDate("desde", desde).setDate("hasta", hasta).uniqueResult();
			if (monto == null) {
				monto = 0.0;
			}
			mes_monto[0] = mes;
			mes_monto[1] = monto;
			montos.add(mes_monto);
		}
		return montos;
	}

	public void agregarMontosMensuales(ObjetoiDTO dto, List<Object[]> montos) {
		for (Object[] monto : montos) {
			Integer mes = (Integer) monto[0];
			Double montoMes = (Double) monto[1];
			dto.getMontosBonDetalleMensual().put(mes, montoMes);
		}
		dto.armarBonDetalles(Meses.getMeses(mesDesde, mesHasta));
		creditos.add(dto);
		bonDetalleDTOs.addAll(dto.getBonDetalleDTOs());
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Bancos getEnte() {
		if (this.ente == null)
			this.ente = new Bancos();
		return ente;
	}

	public void setEnte(Bancos ente) {
		this.ente = ente;
	}

	public Ejercicio getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Ejercicio ejercicio) {
		this.ejercicio = ejercicio;
	}

	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}

	public Integer getMesDesde() {
		return mesDesde;
	}

	public void setMesDesde(Integer mesDesde) {
		this.mesDesde = mesDesde;
	}

	public Integer getMesHasta() {
		return mesHasta;
	}

	public void setMesHasta(Integer mesHasta) {
		this.mesHasta = mesHasta;
	}

	public List<BonDetalleDTO> getBonDetalleDTOs() {
		return bonDetalleDTOs;
	}

	public void setBonDetalleDTOs(List<BonDetalleDTO> bonDetalleDTOs) {
		this.bonDetalleDTOs = bonDetalleDTOs;
	}

	public String getMesDesdeStr() {
		return Meses.getMes(mesDesde).name();
	}

	public String getMesHastaStr() {
		return Meses.getMes(mesHasta).name();
	}
}
