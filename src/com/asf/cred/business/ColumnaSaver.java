package com.asf.cred.business;


import com.asf.cred.Constants;

import com.asf.security.BusinessPersistance;
import com.asf.hibernate.mapping.*;
import com.asf.security.SessionHandler;
import java.util.HashMap;

public class ColumnaSaver 
{
    
    private HashMap<String,Object> chErrores = new HashMap<String,Object>();
    private Columna[] columnas;
    private Reporte reporte;
    
    public boolean excecute(){
        BusinessPersistance bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        
        if(!this.reporte.validate()){
            this.chErrores.putAll(reporte.getChErrores());
        }
        
        if( columnas!=null ){
            for (int i = 0; i < columnas.length; i++) {
                Columna columna = this.columnas[i];
                if(!columna.validate()){
                    this.chErrores.putAll(columna.getChErrores());
                }
            }
        }
        
        if(this.chErrores.size()>0){
            return false;
        }
        
        try{
            bp.begin();
            bp.saveOrUpdate(reporte);
            bp.getCurrentSession().flush();
            bp.getCurrentSession().refresh( reporte );
            
            if( columnas!=null ){
                for (int i = 0; i < this.columnas.length; i++) {
                    Columna columna = this.columnas[i];
                    if("undefined".equals(columna.getId())){
                    	columna.setId(null);
                    }
                    columna.setIdreporte(reporte.getIdreporte());
                    if(columna.getComparador()==null){
                        columna.setComparador(new Long(0));
                    }
                    bp.saveOrUpdate(columna);
                }
            }
            
            crearOpcionMenu( reporte, bp );
            
            bp.commit();
        }catch( Exception e ){
            bp.rollback();
            e.printStackTrace();
            chErrores.put("columnasaver.execute","columnasaver.execute");
            return false;
        }
        return true;
    }
    
    private void crearOpcionMenu( Reporte reporte, BusinessPersistance bp ){
        //select * from menu where ruta like '%SQLReport%' and idmodulo = 3 and activo = 0
        
    	if( new Integer(bp.createQuery( "SELECT COUNT(m) FROM Menu m WHERE m.ruta LIKE 'actions/SQLReport.do?%' AND " +
    			"(m.ruta LIKE '%idReporte=" + reporte.getIdreporte() + "' OR m.ruta LIKE '%idReporte=" + reporte.getIdreporte() + "&%') " +
                " AND m.idmodulo = " + SessionHandler.getCurrentSessionHandler().getIDMODULO()).uniqueResult().toString()).intValue()==0 ){
    		Menu menu=new Menu();        
            menu.setIdmodulo( new Long( SessionHandler.getCurrentSessionHandler().getIDMODULO() ) );
            menu.setIsRaiz( Boolean.FALSE );            
            menu.setMenu(new Menu("1"));
            menu.setIdpadre(menu.getMenu().getId());
            menu.setNombre( reporte.getTitulo() );
            menu.setActivo( new Long( 0 ) );
            menu.setRuta( "actions/SQLReport.do?idReporte=" + reporte.getIdreporte() + "&do=select" ); // idem comentario anterior.
            menu.setTipo( new Long( 0 ) );
            menu.setAyuda( null );
            bp.getCurrentSession().evict(menu.getMenu());
            bp.save( menu );
            bp.getCurrentSession().evict(menu.getMenu());            
        }
        
    }
    
    public void setColumnas(Columna[] columnas) {
        this.columnas = columnas;
    }
    
    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    }
    
    public HashMap<String,Object> getErrores(){
        return this.chErrores;
    }
}
