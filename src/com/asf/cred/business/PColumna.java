package com.asf.cred.business;

import com.asf.cred.business.StrutsArray;

import java.util.*;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.hibernate.mapping.*;


public class PColumna implements com.asf.struts.action.IProcess{
    private BusinessPersistance Bp = null;	
    private HashMap errores = null;
    private StrutsArray columnas = new StrutsArray( Columna.class  );
    private Reporte reporte=null;
    private Columna[] oColumna=null;
    private List listaReporte = new ArrayList();
    private String action="list";
    private String idreporte;
    private boolean cbOk = false;

    public PColumna() {
        this.Bp = SessionHandler.getSessionHandler().getBusinessPersistance();
        this.errores = new HashMap();
        reporte=new Reporte();
    }

    public boolean doProcess(){
        if( this.action.equals("save") ){
            ColumnaSaver saver = new ColumnaSaver();
            saver.setReporte(this.reporte);
            saver.setColumnas(this.oColumna);
            this.cbOk = saver.excecute();
            this.errores.putAll( saver.getErrores() );
            return this.cbOk;
        }else if( this.action.equals("load") ){
            this.listaReporte=Bp.getByFilter("FROM Reporte r where r.idreporte='"+this.idreporte+"'");
            for (Iterator it = listaReporte.iterator(); it.hasNext();) {
                Reporte elem = (Reporte)it.next();
                this.reporte.setIdreporte(elem.getIdreporte());
                this.reporte.setTitulo(elem.getTitulo());
                this.reporte.setSubtitulo(elem.getSubtitulo());
                this.reporte.setSql(elem.getSql());
            }
            this.columnas.addAll( (ArrayList)this.Bp.getByFilter("FROM Columna c where c.idreporte='"+this.idreporte+"'") );
            return true;
        }else if( this.action.equals("delete") ){
            this.Bp.delete(Reporte.class,this.idreporte);
            this.idreporte=null;
            return true;
        }else{
            if( this.idreporte!=null ){
                this.listaReporte=this.Bp.getByFilter("FROM Reporte r where r.idreporte='"+this.idreporte+"'");
            }
            return true;
        }
    }

    public String getForward(){
        if( this.action.equals("new") || this.action.equals("load") )
            return "PColumnas";
        else
            return "PColumnasList";
    }

    public String getInput(){
        return "PColumnas";
    }
          
    public boolean validate(){
        if(oColumna==null&&this.columnas.size()>0){
            this.setItems(this.columnas);
        }

        if( this.action.equals("save") && this.reporte.getTitulo()==null ){
            this.errores.put("reporte.titulo","reporte.titulo");
        }

        if( this.action.equals("save") && this.reporte.getTipo().longValue() > 0 && this.reporte.getSubtitulo()==null ){
            this.errores.put("reporte.subtitulo","reporte.subtitulo");
        }

        return this.errores.isEmpty();
    }
        
    public Object getResult(){
        return this.listaReporte;
    }

    public HashMap getErrors(){
        return this.getErrores();
    }

    private HashMap getErrores() {
        return this.errores;
    }

    public StrutsArray getItems() {
        return columnas;
    }

    public void setItems(StrutsArray columnas) {
        this.columnas = columnas;
        Columna aColumna = null;        
        
        int r = 0;
        int a = 0;
        for ( int i = 0; i < this.columnas.size(); i++) {
            aColumna = (Columna) this.columnas.get(i);
            if ( aColumna.getIdcolumna() == null )
                r++;
        }
        oColumna = new Columna[this.columnas.size()-r];
        for ( int i = 0; i < this.columnas.size(); i++) {
            aColumna = (Columna) this.columnas.get(i);
            if ( aColumna.getIdcolumna() != null ){
            	if(aColumna.getComparador()==null){
            		aColumna.setComparador(new Long(0));
                }
                oColumna[a] = aColumna;
                a++;
            }
        }
    }

    public Reporte getReporte() {
        return reporte;
    }

    public void setReporte(Reporte reporte) {
        this.reporte = reporte;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdreporte() {
        return idreporte;
    }

    public void setIdreporte(String idreporte) {
        this.idreporte = idreporte;
    }

    public List getListaReporte() {
        return listaReporte;
    }

    public void setListaReporte(List listaReporte) {
        this.listaReporte = listaReporte;
    }
}    
