package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.asf.gaf.DirectorHandler;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
import com.asf.util.EmpresaHelper;
import com.asf.util.ReportResult;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.cred.beans.BeanCuota;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Notificacion;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.SolicitudProrroga;
import com.nirven.creditos.hibernate.SolicitudProrrogaEstado;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

public class CreditoCuotas extends CreditoProcess {

	private List<Cuota> cuotas;
	private double desembolsado;
	private String desembolsadoStr;

	private Long[] ids;
	private String[] fechas;
	private String[] montos;
	private String[] porcentajes;
	private Boolean[] editables;
	private int indice;
	private double capitalModificado;

	private Double importe = 0.0;
	private Double porcentaje = 0.0;
	private Double capital = 0.0;
	private Double moratorio = 0.0;
	private Double compensatorio = 0.0;
	private Double punitorio = 0.0;
	private Double bonif = 0.0;
	private Double gastos = 0.0;
	private Double multas = 0.0;
	private boolean error;
	private String detalleError;
	private boolean ocultarCampos;
	private String estadoAcuerdo;

	private Long nroComprobante;
	private Boleto boleto;
	private Objetoi objetoi;
	private Long idCuota;
	private double diferencia;

	private ReportResult reportResult;
	private List<ProrrogaBean> prorrogas;
	private SolicitudProrroga solicitud;

	public CreditoCuotas() {
		ids = new Long[50];
		fechas = new String[50];
		montos = new String[50];
		porcentajes = new String[50];
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		super.setForward("CreditoCuotas");

		if (getIdObjetoi() != null) {
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("CUOTA_OBJETOI",
					getIdObjetoi());
		} else {
			Long id = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("CUOTA_OBJETOI");
			setIdObjetoi(id);
		}
		try {
			estadoAcuerdo = DirectorHandler.getDirector("estado.ObjetoOriginal").getValor();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			buscarObjetoi();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (getObjetoi() != null) {
			if (getObjetoi().getEstadoActual().getEstado().getId().toString().equalsIgnoreCase(estadoAcuerdo)) {
				ocultarCampos = true;
			} else {
				ocultarCampos = false;
			}
		}
		if (action == null || action.equals("load")) {
			super.doProcess();
			buscarDesembolsado();
			buscarCuotas();
			inicializarArreglos();
			buscarProrrogas();
		} else if (action.equals("save") || action.equals("savePorcentajes")) {
			solicitud = (SolicitudProrroga) bp
					.createQuery("select s from SolicitudProrrogaEstado spe inner join spe.solicitudProrroga s "
							+ "where s.objetoi.id = :idCredito and spe.estado.nombreEstado = :nombreEstado and spe.fechaHasta is null")
					.setParameter("idCredito", getIdObjetoi()).setParameter("nombreEstado", "APROBADA").uniqueResult();
			if (solicitud != null) {
				SolicitudProrrogaEstado estadoActual = solicitud.getEstadoActualProrroga();
				if (estadoActual.getEstado().getNombreEstado().equals("APROBADA")) {
					buscarDesembolsado();
					boolean ok = guardarCuotas();
					buscarCuotas();
					if (ok) {
						inicializarArreglos();
						buscarProrrogas();
						verificarCambioEstadoProrroga();
						SessionHandler.getCurrentSessionHandler().getRequest().getSession()
								.setAttribute("CreditoCuotas.cuotas", null);
					}
					return ok;
				} else {
					errores.put("objetoi.cuotas.prorrogaAprobada", "objetoi.cuotas.prorrogaAprobada");
					super.doProcess();
					buscarDesembolsado();
					buscarCuotas();
					inicializarArreglos();
					buscarProrrogas();
					return errores.isEmpty();
				}
			} else {
				buscarDesembolsado();
				boolean ok = guardarCuotas();
				buscarCuotas();
				if (ok) {
					inicializarArreglos();
					buscarProrrogas();
					verificarCambioEstadoProrroga();
					SessionHandler.getCurrentSessionHandler().getRequest().getSession()
							.setAttribute("CreditoCuotas.cuotas", null);
				}
				return ok;
			}

		} else if (action.equals("corregirCapital")) {
			corregirCapital();
		} else if (action.equals("imprimirReporte")) {
			String reporte = "InformeCuota";
			getReportResult(reporte);
			if (this.errores.isEmpty()) {
				setForward("ReportesProcessFrame");
			} else {
				setForward("errorBlancoPage");
			}

		} else if (action != null & action.equals("imprimirBoleto")) {
			imprimirBoletos();
			if (this.errores.isEmpty()) {
				setForward("ReporteCuota");
			} else {
				setForward("errorBlancoPage");
			}

		} else if (action.equals("actualizarCapital")) {
			super.doProcess();
			buscarDesembolsado();
			cuotas = (List<Cuota>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("CreditoCuotas.cuotas");
			if (cuotas == null) {
				buscarCuotas();
			} else {
				actualizarEditables();
			}
			inicializarArreglos();
			actualizarCapital();
		} else if (action.equals("cancelar")) {
			super.doProcess();
			buscarDesembolsado();
			buscarCuotas();
			inicializarArreglos();
			buscarProrrogas();
			SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("CreditoCuotas.cuotas",
					null);
		} else if (action.equals("imprimirBoletoResumen")) {
			imprimirBoletoResumen();
			if (this.errores.isEmpty()) {
				setForward("ReporteCuota");
			} else {
				setForward("errorBlancoPage");
			}
		}
		return true;
	}

	private void actualizarCapital() {
		Cuota cuota = cuotas.get(indice);
		cuota.setCapital(capitalModificado);
		cuotas.remove(cuotas.size() - 1);
		cuotas.add(calcularTotales(cuotas));
		double capital;
		for (int i = 0; i < cuotas.size(); i++) {
			capital = cuotas.get(i).getCapital();
			montos[i] = String.format("%.2f", capital);
			porcentajes[i] = String.format("%.2f", capital / desembolsado * 100);
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("CreditoCuotas.cuotas",
				cuotas);
	}

	private Cuota calcularTotales(List<Cuota> cuotas) {
		double capital = 0.0;
		double moratorio = 0.0;
		double compensatorio = 0.0;
		double punitorio = 0.0;
		double bonif = 0.0;
		double gastos = 0.0;
		double multas = 0.0;
		for (Cuota cuota : cuotas) {
			capital += cuota.getCapital();
			moratorio += cuota.getMoratorio();
			compensatorio += cuota.getCompensatorio();
			punitorio += cuota.getPunitorio();
			bonif += cuota.getBonificacion();
			gastos = cuota.getGastos();
			multas = cuota.getMultas();
		}
		Cuota c = new Cuota();
		c.setCapital(capital);
		c.setMoratorio(moratorio);
		c.setCompensatorio(compensatorio);
		c.setPunitorio(punitorio);
		c.setBonificacion(bonif);
		c.setGastos(gastos);
		c.setMultas(multas);
		c.setNumero(0);
		return c;
	}

	private boolean actualizarEditables() {
		if (cuotas.size() > 0) {
			// 1 extra para la de sumas
			editables = new Boolean[cuotas.size() + 1];
			for (int i = 0; i < editables.length; i++) {
				// todas son editables, excepto la ultima que corresponde a los totales
				editables[i] = !(i == editables.length - 1);
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public void imprimirBoletos() {
		try {
			List<Bolcon> bolcons = bp.createQuery("SELECT b FROM Bolcon b WHERE b.id.boleto.id = :boleto ")
					.setParameter("boleto", nroComprobante).list();
			Double capital = 0.0;
			Double compensatorio = 0.0;
			Double punitorio = 0.0;
			Double moratorio = 0.0;
			for (Bolcon bolcon : bolcons) {
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_CAPITAL)) {
					capital = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_COMPENSATORIO)) {
					compensatorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_PUNITORIO)) {
					punitorio = bolcon.getImporte();
				}
				if (bolcon.getFacturado().getConcepto().getAbreviatura().equals(Concepto.CONCEPTO_MORATORIO)) {
					moratorio = bolcon.getImporte();
				}
			}

			boleto = (Boleto) bp.getById(Boleto.class, nroComprobante);
			objetoi = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName("boletosDePago.jasper");
			String params = "NOMBRE_EMPRESA=" + EmpresaHelper.getNombreEmpresa() + ";idObjetoi=" + objetoi.getId()
					+ ";CAPITAL=" + capital + ";MORATORIO=" + moratorio + ";PUNITORIO=" + punitorio + ";COMPENSATORIO="
					+ compensatorio + ";VENCIMIENTO=" + boleto.getFechaVencimientoStr() + ";idObjetoi=" + getIdObjetoi()
					+ ";idPersona=" + getObjetoi().getPersona_id() + ";EMISION=" + boleto.getFechaEmisionStr()
					+ ";MONEDA=" + objetoi.getLinea().getMoneda().getDenominacion() + ";SIMBOLO="
					+ objetoi.getLinea().getMoneda().getSimbolo() + ";NUMERO_BOLETO="
					+ boleto.getNumeroBoleto().toString();
			reportResult.setParams(params);
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletosDePago"));
		}
	}

	public void imprimirBoletoResumen() {
		objetoi = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		Cuota cuota = (Cuota) bp.getById(Cuota.class, getIdCuota());
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(getIdObjetoi(), cuota.getFechaVencimiento());
		calculoDeuda.calcular();
		List<BeanCtaCte> beans = calculoDeuda.getBeans();
		beans.remove(beans.size() - 1);
		double moratorio = 0.0;
		double punitorio = 0.0;
		String cuotasMoratorio = "";
		String cuotasPunitorio = "";
		for (BeanCtaCte bean : beans) {
			if (bean.getFechaVencimiento().before(calendar.getTime())
					&& bean.getNumero().intValue() < cuota.getNumero().intValue()) {
				if (bean.isCuota()) {
					moratorio += bean.getMoratorio().doubleValue();
					punitorio += bean.getPunitorio().doubleValue();
				} else {
					moratorio -= bean.getMoratorio().doubleValue();
					punitorio -= bean.getPunitorio().doubleValue();
				}
				if (cuotasMoratorio.indexOf(bean.getNumero().toString()) == -1) {
					if (cuotasMoratorio.isEmpty())
						cuotasMoratorio += bean.getNumero().toString();
					else
						cuotasMoratorio += " - " + bean.getNumero().toString();
				}
				if (cuotasPunitorio.indexOf(bean.getNumero().toString()) == -1) {
					if (cuotasPunitorio.isEmpty())
						cuotasPunitorio += bean.getNumero().toString();
					else
						cuotasPunitorio += " - " + bean.getNumero().toString();
				}
			}
		}
		if (objetoi.getLinea().getMoneda().getDenominacion().equals("Peso Argentino")) {
			try {
				boleto = (Boleto) bp.getById(Boleto.class, nroComprobante);
				String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
				reportResult = new ReportResult();
				reportResult.setExport("PDF");
				reportResult.setReportName("boletoResumen.jasper");
				String params = "NOMBRE_EMPRESA=" + EmpresaHelper.getNombreEmpresa() + ";OBJETOI_ID=" + objetoi.getId()
						+ ";idPersona=" + getObjetoi().getPersona_id() + ";CUOTA_ID=" + getIdCuota()
						+ ";VENCIMIENTO_CUOTA=" + cuota.getFechaVencimientoStr() + ";NUMERO_CUOTA=" + cuota.getNumero()
						+ ";FECHA_EMISION=" + cuota.getEmision().getFechaEmisionStr() + ";RELACION_COTOMADOR_ID="
						+ relacionCotomadorId + ";numeroBoleto=" + boleto.getNumeroBoleto() + ";MORATORIO=" + moratorio
						+ ";PUNITORIO=" + punitorio + ";CUOTAS_MORATORIO=" + cuotasMoratorio + ";CUOTAS_PUNITORIO="
						+ cuotasPunitorio + ";idBoletoResumen=" + boleto.getId();
				reportResult.setParams(params);
			} catch (Exception e) {
				e.printStackTrace();
				errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "boletoResumen"));
			}
		} else if (objetoi.getLinea().getMoneda().getDenominacion().equals("Dolar")) {
			try {
				boleto = (Boleto) bp.getById(Boleto.class, nroComprobante);
				String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
				reportResult = new ReportResult();
				reportResult.setExport("PDF");
				reportResult.setReportName("boletoResumenDolares.jasper");
				String params = "NOMBRE_EMPRESA=" + EmpresaHelper.getNombreEmpresa() + ";OBJETOI_ID=" + objetoi.getId()
						+ ";idPersona=" + getObjetoi().getPersona_id() + ";CUOTA_ID=" + getIdCuota()
						+ ";VENCIMIENTO_CUOTA=" + cuota.getFechaVencimientoStr() + ";NUMERO_CUOTA=" + cuota.getNumero()
						+ ";FECHA_EMISION=" + cuota.getEmision().getFechaEmisionStr() + ";RELACION_COTOMADOR_ID="
						+ relacionCotomadorId + ";numeroBoleto=" + boleto.getNumeroBoleto() + ";MORATORIO=" + moratorio
						+ ";PUNITORIO=" + punitorio + ";CUOTAS_MORATORIO=" + cuotasMoratorio + ";CUOTAS_PUNITORIO="
						+ cuotasPunitorio + ";idBoletoResumen=" + boleto.getId();
				reportResult.setParams(params);
			} catch (Exception e) {
				e.printStackTrace();
				errores.put("imprimir.informe.error",
						new ActionMessage("imprimir.informe.error", "boletoResumenDolares"));
			}
		}
	}

	private void getReportResult(String reporte) {
		try {
			reportResult = new ReportResult();
			reportResult.setExport("PDF");
			reportResult.setReportName(reporte + ".jasper");
			String params = "NOMBRE_EMPRESA=" + EmpresaHelper.getNombreEmpresa() + ";idObjetoi=" + getIdObjetoi();
			reportResult.setParams(params);
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", reporte));
		}
	}

	private void buscarDesembolsado() {
		Double suma = (Double) bp.createQuery("select sum(c.capital) from Cuota c where c.credito.id = :idCredito")
				.setLong("idCredito", getIdObjetoi()).uniqueResult();
		if (suma != null) {
			desembolsado = suma.doubleValue();
		}
		desembolsadoStr = String.format("%.2f", desembolsado);
	}

	@SuppressWarnings("unchecked")
	private boolean guardarCuotas() {
		Map<Long, BeanCuota> beans = new HashMap<Long, BeanCuota>();
		boolean hayProrroga;
		List<Cuota> cuotasProrrogadas = new ArrayList<Cuota>();

		com.nirven.creditos.hibernate.ProrrogaMasiva prorroga = new com.nirven.creditos.hibernate.ProrrogaMasiva();
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		prorroga.setUsuario(usuario);

		if (solicitud != null) {
			prorroga.setResolucion(solicitud.getNroResolucion());
			prorroga.setObservaciones(solicitud.getEstadoActualProrroga().getObservacion());
		}

		double totalMontos = 0;
		double totalPorcentajes = 0;

		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date hoy = c.getTime();

		for (int i = 0; i < ids.length; i++) {
			Long idCuota = ids[i];
			if (idCuota == null || idCuota == 0) {
				break;
			}

			BeanCuota bean = beans.get(idCuota);
			if (bean == null) {
				bean = new BeanCuota();
				beans.put(idCuota, bean);
			}

			Double monto = new Double(montos[i].replaceAll(",", "."));
			bean.setCapital(monto);
			Double porcentaje = new Double(porcentajes[i].replaceAll(",", "."));
			// uso saldoCapital para guardar el porcentaje
			bean.setSaldoCapital(porcentaje);
			if (fechas[i] != null) {
				bean.setFechaVencimientoStr(fechas[i]);
			}
			Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);

			if (!cuota.getFechaVencimiento().equals(bean.getFechaVencimiento())) {
				if (cuota.getFechaVencimiento().before(hoy) || cuota.getFechaVencimiento().equals(hoy)) {
					// fecha vencida solo se puede moverse a partir de maniana
					if (!bean.getFechaVencimiento().after(hoy)) {
						errores.put("objetoi.fechaCuotasVencida", "objetoi.fechaCuotasVencida");
					}
				} else if (cuota.getFechaVencimiento().after(hoy)) {
					// fecha futura solo se puede mover hasta hoy, no menos
					if (bean.getFechaVencimiento().before(hoy)) {
						errores.put("objetoi.fechaCuotas", "objetoi.fechaCuotas");
					}
					// no puede pasar de la siguiente cuota
					Long idCuotaSiguiente = ids[i + 1];
					if (idCuotaSiguiente != null && idCuotaSiguiente.longValue() != 0L) {
						Cuota cuotaSiguiente = (Cuota) bp.getById(Cuota.class, idCuotaSiguiente);
						if (bean.getFechaVencimiento().getTime() >= cuotaSiguiente.getFechaVencimiento().getTime()) {
							errores.put("objetoi.fechaCuotasSiguiente", "objetoi.fechaCuotasSiguiente");
						}
					}
				}
			}

			totalMontos += monto;
			totalPorcentajes += porcentaje;
		}

		// redondeo a 2 decimales
		totalPorcentajes = (int) (totalPorcentajes * 100) / 100.0;

		if (Math.abs(totalPorcentajes - 100) > 0.1) {
			log.info("totalPorcentajes " + totalPorcentajes);
			errores.put("objetoi.porcentajeCuotas", "objetoi.porcentajeCuotas");
		}

		boolean guardarMontos = !action.equals("savePorcentajes");

		if (guardarMontos) {
			if (Math.abs(totalMontos - desembolsado) >= 0.01) {
				log.info("totalMontos - desembolsado " + totalMontos + "-" + desembolsado + "="
						+ (totalMontos - desembolsado));
				errores.put("objetoi.montoCuotas", "objetoi.montoCuotas");
			}
		}

		ArrayList<Date> feriados = (ArrayList<Date>) bp.getByFilter("SELECT f.fecha FROM Feriado f");

		if (errores.isEmpty()) {

			for (int i = 0; i < ids.length; i++) {
				Long idCuota = ids[i];
				if (idCuota == null) {
					break;
				}

				Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);

				BeanCuota bean = beans.get(idCuota);
				if (bean != null) {

					Date fecVtoOriginal = cuota.getFechaVencimiento(); // prorroga

					Date fechaHabil = DateHelper.getDiaHabil(feriados, bean.getFechaVencimiento());
					cuota.setFechaVencimiento(fechaHabil);
					double diferencia = bean.getCapital() - cuota.getCapital();

					hayProrroga = false;

					if (DateHelper.getDiffDates(fecVtoOriginal, fechaHabil, 2) > 0) {
						cuota.setFechaVencimientoOriginal(fecVtoOriginal); // prorroga
						hayProrroga = true;
					}

					if (diferencia != 0) {
						cuota.setCapitalOriginal(cuota.getCapital()); // prorroga
						hayProrroga = true;
					}

					if (guardarMontos) {
						cuota.setCapital(bean.getCapital());
					} else {
						// guardar porcentajes
						// saldoCapital guarda el porcentaje
						double capital = bean.getSaldoCapital() / 100 * desembolsado;
						cuota.setCapital(capital);
					}

					if (hayProrroga) {
						cuotasProrrogadas.add(cuota);
						Notificacion notif = new Notificacion();
						notif.setCredito(cuota.getCredito());
						notif.setFechaCreada(prorroga.getFecha());
						if (solicitud != null) {
							notif.setObservaciones(solicitud.getEstadoActualProrroga().getObservacion()
									+ " - Resolución " + solicitud.getNroResolucion());
						} else {
							notif.setObservaciones("");
						}
						notif.setTipoAviso("prorrogaVencimiento");
						bp.save(notif);
					}
				}
			}

			// actualizar montos de interes en cuotas
			// asumir que el 100% fue desembolsado antes de la primera cuota
			List<Desembolso> desembolsos = bp.getNamedQuery("Desembolso.findByCredito")
					.setParameter("idCredito", getIdObjetoi()).list();
			Date fechaPrimerDesembolso = null;
			double totalDesembolsado = 0;
			for (Desembolso desembolso : desembolsos) {
				if (fechaPrimerDesembolso == null) {
					fechaPrimerDesembolso = desembolso.getFechaReal() != null ? desembolso.getFechaReal()
							: desembolso.getFecha();
				}
				totalDesembolsado += (desembolso.getImporteReal() != null) ? desembolso.getImporteReal()
						: desembolso.getImporte();
			}

			// si no hay desembolso, usar la primera cuota
			List<Cuota> cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", getIdObjetoi())
					.list();
			if (fechaPrimerDesembolso == null && !cuotas.isEmpty()) {
				fechaPrimerDesembolso = cuotas.get(0).getFechaVencimiento();
			}
			// primer vencimiento anterior es = desembolso
			CreditoHandler ch = new CreditoHandler();
			Date fechaAnterior = fechaPrimerDesembolso;

			double saldoCapital = totalDesembolsado;

			// Valores para actualizacion de CtaCte
			// - lista de importes de capital
			double[] importesCapital = new double[cuotas.size()];
			int ic = 0; // contador para guardar importes de cuotas
			// - lista de importes desembolsados
			List<Number> desembolsado = bp.createQuery(
					"select d.importeReal from Desembolso d where d.credito.id = :idCredito and (d.estado = '2' or d.estado = '5') "
							+ "order by d.numero")
					.setParameter("idCredito", getIdObjetoi()).list();
			double[] importesDesembolsos = new double[desembolsado.size()];
			int d = 0;
			for (Number n : desembolsado) {
				importesDesembolsos[d++] = n.doubleValue();
			}

			ObjetoiIndice tasaCompensatorio = credito.findTasaCompensatorio();
			for (Cuota cuota : cuotas) {

				int dias = (int) DateHelper.getDiffDates(fechaAnterior, cuota.getFechaVencimiento(), 2);
				int diasTransaccion = dias;
				int diasPeriodo = dias;
				// calculo de compensatorio, no estoy seguro que las fechas sean correctas
				double icb = credito.calcularCompensatorioNew(saldoCapital, diasPeriodo, diasTransaccion, new Date(),
						fechaAnterior, cuota.getFechaVencimiento());
				cuota.setCompensatorio(icb);
				/**
				 * los calculos se realizan con un valor precalculado de la tasa que no es igual
				 * al que se obtiene desde ObjetoiIndice, por lo tanto requiere invocar
				 * directamente al calculo de interes compuesto
				 */
				double tasaNeta = ch.calcularTasaNeta(getIdObjetoi(), cuota.getNumero());
				diasPeriodo = (tasaCompensatorio.getIndice().getDiasPeriodo() == null
						|| tasaCompensatorio.getIndice().getDiasPeriodo() == 0) ? dias
								: tasaCompensatorio.getIndice().getDiasPeriodo();
				double icn = CalculoInteresHelper.calcularInteresCompuesto(saldoCapital, tasaNeta, diasPeriodo,
						diasTransaccion);
				cuota.setBonificacion(icb - icn);

				double deuda = ch.calcularDeudaImponible(cuota);
				if (deuda > 0.01) {
					dias = (int) DateHelper.getDiffDates(cuota.getFechaVencimiento(), new Date(), 2);
					diasPeriodo = dias;
					diasTransaccion = dias;
					double im = credito.calcularMoratorioNew(deuda, diasPeriodo, diasTransaccion,
							cuota.getFechaVencimiento(), cuota.getFechaVencimiento(), new Date());
					double ip = credito.calcularPunitorioNew(deuda, diasPeriodo, diasTransaccion,
							cuota.getFechaVencimiento(), cuota.getFechaVencimiento(), new Date());
				}
				fechaAnterior = cuota.getFechaVencimiento();
				bp.saveOrUpdate(cuota);
				saldoCapital -= cuota.getCapital();
				importesCapital[ic++] = cuota.getCapital();
			}
			prorroga.setCuotas(cuotasProrrogadas);
			bp.save(prorroga);
			reasignarCtaCte(importesCapital, importesDesembolsos);
		}
		return errores.isEmpty();
	}

	/**
	 * Reasigna los importes de capital, luego de haber modificado los importes de
	 * las cuotas manualmente
	 * 
	 * @param importesCapital     los importes de capital ordenados por cuota
	 * @param importesDesembolsos los importes desembolsados ordenados por numero de
	 *                            desembolso
	 */
	@SuppressWarnings("unchecked")
	private void reasignarCtaCte(double[] importesCapital, double[] importesDesembolsos) {
		// se pone todo el capital en cero para reasignar
		List<Ctacte> ccsCapital = bp.createQuery(
				"select c from Ctacte c where c.id.objetoi.id = :idCredito and c.asociado.concepto.abreviatura = :capital and c.tipomov.abreviatura = :debito "
						+ "and c.desembolso is not null")
				.setParameter("idCredito", getIdObjetoi()).setParameter("capital", Concepto.CONCEPTO_CAPITAL)
				.setParameter("debito", Tipomov.TIPOMOV_DEBITO).list();
		for (Ctacte cc : ccsCapital) {
			cc.setImporte(0.0);
			bp.update(cc);
		}

		// busca la Ctacte generada por un desembolso para una cuota
		Query query = bp.createQuery(
				"select c from Ctacte c where c.cuota.numero = :numeroCuota and c.id.objetoi.id = :idCredito and c.desembolso.numero = :numeroDesembolso "
						+ "and c.asociado.concepto.abreviatura = :capital and c.tipomov.abreviatura = :debito")
				.setParameter("idCredito", getIdObjetoi()).setParameter("capital", Concepto.CONCEPTO_CAPITAL)
				.setParameter("debito", Tipomov.TIPOMOV_DEBITO);

		// recorrer importes de capital
		// se va restando lo asignado del mismo arreglo
		for (int i = 0; i < importesCapital.length; i++) {
			// recorrer importes desembolsados para ir restando de cada
			// desembolso
			// y asignar a la cuota actual
			// se va restando lo asignado del mismo arreglo
			for (int j = 0; j < importesDesembolsos.length; j++) {

				Ctacte cc = (Ctacte) query.setParameter("numeroCuota", i + 1).setParameter("numeroDesembolso", j + 1)
						.setMaxResults(1).uniqueResult();

				if (cc == null) {
					// cuota creada manualmente, no tiene debitos de capital
					// creo Ctacte correspondiente
					cc = new Ctacte();

					Tipomov tipoMov = (Tipomov) bp.createQuery("Select t from Tipomov t where t.abreviatura=:ab")
							.setParameter("ab", "db").uniqueResult();
					Usuario usuario = (Usuario) bp.getById(Usuario.class,
							SessionHandler.getCurrentSessionHandler().getCurrentUser());
					int periodo = Calendar.getInstance().get(Calendar.YEAR);
					String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
					long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
					long itemCtaCte = 1;
					Concepto concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL);

					Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero")
							.setParameter("idCredito", getIdObjetoi()).setParameter("nroCuota", i + 1).uniqueResult();

					Desembolso desembolso = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
							.setParameter("idCredito", getIdObjetoi()).setParameter("numero", j + 1).uniqueResult();

					cc.setImporte(0.0);
					cc.setAsociado(concepto);
					cc.setFacturado(concepto);
					cc.setTipomov(tipoMov);
					cc.setTipoMovimiento("cuota");
					cc.setFechaGeneracion(new Date(desembolso.getFechaReal().getTime()));
					cc.setFechaVencimiento(new Date(cuota.getFechaVencimiento().getTime()));
					cc.setCuota(cuota);
					cc.setUsuario(usuario);
					cc.setDesembolso(desembolso);

					// igualar la fecha de proceso a la del desembolso, para que
					// no se separe en la consulta de cta cte
					Date fechaProceso = (Date) bp
							.createSQLQuery("select min(cc.fechaProceso) from Ctacte cc where cc.desembolso_id = :id")
							.setParameter("id", desembolso.getId()).uniqueResult();
					if (fechaProceso != null) {
						cc.setFechaProceso(fechaProceso);
					}

					CtacteKey ck = new CtacteKey();
					ck.setPeriodoCtacte(new Long(periodo));
					ck.setObjetoi(cuota.getCredito());
					ck.setVerificadorCtacte(0L); // TODO: reemplazar con Digito
					// Verificador
					cc.setId(ck);

					cc.getId().setMovimientoCtacte(movimientoCtaCte);
					cc.getId().setItemCtacte(itemCtaCte++);

					CreditoHandler.cargarCotizacion(cc);

					cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_CUOTA_MANUAL);

					bp.save(cc);
				}

				if (cc != null) {
					if (importesCapital[i] <= importesDesembolsos[j]) {
						// el remanente de desembolso alcanza a cubrir todo el
						// capital que falta asignar
						// asigno todo el capital
						cc.setImporte(importesCapital[i]);
						cc.redondear();
						// resto del desembolso lo asignado
						importesDesembolsos[j] -= cc.getImporte();
						importesCapital[i] = 0; // dejo el capital en cero, no
						// queda nada por asignar
					} else {
						// el remanente de desembolso no cubre el capital que
						// falta asignar
						// asigno todo el remanente de desembolso
						cc.setImporte(importesDesembolsos[j]);
						cc.redondear();
						// resto de lo que falta asignar, el remanante de
						// desembolso completo
						importesCapital[i] -= cc.getImporte();
						// dejo el desembolso en cero, se ocupo todo el
						// remanente
						importesDesembolsos[j] = 0;
					}

					bp.update(cc);
				}

				// no queda nada por asignar
				if (importesCapital[i] == 0) {
					break; // break desembolso, next capital
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private boolean buscarCuotas() {
		cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setLong("idCredito", getIdObjetoi()).list();
		if (cuotas.size() > 0) {
			// 1 extra para la de sumas
			editables = new Boolean[cuotas.size() + 1];
			for (int i = 0; i < editables.length; i++) {
				// todas son editables, excepto la ultima que corresponde a los totales
				editables[i] = !(i == editables.length - 1);
			}
			cuotas.add(llenarSuma(cuotas));
		}
		return true;
	}

	private Cuota llenarSuma(List<Cuota> cuotas) {
		for (Cuota cuota : cuotas) {
			importe += cuota.getImporteCuota();
			capital += cuota.getCapital();
			moratorio += cuota.getMoratorio();
			compensatorio += cuota.getCompensatorio();
			punitorio += cuota.getPunitorio();
			bonif += cuota.getBonificacion();
			gastos = cuota.getGastos();
			multas = cuota.getMultas();
		}
		Cuota c = new Cuota();
		c.setCapital(capital);
		c.setMoratorio(moratorio);
		c.setCompensatorio(compensatorio);
		c.setPunitorio(punitorio);
		c.setBonificacion(bonif);
		c.setGastos(gastos);
		c.setMultas(multas);
		c.setNumero(0);
		return c;
	}

	private void inicializarArreglos() {
		ids = new Long[cuotas.size()];
		fechas = new String[cuotas.size()];
		montos = new String[cuotas.size()];
		porcentajes = new String[cuotas.size()];
		double capital;
		for (int i = 0; i < cuotas.size(); i++) {
			ids[i] = cuotas.get(i).getId();
			fechas[i] = cuotas.get(i).getFechaVencimientoStr();
			capital = cuotas.get(i).getCapital();
			montos[i] = String.format("%.2f", capital);
			porcentajes[i] = String.format("%.2f", capital / desembolsado * 100);
		}
	}

	@SuppressWarnings("unchecked")
	private void buscarProrrogas() {
		prorrogas = bp
				.createQuery("select new com.asf.cred.business.ProrrogaBean(p, c) "
						+ "from ProrrogaMasiva p join p.cuotas c where c.credito.id = :idCredito order by p.fecha")
				.setParameter("idCredito", getIdObjetoi()).list();
	}

	@SuppressWarnings("unchecked")
	private void corregirCapital() {
		List<Number> ids = bp.createSQLQuery("select distinct c.credito_id from Cuota c "
				+ "join (select cc.cuota_id idCuota, SUM(cc.importe) capitalcc "
				+ "from Ctacte cc join Concepto con on con.id = cc.asociado_id "
				+ "where con.concepto_concepto = 'cap' and cc.tipomov_id = 2 and cc.tipoMovimiento = 'cuota' "
				+ "group by cc.cuota_id) t on t.idCuota = c.id where abs(c.capital - t.capitalcc) <= :dif "
				+ "and abs(c.capital - t.capitalcc) > 0").setParameter("dif", diferencia).list();
		Query queryCuotas = bp.getNamedQuery("Cuota.findByObjetoi");
		Query queryDesembolsado = bp.createQuery(
				"select d.importeReal from Desembolso d where d.credito.id = :idCredito and (d.estado = '2' or d.estado = '5') "
						+ "order by d.numero");
		CreditoCuotas process = new CreditoCuotas();
		process.setAction("save");
		for (Number number : ids) {
			Long id = number.longValue();
			try {
				List<Cuota> cuotas = queryCuotas.setParameter("idCredito", id).list();
				double[] importesCapital = new double[cuotas.size()];
				int ic = 0;
				for (Cuota cuota : cuotas) {
					importesCapital[ic++] = cuota.getCapital();
				}
				List<Number> desembolsado = queryDesembolsado.setParameter("idCredito", id).list();
				double[] importesDesembolsos = new double[desembolsado.size()];
				int d = 0;
				for (Number n : desembolsado) {
					importesDesembolsos[d++] = n.doubleValue();
				}
				process.setIdObjetoi(id);
				process.reasignarCtaCte(importesCapital, importesDesembolsos);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void verificarCambioEstadoProrroga() {
		if (solicitud != null) {
			Estado estadoNuevo = (Estado) bp.getNamedQuery("Estado.findByNombreEstado")
					.setParameter("nombreEstado", "REGISTRADA").uniqueResult();
			SolicitudProrrogaEstado estadoActual = solicitud.getEstadoActualProrroga();
			if (!estadoActual.getEstado().getNombreEstado().equals("REGISTRADA")) {
				estadoActual.setFechaHasta(new Date());
				SolicitudProrrogaEstado solProEst = new SolicitudProrrogaEstado();
				solProEst.setEstado(estadoNuevo);
				solProEst.setFechaDesde(new Date());
				solProEst.setSolicitudProrroga(solicitud);
				solProEst.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
				bp.update(estadoActual);
				bp.saveOrUpdate(solProEst);
			}
		}
	}

	@Override
	public String getForward() {
		return super.getForward();
	}

	@Override
	public String getInput() {
		return super.getForward();
	}

	public List<Cuota> getCuotas() {
		return cuotas;
	}

	public void setCuotas(List<Cuota> cuotas) {
		this.cuotas = cuotas;
	}

	public double getDesembolsado() {
		return desembolsado;
	}

	public void setDesembolsado(double desembolsado) {
		this.desembolsado = desembolsado;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String[] getFechas() {
		return fechas;
	}

	public void setFechas(String[] fechas) {
		this.fechas = fechas;
	}

	public String[] getMontos() {
		return montos;
	}

	public void setMontos(String[] montos) {
		this.montos = montos;
	}

	public String[] getPorcentajes() {
		return porcentajes;
	}

	public void setPorcentajes(String[] porcentajes) {
		this.porcentajes = porcentajes;
	}

	@Override
	public Object getResult() {
		return reportResult;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Double getCapital() {
		return capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

	public Double getMoratorio() {
		return moratorio;
	}

	public void setMoratorio(Double moratorio) {
		this.moratorio = moratorio;
	}

	public Double getCompensatorio() {
		return compensatorio;
	}

	public void setCompensatorio(Double compensatorio) {
		this.compensatorio = compensatorio;
	}

	public Double getPunitorio() {
		return punitorio;
	}

	public void setPunitorio(Double punitorio) {
		this.punitorio = punitorio;
	}

	public Double getBonif() {
		return bonif;
	}

	public void setBonif(Double bonif) {
		this.bonif = bonif;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getDetalleError() {
		return detalleError;
	}

	public void setDetalleError(String detalleError) {
		this.detalleError = detalleError;
	}

	public boolean isOcultarCampos() {
		return ocultarCampos;
	}

	public void setOcultarCampos(boolean ocultarCampos) {
		this.ocultarCampos = ocultarCampos;
	}

	public Boolean[] getEditables() {
		return editables;
	}

	public Double getGastos() {
		return gastos;
	}

	public void setGastos(Double gastos) {
		this.gastos = gastos;
	}

	public Double getMultas() {
		return multas;
	}

	public void setMultas(Double multas) {
		this.multas = multas;
	}

	public String getEstadoAcuerdo() {
		return estadoAcuerdo;
	}

	public void setEstadoAcuerdo(String estadoAcuerdo) {
		this.estadoAcuerdo = estadoAcuerdo;
	}

	public Long getNroComprobante() {
		return nroComprobante;
	}

	public void setNroComprobante(Long nroComprobante) {
		this.nroComprobante = nroComprobante;
	}

	public Boleto getBoleto() {
		return boleto;
	}

	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	@Override
	public Objetoi getObjetoi() {
		return objetoi;
	}

	@Override
	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public void setEditables(Boolean[] editables) {
		this.editables = editables;
	}

	public void setReportResult(ReportResult reportResult) {
		this.reportResult = reportResult;
	}

	public String getDesembolsadoStr() {
		return desembolsadoStr;
	}

	public void setDesembolsadoStr(String desembolsadoStr) {
		this.desembolsadoStr = desembolsadoStr;
	}

	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}

	public double getCapitalModificado() {
		return capitalModificado;
	}

	public void setCapitalModificado(double capitalModificado) {
		this.capitalModificado = capitalModificado;
	}

	public Long getIdCuota() {
		return idCuota;
	}

	public void setIdCuota(Long idCuota) {
		this.idCuota = idCuota;
	}

	public List<ProrrogaBean> getProrrogas() {
		return prorrogas;
	}

	public void setDiferencia(double diferencia) {
		this.diferencia = diferencia;
	}

	public double getDiferencia() {
		return diferencia;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

}
