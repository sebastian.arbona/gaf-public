package com.asf.cred.business;

import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.DetalleVinedoINV;
import com.nirven.creditos.hibernate.ObjetoiVinedo;

public class EdicionQQ extends ConversationProcess {

	@Save
	private Long idVinedo;
	@Save
	private Long idObjetoi;
	private List<DetalleVinedoINV> detalles;

	@Save
	private Long idDetalle;
	private DetalleVinedoINV detalle = new DetalleVinedoINV();

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean listar() {
		ObjetoiVinedo ov = (ObjetoiVinedo) bp.getById(ObjetoiVinedo.class, idVinedo);
		idObjetoi = ov.getCredito().getId();

		detalles = bp.createQuery("select d from DetalleVinedoINV d where d.vinedo.id = :idVinedo")
				.setParameter("idVinedo", idVinedo).list();
		forward = "EdicionQQ";
		return true;
	}

	@ProcessMethod
	public boolean editar() {
		detalle = (DetalleVinedoINV) bp.getById(DetalleVinedoINV.class, idDetalle);
		forward = "EdicionQQDetalle";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardar() {
		DetalleVinedoINV detalleExistente = (DetalleVinedoINV) bp.getById(DetalleVinedoINV.class, idDetalle);
		detalleExistente.setHectareas(detalle.getHectareas());
		detalleExistente.setQq(detalle.getQq());
		bp.update(detalleExistente);

		listar();

		ObjetoiVinedo ov = detalleExistente.getVinedo();
		double qq = 0;
		for (DetalleVinedoINV d : detalles) {
			qq += d.getQq();
		}

		if (ov.getCredito().getDestino().equalsIgnoreCase("CRDA")) {
			ov.setQqAprobadoElab(qq);
		} else {
			ov.setQqAprobado(qq);
		}

		bp.update(ov);

		List<ObjetoiVinedo> ovs = bp.createQuery("select ov from ObjetoiVinedo ov where ov.vinedo.id = :id")
				.setParameter("id", ov.getVinedo().getId()).list();
		double hectareas = 0;
		for (ObjetoiVinedo v : ovs) {
			List<DetalleVinedoINV> ds = bp.createQuery("select d from DetalleVinedoINV d where d.vinedo.id = :idVinedo")
					.setParameter("idVinedo", v.getId()).list();
			for (DetalleVinedoINV d : ds) {
				hectareas += d.getHectareas();
			}
		}
		ov.getVinedo().setHectareas(hectareas);

		bp.update(ov.getVinedo());

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "EdicionQQ";
	}

	public Long getIdVinedo() {
		return idVinedo;
	}

	public void setIdVinedo(Long idVinedo) {
		this.idVinedo = idVinedo;
	}

	public List<DetalleVinedoINV> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetalleVinedoINV> detalles) {
		this.detalles = detalles;
	}

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}

	public DetalleVinedoINV getDetalle() {
		return detalle;
	}

	public void setDetalle(DetalleVinedoINV detalle) {
		this.detalle = detalle;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

}
