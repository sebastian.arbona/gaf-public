package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.Query;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.util.DateHelper;
import com.asf.util.DirectorHelper;
//import com.nirven.creditos.hibernate.BonDetalle;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Indice;
import com.nirven.creditos.hibernate.Linea;
//import com.nirven.creditos.hibernate.IndiceValor;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;

public class ActualizacionPorIndice extends ConversationProcess {
	private HashMap<String, Object> errores;
	private String forward;
	private String accion;
	private BusinessPersistance bp;
	private List<ObjetoiDTO> creditos;
	private ObjetoiDTO creditoDTO;
	private Objetoi credito;
	private ObjetoiDTO creditoSeleccionado;
	private Indice indice;
	private List<Cuota> cuotas;
	private double desembolsado;
	private String[] montos;
	private String[] porcentajes;
	private String[] diferencias;
	private String[] indices;
	private String actualizacion;
	private boolean puedeConfirmar;
	private String expediente;
	private List<ActualizacionIndiceBean> pagoDeCuotasPorIndice;
	private List<ActualizacionIndiceBean> resultadoCalculo;
	private Long idIndice;
	private Date fechaActualizacion;
	private boolean actualizarSaldo;
	private String lineasCER;

	public ActualizacionPorIndice() {
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		errores = new HashMap<String, Object>();
		creditoDTO = new ObjetoiDTO();
		indice = new Indice();
		creditoSeleccionado = (ObjetoiDTO) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("actualizacionPorIndiceCreditoSeleccionado");
		if (creditoSeleccionado == null)
			creditoSeleccionado = new ObjetoiDTO();
		lineasCER = DirectorHelper.getString(Linea.LINEA_CER);
		limpiar();
	}

	private void limpiar() {
		Integer cantidadCuotas = (Integer) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("actualizacionPorIndiceCantidadCuotas");
		if (cantidadCuotas != null) {
			montos = new String[cantidadCuotas];
			porcentajes = new String[cantidadCuotas];
			diferencias = new String[cantidadCuotas];
			indices = new String[cantidadCuotas];
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		if (accion == null) {
			creditos = (List<ObjetoiDTO>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.getAttribute("actualizacionPorIndiceCreditos");
			forward = "ActualizacionPorIndiceList";
			return false;
		}
		if (accion.equals("listar")) {
			listar();
			forward = "ActualizacionPorIndiceList";
		}
		if (accion.equals("calcular")) {
			calcular();
			forward = "ActualizacionPorIndice";
		}
		if (accion.equals("actualizar")) {
			actualizar();
			forward = "ActualizacionPorIndice";
		}
		return errores.isEmpty();
	}

	public void listar() {
		List<Objetoi> objetos = filtrar();
		creditos = new ArrayList<ObjetoiDTO>();
		ObjetoiEstado objetoiEstado;
		ObjetoiDTO credito;
		for (Objetoi o : objetos) {
			o = (Objetoi) bp.getById(Objetoi.class, o.getId());
			objetoiEstado = o.getEstadoActual();
			if (objetoiEstado != null && objetoiEstado.getEstado() != null) {
				credito = new ObjetoiDTO(o, objetoiEstado.getEstado().getNombreEstado(), 0);
				creditos.add(credito);
			} else {
				credito = new ObjetoiDTO(o, "Sin estado", 0);
				creditos.add(credito);
			}
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("actualizacionPorIndiceCreditos", creditos);
	}

	@SuppressWarnings("unchecked")
	public List<Objetoi> filtrar() {
		String consulta = "SELECT c FROM Objetoi c";
		List<Object> parametros = new ArrayList<Object>();
		if (creditoDTO.getNumeroAtencion().longValue() != 0) {
			consulta += " WHERE c.numeroAtencion = ?";
			parametros.add(creditoDTO.getNumeroAtencion());
		}
		if (creditoDTO.getTitular().getId().longValue() != 0) {
			if (parametros.isEmpty())
				consulta += " WHERE c.persona.id = ?";
			else
				consulta += " AND c.persona.id = ?";
			parametros.add(creditoDTO.getTitular().getId());
		}
		if (creditoDTO.getIdEstado().longValue() != 0) {
			if (parametros.isEmpty())
				consulta += " WHERE c IN(SELECT o FROM ObjetoiEstado oe JOIN oe.objetoi o "
						+ "WHERE oe.fechaHasta IS NULL " + "AND oe.estado.id = ?)";
			else
				consulta += " AND c IN(SELECT o FROM ObjetoiEstado oe JOIN oe.objetoi o "
						+ "WHERE oe.fechaHasta IS NULL " + "AND oe.estado.id = ?)";
			parametros.add(creditoDTO.getIdEstado());
		}
		if (creditoDTO.getIdLinea().longValue() != 0) {
			if (parametros.isEmpty())
				consulta += " WHERE c.linea.id = ?";
			else
				consulta += " AND c.linea.id = ?";
			parametros.add(creditoDTO.getIdLinea());
		}
		if (expediente != null && !expediente.equals("")) {
			if (parametros.isEmpty())
				consulta += " WHERE c.expediente = ?";
			else
				consulta += " AND c.expediente = ?";
			parametros.add(expediente);
		}
		consulta += " ORDER BY c.persona.nomb12";
		Query query = bp.createQuery(consulta);
		for (int i = 0; i < parametros.size(); i++) {
			query.setParameter(i, parametros.get(i));
		}
		return query.list();
	}

	public void load() {
		credito = (Objetoi) bp.getById(Objetoi.class, creditoDTO.getIdObjetoi());
		creditoSeleccionado = new ObjetoiDTO(credito, creditoDTO.getEstado(), 0.0);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("actualizacionPorIndiceCreditoSeleccionado", creditoSeleccionado);
	}

	public void calcular() {
		if (indice.getId() == null || indice.getId().longValue() == 0) {
			errores.put("actualizacionPorIndice.indice", "actualizacionPorIndice.indice");
			return;
		}
		if (actualizacion == null || actualizacion.isEmpty()) {
			errores.put("actualizacionPorIndice.actualizacion", "actualizacionPorIndice.actualizacion");
			return;
		}

		fechaActualizacion = DateHelper.getDate(actualizacion);
		idIndice = indice.getId();

		resultadoCalculo = new ArrayList<ActualizacionIndiceBean>();

		CreditoHandler ch = new CreditoHandler();

		String[] creditosSeleccionados = SessionHandler.getCurrentSessionHandler().getRequest()
				.getParameterValues("creditoseleccionado");

		ProgressStatus.abort = false;
		ProgressStatus.iTotal = creditosSeleccionados.length;
		ProgressStatus.onProcess = true;
		ProgressStatus.iProgress = 0;
		ProgressStatus.sStatus = "Calculando";

		for (String creditoId : creditosSeleccionados) {
			Long id = new Long(creditoId);
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, id);

			List<ActualizacionIndiceBean> resultado = ch.actualizacionIndice(credito.getNumeroAtencion(),
					indice.getId(), DateHelper.getDate(actualizacion), false);
			for (ActualizacionIndiceBean b : resultado) {
				b.setCredito(credito);
			}

			resultadoCalculo.addAll(resultado);

			ProgressStatus.iProgress++;
		}

		getConversation().put("resultadoCalculo", resultadoCalculo);
		getConversation().put("idIndice", idIndice);
		getConversation().put("fechaActualizacion", fechaActualizacion);

		ProgressStatus.iTotal = 0;
		ProgressStatus.iProgress = 0;
		ProgressStatus.onProcess = false;
		ProgressStatus.abort = true;
	}

	@SuppressWarnings("unchecked")
	private void calcularPagosDeCuotas() {
		String consulta;
		Object[] filaObject;
		String cantidadDecimales;
		boolean sinIndice;
		double sumaPagos;
		double sumaPagosCER;
		String fechaGeneracion;

		consulta = "SELECT dir.valor FROM director dir WHERE id = 'PrecDeci';";
		cantidadDecimales = (String) this.bp.createSQLQuery(consulta).list().get(0);

		fechaGeneracion = this.actualizacion.substring(this.actualizacion.lastIndexOf("/") + 1) + "-"
				+ this.actualizacion.substring(this.actualizacion.indexOf("/") + 1, this.actualizacion.lastIndexOf("/"))
				+ "-" + this.actualizacion.substring(0, this.actualizacion.indexOf("/"));

		consulta = "SELECT cuo.numero, cc.fechageneracion, cc.importe, indv.valor, "
				+ "(CASE WHEN indv.valor IS NOT NULL THEN (indv.valor * cc.importe) - cc.importe ELSE null END) 'importeCER' "
				+ "FROM ctacte cc " + "JOIN objetoi obj ON obj.id = cc.objetoi_id "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN('cap') "
				+ "JOIN cuota cuo ON cuo.id = cc.cuota_id "
				+ "LEFT JOIN indicevalor indv ON indv.fecha = DATEADD(dd, -1, cc.fechageneracion) "
				+ "AND indv.indice_id = " + this.indice.getId() + "WHERE obj.numeroAtencion = "
				+ this.creditoSeleccionado.getNumeroAtencion() + " AND cc.tipomov_id = 1 "
				+ "AND cc.fechageneracion <= '" + fechaGeneracion + "' " + "ORDER BY cuo.numero, cc.fechageneracion;";

		List<Object[]> pagosList = this.bp.createSQLQuery(consulta).list();

		sinIndice = false;
		sumaPagos = 0;
		sumaPagosCER = 0;
		for (int i = 0; i < pagosList.size(); i++) {
			filaObject = (Object[]) pagosList.get(i);

			if (filaObject[4] != null) {
				filaObject[1] = DateHelper.getString((Date) filaObject[1]);

				sumaPagos = sumaPagos + (Double) filaObject[2];
				sumaPagosCER = sumaPagosCER + (Double) filaObject[4];

				filaObject[2] = String.format("%." + cantidadDecimales + "f", filaObject[2]);
				filaObject[4] = String.format("%." + cantidadDecimales + "f", filaObject[4]);
			} else {
				i = pagosList.size();
				sinIndice = true;
			}
		}

		if (sinIndice) {
			pagosList = null;
			errores.put("actualizacionPorIndice.sinIndice", "actualizacionPorIndice.sinIndice");
		} else {
			filaObject = new Object[5];
			filaObject[0] = "TOTAL";
			filaObject[1] = "";
			filaObject[2] = String.format("%." + cantidadDecimales + "f", sumaPagos);
			filaObject[3] = "";
			filaObject[4] = String.format("%." + cantidadDecimales + "f", sumaPagosCER);

			pagosList.add(filaObject);

			filaObject = new Object[5];
			filaObject[0] = "Incremento";
			filaObject[1] = "";
			filaObject[2] = "";
			filaObject[3] = "%";
			filaObject[4] = String.format("%." + cantidadDecimales + "f", sumaPagosCER * 100 / sumaPagos);

			pagosList.add(filaObject);
		}
	}

	@SuppressWarnings("unchecked")
	private void actualizar() {
		resultadoCalculo = (List<ActualizacionIndiceBean>) getConversation().get("resultadoCalculo");
		idIndice = (Long) getConversation().get("idIndice");
		fechaActualizacion = (Date) getConversation().get("fechaActualizacion");

		CreditoHandler ch = new CreditoHandler();

		Set<Long> ids = new TreeSet<Long>();
		for (ActualizacionIndiceBean b : resultadoCalculo) {
			ids.add(b.getCredito().getId());
		}

		ProgressStatus.abort = false;
		ProgressStatus.iTotal = ids.size();
		ProgressStatus.onProcess = true;
		ProgressStatus.iProgress = 0;
		ProgressStatus.sStatus = "Impactando en Cuenta Corriente";

		for (Long id : ids) {
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, id);

			ch.actualizacionIndice(credito.getNumeroAtencion(), idIndice, fechaActualizacion, true);

			ProgressStatus.iProgress++;
		}

		errores.put("actualizacionPorIndice.impactoOk", "actualizacionPorIndice.impactoOk");

		resultadoCalculo = new ArrayList<ActualizacionIndiceBean>();
		getConversation().remove("resultadoCalculo");

		ProgressStatus.iTotal = 0;
		ProgressStatus.iProgress = 0;
		ProgressStatus.onProcess = false;
		ProgressStatus.abort = true;
	}

	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public void setCredito(Objetoi credito) {
		this.credito = credito;
	}

	public ObjetoiDTO getCreditoDTO() {
		return creditoDTO;
	}

	public void setCreditoDTO(ObjetoiDTO creditoDTO) {
		this.creditoDTO = creditoDTO;
	}

	public ObjetoiDTO getCreditoSeleccionado() {
		return creditoSeleccionado;
	}

	public void setCreditoSeleccionado(ObjetoiDTO creditoSeleccionado) {
		this.creditoSeleccionado = creditoSeleccionado;
	}

	public Indice getIndice() {
		return indice;
	}

	public void setIndice(Indice indice) {
		this.indice = indice;
	}

	public List<Cuota> getCuotas() {
		return cuotas;
	}

	public void setCuotas(List<Cuota> cuotas) {

		this.cuotas = cuotas;
	}

	public double getDesembolsado() {
		return desembolsado;
	}

	public void setDesembolsado(double desembolsado) {
		this.desembolsado = desembolsado;
	}

	public String[] getMontos() {
		return montos;
	}

	public void setMontos(String[] montos) {
		this.montos = montos;
	}

	public String[] getPorcentajes() {
		return porcentajes;
	}

	public void setPorcentajes(String[] porcentajes) {
		this.porcentajes = porcentajes;
	}

	public String[] getDiferencias() {
		return diferencias;
	}

	public void setDiferencias(String[] diferencias) {
		this.diferencias = diferencias;
	}

	public String getActualizacion() {
		return actualizacion;
	}

	public void setActualizacion(String actualizacion) {
		this.actualizacion = actualizacion;
	}

	public String[] getIndices() {
		return indices;
	}

	public void setIndices(String[] indices) {
		this.indices = indices;
	}

	public boolean isPuedeConfirmar() {
		return puedeConfirmar;
	}

	public void setPuedeConfirmar(boolean puedeConfirmar) {
		this.puedeConfirmar = puedeConfirmar;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public List<ActualizacionIndiceBean> getPagoDeCuotasPorIndice() {
		return this.pagoDeCuotasPorIndice;
	}

	public void setPagoDeCuotasPorIndice(List<ActualizacionIndiceBean> pagoDeCuotasPorIndice) {
		this.pagoDeCuotasPorIndice = pagoDeCuotasPorIndice;
	}

	/*
	 * private String getDesdeFechaStr(Date fecha) { return
	 * DateHelper.getString(fecha); }
	 */

	public boolean isActualizarSaldo() {
		return actualizarSaldo;
	}

	public void setActualizarSaldo(boolean actualizarSaldo) {
		this.actualizarSaldo = actualizarSaldo;
	}

	@Override
	protected String getDefaultForward() {
		return "ActualizacionPorIndiceList";
	}

	public List<ActualizacionIndiceBean> getResultadoCalculo() {
		return resultadoCalculo;
	}

	public void setResultadoCalculo(List<ActualizacionIndiceBean> resultadoCalculo) {
		this.resultadoCalculo = resultadoCalculo;
	}

	public Long getIdIndice() {
		return idIndice;
	}

	public void setIdIndice(Long idIndice) {
		this.idIndice = idIndice;
	}

	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getLineasCER() {
		return lineasCER;
	}

	public void setLineasCER(String lineasCER) {
		this.lineasCER = lineasCER;
	}

}
