package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanMovCtaCte;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Movpagos;
import com.nirven.creditos.hibernate.MovpagosKey;
import com.nirven.creditos.hibernate.Pagos;

public class EdicionCtaCte extends ConversationProcess {

	private String numeroAtencion;
	private List<BeanMovCtaCte> beans;
	private Ctacte ctacte;
	@Save
	private String ctacteId;
	private Pagos pago;
	private String concepto;
	private Long cuotaId;
	private Double importe;
	private Long boletoId;
	private Date fechaGeneracion;
	@Save
	private String pagoId;

	@ProcessMethod
	@SuppressWarnings("unchecked")
	public boolean buscar() {
		beans = new ArrayList<BeanMovCtaCte>();
		beans = bp
				.createQuery("select new com.civitas.cred.beans.BeanMovCtaCte(c) from Ctacte c "
						+ "where c.id.objetoi.numeroAtencion = :numeroAtencion "
						+ "order by c.cuota.numero, c.tipomov.abreviatura desc, c.fechaGeneracion")
				.setParameter("numeroAtencion", new Long(numeroAtencion)).list();
		Query q = bp
				.createQuery("select mp.pagos from Movpagos mp "
						+ "where mp.id.movimientoCtacte = :m and mp.id.itemCtacte = :i and mp.id.periodoCtacte = :p")
				.setMaxResults(1);
		for (BeanMovCtaCte b : beans) {
			if (b.getCtacte().getTipoMovimiento().equalsIgnoreCase("pago") && b.getCtacte().getBoleto() != null) {
				Pagos p = (Pagos) q.setParameter("m", b.getCtacte().getId().getMovimientoCtacte())
						.setParameter("i", b.getCtacte().getId().getItemCtacte())
						.setParameter("p", b.getCtacte().getId().getPeriodoCtacte()).uniqueResult();
				b.setPago(p);
			}
		}
		return true;
	}

	@ProcessMethod
	public boolean editar() {
		cuotaId = ctacte.getCuota_id();
		importe = ctacte.getImporte();
		concepto = ctacte.getAsociado().getConcepto().getAbreviatura().toUpperCase();
		ctacteId = ctacte.getId().getMovimientoCtacte() + "|" + ctacte.getId().getItemCtacte() + "|"
				+ ctacte.getId().getPeriodoCtacte();
		fechaGeneracion = ctacte.getFechaGeneracion();
		forward = "EdicionCtaCteEditar";
		return true;
	}

	@ProcessMethod
	public boolean guardar() {
		if (ctacteId == null) {
			return false;
		}
		// buscar ctacte
		setId(ctacteId);
		Boleto boletoAnterior = ctacte.getBoleto();
		ctacte.setFechaGeneracion(fechaGeneracion);
		ctacte.setImporte(importe);
		Cuota nuevaCuota = (Cuota) bp.getById(Cuota.class, cuotaId);
		ctacte.setCuota(nuevaCuota);
		Boleto nuevoBoleto = null;
		if (boletoId != null) {
			nuevoBoleto = (Boleto) bp.getById(Boleto.class, boletoId);
			ctacte.setBoleto(nuevoBoleto);
		}
		Concepto asociado = (Concepto) bp.getNamedQuery("Concepto.findByTipoPeriodo").setString("tipo", concepto)
				.setInteger("periodo", ctacte.getId().getPeriodoCtacte().intValue()).setMaxResults(1).uniqueResult();
		ctacte.setAsociado(asociado);
		ctacte.setFacturado(asociado);
		bp.update(ctacte);
		bp.getCurrentSession().flush();
		if (nuevoBoleto != null) {
			// buscar Bolcon asociado y modificar igual
			bp.createSQLQuery("update Bolcon set boleto_id = :nuevo, importe = :importe, "
					+ "cuota_id = :cuota, facturado_id = :concepto, original_id = :concepto where boleto_id = :b "
					+ "and movimientoCtacte = :m and itemCtacte = :i and periodoCtacte = :p")
					.setParameter("nuevo", nuevoBoleto.getId()).setParameter("b", boletoAnterior.getId())
					.setParameter("m", ctacte.getId().getMovimientoCtacte())
					.setParameter("i", ctacte.getId().getItemCtacte())
					.setParameter("p", ctacte.getId().getPeriodoCtacte()).setParameter("importe", importe)
					.setParameter("cuota", nuevaCuota.getId()).setParameter("concepto", asociado.getId())
					.executeUpdate();
			// buscar Movpagos asociado y modificar igual
			bp.createSQLQuery("update Movpagos set boleto_id = :nuevo, importe = :importe, "
					+ "cuota_id = :cuota, facturado_id = :concepto, original_id = :concepto where boleto_id = :b "
					+ "and movimientoCtacte = :m and itemCtacte = :i and periodoCtacte = :p")
					.setParameter("nuevo", nuevoBoleto.getId()).setParameter("b", boletoAnterior.getId())
					.setParameter("m", ctacte.getId().getMovimientoCtacte())
					.setParameter("i", ctacte.getId().getItemCtacte())
					.setParameter("p", ctacte.getId().getPeriodoCtacte()).setParameter("importe", importe)
					.setParameter("cuota", nuevaCuota.getId()).setParameter("concepto", asociado.getId())
					.executeUpdate();
		}
		if (boletoAnterior != null) {
			actualizarImporteBoleto(boletoAnterior);
			bp.update(boletoAnterior);
		}
		if (nuevoBoleto != null) {
			actualizarImporteBoleto(nuevoBoleto);
			bp.update(nuevoBoleto);
		}
		numeroAtencion = ctacte.getId().getObjetoi().getNumeroAtencion().toString();
		return buscar();
	}

	private void actualizarImporteBoleto(Boleto boleto) {
		Number sum = (Number) bp.createSQLQuery("select sum(b.importe) from Bolcon b where b.boleto_id = :boleto")
				.setParameter("boleto", boleto.getId()).uniqueResult();
		if (sum != null) {
			boleto.setImporte(sum.doubleValue());
		} else {
			boleto.setImporte(0.0);
		}
	}

	@ProcessMethod
	public boolean eliminar() {
		numeroAtencion = ctacte.getId().getObjetoi().getNumeroAtencionStr();
		if (ctacte.getBoleto() != null) {
			Boleto boleto = ctacte.getBoleto();
			BolconKey bk = new BolconKey();
			bk.setMovimientoCtacte(ctacte.getId().getMovimientoCtacte());
			bk.setItemCtacte(ctacte.getId().getItemCtacte());
			bk.setPeriodoCtacte(ctacte.getId().getPeriodoCtacte());
			bk.setVerificadorCtacte(ctacte.getId().getVerificadorCtacte());
			bk.setBoleto(ctacte.getBoleto());
			Bolcon bolcon = (Bolcon) bp.getCurrentSession().get(Bolcon.class, bk);
			if (bolcon != null) {
				bp.delete(bolcon);
			}
			if (ctacte.getCaratula() != null) {
				MovpagosKey mk = new MovpagosKey();
				mk.setMovimientoCtacte(ctacte.getId().getMovimientoCtacte());
				mk.setItemCtacte(ctacte.getId().getItemCtacte());
				mk.setPeriodoCtacte(ctacte.getId().getPeriodoCtacte());
				mk.setVerificadorCtacte(ctacte.getId().getVerificadorCtacte());
				mk.setBoleto(ctacte.getBoleto());
				mk.setCaratula(ctacte.getCaratula());
				Movpagos mp = (Movpagos) bp.getCurrentSession().get(Movpagos.class, mk);
				if (mp != null) {
					bp.delete(mp);
				}
			}
			actualizarImporteBoleto(boleto);
		}
		bp.delete(ctacte);
		return buscar();
	}

	@ProcessMethod
	public boolean duplicar() {
		ctacte = (Ctacte) bp.getCurrentSession().get(Ctacte.class, ctacte.getId());
		CtacteKey ck = ctacte.getId();
		CtacteKey nck = new CtacteKey();
		nck.setMovimientoCtacte(ck.getMovimientoCtacte());
		nck.setItemCtacte(ck.getItemCtacte());
		nck.setPeriodoCtacte(ck.getPeriodoCtacte());
		nck.setVerificadorCtacte(ck.getVerificadorCtacte());
		nck.setObjetoi(ck.getObjetoi());
		Ctacte existente;
		do {
			nck.setItemCtacte(nck.getItemCtacte() + 1);
			existente = (Ctacte) bp.getCurrentSession().get(Ctacte.class, nck);
		} while (existente != null);
		bp.createSQLQuery("insert into Ctacte "
				+ "(itemCtacte, movimientoCtacte, periodoCtacte, verificadorCtacte, comprobante, cotizaMov, "
				+ "expediente, fechaGeneracion, fechaVencimiento, importe, tipoMovimiento, objetoi_id, "
				+ "asociado_id, boleto_id, caratula_id, cuota_id, cuotaOriginal_id, emideta_id, enteBonificador_CODI_BA, "
				+ "facturado_id, tipomov_id, usuario_causerK, DTYPE, detalle, contabiliza, "
				+ "fechaProceso, noContabiliza, desembolso_id, cotizacionDiferencia, debePesos, haberPesos, idBoletoResumen) "
				+ "select :item, movimientoCtacte, periodoCtacte, verificadorCtacte, comprobante, cotizaMov, "
				+ "expediente, fechaGeneracion, fechaVencimiento, importe, tipoMovimiento, objetoi_id, "
				+ "asociado_id, boleto_id, caratula_id, cuota_id, cuotaOriginal_id, emideta_id, enteBonificador_CODI_BA, "
				+ "facturado_id, tipomov_id, usuario_causerK, DTYPE, detalle, contabiliza, "
				+ "fechaProceso, noContabiliza, desembolso_id, cotizacionDiferencia, debePesos, haberPesos, idBoletoResumen "
				+ "from Ctacte where movimientoCtacte = :m and itemCtacte = :i and periodoCtacte = :p")
				.setParameter("item", nck.getItemCtacte()).setParameter("i", ck.getItemCtacte())
				.setParameter("m", ck.getMovimientoCtacte()).setParameter("p", ck.getPeriodoCtacte()).executeUpdate();
		if (ctacte.getBoleto() != null) {
			bp.createSQLQuery(
					"insert into Bolcon (itemCtacte, movimientoCtacte, periodoCtacte, verificadorCtacte, importe, "
							+ "mes, boleto_id, cuota_id, facturado_id, original_id, tipomov_id) "
							+ "select :item, movimientoCtacte, periodoCtacte, verificadorCtacte, importe, "
							+ "mes, boleto_id, cuota_id, facturado_id, original_id, tipomov_id "
							+ "from Bolcon where movimientoCtacte = :m and itemCtacte = :i and periodoCtacte = :p")
					.setParameter("item", nck.getItemCtacte()).setParameter("i", ck.getItemCtacte())
					.setParameter("m", ck.getMovimientoCtacte()).setParameter("p", ck.getPeriodoCtacte())
					.executeUpdate();
			if (ctacte.getCaratula() != null) {
				bp.createSQLQuery("insert into Movpagos "
						+ "(itemCtacte, movimientoCtacte, periodoCtacte, verificadorCtacte, actualizado, importe, "
						+ "mes, caratula_id, boleto_id, cuota_id, facturado_id, original_id, "
						+ "pagos_boleto_id, pagos_caratula_id, tipomov_id) "
						+ "select :item, movimientoCtacte, periodoCtacte, verificadorCtacte, actualizado, importe, "
						+ "mes, caratula_id, boleto_id, cuota_id, facturado_id, original_id, "
						+ "pagos_boleto_id, pagos_caratula_id, tipomov_id "
						+ "from Movpagos where movimientoCtacte = :m and itemCtacte = :i and periodoCtacte = :p")
						.setParameter("item", nck.getItemCtacte()).setParameter("i", ck.getItemCtacte())
						.setParameter("m", ck.getMovimientoCtacte()).setParameter("p", ck.getPeriodoCtacte())
						.executeUpdate();
			}
		}
		numeroAtencion = ctacte.getId().getObjetoi().getNumeroAtencionStr();
		return buscar();
	}

	@ProcessMethod
	public boolean editarPago() {
		pagoId = pago.getId().getBoleto().getId() + "|" + pago.getId().getCaratula().getId();
		forward = "EdicionCtaCtePago";
		return true;
	}

	@ProcessMethod
	public boolean guardarPago() {
		if (pagoId == null) {
			return false;
		}
		setPagoId(pagoId);
		Boleto recibo = (Boleto) bp.getById(Boleto.class, boletoId);
		pago.setRecibo(recibo);
		bp.update(pago);

		numeroAtencion = recibo.getObjetoi().getNumeroAtencionStr();
		return buscar();
	}

	@ProcessMethod
	public boolean editarEstadoCuota() {
		if (cuotaId == null) {
			return false;
		}
		Cuota c = (Cuota) bp.getById(Cuota.class, cuotaId);
		c.setEstado((c.getEstado().equals(Cuota.SIN_CANCELAR) ? Cuota.CANCELADA : Cuota.SIN_CANCELAR));
		bp.update(c);
		numeroAtencion = c.getCredito().getNumeroAtencionStr();
		return buscar();
	}

	@Override
	protected String getDefaultForward() {
		return "EdicionCtaCte";
	}

	public List<BeanMovCtaCte> getBeans() {
		return beans;
	}

	public void setNumeroAtencion(String numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public String getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setId(String id) {
		if (id == null) {
			return;
		}
		String[] ps = id.split("\\|");
		long movimientoCtacte = new Long(ps[0]);
		long itemCtacte = new Long(ps[1]);
		long periodoCtacte = new Long(ps[2]);
		ctacte = (Ctacte) bp
				.createQuery("select c from Ctacte c where c.id.movimientoCtacte = :m "
						+ "and c.id.itemCtacte = :i and c.id.periodoCtacte = :p")
				.setParameter("m", movimientoCtacte).setParameter("i", itemCtacte).setParameter("p", periodoCtacte)
				.uniqueResult();
	}

	public Ctacte getCtacte() {
		return ctacte;
	}

	public void setPagoId(String pagoId) {
		if (pagoId == null) {
			return;
		}
		String[] ps = pagoId.split("\\|");
		long boletoId = new Long(ps[0]);
		long caratulaId = new Long(ps[1]);
		pago = (Pagos) bp.createQuery("select p from Pagos p where p.id.boleto.id = :b and p.id.caratula.id = :c")
				.setParameter("b", boletoId).setParameter("c", caratulaId).uniqueResult();
	}

	public Pagos getPago() {
		return pago;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public Long getCuotaId() {
		return cuotaId;
	}

	public void setCuotaId(Long cuotaId) {
		this.cuotaId = cuotaId;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Long getBoletoId() {
		return boletoId;
	}

	public void setBoletoId(Long boletoId) {
		this.boletoId = boletoId;
	}

	public String getFechaGeneracion() {
		return DateHelper.getString(fechaGeneracion);
	}

	public void setFechaGeneracion(String f) {
		fechaGeneracion = DateHelper.getDate(f);
	}
}
