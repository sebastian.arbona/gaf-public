package com.asf.cred.business;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Transient;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Objetoi;

public class ObjetoiDTO implements Serializable {

	private static final long serialVersionUID = -6032626509029343512L;
	private static DecimalFormat format = new DecimalFormat("#,##0.00;-#,##0.00");

	private Long idObjetoi;
	private String numeroCredito;
	private String expediente;
	private String linea;
	private String estado;
	private Long numeroAtencion;
	private double deuda;
	private boolean seleccionado;
	private Persona titular;
	private String destino;
	private double financiamiento;
	private String fechaSolicitudStr;
	private String fechaContratoStr;
	private String tipoDesembolso;
	private String numeroCuota;
	private HashMap<Integer, Double> montosBonDetalleMensual;
	private List<BonDetalleDTO> bonDetalleDTOs;
	private Long idEstado;
	private Long idLinea;
	private Double desembolso;
	private Double gastos;
	private String comportamiento;
	private Integer nroOrden;
	private int diasAtraso;
	private String cuil;
	private String tipoGarantia;
	private Long idTipoGarantia;

	private Double totalInmovilizado;
	private String producto;
	private Double liberadoALaFecha;
	private String ultimaLiberacion;
	private Double deudaActual;
	private Double nuevoVolumen;
	private Double saldoInmovilizado;
	private Double liberacionSolicitada;

	private String fechaCancelacionStr;
	private double totalAdeudado;

	private Double liberacionSolicitar;

	private String idUnicoGarantia;

	private boolean banderaFiltrosColor = false;

	private String auditoriaFinalPosee;

	// Ticket 9949
	private float libAforo;
	private float libPrecioProducto;
	private float libImporte;

	public ObjetoiDTO() {
		titular = new Persona();
		montosBonDetalleMensual = new HashMap<Integer, Double>();
		bonDetalleDTOs = new ArrayList<BonDetalleDTO>();
	}

	public ObjetoiDTO(Objetoi o) {
		this.idObjetoi = o.getId();
		this.numeroAtencion = o.getNumeroAtencion();
		this.expediente = o.getExpediente();
		this.linea = (o.getLinea() != null && o.getLinea().getNombre() != null ? o.getLinea().getNombre()
				: "Sin linea");
		this.titular = o.getPersona();
	}

	public ObjetoiDTO(Objetoi o, String estado, double deuda) {
		this.idObjetoi = o.getId();
		this.numeroCredito = o.getNumeroCredito();
		this.expediente = o.getExpediente();
		this.linea = (o.getLinea() != null && o.getLinea().getNombre() != null ? o.getLinea().getNombre()
				: "Sin linea");
		this.estado = estado;
		this.deuda = deuda;
		this.numeroAtencion = o.getNumeroAtencion();
		this.titular = o.getPersona();
		this.destino = o.getDestino();
		this.financiamiento = (o.getFinanciamiento() != null ? o.getFinanciamiento() : 0);
		this.fechaSolicitudStr = o.getFechaSolicitudStr();
		this.fechaContratoStr = o.getFechaMutuoStr();
	}

	public ObjetoiDTO(Objetoi o, String estado, double deuda, Cuota cuota) {
		this(o, estado, deuda);
		numeroCuota = Integer.toString(cuota.getNumero());
		montosBonDetalleMensual = new HashMap<Integer, Double>();
		bonDetalleDTOs = new ArrayList<BonDetalleDTO>();
	}

	public ObjetoiDTO(Long id, Long numeroAtencion, String nomb12, String expediente, Double totalInmovilizado,
			String producto, Double liberadoAlaFecha, String ultimaLiberacion, Double deudaActual,
			Double nuevoVolumen) {
		this.idObjetoi = id;
		this.numeroAtencion = numeroAtencion;
		this.titular = new Persona();
		this.titular.setNomb12(nomb12);
		this.expediente = expediente;
		this.totalInmovilizado = totalInmovilizado;
		this.producto = producto;
		this.liberadoALaFecha = liberadoAlaFecha;
		this.ultimaLiberacion = ultimaLiberacion;
		this.deudaActual = deudaActual;
		this.nuevoVolumen = nuevoVolumen;
	}

	public void armarBonDetalles(List<Meses> meses) {
		llenarMontosVacios();
		for (Meses mes : meses) {
			BonDetalleDTO dto = new BonDetalleDTO();
			dto.setMes(mes.name());
			dto.setMonto(montosBonDetalleMensual.get(mes.ordinal()));
			bonDetalleDTOs.add(dto);
		}
	}

	public void llenarMontosVacios() {
		for (int i = 0; i <= 11; i++) {
			if (!montosBonDetalleMensual.containsKey(i))
				montosBonDetalleMensual.put(i, 0.0);
		}
	}

	@Transient
	public void analizarfiltros() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, idObjetoi);
		if (objetoi.getComportamientoActual().trim().equals("SITUACI�N NORMAL")) {
			CreditoHandler handler = new CreditoHandler();
			List<Ctacte> ccs = handler.listarCtaCteConceptos(objetoi, Concepto.CONCEPTO_CAPITAL);
			double saldoCapital = handler.calcularSaldo(ccs);
			if (saldoCapital > 0) {
				CreditoCalculoDeuda ccd = new CreditoCalculoDeuda(objetoi.getId(), new Date());
				ccd.calcular();
				double deuda = ccd.getDeudaVencidaFechaEstricta();
				if (deuda == 0) {
					CreditoCalculoDeuda crecaldeu = new CreditoCalculoDeuda(objetoi.getId(), new Date());
					crecaldeu.calcular();
					List<BeanCtaCte> vencidas = crecaldeu.getVistaVencida();
					if (vencidas != null && vencidas.size() > 0) {
						// Buscar el �ltimo n�mero
						BeanCtaCte ultimaVencida = vencidas.get(vencidas.size() - 1);
						List<BeanCtaCte> siguienteCuota = crecaldeu.getVistaCuota(ultimaVencida.getNumero() + 1);
						if (siguienteCuota != null && siguienteCuota.size() > 0) {
							// sacar el primero:
							BeanCtaCte primerBean = siguienteCuota.get(0);
							double capitalCuota = primerBean.getCapital();
							double saldoCapitalCuota = 0;

							for (BeanCtaCte b : siguienteCuota) {
								if (b.isDebito()) {
									saldoCapitalCuota += b.getCapital();
								} else {
									saldoCapitalCuota -= b.getCapital();
								}
							}

							double porcentajeSaldo = saldoCapitalCuota / capitalCuota;
							/*
							 * Ticket 9430 if (saldoCapitalCuota == capitalCuota || porcentajeSaldo <= 0.5)
							 * {
							 */
							if (saldoCapitalCuota == capitalCuota || porcentajeSaldo <= 0.25) {
								/*
								 * Ticket 9430 if (nuevoVolumen > 1000 || nuevoVolumen <= 0) {
								 */
								if (nuevoVolumen < 1000 || nuevoVolumen <= 0) {
									setBanderaFiltrosColor(true); // rojo
								}
							}

						} else {
							/*
							 * Ticket 9430 if (nuevoVolumen > 1000 || nuevoVolumen <= 0) {
							 */
							if (nuevoVolumen < 1000 || nuevoVolumen <= 0) {
								setBanderaFiltrosColor(true); // rojo
							}
						}
					} else {
						/*
						 * Ticket 9430 if (nuevoVolumen > 1000 || nuevoVolumen <= 0) {
						 */
						if (nuevoVolumen < 1000 || nuevoVolumen <= 0) {
							setBanderaFiltrosColor(true); // rojo
						}
					}

				}
			}
		}
	}

	private int contadorMes;

	public String getMes() {
		return bonDetalleDTOs.get(contadorMes++).getMes();
	}

	private int contadorMonto;

	public String getMonto() {
		return String.format("%10.2f$", bonDetalleDTOs.get(contadorMonto++).getMonto());
	}

	public Double getLiberacionSolicitar() {
		if (liberacionSolicitar != null) {
			return liberacionSolicitar;
		}
		// total del contrato
		return (totalInmovilizado != null ? totalInmovilizado : 0) -
		// liberaciones todavia no realizadas
				(liberacionSolicitada != null ? liberacionSolicitada : 0) -
				// liberaciones realizadas
				(liberadoALaFecha != null ? liberadoALaFecha : 0) -
				// restando lo que debe quedar inmovilizado, resulta lo que hay
				// que liberar
				(nuevoVolumen != null ? nuevoVolumen : 0);
	}

	public void setLiberacionSolicitar(Double liberacionSolicitar) {
		this.liberacionSolicitar = liberacionSolicitar;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	@Transient
	public Objetoi getObjetoi() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, this.idObjetoi);
		return objetoi;
	}

	public boolean isSeleccionado() {
		return seleccionado;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public double getDeuda() {
		return deuda;
	}

	public void setDeuda(double deuda) {
		this.deuda = deuda;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public boolean getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	public Persona getTitular() {
		return titular;
	}

	public void setTitular(Persona titular) {
		this.titular = titular;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public double getFinanciamiento() {
		return financiamiento;
	}

	public void setFinanciamiento(double financiamiento) {
		this.financiamiento = financiamiento;
	}

	public String getFechaSolicitudStr() {
		return fechaSolicitudStr;
	}

	public void setFechaSolicitudStr(String fechaSolicitudStr) {
		this.fechaSolicitudStr = fechaSolicitudStr;
	}

	public String getTipoDesembolso() {
		return tipoDesembolso;
	}

	public void setTipoDesembolso(String tipoDesembolso) {
		this.tipoDesembolso = tipoDesembolso;
	}

	public String getFechaContratoStr() {
		return fechaContratoStr;
	}

	public void setFechaContratoStr(String fechaContratoStr) {
		this.fechaContratoStr = fechaContratoStr;
	}

	public String getNumeroCuota() {
		return numeroCuota;
	}

	public void setNumeroCuota(String numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	public HashMap<Integer, Double> getMontosBonDetalleMensual() {
		return montosBonDetalleMensual;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public List<BonDetalleDTO> getBonDetalleDTOs() {
		return bonDetalleDTOs;
	}

	public void setBonDetalleDTOs(List<BonDetalleDTO> bonDetalleDTOs) {
		this.bonDetalleDTOs = bonDetalleDTOs;
	}

	public String getFechaCancelacionStr() {
		return fechaCancelacionStr;
	}

	public void setFechaCancelacionStr(String fechaCancelacionStr) {
		this.fechaCancelacionStr = fechaCancelacionStr;
	}

	public double getDesembolso() {
		return desembolso != null ? desembolso : 0;
	}

	public Integer getNroOrden() {
		return nroOrden;
	}

	public void setNroOrden(Integer nroOrden) {
		this.nroOrden = nroOrden;
	}

	public boolean isEjecutable() {
		if (nroOrden != null && nroOrden.longValue() > 0) {
			return true;
		}
		return false;
	}

	public void setDesembolso(Double desembolso) {
		this.desembolso = desembolso;
	}

	public String getDesembolsoStr() {
		if (desembolso != null && desembolso >= 0)
			return String.format("%.2f$", desembolso);
		else if (desembolso == null || desembolso < 0) {
			return "0.0";
		}
		return "";
	}

	public double getGastos() {
		return gastos != null ? gastos : 0;
	}

	public void setGastos(double gastos) {
		this.gastos = gastos;
	}

	public String getGastosStr() {
		if (gastos != null)
			return String.format("%.2f$", gastos);
		else
			return "";
	}

	public String getFinanciamientoStr() {
		return String.format("%.2f$", financiamiento);
	}

	public double getTotalAdeudado() {
		return totalAdeudado;
	}

	public void setTotalAdeudado(double totalAdeudado) {
		this.totalAdeudado = totalAdeudado;
	}

	public String getTotalAdeudadoStr() {
		return String.format("%.2f", totalAdeudado);
	}

	public String getComportamiento() {
		return comportamiento;
	}

	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}

	public int getDiasAtraso() {
		return diasAtraso;
	}

	public void setDiasAtraso(int diasAtraso) {
		this.diasAtraso = diasAtraso;
	}

	public Double getTotalInmovilizado() {
		return totalInmovilizado;
	}

	public String getTotalInmovilizadoStr() {
		if (totalInmovilizado != null && totalInmovilizado >= 0) {
			return format.format(totalInmovilizado);
		} else {
			return "";
		}
	}

	public void setTotalInmovilizado(Double totalInmovilizado) {
		this.totalInmovilizado = totalInmovilizado;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Double getLiberadoALaFecha() {
		return liberadoALaFecha;
	}

	public String getLiberadoALaFechaStr() {
		if (liberadoALaFecha != null && liberadoALaFecha >= 0) {
			return format.format(liberadoALaFecha);
		} else {
			return "";
		}
	}

	public void setLiberadoALaFecha(Double liberadoALaFecha) {
		this.liberadoALaFecha = liberadoALaFecha;
	}

	public String getUltimaLiberacion() {
		return ultimaLiberacion;
	}

	public void setUltimaLiberacion(String ultimaLiberacion) {
		this.ultimaLiberacion = ultimaLiberacion;
	}

	public Double getDeudaActual() {
		return deudaActual;
	}

	public String getDeudaActualStr() {
		if (deudaActual != null && deudaActual >= 0) {
			return format.format(deudaActual);
		} else {
			return "";
		}

	}

	public void setDeudaActual(Double deudaActual) {
		this.deudaActual = deudaActual;
	}

	public Double getNuevoVolumen() {
		return nuevoVolumen;
	}

	public String getNuevoVolumenStr() {
		if (nuevoVolumen != null && nuevoVolumen >= 0) {
			return format.format(nuevoVolumen);
		} else {
			return "";
		}
	}

	public void setNuevoVolumen(Double nuevoVolumen) {
		this.nuevoVolumen = nuevoVolumen;
	}

	public String getCuil() {
		return cuil;
	}

	public void setCuil(String cuil) {
		this.cuil = cuil;
	}

	public String getTipoGarantia() {
		return tipoGarantia;
	}

	public void setTipoGarantia(String tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}

	public Long getIdTipoGarantia() {
		return idTipoGarantia;
	}

	public void setIdTipoGarantia(Long idTipoGarantia) {
		this.idTipoGarantia = idTipoGarantia;
	}

	public Double getSaldoInmovilizado() {
		return saldoInmovilizado;
	}

	public String getSaldoInmovilizadoStr() {
		if (saldoInmovilizado != null && saldoInmovilizado >= 0) {
			return format.format(saldoInmovilizado);
		} else {
			return "";
		}
	}

	public void setSaldoInmovilizado(Double saldoInmovilizado) {
		this.saldoInmovilizado = saldoInmovilizado;
	}

	public Double getLiberacionSolicitada() {
		return liberacionSolicitada;
	}

	public void setLiberacionSolicitada(Double liberacionSolicitada) {
		this.liberacionSolicitada = liberacionSolicitada;
	}

	public String getIdUnicoGarantia() {
		return idUnicoGarantia;
	}

	public void setIdUnicoGarantia(String idUnicoGarantia) {
		this.idUnicoGarantia = idUnicoGarantia;
	}

	public boolean isBanderaFiltrosColor() {
		return banderaFiltrosColor;
	}

	public void setBanderaFiltrosColor(boolean banderaFiltrosColor) {
		this.banderaFiltrosColor = banderaFiltrosColor;
	}

	public float getLibAforo() {
		return libAforo;
	}

	public void setLibAforo(float aforo) {
		this.libAforo = aforo;
	}

	public float getLibPrecioProducto() {
		return libPrecioProducto;
	}

	public void setLibPrecioProducto(float precio) {
		this.libPrecioProducto = precio;
	}

	public float getLibImporte() {
		return libImporte;
	}

	public void setLibImporte(float libImporte) {
		this.libImporte = libImporte;
	}

	public String getAuditoriaFinalPosee() {
		return auditoriaFinalPosee;
	}

	public void setAuditoriaFinalPosee(String auditoriaFinalPosee) {
		this.auditoriaFinalPosee = auditoriaFinalPosee;
	}

}
