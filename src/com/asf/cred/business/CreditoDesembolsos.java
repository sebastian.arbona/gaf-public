package com.asf.cred.business;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;

import com.asf.gaf.DirectorHandler;
import com.asf.gaf.hibernate.Cotizacion;
import com.asf.gaf.hibernate.CuentaBancaria;
import com.asf.hibernate.mapping.Claves;
import com.asf.hibernate.mapping.Menu;
import com.asf.hibernate.mapping.Permiso;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.AbstractEstado;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.civitas.util.PdfHelper;
import com.civitas.util.ReportResultHelper;
import com.nirven.creditos.bpm.ImputacionesHelper;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.DetalleCuota;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.RequisitoDesembolso;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Archivo;
import com.nirven.expedientes.persistencia.Numerador;
import com.nirven.expedientes.persistencia.TipoDeArchivo;

public class CreditoDesembolsos extends CreditoProcess {

	private Long idDesembolso;
	private List<Desembolso> desembolsos;
	private Desembolso desembolso;
	private String relacionId = "";

	private String forward;

	private Boolean usaInterbanking;
	private boolean ejecutado;
	private ReportResult reporte;
	private Boolean esCuentaPropia;

	private CuentaBancaria cuentaTercero;
	private String cuentaTerceroId;
	private Double importeGastos;
	private Double importeReal;

	private boolean analisis = false;
	private String msg;
	private boolean pendiente;
	private boolean editable;
	private String estadoDesistimiento;

	private Double gastos = new Double(0.0);
	// usuario loggeado
	Usuario usuario = (Usuario) bp.getById(Usuario.class, SessionHandler.getCurrentSessionHandler().getCurrentUser());
	private boolean desistidoOk;

	public CreditoDesembolsos() {
		super();
		setIdObjetoi((Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("idObjetoiCreditoDesembolsos"));
		idDesembolso = (Long) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("idDesembolsoCreditoDesembolsos");
		setAction((String) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("accionCreditoDesembolsos"));
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("accionCreditoDesembolsos",
				null);
		desembolso = new Desembolso();
		desembolso.setEstado("4");// Se setea el codigo del Tipificador
		forward = "CreditoDesembolsos";
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean doProcess() {
		forward = "CreditoDesembolsos";
		relacionId = DirectorHelper.getString("relacion.cuenta", "-1");
		if (action == null || action.equals("load")) {
			super.doProcess();
			return buscarDesembolsos();
		} else if (action.equals("save")) {
			return guardarObjetoi();
		} else if (action.equals("loadEjecucion")) {// aca
			boolean b = buscarDesembolsoEjecucion();
			if (!b) {
				this.errores.put("desembolso.requisitosNoCumplidos", "desembolso.requisitosNoCumplidos");
				this.setAction("load");
				doProcess();
			}
			return b;
		} else if (action.equals("ejecutar")) {
			try {
				super.buscarObjetoi();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			bp.begin();
			if (!ejecutarDesembolso()) {
				bp.rollback();
				forward = "CreditoEjecutarDesembolso";
				return false;
			} else {
				bp.commit();
				return true;
			}
		} else if (action.equals("reporte") || action.equals("reporteRev1") || action.equals("reporteRev2")) {
			int hoja = 0;
			if (action.equals("reporteRev1")) {
				hoja = 1;
			} else if (action.equals("reporteRev2")) {
				hoja = 2;
			}
			crearReporteDesembolso(hoja);
			if (this.errores.isEmpty()) {
				this.forward = "ReportesProcessFrame";
			}
			return errores.isEmpty();
		} else if (action.equals("solicitud")) {
			try {
				super.buscarObjetoi();
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return solicitarDesembolso();
		} else if (action.equalsIgnoreCase("solicitarGaf")) {
			if (validarDesembolso()) {
				Desembolso desembolso1 = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
				Double importeAnterior = desembolso1.getImporteSolicitado();
				Double importeGastoAnterior = desembolso1.getImporteGastos();
				/**
				 * esto no tiene mas uso que cargar la variable gastos, no se transportan a gaf
				 * ningun id
				 */
				Number g = (Number) bp.createQuery(
						"SELECT SUM(d.importe) FROM DetalleFactura d WHERE d.credito.id = :id AND d.impactado = :false")
						.setParameter("id", getIdObjetoi()).setParameter("false", false).uniqueResult();
				if (g != null) {
					gastos = DoubleHelper.redondear(g.doubleValue());
				}
				desembolso1.setImporteSolicitado(desembolso.getImporteReal());
				desembolso1.setImporteGastos(desembolso.getImporteGastos());
				bp.update(desembolso1);
				bp.getCurrentSession().flush();
				HashMap<String, Object> erroresSolicitud = DesembolsoHelper.solicitar(desembolso1);
				if (!erroresSolicitud.isEmpty()) {
					// existieron errores
					desembolso1.setImporteSolicitado(importeAnterior);
					desembolso1.setImporteGastos(importeGastoAnterior);
					bp.update(desembolso1);
					errores.putAll(erroresSolicitud);
					buscarDesembolsos();
					return false;
				}
				if (desembolso1.getNumero() != null && desembolso1.getNumero() == 1) {
					Objetoi credito = desembolso1.getCredito();
					if (credito.getLinea().isLineaCosecha()) {
						List<ObjetoiVinedo> vinedos = bp
								.createQuery("Select v from ObjetoiVinedo v where v.credito = :credito")
								.setParameter("credito", credito).list();
						for (ObjetoiVinedo objetoVin : vinedos) {
							Date fecha = new Date();
							objetoVin.setFechaSolicitudInforme2(fecha);
							bp.update(objetoVin);
						}
						CreditoHandler ch = new CreditoHandler();
						ch.comprobarInmovilizacion(credito.getId());
						bp.update(credito);
					}
				}
				adjuntarComprobantes();
				forward = "CreditoDesembolsos";
				return buscarDesembolsos();
			} else {
				forward = "CreditoEjecutarDesembolso";
				return false;
			}
		} else if (action.equalsIgnoreCase("reporteOrden")) {
			imprimirReporteOrden();
			this.forward = "ReportesProcess2";
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("idObjetoiCreditoDesembolsos", getIdObjetoi());
		} else if (action.equalsIgnoreCase("guardar")) {
			guardar();
			setAction("load");
			doProcess();
		} else if (action.equals("desistir")) {
			try {
				super.buscarObjetoi();
				desistir();
				this.forward = "DesistirDesembolso";
			} catch (HibernateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (action.equals("imprimirAutorizacion")) {
			imprimirAutorizacion();
			this.forward = "ReportesProcessFrame";
		} else if (action.equalsIgnoreCase("cargarDesistir")) {
			this.forward = "DesistirDesembolso";
		} else if (action.equalsIgnoreCase("adjuntarComprobantes")) {
			adjuntarComprobantes();
		}
		return errores.isEmpty();
	}

	private void adjuntarComprobantes() {

		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		Persona titular = objetoi.getPersona();

		String origen = SessionHandler.getCurrentSessionHandler().getIDMODULO();
		Claves currentUser = new Claves(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		String nombreArchivo, contentType, descripcion;
		Long idTipoDeArchivo;
		Integer paginas;
		TipoDeArchivo tipoDeArchivo;

		/**
		 * genera el reportResult de la solicitud de desembolso
		 */
		generarReporteDesembolso(0);
		byte[] reporteDesembolso = ReportResultHelper.getReportePdf(getReporte());
		nombreArchivo = titular.getCuil12Str() + "-" + objetoi.getExpediente() + "-Solicitud-de-Desembolso";
		nombreArchivo += ".pdf";
		paginas = PdfHelper.getPdfCantidadPaginas(reporteDesembolso);
		contentType = "application/pdf";
		idTipoDeArchivo = DirectorHelper.getLong("DocumentoDigital.SolicitudDesembolso", 756l);
		tipoDeArchivo = (TipoDeArchivo) bp.getById(TipoDeArchivo.class, idTipoDeArchivo);
		descripcion = "Descripcion: Solicitud de Desembolso;" + "Proyecto: " + objetoi.getNumeroAtencionStr() + ";"
				+ "Titular: " + titular.getNomb12() + ";" + "CUIT: " + titular.getCuil12Str();
		/**
		 * repositorio ADE
		 */
		Archivo solicitudDesembolso = new Archivo(nombreArchivo, contentType, tipoDeArchivo, currentUser, descripcion,
				origen, paginas);
		solicitudDesembolso.setArchivo(reporteDesembolso);
		bp.save(solicitudDesembolso);
		/**
		 * grila de documentacion
		 */
		ObjetoiArchivo objetoiArchivoSolicitud = new ObjetoiArchivo(objetoi, solicitudDesembolso, nombreArchivo,
				reporteDesembolso, contentType, Boolean.TRUE, descripcion, null, "1");
		objetoiArchivoSolicitud.setArchivoRepo(solicitudDesembolso);
		bp.save(objetoiArchivoSolicitud);

		/**
		 * genera el reportResult del informe general
		 */
		generarInformeGeneral();
		byte[] reporteGeneral = ReportResultHelper.getReportePdf(getReporte());
		nombreArchivo = titular.getCuil12Str() + "-" + objetoi.getExpediente() + "-Informe-de-Otorgamiento";
		nombreArchivo += ".pdf";
		paginas = PdfHelper.getPdfCantidadPaginas(reporteGeneral);
		contentType = "application/pdf";
		idTipoDeArchivo = DirectorHelper.getLong("DocumentoDigital.InformeGeneral", 700l);
		tipoDeArchivo = (TipoDeArchivo) bp.getById(TipoDeArchivo.class, idTipoDeArchivo);
		descripcion = "Descripcion: Informe de Otorgamiento;" + "Proyecto: " + objetoi.getNumeroAtencionStr() + ";"
				+ "Titular: " + titular.getNomb12() + ";" + "CUIT: " + titular.getCuil12Str();
		/**
		 * repositorio ADE
		 */
		Archivo informeGeneral = new Archivo(nombreArchivo, contentType, tipoDeArchivo, currentUser, descripcion,
				origen, paginas);
		informeGeneral.setArchivo(reporteGeneral);
		bp.save(informeGeneral);
		/**
		 * grilla de documentacion
		 */
		ObjetoiArchivo objetoiArchivoInforme = new ObjetoiArchivo(objetoi, informeGeneral, nombreArchivo,
				reporteGeneral, contentType, Boolean.TRUE, descripcion, null, "1");
		objetoiArchivoInforme.setArchivoRepo(informeGeneral);
		bp.save(objetoiArchivoInforme);

	}

	private void imprimirAutorizacion() {
		try {
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("prueba.jasper");
			reporte.setParams("REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ ";DESEMBOLSO_ID=" + idDesembolso);
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "prueba"));
		}
	}

	public boolean validarDesembolso() {
		Objetoi objetoi = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		String estadoActual = objetoi.getEstadoActual().getEstado().getNombreEstado();
		if (!estadoActual.equalsIgnoreCase("CONTRATO EMITIDO") && !estadoActual.equalsIgnoreCase("PRIMER DESEMBOLSO")
				&& !estadoActual.equalsIgnoreCase("PENDIENTE SEGUNDO DESEMBOLSO")
				&& !estadoActual.equalsIgnoreCase("EJECUCION")) {
			errores.put("desembolso.estado", new ActionMessage("desembolso.estado", this.msg));
		}
		// No se puede pedir mas que el financiamiento solicitado durante etapa
		// ANALISIS
		String queryDesemb = "select sum(case when estado = '2' then importeReal when estado = '1' then importeSolicitado else 0 end) from desembolso where credito_id = "
				+ getIdObjetoi();
		Number totalDesemb = (Number) bp.createSQLQuery(queryDesemb).uniqueResult();
		if (totalDesemb != null
				&& (totalDesemb.doubleValue() + desembolso.getImporteReal()) - objetoi.getFinanciamiento() >= 0.01) {
			errores.put("desembolso.solicitado.maximo", "desembolso.solicitado.maximo");
		}
		if (!objetoi.validarRegistroGarantiaDesembolso()) {
			errores.put("desembolso.garantiaInscripcion", "desembolso.garantiaInscripcion");
		}
		Desembolso desembolso1 = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		String expedientePago = desembolso1.getExpedientePago();
		if (expedientePago == null || expedientePago.isEmpty()) {
			errores.put("desembolso.expedientePago.obligatorio", "desembolso.expedientePago.obligatorio");
		}
		return errores.isEmpty();
	}

	public void guardar() {
		try {
			buscarObjetoi();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		CuentaBancaria c = null;
		String observacion = "";
		boolean ctaPropia = true;
		if (usaInterbanking != null && usaInterbanking) {
			observacion = (desembolso.getObservacion());
			if (esCuentaPropia) {
				ctaPropia = true;
				c = (CuentaBancaria) bp.getById(CuentaBancaria.class, new Long(desembolso.getCuentaInterbankingId()));
			} else {
				ctaPropia = false;
				c = (CuentaBancaria) bp.getById(CuentaBancaria.class, new Long(cuentaTerceroId));
			}

		} else if (usaInterbanking != null && !usaInterbanking) {
			c = null;
			observacion = (desembolso.getObservacion());
		} else {
			return;
		}
		Double gastos = desembolso.getImporteGastos();
		Double importeReal = desembolso.getImporteReal();

		desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		desembolso.setObservacion(observacion);
		desembolso.setCuentaInterbanking(c);
		desembolso.setCuentaPropia(ctaPropia);
		desembolso.setImporteGastos(DoubleHelper.redondear(gastos));
		desembolso.setImporteReal(importeReal);

		bp.saveOrUpdate(desembolso);
		bp.commit();
	}

	private void imprimirReporteOrden() {
		desembolso = (Desembolso) bp.getById(Desembolso.class, desembolso.getId());
		String schemaGAF = BusinessPersistance.getSchema();
		try {
			schemaGAF = (DirectorHandler.getDirector("SGAF")).getValor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String parametros = "WHERE=WHERE OP.IDORDEPAGO=" + desembolso.getIdOrdepago() + ";SCHEMA_GAF=" + schemaGAF
				+ ";REPORTS_PATH=" + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath();
		reporte = new ReportResult();
		reporte.setParams(parametros);
		reporte.setReportName("OrdenDePago.jasper");
		reporte.setExport("PDF");
	}

	private void crearReporteDesembolso(int hoja) {
		generarReporteDesembolso(hoja);
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("accionCreditoDesembolsos",
				"loadEjecucion");
	}

	private void generarReporteDesembolso(int hoja) {
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		try {
			if (desembolso.getCuentaInterbankingId() == null) {
				desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
			}
			double gastoRec = calcularGastosRecuperar(desembolso);
			reporte = new ReportResult();
			reporte.setExport("PDF");
			switch (hoja) {
			case 0:
				reporte.setReportName("solicitudDesembolso.jasper");
				break;
			case 1:
				reporte.setReportName("revisionDesembolso.jasper");
				break;
			case 2:
				reporte.setReportName("revisionDesembolso2.jasper");
				break;
			}
			reporte.setParams(
					"idObjetoi=" + getIdObjetoi() + ";DESEMBOLSO_ID=" + idDesembolso + ";cuentaInterbanking_id="
							+ desembolso.getCuentaInterbankingId() + ";SCHEMA=" + BusinessPersistance.getSchema()
							+ ";GASTOS=" + gastoRec + ";relacionCotomadorId=" + relacionCotomadorId + ";REPORTS_PATH="
							+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath());
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "Desembolso"));
		}
	}

	private void generarInformeGeneral() {
		String relacionCotomadorId = DirectorHelper.getString("relacion.cotomador");
		String marcaagua = DirectorHelper.getString("marca.agua");
		try {
			buscarObjetoi();
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("otorgamiento.jasper");
			reporte.setParams("idObjetoi=" + getIdObjetoi() + ";SCHEMA=" + BusinessPersistance.getSchema()
					+ ";idPersona=" + credito.getPersona_id() + ";relacionCotomadorId=" + relacionCotomadorId
					+ ";MONEDA=" + credito.getLinea().getMoneda().getSimbolo() + ";REPORTS_PATH="
					+ SessionHandler.getCurrentSessionHandler().getRequest().getContextPath() + ";MARCAAGUA="
					+ marcaagua);
		} catch (Exception e) {
			this.errores.put("imprimir.informe.error", new ActionMessage("imprimir.informe.error", "otorgamiento"));
		}
	}

	@SuppressWarnings("unchecked")
	private boolean buscarDesembolsos() {
		desembolsos = bp.getNamedQuery("Desembolso.findByCredito").setLong("idCredito", getIdObjetoi()).list();
		SessionHandler.getCurrentSessionHandler().getRequest().getSession().setAttribute("idObjetoiCreditoDesembolsos",
				getIdObjetoi());
		return errores.isEmpty();
	}

	private boolean buscarDesembolsoEjecucion() {
		try {
			buscarObjetoi();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Number g = (Number) bp.createQuery(
				"SELECT SUM(d.importe) FROM DetalleFactura d WHERE d.credito.id = :id AND d.impactado = false AND d.factura.fechaPago is not null")
				.setParameter("id", getIdObjetoi()).uniqueResult();
		desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		if (g != null) {
			gastos = DoubleHelper.redondear(g.doubleValue());
		}
		if (getObjetoi().getLinea().getGastosEnDesembolso() != null
				&& getObjetoi().getLinea().getGastosEnDesembolso().booleanValue()) {
			if (desembolso.getNumero() != null && desembolso.getNumero().intValue() == 1) {
				importeGastos = getObjetoi().getLinea().getGastosDesembolso(getObjetoi().getFinanciamiento());
			}
		} else {
			importeGastos = getObjetoi().getLinea().getGastosDesembolso(
					desembolso.getImporteReal() == null ? desembolso.getImporte() : desembolso.getImporteReal());
		}
		desembolso.setImporteGastos(importeGastos);
		if (!buscarRequisitosCumplidos(desembolso)) {
			return false;
		}
		if (desembolso.getEstado() != null && desembolso.getEstado().trim().equals("4")) {
			if (desembolso.getImporteReal() == null || desembolso.getImporteReal() == 0.0) {
				desembolso.setImporteReal(desembolso.getImporte());
			}
		} else {
			ejecutado = true;
		}
		if (desembolso.getEstado() != null && desembolso.getEstado().trim().equals("1")) {
			pendiente = true;
			ejecutado = false;
		}
		if (usaInterbanking == null) {
			if (desembolso.getCuentaInterbanking() != null) {
				usaInterbanking = true;
			} else {
				usaInterbanking = false;
			}
		}
		if (esCuentaPropia == null) {
			if (desembolso.getCuentaPropia() != null) {
				if (desembolso.getCuentaPropia() == true) {
					esCuentaPropia = true;
				} else {
					esCuentaPropia = false;
				}
			} else {
				esCuentaPropia = true;
			}
		}
		if (desembolso.getEstado().equals("1") || desembolso.getEstado().equals("4")) {
			editable = true;
		}
		// Compruebo que la moneda de la linea del credito no sea Peso. Si es
		// asi, guardo la cotizacion del dia
		if (!(credito.getLinea().getMoneda().getAbreviatura().equalsIgnoreCase("Peso"))) {
			Cotizacion coti;
			try {
				coti = (Cotizacion) bp.createQuery(
						"Select c from Cotizacion c where c.moneda=:moneda and c.fechaHasta is null order by c.fechaDesde desc")
						.setMaxResults(1).setParameter("moneda", credito.getLinea().getMoneda()).uniqueResult();
				if (coti != null) {
					desembolso.setCotizaSolicitud(coti.getVenta());
				}
			} catch (NoResultException e) {
				// no est� bien parametrizado
			}
			bp.saveOrUpdate(desembolso);
		}
		forward = "CreditoEjecutarDesembolso";
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("idDesembolsoCreditoDesembolsos", idDesembolso);
		return errores.isEmpty();
	}

	public Double calcularGastosRecuperar(Desembolso desembolso) {
		Number g = (Number) bp.createQuery(
				"SELECT SUM(d.importe) FROM DetalleFactura d WHERE d.credito.id = :id AND d.impactado = :false AND d.factura.fechaPago is not null")
				.setParameter("id", getIdObjetoi()).setParameter("false", false).uniqueResult();
		if (g != null) {
			return (Double) g;
		} else {
			return new Double(0);
		}
	}

	@SuppressWarnings("unchecked")
	private boolean buscarRequisitosCumplidos(Desembolso desembolso) {
		List<RequisitoDesembolso> lista = bp.createQuery(
				"SELECT r FROM RequisitoDesembolso r WHERE r.desembolso = :desembolso AND r.fechaCumplido is null and r.requisito.obligatorio=1")
				.setParameter("desembolso", desembolso).list();
		if (lista.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean guardarObjetoi() {
		if (!validarObjetoi()) {
			return false;
		}
		Objetoi existente = (Objetoi) bp.getById(Objetoi.class, getIdObjetoi());
		existente.setNombreAuthValores1(credito.getNombreAuthValores1());
		existente.setCuitAuthValores1(credito.getCuitAuthValores1());
		existente.setNombreAuthValores2(credito.getNombreAuthValores2());
		existente.setCuitAuthValores2(credito.getCuitAuthValores2());
		bp.saveOrUpdate(existente);
		// queda para mostrar luego de guardar
		credito = existente;
		buscarDesembolsos();
		return errores.isEmpty();
	}

	private boolean solicitarDesembolso() {
		Desembolso existente = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		Objetoi credito = existente.getCredito();
		if (!credito.validarRegistroGarantiaDesembolso()) {
			errores.put("desembolso.garantiaInscripcion", "desembolso.garantiaInscripcion");
			return false;
		}

		existente.setEstado("1"); // PENDIENTE

		bp.update(existente);

		try {
			buscarObjetoi();
		} catch (HibernateException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		buscarDesembolsos();

		return errores.isEmpty();
	}

	public boolean ejecutarDesembolso(Long idDesembolso, Double total, Date fechaEjecucion, Integer numeroRecibo,
			Double otrosGastos, Double gastosAdministrativos, Double gastosNovedades, Double gastosARecuperar)
			throws Exception {
		this.setIdDesembolso(idDesembolso);
		desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		if (!desembolso.getEstado().trim().equals("1")) {
			log.info("****************El desembolso " + desembolso.getId() + " no se encuentra en estado 1");
			if (desembolso.getEstado().trim().equals("2") || desembolso.getEstado().trim().equals("5")) {
				return true;
			}
			return false;
		}

		if (esCuentaPropia == null) {
			if (desembolso.getCuentaPropia() != null) {
				if (desembolso.getCuentaPropia() == true) {
					esCuentaPropia = true;
				} else {
					esCuentaPropia = false;
				}
			} else {
				esCuentaPropia = true;
			}
		}

		// gastos adicionales + administrativos
		desembolso.setImporteGastos(
				(otrosGastos != null ? otrosGastos : 0) + (gastosAdministrativos != null ? gastosAdministrativos : 0));

		boolean esCosecha = desembolso.getCredito().getLinea().isLineaCosecha();
		if (esCosecha) {
			EjecucionDesembolsosMasivos em = new EjecucionDesembolsosMasivos();
			if (desembolso.getNumero().intValue() == 1)
				em.ejecutarDesembolso1(desembolso, total, fechaEjecucion, numeroRecibo);
			else
				em.ejecutarDesembolso2(desembolso, total, fechaEjecucion, numeroRecibo);
			if (!em.getErrors().isEmpty())
				log.info(em.getErrors());
			return em.getErrors().isEmpty();
		} else {
			// this.setUsaInterbanking(desembolso.getCuentaInterbankingId()==null?false:true);
			setObjetoi(desembolso.getCredito());
			setIdObjetoi(getObjetoi().getId());
			desembolso.setImporteReal(total);
			desembolso.setFechaReal(fechaEjecucion);
			desembolso.setNumeroRecibo(numeroRecibo);
			bp.update(desembolso);
			bp.getCurrentSession().evict(this.desembolso);
			this.buscarObjetoi();
			return this.ejecutarDesembolso();
		}
	}

	@SuppressWarnings("unchecked")
	private boolean ejecutarDesembolso() {
		Desembolso existente = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		if (existente.getNroOrden() == null || existente.getNroOrden().longValue() <= 0) {
			return false;
		}

		if (cuentaTerceroId != null) {
			cuentaTercero = (CuentaBancaria) bp.getById(CuentaBancaria.class, new Long(cuentaTerceroId));
		}
		try {
			buscarObjetoi();
		} catch (HibernateException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		existente.setObservacion(desembolso.getObservacion());
		usaInterbanking = desembolso.getCuentaInterbankingId() != null || cuentaTercero != null;
		if (usaInterbanking != null && usaInterbanking) {
			if (!esCuentaPropia) {
				existente.setCuentaInterbanking(cuentaTercero);
				existente.setCuentaPropia(false);
			} else {
				existente.setCuentaInterbanking(desembolso.getCuentaInterbanking());
				existente.setCuentaPropia(true);
			}
		} else {
			existente.setCuentaInterbanking(null);
		}

		ObjetoiEstado estadoViejo = getObjetoi().getEstadoActual();
		Long cantidadDesembolsos = (Long) bp.createQuery("SELECT COUNT(d) FROM Desembolso d WHERE d.credito = :credito")
				.setParameter("credito", credito).uniqueResult();
		boolean esCosecha = existente.getCredito().getLinea().isLineaCosecha();
		if (cantidadDesembolsos == 1 || !esCosecha) {
			if (!estadoViejo.getEstado().getNombreEstado().equalsIgnoreCase("EJECUCION")) {
				estadoViejo.setFechaHasta(new Date());
				bp.saveOrUpdate(estadoViejo);
				Estado estado = (Estado) bp.createQuery("Select e from Estado e where e.nombreEstado=:nombre")
						.setParameter("nombre", "EJECUCION").uniqueResult();
				ObjetoiEstado estadoNuevo = new ObjetoiEstado();
				estadoNuevo.setFecha(new Date());
				estadoNuevo.setEstado(estado);
				estadoNuevo.setObjetoi(getObjetoi());
				bp.saveOrUpdate(estadoNuevo);

			}
		} else {
			if (existente.getNumero() == 1) {
				if (verificarPrimerDesembolso()) {
					estadoViejo.setFechaHasta(new Date());
					bp.saveOrUpdate(estadoViejo);
					Estado estado = (Estado) bp.createQuery("Select e from Estado e where e.nombreEstado=:nombre")
							.setParameter("nombre", "PENDIENTE SEGUNDO DESEMBOLSO").uniqueResult();
					ObjetoiEstado estadoNuevo = new ObjetoiEstado();
					estadoNuevo.setFecha(new Date());
					estadoNuevo.setEstado(estado);
					estadoNuevo.setObjetoi(getObjetoi());
					bp.saveOrUpdate(estadoNuevo);
				}
			} else {
				if (!estadoViejo.getEstado().getNombreEstado().equalsIgnoreCase("EJECUCION")) {
					estadoViejo.setFechaHasta(new Date());
					bp.saveOrUpdate(estadoViejo);

					Estado estado = (Estado) bp.createQuery("Select e from Estado e where e.nombreEstado=:nombre")
							.setParameter("nombre", "EJECUCION").uniqueResult();

					ObjetoiEstado estadoNuevo = new ObjetoiEstado();
					estadoNuevo.setFecha(new Date());
					estadoNuevo.setEstado(estado);
					estadoNuevo.setObjetoi(getObjetoi());
					bp.saveOrUpdate(estadoNuevo);
				}
			}
		}

		existente.setEstado("2"); // Realizado
		existente.setFechaReal(desembolso.getFechaReal());
		existente.setImporteReal(desembolso.getImporteReal());
		existente.setImporteGastos(desembolso.getImporteGastos());
		existente.setNumeroRecibo(desembolso.getNumeroRecibo());

		existente.setImporteMultas(desembolso.getImporteMultas());
		if (existente.getImporteMultas() == null) {
			existente.setImporteMultas(new Double(0));
		}
		bp.saveOrUpdate(existente);
		try {
			buscarObjetoi();
		} catch (HibernateException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			CreditoHandler handler = new CreditoHandler();
			handler.ejecutarDesembolso(getObjetoi(), existente);

			List<NovedadCtaCte> detalles = bp.createQuery("SELECT n FROM NovedadCtaCte n LEFT JOIN n.detalleFactura d "
					+ "LEFT JOIN d.factura f WHERE n.credito.id = :id AND (d is null or f.fechaPago is not null)")
					.setParameter("id", getIdObjetoi()).list();
			// solo se impacta cuando la moneda es Pesos
			if (credito.getLinea().getMoneda_id() != null && credito.getLinea().getMoneda_id().longValue() == 1L) {
				for (NovedadCtaCte novedad : detalles) {
					impactarDetalleFactura(novedad, existente);
				}
				for (NovedadCtaCte novedad : detalles) {
					if (novedad.getMovimientoDeb() != null && novedad.getMovimientoDeb().getDesembolso() != null
							&& novedad.getMovimientoDeb().getDesembolso().getId().equals(existente.getId())) {
						crearPagos(novedad, existente);
					}
				}
			}
			if (existente.getNumero().intValue() == 1 && esCosecha) {
				handler.comprobarInmovilizacion(credito.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			errores.put("desembolso.ejecutar.error", "desembolso.ejecutar.error");
		}
		// para mostrar al volver a la lista
		buscarDesembolsos();
		return errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public Cuota buscarCuotaActual() {
		List<Cuota> cuotas = bp.createQuery(
				"SELECT c FROM Cuota c WHERE c.fechaVencimiento >= :hoy AND c.estado =:estado AND c.credito.id = :idCredito ORDER BY c.numero")
				.setParameter("hoy", new Date()).setParameter("estado", "1").setParameter("idCredito", getIdObjetoi())
				.list();

		if (cuotas != null && cuotas.size() > 0) {
			return cuotas.get(0);
		} else {
			return null;
		}
	}

	private boolean impactarDetalleFactura(NovedadCtaCte detalle, Desembolso desembolso) {
		Cuota c = buscarCuotaActual();
		Tipomov tipoDebito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", "DB")
				.uniqueResult();
		// Tipomov tipoCredito = (Tipomov)
		// bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura",
		// "CR").uniqueResult();
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
		Concepto concepto = Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR);

		if (!detalle.isImpactado()) {
			// crea una nota de debito
			Boleto notaDebito = new Boleto();
			notaDebito.setFechaEmision(desembolso.getFechaReal());
			notaDebito.setFechaVencimiento(c.getFechaVencimiento());
			notaDebito.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
			notaDebito.setUsuario(usuario);
			notaDebito.setNumeroCuota(c.getNumero());
			notaDebito.setObjetoi(getObjetoi());
			notaDebito.setPeriodoBoleto((long) periodo);
			notaDebito.setVerificadorBoleto(0L);
			notaDebito.setImporte(gastos);
			notaDebito.setNumeroBoleto(Numerador.getNext(notaDebito.getNumerador()));
			bp.save(notaDebito);

			Ctacte deb = getObjetoi().crearCtacte(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(),
					desembolso.getFechaReal(), notaDebito, getObjetoi(), c, Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
			deb.setTipomov(tipoDebito);
			deb.setTipoMovimiento("cuota");
			deb.setAsociado(concepto);
			deb.setFacturado(concepto);
			deb.setUsuario(usuario);
			deb.getId().setMovimientoCtacte(movimientoCtaCte);
			deb.getId().setItemCtacte(new Long(1));
			deb.setBoleto(notaDebito);
			// deb.setDetalle("Gastos Proveedor: "+
			// detalle.getFactura().getProveedor().getPersona().getNomb12()
			// +" - Factura: "
			// + detalle.getFactura().getNumero());
			deb.setDetalle(detalle.getDetalle());
			deb.setContabiliza(false);// este d�bito se contabiliza desde GAF
			deb.setDesembolso(desembolso);
			// crear nuevo registro bolcon
			Bolcon bcDeb = getObjetoi().crearBolcon(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(),
					notaDebito, deb.getId(), c);
			bcDeb.setTipomov(tipoDebito);

			bp.save(deb);
			bp.save(bcDeb);

			detalle.setMovimientoDeb(deb);
			detalle.setImpactado(true);
			bp.update(detalle);

			if (detalle.getDetalleFactura() != null) {
				detalle.getDetalleFactura().setMovimientoCtaCte(deb.getId().getMovimientoCtacte());
				detalle.getDetalleFactura().setImpactado(true);
				bp.update(detalle.getDetalleFactura());
			}

			return true;
		}

		return false;
	}

	public void crearPagos(NovedadCtaCte detalle, Desembolso desembolso) {
		Cuota c = buscarCuotaActual();
		Tipomov tipoCredito = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura").setString("abreviatura", "CR")
				.uniqueResult();
		int periodo = Calendar.getInstance().get(Calendar.YEAR);
		String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
		long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);

		if (detalle.getMovimientoDeb() == null || detalle.getMovimientoCred() == null) {
			// crea una nota de credito
			Boleto notaCredito = new Boleto();
			notaCredito.setFechaEmision(desembolso.getFechaReal());
			notaCredito.setFechaVencimiento(c.getFechaVencimiento());
			notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
			notaCredito.setUsuario(usuario);
			notaCredito.setNumeroCuota(c.getNumero());
			notaCredito.setObjetoi(getObjetoi());
			notaCredito.setPeriodoBoleto((long) periodo);
			notaCredito.setVerificadorBoleto(0L);
			notaCredito.setImporte(detalle.getImporte());
			notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));
			bp.save(notaCredito);

			// Creo credito el la cta cte
			Ctacte cc = getObjetoi().crearCtacte(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(),
					desembolso.getFechaReal(), notaCredito, getObjetoi(), c, Ctacte.TIPO_CONCEPTO_DESEMBOLSO);
			cc.setTipomov(tipoCredito);
			cc.setTipoMovimiento("Pago");
			cc.setUsuario(usuario);
			cc.getId().setMovimientoCtacte(movimientoCtaCte);
			cc.getId().setItemCtacte(new Long(2));
			// cc.setDetalle("Gastos Proveedor: "+
			// detalle.getFactura().getProveedor().getPersona().getNomb12()
			// +" - Factura: "
			// + detalle.getFactura().getNumero());
			cc.setDetalle(detalle.getDetalle());
			cc.setDesembolso(desembolso);
			bp.save(cc);

			Bolcon bc = getObjetoi().crearBolcon(Concepto.CONCEPTO_GASTOS_A_RECUPERAR, detalle.getImporte(),
					notaCredito, cc.getId(), c);
			bc.setTipomov(tipoCredito);
			bp.save(bc);

			detalle.setImpactado(true);

			if (detalle.getDetalleFactura() != null && !detalle.isImpactado()) {
				detalle.getDetalleFactura().setImpactado(true);
				bp.update(detalle.getDetalleFactura());
			}

			detalle.setMovimientoCred(cc);
			bp.update(detalle);
		}
	}

	public boolean guardarCuentaBancaria() {
		Desembolso existente = (Desembolso) bp.getById(Desembolso.class, idDesembolso);
		if (cuentaTercero != null) {
			cuentaTercero = (CuentaBancaria) bp.getById(CuentaBancaria.class, new Long(cuentaTerceroId));
			if (!verificarDatos()) {
				return false;
			}
		}
		existente.setObservacion(desembolso.getObservacion());
		if (usaInterbanking != null && usaInterbanking) {
			if (!esCuentaPropia) {
				existente.setCuentaInterbanking(cuentaTercero);
			} else {
				existente.setCuentaInterbanking(desembolso.getCuentaInterbanking());
			}
		} else {
			existente.setCuentaInterbanking(null);
		}
		bp.update(existente);
		buscarDesembolsos();
		return errores.isEmpty();
	}

	@SuppressWarnings("rawtypes")
	public boolean verificarPermisoSolicitarGAF() {
		Menu menu = (Menu) bp
				.createSQLQuery(
						"SELECT * FROM MENU WHERE RUTA = '/services/Imputaciones/imputarDesembolso' AND IDMODULO = 5")
				.addEntity(Menu.class).setMaxResults(1).uniqueResult();
		if (menu == null) {
			return false;
		}
		String sUsuarioLogeado = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		int tipo = 2;
		if (menu.getTipo().intValue() == 2) {
			// Tipo 2=No requiere permiso
			if (bp.getCurrentSession().contains(menu))
				bp.getCurrentSession().evict(menu);
			return true;
		}
		if (bp.getCurrentSession().contains(menu))
			bp.getCurrentSession().evict(menu);

		// Tengo los datos, consulto por los permisos del usuario
		List list = bp.getByFilter("FROM Permiso p WHERE p.id.causerk='" + sUsuarioLogeado + "' AND p.id.idmenu= '"
				+ menu.getId().toString() + "'");
		if (list.size() == 1) {// Si aca define un permiso, es el que va
			Permiso permiso = (Permiso) list.get(0);
			bp.getCurrentSession().evict(permiso);
			if (permiso.getTipo() != null && permiso.getTipo().intValue() >= tipo) {
				return true;
			} else {
				return false;
			}
		}
		// si no tiene permiso, me fijo si el perfil lo tiene
		try {
			Claves usuario = (Claves) bp.getById(Claves.class, sUsuarioLogeado);
			if (usuario.getPerfilK() == null) {
				return false;
			}
			Integer cant = new Integer(bp
					.getCurrentSession().createQuery("SELECT p.tipo FROM Permiso p WHERE p.id.causerk='"
							+ usuario.getPerfilK() + "' AND p.id.idmenu='" + menu.getId().toString() + "'")
					.uniqueResult().toString());
			return cant.intValue() >= tipo;
		} catch (Exception e) { // puede entrar por objectnot found o por
			// nullpointerexception

			return false;
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	private boolean verificarImporteDesembolsos() {
		List<Desembolso> listaDesembolso = bp
				.createQuery("select d from Desembolso d where d.credito = :credito order by d.numero")
				.setParameter("credito", getObjetoi()).list();
		if (listaDesembolso != null && !listaDesembolso.isEmpty()) {
			if (sumarMonto(listaDesembolso) > credito.getFinanciamiento()) {
				this.errores.put("CronogramaDesembolso.importe", "CronogramaDesembolso.importe");
			}
		}
		return errores.isEmpty();
	}

	private int sumarMonto(List<Desembolso> listaDesembolso) {
		int suma = 0;
		for (Desembolso desembolso : listaDesembolso) {
			suma += desembolso.getImporte();
		}
		return suma;
	}

	public void desistir() {
		try {

			desembolso = (Desembolso) bp.getById(Desembolso.class, desembolso.getId());

			// Ajustar imputaciones
			ImputacionesHelper ih = new ImputacionesHelper();
			ih.setEtapa(1l);
			ih.setTipoExpediente(15l);
			try {
				log.debug("Llamo a imputacion WS.");
				ih.imputarDesembolsoDesistido(desembolso);
				log.debug("Imputacion WS OK.");
				if (estadoDesistimiento == "1") {
					desembolso.setMotivoModificacion("Solicitado por la parte tomadora");
				} else if (estadoDesistimiento == "2") {
					desembolso.setMotivoModificacion("Econom�a de fondos en la aplicaci�n al destino del cr�dito");
				} else if (estadoDesistimiento == "3") {
					desembolso.setMotivoModificacion("Modificaci�n del destino y/o importe inicial del cr�dito");
				} else if (estadoDesistimiento == "4") {
					desembolso.setMotivoModificacion("Incumplimiento de otras obligaciones contractuales");
				} else if (estadoDesistimiento == "5") {
					desembolso.setMotivoModificacion("Otras causales de reducci�n");
				}

			} catch (Exception e) {
				errores.put("desembolso.desistir.ws", "desembolso.desistir.ws");
				e.printStackTrace();
				return;
			}
			// desistir desembolso
			desembolso = (Desembolso) bp.getById(Desembolso.class, desembolso.getId());
			desembolso.setEstado("3");
			desembolso.setUsuarioDesiste(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			bp.begin();
			bp.update(desembolso);

			credito = (Objetoi) bp.getById(Objetoi.class, desembolso.getCredito_id());
			if (credito.getTieneGarantiaFiduciaria()) {
				credito.setLiberar(true);
				bp.update(credito);
			}

			if (desembolso.getNumero() == 2 && credito.getLinea().isLineaCosecha()
					|| credito.getLinea().isLineaCosechaPreAprobada() && !credito.getEstadoActual().getEstado()
							.getNombreEstado().equalsIgnoreCase(AbstractEstado.EJECUCION)) {
				credito.setEstadoActual(AbstractEstado.EJECUCION, "Desistimiento de segundo desembolso.");
			}
			Auditar();
			bp.commit();
			desistidoOk = true;
		} catch (Exception e) {
			bp.rollback();
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void Auditar() {
		String sql = "select d from Desembolso d where d.id = " + desembolso.getId();
		List<Desembolso> midesembolso = bp.createQuery(sql).list();

		if (midesembolso.get(0).getNumero() == 2 && midesembolso.get(0).getCredito().getLinea().isLineaCosecha()) {
			sql = "select d from Desembolso d where d.credito = " + midesembolso.get(0).getCredito_id();
			sql += " order by numero";
			List<Desembolso> desenbolsos = bp.createQuery(sql).list();
			for (Desembolso undesembolso : desenbolsos) {
				if (undesembolso.getNumero() == 1 && undesembolso.getEstado().equals(Desembolso.REALIZADO)) {
					Double TotalQQTotal = undesembolso.getCredito().getQqIngresadosTotales();
					Double TotalQQContrato = undesembolso.getCredito().getQqsolicitado();
					float porcAuditoriaFinal = undesembolso.getCredito().getCosechaConfig().getPorcAuditoriaFinal();
					float porc = (float) ((TotalQQTotal * 100) / TotalQQContrato);
					if (porc >= porcAuditoriaFinal) {
						AuditoriaFinal auditoriafina = new AuditoriaFinal();
						auditoriafina.setFechaProceso(new Date());
						auditoriafina.setCredito(undesembolso.getCredito());
						auditoriafina.setObservaciones(
								"Carga autom�tica - desistiemiento 2� desembolso y QQ Ingresados superior al 35%");
						auditoriafina.setAplicaFondo("si");
						bp.save(auditoriafina);
					}

				}
			}
		}

	}

	@SuppressWarnings("unchecked")
	private boolean verificarDatos() {

		Objetoi credito = getObjetoi();

		if (credito.getTipoAmortizacion() == null) {
			errores.put("desembolsosEjecucion.tipoAmortizacionNulo", "desembolsosEjecucion.tipoAmortizacionNulo");
		}

		List<ObjetoiIndice> listaIndice = bp.createQuery("SELECT o FROM ObjetoiIndice o WHERE o.credito = :credito")
				.setParameter("credito", credito).list();

		if (listaIndice == null || listaIndice.isEmpty()) {
			errores.put("desembolsosEjecucion.tazaInteresCompensatorioNulo",
					"desembolsosEjecucion.tazaInteresCompensatorioNulo");
		}
		if (credito.getPrimerVencCapital() == null) {
			errores.put("desembolsosEjecucion.primerVencimientoCapitalNulo",
					"desembolsosEjecucion.primerVencimientoCapitalNulo");
		}
		if (credito.getPlazoCapital() == null) {
			errores.put("desembolsosEjecucion.cantidadCuotasCampitalNulo",
					"desembolsosEjecucion.cantidadCuotasCampitalNulo");
		}
		if (credito.getPrimerVencInteres() == null) {
			errores.put("desembolsosEjecucion.primerVencimientoInteresNulo",
					"desembolsosEjecucion.primerVencimientoInteresNulo");
		}
		if (credito.getPlazoCompensatorio() == null) {
			errores.put("desembolsosEjecucion.cantidadCuotasInteresNulo",
					"desembolsosEjecucion.cantidadCuotasInteresNulo");
		}
		if (credito.getFrecuenciaCapital() == null) {
			errores.put("desembolsosEjecucion.periocidadCuotasCapitalNulo",
					"desembolsosEjecucion.periocidadCuotasCapitalNulo");
		}
		if (credito.getFrecuenciaInteres() == null) {
			errores.put("desembolsosEjecucion.periocidadCuotasInteresNulo",
					"desembolsosEjecucion.periocidadCuotasInteresNulo");
		}
		if (credito.getLinea().getMoneda() == null) {
			errores.put("desembolsosEjecucion.monedaNulo", "desembolsosEjecucion.monedaNulo");
		}

		return errores.isEmpty();
	}

	private boolean validarObjetoi() {
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean verificarPrimerDesembolso() {
		List<ObjetoiVinedo> vin = bp.createQuery("Select v from ObjetoiVinedo v where v.credito=:credito")
				.setParameter("credito", credito).list();
		Double total = 0.0;
		for (ObjetoiVinedo ov : vin) {
			total += ov.getTotalQQIngresados();
		}
		if (total < (credito.getQqFinal() * 0.7)) {
			return false;
		}
		if (!credito.getEstadoActual().getEstado().getNombreEstado().trim().equals("PRIMER DESEMBOLSO")) {
			return false;
		}
		Desembolso segundo = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", credito.getId()).setInteger("numero", 2).setMaxResults(1).uniqueResult();
		if (segundo == null) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public String anularEntregaPago(Long idDesembolso, Date fecha) throws Exception {
		Desembolso desembolso = (Desembolso) bp.getById(Desembolso.class, idDesembolso);

		if (desembolso.getEstado().equals("1")) {
			// ya esta anulado o esta pendiente
			return "ok";
		}

		this.credito = desembolso.getCredito();

		Number ultimoEjecutado = (Number) bp.createQuery(
				"select d.numero from Desembolso d where d.credito.id = :idCredito and d.ejecutado = true order by d.numero desc")
				.setParameter("idCredito", desembolso.getCredito().getId()).setMaxResults(1).uniqueResult();
		if (ultimoEjecutado != null && ultimoEjecutado.longValue() != desembolso.getNumero().longValue()) {
			throw new Exception("Solo se puede anular el ultimo desembolso ejecutado.");
		}

		boolean ejecutado = false;
		if (desembolso.getEjecutado() != null && desembolso.getEjecutado() == Boolean.TRUE) {
			ejecutado = true;
		}

		if (fecha == null)
			fecha = new Date();

		if (ejecutado) {
			// Validaciones
			List<Object[]> ccs = bp.createSQLQuery(
					"SELECT cc.objetoi_id, cc.periodoCtacte, cc.movimientoCtacte, cc.verificadorCtacte, cc.itemCtacte "
							+ "FROM Ctacte cc JOIN Concepto co ON co.id = cc.facturado_id "
							+ "JOIN CConcepto cco ON cco.concepto = co.concepto_concepto "
							+ "WHERE cc.objetoi_id = :objetoi_id AND cco.detalle = 'Compensatorio'")
					.setLong("objetoi_id", desembolso.getCredito_id()).list();
			if (!ccs.isEmpty()) {
				return "No es posible anular por existir intereses devengados.";
			}

			try {
				Usuario usuario = (Usuario) bp.getById(Usuario.class,
						SessionHandler.getCurrentSessionHandler().getCurrentUser());
				int periodo = Calendar.getInstance().get(Calendar.YEAR);
				// Crear nota de cr�dito capital
				Boleto notaCredito = new Boleto();
				notaCredito.setFechaEmision(fecha);
				notaCredito.setFechaVencimiento(null);
				notaCredito.setTipo(Boleto.TIPO_BOLETO_NOTACRED);
				notaCredito.setUsuario(usuario);
				notaCredito.setNumeroCuota(null);
				notaCredito.setObjetoi(desembolso.getCredito());
				notaCredito.setPeriodoBoleto((long) periodo);
				notaCredito.setVerificadorBoleto(0L);
				notaCredito.setNumeroBoleto(Numerador.getNext(notaCredito.getNumerador()));

				double importeBoleto = desembolso.getImporteReal();
				importeBoleto = Math.rint(importeBoleto * 100) / 100;
				notaCredito.setImporte(importeBoleto);

				bp.save(notaCredito);

				List<Cuota> cuotas = bp.createSQLQuery(
						"SELECT * FROM Cuota c WHERE c.credito_id = :credito_id AND c.fechaVencimiento >= :fecha")
						.addEntity(Cuota.class).setLong("credito_id", desembolso.getCredito_id())
						.setDate("fecha", desembolso.getFechaReal()).list();

				Cuota cuotaGtos = null;
				if (cuotas.size() > 0) {
					cuotaGtos = cuotas.get(0);
				}

				Concepto conceptoCapital = Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL);
				Tipomov tipoMov = (Tipomov) bp.getNamedQuery("Tipomov.findByAbreviatura")
						.setString("abreviatura", Tipomov.TIPOMOV_CREDITO).uniqueResult();
				String numeradorCtaCte = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
				long movimientoCtaCte = Numerador.getNext(numeradorCtaCte);
				long itemCtaCte = 1;

				// Buscar Ctactes generadas con el desembolso
				List<Ctacte> ccsAnteriores = bp.createQuery(
						"select c from Ctacte c where c.desembolso.id = :idDesembolso and c.tipoMovimiento = 'cuota'")
						.setParameter("idDesembolso", desembolso.getId()).list();
				for (Ctacte ccAnterior : ccsAnteriores) {

					Cuota cuota = ccAnterior.getCuota();

					// Crear cuentas corrientes CREDITO de capital
					Ctacte cc = new Ctacte();
					cc.setImporte(ccAnterior.getImporte());
					cc.setAsociado(conceptoCapital);
					cc.setFacturado(conceptoCapital);
					cc.setTipomov(tipoMov);
					cc.setTipoMovimiento("movManualCred");
					cc.setFechaGeneracion(fecha);
					cc.setFechaVencimiento(cuota.getFechaVencimiento());
					cc.setCuota(cuota);
					cc.setUsuario(usuario);
					cc.setBoleto(notaCredito);
					cc.setDetalle("Anulaci�n de Desembolso Recibo Nro. " + desembolso.getNumeroRecibo());

					CtacteKey ck = new CtacteKey();
					ck.setObjetoi(cuota.getCredito());
					ck.setPeriodoCtacte(new Long(periodo));
					ck.setMovimientoCtacte(movimientoCtaCte);
					// TODO: reemplazar con Digito Verificador
					ck.setVerificadorCtacte(0L);
					ck.setItemCtacte(itemCtaCte++);
					cc.setId(ck);
					cc.setCotizaMov(ccAnterior.getCotizaMov());
					cc.setTipoConcepto(Ctacte.TIPO_CONCEPTO_ANULACION_DESEMBOLSO);

					bp.save(cc);

					Bolcon bolcon = new Bolcon();
					bolcon.setFacturado(conceptoCapital);
					bolcon.setOriginal(conceptoCapital);
					bolcon.setImporte(ccAnterior.getImporte());
					bolcon.setCuota(cuota);
					bolcon.setTipomov(tipoMov);

					BolconKey bck = new BolconKey();
					bck.setBoleto(notaCredito);
					bck.setPeriodoCtacte(ck.getPeriodoCtacte());
					bck.setMovimientoCtacte(ck.getMovimientoCtacte());
					bck.setVerificadorCtacte(ck.getVerificadorCtacte());
					bck.setItemCtacte(ck.getItemCtacte());
					bolcon.setId(bck);
					bp.save(bolcon);

					// restar capital sumado a las cuotas
					List<DetalleCuota> detallesCuota = bp.createQuery(
							"select d from DetalleCuota d where d.cuota.id = :idCuota and d.desembolso = :numero")
							.setParameter("idCuota", cuota.getId()).setParameter("numero", desembolso.getNumero())
							.setMaxResults(1).list();
					for (DetalleCuota detalle : detallesCuota) {
						cuota.setCapital(cuota.getCapital() - detalle.getCapital());
						cuota.setCompensatorio(cuota.getCompensatorio() - detalle.getCompensatorio());
					}
					bp.update(cuota);

					// Modificar debitos de capital anteriores, con
					// tipoMovimiento = cuota
					// Hacer que sean movManualDeb para que no se consideren
					// como parte del desembolso
					// y se separen en la Composicion de Credito y Cuenta
					// Corriente
					ccAnterior.setTipoMovimiento("movManualDeb");
					ccAnterior.setDetalle("Desembolso anulado");
					ccAnterior.setDesembolso(null);
					bp.update(ccAnterior);
				}

				if (desembolso.getImporteGastos() != null && desembolso.getImporteGastos() != 0) {
					String numeradorCtaCteGtos = Ctacte.NUMERADOR_CTACTE.replaceAll("periodo", "" + periodo);
					Tipomov tipoMovDeb = (Tipomov) bp.createQuery("Select t from Tipomov t where t.abreviatura=:ab")
							.setParameter("ab", "db").uniqueResult();
					long movimientoCtaCteGtos = Numerador.getNext(numeradorCtaCteGtos);

					if (cuotaGtos != null) {
						// crear nota de debito por anulacion de Pago de Gastos
						Boleto boleto = new Boleto();
						boleto.setFechaEmision(fecha);
						boleto.setFechaVencimiento(fecha);
						boleto.setTipo(Boleto.TIPO_BOLETO_NOTADEB);
						boleto.setUsuario(usuario);
						boleto.setObjetoi(credito);
						boleto.setPeriodoBoleto((long) periodo);
						boleto.setVerificadorBoleto(0L);
						boleto.setImporte(desembolso.getImporteGastos());
						boleto.setNumeroBoleto(Numerador.getNext(boleto.getNumerador()));
						bp.save(boleto);

						// crea entrada en CtaCte para anulacion de Pago de
						// Gastos (debito)
						Ctacte ctacte = credito.crearCtacte(Concepto.CONCEPTO_GASTOS, desembolso.getImporteGastos(),
								fecha, boleto, credito, cuotaGtos, Ctacte.TIPO_CONCEPTO_ANULACION_DESEMBOLSO);
						ctacte.setTipomov(tipoMovDeb);
						ctacte.setTipoMovimiento("movManualDeb");
						ctacte.setUsuario(usuario);
						ctacte.getId().setMovimientoCtacte(movimientoCtaCteGtos);
						ctacte.getId().setItemCtacte(1L);
						ctacte.setBoleto(boleto);
						ctacte.setContabiliza(false);

						Bolcon bolcon = credito.crearBolcon(Concepto.CONCEPTO_GASTOS, desembolso.getImporteGastos(),
								boleto, ctacte.getId(), cuotaGtos);
						bolcon.setTipomov(tipoMovDeb);

						bp.save(ctacte);
						bp.save(bolcon);
					}
				}

				desembolso.setFechaReal(null);
				desembolso.setImporteReal(null);
				desembolso.setNumeroRecibo(null);
				desembolso.setEstado("1");
				desembolso.setEjecutado(false);
				bp.update(desembolso);

				// actualizar estado de credito
				boolean esCosecha = desembolso.getCredito().getLinea().isLineaCosecha();
				String observacionEstado = "Anulacion de entrega de pago";

				if (desembolso.getNumero() == 1) {
					desembolso.getCredito().setEstadoActual(AbstractEstado.CONTRATO_EMITIDO, observacionEstado);
				} else {
					if (esCosecha) {
						desembolso.getCredito().setEstadoActual(AbstractEstado.PENDIENTE_SEGUNDO_DESEMBOLSO,
								observacionEstado);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception();
			}
		}
		return "ok";
	}

	@SuppressWarnings("unchecked")
	public String anularPagosDesembolsos(Long idOrdepago, Long nroOrden) throws Exception {
		List<Desembolso> desembolsos = bp.createSQLQuery("SELECT * FROM Desembolso d WHERE d.idOrdepago = :idOrdepago")
				.addEntity(Desembolso.class).setLong("idOrdepago", idOrdepago).list();
		if (desembolsos.size() > 1) {
			return "Existe m�s de un desembolso asociado a la misma orden de pago.";
		}
		if (desembolsos.size() == 0) {
			return "No se encontr� un desembolso asociado a la orden de pago.";
		}
		for (Desembolso desembolso : desembolsos) {
			// Validaciones
			List<Object[]> ccs = bp.createSQLQuery(
					"SELECT cc.objetoi_id, cc.periodoCtacte, cc.movimientoCtacte, cc.verificadorCtacte, cc.itemCtacte FROM Ctacte cc JOIN Concepto co ON co.id = cc.facturado_id "
							+ "JOIN CConcepto cco ON cco.concepto = co.concepto_concepto WHERE cc.objetoi_id = :objetoi_id AND cco.detalle = 'Compensatorio'")
					.setLong("objetoi_id", desembolso.getCredito_id()).list();
			if (!ccs.isEmpty()) {
				return "No es posible anular por existir intereses devengados.";
			}

			// Modificar desembolso
			desembolso.setNroOrden(null);
			desembolso.setIdOrdepago(null);
			desembolso.setOrdepago_ejercicio(null);

			bp.update(desembolso);
		}
		return "ok";
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public List<Desembolso> getDesembolsos() {
		return desembolsos;
	}

	public void setDesembolsos(List<Desembolso> desembolsos) {
		this.desembolsos = desembolsos;
	}

	public Long getIdDesembolso() {
		return idDesembolso;
	}

	public void setIdDesembolso(Long idDesembolso) {
		this.idDesembolso = idDesembolso;
	}

	public Desembolso getDesembolso() {
		return desembolso;
	}

	public void setDesembolso(Desembolso desembolso) {
		this.desembolso = desembolso;
	}

	public boolean isUsaInterbanking() {
		return usaInterbanking;
	}

	public void setUsaInterbanking(boolean usaInterbanking) {
		this.usaInterbanking = usaInterbanking;
	}

	public boolean isEjecutado() {
		return ejecutado;
	}

	public void setEjecutado(boolean ejecutado) {
		this.ejecutado = ejecutado;
	}

	public ReportResult getReporte() {
		return reporte;
	}

	public void setReporte(ReportResult reporte) {
		this.reporte = reporte;
	}

	@Override
	public void setForward(String forward) {
		this.forward = forward;
	}

	public boolean isEsCuentaPropia() {
		return esCuentaPropia;
	}

	public void setEsCuentaPropia(boolean esCuentaPropia) {
		this.esCuentaPropia = esCuentaPropia;
	}

	public String getRelacionId() {
		return relacionId;
	}

	public void setRelacionId(String relacionId) {
		this.relacionId = relacionId;
	}

	public CuentaBancaria getCuentaTercero() {
		return cuentaTercero;
	}

	public void setCuentaTercero(CuentaBancaria cuentaTercero) {
		this.cuentaTercero = cuentaTercero;
	}

	public String getCuentaTerceroId() {
		return cuentaTerceroId;
	}

	public void setCuentaTerceroId(String cuentaTerceroId) {
		this.cuentaTerceroId = cuentaTerceroId;
	}

	public boolean isPendiente() {
		return pendiente;
	}

	public void setPendiente(boolean pendiente) {
		this.pendiente = pendiente;
	}

	public void setImporteGastos(Double importeGastos) {
		this.importeGastos = importeGastos;
	}

	public Double getImporteGastos() {
		return importeGastos;
	}

	public void setImporteReal(Double importeReal) {
		this.importeReal = importeReal;
	}

	public Double getImporteReal() {
		return importeReal;
	}

	public void setAnalisis(boolean analisis) {
		this.analisis = analisis;
	}

	public boolean isAnalisis() {
		return analisis;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public Double getGastos() {
		return gastos;
	}

	public void setGastos(Double gastos) {
		this.gastos = gastos;
	}

	public String getEstadoDesistimiento() {
		return estadoDesistimiento;
	}

	public void setEstadoDesistimiento(String estadoDesistimiento) {
		this.estadoDesistimiento = estadoDesistimiento;
	}

	public boolean isDesistidoOk() {
		return desistidoOk;
	}

	public void setDesistidoOk(boolean desistidoOk) {
		this.desistidoOk = desistidoOk;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
