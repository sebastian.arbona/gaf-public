select c.id,
CASE WHEN con.concepto_concepto = 'bon' THEN 'com' ELSE con.concepto_concepto END as concepto,
SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) as saldo
from Cuota c
join Ctacte cc on cc.cuota_id = c.id
join ObjetoiEstado oe on oe.objetoi_id = c.credito_id and oe.fechaHasta is null
join Concepto con on con.id = cc.asociado_id
where oe.estado_idEstado = 12 and con.concepto_concepto <> 'cap'
group by c.id, CASE WHEN con.concepto_concepto = 'bon' THEN 'com' ELSE con.concepto_concepto END
having SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) >= 0.01
order by c.id