package com.asf.cred.business;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.nirven.creditos.hibernate.RequisitoDesembolso;

public class CreditoRequisitos implements IProcess {

	private String forward;
	private HashMap errores = new HashMap();

	@Override
	public boolean doProcess() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		String idObjetoi = request.getParameter("idObjetoi");
		String idDesembolso = request.getParameter("idDesembolso");
		Enumeration<String> paramNames = request.getParameterNames();
		Date hoy = new Date();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("requisito")) {
				String[] p = param.split("-");
				Long idRequisito = new Long(p[1]);
				RequisitoDesembolso requisito = (RequisitoDesembolso) bp.getById(RequisitoDesembolso.class,
						idRequisito);
				if (requisito.getFechaCumplido() == null) { // no pisar la fecha de cumplimiento
					requisito.setFechaCumplido(hoy);
				}
				bp.update(requisito);
			}
		}
		forward = "Location: " + request.getContextPath() + "/actions/abmAction.do?paramComp[0]==&idObjetoi="
				+ idObjetoi + "&paramValue[0]=" + idDesembolso
				+ "&paramName[0]=desembolsoId&do=list&filter=true&entityName=RequisitoDesembolso";
		return true;
	}

	@Override
	public HashMap getErrors() {
		return errores;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return "RequisitoDesembolsoList";
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public boolean validate() {
		return true;
	}

}
