package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.civitas.cred.beans.BeanCtaCte;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Objetoi;
/**
 * @author eselman
 */

public class ListarAcuerdoPago implements IProcess
{
//=========================ATRIBUTOS===========================
	//atributos utilitarios del proceso.
	private String forward;
	private HashMap<String, Object> errores;
	private String accion;
	private BusinessPersistance bp;
	private List<ObjetoiDTO> creditos;
	private Objetoi acuerdoPago;
	private Persona persona;

	//=======================CONSTRUCTORES=========================
	public ListarAcuerdoPago()
	{
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		acuerdoPago = new Objetoi();
		persona = new Persona();
		forward = "ListarAcuerdoPago";
		accion = "listar";
	}
//======================FUNCIONALIDADES========================
	public boolean doProcess() {
		if(accion.equals("listar")){
			listarCreditos();
		}else if(accion.equals("listar2")){
			listarAcuerdoPago();
		}
		return this.errores.isEmpty();
	}
//=====================UTILIDADES PRIVADAS=====================
	
	@SuppressWarnings("unchecked")
	public void listarCreditos()
	{
		ArrayList<Objetoi> objetois = (ArrayList<Objetoi>) 
													bp.createQuery("SELECT o " +
													"FROM Objetoi o " +
													"WHERE o.acuerdoPago = :acuerdoPago ")
													.setEntity("acuerdoPago", acuerdoPago)
													.list();
		CreditoHandler handler = new CreditoHandler();
		Estado estado;
		ObjetoiDTO credito;
		creditos = new ArrayList<ObjetoiDTO>();
		for (Objetoi o : objetois) {
			estado = handler.findEstado(o);
			if(estado != null && !estado.getNombreEstado().equals("ESPERANDO DOCUMENTACION") && o.getExpediente() != null && !o.getExpediente().isEmpty() && !o.getExpediente().equals("0")) {
				credito = new ObjetoiDTO(o, estado.getNombreEstado(), 0);
				CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
				calculoDeuda.calcular();
				BeanCtaCte beanTotal = calculoDeuda.getTotal();
				double deudaTotal = beanTotal.getSaldoCuota();
				//credito.setTotalAdeudado((buscarCuotas(o) ? calcularDeuda() : 0.0));
				credito.setTotalAdeudado(deudaTotal);
				o.setTotalDeudaActual(deudaTotal);
				credito.setComportamiento(o.getComportamientoActual());
				creditos.add(credito);
			} else if(estado == null) {
				credito = new ObjetoiDTO(o, "Sin estado", 0);
				credito.setComportamiento(o.getComportamientoActual());
				creditos.add(credito);
			}										
		}
	}
	
	public void listarAcuerdoPago(){
		acuerdoPago = (Objetoi) bp.getById(Objetoi.class, acuerdoPago.getId());
		CreditoHandler handler = new CreditoHandler();
		Estado estado;
		ObjetoiDTO credito;
		creditos = new ArrayList<ObjetoiDTO>();
		Objetoi o = acuerdoPago.getAcuerdoPago();
		estado = handler.findEstado(o);
		if(estado != null && !estado.getNombreEstado().equals("ESPERANDO DOCUMENTACION") && o.getExpediente() != null && !o.getExpediente().isEmpty() && !o.getExpediente().equals("0")) {
			credito = new ObjetoiDTO(o, estado.getNombreEstado(), 0);
			CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
			calculoDeuda.calcular();
			BeanCtaCte beanTotal = calculoDeuda.getTotal();
			double deudaTotal = beanTotal.getSaldoCuota();
			//credito.setTotalAdeudado((buscarCuotas(o) ? calcularDeuda() : 0.0));
			credito.setTotalAdeudado(deudaTotal);
			o.setTotalDeudaActual(deudaTotal);
			credito.setComportamiento(o.getComportamientoActual());
			creditos.add(credito);
		} else if(estado == null) {
			credito = new ObjetoiDTO(o, "Sin estado", 0);
			credito.setComportamiento(o.getComportamientoActual());
			creditos.add(credito);
		}
	}
	
	//========================VALIDACIONES=========================
	@Override
	public boolean validate()
	{
		return true;
	}
	@Override
	public HashMap<String, Object> getErrors() {
		return errores;
	}
	@Override
	public String getForward() {
		return forward;
	}
	@Override
	public String getInput() {
		return forward;
	}
	@Override
	public Object getResult() {
		return null;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}
	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}
	public Objetoi getAcuerdoPago() {
		return acuerdoPago;
	}
	public void setAcuerdoPago(Objetoi acuerdoPago) {
		this.acuerdoPago = acuerdoPago;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}