package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.civitas.hibernate.persona.Localidad;
import com.nirven.creditos.hibernate.Departamento;
import com.nirven.creditos.hibernate.LocalidadDepartamento;
import com.nirven.creditos.hibernate.LocalidadDepartamentoPK;

public class LocalidadDepartamentoProcess extends ConversationProcess {

	private Long idDepartamento;
	private List<Localidad> localidades;
	private String localidadesAsociadas;
	@Save(refresh = true)
	private Departamento departamento;

	private String forwardURL;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean listarLocalidades() {
		if (idDepartamento == null) {
			return false;
		}

		departamento = (Departamento) bp.getById(Departamento.class, idDepartamento);

		List<Localidad> asociadas = bp
				.createSQLQuery("select distinct * from LOCALIDAD l where l.IDLOCALIDAD in "
						+ "(select idLocalidad from LocalidadDepartamento where idDepartamento = :idDepartamento) "
						+ "order by l.NOMBRE")
				.addEntity(Localidad.class).setParameter("idDepartamento", idDepartamento).list();
		List<Localidad> existentes = bp
				.createSQLQuery("select distinct * from LOCALIDAD l where l.IDLOCALIDAD not in "
						+ "(select idLocalidad from LocalidadDepartamento where idDepartamento <> :idDepartamento) "
						+ "AND l.CODI_08 = :idProvincia " + "AND l.IDLOCALIDAD not in "
						+ "(select idLocalidad from LocalidadDepartamento where idDepartamento = :idDepartamento) "
						+ "order by l.NOMBRE")
				.addEntity(Localidad.class).setParameter("idProvincia", departamento.getProvincia().getId())
				.setParameter("idDepartamento", idDepartamento).list();

		localidadesAsociadas = "";
		for (Localidad l : asociadas) {
			localidadesAsociadas += "-" + l.getId() + "-";
		}

		localidades = new ArrayList<Localidad>();
		localidades.addAll(asociadas);
		localidades.addAll(existentes);

		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardar() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();

		String[] seleccion = request.getParameterValues("seleccionLocalidades");
		if (seleccion == null) {
			return false;
		}

		List<String> seleccionList = new ArrayList<String>(Arrays.asList(seleccion));

		List<LocalidadDepartamento> existentes = bp
				.createQuery("select ld from LocalidadDepartamento ld where ld.id.departamento.id = :idDepartamento")
				.setParameter("idDepartamento", departamento.getId()).list();

		for (LocalidadDepartamento ex : existentes) {
			// saco de la seleccion las existentes
			// si no existia, hay que eliminarla
			if (!seleccionList.remove(ex.getId().getLocalidad().getId().toString())) {
				bp.delete(ex);
			}
		}

		for (String idLoc : seleccionList) {
			Localidad loc = (Localidad) bp.getById(Localidad.class, new Long(idLoc));
			if (loc != null) {
				LocalidadDepartamentoPK pk = new LocalidadDepartamentoPK(loc, departamento);
				LocalidadDepartamento locDep = new LocalidadDepartamento();
				locDep.setId(pk);
				bp.save(locDep);
			}
		}

		forward = "ProcessRedirect";
		forwardURL = "/actions/abmAction.do?do=list&entityName=Departamento";

		return true;
	}

	public List<Localidad> getLocalidades() {
		return localidades;
	}

	public String getLocalidadesAsociadas() {
		return localidadesAsociadas;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public Long getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	@Override
	protected String getDefaultForward() {
		return "LocalidadDepartamento";
	}

	public String getForwardURL() {
		return forwardURL;
	}
}
