package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;

public class ProcesamientoRecaudacionProcess implements IProcess{

    private List lResult = new ArrayList();
    private BusinessPersistance bp = null;
    private Date fenv12; //Filtro Fecha

    public ProcesamientoRecaudacionProcess() {
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
    }

    public boolean doProcess() {
        if (fenv12 != null) {
            List lcaratula = bp.createQuery("FROM Caratula c WHERE c.fechaEnvio= :FENV12").setDate("FENV12", fenv12).list();
            if (lcaratula.size() > 0) {
                lResult = lcaratula;
            }
        }
        return true;
    }

    public HashMap getErrors() {
        return null;
    }

    public String getForward() {
        return "ProcesamientoRecaudacion";
    }

    public String getInput() {
        return "ProcesamientoRecaudacion";
    }

    public Object getResult() {
        return this.lResult;
    }

    public boolean validate() {
        return true;
    }

    public String getFenv12Str() {
        return DateHelper.getString(fenv12);
    }

    public void setFenv12Str(String fenv12) {
        if (fenv12 != null && fenv12.length() > 0) {
            this.fenv12 = DateHelper.getDate(fenv12);
        } else {
            this.fenv12 = DateHelper.getDate("");
        }
    }

    public Date getFenv12() {
        return fenv12;
    }

    public void setFenv12(Date fenv12) {
        this.fenv12 = fenv12;
    }
}
