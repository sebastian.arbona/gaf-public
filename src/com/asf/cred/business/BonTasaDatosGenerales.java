package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.validation.Errors;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaEstado;
import com.nirven.creditos.hibernate.TipoResolucionBonTasa;

public class BonTasaDatosGenerales extends NuevaBonTasa{
	
	private boolean mostrarPestania;
	private boolean pasarInstancia;
	private boolean haPasado;
	private Long idBonTasa;
	private BonTasaEstado estadoActual;
	
	private String expediente;
	private Date fechaSolicitud;
	private Long idUnidad;
	private String fechaResolucion;
	private String resolucion;
	private String fechaMutuo;
	private Long volumenVtaAnual;
	private Integer periodoVolVtaAnual;
	private String inicioActividad;
	private Integer personalOcupado;
	private Double facturacionMercExterno;
	private Double hasBeneficiadas;
	private Double qqCosechados;
	private String certifNormas;
	private String observaciones;
	private String condicionamientos;
	private String objeto;
	private String fechaResolucion2;
	private String fechaMutuo2;
	
	
	private String actividadPrincipalSolicitante; //tipificador 
	private String numeroInscripcionIIBB;
	private String sectorEconomico; //Tipificador
	private Integer cantidadAsociados; //en casos de cooperativa
	private String manoObraPermanente;
	private String manoObraTemporaria;
	private Integer sucursalEntidadCredito;
	private String numeroDecretoProvincial;
	private String numeroActaAPF;
	private Date fechaAutorizacionEspecial;
	
	
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	
    @Override
    public boolean doProcess() {
        if (getAccion() == null || getAccion().equals("load")) {
        	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
    		.getBusinessPersistance();
            buscarBonTasa(bp);
            cargarPropiedades();
        	load();
        	setForward("BonTasaGenerales");
        } else if (getAccion().equals("save")) {
        	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler()
    		.getBusinessPersistance();
        	buscarBonTasa(bp);        	
        	cargarBonificacion();
        	if(!validarDatos(bp)){
        		setForward("BonTasaGenerales");
        		return false;
        	}
        	load();        	
        	guardarDatosGenerales(bp);
        	buscarBonTasa(bp);
        	cargarPropiedades();
        	setForward("BonTasaGenerales");
        }
        return false;
    }
    @SuppressWarnings("unchecked")
	private boolean validarDatos(BusinessPersistance bp){
    	 List<BonTasaEstado> estados= bp.createQuery("Select eb from BonTasaEstado eb "
    	 		+ " where eb.bonTasa=:bon and eb.tipoResolucion = :tipoResolucion order by eb.fechaCambio desc")
				.setParameter("bon", getBonificacion())
				.setParameter("tipoResolucion", TipoResolucionBonTasa.APROBATORIA).list();
    	BonTasaEstado estadoAprobatoria = null;
    	if(estados != null && !estados.isEmpty()){
    		estadoAprobatoria = estados.get(0);
    	}
    	if(getBonificacion().getFechaMutuo() != null 
    			&& (estadoAprobatoria == null || getBonificacion().getFechaMutuo().before(estadoAprobatoria.getFechaCambio()))){
    		errors.put("bontasadatosgenerales.error.fechainstrumentacion.anteriorfechaaprobatoria"
    				 , "bontasadatosgenerales.error.fechainstrumentacion.anteriorfechaaprobatoria");
    		return false;
    	}
    	return true;
    }

    private void guardarDatosGenerales(BusinessPersistance bp){
    	bp.saveOrUpdate(getBonificacion());
    }
    private void load(){
    	if(getEstadoActual() != null && getEstadoActual().getEstado().getNombreEstado().equals("GESTION JUDICIAL")) {
    		mostrarPestania = true;
    	} else {
    		mostrarPestania = false;
    	}
    }
    
    private void buscarBonTasa(BusinessPersistance bp){
    	setBonificacion((BonTasa) bp.getById(BonTasa.class, idBonTasa));
    }
    
    private void cargarPropiedades(){
    	setExpediente(getBonificacion().getExpediente());
    	setIdUnidad(getBonificacion().getUnidad().getId()); 
    	setFechaSolicitud(getBonificacion().getFechaSolicitud());
    	setFechaResolucion(getBonificacion().getFechaResolucionStr());
    	setResolucion(getBonificacion().getResolucion());
    	setFechaMutuo(getBonificacion().getFechaMutuoStr());
    	if(getBonificacion().getFechaResolucion()!=null){
    		fechaResolucion2=fechaResolucion;
    		fechaMutuo2= fechaMutuo;
    	}
    	setVolumenVtaAnual(getBonificacion().getVolumenVtaAnual());
    	setCondicionamientos(getBonificacion().getCondicionamientos());
    	setCertifNormas(getBonificacion().getCertifNormas());
    	setFacturacionMercExterno(getBonificacion().getFacturacionMercExterno());
    	setPeriodoVolVtaAnual(getBonificacion().getPeriodoVolVtaAnual());
    	setPersonalOcupado(getBonificacion().getPersonalOcupado());
    	setInicioActividad(getBonificacion().getInicioActividadStr());
    	setHasBeneficiadas(getBonificacion().getHasBeneficiadas());
    	setQqCosechados(getBonificacion().getQqCosechados());
    	setObservaciones(getBonificacion().getObservaciones());
    	setObjeto(getBonificacion().getObjeto());
    	setTipoEmpresa(getBonificacion().getTipoEmpresa());
    	
    	
    	setActividadPrincipalSolicitante(getBonificacion().getActividadPrincipalSolicitante());    	
    	setNumeroInscripcionIIBB(getBonificacion().getNumeroInscripcionIIBB());
    	setSectorEconomico(getBonificacion().getSectorEconomico());
    	setCantidadAsociados(getBonificacion().getCantidadAsociados());
    	setManoObraPermanente(getBonificacion().getManoObraPermanente());
    	setManoObraTemporaria(getBonificacion().getManoObraTemporaria());
    	setSucursalEntidadCredito(getBonificacion().getSucursalEntidadCredito());
    	setNumeroDecretoProvincial(getBonificacion().getNumeroDecretoProvincial());
    	setNumeroActaAPF(getBonificacion().getNumeroActaAPF());
    	setFechaAutorizacionEspecial(DateHelper.getString(getBonificacion().getFechaAutorizacionEspecial()));
    	
    }
    private void cargarBonificacion(){
    	getBonificacion().setExpediente(expediente);
    	if(fechaResolucion==null){
    		getBonificacion().setFechaResolucionStr(fechaResolucion2);    		
    	}else{
    	getBonificacion().setFechaResolucionStr(fechaResolucion);
    	}
    	if(fechaMutuo==null){
    		getBonificacion().setFechaMutuoStr(fechaMutuo2);
    	}else{
    		getBonificacion().setFechaMutuoStr(fechaMutuo);
    	}
       	getBonificacion().setResolucion(resolucion);
    	getBonificacion().setVolumenVtaAnual(volumenVtaAnual);
    	getBonificacion().setCondicionamientos(condicionamientos);
    	getBonificacion().setCertifNormas(certifNormas);
    	getBonificacion().setFacturacionMercExterno(facturacionMercExterno);
    	getBonificacion().setPeriodoVolVtaAnual(periodoVolVtaAnual);
    	getBonificacion().setPersonalOcupado(personalOcupado);    	
    	getBonificacion().setHasBeneficiadas(hasBeneficiadas);
    	getBonificacion().setQqCosechados(qqCosechados);
    	getBonificacion().setObservaciones(observaciones);
    	getBonificacion().setTipoEmpresa(getTipoEmpresa());
    	
    	getBonificacion().setActividadPrincipalSolicitante(actividadPrincipalSolicitante);
    	getBonificacion().setNumeroInscripcionIIBB(numeroInscripcionIIBB);
    	getBonificacion().setSectorEconomico(sectorEconomico);
    	getBonificacion().setCantidadAsociados(cantidadAsociados);
    	getBonificacion().setManoObraPermanente(manoObraPermanente);
    	getBonificacion().setManoObraTemporaria(manoObraTemporaria);    	
    	getBonificacion().setSucursalEntidadCredito(sucursalEntidadCredito);
    	getBonificacion().setNumeroDecretoProvincial(numeroDecretoProvincial);
    	getBonificacion().setNumeroActaAPF(numeroActaAPF);
    	getBonificacion().setFechaAutorizacionEspecial(fechaAutorizacionEspecial);    
    	
    }
	public boolean isMostrarPestania() {
		return mostrarPestania;
	}

	public void setMostrarPestania(boolean mostrarPestania) {
		this.mostrarPestania = mostrarPestania;
	}
	public void setPasarInstancia(boolean pasarInstancia) {
		this.pasarInstancia = pasarInstancia;
	}
	public boolean isPasarInstancia() {
		return pasarInstancia;
	}
	public boolean isHaPasado() {
		return haPasado;
	}
	public void setHaPasado(boolean haPasado) {
		this.haPasado = haPasado;
	}
	public BonTasaEstado getEstadoActual() {
		if (estadoActual==null){
			estadoActual=getBonificacion().getEstadoActual();
		}
		return estadoActual;
	}


	public void setEstadoActual(BonTasaEstado estadoActual) {
		this.estadoActual = estadoActual;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public String getFechaSolicitud() {
		return formato.format(fechaSolicitud);
	}
	
	public String getFechaSolicitudStr() {
		return formato.format(fechaSolicitud);
	}
	
	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}
	public void setFechaSolicitudStr(String fechaSolicitud) {
		this.fechaSolicitud = DateHelper.getDate(fechaSolicitud);
	}
	

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public String getFechaResolucion() {
		return formato.format(DateHelper.getDate(fechaResolucion));
	}

	public void setFechaResolucion(String fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}
	public String getFechaMutuo() {
		return formato.format(DateHelper.getDate(fechaMutuo));
	}

	public void setFechaMutuo(String fechaMutuo) {
		this.fechaMutuo = fechaMutuo;
	}

	public String getInicioActividad() {
		return formato.format(DateHelper.getDate(inicioActividad));
	}

	public void setInicioActividad(String inicioActividad) {
		this.inicioActividad = inicioActividad;
	}

	public Integer getPersonalOcupado() {
		return personalOcupado;
	}

	public void setPersonalOcupado(Integer personalOcupado) {
		this.personalOcupado = personalOcupado;
	}

	public Double getFacturacionMercExterno() {
		return facturacionMercExterno;
	}

	public void setFacturacionMercExterno(Double facturacionMercExterno) {
		this.facturacionMercExterno = facturacionMercExterno;
	}

	public Double getHasBeneficiadas() {
		return hasBeneficiadas;
	}

	public void setHasBeneficiadas(Double hasBeneficiadas) {
		this.hasBeneficiadas = hasBeneficiadas;
	}

	public Double getQqCosechados() {
		return qqCosechados;
	}

	public void setQqCosechados(Double qqCosechados) {
		this.qqCosechados = qqCosechados;
	}

	public String getCertifNormas() {
		return certifNormas;
	}

	public void setCertifNormas(String certifNormas) {
		this.certifNormas = certifNormas;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getCondicionamientos() {
		return condicionamientos;
	}

	public void setCondicionamientos(String condicionamientos) {
		this.condicionamientos = condicionamientos;
	}

	public Long getVolumenVtaAnual() {
		return volumenVtaAnual;
	}

	public void setVolumenVtaAnual(Long volumenVtaAnual) {
		this.volumenVtaAnual = volumenVtaAnual;
	}

	public Integer getPeriodoVolVtaAnual() {
		return periodoVolVtaAnual;
	}

	public void setPeriodoVolVtaAnual(Integer periodoVolVtaAnual) {
		this.periodoVolVtaAnual = periodoVolVtaAnual;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public String getFechaResolucion2() {
		return fechaResolucion2;
	}

	public void setFechaResolucion2(String fechaResolucion2) {
		this.fechaResolucion2 = fechaResolucion2;
	}

	public String getFechaMutuo2() {
		return fechaMutuo2;
	}

	public void setFechaMutuo2(String fechaMutuo2) {
		this.fechaMutuo2 = fechaMutuo2;
	}
	
	public String getFechaAutorizacionEspecial() {
		return DateHelper.getString(fechaAutorizacionEspecial);
	}
	
	public void setFechaAutorizacionEspecial(String f) {
		this.fechaAutorizacionEspecial = DateHelper.getDate(f);
	}

	public String getActividadPrincipalSolicitante() {
		return actividadPrincipalSolicitante;
	}

	public void setActividadPrincipalSolicitante(String actividadPrincipalSolicitante) {
		this.actividadPrincipalSolicitante = actividadPrincipalSolicitante;
	}

	public String getNumeroInscripcionIIBB() {
		return numeroInscripcionIIBB;
	}

	public void setNumeroInscripcionIIBB(String numeroInscripcionIIBB) {
		this.numeroInscripcionIIBB = numeroInscripcionIIBB;
	}

	public String getSectorEconomico() {
		return sectorEconomico;
	}

	public void setSectorEconomico(String sectorEconomico) {
		this.sectorEconomico = sectorEconomico;
	}

	public Integer getCantidadAsociados() {
		return cantidadAsociados;
	}

	public void setCantidadAsociados(Integer cantidadAsociados) {
		this.cantidadAsociados = cantidadAsociados;
	}

	public String getManoObraPermanente() {
		return manoObraPermanente;
	}

	public void setManoObraPermanente(String manoObraPermanente) {
		this.manoObraPermanente = manoObraPermanente;
	}

	public String getManoObraTemporaria() {
		return manoObraTemporaria;
	}

	public void setManoObraTemporaria(String manoObraTemporaria) {
		this.manoObraTemporaria = manoObraTemporaria;
	}

	public Integer getSucursalEntidadCredito() {
		return sucursalEntidadCredito;
	}

	public void setSucursalEntidadCredito(Integer sucursalEntidadCredito) {
		this.sucursalEntidadCredito = sucursalEntidadCredito;
	}

	public String getNumeroDecretoProvincial() {
		return numeroDecretoProvincial;
	}

	public void setNumeroDecretoProvincial(String numeroDecretoProvincial) {
		this.numeroDecretoProvincial = numeroDecretoProvincial;
	}

	public String getNumeroActaAPF() {
		return numeroActaAPF;
	}

	public void setNumeroActaAPF(String numeroActaAPF) {
		this.numeroActaAPF = numeroActaAPF;
	}

	
}

