package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;

public class InformeAlertaBonificaciones extends ConversationProcess {

	private Date fecha = new Date();

	private Long idLinea;
	private Long idConvenio;
	private Integer dias;

	private List<InformeBonificacionesBean> beans;

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean generar() {
		String sql = "select o.id, o.numeroAtencion, o.expediente, p.nomb_12, p.cuil_12, "
				+ "l.nombre as linea, tcomp.tf_descripcion as comportamiento, e.nombreEstado, "
				+ "cb.nombre as convenio, cast(b.tasaBonificada as varchar) + '%' as tasaBonificacion, "
				+ "tbon.tf_descripcion as tipoBonificacion, "
				+ "datediff(day, c.fechaVencimiento, :fecha) as diasMora, "
				+ "cb.diasMora - datediff(day, c.fechaVencimiento, :fecha) as diasBaja, "
				+ "n.fechaCreada as fechaNotificacion, n.observaciones, tnot.tf_descripcion as tipoAviso "
				+ "from Objetoi o join Persona p on o.persona_IDPERSONA = p.IDPERSONA "
				+ "join Linea l on l.id = o.linea_id " + "join ObjetoiBonificacion ob on o.id = ob.idCredito "
				+ "join Bonificacion b on b.id = ob.idBonificacion "
				+ "join ConvenioBonificacion cb on cb.id = b.convenio_id "
				+ "join ObjetoiComportamiento oc on oc.objetoi_id = o.id and oc.fechaHasta is null "
				+ "join Tipificadores tcomp on oc.comportamientoPago = tcomp.tf_codigo and tcomp.tf_categoria = 'comportamientoPago' "
				+ "join Tipificadores tbon on b.tipoDeBonificacion = tbon.tf_codigo and tbon.tf_categoria = 'bonificacion.tipo' "
				+ "join ObjetoiEstado oe on oe.objetoi_id = o.id "
				+ "join Estado e on e.idEstado = oe.estado_idEstado and oe.fechaHasta is null "
				+ "left join Cuota c on c.credito_id = o.id and c.numero = (select min(numero) from Cuota where credito_id = o.id and estado = '1') "
				+ "left join Notificacion n on n.credito_id = o.id and n.fechaCreada = (select max(fechaCreada) from Notificacion where credito_id = o.id) "
				+ "left join Tipificadores tnot on n.tipoAviso = tnot.tf_codigo and tnot.tf_categoria = 'notificacion.TipoAviso' "
				+ "where o.fechaBajaBonificacion is null and c.id is not null and c.fechaVencimiento <= :fecha "
				+ "and e.nombreEstado in ('EJECUCION', 'PRIMER DESEMBOLSO', 'PENDIENTE SEGUNDO DESEMBOLSO') ";

		if (idLinea != null && idLinea != 0L) {
			sql += "and l.id = :idLinea ";
		}
		if (idConvenio != null && idConvenio != 0L) {
			sql += "and cb.id = :idConvenio ";
		}
		if (dias != null && dias != 0) {
			sql += "and cb.diasMora - datediff(day, c.fechaVencimiento, :fecha) < :dias ";
		}

		sql += "order by cb.diasMora - datediff(day, c.fechaVencimiento, :fecha)";

		Query query = bp.createSQLQuery(sql).setParameter("fecha", fecha);

		if (idLinea != null && idLinea != 0L) {
			query.setParameter("idLinea", idLinea);
		}
		if (idConvenio != null && idConvenio != 0L) {
			query.setParameter("idConvenio", idConvenio);
		}
		if (dias != null && dias != 0) {
			query.setParameter("dias", dias);
		}

		List<Object[]> rows = query.list();

		beans = new ArrayList<InformeBonificacionesBean>(rows.size());

		for (Object[] row : rows) {
			InformeBonificacionesBean bean = new InformeBonificacionesBean(row);

//			Long idCredito = bean.getIdObjetoi();
//			
//			CreditoCalculoDeuda c = new CreditoCalculoDeuda(idCredito, fecha);
//			c.calcular();
//			double deuda = c.getDeudaTotal();
//			bean.setDeuda(deuda);

			beans.add(bean);
		}
		return false;
	}

	@Override
	protected String getDefaultForward() {
		return "InformeAlertaBonificaciones";
	}

	public String getFecha() {
		return DateHelper.getString(fecha);
	}

	public void setFecha(String fecha) {
		this.fecha = DateHelper.getDate(fecha);
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public Long getIdConvenio() {
		return idConvenio;
	}

	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}

	public List<InformeBonificacionesBean> getBeans() {
		return beans;
	}

	public void setBeans(List<InformeBonificacionesBean> beans) {
		this.beans = beans;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

}
