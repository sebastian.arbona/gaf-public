package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.cred.security.SessionHandler;
import com.asf.gaf.business.presupuesto.imputaciones.sueldos.Convenio;
import com.asf.gaf.hibernate.Bancos;
import com.asf.hibernate.mapping.Director;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.BonTasa;
import com.nirven.creditos.hibernate.BonTasaEstado;
import com.nirven.creditos.hibernate.CargaRequisitoBon;
import com.nirven.creditos.hibernate.ConvenioBonificacionExterna;
import com.nirven.creditos.hibernate.EstadoBonifExterna;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.Requisito;
import com.nirven.creditos.hibernate.TitularesBonTasa;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Unidad;

public class NuevaBonTasaGeneral implements IProcess {

	private String forward;
	private String accion;
	private Object result;
	private Long idPersona;
	private BonTasa bonificacion;
	private Date fechaHoy = new Date();
	private boolean creada = false;
	private Long idBonTasa;

	private Long volumenVtaAnual;
	private Integer periodoVolVtaAnual;
	private String inicioActividad;
	private String idTipoEmpresa;
	private Integer personalOcupado;
	private Double facturacionMercExterno;
	private Double hasBeneficiadas;
	private Double qqCosechados;
	private String objeto;
	private String certifNormas;
	private String condicionamientos;
	private String observaciones;
	private Long idConvenio;
	private Long idEnte;
	private ConvenioBonificacionExterna convenio;
	private String operatoria;
	private Double montoFinanciamiento;
	private Double montoNeto;
	private Double montoIva;
	private Double montoBonif;
	private String expediente;

	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	private String detaBa;
	private String linea;
	private String tipoEmpresa;
	private HashMap<String, String> errors = new HashMap<String, String>();
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	@Override
	public boolean doProcess() {
		if (accion.equalsIgnoreCase("nueva")) {
			generarObjetos();
			forward = "NuevaBonTasaGeneral";
		} else if (accion.equalsIgnoreCase("guardar")) {
			if (validarBonTasa()) {
				guardarBonTasa();
				crearRequisitos();
				creada = true;
			}
			forward = "NuevaBonTasaGeneral";
		} else if (accion.equalsIgnoreCase("loadConvenio")) {
			loadConvenio();
			forward = "NuevaBonTasaGeneral";
		}
		return errors.isEmpty();
	}

	private void loadConvenio() {
		if (idConvenio == null)
			return;

		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		// convenio = (ConvenioBonificacionExterna) bp.createQuery("select c from
		// ConvenioBonificacionExterna c where c.id=:id").setLong("id",
		// idConvenio).uniqueResult();
		detaBa = convenio.getBanco().getDetaBa();
		operatoria = convenio.getOperatoriaStr();
		linea = convenio.getLinea().getNombre();
		objeto = convenio.getDestino();
		tipoEmpresa = convenio.getTipoEmpresaStr();
	}

	public void generarObjetos() {
		this.bonificacion = new BonTasa();
		this.fechaHoy = new Date();
	}

	private void crearRequisitos() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		// Director director = (Director) bp.createQuery("Select d from Director d where
		// d.codigo=:cod").setParameter("cod", "BonTasa.IDLinea").uniqueResult();
		// Linea l= (Linea) bp.getById(Linea.class,
		// Long.parseLong(director.getValor()));
		List<Requisito> requisitos = (List<Requisito>) bp.createQuery("Select r from Requisito r where r.linea= :linea")
				.setLong("linea", convenio.getLinea().getId()).list();
		for (Requisito requisito : requisitos) {
			CargaRequisitoBon req = new CargaRequisitoBon();
			req.setBonTasa(bonificacion);
			req.setRequisito(requisito);
			bp.save(req);
		}
	}

	private boolean validarBonTasa() {
		if (montoBonif > montoNeto) {
			errors.put("bonTasa.montoMayor", "bonTasa.montoMayor");
		}

		return errors.isEmpty();
	}

	public void guardarBonTasa() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		Persona persona = (Persona) bp.getById(Persona.class, idPersona);
		if (bonificacion == null) {
			bonificacion = new BonTasa();
		}
		bonificacion.setPersona(persona);
//		bonificacion.setInicioActividadStr(inicioActividad);

//		bonificacion.setPersonalOcupado(personalOcupado);
//		bonificacion.setFacturacionMercExterno(facturacionMercExterno);
//		bonificacion.setHasBeneficiadas(hasBeneficiadas);
//		bonificacion.setQqCosechados(qqCosechados);
		bonificacion.setObjeto(objeto);
		bonificacion.setFechaSolicitud(new Date());
		bonificacion.setConvenio(convenio);
//		bonificacion.setCertifNormas(certifNormas);
//		bonificacion.setObservaciones(observaciones);
//		bonificacion.setCondicionamientos(condicionamientos);
//		bonificacion.setPeriodoVolVtaAnual(periodoVolVtaAnual);
//		bonificacion.setVolumenVtaAnual(volumenVtaAnual);
		bonificacion.setNumeroBonificacion(asignarNumero(bp));
		bonificacion.setAsesor(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		bonificacion.setUnidad(buscarUnidad(bp));
		// Guarda el string que corresponde a la descripcion del tipificador que se le
		// va a asignar al BonTasaEstado
		bonificacion.setTipoEstadoBonTasa(EstadoBonifExterna.PENDIENTE_APROBACION.getNombre());

		// ConvenioBonificacionExterna convenio = (ConvenioBonificacionExterna)
		// bp.getById(ConvenioBonificacionExterna.class, idConvenio);

		// Bancos banco = convenio.getBanco();
		operatoria = convenio.getOperatoriaStr();
		idTipoEmpresa = convenio.getTipoEmpresa();

		bonificacion.setTipoEmpresa(idTipoEmpresa);
		bonificacion.setBanco(convenio.getBanco());
		bonificacion.setMontoFinanciamiento(montoFinanciamiento);
		bonificacion.setMontoBonificacion(montoBonif);
		bonificacion.setMontoIva(montoIva);
		bonificacion.setMontoNeto(montoNeto);
		bonificacion.setExpediente(expediente);
		bonificacion.setOperatoria(operatoria);

		bp.save(bonificacion);
		asignarEstado(bp, bonificacion);
		aumentarNum();
		guardarTitular(persona, bp);
		idBonTasa = bonificacion.getId();
	}

	private void guardarTitular(Persona persona, BusinessPersistance bp) {
		TitularesBonTasa titular = new TitularesBonTasa();
		titular.setPersona(persona);
		titular.setBonTasa(bonificacion);
		bp.save(titular);
	}

	private Unidad buscarUnidad(BusinessPersistance bp) {
		Usuario u = (Usuario) bp.createQuery("Select u from Usuario u where u.id=:asesor")
				.setParameter("asesor", bonificacion.getAsesor()).uniqueResult();
		return u.getDelegacion();
	}
	/*
	 * private void buscarTipoEmpresa(BusinessPersistance bp){
	 * bonificacion.setTipoEmpresa((Tipificadores) bp.getById(Tipificadores.class,
	 * Long.parseLong(idTipoEmpresa))); }
	 */

	private void asignarEstado(BusinessPersistance bp, BonTasa bonTasa) {
		BonTasaEstado estado = new BonTasaEstado();
		List<Estado> estados = bp
				.createQuery("select e from Estado e where e.nombreEstado = :estado and e.tipo = 'BonTasa'")
				.setParameter("estado", bonTasa.getTipoEstadoBonTasa()).setMaxResults(1).list();
		if (!estados.isEmpty()) {
			estado.setEstado(estados.get(0));
		}
		Date fecha = new Date();
		estado.setFechaCambio(fecha);
		estado.setAsesor(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		estado.setBonTasa(bonTasa);
		bp.save(estado);
	}

	public Long asignarNumero(BusinessPersistance b) {
		long numero = (Long) b.createQuery("Select n.numero from Numerador n where n.nombre=:nombre")
				.setParameter("nombre", "bonTasa").uniqueResult();
		return numero + 1;
	}

	private void aumentarNum() {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		com.nirven.expedientes.persistencia.Numerador num = (com.nirven.expedientes.persistencia.Numerador) bp
				.createQuery("Select n from Numerador n where n.nombre=:nombre").setParameter("nombre", "bonTasa")
				.uniqueResult();
		num.setNumero(num.getNumero() + 1);
		bp.update(num);
	}

	@Override
	public HashMap<String, String> getErrors() {
		return errors;
	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	public String getInput() {
		return forward;
	}

	@Override
	public Object getResult() {
		return result;
	}

	@Override
	public boolean validate() {

		if (idPersona != null) {
			if (idPersona == 0) {
				errors.put("BonTasaGeneral.Persona", "BonTasaGeneral.Persona");
			}
		}
		if (idConvenio != null) {
			if (idConvenio == 0) {
				errors.put("BonTasaGeneral.Convenio", "BonTasaGeneral.Convenio");
			}
		}
		forward = "NuevaBonTasaGeneral";
		return errors.isEmpty();
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public BonTasa getBonificacion() {
		return bonificacion;
	}

	public void setBonificacion(BonTasa bonificacion) {
		this.bonificacion = bonificacion;
	}

	public Date getFechaHoy() {
		return fechaHoy;
	}

	public void setFechaHoy(Date fechaHoy) {
		this.fechaHoy = fechaHoy;
	}

	public String getFechaHoyStr() {
		return DateHelper.getString(fechaHoy);
	}

	public void setFechaHoyStr(String fechaHoy) {
		this.fechaHoy = DateHelper.getDate(fechaHoy);
	}

	public boolean isCreada() {
		return creada;
	}

	public void setCreada(boolean creada) {
		this.creada = creada;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Long getVolumenVtaAnual() {
		return volumenVtaAnual;
	}

	public void setVolumenVtaAnual(Long volumenVtaAnual) {
		this.volumenVtaAnual = volumenVtaAnual;
	}

	public Integer getPeriodoVolVtaAnual() {
		return periodoVolVtaAnual;
	}

	public void setPeriodoVolVtaAnual(Integer periodoVolVtaAnual) {
		this.periodoVolVtaAnual = periodoVolVtaAnual;
	}

	public String getInicioActividad() {
		return inicioActividad;
	}

	public void setInicioActividad(String inicioActividad) {
		this.inicioActividad = inicioActividad;
	}

	public String getIdTipoEmpresa() {
		return idTipoEmpresa;
	}

	public void setIdTipoEmpresa(String idTipoEmpresa) {
		this.idTipoEmpresa = idTipoEmpresa;
	}

	public Integer getPersonalOcupado() {
		return personalOcupado;
	}

	public void setPersonalOcupado(Integer personalOcupado) {
		this.personalOcupado = personalOcupado;
	}

	public Double getFacturacionMercExterno() {
		return facturacionMercExterno;
	}

	public void setFacturacionMercExterno(Double facturacionMercExterno) {
		this.facturacionMercExterno = facturacionMercExterno;
	}

	public Double getHasBeneficiadas() {
		return hasBeneficiadas;
	}

	public void setHasBeneficiadas(Double hasBeneficiadas) {
		this.hasBeneficiadas = hasBeneficiadas;
	}

	public Double getQqCosechados() {
		return qqCosechados;
	}

	public void setQqCosechados(Double qqCosechados) {
		this.qqCosechados = qqCosechados;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public String getCertifNormas() {
		return certifNormas;
	}

	public void setCertifNormas(String certifNormas) {
		this.certifNormas = certifNormas;
	}

	public String getCondicionamientos() {
		return condicionamientos;
	}

	public void setCondicionamientos(String condicionamientos) {
		this.condicionamientos = condicionamientos;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Long getIdBonTasa() {
		return idBonTasa;
	}

	public void setIdBonTasa(Long idBonTasa) {
		this.idBonTasa = idBonTasa;
	}

	public Long getIdEnte() {
		return idEnte;
	}

	public void setIdEnte(Long idEnte) {
		this.idEnte = idEnte;
	}

	public Long getIdConvenio() {
		return idConvenio;
	}

	public void setIdConvenio(Long idConvenio) {
		this.idConvenio = idConvenio;
	}

	public void setMontoFinanciamiento(Double montoFinanciamiento) {
		this.montoFinanciamiento = montoFinanciamiento;
	}

	public Double getMontoFinanciamiento() {
		return montoFinanciamiento;
	}

	public String getOperatoria() {
		return operatoria;
	}

	public void setOperatoria(String operatoria) {
		this.operatoria = operatoria;
	}

	public String getDetaBa() {
		return detaBa;
	}

	public String getLinea() {
		return linea;
	}

	public String getTipoEmpresa() {
		return tipoEmpresa;
	}

	public Double getMontoNeto() {
		return montoNeto;
	}

	public void setMontoNeto(Double montoNeto) {
		this.montoNeto = montoNeto;
	}

	public Double getMontoIva() {
		return montoIva;
	}

	public void setMontoIva(Double montoIva) {
		this.montoIva = montoIva;
	}

	public Double getMontoBonif() {
		return montoBonif;
	}

	public void setMontoBonif(Double montoBonif) {
		this.montoBonif = montoBonif;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public void setConvenio_id(String id) {
		if (!id.equals(""))
			convenio = (ConvenioBonificacionExterna) bp
					.createQuery("select c from ConvenioBonificacionExterna c where c.id=:id").setString("id", id)
					.uniqueResult();
		this.setIdConvenio(convenio.getId());
		// loadConvenio();
	}

	public String getConvenio_id() {
		if (convenio == null)
			return "";
		return convenio.getId().toString();
	}

}
