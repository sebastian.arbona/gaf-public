/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.asf.cred.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.asf.cred.security.SessionHandler;
import com.asf.security.BusinessPersistance;
import com.asf.struts.action.IProcess;
import com.civitas.hibernate.persona.Persona;
import com.nirven.creditos.hibernate.Objetoi;
import com.civitas.hibernate.persona.Domicilio;

/**
 * @author cnoguerol
 */
public class DomicilioObjetoi implements IProcess {
    // =========================ATRIBUTOS==================================
    private String forward = "domicilioObjetoiList";
    private HashMap<String, Object> errors;
    private String accion = "";
    private BusinessPersistance bp;
    private Persona personaTitular;
    private Domicilio domicilio;
    private List<Domicilio> listaDomicilios;
    private List<com.nirven.creditos.hibernate.DomicilioObjetoi> listaDomiciliosObjetoi;
    private Long idpais;
    private Objetoi objetoi;
    private com.nirven.creditos.hibernate.DomicilioObjetoi domicilioObjetoi;
	// =========================CONSTRUCTORES================================
    public DomicilioObjetoi() {
        this.errors = new HashMap<String, Object>();
        this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
        this.domicilio = new Domicilio();
        this.personaTitular = new Persona();
        listaDomicilios = new ArrayList<Domicilio>();
        listaDomiciliosObjetoi = new ArrayList<com.nirven.creditos.hibernate.DomicilioObjetoi>();
        objetoi = new Objetoi();
    }

    // =========================FUNCIONALIDAD==================================
    public boolean doProcess() {
        if (this.getAccion().equalsIgnoreCase("asignar")) {
            this.forward = "domicilioObjetoi";
        } else if (this.getAccion().equalsIgnoreCase("guardar")) {
            this.validarDomicilio();
            if (this.errors.isEmpty()) {
                this.asignarDomicilio();
                if (this.errors.isEmpty()) {
                    this.forward = "domicilioObjetoiList";
                } else {
                    this.forward = "domicilioObjetoi";
                }
                this.listarDomicilios();
            } else {
                this.forward = "domicilioObjetoi";
            }
        } else if (this.getAccion().equalsIgnoreCase("cancelar")) {
        	domicilioObjetoi = null;
            this.forward = "domicilioObjetoiList";
            this.listarDomicilios();
        } else if (this.getAccion().equalsIgnoreCase("eliminar")) {
            this.eliminarDomicilio();
            this.listarDomicilios();
        } else if (this.getAccion().equalsIgnoreCase("load")) {
            this.cargarDomicilio();
            this.forward = "domicilioObjetoi";
        } else if (this.getAccion().equalsIgnoreCase("listar")) {
        	listarDomicilios();
        }
        
        return this.errors.isEmpty();
    }

    // =========================UTILIDADES PRIVADAS==============================
    private void validarDomicilio() {
        if (("").equalsIgnoreCase(this.getDomicilio().getProvinciaStr())) {
            this.errors.put("Domicilio.Provincia.vacia", "Domicilio.Provincia.vacia");
        }
        if (("").equalsIgnoreCase(this.getDomicilio().getLocalidadStr())) {
            this.errors.put("Domicilio.Localidad.vacia", "Domicilio.Localidad.vacia");
        }
        if (("").equalsIgnoreCase(this.getDomicilio().getCalleStr())) {
            this.errors.put("Domicilio.Calle.vacia", "Domicilio.Calle.vacia");
        }
        if (this.getDomicilio().getTipo() == null) {
            this.errors.put("Domicilio.Tipo.vacio", "Domicilio.Tipo.vacio");
        }
    }

    private void asignarDomicilio() {
        try {
            if(domicilio.getId() == 0) {
            	domicilioObjetoi = new com.nirven.creditos.hibernate.DomicilioObjetoi();
            	String consulta = "SELECT o FROM Objetoi o " +
									"WHERE o.id = " + objetoi.getId().longValue();
            	objetoi = (Objetoi )bp.getByFilter(consulta).get(0);            	
            	domicilioObjetoi.setObjetoi(objetoi);
            	domicilioObjetoi.setDomicilio(domicilio);
            	bp.save(domicilioObjetoi);
            } else {
            	bp.update(domicilio);
            }
            this.setDomicilio(new Domicilio());
            domicilioObjetoi = null;
        }
        catch (Exception e) {
            this.errors.put("Domicilio.Error.guardar", "Domicilio.Error.guardar");
            e.printStackTrace();
        }
    }

    private void eliminarDomicilio() {
        if (domicilio.getId() != null) {
            try {
            	String consulta = "SELECT d FROM DomicilioObjetoi d";
                consulta += " WHERE d.domicilio = " + domicilio.getId().longValue();
                domicilioObjetoi = (com.nirven.creditos.hibernate.DomicilioObjetoi) bp.getByFilter(consulta).get(0);
                bp.delete(domicilioObjetoi);
            }
            catch (Exception e) {
                this.errors.put("Domicilio.Error.eliminar", "Domicilio.Error.eliminar");
                e.printStackTrace();
            }
        }
    }

    private void cargarDomicilio() {
        if (getDomicilio() != null) {
            try {
                setDomicilio((Domicilio) bp.getById(Domicilio.class, getDomicilio().getId()));
            }
            catch (Exception e) {
                this.setDomicilio(new Domicilio());
                this.errors.put("Domicilio.Error.cargar", "Domicilio.Error.carga");
                e.printStackTrace();
            }
        }
    }

    private void listarDomicilios() {
    	if (objetoi != null && objetoi.getId() != null && objetoi.getId().longValue() > 0) {
            try {
            	String consulta2 = "SELECT o FROM Objetoi o " +
            						"WHERE o.id = " + objetoi.getId().longValue();
            	objetoi = (Objetoi )bp.getByFilter(consulta2).get(0);
                String consulta = "SELECT d FROM DomicilioObjetoi d";
                consulta += " WHERE d.objetoi = " + objetoi.getId().longValue();
                listaDomiciliosObjetoi = bp.getByFilter(consulta);
                listaDomicilios.clear();
                for(com.nirven.creditos.hibernate.DomicilioObjetoi domicilioObjetoi : listaDomiciliosObjetoi) {
                	if(domicilioObjetoi.getDomicilio() != null) {
                		listaDomicilios.add(domicilioObjetoi.getDomicilio());
                	}
                }
            }
            catch (Exception e) {
            	e.printStackTrace();
            }
        }	
    }

    // =========================GETTERS y SETTERS==================================
    public HashMap<String, Object> getErrors() {
        return this.errors;
    }

    public Object getResult() {
        return null;
    }

    public String getInput() {
        return this.forward;
    }

    public String getForward() {
        return this.forward;
    }

    public String getAccion() {
        return this.accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public Domicilio getDomicilio() {
        return this.domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public Persona getPersonaTitular() {
        return this.personaTitular;
    }

    public void setPersonaTitular(Persona personaTitular) {
        this.personaTitular = personaTitular;
    }

    public List<Domicilio> getListaDomicilios() {
        return this.listaDomicilios;
    }

    public void setListaDomicilios(List<Domicilio> listaDomicilios) {
        this.listaDomicilios = listaDomicilios;
    }

    public Long getIdpais() {
        return this.idpais;
    }

    public void setIdpais(Long idpais) {
        this.idpais = idpais;
    }

	public void setObjetoi(Objetoi objetoi) {
		this.objetoi = objetoi;
	}

	public Objetoi getObjetoi() {
		return objetoi;
	}

    public com.nirven.creditos.hibernate.DomicilioObjetoi getDomicilioObjetoi() {
		return domicilioObjetoi;
	}

	public void setDomicilioObjetoi(
			com.nirven.creditos.hibernate.DomicilioObjetoi domicilioObjetoi) {
		this.domicilioObjetoi = domicilioObjetoi;
	}

	// =========================VALIDACIONES==================================
    public boolean validate() {
        return this.errors.isEmpty();
    }
}
