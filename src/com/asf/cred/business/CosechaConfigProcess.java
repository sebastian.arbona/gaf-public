package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.MultiMap;
import org.apache.struts.action.ActionMessage;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.PeriodoCosechaConfig;

public class CosechaConfigProcess extends ConversationProcess {

	@Save
	private List<PeriodoCosechaConfig> periodos;
	private CosechaConfig cosechaConfig;
	@Save
	private CosechaConfig cosechaConfigSave;
	private PeriodoCosechaConfig periodoCosechaConfig;
	private String forwardURL;
	private Long idCosechaConfig;
	private int periodoIndex;

	public CosechaConfigProcess() {
		periodos = new ArrayList<PeriodoCosechaConfig>();
		cosechaConfig = new CosechaConfig();
		cosechaConfigSave = new CosechaConfig();
		periodoCosechaConfig = new PeriodoCosechaConfig();
	}

	@ProcessMethod
	public boolean cargar() {
		if (idCosechaConfig == null) {
			return false;
		}

		cosechaConfigSave = (CosechaConfig) bp.getById(CosechaConfig.class, idCosechaConfig);
		cosechaConfig = cosechaConfigSave;

		periodos = cosechaConfigSave.getPeriodos();

		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardar() {
		if (cosechaConfigSave == null) {
			cosechaConfigSave = new CosechaConfig();
		}
		actualizarCabecera();
		if (periodos == null) {
			periodos = new ArrayList<PeriodoCosechaConfig>();
		}

		actualizarCabecera();

		cosechaConfigSave.setPeriodos(periodos);

		if (validateCosechaConfig(cosechaConfigSave)) {
			if (cosechaConfigSave.getId() != null && cosechaConfigSave.getId() != 0) {
				// eliminar los que se borraron
				List<PeriodoCosechaConfig> periodosExistentes = bp
						.createQuery("select p from PeriodoCosechaConfig p where p.cosechaConfig.id = :idCosecha")
						.setParameter("idCosecha", cosechaConfigSave.getId()).list();

				for (PeriodoCosechaConfig periodoExistente : periodosExistentes) {
					if (!periodos.contains(periodoExistente)) {
						bp.delete(periodoExistente);
					} else {
						bp.getCurrentSession().evict(periodoExistente);
					}
				}

				CosechaConfig existente = (CosechaConfig) bp.getById(CosechaConfig.class, cosechaConfigSave.getId());
				bp.getCurrentSession().evict(existente);
			}

			bp.saveOrUpdate(cosechaConfigSave);

			for (PeriodoCosechaConfig periodo : periodos) {
				periodo.setCosechaConfig(cosechaConfigSave);
				bp.saveOrUpdate(periodo);
			}
		} else {
			periodoCosechaConfig = new PeriodoCosechaConfig();
			return false;
		}

		forwardURL = "/actions/abmAction.do?do=list&entityName=CosechaConfig";
		forward = "ProcessRedirect";
		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean modificarPeriodo() {
		periodoCosechaConfig = periodos.remove(periodoIndex - 1);
		return true;
	}

	@ProcessMethod
	public boolean eliminarPeriodo() {
		periodos.remove(periodoIndex - 1);
		periodoCosechaConfig = new PeriodoCosechaConfig();
		return true;
	}

	@ProcessMethod
	public boolean cancelar() {
		forwardURL = "/actions/abmAction.do?do=list&entityName=CosechaConfig";
		forward = "ProcessRedirect";
		return errors.isEmpty();
	}

	@ProcessMethod
	public boolean agregarPeriodo() {
		if (cosechaConfigSave == null) {
			cosechaConfigSave = new CosechaConfig();
		}
		actualizarCabecera();
		if (validatePeriodos(periodoCosechaConfig)) {
			if (periodos == null) {
				periodos = new ArrayList<PeriodoCosechaConfig>();
			}

			if (periodoCosechaConfig.getTipoGarantia_id() != null && periodoCosechaConfig.getTipoGarantia_id() == 0L) {
				periodoCosechaConfig.setTipoGarantia_id(null);
			}

			periodos.add(periodoCosechaConfig);
			periodoCosechaConfig = new PeriodoCosechaConfig();
		}
		return errors.isEmpty();
	}

	public void actualizarCabecera() {
		cosechaConfigSave.setAforo(cosechaConfig.getAforo());
		cosechaConfigSave.setCantCuotas(cosechaConfig.getCantCuotas());
		cosechaConfigSave.setCostoQQCosecha(cosechaConfig.getCostoQQCosecha());
		cosechaConfigSave.setCostoQQElab(cosechaConfig.getCostoQQElab());
		cosechaConfigSave.setFechaFinPer(cosechaConfig.getFechaFinPer());
		cosechaConfigSave.setFechaFinPerStr(cosechaConfig.getFechaFinPerStr());
		cosechaConfigSave.setFechaInicioPer(cosechaConfig.getFechaInicioPer());
		cosechaConfigSave.setFechaInicioPerStr(cosechaConfig.getFechaInicioPerStr());
		cosechaConfigSave.setFechaPrimerVto(cosechaConfig.getFechaPrimerVto());
		cosechaConfigSave.setFechaPrimerVtoStr(cosechaConfig.getFechaPrimerVtoStr());
		cosechaConfigSave.setPeriodicidad(cosechaConfig.getPeriodicidad());
		cosechaConfigSave.setPrecioMosto(cosechaConfig.getPrecioMosto());
		cosechaConfigSave.setPrecioVinoBlanco(cosechaConfig.getPrecioVinoBlanco());
		cosechaConfigSave.setPrecioVinoTinto(cosechaConfig.getPrecioVinoTinto());
		cosechaConfigSave.setPrecioMalbec(cosechaConfig.getPrecioMalbec());
		cosechaConfigSave.setPrecioOtrosVarietales(cosechaConfig.getPrecioOtrosVarietales());
		cosechaConfigSave.setPrimerDesembolso(cosechaConfig.getPrimerDesembolso());
		cosechaConfigSave.setPrimerDesembolsoStr(cosechaConfig.getPrimerDesembolsoStr());
		cosechaConfigSave.setSegundoDesembolso(cosechaConfig.getSegundoDesembolso());
		cosechaConfigSave.setSegundoDesembolsoStr(cosechaConfig.getSegundoDesembolsoStr());
		cosechaConfigSave.setTemporada(cosechaConfig.getTemporada());
		cosechaConfigSave.setMaxQQ(cosechaConfig.getMaxQQ());
		cosechaConfigSave.setVarietales(cosechaConfig.getVarietales());
		cosechaConfigSave.setPorcMosto(cosechaConfig.getPorcMosto());
		cosechaConfigSave.setPorcVinoBlanco(cosechaConfig.getPorcVinoBlanco());
		cosechaConfigSave.setPorcVinoTinto(cosechaConfig.getPorcVinoTinto());
		cosechaConfigSave.setPorcMalbec(cosechaConfig.getPorcMalbec());
		cosechaConfigSave.setPorcOtrosVarietales(cosechaConfig.getPorcOtrosVarietales());
		cosechaConfigSave.setPorcAuditoriaFinal(cosechaConfig.getPorcAuditoriaFinal());
		cosechaConfigSave.setSuperficiePermitida(cosechaConfig.getSuperficiePermitida());
	}

	@Override
	protected String getDefaultForward() {
		return "CosechaConfig";
	}

	@SuppressWarnings("unchecked")
	public boolean validateCosechaConfig(CosechaConfig cosecha) {
//		boolean tieneGarantia = true;

		List<Integer> temporadas = bp
				.createQuery(
						"Select cc.temporada from CosechaConfig cc where not cc.id=:id and cc.varietales = :varietales")
				.setParameter("id", cosecha.getId()).setParameter("varietales", cosecha.getVarietales()).list();
		for (Integer tt : temporadas) {
			if (tt.compareTo(cosecha.getTemporada()) == 0) {
				errors.put("CosechaConfig.temporada",
						new ActionMessage("CosechaConfig.temporada", cosecha.getTemporada().toString()));
				break;
			}
		}

		// separar por garantias (inclusive los sin garantia)
		MultiMap periodosPorGarantia = new MultiHashMap();
		for (PeriodoCosechaConfig periodo : cosecha.getPeriodos()) {
			if (periodo.getTipoGarantia_id() != null) {
				periodosPorGarantia.put(periodo.getTipoGarantia_id(), periodo);
			} else {
				periodosPorGarantia.put(0L, periodo); // sin garantia, con id = 0
			}
		}

		// revisar que no se solapen
		for (Object key : periodosPorGarantia.keySet()) {
			// periodos de la misma garantia
			List<PeriodoCosechaConfig> periodosGar = new ArrayList<PeriodoCosechaConfig>(
					(Collection<PeriodoCosechaConfig>) periodosPorGarantia.get(key));

			for (int i = 0; i < periodosGar.size(); i++) {
				PeriodoCosechaConfig p = periodosGar.get(i); // periodo a comparar

				for (int j = 0; j < periodosGar.size(); j++) { // comparo con todos los otros
					PeriodoCosechaConfig otro = periodosGar.get(j);
					if (p != otro) {
						if (p.isSolapado(otro)) {
							errors.put("CosechaConfig.periodo.solapa", "CosechaConfig.periodo.solapa");
							break;
						}
					}
				}
			}
		}

//		for (PeriodoCosechaConfig periodo : cosecha.getPeriodos()) {
//			if(tieneGarantia==true && periodo.getTipoGarantia_id()==null){
//				tieneGarantia=false;
//			}else if(tieneGarantia==false && (periodo.getTipoGarantia_id()==null || periodo.getTipoGarantia_id() == 0L)){
//				errors.put("CosechaConfig.periodo.garantia","CosechaConfig.periodo.garantia");
//			}else if(tieneGarantia==false && periodo.getTipoGarantia_id()!=null){
//				continue;
//			}
//		}
		return this.errors.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public boolean validatePeriodos(PeriodoCosechaConfig periodo) {
		errors = new HashMap<String, Object>();
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

		if (periodo.getFechaHasta() == null || periodo.getFechaDesde() == null) {
			errors.put("cosechaConfig.fechas", "cosechaConfig.fechas");
		}

		List<PeriodoCosechaConfig> periodos = bp
				.createQuery("SELECT p FROM PeriodoCosechaConfig p WHERE p.fechaDesde =:desde AND p.fechaHasta =:hasta "
						+ " AND p.tipoGarantia_id = :idGarantia")
				.setParameter("desde", periodo.getFechaDesde()).setParameter("hasta", periodo.getFechaHasta())
				.setParameter("idGarantia", periodo.getTipoGarantia_id()).list();

		if (periodos != null && periodos.size() > 0) {
			errors.put("CosechaConfig.garantias", "CosechaConfig.garantias");
		}

		if (errors.isEmpty()) {
			if (periodo.getFechaHasta().before(periodo.getFechaDesde())) {
				errors.put("CosechaConfig.periodos", "CosechaConfig.periodos");
			}
		}

		if (errors.isEmpty()) {
			if (periodo.getValorPorGar() == 0) {
				errors.put("CosechaConfig.por", "CosechaConfig.por");
			}
		}

		return this.errors.isEmpty();
	}

	public List<PeriodoCosechaConfig> getPeriodos() {
		return periodos;
	}

	public void setPeriodos(List<PeriodoCosechaConfig> periodos) {
		this.periodos = periodos;
	}

	public PeriodoCosechaConfig getPeriodoCosechaConfig() {
		return periodoCosechaConfig;
	}

	public void setPeriodoCosechaConfig(PeriodoCosechaConfig periodoCosechaConfig) {
		this.periodoCosechaConfig = periodoCosechaConfig;
	}

	public CosechaConfig getCosechaConfig() {
		return cosechaConfig;
	}

	public void setCosechaConfig(CosechaConfig cosechaConfig) {
		this.cosechaConfig = cosechaConfig;
	}

	public CosechaConfig getCosechaConfigSave() {
		return cosechaConfigSave;
	}

	public void setCosechaConfigSave(CosechaConfig cosechaConfigSave) {
		this.cosechaConfigSave = cosechaConfigSave;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public void setForwardURL(String forwardURL) {
		this.forwardURL = forwardURL;
	}

	public Long getIdCosechaConfig() {
		return idCosechaConfig;
	}

	public void setIdCosechaConfig(Long idCosechaConfig) {
		this.idCosechaConfig = idCosechaConfig;
	}

	public int getPeriodoIndex() {
		return periodoIndex;
	}

	public void setPeriodoIndex(int periodoIndex) {
		this.periodoIndex = periodoIndex;
	}
}
