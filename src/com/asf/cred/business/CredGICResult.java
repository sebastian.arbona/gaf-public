package com.asf.cred.business;

import java.io.Serializable;

public class CredGICResult implements Serializable {

	private static final long serialVersionUID = 1913339866183487930L;
	
	private String codigo1;
	private Long idCredito;
	private String titular;
	private String lineaCredito;
	private String lineaOriginal;
	private String mensaje;
	
	public String getCodigo1() {
		return codigo1;
	}
	public void setCodigo1(String codigo1) {
		this.codigo1 = codigo1;
	}
	public Long getIdCredito() {
		return idCredito;
	}
	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getLineaCredito() {
		return lineaCredito;
	}
	public void setLineaCredito(String lineaCredito) {
		this.lineaCredito = lineaCredito;
	}
	public String getLineaOriginal() {
		return lineaOriginal;
	}
	public void setLineaOriginal(String lineaOriginal) {
		this.lineaOriginal = lineaOriginal;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String error) {
		this.mensaje = error;
	}
	
}
