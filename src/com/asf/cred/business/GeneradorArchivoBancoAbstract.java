package com.asf.cred.business;

import com.asf.gaf.hibernate.Bancos;

public abstract class GeneradorArchivoBancoAbstract {
	
	private Bancos banco;
	
	public abstract boolean generar();
	
	public void setBanco(Bancos banco) {
		this.banco = banco;
	}
	
	public Bancos getBanco() {
		return banco;
	}

}
