package com.asf.cred.business;

import java.util.Date;
import java.util.HashMap;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;

public class ReporteTendenciaProyectoProcess implements IProcess {
	private String forward = "ReporteTendenciaProyectoProcess";
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;
	private String action;
	private String estadistica;
	private Date desdeFecha;
	private Date hastaFecha;

	private Long idLinea;
	private String estadisticas;

	public ReporteTendenciaProyectoProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
	}

	public boolean doProcess() {
		if (this.action != null && this.action.equals("consultar")) {
			// getEstadisticaProyecto();
		}
		return this.errores.isEmpty();
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public boolean validate() {
		return this.errores.isEmpty();
	}

	@Override
	public Object getResult() {
		return null;
	}

	public HashMap getErrors() {
		return this.errores;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public Date getDesdeFecha() {
		return desdeFecha;
	}

	public void setDesdeFecha(Date desdeFecha) {
		this.desdeFecha = desdeFecha;
	}

	public String getDesdeFechaStr() {
		return DateHelper.getString(desdeFecha);
	}

	public void setDesdeFechaStr(String f) {
		this.desdeFecha = DateHelper.getDate(f);
	}

	public Date getHastaFecha() {
		return hastaFecha;
	}

	public void setHastaFecha(Date hastaFecha) {
		this.hastaFecha = hastaFecha;
	}

	public String getHastaFechaStr() {
		return DateHelper.getString(hastaFecha);
	}

	public void setHastaFechaStr(String f) {
		setHastaFecha(DateHelper.getDate(f));
	}

	public String getEstadisticas() {
		return estadisticas;
	}

	public void setEstadisticas(String estadisticas) {
		this.estadisticas = estadisticas;
	}

}
