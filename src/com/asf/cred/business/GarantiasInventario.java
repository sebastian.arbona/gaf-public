package com.asf.cred.business;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.TipoGarantia;

public class GarantiasInventario extends ConversationProcess {

	private static String sql;
	private String[] tiposGarantias;
	private String[] estadosGarantias;
	private String[] lineas;
	private String estadoSeguro;
	private List<GarantiasInventarioBean> resultado;
	private String fechaDesdeStr;
	private String fechaHastaStr;
	private int fechaProsValor = 1;
	private String consultaProcesoValor;
	private String usuario;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean ejecutar() {
		SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
		String query = getSql().replace(":hoy", new Timestamp(new Date().getTime()).toString());
		if (tiposGarantias != null && tiposGarantias.length > 0) {
			query = query.replace(":idTipoGarantia", "1").replace(":tiposGarantias", getArrayString(tiposGarantias));
		} else {
			query = query.replace(":idTipoGarantia", "null").replace(":tiposGarantias", "0");
		}

		if (estadoSeguro != null && !estadoSeguro.isEmpty()) {
			query = query.replace(":estadoSeguro", "'" + estadoSeguro + "'");
		} else {
			query = query.replace(":estadoSeguro", "null");
		}

		if (estadosGarantias != null && estadosGarantias.length > 0) {

			query = query.replace(":estadosGarantias", getArrayString(estadosGarantias, true)).replace(":estado", "1");

		} else {
			if ("tribunalDeCuentas".equals(this.usuario)) {

				List<Estado> listEstado = this.getEstadosGarantiasHtcList();
				String[] estadosGarantiaEnum = new String[listEstado.size()];

				int i = 0;
				for (Estado elmEstado : listEstado) {

					if (!"CANCELADA".equals(elmEstado.getComportamiento())
							&& !"EJECUTADA".equals(elmEstado.getComportamiento())
							&& !"VENCIDA".equals(elmEstado.getComportamiento())) {

						estadosGarantiaEnum[i] = elmEstado.getComportamiento();
						i++;
					}
				}

				query = query.replace(":estadosGarantias", getArrayString(estadosGarantiaEnum, true)).replace(":estado",
						"1");

			} else {
				query = query.replace(":estadosGarantias", "0").replace(":estado", "null");
			}
		}

		if (lineas != null && lineas.length > 0) {
			query = query.replace(":idLinea", "1").replace(":idsLineas", getArrayString(lineas));
		} else {
			query = query.replace(":idLinea", "null").replace(":idsLineas", "0");
		}

		if (fechaDesdeStr != null && fechaDesdeStr.trim().length() != 0) {
			query = query.replace(":fechaDesde",
					"'" + formatoFecha.format(DateHelper.getDate(this.fechaDesdeStr)) + "'");
		} else {
			query = query.replace(":fechaDesde", "null");
		}

		if (fechaHastaStr != null && fechaHastaStr.trim().length() != 0) {
			query = query.replace(":fechaHasta",
					"'" + formatoFecha.format(DateHelper.getDate(this.fechaHastaStr)) + "'");
		} else {
			query = query.replace(":fechaHasta", "null");
		}

		switch (fechaProsValor) {
		case 1:
			consultaProcesoValor = "ge.fechaProceso = (select max(fechaProceso) from GarantiaEstado where objetoiGarantia_id = og.id and fechaProceso";
			break;
		case 2:
			consultaProcesoValor = "ge.fechaEstado = (select max(fechaEstado) from GarantiaEstado where objetoiGarantia_id = og.id and fechaEstado";
			break;
		}

		// String consulta = "ge.fechaEstado = (select max(fechaEstado) from
		// GarantiaEstado where objetoiGarantia_id = og.id and fechaEstado";
		query = query.replace(":consultaProcesoValor", consultaProcesoValor);

		List<Object[]> result = bp.createSQLQuery(query).list();

		resultado = new ArrayList<GarantiasInventarioBean>();
		for (Object[] r : result) {
			GarantiasInventarioBean b = new GarantiasInventarioBean(r);
			resultado.add(b);
		}

		return true;
	}

	@Override
	protected String getDefaultForward() {
		return "GarantiasInventario";
	}

	private static String getArrayString(String[] array) {
		return getArrayString(array, false);
	}

	private static String getArrayString(String[] array, boolean quote) {
		if (array == null || array.length == 0) {
			return "";
		}

		String r = "";
		for (String s : array) {

			r += (quote ? "'" : "") + s + (quote ? "'" : "") + ",";
		}
		r = r.substring(0, r.length() - 1);
		return r;
	}

	private static String getSql() {
		if (sql == null) {
			try {
				sql = IOUtils.toString(GarantiasInventario.class.getResourceAsStream("garantias-inventario.sql"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sql;
	}

	@SuppressWarnings("unchecked")
	public List<TipoGarantia> getTiposGarantiasList() {
		return bp.getByFilter("select t from TipoGarantia t order by t.id");
	}

	@SuppressWarnings("unchecked")
	public List<Estado> getEstadosGarantiasList() {
		return bp.getByFilter("select e from Estado e where e.tipo = 'Garantia' order by e.nombreEstado");
	}

	@SuppressWarnings("unchecked")
	public List<Estado> getEstadosGarantiasHtcList() {
		return bp.getByFilter(
				"select e from Estado e where e.tipo = 'Garantia' AND e.comportamiento NOT IN ('CANCELADA', 'EJECUTADA', 'VENCIDA') order by e.nombreEstado");
	}

	@SuppressWarnings("unchecked")
	public List<Linea> getLineasList() {
		return bp.getByFilter("select l from Linea l order by l.nombre");
	}

	public String[] getTiposGarantias() {
		return tiposGarantias;
	}

	public void setTiposGarantias(String[] tiposGarantias) {
		this.tiposGarantias = tiposGarantias;
	}

	public String[] getEstadosGarantias() {
		return estadosGarantias;
	}

	public void setEstadosGarantias(String[] estadosGarantias) {
		this.estadosGarantias = estadosGarantias;
	}

	public String[] getLineas() {
		return lineas;
	}

	public void setLineas(String[] lineas) {
		this.lineas = lineas;
	}

	public String getEstadoSeguro() {
		return estadoSeguro;
	}

	public void setEstadoSeguro(String estadoSeguro) {
		this.estadoSeguro = estadoSeguro;
	}

	public List<GarantiasInventarioBean> getResultado() {
		return resultado;
	}

	public void setResultado(List<GarantiasInventarioBean> resultado) {
		this.resultado = resultado;
	}

	public String getFechaDesdeStr() {
		return fechaDesdeStr;
	}

	public void setFechaDesdeStr(String fechaDesdeStr) {
		this.fechaDesdeStr = fechaDesdeStr;
	}

	public String getFechaHastaStr() {
		return fechaHastaStr;
	}

	public void setFechaHastaStr(String fechaHastaStr) {
		this.fechaHastaStr = fechaHastaStr;
	}

	public int getFechaProsValor() {
		return fechaProsValor;
	}

	public void setFechaProsValor(int fechaProsValor) {
		this.fechaProsValor = fechaProsValor;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}
