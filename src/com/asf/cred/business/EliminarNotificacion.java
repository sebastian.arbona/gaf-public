package com.asf.cred.business;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.nirven.creditos.hibernate.Notificacion;

public class EliminarNotificacion extends ConversationProcess {

	private Long idObjetoi;
	private Long idNotificacion;
	private Long idPersona;

	@ProcessMethod(defaultAction = true)
	public boolean eliminar() {
		Notificacion notificacion = (Notificacion) bp.getById(Notificacion.class, idNotificacion);
		idObjetoi = notificacion.getCredito() != null ? notificacion.getCredito().getId() : null;
		idPersona = notificacion.getPersona() != null ? notificacion.getPersona().getId() : null;

		if (idObjetoi != null && idObjetoi != 0L) {
			bp.delete(notificacion);

			forward = "Location: " + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ "/actions/process.do?do=process&processName=NotificacionProcess" + "&process.idCredito="
					+ idObjetoi + "&process.accion=verNotif&process.pestania=true";
			return true;
		} else if (idPersona != null && idPersona != 0L) {
			bp.delete(notificacion);

			forward = "Location: " + SessionHandler.getCurrentSessionHandler().getRequest().getContextPath()
					+ "/actions/process.do?do=process&processName=NotificacionProcess" + "&process.idPersona="
					+ idPersona + "&process.accion=verNotifPersona&process.pestania=true";
			return true;
		} else {
			return false;
		}

	}

	@Override
	public String getForward() {
		return forward;
	}

	@Override
	protected String getDefaultForward() {
		return forward;
	}

	public void setIdNotificacion(Long idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
}
