package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.ArrayHelper;
import com.asf.util.DateHelper;
import com.civitas.hibernate.persona.Estado;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.ObjetoiComportamiento;

public class ReporteEstadisticaProyectoProcess implements IProcess {

	private static final int PESOS = 1;
	private static final int FECHA_PROCESO = 1;

	private String forward = "ReporteEstadisticaProyectoProcess";
	private HashMap<String, Object> errores;
	private BusinessPersistance bp;
	private String action;
	private String estadistica;
	private Date desdeFecha;
	private Date hastaFecha;
	private Date informacionAFecha;

	private Long idLinea;
	private int filtroMoneda;
	private String directorIdEstados = "";
	private List<Estado> estados;
	private int fechas;
	private String estadisticas;

	public ReporteEstadisticaProyectoProcess() {
		this.errores = new HashMap<String, Object>();
		this.bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		this.filtroMoneda = PESOS;
		this.fechas = FECHA_PROCESO;
	}

	public boolean doProcess() {
		this.filtraEstadosVisiblesPorUsuario();
		if (this.action != null && this.action.equals("consultar")) {
			getEstadisticaProyecto();
		}
		return this.errores.isEmpty();
	}

	@SuppressWarnings("unchecked")
	private void getEstadisticaProyecto() {
		try {
			String consulta = getQuery();

			List<Object[]> resultado = this.bp.createSQLQuery(consulta).list();

			String nombreEstado = null;

			JsonArray list = new JsonArray();
			if (resultado != null) {

				ArrayList<String> nombreEstados = new ArrayList<>();
				for (Object[] l : resultado) {
					nombreEstado = (String) l[1];
					if (nombreEstado != null && !nombreEstado.isEmpty()) {
						nombreEstados.add(nombreEstado);
					}
				}

				nombreEstados = ArrayHelper.eliminarDuplicadosArrayString(nombreEstados);

				for (String estado : nombreEstados) {
					int cantProy = 0;
					Double acumulado = 0.0;

					for (Object[] l : resultado) {
						nombreEstado = (String) l[1];
						if (estado.equals(nombreEstado)) {
							Double desembolso = (Double) l[3];
							cantProy++;
							acumulado += (desembolso != null ? desembolso : 0.0);
						}
					}

					JsonObject obj = new JsonObject();
					obj.addProperty("estado", estado);
					obj.addProperty("cantProy", cantProy);
					obj.addProperty("valor", acumulado);
					list.add(obj);
				}

			}
			this.estadistica = list.toString();

		} catch (Exception e) {
			this.estadistica = "";
			e.printStackTrace();
		}
	}

	private String getQuery() {
		String monedaAbreviatura;
		String fechaInformacionAFecha;
		String cotizacion, cotizacionDesembolso;
		String fechaCtacte = "fechaProceso";
		String fechaNoCorriente;
		String fechaDesde;
		String fechaHasta;

		fechaDesde = this.getDesdeFechaStr();
		fechaDesde = fechaDesde.substring(fechaDesde.lastIndexOf("/") + 1) + "-"
				+ fechaDesde.substring(fechaDesde.indexOf("/") + 1, fechaDesde.lastIndexOf("/")) + "-"
				+ fechaDesde.substring(0, fechaDesde.indexOf("/")) + " 00:00:00";

		fechaHasta = this.getHastaFechaStr();
		fechaHasta = fechaHasta.substring(fechaHasta.lastIndexOf("/") + 1) + "-"
				+ fechaHasta.substring(fechaHasta.indexOf("/") + 1, fechaHasta.lastIndexOf("/")) + "-"
				+ fechaHasta.substring(0, fechaHasta.indexOf("/")) + " 23:59:59";

		fechaInformacionAFecha = this.getInformacionAFechaStr();
		fechaInformacionAFecha = fechaInformacionAFecha.substring(fechaInformacionAFecha.lastIndexOf("/") + 1) + "-"
				+ fechaInformacionAFecha.substring(fechaInformacionAFecha.indexOf("/") + 1,
						fechaInformacionAFecha.lastIndexOf("/"))
				+ "-" + fechaInformacionAFecha.substring(0, fechaInformacionAFecha.indexOf("/")) + " 23:59:59";

		fechaNoCorriente = this.getInformacionAFechaStr();
		fechaNoCorriente = Integer.parseInt(fechaNoCorriente.substring(fechaNoCorriente.lastIndexOf("/") + 1)) + 1
				+ "-01-01 00:00:00";

		if (this.filtroMoneda == PESOS)
			monedaAbreviatura = "Peso";
		else
			monedaAbreviatura = "Dolar";

		boolean ultimoDia = false;
		String periodo = "";
		if (this.fechas == FECHA_PROCESO) {
			Calendar c = Calendar.getInstance();
			c.setTime(DateHelper.getDate(getInformacionAFechaStr()));
			if (c.get(Calendar.DATE) == 31 && c.get(Calendar.MONTH) == Calendar.DECEMBER) {
				ultimoDia = true;
			}
			periodo = Integer.toString(c.get(Calendar.YEAR));
		}

		cotizacion = "(SELECT TOP 1 CASE WHEN cotiza.DTYPE = 'CtaCteAjuste' "
				+ "THEN (CASE WHEN cotiza.cotizacionDiferencia IS NULL THEN 1 ELSE cotiza.cotizacionDiferencia END) "
				+ "ELSE (CASE WHEN cotiza.cotizaMov IS NULL THEN 1 ELSE cotiza.cotizaMov END) END "
				+ "FROM ctacte cotiza WHERE cotiza.objetoi_id = cc.objetoi_id AND cotiza." + fechaCtacte
				+ " = (SELECT MAX(cta." + fechaCtacte + ") FROM ctacte cta WHERE cta." + fechaCtacte + " <= '"
				+ fechaInformacionAFecha + "' AND cta.objetoi_id = cotiza.objetoi_id)) ";

		cotizacionDesembolso = "(SELECT TOP 1 CASE WHEN cotiza.DTYPE = 'CtaCteAjuste' "
				+ "THEN (CASE WHEN cotiza.cotizacionDiferencia IS NULL THEN 1 ELSE cotiza.cotizacionDiferencia END) "
				+ "ELSE (CASE WHEN cotiza.cotizaMov IS NULL THEN 1 ELSE cotiza.cotizaMov END) END "
				+ "FROM ctacte cotiza WHERE cotiza.objetoi_id = des.credito_id AND cotiza." + fechaCtacte
				+ " = (SELECT MAX(cta." + fechaCtacte + ") FROM ctacte cta WHERE cta." + fechaCtacte + " <= '"
				+ fechaInformacionAFecha + "' AND cta.objetoi_id = cotiza.objetoi_id)) ";

		String consulta = "SELECT lin.nombre, est.nombreEstado, mon.abreviatura,"
				+ "(CASE WHEN desemb.desembOriginal IS NULL THEN 0 ELSE desemb.desembOriginal END) 'desembolsadoOriginal' "
				+ "FROM objetoi obj LEFT JOIN AcuerdoPago ap ON ap.creditoAcuerdo_id = obj.id "
				+ "JOIN persona per ON per.idpersona = obj.persona_idpersona "
				+ "JOIN linea lin ON lin.id = obj.linea_id "
				+ ((this.idLinea != null && this.idLinea > 0L) ? ("AND lin.id =" + this.idLinea) : "")
				+ " JOIN moneda mon ON lin.moneda_idmoneda = mon.idMoneda AND mon.abreviatura = '" + monedaAbreviatura
				+ "' JOIN objetoiestado obje ON obje.objetoi_id = obj.id "
				+ "AND obje.fecha = (SELECT MAX(obje2.fecha) FROM ObjetoiEstado obje2 WHERE obje.objetoi_id = obje2.objetoi_id "
				+ "AND obje2.fecha <= '" + fechaInformacionAFecha + "' AND obje2.estado_idestado IS NOT NULL) "
				+ "JOIN estado est ON est.idEstado = obje.estado_idEstado AND est.idEstado IN ("
				+ this.directorIdEstados + ") JOIN "
				+ "(SELECT objc.objetoi_id, area.nombre, TC.TF_DESCRIPCION FROM objetoiComportamiento objc "
				+ "JOIN unidad area ON (" + AreaComportamientoPago("DEPTESA") + " OR "
				+ AreaComportamientoPago("DSECRE") + " OR " + AreaComportamientoPago("LEES") + " OR "
				+ AreaComportamientoPago("SUDDRL") + ") "
				+ "LEFT JOIN TIPIFICADORES TC ON objc.comportamientoPago = TC.TF_CODIGO AND TC.TF_CATEGORIA = 'comportamientoPago' "
				+ "WHERE objc.fecha = (SELECT MAX(objc2.fecha) FROM ObjetoiComportamiento objc2 WHERE objc.objetoi_id = objc2.objetoi_id "
				+ "AND objc2.fecha <= '" + fechaInformacionAFecha
				+ "' AND objc2.comportamientoPago IS NOT NULL)) comportamiento "
				+ "ON comportamiento.objetoi_id = obj.id LEFT JOIN (SELECT cc.objetoi_id, "
				// saldoCapitalOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'saldoCapitalOriginal', "
				// saldoCapitalPesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
				+ "'saldoCapitalPesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN('cap') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) saldoCapital ON saldoCapital.objetoi_id = obj.id LEFT JOIN "
				+ "(SELECT cc.objetoi_id, "
				// interesesOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'interesesOriginal', "
				// interesesPesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
				+ "'interesesPesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('com','mor','pun','bon') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) intereses ON intereses.objetoi_id = obj.id LEFT JOIN "
				+ "(SELECT cc.objetoi_id, "
				// intcompOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'intcompOriginal', "
				// intcompPesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
				+ "'intcompPesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('com','bon') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) intcomp ON intcomp.objetoi_id = obj.id LEFT JOIN "
				+ "(SELECT cc.objetoi_id, "
				// intmorOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'intmorOriginal', "
				// intmorPesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
				+ "'intmorPesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('mor') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) intmor ON intmor.objetoi_id = obj.id LEFT JOIN (SELECT cc.objetoi_id, "
				// intpunOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'intpunOriginal', "
				// intpunPesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
				+ "'intpunPesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('pun') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) intpun ON intpun.objetoi_id = obj.id "
				// CER
				+ "LEFT JOIN (SELECT cc.objetoi_id, "
				// cer
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'cer' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('CER') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) cer ON cer.objetoi_id = obj.id LEFT JOIN (SELECT des.credito_id, "
				// desembOriginal
				+ "SUM(des.importeReal) 'desembOriginal', "
				// desembPesos
				+ "SUM(des.importeReal) * " + cotizacionDesembolso + "'desembPesos' FROM Desembolso des "
				+ "WHERE des.importeReal IS NOT NULL GROUP BY des.credito_id) desemb "
				+ "ON desemb.credito_id = obj.id LEFT JOIN (SELECT cc.objetoi_id, "
				// otrosOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) 'otrosOriginal', "
				// otrosPesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 THEN cc.importe ELSE -cc.importe END) * " + cotizacion
				+ "'otrosPesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN ('rec','gas','mul') "
				+ (ultimoDia ? "WHERE cc.periodoCtacte <= " + periodo + " "
						: "WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha + "' ")
				+ "GROUP BY cc.objetoi_id) otros ON otros.objetoi_id = obj.id LEFT JOIN (SELECT cc.objetoi_id, "
				// saldoCorrienteOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento < '" + fechaNoCorriente
				+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento < '" + fechaNoCorriente
				+ "' THEN -cc.importe ELSE 0 END) END) 'saldoCorrienteOriginal', "
				// saldoCorrientePesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento < '" + fechaNoCorriente
				+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento < '" + fechaNoCorriente
				+ "' THEN -cc.importe ELSE 0 END) END) * " + cotizacion + "'saldoCorrientePesos', "
				// saldoNoCorrienteOriginal
				+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento >= '" + fechaNoCorriente
				+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento >= '" + fechaNoCorriente
				+ "' THEN -cc.importe ELSE 0 END) END) 'saldoNoCorrienteOriginal', "
				// saldoNoCorrientePesos
				+ "SUM(CASE WHEN cc.tipomov_id = 2 AND cuo.fechaVencimiento >= '" + fechaNoCorriente
				+ "' THEN cc.importe ELSE (CASE WHEN cc.tipomov_id = 1 AND cuo.fechaVencimiento >= '" + fechaNoCorriente
				+ "' THEN -cc.importe ELSE 0 END) END) * " + cotizacion + "'saldoNoCorrientePesos' FROM ctacte cc "
				+ "JOIN concepto con ON cc.asociado_id = con.id AND con.concepto_concepto IN('cap','ADC') "
				+ "JOIN cuota cuo ON cc.cuota_id = cuo.id WHERE cc." + fechaCtacte + " <= '" + fechaInformacionAFecha
				+ "' GROUP BY cc.objetoi_id) corriente "
				+ "ON corriente.objetoi_id = obj.id WHERE 0 < (SELECT COUNT(des.id) FROM desembolso des "
				+ "WHERE des.fechaReal BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "' "
				+ "AND des.fechaReal IS NOT NULL AND des.numero = 1 AND des.credito_id = obj.id) "
				+ "ORDER BY lin.codigoCtaContablePatrimonioCapital, obj.numeroAtencion;";

		return consulta;
	}

	@SuppressWarnings("unchecked")
	private void filtraEstadosVisiblesPorUsuario() {
		// Filtrar los estados por tipo de usuario.
		this.directorIdEstados = (String) bp
				.createQuery("SELECT d.valor FROM Director d WHERE d.id = 'idInventarioDeCuentasCorrientes'").list()
				.get(0);

		if (this.directorIdEstados.trim().length() > 0) {
			this.estados = bp.createQuery(
					"SELECT e FROM Estado e WHERE e.nombreEstado IS NOT NULL AND e.tipo='Objetoi' AND e.id IN ("
							+ this.directorIdEstados + ") ORDER BY e.nombreEstado")
					.list();
		} else {
			this.estados = bp.createQuery(
					"SELECT e FROM Estado e WHERE e.nombreEstado IS NOT NULL AND e.tipo='Objetoi' ORDER BY e.nombreEstado")
					.list();
		}
	}

	private String procesarPorEstado(String columna, String[] id) {
		String estractoConsulta = "";
		if (id != null && id.length > 0) {
			estractoConsulta = "AND " + columna + " IN (";
			for (int i = 0; i < id.length; i++) {
				estractoConsulta += id[i] + ", ";
			}
			estractoConsulta = estractoConsulta.substring(0, estractoConsulta.length() - 2) + ") ";
		} else
			estractoConsulta = "AND " + columna + " IN (" + this.directorIdEstados + ") ";
		return estractoConsulta;
	}

	@SuppressWarnings("static-access")
	private String AreaComportamientoPago(String area) {
		String estractoConsulta = "";
		ObjetoiComportamiento comportamiento = new ObjetoiComportamiento();
		if (area.equals("DEPTESA")) {
			estractoConsulta = "(area.codigo = 'DEPTESA' AND objc.comportamientoPago = "
					+ comportamiento.SITUACION_NORMAL + ")";
		} else if (area.equals("DSECRE")) {
			estractoConsulta = "(area.codigo = 'DSECRE' AND (objc.comportamientoPago = "
					+ comportamiento.INADECUADO_CON_ATRASO + " OR objc.comportamientoPago = "
					+ comportamiento.CON_PROBLEMAS_DE_ATRASO + " OR objc.comportamientoPago = "
					+ comportamiento.ALTO_RIESGO_CON_ATRASO + "))";
		} else if (area.equals("LEES")) {
			estractoConsulta = "(area.codigo = 'LEES' AND (objc.comportamientoPago = " + comportamiento.INCOBRABLE + " "
					+ "OR objc.comportamientoPago = " + comportamiento.CONCURSADOS + " OR objc.comportamientoPago = "
					+ comportamiento.CREDITOS_EN_GESTION_JUDICIAL + "))";
		} else if (area.equals("SUDDRL")) {
			estractoConsulta = "(area.codigo = 'SUDDRL' AND objc.comportamientoPago = "
					+ comportamiento.RECLAMO_PREJUDICIAL + ")";
		}
		return estractoConsulta;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public boolean validate() {
		return this.errores.isEmpty();
	}

	@Override
	public Object getResult() {
		return null;
	}

	public HashMap getErrors() {
		return this.errores;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getEstadistica() {
		return estadistica;
	}

	public void setEstadistica(String estadistica) {
		this.estadistica = estadistica;
	}

	public Long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(Long idLinea) {
		this.idLinea = idLinea;
	}

	public Date getDesdeFecha() {
		return desdeFecha;
	}

	public void setDesdeFecha(Date desdeFecha) {
		this.desdeFecha = desdeFecha;
	}

	public String getDesdeFechaStr() {
		return DateHelper.getString(desdeFecha);
	}

	public void setDesdeFechaStr(String f) {
		this.desdeFecha = DateHelper.getDate(f);
	}

	public Date getHastaFecha() {
		return hastaFecha;
	}

	public void setHastaFecha(Date hastaFecha) {
		this.hastaFecha = hastaFecha;
	}

	public String getHastaFechaStr() {
		return DateHelper.getString(hastaFecha);
	}

	public void setHastaFechaStr(String f) {
		setHastaFecha(DateHelper.getDate(f));
	}

	public Date getInformacionAFecha() {
		return this.informacionAFecha;
	}

	public void setInformacionAFecha(Date informacionAFecha) {
		this.informacionAFecha = informacionAFecha;
	}

	public String getInformacionAFechaStr() {
		return DateHelper.getString(this.informacionAFecha);
	}

	public void setInformacionAFechaStr(String informacionAFecha) {
		this.informacionAFecha = DateHelper.getDate(informacionAFecha);
	}

	public int getFiltroMoneda() {
		return filtroMoneda;
	}

	public void setFiltroMoneda(int filtroMoneda) {
		this.filtroMoneda = filtroMoneda;
	}

	public String getDirectorIdEstados() {
		return directorIdEstados;
	}

	public void setDirectorIdEstados(String directorIdEstados) {
		this.directorIdEstados = directorIdEstados;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public int getFechas() {
		return fechas;
	}

	public void setFechas(int fechas) {
		this.fechas = fechas;
	}

	public String getEstadisticas() {
		return estadisticas;
	}

	public void setEstadisticas(String estadisticas) {
		this.estadisticas = estadisticas;
	}

}
