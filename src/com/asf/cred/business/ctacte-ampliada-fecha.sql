select fechaGeneracion, min(periodoCtacte) as periodo, min(movimientoCtacte) as movimiento, 
min(verificadorCtacte) as verificador, min(itemCtacte) as item, tipo, numeroBoleto, idBoleto, detalle, 
sum(capital) as capital, sum(compensatorio) as compensatorio, sum(moratorio) as moratorio, 
sum(punitorio) as punitorio, sum(gastos) as gastos, sum(debe) as debe, sum(haber) as haber, 
caratula_id, boletoReciboId, fechaVencimiento, idObjetoi , min(fechaProceso) as fechaProceso,
cotizaMov, sum(debePesos) AS debePesos, sum(haberPesos) AS haberPesos,
-- min(CONVERT(VARCHAR, movimientoCtacte) + '-' + CONVERT(VARCHAR, periodoCtacte) + '-' + CONVERT(VARCHAR, itemCtacte) + '-' + CONVERT(VARCHAR, verificadorCtacte)) AS idctacte,
min(@EXPRESION_IDCTACTE) AS idctacte, 
sum(cer) as cer 
from (select cc.objetoi_id,  
case when cc.tipomov_id = 2 and conc.concepto_concepto = 'cap' and (cc.tipoMovimiento = 'cuota' or cc.tipoMovimiento = 'movManualDeb') 
then cc.fechaGeneracion 
when cc.tipomov_id = 1 and conc.concepto_concepto = 'cap' and cc.tipoMovimiento = 'movManualCred' then cc.fechaGeneracion 
else cc.fechaVencimiento end  as fechaVencimiento, 
case when cc.emideta_id is not null then null else cc.fechaGeneracion end as fechaGeneracion,
cc.fechaProceso, cc.periodoCtacte, 
case when bol.tipo = 'Nota Debito Desaplicacion' then 'Nota Debito' 
when bol.tipo = 'Nota Credito Anulacion' then 'Nota Credito'
else bol.tipo end as tipo, 
bol.numeroBoleto, bol.id as idBoleto, cc.tipomov_id,  
conc.concepto_concepto, cc.tipoMovimiento,  
case 
-- when cc.tipomov_id = 2 and conc.concepto_concepto = 'cap' and cc.tipoMovimiento = 'cuota' and cc.desembolso_id IS NOT NULL then @DESEMBOLSO_EXPR1 
-- when cc.tipomov_id = 2 and conc.concepto_concepto = 'cap' and cc.tipoMovimiento = 'cuota' and cc.desembolso_id IS NULL then @DESEMBOLSO_EXPR2 
when bol.tipo = 'Nota Debito Desaplicacion' then cc.detalle 
when bol.tipo = 'Nota Credito Anulacion' then cc.detalle 
when cc.tipomov_id = 2 and (conc.concepto_concepto = 'com' or conc.concepto_concepto = 'gas') and cc.tipoMovimiento = 'cuota' then 'Comp y Gastos'  
when cc.tipomov_id = 2 and ((conc.concepto_concepto = 'mor' or conc.concepto_concepto = 'pun') and cc.tipoMovimiento = 'pago') and tipoConcepto not in (110,116) then 'Mor y Pun'  
when cc.tipomov_id = 1 and conc.concepto_concepto = 'bon' and cc.tipoMovimiento = 'bonificacion' then cc.detalle  
when cc.tipomov_id = 1 and cc.detalle = 'Pago excedente' then 'Cobranza' 
when cc.tipomov_id = 1 and (((conc.concepto_concepto = 'gas' and cc.tipoMovimiento = 'cuota') or (conc.concepto_concepto = 'rec' and cc.tipoMovimiento = 'cuota')) and tipoConcepto<>116) then 'Pago Gastos'
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'rec') and cc.caratula_id is null and cc.tipoMovimiento like 'pago' and cc.detalle is not null then cc.detalle 
when cc.tipomov_id = 1 and (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'rec') and cc.caratula_id is null and cc.tipoMovimiento like 'pago' and tipoConcepto<>116 then 'Pago Gastos' 
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'pago' and cc.detalle is not null then cc.detalle
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'pago' then 'Cobranza' 
when cc.tipomov_id = 1 and cc.tipoMovimiento = 'reimputacion' then cc.detalle 
else cc.detalle end as detalle,  
case when conc.concepto_concepto = 'cap' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as capital,  
case when (conc.concepto_concepto = 'com' or conc.concepto_concepto = 'bon') then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as compensatorio,  
case when conc.concepto_concepto = 'mor' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as moratorio,  
case when conc.concepto_concepto = 'pun' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as punitorio,  
case when (conc.concepto_concepto = 'gas' or conc.concepto_concepto = 'mul' or conc.concepto_concepto = 'rec') then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as gastos,  
case when conc.concepto_concepto = 'cer' then case when cc.tipomov_id = 2 then cc.importe else -cc.importe end else 0 end as cer, 
case cc.tipomov_id when 2 then cc.importe else 0 end as debe,  
case cc.tipomov_id when 1 then cc.importe else 0 end as haber,  
case when (cc.tipoMovimiento = 'pago' or cc.tipoMovimiento = 'reimputacion') then 5  
else case conc.concepto_concepto when 'cap' then 1  
	when 'com' then 2  
	when 'gas' then 2  
	when 'mul' then 2  
	when 'bon' then 3  
	when 'mor' then 4  
	when 'pun' then 4  
	else 5 end  
end as orden,  
cc.movimientoCtacte, cc.verificadorCtacte, cc.itemCtacte, p.caratula_id, p.boleto_id as boletoReciboId, cc.objetoi_id as idObjetoi,
CASE cc.DTYPE WHEN 'CtaCteAjuste' THEN cc.cotizacionDiferencia 
	ELSE CASE WHEN cc.cotizaMov IS NOT NULL THEN cc.cotizaMov ELSE 1 END END AS cotizaMov, 
CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.debePesos 
	WHEN cc.tipomov_id = 2 THEN cc.importe * CASE WHEN cc.cotizaMov IS NOT NULL THEN cc.cotizaMov ELSE 1 END 
	ELSE 0 END AS debePesos, 
CASE WHEN cc.DTYPE = 'CtaCteAjuste' THEN cc.haberPesos 
	WHEN cc.tipomov_id = 1 THEN cc.importe * CASE WHEN cc.cotizaMov IS NOT NULL THEN cc.cotizaMov ELSE 1 END 
	ELSE 0 END AS haberPesos
from Ctacte cc left join Boleto bol on cc.boleto_id = bol.id left join Pagos p on p.recibo_id = bol.id, Concepto conc, CConcepto cconc  
where cc.objetoi_id = @IDOBJETOI and cc.asociado_id = conc.id and conc.concepto_concepto = cconc.concepto 
@FECHAS_WHERE_EXPR
) t  
group by fechaGeneracion, tipo, numeroBoleto, idBoleto,  
detalle, caratula_id, boletoReciboId, fechaVencimiento, idObjetoi, cast(fechaProceso as date), cotizaMov 
order by fechaProceso, movimiento, item