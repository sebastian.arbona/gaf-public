package com.asf.cred.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.hibernate.HibernateException;

import com.asf.hibernate.mapping.Claves;
import com.asf.hibernate.mapping.Menu;
import com.asf.hibernate.mapping.Permiso;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.struts.action.IProcess;
import com.asf.util.DateHelper;
import com.asf.util.ReportResult;
import com.civitas.hibernate.persona.Estado;
import com.civitas.hibernate.persona.Persona;
import com.civitas.logger.LoggerService;
import com.nirven.creditos.hibernate.AuditoriaFinal;
import com.nirven.creditos.hibernate.CosechaConfig;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.EstadoLiberacion;
import com.nirven.creditos.hibernate.Garantia;
import com.nirven.creditos.hibernate.Liberaciones;
import com.nirven.creditos.hibernate.Linea;
import com.nirven.creditos.hibernate.LoteDesembolso;
import com.nirven.creditos.hibernate.NovedadCtaCte;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiEstado;
import com.nirven.creditos.hibernate.ObjetoiIndice;
import com.nirven.creditos.hibernate.ObjetoiVinedo;
import com.nirven.creditos.hibernate.PeriodoCosechaConfig;
import com.nirven.creditos.hibernate.Usuario;

/**
 * @author eselman
 */

public class EjecucionDesembolsosMasivos implements IProcess {
	// =========================ATRIBUTOS===========================
	// atributos utilitarios del proceso.-
	private String forward = "EjecucionDesembolsosMasivos";
	private HashMap<String, Object> errores;
	private String accion = "";
	private BusinessPersistance bp;
	// atributos del proceso.-
	private Persona persona;
	private List<ObjetoiDTO> creditos;
	@SuppressWarnings("unused")
	private ReportResult reportResult;
	private Long idObjetoi;
	private Long idPersona;
	private boolean primeraEtapa = true;
	private boolean ejecucion2;
	private Double importeGastosIngresado;
	private Double importeMultasIngresado;
	private boolean ejecutado;
	// Provisorio hasta definir
	private boolean ocultarGastosMultas;
	private Double importeGastosCosecha;
	private ReportResult reporte;
	@SuppressWarnings("unused")
	private String fechaActual;
	private String nroLoteElegido;
	private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	private Date fechaLote = new Date();
	private List<Linea> lineas;
	private Long[] seleccion;
	LoggerService logger = LoggerService.getInstance();

	// =======================CONSTRUCTORES=========================
	@SuppressWarnings("unchecked")
	public EjecucionDesembolsosMasivos() {
		errores = new HashMap<String, Object>();
		bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		creditos = (List<ObjetoiDTO>) SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.getAttribute("creditosEjecucionDesembolsosMasivos");
		if (creditos == null) {
			creditos = new ArrayList<ObjetoiDTO>();
		}
		lineas = bp.createQuery("select l from Linea l order by l.nombre").list();
	}

	// ======================FUNCIONALIDADES========================
	public boolean doProcess() {

		if (accion != null) {
			switch (accion) {
			case "listar1":
				listarCreditos();
				calcular();
				break;
			case "listar2":
				ejecucion2 = true;
				listarCreditos();
				calcular2();
				break;
			case "imprimirReporte":
			case "imprimirVolante":
				if (nroLoteElegido == null || nroLoteElegido.equals("")) {
					errores.put("ejecucionMasiva.lote", "ejecucionMasiva.lote");
				} else {
					imprimir();
				}
				if (this.errores.isEmpty()) {
					this.forward = "ReportesProcess";
				}
				break;
			case "solicitar1":
				if (verificarPermisoSolicitarGAF()) {
					solicitar();
				} else {
					errores.put("desembolsosEjecucion.SolicitarGafPermiso",
							new ActionMessage("desembolsosEjecucion.SolicitarGafPermiso",
									"El usuario no tiene permisos para solicitar."));
				}
				break;
			case "solicitar2":
				if (verificarPermisoSolicitarGAF()) {
					solicitar2();
				} else {
					errores.put("desembolsosEjecucion.SolicitarGafPermiso",
							new ActionMessage("desembolsosEjecucion.SolicitarGafPermiso",
									"El usuario no tiene permisos para solicitar."));
				}
				break;
			default:
				break;
			}
		}
		return errores.isEmpty();
	}

	// =====================UTILIDADES PRIVADAS=====================
	@SuppressWarnings("unchecked")
	private void listarCreditos() {
		try {
			Boolean procesoCosecha = false;
			String lineasIds = "";
			Linea tempLinea = null;
			for (Long lineaId : seleccion) {
				lineasIds += lineaId + ",";
				tempLinea = (Linea) bp.getById(Linea.class, lineaId);
				procesoCosecha = procesoCosecha || tempLinea.isLineaCosecha() || tempLinea.isLineaCosechaPreAprobada();
			}
			if (!lineasIds.isEmpty()) {
				lineasIds = lineasIds.substring(0, lineasIds.length() - 1);
			}
			List<Objetoi> objetos;
			if (ejecucion2) {
				objetos = bp.createQuery(
						"SELECT credito FROM ObjetoiEstado oe JOIN oe.objetoi credito JOIN oe.estado estado JOIN credito.linea l WHERE estado.nombreEstado = 'EJECUCION' "
								+ " AND EXISTS (SELECT 1 FROM Desembolso des WHERE des.credito = oe.objetoi AND des.numero = 1 AND des.fechaReal IS NOT NULL AND des.fechaSolicitud IS NOT NULL) "
								+ " AND EXISTS (SELECT 1 FROM Desembolso des WHERE des.credito = oe.objetoi AND des.numero = 2 AND des.fechaReal IS NULL AND des.fechaSolicitud IS NULL) AND oe.fechaHasta IS NULL AND l.id IN ("
								+ lineasIds + ") AND credito.autorizadoQQManual = 1")
						.list();
			} else {

				objetos = bp.createQuery(
						"SELECT credito FROM ObjetoiEstado oe JOIN oe.objetoi credito JOIN oe.estado estado JOIN credito.linea l WHERE estado.nombreEstado = 'CONTRATO EMITIDO' "
								+ " AND EXISTS (SELECT 1 FROM Desembolso des WHERE des.credito = oe.objetoi AND des.numero = 1 AND des.fechaReal IS NULL AND des.fechaSolicitud IS NULL) "
								+ (procesoCosecha
										? " AND EXISTS (SELECT 1 FROM Desembolso des WHERE des.credito = oe.objetoi AND des.numero = 2 AND des.fechaReal IS NULL AND des.fechaSolicitud IS NULL) "
										: "")
								+ " AND oe.fechaHasta IS NULL AND credito.fechaFirmaContrato IS NOT NULL AND l.id IN ("
								+ lineasIds + ") ")
						.list();
			}
			creditos = new ArrayList<ObjetoiDTO>();
			Estado estado;
			ObjetoiDTO credito = null;
			Desembolso desembolso1;
			Desembolso desembolso2;
			for (Objetoi o : objetos) {
				desembolso2 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
						.setLong("idCredito", o.getId()).setInteger("numero", 2).setMaxResults(1).uniqueResult();
				if (ejecucion2) {
					if (desembolso2 != null && !desembolso2.getEstado().equals("4")) {
						continue;
					}
				}
				desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
						.setLong("idCredito", o.getId()).setInteger("numero", 1).setMaxResults(1).uniqueResult();
				estado = o.getEstadoActual().getEstado();
				if ((desembolso1 != null && !desembolso1.getEstado().equals("3"))
						|| (desembolso2 != null && !desembolso2.getEstado().equals("3"))) {
					if (estado != null) {
						credito = new ObjetoiDTO(o, estado.getNombreEstado(), 0);
						creditos.add(credito);
					} else if (estado == null) {
						credito = new ObjetoiDTO(o, "Sin estado", 0);
						creditos.add(credito);
					}
					if (credito != null) {
						List<Garantia> garantias = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
								.setParameter("credito", o).list();
						if (!garantias.isEmpty()) {
							credito.setTipoGarantia(garantias.get(0).getTipo().getNombre());
						}
					}
					if (ejecucion2) {
						setearTipoDesembolso(creditos.get(creditos.size() - 1));
					}
				}
			}
			SessionHandler.getCurrentSessionHandler().getRequest().getSession()
					.setAttribute("creditosEjecucionDesembolsosMasivos", creditos);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void calcular() {
		Iterator<ObjetoiDTO> iteratorCreditos = creditos.iterator();
		while (iteratorCreditos.hasNext()) {
			ObjetoiDTO dto = iteratorCreditos.next();
			Objetoi credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
			Desembolso desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
					.setLong("idCredito", credito.getId()).setInteger("numero", 1).uniqueResult();
			if (desembolso1 != null) {
				dto.setNroOrden(desembolso1.getNroOrden());
				dto.setDesembolso(desembolso1.getImporte());
				primeraEtapa = false;
				importeGastosCosecha = obtenerGastosDesembolso(desembolso1);
				dto.setGastos(importeGastosCosecha);
			} else {
				importeGastosCosecha = obtenerGastosDesembolso(credito);
				dto.setGastos(importeGastosCosecha);
			}
		}
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosEjecucionDesembolsosMasivos", creditos);
	}

	@SuppressWarnings("unchecked")
	private void solicitarInmovilizacion(ObjetoiDTO credito) {

		CreditoHandler ch = new CreditoHandler();
//		for (ObjetoiDTO credito : creditos) {
		List<ObjetoiVinedo> vinedos = bp.createQuery("Select v from ObjetoiVinedo v where v.credito=:credito")
				.setParameter("credito", bp.getById(Objetoi.class, credito.getIdObjetoi())).list();
		for (ObjetoiVinedo objetoVin : vinedos) {
			Date fecha = new Date();
			objetoVin.setFechaSolicitudInforme2(fecha);
			bp.update(objetoVin);
		}
		ch.comprobarInmovilizacion(credito.getIdObjetoi());
//		}
	}

	private HashSet<Long> getSeleccionados() {
		HttpServletRequest request = SessionHandler.getCurrentSessionHandler().getRequest();
		Enumeration<String> paramNames = request.getParameterNames();
		HashSet<Long> seleccionados = new HashSet<Long>();
		while (paramNames.hasMoreElements()) {
			String param = paramNames.nextElement();
			if (param.startsWith("requisito")) {
				String[] p = param.split("-");
				seleccionados.add(new Long(p[1]));
			}
		}
		return seleccionados;
	}

	private double obtenerGastosDesembolso(Desembolso desembolso) {
		double importeGastos = 0.0;
		if (desembolso.getCredito().getLinea().getGastosEnDesembolso() != null
				&& desembolso.getCredito().getLinea().getGastosEnDesembolso().booleanValue()) {
			if (desembolso.getNumero() != null && desembolso.getNumero().intValue() == 1) {
				importeGastos = desembolso.getCredito().getLinea()
						.getGastosDesembolso(desembolso.getCredito().getFinanciamiento());
			}
		} else {
			importeGastos = desembolso.getCredito().getLinea().getGastosDesembolso(
					desembolso.getImporteReal() != null ? desembolso.getImporteReal() : desembolso.getImporte());
		}
		return importeGastos;
	}

	private double obtenerGastosDesembolso(Objetoi credito) {
		double importeGastos = 0.0;
		importeGastos = credito.getLinea().getGastosDesembolso(credito.getFinanciamiento());
		return importeGastos;
	}

	@SuppressWarnings("unchecked")
	public boolean solicitar() {
		// estos dos campos se setean en 0 hasta definir
		importeGastosIngresado = 0.0;
		importeMultasIngresado = 0.0;
		if (importeGastosCosecha == null || importeGastosCosecha.isNaN()) {
			errores.put("ejecucionDesembolsoMasivo.importeGastos", "ejecucionDesembolsoMasivo.importeGastos");
			return false;
		}
		if (importeGastosIngresado == null || importeGastosIngresado.isNaN()) {
			errores.put("ejecucionDesembolsoMasivo.importeMultas", "ejecucionDesembolsoMasivo.importeMultas");
			return false;
		}
		HashSet<Long> seleccionados = getSeleccionados();
		if (seleccionados.isEmpty()) {
			errores.put("ejecucionDesembolsoMasivo.seleccion", "ejecucionDesembolsoMasivo.seleccion");
			primeraEtapa = false;
			return false;
		}
		// Busco el numero de lote correspondiente de acuerdo a la fecha
		LoteDesembolso lote = new LoteDesembolso();
		try {
			setFechaLote(new Date());
			List<LoteDesembolso> lotes = bp.createQuery("Select l from LoteDesembolso l where l.fecha = :fecha")
					.setParameter("fecha", getFechaLote()).list();
			Date fechaHoy = new Date();
			if (lotes.size() == 0) {
				lote.setFecha(fechaHoy);
				lote.setNroLote(1L);
			} else {
				LoteDesembolso ultimoLote = lotes.get(lotes.size() - 1);
				lote.setFecha(ultimoLote.getFecha());
				lote.setNroLote(ultimoLote.getNroLote() + 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Iterator<ObjetoiDTO> iteratorCreditos = creditos.iterator();
		while (iteratorCreditos.hasNext()) {
			ObjetoiDTO dto = iteratorCreditos.next();
			if (seleccionados.contains(dto.getIdObjetoi())) {
				Objetoi credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
				if (!credito.validarRegistroGarantiaDesembolso()) {
					errores.put("desembolso.garantiaInscripcion", "desembolso.garantiaInscripcion");
					continue;
				}
				Desembolso desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
						.setLong("idCredito", credito.getId()).setInteger("numero", 1).uniqueResult();
				desembolso1.setImporteSolicitado(dto.getDesembolso());
				desembolso1.setImporteGastos(dto.getGastos());
				bp.saveOrUpdate(desembolso1);
				HashMap<String, Object> erroresSolicitud = DesembolsoHelper.solicitar(desembolso1);
				if (erroresSolicitud.isEmpty()) {
					bp.save(lote);
					desembolso1.setLote(lote);
					bp.update(desembolso1);
					if (credito.getLinea().isLineaCosecha() || credito.getLinea().isLineaCosechaPreAprobada()) {
						solicitarInmovilizacion(dto);
					}
				} else {
					errores.putAll(erroresSolicitud);
				}
			}
		}
		if (errores.isEmpty()) {
//			solicitarInmovilizacion();
			ejecutado = true;
		}
		return errores.isEmpty();
	}

	public void ejecutar() {
		// estos dos campos se setean en 0 hasta definir
		importeGastosIngresado = 0.0;
		importeMultasIngresado = 0.0;
		if (importeGastosCosecha == null || importeGastosCosecha.isNaN()) {
			errores.put("ejecucionDesembolsoMasivo.importeGastos", "ejecucionDesembolsoMasivo.importeGastos");
			return;
		}
		if (importeGastosIngresado == null || importeGastosIngresado.isNaN()) {
			errores.put("ejecucionDesembolsoMasivo.importeMultas", "ejecucionDesembolsoMasivo.importeMultas");
			return;
		}
		HashSet<Long> seleccionados = getSeleccionados();
		if (seleccionados.isEmpty()) {
			errores.put("ejecucionDesembolsoMasivo.seleccion", "ejecucionDesembolsoMasivo.seleccion");
			primeraEtapa = false;
			return;
		}
		bp.begin();
		Iterator<ObjetoiDTO> iteratorCreditos = creditos.iterator();
		while (iteratorCreditos.hasNext()) {
			ObjetoiDTO dto = iteratorCreditos.next();
			if (seleccionados.contains(dto.getIdObjetoi())) {
				Objetoi credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
				Desembolso desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
						.setLong("idCredito", credito.getId()).setInteger("numero", 1).uniqueResult();
				this.ejecutarDesembolso1(desembolso1, desembolso1.getImporte(), new Date(), null);
			}
		}
		if (errores.isEmpty()) {
			bp.commit();
			ejecutado = true;
		} else {
			bp.rollback();
		}
	}

	@SuppressWarnings("unchecked")
	public void ejecutarDesembolso1(Desembolso desembolso, Double total, Date fechaEjecucion, Integer numeroRecibo) {
		if (desembolso.getNroOrden() == null || desembolso.getNroOrden().longValue() <= 0) {
			return;
		}
		CreditoHandler creditoHandler = new CreditoHandler();
		desembolso.setEstado("2"); // Realizado
		desembolso.setFechaReal(fechaEjecucion);
		desembolso.setImporteReal(total);
		desembolso.setNumeroRecibo(numeroRecibo);
		if (desembolso.getImporteGastos() == null) {
			desembolso.setImporteGastos(new Double(0));
		}
		desembolso.setImporteMultas(desembolso.getImporteMultas());
		if (desembolso.getImporteMultas() == null) {
			desembolso.setImporteMultas(new Double(0));
		}
		Objetoi credito = desembolso.getCredito();
		if (faltanDatosFinancieros(credito)) {
			return;
		}
		creditoHandler.ejecutarDesembolso(credito, desembolso);

		Estado estado = (Estado) bp.getNamedQuery("Estado.findByNombreEstado").setString("nombreEstado", "EJECUCION")
				.uniqueResult();
		if (!credito.getEstadoActual().getEstado().getNombreEstado().equalsIgnoreCase("EJECUCION")) {
			ObjetoiEstado anterior = credito.getEstadoActual();
			anterior.setFechaHasta(new Date());
			bp.update(anterior);
			ObjetoiEstado nuevo = new ObjetoiEstado();
			nuevo.setObjetoi(credito);
			nuevo.setEstado(estado);
			nuevo.setFecha(new Date());
			bp.save(nuevo);
		}

		// Gastos a recuperar - NC
		// solo se impacta cuando la moneda es Pesos
		if (credito.getLinea().getMoneda_id() != null && credito.getLinea().getMoneda_id().longValue() == 1L) {
			List<NovedadCtaCte> detalles = bp.createQuery(
					"SELECT n FROM NovedadCtaCte n LEFT JOIN n.detalleFactura d LEFT JOIN d.factura f WHERE n.credito.id = :id AND (d is null or f.fechaPago is not null)")
					.setParameter("id", credito.getId()).list();

			// reutilizo metodos para gastos a recuperar
			CreditoDesembolsos creditoDesembolsos = new CreditoDesembolsos();
			creditoDesembolsos.setIdDesembolso(desembolso.getId());
			creditoDesembolsos.setIdObjetoi(credito.getId());
			creditoDesembolsos.setDesembolso(desembolso);
			creditoDesembolsos.setObjetoi(desembolso.getCredito());
			Usuario usuario = (Usuario) bp.getById(Usuario.class,
					SessionHandler.getCurrentSessionHandler().getCurrentUser());
			creditoDesembolsos.setUsuario(usuario);
			for (NovedadCtaCte detalleFactura : detalles) {
				creditoDesembolsos.crearPagos(detalleFactura, desembolso);
			}
		}
		boolean esCosecha = credito.getLinea().isLineaCosecha();
		if (esCosecha) {
			creditoHandler.comprobarInmovilizacion(credito.getId());
		}
		ejecucion2 = true;
		primeraEtapa = true;
	}

	@SuppressWarnings("unchecked")
	public Double calcularTasaInteresCompensatorio(Objetoi credito)
			throws NumberFormatException, HibernateException, Exception {
		List<CosechaConfig> list = bp.getNamedQuery("CosechaConfig.findByFecha")
				.setParameter("fecha", credito.getFechaSolicitud()).setParameter("varietales", credito.getVarietales())
				.list();

		if (list.isEmpty()) {
			errores.put("ejecucionDesembolsosMasivos.tasaFinal.cosechaConfig",
					"ejecucionDesembolsosMasivos.tasaFinal.cosechaConfig");
			return null;
		}
		CosechaConfig cosecha = list.get(0);

		Garantia garantia;
		List<Garantia> garantias = bp.getNamedQuery("Garantia.findByCreditoYValorMayor")
				.setParameter("credito", credito).list();
		if (garantias.isEmpty()) {
			errores.put("ejecucionDesembolsosMasivos.tasaFinal.garantia",
					"ejecucionDesembolsosMasivos.tasaFinal.garantia");
			return null;
		} else {
			garantia = garantias.get(0);
		}

		PeriodoCosechaConfig periodoSinGarantia = null;
		PeriodoCosechaConfig periodo = null;

		List<PeriodoCosechaConfig> periodos = bp
				.createQuery("select p from PeriodoCosechaConfig p where "
						+ "p.cosechaConfig.id = :idCosecha and p.fechaDesde <= :fecha and p.fechaHasta >= :fecha")
				.setParameter("fecha", credito.getFechaSolicitud()).setParameter("idCosecha", cosecha.getId()).list();

		for (PeriodoCosechaConfig p : periodos) {
			if (p.getTipoGarantia_id() == null || p.getTipoGarantia_id() == 0) {
				periodoSinGarantia = p;
				continue;
			}
			if (p.getTipoGarantia_id().equals(garantia.getTipoId())
					&& (garantia.getTipoProducto() == null || p.getTipoProducto().equals(garantia.getTipoProducto()))) {
				periodo = p;
				break;
			}
		}

		if (periodo == null && periodoSinGarantia == null) {
			errores.put("Solicitud.periodoCosecha", "Solicitud.periodoCosecha");

			return null;
		}

		if (periodo != null) {
			return periodo.calcularTasaFinalGar(new Date());
		} else {
			return periodoSinGarantia.calcularTasaFinalGar(new Date());
		}
	}

	public void calcular2() {

		primeraEtapa = false;
		ejecucion2 = true;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosEjecucionDesembolsosMasivos", creditos);
	}

	public void setearTipoDesembolso(ObjetoiDTO dto) {
		Objetoi credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
		logger.log("debug", "Calculo Tipo Desembolso " + credito.getNumeroAtencionStr());

		if (credito.getQqFinal() == null) {
			credito.setQqFinal(0.0);
		}
		Float porcCosecha = credito.getPorcentajeCosecha();

		double financiado = credito.getQqFinal();
		double ingresadoTotal = credito.getQqIngresadosTotales();
		double ingresadoBodega = credito.getQqIngresadosGarantia();

		boolean condicion1 = ingresadoTotal >= (financiado * 70 / 100);
		boolean condicion2 = (credito.getTipoGarantia() != 2)
				|| (credito.getTipoGarantia() == 2 && ingresadoBodega >= (financiado * porcCosecha / 100));

		Desembolso desembolso1 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", credito.getId()).setInteger("numero", 1).uniqueResult();
		Desembolso desembolso2 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
				.setLong("idCredito", credito.getId()).setInteger("numero", 2).uniqueResult();

		CosechaConfig cosechaConfig = (CosechaConfig) bp.getNamedQuery("CosechaConfig.findByTemporadaMayor")
				.setParameter("varietales", credito.getVarietales()).list().get(0);

		logger.log("debug",
				"Credito: " + credito.getId() + "-" + credito.getNumeroAtencionStr() + ", Condicion1: " + condicion1
						+ ", Condicion2: " + condicion2 + ", Financiado: " + financiado + ", Ingreso Total: "
						+ ingresadoTotal + ", Ingreso Bodega: " + ingresadoBodega);

		if (condicion1 && condicion2) {
			dto.setTipoDesembolso("Total");
			if (credito.getDestino() == null) {
				// error, no desembolsa
				dto.setTipoDesembolso("No desembolsa");
			} else {
				dto.setNroOrden(desembolso2.getNroOrden());
				dto.setDesembolso(desembolso2.getImporte());
			}
		} else {
			dto.setTipoDesembolso("Parcial");
			if (credito.getDestino() == null) {
				// error, no desembolsa
				dto.setTipoDesembolso("No desembolsa - Destino no definido");
			} else if (credito.getDestino().equals("CRDC")) { // cosecha
				dto.setNroOrden(desembolso1.getNroOrden());
				dto.setDesembolso((ingresadoTotal * cosechaConfig.getCostoQQCosecha()) - desembolso1.getImporteReal());
			} else if (credito.getDestino().equals("CRDA")) { // elaboracion
				dto.setNroOrden(desembolso1.getNroOrden());
				dto.setDesembolso((ingresadoTotal * cosechaConfig.getCostoQQElab()) - desembolso1.getImporteReal());
			}
			if (dto.getDesembolso() <= 0) {
				dto.setTipoDesembolso("No desembolsa - Monto <= 0");
			}
		}
		if (faltanDatosFinancieros(credito)) {
			dto.setTipoDesembolso("Faltan Datos Financieros");
		}

	}

	@SuppressWarnings("static-access")
	public void imprimir() {
		try {
			Integer modeloReporte = accion.equalsIgnoreCase("imprimirReporte") ? 1
					: (accion.equalsIgnoreCase("imprimirVolante") ? 2 : 0);
			reporte = new ReportResult();
			reporte.setExport("PDF");
			reporte.setReportName("solicitudDesembolsoMasivo.jasper");
			reporte.setParams("SCHEMA=" + SessionHandler.getCurrentSessionHandler().getBusinessPersistance().getSchema()
					+ ";idLote=" + getNroLoteElegido() + ";modeloReporte=" + modeloReporte);
		} catch (Exception e) {
			errores.put("imprimir.informe.error",
					new ActionMessage("imprimir.informe.error", "solicitudDesembolsoMasivo"));
		}
	}

	@SuppressWarnings("unchecked")
	public boolean solicitar2() {
		// Provisorio hasta definir
		importeGastosIngresado = 0.0;
		importeMultasIngresado = 0.0;
		if (importeGastosIngresado == null || importeGastosIngresado.isNaN()) {
			errores.put("ejecucionDesembolsoMasivo.importeGastos", "ejecucionDesembolsoMasivo.importeGastos");
			return false;
		}
		if (importeGastosIngresado == null || importeGastosIngresado.isNaN()) {
			errores.put("ejecucionDesembolsoMasivo.importeMultas", "ejecucionDesembolsoMasivo.importeMultas");
			return false;
		}
		HashSet<Long> seleccionados = getSeleccionados();

		if (seleccionados.isEmpty()) {
			errores.put("ejecucionDesembolsoMasivo.seleccion", "ejecucionDesembolsoMasivo.seleccion");
			return false;
		}

		// Busco el numero de lote correspondiente de acuerdo a la fecha
		LoteDesembolso lote = new LoteDesembolso();
		try {
			setFechaLote(new Date());
			List<LoteDesembolso> lotes = bp.createQuery("Select l from LoteDesembolso l where l.fecha = :fecha")
					.setParameter("fecha", getFechaLote()).list();
			Date fechaHoy = new Date();
			if (lotes.size() == 0) {
				lote.setFecha(fechaHoy);
				lote.setNroLote(1L);
			} else {
				LoteDesembolso ultimoLote = lotes.get(lotes.size() - 1);
				lote.setFecha(ultimoLote.getFecha());
				lote.setNroLote(ultimoLote.getNroLote() + 1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean loteGuardardo = false;
		Iterator<ObjetoiDTO> iteratorCreditos = creditos.iterator();
		while (iteratorCreditos.hasNext()) {
			ObjetoiDTO dto = iteratorCreditos.next();
			if (seleccionados.contains(dto.getIdObjetoi())) {
				setearTipoDesembolso(dto);
				Objetoi credito = (Objetoi) bp.getById(Objetoi.class, dto.getIdObjetoi());
				if ((dto.getTipoDesembolso().equals("Parcial") || dto.getTipoDesembolso().equals("Total"))
						&& credito.findTasaCompensatorio() != null) {
					if (!credito.validarRegistroGarantiaDesembolso()) {
						errores.put("desembolso.garantiaInscripcion", "desembolso.garantiaInscripcion");
						continue;
					}
					Desembolso desembolso2 = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
							.setLong("idCredito", credito.getId()).setInteger("numero", 2).uniqueResult();
					desembolso2.setImporteSolicitado(dto.getDesembolso());
					bp.saveOrUpdate(desembolso2);

					HashMap<String, Object> erroresSolicitud = DesembolsoHelper.solicitar(desembolso2);
					if (erroresSolicitud.isEmpty()) {
						if (!loteGuardardo) {
							bp.save(lote);
							loteGuardardo = true;
						}
						desembolso2.setLote(lote);
						bp.saveOrUpdate(desembolso2);
					} else {
						errores.putAll(erroresSolicitud);
					}

					iteratorCreditos.remove();
				} else if (dto.getTipoDesembolso().equalsIgnoreCase("No Desembolsa")) {
					errores.put("ejecucionDesembolsosMasivos.QQ", "ejecucionDesembolsosMasivos.QQ");
				} else if (dto.getTipoDesembolso().equals("Faltan Datos Financieros")) {
					faltanDatosFinancieros(credito);
				}
			}
		}
		if (errores.isEmpty()) {
			ejecutado = true;
		}
		primeraEtapa = false;
		ejecucion2 = true;
		SessionHandler.getCurrentSessionHandler().getRequest().getSession()
				.setAttribute("creditosEjecucionDesembolsosMasivos", creditos);
		return errores.isEmpty();
	}

	public void ejecutarDesembolso2(Desembolso desembolso2, Double total, Date fechaEjecucion, Integer numeroRecibo) {
		CreditoHandler creditoHandler = new CreditoHandler();
		Objetoi credito = desembolso2.getCredito();
		ObjetoiDTO dto = new ObjetoiDTO(credito);
		this.setearTipoDesembolso(dto);
		if (dto.getTipoDesembolso().equals("Parcial")) {
			desembolso2.setEstado("5");// Parcial
		} else {
			desembolso2.setEstado("2"); // Realizado
		}
		try {
			desembolso2.setFechaReal(fechaEjecucion);
			desembolso2.setImporteReal(total);
			desembolso2.setNumeroRecibo(numeroRecibo);
			if (desembolso2.getImporteGastos() == null) {
				desembolso2.setImporteGastos(new Double(0));
			}
			desembolso2.setImporteMultas(desembolso2.getImporteMultas());
			if (desembolso2.getImporteMultas() == null) {
				desembolso2.setImporteMultas(new Double(0));
			}
			creditoHandler.ejecutarDesembolso(credito, desembolso2);

			Auditar(desembolso2);

			if (dto.getTipoDesembolso().equals("Parcial")) {
				double liberarVolumen = credito.getDepositoADevolver();
				if (liberarVolumen > 0) {
					String producto = (String) bp.createSQLQuery(
							"select distinct g.tipoProducto from ObjetoiGarantia og join Garantia g on og.garantia_id = g.id where og.objetoi_id = :idCredito and g.tipoProducto is not null")
							.setParameter("idCredito", credito.getId()).setMaxResults(1).uniqueResult();
					Liberaciones lib = new Liberaciones();
					lib.setEstado(EstadoLiberacion.PENDIENTE);
					lib.setFecha(new Date());
					lib.setObjetoi(credito);
					lib.setProducto(producto);
					lib.setVolumen(liberarVolumen);
					bp.save(lib);
				}
				credito.setLiberar(false);
				bp.update(credito);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void Auditar(Desembolso desembolso) {
		if (desembolso.getNumero() == 2 && desembolso.getCredito().getLinea().isLineaCosecha()) {
			AuditoriaFinal auditoriafina = new AuditoriaFinal();
			auditoriafina.setFechaProceso(new Date());
			auditoriafina.setCredito(desembolso.getCredito());
			auditoriafina.setObservaciones("Carga automática - pago 2º desembolso");
			auditoriafina.setAplicaFondo("si");
			bp.save(auditoriafina);
		}
	}

	@SuppressWarnings("unchecked")
	public boolean faltanDatosFinancieros(Objetoi credito) {
		int cantidadErrores = errores.size();
		if (credito.getTipoAmortizacion() == null) {
			errores.put("desembolsosEjecucion.tipoAmortizacionNulo", "desembolsosEjecucion.tipoAmortizacionNulo");
		}
		List<ObjetoiIndice> listaIndice = bp.createQuery("SELECT o FROM ObjetoiIndice o WHERE o.credito = :credito")
				.setParameter("credito", credito).list();
		if (listaIndice == null || listaIndice.isEmpty()) {
			errores.put("desembolsosEjecucion.tazaInteresCompensatorioNulo",
					"desembolsosEjecucion.tazaInteresCompensatorioNulo");
		}
		if (credito.getPrimerVencCapital() == null) {
			errores.put("desembolsosEjecucion.primerVencimientoCapitalNulo",
					"desembolsosEjecucion.primerVencimientoCapitalNulo");
		}
		if (credito.getPlazoCapital() == null || credito.getPlazoCapital().intValue() == 0) {
			errores.put("desembolsosEjecucion.cantidadCuotasCampitalNulo",
					"desembolsosEjecucion.cantidadCuotasCampitalNulo");
		}
		if (credito.getPrimerVencInteres() == null) {
			errores.put("desembolsosEjecucion.primerVencimientoInteresNulo",
					"desembolsosEjecucion.primerVencimientoInteresNulo");
		}
		if (credito.getPlazoCompensatorio() == null || credito.getPlazoCompensatorio().intValue() == 0) {
			errores.put("desembolsosEjecucion.cantidadCuotasInteresNulo",
					"desembolsosEjecucion.cantidadCuotasInteresNulo");
		}
		if (credito.getFrecuenciaCapital() == null) {
			errores.put("desembolsosEjecucion.periocidadCuotasCapitalNulo",
					"desembolsosEjecucion.periocidadCuotasCapitalNulo");
		}
		if (credito.getFrecuenciaInteres() == null) {
			errores.put("desembolsosEjecucion.periocidadCuotasInteresNulo",
					"desembolsosEjecucion.periocidadCuotasInteresNulo");
		}
		if (credito.getLinea().getMoneda() == null) {
			errores.put("desembolsosEjecucion.monedaNulo", "desembolsosEjecucion.monedaNulo");
		}
		if (errores.size() > cantidadErrores)
			return true;
		else
			return false;
	}

	@SuppressWarnings({ "rawtypes" })
	public boolean verificarPermisoSolicitarGAF() {
		Menu menu = (Menu) bp
				.createSQLQuery(
						"SELECT * FROM MENU WHERE RUTA = '/services/Imputaciones/imputarDesembolso' AND IDMODULO = 5")
				.addEntity(Menu.class).setMaxResults(1).uniqueResult();
		if (menu == null) {
			return false;
		}
		String sUsuarioLogeado = SessionHandler.getCurrentSessionHandler().getCurrentUser();
		int tipo = 2;
		if (menu.getTipo().intValue() == 2) {
			// Tipo 2=No requiere permiso
			if (bp.getCurrentSession().contains(menu)) {
				bp.getCurrentSession().evict(menu);
			}
			return true;
		}
		if (bp.getCurrentSession().contains(menu)) {
			bp.getCurrentSession().evict(menu);
		}
		// Tengo los datos, consulto por los permisos del usuario
		List list = bp.getByFilter("FROM Permiso p WHERE p.id.causerk='" + sUsuarioLogeado + "' AND p.id.idmenu= '"
				+ menu.getId().toString() + "'");
		if (list.size() == 1) {// Si aca define un permiso, es el que va
			Permiso permiso = (Permiso) list.get(0);
			bp.getCurrentSession().evict(permiso);
			if (permiso.getTipo() != null && permiso.getTipo().intValue() >= tipo) {
				return true;
			} else {
				return false;
			}
		}
		// si no tiene permiso, me fijo si el perfil lo tiene
		try {
			Claves usuario = (Claves) bp.getById(Claves.class, sUsuarioLogeado);
			if (usuario.getPerfilK() == null) {
				return false;
			}
			Integer cant = new Integer(bp
					.getCurrentSession().createQuery("SELECT p.tipo FROM Permiso p WHERE p.id.causerk='"
							+ usuario.getPerfilK() + "' AND p.id.idmenu='" + menu.getId().toString() + "'")
					.uniqueResult().toString());
			return cant.intValue() >= tipo;
		} catch (Exception e) {
			// puede entrar por objectnot found o por nullpointerexception
			return false;
		}
	}

	// ======================GETTERS Y SETTERS======================
	public HashMap<String, Object> getErrors() {
		return this.errores;
	}

	public String getForward() {
		return this.forward;
	}

	public String getInput() {
		return this.forward;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getAccion() {
		return this.accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public List<ObjetoiDTO> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<ObjetoiDTO> creditos) {
		this.creditos = creditos;
	}

	public Long getIdObjetoi() {
		return idObjetoi;
	}

	public void setIdObjetoi(Long idObjetoi) {
		this.idObjetoi = idObjetoi;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public boolean isPrimeraEtapa() {
		return primeraEtapa;
	}

	public void setPrimeraEtapa(boolean primeraEtapa) {
		this.primeraEtapa = primeraEtapa;
	}

	public boolean isEjecucion2() {
		return ejecucion2;
	}

	public void setEjecucion2(boolean ejecucion2) {
		this.ejecucion2 = ejecucion2;
	}

	public Double getImporteGastosIngresado() {
		return importeGastosIngresado;
	}

	public void setImporteGastosIngresado(Double importeGastosIngresado) {
		this.importeGastosIngresado = importeGastosIngresado;
	}

	public Double getImporteMultasIngresado() {
		return importeMultasIngresado;
	}

	public void setImporteMultasIngresado(Double importeMultasIngresado) {
		this.importeMultasIngresado = importeMultasIngresado;
	}

	public boolean isEjecutado() {
		return ejecutado;
	}

	public void setEjecutado(boolean ejecutado) {
		this.ejecutado = ejecutado;
	}

	public boolean isOcultarGastosMultas() {
		return ocultarGastosMultas;
	}

	public void setOcultarGastosMultas(boolean ocultarGastosMultas) {
		this.ocultarGastosMultas = ocultarGastosMultas;
	}

	public Double getImporteGastosCosecha() {
		return importeGastosCosecha;
	}

	public void setImporteGastosCosecha(Double importeGastosCosecha) {
		this.importeGastosCosecha = importeGastosCosecha;
	}

	// ========================VALIDACIONES=========================
	public boolean validate() {
		return this.errores.isEmpty();
	}

	@Override
	public Object getResult() {
		return reporte;
	}

	public String getFechaActual() {
		return FORMAT.format(fechaLote);
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getNroLoteElegido() {
		return nroLoteElegido;
	}

	public void setNroLoteElegido(String nroLoteElegido) {
		this.nroLoteElegido = nroLoteElegido;
	}

	public Date getFechaLote() {
		return fechaLote;
	}

	public void setFechaLote(Date fechaLote) {
		this.fechaLote = fechaLote;
	}

	public String getFechaLoteStr() {
		return DateHelper.getString(getFechaLote());
	}

	public void setFechaLoteStr(String date) {
		setFechaLote(DateHelper.getDate(date));
	}

	public Long[] getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(Long[] seleccion) {
		this.seleccion = seleccion;
	}

	public List<Linea> getLineas() {
		return lineas;
	}
}