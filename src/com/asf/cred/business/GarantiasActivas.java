package com.asf.cred.business;

import java.io.Serializable;
import java.util.List;

import com.asf.security.BusinessPersistance;
import com.nirven.creditos.hibernate.ComportamientoGarantiaEnum;
import com.nirven.creditos.hibernate.GarantiaEstado;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class GarantiasActivas implements Serializable {

	private static final long serialVersionUID = 7946329452841252833L;

	private BusinessPersistance bp;

	public GarantiasActivas(BusinessPersistance bp) {
		this.bp = bp;
	}

	@SuppressWarnings("unchecked")
	public void ejecutar() {
		List<Number> ids = bp.createSQLQuery("select og.id from ObjetoiGarantia og "
				+ "join Objetoi o on og.objetoi_id = o.id join ObjetoiEstado e on e.objetoi_id = o.id "
				+ "where e.estado_idEstado = 8 and e.fechaHasta is null "
				+ "and not exists (select ge.* from GarantiaEstado ge where og.id = ge.objetoiGarantia_id and ge.estado = 'ACTIVA' and ge.importe >= 0)")
				.list();

		if (ids != null && ids.size() > 0) {
			for (Number id : ids) {
				ObjetoiGarantia og = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, id.longValue());
				if (og != null) {

					GarantiaEstado estadoNuevo = new GarantiaEstado(ComportamientoGarantiaEnum.ACTIVA,
							og.getObjetoi().getFechaSolicitud(), og.getValor());
					estadoNuevo.setObjetoiGarantia(og);
					bp.save(estadoNuevo);
				}
			}
		}
	}
}
