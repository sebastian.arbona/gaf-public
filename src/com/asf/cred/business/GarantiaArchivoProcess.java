package com.asf.cred.business;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.nirven.creditos.hibernate.GarantiaArchivo;
import com.nirven.creditos.hibernate.ObjetoiGarantia;

public class GarantiaArchivoProcess extends ConversationProcess {

	@Save
	private Long idObjetoiGarantia;
	private List<GarantiaArchivo> archivos;
	private FormFile formFile;
	private String detalle;
	private Long tipoId;
	private String forwardURL;
	private Long idArchivo;

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean listar() {
		ObjetoiGarantia objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);

		archivos = bp
				.createQuery("select a from GarantiaArchivo a where a.garantia.id = :id order by a.fechaAdjunto asc")
				.setParameter("id", objetoiGarantia.getGarantia().getId()).list();

		return true;
	}

	@ProcessMethod
	public boolean nuevo() {
		forward = "GarantiaArchivo";
		return true;
	}

	@ProcessMethod
	public boolean guardar() {
		try {
			if (formFile == null || formFile.getFileSize() == 0) {
				errors.put("garantiaArchivo.vacio", "garantiaArchivo.vacio");
				return false;
			}

			ObjetoiGarantia objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);

			GarantiaArchivo g = new GarantiaArchivo();
			g.setDetalle(detalle);
			g.setFechaAdjunto(new Date());
			g.setGarantia(objetoiGarantia.getGarantia());
			g.setNoBorrar(false);
			g.setTipoId(tipoId);

			g.setArchivo(formFile.getFileData());
			g.setMimetype(formFile.getContentType());
			g.setNombre(formFile.getFileName());

			g.setCredito(null);

			g.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());

			bp.save(g);

			listar();

			return true;
		} catch (IOException e) {
			e.printStackTrace();
			errors.put("garantiaArchivo.error", "garantiaArchivo.error");
			return false;
		}
	}

	@ProcessMethod
	public boolean eliminar() {
		GarantiaArchivo g = (GarantiaArchivo) bp.getById(GarantiaArchivo.class, idArchivo);
		bp.delete(g);

		listar();

		return true;
	}

	@ProcessMethod
	public boolean cancelar() {
		return true;
	}

	@ProcessMethod
	public boolean volver() {
		ObjetoiGarantia objetoiGarantia = (ObjetoiGarantia) bp.getById(ObjetoiGarantia.class, idObjetoiGarantia);

		forward = "ProcessRedirect";
		forwardURL = "/actions/process.do?processName=GarantiaProcess&do=process" + "&process.idSolicitud="
				+ objetoiGarantia.getObjetoi().getId();
		return true;
	}

	public String getForwardURL() {
		return forwardURL;
	}

	public List<GarantiaArchivo> getArchivos() {
		return archivos;
	}

	public Long getIdObjetoiGarantia() {
		return idObjetoiGarantia;
	}

	public void setIdObjetoiGarantia(Long idObjetoiGarantia) {
		this.idObjetoiGarantia = idObjetoiGarantia;
	}

	public FormFile getFormFile() {
		return formFile;
	}

	public void setFormFile(FormFile formFile) {
		this.formFile = formFile;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Long getTipoId() {
		return tipoId;
	}

	public void setTipoId(Long tipoId) {
		this.tipoId = tipoId;
	}

	public Long getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(Long idArchivo) {
		this.idArchivo = idArchivo;
	}

	@Override
	protected String getDefaultForward() {
		return "GarantiaArchivoList";
	}

}
