package com.asf.cred.business;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;

public class CuentaCorrienteAmpliadaBean implements Serializable {
	
	private static final long serialVersionUID = -2965742589113879243L;
	
	private static DecimalFormat format = new DecimalFormat("#,##0.00;-#,##0.00");
	
	private Date fechaGeneracion;
	private Long asiento;
	private Long idBoleto;
	private String tipoBoleto;
	private Long numeroBoleto;
	private String detalle;
	private double capital;
	private double compensatorio;
	private double moratorio;
	private double punitorio;
	private double gastos;
	private double iva;
	private double cer;
	private double debe;
	private double haber;
	private double saldo;
	private Long periodoCtacte;
    private Long movimientoCtacte;
    private Long verificadorCtacte;
    private Long itemCtacte;
    
    private Integer cuota;
    private Date fechaVencimiento;
    private String estado;
    
    private Long idCaratula;
    private Long idBoletoRecibo;
    
    private Long idOrdenPago;
    private Long numeroOrdenPago;
    private Long ejercicioOrdenPago;
    private Long idObjetoi;
    private Date fechaProceso;
    
    private double cotizaMov;
    private double debePesos;
    private double haberPesos;
    private double saldoPesos;

	private boolean diferenciaCotizacion;

	private String idCtacte;
    
    public CuentaCorrienteAmpliadaBean() {
	}
    
	public CuentaCorrienteAmpliadaBean(Object[] r) {
		this(r, false);
	}
	
	public CuentaCorrienteAmpliadaBean(Object[] r, boolean cuota) {
		if (!cuota) {
			this.fechaGeneracion = (Date) r[0];
			Number periodo = (Number) r[1];
			this.periodoCtacte = periodo != null ? periodo.longValue() : null;
			Number mc = (Number) r[2];
			this.movimientoCtacte = mc != null ? mc.longValue() : null;
			Number vc = (Number) r[3];
			this.verificadorCtacte = vc != null ? vc.longValue() : null;
			Number ic = (Number) r[4];
			this.itemCtacte = ic != null ? ic.longValue() : null;
			this.tipoBoleto = (String) r[5];
			Number nroBol = (Number) r[6];
			this.numeroBoleto = nroBol != null ? nroBol.longValue() : null;
			Number idBol = (Number) r[7];
			this.idBoleto = idBol != null ? idBol.longValue() : null;
			this.detalle = (String) r[8];
			
			Double cap = (Double) r[9];
			this.capital = cap != null ? cap : 0;
			Double comp = (Double) r[10];
			this.compensatorio = comp != null ? comp : 0;
			Double mor = (Double) r[11];
			this.moratorio = mor != null ? mor : 0;
			Double pun = (Double) r[12];
			this.punitorio = pun != null ? pun : 0;
			Double gas = (Double) r[13];
			this.gastos = gas != null ? gas: 0;
			Double cer = (Double) r[25];
			this.cer = cer != null ? cer: 0;
			
			Double d = (Double) r[14];
			this.debe = d != null ? d : 0;
			Double h = (Double) r[15];
			this.haber = h != null ? h : 0;
			
			Number car = (Number) r[16];
			this.idCaratula = car != null ? car.longValue() : 0;
			Number recibo = (Number) r[17];
			this.idBoletoRecibo = recibo != null ? recibo.longValue() : 0;
			this.fechaVencimiento = (Date) r[18];
			Number idObjetoi = (Number)r[19];
			this.idObjetoi = idObjetoi != null ? idObjetoi.longValue() : null;
			this.fechaProceso = (Date) r[20];
			
			Double cot = (Double) r[21];
			this.cotizaMov = cot != null ? cot : 0;
			Double dp = (Double) r[22];
			this.debePesos = dp != null ? dp : 0;
			Double hp = (Double) r[23];
			this.haberPesos = hp != null ? hp : 0;
			
			this.idCtacte = (String) r[24];
		} else {
			this.fechaGeneracion = (Date) r[0];
			Number periodo = (Number) r[1];
			this.periodoCtacte = periodo != null ? periodo.longValue() : null;
			Number mc = (Number) r[2];
			this.movimientoCtacte = mc != null ? mc.longValue() : null;
			Number vc = (Number) r[3];
			this.verificadorCtacte = vc != null ? vc.longValue() : null;
			Number ic = (Number) r[4];
			this.itemCtacte = ic != null ? ic.longValue() : null;
			this.tipoBoleto = (String) r[5];
			Number nroBol = (Number) r[6];
			this.numeroBoleto = nroBol != null ? nroBol.longValue() : null;
			Number idBol = (Number) r[7];
			this.idBoleto = idBol != null ? idBol.longValue() : null;
			this.detalle = (String) r[8];
			
			Double cap = (Double) r[9];
			this.capital = cap != null ? cap : 0;
			Double comp = (Double) r[10];
			this.compensatorio = comp != null ? comp : 0;
			Double mor = (Double) r[11];
			this.moratorio = mor != null ? mor : 0;
			Double pun = (Double) r[12];
			this.punitorio = pun != null ? pun : 0;
			Double gas = (Double) r[13];
			this.gastos = gas != null ? gas: 0;
			Double cer = (Double) r[23];
			this.cer = cer != null ? cer : 0;
			
			this.cuota = (Integer) r[14];
			this.fechaVencimiento = (Date) r[15];
			this.estado = (String) r[16];
			
			Number car = (Number) r[18];
			this.idCaratula = car != null ? car.longValue() : 0;
			Number recibo = (Number) r[19];
			this.idBoletoRecibo = recibo != null ? recibo.longValue() : 0;
			Number idObjetoi = (Number)r[20];
			this.idObjetoi = idObjetoi != null ? idObjetoi.longValue() : null;
			this.fechaProceso = (Date) r[21];
			
			this.idCtacte = (String) r[22];
		}
	}
	
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}
	public String getFechaGeneracionStr() {
		return DateHelper.getString(fechaGeneracion);
	}
	public Long getAsiento() {
		return asiento;
	}
	public void setAsiento(Long asiento) {
		this.asiento = asiento;
	}
	public Long getIdBoleto() {
		return idBoleto;
	}
	public void setIdBoleto(Long idBoleto) {
		this.idBoleto = idBoleto;
	}
	public String getTipoBoleto() {
		return tipoBoleto;
	}
	public void setTipoBoleto(String tipoBoleto) {
		this.tipoBoleto = tipoBoleto;
	}
	public Long getNumeroBoleto() {
		return numeroBoleto;
	}
	public void setNumeroBoleto(Long numeroBoleto) {
		this.numeroBoleto = numeroBoleto;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public double getDebe() {
		return debe;
	}
	public void setDebe(double debe) {
		this.debe = debe;
	}
	public double getHaber() {
		return haber;
	}
	public void setHaber(double haber) {
		this.haber = haber;
	}
	public double getSaldo() {
		return saldo;
	}
	public String getSaldoStr() {
		return format.format(saldo);
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public Long getPeriodoCtacte() {
		return periodoCtacte;
	}
	public void setPeriodoCtacte(Long periodoCtacte) {
		this.periodoCtacte = periodoCtacte;
	}
	public Long getMovimientoCtacte() {
		return movimientoCtacte;
	}
	public void setMovimientoCtacte(Long movimientoCtacte) {
		this.movimientoCtacte = movimientoCtacte;
	}
	public Long getVerificadorCtacte() {
		return verificadorCtacte;
	}
	public void setVerificadorCtacte(Long verificadorCtacte) {
		this.verificadorCtacte = verificadorCtacte;
	}
	public Long getItemCtacte() {
		return itemCtacte;
	}
	public void setItemCtacte(Long itemCtacte) {
		this.itemCtacte = itemCtacte;
	}
	public double getCapital() {
		return capital;
	}
	public String getCapitalStr() {
		return format.format(capital);
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	public double getCompensatorio() {
		return compensatorio;
	}
	public String getCompensatorioStr() {
		return format.format(compensatorio);
	}
	public void setCompensatorio(double compensatorio) {
		this.compensatorio = compensatorio;
	}
	public double getMoratorio() {
		return moratorio;
	}
	public String getMoratorioStr() {
		return format.format(moratorio);
	}
	public void setMoratorio(double moratorio) {
		this.moratorio = moratorio;
	}
	public double getPunitorio() {
		return punitorio;
	}
	public String getPunitorioStr() {
		return format.format(punitorio);
	}
	public void setPunitorio(double punitorio) {
		this.punitorio = punitorio;
	}
	public double getGastos() {
		return gastos;
	}
	public String getGastosStr() {
		return format.format(gastos);
	}
	public void setGastos(double gastos) {
		this.gastos = gastos;
	}
	public double getCer() {
		return cer;
	}
	public String getCerStr() {
		return format.format(cer);
	}
	public void setCer(double cer) {
		this.cer = cer;
	}
	public Integer getCuota() {
		return cuota;
	}
	public void setCuota(Integer cuota) {
		this.cuota = cuota;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public String getFechaVencimientoStr() {
		return DateHelper.getString(fechaVencimiento);
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public Date getFechaProceso() {
		return fechaProceso;
	}
	public String getFechaProcesoStr() {
		return DateHelper.getString(fechaProceso);
	}
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Long getIdCaratula() {
		return idCaratula;
	}
	public void setIdCaratula(Long idCaratula) {
		this.idCaratula = idCaratula;
	}
	public Long getIdBoletoRecibo() {
		return idBoletoRecibo;
	}
	public void setIdBoletoRecibo(Long idBoletoRecibo) {
		this.idBoletoRecibo = idBoletoRecibo;
	}
	public Long getIdOrdenPago() {
		return idOrdenPago;
	}
	public void setIdOrdenPago(Long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}
	public Long getNumeroOrdenPago() {
		return numeroOrdenPago;
	}
	public void setNumeroOrdenPago(Long numeroOrdenPago) {
		this.numeroOrdenPago = numeroOrdenPago;
	}
	public Long getEjercicioOrdenPago() {
		return ejercicioOrdenPago;
	}
	public void setEjercicioOrdenPago(Long ejercicioOrdenPago) {
		this.ejercicioOrdenPago = ejercicioOrdenPago;
	}
	public double getCotizaMov() {
		return cotizaMov;
	}
	public void setCotizaMov(double cotizaMov) {
		this.cotizaMov = cotizaMov;
	}
	public String getCotizaMovStr() {
		return format.format(cotizaMov);
	}
	public double getDebePesos() {
		return debePesos;
	}
	public void setDebePesos(double debePesos) {
		this.debePesos = debePesos;
	}
	public String getDebePesosStr() {
		return format.format(debePesos);
	}
	public double getHaberPesos() {
		return haberPesos;
	}
	public void setHaberPesos(double haberPesos) {
		this.haberPesos = haberPesos;
	}
	public String getHaberPesosStr() {
		return format.format(haberPesos);
	}
	public double getSaldoPesos() {
		return saldoPesos;
	}
	public void setSaldoPesos(double saldoPesos) {
		this.saldoPesos = saldoPesos;
	}
	public String getSaldoPesosStr() {
		return format.format(saldoPesos);
	}
	public Long getNroAsiento(){
		if(isDiferenciaCotizacion() || detalle != null && detalle.equals("Total")){
			return null;
		}else{
		Ctacte cc = new Ctacte();
		cc.setId(getCtacteKey());
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		cc = (Ctacte) bp.getById(Ctacte.class, cc.getId());
		return cc.getNroAsiento(); 
		}
	}	
	public Long getIdAsiento(){
		if(isDiferenciaCotizacion() || detalle != null && detalle.equals("Total")){
			return null;
		}else{
		Ctacte cc = new Ctacte();
		cc.setId(getCtacteKey());
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		cc = (Ctacte) bp.getById(Ctacte.class, cc.getId());
		return cc.getIdAsiento(); 
		}
	}
	
	public CtacteKey getCtacteKey() {
		if (idCtacte == null) {
			return null;
		}
		
		CtacteKey ck = new CtacteKey();
		String[] partes = idCtacte.split("-");
		ck.setMovimientoCtacte(Long.parseLong(partes[0]));
		ck.setPeriodoCtacte(Long.parseLong(partes[1]));
		ck.setItemCtacte(Long.parseLong(partes[2]));
		ck.setVerificadorCtacte(Long.parseLong(partes[3]));
		ck.setObjetoi_id(idObjetoi);
		
		return ck;
	}
	
	public boolean isDiferenciaCotizacion() {
		return diferenciaCotizacion;
	}
	
	public void setDiferenciaCotizacion(boolean diferenciaCotizacion) {
		this.diferenciaCotizacion = diferenciaCotizacion;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}
}
