package com.asf.cred.business;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.asf.util.DateHelper;
import com.civitas.cred.beans.BeanCtaCte;

public class BoletoResumenHelper {

	private static Map<String, BoletoResumenHelper> instances;

	private static Map<String, BoletoResumenHelper> getInstances() {
		if (instances == null) {
			instances = new HashMap<String, BoletoResumenHelper>();
		}
		return instances;
	}
	
	public static BoletoResumenHelper getInstance(String id) {
		if (!getInstances().containsKey(id)) {
			getInstances().put(id, new BoletoResumenHelper());
		}
		return getInstances().get(id);
	}
	
	private static class InteresesBean {
		public double moratorio;
		public double punitorio;
		public String cuotasMoratorio;
		public String cuotasPunitorio;
	}
	
	private Map<Long, InteresesBean> intereses = new HashMap<Long, InteresesBean>();

	public Double calcularMoratorio(Long idObjetoi) {
		if (idObjetoi == null) {
			return 0.0;
		}
		
		if (intereses.containsKey(idObjetoi)) {
			return intereses.get(idObjetoi).moratorio;
		}
		return 0.0;
	}
	
	public Double calcularPunitorio(Long idObjetoi) {
		if (idObjetoi == null) {
			return 0.0;
		}
		
		if (intereses.containsKey(idObjetoi)) {
			return intereses.get(idObjetoi).punitorio;
		}
		return 0.0;
	}
	
	public String calcularCuotasMoratorio(Long idObjetoi) {
		if (idObjetoi == null) {
			return "";
		}
		
		if (intereses.containsKey(idObjetoi)) {
			return intereses.get(idObjetoi).cuotasMoratorio;
		}
		return "";
	}
	
	public String calcularCuotasPunitorio(Long idObjetoi) {
		if (idObjetoi == null) {
			return "";
		}
		
		if (intereses.containsKey(idObjetoi)) {
			return intereses.get(idObjetoi).cuotasPunitorio;
		}
		return "";
	}
	
	public void calcularIntereses(Long idObjetoi, Date fecha, Integer numeroCuota) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(new Long(idObjetoi), fecha);
		calculoDeuda.calcular();
		List<BeanCtaCte> beans = calculoDeuda.getBeans();
		beans.remove(beans.size() - 1);
		double moratorio = 0.0;
		double punitorio = 0.0;
		String cuotasMoratorio = "";
		String cuotasPunitorio = "";
		
		InteresesBean resultado = new InteresesBean();
		
		Date fechaVencimientoActual = null;
		int numeroActual = 0;
		for(BeanCtaCte bean : beans){
			if (bean.getDetalle() != null && bean.getDetalle().equalsIgnoreCase("Cuota")) {
				fechaVencimientoActual = bean.getFechaVencimiento();
				numeroActual = bean.getNumero().intValue();
			}
			
			if(fechaVencimientoActual.before(calendar.getTime())
					&& numeroActual < numeroCuota.intValue()){
				if(bean.isCuota()){
					moratorio += bean.getMoratorio().doubleValue();
					punitorio += bean.getPunitorio().doubleValue();
				}else{
					moratorio -= bean.getMoratorio().doubleValue();
					punitorio -= bean.getPunitorio().doubleValue();
				}
				if(cuotasMoratorio.indexOf(bean.getNumero().toString()) == -1){
					if(cuotasMoratorio.isEmpty())
						cuotasMoratorio += bean.getNumero().toString();
					else
						cuotasMoratorio += " - " + bean.getNumero().toString();
				}
				if(cuotasPunitorio.indexOf(bean.getNumero().toString()) == -1){
					if(cuotasPunitorio.isEmpty())
						cuotasPunitorio += bean.getNumero().toString();
					else
						cuotasPunitorio += " - " + bean.getNumero().toString();
				}
			}
		}
		
		resultado.cuotasMoratorio = cuotasMoratorio;
		resultado.cuotasPunitorio = cuotasPunitorio;
		resultado.moratorio = moratorio;
		resultado.punitorio = punitorio;
		
		intereses.put(new Long(idObjetoi), resultado);
	}
}
