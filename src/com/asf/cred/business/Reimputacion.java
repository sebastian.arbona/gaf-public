package com.asf.cred.business;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.NoResultException;

import org.hibernate.Query;

import com.asf.gaf.hibernate.Moneda;
import com.asf.gaf.hibernate.Recaudacion;
import com.asf.security.SessionHandler;
import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.asf.util.DoubleHelper;
import com.nirven.creditos.hibernate.Bolcon;
import com.nirven.creditos.hibernate.BolconKey;
import com.nirven.creditos.hibernate.Bolepago;
import com.nirven.creditos.hibernate.BolepagoId;
import com.nirven.creditos.hibernate.Boleto;
import com.nirven.creditos.hibernate.Caratula;
import com.nirven.creditos.hibernate.Cobropago;
import com.nirven.creditos.hibernate.Concepto;
import com.nirven.creditos.hibernate.Ctacte;
import com.nirven.creditos.hibernate.CtacteKey;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Movpagos;
import com.nirven.creditos.hibernate.MovpagosKey;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Pagos;
import com.nirven.creditos.hibernate.PagosKey;
import com.nirven.creditos.hibernate.Tipomov;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Numerador;

/**
 * Proceso de reimputacion sobre la cuenta corriente
 * 
 * @author Cesar
 *
 */
public class Reimputacion extends ConversationProcess {

	// paso 1, busqueda de recaudacion
	private Date fechaDesde;
	private Date fechaHasta = new Date();
	@Save
	private double importeDesde;
	@Save
	private double importeHasta;
	@Save
	private Long idEnteRecaudador;
	@Save
	private List<Recaudacion> recaudaciones;

	// paso 2, seleccion de recaudacion
	private Long idRecaudacion;
	@Save
	private double importe;
	@Save(refresh = true)
	private Recaudacion recaudacion;

	@Save(refresh = true)
	private List<RecaudacionSeleccionada> seleccionadas;

	private Long idRecaudacion2;

	// paso 3, seleccion de credito
	private Long numeroAtencion;
	@Save(refresh = true)
	private Objetoi credito;
	@Save
	private List<Movpagos> movpagos;

	// paso 4, movimientos a predesaplicar
	@Save
	private List<DesaplicacionPagoBean> predesaplicaciones;
	@Save
	private SortedSet<Long> idsCuotasAfectadas;

	// paso 5, desaplicacion
	@Save
	private List<DesaplicacionPagoBean> desaplicaciones;

	// paso 6, reaplicacion
	@Save
	private List<DesaplicacionPagoBean> reaplicaciones;

	// paso 7, ejecucion
	private Map<String, Boleto> recibosGenerados;

	@Save
	private List<DesaplicacionPagoBean> movimientosNuevos;

	private List<Ctacte> ctacteGenerados;
	private boolean terminado;

	/**
	 * @return falso: si los parametros de busqueda no fueron definidos verdadero:
	 *         si los parametros fueron definidos y ademas carga la lista de
	 *         resultados
	 */
	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean buscarRecaudaciones() {
		if (fechaDesde == null || idEnteRecaudador == null) {
			return false;
		}
		recaudaciones = bp.createSQLQuery(
				"SELECT r.* FROM Recaudacion r WHERE r.fechamovimiento >= :fechad and r.fechamovimiento <= :fechah AND r.codi_ba = :codi_ba AND (r.saldo >= :sd and r.saldo <= :sh)")
				.addEntity(Recaudacion.class).setDate("fechad", fechaDesde).setDate("fechah", fechaHasta)
				.setLong("codi_ba", idEnteRecaudador).setDouble("sd", importeDesde).setDouble("sh", importeHasta)
				.list();
		forward = "reimputacion1Recaudacion";
		return true;
	}

	@ProcessMethod
	public boolean quitar() {
		if (seleccionadas == null || seleccionadas.isEmpty())
			return false;
		seleccionadas.remove(new RecaudacionSeleccionada(idRecaudacion2, importe));
		return this.buscarRecaudaciones();
	}

	@ProcessMethod
	public boolean continuar() {
		if (seleccionadas == null || seleccionadas.isEmpty())
			return false;
		forward = "reimputacion2Credito";
		return true;

	}

	/**
	 * @return falso: si no se han seleccionado valores o no se puede encontrar una
	 *         recaudacion seleccionada. verdadero: en el caso contrario
	 */

	@ProcessMethod
	public boolean seleccionarRecaudacion() {
		if (idRecaudacion == null) {
			error("reimputacion.noseleccion");
			return false;
		}
		if (importe == 0) {
			error("reimputacion.importe.cero");
			return false;
		}
		recaudacion = (Recaudacion) bp.getById(Recaudacion.class, idRecaudacion);
		if (recaudacion.getSaldo() - importe < 0) {
			error("reimputacion.importe.excede");
			return false;
		}

		// forward = "reimputacion1Credito";
		if (seleccionadas == null) {
			seleccionadas = new ArrayList<Reimputacion.RecaudacionSeleccionada>();
		}
		RecaudacionSeleccionada rs = new RecaudacionSeleccionada(idRecaudacion, importe);
		if (seleccionadas.contains(rs)) {
			error("reimputacion.recaudacion.seleccionada");
			return false;
		}
		this.seleccionadas.add(rs);
		this.buscarRecaudaciones();
		return true;
	}

	/**
	 * @return
	 */
	@ProcessMethod
	public boolean seleccionarCredito() {
		if (numeroAtencion == null) {
			return false;
		}
		credito = (Objetoi) bp.getNamedQuery("Objetoi.findByNroAtencion").setParameter("nroAtencion", numeroAtencion)
				.uniqueResult();
		forward = "reimputacion2Credito";
		if (credito == null) {
			error("reimputacion.credito.noexiste");
			movpagos = null;
			return false;
		}
		armarPagos();
		return true;
	}

	private Date getMinFecha(List<RecaudacionSeleccionada> rss) {
		Date fecha = null;
		for (RecaudacionSeleccionada rs : rss) {
			if (fecha == null || rs.getRecaudacion().getFechamovimiento().before(fecha))
				fecha = rs.getRecaudacion().getFechamovimiento();
		}
		return fecha;
	}

	private void armarPagos() {

		movpagos = new ArrayList<Movpagos>();
		// movimiento ficticio para mostrar el pago pendiente
		for (RecaudacionSeleccionada rs : seleccionadas) {
			Movpagos pendiente = new Movpagos();
			pendiente.setImporte(rs.getImporte());
			pendiente.setEstadoReimputacion("reimputacion.estado.pendiente");
			if (rs.getRecaudacion().getBancos() != null) {
				pendiente.setInfoBanco(rs.getRecaudacion().getBancos().getDetaBa());
			}
			rs.setMp(pendiente);
			Ctacte ccPendiente = new Ctacte();
			ccPendiente.setFechaProceso(rs.getRecaudacion().getFechaProceso());
			ccPendiente.setFechaGeneracion(rs.getRecaudacion().getFechamovimiento());
			pendiente.setCtacte(ccPendiente);
			movpagos.add(pendiente);
		}
		List<Object[]> mpcc = listarMovpagos(getMinFecha(seleccionadas));
		Map<String, Movpagos> movsPorPago = new HashMap<String, Movpagos>();
		for (Object[] mp : mpcc) {
			Movpagos movpago = (Movpagos) mp[0];
			Ctacte cc = (Ctacte) mp[1];
			String key = movpago.getPagos().getId().getBoleto().getId() + "-"
					+ movpago.getPagos().getId().getCaratula().getId();
			Movpagos movpagoGeneral = movsPorPago.get(key);
			if (movpagoGeneral == null) {
				movpagoGeneral = new Movpagos();
				movpagoGeneral.setImporte(movpago.getImporte());
				movpagoGeneral.setEstadoReimputacion("reimputacion.estado.reimputar");
				movpagoGeneral.setCtacte(cc);
				if (movpago.getId().getCaratula().getBanco() != null) {
					movpagoGeneral.setInfoBanco(movpago.getId().getCaratula().getBanco().getDetaBa());
				}
				movsPorPago.put(key, movpagoGeneral);
				movpagos.add(movpagoGeneral);
			} else {
				movpagoGeneral.setImporte(movpagoGeneral.getImporte() + movpago.getImporte());
			}
		}
		Collections.sort(movpagos);
	}

	@ProcessMethod
	public boolean listarPreDesaplicacion() {
		List<Object[]> mpcc = listarMovpagos(getMinFecha(seleccionadas));
		predesaplicaciones = new ArrayList<DesaplicacionPagoBean>();
		idsCuotasAfectadas = new TreeSet<Long>();
		Set<Long> recibosProcesados = new HashSet<Long>();
		Map<String, DesaplicacionPagoBean> beansPorReciboCuota = new HashMap<String, DesaplicacionPagoBean>();
		Map<Long, List<DesaplicacionPagoBean>> interesesPorRecibo = new HashMap<Long, List<DesaplicacionPagoBean>>();
		Long idReciboAnterior = null;
		for (Object[] mp : mpcc) {
			Movpagos movpago = (Movpagos) mp[0];
			Ctacte cc = (Ctacte) mp[1];
			Long idRecibo = movpago.getId().getBoleto().getId();
			// agregar los intereses del boleto anterior
			if (idReciboAnterior != null && !idReciboAnterior.equals(idRecibo)) {
				predesaplicaciones.addAll(interesesPorRecibo.get(idReciboAnterior));
			}
			idReciboAnterior = idRecibo;
			idsCuotasAfectadas.add(movpago.getCuota().getId());
			String key = idRecibo + "-" + movpago.getCuota().getId();
			DesaplicacionPagoBean reciboBean = beansPorReciboCuota.get(key);
			if (reciboBean == null) {
				reciboBean = new DesaplicacionPagoBean(cc, !recibosProcesados.contains(idRecibo));
				beansPorReciboCuota.put(key, reciboBean);
				predesaplicaciones.add(reciboBean);
			} else {
				reciboBean.addMovimiento(cc);
			}
			if (!recibosProcesados.contains(idRecibo)) {
				List<Ctacte> intereses = buscarIntereses(cc.getFechaGeneracion(),
						movpago.getId().getCaratula().getId());
				Map<Long, DesaplicacionPagoBean> interesesPorBoleto = new HashMap<Long, DesaplicacionPagoBean>();
				List<DesaplicacionPagoBean> beansIntereses = new ArrayList<DesaplicacionPagoBean>();
				for (Ctacte ccInteres : intereses) {
					DesaplicacionPagoBean beanInteres = interesesPorBoleto.get(ccInteres.getBoleto().getId());
					if (beanInteres == null) {
						beanInteres = new DesaplicacionPagoBean(ccInteres);
						interesesPorBoleto.put(ccInteres.getBoleto().getId(), beanInteres);
						beansIntereses.add(beanInteres);
					} else {
						beanInteres.addMovimiento(ccInteres);
					}
				}
				interesesPorRecibo.put(idRecibo, beansIntereses);
			}
			recibosProcesados.add(idRecibo);
		}
		if (idReciboAnterior != null) {
			predesaplicaciones.addAll(interesesPorRecibo.get(idReciboAnterior));
		}
		forward = "reimputacion3Predesaplicacion";
		return true;
	}

	@SuppressWarnings("unchecked")
	private List<Object[]> listarMovpagos(Date fecha) {
		return bp.createSQLQuery(
				"select {m.*}, {cc.*} from Movpagos m join Ctacte cc on m.movimientoCtacte = cc.movimientoCtacte "
						+ "and m.itemCtacte = cc.itemCtacte and m.periodoCtacte = cc.periodoCtacte and m.verificadorCtacte = cc.verificadorCtacte where cc.objetoi_id = :id "
						+ "and cc.fechaGeneracion >= :fecha order by cc.fechaGeneracion")
				.addEntity("m", Movpagos.class).addEntity("cc", Ctacte.class).setParameter("id", credito.getId())
				.setParameter("fecha", fecha).list();
	}

	@SuppressWarnings("unchecked")
	private List<Ctacte> buscarIntereses(Date fecha, Long idCaratula) {
		return bp.createQuery(
				"select cc from Ctacte cc where cc.fechaGeneracion = :fecha and cc.id.objetoi.id = :id and cc.asociado.concepto.concepto in ('mor','pun') "
						+ "and cc.tipomov.abreviatura = 'DB' and cc.caratula.id = :idCaratula order by cc.fechaProceso")
				.setParameter("fecha", fecha).setParameter("idCaratula", idCaratula).setParameter("id", credito.getId())
				.list();
	}

	@ProcessMethod
	public boolean desaplicar() {
		desaplicaciones = new ArrayList<DesaplicacionPagoBean>();
		movimientosNuevos = new ArrayList<DesaplicacionPagoBean>();

		Numerador numDesapl = (Numerador) bp.getById(Numerador.class, Boleto.NUMERADOR_BOLETO_NOTADEB_DESAPL);
		Numerador numAnul = (Numerador) bp.getById(Numerador.class, Boleto.NUMERADOR_BOLETO_NOTACRED_ANUL);

		long ndNum = (numDesapl != null ? numDesapl.getNumero() : 0);
		long ncNum = (numAnul != null ? numAnul.getNumero() : 0);

		Date hoy = new Date();

		Map<Long, List<DesaplicacionPagoBean>> desaplicacionesPorIdBoleto = new HashMap<Long, List<DesaplicacionPagoBean>>();

		// generar movimientos de desaplicacion
		for (DesaplicacionPagoBean desapBean : predesaplicaciones) {
			DesaplicacionPagoBean desapComprob = new DesaplicacionPagoBean();

			if (desapBean.getTipoComprobante().equals(Boleto.TIPO_BOLETO_RECIBO)) {
				// generar ND Desapl
				ndNum++;
				desapComprob.setTipoComprobante(Boleto.TIPO_BOLETO_NOTADEB_DESAPL);
				desapComprob.setNumeroComprobante(ndNum);
				desapComprob.setDetalle("Desapl Cobranza " + desapBean.getNumeroComprobante());
			} else if (desapBean.getTipoComprobante().equals(Boleto.TIPO_BOLETO_NOTADEB)) {
				// generar NC Anul
				ncNum++;
				desapComprob.setTipoComprobante(Boleto.TIPO_BOLETO_NOTACRED_ANUL);
				desapComprob.setNumeroComprobante(ncNum);
				desapComprob.setDetalle("Anul ND " + desapBean.getNumeroComprobante());
			}

			desapComprob.setImporteTotal(-desapBean.getImporteTotal());
			desapComprob.setFechaProceso(hoy);
			desapComprob.setFechaPago(desapBean.getFechaPago());
			desapComprob.setComprobante(desapBean.isComprobante());

			desapComprob.setNumeroCuota(desapBean.getNumeroCuota());
			desapComprob.setImporteDesaplicar(-desapBean.getImporteDesaplicar());
			desapComprob.setCapital(-desapBean.getCapital());
			desapComprob.setCompensatorio(-desapBean.getCompensatorio());
			desapComprob.setMoratorio(-desapBean.getMoratorio());
			desapComprob.setPunitorio(-desapBean.getPunitorio());
			desapComprob.setGastos(-desapBean.getGastos());
			desapComprob.setGastosRec(-desapBean.getGastosRec());
			desapComprob.setMultas(-desapBean.getMultas());

			desapComprob.setIdBoletoAnulado(desapBean.getIdBoleto());

			List<DesaplicacionPagoBean> desaplicacionesBoleto = desaplicacionesPorIdBoleto.get(desapBean.getIdBoleto());
			if (desaplicacionesBoleto == null) {
				desaplicacionesBoleto = new ArrayList<DesaplicacionPagoBean>();
				desaplicacionesPorIdBoleto.put(desapBean.getIdBoleto(), desaplicacionesBoleto);
			}

			desaplicacionesBoleto.add(desapComprob);

			movimientosNuevos.add(desapComprob);
		}

		// insertar boletos y desaplicaciones por cada boleto
		DesaplicacionPagoBean ultimo = null;
		for (DesaplicacionPagoBean desapBean : predesaplicaciones) {
			if (ultimo != null && !ultimo.getIdBoleto().equals(desapBean.getIdBoleto())) {
				desaplicaciones.addAll(desaplicacionesPorIdBoleto.get(ultimo.getIdBoleto()));
			}

			desaplicaciones.add(desapBean);
			ultimo = desapBean;
		}
		if (ultimo != null) {
			desaplicaciones.addAll(desaplicacionesPorIdBoleto.get(ultimo.getIdBoleto()));
		}

		forward = "reimputacion4Desaplicacion";
		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean preaplicar() {
		reaplicaciones = new ArrayList<DesaplicacionPagoBean>();

		Map<Long, List<DesaplicacionPagoBean>> nuevosInteresesPorIdCuota = new HashMap<Long, List<DesaplicacionPagoBean>>();
		Map<Long, DesaplicacionPagoBean> saldosPorIdCuota = new HashMap<Long, DesaplicacionPagoBean>();
		Set<String> movPagosCuota = new HashSet<String>();

		CreditoHandler ch = new CreditoHandler();
		Date hoy = new Date();

		Long idUltimaCuota = idsCuotasAfectadas.last();
		// buscar siguientes por si sobra saldo
		List<Long> idsSiguientes = bp
				.createQuery("select c.id from Cuota c where c.credito.id = :id and c.id > :idCuota order by c.id")
				.setParameter("id", credito.getId()).setParameter("idCuota", idUltimaCuota).list();

		SortedSet<Long> idsCuotas = new TreeSet<Long>(idsCuotasAfectadas);
		idsCuotas.addAll(idsSiguientes);
		long nroBoleto = -10;

		for (Movpagos mp : movpagos) {
			double saldoPago = mp.getImporte();
			Date fechaPago = mp.getCtacte().getFechaGeneracion();
			for (Long idCuota : idsCuotas) {
				if (saldoPago < 0.005) {
					break; // no hay mas saldo
				}
				Cuota cuota = (Cuota) bp.getById(Cuota.class, idCuota);
				DesaplicacionPagoBean saldoCuota = saldosPorIdCuota.get(cuota.getId());
				if (saldoCuota == null) {
					saldoCuota = new DesaplicacionPagoBean();
					// buscar saldo existente en base de datos
					List<Ctacte> ccs = ch.listarCtaCteConceptos(cuota, hoy, Concepto.CONCEPTO_CAPITAL,
							Concepto.CONCEPTO_COMPENSATORIO, Concepto.CONCEPTO_BONIFICACION,
							Concepto.CONCEPTO_MORATORIO, Concepto.CONCEPTO_PUNITORIO, Concepto.CONCEPTO_GASTOS,
							Concepto.CONCEPTO_GASTOS_A_RECUPERAR);
					for (Ctacte cc : ccs) {
						saldoCuota.addMovimiento(cc);
					}
					// aplicar movimientos nuevos de desaplicacion
					for (DesaplicacionPagoBean desap : movimientosNuevos) {
						if (desap.getNumeroCuota() == cuota.getNumero().intValue())
							saldoCuota.addMovimiento(desap);
					}
					saldosPorIdCuota.put(cuota.getId(), saldoCuota);
				}
				if (saldoCuota.isSaldoCero()) {
					continue;
				}
				// intereses morat y punit
				if (cuota.getFechaVencimiento().before(fechaPago)) {
					List<DesaplicacionPagoBean> nuevosIntereses = nuevosInteresesPorIdCuota.get(cuota.getId());
					if (nuevosIntereses == null) {
						nuevosIntereses = new ArrayList<DesaplicacionPagoBean>();
						nuevosInteresesPorIdCuota.put(cuota.getId(), nuevosIntereses);
					}
					Date fechaDesdeIntereses = cuota.getFechaVencimiento();
					if (!nuevosIntereses.isEmpty()) {
						fechaDesdeIntereses = nuevosIntereses.get(nuevosIntereses.size() - 1).getFechaPago();
					} else {
						List<Movpagos> pagosAnteriores = bp.createQuery(
								"select m from Movpagos m where m.pagos.fechaPago < :fecha and m.cuota.id = :idCuota order by m.pagos.fechaPago desc")
								.setParameter("idCuota", cuota.getId())
								.setParameter("fecha", getMinFecha(seleccionadas)).list();
						if (!pagosAnteriores.isEmpty()) {
							Movpagos pagoAnterior = pagosAnteriores.get(0);
							fechaDesdeIntereses = pagoAnterior.getPagos().getFechaPago();
						}
					}

					int dias = (int) DateHelper.getDiffDates(fechaDesdeIntereses, fechaPago, 2);
					double deuda = saldoCuota.getDeudaImponible();
					Objetoi proyectoCalculo = cuota.getCredito();
					double moratorio = proyectoCalculo.calcularMoratorioNew(deuda, dias, dias, fechaDesdeIntereses,
							fechaDesdeIntereses, fechaPago);
					double punitorio = proyectoCalculo.calcularPunitorioNew(deuda, dias, dias, fechaDesdeIntereses,
							fechaDesdeIntereses, fechaPago);
					DesaplicacionPagoBean intereses = new DesaplicacionPagoBean();
					intereses.setFechaProceso(hoy);
					intereses.setFechaVencimiento(cuota.getFechaVencimiento());
					intereses.setImporteTotal(moratorio + punitorio);
					intereses.setNumeroCuota(cuota.getNumero());
					intereses.setTipoComprobante(Boleto.TIPO_BOLETO_NOTADEB);
					intereses.setMoratorio(moratorio);
					intereses.setPunitorio(punitorio);
					intereses.setFechaPago(fechaPago);
					nuevosIntereses.add(intereses);
					movimientosNuevos.add(intereses);
					saldoCuota.addMovimiento(intereses);
				}

				DesaplicacionPagoBean reaplicacion = new DesaplicacionPagoBean();
				reaplicacion.setNumeroCuota(cuota.getNumero());
				reaplicacion.setFechaVencimiento(cuota.getFechaVencimiento());
				reaplicacion.setImporteTotal(mp.getImporte());
				reaplicacion.setFechaProceso(hoy);
				reaplicacion.setFechaPago(fechaPago);

				if (mp.getEstadoReimputacion().equals("reimputacion.estado.pendiente")) {
					reaplicacion.setComportamientoReimputacion("reimputacion.comportamiento.nuevo");
					reaplicacion.setTipoComprobante(Boleto.TIPO_BOLETO_RECIBO);
					// -10 para identificar al nuevo
					reaplicacion.setNumeroComprobante(nroBoleto--);
				} else {
					reaplicacion.setComportamientoReimputacion("reimputacion.comportamiento.reaplicado");
					reaplicacion.setTipoComprobante(Boleto.TIPO_BOLETO_NOTACRED);
					reaplicacion.setNumeroComprobante(
							mp.getCtacte().getBoleto() != null ? mp.getCtacte().getBoleto().getNumeroBoleto() : 0L);
					reaplicacion.setDetalle("Re-Aplicac Recibo " + reaplicacion.getNumeroComprobante());
				}

				String key = (mp.getCtacte().getBoleto() != null ? mp.getCtacte().getBoleto().getId().toString()
						: getKey(mp));
				reaplicacion.setComprobante(!movPagosCuota.contains(key));
				movPagosCuota.add(key);

				double saldoAntesDeAplicar = saldoPago;
				saldoPago = reaplicacion.pagar(saldoCuota, saldoPago);
				double montoAplicado = saldoAntesDeAplicar - saldoPago;
				reaplicacion.setImporteDesaplicar(montoAplicado);
				reaplicacion.setRecaudacion(getRecaudacion(mp));
				reaplicaciones.add(reaplicacion);
				movimientosNuevos.add(reaplicacion);
			}
		}

		forward = "reimputacion5Preaplicacion";
		return true;
	}

	private String getKey(Movpagos mp) {
		for (RecaudacionSeleccionada rs : seleccionadas) {
			if (mp == rs.getMp()) {
				return "pendiente" + rs.getRecaudacion().getId();
			}
		}
		return null;
	}

	private RecaudacionSeleccionada getRecaudacion(Movpagos mp) {
		for (RecaudacionSeleccionada rs : seleccionadas) {
			if (mp == rs.getMp()) {
				return rs;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean ejecutar() {
		try {
			bp.begin();
			recibosGenerados = new HashMap<String, Boleto>();
			Caratula caratula = null;
			Pagos pago = null;
			for (DesaplicacionPagoBean b : movimientosNuevos) {
				// se resetean para cada comprobante
				ctacteGenerados = new ArrayList<Ctacte>();
				Boleto comprobante = generarComprobante(b);
				if (b.getIdBoletoAnulado() != null) {
					Boleto anulado = (Boleto) bp.getById(Boleto.class, b.getIdBoletoAnulado());
					anulado.setAnulado(true);
					anulado.setBoletoAnulacion(comprobante);
					bp.update(anulado);
				}
				if (b.getTipoComprobante().equals(Boleto.TIPO_BOLETO_RECIBO)) {
					if (caratula == null) {
						caratula = generarCaratula(b.getRecaudacion());
					}
					if (pago == null) {
						pago = generarPago(b, comprobante, caratula);
					}
					for (Ctacte cc : ctacteGenerados) {
						// generar movpagos
						Movpagos mp = new Movpagos();
						MovpagosKey mk = new MovpagosKey();
						mk.setBoleto(comprobante);
						mk.setCaratula(caratula);
						mk.setMovimientoCtacte(cc.getId().getMovimientoCtacte());
						mk.setItemCtacte(cc.getId().getItemCtacte());
						mk.setPeriodoCtacte(cc.getId().getPeriodoCtacte());
						mk.setVerificadorCtacte(cc.getId().getVerificadorCtacte());
						mp.setId(mk);
						mp.setActualizado(true);
						mp.setCuota(cc.getCuota());
						mp.setFacturado(cc.getFacturado());
						mp.setImporte(cc.getImporte());
						mp.setOriginal(cc.getFacturado());
						mp.setPagos(pago);
						mp.setTipomov(Tipomov.buscarCredito());
						bp.save(mp);
						cc.setCaratula(caratula);
						bp.update(cc);
					}
					Cobropago cp = new Cobropago();
					cp.setAutorizacion("");
					cp.setCaratula(caratula);
					cp.setCuotas(0L);
					cp.setEntidadEmisora("");
					cp.setImporte(b.getImporteTotal());
					cp.setMediopagoId(51L); // recaudaciones
					cp.setNumeroComprobante(0L);
					cp.setRecaudacion(b.getRecaudacion().getRecaudacion());
					Long lote = (Long) bp.getByFilter("SELECT max(c.numeroLote) FROM Cobropago c").iterator().next();
					lote = lote == null ? new Long(1) : (lote + 1);
					cp.setNumeroLote(lote);
					bp.save(cp);
					Bolepago bolepago = new Bolepago();
					bolepago.setImporte(b.getRecaudacion().getImporte());
					BolepagoId bk = new BolepagoId();
					bk.setCobropago(cp);
					bk.setBoleto(pago.getBoleto());
					bolepago.setId(bk);
					bp.save(bolepago);
				}
			}
			List<Cuota> cuotasSinCancelar = bp.getNamedQuery("Cuota.findSinCancelar")
					.setParameter("idCredito", credito.getId()).list();
			Query queryCtacte = bp.getNamedQuery("Ctacte.findByCuota");
			CreditoHandler ch = new CreditoHandler();
			Date hoy = new Date();
			for (Cuota cuota : cuotasSinCancelar) {
				if (cuota.getFechaVencimiento().after(hoy))
					continue;
				List<Ctacte> ccs = queryCtacte.setParameter("idCuota", cuota.getId()).list();
				double saldo = ch.calcularSaldo(ccs);
				if (saldo < 0.005) {
					// saldada
					cuota.setEstado(Cuota.CANCELADA);
					bp.update(cuota);
				}
			}
			// #22617
			double saldo;
			Recaudacion recaudacion = null;
			for (RecaudacionSeleccionada seleccion : seleccionadas) {
				recaudacion = seleccion.getRecaudacion();
				saldo = DoubleHelper.redondear(recaudacion.getSaldo() - seleccion.getImporte());
				recaudacion.setSaldo(saldo);
				bp.saveOrUpdate(recaudacion);
			}
			terminado = true;
			bp.commit();
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			bp.rollback();
			error("reimputacion.error");
			return false;
		}
	}

	/**
	 * @return verdadero
	 */
	@ProcessMethod
	public boolean volver() {
		return true;
	}

	private String getTipoConcepto(DesaplicacionPagoBean b) {
		if (b.getTipoComprobante().equals(Boleto.TIPO_BOLETO_NOTACRED_ANUL)) {
			return Ctacte.TIPO_CONCEPTO_REIMPUTACION1;
		} else if (b.getTipoComprobante().equals(Boleto.TIPO_BOLETO_NOTADEB_DESAPL)) {
			return Ctacte.TIPO_CONCEPTO_REIMPUTACION2;
		} else if (b.getTipoComprobante().equals(Boleto.TIPO_BOLETO_NOTADEB)) {
			return Ctacte.TIPO_CONCEPTO_REIMPUTACION3;
		} else {
			return Ctacte.TIPO_CONCEPTO_COBRANZA;
		}
	}

	private Boleto generarComprobante(DesaplicacionPagoBean b) {
		Boleto bol;
		String key = b.getNumeroComprobante() != null ? b.getTipoComprobante() + b.getNumeroComprobante() : null;
		if (key != null && recibosGenerados.containsKey(key)) {
			bol = recibosGenerados.get(key);
		} else {
			bol = crearBoleto(b.getTipoComprobante(), 0);
			if (key != null) {
				recibosGenerados.put(key, bol);
			}
		}
		double importe = b.getDeudaImponible() + b.getMoratorio() + b.getPunitorio();
		bol.setImporte(importe);
		bp.update(bol);
		Cuota cuota = (Cuota) bp.getNamedQuery("Cuota.findByNumero").setParameter("idCredito", credito.getId())
				.setParameter("nroCuota", b.getNumeroCuota()).uniqueResult();
		Long periodo = new Long(Calendar.getInstance().get(Calendar.YEAR));
		long itemCtacte = 1;
		Long movimiento = Numerador.getNext("ctacte." + periodo);
		crearMovimiento(b.getCapital(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_CAPITAL), bol, cuota, movimiento, itemCtacte++,
				getTipoConcepto(b));
		crearMovimiento(b.getCompensatorio(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_COMPENSATORIO), bol, cuota, movimiento, itemCtacte++,
				getTipoConcepto(b));
		crearMovimiento(b.getMoratorio(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_MORATORIO), bol, cuota, movimiento, itemCtacte++,
				getTipoConcepto(b));
		crearMovimiento(b.getPunitorio(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_PUNITORIO), bol, cuota, movimiento, itemCtacte++,
				getTipoConcepto(b));
		crearMovimiento(b.getGastos(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS), bol, cuota, movimiento, itemCtacte++,
				getTipoConcepto(b));
		crearMovimiento(b.getGastosRec(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_GASTOS_A_RECUPERAR), bol, cuota, movimiento, itemCtacte++,
				getTipoConcepto(b));
		crearMovimiento(b.getMultas(), b.getFechaPago(), b.getDetalle(),
				Concepto.buscarConcepto(Concepto.CONCEPTO_MULTAS), bol, cuota, movimiento, itemCtacte,
				getTipoConcepto(b));
		return bol;
	}

	private Pagos generarPago(DesaplicacionPagoBean b, Boleto recibo, Caratula caratula) {
		Moneda peso = (Moneda) bp.getById(Moneda.class, 1L);
		Usuario usuario = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
		double importe = Math.abs(b.getImporteTotal());
		Boleto capital = crearBoleto(Boleto.TIPO_BOLETO_CAPITAL, importe);
		Pagos pago = new Pagos();
		PagosKey pk = new PagosKey();
		pk.setCaratula(caratula);
		pk.setBoleto(capital);
		pago.setId(pk);
		pago.setActualizado(true);
		pago.setCotizacion(1.0); // no hay soporte para otras monedas
		pago.setFechaEmision(capital.getFechaEmision());
		pago.setFechaPago(b.getFechaPago());
		pago.setFechaVencimiento(capital.getFechaVencimiento());
		pago.setImporte(importe);
		pago.setMoneda(peso);
		pago.setMontoOriginal(importe);
		pago.setObjetoi(credito);
		pago.setUsuario(usuario);
		pago.setRecibo(recibo);
		bp.save(pago);
		return pago;
	}

	private Caratula generarCaratula(RecaudacionSeleccionada rs) {
		Date hoy = new Date();
		Long ultimoGrupo;
		try {
			ultimoGrupo = (Long) bp.createQuery("SELECT MAX(c.grupo) FROM Caratula c WHERE c.fechaEnvio = :fecha")
					.setDate("fecha", hoy).uniqueResult();
			if (ultimoGrupo == null) {
				ultimoGrupo = 0L;
			}
		} catch (NoResultException e) {
			ultimoGrupo = 0L;
		}
		Caratula c = new Caratula();
		c.setActualizada(1L);
		c.setBanco(rs.getRecaudacion().getBancos());
		c.setComprobantes(1L);
		c.setCotiza(1.0); // no hay soporte para otras monedas
		c.setFechaActualizacion(hoy);
		c.setFechaCobranza(rs.getRecaudacion().getFechamovimiento());
		c.setFechaEnvio(hoy);
		c.setFecovita(false);
		c.setGrupo(++ultimoGrupo);
		c.setImporte(rs.getImporte());
		c.setMetodoCarga(2); // procesamiento
		c.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		bp.save(c);
		return c;
	}

	private Boleto crearBoleto(String tipo, double importe) {
		Date hoy = new Date();
		Boleto bol = new Boleto();
		bol.setTipo(tipo);
		Long periodo = new Long(Calendar.getInstance().get(Calendar.YEAR));
		bol.setPeriodoBoleto(periodo);
		bol.setFechaEmision(hoy);
		bol.setFechaVencimiento(hoy);
		bol.setImporte(importe);
		bol.setImporteVencido(bol.getImporte());
		bol.setNumeroBoleto(Numerador.getNext(bol.getNumerador()));
		bol.setObjetoi(credito);
		bol.setUsuario_id(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		bol.setVerificadorBoleto(0L);
		bp.save(bol);
		return bol;
	}

	private void crearMovimiento(double importe, Date fechaPago, String detalle, Concepto concepto, Boleto bol,
			Cuota cuota, Long movimiento, Long itemCtacte, String tipoConcepto) {
		if (Math.abs(importe) >= 0.005) {
			Tipomov tipomov = null;
			if (bol.getTipo().equals(Boleto.TIPO_BOLETO_NOTADEB_DESAPL)
					|| bol.getTipo().equals(Boleto.TIPO_BOLETO_NOTADEB)) {
				tipomov = Tipomov.buscarDebito();
			} else if (bol.getTipo().equals(Boleto.TIPO_BOLETO_RECIBO)
					|| bol.getTipo().equals(Boleto.TIPO_BOLETO_NOTACRED_ANUL)
					|| bol.getTipo().equals(Boleto.TIPO_BOLETO_NOTACRED)) {
				tipomov = Tipomov.buscarCredito();
			}
			Ctacte cc = crearCtacte(fechaPago, importe, detalle, concepto, tipomov, bol, cuota, movimiento, itemCtacte,
					tipoConcepto);
			Bolcon bc = crearBolcon(cc);
			bp.save(cc);
			bp.save(bc);
			ctacteGenerados.add(cc);
		}
	}

	private Ctacte crearCtacte(Date fecha, double importe, String detalle, Concepto concepto, Tipomov tipomov,
			Boleto bol, Cuota cuota, Long movimiento, Long item, String tipoConcepto) {
		Long periodo = new Long(Calendar.getInstance().get(Calendar.YEAR));
		Ctacte cc = new Ctacte();
		CtacteKey ck = new CtacteKey();
		ck.setObjetoi(credito);
		ck.setPeriodoCtacte(periodo);
		ck.setVerificadorCtacte(0L);
		ck.setItemCtacte(item);
		ck.setMovimientoCtacte(movimiento);
		cc.setId(ck);
		cc.setAsociado(concepto);
		cc.setBoleto(bol);
		cc.setCuota(cuota);
		cc.setFacturado(cc.getAsociado());
		cc.setFechaGeneracion(fecha);
		cc.setFechaVencimiento(cuota.getFechaVencimiento());
		cc.setImporte(Math.abs(importe));
		cc.setTipomov(tipomov);
		cc.setTipoMovimiento("pago");
		cc.setUsuario_id(SessionHandler.getCurrentSessionHandler().getCurrentUser());
		cc.setTipoConcepto(tipoConcepto);
		cc.setDetalle(detalle);
		return cc;
	}

	private Bolcon crearBolcon(Ctacte cc) {
		CtacteKey ck = cc.getId();
		Bolcon bc = new Bolcon();
		BolconKey bk = new BolconKey();
		bk.setMovimientoCtacte(ck.getMovimientoCtacte());
		bk.setBoleto(cc.getBoleto());
		bk.setItemCtacte(ck.getItemCtacte());
		bk.setPeriodoCtacte(ck.getPeriodoCtacte());
		bk.setVerificadorCtacte(ck.getVerificadorCtacte());
		bc.setId(bk);
		bc.setCuota(cc.getCuota());
		bc.setFacturado(cc.getFacturado());
		bc.setImporte(cc.getImporte());
		bc.setOriginal(cc.getFacturado());
		bc.setTipomov(cc.getTipomov());
		return bc;
	}

	/**
	 * @return la fechaDesde en formato dd/MM/yyyy
	 */
	public String getFechaDesde() {
		return DateHelper.getString(fechaDesde);
	}

	/**
	 * @param fechaDesde la fecha desde a asignar en formato dd/MM/yyyy
	 */
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = DateHelper.getDate(fechaDesde);
	}

	/**
	 * @return la fechaHasta en formato dd/MM/yyyy
	 */
	public String getFechaHasta() {
		return DateHelper.getString(fechaHasta);
	}

	/**
	 * @param fechaHasta la fechaHasta asignar en formato dd/MM/yyyy
	 */
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = DateHelper.getDate(fechaHasta);
	}

	/**
	 * @return importeDesde
	 */
	public double getImporteDesde() {
		return importeDesde;
	}

	/**
	 * @param importeDesde valor double a asignar
	 */
	public void setImporteDesde(double importeDesde) {
		this.importeDesde = importeDesde;
	}

	/**
	 * @return el valor double del importeHasta
	 */
	public double getImporteHasta() {
		return importeHasta;
	}

	public void setImporteHasta(double importeHasta) {
		this.importeHasta = importeHasta;
	}

	public Long getIdEnteRecaudador() {
		return idEnteRecaudador;
	}

	public void setIdEnteRecaudador(Long idEnteRecaudador) {
		this.idEnteRecaudador = idEnteRecaudador;
	}

	public List<Recaudacion> getRecaudaciones() {
		return recaudaciones;
	}

	public Long getIdRecaudacion() {
		return idRecaudacion;
	}

	public void setIdRecaudacion(Long idRecaudacion) {
		this.idRecaudacion = idRecaudacion;
	}

	public Recaudacion getRecaudacion() {
		return recaudacion;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public Long getNumeroAtencion() {
		return numeroAtencion;
	}

	public void setNumeroAtencion(Long numeroAtencion) {
		this.numeroAtencion = numeroAtencion;
	}

	public List<Movpagos> getMovpagos() {
		return movpagos;
	}

	public List<DesaplicacionPagoBean> getPredesaplicaciones() {
		return predesaplicaciones;
	}

	public Objetoi getCredito() {
		return credito;
	}

	@Override
	protected String getDefaultForward() {
		return "reimputacion1Recaudacion";
	}

	public List<DesaplicacionPagoBean> getDesaplicaciones() {
		return desaplicaciones;
	}

	public List<DesaplicacionPagoBean> getReaplicaciones() {
		return reaplicaciones;
	}

	public boolean isTerminado() {
		return terminado;
	}

	public void setTerminado(boolean terminado) {
		this.terminado = terminado;
	}

	public class RecaudacionSeleccionada implements Serializable {

		private static final long serialVersionUID = 4699230741666164741L;
		private Long idRecaudacion;
		private double importe;
		private Movpagos mp;

		public RecaudacionSeleccionada() {
			super();
		}

		public RecaudacionSeleccionada(Long idRecaudacion, double importe) {
			super();
			this.idRecaudacion = idRecaudacion;
			this.importe = importe;
		}

		public Long getIdRecaudacion() {
			return idRecaudacion;
		}

		public void setIdRecaudacion(Long idRecaudacion) {
			this.idRecaudacion = idRecaudacion;
		}

		public Recaudacion getRecaudacion() {
			return (Recaudacion) SessionHandler.getCurrentSessionHandler().getBusinessPersistance()
					.getById(Recaudacion.class, idRecaudacion);
		}

		public double getImporte() {
			return importe;
		}

		public void setImporte(double importe) {
			this.importe = importe;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof RecaudacionSeleccionada) {
				return this.idRecaudacion.equals(((RecaudacionSeleccionada) obj).idRecaudacion);
			}
			return false;
		}

		public String getImporteStr() {
			return NumberFormat.getCurrencyInstance().format(this.getImporte());
		}

		public Movpagos getMp() {
			return mp;
		}

		public void setMp(Movpagos mp) {
			this.mp = mp;
		}

	}

	public List<RecaudacionSeleccionada> getSeleccionadas() {
		return seleccionadas;
	}

	public void setSeleccionadas(List<RecaudacionSeleccionada> seleccionadas) {
		this.seleccionadas = seleccionadas;
	}

	public Long getIdRecaudacion2() {
		return idRecaudacion2;
	}

	public void setIdRecaudacion2(Long idRecaudacion2) {
		this.idRecaudacion2 = idRecaudacion2;
	}

	public double getTotalRecaudaciones() {
		double ret = 0.0;
		for (RecaudacionSeleccionada rs : this.seleccionadas)
			ret += rs.getImporte();
		return ret;
	}

	public String getTotalRecaudacionesStr() {
		return NumberFormat.getCurrencyInstance().format(this.getTotalRecaudaciones());
	}
}
