package com.asf.cred.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.asf.struts.action.ConversationProcess;
import com.asf.struts.action.ProcessMethod;
import com.asf.struts.action.Save;
import com.asf.util.DateHelper;
import com.nirven.creditos.hibernate.Cuota;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Objetoi;

public class NuevaCuotaProcess extends ConversationProcess {

	private int numeroCuota;
	private Date fechaVencimiento;
	private Objetoi credito;
	@Save
	private Long idCredito;
	private List<Cuota> cuotas;
	@Save
	private List<Integer> numerosPosibles;
	private boolean guardado;

	@Override
	protected String getDefaultForward() {
		return "NuevaCuota";
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod(defaultAction = true)
	public boolean buscar() {
		if (idCredito == null || idCredito == 0) {
			return false;
		}

		credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", idCredito).list();

		numerosPosibles = new ArrayList<Integer>();

		CreditoHandler ch = new CreditoHandler();

		for (Cuota cuota : cuotas) {
			if (cuota.getEmision() == null && !ch.tieneCompensatorio(cuota)) {
				numerosPosibles.add(cuota.getNumero());
			}
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	@ProcessMethod
	public boolean guardar() {
		// validar numero
		if (numeroCuota == 0 || !numerosPosibles.contains(numeroCuota)) {
			errors.put("nuevaCuota.numero", "nuevaCuota.numero");
			return false;
		}

		credito = (Objetoi) bp.getById(Objetoi.class, idCredito);
		cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", idCredito).list();

		int indiceCuota = numeroCuota - 1;

		// validar fecha
		Date fechaLimiteDesde;
		if (indiceCuota > 0) {
			Cuota anterior = cuotas.get(indiceCuota - 1);
			fechaLimiteDesde = anterior.getFechaVencimiento();
		} else {
			// usar desembolso como fecha anterior
			Desembolso primerDesembolso = (Desembolso) bp.getNamedQuery("Desembolso.findByCreditoYNumero")
					.setParameter("idCredito", idCredito).setParameter("numero", 1).uniqueResult();
			if (primerDesembolso == null) {
				errors.put("nuevaCuota.estado", "nuevaCuota.estado");
				return false;
			}
			fechaLimiteDesde = primerDesembolso.getFechaReal();
		}

		Cuota elegida = cuotas.get(indiceCuota);
		Date fechaLimiteHasta = elegida.getFechaVencimiento();

		if (fechaVencimiento == null || fechaVencimiento.getTime() <= fechaLimiteDesde.getTime()
				|| fechaVencimiento.getTime() >= fechaLimiteHasta.getTime()) {
			errors.put("nuevaCuota.fecha", "nuevaCuota.fecha");
			return false;
		}

		// empujar numeros hacia adelante
		for (int i = indiceCuota; i < cuotas.size(); i++) {
			Cuota cuota = cuotas.get(i);
			cuota.setNumero(cuota.getNumero() + 1);
			bp.update(cuota);
		}

		// crear nueva cuota
		Cuota cuotaNueva = new Cuota();
		cuotaNueva.setCredito(credito);
		cuotaNueva.setEstado(Cuota.SIN_CANCELAR);
		cuotaNueva.setFechaGeneracion(fechaVencimiento);
		cuotaNueva.setFechaVencimiento(fechaVencimiento);
		cuotaNueva.setNumero(numeroCuota);

		cuotaNueva.setCapital(0.0);
		cuotaNueva.setMoratorio(0.0);
		cuotaNueva.setCompensatorio(0.0);
		cuotaNueva.setPunitorio(0.0);
		cuotaNueva.setBonificacion(0.0);
		cuotaNueva.setGastos(0.0);
		cuotaNueva.setMultas(0.0);
		cuotaNueva.setTasaCompensatorio(0.0);
		cuotaNueva.setTasaMoratorio(0.0);
		cuotaNueva.setTasaPunitorio(0.0);

		bp.save(cuotaNueva);

		bp.getCurrentSession().flush();

		// buscar para mostrar como quedaron las cuotas
		cuotas = bp.getNamedQuery("Cuota.findByObjetoi").setParameter("idCredito", idCredito).list();

		guardado = true;

		return true;
	}

	public Objetoi getCredito() {
		return credito;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public int getNumeroCuota() {
		return numeroCuota;
	}

	public void setNumeroCuota(int numeroCuota) {
		this.numeroCuota = numeroCuota;
	}

	public String getFechaVencimiento() {
		return DateHelper.getString(fechaVencimiento);
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = DateHelper.getDate(fechaVencimiento);
	}

	public List<Cuota> getCuotas() {
		return cuotas;
	}

	public void setCuotas(List<Cuota> cuotas) {
		this.cuotas = cuotas;
	}

	public boolean isGuardado() {
		return guardado;
	}

	public List<Integer> getNumerosPosibles() {
		return numerosPosibles;
	}
}
