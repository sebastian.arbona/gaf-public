package com.asf.cred.security;

import com.asf.security.SessionHandler;
import com.civitas.webservices.autenticacion.AutenticacionServidorHandler;

public class AutenticacionServidorHandlerCREDImpl extends
		AutenticacionServidorHandler {

	@Override
	public SessionHandler getSessionHandler() {		
		return new com.asf.cred.security.SessionHandler();
	}
	
	

}
