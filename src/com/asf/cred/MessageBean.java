package com.asf.cred;

import com.asf.util.ReportResult;

public class MessageBean {
	private String key;
	private String arg0, arg1, arg2, arg3, arg4;
	ReportResult report=null;
	
	
	
	public MessageBean(String key, String arg0, String arg1) {
		super();
		this.key = key;
		this.arg0 = arg0;
		this.arg1 = arg1;
	}
	public MessageBean(String key, String arg0) {
		super();
		this.key = key;
		this.arg0 = arg0;
	}
	public MessageBean(String key) {
		super();
		this.key = key;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the arg0
	 */
	public String getArg0() {
		return arg0;
	}
	/**
	 * @param arg0 the arg0 to set
	 */
	public void setArg0(String arg0) {
		this.arg0 = arg0;
	}
	/**
	 * @return the arg1
	 */
	public String getArg1() {
		return arg1;
	}
	/**
	 * @param arg1 the arg1 to set
	 */
	public void setArg1(String arg1) {
		this.arg1 = arg1;
	}
	/**
	 * @return the arg2
	 */
	public String getArg2() {
		return arg2;
	}
	/**
	 * @param arg2 the arg2 to set
	 */
	public void setArg2(String arg2) {
		this.arg2 = arg2;
	}
	/**
	 * @return the arg3
	 */
	public String getArg3() {
		return arg3;
	}
	/**
	 * @param arg3 the arg3 to set
	 */
	public void setArg3(String arg3) {
		this.arg3 = arg3;
	}
	/**
	 * @return the arg4
	 */
	public String getArg4() {
		return arg4;
	}
	/**
	 * @param arg4 the arg4 to set
	 */
	public void setArg4(String arg4) {
		this.arg4 = arg4;
	}
	/**
	 * @return the report
	 */
	public ReportResult getReport() {
		return report;
	}
	/**
	 * @param report the report to set
	 */
	public void setReport(ReportResult report) {
		this.report = report;
	}

}
