package com.asf.cred.estados;

import java.util.Date;

import com.asf.util.DateHelper;

public class EstaObs 
{
//====================================ATRIBUTOS=============================
	private String estaobs;
	private String color;
	private Date fecha;
	private Date fechaHasta;
	private String observaciones;
	private String areaResponsable;
//====================================CONSTRUCTORES==========================
	public EstaObs()
	{
		
	}
//==============================GETTERS Y SETTERS=============================
	public String getEstaobs() 
	{
		return this.estaobs;
	}
	public void setEstaobs(String estaobs) 
	{
		this.estaobs = estaobs;
	}
	public String getColor() 
	{
		return this.color;
	}
	public void setColor(String color) 
	{
		this.color = color;
	}
	public Date getFecha() 
	{
		return this.fecha;
	}
	public void setFecha(Date fecha) 
	{
		this.fecha = fecha;
	}
	public String getFechaStr()
	{
		return DateHelper.getString(this.getFecha());
	}
	public void setFechaStr( String fecha )
	{
		if ( fecha == null || fecha.length() == 0)
		{
			this.setFecha( null );
			return;
		}
		this.setFecha( DateHelper.getDate( fecha ) );
	}
	public Date getFechaHasta() 
	{
		return this.fechaHasta;
	}
	public void setFechaHasta(Date fechaHasta) 
	{
		this.fechaHasta = fechaHasta;
	}
	public String getFechaHastaStr()
	{
		return DateHelper.getString(this.getFechaHasta());
	}
	public void setFechaHastaStr( String fechaHasta )
	{
		if ( fechaHasta == null || fechaHasta.length() == 0)
		{
			this.setFechaHasta( null );
			return;
		}
		this.setFechaHasta( DateHelper.getDate( fechaHasta ) );
	}
	public String getObservaciones() 
	{
		return this.observaciones;
	}
	public void setObservaciones(String observaciones) 
	{
		this.observaciones = observaciones;
	}
	public String getAreaResponsable() {
		return areaResponsable;
	}
	public void setAreaResponsable(String areaResponsable) {
		this.areaResponsable = areaResponsable;
	}
	
}
