package com.asf.cred.estados;

import java.util.Comparator;
import java.util.Date;

import com.asf.cred.business.EstadosPersonas;
import com.asf.util.DateHelper;

/**
 * Este comparador se utiliza para ordenar los movimientos de estados
 * y observaciones de una persona.-
 * Ordena por:
 * 1) Fecha Desde del movimiento.-
 * 2) Fecha Hasta del movimiento.-
 * 2) Tipo de Movimiento.-
 * 3) Id de Movimiento.-
 */
public class ComparadorEstaObs implements Comparator<String>
{
	public ComparadorEstaObs()
	{
		
	}
	
	public int compare(String key1, String key2) 
	{
		int resultado = -1;
		if(key1!=null && key2!=null)
		{
			String[] llave1 = key1.split(EstadosPersonas.separador);
			String[] llave2 = key2.split(EstadosPersonas.separador);
			
			if(llave1.length == 4 && llave2.length == 4)
			{
				//Llave 1.-
				Date primervalor1  = DateHelper.getDate(llave1[0]);
				String segundovalor1 = llave1[1];
				Long tercervalor1 = new Long(llave1[2]);
				Long cuartovalor1  = new Long(llave1[3]);
				
				//Llave 2.-
				Date primervalor2  = DateHelper.getDate(llave2[0]);
				String segundovalor2 = llave2[1];
				Long tercervalor2 = new Long(llave2[2]);
				Long cuartovalor2  = new Long(llave2[3]);
				
				//1)
				if(primervalor1.equals(primervalor2))
				{
					//Si tienen la misma fecha desde, ordeno por fecha hasta.-
					//2)
					if(segundovalor1.equals(segundovalor2))
					{
						//Si tienen la misma fecha hasta, ordeno por tipo de movimiento.-
						if(tercervalor1.longValue() == tercervalor2.longValue())
						{
							//Si tienen el mismo tipo de movimiento, ordeno por id.-
							//3)
							if(cuartovalor1.longValue() == cuartovalor2.longValue())
							{
								resultado = 0;
							}
							else
							{
								resultado = cuartovalor1.longValue() < cuartovalor2.longValue() ? -1 : 1;
							}
						}
						else
						{
							resultado = tercervalor1.longValue() < tercervalor2.longValue() ? -1 : 1;
						}
					}
					else
					{
						//Si alguna de las fechas es vacia.-
						if(segundovalor1.equals("0"))
						{
							resultado = -1;
						}
						else if(segundovalor2.equals("0"))
						{
							resultado = 1;
						}
						else
						{
							Date fechaHasta1 = DateHelper.getDate(segundovalor1);
							Date fechaHasta2 = DateHelper.getDate(segundovalor2);
							
							resultado = fechaHasta1.before(fechaHasta2) ? 1 : -1;
						}
					}
				}
				else
				{
					resultado = primervalor1.before(primervalor2) ? 1 : -1;
				}
			}
		}
		
		return resultado;
	}
}