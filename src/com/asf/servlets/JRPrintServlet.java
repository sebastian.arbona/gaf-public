package com.asf.servlets;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;


import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asf.security.SessionHandler;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;

public class JRPrintServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public JRPrintServlet() {
		super();
	}
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		
		ServletContext context = this.getServletConfig().getServletContext();
				
		File reportFile = new File(context.getRealPath("/applets/reporte.jasper"));
		if (!reportFile.exists())
			throw new JRRuntimeException("File WebappReport.jasper not found. The report design must be compiled first.");

		Map parameters = new HashMap();
//		parameters.put("ReportTitle", "Address Report");
//		parameters.put("BaseDir", reportFile.getParentFile());
					
		JasperPrint jasperPrint = null;
		
				
		try
		{
			Connection cn = SessionHandler.getSessionHandler().getBusinessPersistance().getCurrentSession().connection();
//			String driver = "oracle.jdbc.driver.OracleDriver";
//			String url = "jdbc:oracle:thin:@sagitarius:1521:civitas";
//			String user = "grh";
//			String password = "grh";
//			
//			Class.forName(driver);	
//			
//			 Connection cn=  DriverManager.getConnection( url, user, password );	
			
			JasperReport jasperReport = (JasperReport)JRLoader.loadObjectFromFile(reportFile.getPath());
			
			jasperPrint = 
				JasperFillManager.fillReport(
					jasperReport,
					parameters, 
					cn
					);
		}
		catch (JRException e)
		{ 
			response.setContentType("text/html");
			response.setCharacterEncoding("iso-8859-1");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>JasperReports - Web Application Sample</title>");
			out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
			out.println("</head>");
			
			out.println("<body bgcolor=\"white\">");

			out.println("<span class=\"bnew\">JasperReports encountered this error :</span>");
			out.println("<pre>");

			e.printStackTrace(out);

			out.println("</pre>");

			out.println("</body>");
			out.println("</html>");

			return;
		}catch(Exception e){}

		if (jasperPrint != null)
		{
			response.setContentType("application/octet-stream");
			ServletOutputStream ouputStream = response.getOutputStream();
			
			ObjectOutputStream oos = new ObjectOutputStream(ouputStream);
			oos.writeObject(jasperPrint);
			oos.flush();
			oos.close();

			ouputStream.flush();
			ouputStream.close();
		}
		else
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("<head>");
			out.println("<title>JasperReports - Web Application Sample</title>");
			out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\" title=\"Style\">");
			out.println("</head>");
			
			out.println("<body bgcolor=\"white\">");
	
			out.println("<span class=\"bold\">Empty response.</span>");
	
			out.println("</body>");
			out.println("</html>");
		}
	}

}
