package com.asf.util;

public class EscapeHelper {
	public static String escapeXML(String xml){
		return xml.replace("<", "&lt;").replace(">", "&gt;");
	}
	
	public static String escapeQL(String xml){
		return xml.replace("'", "''");
	}
	
	

}
