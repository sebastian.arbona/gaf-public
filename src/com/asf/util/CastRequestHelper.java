package com.asf.util;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.asf.util.DateHelper;

public final class CastRequestHelper {
	
	public static Integer toInteger( String key, HttpServletRequest request ) {
		Integer res = (request.getParameter(key) != null && !request.getParameter(key).isEmpty()) ? new Integer(request.getParameter(key)) :0;
		return res;
	}
	
	public static Long toLong( String key, HttpServletRequest request ) {
		Long res = (request.getParameter(key) != null && !request.getParameter(key).isEmpty()) ? new Long (request.getParameter(key)) :null;
		return res;
	}	
	
	public static String toString( String key, HttpServletRequest request ) {
		String res = (request.getParameter(key) != null && !request.getParameter(key).isEmpty()) ? new String (request.getParameter(key)) :null;
		return res;
	}
	
	public static Date toDate( String key, HttpServletRequest request ) {
		Date res = (request.getParameter(key) != null && !request.getParameter(key).isEmpty()) ? DateHelper.getDate(request.getParameter(key)): null;
		return res;
	}	
	
	public static Boolean toBoolean( String key, HttpServletRequest request ) {
		Boolean res =  (request.getParameter(key) != null && !request.getParameter(key).isEmpty()) ? new Boolean (request.getParameter(key)) :null;
		return res;
	}	
	
	public static boolean toBool( String key, HttpServletRequest request ) {
		String val = request.getParameter(key);
		if(val == null || val.isEmpty()) {
			return false;
		}else {
			if(val.equals("1") || val.equals("on")) {
				return true;
			}else {
				return false;
			}
		}
	}	
	
	public static Float toFloat( String key, HttpServletRequest request ) {
		Float res = (request.getParameter(key) != null && !request.getParameter(key).isEmpty()) ? new Float (request.getParameter(key)) :null;
		return res;
	}
}
