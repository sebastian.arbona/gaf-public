package com.nirven.creditos.bpm;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.nirven.creditos.hibernate.Objetoi;

public class TipoDeDocumentoHelper implements ActionHandler {

    private static final long serialVersionUID = 6323475351098240922L;

    @Override
    public void execute(ExecutionContext ec) throws Exception {
	try {
		Objetoi o = (Objetoi) ec.getVariable("objetoi");
	    String tipo = o.getLinea().getTipoExpediente();
	    Object[] os = DocumentoHelper.invoke("getNumeradorAutomatico", new String[] { tipo });
	    ec.setVariable("numeradorAutomatico", os[0]);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
