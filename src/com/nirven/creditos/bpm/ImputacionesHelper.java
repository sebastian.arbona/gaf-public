package com.nirven.creditos.bpm;

import org.codehaus.xfire.client.Client;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.asf.security.SessionHandler;
import com.asf.util.DirectorHelper;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Desembolso;
import com.nirven.creditos.hibernate.Objetoi;

public class ImputacionesHelper implements ActionHandler {

	private static final long serialVersionUID = 1L;
	Long etapa;
	Long tipoExpediente;
	Long idCredito;
	Long idEstado;
	String expediente;

	@Override
	public void execute(ExecutionContext ec) throws Exception {
		Objetoi o = (Objetoi) ec.getVariable("objetoi");
		try {
			this.imputar(o);
			ec.getToken().signal("ok");
		} catch (Exception e) {
			e.printStackTrace();
			ec.setVariable("errCreacionExpediente", e.getMessage());
			ec.getToken().signal("error");
		}
	}

	public void imputar(Objetoi o) throws Exception {
		Object[] params = new Object[] { new Long(etapa), new Long(tipoExpediente), o.getId(), o.getExpediente() };
		this.invoke("imputar", params);
	}

	public void imputar(Desembolso d) throws Exception {
		Object[] params = new Object[] { new Long(etapa), new Long(tipoExpediente), d.getId() };
		this.invoke("imputarDesembolso", params);
	}

	public void imputarDesembolsoDesistido(Desembolso d) throws Exception {
		Object[] params = new Object[] { new Long(etapa), new Long(tipoExpediente), d.getCredito().getId(),
				d.getCredito().getExpediente(), d.getId() };
		this.invoke("imputarDesembolsoDesistido", params);
	}

	public void imputar(Long etapaDesde, Long etapaHasta, Long idTipoDeExpediente, Long idObjetoi, String expediente,
			Double importe, Long idIEgreso, Long idInstitucional, Long idNomenclador) throws Exception {
		Object[] params = new Object[] { etapaDesde, etapaHasta, idTipoDeExpediente, idObjetoi, expediente, importe,
				idIEgreso, idInstitucional, idNomenclador };
		this.invoke("imputarEtapas", params);
	}

	private Object[] invoke(String metodo, Object[] params) throws Exception {
		String urlGAF = DirectorHelper.getString("URL.GAF");
		Object[] resultado;
		if (urlGAF == null || urlGAF.trim().isEmpty()) {
			resultado = null;
		} else {
			SessionHandler sh = SessionHandler.getCurrentSessionHandler();
			String url = urlGAF + "/services/Imputaciones?wsdl";
			Client c = WebServiceHelper.getCliente(url);
			c.addOutHandler(sh.getAutenticacionClienteHandler());
			resultado = c.invoke(metodo, params);
		}
		return resultado;

	}

	public void desimputar() throws Exception {
		Object[] params = new Object[] { new Long(idCredito), idEstado };
		this.invoke("desimputarCredito", params);
	}

	public Long getEtapa() {
		return etapa;
	}

	public void setEtapa(Long etapa) {
		this.etapa = etapa;
	}

	public Long getTipoExpediente() {
		return tipoExpediente;
	}

	public void setTipoExpediente(Long tipoExpediente) {
		this.tipoExpediente = tipoExpediente;
	}

	public Long getIdCredito() {
		return idCredito;
	}

	public String getExpediente() {
		return expediente;
	}

	public void setIdCredito(Long idCredito) {
		this.idCredito = idCredito;
	}

	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
}
