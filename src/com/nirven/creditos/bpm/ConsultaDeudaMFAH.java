package com.nirven.creditos.bpm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.ConsultaDeuda;
import com.nirven.creditos.hibernate.ConsultaDeuda.EntidadEnum;
import com.nirven.creditos.hibernate.ConsultaDeudaPersona;
import com.nirven.creditos.hibernate.ConsultaDeudaRespuesta;
import com.nirven.creditos.hibernate.Objetoi;

public class ConsultaDeudaMFAH implements ActionHandler {

	private static final long serialVersionUID = 1L;

	@Override
	public void execute(ExecutionContext ec) throws Exception {

		String resultado = "sinDeuda";
		ConsultaDeuda consulta = (ConsultaDeuda) ec.getVariable("consulta");

		if (!consulta.isFt())
			return;

		for (ConsultaDeudaPersona p : consulta.getPersonas()) {
			if (this.isTieneDeuda(p)) {
				resultado = "conDeuda";
				break;
			}
		}

		ec.getToken().signal(resultado);
	}

	private boolean isTieneDeuda(ConsultaDeudaPersona p) {
		return false;
	}

}
