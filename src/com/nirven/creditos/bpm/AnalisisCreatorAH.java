package com.nirven.creditos.bpm;

import java.util.Date;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Analisis;
import com.nirven.creditos.hibernate.Objetoi;

public class AnalisisCreatorAH implements ActionHandler{
	private String tipo;
	private String situacion;
	private String variableName="analisisSolicitud";

	@Override
	public void execute(ExecutionContext ec) throws Exception {
		
		if(ec.getTaskInstance().getActorId()==null||ec.getVariable(variableName)!=null)
			return;
		Objetoi o=(Objetoi)ec.getVariable("objetoi");
		Analisis a=new Analisis();
		a.setObjetoi(o);
		a.setFechaIngreso(new Date());
		a.setTipo(tipo);
		a.setSituacion(situacion);
		a.setObservacion("");
		
		
		SessionHandler.getCurrentSessionHandler().getBusinessPersistance().save(a);
		ec.setVariable(variableName, a);
		
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

}
