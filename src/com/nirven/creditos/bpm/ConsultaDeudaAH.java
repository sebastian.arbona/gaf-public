package com.nirven.creditos.bpm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.gaf.DirectorHandler;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.ConsultaDeuda;
import com.nirven.creditos.hibernate.ConsultaDeudaPersona;
import com.nirven.creditos.hibernate.ConsultaDeudaRespuesta;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ConsultaDeuda.EntidadEnum;

/*
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
*/

public class ConsultaDeudaAH  implements ActionHandler{

	private static final long serialVersionUID = 1L;

	@Override
	public void execute(ExecutionContext ec) throws Exception {
		
		
		ConsultaDeuda consulta=(ConsultaDeuda)ec.getVariable("consulta");
		
		if(!consulta.isFt())
			return;
		
		for(ConsultaDeudaPersona p:consulta.getPersonas()){
			this.consultar(p);
		}
	}

/*
	@Override
	public void execute(ExecutionContext ec) throws Exception {
		ConsultaDeuda consulta=(ConsultaDeuda)ec.getVariable("consulta");
		
		if(consulta.isFt()) {
			for(ConsultaDeudaPersona p:consulta.getPersonas()){
				this.consultar(p);
			}
		}

		if(consulta.isFv()) {
			for(ConsultaDeudaPersona p:consulta.getPersonas()){
				this.obtenerMontoFondoVitivinicola(p);
			}
		}
	}
*/

	@SuppressWarnings({ "unchecked", "static-access" })
	private void consultar(ConsultaDeudaPersona cdp) throws Exception {
		BusinessPersistance bp=SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		long idRelacionCotomador = new Long(DirectorHandler.getDirector("relacion.cotomador").getValor()).longValue();
		List<Number> personaVinculada=new ArrayList<Number>();
		HashSet<Objetoi> objetos=new LinkedHashSet<Objetoi>();

		personaVinculada.addAll(bp.createSQLQuery("select p.personaTitular_IDPERSONA \n"+ 
				"from "+bp.getSchema()+".tipoRelacion tr \n"+
				"inner join "+bp.getSchema()+".relacion r on r.tipoRelacion_idTipoRelacion=tr.idTipoRelacion \n"+
				"inner join "+bp.getSchema()+".PersonaVinculada p on p.relacion_idRelacion=r.idRelacion \n"+
				"WHERE p.personaVinculada_IDPERSONA=:id \n" + 
				"AND ((tr.descripcion = 'Cotomador' or tr.idTipoRelacion = 5) \n" + 
				"OR ((tr.descripcion = 'Comercial' OR tr.idTipoRelacion = 4) AND (r.nombreRelacion = 'Fiador' OR r.idRelacion = 21))) \n"  
				).setLong("id", cdp.getPersona().getId()).list());

		// buscar los creditos de todas las personas titulares;
		objetos.addAll(bp.createQuery("SELECT o FROM Objetoi o WHERE o.persona.idpersona = :idPersona ").setLong("idPersona", cdp.getPersona().getId()).list());
		
		// buscar los creditos de todas las personas vinculadas;
		for(Number personaV:personaVinculada)
			objetos.addAll(bp.createQuery("SELECT o FROM Objetoi o WHERE o.id IN "
					+ "(SELECT p.idObjetoi FROM PersonaVinculada p WHERE personaTitular_IDPERSONA = :idPersonaVinculada AND personaVinculada_IDPERSONA = :idPersona AND relacion_idRelacion = :idRelacionCotomador) ORDER BY o.expediente ASC")
			.setLong("idPersonaVinculada", personaV.longValue()).setLong("idPersona", cdp.getPersona().getId()).setLong("idRelacionCotomador", idRelacionCotomador).list());
		
		double deuda=0.0;
		Date fechaVto=null;
		for(Objetoi o:objetos){
			if(!o.getEstadoActual().getEstado().getNombreEstado().equals("ORIGINAL CON ACUERDO")){	//excluir los que estén en original con acuerdo
				CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
				calculoDeuda.calcular();
				if(calculoDeuda.getTotal().getSaldoCuota()>0.05){
					deuda+=calculoDeuda.getTotal().getSaldoCuota();
					Date f=calculoDeuda.getPrimerVtoImpago();
					if(f!=null&&(fechaVto==null||fechaVto.after(f)))
						fechaVto=f;			
				}
			}
		}
		ConsultaDeudaRespuesta r=cdp.getRespuesta(EntidadEnum.FTYC);
		if(r.getFechaConsulta()==null)
			r.setFechaConsulta(new Date());
		if(r.getFechaVencimiento()==null||(fechaVto!=null&&r.getFechaVencimiento().after(fechaVto)))
			r.setFechaVencimiento(fechaVto);
		if(r.getMonto()==null)
			r.setMonto(0.0);
		r.setMonto(Math.round((deuda+r.getMonto())*100.0)/100.0);
		if(r.getObservaciones()==null||r.getObservaciones().isEmpty())
			r.setObservaciones("Consulta realizada automáticamente");
		
		bp.saveOrUpdate(r);
	}

/*
	private void obtenerMontoFondoVitivinicola(ConsultaDeudaPersona cdp) {
		try {
			BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
			ConsultaDeudaRespuesta consultaFV = cdp.getRespuesta(EntidadEnum.Fondo_Vitivinicola);

			consultaFV.setMonto(new Double(0));
			consultaFV.setFechaConsulta(new Date());
			consultaFV.setObservaciones("Consulta realizada automáticamente");

			if(cdp.getPersona().getCuil12() != 0) {
				Class.forName("org.firebirdsql.jdbc.FBDriver");
				Connection connection = DriverManager.getConnection(
					"jdbc:firebirdsql:ftyc.no-ip.org/3050:d:\\teagro\\fondovimza\\db\\fovimzanew.gdb",
					"afovimza", "fovimza10");

				PreparedStatement p =  connection.prepareStatement("select * from v_saldos_cuit('" + cdp.getPersona().getCuil12Str() + "')");
				p.execute();
				ResultSet rs  = p.getResultSet();

				while(rs.next()) {
					consultaFV.setMonto(Math.round(rs.getDouble(2)*100.0)/100.0);
					consultaFV.setFechaConsulta(new Date());
				}
			}
			else
				consultaFV.setObservaciones("Persona sin CUIL");

			bp.saveOrUpdate(consultaFV);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/	
}
