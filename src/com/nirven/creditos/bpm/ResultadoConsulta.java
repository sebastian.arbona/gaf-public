package com.nirven.creditos.bpm;

import java.util.Date;

public class ResultadoConsulta{
	Double valor;
	Date fechaVencimiento;
	
	
	public ResultadoConsulta(Double valor, Date fechaVencimiento) {
		super();
		this.valor = valor;
		this.fechaVencimiento = fechaVencimiento;
	}
	
	
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	
}
