package com.nirven.creditos.bpm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;


import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.asf.cred.business.CreditoCalculoDeuda;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DirectorHelper;
import com.asf.util.DoubleHelper;
import com.nirven.creditos.hibernate.ConsultaDeuda;
import com.nirven.creditos.hibernate.ConsultaDeuda.EntidadEnum;
import com.nirven.creditos.hibernate.ConsultaDeudaPersona;
import com.nirven.creditos.hibernate.ConsultaDeudaRespuesta;
import com.nirven.creditos.hibernate.Objetoi;

public class ConsultaDeudaAHFt implements ActionHandler {

	private static final long serialVersionUID = 1L;

	@Override
	public void execute(ExecutionContext ec) throws Exception {

		ConsultaDeuda consulta = (ConsultaDeuda) ec.getVariable("consulta");

		if (!consulta.isFt())
			return;

		for (ConsultaDeudaPersona p : consulta.getPersonas()) {
			this.consultar(p);
		}
	}

	@SuppressWarnings({ "unchecked", "static-access" })
	private void consultar(ConsultaDeudaPersona cdp) throws Exception {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		ResultadoConsulta rc=getDeuda(cdp.getPersona().getId());
		
		ConsultaDeudaRespuesta r = cdp.getRespuesta(EntidadEnum.FTYC);
		if (r.getFechaConsulta() == null) {
			r.setFechaConsulta(new Date());
		}
		if (r.getFechaVencimiento() == null || (rc.getFechaVencimiento() != null && r.getFechaVencimiento().after(rc.getFechaVencimiento()))) {
			r.setFechaVencimiento(rc.getFechaVencimiento());
		}
		if (r.getMonto() == null) {
			r.setMonto(0.0);
		}
		r.setMonto(DoubleHelper.redondear(rc.getValor() + r.getMonto()));
		if (r.getObservaciones() == null || r.getObservaciones().isEmpty()) {
			r.setObservaciones("Consulta realizada automáticamente");
		}

		bp.saveOrUpdate(r);
	}

	public ResultadoConsulta getDeuda(Long id) {
		BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();
		long idRelacionCotomador = DirectorHelper.getLong("relacion.cotomador", -1l);
		List<Number> personaVinculada = new ArrayList<Number>();
		HashSet<Objetoi> objetos = new LinkedHashSet<Objetoi>();
		personaVinculada.addAll(bp.createSQLQuery("select p.personaTitular_IDPERSONA from " + bp.getSchema()
				+ ".tipoRelacion tr inner join " + bp.getSchema()
				+ ".relacion r on r.tipoRelacion_idTipoRelacion = tr.idTipoRelacion inner join " + bp.getSchema()
				+ ".PersonaVinculada p on p.relacion_idRelacion = r.idRelacion "
				+ "WHERE p.personaVinculada_IDPERSONA = :id "
				+ "AND ((tr.descripcion = 'Cotomador' or tr.idTipoRelacion = 5) "
				+ "OR ((tr.descripcion = 'Comercial' OR tr.idTipoRelacion = 4) AND (r.nombreRelacion = 'Fiador' OR r.idRelacion = 21)))")
				.setLong("id", id).list());

		// buscar los creditos de todas las personas titulares;
		objetos.addAll(bp.createQuery("SELECT o FROM Objetoi o WHERE o.persona.idpersona = :idPersona ")
				.setLong("idPersona", id).list());

		// buscar los creditos de todas las personas vinculadas;
		for (Number personaV : personaVinculada)
			objetos.addAll(bp.createQuery("SELECT o FROM Objetoi o WHERE o.id IN "
					+ "(SELECT p.idObjetoi FROM PersonaVinculada p WHERE personaTitular_IDPERSONA = :idPersonaVinculada AND personaVinculada_IDPERSONA = :idPersona AND relacion_idRelacion = :idRelacionCotomador) ORDER BY o.expediente ASC")
					.setLong("idPersonaVinculada", personaV.longValue()).setLong("idPersona", id)
					.setLong("idRelacionCotomador", idRelacionCotomador).list());

		double deuda = 0.0;
		Date fechaVto = null;
		for (Objetoi o : objetos) {
			// excluir los que estén en original con acuerdo
			if (!o.getEstadoActual().getEstado().getNombreEstado().equals("ORIGINAL CON ACUERDO")) {
				if (("EJECUCION,PRIMER DESEMBOLSO,PENDIENTE SEGUNDO DESEMBOLSO,GESTION EXTRAJUDICIAL,CONTRATO EMITIDO,ESPERANDO DOCUMENTACION,GESTION JUDICIAL,ACUERDO INCUMPLIDO,INCOBRABLE,")
						.contains(o.getEstadoActual().getEstado().getNombreEstado() + ",")) {
					CreditoCalculoDeuda calculoDeuda = new CreditoCalculoDeuda(o.getId(), new Date());
					calculoDeuda.calcular();
					if (calculoDeuda.getTotal().getSaldoCuota() > 0.05) {
						deuda += calculoDeuda.getTotal().getSaldoCuota();
						Date f = calculoDeuda.getPrimerVtoImpago();
						if (f != null && (fechaVto == null || fechaVto.after(f)))
							fechaVto = f;
					}
				}
			}
		}
		return new ResultadoConsulta(deuda, fechaVto);
	}


}
