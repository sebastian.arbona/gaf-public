package com.nirven.creditos.bpm;

import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.xfire.aegis.AegisBindingProvider;
import org.codehaus.xfire.aegis.type.TypeMapping;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.service.Service;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;

import com.asf.gaf.hibernate.documentos.DocumentoADE;
import com.asf.security.BusinessPersistance;
import com.asf.security.SessionHandler;
import com.asf.util.DirectorHelper;
import com.civitas.util.WebServiceHelper;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.ObjetoiArchivo;
import com.nirven.creditos.hibernate.Usuario;
import com.nirven.expedientes.persistencia.Unidad;
import com.nirven.expedientes.tools.DocumentoData;
import com.nirven.jcr.VariableFileJCR;

public class DocumentoHelper implements ActionHandler {

    private static final long serialVersionUID = 163895542373920815L;
    //public static final String TIPO_EXPEDIENTE = "EXP";

    @Override
    public void execute(ExecutionContext ec) throws Exception {
	DocumentoData dd = new DocumentoData();
	Objetoi o = (Objetoi) ec.getVariable("objetoi");
	BusinessPersistance bp = SessionHandler.getCurrentSessionHandler().getBusinessPersistance();

	try {
	    dd.setAsunto("Solicitud de Cr�dito - " + o.getLinea().getNombre());
	    dd.setCodigoTipoDeDocumento(o.getLinea().getTipoExpediente());
	    dd.setConfidencialidadId(2l);
	    if (ec.getVariable("numeradorAutomatico") == null || !(Boolean) ec.getVariable("numeradorAutomatico")) {
	    	dd.setIdentificacion(o.getExpediente());
	    }

	    dd.setCuerpos(new Integer(ec.getVariable("cantCuerpos").toString()));
	    dd.setFojas(new Integer(ec.getVariable("cantFojas").toString()));
	    dd.setDetalle("Creaci�n autom�tica de Expediente");
	    dd.setDomicilioIniciador(o.getPersona().getDomicilioCompleto());
	    dd.setNombreIniciador(o.getPersona().getNomb12());
	    dd.setPrioridadId(1l);
	    // dd.setIdentificacionIniciador();
	    dd.setIdIniciador(o.getPersona().getId());
	    Long temaId = new Long(DirectorHelper.getString("BPM.TemaInicialExpte"));
	    dd.setTemasId(new Long[] { new Long(temaId) });

	    Object[] ret = this.invoke("crearDocumento", new Object[] { dd });
	    o.setExpediente(ret[0].toString());
	    o.setFechaExpediente(new Date());
	    ec.setVariable("transferir", "1");

	    // Lo transfiero al expediente
	    if (o.getLinea().getResponsable() != null) {

			Usuario usuarioDestino = o.getLinea().getResponsable();
			Usuario usuarioOrigen = (Usuario) bp.getById(Usuario.class,
				SessionHandler.getCurrentSessionHandler().getCurrentUser());
			 if (usuarioOrigen!=null&&
					 usuarioDestino.getUnidad()!= null&&
					 usuarioOrigen.getUnidad()!=null&&
					 !usuarioDestino.getUnidad().equals(usuarioOrigen.getUnidad())) {
				DocumentoADE d = new DocumentoADE();
				d.setIdentificacion(dd.getIdentificacion());
				d.setTipoDeDocumento(1l);
				d.setUnidadActual(usuarioOrigen.getUnidad());
				Long motivoId = new Long(DirectorHelper.getString("BPM.MotivoTransferExpte"));
				try {
				    String retTransf = this.transferir(d, usuarioDestino.getUnidad(),
					    SessionHandler.getCurrentSessionHandler().getCurrentUser(), dd.getFojas(), dd.getCuerpos(),
					    motivoId, "Transferencia autom�tica a Responsable de linea.", true);
				    ec.setVariable("UltimoRemito", retTransf);
				} catch (Exception e) {
				    e.printStackTrace();
				}
			 }
	    }

	    for (int i = 1; i < 6; i++) {
			VariableFileJCR v = new VariableFileJCR();
			v.setValue(ec.getVariable("archivo" + i));
			if (v.getValue() != null) {
			    ObjetoiArchivo oa = this.getObjetoiArchivo(o.getObjetoiArchivos(), v.getFileName());
			    if (oa == null) {
				oa = new ObjetoiArchivo();
			    }
			    o.getId();
			    oa.setCredito(o);
			    oa.setDetalle(ec.getVariable("descripcionArchivo" + i).toString());
			    oa.setOrigen(ec.getVariable("tipoArchivo" + i).toString());
			    oa.setArchivo(IOUtils.toByteArray(v.getData()));
			    oa.setFechaAdjunto(new Date());
			    oa.setMimetype(v.getMimeType());
			    oa.setNombre(v.getFileName());
			    oa.setUsuario(SessionHandler.getCurrentSessionHandler().getCurrentUser());
			    bp.saveOrUpdate(oa);
			    ec.getContextInstance().deleteVariable("archivo" + i);
			}
	    }
	    ec.getToken().signal("ok");
	} catch (Exception e) {
	    e.printStackTrace();
	    o.setExpediente("");
	    o.setFechaExpediente(null);
	    ec.setVariable("errCreacionExpediente", e.getMessage());
	    ec.getToken().signal("error");
	}
    }

    private ObjetoiArchivo getObjetoiArchivo(List<ObjetoiArchivo> objetoiArchivos, String fileName) {
	if (objetoiArchivos == null || objetoiArchivos.isEmpty())
	    return null;
	for (ObjetoiArchivo oa : objetoiArchivos)
	    if (oa.getNombre().equals(fileName)) {
		return oa;
	    }
	return null;
    }

    public String transferir(DocumentoADE documento, Unidad destino, String usuarioOrigen, Integer fojas,
	    Integer cuerpos, Long idMotivo, String observaciones, boolean recepcionAutomatica) throws Exception {
	Object[] ret = this.invoke("transferir",
		new Object[] { documento.getTipoDeDocumento(), documento.getIdentificacion(),
			documento.getUnidadActual().getCodigo(), destino.getCodigo(), usuarioOrigen, idMotivo, cuerpos,
			fojas, observaciones, new Boolean(recepcionAutomatica) });
	return ret[0].toString();
    }

    public String modificarIniciador(String expediente, String idIniciador, String nombreIniciador,
	    String domicilioIniciador) {
	Object[] params = new Object[] { expediente, idIniciador, nombreIniciador, domicilioIniciador };
	Object[] ret = null;
	try {
	    ret = this.invoke("modificarIniciador", params);
	    return ret[0].toString();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return (ret != null ? ret[0].toString() : null);
    }

    public static Object[] invoke(String metodo, Object[] params) throws Exception {
	SessionHandler sh = SessionHandler.getCurrentSessionHandler();
	String url = DirectorHelper.getString("URL.ADE") + "/services/Documentos?wsdl";
	Client c = WebServiceHelper.getCliente(url);
	Service model = c.getService();
	AegisBindingProvider bp = (AegisBindingProvider) model.getBindingProvider();
	TypeMapping typeMapping = bp.getTypeMapping(model);
	c.addOutHandler(sh.getAutenticacionClienteHandler());
	return c.invoke(metodo, params);
    }
}
