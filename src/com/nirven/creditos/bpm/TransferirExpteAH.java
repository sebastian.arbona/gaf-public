package com.nirven.creditos.bpm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jbpm.graph.def.ActionHandler;
import org.jbpm.graph.exe.ExecutionContext;
import com.asf.gaf.hibernate.documentos.DocumentoADE;
import com.asf.security.SessionHandler;
import com.nirven.creditos.hibernate.Objetoi;
import com.nirven.creditos.hibernate.Usuario;

public class TransferirExpteAH implements ActionHandler{
	private Long idMotivo=new Long(13);
	private String observaciones="Transferencia por proceso";

	private static Log log = LogFactory.getLog( TransferirExpteAH.class );
	@Override
	public void execute(ExecutionContext ec) throws Exception {
		boolean transferir=false;
		Usuario usuarioOrigen=null;
		
		if(ec.getTaskInstance().getActorId()==null||ec.getTaskInstance().getActorId().isEmpty()){			
			return;
		}
		try{
			usuarioOrigen=Usuario.load((String)ec.getVariable("PreviousActorId"));
		}catch(Exception e){
			
		}
		
		ec.setVariable("PreviousActorId", ec.getTaskInstance().getActorId());
		ec.setVariable("ResultadoTransferencia", "");
		ec.setVariable("UltimoRemito", "");
		
		if(usuarioOrigen==null||usuarioOrigen.getUnidad()==null){
			log.info("No se encontro el usuario origen o este no tiene cargada la unidad.");
			return;
		}
		
		if(SessionHandler.getCurrentSessionHandler().getCurrentUser().equals(usuarioOrigen.getId())){
			log.info("Usuario origen y destino iguales.");
			return;
		}
		
		try{
			transferir=new Integer((String)ec.getVariable("transferir")).intValue()>0;			
		}catch(Exception e){
			
		}
		
		
		if(!transferir)
			return;
		if(ec.getVariable("objetoi")==null){
			log.info("No encontre el Objetoi");
			return;
		}
		Objetoi o=(Objetoi)ec.getVariable("objetoi");
		if(o.getExpediente()==null||o.getExpediente().isEmpty()){
			log.info("El objetoi no tiene expediente");
			return;
		}
		DocumentoADE d=DocumentoADE.load(o.getExpediente());
		if(d==null){
			log.info("No se encontro el expediente " + o.getExpediente());
			return;
		}
		if(!d.getUnidadActual().equals(usuarioOrigen.getUnidad())){
			log.info("El expediente no se encuentra en el area del usuario origen. ");
			ec.setVariable("ResultadoTransferencia", "No se pudo realizar la transferencia porque el expediente no se encuentra en el �rea del usuario de la tarea anterior. ");
			return;
		}
		Usuario u=Usuario.load(ec.getTaskInstance().getActorId());
		if(u==null||u.getUnidad()==null){
			log.info("El usuario no existe o no tiene unidad asignada");
			ec.setVariable("ResultadoTransferencia", "No se pudo realizar la transferencia porque el usuario no tiene cargada la unidad. ");
			return;
		}
		if(u.getUnidad().equals(d.getUnidadActual()))
			return;
		Integer fojas=new Integer((String)ec.getVariable("cantFojas"));
		Integer cuerpos=new Integer((String)ec.getVariable("cantCuerpos"));		
		if(fojas==null)
			fojas=d.getFojas();
		if(cuerpos==null)
			cuerpos=d.getCuerpos();
		DocumentoHelper dh=new DocumentoHelper();
		try{ 			
			String ret=dh.transferir(d, u.getUnidad(), usuarioOrigen.getId(), fojas, cuerpos, idMotivo, observaciones, true);
			ec.setVariable("ResultadoTransferencia", "Se gener� la transferencia de expediente de "+usuarioOrigen.getUnidad().getCodigo()+" a "+u.getUnidad().getCodigo()+" con remito "+ret);
			ec.setVariable("UltimoRemito", ret);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
	public Long getIdMotivo() {
		return idMotivo;
	}
	public void setIdMotivo(Long idMotivo) {
		this.idMotivo = idMotivo;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

}
